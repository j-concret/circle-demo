@RestResource(urlMapping='/addNotifyparties/*')
Global class AddNotifyPartiesToSO {
    
    @Httppost
    global static void AddNotifyPartiesToSO(){
        
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        Blob body = req.requestBody;
        res.addHeader('Content-Type', 'application/json');
        String requestString = body.toString();
        AddNotifyPartiesToSOwra rw = (AddNotifyPartiesToSOwra)JSON.deserialize(requestString,AddNotifyPartiesToSOwra.class);
        
        try {
            String Message;
            Access_token__c At =[select status__c,Access_Token__c from Access_token__c where Access_Token__c=:rw.Accesstoken ];
            if( at.status__C =='Active' ){
              
                List<Notify_Parties__c> newparties = new list<Notify_Parties__c>();
                list<contact> contlist =[select id,name, App_Status__c from contact where id in: rw.contactids and App_Status__c='Activated' and Include_in_SO_Communication__c = TRUE];
                if(contlist.isempty()){
                    message='There is no activated contacts found';
                }
                else{
                    map<ID,Notify_Parties__c> NotifyPartiesmap = new  map<ID,Notify_Parties__c>();
                    for(Notify_Parties__c existingnotifyparties : [SELECT Id, Contact__c, Shipment_Order__c, Name FROM Notify_Parties__c where Contact__c in:contlist and Shipment_Order__c =:rw.soid ]){
                        NotifyPartiesmap.put(existingnotifyparties.Contact__c,existingnotifyparties);
                    }
                  
                    for(contact con :contlist){
                        if(!NotifyPartiesmap.containskey(con.Id)){
                            Notify_Parties__c NP = new Notify_Parties__c (
                                Contact__c=con.Id,
                                Shipment_Order__c=rw.SOID
                            
                            );
                            newparties.add(NP);
                            
                        }
                        
                    }
                    message= 'New notify parties are inserted';  
                insert newparties;      
                    }
                
                    
                 }
        
                
                
                JSONGenerator gen = JSON.createGenerator(true);
                gen.writeStartObject();
                
                gen.writeFieldName('Success');
                gen.writeStartObject();
                
                if(Message == null) {gen.writeNullField('Message');} else{gen.writeStringField('Message', +Message );}
                
                gen.writeEndObject();
                gen.writeEndObject();
                String jsonData = gen.getAsString();
                res.responseBody = Blob.valueOf(jsonData);
                res.addHeader('Content-Type', 'application/json');
                res.statusCode = 200;        
                
                API_Log__c Al = New API_Log__c();
                Al.EndpointURLName__c='AddNotifyPartiesToSO';
                Al.Response__c= message;
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al; 
            }
        
        catch (Exception e){
            String ErrorString ='Something went wrong while updating cost estimate, please contact support_SF@tecex.com'+e.getMessage();
            res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
            res.addHeader('Content-Type', 'application/json');
            res.statusCode = 500;
            API_Log__c Al = New API_Log__c();
            Al.EndpointURLName__c='AddNotifyPartiesToSO';
            Al.Response__c=e.getMessage()+' at line number: '+e.getLineNumber();
            Al.StatusCode__c=string.valueof(res.statusCode);
            Insert Al;
        }
    }

}