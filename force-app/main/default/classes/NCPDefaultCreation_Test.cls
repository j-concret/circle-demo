@isTest
public class NCPDefaultCreation_Test {
    
    //Covering If Condition, when CPDefaults__c is not available
    public static testMethod void CPDefaultCreations_Test1(){
        
        //Test Data
        Account acc = TestDataFactory.myClient();
        Contact con = TestDataFactory.myContact();
        
        //Json Body
        String json = '{"DefaultPref" : [{"ClientID":"'+acc.Id+'","ContactID":"'+con.Id+'","Chargeable_Weight_Units":"KGs","ion_Batteries":"Yes","Order_Type":"Pro-Forma Quote",'+
            +'"Package_Dimensions":"Cm","Package_Weight_Units":"KGs","Second_hand_parts":"Yes","Service_Type":"IOR","TecEx_to_handle_freight":"Yes",'+
            +'"Country":"South Africa","DefaultCurrency":"CZK"}]}';
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
        req.requestURI = '/services/apexrest/NCPDefaultcreation'; 
        req.httpMethod = 'Post';
        req.addHeader('Content-Type', 'application/json');
        req.requestBody = Blob.valueof(json);
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
        NCPDefaultCreation.CPDefaultCreations();
        Test.stopTest(); 
    }
    
    //Covering else Condition, when CPDefaults__c is available
    public static testMethod void CPDefaultCreations_Test2(){
        
        //Test Data
        Account acc = TestDataFactory.myClient();
        Contact con = TestDataFactory.myContact();
        CPDefaults__c cpDef = new CPDefaults__c(Client_Account__c = acc.Id, Order_Type__c = 'Pro-Forma Quote');
        insert cpDef;
        
        //Json Body
        String json = '{"DefaultPref" : [{"ClientID":"'+acc.Id+'","ContactID":"'+con.Id+'","Chargeable_Weight_Units":"KGs","ion_Batteries":"Yes","Order_Type":"Pro-Forma Quote",'+
            +'"Package_Dimensions":"Cm","Package_Weight_Units":"KGs","Second_hand_parts":"Yes","Service_Type":"IOR","TecEx_to_handle_freight":"Yes",'+
            +'"Country":"South Africa","DefaultCurrency":"CZK"}]}';
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
        req.requestURI = '/services/apexrest/NCPDefaultcreation'; 
        req.httpMethod = 'Post';
        req.addHeader('Content-Type', 'application/json');
        req.requestBody = Blob.valueof(json);
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
        NCPDefaultCreation.CPDefaultCreations();
        Test.stopTest(); 
    }
    
    //Covering Catch Exception
    public static testMethod void CPDefaultCreations_Test3(){
        
        //Test Data
        Account acc = TestDataFactory.myClient();
        Contact con = TestDataFactory.myContact();
        
        //Json Body
        String json = '{"DefaultPref" : [{"ClientID":"","ContactID":"'+con.Id+'","Chargeable_Weight_Units":"KGs","ion_Batteries":"Yes","Order_Type":"Pro-Forma Quote",'+
            +'"Package_Dimensions":"Cm","Package_Weight_Units":"KGs","Second_hand_parts":"Yes","Service_Type":"IOR","TecEx_to_handle_freight":"Yes",'+
            +'"Country":"South Africa","DefaultCurrency":"CZK"}]}';
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
        req.requestURI = '/services/apexrest/NCPDefaultcreation'; 
        req.httpMethod = 'Post';
        req.addHeader('Content-Type', 'application/json');
        req.requestBody = Blob.valueof(json);
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
        NCPDefaultCreation.CPDefaultCreations();
        Test.stopTest(); 
    }    
}