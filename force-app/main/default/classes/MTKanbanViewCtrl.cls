public class MTKanbanViewCtrl {

    @AuraEnabled
    public static Map<String,Object> getTasks(Id soId,String displayedTo,String assignedToId,String hiddenTask){
        try {
            Boolean isNotApplicable =  (Boolean)JSON.deserialize(hiddenTask,Boolean.class);
            Set<Id> masterTaskIds = new Set<Id>();
            Map<String,CategoryWrapper> categories = getPicklistValues('Task','Task_Category__c');
            system.debug('categories :'+categories);
            Shipment_Order__c so = [SELECT Id,Name,IOR_CSE__c,IOR_CSE__r.Name,ICE__c,ICE__r.Name,Lead_AM__c,Lead_AM__r.Name,Lead_ICE__c,Lead_ICE__r.Name,Financial_Controller__c,Financial_Controller__r.Name,Compliance_Team__c,Compliance_Team__r.Name,Service_Manager__c,Service_Manager__r.Name,Vat_Team__c,Vat_Team__r.Name,Freight_co_ordinator__c,Freight_co_ordinator__r.Name FROM shipment_order__c WHERE Id =: soId];
            Set<Object> tecexPersons = getPersons(so);

            String query = 'SELECT Id,Manually_Override_Inactive_Field__c,Task_Category__c,ETA_Countdown__c,Shipment_Order__r.Mapped_Shipping_status__c ,Critical_Path_Task__c,IsNotApplicable__c,State__c,Master_Task__r.Set_Default_to_Inactive__c,Inactive__c,Blocks_Approval_to_Ship__c,Master_Task__c FROM Task WHERE Master_Task__c != null AND WhatId =:soId AND IsNotApplicable__c =:isNotApplicable';
            if(String.isNotBlank(assignedToId)) query += ' AND Assigned_to__c=:assignedToId';

            for(Task tsk : Database.query(query)) {
                masterTaskIds.add(tsk.Master_Task__c);
                if(categories.containsKey(tsk.Task_Category__c)) {
                    Map<String,List<TaskWrapper> > stateWithTasks = categories.get(tsk.Task_Category__c).stateWithTasks;
                    if(stateWithTasks.containsKey(tsk.State__c))
                        stateWithTasks.get(tsk.State__c).add(new TaskWrapper(tsk));
                    else
                        stateWithTasks.put(tsk.State__c,new List<TaskWrapper> {new TaskWrapper(tsk)});
                }
            }

            Map<Id,List<Id> > masterTaskPrerequisiteIds = new Map<Id,List<Id> >();
            for(Master_Task_Prerequisite__c mstPre : [SELECT id,Name, Master_Task_Dependent__c, Master_Task_Prerequisite__c FROM Master_Task_Prerequisite__c WHERE Master_Task_Dependent__c IN: masterTaskIds]) {
                if(masterTaskPrerequisiteIds.containsKey(mstPre.Master_Task_Prerequisite__c))
                    masterTaskPrerequisiteIds.get(mstPre.Master_Task_Prerequisite__c).add(mstPre.Master_Task_Dependent__c);
                else
                    masterTaskPrerequisiteIds.put( mstPre.Master_Task_Prerequisite__c,new List<Id> {mstPre.Master_Task_Dependent__c});
            }

            Set<Id> inactiveTasks = new Set<Id>();

            for(Task tsk : [SELECT Id,Master_Task__c FROM Task WHERE Master_Task__c IN:masterTaskPrerequisiteIds.keySet() AND WhatID =:soId AND State__c !='Resolved' AND State__c !='Not Applicable' AND IsNotApplicable__c = false]) {
                inactiveTasks.addAll(masterTaskPrerequisiteIds.get(tsk.Master_Task__c));
            }
            for(Master_Task_Template__c temp : [SELECT Id, Task_Title__c, State__c,Master_Task__r.Set_Default_to_Inactive__c, Master_Task__c,Master_Task__r.States__c,Master_Task__r.Task_Category__c, Displayed_to__c FROM Master_Task_Template__c where Master_Task__c IN:masterTaskIds AND Displayed_to__c =:displayedTo]) {

                if(categories.containsKey(temp.Master_Task__r.Task_Category__c)) {
                    Map<String,List<TaskWrapper> > stateWithTasks = categories.get(temp.Master_Task__r.Task_Category__c).stateWithTasks;
                    if(stateWithTasks.containsKey(temp.State__c)) {
                        Integer taskCount = 0;
                        for(TaskWrapper tsk :stateWithTasks.get(temp.State__c)) {
                            if(tsk.masterTaskId == temp.Master_Task__c) {
                                tsk.taskTitle = temp.Task_Title__c;
                                tsk.mtStates = temp.Master_Task__r.States__c.split(';');
                                if(!tsk.manualOverride) {
                                    tsk.inActive = temp.Master_Task__r.Set_Default_to_Inactive__c ? temp.Master_Task__r.Set_Default_to_Inactive__c : inactiveTasks.contains(temp.Master_Task__c);
                                    if(tsk.inActive)
                                        tsk.sortOrder = tsk.inActive && !tsk.blockApproval ? 4 : !tsk.inActive && !tsk.blockApproval ? 3 : tsk.inActive && tsk.blockApproval ? 2 : 1;
                                }
                            }
                            if(String.isNotBlank(tsk.taskTitle)) {
                                taskCount += 1;
                            }
                        }
                        if(!categories.get(temp.Master_Task__r.Task_Category__c).isMoreTask)
                            categories.get(temp.Master_Task__r.Task_Category__c).isMoreTask = taskCount >1;
                    }
                }
            }
            return new Map<String,Object> {'so'=>so,'categories'=>categories.values(),'tecexPersons'=>tecexPersons};
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    public static Set<Object> getPersons(shipment_order__c so){
        Set<Object> tecexPersons = new Set<Object> {new Map<String,String> {'value'=>'','label'=>'All'}};
        if(so.IOR_CSE__c != null) {
            tecexPersons.add(new Map<String,String> {'value'=>so.IOR_CSE__c,'label'=>so.IOR_CSE__r.Name});
        }
        if(so.Lead_AM__c != null) {
            tecexPersons.add(new Map<String,String> {'value'=>so.Lead_AM__c,'label'=>so.Lead_AM__r.Name});
        }
        if(so.Lead_ICE__c != null) {
            tecexPersons.add(new Map<String,String> {'value'=>so.Lead_ICE__c,'label'=>so.Lead_ICE__r.Name});
        }
        if(so.ICE__c != null) {
            tecexPersons.add(new Map<String,String> {'value'=>so.ICE__c,'label'=>so.ICE__r.Name});
        }
        if(so.Financial_Controller__c != null) {
            tecexPersons.add(new Map<String,String> {'value'=>so.Financial_Controller__c,'label'=>so.Financial_Controller__r.Name});
        }
        if(so.Compliance_Team__c != null) {
            tecexPersons.add(new Map<String,String> {'value'=>so.Compliance_Team__c,'label'=>so.Compliance_Team__r.Name});
        }
        if(so.Service_Manager__c != null) {
            tecexPersons.add(new Map<String,String> {'value'=>so.Service_Manager__c,'label'=>so.Service_Manager__r.Name});
        }
        if(so.Vat_Team__c != null) {
            tecexPersons.add(new Map<String,String> {'value'=>so.Vat_Team__c,'label'=>so.Vat_Team__r.Name});
        }
        if(so.Freight_co_ordinator__c != null) {
            tecexPersons.add(new Map<String,String> {'value'=>so.Freight_co_ordinator__c,'label'=>so.Freight_co_ordinator__r.Name});
        }
        return tecexPersons;
    }

    public static Map<String,CategoryWrapper> getPicklistValues(String object_name, String field_name) {
        Map<String,CategoryWrapper> values = new Map<String,CategoryWrapper>();
        String[] types = new String[] {object_name};
        Schema.DescribeSobjectResult[] results = Schema.describeSObjects(types);
        for(Schema.DescribeSobjectResult res : results) {
            for (Schema.PicklistEntry entry : res.fields.getMap().get(field_name).getDescribe().getPicklistValues()) {
                if (entry.isActive() && entry.getValue()!='Onboarding') {
                    values.put(entry.getValue(),new CategoryWrapper(entry.getValue()));
                }
            }
        }
        return values;
    }

    @AuraEnabled
    public static void updateTask(String taskIdState){
        try {
            List<String> taskDetail = taskIdState.split('_');
            if(taskDetail.size() < 2) throw new AuraHandledException('Some Error occurred.');
            update new Task(Id = taskDetail[0],State__c=taskDetail[1]);
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }

    }

    public class CategoryWrapper {
        @AuraEnabled public String categoryName;
        @AuraEnabled public Boolean isMoreTask =  false; // one of state contains 2 or more task.
        @AuraEnabled public Map<String,List<TaskWrapper> > stateWithTasks;

        public CategoryWrapper(String name){
            this.categoryName = name;
            this.stateWithTasks = new Map<String,List<TaskWrapper> >();
        }
    }

    public class TaskWrapper {
        @AuraEnabled public String taskId;
        @AuraEnabled public String state;
        @AuraEnabled public List<String> mtStates;
        @AuraEnabled public String taskTitle;
        @AuraEnabled public Integer sortOrder;
        @AuraEnabled public Boolean inActive;
        @AuraEnabled public Boolean manualOverride;
        @AuraEnabled public Boolean blockApproval;
        @AuraEnabled public String masterTaskId;
        @AuraEnabled public Boolean criticalPath;
        @AuraEnabled public Boolean isNegative;
        @AuraEnabled public String ETACountdown; 
        @AuraEnabled public String SOMappedStatus;

        public TaskWrapper(Task task){
            this.taskId = task.Id;
            this.state = task.State__c;
            this.inActive = task.Inactive__c; //? task.Inactive__c : task.Master_Task__r.Set_Default_to_Inactive__c;
            this.masterTaskId = task.Master_Task__c;
            this.mtStates = new List<String>();
            this.manualOverride = task.Manually_Override_Inactive_Field__c;
            this.blockApproval = task.Blocks_Approval_to_Ship__c;
            this.sortOrder = this.inActive && !this.blockApproval ? 4 : !this.inActive && !this.blockApproval ? 3 : this.inActive && this.blockApproval ? 2 : 1;
            this.criticalPath = task.Critical_Path_Task__c;
            this.ETACountdown = task.ETA_Countdown__c; 
            if((this.ETACountdown).substring (0,1) == '-'){
                this.isNegative = true;
            }
            if((this.ETACountdown).substring (0,1) == '.'){
               this.ETACountdown = '0'+task.ETA_Countdown__c;
            }
            this.SOMappedStatus = task.Shipment_Order__r.Mapped_Shipping_status__c;
        }
    }
}