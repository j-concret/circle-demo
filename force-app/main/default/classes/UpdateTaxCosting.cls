public with sharing class UpdateTaxCosting {
    @InvocableMethod(label='updateTaxCostings' description='update Taxes and duties recharge type costing')
    public static void updateCosting(List<Id> soid){
        System.debug('updateTaxesCostingWithTax ');
        List<shipment_order__c> shipmentOrdersList = [SELECT Id, Name,  Insurance_Cost_CIF__c, Shipment_Value_USD__c, IOR_Fee_new__c, FC_IOR_and_Import_Compliance_Fee_USD__c, FC_Total_Customs_Brokerage_Cost__c,
                                                      FC_Total_Handling_Costs__c, FC_Total_License_Cost__c, FC_Total_Clearance_Costs__c, FC_Insurance_Fee_USD__c, FC_Miscellaneous_Fee__c, On_Charge_Mark_up__c, FC_International_Delivery_Fee__c,
                                                      TecEx_Shipping_Fee_Markup__c, CPA_v2_0__c, Chargeable_Weight__c, Account__c, Service_Type__c, Total_Taxes__c, of_Line_Items__c, FC_Total__c, of_Unique_Line_Items__c, CPA_Costings_Added__c,
                                                      of_packages__c, Final_Deliveries_New__c, IOR_Refund_of_Tax_Duty__c,Tax_Cost__c FROM Shipment_Order__c WHERE Id IN :soid];

        Map<Id,List<CPA_Costing__c> > soCostings = new Map<Id,List<CPA_Costing__c> >();
        Set<String> currencies = new Set<String>();

        for(CPA_Costing__c cost : [SELECT Id, Name, DeleteAll__c, Shipment_Order_Old__c, IOR_EOR__c,Amount__c, Applied_to__c, Ceiling__c, Conditional_value__c, Condition__c, Floor__c,Cost_Category__c, Cost_Type__c, Max__c, Min__c, Rate__c, CostStatus__c, Updating__c, RecordTypeID, VAT_Rate__c, VAT_applicable__c, Variable_threshold__c,Threshold_Percentage__c, Within_Threshold__c, Currency__c, Supplier_Invoice__c,Additional_Percent__c, Amended__c, Cost_Note__c FROM CPA_Costing__c where Cost_Category__c = 'Tax' AND Shipment_Order_Old__c IN: soid]) {
            currencies.add(cost.Currency__c);
            if(soCostings.containsKey(cost.Shipment_Order_Old__c))
                soCostings.get(cost.Shipment_Order_Old__c).add(cost);
            else
                soCostings.put(cost.Shipment_Order_Old__c,new List<CPA_Costing__c> {cost});
        }

        Map<String,Currency_Management2__c> costingCurrency = new Map<String,Currency_Management2__c>();
        for(Currency_Management2__c curr : [SELECT Name, Conversion_Rate__c, ISO_Code__c, Currency__c FROM Currency_Management2__c WHERE Currency__c IN: currencies]) {
            costingCurrency.put(curr.Currency__c,curr);
        }

        List<CPA_Costing__c> costingsList = new List<CPA_Costing__c>();

        for(shipment_order__c shipOrder : shipmentOrdersList) {
            if(soCostings.containsKey(shipOrder.Id)) {
                for(CPA_Costing__c cost : soCostings.get(shipOrder.Id)) {
                    if(cost.Name =='Taxes and duties recharge') {
                        cost.Amount__c = shipOrder.Total_Taxes__c;
                    }
                    SOTriggerHelper.calculateSOCostingAmount(shipOrder, cost,costingCurrency.get(cost.Currency__c));
                    costingsList.add(cost);
                }
            }
        }
        if(!costingsList.isEmpty()) {
            RecusrsionHandler.isBeforeUpdate = false;
            update costingsList;
        }
    }
}