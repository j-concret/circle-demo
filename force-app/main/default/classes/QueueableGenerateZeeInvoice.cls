public class QueueableGenerateZeeInvoice implements Queueable {

    Set<Id> invoiceList;
    public QueueableGenerateZeeInvoice(Set<Id> invoiceList){
        this.invoiceList = invoiceList;
    }

    public void execute(QueueableContext context) {
        CTRL_generateAndSendInvoice.SaveInvoiceUsingTrigger(invoiceList);
    }
}