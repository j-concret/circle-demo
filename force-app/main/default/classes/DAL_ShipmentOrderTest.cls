@isTest
private class DAL_ShipmentOrderTest {
	
	@isTest static void TestDAL_ShipmentOrder() {
        
        //Preparing testing data 
        Shipment_Order__c shipmentOrderRecord = TestDataFactory.myShipmentOrder();
        Id shipmentOrderId              = shipmentOrderRecord.Id;
        Set<Id> idSet                         = new Set<Id>{shipmentOrderId};

        Test.startTest();
        
        //Class instantiation
        DAL_ShipmentOrder dalShipmentOrder         = new DAL_ShipmentOrder();
        
        //getSObjectFieldList 
        List<Schema.SObjectField> sobjectFieldList = dalShipmentOrder.getSObjectFieldList();

        //getSObjectType()
        Schema.SObjectType sobjectType             = dalShipmentOrder.getSObjectType();

        //selectById
        Shipment_Order__c singleOrderById          = dalShipmentOrder.selectById(shipmentOrderId);
        List<Shipment_Order__c> orderByIdSet       = dalShipmentOrder.selectById(idSet);

        Test.stopTest();

        //getSObjectFieldList assert
        System.assertEquals(2,sobjectFieldList.size());

        //getSObjectType() assert
        system.assert(shipmentOrderRecord.getsObjectType() == sobjectType);

        //selectById assert
        System.assert(shipmentOrderRecord.getSObjectType() == singleOrderById.getSObjectType());
        System.assertEquals(1, orderByIdSet.size());
    }
}