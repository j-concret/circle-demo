@IsTest
public class NCPUpdateCourierservice_Test {

    static testMethod void NCPGetCourierServicesTest(){
        
        Id accountRecTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Freight Supplier').getRecordTypeId();
        Id shipmentOrderRecTypeId = Schema.SObjectType.shipment_order__c.getRecordTypeInfosByName().get('Cost Estimate').getRecordTypeId();
        
          Access_token__c accessTokenObj = new Access_token__c();
        accessTokenObj.Status__c = 'Active';
        accessTokenObj.Access_Token__c = 'asdfghj-sdfgfds-sfdfgf';
        
        Insert accessTokenObj;
        
        Account acc = new Account(Name = 'Acme Test', RecordTypeId = accountRecTypeId);
        
        Insert acc;
         
         Country__c country = new Country__c(Brokerage_Costs__c = 618, CIF_Adjustment_Factor__c = 0.035, CIF_Absolute_value_adjustment__c = 200, Clearance_Costs__c = 488, 
                                            Country__c = 'Brazil', Name = 'Brazil', Handling_Costs__c = 1180, License_Permit_Costs__c = 0);
        insert country;
        
         CPA_v2_0__c RelatedCPA = new CPA_v2_0__c(Name = 'Brazil', Country_Applied_Costings__c=true );
        insert RelatedCPA;
        CPA_v2_0__c cpav2 = new CPA_v2_0__c(Name = 'Brazil Testing 2.0', Supplier__c = acc.Id, Service_Type__c = 'IOR/EOR', Related_Costing_CPA__c = RelatedCPA.Id, Preferred_Supplier__c = TRUE,
                                            VAT_Rate__c = 0.1, Destination__c = 'Brazil',  
                                            
                                            Default_Section_1_Address_Line_1__c= 'Address_Line_1' ,
                                            Default_Section_1_Address_Line_2__c= 'Address_Line_2' ,
                                            Default_Section_1_Address_Line_3__c= 'Address_Line_3' ,
                                            Default_Section_1_City__c= 'City' ,
                                            Default_Section_1_Zip__c= 'Zip' ,
                                            Default_Section_1_Country__c = 'Country' , 
                                            Default_Section_1_Other__c= 'Other' ,
                                            Default_Section_1_Tax_Name__c= 'Tax_Name' ,
                                            Default_Section_1_Tax_Id__c= 'Tax_Id' ,
                                            Default_Section_1_Contact_Name__c= 'Contact_Name' ,
                                            Default_Section_1_Contact_Email__c= 'Contact_Email' ,
                                            Default_Section_1_Contact_Tel__c= 'Contact_Tel' ,
                                            Alternate_Section_1_Address_Line_1__c= 'Address_Line_1' ,
                                            Alternate_Section_1_Address_Line_2__c= 'Address_Line_2' ,
                                            Alternate_Section_1_Address_Line_3__c= 'Address_Line_3' ,
                                            Alternate_Section_1_City__c= 'City' ,
                                            Alternate_Section_1_Zip__c= 'Zip' ,
                                            Alternate_Section_1_Country__c = 'Country' ,
                                            Alternate_Section_1_Other__c = 'Other' ,
                                            Alternate_Section_1_Tax_Name__c = 'Tax_Name' ,
                                            Alternate_Section_1_Tax_Id__c = 'Tax_Id' ,
                                            Alternate_Section_1_Contact_Name__c= 'Contact_Name' ,
                                            Alternate_Section_1_Contact_Email__c= 'Contact_Email' ,
                                            Alternate_Section_1_Contact_Tel__c= 'Contact_Tel', 
                                            Default_Section_2_Address_Line_1__c= 'Address_Line_1' ,
                                            Default_Section_2_Address_Line_2__c= 'Address_Line_2' ,
                                            Default_Section_2_Address_Line_3__c= 'Address_Line_3' ,
                                            Default_Section_2_City__c= 'City' ,
                                            Default_Section_2_Zip__c= 'Zip' ,
                                            Default_Section_2_Country__c = 'Country' , 
                                            Default_Section_2_Other__c= 'Other' ,
                                            Default_Section_2_Tax_Name__c= 'Tax_Name' ,
                                            Default_Section_2_Tax_Id__c= 'Tax_Id' ,
                                            Default_Section_2_Contact_Name__c= 'Contact_Name' ,
                                            Default_Section_2_Contact_Email__c= 'Contact_Email' ,
                                            Default_Section_2_Contact_Tel__c= 'Contact_Tel' ,
                                            Alternate_Section_2_Address_Line_1__c= 'Address_Line_1' ,
                                            Alternate_Section_2_Address_Line_2__c= 'Address_Line_2' ,
                                            Alternate_Section_2_Address_Line_3__c= 'Address_Line_3' ,
                                            Alternate_Section_2_City__c= 'City' ,
                                            Alternate_Section_2_Zip__c= 'Zip' ,
                                            Alternate_Section_2_Country__c = 'Country' ,
                                            Alternate_Section_2_Other__c = 'Other' ,
                                            Alternate_Section_2_Tax_Name__c = 'Tax_Name' ,
                                            Alternate_Section_2_Tax_Id__c = 'Tax_Id' ,
                                            Alternate_Section_2_Contact_Name__c= 'Contact_Name' ,
                                            Alternate_Section_2_Contact_Email__c= 'Contact_Email',
                                            Alternate_Section_2_Contact_Tel__c= 'Contact_Tel',
                                            Default_Section_3_Address_Line_1__c= 'Address_Line_1' ,
                                            Default_Section_3_Address_Line_2__c= 'Address_Line_2' ,
                                            Default_Section_3_Address_Line_3__c= 'Address_Line_3' ,
                                            Default_Section_3_City__c= 'City' ,
                                            Default_Section_3_Zip__c= 'Zip' ,
                                            Default_Section_3_Country__c = 'Country' , 
                                            Default_Section_3_Other__c= 'Other' ,
                                            Default_Section_3_Tax_Name__c= 'Tax_Name' ,
                                            Default_Section_3_Tax_Id__c= 'Tax_Id' ,
                                            Default_Section_3_Contact_Name__c= 'Contact_Name' ,
                                            Default_Section_3_Contact_Email__c= 'Contact_Email' ,
                                            Default_Section_3_Contact_Tel__c= 'Contact_Tel' ,
                                            Alternate_Section_3_Address_Line_1__c= 'Address_Line_1' ,
                                            Alternate_Section_3_Address_Line_2__c= 'Address_Line_2' ,
                                            Alternate_Section_3_Address_Line_3__c= 'Address_Line_3' ,
                                            Alternate_Section_3_City__c= 'City' ,
                                            Alternate_Section_3_Zip__c= 'Zip' ,
                                            Alternate_Section_3_Country__c = 'Country' ,
                                            Alternate_Section_3_Other__c = 'Other' ,
                                            Alternate_Section_3_Tax_Name__c = 'Tax_Name' ,
                                            Alternate_Section_3_Tax_Id__c = 'Tax_Id' ,
                                            Alternate_Section_3_Contact_Name__c= 'Contact_Name' ,
                                            Alternate_Section_3_Contact_Email__c= 'Contact_Email' ,
                                            Alternate_Section_3_Contact_Tel__c= 'Contact_Tel',
                                            Default_Section_4_Address_Line_1__c= 'Address_Line_1' ,
                                            Default_Section_4_Address_Line_2__c= 'Address_Line_2' ,
                                            Default_Section_4_Address_Line_3__c= 'Address_Line_3' ,
                                            Default_Section_4_City__c= 'City' ,
                                            Default_Section_4_Zip__c= 'Zip' ,
                                            Default_Section_4_Country__c = 'Country' , 
                                            Default_Section_4_Other__c= 'Other' ,
                                            Default_Section_4_Tax_Name__c= 'Tax_Name' ,
                                            Default_Section_4_Tax_Id__c= 'Tax_Id' ,
                                            Default_Section_4_Contact_Name__c= 'Contact_Name' ,
                                            Default_Section_4_Contact_Email__c= 'Contact_Email' ,
                                            Default_Section_4_Contact_Tel__c= 'Contact_Tel' ,
                                            Alternate_Section_4_Address_Line_1__c= 'Address_Line_1' ,
                                            Alternate_Section_4_Address_Line_2__c= 'Address_Line_2' ,
                                            Alternate_Section_4_Address_Line_3__c= 'Address_Line_3' ,
                                            Alternate_Section_4_City__c= 'City' ,
                                            Alternate_Section_4_Zip__c= 'Zip' ,
                                            Alternate_Section_4_Country__c = 'Country' ,
                                            Alternate_Section_4_Other__c = 'Other' ,
                                            Alternate_Section_4_Tax_Name__c = 'Tax_Name' ,
                                            Alternate_Section_4_Tax_Id__c = 'Tax_Id' ,
                                            Alternate_Section_4_Contact_Name__c= 'Contact_Name' ,
                                            Alternate_Section_4_Contact_Email__c= 'Contact_Email' ,
                                            Alternate_Section_4_Contact_Tel__c= 'Contact_Tel' ,
                                            VAT_Reclaim_Destination__c = FALSE,
                                            Final_Destination__c ='Brazil',
                                            Country__c = country.id,
                                            CIF_Freight_and_Insurance__c  = 'CIF Amount'
                                           );
        insert cpav2;
        
         
        CpaRules__c cpaRule = new CpaRules__c();
        cpaRule.Country__c = 'Finland';
        cpaRule.Criteria__c = 'Default';
        cpaRule.DefaultCPA2__c = cpav2.Id;
        cpaRule.CPA2__c =  cpav2.Id;
        Insert cpaRule;
        
       
        
        IOR_Price_List__c priceList = new IOR_Price_List__c();
        priceList.Destination__c = 'Finland';
        priceList.Client_Name__c = acc.Id;
        priceList.IOR_Fee__c = 10;
        
        Insert priceList;
        
        shipment_order__c shipment = new shipment_order__c();
        shipment.RecordTypeId = shipmentOrderRecTypeId;
        shipment.Destination__c = 'Finland';
        shipment.IOR_Price_List__c = priceList.Id;
        shipment.Account__c = acc.Id;
         shipment.CPA_v2_0__c = cpav2.Id;
        shipment.Shipping_Status__c = 'Cost Estimate Abandoned';
        Insert shipment;
        
        Freight__c freight = New Freight__c(status__c='Cancel',Packages_of_Same_Weight_Dimensions__c=1,
                                       Actual_Weight_KGs__c=40,Logistics_provider__c=acc.Id,
                                       Li_Ion_Batteries__c='No',Shipment_Order__c=shipment.Id,
                                       Ship_Froma__c='Finland',Ship_From__c='Finland',
                                       Ship_To__c='Australia',Chargeable_weight_in_KGs_packages__c=40,
                                       Estimated_chargeable_weight__c=40);
        insert freight;
        
         Courier_rates__C cr = new Courier_rates__C(
            rate__c=0,
            Freight_Request__c= freight.Id,
            Carrier_Name__c='ABC info',
            Cost__c=0,
            currencyIsoCode__C='EUR',
            service_type__c='IOR',
            service_type_name__c='IOR',
            Status__c = 'Selected'
        );
        Insert cr;
        
        NCPUpdateCourierServiceswra wrapper = new NCPUpdateCourierServiceswra();
        wrapper.Accesstoken = accessTokenObj.Access_Token__c;
        wrapper.CRID = cr.id;
        wrapper.Status = 'Expired';
        
        RestRequest req1 = new RestRequest();
        RestResponse res1 = new RestResponse();
        req1.requestURI = '/services/apexrest/UpdateCourierrates'; 
        req1.httpMethod = 'POST';
        req1.requestBody = Blob.valueOf(JSON.serialize(wrapper));     
        RestContext.request = req1;
        RestContext.response = res1;
        RestContext.request.addHeader('Content-Type', 'application/json');
        
        Test.startTest(); 
        NCPUpdateCourierservice.NCPGetCourierServices();
        Test.stopTest();
    }
    
    static testMethod void NCPGetCourierServicesCatchBlockTest(){
       
        
        RestRequest req1 = new RestRequest();
        RestResponse res1 = new RestResponse();
        req1.requestURI = '/services/apexrest/UpdateCourierrates'; 
        req1.httpMethod = 'POST';
        req1.requestBody = Blob.valueOf('{"value":"Test"}');     
        RestContext.request = req1;
        RestContext.response = res1;
        RestContext.request.addHeader('Content-Type', 'application/json');
        
        Test.startTest(); 
        NCPUpdateCourierservice.NCPGetCourierServices();
        Test.stopTest();
    }
}