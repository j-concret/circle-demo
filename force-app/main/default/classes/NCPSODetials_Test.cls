@isTest
public class NCPSODetials_Test {
    
	static testMethod void  testForNCPSODetials(){
         Id shipmentOrderRecTypeId = Schema.SObjectType.shipment_order__c.getRecordTypeInfosByName().get('Cost Estimate').getRecordTypeId();
        Id accountRecTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('IOR/EOR Supplier').getRecordTypeId();

        Access_token__c accessTokenObj = new Access_token__c(
        	AccessToken_encoded__c='zmM0MDcxOTItZTJiZi00YWRhLTlkZGQtZGZhMmUxM2FiYTlj',
            Access_Token__c='cc407192-e2bf-4ada-9ddd-dfa2e13aba9c',
            Status__c='Active');
        
        Insert accessTokenObj;
        
        Account acc = new Account(Name = 'Acme Test',RecordTypeId = accountRecTypeId);
        
        Insert acc; 
        
        Account account4 = new Account (name='TecEx Prospective Client', Type ='Client',New_Invoicing_Structure__c = true);
        insert account4;
        
        Contact con = new Contact(
            LastName = 'Test Contact',Client_Notifications_Choice__c='Opt-In',
            AccountId = acc.Id);
        Insert con;
        
        CpaRules__c cpaRules = new CpaRules__c(
            Country__c = 'Finland',
        	Criteria__c = 'Based on Condition');
        Insert cpaRules;
        
        IOR_Price_List__c iorpl = new IOR_Price_List__c ( Client_Name__c = acc.Id, Name = 'Finland', IOR_Fee__c = 0.15, On_Charge_Mark_up__c = 0.15,
                                                         TecEx_Shipping_Fee_Markup__c =0.40, IOR_Min_Fee__c = 100,  Admin_Fee__c =100, Set_up_fee__c =100, Bank_Fees__c =200, Tax_Rate__c = 0.20,
                                                         Estimated_Customs_Brokerage__c =250, Estimated_Customs_Clearance__c =250, Estimated_Customs_Handling__c =250, Estimated_Import_License__c =250, Destination__c = 'Brazil' );
        insert iorpl; 
        
        IOR_Price_List__c iorpl1 = new IOR_Price_List__c ( Client_Name__c = account4.Id, Name = 'Finland', IOR_Fee__c = 0.15, On_Charge_Mark_up__c = 0.15,
                                                              TecEx_Shipping_Fee_Markup__c =0.40, IOR_Min_Fee__c = 100,  Admin_Fee__c =100, Set_up_fee__c =100, Bank_Fees__c =200, Tax_Rate__c = 0.20,
                                                              Estimated_Customs_Brokerage__c =250, Estimated_Customs_Clearance__c =250, Estimated_Customs_Handling__c =250, Estimated_Import_License__c =250, Destination__c = 'Belgium' );
            
        insert iorpl1;
        
         Country__c country = new Country__c(Brokerage_Costs__c = 618, CIF_Adjustment_Factor__c = 0.035, CIF_Absolute_value_adjustment__c = 200, Clearance_Costs__c = 488, 
                                            Country__c = 'Brazil', Name = 'Brazil', Handling_Costs__c = 1180, License_Permit_Costs__c = 0);
        insert country;
        
        CPA_v2_0__c RelatedCPA = new CPA_v2_0__c(Name = 'Brazil', Country_Applied_Costings__c=true );
        insert RelatedCPA;
        CPA_v2_0__c cpav2 = new CPA_v2_0__c(Name = 'Brazil Testing 2.0', Supplier__c = acc.Id, Service_Type__c = 'IOR/EOR', Related_Costing_CPA__c = 'a260Y00000EnDjr', Preferred_Supplier__c = TRUE,
                                            VAT_Rate__c = 0.1, Destination__c = 'Brazil',  
                                            VAT_Reclaim_Destination__c = FALSE,
                                            Final_Destination__c ='Brazil',
                                            Country__c = country.id,
                                            CIF_Freight_and_Insurance__c  = 'CIF Amount'
                                           );
        insert cpav2;
        
        shipment_order__c shipment = new shipment_order__c();
        shipment.RecordTypeId = shipmentOrderRecTypeId;
        shipment.Destination__c = 'Brazil';
        shipment.Account__c = acc.Id;
        shipment.CPA_v2_0__c = cpav2.Id;
        shipment.Special_Client_Invoicing__c = true;
        shipment.Clear_out_all_costings__c = false;
        shipment.shipping_status__c='Cost Estimate';
        shipment.Shipment_Value_USD__c = 2000;
        shipment.Insurance_Fee__c = 0;
        shipment.Client_Actual_Freight_Value__c = 10;
        shipment.IOR_Cost_If_in_USD_2__c = 10;
        shipment.IOR_Cost_Custom_b__c = 10;
        shipment.IOR_Refund_of_Tax_and_Duty__c = 10;
        shipment.Customs_Brokerage_Cost_fixed__c = 10;
        shipment.Bank_Cost_b__c = 10;
        shipment.Supplier_Admin_Cost__c = 10 ;
        shipment.Customs_Handling_Cost__c = 10;
        shipment.Courier_Handover_Cost__c = 10;
        shipment.Delvier_to_IOR_Warehouse_Cost__c = 10;
        shipment.Local_Delivery_Cost__c = 10;
        shipment.Examination_Cost__c = 10 ;
        shipment.Open_and_Repack_Cost__c = 10;
        shipment.Loading_Cost__c = 10;
        shipment.Storage_Cost__c = 10;
        shipment.Terminal_Charges_Cost__c = 10;
        shipment.Handling_other_costs__c = 10;
        shipment.Customs_Clearance_Cost_b__c = 10;
        shipment.Import_Declaration_Cost__c = 10;
        shipment.Quarantine_cost__c = 10;
        shipment.Customs_Inspection_Cost__c = 10;
        shipment.Delivery_Order_Cost__c = 10;
        shipment.License_Permit_Cost__c = 10;
        shipment.Who_arranges_International_courier__c = 'Client';
        shipment.Fixed_IOR_Cost__c = 10;
        shipment.Miscellaneous_Costs__c = 10;
        shipment.Liability_Cover_Fee_USD__c= 0.0;
        shipment.Liability_Cover_Cost_USD__c = 0.0;
        shipment.Forecast_Override__c = true;
        shipment.Handling_and_Admin_Fee__c = 0.0;
        shipment.Bank_Fees__c = 0.0;
               
        Insert shipment;
        
        Map<String,String> body = new Map<String,String> {'Accesstoken'=>accessTokenObj.Access_Token__c,'AccountID'=>acc.Id,'ContactID'=>con.Id,'SOID'=>shipment.Id};
        RestRequest req1 = new RestRequest();
        RestResponse res1 = new RestResponse();
        req1.requestURI = '/services/apexrest/SODetails/'; 
        req1.httpMethod = 'POST';
        req1.requestBody = Blob.valueOf(JSON.serialize(body));     
        RestContext.request = req1;
        RestContext.response = res1;
        RestContext.request.addHeader('Content-Type', 'application/json');
        
        Test.startTest(); 
        	NCPSODetials.NCPSODetials();
        Test.stopTest();
        
    }
    
    static testMethod void  testNegativeForNCPSODetials(){
                
        RestRequest req1 = new RestRequest();
        RestResponse res1 = new RestResponse();
        req1.requestURI = '/services/apexrest/SODetails/'; 
        req1.httpMethod = 'POST';
        req1.requestBody = Blob.valueOf('{"value":"Test response"}');     
        RestContext.request = req1;
        RestContext.response = res1;
        RestContext.request.addHeader('Content-Type', 'application/json');
        
        Test.startTest(); 
        	NCPSODetials.NCPSODetials();
        Test.stopTest();
        
    }
}