@RestResource(urlMapping='/UpdateAddressStatus/*')
Global class CPUpdateAddressStatus {
    
     @Httppost
    global static void CPPickupAddressCreation(){
     	RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        Blob body = req.requestBody;
    	res.addHeader('Content-Type', 'application/json');
        String requestString = body.toString();
        CPupdateaddressstatuswrapper rw = (CPupdateaddressstatuswrapper)JSON.deserialize(requestString,CPupdateaddressstatuswrapper.class);
     try {
         
          If(!rw.AddressStatus.isEmpty()){
              
              List<id> addrec = new list<id>();
              list<Client_Address__c> AllAddress = new list<Client_Address__c>();
              list<Client_Address__c> UpdateAddress = new list<Client_Address__c>();
              
              for(Integer i=0; rw.AddressStatus.size()>i;i++) {
                   addrec.add(rw.AddressStatus[i].AddressID);
                   }
              
              AllAddress = [Select id,Client__c,default__c,address_status__c from Client_Address__c where id in :addrec ];
              System.debug('AllAddress'+AllAddress);
              
               for(Integer i=0;AllAddress.size()>i;i++) {
                   
                  
 					for(Integer j=0;rw.AddressStatus.size()>j;j++) {
                        
                   if(AllAddress[i].ID == rw.AddressStatus[j].AddressID && rw.AddressStatus[j].AddressStatus =='Inactive') {
                                   		AllAddress[i].address_status__c = rw.AddressStatus[j].AddressStatus;
                       					AllAddress[i].default__c = FAlse;
                       UpdateAddress.add(AllAddress[i]);
                      
                       		}
                   Else if(AllAddress[i].ID == rw.AddressStatus[j].AddressID){
                       AllAddress[i].address_status__c = rw.AddressStatus[j].AddressStatus;
                       UpdateAddress.add(AllAddress[i]);
                      
                     }
        				}
                   }
              System.debug('UpdateAddress-->'+UpdateAddress);
              Update UpdateAddress;
              
              
            String ASD = 'Success-Addrerss updated';	
            res.responseBody = Blob.valueOf(JSON.serializePretty(ASD));
            res.statusCode = 200;
              	API_Log__c Al = New API_Log__c();
                Al.Account__c=AllAddress[0].Client__c;
                Al.EndpointURLName__c='UpdateAddressStatus';
                Al.Response__c='Success - AddressUpdated';
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al; 
              
              
              
          }
         
     }
        catch (Exception e){
                
              	String ErrorString ='Something went wrong while activating / inactivating address, please contact support_SF@tecex.com';
               	res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
            	res.statusCode = 500;
                API_Log__c Al = New API_Log__c();
                //Al.Account__c=AllAddress[0].Client__c;
                Al.EndpointURLName__c='UpdateAddressStatus';
                Al.Response__c=e.getMessage()+' at line number: '+e.getLineNumber();
                Al.StatusCode__c=string.valueof(res.statusCode);
              	Insert Al;

              	}

}
}