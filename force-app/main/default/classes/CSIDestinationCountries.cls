@RestResource(urlMapping='/MyDestinationCountrieslist/*')
global class CSIDestinationCountries {
    @Httpget
    global static void NCPAPIDestinations(){
          RestRequest req = RestContext.request;
         RestResponse res = RestContext.response;
		set<String> Picklistvalues=new Set<String>();
        Map<String,string> ShipFromCountryList = New Map<String,string>();
      
        
        
        Schema.DescribeFieldResult F = Shipment_Order__c.Destination__c.getDescribe();
        Schema.sObjectField T = F.getSObjectField();
        List<PicklistEntry> entries = T.getDescribe().getPicklistValues();
        
        For(PicklistEntry PLE:entries ){
            IF(PLe.active == true){
            Picklistvalues.add(PLE.value);
            }
        }
        List<CountryofOriginMap__c> Coo= [SELECT Id, Name, Two_Digit_Country_Code__c,Three_Digit_Country_Code__c, Country__c FROM CountryofOriginMap__c where country__c in :Picklistvalues];
       
        Map<String,CountryofOriginMap__c> cooMap = new Map<String,CountryofOriginMap__c>();
        for(CountryofOriginMap__c co :Coo){
            cooMap.put(co.Country__c,co);
        }

        		JSONGenerator gen = JSON.createGenerator(true);
                gen.writeStartObject();                
                gen.writeFieldName('ShipTOCountriesList');
       				 			
       						 gen.writeStartArray();	 
        					 for(String country: cooMap.keySet()){
            
                                        CountryofOriginMap__c co = cooMap.get(country);
                                        
                                        gen.writeStartObject();                                    
                                        if(co.country__C== null) {gen.writeNullField('Name');} else{gen.writeStringField('Name',co.country__C);}
                                        if(co.Two_Digit_Country_Code__c== null) {gen.writeNullField('TwoDigitISO');} else{gen.writeStringField('TwoDigitISO',co.Two_Digit_Country_Code__c);}
                                        if(co.Three_Digit_Country_Code__c== null) {gen.writeNullField('ThreeDigitISO');} else{gen.writeStringField('ThreeDigitISO',co.Three_Digit_Country_Code__c);}
                                        //  if(Coo[i].country__C== null) {gen.writeNullField('Name');} else{gen.writeStringField('Name',Coo[i].country__C);}
                                        gen.writeEndObject(); 
                                    }    
          					gen.writeEndArray();
       			
                 gen.writeEndObject();
                 string jsonData = gen.getAsString();
               
        		res.addHeader('Content-Type', 'application/json');
         		res.responseBody = Blob.valueOf(jsonData);
        	//	res.responseBody = Blob.valueOf(JSON.serializePretty(FinalPicklistvalues));
        	 	res.statusCode = 200;

}

}