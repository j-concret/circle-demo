public with sharing class BatteryPINumberCtrl {

    @AuraEnabled
    public static List<Shipment_Order_Package__c> getPackages(String soId){
        try{
            return [SELECT Id,Name,Contains_Batteries__c,Actual_Weight_KGs__c,ION_PI966__c,ION_PI967__c,Metal_PI969__c,Metal_PI970__c FROM Shipment_Order_Package__c WHERE Contains_Batteries__c = true AND Shipment_Order__c =: soid];
        }catch(Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    @AuraEnabled
    public static void updateBatteryPI(List<Shipment_Order_Package__c> packages){
        try {
            update packages;
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }
}