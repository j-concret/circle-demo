public class CustomMetadataLoaderCtrl {
    public static final String QUALIFIED_API_NAME_ATTRIBUTE = 'QualifiedApiName';
    public static final String FULL_NAME_ATTRIBUTE = 'FullName';
    public static final String LABEL_ATTRIBUTE = 'Label';
    public static final String DEV_NAME_ATTRIBUTE = 'DeveloperName';
    public static final String DESC_ATTRIBUTE = 'Description';

    @AuraEnabled
    public static Map<String,Object> saveRecord( Id cntId ) {
        Map<String,Object> result = new Map<String,Object>();
        Savepoint sp = Database.setSavepoint();
       // try{
            ContentVersion contentVersionObj = [SELECT Id, VersionData FROM ContentVersion where ContentDocumentId =:cntId];
            //Blob b = EncodingUtil.base64Decode(contentVersionObj.VersionData);
            List<List<String>> rows = Util.parseCSV(contentVersionObj.VersionData,false);
            Compliance_Document_Field_Rules__mdt.getSobjectType();
            Schema.DescribeSObjectResult dsr = Compliance_Document_Field_Rules__mdt.sObjectType.getDescribe();
			Set<String> standardFields = new Set<String>();
            standardFields.add(DEV_NAME_ATTRIBUTE);
            standardFields.add(LABEL_ATTRIBUTE);
            standardFields.add(DESC_ATTRIBUTE);
            
            Map<String, Schema.SObjectField> fieldMap = dsr.fields.getMap();
            
            Metadata.DeployContainer mdContainer = new Metadata.DeployContainer();
            List<String> fieldNameSet = rows.remove(0);
            for(List<String> fieldValueList : rows){
                
                Map<String, String> fieldsAndValues = new Map<String, String>();
                Integer index = 0;
                for(String fieldName : fieldNameSet) {
                    
                    if(fieldName.equals(DEV_NAME_ATTRIBUTE)) {
                        if (fieldValueList.size() > index) {
                            fieldsAndValues.put(FULL_NAME_ATTRIBUTE, 'Compliance_Document_Field_Rules__mdt.'+ fieldValueList.get(index));
                            
                            //adding dev_name here since we might need it to default label
                            fieldsAndValues.put(fieldName, fieldValueList.get(index));
                        }
                    } else {
                        if (fieldValueList.size() > index) {
                            fieldsAndValues.put(fieldName, fieldValueList.get(index));
                        }
                        //fieldsAndValues.put(fieldName, singleRowOfValues.get(index));
                    }
                    index++;
                }
                if(fieldsAndValues.get(FULL_NAME_ATTRIBUTE) == null) {
                    String strippedLabel = fieldsAndValues.get(LABEL_ATTRIBUTE).replaceAll('\\W+', '_').replaceAll('__+', '_').replaceAll('\\A[^a-zA-Z]+', '').replaceAll('_$', '');
                    //default fullName to type_dev_name.label
                    fieldsAndValues.put(FULL_NAME_ATTRIBUTE, 'Compliance_Document_Field_Rules__mdt.'+ strippedLabel);
                }else if(fieldsAndValues.get(LABEL_ATTRIBUTE) == null) {
                    //default label to dev_name
                    fieldsAndValues.put(LABEL_ATTRIBUTE, fieldsAndValues.get(DEV_NAME_ATTRIBUTE));
                }
                mdContainer.addMetadata(transformToCustomMetadata(fieldMap,standardFields, fieldsAndValues));
            }
            //System.debug(JSON.serialize(mdContainer));
            CustomMetadataCallback callback = new CustomMetadataCallback();
            String jobId = Test.isRunningTest()?'FakeId':Metadata.Operations.enqueueDeployment(mdContainer, callback);
            result.put('jobId',jobId);
            
        /*}catch(Exception e){
            Database.rollback(sp);
            result.put('error',e.getMessage());
        }
        finally{
            deleteContentDoc(cntId);
        }*/
        return result;
    }
    
    private static Metadata.CustomMetadata transformToCustomMetadata(Map<String, Schema.SObjectField> fieldMap,Set<String> standardFields, Map<String, String> fieldsAndValues){
        Metadata.CustomMetadata customMetadata = new Metadata.CustomMetadata();
        customMetadata.label = fieldsAndValues.get(LABEL_ATTRIBUTE);
        customMetadata.fullName = fieldsAndValues.get(FULL_NAME_ATTRIBUTE);
        customMetadata.description = fieldsAndValues.get(DESC_ATTRIBUTE);
        
        //custom fields
        Metadata.CustomMetadataValue[] customMetadataValues = new List<Metadata.CustomMetadataValue>();
        if(fieldsAndValues != null) {
            for (String fieldName : fieldsAndValues.keySet()) {
                if(!standardFields.contains(fieldName) && !FULL_NAME_ATTRIBUTE.equals(fieldName)) {
                    Metadata.CustomMetadataValue cmRecordValue = new Metadata.CustomMetadataValue();
                    cmRecordValue.field=fieldName;
                    system.debug('Field name -->'+fieldName);
                    cmRecordValue.value= getValue(fieldMap.get(fieldName),fieldsAndValues.get(fieldName));
                    
                    customMetadataValues.add(cmRecordValue);
                }
            }
        }
        customMetadata.values = customMetadataValues;
        return customMetadata;
    }
    
    @future
    public static void deleteContentDoc(Id cntId){
        Delete [SELECT Id FROM ContentDocument where Id=:cntId];        
    }
    
    public static Object getValue(Schema.SObjectField field,String value){
        Schema.DescribeFieldResult fieldDescribe = field.getDescribe();
        if(Schema.DisplayType.BOOLEAN == fieldDescribe.getType()){
            return Boolean.valueOf(value);
        }
        else if(Schema.DisplayType.STRING == fieldDescribe.getType()){
            return String.valueOf(value);
        }
        return value;
        
    }
    
    public class CustomException extends Exception{} 
    
}