@IsTest
public class CPCEProformaCreationWrapper_Test {
    
    static testMethod void testParse() {
		String json = '{'+
		'\"AccountID\" :\"0010Y00000PEqvHQAT\",'+
		'\"ContactID\" :\"0031v00001hY02HAAS\",'+
		'\"estimatedChargableweight\":0,'+
		'\"ServiceType\": \"IOR\",'+
		'\"Courier_responsibility\": \"TecEx\",'+
		'\"Reference1\": \"Ref1\",'+
		'\"Reference2\": \"Ref2\",'+
		'\"ShipFrom\": \"France\",'+
		'\"ShipTo\": \"Belgium\",'+
		'\"ShipmentvalueinUSD\": 80,'+
		'\"PONumber\": \"TestPONumber\",'+
		'\"Type_of_Goods\" : \"Refurbished\",'+
		'\"Li_ion_Batteries\" : \"Yes\",'+
		'\"Li_ion_BatteryTypes\" : \"Lithium ION batteries\" '+
		
		''+
		'}';
		CPCEProformaCreationWrapper obj = CPCEProformaCreationWrapper.parse(json);
		System.assert(obj != null);
	}

}