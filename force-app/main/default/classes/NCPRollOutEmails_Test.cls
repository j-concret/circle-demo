@IsTest
public class NCPRollOutEmails_Test {

    static testMethod void  TestForNCPRollOutEmailsInactive(){
        Access_token__c accessTokenObj = new Access_token__c(
        	AccessToken_encoded__c='zmM0MDcxOTItZTJiZi00YWRhLTlkZGQtZGZhMmUxM2FiYTlj',
            Access_Token__c='cc407192-e2bf-4ada-9ddd-dfa2e13aba9c',
            Status__c='Expired');
        
        Insert accessTokenObj;
        Account acc = new Account(Name = 'Acme Test');
        
        Insert acc;
        NCPRollOutEmailsWra wrapper = new NCPRollOutEmailsWra();
        wrapper.Accesstoken = accessTokenObj.Access_Token__c;
        
        wrapper.ccEmailID = 'Test@Test.com';
        wrapper.toEmailID = 'Test1@test.com';
        wrapper.RolloutId = acc.Id;
        RestRequest req1 = new RestRequest();
        RestResponse res1 = new RestResponse();
        req1.requestURI = '/services/apexrest/NCPAddParticipants/'; 
        req1.httpMethod = 'POST';
        req1.requestBody = Blob.valueOf(JSON.serialize(wrapper));     
        RestContext.request = req1;
        RestContext.response = res1;
        RestContext.request.addHeader('Content-Type', 'application/json');
        
        Test.startTest(); 
        	NCPRollOutEmails.NCPRollOutEmails();
        Test.stopTest();
    }
    static testMethod void  TestForNCPRollOutEmails(){
       
        Account acc = new Account(Name = 'Acme Test');
        
        Insert acc;
        
        Contact contact = new Contact ( lastname ='Testing Individual',Client_Notifications_Choice__c='Opt-In',  AccountId = acc.Id,email='abc@def.com');
        insert contact; 
        
        Profile profileId = [SELECT Id FROM Profile WHERE Name = 'Tecex Customer' LIMIT 1];
        
        User usr = new User(LastName = 'User',
                            FirstName='Testing',
                            Alias = 'tuser',
                            Email = 'testingUser@asdf.com',
                            Username = 'testingUser@asdf.com',
                            ContactId = contact.Id,
                            ProfileId = profileId.Id,
                            TimeZoneSidKey = 'GMT',
                            LanguageLocaleKey = 'en_US',
                            EmailEncodingKey = 'UTF-8',
                            LocaleSidKey = 'en_US'
                           );
        insert usr;
        
             
        System.runAs(usr){
             Account acc1 = [select id,Name from Account where Name = 'Acme Test' limit 1];
			Contact contact1 = [select id from Contact limit 1];
         Access_token__c accessTokenObj = new Access_token__c(
        	AccessToken_encoded__c='zmM0MDcxOTItZTJiZi00YWRhLTlkZGQtZGZhMmUxM2FiYTlj',
            Access_Token__c='cc407192-e2bf-4ada-9ddd-dfa2e13aba9c',
            Status__c='Active');
        
        Insert accessTokenObj;
            
            Roll_Out__c new_roll_out = new Roll_Out__c( Client_Name__c=acc1.id,
                                                          Contact_For_The_Quote__c=contact1.id,
                                                          Shipment_Value_in_USD__c=200,
                                                          Destinations__c='Brazil',
                                                          Number_of_Cost_Estimates__c =0
                                                         );
     
      	Insert new_roll_out;
        NCPRollOutEmailsWra wrapper = new NCPRollOutEmailsWra();
        wrapper.Accesstoken = accessTokenObj.Access_Token__c;
        
        wrapper.ccEmailID = 'Test@Test.com';
        wrapper.toEmailID = 'Test1@test.com';
        wrapper.RolloutId = new_roll_out.Id;
        RestRequest req1 = new RestRequest();
        RestResponse res1 = new RestResponse();
        req1.requestURI = '/services/apexrest/NCPAddParticipants/'; 
        req1.httpMethod = 'POST';
        req1.requestBody = Blob.valueOf(JSON.serialize(wrapper));     
        RestContext.request = req1;
        RestContext.response = res1;
        RestContext.request.addHeader('Content-Type', 'application/json');
        
        Test.startTest(); 
        	NCPRollOutEmails.NCPRollOutEmails();
        Test.stopTest();
        }
    }
      static testMethod void  TestForNCPRollOutEmails1(){
       
        Account acc = new Account(Name = 'Acme Test');
        
        Insert acc;
        
        Contact contact = new Contact ( lastname ='Testing Individual',Client_Notifications_Choice__c='Opt-In',  AccountId = acc.Id,email='abc@def.com');
        insert contact; 
        
        Profile profileId = [SELECT Id FROM Profile WHERE Name = 'Tecex Customer' LIMIT 1];
        
        User usr = new User(LastName = 'User',
                            FirstName='Testing',
                            Alias = 'tuser',
                            Email = 'testingUser@asdf.com',
                            Username = 'testingUser@asdf.com',
                            ContactId = contact.Id,
                            ProfileId = profileId.Id,
                            TimeZoneSidKey = 'GMT',
                            LanguageLocaleKey = 'en_US',
                            EmailEncodingKey = 'UTF-8',
                            LocaleSidKey = 'en_US'
                           );
        insert usr;
        
             
        System.runAs(usr){
             Account acc1 = [select id,Name from Account where Name = 'Acme Test' limit 1];
			Contact contact1 = [select id from Contact limit 1];
         Access_token__c accessTokenObj = new Access_token__c(
        	AccessToken_encoded__c='zmM0MDcxOTItZTJiZi00YWRhLTlkZGQtZGZhMmUxM2FiYTlj',
            Access_Token__c='cc407192-e2bf-4ada-9ddd-dfa2e13aba9c',
            Status__c='Active');
        
        Insert accessTokenObj;
            
            Roll_Out__c new_roll_out = new Roll_Out__c( Client_Name__c=acc1.id,
                                                          Contact_For_The_Quote__c=contact1.id,
                                                          Shipment_Value_in_USD__c=200,
                                                          Destinations__c='Brazil',
                                                          Number_of_Cost_Estimates__c =0
                                                         );
     
      	Insert new_roll_out;
        NCPRollOutEmailsWra wrapper = new NCPRollOutEmailsWra();
        wrapper.Accesstoken = accessTokenObj.Access_Token__c;
        
        wrapper.ccEmailID = 'Test@Test.com';
        wrapper.toEmailID = 'Test1@test.com';
        wrapper.RolloutId = acc1.Id;
        RestRequest req1 = new RestRequest();
        RestResponse res1 = new RestResponse();
        req1.requestURI = '/services/apexrest/NCPAddParticipants/'; 
        req1.httpMethod = 'POST';
        req1.requestBody = Blob.valueOf(JSON.serialize(wrapper));     
        RestContext.request = req1;
        RestContext.response = res1;
        RestContext.request.addHeader('Content-Type', 'application/json');
        
        Test.startTest(); 
        	NCPRollOutEmails.NCPRollOutEmails();
        Test.stopTest();
        }
    }
    
     
     
      static testMethod void  TestForNCPaccesstoken(){
        Access_token__c accessTokenObj = new Access_token__c(
        	AccessToken_encoded__c='zmM0MDcxOTItZTJiZi00YWRhLTlkZGQtZGZhMmUxM2FiYTlj',
            Access_Token__c='cc407192-e2bf-4ada-9ddd-dfa2e13aba9c',
            Status__c='Expired');
        
        Insert accessTokenObj;
        Account acc = new Account(Name = 'Acme Test');
        
        Insert acc;
        NCPRollOutEmailsWra wrapper = new NCPRollOutEmailsWra();
        wrapper.Accesstoken = accessTokenObj.Access_Token__c;
        
        wrapper.ccEmailID = 'Test';
        wrapper.toEmailID = 'Test1';
        wrapper.RolloutId = acc.Id;
        RestRequest req1 = new RestRequest();
        RestResponse res1 = new RestResponse();
        req1.requestURI = '/services/apexrest/NCPAddParticipants/'; 
        req1.httpMethod = 'POST';
        req1.requestBody = Blob.valueOf(JSON.serialize(wrapper));     
        RestContext.request = req1;
        RestContext.response = res1;
        RestContext.request.addHeader('Content-Type', 'application/json');
        
        Test.startTest(); 
        	NCPRollOutEmails.NCPRollOutEmails();
        Test.stopTest();
    }
}