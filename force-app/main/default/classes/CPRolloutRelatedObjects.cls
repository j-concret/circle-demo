@RestResource(urlMapping='/RORelatedObjects/*')
global class CPRolloutRelatedObjects {
    
      @Httpget
    global static void RolloutRelatedObjects(){
    
        String jsonData = 'Test';
        RestRequest req = RestContext.request;
    	RestResponse res = RestContext.response;
     	res.addHeader('Content-Type', 'application/json');
        String Username = req.params.get('Username');
        String Password = req.params.get('Password');
        String ROID = req.params.get('ROID');
       	Contact logincontact = [select Id,Account.id,lastname,Email,Password__c,UserName__c,Contact_Status__c from Contact where email =:username and Contact_status__c = 'Active' ];
        try {
            if(logincontact.Password__c == Password && logincontact.UserName__c == Username && logincontact.Contact_status__C == 'Active' ){
            
            roll_out__c ROs = [select id,name,Client_Name__r.Name,Contact_For_The_Quote__r.Name,Destinations__c from roll_out__C where ID =:ROID ];
            List<Shipment_order__c> SOs = [Select ID,Potential_Cash_Outlay_Fee__c,Account__c,Account__r.Cash_outlay_fee_base__c,Account__r.Finance_Charge__c,All_Taxes_and_Duties_Summary__c,Bank_Fees__c,Chargeable_Weight__c,Cost_Estimate_Number_HyperLink__c,Delivery_Contact_Name__c,Delivery_Contact_Tel_number__c,Estimate_Customs_Clearance_Time_Formula__c,Estimate_Transit_Time_Formula__c,Final_Delivery_Address__c,Government_and_In_country_Summary__c,Handling_and_Admin_Fee__c,Insurance_Fee_USD__c,International_Delivery_Fee__c,IOR_FEE_USD__c,IOR_Price_List__c,IOR_Price_List__r.Name,Name,Recharge_Handling_Costs__c,Recharge_License_Cost__c,Recharge_Tax_and_Duty__c,RecordType.Name,RecordTypeId,Shipment_Value_USD__c,Shipping_Notes_Formula__c,Shipping_Notes_New__c,Shipping_Status__c,Tax_Cost__c,Total_clearance_Costs__c,Total_Customs_Brokerage_Cost__c,Total_Invoice_Amount__c,Who_arranges_International_courier__c from shipment_order__c where Roll_out__c =: ROID];
                
                 JSONGenerator gen = JSON.createGenerator(true);
                 gen.writeStartObject();
                					//Creating Array & Object - Rollout
                                          gen.writeFieldName('RollOut');
                                          gen.writeStartObject();
                                          	if(ROs.Id == null) {gen.writeNullField('Id');} else{gen.writeStringField('Id', ROs.Id);}
                							if(ROs.name == null) {gen.writeNullField('name');} else{gen.writeStringField('name', ROs.name);}
                							if(ROs.Client_Name__c == null) {gen.writeNullField('AccountName');} else{gen.writeStringField('AccountName', ROs.Client_Name__r.Name);}
                							if(ROs.Contact_For_The_Quote__c == null) {gen.writeNullField('ContactName');} else{gen.writeStringField('ContactName', ROs.Contact_For_The_Quote__r.Name);}
                							if(ROs.Destinations__c == null) {gen.writeNullField('Destinations');} else{gen.writeStringField('Destinations', ROs.Destinations__c);}
                
                                          gen.writeEndObject();
                                          //End of Array & Object - RollOut
           If(!SOs.isEmpty()){
             //Creating Array & Object - Shipment Order
             gen.writeFieldName('ShipmentOrders');
             	gen.writeStartArray();
     		         for(Shipment_Order__c SOs1 :SOs){
        				//Start of Object - Shipment Order	
                         gen.writeStartObject();
                          
                         if(SOs1.Id == null) {gen.writeNullField('Id');} else{gen.writeStringField('Id', SOs1.Id);}
                         if(SOs1.Name == null) {gen.writeNullField('Name');} else{gen.writeStringField('Name', SOs1.Name);}
                         if(SOs1.IOR_Price_List__r.name == null) {gen.writeNullField('Ship_to_Country');} else{gen.writeStringField('Ship_to_Country', SOs1.IOR_Price_List__r.name);}
                         if(SOs1.Shipping_Status__c == null) {gen.writeNullField('Shipping_Status');} else{gen.writeStringField('Shipping_Status', SOs1.Shipping_Status__c);}
                         if(SOs1.Shipment_Value_USD__c == null) {gen.writeNullField('Shipment_Value');} else{gen.writeNumberField('Shipment_Value', SOs1.Shipment_Value_USD__c);}
                         if(SOs1.Total_Invoice_Amount__c == null) {gen.writeNullField('Total_Invoice_Amount');} else{gen.writeNumberField('Total_Invoice_Amount', SOs1.Total_Invoice_Amount__c);}
                         if(SOs1.Tax_Cost__c == null) {gen.writeNullField('Estimated_Tax_Duty_Cost');} else{gen.writeNumberField('Estimated_Tax_Duty_Cost', SOs1.Tax_Cost__c);}
                         if(SOs1.Chargeable_Weight__c == null) {gen.writeNullField('Chargeable_Weight');} else{gen.writeNumberField('Chargeable_Weight', SOs1.Chargeable_Weight__c);}
                         if(SOs1.Who_arranges_International_courier__c == null) {gen.writeNullField('Courier_responsibility');} else{gen.writeStringField('Courier_responsibility', SOs1.Who_arranges_International_courier__c);}
                         if(SOs1.Account__c == null) {gen.writeNullField('Account');} else{gen.writeStringField('Account', SOs1.Account__c);}
                         if(SOs1.Account__r.Cash_outlay_fee_base__c == null) {gen.writeNullField('Cash_outlay_fee_base');} else{gen.writeStringField('Cash_outlay_fee_base', SOs1.Account__r.Cash_outlay_fee_base__c);}
                         if(SOs1.Account__r.Finance_Charge__c == null) {gen.writeNullField('Finance_Charge');} else{gen.writeNumberField('Finance_Charge', SOs1.Account__r.Finance_Charge__c);}
                         if(SOs1.All_Taxes_and_Duties_Summary__c == null) {gen.writeNullField('All_Taxes_and_Duties_Summary');} else{gen.writeNumberField('All_Taxes_and_Duties_Summary', SOs1.All_Taxes_and_Duties_Summary__c);}
                         if(SOs1.Bank_Fees__c == null) {gen.writeNullField('Bank_Fees');} else{gen.writeNumberField('Bank_Fees', SOs1.Bank_Fees__c);}
                   		// if(SOs1.Cost_Estimate_Number_HyperLink__c == null) {gen.writeNullField('Cost_Estimate_Number_HyperLink');} else{gen.writeNumberField('Cost_Estimate_Number_HyperLink', SOs1.Cost_Estimate_Number_HyperLink);}
                         if(SOs1.Delivery_Contact_Name__c == null) {gen.writeNullField('Delivery_Contact_Name');} else{gen.writeStringField('Delivery_Contact_Name', SOs1.Delivery_Contact_Name__c);}
                   	     if(SOs1.Delivery_Contact_Tel_number__c == null) {gen.writeNullField('Delivery_Contact_Tel_number');} else{gen.writeStringField('Delivery_Contact_Tel_number', SOs1.Delivery_Contact_Tel_number__c);}
                   	     if(SOs1.Estimate_Customs_Clearance_Time_Formula__c == null) {gen.writeNullField('Estimate_Customs_Clearance_Time_Formula');} else{gen.writeStringField('Estimate_Customs_Clearance_Time_Formula', SOs1.Estimate_Customs_Clearance_Time_Formula__c);}
                   	     if(SOs1.Estimate_Transit_Time_Formula__c == null) {gen.writeNullField('Estimate_Transit_Time_Formula');} else{gen.writeStringField('Estimate_Transit_Time_Formula', SOs1.Estimate_Transit_Time_Formula__c);}
                         if(SOs1.Final_Delivery_Address__c == null) {gen.writeNullField('Final_Delivery_Address');} else{gen.writeStringField('Final_Delivery_Address', SOs1.Final_Delivery_Address__c);}
                         if(SOs1.Government_and_In_country_Summary__c == null) {gen.writeNullField('Government_and_In_country_Summary');} else{gen.writeNumberField('Government_and_In_country_Summary', SOs1.Government_and_In_country_Summary__c);}
                         if(SOs1.Handling_and_Admin_Fee__c == null) {gen.writeNullField('Handling_and_Admin_Fee');} else{gen.writeNumberField('Handling_and_Admin_Fee', SOs1.Handling_and_Admin_Fee__c);}
                         if(SOs1.Insurance_Fee_USD__c == null) {gen.writeNullField('Insurance_Fee_USD');} else{gen.writeNumberField('Insurance_Fee_USD', SOs1.Insurance_Fee_USD__c);}
                         if(SOs1.International_Delivery_Fee__c == null) {gen.writeNullField('International_Delivery_Fee');} else{gen.writeNumberField('International_Delivery_Fee', SOs1.International_Delivery_Fee__c);}
                         if(SOs1.IOR_FEE_USD__c == null) {gen.writeNullField('IOR_FEE_USD');} else{gen.writeNumberField('IOR_FEE_USD', SOs1.IOR_FEE_USD__c);}
                         if(SOs1.Recharge_Handling_Costs__c == null) {gen.writeNullField('Recharge_Handling_Costs');} else{gen.writeNumberField('Recharge_Handling_Costs', SOs1.Recharge_Handling_Costs__c);}
                         if(SOs1.Recharge_License_Cost__c == null) {gen.writeNullField('Recharge_License_Cost');} else{gen.writeNumberField('Recharge_License_Cost', SOs1.Recharge_License_Cost__c);}
                         if(SOs1.Recharge_Tax_and_Duty__c == null) {gen.writeNullField('Recharge_Tax_and_Duty');} else{gen.writeNumberField('Recharge_Tax_and_Duty', SOs1.Recharge_Tax_and_Duty__c);}
                         if(SOs1.RecordType.Name == null) {gen.writeNullField('RecordType_Name');} else{gen.writeStringField('RecordType_Name', SOs1.RecordType.Name);}
                         if(SOs1.Shipping_Notes_Formula__c == null) {gen.writeNullField('Shipping_Notes_Formula');} else{gen.writeStringField('Shipping_Notes_Formula', SOs1.Shipping_Notes_Formula__c);}
                         if(SOs1.Shipping_Notes_New__c == null) {gen.writeNullField('Shipping_Notes_New');} else{gen.writeStringField('Shipping_Notes_New', SOs1.Shipping_Notes_New__c);}
                         if(SOs1.Total_clearance_Costs__c == null) {gen.writeNullField('Total_clearance_Costs');} else{gen.writeNumberField('Total_clearance_Costs', SOs1.Total_clearance_Costs__c);}
                         if(SOs1.Total_Customs_Brokerage_Cost__c == null) {gen.writeNullField('Total_Customs_Brokerage_Cost');} else{gen.writeNumberField('Total_Customs_Brokerage_Cost', SOs1.Total_Customs_Brokerage_Cost__c);}
                         if(SOs1.Potential_Cash_Outlay_Fee__c == null) {gen.writeNullField('Potential_Cash_Outlay_Fee');} else{gen.writeNumberField('Potential_Cash_Outlay_Fee', SOs1.Potential_Cash_Outlay_Fee__c);}
                         
                   	     
        					gen.writeEndObject();
                         //End of Object - Shipment Order
    					}
    					gen.writeEndArray();
                		//End of Array - Shipment
         			}
                
                gen.writeEndObject();
    			jsonData = gen.getAsString();
               
                  res.responseBody = Blob.valueOf(jsonData);
        	 	  res.statusCode = 200;
                
                
                API_Log__c Al = New API_Log__c();
               // Al.Account__c=logincontact.AccountID;
                //Al.Login_Contact__c=logincontact.Id;
                Al.EndpointURLName__c='RORelatedObjects';
                Al.Response__c='Success - RO RelatedObject Details are sent';
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al;

            
            }
            
              else{
                  
                  String ErrorString ='Authentication failed, Please check username and password';
               	  res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
        	  	  res.statusCode = 400;
                
                API_Log__c Al = New API_Log__c();
               // Al.Account__c=logincontact.AccountID;
               // Al.Login_Contact__c=logincontact.Id;
                Al.EndpointURLName__c='RORelatedObjects';
                Al.Response__c=ErrorString;
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al;
               
                                              
                 }
            
        }
    	catch(exception e){
            
            res.responseBody = Blob.valueOf(e.getMessage());
            res.statusCode = 500;
                API_Log__c Al = New API_Log__c();
              //  Al.Account__c=logincontact.AccountID;
              //  Al.Login_Contact__c=logincontact.Id;
                Al.EndpointURLName__c='RORelatedObjects';
                Al.Response__c=e.getMessage()+' at line number: '+e.getLineNumber();
                Al.StatusCode__c=string.valueof(res.statusCode);
              	Insert Al;
        
        
        
    		}
        
    }

}