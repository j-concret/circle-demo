public class updateEphesoftResponse{

//@future (callout=true)
    
    public static void updateAtt(Id pId, String apiResponse){
    
        List<Attachment__c> u=[select Id, Attachment_Type__c, Initial_HTTP_Response__c, Validation_HTTP_Response__c from Attachment__c where Id=:pId];
            
        If
       ( u[0].Initial_HTTP_Response__c == null){
           
        u[0].Initial_HTTP_Response__c  = apiResponse;
        }
        
        else{
        
        u[0].Validation_HTTP_Response__c = apiResponse;
        }
        
        update u;
    }
}