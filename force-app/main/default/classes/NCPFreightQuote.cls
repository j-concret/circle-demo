@RestResource(urlMapping='/FreightQuote/*')
Global class NCPFreightQuote {
    
    
     @Httppost
      global static void NCPFreightQuote(){
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        Blob body = req.requestBody;
        res.addHeader('Content-Type', 'application/json');
        String requestString = body.toString();
        NCPFreightQuotewra rw  = (NCPFreightQuotewra)JSON.deserialize(requestString,NCPFreightQuotewra.class);
          try{
              
              //Accesstoken test has to come here
              Access_token__c At =[select status__c,Access_Token__c from Access_token__c where Access_Token__c=:rw.Accesstoken ];
            if( at.status__C =='Active'){
                
               List<Freight__c> Freightlist = [select Id,Freight_age_in_Mins__c,Shipment_Order__c,
                                               Shipment_Order__r.Total_Invoice_Amount__c,
                                               Shipment_Order__r.Total_including_estimated_duties_and_tax__c,
                                               Shipment_Order__r.Total_Cost_Range_Min__c,
                                               Shipment_Order__r.Total_Cost_Range_Max__c,
                                               Shipment_Order__r.Minimum_tax_range__c,
                                               Shipment_Order__r.maximum_tax_range__c,
                                               Shipment_Order__r.Tax_Cost__c,
                                               Shipment_Order__r.International_Delivery_Fee__c,FF_Only_Lane__c,Courier_Type__c,Shipment_Order__r.Service_type__c,Display_International_Delivery_fee__c,Freight_Quote_Text__c,Selected_Rate_From_Existing__c,createddate  from Freight__c  where Shipment_Order__c = :rw.SOID limit 1];
              
              if(!Freightlist.isempty()){
                  
                  String Status= null;
                  String compare= null;
                  
                  
                  if(Freightlist[0].FF_Only_Lane__c==FALSE && Freightlist[0].Shipment_Order__r.Service_type__c != 'EOR' &&  Freightlist[0].Freight_age_in_Mins__c <= 2 &&(Freightlist[0].Selected_Rate_From_Existing__c == 0 || Freightlist[0].Selected_Rate_From_Existing__c == null) ){
                     //condition 3
                        System.debug('Condition 3');
                        System.debug('FF_Only_Lane__c-->'+Freightlist[0].FF_Only_Lane__c);
                        System.debug('Service_type__c-->'+Freightlist[0].Shipment_Order__r.Service_type__c);
                        System.debug('Selected_Rate_From_Existing__c-->'+Freightlist[0].Selected_Rate_From_Existing__c);
                        System.debug('Courier_Type__c-->'+Freightlist[0].Courier_Type__c);
                      compare='condition 3'+'FF_Only_Lane__c-->'+Freightlist[0].FF_Only_Lane__c+'Service_type__c-->'+Freightlist[0].Shipment_Order__r.Service_type__c+'Selected_Rate_From_Existing__c-->'+Freightlist[0].Selected_Rate_From_Existing__c+'Courier_Type__c-->'+Freightlist[0].Courier_Type__c+'Freight_age_in_Mins__c'+Freightlist[0].Freight_age_in_Mins__c;
                      
                      Status ='SEARCHING';
                  } else if(Freightlist[0].FF_Only_Lane__c==FALSE && Freightlist[0].Shipment_Order__r.Service_type__c != 'EOR' && Freightlist[0].Selected_Rate_From_Existing__c > 0 && Freightlist[0].Courier_Type__c=='Courier - Auto' ){
                        System.debug('Condition 4');
                        System.debug('FF_Only_Lane__c-->'+Freightlist[0].FF_Only_Lane__c);
                        System.debug('Service_type__c-->'+Freightlist[0].Shipment_Order__r.Service_type__c);
                        System.debug('Selected_Rate_From_Existing__c-->'+Freightlist[0].Selected_Rate_From_Existing__c);
                        System.debug('Courier_Type__c-->'+Freightlist[0].Courier_Type__c);
                      compare='condition 4'+'FF_Only_Lane__c-->'+Freightlist[0].FF_Only_Lane__c+'Service_type__c-->'+Freightlist[0].Shipment_Order__r.Service_type__c+'Selected_Rate_From_Existing__c-->'+Freightlist[0].Selected_Rate_From_Existing__c+'Courier_Type__c-->'+Freightlist[0].Courier_Type__c+'Freight_age_in_Mins__c'+Freightlist[0].Freight_age_in_Mins__c;
                       
                      //condition 4
                      Status ='CHANGE_SHIPMENT_PROVIDER';
                  }
                  else{
                        System.debug('Condition 1 & 2');
                        System.debug('FF_Only_Lane__c-->'+Freightlist[0].FF_Only_Lane__c);
                        System.debug('Service_type__c-->'+Freightlist[0].Shipment_Order__r.Service_type__c);
                        System.debug('Selected_Rate_From_Existing__c-->'+Freightlist[0].Selected_Rate_From_Existing__c);
                        System.debug('Courier_Type__c-->'+Freightlist[0].Courier_Type__c);
                      //condition 1 & 2
                       compare='condition 1 & 2'+'FF_Only_Lane__c-->'+Freightlist[0].FF_Only_Lane__c+'Service_type__c-->'+Freightlist[0].Shipment_Order__r.Service_type__c+'Selected_Rate_From_Existing__c-->'+Freightlist[0].Selected_Rate_From_Existing__c+'Courier_Type__c-->'+Freightlist[0].Courier_Type__c+'Freight_age_in_Mins__c'+Freightlist[0].Freight_age_in_Mins__c;
                      
                      Status ='CURRENT_BEST_RATE';
                      
                  }
                  
                  
            	    JSONGenerator gen = JSON.createGenerator(true);
            		gen.writeStartObject();
            
             		gen.writeFieldName('Success');
             		gen.writeStartObject();
            
                  if(Status =='SEARCHING'){
                       if(compare == NULL) {gen.writeNullField('Compare');} else{gen.writeStringField('compare', compare);}  
                     if(Freightlist[0].Id == NULL) {gen.writeNullField('FreightId');} else{gen.writeStringField('FreightId', Freightlist[0].ID);}  
                     if(Freightlist[0].Shipment_Order__r.International_Delivery_Fee__c == NULL) {gen.writeNullField('InternationalDeliveryFee');} else{gen.writeNumberField('InternationalDeliveryFee', Freightlist[0].Shipment_Order__r.International_Delivery_Fee__c);}  
                     if(Freightlist[0].Freight_Quote_Text__c == NULL) {gen.writeNullField('FreightQuoteText');} else{gen.writeStringField('FreightQuoteText','We are searching for better real-time rates. The best option will appear here shortly.');}  
                     if(Freightlist[0].Display_International_Delivery_fee__c == NULL) {gen.writeNullField('DisplayInternationalDeliveryfee');} else{gen.writeStringField('DisplayInternationalDeliveryfee', 'UNKNOWN');}  
                     if(Status == NULL) {gen.writeNullField('Status');} else{gen.writeStringField('Status', Status);}  
                       if(Freightlist[0].Shipment_Order__c == NULL) {gen.writeNullField('SOID');} else{gen.writeStringField('SOID', Freightlist[0].Shipment_Order__c);}  
                       if(Freightlist[0].Shipment_Order__r.Total_Invoice_Amount__c== NULL) {gen.writeNullField('TotalInvoiceAmount');} else{gen.writeNumberfield('TotalInvoiceAmount',Freightlist[0].Shipment_Order__r.Total_Invoice_Amount__c);}  
                       if(Freightlist[0].Shipment_Order__r.Total_including_estimated_duties_and_tax__c == NULL) {gen.writeNullField('Totalincludingestimateddutiesandtax');} else{gen.writeNumberField('Totalincludingestimateddutiesandtax', Freightlist[0].Shipment_Order__r.Total_including_estimated_duties_and_tax__c);}  
                       if(Freightlist[0].Shipment_Order__r.Total_Cost_Range_Min__c == NULL) {gen.writeNullField('TotalCostRangeMin');} else{gen.writeNumberField('TotalCostRangeMin', Freightlist[0].Shipment_Order__r.Total_Cost_Range_Min__c);}  
                       if(Freightlist[0].Shipment_Order__r.Total_Cost_Range_Max__c == NULL) {gen.writeNullField('TotalCostRangeMax');} else{gen.writeNumberField('TotalCostRangeMax', Freightlist[0].Shipment_Order__r.Total_Cost_Range_Max__c);}  
                     if(Freightlist[0].Shipment_Order__r.Tax_Cost__c == NULL) {gen.writeNullField('TaxCost');} else{gen.writeNumberField('TaxCost', Freightlist[0].Shipment_Order__r.Tax_Cost__c);}  
                     if(Freightlist[0].Shipment_Order__r.Minimum_tax_range__c == NULL) {gen.writeNullField('Minimumtaxrange');} else{gen.writeNumberField('Minimumtaxrange', Freightlist[0].Shipment_Order__r.Minimum_tax_range__c);}  
                     if(Freightlist[0].Shipment_Order__r.Maximum_tax_range__c == NULL) {gen.writeNullField('Maximumtaxrange');} else{gen.writeNumberField('Maximumtaxrange', Freightlist[0].Shipment_Order__r.Maximum_tax_range__c);}  
                    
                      
                  }
                  else  if(Status =='CHANGE_SHIPMENT_PROVIDER'){
                       if(compare == NULL) {gen.writeNullField('Compare');} else{gen.writeStringField('compare', compare);}  
                     if(Freightlist[0].Id == NULL) {gen.writeNullField('FreightId');} else{gen.writeStringField('FreightId', Freightlist[0].ID);}  
                     if(Freightlist[0].Shipment_Order__r.International_Delivery_Fee__c == NULL) {gen.writeNullField('InternationalDeliveryFee');} else{gen.writeNumberField('InternationalDeliveryFee', Freightlist[0].Shipment_Order__r.International_Delivery_Fee__c);}  
                     if(Freightlist[0].Freight_Quote_Text__c == NULL) {gen.writeNullField('FreightQuoteText');} else{gen.writeStringField('FreightQuoteText','Change Shipment Provider');}  
                     if(Freightlist[0].Display_International_Delivery_fee__c == NULL) {gen.writeNullField('DisplayInternationalDeliveryfee');} else{gen.writeStringField('DisplayInternationalDeliveryfee', 'Courier Rates');}  
                     if(Status == NULL) {gen.writeNullField('Status');} else{gen.writeStringField('Status', Status);}  
                       if(Freightlist[0].Shipment_Order__c == NULL) {gen.writeNullField('SOID');} else{gen.writeStringField('SOID', Freightlist[0].Shipment_Order__c);}  
                       if(Freightlist[0].Shipment_Order__r.Total_Invoice_Amount__c== NULL) {gen.writeNullField('TotalInvoiceAmount');} else{gen.writeNumberfield('TotalInvoiceAmount',Freightlist[0].Shipment_Order__r.Total_Invoice_Amount__c);}  
                       if(Freightlist[0].Shipment_Order__r.Total_including_estimated_duties_and_tax__c == NULL) {gen.writeNullField('Totalincludingestimateddutiesandtax');} else{gen.writeNumberField('Totalincludingestimateddutiesandtax', Freightlist[0].Shipment_Order__r.Total_including_estimated_duties_and_tax__c);}  
                       if(Freightlist[0].Shipment_Order__r.Total_Cost_Range_Min__c == NULL) {gen.writeNullField('TotalCostRangeMin');} else{gen.writeNumberField('TotalCostRangeMin', Freightlist[0].Shipment_Order__r.Total_Cost_Range_Min__c);}  
                       if(Freightlist[0].Shipment_Order__r.Total_Cost_Range_Max__c == NULL) {gen.writeNullField('TotalCostRangeMax');} else{gen.writeNumberField('TotalCostRangeMax', Freightlist[0].Shipment_Order__r.Total_Cost_Range_Max__c);}  
                     if(Freightlist[0].Shipment_Order__r.Tax_Cost__c == NULL) {gen.writeNullField('TaxCost');} else{gen.writeNumberField('TaxCost', Freightlist[0].Shipment_Order__r.Tax_Cost__c);}  
                     if(Freightlist[0].Shipment_Order__r.Minimum_tax_range__c == NULL) {gen.writeNullField('Minimumtaxrange');} else{gen.writeNumberField('Minimumtaxrange', Freightlist[0].Shipment_Order__r.Minimum_tax_range__c);}  
                     if(Freightlist[0].Shipment_Order__r.Maximum_tax_range__c == NULL) {gen.writeNullField('Maximumtaxrange');} else{gen.writeNumberField('Maximumtaxrange', Freightlist[0].Shipment_Order__r.Maximum_tax_range__c);}  
                    
                  }
                   else {
                      if(compare == NULL) {gen.writeNullField('Compare');} else{gen.writeStringField('compare', compare);}  
                     if(Freightlist[0].Id == NULL) {gen.writeNullField('FreightId');} else{gen.writeStringField('FreightId', Freightlist[0].ID);}  
                     if(Freightlist[0].Shipment_Order__r.International_Delivery_Fee__c == NULL) {gen.writeNullField('InternationalDeliveryFee');} else{gen.writeNumberField('InternationalDeliveryFee', Freightlist[0].Shipment_Order__r.International_Delivery_Fee__c);}  
                     if(Freightlist[0].Freight_Quote_Text__c == NULL) {gen.writeNullField('FreightQuoteText');} else{gen.writeStringField('FreightQuoteText','Current best rate. We will keep searching for other shipping options');}  
                     if(Freightlist[0].Display_International_Delivery_fee__c == NULL) {gen.writeNullField('DisplayInternationalDeliveryfee');} else{gen.writeStringField('DisplayInternationalDeliveryfee', 'Internal');}  
                     if(Status == NULL) {gen.writeNullField('Status');} else{gen.writeStringField('Status', Status);}  
                       if(Freightlist[0].Shipment_Order__c == NULL) {gen.writeNullField('SOID');} else{gen.writeStringField('SOID', Freightlist[0].Shipment_Order__c);}  
                       if(Freightlist[0].Shipment_Order__r.Total_Invoice_Amount__c== NULL) {gen.writeNullField('TotalInvoiceAmount');} else{gen.writeNumberfield('TotalInvoiceAmount',Freightlist[0].Shipment_Order__r.Total_Invoice_Amount__c);}  
                       if(Freightlist[0].Shipment_Order__r.Total_including_estimated_duties_and_tax__c == NULL) {gen.writeNullField('Totalincludingestimateddutiesandtax');} else{gen.writeNumberField('Totalincludingestimateddutiesandtax', Freightlist[0].Shipment_Order__r.Total_including_estimated_duties_and_tax__c);}  
                       if(Freightlist[0].Shipment_Order__r.Total_Cost_Range_Min__c == NULL) {gen.writeNullField('TotalCostRangeMin');} else{gen.writeNumberField('TotalCostRangeMin', Freightlist[0].Shipment_Order__r.Total_Cost_Range_Min__c);}  
                       if(Freightlist[0].Shipment_Order__r.Total_Cost_Range_Max__c == NULL) {gen.writeNullField('TotalCostRangeMax');} else{gen.writeNumberField('TotalCostRangeMax', Freightlist[0].Shipment_Order__r.Total_Cost_Range_Max__c);}  
                     if(Freightlist[0].Shipment_Order__r.Tax_Cost__c == NULL) {gen.writeNullField('TaxCost');} else{gen.writeNumberField('TaxCost', Freightlist[0].Shipment_Order__r.Tax_Cost__c);}  
                     if(Freightlist[0].Shipment_Order__r.Minimum_tax_range__c == NULL) {gen.writeNullField('Minimumtaxrange');} else{gen.writeNumberField('Minimumtaxrange', Freightlist[0].Shipment_Order__r.Minimum_tax_range__c);}  
                     if(Freightlist[0].Shipment_Order__r.Maximum_tax_range__c == NULL) {gen.writeNullField('Maximumtaxrange');} else{gen.writeNumberField('Maximumtaxrange', Freightlist[0].Shipment_Order__r.Maximum_tax_range__c);}  
                    
                  }
            	
            		
                 
                    gen.writeEndObject();
            		gen.writeEndObject();
            		String jsonData = gen.getAsString();
            		res.responseBody = Blob.valueOf(jsonData);
            		res.addHeader('Content-Type', 'application/json');
            		res.statusCode = 200;  
                  
                  
                  
                  
                API_Log__c Al = New API_Log__c();
                Al.EndpointURLName__c='FreightQuote';
                Al.StatusCode__c=string.valueof(res.statusCode);
                AL.Response__c = 'Success - FreightQuote sent';
                Insert Al;  
              }
              Else{
                  
            JSONGenerator gen = JSON.createGenerator(true);
            gen.writeStartObject();
            
             gen.writeFieldName('Success');
             gen.writeStartObject();
            
                 if(Freightlist.isempty()) {gen.writeStringField('Response', 'Freight are not available');}
            
            gen.writeEndObject();
            gen.writeEndObject();
            String jsonData = gen.getAsString();
            res.responseBody = Blob.valueOf(jsonData);
            res.addHeader('Content-Type', 'application/json');
            res.statusCode = 400;        
                  
               }   
                  
              }
                           
              
            } catch(Exception e){
                
                String ErrorString ='Something went wrong, please contact support_SF@tecex.com';
                       res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
                   res.addHeader('Content-Type', 'application/json');
                    res.statusCode = 400;
                
                API_Log__c Al = New API_Log__c();
               // Al.Account__c=rw.AccountID;
                //Al.Login_Contact__c=logincontact.Id;
                Al.EndpointURLName__c='GetCourierrates';
                Al.Response__c=ErrorString;
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al;
                      
          }
     
      }

}