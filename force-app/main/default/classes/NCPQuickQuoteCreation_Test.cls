@IsTest
public class NCPQuickQuoteCreation_Test {
    @testSetup
    static void setup(){
        Access_token__c accessTokenObj = new Access_token__c();
        accessTokenObj.Status__c = 'Active';
        accessTokenObj.Access_Token__c = 'asdfghj-sdfgfds-sfdfgf';
        
        Insert accessTokenObj;
        
        DefaultFreightValues__c df = new DefaultFreightValues__c();
        df.Name = 'Test';
        df.Courier_Base_Rate_KG__c = 12;
        df.Courier_Dangerous_Goods_premium__c = 12;
        df.Courier_Fixed_Charge__c = 12;
        df.Courier_Non_stackable_premium__c = 12;
        df.Courier_Oversized_premium__c = 12;
        df.FF_Base_Rate_KG__c = 12;
        df.FF_Dangerous_Goods_Fixed_Charge_Premium__c = 12;
        df.FF_Dangerous_Goods_premium__c = 12;
        df.FF_Dedicated_Pickup__c = 12;
        df.FF_Fixed_Charge__c = 12;
        df.FF_Non_stackable_Fixed_Charge_Premium__c = 12;
        df.FF_Non_stackable_premium__c = 12;
        df.FF_Oversized_Fixed_Charge_Premium__c = 12;
        df.FF_Oversized_premium__c = 12;
        insert df;
        
        RegionalFreightValues__c rfm = new RegionalFreightValues__c();
        rfm.Name = 'Oceania to South America';
        rfm.Courier_Discount_Premium__c = 12;
        rfm.Courier_Fixed_Premium__c = 10;
        rfm.Destination__c = 'Brazil';
        rfm.FF_Fixed_Premium__c = 123;
        rfm.FF_Regional_Discount_Premium__c = 12;
        rfm.Origin__c = 'Australia';
        rfm.Preferred_Courier_Id__c = 'TECEX';
        rfm.Preferred_Courier_Name__c = 'TECEX';
        rfm.Preferred_Freight_Forwarder_Id__c = 'TECEX';
        rfm.Preferred_Freight_Forwarder_Name__c = 'TECEX';
        rfm.Risk_Adjustment_Factor__c = 1;
        insert rfm;
        
        CountryandRegionMap__c crm = new CountryandRegionMap__c();
        crm.Name = 'Finland';
        crm.Country__c = 'Finland';
        crm.European_Union__c = 'No';
        crm.Region__c = 'South America';
        
        insert crm;
        
        CountryandRegionMap__c crm1 = new CountryandRegionMap__c();
        crm1.Name = 'United States';
        crm1.Country__c = 'United States';
        crm1.European_Union__c = 'No';
        crm1.Region__c = 'Oceania';
        
        insert crm1;
        
        Id accountRecTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Freight Supplier').getRecordTypeId();
        
        Account acc = new Account(Name = 'Testing',type = 'Supplier',CSE_IOR__c = '0050Y000001LTZO', 
                                  Service_Manager__c = '0050Y000001LTZO', 
                                  Financial_Manager__c='0050Y000001LTZO', 
                                  Financial_Controller__c='0050Y000001LTZO',RecordTypeId = accountRecTypeId);
        insert acc;
        
        Account account4 = new Account (name='TecEx Prospective Client', Type ='Client',New_Invoicing_Structure__c = true);
        insert account4;
        
        String accountRID = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Client (E-commerce)').getRecordTypeId();
        Account account5 = new Account (name='E-commerce Quick Quote Demo', RecordTypeId = accountRID,Type ='Client',New_Invoicing_Structure__c = true);
        insert account5;
        
        String accountRID1 = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Client').getRecordTypeId();
        Account account6 = new Account (name='TecEx Quick Quote Demo', RecordTypeId = accountRID1,Type ='Client',New_Invoicing_Structure__c = true);
        insert account6;
        
        Contact con2 = new Contact(FirstName = 'Testing',LastName = 'E-Commerce - Don\'t Delete',AccountId = account5.Id);
        insert con2;
        
        Contact con3 = new Contact(FirstName = 'Testing',LastName = 'TecEx - Don\'t Delete',AccountId = account6.Id);
        insert con3;
        
        IOR_Price_List__c priceList = new IOR_Price_List__c();
        priceList.Destination__c = 'Finland';
        priceList.Client_Name__c = acc.Id;
        priceList.IOR_Fee__c = 10;
        
        Insert priceList;
        
        IOR_Price_List__c iorpl1 = new IOR_Price_List__c ( Client_Name__c = account4.Id, Active__c = TRUE, Name = 'United States', IOR_Fee__c = 0.15, On_Charge_Mark_up__c = 0.15,
                                                          TecEx_Shipping_Fee_Markup__c =0.40, IOR_Min_Fee__c = 100,  Admin_Fee__c =100, Set_up_fee__c =100, Bank_Fees__c =200, Tax_Rate__c = 0.20,
                                                          Estimated_Customs_Brokerage__c =250, Estimated_Customs_Clearance__c =250, Estimated_Customs_Handling__c =250, Estimated_Import_License__c =250, Destination__c = 'United States' );
        
        insert iorpl1;
        
        Profile prof = [select id from profile where name LIKE '%Standard%']; 
        
        User user = new User();
        user.firstName = 'Firstname1@';
        user.lastName = 'lastname1@';
        user.profileId = prof.id;
        user.email = '123_1@test.com';
        user.username = '123_1@test.com';
        user.EmailEncodingKey = 'ISO-8859-1';
        user.Alias = 'Alias';
        user.TimeZoneSidKey = 'America/Los_Angeles';
        user.LanguageLocaleKey = 'en_US';
        user.LocaleSidKey = 'en_US';
        
        insert user;
        
        country_price_approval__c cpa = new country_price_approval__c( name ='Finland', Billing_term__c = 'DAP/CIF - IOR pays',  Airwaybill_Instructions__c = 'User IOR Address', Supplier__c = acc.Id, 
                                                                      In_Country_Specialist__c = '0050Y000001km5c', Clearance_Destination__c = 'Finland', Preferred_Supplier__c = TRUE, Destination__c = 'Finland' );
        insert cpa;
        
        Country__c country = new Country__c(Brokerage_Costs__c = 618, CIF_Adjustment_Factor__c = 0.035, CIF_Absolute_value_adjustment__c = 200, Clearance_Costs__c = 488, 
                                            Country__c = 'Finland', Name = 'Finland', Handling_Costs__c = 1180, License_Permit_Costs__c = 0);
        insert country;
        
        //newly added
        CPA_v2_0__c RelatedCostingCPA = new CPA_v2_0__c(Name = 'Finland',Country_Applied_Costings__c =TRUE);
        Insert RelatedCostingCPA;
        
        
        CPA_v2_0__c cpav2 = new CPA_v2_0__c(Name = 'Finland Testing 2.0', Supplier__c = acc.Id,Related_Costing_CPA__c = RelatedCostingCPA.Id, Service_Type__c = 'IOR/EOR',  Preferred_Supplier__c = TRUE,
                                            VAT_Rate__c = 0.1, Destination__c = 'Finland',  
                                            VAT_Reclaim_Destination__c = FALSE,
                                            Final_Destination__c ='Finland',
                                            Country__c = country.id
                                           );
        insert cpav2;
        
        List<CPARules__c> CPARules = new List<CPARules__c>();
        CPARules.add(new CPARules__c(Country__c = 'Finland',
                                     Criteria__c ='Based on Condition', 
                                     Chargable_weight__c = 'NONE', 
                                     Courier_Responsibility__c = 'NONE',   
                                     ECCN_No__c = 'NONE', 
                                     NL_Product_User__c = 'NONE', 
                                     Number_of_final_deliveries__c = 'NONE', 
                                     ShipmentValue__c ='NONE', 
                                     Sum_of_quantity_of_matched_products__c = 'NONE', 
                                     Manufacturer__c = 'NONE', 
                                     DefaultCPA2__c = cpav2.Id, 
                                     CPA2__c =  cpav2.Id));
        insert CPARules;
        
        Buyer_Accounts__c buyerAcc = new Buyer_Accounts__c(Name = 'Buyer Account Testing');
        insert buyerAcc;
        
        Contact con = new Contact(FirstName = 'Testing',LastName = 'contact',AccountId = acc.Id);
        insert con;
        EmailBlocker__c emailB = new EmailBlocker__c(Name='gmail',Type_of_business__c='Medical Equipment;Tech;Marketing material;Other;Amazon FBA');
		insert emailB;
    }
    
    static testMethod void testNCPQuickQuoteCreation(){
        Access_token__c accessTokenObj = [SELECT Access_Token__c FROM Access_token__c LIMIT 1];
        
        NCPQuickQuoteCreationWrap wrapperObj = new NCPQuickQuoteCreationWrap();
        wrapperObj.Accesstoken = accessTokenObj.Access_Token__c; 
        wrapperObj.shipFrom = 'Australia';
        wrapperObj.shipTo = 'United States';
        wrapperObj.shipmentValueUSD = 20000;
        wrapperObj.chargableWeight = 10;
        wrapperObj.weightUnit = 'KG';
        wrapperObj.typeOfGoods = 'Amazon FBA';
        wrapperObj.aboutShipment = 'Description for the shipment';
        wrapperObj.companyName = 'Test';
        wrapperObj.companyLocation = 'United States';
        wrapperObj.FirstName = 'Test';
        wrapperObj.LastName = 'User';
        //public String Name;
        wrapperObj.countryCode = '+91';
        wrapperObj.contactNumber = '9876543210';
        wrapperObj.emailAddress = 'abc@def.com';
        
        RestRequest request = new RestRequest();
        RestResponse res = new RestResponse();
        request.requestUri ='/services/apexrest/api/NCPQuickQuoteCreation/';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueOf(JSON.serialize(wrapperObj));
        RestContext.request = request;
        RestContext.response = res;
        
        Test.startTest();
        NCPQuickQuoteCreation.NCPQuickQuoteCreation();
        Test.stopTest();
    }
    
     static testMethod void testNCPQuickEmailBolcking(){
        Access_token__c accessTokenObj = [SELECT Access_Token__c FROM Access_token__c LIMIT 1];
        
        NCPQuickQuoteCreationWrap wrapperObj = new NCPQuickQuoteCreationWrap();
        wrapperObj.Accesstoken = accessTokenObj.Access_Token__c; 
        wrapperObj.shipFrom = 'Australia';
        wrapperObj.shipTo = 'United States';
        wrapperObj.shipmentValueUSD = 20000;
        wrapperObj.chargableWeight = 10;
        wrapperObj.weightUnit = 'LBs';
        wrapperObj.typeOfGoods = 'Other';
        wrapperObj.aboutShipment = 'Description for the shipment';
        wrapperObj.companyName = 'Test';
        wrapperObj.companyLocation = 'United States';
        wrapperObj.FirstName = 'Test';
        wrapperObj.LastName = 'User';
        //public String Name;
        wrapperObj.countryCode = '+91';
        wrapperObj.contactNumber = '9876543210';
        wrapperObj.emailAddress = 'abc@gmail.com';
        
        RestRequest request = new RestRequest();
        RestResponse res = new RestResponse();
        request.requestUri ='/services/apexrest/api/NCPQuickQuoteCreation/';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueOf(JSON.serialize(wrapperObj));
        RestContext.request = request;
        RestContext.response = res;
        
        Test.startTest();
        NCPQuickQuoteCreation.NCPQuickQuoteCreation();
        Test.stopTest();
    }
    
     static testMethod void testNCPQuickAuthFailed(){
        //Access_token__c accessTokenObj = [SELECT Access_Token__c FROM Access_token__c LIMIT 1];
        
        NCPQuickQuoteCreationWrap wrapperObj = new NCPQuickQuoteCreationWrap();
        wrapperObj.Accesstoken = 'ABC'; 
        wrapperObj.shipFrom = 'Australia';
        wrapperObj.shipTo = 'United States';
        wrapperObj.shipmentValueUSD = 20000;
        wrapperObj.chargableWeight = 10;
        wrapperObj.weightUnit = 'LBs';
        wrapperObj.typeOfGoods = 'Other';
        wrapperObj.aboutShipment = 'Description for the shipment';
        wrapperObj.companyName = 'Test';
        wrapperObj.companyLocation = 'United States';
        wrapperObj.FirstName = 'Test';
        wrapperObj.LastName = 'User';
        //public String Name;
        wrapperObj.countryCode = '+91';
        wrapperObj.contactNumber = '9876543210';
        wrapperObj.emailAddress = 'abc@gmail.com';
        
        RestRequest request = new RestRequest();
        RestResponse res = new RestResponse();
        request.requestUri ='/services/apexrest/api/NCPQuickQuoteCreation/';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueOf(JSON.serialize(wrapperObj));
        RestContext.request = request;
        RestContext.response = res;
        
        Test.startTest();
        NCPQuickQuoteCreation.NCPQuickQuoteCreation();
        Test.stopTest();
    }
    
     static testMethod void testNCPQuickInvalidEmail(){
        Access_token__c accessTokenObj = [SELECT Access_Token__c FROM Access_token__c LIMIT 1];
        
        NCPQuickQuoteCreationWrap wrapperObj = new NCPQuickQuoteCreationWrap();
        wrapperObj.Accesstoken = accessTokenObj.Access_Token__c; 
        wrapperObj.shipFrom = 'Australia';
        wrapperObj.shipTo = 'United States';
        wrapperObj.shipmentValueUSD = 20000;
        wrapperObj.chargableWeight = 10;
        wrapperObj.weightUnit = 'LBs';
        wrapperObj.typeOfGoods = 'Other';
        wrapperObj.aboutShipment = 'Description for the shipment';
        wrapperObj.companyName = 'Test';
        wrapperObj.companyLocation = 'United States';
        wrapperObj.FirstName = 'Test';
        wrapperObj.LastName = 'User';
        //public String Name;
        wrapperObj.countryCode = '+91';
        wrapperObj.contactNumber = '9876543210';
        wrapperObj.emailAddress = 'abcgmailcom';
        
        RestRequest request = new RestRequest();
        RestResponse res = new RestResponse();
        request.requestUri ='/services/apexrest/api/NCPQuickQuoteCreation/';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueOf(JSON.serialize(wrapperObj));
        RestContext.request = request;
        RestContext.response = res;
        
        Test.startTest();
        NCPQuickQuoteCreation.NCPQuickQuoteCreation();
        Test.stopTest();
    }
}