@IsTest (SeeAllData=true)
private class FileUploader_TestMethoD{

static testmethod void testfileupload(){
   
   Account account = new Account (name='Acme4', Type ='Client', CSE_IOR__c = '0050Y000001LTZO');
   insert account; 
   
   Contact contact = new Contact ( lastname ='Testing Individual',  AccountId = account.Id);
   insert contact; 
   
   country_price_approval__c cpa = new country_price_approval__c( name ='Aruba', Billing_term__c = 'DAP/CIF - IOR pays',  Airwaybill_Instructions__c = 'User IOR Address', Preferred_Supplier__c = True);
        insert cpa;
        
        IOR_Price_List__c iorpl = new IOR_Price_List__c ( Client_Name__c = account.Id, Name = 'Aruba', IOR_Fee__c = 0.15, On_Charge_Mark_up__c = 0.15,
        TecEx_Shipping_Fee_Markup__c =0.40, IOR_Min_Fee__c = 100,  Admin_Fee__c =100, Set_up_fee__c =100, Bank_Fees__c =200, Tax_Rate__c = 0.20,
        Estimated_Customs_Brokerage__c =250, Estimated_Customs_Clearance__c =250, Estimated_Customs_Handling__c =250, Estimated_Import_License__c =250);
        insert iorpl; 
   
   Roll_Out__c rollout = new Roll_Out__c( Client_Name__c = account.Id,   Contact_For_The_Quote__c = contact.Id);
   insert rollout;
   
   Cost_Estimate__c costEstimate = new Cost_Estimate__c( Client_Name__c = account.Id,   Contact_for_Quote__c = contact.Id, Roll_Out__c = rollout.Id, Shipment_Value_in_USD__c = 1000,
    Ship_to_Country__c = iorpl.Id,  Int_Courier_Responsibility_of__c = 'Client', Destination__c ='Aruba', IOR_Supplier__c = cpa.Id);
   insert costEstimate;
   
   
  StaticResource testdoc = [Select Id,Body from StaticResource where name ='testMethodCSVUpload'];
    

  
   test.startTest();
   
 PageReference pageRef = Page.uploadCSVCostEstimate2;
 Test.setCurrentPage(pageRef);
  pageRef.getParameters().put('id',rollout.id);

   PageReference pageRef2 = Page.ExportRollOut;
 Test.setCurrentPage(pageRef2);
  pageRef2.getParameters().put('id',rollout.id);
  

  
       
   importDataFromCSVController2  testUpload = new importDataFromCSVController2(new ApexPages.StandardController(rollOut));  
    testUpload.csvFileBody = testdoc.Body;
    testUpload.importCSVFile2();
    testUpload.exportAll();
  
    
            for(importDataFromCSVController2.cCostEstimate Costs: testUpload.costList )
            {
                Costs.selected = true;
            }
            testUpload.processSelected();  
   
    
     test.stopTest();
     }
}