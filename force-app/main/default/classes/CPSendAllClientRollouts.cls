@RestResource(urlMapping='/SendAllClientRollouts/*')
global class CPSendAllClientRollouts {
    
    @Httpget
    global static void CPSendAllClientRollout(){
    
    	RestRequest req = RestContext.request;
    	RestResponse res = RestContext.response;
        res.addHeader('Content-Type', 'application/json');
        String Username = req.params.get('Username');
        String Password = req.params.get('Password');
        String ClientAccId = req.params.get('ClientAccId');
        try {
            Contact logincontact = [select Id,Account.id, lastname,Email,Password__c,Contact_Status__c from Contact where email =:username and Contact_status__c = 'Active' ];
       
             if(logincontact.Password__c == Password && logincontact.Email == Username && logincontact.Contact_status__C == 'Active' ){
               
                    
                List<roll_out__c> ROs = [select id,name,Client_Name__r.name,Contact_For_The_Quote__r.name,Destinations__c,Shipment_Value_in_USD__c from roll_out__C where Client_Name__c =:ClientAccId ];
                
                    res.responseBody = Blob.valueOf(JSON.serializePretty(ROs));
        			res.statusCode = 200;
                
                API_Log__c Al = New API_Log__c();
                Al.Account__c=logincontact.AccountID;
                Al.Login_Contact__c=logincontact.Id;
                Al.EndpointURLName__c='SendAllClientRollouts';
                Al.Response__c='Success - List of Rollouts are sent';
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al;
               
            
              }
            else{
                    String ErrorString ='Authentication failed, Please check username and password';
               		res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
        			res.statusCode = 400;
                
                API_Log__c Al = New API_Log__c();
                Al.Account__c=logincontact.AccountID;
                Al.Login_Contact__c=logincontact.Id;
                Al.EndpointURLName__c='SendAllClientRollouts';
                Al.Response__c=ErrorString;
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al;
                
            	}


        }
        catch(exception e){
                String ErrorString ='Something went wrong, please contact support_SF@tecex.com';
               	res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
            res.statusCode = 500;
                API_Log__c Al = New API_Log__c();
                Al.Account__c=ClientAccId;
             //   Al.Login_Contact__c=logincontact.Id;
                Al.EndpointURLName__c='SendAllClientRollouts';
                Al.Response__c=e.getMessage()+'- UserName ->  '+Username;
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al;
            
            
        }
    
    
    
    
    }

}