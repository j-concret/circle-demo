global class invoicePenalty implements Database.Batchable<sObject> {


    global Database.QueryLocator start(Database.BatchableContext BC) {
        // collect the batches of records or objects to be passed to execute
        Id recordTypeIds = Schema.Sobjecttype.Invoice_New__c.getRecordTypeInfosByDeveloperName().get('Client').getRecordTypeId();
        DATE today = date.Today();
        String invoiceType = 'Invoice';
        String query = 'SELECT Due_Date__c, Id, Overdue__c,To_Due__c, Account__c  FROM Invoice_New__c  where Shipment_Order__c != NULL AND RecordTypeiD =\'' +String.escapeSingleQuotes(recordTypeIds)+ '\' AND Invoice_Type__c = \'' + String.escapeSingleQuotes(invoiceType) +'\' AND Amount_Outstanding__c > 1 AND  isPostSend__c = TRUE AND To_Due__c = 2';
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<Invoice_New__c> invoiceList) {
        
        Map<Id, Account> accountMap = new Map<Id,Account>([Select Id, Name, Region__c, Dimension__c, Invoicing_Term_Parameters__c, IOR_Payment_Terms__c, Invoice_Timing__c, New_Invoicing_Structure__c
                                                    From Account]);     
       
        for(Invoice_New__c invoice : invoiceList){ 	             
          
            If(accountMap.get(invoice.Account__c).New_Invoicing_Structure__c == TRUE ){ 
            invoice.Overdue__c = TRUE;     
        }
            
        }
        try {
        	// Update the Account Record
            update invoiceList;
        
        } catch(Exception e) {
            System.debug(e);
        }
        
    }   
    
    global void finish(Database.BatchableContext BC) {
    	// execute any post-processing operations
  }
}