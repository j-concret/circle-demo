@isTest
public class TPaymentsValidationTest implements HttpCalloutMock {
    private static HttpResponse resp;
    
    public TPaymentsValidationTest(String testBody,Boolean isTest) {
        resp = new HttpResponse();
        resp.setBody(testBody);
        
        if(isTest == true){
            resp.setStatus('OK');
            resp.setStatusCode(200);
        }else{
            resp.setStatus('Error');
            resp.setStatusCode(404);
        }
    }
    
    public HTTPResponse respond(HTTPRequest req) {
        return resp;
    }
    
    public testmethod static void testPositive() {
        String testBody = 'This is a test :-)';
        Account account = new Account (name='Acme1', Type ='Client',
                                       CSE_IOR__c = '0050Y000001LTZO',
                                       Eligible_for_Tipalti_Autopay__c = true,
                                       Service_Manager__c = '0050Y000001LTZO',
                                       Financial_Manager__c='0050Y000001LTZO',
                                       Financial_Controller__c='0050Y000001LTZO',
                                       Tax_recovery_client__c  = FALSE,
                                       Ship_From_Line_1__c = 'Ship_From_Line_1',
                                       Ship_From_Line_2__c = 'Ship_From_Line_2',
                                       Ship_From_Line_3__c = 'Ship_From_Line_3',
                                       Ship_From_City__c = 'Ship_From_City',
                                       Ship_From_Zip__c = 'Ship_From_Zip',
                                       Ship_From_Country__c = 'Ship_From_Country',
                                       Ship_From_Other_notes__c = 'Ship_From_Other',
                                       Ship_From_Contact_Name__c = 'Ship_From_Contact',
                                       Ship_From_Contact_Email__c = 'Ship_From_Email',
                                       Ship_From_Contact_Tel__c = 'Ship_From_Tel',
                                       Client_Address_Line_1__c = 'Client_Address_Line_1',
                                       Client_Address_Line_2__c = 'Client_Address_Line_2',
                                       Client_Address_Line_3__c = 'Client_Address_Line_3',
                                       Client_City__c = 'Client_City',
                                       Client_Zip__c = 'Client_Zip',
                                       Client_Country__c = 'Client_Country',
                                       Other_notes__c = 'Other_notes',
                                       Tax_Name__c = 'Tax_Name',
                                       Tax_ID__c = 'Tax_ID',
                                       Contact_Name__c = 'Contact_Name',
                                       Contact_Email__c = 'Contact_Email',
                                       Contact_Tel__c = 'Contact_Tel'
                                      );
        insert account;
        
        Account account2 = new Account (name='Test Supplier', Type ='Supplier', ICE__c = '0050Y000001km5c', RecordTypeId = '0120Y0000006KjQQAU');
        insert account2;
        
        Account account3 = new Account (name='Test Manufacturer', Type ='Manufacturer', RecordTypeId = '0120Y0000002wa0QAA', Manufacturer_Alias__c = 'Tester,Who' ); insert account3;
        
        Account account4 = new Account (name='ACME', Type ='Manufacturer', RecordTypeId = '0120Y0000002wa0QAA', Manufacturer_Alias__c = 'Buggs,Bunny' ); insert account4;
        
        
        Contact contact = new Contact ( lastname ='Testing Individual',Client_Notifications_Choice__c='Not applicable',  AccountId = account.Id);
        insert contact;
        
        Contact contact2 = new Contact ( lastname ='Testing supplier', Client_Notifications_Choice__c='Not applicable', AccountId = account2.Id);
        insert contact2;
        
        IOR_Price_List__c iorpl = new IOR_Price_List__c ( Client_Name__c = account.Id, Name = 'Brazil', IOR_Fee__c = 0.15, On_Charge_Mark_up__c = 0.15,
                                                         TecEx_Shipping_Fee_Markup__c =0.40, IOR_Min_Fee__c = 100,  Admin_Fee__c =100, Set_up_fee__c =100, Bank_Fees__c =200, Tax_Rate__c = 0.20,
                                                         Estimated_Customs_Brokerage__c =250, Estimated_Customs_Clearance__c =250, Estimated_Customs_Handling__c =250, Estimated_Import_License__c =250, Destination__c = 'Brazil' );
        insert iorpl;
        
        country_price_approval__c cpa = new country_price_approval__c( name ='Brazil', Billing_term__c = 'DAP/CIF - IOR pays',  Airwaybill_Instructions__c = 'User IOR Address', Supplier__c = account2.Id,
                                                                      In_Country_Specialist__c = '0050Y000001km5c', Clearance_Destination__c = 'Brazil', Preferred_Supplier__c = TRUE, Destination__c = 'Brazil' );
        insert cpa;
        
        Currency_Management2__c CM = new Currency_Management2__c (Name = 'US Dollar (USD)', Currency__c = 'US Dollar (USD)', ISO_Code__c = 'USD', Conversion_Rate__c = 1);
        insert CM;
        
        Country__c country = new Country__c(Brokerage_Costs__c = 618, CIF_Adjustment_Factor__c = 0.035, CIF_Absolute_value_adjustment__c = 200, Clearance_Costs__c = 488,
                                            Country__c = 'Brazil', Name = 'Brazil', Handling_Costs__c = 1180, License_Permit_Costs__c = 0);
        insert country;
        
        Tax_Structure_Per_Country__c tax = new Tax_Structure_Per_Country__c(Applied_to_Value__c = 'CIF', Country__c = country.Id, Default_Duty_Rate__c = 0.5625,
                                                                            Order_Number__c = '1', Part_Specific__c = TRUE, Name = 'II', Tax_Type__c = 'Duties');
        insert tax;
        
        CPA_v2_0__c RelatedCPA = new CPA_v2_0__c(Name = 'Brazil', Country_Applied_Costings__c=true );
        insert RelatedCPA;
        CPA_v2_0__c cpav2 = new CPA_v2_0__c(Name = 'Brazil Testing 2.0', Supplier__c = account2.Id, Service_Type__c = 'IOR/EOR', Related_Costing_CPA__c = RelatedCPA.Id, Preferred_Supplier__c = TRUE,
                                            VAT_Rate__c = 0.1, Destination__c = 'Brazil',
                                            
                                            Default_Section_1_Address_Line_1__c= 'Address_Line_1',
                                            Default_Section_1_Address_Line_2__c= 'Address_Line_2',
                                            Default_Section_1_Address_Line_3__c= 'Address_Line_3',
                                            Default_Section_1_City__c= 'City',
                                            Default_Section_1_Zip__c= 'Zip',
                                            Default_Section_1_Country__c = 'Country',
                                            Default_Section_1_Other__c= 'Other',
                                            Default_Section_1_Tax_Name__c= 'Tax_Name',
                                            Default_Section_1_Tax_Id__c= 'Tax_Id',
                                            Default_Section_1_Contact_Name__c= 'Contact_Name',
                                            Default_Section_1_Contact_Email__c= 'Contact_Email',
                                            Default_Section_1_Contact_Tel__c= 'Contact_Tel',
                                            Alternate_Section_1_Address_Line_1__c= 'Address_Line_1',
                                            Alternate_Section_1_Address_Line_2__c= 'Address_Line_2',
                                            Alternate_Section_1_Address_Line_3__c= 'Address_Line_3',
                                            Alternate_Section_1_City__c= 'City',
                                            Alternate_Section_1_Zip__c= 'Zip',
                                            Alternate_Section_1_Country__c = 'Country',
                                            Alternate_Section_1_Other__c = 'Other',
                                            Alternate_Section_1_Tax_Name__c = 'Tax_Name',
                                            Alternate_Section_1_Tax_Id__c = 'Tax_Id',
                                            Alternate_Section_1_Contact_Name__c= 'Contact_Name',
                                            Alternate_Section_1_Contact_Email__c= 'Contact_Email',
                                            Alternate_Section_1_Contact_Tel__c= 'Contact_Tel',
                                            Default_Section_2_Address_Line_1__c= 'Address_Line_1',
                                            Default_Section_2_Address_Line_2__c= 'Address_Line_2',
                                            Default_Section_2_Address_Line_3__c= 'Address_Line_3',
                                            Default_Section_2_City__c= 'City',
                                            Default_Section_2_Zip__c= 'Zip',
                                            Default_Section_2_Country__c = 'Country',
                                            Default_Section_2_Other__c= 'Other',
                                            Default_Section_2_Tax_Name__c= 'Tax_Name',
                                            Default_Section_2_Tax_Id__c= 'Tax_Id',
                                            Default_Section_2_Contact_Name__c= 'Contact_Name',
                                            Default_Section_2_Contact_Email__c= 'Contact_Email',
                                            Default_Section_2_Contact_Tel__c= 'Contact_Tel',
                                            Alternate_Section_2_Address_Line_1__c= 'Address_Line_1',
                                            Alternate_Section_2_Address_Line_2__c= 'Address_Line_2',
                                            Alternate_Section_2_Address_Line_3__c= 'Address_Line_3',
                                            Alternate_Section_2_City__c= 'City',
                                            Alternate_Section_2_Zip__c= 'Zip',
                                            Alternate_Section_2_Country__c = 'Country',
                                            Alternate_Section_2_Other__c = 'Other',
                                            Alternate_Section_2_Tax_Name__c = 'Tax_Name',
                                            Alternate_Section_2_Tax_Id__c = 'Tax_Id',
                                            Alternate_Section_2_Contact_Name__c= 'Contact_Name',
                                            Alternate_Section_2_Contact_Email__c= 'Contact_Email',
                                            Alternate_Section_2_Contact_Tel__c= 'Contact_Tel',
                                            Default_Section_3_Address_Line_1__c= 'Address_Line_1',
                                            Default_Section_3_Address_Line_2__c= 'Address_Line_2',
                                            Default_Section_3_Address_Line_3__c= 'Address_Line_3',
                                            Default_Section_3_City__c= 'City',
                                            Default_Section_3_Zip__c= 'Zip',
                                            Default_Section_3_Country__c = 'Country',
                                            Default_Section_3_Other__c= 'Other',
                                            Default_Section_3_Tax_Name__c= 'Tax_Name',
                                            Default_Section_3_Tax_Id__c= 'Tax_Id',
                                            Default_Section_3_Contact_Name__c= 'Contact_Name',
                                            Default_Section_3_Contact_Email__c= 'Contact_Email',
                                            Default_Section_3_Contact_Tel__c= 'Contact_Tel',
                                            Alternate_Section_3_Address_Line_1__c= 'Address_Line_1',
                                            Alternate_Section_3_Address_Line_2__c= 'Address_Line_2',
                                            Alternate_Section_3_Address_Line_3__c= 'Address_Line_3',
                                            Alternate_Section_3_City__c= 'City',
                                            Alternate_Section_3_Zip__c= 'Zip',
                                            Alternate_Section_3_Country__c = 'Country',
                                            Alternate_Section_3_Other__c = 'Other',
                                            Alternate_Section_3_Tax_Name__c = 'Tax_Name',
                                            Alternate_Section_3_Tax_Id__c = 'Tax_Id',
                                            Alternate_Section_3_Contact_Name__c= 'Contact_Name',
                                            Alternate_Section_3_Contact_Email__c= 'Contact_Email',
                                            Alternate_Section_3_Contact_Tel__c= 'Contact_Tel',
                                            Default_Section_4_Address_Line_1__c= 'Address_Line_1',
                                            Default_Section_4_Address_Line_2__c= 'Address_Line_2',
                                            Default_Section_4_Address_Line_3__c= 'Address_Line_3',
                                            Default_Section_4_City__c= 'City',
                                            Default_Section_4_Zip__c= 'Zip',
                                            Default_Section_4_Country__c = 'Country',
                                            Default_Section_4_Other__c= 'Other',
                                            Default_Section_4_Tax_Name__c= 'Tax_Name',
                                            Default_Section_4_Tax_Id__c= 'Tax_Id',
                                            Default_Section_4_Contact_Name__c= 'Contact_Name',
                                            Default_Section_4_Contact_Email__c= 'Contact_Email',
                                            Default_Section_4_Contact_Tel__c= 'Contact_Tel',
                                            Alternate_Section_4_Address_Line_1__c= 'Address_Line_1',
                                            Alternate_Section_4_Address_Line_2__c= 'Address_Line_2',
                                            Alternate_Section_4_Address_Line_3__c= 'Address_Line_3',
                                            Alternate_Section_4_City__c= 'City',
                                            Alternate_Section_4_Zip__c= 'Zip',
                                            Alternate_Section_4_Country__c = 'Country',
                                            Alternate_Section_4_Other__c = 'Other',
                                            Alternate_Section_4_Tax_Name__c = 'Tax_Name',
                                            Alternate_Section_4_Tax_Id__c = 'Tax_Id',
                                            Alternate_Section_4_Contact_Name__c= 'Contact_Name',
                                            Alternate_Section_4_Contact_Email__c= 'Contact_Email',
                                            Alternate_Section_4_Contact_Tel__c= 'Contact_Tel',
                                            VAT_Reclaim_Destination__c = FALSE,
                                            Final_Destination__c ='Brazil',
                                            Country__c = country.id
                                            
                                           );
        insert cpav2;
        
        List<CPARules__c> CPARules = new List<CPARules__c>();
        CPARules.add(new CPARules__c(Country__c = 'Brazil',Criteria__c ='Default', Chargable_weight__c = 'NONE', Courier_Responsibility__c = 'NONE',   ECCN_No__c = 'NONE', NL_Product_User__c = 'NONE', Number_of_final_deliveries__c = 'NONE', ShipmentValue__c ='NONE', Sum_of_quantity_of_matched_products__c = 'NONE', Manufacturer__c = 'NONE', DefaultCPA2__c = cpav2.Id, CPA2__c =  cpav2.Id));
        insert CPARules;
        
        List<CPA_Costing__c> costs = new List<CPA_Costing__c>();
        costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'Shipment value', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'IOR', Min__c = 375, Max__c = 14000, Conditional_value__c = 'Shipment value', Condition__c = 'Between', Floor__c = 0, Ceiling__c = 10000, Currency__c = 'US Dollar (USD)'));
        costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'Anil', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'IOR', Currency__c = 'US Dollar (USD)',Variable_threshold__c = null));
        insert costs;
        
        
        Product2 product = new Product2(Name = 'CAB-ETH-S-RJ45', US_HTS_Code__c='85444210',description='Anil', ProductCode = 'CAB-ETH-S-RJ45', Manufacturer__c = account3.Id); insert product;
        Product2 product2 = new Product2(Name = 'CAB-ETH-S-RJ46',US_HTS_Code__c='85444210', ProductCode = 'CAB-ETH-S-RJ46', Manufacturer__c = account3.Id); insert product2;
        Product2 product3 = new Product2(Name = 'CAB-ETH-S', US_HTS_Code__c='85444210',ProductCode = 'CAB-ETH-S', Manufacturer__c = account3.Id); insert product3;
        
        
        
        
        Shipment_Order__c shipmentOrder = new Shipment_Order__c(Account__c = account.id, Shipment_Value_USD__c = 2000, CPA_v2_0__c = cpav2.Id,
                                                                Client_Contact_for_this_Shipment__c = contact.Id, Who_arranges_International_courier__c ='Client', Tax_Treatment__c ='DAP/CIF - IOR pays',
                                                                Ship_to_Country__c = cpa.Id, Destination__c = 'Brazil', Service_Type__c = 'IOR', IOR_Price_List__c = iorpl.Id, Chargeable_Weight__c = 200,
                                                                Shipping_Status__c = 'Cost Estimate Abandoned' );
        Test.startTest(); 
        insert shipmentOrder; 
        
        String supplierRTypeId = Schema.Sobjecttype.Invoice_New__c.getRecordTypeInfosByDeveloperName().get('Supplier').getRecordTypeId();
        List<Invoice_New__c> invList = new List<Invoice_New__c>();       
        Invoice_New__c inv1 = new Invoice_New__c(
            Actual_Miscellaneous_Costs__c = 100,
            Actual_Bank_Costs__c  = 100,
            Actual_Finance_Costs__c = 100,
            Actual_Duties_and_Taxes__c  = 100,
            Actual_International_Delivery_Costs__c =100,
            Actual_Customs_Brokerage_Costs__c  = 100,
            Actual_Insurance_Costs__c  = 100,
            Actual_Admin_Costs__c = 100,
            Invoice_amount_USD__c = 800,
            Invoice_Amount_Local_Currency__c = 800,
            Due_Date__c = System.today(),
            Account__c = account.Id,
            Conversion_Rate__c = 1,
            Shipment_Order__c = shipmentOrder.Id,
            Submitted_to_Tipalti__c = false,
            Invoice_Currency__c = 'US Dollar (USD)',
            Invoice_Type__c = 'Invoice',
            RecordTypeId = supplierRTypeId,
            Payment_Tracking__c = 'Ready For Payment',
            Auto_Pay__c = 'Yes',
            Tipalti_validation_completed__c = false,
            Invoice_Status__c = 'FM Approved'
            //Enterprise_Billing__c = shipmentOrders.Enterprise_Billing_Id__c
        );
        invList.add(inv1);
        
        insert invList;
        
        HttpCalloutMock mock = new TPaymentsValidationTest(testBody,true);
        Test.setMock(HttpCalloutMock.class, mock);
        TPaymentsValidation.checkvalidation();
        System.assertEquals(200, resp.getStatusCode());        
        System.assertEquals(resp.getBody(), testBody );
        Test.stopTest();
    }
    public testmethod static void testNegative() {
        String testBody = 'This is a test :-)';
        
        HttpCalloutMock mock = new TPaymentsValidationTest(testBody,false);
        Test.setMock(HttpCalloutMock.class, mock);
        
        //TPayments callout = new TPayments();
        TPaymentsValidation.checkvalidation();
        //TPaymentsSubmission.submit();
        System.assertEquals(404, resp.getStatusCode());        
        System.assertEquals(resp.getBody(), testBody );
    }
    
}