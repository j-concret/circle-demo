public class cpa2GeneralDetail_V3_Ctrl {
    
    @AuraEnabled
    public static String getCPARecordTypeName(String recordTypeId){
        
        CPA_v2_0__c cpa = [Select Id, RecordType.Name from CPA_v2_0__c Where Id = :recordTypeId];
		
        if(cpa.RecordType.Name == null){
            return null;
        }
        return cpa.RecordType.Name;
        
    }

}