@IsTest
public class CPAddressManagement_Test {
        static testMethod void  setUpData(){
          Account account = new Account (name='Acme1', Type ='Client', 
                                       CSE_IOR__c = '0050Y000001LTZO', 
                                       Service_Manager__c = '0050Y000001LTZO', 
                                       Financial_Manager__c='0050Y000001LTZO', 
                                       Financial_Controller__c='0050Y000001LTZO', 
                                       Tax_recovery_client__c  = FALSE,
                                       Ship_From_Line_1__c = 'Ship_From_Line_1',
                                       Ship_From_Line_2__c = 'Ship_From_Line_2',
                                       Ship_From_Line_3__c = 'Ship_From_Line_3',
                                       Ship_From_City__c = 'Ship_From_City', 
                                       Ship_From_Zip__c = 'Ship_From_Zip',
                                       Ship_From_Country__c = 'Ship_From_Country' , 
                                       Ship_From_Other_notes__c = 'Ship_From_Other',
                                       Ship_From_Contact_Name__c = 'Ship_From_Contact',
                                       Ship_From_Contact_Email__c = 'Ship_From_Email',
                                       Ship_From_Contact_Tel__c = 'Ship_From_Tel',
                                       Client_Address_Line_1__c = 'Client_Address_Line_1',
                                       Client_Address_Line_2__c = 'Client_Address_Line_2',
                                       Client_Address_Line_3__c = 'Client_Address_Line_3',
                                       Client_City__c = 'Client_City',
                                       Client_Zip__c = 'Client_Zip',
                                       Client_Country__c = 'Client_Country', 
                                       Other_notes__c = 'Other_notes',
                                       Tax_Name__c = 'Tax_Name',
                                       Tax_ID__c = 'Tax_ID',
                                       Contact_Name__c = 'Contact_Name',
                                       Contact_Email__c = 'Contact_Email',
                                       Contact_Tel__c = 'Contact_Tel'
                                      );
        insert account;  
         
         
        Account account2 = new Account (name='Test Supplier', Type ='Supplier', ICE__c = '0050Y000001km5c', RecordTypeId = '0120Y0000006KjQQAU');
        insert account2;        
         
        Contact contact = new Contact ( lastname ='Testing Individual',  AccountId = account.Id,Email='anilk@tecex.com',UserName__c='anilk@tecex.com',Password__c='Test@1234',Contact_status__C = 'Active');
        insert contact;  
        
        Contact contact2 = new Contact ( lastname ='Testing supplier',  AccountId = account2.Id);
        insert contact2; 
          
           Id recordTypeId = Schema.SObjectType.Client_Address__c.getRecordTypeInfosByDeveloperName().get('PickUp').getRecordTypeId();
           Client_Address__c PickAdd1 = new Client_Address__c(RecordTypeID=recordTypeId,
         												Name='Name',
                                                        Contact_Full_Name__c='FullName',
                                                        Contact_Email__c='anilk@teccex.com',
                                                        Contact_Phone_Number__c='2563563',
                                                        Address__c='Add1',
                                                        Address2__c='Add2',
                                                        City__c='City',
                                                        Province__c='Proince',
                                                        Postal_Code__c='5236',
                                                        Default__C=TRUE,
                                                              Address_Status__c='Active',
                                                        All_Countries__c= 'South Africa',
                                                        Client__c=account.id ) ;  
          INsert PickAdd1;
         
         CPDefaults__c CPD = new CPDefaults__c(
       			Client_Account__c = account.id,
       			Order_Type__c ='Order',
       			Chargeable_Weight_Units__c ='KGs',
       			Li_ion_Batteries__c ='No',
       			Package_Dimensions__c ='Cm',
       			Package_Weight_Units__c ='LBS',
       			Second_hand_parts__c='No',
                Ship_From__c='South Africa');
         Insert CPD;
         
           
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
       
       
        req.requestURI = '/services/apexrest/MyAllAddresses'; 
        req.httpMethod = 'GET';
        req.addHeader('Content-Type', 'application/json');
        req.addParameter('ClientAccId',account.id);
        
        
        RestContext.request = req;
        RestContext.response = res;

         Test.startTest();
         CPAddressManagement.CPPickupAddress();
         Test.stopTest();
         
    }
     static testMethod void  setUpData2(){
          Account account = new Account (name='Acme1', Type ='Client', 
                                       CSE_IOR__c = '0050Y000001LTZO', 
                                       Service_Manager__c = '0050Y000001LTZO', 
                                       Financial_Manager__c='0050Y000001LTZO', 
                                       Financial_Controller__c='0050Y000001LTZO', 
                                       Tax_recovery_client__c  = FALSE,
                                       Ship_From_Line_1__c = 'Ship_From_Line_1',
                                       Ship_From_Line_2__c = 'Ship_From_Line_2',
                                       Ship_From_Line_3__c = 'Ship_From_Line_3',
                                       Ship_From_City__c = 'Ship_From_City', 
                                       Ship_From_Zip__c = 'Ship_From_Zip',
                                       Ship_From_Country__c = 'Ship_From_Country' , 
                                       Ship_From_Other_notes__c = 'Ship_From_Other',
                                       Ship_From_Contact_Name__c = 'Ship_From_Contact',
                                       Ship_From_Contact_Email__c = 'Ship_From_Email',
                                       Ship_From_Contact_Tel__c = 'Ship_From_Tel',
                                       Client_Address_Line_1__c = 'Client_Address_Line_1',
                                       Client_Address_Line_2__c = 'Client_Address_Line_2',
                                       Client_Address_Line_3__c = 'Client_Address_Line_3',
                                       Client_City__c = 'Client_City',
                                       Client_Zip__c = 'Client_Zip',
                                       Client_Country__c = 'Client_Country', 
                                       Other_notes__c = 'Other_notes',
                                       Tax_Name__c = 'Tax_Name',
                                       Tax_ID__c = 'Tax_ID',
                                       Contact_Name__c = 'Contact_Name',
                                       Contact_Email__c = 'Contact_Email',
                                       Contact_Tel__c = 'Contact_Tel'
                                      );
        insert account;  
         
         
        Account account2 = new Account (name='Test Supplier', Type ='Supplier', ICE__c = '0050Y000001km5c', RecordTypeId = '0120Y0000006KjQQAU');
        insert account2;        
         
        Contact contact = new Contact ( lastname ='Testing Individual',  AccountId = account.Id,Email='anilk@tecex.com',UserName__c='anilk@tecex.com',Password__c='Test@1234',Contact_status__C = 'Active');
        insert contact;  
        
        Contact contact2 = new Contact ( lastname ='Testing supplier',  AccountId = account2.Id);
        insert contact2; 
          
           Id recordTypeId = Schema.SObjectType.Client_Address__c.getRecordTypeInfosByDeveloperName().get('PickUp').getRecordTypeId();
           Client_Address__c PickAdd1 = new Client_Address__c(RecordTypeID=recordTypeId,
         												Name='Name',
                                                        Contact_Full_Name__c='FullName',
                                                        Contact_Email__c='anilk@teccex.com',
                                                        Contact_Phone_Number__c='2563563',
                                                        Address__c='Add1',
                                                        Address2__c='Add2',
                                                        City__c='City',
                                                        Province__c='Proince',
                                                        Postal_Code__c='5236',
                                                        Default__C=TRUE,
                                                              Address_Status__c='Active',
                                                        All_Countries__c= 'South Africa',
                                                        Client__c=account.id ) ;  
        //  INsert PickAdd1;
         
         CPDefaults__c CPD = new CPDefaults__c(
       			Client_Account__c = account.id,
       			Order_Type__c ='Order',
       			Chargeable_Weight_Units__c ='KGs',
       			Li_ion_Batteries__c ='No',
       			Package_Dimensions__c ='Cm',
       			Package_Weight_Units__c ='LBS',
       			Second_hand_parts__c='No',
                Ship_From__c='South Africa');
         Insert CPD;
         
           
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
       
       
        req.requestURI = '/services/apexrest/MyAllAddresses'; 
        req.httpMethod = 'GET';
        req.addHeader('Content-Type', 'application/json');
        req.addParameter('ClientAccId',account.id);
        
        
        RestContext.request = req;
        RestContext.response = res;

         Test.startTest();
         CPAddressManagement.CPPickupAddress();
         Test.stopTest();
         
    }
     static testMethod void  setUpData3(){
          Account account = new Account (name='Acme1', Type ='Client', 
                                       CSE_IOR__c = '0050Y000001LTZO', 
                                       Service_Manager__c = '0050Y000001LTZO', 
                                       Financial_Manager__c='0050Y000001LTZO', 
                                       Financial_Controller__c='0050Y000001LTZO', 
                                       Tax_recovery_client__c  = FALSE,
                                       Ship_From_Line_1__c = 'Ship_From_Line_1',
                                       Ship_From_Line_2__c = 'Ship_From_Line_2',
                                       Ship_From_Line_3__c = 'Ship_From_Line_3',
                                       Ship_From_City__c = 'Ship_From_City', 
                                       Ship_From_Zip__c = 'Ship_From_Zip',
                                       Ship_From_Country__c = 'Ship_From_Country' , 
                                       Ship_From_Other_notes__c = 'Ship_From_Other',
                                       Ship_From_Contact_Name__c = 'Ship_From_Contact',
                                       Ship_From_Contact_Email__c = 'Ship_From_Email',
                                       Ship_From_Contact_Tel__c = 'Ship_From_Tel',
                                       Client_Address_Line_1__c = 'Client_Address_Line_1',
                                       Client_Address_Line_2__c = 'Client_Address_Line_2',
                                       Client_Address_Line_3__c = 'Client_Address_Line_3',
                                       Client_City__c = 'Client_City',
                                       Client_Zip__c = 'Client_Zip',
                                       Client_Country__c = 'Client_Country', 
                                       Other_notes__c = 'Other_notes',
                                       Tax_Name__c = 'Tax_Name',
                                       Tax_ID__c = 'Tax_ID',
                                       Contact_Name__c = 'Contact_Name',
                                       Contact_Email__c = 'Contact_Email',
                                       Contact_Tel__c = 'Contact_Tel'
                                      );
        insert account;  
         
         
        Account account2 = new Account (name='Test Supplier', Type ='Supplier', ICE__c = '0050Y000001km5c', RecordTypeId = '0120Y0000006KjQQAU');
        insert account2;        
         
        Contact contact = new Contact ( lastname ='Testing Individual',  AccountId = account.Id,Email='anilk@tecex.com',UserName__c='anilk@tecex.com',Password__c='Test@1234',Contact_status__C = 'Active');
        insert contact;  
        
        Contact contact2 = new Contact ( lastname ='Testing supplier',  AccountId = account2.Id);
        insert contact2; 
          
           Id recordTypeId = Schema.SObjectType.Client_Address__c.getRecordTypeInfosByDeveloperName().get('PickUp').getRecordTypeId();
           Client_Address__c PickAdd1 = new Client_Address__c(RecordTypeID=recordTypeId,
         												Name='Name',
                                                        Contact_Full_Name__c='FullName',
                                                        Contact_Email__c='anilk@teccex.com',
                                                        Contact_Phone_Number__c='2563563',
                                                        Address__c='Add1',
                                                        Address2__c='Add2',
                                                        City__c='City',
                                                        Province__c='Proince',
                                                        Postal_Code__c='5236',
                                                        Default__C=TRUE,
                                                              Address_Status__c='Active',
                                                        All_Countries__c= 'South Africa',
                                                        Client__c=account.id ) ;  
        //  INsert PickAdd1;
         
         CPDefaults__c CPD = new CPDefaults__c(
       			Client_Account__c = account.id,
       			Order_Type__c ='Order',
       			Chargeable_Weight_Units__c ='KGs',
       			Li_ion_Batteries__c ='No',
       			Package_Dimensions__c ='Cm',
       			Package_Weight_Units__c ='LBS',
       			Second_hand_parts__c='No',
                Ship_From__c='South Africa');
         Insert CPD;
         
           
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
       
       
        req.requestURI = '/services/apexrest/MyAllAddresses'; 
        req.httpMethod = 'GET';
        req.addHeader('Content-Type', 'application/json');
        req.addParameter('ClientAccId',account2.id);
        
        
        RestContext.request = req;
        RestContext.response = res;

         Test.startTest();
         CPAddressManagement.CPPickupAddress();
         Test.stopTest();
         
    }
    
    

}