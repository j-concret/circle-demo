global class cashOutlay implements Database.Batchable<sObject> {

    Id recordTypeIds = Schema.Sobjecttype.Invoice_New__c.getRecordTypeInfosByDeveloperName().get('Client').getRecordTypeId();
    String invoiceType = 'Invoice';
    global final String query = 'SELECT POD_Date_New__c, Conversion_Rate__c, Accounting_Period_new__c, Prepayment_Date__c, Id, Account__c, Due_Date__c, Invoice_Sent_Date__c, Shipment_Order__c, Possible_Cash_Outlay__c, Invoice_Currency__c FROM Invoice_New__c  where Shipment_Order__c != NULL AND RecordTypeiD =\'' +String.escapeSingleQuotes(recordTypeIds)+ '\' AND Invoice_Type__c = \'' + String.escapeSingleQuotes(invoiceType) +'\' AND Amount_Outstanding__c > 1 AND  isPostSend__c = TRUE AND Account__r.New_Invoicing_Structure__c = TRUE AND Upfront_No_Terms__c = TRUE AND To_Prepaid__c = 1';
    global list<ID> invoiceIds = new list<ID>();
    global Map<Id, Invoice_New__c> invoicesToUpdate = new Map<Id, Invoice_New__c>();

    global Database.QueryLocator start(Database.BatchableContext BC) {
        // collect the batches of records or objects to be passed to execute
        
     return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<Invoice_New__c> invoiceList) {
        
        system.debug('invoiceList.size()----->'+invoiceList.size());
        system.debug('invoiceList----->'+invoiceList);
       
        // process each batch of records
        List<Invoice_New__c> cashOutlayInvoices = new List<Invoice_New__c>();
        List<Shipment_Order__c> cashOutlayInvoicesSO = new List<Shipment_Order__c>();
        Set<Invoice_New__c> cashOutlayInvoicesSet = new Set<Invoice_New__c>();
        List<Invoice_New__c> finalCashOutlayInvoices = new List<Invoice_New__c>();
        
        Map<Id, Shipment_Order__c> soMap = new Map<Id, Shipment_Order__c>([Select Finance_Fee__c, Finance_fee_percentage__c from Shipment_Order__c Where Outstanding__c > 0 AND Populate_Invoice__c = TRUE]);
        
        Map<Id, Account> accountMap = new Map<Id,Account>([Select Id, Name, Region__c, Dimension__c, Invoicing_Term_Parameters__c, IOR_Payment_Terms__c, Invoice_Timing__c, Finance_Charge__c
                                                    From Account]);
        
        for(Invoice_New__c invoice : invoiceList)
        { 	
           iF(date.today() == invoice.Prepayment_Date__c.addDays(1)){
               
               
           Invoice_New__c cashOutLay = new Invoice_New__c(
           Account__c = invoice.Account__c,
           Due_Date__c = invoice.Due_Date__c,
           Invoice_Sent_Date__c = invoice.Invoice_Sent_Date__c,
           Invoice_Amount_Local_Currency__c = invoice.Possible_Cash_Outlay__c ,
           Invoice_amount_USD__c = invoice.Possible_Cash_Outlay__c * invoice.Conversion_Rate__c,
           Invoice_Currency__c = invoice.Invoice_Currency__c,
           Invoice_Type__c = 'Cash Outlay Invoice',
           Invoice_Status__c = 'Invoice Sent',  
           POD_Date_New__c = invoice.POD_Date_New__c,
           Conversion_Rate__c =  invoice.Conversion_Rate__c,
           RecordTypeId = Schema.Sobjecttype.Invoice_New__c.getRecordTypeInfosByDeveloperName().get('Client').getRecordTypeId(),
           Shipment_Order__c = invoice.Shipment_Order__c,
           Accounting_Period_New__c = invoice.Accounting_Period_New__c,
           Transaction_Total__c = invoice.Possible_Cash_Outlay__c * invoice.Conversion_Rate__c,
           Neg_Transaction_Total__c =   (invoice.Possible_Cash_Outlay__c * invoice.Conversion_Rate__c) * -1,
           Cash_Outlay_Fee__c = invoice.Possible_Cash_Outlay__c);
           cashOutlayInvoices.add(cashOutLay);
            
               
           invoice.Cash_Outlay_Invoice_Created__c = TRUE;
           
            
            
            FOR(Id sos : soMap.keySet()){
                
                If(sos == invoice.Shipment_Order__c){
                     
                soMap.get(invoice.Shipment_Order__c).Finance_fee_percentage__c = accountMap.get(invoice.Account__c).Finance_Charge__c;  
                soMap.get(invoice.Shipment_Order__c).Finance_Fee__c = invoice.Possible_Cash_Outlay__c; 
                cashOutlayInvoicesSO.add(soMap.get(invoice.Shipment_Order__c));
                }
                
                
                
            }
            

        }
            
        }
        try {
        	// Update the Account Record
            insert cashOutlayInvoices;
            
            
            
           
            update cashOutlayInvoicesSO;
            
            for(Invoice_New__c invoiceCheck: cashOutlayInvoices){
                
               invoicesToUpdate.put(invoiceCheck.id,invoiceCheck);    
            }
            
            
            system.debug('invoicesToUpdate---->'+invoicesToUpdate);
            system.debug('invoicesToUpdate.size()---->'+invoicesToUpdate.size());
            
            
            
           
            update invoiceList;
            
        } catch(Exception e) {
            System.debug(e);
        }
        
    }   
    
    global void finish(Database.BatchableContext BC) {
    	// execute any post-processing operations
  }
}