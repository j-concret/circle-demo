@RestResource(urlMapping='/NCPRollOutCreation/*')
Global class NCPRolloutCreation {

    @Httppost
    global static void NCPRolloutCreation(){
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        Blob body = req.requestBody;
        res.addHeader('Content-Type', 'application/json');
        String requestString = body.toString();
        NCPRolloutWrapper rw = (NCPRolloutWrapper)JSON.deserialize(requestString,NCPRolloutWrapper.class);


        try {
            Access_token__c At =[select status__c,Access_Token__c from Access_token__c where Access_Token__c=:rw.Accesstoken ];
            if( at.status__c =='Active') {

                double CummActWeight=0;
                double Actweight=0;
                double CummVolWeight=0;
                double Volweight=0;
                double CummChargableWeight=0;    // Final
                double charweight=0;
                double no_of_packages = 0;
                user usr=[select id,contactid from user where contactid=:rw.ContactID limit 1];

                //Calculate Chargable weight
                if(!rw.ShipmentOrder_Packages.isEmpty()) {
                    for(Integer l=0; rw.ShipmentOrder_Packages.size()>l; l++) {

                        if(rw.ShipmentOrder_Packages[l].Weight_Unit =='KGs') {
                            Actweight = (rw.ShipmentOrder_Packages[l].Actual_Weight*rw.ShipmentOrder_Packages[l].Packages_of_Same_Weight);
                        } else{
                            Actweight=(rw.ShipmentOrder_Packages[l].Actual_Weight*rw.ShipmentOrder_Packages[l].Packages_of_Same_Weight*0.453592);
                        }
                        if(rw.ShipmentOrder_Packages[l].Dimension_Unit =='CMs') {
                            Volweight = ((rw.ShipmentOrder_Packages[l].Length*rw.ShipmentOrder_Packages[l].Breadth*rw.ShipmentOrder_Packages[l].Height)/5000)*rw.ShipmentOrder_Packages[l].Packages_of_Same_Weight;
                        } else{
                            Volweight = ((rw.ShipmentOrder_Packages[l].Length*rw.ShipmentOrder_Packages[l].Breadth*rw.ShipmentOrder_Packages[l].Height)/305.1200198)*rw.ShipmentOrder_Packages[l].Packages_of_Same_Weight;
                        }
                        if(Actweight >= Volweight) {
                            charweight = Actweight;
                        } else{
                            charweight = Volweight;
                        }

                        no_of_packages += rw.ShipmentOrder_Packages[l].Packages_of_Same_Weight;
                        CummActWeight=CummActWeight+Actweight;
                        CummVolWeight=CummVolWeight+Volweight;
                        CummChargableWeight=CummChargableWeight+charweight;
                    }
                }

                Roll_Out__c new_roll_out = new Roll_Out__c( );
                List<String> destinations =  rw.ShipTo.split(';');

                new_roll_out = new Roll_Out__c( Client_Name__c=rw.AccountID,
                                                Contact_For_The_Quote__c=rw.ContactId,
                                                Shipment_Value_in_USD__c=rw.Shipment_Value_USD,
                                                Destinations__c = rw.ShipTo,
                                               Createdbyid=usr.id,
                                                Number_of_Cost_Estimates__c = destinations.size()
                                                );
                Insert new_roll_out;


                Account ac = [SELECT ID,Lead_AM__c,Freight_preference__c,Compliance__c,Service_Manager__c,Operations_manager__C,Vat_Team__c,Financial_Manager__c,Financial_Controller__c,Logistics_Coordinator__c,cse_IOR__c,finance_Team__C,CDC__c,invoice_Timing__c FROM Account WHERE id =:rw.AccountID ];
/*
 // for 12965 & 21670 tickets
  
                        if(ac.Freight_preference__c == 'TecEx' || ac.Freight_preference__c=='Case-by-case')
                        { rw.Courier_responsibility ='Tecex';
                        }
                        else { rw.Courier_responsibility ='Client';
                        }
  // end 12965& 21670    
 */              
                List<Shipment_Order__c> SOList = new List<Shipment_Order__c>();

                for(String destination: destinations) {

                    Shipment_Order__c Cos = new Shipment_Order__c();
                    Cos.Account__c = rw.AccountID;
                    Cos.Client_Contact_for_this_Shipment__c = rw.ContactID;
                    Cos.Shipment_Value_USD__c= rw.Shipment_Value_USD;
                    Cos.Source__c = 'Client Platform';
                    Cos.Who_arranges_International_courier__c= rw.Courier_responsibility;
                    Cos.Type_of_Goods__c=rw.Type_of_Goods;
                    Cos.Li_ion_Batteries__c = rw.Li_ion_Batteries;
                    Cos.Lithium_Battery_Types__c=rw.Li_ion_BatteryTypes;
                    Cos.buyer_account__c =rw.BuyerId;
                    cos.OwnerId = Usr.ID;
                    cos.CreatedById=Usr.ID;
                        

                    Cos.RecordTypeId= '0120Y0000009cEz';
                    Cos.Roll_Out__c= new_roll_out.Id;
                    Cos.Service_Type__c = rw.ServiceType;
                    if(rw.ServiceType == 'IOR' ) {
                        Cos.Ship_From_Country__c = rw.ShipFrom;
                        Cos.Destination__c = destination;
                    } else{
                        Cos.Ship_From_Country__c = destination;
                        Cos.Destination__c = rw.ShipFrom;
                    }
                    Cos.Client_Reference__c = rw.Reference1;
                    Cos.Client_Reference_2__c = rw.Reference2;
                    Cos.service_Manager__C = ac.Service_Manager__c;
                    Cos.Shipping_Status__c = 'Roll-Out';
                    Cos.Finance_Team__c =ac.finance_Team__c;
                    Cos.IOR_CSE__c =ac.cse_IOR__c;
                    Cos.CDC__c =ac.CDC__c;
                    Cos.invoice_Timing__c = ac.invoice_Timing__c;
                    Cos.Lead_AM__c = ac.Lead_AM__c;
                    Cos.Financial_Controller__c=ac.Financial_Controller__c;
                    Cos.Financial_Manager__c =ac.Financial_Manager__c;
                    Cos.Operations_manager__C =ac.Operations_manager__c;
                    Cos.Freight_co_ordinator__c = ac.Logistics_Coordinator__c;
                    Cos.Compliance_Team__c = ac.Compliance__c;
                    Cos.Vat_Team__c = ac.Vat_Team__c;

                    if (!rw.ShipmentOrder_Packages.isEmpty()) {
                        Cos.Chargeable_Weight__c = CummChargableWeight;
                        Cos.Actual_Weight_KGs__c = CummActWeight;
                        Cos.of_packages__c = no_of_packages;
                    } else {
                        Cos.of_packages__c = 0;
                        Cos.Chargeable_Weight__c = decimal.valueof(rw.Chargable_Weight);
                    }
                    SOList.add(Cos);
                }
                insert SOList;

                if(!rw.ShipmentOrder_Packages.isEmpty()) {
                    List<Shipment_Order_Package__c> SOPL = new List<Shipment_Order_Package__c>();
                    for(Shipment_order__c shipment : SOList) {
                        for(Integer l=0; rw.ShipmentOrder_Packages.size()>l; l++) {
                            //Create shipmentOrder Packages
                            Shipment_Order_Package__c SOP = new Shipment_Order_Package__c(
                                packages_of_same_weight_dims__c = rw.ShipmentOrder_Packages[l].Packages_of_Same_Weight,
                                Weight_Unit__c = rw.ShipmentOrder_Packages[l].Weight_Unit,
                                Actual_Weight__c = rw.ShipmentOrder_Packages[l].Actual_Weight,
                                Dimension_Unit__c = rw.ShipmentOrder_Packages[l].Dimension_Unit,
                                Length__c = rw.ShipmentOrder_Packages[l].Length,
                                Breadth__c = rw.ShipmentOrder_Packages[l].Breadth,
                                Height__c = rw.ShipmentOrder_Packages[l].Height,
                                Created_From__c='Client Portal',
                                Shipment_Order__c = shipment.id
                                );
                            SOPL.add(SOP);
                        }
                    }
                    RecusrsionHandler.skipSOPTriggerExecutionOnSOCreation = true;
                    Insert SOPL;
                }

                List<shipment_order__c> SOs = [SELECT Account__r.Name,Ship_to_Country__r.Name,Name,NCP_Quote_Reference__c,Roll_out__C,roll_out__r.name,Shipping_Status__c,Shipment_Value_USD__c,Chargeable_Weight__c,Who_arranges_International_courier__c FROM Shipment_Order__c WHERE Roll_out__c =: new_roll_out.Id ];

                JSONGenerator gen = JSON.createGenerator(true);
                gen.writeStartObject();
                if(!SOs.isEmpty()) {
                    //Creating Array & Object - Shipment Orders
                    gen.writeFieldName('ShipmentOrders');
                    gen.writeStartArray();
                    for(Shipment_Order__c SOs1 :SOs) {
                        //Start of Object - Shipment Order
                        gen.writeStartObject();
                        if(SOs1.Id == null) {gen.writeNullField('Id');} else{gen.writeStringField('Id', SOs1.Id);}
                        if(SOs1.Name == null) {gen.writeNullField('Name');} else{gen.writeObjectField('Name', SOs1.Name);}
                        if(SOs1.Roll_Out__c == null) {gen.writeNullField('RolloutID');} else{gen.writeObjectField('RolloutID', SOs1.Roll_Out__c);}
                        if(SOs1.Roll_Out__r.Name == null) {gen.writeNullField('RolloutName');} else{gen.writeObjectField('RolloutName', SOs1.Roll_Out__r.Name);}
                        if(SOS1.Account__r.name == null) {gen.writeNullField('Accountname');} else{gen.writeObjectField('AccountName', SOS1.Account__r.name);}
                        if(SOs1.shipping_Status__c == null) {gen.writeNullField('ShippingStatus');} else{gen.writeObjectField('ShippingStatus', SOs1.Shipping_status__c);}
                        if(SOs1.Shipment_Value_USD__c == null) {gen.writeNullField('ShipmentValueUSD');} else{gen.writeNumberField('ShipmentValueUSD', SOs1.Shipment_Value_USD__c);}
                        if(SOs1.Chargeable_Weight__c == null) {gen.writeNullField('ChargableWeight');} else{gen.writeNumberField('ChargableWeight', SOs1.Chargeable_Weight__c);}
                        if(SOs1.Who_arranges_International_courier__c == null) {gen.writeNullField('CourierResponsibility');} else{gen.writeStringField('CourierResponsibility', SOs1.Who_arranges_International_courier__c);}
                        if(SOs1.NCP_Quote_Reference__c == null) {gen.writeNullField('NCPQuoteReference');} else{gen.writeStringField('NCPQuoteReference', SOs1.NCP_Quote_Reference__c);}
                        gen.writeEndObject();
                        //End of Object - Shipment Order
                    }
                    gen.writeEndArray();
                }
                gen.writeEndObject();
                String jsonData = gen.getAsString();
                res.responseBody = Blob.valueOf(jsonData);
                res.addHeader('Content-Type', 'application/json');
                res.statusCode = 200;

                API_Log__c Al = New API_Log__c();
                Al.Account__c=rw.AccountID;
                Al.Login_Contact__c=rw.contactID;
                Al.EndpointURLName__c='RollOutCreation';
                Al.Response__c='Success - RollOut Created with Shipment Order Packages';
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al;

                if(new_roll_out != null && !String.isBlank(new_roll_out.Id)) {
                    List<Id>recordsToProcess = new list<id>();

                    if(!Test.isRunningTest()) {
                        recordsToProcess.add(new_roll_out.Id);
                        Database.executeBatch(new ProcessRollOutSOBatch(recordsToProcess),1);
                    }
                }
            }     //If end
        }
        catch(exception e) {
            System.debug('Text from Wrapper Exception' +e.getMessage());
            String ErrorString ='Something went wrong, please contact support_SF@tecex.com';
            res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
            res.addHeader('Content-Type', 'application/json');
            res.statusCode = 500;
            API_Log__c Al = New API_Log__c();
            Al.Account__c=rw.AccountID;
            Al.Login_Contact__c=rw.contactID;
            Al.EndpointURLName__c='RollOutCreation';
            Al.Response__c='error while RollOut Created ===>'+e.getMessage();
            Al.StatusCode__c=string.valueof(res.statusCode);
            Insert Al;
        }
    }
}