public class FieldChangeMaps { 
	private Type fItemType;
	private list<FieldChangeMap> fItems;  
	private map<String, FieldDefinition> fieldDefs;
	public list<FieldChangeMap> items {
		get {return fItems;}  
	}

	public FieldChangeMaps(Type t) {
		fItems = new list<FieldChangeMap>();
		fieldDefs = new map<String, FieldDefinition>();
		fItemType = t;
	}

    public void clearItems() {
    	fItems.clear();
    }
	private FieldDefinition getFieldDef(SObject item, String fieldName) {
		FieldDefinition result = fieldDefs.get(fieldName);
		if (result == null) {
			result = new FieldDefinition(item.getSObjectType().getDescribe().fields.getMap().get(fieldName).getDescribe());
			fieldDefs.put(fieldName, result);
			System.Debug('Adding describe for field: ' + fieldName);
		}  
		return result;
	} 
	public FieldChangeMap addIfChanged(SObject oldItem, SObject newItem, String fieldName) {
		FieldChangeMap result = null;
		if (oldItem.get(fieldName) != newItem.get(fieldName)) {
			result = ((FieldChangeMap)fItemType.newInstance()).initialize(oldItem, newItem, getFieldDef(newItem, fieldName));
			items.add(result); 
		} 
		return result;
	} 

	public map<String, String> XMLoldValues() {
		map<String, String> result = new map<String, String>();
		for (FieldChangeMap item: items) { result.put(item.fieldName, item.XMLOldValue); }
		return result;
	} 
	public map<String, String> XMLnewValues() {
		map<String, String> result = new map<String, String>();
		for (FieldChangeMap item: items) { result.put(item.fieldName, item.XMLNewValue); }
		return result;
	}
	
	public String FieldXMLValue(SObject item, String fieldName) {
		return ((FieldChangeMap)fItemType.newInstance()).initialize(item, getFieldDef(item, fieldName)).XMLNewValue;
	}   
}