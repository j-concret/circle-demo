public class StripeCheckout {

    @future(callout = True)
    Public static  void submit(Set<Id> InvList){
        String TecexclientRTypeId = Schema.Sobjecttype.Invoice_New__c.getRecordTypeInfosByDeveloperName().get('Client').getRecordTypeId();
        String ZeeclientRTypeId = Schema.Sobjecttype.Invoice_New__c.getRecordTypeInfosByDeveloperName().get('Zee').getRecordTypeId();
        String successURL;
		String cancelURL;

        
        List<Invoice_New__c> invoiceList = [select Id,Name,recordtypeid,Invoice_Amount_Local_Currency__c,Invoice_Currency__c,Amount_Outstanding_Local_Currency__c,
                                  Stripe_Link__c,Client_Stripe_Payment_Status__c ,Stripe_ID__c ,Stripe_Paymnet_Intent__c   
                                  FROM Invoice_New__c 
                                  where Id IN:InvList  ]; 
        
	//	Map<String,StripeLineitems__c> StripeLineItems = (Map<String,StripeLineitems__c >)StripeLineitems__c.getAll();
        List<API_Log__c> ApiLogs = new List<API_Log__c>(); 
		//        String token='Bearer sk_live_NglxwAz9ApaV4YEAAWai8VG0'; //Prod
         String token='Bearer sk_test_GZXm4uDEQwqLIsDQSNPd6cft'; //Account2
       
        //String Endpoint='https://api.stripe.com/v1/checkout/sessions?success_url=https://tecex.com&cancel_url=https://tecex.com&line_items[0][price]=price_1J8PKOCmiZaJxkqvUU9K92io&mode=payment&payment_method_types[]=card&line_items[0][quantity]=20';
        for(Invoice_New__c invoice:invoiceList){
            if(invoice.RecordTypeId==TecexclientRTypeId) {
                successURL='https://tecex.com/payment-success/';
                cancelURL='https://tecex.com/payment-error/';
            }   
            else{
                successURL='https://app-staging.zee.co/payment-callback?invoiceId='+invoice.id+'%26status=SUCCESS'; //https://zee.co/payment-success
                cancelURL='https://app-staging.zee.co/payment-callback?invoiceId='+invoice.id+'%26status=CANCELED';//https://zee.co/payment-error
                
            }
            
           // StripeLineitems__c stplines = StripeLineItems.get('USD');
       // String Endpoint='https://api.stripe.com/v1/checkout/sessions?success_url='+'\''+successURL+'\''+'&cancel_url='+'\''+cancelURL+'\''+'&mode=payment&payment_method_types[]=card&line_items[0][quantity]=1&line_items[0][name]=Total+Invoice+Amount&line_items[0][currency]='+(invoice.Invoice_Currency__c).substringBetween('(', ')')+'&line_items[0][amount]='+Integer.valueOf(invoice.Amount_Outstanding_Local_Currency__c*100);
		String Endpoint='https://api.stripe.com/v1/checkout/sessions?success_url='+successURL+'&cancel_url='+cancelURL+'&mode=payment&payment_method_types[]=card&line_items[0][quantity]=1&line_items[0][name]=Total+Invoice+Amount&line_items[0][currency]='+(invoice.Invoice_Currency__c).substringBetween('(', ')')+'&line_items[0][amount]='+Integer.valueOf(invoice.Amount_Outstanding_Local_Currency__c*100);
		
        System.debug('Endpoint');        
		System.debug(Endpoint);       
        Http P=new Http();
        HttpRequest req = new HttpRequest();
        
        req.setEndpoint(endpoint); 
        req.setMethod('POST');
        req.setTimeout(120000);
        req.setHeader('Content-Type','multipart/form-data');
        req.setHeader('Authorization',token);
       
        
        
        
        HttpResponse response = P.send(req);
        system.debug('status code is >>>>'+ response.getStatusCode() + ' ' + response.getStatus());
        system.debug('status code is >>>>'+ response.getStatusCode() + ' ' + response.getStatus());
        System.debug('Body is >>>>'+response.getBody());
        
        if (response.getStatus()=='OK') {
         //  System.debug(Json.deserializeUntyped(response.getBody())); 
			Map<String,Object> responseData= (Map<String,Object>)Json.deserializeUntyped(response.getBody());
			System.debug(responseData.get('id'));
			invoice.Stripe_ID__c = (String)responseData.get('id');  
            invoice.Stripe_Paymnet_Intent__c = (String)responseData.get('payment_intent'); 
            invoice.Stripe_Link__c = (String)responseData.get('url'); 
            invoice.Client_Stripe_Payment_Status__c = (String)responseData.get('payment_status'); 
        } else {
            System.debug('Error');
            System.debug(response.getBody());
            API_Log__c apiLog = new API_Log__c (StatusCode__c = String.valueOf((Integer)response.getStatusCode()),Response__c = response.getBody(), Calling_Method_Name__c  = 'Stripe '+invoice.Id);
        	ApiLogs.add(apiLog);
             System.debug(JSON.serializePretty(ApiLogs));
        }   
        }
        
        if(invoiceList != null)
        update invoiceList;
        
        if(ApiLogs !=null && !ApiLogs.isEmpty()){
            insert ApiLogs;
            System.debug(JSON.serializePretty(ApiLogs));
        }
    }
    
    @future(callout = True)
    Public static  void CancelPaymentIntent(String PIID){
     //   String token='Bearer sk_live_NglxwAz9ApaV4YEAAWai8VG0'; //Prod
      String token='Bearer sk_test_GZXm4uDEQwqLIsDQSNPd6cft'; //Account2
      String Endpoint = 'https://api.stripe.com/v1/payment_intents/'+PIID+'/cancel';
        System.debug('Endpoint');        
		System.debug(Endpoint);       
        Http P=new Http();
        HttpRequest req = new HttpRequest();
        
        req.setEndpoint(endpoint); 
        req.setMethod('POST');
        req.setTimeout(120000);
        req.setHeader('Content-Type','multipart/form-data');
        req.setHeader('Authorization',token);
        
        
        
        HttpResponse response = P.send(req);
        
        system.debug('//-- payment intent cancel response ---//');
        system.debug('status code is >>>>'+ response.getStatusCode() + ' ' + response.getStatus());
        system.debug('status code is >>>>'+ response.getStatusCode() + ' ' + response.getStatus());
        System.debug('Body is >>>>'+response.getBody());
        
        
    }
  
}