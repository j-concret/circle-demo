@isTest
public class NCPOnboarding_Test {
    
    static testMethod void  testForNCPOnboarding_1(){
        
        Access_token__c at = new Access_token__c();
        at.Access_Token__c = 'cc407192-e2bf-4ada-9ddd-dfa2e13aba9c';
        at.Status__c='Active';
        Insert at;
        
        Account acc = new Account(Name = 'Acme Test');
        Insert acc;
        
        Contact con = new Contact(
            LastName = 'Test Contact',
            AccountId = acc.Id);
        Insert con;
        
        Lead ld = new Lead(
            Company = 'Marvel',
            LastName = 'Lang',
            status = 'New Lead');
        Insert ld;
        
        NCPOnboardingWrap wrapperObject = new NCPOnboardingWrap();
        wrapperObject.Accesstoken = at.Access_Token__c;
        wrapperObject.CreateLead = TRUE;
        wrapperObject.CreateQuote = TRUE;
        wrapperObject.CreateAccount = TRUE;
        wrapperObject.CreateCase = TRUE;
        wrapperObject.companyName = 'Marvel';
        wrapperObject.typeOfCompany = 'Reseller';
        wrapperObject.country = 'USA';
        wrapperObject.firstName = 'Scott';
        wrapperObject.lastName = 'Lang';
        wrapperObject.email = 'scottlang@gmail.com';
        wrapperObject.LeadId = ld.Id;
        wrapperObject.Role = 'Developer';
        wrapperObject.shippingFrom = 'USA';
        wrapperObject.shippingTo = 'Australia';
        wrapperObject.shipmentValue = 450;
        wrapperObject.chargeableWeight = 500;
        wrapperObject.weightUnit = 'kg';
        wrapperObject.typeOfGoods = 'blu-ray Player';
        wrapperObject.shippingNotes = 'Handle with care!';
        wrapperObject.Subject = 'Invoice';
        wrapperObject.Description = 'please make sure it reaches destination.';
        wrapperObject.AccountID = acc.Id;
        wrapperObject.ContactID = con.Id; 
        
        RestRequest req1 = new RestRequest();
        RestResponse res1 = new RestResponse();
        req1.requestURI = '/services/apexrest/NCPOnboarding/'; 
        req1.httpMethod = 'POST';
        req1.requestBody = Blob.valueOf(JSON.serialize(wrapperObject));    
        RestContext.request = req1;
        RestContext.response = res1;
        RestContext.request.addHeader('Content-Type', 'application/json');
        
        Test.startTest(); 
        	NCPOnboarding.NCPOnboarding();
        Test.stopTest();
        
    }
    
    static testMethod void  testForNCPOnboarding_2(){
        
        Access_token__c at = new Access_token__c();
        at.Access_Token__c = 'cc407192-e2bf-4ada-9ddd-dfa2e13aba9c';
        at.Status__c='Active';
        Insert at;
        
        Account acc = new Account(Name = 'Test Account');
        Insert acc;
        
        Contact con = new Contact(
            LastName = 'Test Contact',
            AccountId = acc.Id);
        Insert con;
        
        Lead ld = new Lead(
            Company = 'Marvel',
            LastName = 'Lang',
            status = 'New Lead');
        Insert ld;
        
        NCPOnboardingWrap wrapperObject = new NCPOnboardingWrap();
        wrapperObject.Accesstoken = at.Access_Token__c;
        wrapperObject.CreateLead = FALSE;
        wrapperObject.CreateQuote = FALSE;
        wrapperObject.CreateAccount = TRUE;
        wrapperObject.CreateCase = TRUE;
        wrapperObject.companyName = 'Marvel';
        wrapperObject.typeOfCompany = 'Reseller';
        wrapperObject.country = 'United States';
        wrapperObject.firstName = 'Scott';
        wrapperObject.lastName = 'Lang';
        wrapperObject.email = 'scottlang@gmail.com';
        wrapperObject.LeadId = ld.Id;
        wrapperObject.Role = 'Developer';
        wrapperObject.shippingFrom = 'USA';
        wrapperObject.shippingTo = 'Australia';
        wrapperObject.shipmentValue = 450;
        wrapperObject.chargeableWeight = 500;
        wrapperObject.weightUnit = 'kg';
        wrapperObject.typeOfGoods = 'blu-ray Player';
        wrapperObject.shippingNotes = 'Handle with care!';
        wrapperObject.Subject = 'Invoice';
        wrapperObject.Description = 'please make sure it reaches destination.';
        wrapperObject.AccountID = acc.Id;
        wrapperObject.ContactID = con.Id; 
        
        RestRequest req1 = new RestRequest();
        RestResponse res1 = new RestResponse();
        req1.requestURI = '/services/apexrest/NCPOnboarding/'; 
        req1.httpMethod = 'POST';
        req1.requestBody = Blob.valueOf(JSON.serialize(wrapperObject));    
        RestContext.request = req1;
        RestContext.response = res1;
        RestContext.request.addHeader('Content-Type', 'application/json');
        
        Test.startTest(); 
        	NCPOnboarding.NCPOnboarding();
        Test.stopTest();
        
    }
    
    static testMethod void  testForNCPOnboarding_3(){
        
        Access_token__c at = new Access_token__c();
        at.Access_Token__c = 'cc407192-e2bf-4ada-9ddd-dfa2e13aba9c';
        at.Status__c='Active';
        Insert at;
        
        Account acc = new Account(Name = 'Test Account');
        Insert acc;
        
        Contact con = new Contact(
            LastName = 'Test Contact',
            AccountId = acc.Id);
        Insert con;
        
        Lead ld = new Lead(
            Company = 'Marvel',
            LastName = 'Lang',
            status = 'New Lead');
        Insert ld;
        
        NCPOnboardingWrap wrapperObject = new NCPOnboardingWrap();
        wrapperObject.Accesstoken = at.Access_Token__c;
        wrapperObject.CreateLead = FALSE;
        wrapperObject.CreateQuote = FALSE;
        wrapperObject.CreateAccount = FALSE;
        wrapperObject.CreateCase = TRUE;
        wrapperObject.companyName = 'Marvel';
        wrapperObject.typeOfCompany = 'Reseller';
        wrapperObject.country = 'United States';
        wrapperObject.firstName = 'Scott';
        wrapperObject.lastName = 'Lang';
        wrapperObject.email = 'scottlang@gmail.com';
        wrapperObject.LeadId = ld.Id;
        wrapperObject.Role = 'Developer';
        wrapperObject.shippingFrom = 'USA';
        wrapperObject.shippingTo = 'Australia';
        wrapperObject.shipmentValue = 450;
        wrapperObject.chargeableWeight = 500;
        wrapperObject.weightUnit = 'kg';
        wrapperObject.typeOfGoods = 'blu-ray Player';
        wrapperObject.shippingNotes = 'Handle with care!';
        wrapperObject.Subject = 'Invoice';
        wrapperObject.Description = 'please make sure it reaches destination.';
        wrapperObject.AccountID = acc.Id;
        wrapperObject.ContactID = con.Id; 
        
        RestRequest req1 = new RestRequest();
        RestResponse res1 = new RestResponse();
        req1.requestURI = '/services/apexrest/NCPOnboarding/'; 
        req1.httpMethod = 'POST';
        req1.requestBody = Blob.valueOf(JSON.serialize(wrapperObject));    
        RestContext.request = req1;
        RestContext.response = res1;
        RestContext.request.addHeader('Content-Type', 'application/json');
        
        Test.startTest(); 
        	NCPOnboarding.NCPOnboarding();
        Test.stopTest();
        
    }
    
    static testMethod void  testErrorForNCPOnboarding(){
        
        Access_token__c at = new Access_token__c();
        at.Access_Token__c = 'cc407192-e2bf-4ada-9ddd-dfa2e13aba9c';
        at.Status__c='Active';
        Insert at;
        
        NCPOnboardingWrap wrapperObject = new NCPOnboardingWrap();
        wrapperObject.Accesstoken = at.Access_Token__c;
        wrapperObject.CreateLead = TRUE;
        wrapperObject.CreateQuote = FALSE;
        wrapperObject.CreateAccount = FALSE;
        wrapperObject.CreateCase = TRUE;
        
        RestRequest req1 = new RestRequest();
        RestResponse res1 = new RestResponse();
        req1.requestURI = '/services/apexrest/NCPOnboarding/'; 
        req1.httpMethod = 'POST';
        req1.requestBody = Blob.valueOf(JSON.serialize(wrapperObject));     
        RestContext.request = req1;
        RestContext.response = res1;
        RestContext.request.addHeader('Content-Type', 'application/json');
        
        Test.startTest(); 
        	NCPOnboarding.NCPOnboarding();
        Test.stopTest();
    }
}