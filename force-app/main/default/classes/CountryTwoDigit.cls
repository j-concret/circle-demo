@RestResource(urlMapping='/CountryTwodigitMap/*')
Global class CountryTwoDigit {

    @Httpget
    global static void CountryTwoDigitmap(){
     
      RestRequest req = RestContext.request;
      RestResponse res = RestContext.response;
      res.addHeader('Content-Type', 'application/json');
        
        Try{
            List<CountryofOriginMap__c> COO = [select id,name,Country__c,Two_Digit_Country_Code__c from CountryofOriginMap__c];
              
              res.responseBody = Blob.valueOf(JSON.serializePretty(COO));
              res.statusCode = 200;
                            
        }
        
        Catch(exception e){}
        
    }
    
}