public class TipaltiRemittanceEmail {
    
    public static void sendRemittanceEmail(){
        
        Double total = 0;
        
        Map<Id, List<String>> accountToInvoiceStringMap = new Map<Id, List<String>>();  
        Map<Id, Double> accountToTotalMap = new Map<Id, Double>();
        Map<Id, List<String>> accountToContactMail = new Map<Id, List<String>>();
        Map<Id, Account> accountMap;
        List<String> ccAddress;
        String htmlRow = '';
        String shipmentName = '';
        
        List <Messaging.SingleEmailMessage> mails = new List <Messaging.SingleEmailMessage>();
        List<String> invoiceHtmlRow = new List<String>();
        
        List<Invoice_New__c> invoiceList = [Select Id, Shipment_Order__c, Shipment_Order__r.Name, Amount_Paid_via_Tipalti_Local_Currency__c, Invoice_Name__c, Account__c, Name, Invoice_Currency__c, Invoice_Amount_Local_Currency__c  from Invoice_New__c Where RecordType.Name = 'Supplier' AND Invoice_Status__c = 'Paid by Tipalti' AND Paid_Date_Tipalti__c = Today order by Account__c];
        System.debug('invoiceList --> '+JSON.serialize(invoiceList));
        for(Invoice_New__c invoice : invoiceList){
            
            total = 0;
            if(invoice.Shipment_Order__c != null){
                shipmentName = invoice.Shipment_Order__r.Name;
            }
            
            if(accountToInvoiceStringMap.containsKey(invoice.Account__c)){
                
                htmlRow = '<tr><td style="padding:5px;border: 1px solid black;"">'+ shipmentName +'</td><td style="padding:5px;border: 1px solid black;"">'+invoice.Invoice_Name__c+'</td><td style="padding:5px;border: 1px solid black;"">'+invoice.Name+'</td><td style="padding:5px;border: 1px solid black;"">'+invoice.Id+'</td><td style="padding:5px;border: 1px solid black;">'+invoice.Invoice_Currency__c+'</td><td style="padding:5px;border: 1px solid black;">'+invoice.Amount_Paid_via_Tipalti_Local_Currency__c.setscale(2)+'</td></tr>';
                
                accountToInvoiceStringMap.get(invoice.Account__c).add(htmlRow);
            }
            else{
                htmlRow = '<tr><td style="padding:5px;border: 1px solid black;"">'+ shipmentName +'</td><td style="padding:5px;border: 1px solid black;"">'+invoice.Invoice_Name__c+'</td><td style="padding:5px;border: 1px solid black;"">'+invoice.Name+'</td><td style="padding:5px;border: 1px solid black;"">'+invoice.Id+'</td><td style="padding:5px;border: 1px solid black;">'+invoice.Invoice_Currency__c+'</td><td style="padding:5px;border: 1px solid black;">'+invoice.Amount_Paid_via_Tipalti_Local_Currency__c.setscale(2)+'</td></tr>';
                accountToInvoiceStringMap.put(invoice.Account__c, new List<String>{htmlRow});
            } 
            
            if(accountToTotalMap.containsKey(invoice.Account__c)){
           		total = accountToTotalMap.get(invoice.Account__c) + invoice.Amount_Paid_via_Tipalti_Local_Currency__c;
                accountToTotalMap.put(invoice.Account__c, total);
            }
            else{
                accountToTotalMap.put(invoice.Account__c, invoice.Amount_Paid_via_Tipalti_Local_Currency__c);
            }
        }
        
        for(Contact con : [Select Id, Email, AccountId from Contact Where AccountId IN :accountToInvoiceStringMap.keySet() AND Include_in_invoicing_emails__c = true AND Email != null]){
            
            if(accountToContactMail.containsKey(con.AccountId)){
                accountToContactMail.get(con.AccountId).add(con.Email);
            }
            else{
                accountToContactMail.put(con.AccountId, new List<String>{con.Email});
            }
            
        }
        
        accountMap = new Map<Id, Account>([Select Id, Name, Financial_Manager__c, Financial_Controller__c from Account Where Id IN :accountToInvoiceStringMap.keySet()]);
        
        for(Id accId : accountToInvoiceStringMap.keySet()){
            
            ccAddress = new List<String>{'finance@tecex.com'};
            
            if(accountMap.containsKey(accId) && accountMap.get(accId).Financial_Controller__c != null){
            	ccAddress.add(accountMap.get(accId).Financial_Controller__c);
            }
            if(accountMap.containsKey(accId) && accountMap.get(accId).Financial_Manager__c != null){
            	ccAddress.add(accountMap.get(accId).Financial_Manager__c);
            }
            
            string body = '<font face="Calibri">Disclaimer: Do not reply to this email as this inbox is not monitored. Please contact finance@tecex.com should you have any queries. <br/><br/>Hi '+accountMap.get(accId).Name+' Team,<br/><br/>Please see below remittance for the Tipalti payment made to your account today:';
            body += '<br/><br/><table cellspacing=0 style="font-family:Calibri;border: 1px solid black;border-collapse: collapse;"><tr><th style="padding:5px;border: 1px solid black;">Shipment Order</th><th style="padding:5px;border: 1px solid black;">Invoice Reference (Supplier)</th><th style="padding:5px;border: 1px solid black;">Invoice Reference (TecEx)</th><th style="padding:5px;border: 1px solid black;">Record ID</th><th style="padding:5px;border: 1px solid black;">Invoice Currency</th><th style="padding:5px;border: 1px solid black;">Invoice Amount (Local Currency)</th></tr>';
            body += String.join(accountToInvoiceStringMap.get(accId), '');
            body += '<tr><td style="padding:5px;border: 1px solid black;""></td><td style="padding:5px;border: 1px solid black;""></td><td style="padding:5px;border: 1px solid black;""></td><td style="padding:5px;border: 1px solid black;""></td><td style="padding:5px;border: 1px solid black;"">Total</td><td style="padding:5px;border: 1px solid black;"">'+accountToTotalMap.get(accId)+'</td></tr>';
            body += '</table><br/><br/>Note that you can review your payment history and download proof of payment (POP) for all Tipalti payments made to date on our Supplier Portal (<a href="http://tecex.force.com/SupllierPortal/s/"> http://tecex.force.com/SupllierPortal/s/ </a>). Please log in using the email address with which you registered. Navigate to "More" followed by "My payment history" to select and download whichever POP you require.<br/><br/>If you have any queries, please respond to <a href="mailto:finance@tecex.com">finance@tecex.com</a> and not to this email.<br/><br/>Have a great day further.<br/><br/>TecEx';
            
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setSubject('TecEx Tipalti Payment Remittance for '+accountMap.get(accId).Name+' - '+ system.today().format());
            mail.setToAddresses(accountToContactMail.get(accId));//new List<String>{'vishaltulsani@g.tecex.com','matthewb@tecex.com'}
            mail.setCcAddresses(ccAddress);
            mail.setSaveAsActivity(false);
            mail.setWhatId(accId);
            mail.setHtmlBody(body);
            mails.add(mail);
                        
        }
        
        Messaging.sendEmail(mails);
        
    }
    
}