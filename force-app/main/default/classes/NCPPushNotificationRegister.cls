@RestResource(urlMapping='/Pushnotificationregister/*')
Global class NCPPushNotificationRegister {
    
    @Httppost
    global static void NCPPushNotificationRegister(){
        
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        Blob body = req.requestBody;
        res.addHeader('Content-Type', 'application/json');
        String requestString = body.toString();
        NCPPushNotificationRegisterWra rw = (NCPPushNotificationRegisterWra)JSON.deserialize(requestString,NCPPushNotificationRegisterWra.class);
        
        try {
            List<Access_token__c> at = [SELECT status__c,Access_Token__c FROM Access_token__c WHERE Access_Token__c =: rw.Accesstoken and status__c = 'Active' LIMIT 1];
            if(!At.isempty()){
                contact Con = [SELECT ID,Push_Notification_Choice__c FROM Contact WHERE ID =: rw.ContactID LIMIT 1];
                Con.Push_Notification_Choice__c= True;
                
                list<APP_Tokens__c> PAL = new list<APP_Tokens__c>();    
                for(APP_Tokens__c apptkn : [select Id,Contact__c,Device_Name__c,Active__c FROM APP_Tokens__c where Contact__c =:rw.ContactID AND Device_Name__c =:rw.DeviceName AND Active__c = true]){
                    apptkn.Active__c = false;
                    PAL.add(apptkn);
                }
                
                
                APP_Tokens__c PA = new APP_Tokens__c(                      	 
                    Contact__c =rw.ContactID,
                    Device_Name__c =rw.DeviceName,
                    Active__c=TRUE,
                    Registration_Token__c =rw.Registertoken);
                PAL.add(PA);
                
                upsert PAL;
                
                List<APP_Tokens__c> PAL1=[select Id,Contact__c,Device_Name__c,Registration_Token__c,Active__C from APP_Tokens__c where Id in :PAL];
                
                Update Con;
                res.responseBody = Blob.valueOf(JSON.serializePretty(PAL1));
                res.statusCode = 200;
                API_Log__c Al = New API_Log__c();
                Al.Login_Contact__c=rw.ContactID;
                Al.EndpointURLName__c='Pushnotificationregister';
                Al.Response__c='Success - Pushnotification registered';
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al; 
            }
            else{
                String ErrorString ='INvalid access token or expired';
                res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
                res.statusCode = 400;
                API_Log__c Al = New API_Log__c();
                Al.Login_Contact__c=rw.ContactID;
                Al.EndpointURLName__c='Pushnotificationregister';
                Al.Response__c=ErrorString;
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al;
                
            }
            
        }
        Catch(Exception e){
            
            String ErrorString ='Something went wrong while creating pushnotifications, please contact support_SF@tecex.com';
            res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
            res.statusCode = 500;
            API_Log__c Al = New API_Log__c();
            Al.Login_Contact__c=rw.ContactID;
            Al.EndpointURLName__c='Pushnotificationregister';
            Al.Response__c=e.getMessage()+' at line number: '+e.getLineNumber();
            Al.StatusCode__c=string.valueof(res.statusCode);
            Insert Al;
            
            
        }
        
        
        
        
    }
}