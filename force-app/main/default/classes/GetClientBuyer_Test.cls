@isTest
public class GetClientBuyer_Test {
	
    public static testMethod void ClientBuyer_Test(){
        
        //Test Data
        Access_token__c aToken = new Access_token__c();
        aToken.Status__c = 'Active';
        aToken.Access_Token__c = '123';
        
        Insert aToken;
        
        Buyer_Accounts__c byAcc = new Buyer_Accounts__c(Name = 'Buyer test');
        
        insert  byAcc;
        
        //Json Body
        String json = '{"Accesstoken":"'+aToken.Access_Token__c+'","searchaccountName":"test"}';
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
        req.requestURI = '/services/apexrest/NCPGetClientBuyer'; 
        req.httpMethod = 'Post';
        req.addHeader('Content-Type', 'application/json');
        req.requestBody = Blob.valueof(json);
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
        GetClientBuyer.ClientBuyer();
        Test.stopTest(); 
    }
    
     public static testMethod void ClientBuyer_Test1(){
        
        //Test Data
        Access_token__c aToken = new Access_token__c();
        aToken.Status__c = 'Active';
        aToken.Access_Token__c = '123';
        
        Insert aToken;
        
        Buyer_Accounts__c byAcc = new Buyer_Accounts__c(Name = 'Buyer test');
        
        insert  byAcc;
        
        //Json Body
        String json = '{"Accesstoken":"Null","searchaccountName":"test"}';
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
        req.requestURI = '/services/apexrest/NCPGetClientBuyer'; 
        req.httpMethod = 'Post';
        req.addHeader('Content-Type', 'application/json');
        req.requestBody = Blob.valueof(json);
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
        GetClientBuyer.ClientBuyer();
        Test.stopTest(); 
    }
    
}