public with sharing class tecexCIComponent_CTRL {
    
    /*@AuraEnabled
    public static List<String> fetchConsignee(String recId){
        // '0010Y00000TS8bCQAT'
        Shipment_Order__c shipO = [SELECT Id, CPA_v2_0__r.Supplier__c, Destination__c, CPA_v2_0__c FROM Shipment_Order__c WHERE Id =: recId LIMIT 1];
        List<Client_Address__c> conList = new List<Client_Address__c>([SELECT Name, CompanyName__c  FROM Client_Address__c  WHERE  
                                                                     recordTypeId = :Schema.SObjectType.Client_Address__c.getRecordTypeInfosByName().get('Consignee Address').getRecordTypeId()]);
                                                                    // AND CPA_v2_0__c =: shipO.CPA_v2_0__c
                                                                    //AND All_Countries__c = :shipO.Destination__c
                                                                    
        List<String> conAds = new List<String>();
        if(!conList.isEmpty()){
            
            for(Client_Address__c con : conList){
                conAds.add(con.Name);

            }
        }

        return conAds;
    }*/

    @AuraEnabled
    public static String fetchServirceType(String recId){
        Shipment_Order__c soRec = [SELECT Account__r.Client_type__c FROM Shipment_Order__c WHERE Id =: recId];
        
        String clientType;
        if(soRec != null){
            clientType = soRec.Account__r.Client_type__c;
        }
        return  clientType;
    }

    /*@AuraEnabled
    public static List<String> fetchpickUpAddr(String recId){
        
        Shipment_Order__c shipO = [SELECT Id, CPA_v2_0__r.Supplier__c, Account__c, Destination__c, CPA_v2_0__c FROM Shipment_Order__c WHERE Id =: recId LIMIT 1];
        List<Client_Address__c> pickupList = [SELECT Id, Name, Client__r.Name,  Address__c, Address2__c, City__c, Contact_Phone_Number__c, Contact_Full_Name__c, Country__c, All_Countries__c, CompanyName__c,  Postal_Code__c, Province__c, Type__c, AdditionalContactNumber__c, Contact_Email__c FROM Client_Address__c WHERE RecordTypeId = :Schema.SObjectType.Client_Address__c.getRecordTypeInfosByName().get('PickUp').getRecordTypeId()  AND Client__c = :shipO.Account__c  ];

        List<String> pickAds = new List<String>();
        if(!pickupList.isEmpty()){
            
            for(Client_Address__c pick : pickupList){
                pickAds.add(pick.Name);

            }
        }

        return pickAds;

    }*/

    @AuraEnabled
    public static Boolean saveCI(Id soId, String SoNumber, String sType){

        Boolean isSaved = false; 

    try{ 
        PageReference ciPdf;

        if(sType == 'Freight forwarder'){
            ciPdf = new PageReference('/apex/tecexCIFFExcel?id='+soId);
        }
        else{
            ciPdf = new PageReference('/apex/tecexCIExcel?id='+soId);
        }
        //CI Excel
        //PageReference ciPdf = new PageReference('/apex/tecexCIExcel?id='+soId);
        ciPdf.getParameters().put('id', soId);
        ciPdf.setRedirect(true);
        Blob pdfPageBlob = (Test.isRunningTest()) ?
        Blob.valueOf(String.valueOf('This is a test page')) : ciPdf.getContent();
        
        Attachment ciAttachment  = new Attachment();
        ciAttachment.Body        = pdfPageBlob;
        ciAttachment.Name        = 'Commercial Invoice-' + SoNumber +'-'+(System.now()).date().format() + '.xls';
        ciAttachment.ParentId    = soId;
        ciAttachment.IsPrivate   = false;
        ciAttachment.Description = 'Commercial Invoice';
        insert ciAttachment;
        isSaved = true;
    }
    catch (Exception e){
        System.debug('Error while trying to insert Statement ###: ' + e.getMessage());
    }
    
    return isSaved;
    }

}