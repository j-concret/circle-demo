global class CCDFilesWrapper{
    @AuraEnabled
    public DateTime ModDateTime;
    
    @AuraEnabled
    public String FileTitle;

    @AuraEnabled
    public String FileDate;

    @AuraEnabled
    public String FileSize;

    @AuraEnabled
    Public String FileType;

    @AuraEnabled
    Public String FileId;

    @AuraEnabled
    Public String FileURL;
    
    // Constructors
    public CCDFilesWrapper(ContentVersion con){
        FileTitle = con.Title;
        FileType = con.FileExtension;
        ModDateTime = con.CreatedDate;
        FileDate = ModDateTime.format('MMM d, YYYY');
        FileURL = '/sfc/servlet.shepherd/version/download/'+con.Id;
        FileId = con.Id;
        Integer sizeTemp = 0;
        if (con.ContentSize < 1024){
            sizeTemp = con.ContentSize;
            FileSize = sizeTemp.format()+' bytes';
        }
        else if (con.ContentSize < 1024*1024){
            sizeTemp = con.ContentSize / 1024;
            if (math.mod(con.ContentSize, 1024) > 0){
                sizeTemp = sizeTemp + 1;
            }
            FileSize = sizeTemp.format()+' kB';
        }
        else
        {
            sizeTemp = con.ContentSize / (1024*1024);
            if (math.mod(con.ContentSize, (1024*1024)) > 0){
                sizeTemp = sizeTemp + 1;
            }
            FileSize = sizeTemp.format()+' MB';
        }            
    }
 
}