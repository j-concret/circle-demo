@isTest
public class NCPSendPortalTrackingLink_Test {
   
/*    @testSetup
   static void testSetup(){
        EmailTemplate etemplate = new EmailTemplate (FolderId = '00D1j000000Cuo4EAC', developerName = 'sendTrackingnumber', TemplateType= 'Text', Name = 'sendTrackingnumber', isActive = true);
		Insert etemplate;
    }
  */  
    static testMethod void  testForNCPSendPortalTrackingLink(){
        
       Id shipmentOrderRecTypeId = Schema.SObjectType.shipment_order__c.getRecordTypeInfosByName().get('Cost Estimate').getRecordTypeId();
        
        
        CpaRules__c cpaRule = new CpaRules__c();
        cpaRule.Country__c = 'Finland';
        cpaRule.Criteria__c = 'Default';
        
        Insert cpaRule;
        
        Access_token__c at = new Access_token__c();
        at.Status__c = 'Active';
        at.Access_Token__c = '123';
        
        Insert at;
        
        Account acc = new Account(Name = 'Acme Test');
        
        Insert acc;
        
       Contact con = new Contact(LastName='Test Contact1',AccountId=acc.Id, Email = 'Test@test.io');
        
      Insert con;
        
        Account acc1 = new Account(Name = 'TecEx Prospective Client');
        
        Insert acc1;
        
        IOR_Price_List__c priceList = new IOR_Price_List__c();
        priceList.Destination__c = 'Finland';
        priceList.Client_Name__c = acc.Id;
        priceList.IOR_Fee__c = 10;
        
        Insert priceList;
        
        shipment_order__c shipment = new shipment_order__c();
        shipment.RecordTypeId = shipmentOrderRecTypeId;
        shipment.Destination__c = 'Finland';
        shipment.IOR_Price_List__c = priceList.Id;
        shipment.Shipment_Value_USD__c = 600;
        shipment.Account__c = acc.Id;
        shipment.Client_Contact_for_this_Shipment__c = con.Id;
        
        Insert shipment;
        
        NCPPortalTrackingwra obj = new NCPPortalTrackingwra();
        obj.Accesstoken = at.Access_Token__c;
        obj.SOID = shipment.Id;
        obj.toemailaddress = 'Test@test.io';
        
        RestRequest req1 = new RestRequest();
        RestResponse res1 = new RestResponse();
        req1.requestURI = '/services/apexrest/PortalTrackingLink/*'; 
        req1.httpMethod = 'POST';
        req1.requestBody = Blob.valueOf(JSON.serialize(obj));     
        RestContext.request = req1;
        RestContext.response = res1;
        RestContext.request.addHeader('Content-Type', 'application/json');
        
        Test.startTest(); 
        	NCPSendPortalTrackingLink.NCPSendPortalTrackingLink();
        Test.stopTest();
    }
    
    static testMethod void  testForNCPSendPortalTrackingLink_2(){
        
       Id shipmentOrderRecTypeId = Schema.SObjectType.shipment_order__c.getRecordTypeInfosByName().get('Cost Estimate').getRecordTypeId();
        
        
        CpaRules__c cpaRule = new CpaRules__c();
        cpaRule.Country__c = 'Finland';
        cpaRule.Criteria__c = 'Default';
        
        Insert cpaRule;
        
        Access_token__c at = new Access_token__c();
        at.Status__c = 'Active';
        at.Access_Token__c = '123';
        
        Insert at;
        
        Account acc = new Account(Name = 'Acme Test');
        
        Insert acc;
        
       Contact con = new Contact(LastName='Test Contact1',AccountId=acc.Id, Email = 'Test@test.io');
        
      Insert con;
        
        Account acc1 = new Account(Name = 'TecEx Prospective Client');
        
        Insert acc1;
        
        IOR_Price_List__c priceList = new IOR_Price_List__c();
        priceList.Destination__c = 'Finland';
        priceList.Client_Name__c = acc.Id;
        priceList.IOR_Fee__c = 10;
        
        Insert priceList;
        
        shipment_order__c shipment = new shipment_order__c();
        shipment.RecordTypeId = shipmentOrderRecTypeId;
        shipment.Destination__c = 'Finland';
        shipment.IOR_Price_List__c = priceList.Id;
        shipment.Shipment_Value_USD__c = 600;
        shipment.Account__c = acc.Id;
        shipment.Client_Contact_for_this_Shipment__c = con.Id;
        
        Insert shipment;
        
        NCPPortalTrackingwra obj = new NCPPortalTrackingwra();
        obj.SOID = shipment.Id;
        obj.toemailaddress = 'Test@test.io';
        
        RestRequest req1 = new RestRequest();
        RestResponse res1 = new RestResponse();
        req1.requestURI = '/services/apexrest/PortalTrackingLink/*'; 
        req1.httpMethod = 'POST';
        req1.requestBody = Blob.valueOf(JSON.serialize(obj));     
        RestContext.request = req1;
        RestContext.response = res1;
        RestContext.request.addHeader('Content-Type', 'application/json');
        
        Test.startTest(); 
        	NCPSendPortalTrackingLink.NCPSendPortalTrackingLink();
        Test.stopTest();
    }

}