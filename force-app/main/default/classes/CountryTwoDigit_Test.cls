@isTest
public class CountryTwoDigit_Test {
     static testMethod void  setUpData(){
         
          CountryofOriginMap__c CTWO = new CountryofOriginMap__c (name='India', 
                                       Country__c = 'India',
                                       Two_Digit_Country_Code__c = 'IN'
                                      );
          insert CTWO;  
         
         RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
       
       
        req.requestURI = '/services/apexrest/CountryTwodigitMap'; 
        req.httpMethod = 'GET';
       // req.requestBody = Blob.valueOf(jsonReq);
        req.addHeader('Content-Type', 'application/json');
        
        RestContext.request = req;
        RestContext.response = res;

         Test.startTest();
         CountryTwoDigit.CountryTwoDigitmap();
         Test.stopTest();
         
     }

}