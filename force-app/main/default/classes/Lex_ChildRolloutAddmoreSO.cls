public class Lex_ChildRolloutAddmoreSO {
    
    @AuraEnabled
    public static void processRollouts(Id rolloutId){
        List<Id> recordsToProcess = new List<Id>();
        if(!Test.isRunningTest()){
            recordsToProcess.add(rolloutId);
            Database.executeBatch(new ProcessRollOutSOBatch(recordsToProcess),1);
        }
    }
    
    @AuraEnabled
    
    public static List<Shipment_order__C> CreateRollout(ID PAccID,ID PConID,String PshipFrom,List<String> Pdestination,String Pint_courier,String PService_required,Decimal Pshipment_value,String Pref_1,String Pref_2,String Pchargeable_weight_estimate,List<Shipment_Order_Package__c> Pshipment_order_package_list,String PPart_List,string PSecondhandgoods,string PLi_ion_Batteries,string PLi_ion_BatteryTypes,Decimal PDestinationlistsize,integer PNumber_of_FinalDeliveries,String PROID ){
        
        
        
        double CummActWeight=0;
        double Actweight=0;
        double CummVolWeight=0;
        double Volweight=0;
        double CummChargableWeight=0;  // Final
        double charweight=0;
        
        String ROID='asdfgh';  
        string code = String.valueof(Pdestination);
        string Destinationwithsemicol = code.replace('(["','').replace('","',';').replace('"])','').replace('(','').replace(')','');
        System.debug('Destinationwithsemicol-->'+Destinationwithsemicol);
        System.debug('PDestinationlistsize-->'+PDestinationlistsize);
        
        //Calculate Chargable weight
        If(  !Pshipment_order_package_list.isEmpty() && Pint_courier == 'TecEx'){ 
            
            for(Integer l=0; Pshipment_order_package_list.size()>l;l++) {
                
                if(Pshipment_order_package_list[l].Weight_Unit__c =='KGs') { Actweight = (Pshipment_order_package_list[l].Actual_Weight__c*Pshipment_order_package_list[l].packages_of_same_weight_dims__c); } else{Actweight=(Pshipment_order_package_list[l].Actual_Weight__c*Pshipment_order_package_list[l].packages_of_same_weight_dims__c*0.453592);}
                if(Pshipment_order_package_list[l].Dimension_Unit__C =='CMs') { Volweight = ((Pshipment_order_package_list[l].Length__C*Pshipment_order_package_list[l].Breadth__C*Pshipment_order_package_list[l].Height__C)/5000)*Pshipment_order_package_list[l].packages_of_same_weight_dims__c; } else{ Volweight = ((Pshipment_order_package_list[l].Length__C*Pshipment_order_package_list[l].Breadth__C*Pshipment_order_package_list[l].Height__C)/305.1200198)*Pshipment_order_package_list[l].packages_of_same_weight_dims__c;}
                if(Actweight >= Volweight) { charweight =Actweight ; } else{charweight =Volweight;}
                
                CummActWeight=CummActWeight+Actweight;
                CummVolWeight=CummVolWeight+Volweight;
                CummChargableWeight=CummChargableWeight+charweight;
                
            }
            
            
        }
        
        else If( !Pshipment_order_package_list.isEmpty() && Pint_courier != 'TecEx'){
            for(Integer l=0; Pshipment_order_package_list.size()>l;l++) {
                if(Pshipment_order_package_list[l].Weight_Unit__c =='KGs') { Actweight = (Pshipment_order_package_list[l].Actual_Weight__c*Pshipment_order_package_list[l].packages_of_same_weight_dims__c); } else{Actweight=(Pshipment_order_package_list[l].Actual_Weight__c*Pshipment_order_package_list[l].packages_of_same_weight_dims__c*0.453592);}
                if(Pshipment_order_package_list[l].Dimension_Unit__C =='CMs') { Volweight = ((Pshipment_order_package_list[l].Length__C*Pshipment_order_package_list[l].Breadth__C*Pshipment_order_package_list[l].Height__C)/5000)*Pshipment_order_package_list[l].packages_of_same_weight_dims__c; } else{ Volweight = ((Pshipment_order_package_list[l].Length__C*Pshipment_order_package_list[l].Breadth__C*Pshipment_order_package_list[l].Height__C)/305.1200198)*Pshipment_order_package_list[l].packages_of_same_weight_dims__c;}
                if(Actweight >= Volweight) { charweight =Actweight ; } else{charweight =Volweight;}
                
                CummActWeight=CummActWeight+Actweight;
                CummVolWeight=CummVolWeight+Volweight;
                CummChargableWeight=CummChargableWeight+charweight;
            }
        }
        
        
        
        //rollout and so creation    
        
        
        
        ROID =  PROID; 
        
        
        Account Ac =[Select ID,Lead_AM__c,Service_Manager__C,Operations_Manager__c,Compliance__c,Financial_Manager__c,Financial_Controller__c,cse_IOR__c,finance_Team__C,CDC__c,invoice_Timing__C from Account where Id=:PAccID];
        List<Shipment_Order__c> SOList = new List<Shipment_Order__c>();
        
        
        for(String destinationlist: Destinationwithsemicol.split(';')) {
            
            Shipment_Order__c Cos = new Shipment_Order__c();
            Cos.Account__c = PAccID;
            Cos.Client_Contact_for_this_Shipment__c = PConID;  
            Cos.Shipment_Value_USD__c= Pshipment_value;
            Cos.Who_arranges_International_courier__c=Pint_courier;
            
            Cos.RecordTypeId= '0120Y0000009cEz';
            Cos.Roll_Out__c= ROID; 
            Cos.Service_Type__c= PService_required;
            Cos.Client_Reference__c= Pref_1;
            Cos.Client_Reference_2__c= Pref_2;
            
            if(PService_required=='IOR' ) { Cos.Ship_From_Country__c = PshipFrom; Cos.Destination__c= destinationlist;} else{ Cos.Ship_From_Country__c = destinationlist; Cos.Destination__c=PshipFrom ;} 
            
            
            
            Cos.service_Manager__C =ac.Service_Manager__c;
            Cos.Financial_Manager__c =ac.Financial_Manager__c;
            Cos.Financial_Controller__c =ac.Financial_Controller__c;
            Cos.Finance_Team__c =ac.finance_Team__C; 
            Cos.IOR_CSE__c =ac.cse_IOR__c; 
            Cos.CDC__c =ac.CDC__c; 
            Cos.invoice_Timing__C=ac.invoice_Timing__C;
            Cos.Lead_AM__c=ac.Lead_AM__c;
            Cos.Operations_Manager__c=AC.Operations_Manager__c; // Added Anil 06222020
            Cos.Compliance_Team__c = AC.Compliance__c; //Added new 12012020
            
            Cos.Shipping_Status__c   ='Roll-Out';
            Cos.Chargeable_Weight__c  =decimal.valueof(Pchargeable_weight_estimate);
            Cos.Final_Deliveries_New__c=PNumber_of_FinalDeliveries;
            If( Pshipment_order_package_list.isEmpty()	){	Cos.Chargeable_Weight__c  =decimal.valueof(Pchargeable_weight_estimate);} else{Cos.Chargeable_Weight__c  =CummChargableWeight;}
            
            SOList.add(Cos);
        }
        
        
        Insert SoList;               
        
        
        return [select ID,name,Roll_Out__c,Account__c,Service_Type__c,Client_Reference__c,Client_Reference_2__c,Client_Contact_for_this_Shipment__c,Ship_From_Country__c,Destination__c,Shipment_Value_USD__c,Who_arranges_International_courier__c,Shipping_Status__c from Shipment_Order__c where ID in : SoList];     
        
        
        
        
        
        
    }
    
    @AuraEnabled
    
    public static string updatepackages(String PROID,List<Shipment_Order_Package__c> Pshipment_order_package_list,List<Shipment_order__c> PSOLIST ){
        
        
        try{
            list<Shipment_Order_Package__c> SOPL = new list<Shipment_Order_Package__c>();
            List<Shipment_order__c> SOList1 = [Select Id from Shipment_order__c where Roll_out__c =:PROID and id IN :PSOLIST];
            
            for(Integer i=0; i<=SOList1.size();i++) {
                if(i<SOList1.size()){
                    
                    
                    for(Integer l=0; Pshipment_order_package_list.size()>l;l++) {
                        //Create shipmentOrder Packages
                        Shipment_Order_Package__c SOP = new Shipment_Order_Package__c(
                            packages_of_same_weight_dims__c =Pshipment_order_package_list[l].packages_of_same_weight_dims__c,
                            Weight_Unit__c=Pshipment_order_package_list[l].Weight_Unit__c,
                            Actual_Weight__c=Pshipment_order_package_list[l].Actual_Weight__c,
                            Dimension_Unit__c=Pshipment_order_package_list[l].Dimension_Unit__c,
                            Length__c=Pshipment_order_package_list[l].Length__c,
                            Breadth__c=Pshipment_order_package_list[l].Breadth__c,
                            Height__c=Pshipment_order_package_list[l].Height__c,
                            Shipment_Order__c=SOList1[i].id
                            
                        );
                        SOPL.add(SOP);
                        
                    }
                    
                    
                    
                }
            }
            
            Insert SOPL;
            
            
            return 'SOP inserted Succesfully';   
        }
        catch (exception e){
            return e.getMessage();
        }
    }
    
    
    
    @AuraEnabled
    
    public static string Insertingparts(String PROID,String PPart_List,List<Shipment_order__c> PSOLIST  ){
        
        
        try{
            
            List<Shipment_order__c> SOList = [Select Id from Shipment_order__c where Roll_out__c =:PROID and id IN :PSOLIST];
            
            
            System.debug('JSONString PPart_List-->'+PPart_List);
            string code1 = PPart_List.replace('[','{ "Parts":[').replace(']','] }');
            String  JsonString = code1;
            System.debug('JSONString'+JSONString);
            CPCreatePartsWrapperInternal rw;
            If(!JSONString.contains('Chuck')){
                rw = (CPCreatePartsWrapperInternal)JSON.deserialize(JSONString,CPCreatePartsWrapperInternal.class);
                System.debug('rw.Parts-->'+rw.parts);
            }
            
            If(String.isNotBlank(PPart_List) && !JSONString.contains('Chuck') ){
                
                list<Part__c> PAL = new list<Part__c>();  
                
                
                for(Integer i=0; i<=SOList.size();i++) {
                    if(i<SOList.size()){
                        
                        for(Integer l=0; rw.Parts.size()>l;l++) {
                            Part__c PA = new Part__c(
                                
                                Name =rw.Parts[l].PartNumber,
                                Description_and_Functionality__c =rw.Parts[l].PartDescription,
                                Quantity__c =rw.Parts[l].Quantity,
                                Commercial_Value__c =rw.Parts[l].UnitPrice,
                                US_HTS_Code__c =rw.Parts[l].HSCode,
                                Country_of_Origin2__c =rw.Parts[l].CountryOfOrigin,
                                ECCN_NO__c =rw.Parts[l].ECCNNo,
                                Type_of_Goods__c=rw.Parts[l].Type_of_Goods,
                                Li_ion_Batteries__c=rw.Parts[l].Li_ion_Batteries,
                                Lithium_Battery_Types__c=rw.Parts[l].Li_ion_BatteryTypes,
                                Shipment_Order__c=SOList[i].id
                                
                            );
                            PAL.add(PA);
                        }
                    }
                }
                
                insert PAL;             
            }
            
            
            
            return 'Parts inserted Succesfully';   
        }
        catch (exception e){
            return e.getMessage();
        }
    }
    
    @AuraEnabled
    public static List <String> getDestinationValues() {
        List<String> plValues = new List<String>();
        
        Schema.SObjectType objType = Schema.getGlobalDescribe().get('Roll_Out__c');
        Schema.DescribeSObjectResult objDescribe = objType.getDescribe();
        Schema.DescribeFieldResult objFieldInfo = objDescribe.fields.getMap().get('Destinations__c').getDescribe();
        
        //Get the picklist field values.
        List<Schema.PicklistEntry> picklistvalues = objFieldInfo.getPicklistValues();
        
        //Add the picklist values to list.
        for(Schema.PicklistEntry plv: picklistvalues) {
            plValues.add(plv.getValue());
        }
        plValues.sort();
        return plValues;
    }
    
    
}