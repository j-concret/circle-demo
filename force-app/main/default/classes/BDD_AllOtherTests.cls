@isTest(SeeAllData=true) 
private class BDD_AllOtherTests {
	
	
	static testMethod void testContactChanged () {
		Test.setMock(HttpCalloutMock.class, new TX_Dragon_Mock());

		Map<Contact, Contact> changes = new Map<Contact, Contact>();
		List<Contact> items = Database.query(BDD_Utils.itemsQry('Contact', XF_Dragon_Request.ContactFieldMap));

		if (items.size() > 0) items.get(0).Last_API_Update__c = DateTime.now();
		for (Contact item : items) {
			Contact newItem = item.clone(true, false, true, true);
			newItem.Email = item.Email + ' modified by test';
			newItem.Phone = item.Phone + ' modified by test';
			changes.put(item, newItem);
		}	

		TH_Contact handler = new TH_Contact();
		
		test.startTest();
		
		handler.doAfterUpdate(changes);
		// Test update statement
		if (items.size() > 0) update items.get(0); 
		// Test exception
		for (Contact a : changes.keySet())
			changes.put(a, null);
		try { handler.doAfterUpdate(changes); } catch (Exception e) {} 
		
		test.stopTest();		
	}
	 
	static testMethod void testContactAdded() {
		Test.setMock(HttpCalloutMock.class, new TX_Dragon_Mock());
		List<Contact> items = Database.query(BDD_Utils.itemsQry('Contact', XF_Dragon_Request.ContactFieldMap));
		if (items.size() > 0) items.get(0).Last_API_Update__c = DateTime.now();  
		
		TH_Contact handler = new TH_Contact();
		
		test.startTest();
		
		handler.doAfterInsert(items);
		// Test adding a contact
		if (items.size() > 0) {
			items.get(0).ID = null;
			insert items.get(0);
		}  
		
		// Test exception handling
		items.set(0, null);
		try { handler.doAfterInsert(items); } catch (Exception e) {}
		
		test.stopTest();		
	}
	
	static testMethod void testContactDeleted() {
		Test.setMock(HttpCalloutMock.class, new TX_Dragon_Mock());

		test.startTest();
		
		delete [SELECT ID FROM Contact LIMIT 3];
		
		test.stopTest();		
	}
	
	static testMethod void testPushClientToDragonService() {
		Test.setMock(HttpCalloutMock.class, new TX_Dragon_Mock());
		test.startTest();
		TX_WebServices.pushClientToDragon(BDD_Utils.getRandomAccountID());
		test.stopTest();		
	}
	
	static testMethod void testErrorWebServerResult() {
		test.startTest();
		  
		XF_ErrorWebServerResult x = new XF_ErrorWebServerResult('<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">' +
			'<soapenv:Body>' + 
				'<soapenv:Fault>' + 
					'<faultcode>soapenv:Server.Application</faultcode>' + 
					'<faultstring>Application Error</faultstring>' + 
					'<detail>' + 
						'<error code="10060">Socket error: Could not bind the socket to the appropriate port and interface [trace 25] (Route: Dragon Server (Gilbert) - Address: rosetta:1687)</error>' +
					'</detail>' +
				'</soapenv:Fault>' +
			'</soapenv:Body>' + 
			'</soapenv:Envelope>');
		x.toJSON();		
		
		test.stopTest();		
	}

	static testMethod void testSysUtils() {
		test.startTest();
		System.assertEquals(SysUtils.empty(SysUtils.ListOp.SOME, new List<String> {'something', ''}), true);
		System.assertEquals(SysUtils.getOrElse(null, 'default'), 'default');
		test.stopTest();		
	}
	
	// Test cases that are not covered by the broad module tests
	static testMethod void testEdgeCases() {
		test.startTest();
		test.stopTest();		
	}
}