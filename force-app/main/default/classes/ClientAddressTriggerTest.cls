@isTest
public class ClientAddressTriggerTest {
    @isTest
    public static void validateAddress(){
        User user = [Select Id, Name, userName, Profile.Name, UserRole.name From user where Profile.Name = 'System Administrator' LIMIT 1];
        
        Id accManufRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Manufacturer').getRecordTypeId();
        Id accSupplierRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('IOR/EOR Supplier').getRecordTypeId();
        
        List<Account> accountToInsert = new List<Account>();
        accountToInsert.add(new Account (name='Acme1', Type ='Client',
                                         CSE_IOR__c = user.Id,
                                         Service_Manager__c = user.Id,
                                         Financial_Manager__c=user.Id,
                                         Financial_Controller__c=user.Id,
                                         Tax_recovery_client__c  = FALSE,
                                         Ship_From_Line_1__c = 'Ship_From_Line_1',
                                         Ship_From_Line_2__c = 'Ship_From_Line_2',
                                         Ship_From_Line_3__c = 'Ship_From_Line_3',
                                         Ship_From_City__c = 'Ship_From_City',
                                         Ship_From_Zip__c = 'Ship_From_Zip',
                                         Ship_From_Country__c = 'Ship_From_Country',
                                         Ship_From_Other_notes__c = 'Ship_From_Other',
                                         Ship_From_Contact_Name__c = 'Ship_From_Contact',
                                         Ship_From_Contact_Email__c = 'Ship_From_Email',
                                         Ship_From_Contact_Tel__c = 'Ship_From_Tel',
                                         Client_Address_Line_1__c = 'Client_Address_Line_1',
                                         Client_Address_Line_2__c = 'Client_Address_Line_2',
                                         Client_Address_Line_3__c = 'Client_Address_Line_3',
                                         Client_City__c = 'Client_City',
                                         Client_Zip__c = 'Client_Zip',
                                         Client_Country__c = 'Client_Country',
                                         Other_notes__c = 'Other_notes',
                                         Tax_Name__c = 'Tax_Name',
                                         Tax_ID__c = 'Tax_ID',
                                         Contact_Name__c = 'Contact_Name',
                                         Contact_Email__c = 'Contact_Email',
                                         Contact_Tel__c = 'Contact_Tel'
                                        ));
        
        
        
        accountToInsert.add(new Account (name='TecEx Prospective Client', Type ='Supplier', ICE__c = user.Id, RecordTypeId = accSupplierRecordTypeId));
        
        insert accountToInsert;
        
        Id PickuprecordTypeId = Schema.SObjectType.Client_Address__c.getRecordTypeInfosByDeveloperName().get('PickUp').getRecordTypeId();
        
        Client_Address__c Pickadd = new Client_Address__c(
            
            recordtypeID=PickuprecordTypeId,
            Name = 'Test',
            Contact_Full_Name__c= 'TEstName',
            Contact_Email__c ='TEst@test.com',
            Contact_Phone_Number__c = '0722854399',
            CompanyName__c= 'CompanyName',
            Address__c='Adddress',
            City__c='City',
            Province__c='Province',
            Postal_Code__c='1235',
            All_Countries__c='Algeria',
            Client__c=accountToInsert[1].Id,
            Naming_Conventions__c = 'Use CPA Details as per the below'
        );
        
        insert Pickadd; 
        
       
    }
    
    @isTest
    public static void inValidateAddress(){
        User user = [Select Id, Name, userName, Profile.Name, UserRole.name From user where Profile.Name = 'System Administrator' LIMIT 1];
        
        Id accManufRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Manufacturer').getRecordTypeId();
        Id accSupplierRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('IOR/EOR Supplier').getRecordTypeId();
        
        List<Account> accountToInsert = new List<Account>();
        accountToInsert.add(new Account (name='Acme1', Type ='Client',
                                         CSE_IOR__c = user.Id,
                                         Service_Manager__c = user.Id,
                                         Financial_Manager__c=user.Id,
                                         Financial_Controller__c=user.Id,
                                         Tax_recovery_client__c  = FALSE,
                                         Ship_From_Line_1__c = 'Ship_From_Line_1',
                                         Ship_From_Line_2__c = 'Ship_From_Line_2',
                                         Ship_From_Line_3__c = 'Ship_From_Line_3',
                                         Ship_From_City__c = 'Ship_From_City',
                                         Ship_From_Zip__c = 'Ship_From_Zip',
                                         Ship_From_Country__c = 'Ship_From_Country',
                                         Ship_From_Other_notes__c = 'Ship_From_Other',
                                         Ship_From_Contact_Name__c = 'Ship_From_Contact',
                                         Ship_From_Contact_Email__c = 'Ship_From_Email',
                                         Ship_From_Contact_Tel__c = 'Ship_From_Tel',
                                         Client_Address_Line_1__c = 'Client_Address_Line_1',
                                         Client_Address_Line_2__c = 'Client_Address_Line_2',
                                         Client_Address_Line_3__c = 'Client_Address_Line_3',
                                         Client_City__c = 'Client_City',
                                         Client_Zip__c = 'Client_Zip',
                                         Client_Country__c = 'Client_Country',
                                         Other_notes__c = 'Other_notes',
                                         Tax_Name__c = 'Tax_Name',
                                         Tax_ID__c = 'Tax_ID',
                                         Contact_Name__c = 'Contact_Name',
                                         Contact_Email__c = 'Contact_Email',
                                         Contact_Tel__c = 'Contact_Tel'
                                        ));
        
        
        
        accountToInsert.add(new Account (name='TecEx Prospective Client', Type ='Supplier', ICE__c = user.Id, RecordTypeId = accSupplierRecordTypeId));
        
        insert accountToInsert;
        
        Id PickuprecordTypeId = Schema.SObjectType.Client_Address__c.getRecordTypeInfosByDeveloperName().get('PickUp').getRecordTypeId();
        
        Client_Address__c Pickadd = new Client_Address__c(
            
            recordtypeID=PickuprecordTypeId,
            Name = 'Test',
            Contact_Full_Name__c= 'TEstNametestestesttestestetstestestesttttt',
            Contact_Email__c ='TEsttest.com',
            Contact_Phone_Number__c = 'ext0722854399',
            CompanyName__c= '',
            Address__c='Adddres test test test test test test test test test test ',
            City__c=null,
            Province__c=null,
            Postal_Code__c='1235',
            All_Countries__c='Algeria',
            Client__c=accountToInsert[1].Id,
            Naming_Conventions__c = 'Use CPA Details as per the below'
        );
        
  
        try{
            ClientAddressTriggerHandler.validateAddress(new List<Client_Address__c>{Pickadd});
        }catch(Exception e){
            system.debug('error ' + e.getMessage()); 
        }

    }
}