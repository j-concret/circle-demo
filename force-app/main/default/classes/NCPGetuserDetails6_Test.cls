@IsTest
public class NCPGetuserDetails6_Test {

    @testSetup
    static void setup() {
       
        
       UserRole r = new UserRole(DeveloperName = 'MyCustomRole', Name = 'My Role');
        insert r;
        
        User usr = new User(
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id,
            LastName = 'Berkeley',
            Email = 'testingUser@asdf.com',
            Username = 'testingUser@asdf.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            FirstName='Vat',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            UserRoleId = r.Id
        );
        insert usr;
                
        
        System.runAs(usr) { 
		 Account acc = new Account(name='TecEx Prospective Client');
        insert acc;
        
        Contact contact = new Contact ( Client_Notifications_Choice__c='Opt-In', lastname ='Testing Individual',  AccountId = acc.Id); 
        insert contact;  
        
        Profile profileId = [SELECT Id FROM Profile WHERE Name = 'Tecex Customer' LIMIT 1];
        
        User usr1 = new User(LastName = 'User',
                            FirstName='Testing',
                            Alias = 'tuser',
                            Email = 'testingUser1@asdf.com',
                            Username = 'testingUser@asdf.com',
                            ContactId = contact.Id,
                            ProfileId = profileId.Id,
                            TimeZoneSidKey = 'GMT',
                            LanguageLocaleKey = 'en_US',
                            EmailEncodingKey = 'UTF-8',
                            LocaleSidKey = 'en_US'
                           );
        insert usr1;
        }
    }
    
    
    static testmethod void testGetMethod(){
       User usr = [SELECT Id FROM User where Email ='testingUser@asdf.com' LIMIT 1];
         System.runAs(usr) {
        User usr1 = [SELECT Id FROM User where Email ='testingUser1@asdf.com' LIMIT 1];
        
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();             
        req.requestURI = '/services/apexrest/NCPGetuserDetails/';
        req.httpMethod = 'GET';
        req.addParameter('ClientuserID', usr1.Id);
        RestContext.request = req;
        RestContext.response= res;
        Test.startTest();
        NCPGetuserDetails6.NCPGetuserDetails6();
        Test.stopTest();
         }
    }

 static testmethod void testGetMethod1(){
        User usr = [SELECT Id FROM User where Email ='testingUser@asdf.com' LIMIT 1];
        
     	System.runAs(usr) {
       User usr1 = [SELECT Id FROM User where Email ='testingUser1@asdf.com' LIMIT 1];
        Account acc = new Account(name='TecEx Prospective Client');
        insert acc;
        Contact contact = new Contact ( Client_Notifications_Choice__c='Opt-In', lastname ='Testing Individual',  AccountId = acc.Id); 
        insert contact; 
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();             
        req.requestURI = '/services/apexrest/NCPGetuserDetails/';
        req.httpMethod = 'GET';
        req.addParameter('ClientuserID', '123');
        RestContext.request = req;
        RestContext.response= res;
        Test.startTest();
        NCPGetuserDetails6.NCPGetuserDetails6();
        Test.stopTest();
         }
    }    
}