Global class TecexWebserviceAura {
    
    
    @AuraEnabled
     webservice static String sendEmailSummarySupplier(List<Shipment_Order__c> SOList,String SupplierId, String newEmailAddress){  
      Shipping_Order_Email_Summary_Supplier.sendEmailSummary(null,SupplierId,newEmailAddress);
      return 'Email Sent';
          
    }
    
    @AuraEnabled
    webservice static String sendEmailSummaryClient(List<Shipment_Order__c> SOList, String ClientId, String newEmailAddress){  
      Shipping_Order_Email_Summary.sendEmailSummary(null,ClientId,newEmailAddress);
      return 'Email Sent';
    }
    

}