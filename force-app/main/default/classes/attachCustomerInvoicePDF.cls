public class attachCustomerInvoicePDF {


   public Customer_Invoice__c customerInvoice {get;set;}
   public Shipment_Order__c shipmentOrder {get;set;}
   public Account account {get;set;}
  
    
    //constructor
    
    public attachCustomerInvoicePDF(ApexPages.StandardController controller) {
        this.customerInvoice = [ Select Id, Name, Admin_Fees__c, Amount_Outstanding__c, Bank_Account__c, Bank_fees__c, Cash_Amount_Receipted__c, 
                                 Client__c, CSE_IOR_on_Shipment__c, Customer_Bank_Fee__c, Customs_Brokerage_Fees__c, Customs_Clearance_Fees__c, 
                                 Customs_Handling_Fees__c, Due_Date__c, Insurance_Fee__c, International_Freight_Fee__c, Invoice_Amount__c, Invoice_Notes__c, 
                                 Invoice_Status__c, Invoice_Type__c, IOR_Fees__c, Licence_Inspection__c, OwnerIdOld__c, Receipt_Date__c, sfoldid__c, Shipment_Order__c, 
                                 Shipping_Co_ordinator__c, Taxes_and_Duties__c, Total_amount_received__c, Total_Invoice_Amount_Formula__c, Billing_Contact__c, Miscellaneous_Fee__c,
                                 Miscellaneous_Fee_Name__c, Finance_fee__c,Finance_fee_Formula__c,Client_PO_ReferenceNumber__c,EOR_Fees__c,Recharge_Tax_and_Duty_Other__c FROM Customer_Invoice__c WHERE Id = :ApexPages.currentPage().getParameters().get('id') limit 1];

        this.shipmentOrder =  [ Select Id, Name, Client_Reference__c, Client_Reference_2__c, IOR_Price_List__r.Name, Ship_From_Country__c, Shipment_Value_USD__c, Ship_to_Country_new__c 
                                FROM Shipment_Order__c where Id = :customerInvoice.Shipment_Order__c];
        
        this.account = [Select Id, Name, BillingAddress, IOR_Payment_Terms__c, VAT_Number__c, BillingStreet, BillingCity, BillingState, BillingPostalCode, BillingCountry  from Account where Id = :customerInvoice.Client__c];
    
    }
    
    

    public PageReference attachPDF() {

    PageReference pdf = new PageReference('/apex/customerInvoicePDF2?id='+ApexPages.currentPage().getParameters().get('id'));

    // create the new attachment
    Attachment attach = new Attachment();

    // the contents of the attachment from the pdf
    Blob body;

    try {

        // returns the output of the page as a PDF
        body = pdf.getContentAsPDF();

        // need to pass unit test -- current bug    
        } catch (VisualforceException e) {
            system.debug('in the catch block');
             body = Blob.valueOf('Some Text');
        }

    attach.Body = body;
    // add the user entered name
    
    If( customerInvoice.Invoice_Type__c == 'Top-up Invoice')
    { attach.Name = shipmentOrder.Name+ 'b'+'.pdf';
    
    } Else {
    
    attach.Name = shipmentOrder.Name+'.pdf';
    
    }
    
    attach.IsPrivate = false;
    // attach the pdf to the account
    attach.ParentId = ApexPages.currentPage().getParameters().get('id');
    insert attach;
    

// Attachment attach2 = new Attachment();

// Blob body2;

   //  try {

        // returns the output of the page as a PDF
       //  body2 = pdf.getContentAsPDF();

        // need to pass unit test -- current bug    
     //    } catch (VisualforceException e) {
           //  system.debug('in the catch block');
          //    body = Blob.valueOf('Some Text');
    //     }

   //  attach2.Body = body2;
    // add the user entered name
    
   //  If( customerInvoice.Invoice_Type__c == 'Top-up Invoice')
  //   { attach2.Name = shipmentOrder.Name+ 'b'+'.pdf';
    
  //   } Else {
    
  //   attach2.Name = shipmentOrder.Name+'.pdf';
    
  //   }
 //    attach2.IsPrivate = false;
    // attach the pdf to the account
 //    attach2.ParentId = shipmentOrder.Id;
 //    If(!Test.isRunningTest()){
 //    insert attach2;
  //   }

    
   PageReference returnToPage= new PageReference('/'+ApexPages.currentPage().getParameters().get('id'));
    returnToPage.setRedirect(true);
     return returnToPage;
    
    }
}