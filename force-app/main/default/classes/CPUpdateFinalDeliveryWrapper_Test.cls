@IsTest
public class CPUpdateFinalDeliveryWrapper_Test {
    static testMethod void testParse() {
		String json = '{  '+
		'   \"ID\":\"a1o1l000000Dn2G\",'+
		'   \"Name\":\"Test Name\",'+
		'   \"ContactName\":\"\",'+
		'   \"ContactEmail\":\"\",'+
		'   \"ContactNumber\":\"2356586\",'+
		'   \"ContactAddreaaLine1\":\"Line1\",'+
		'   \"ContactAddreaaLine2\":\"Line2\",'+
		'   \"City\":\"City\",'+
		'   \"ZIP\":\"1235\",'+
		'   \"Country\":\"South Africa\"'+
		'}';
		CPUpdateFinalDeliveryWrapper obj = CPUpdateFinalDeliveryWrapper.parse(json);
		System.assert(obj != null);
	}

}