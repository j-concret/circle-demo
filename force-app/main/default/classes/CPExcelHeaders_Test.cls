@isTest
public class CPExcelHeaders_Test {
    static testmethod void setupdataException(){
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
       
       
        req.requestURI = '/services/apexrest/Excelheaders'; 
        req.httpMethod = 'GET';
        req.addHeader('Content-Type', 'application/json');
        
        RestContext.request = req;
        RestContext.response = res;
        Test.startTest();
        
       CPExcelHeaders.ExcelHeaders();
        
        Test.stopTest();
    }   

}