public class DAL_Lead 
{
	public static final String FIELD_OWNERID = Lead.OwnerId.getDescribe().getLocalName();	    
    public static final String ERROR_LEAD_ALLOCATION = 'Unable to allocate lead as it exceeds the user set limit of {0} leads.';
    public static final String EMIL_TEMPLATE_PLACEHOLDER = '{Number}';
    //If changing the constant here, update the count alias on line 48 of this class as well
    public static final String AGGREGATE_NUMBER_LEADS = 'num';
    
    public static final String LEAD_POOL_NAME = 'Lead Pool';

    private static final String LEAD_RECORD_NAME = 'VATit Sales';
    private static final String LEAD_DEF_BRANCH = 'Belgium';
    private static final String LEAD_STATUS = 'Contract Pending';  
    private static final String LEAD_RATING = 'Multinational';
    private static final String LEAD_CONTACT_ROLE = 'Executive';
    private static final String LEAD_NEW_PRODUCT = 'Import VAT';
    private static final String LEAD_PRODUCT1 = 'Not Applicable';  
    private static final String LEAD_EXPENSE_SYSTEM = 'SAP';
    private static final String LEAD_INDUSTRY = 'Energy';
    private static final String LEAD_ENTITY_TYPE = 'Registered Entity'; 
    private static final String FAKE_VAT_NUMBER = '776847965865';
    private static final String FAKE_EMAIL ='fake@email.com';
    private static final String FAKE_PHONE = '415.555.1212'; 


    private static final Decimal LEAD_FULL_RETRIEVAL_ORIGINAL_FEE = 0.04;
    private static final Decimal LEAD_SELF_RETRIEVAL_FEE = 0.08;
    private static final Decimal LEAD_REISSUE_FEE = 0.09;
    private static final Decimal LEAD_RESTYLE_FEE = 0.06;
    private static final Decimal VAT_ESTIMATE = 35.00;
    private static final Integer DEFAULT_NAME_LEN = 10;

    private static Map<String,Schema.RecordTypeInfo> RECORD_TYPES = buildRecordTypeMap();

    private static Map<String,Schema.RecordTypeInfo> buildRecordTypeMap()
    {
        Schema.DescribeSObjectResult cfrSchema = Schema.SObjectType.Lead; 
        return cfrSchema.getRecordTypeInfosByName();
    }

	public static List<AggregateResult> leadCountsPerUser (Map<Id,Integer> users, Integer upperLimit, Decimal factorOfLimit)
	{
       	List<AggregateResult> leadCountsPerUserFinal =  new List<AggregateResult>();
		Integer topCount = Integer.valueOf(Decimal.valueOf(upperLimit) * factorOfLimit);
		List<AggregateResult> leadCountsPerUserBase = [SELECT Lead.OwnerID OwnerId, COUNT(Id) num 
                FROM Lead 
                WHERE Lead.OwnerID IN :users.keyset()
                AND IsConverted = false
                GROUP BY Lead.OwnerId
                HAVING Count(Id) > :topCount];
        
        for(AggregateResult currentCount : leadCountsPerUserBase){
            integer currentOwnedLeads = (Integer)currentCount.get('num');
            Id currentOwnerId = (Id)currentCount.get('OwnerId');
            integer userSpecificLimit = users.get(currentOwnerId);
            if(currentOwnedLeads > (userSpecificLimit * factorOfLimit)){
                leadCountsPerUserFinal.add(currentCount);
            }
        }
        
        //If changing the count alias here, update the constant on line 6 of this class as well
		return leadCountsPerUserFinal;
            /*[
                SELECT Lead.OwnerID OwnerId, COUNT(Id) num 
                FROM Lead 
                WHERE Lead.OwnerID IN :users.keyset()
                AND IsConverted = false
                GROUP BY Lead.OwnerId
                HAVING Count(Id) > :topCount
            ];*/
	}

	//Creates a new Lead. Fields populated are mostly required for Lead conversion
    public static Lead newLead(User usr)
    {         
        Lead lead = new Lead(recordtypeid = RECORD_TYPES.get(LEAD_RECORD_NAME).getRecordTypeId());
        lead.Company = al.RandomStringUtils.randomAlphabetic(DEFAULT_NAME_LEN);
        lead.LastName = al.RandomStringUtils.randomAlphabetic(DEFAULT_NAME_LEN); 
        lead.Estimated_VAT__c = VAT_ESTIMATE;
        lead.Branch_PL__c = LEAD_DEF_BRANCH;
        lead.Branch__c = LEAD_DEF_BRANCH;
        lead.Status = LEAD_STATUS;
        lead.Email = FAKE_EMAIL;
        lead.Phone = FAKE_PHONE;
        lead.Rating = LEAD_RATING;
        lead.Contact_Role__c = LEAD_CONTACT_ROLE;
        lead.Product_New__c = LEAD_NEW_PRODUCT;
        lead.Product1__c = LEAD_PRODUCT1;
        lead.Full_Retrieval_Original_Fee__c = LEAD_FULL_RETRIEVAL_ORIGINAL_FEE;
        lead.Self_Retrieval_Fee__c = LEAD_SELF_RETRIEVAL_FEE;
        lead.Re_issue_Re_Write_Fee__c = LEAD_REISSUE_FEE;
        lead.Restyle_Fee__c = LEAD_RESTYLE_FEE;
        lead.Rates_Approved__c = true;
        lead.Sales_Executive__c = usr.Id;
        lead.CSE_New__c = usr.Id;
        lead.VAT_Number__c = FAKE_VAT_NUMBER;
        lead.Travel_Expense_Management_System_Used__c = LEAD_EXPENSE_SYSTEM;
        lead.Industry = LEAD_INDUSTRY;
        lead.Date_Signed__c = Date.today();
        lead.Contract_Expiration_Date__c = Date.today().addDays(al.RandomUtils.nextInteger(1));
        lead.Entity_Type__c = LEAD_ENTITY_TYPE;
        lead.Country = al.RandomStringUtils.randomAlphabetic(DEFAULT_NAME_LEN);   
        lead.City = al.RandomStringUtils.randomAlphabetic(DEFAULT_NAME_LEN);
        lead.Street = al.RandomStringUtils.randomAlphabetic(DEFAULT_NAME_LEN);
         
        return lead;
    }

    //Change the owner of the passed in List of Leads to the passed in user
    public static void changeOwnership(List<Lead> queueLeads, Id ownerId)
    {
        for(Lead lld: queueLeads) 
            lld.OwnerId = ownerId;
    }

    public static List<Lead> createLeads(Integer leadsToCreate)
    {
    	User cse = DAL_User.getRandomConsultantUser();
    	List<Lead> leads = new List<Lead>();

        for(Integer i = 0; i < leadsToCreate; i++) 
        {
            Lead lead1 = DAL_Lead.newLead(cse);
            leads.add(lead1);
        }

        return leads;
    }

    public static LeadStatus getConvertedStatus()
    {
        return 
        [
            SELECT Id, MasterLabel 
            FROM LeadStatus 
            WHERE IsConverted = true 
            LIMIT 1
        ];
    } 
    
    //Create 10 new Leads, assign them to a User and convert them to Clients (Accounts)
    public static void addConvertedLeadsToUser(User user)
    {
        List<Database.LeadConvert> leadConverts = new List<Database.LeadConvert>();

        LeadStatus convertStatus = DAL_Lead.getConvertedStatus();        
        List<Lead> converted = DAL_Lead.createLeads(10);
        DAL_Lead.changeOwnership(converted, user.Id);

        insert converted;

        for(Lead lead: converted) 
        {
            Database.LeadConvert leadConvert = new database.LeadConvert();
            leadConvert.setLeadId(lead.id);
            leadConvert.setConvertedStatus(convertStatus.MasterLabel);
            leadConvert.setDoNotCreateOpportunity(True);
            
            leadConverts.add(leadConvert);
        }

        List<Database.LeadConvertResult> leadConversionResults = Database.convertLead(leadConverts);
    }
}