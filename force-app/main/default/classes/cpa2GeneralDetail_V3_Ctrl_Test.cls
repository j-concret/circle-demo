@IsTest
public class cpa2GeneralDetail_V3_Ctrl_Test {
    
    
    @IsTest
    public static void getCPARecordTypeName_Test(){
        
        User userObj = [Select Id, Name, userName, Profile.Name, UserRole.name From user where Profile.Name = 'System Administrator' LIMIT 1];
        
        List<Account> accList = new List<Account>();
        
        accList.add(new Account (name='Acme1', Type ='Client',
                                       CSE_IOR__c = userObj.Id,
                                       Service_Manager__c = userObj.Id,
                                       Financial_Manager__c=userObj.Id,
                                       Financial_Controller__c= userObj.Id,
                                       Tax_recovery_client__c  = FALSE,
                                       Ship_From_Line_1__c = 'Ship_From_Line_1',
                                       Ship_From_Line_2__c = 'Ship_From_Line_2',
                                       Ship_From_Line_3__c = 'Ship_From_Line_3',
                                       Ship_From_City__c = 'Ship_From_City',
                                       Ship_From_Zip__c = 'Ship_From_Zip',
                                       Ship_From_Country__c = 'Ship_From_Country',
                                       Ship_From_Other_notes__c = 'Ship_From_Other',
                                       Ship_From_Contact_Name__c = 'Ship_From_Contact',
                                       Ship_From_Contact_Email__c = 'Ship_From_Email',
                                       Ship_From_Contact_Tel__c = 'Ship_From_Tel',
                                       Client_Address_Line_1__c = 'Client_Address_Line_1',
                                       Client_Address_Line_2__c = 'Client_Address_Line_2',
                                       Client_Address_Line_3__c = 'Client_Address_Line_3',
                                       Client_City__c = 'Client_City',
                                       Client_Zip__c = 'Client_Zip',
                                       Client_Country__c = 'Algeria',
                                       Other_notes__c = 'Other_notes',
                                       Tax_Name__c = 'Tax_Name',
                                       Tax_ID__c = 'Tax_ID',
                                       Contact_Name__c = 'Contact_Name',
                                       Contact_Email__c = 'Contact_Email',
                                       Contact_Tel__c = 'Contact_Tel',
                                       NL_Product_User__c='Yes'
                                       ));

        accList.add(new Account (name='Orange Business Services (USA)', Type ='Client',
                                        CSE_IOR__c = userObj.Id,
                                        Service_Manager__c = userObj.Id,
                                        Financial_Manager__c=userObj.Id,
                                        Financial_Controller__c= userObj.Id,
                                        Tax_recovery_client__c  = FALSE,
                                        Ship_From_Line_1__c = 'Ship_From_Line_1',
                                        Ship_From_Line_2__c = 'Ship_From_Line_2',
                                        Ship_From_Line_3__c = 'Ship_From_Line_3',
                                        Ship_From_City__c = 'Ship_From_City',
                                        Ship_From_Zip__c = 'Ship_From_Zip',
                                        Ship_From_Country__c = 'Ship_From_Country',
                                        Ship_From_Other_notes__c = 'Ship_From_Other',
                                        Ship_From_Contact_Name__c = 'Ship_From_Contact',
                                        Ship_From_Contact_Email__c = 'Ship_From_Email',
                                        Ship_From_Contact_Tel__c = 'Ship_From_Tel',
                                        Client_Address_Line_1__c = 'Client_Address_Line_1',
                                        Client_Address_Line_2__c = 'Client_Address_Line_2',
                                        Client_Address_Line_3__c = 'Client_Address_Line_3',
                                        Client_City__c = 'Client_City',
                                        Client_Zip__c = 'Client_Zip',
                                        Client_Country__c = 'Algeria',
                                        Other_notes__c = 'Other_notes',
                                        Tax_Name__c = 'Tax_Name',
                                        Tax_ID__c = 'Tax_ID',
                                        Contact_Name__c = 'Contact_Name',
                                        Contact_Email__c = 'Contact_Email',
                                        Contact_Tel__c = 'Contact_Tel',
                                        NL_Product_User__c='No'
                                        ));

        accList.add(new Account (name='TecEx Prospective Client', NL_Product_User__c ='Yes',Type ='Supplier', ICE__c = userObj.Id, RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('IOR/EOR Supplier').getRecordTypeId()));
        insert accList;
        
        Country__c country = new Country__c(Brokerage_Costs__c = 618, CIF_Adjustment_Factor__c = 0.035, CIF_Absolute_value_adjustment__c = 200, Clearance_Costs__c = 488,
                                            Country__c = 'Nigeria', Name = 'Nigeria', Handling_Costs__c = 1180, License_Permit_Costs__c = 0);
        insert country;
        
        CPA_v2_0__c relatedCPA = new CPA_v2_0__c(Name = 'Nigeria', Country_Applied_Costings__c=true );
        insert relatedCPA;

        CPA_v2_0__c cpav2 = new CPA_v2_0__c(Name = 'Nigeria Testing 2.0', Supplier__c = accList[2].Id, Service_Type__c = 'IOR/EOR', Related_Costing_CPA__c = relatedCPA.Id, Preferred_Supplier__c = TRUE,
                                            VAT_Rate__c = 0.1, Destination__c = 'Nigeria',

                                            Default_Section_1_Address_Line_1__c= 'Address_Line_1',
                                            Default_Section_1_Address_Line_2__c= 'Address_Line_2',
                                            Default_Section_1_Address_Line_3__c= 'Address_Line_3',
                                            Default_Section_1_City__c= 'City',
                                            Default_Section_1_Zip__c= 'Zip',
                                            Default_Section_1_Country__c = 'Country',
                                            Default_Section_1_Other__c= 'Other',
                                            Default_Section_1_Tax_Name__c= 'Tax_Name',
                                            Default_Section_1_Tax_Id__c= 'Tax_Id',
                                            Default_Section_1_Contact_Name__c= 'Contact_Name',
                                            Default_Section_1_Contact_Email__c= 'Contact_Email',
                                            Default_Section_1_Contact_Tel__c= 'Contact_Tel',
                                            Alternate_Section_1_Address_Line_1__c= 'Address_Line_1',
                                            Alternate_Section_1_Address_Line_2__c= 'Address_Line_2',
                                            Alternate_Section_1_Address_Line_3__c= 'Address_Line_3',
                                            Alternate_Section_1_City__c= 'City',
                                            Alternate_Section_1_Zip__c= 'Zip',
                                            Alternate_Section_1_Country__c = 'Country',
                                            Alternate_Section_1_Other__c = 'Other',
                                            Alternate_Section_1_Tax_Name__c = 'Tax_Name',
                                            Alternate_Section_1_Tax_Id__c = 'Tax_Id',
                                            Alternate_Section_1_Contact_Name__c= 'Contact_Name',
                                            Alternate_Section_1_Contact_Email__c= 'Contact_Email',
                                            Alternate_Section_1_Contact_Tel__c= 'Contact_Tel',
                                            Default_Section_2_Address_Line_1__c= 'Address_Line_1',
                                            Default_Section_2_Address_Line_2__c= 'Address_Line_2',
                                            Default_Section_2_Address_Line_3__c= 'Address_Line_3',
                                            Default_Section_2_City__c= 'City',
                                            Default_Section_2_Zip__c= 'Zip',
                                            Default_Section_2_Country__c = 'Country',
                                            Default_Section_2_Other__c= 'Other',
                                            Default_Section_2_Tax_Name__c= 'Tax_Name',
                                            Default_Section_2_Tax_Id__c= 'Tax_Id',
                                            Default_Section_2_Contact_Name__c= 'Contact_Name',
                                            Default_Section_2_Contact_Email__c= 'Contact_Email',
                                            Default_Section_2_Contact_Tel__c= 'Contact_Tel',
                                            Alternate_Section_2_Address_Line_1__c= 'Address_Line_1',
                                            Alternate_Section_2_Address_Line_2__c= 'Address_Line_2',
                                            Alternate_Section_2_Address_Line_3__c= 'Address_Line_3',
                                            Alternate_Section_2_City__c= 'City',
                                            Alternate_Section_2_Zip__c= 'Zip',
                                            Alternate_Section_2_Country__c = 'Country',
                                            Alternate_Section_2_Other__c = 'Other',
                                            Alternate_Section_2_Tax_Name__c = 'Tax_Name',
                                            Alternate_Section_2_Tax_Id__c = 'Tax_Id',
                                            Alternate_Section_2_Contact_Name__c= 'Contact_Name',
                                            Alternate_Section_2_Contact_Email__c= 'Contact_Email',
                                            Alternate_Section_2_Contact_Tel__c= 'Contact_Tel',
                                            Default_Section_3_Address_Line_1__c= 'Address_Line_1',
                                            Default_Section_3_Address_Line_2__c= 'Address_Line_2',
                                            Default_Section_3_Address_Line_3__c= 'Address_Line_3',
                                            Default_Section_3_City__c= 'City',
                                            Default_Section_3_Zip__c= 'Zip',
                                            Default_Section_3_Country__c = 'Country',
                                            Default_Section_3_Other__c= 'Other',
                                            Default_Section_3_Tax_Name__c= 'Tax_Name',
                                            Default_Section_3_Tax_Id__c= 'Tax_Id',
                                            Default_Section_3_Contact_Name__c= 'Contact_Name',
                                            Default_Section_3_Contact_Email__c= 'Contact_Email',
                                            Default_Section_3_Contact_Tel__c= 'Contact_Tel',
                                            Alternate_Section_3_Address_Line_1__c= 'Address_Line_1',
                                            Alternate_Section_3_Address_Line_2__c= 'Address_Line_2',
                                            Alternate_Section_3_Address_Line_3__c= 'Address_Line_3',
                                            Alternate_Section_3_City__c= 'City',
                                            Alternate_Section_3_Zip__c= 'Zip',
                                            Alternate_Section_3_Country__c = 'Country',
                                            Alternate_Section_3_Other__c = 'Other',
                                            Alternate_Section_3_Tax_Name__c = 'Tax_Name',
                                            Alternate_Section_3_Tax_Id__c = 'Tax_Id',
                                            Alternate_Section_3_Contact_Name__c= 'Contact_Name',
                                            Alternate_Section_3_Contact_Email__c= 'Contact_Email',
                                            Alternate_Section_3_Contact_Tel__c= 'Contact_Tel',
                                            Default_Section_4_Address_Line_1__c= 'Address_Line_1',
                                            Default_Section_4_Address_Line_2__c= 'Address_Line_2',
                                            Default_Section_4_Address_Line_3__c= 'Address_Line_3',
                                            Default_Section_4_City__c= 'City',
                                            Default_Section_4_Zip__c= 'Zip',
                                            Default_Section_4_Country__c = 'Country',
                                            Default_Section_4_Other__c= 'Other',
                                            Default_Section_4_Tax_Name__c= 'Tax_Name',
                                            Default_Section_4_Tax_Id__c= 'Tax_Id',
                                            Default_Section_4_Contact_Name__c= 'Contact_Name',
                                            Default_Section_4_Contact_Email__c= 'Contact_Email',
                                            Default_Section_4_Contact_Tel__c= 'Contact_Tel',
                                            Alternate_Section_4_Address_Line_1__c= 'Address_Line_1',
                                            Alternate_Section_4_Address_Line_2__c= 'Address_Line_2',
                                            Alternate_Section_4_Address_Line_3__c= 'Address_Line_3',
                                            Alternate_Section_4_City__c= 'City',
                                            Alternate_Section_4_Zip__c= 'Zip',
                                            Alternate_Section_4_Country__c = 'Country',
                                            Alternate_Section_4_Other__c = 'Other',
                                            Alternate_Section_4_Tax_Name__c = 'Tax_Name',
                                            Alternate_Section_4_Tax_Id__c = 'Tax_Id',
                                            Alternate_Section_4_Contact_Name__c= 'Contact_Name',
                                            Alternate_Section_4_Contact_Email__c= 'Contact_Email',
                                            Alternate_Section_4_Contact_Tel__c= 'Contact_Tel',
                                            VAT_Reclaim_Destination__c = FALSE,
                                            Final_Destination__c ='Nigeria',
                                            Country__c = country.id

                                            );
        insert cpav2;
        
        
        Test.startTest();
        	cpa2GeneralDetail_V3_Ctrl.getCPARecordTypeName(cpav2.Id);
        Test.stopTest();
    }

}