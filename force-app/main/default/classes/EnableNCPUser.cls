public class EnableNCPUser {
    
    @AuraEnabled
    public static contacts getContacts(Id accId){
        contacts wrapperObj = new contacts();
        Set<Id> conIds = new Set<Id>();
        if(String.isNotBlank(accId)){
            Map<Id, Contact> contactsOfAcc = new Map<Id, Contact>([SELECT Id,Client_Push_Notifications_Choice__c ,
                                                                   Email, Name, UserName__c,LastName,FirstName,
                                                                   Client_Notifications_Choice__c ,AccountId,
                                                                   Client_Tasks_notification_choice__c,App_Status__c
                                                                   FROM Contact 
                                                                   WHERE AccountId =: accId]);
            PermissionSet ps = [SELECT Id, Name FROM PermissionSet WHERE name= 'NCP_Login'];  
            Profile profileId = [SELECT Id FROM Profile WHERE Name = 'Tecex Customer' LIMIT 1];
            Map<Id, User> userWithNCP = new Map<Id, User>([SELECT Id, IsActive, ContactId FROM User WHERE 
                                                           ContactId =: contactsOfAcc.keyset() AND ProfileId   =: profileId.Id]);
            Map<Id, User> userWithNCPButNoPSAssigned = new Map<Id, User>(userWithNCP);
            Map<Id, PermissionSetAssignment> psa = new Map<Id, PermissionSetAssignment>([SELECT Id, AssigneeId, 
                                                                                         Assignee.Name,PermissionSetId
                                                                                         FROM PermissionSetAssignment
                                                                                         WHERE AssigneeId =: userWithNCP.keySet()
                                                                                         AND PermissionSetId =: ps.Id]);
            
            
            Set<Id> contactsIdwithNCP = new Set<Id>();
            List<Contact> contactsWithNCP = new List<Contact>();
            List<Contact> contactsNotActiveNCP = new List<Contact>();
            
            List<Contact> contactInactiveANDNotPS = new List<Contact>();
            
            For(PermissionSetAssignment psaObj : psa.Values()){
                userWithNCPButNoPSAssigned.remove(psaObj.AssigneeId);
                User userObj = userWithNCP.get(psaObj.AssigneeId);
                Contact conObj = contactsOfAcc.get(userObj.ContactId);
                if(conObj != Null){
                    if(userObj.IsActive){
                        contactsWithNCP.add(conObj); 
                        if(conObj.App_Status__c != 'Activated'){
                            contactInactiveANDNotPS.add(new Contact(Id = conObj.Id, App_Status__c = 'Activated'));
                        }
                    }else{
                        contactsNotActiveNCP.add(conObj);  
                        if(conObj.App_Status__c != 'Not Activated'){
                            contactInactiveANDNotPS.add(new Contact(Id = conObj.Id, App_Status__c = 'Not Activated'));
                        }
                    }
                    contactsIdwithNCP.add(conObj.Id);
                }
            }
            
            wrapperObj.contactsWithNCP = contactsWithNCP;
            wrapperObj.contactsNotActiveNCP = contactsNotActiveNCP;
            
            Map<Id, User> userWithDiffProf = new Map<Id, User>([SELECT Id, IsActive, ContactId FROM User WHERE 
                                                                ContactId =: contactsOfAcc.keyset() AND ProfileId !=: profileId.Id]);
            
            for(User usr : userWithDiffProf.Values()){
                conIds.add(usr.ContactId);
            }
            List<Contact> conWithoutNCP = [SELECT Id,Client_Push_Notifications_Choice__c ,
                                           Email,AccountId, Name, UserName__c,LastName,FirstName,
                                           Client_Notifications_Choice__c ,
                                           Client_Tasks_notification_choice__c
                                           FROM Contact 
                                           WHERE AccountId =: accId
                                           AND Id NOT IN : contactsIdwithNCP
                                           AND Id NOT IN : conIds];
            
            conWithoutNCP.addAll(contactsNotActiveNCP);
            wrapperObj.contactsWithoutNCP = conWithoutNCP;
            
            for(User usr : userWithNCPButNoPSAssigned.values()){
                Contact conObj = contactsOfAcc.get(usr.ContactId);
                system.debug('conObj>Name : '+conObj.Name);
                if(conObj.App_Status__c != 'Not Activated'){
                    contactInactiveANDNotPS.add(new Contact(Id = conObj.Id, App_Status__c = 'Not Activated'));
                }
            }
            if(contactInactiveANDNotPS.size() > 0){
                update contactInactiveANDNotPS;
            }
            
        }
        
        return wrapperObj;
        
    }    
    
    @AuraEnabled
    public static void createNCPUser(List<Contact> contacts){
        Map<Id, Contact> mapCon = new Map<Id, Contact>(contacts);
        // UpdateContacts(JSON.serialize(contacts));
        List<Contact> newConListToUpdateUser = new List<Contact>();
        
        Profile profileId = [SELECT Id FROM Profile WHERE Name = 'Tecex Customer' LIMIT 1];
        PermissionSet ps = [SELECT Id, Name FROM PermissionSet WHERE name= 'NCP_Login']; 
        
        Map<Id, User> userOfContacts = new Map<Id, User>([SELECT Id, IsActive,CommunityNickname, ContactId FROM User WHERE ContactId =: mapCon.keyset()]); 
        
        List<PermissionSetAssignment> permissionSetToUpdate = new List<PermissionSetAssignment>();
        Map<Id, User> userToCheckPermAndLicense = new Map<Id, User>();
        for(User usr : userOfContacts.values()){
            userToCheckPermAndLicense.put(usr.Id, usr);
            mapCon.remove(usr.ContactId);
        }
        
        Map<Id, PermissionSetAssignment> permSetAssLst = new Map<Id, PermissionSetAssignment>([SELECT Id, AssigneeId, 
                                                                                               Assignee.Name,PermissionSetId
                                                                                               FROM PermissionSetAssignment
                                                                                               WHERE AssigneeId =: userToCheckPermAndLicense.keySet()
                                                                                              ]);
        Set<Id> contactIdsToUpdateItsStatus = new Set<Id>();
        
        for(PermissionSetAssignment pSet : permSetAssLst.Values()){
            
            User userobj = userToCheckPermAndLicense.get(pSet.AssigneeId);
            if(userobj != null && pSet.PermissionSetId == ps.Id){
                contactIdsToUpdateItsStatus.add(userobj.ContactId);
                userToCheckPermAndLicense.remove(userobj.Id);
            }
        }
        for(User usr : userToCheckPermAndLicense.values()){
            contactIdsToUpdateItsStatus.add(usr.ContactId);
            PermissionSetAssignment psa = new PermissionSetAssignment (PermissionSetId = ps.Id, AssigneeId = usr.Id);
            permissionSetToUpdate.add(psa);
        }
        activeAndDeactivateUsers(contacts,true);       
        if(permissionSetToUpdate.size() > 0){
            upsert permissionSetToUpdate;
        }        
        
        // activeAndDeactivateUsers(contacts,true);       
        resetPasswords(userOfContacts.values());
        List<User> userList = new List<User>();
        
        //mapCon = Contact to create User
        
        List<String> nickNames = new List<String>();
        for(User usr : [SELECT Id, CommunityNickname FROM User LIMIT 10000]){
            nickNames.add(usr.CommunityNickname);
        }       
        for(Contact con : mapCon.values()){
            String nickname;
            if(nickNames.contains(con.Name)){
                nickname = con.Name +' '+con.Email.split('@', 2)[1];
                system.debug('nicknameTest :'+nickname);
                if(nickNames.contains(con.Name +' '+con.Email.split('@', 2)[1])){
                    nickname = con.Name +' '+con.Email.split('@', 2)[1]+'1';
                }
            }else{
                nickname = con.Name;
            }
            system.debug('nickname :'+nickname);
            User usr = new User(LastName = con.LastName,
                                FirstName=con.FirstName,
                                Alias = con.Name.left(8),
                                Email = con.Email,
                                CommunityNickname  = nickname,
                                Username = con.Email,
                                ContactId = con.Id,
                                ProfileId = profileId.Id,
                                TimeZoneSidKey = 'GMT',
                                LanguageLocaleKey = 'en_US',
                                EmailEncodingKey = 'UTF-8',
                                LocaleSidKey = 'en_US'
                               );
            userList.add(usr);
            
        }
        Map<String,Object> mapRec = new Map<String, Object>();
        if(userList.size() > 0){
            mapRec.put('CreateMultipleUser', userList);
            
            Flow.Interview.Create_NCP_User_1 flo = new  Flow.Interview.Create_NCP_User_1(mapRec);
            flo.start();
            
            List<User> createdUsers = (List<User>) flo.getVariableValue('CreateMultipleUser');
            
            
            List<PermissionSetAssignment> permissionSetList = new List<PermissionSetAssignment>();
            
            for(User usr : createdUsers){
                PermissionSetAssignment psa = new PermissionSetAssignment (PermissionSetId = ps.Id, AssigneeId = usr.Id);
                contactIdsToUpdateItsStatus.add(usr.contactId);
                permissionSetList.add(psa);
            }
            
            
            try{
                if(permissionSetList.size() > 0){
                    upsert permissionSetList;
                }
                for(PermissionSetAssignment psa : permissionSetList){
                    System.resetPassword(psa.AssigneeId, true);
                }
                /* if(contacts.size() > 0){
update contacts;
}*/
            }catch(exception e){
                system.debug('exception caught' + e);
            }
            
        }
        system.debug('contactIdsToUpdateItsStatus : '+contactIdsToUpdateItsStatus);
        List<Contact> conLisToUpdateItsStatus = new List<Contact>();
        for(Contact con : [SELECT Id, App_Status__C FROM Contact WHERE Id =: contactIdsToUpdateItsStatus]){
            conLisToUpdateItsStatus.add(new Contact(Id = con.Id, App_Status__C = 'Activated'));
        }
        system.debug('conLisToUpdateItsStatus1 : '+conLisToUpdateItsStatus);
        if(conLisToUpdateItsStatus.size() > 0){
            UpdateContacts(JSON.serialize(conLisToUpdateItsStatus));
            // update conLisToUpdateItsStatus;
        }
        
    }
    
    @AuraEnabled
    public static void saveContacts(List<Contact> lstCon){
        try{
            if(lstCon.size() > 0){
                update lstCon;
            }
        }catch(exception e){
            system.debug('exception caught' + e);
        }
    }
    
    @future
    public static void UpdateContacts(String jsonCon){
        String jsonStr = jsonCon.replaceAll('null', '""');
        List<Contact> lstCon = (List<Contact>) JSON.deserialize(jsonCon, List<Contact>.class);
        
        try{
            if(lstCon.size() > 0){
                update lstCon;
            }
        }catch(exception e){
            system.debug('exception caught' + e);
        }
    }
    
    @AuraEnabled
    public static void activeAndDeactivateUsers(List<Contact> lstCon, Boolean flag){
        List<User> updateUsers = new List<User>();
        List<Contact> conLisToUpdateItsStatus = new List<Contact>();
        try{
            if(lstCon.size() > 0){
                List<User> userWithNCP = new List<User>([SELECT Id, IsActive, ContactId FROM User WHERE ContactId =: lstCon]);
                for(User userObj : userWithNCP){
                    if(!flag){
                        conLisToUpdateItsStatus.add(new Contact(Id = userObj.ContactId, App_Status__C = 'Not Activated'));
                    }
                    /* if(flag){
conLisToUpdateItsStatus.add(new Contact(Id = userObj.ContactId, App_Status__C = 'Activated'));
}*/
                    userObj.IsActive = flag;
                    updateUsers.add(userObj);
                }
                if(updateUsers.size() > 0){
                    update updateUsers;
                }
                system.Debug('conLisToUpdateItsStatus :'+conLisToUpdateItsStatus);
                if(conLisToUpdateItsStatus.size() > 0){
                    UpdateContacts(JSON.serialize(conLisToUpdateItsStatus));
                }
            }
        }catch(exception e){
            system.debug('exception caught' + e);
        }
    }
    
    public static void resetPasswords(List<User> users){
        try{
            if(users.size() > 0){
                for(User userObj : users){
                    System.resetPassword(userObj.Id, true);
                }                
            }
        }catch(exception e){
            system.debug('exception caught' + e);
        }
    }
    
    public class contacts{
        @AuraEnabled
        public List<Contact> contactsWithNCP;
        @AuraEnabled
        public List<Contact> contactsWithoutNCP;
        @AuraEnabled
        public List<Contact> contactsNotActiveNCP;
    }
    
    
    
    @future(callout = true)
    public static void NCPRegistrationaddPM(ID Usr){
        PermissionSet ps = [SELECT Id, Name FROM PermissionSet WHERE name= 'NCP_Login']; 
        PermissionSetAssignment psa = new PermissionSetAssignment (PermissionSetId =ps.Id, AssigneeId = usr);
        insert psa;
        System.resetPassword(usr, true);
        
    }
    
    
}