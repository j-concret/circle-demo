public class RegistrationsTriggerHandler {
    
    public static void checkRegisrationsRecord(List<Registrations__c> registrationsListToValidate){
        
        Map<String, List<String>> accountToCountryMap = new Map<String, List<String>>();
        List<String> accountList = new List<String>();
        
        for(Registrations__c regt : registrationsListToValidate){
            accountList.add(regt.Company_name__c);
        }
        
        Map<Id, Registrations__c> registrationsMap = new Map<Id, Registrations__c>([Select Id,  Country__c , Company_name__c FROM Registrations__c where Company_name__c IN :accountList]);
        
        for(Registrations__c regt : registrationsMap.values()){
            if(accountToCountryMap.containsKey(regt.Company_name__c)){
                accountToCountryMap.get(regt.Company_name__c).add(regt.Country__c);
            }
            else{
                accountToCountryMap.put(regt.Company_name__c, new List<String>{regt.Country__c});
            }
            
        }
        
        for(Registrations__c regt : registrationsListToValidate){
            
            if(accountToCountryMap.containsKey(regt.Company_name__c) && accountToCountryMap.get(regt.Company_name__c).contains(regt.Country__c) && !registrationsMap.containsKey(regt.Id)){
                regt.addError('This client already has a registration for the selected Country.');
            }
        }
        
        
    }

}