@RestResource(urlMapping='/UpdateFinalDelivery/*')
global class CPUpdateFinalDelivery {
    @Httpput
    global static void UpdateFinalDelivery(){
    
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        Blob body = req.requestBody;
    	res.addHeader('Content-Type', 'application/json');
        String requestString = body.toString();
        CPUpdateFinalDeliveryWrapper rw = (CPUpdateFinalDeliveryWrapper)JSON.deserialize(requestString,CPUpdateFinalDeliveryWrapper.class);
        Final_Delivery__c FD = [select id,Shipment_Order__r.account__c,Name,Contact_name__c,Contact_email__c,Contact_number__c,Address_Line_1__c,Address_Line_2__c,City__c,Zip__c,Country__c from Final_Delivery__c where id=:rw.ID];
           try {
            FD.name = Rw.Name;
            FD.Contact_name__c= rw.ContactName;
            FD.Contact_email__c = rw.ContactEmail;
            FD.Contact_number__c = rw.ContactNumber;
            FD.Address_Line_1__c=rw.ContactAddreaaLine1;
            FD.Address_Line_2__c=rw.ContactAddreaaLine2;
            FD.City__c=rw.City;
            FD.Zip__c=rw.ZIP;
            FD.Country__c=rw.Country;
            
            
            Update FD;
            
            String ASD = 'Success- FinalDelivery Updated';	
            res.responseBody = Blob.valueOf(JSON.serializePretty(ASD));
            res.statusCode = 200;
              	API_Log__c Al = New API_Log__c();
                Al.Account__c=FD.Shipment_Order__r.Account__c;
                Al.EndpointURLName__c='UpdateFinalDelivery';
                Al.Response__c='Success- FinalDelivery Updated';
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al; 
            
            }
        	catch (Exception e){
                
              	 String ErrorString ='Something went wrong while updating final delivery details, please contact support_SF@tecex.com';
               	res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
            	res.statusCode = 500;
                API_Log__c Al = New API_Log__c();
                Al.Account__c=FD.Shipment_Order__r.Account__c;
                Al.EndpointURLName__c='UpdateFinalDelivery';
                Al.Response__c=e.getMessage()+' at line number: '+e.getLineNumber();
                Al.StatusCode__c=string.valueof(res.statusCode);
              	Insert Al;

            
        	}

        
        
    }
    
    

}