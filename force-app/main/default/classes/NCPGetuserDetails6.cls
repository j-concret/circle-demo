@RestResource(urlMapping='/NCPGetuserDetails/*')
global class NCPGetuserDetails6 {
    
    @Httpget
    global static void NCPGetuserDetails6(){	
        String jsonData='Test';
        ID AccID;
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        String ClientuserID = req.params.get('ClientuserID');//0050E0000076EKGQA2
        
        try {
            ID contactId = [Select contactid from User where id =: ClientuserID].contactId;
            AccID  = [Select AccountID from Contact where id =: contactid].AccountId;
            Account AC = new  Account();
            AC = [Select name,Id,Allow_Customised_Final_Deliveries__C,vetted_account__c,CSE_IOR__c,CSE_IOR__r.Firstname,CSE_IOR__r.Lastname,CSE_IOR__r.FullPhotoUrl,Financial_Controller__r.FullPhotoUrl,Financial_Controller__c,Financial_Controller__r.name from Account where ID =:AccID];
            
            // List<Contact> con = [Select id,firstname,lastname,email from contact where id =: contactId limit 1];
            user u = new user();
            u= [select id,name,email,FullPhotoUrl,contactid,profile.name from user where id =:ClientuserID and (profile.name= 'Tecex Customer' or profile.name='Zee Customer') limit 1];
            
            if(u.id != null){
                //user u= [select id,name,email,FullPhotoUrl,contactid from user where id ='0050E0000076EKGQA2' limit 1]; 
                //   system.debug('FullPhotoUrl-->'+u.FullPhotoUrl);
                String Accesstoken = GuidUtilCP.NewGuid();
                System.debug('Accesstoken'+Accesstoken);
                string Base64Accesstoken = EncodingUtil.base64Encode(Blob.valueof(Accesstoken));
                System.debug('Base64Accesstoken'+Base64Accesstoken);
                
                Access_token__c At = New Access_token__c();
                At.Access_Token__c=Accesstoken;
                At.AccessToken_encoded__c=Base64Accesstoken;
                Insert At;  
                
                JSONGenerator gen = JSON.createGenerator(true);
                gen.writeStartObject();
                
                
                //Creating Array & Object - Client Defaults
                gen.writeFieldName('ClientUserIds');
                gen.writeStartArray();
                
                //Start of Object - SClient Defaults	
                gen.writeStartObject();
                
                if(contactId == null) {gen.writeNullField('ClientContactId');} else{gen.writeStringField('ClientContactId', contactId);}
                if(u == null) {gen.writeNullField('loginContactEmail');} else{gen.writeStringField('loginContactEmail', u.email);}
                if(u == null) {gen.writeNullField('loginContactName');} else{gen.writeStringField('loginContactName', u.name);}
                if(u == null) {gen.writeNullField('loginContactphotourl');} else{gen.writeStringField('loginContactphotourl', u.FullPhotoUrl);}
                
                if(AccID == null) {gen.writeNullField('ClientAccID');} else{gen.writeStringField('ClientAccID', AccID);}
                if(Ac.Name == null) {gen.writeNullField('AccountName');} else{gen.writeStringField('AccountName', Ac.Name);}
                if(ClientuserID == null) {gen.writeNullField('ClientUserID');} else{gen.writeStringField('ClientUserID', ClientuserID);}
                if(at.AccessToken_encoded__c== null) {gen.writeNullField('AccessToken');} else{gen.writeStringField('AccessToken', at.AccessToken_encoded__c);}
                if(ac.CSE_IOR__c== null) {gen.writeNullField('AccountManagerID');} else{gen.writeStringField('AccountManagerID', ac.CSE_IOR__c);}
                if(ac.CSE_IOR__c== null) {gen.writeNullField('AccountManagerName');} else{gen.writeStringField('AccountManagerName', ac.CSE_IOR__r.Firstname+' '+ac.CSE_IOR__r.Lastname);}
                if(ac.CSE_IOR__c== null) {gen.writeNullField('AccountManagerProfilepic');} else{gen.writeStringField('AccountManagerProfilepic', ac.CSE_IOR__r.FullPhotoUrl);}
				if(ac.Financial_Controller__c== null) {gen.writeNullField('FinancialControllerId');} else{gen.writeStringField('FinancialControllerId', ac.Financial_Controller__c);}
				if(ac.Financial_Controller__r.name == null) {gen.writeNullField('FinancialControllerName');} else{gen.writeStringField('FinancialControllerName', ac.Financial_Controller__r.name);}
                if(ac.Financial_Controller__r.FullPhotoUrl == null) {gen.writeNullField('FinancialControllerProfilePic');} else{gen.writeStringField('FinancialControllerProfilePic', ac.Financial_Controller__r.FullPhotoUrl);}
                if(ac.vetted_account__c== null) {gen.writeNullField('vettedaccount');} else{gen.writeBooleanField('vettedaccount', ac.vetted_account__c);}
				if(ac.Allow_Customised_Final_Deliveries__C== null) {gen.writeNullField('AllowCustomisedFinalDeliveries');} else{gen.writeBooleanField('AllowCustomisedFinalDeliveries', ac.Allow_Customised_Final_Deliveries__C);}
				
                gen.writeEndObject();
                //End of Object - Client Defaults
                
                gen.writeEndArray();
                //End of Array - Client Defaults 
                
                gen.writeEndObject();
                jsonData = gen.getAsString();
                
                
                res.responseBody = Blob.valueOf(jsonData);
                res.addHeader('Content-Type', 'application/json');
                res.statusCode = 200;
                
                API_Log__c Al = New API_Log__c();
                Al.Account__c=AccID;
                Al.EndpointURLName__c='NCP-GetUserdetails';
                Al.Response__c='NCP-Success - User details are sent';
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al;
            }else{
                JSONGenerator gen = JSON.createGenerator(true);
                gen.writeStartObject();
                
                
                //Creating Array & Object - Client Defaults
                gen.writeFieldName('ClientUserIds');
                gen.writeStartArray();
                
                //Start of Object - SClient Defaults	
                gen.writeStartObject();
                gen.writeStringField('Error', ' What are you trying to do?');
                
                //End of Object - Client Defaults
                
                gen.writeEndArray();
                //End of Array - Client Defaults 
                
                gen.writeEndObject();
                jsonData = gen.getAsString();
                res.responseBody = Blob.valueOf(jsonData);
                res.addHeader('Content-Type', 'application/json');
                res.statusCode = 200;
                
                API_Log__c Al = New API_Log__c();
                Al.Account__c=AccID;
                Al.EndpointURLName__c='NCP-GetUserdetails';
                Al.Response__c='NCP-Success - User details are sent';
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al;
                
            }
            
            
        }
        Catch (Exception e){
            
            System.debug('Line number ====> '+e.getLineNumber() + 'Exception Message =====> '+e.getMessage());
            
            String ErrorString ='NCP- Error - User details are not sent';
            res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
            res.addHeader('Content-Type', 'application/json');
            res.statusCode = 500;
            API_Log__c Al = New API_Log__c();
            Al.Account__c=AccID;
            Al.EndpointURLName__c='NCP - GetUserDetails';
            Al.Response__c=e.getMessage()+' at line number: '+e.getLineNumber();
            Al.StatusCode__c=string.valueof(res.statusCode);
            Insert Al;
            
        }
    }
}