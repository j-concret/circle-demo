@RestResource(urlMapping='/CurrencyConvertionrates/*')
global class CSICurrencyConvertion {
  @Httppost
      global static void NCPCurrencyConvertion(){
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        Blob body = req.requestBody;
      	res.addHeader('Content-Type', 'application/json');
        String requestString = body.toString();
         NCPCurrencyConvertionWra rw = (NCPCurrencyConvertionWra)JSON.deserialize(requestString,NCPCurrencyConvertionWra.class); 
          
          try {
              
            Access_token__c At =[select status__c,Access_Token__c from Access_token__c where Access_Token__c=:rw.Accesstoken ];
            	if( at.status__C =='Active'){
                    
                     Currency_Management2__c Cm   =[select id,Conversion_Rate__c,Currency__c,ISO_Code__c from Currency_Management2__c where ISO_Code__c =: rw.curr limit 1];
                
             JSONGenerator gen = JSON.createGenerator(true);
             gen.writeStartObject();
             
         
             //Creating Array & Object - Client Defaults
             gen.writeFieldName('CurrencyConvertion');
             	gen.writeStartArray();
     		        
        				//Start of Object - SClient Defaults	
                         gen.writeStartObject();
                          
                    if(cm.Conversion_Rate__c == null) {gen.writeNullField('Conversion_Rate');} else{gen.writeNumberField('Conversion_Rate', cm.Conversion_Rate__c);}
                    if(cm.Currency__c == null) {gen.writeNullField('Currency');} else{gen.writeStringField('Currency', cm.Currency__c);}
                    if(cm.ISO_Code__c == null) {gen.writeNullField('ISO_Code');} else{gen.writeStringField('ISO_Code', cm.ISO_Code__c);}
                    		           				 
                							
         gen.writeEndObject();
                         //End of Object - Client Defaults
    				
    		gen.writeEndArray();
                //End of Array - Client Defaults 
             
            gen.writeEndObject();
    		String jsonData = gen.getAsString();
             
             
              res.responseBody = Blob.valueOf(jsonData);
              res.addHeader('Content-Type', 'application/json');
              res.statusCode = 200;
                
                API_Log__c Al = New API_Log__c();
                Al.Account__c=rw.AccountID;
                Al.Login_Contact__c=rw.ContactID;
                Al.EndpointURLName__c='NCP-CurrencyConvertion';
                Al.Response__c='NCP-Success - Currency Convertion are sent';
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al;

                    
            				}
              
          		}
          catch(exception e){
              
              System.debug('Line number ====> '+e.getLineNumber() + 'Exception Message =====> '+e.getMessage());
            
           		String ErrorString ='NCP- Error - Currency Convertion are not sent';
               	res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
                res.addHeader('Content-Type', 'application/json');
            	res.statusCode = 500;
                API_Log__c Al = New API_Log__c();
                 Al.Account__c=rw.AccountID;
                Al.Login_Contact__c=rw.ContactID;
                Al.EndpointURLName__c='NCP - Currency Convertion';
                Al.Response__c=e.getMessage()+' at line number: '+e.getLineNumber();
                Al.StatusCode__c=string.valueof(res.statusCode);
              	Insert Al;
          
          	}
          
          
      }

    
}