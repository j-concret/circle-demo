public with sharing class Lex_ServiceTeamDB {
    
    @Auraenabled
    public static DetailDashBoardWrapper getSORecords(List<String> filtericeUsers, List<String> serviceType,  List<String> whoDoesFreight, List<String> region){
        System.debug('serviceType '+serviceType);
       
     
        String subWhereClause ='';
        
        if(serviceType.size()> 0 || whoDoesFreight.size() > 0 || region.size()>0){
            subWhereClause= 'AND';
        }
        if(serviceType.size() > 0){
            subWhereClause = subWhereClause + ' Service_Type__c =: serviceType AND';
        }
        
        if(whoDoesFreight.size() > 0){
            subWhereClause = subWhereClause + ' Who_arranges_International_courier__c =: whoDoesFreight AND';
        }
        if(region.size() > 0){
            subWhereClause = subWhereClause + ' Region__c =: region AND';
        }
       
        subWhereClause = subWhereClause.left(subWhereClause.length() - 3);


       
        User userDetails = [SELECT Id, Name, Email, Profile.Name, UserRole.Name FROM User where Id=:userinfo.getUserId()];
       String fieldName;
       String userId = userinfo.getUserId();
       Boolean isAmUser = true;
       
       if(userDetails.UserRole.Name == 'TecEx AM Team Leader' || userDetails.UserRole.Name == 'TecEx AM Team'){
        fieldName = 'IOR_CSE__c';
       }else if(userDetails.UserRole.Name == 'TecEx ICE Team Leader' || userDetails.UserRole.Name == 'TecEx ICE Team'){
        fieldName = 'ICE__c';
        isAmUser = false;
       }else{
        fieldName = 'IOR_CSE__c'; 
       }
       String tecexAssignedTo = isAmUser ? 'AM' : 'ICE';
        Map<String, Object> result = new map<String, Object>();
        List<String> statusToExclude = new List<String>{'Cancelled - With fees/costs' ,'Cancelled - No fees/costs' ,'Shipment Abandoned'};
        String query;
        if(filtericeUsers.size() > 0){
            query = 'SELECT ID, Account__r.Name,ICE__r.Name, ICE__c,ETA_Days_Business__c,Account__c,Pressure_Point__c,Who_arranges_International_courier__c,Mapped_Shipping_status__c, Internal_Shipment_Type__c, Name, Ship_From_Country__c, Ship_to_Country_new__c,'+
            +'CPA_v2_0__r.Name,CPA_v2_0__r.Tracking_Term__c,CPA_v2_0__r.Green_Lane__c, Shipment_Value_USD__c,Shipping_Status__c,IOR_CSE__c, IOR_CSE__r.Name, Lead_ICE__r.Name, Sub_Status_Update__c,ETA_Days__c,(SELECT Id, Name,Pickup_availability_Ready__c,Create_Pickup__c FROM Freight__r LIMIT 1),'+
            +'(SELECT Id,Description__c,Date_Time__c,Task_State__c FROM SO_Travel_History__r ORDER By lastmodifieddate desc LIMIT 1),'+
            +'Customs_Compliance__c,Shipping_Documents__c,Pick_up_Coordination__c,Invoice_Payment__c,Region__c,Domestic_Tracking_Link__c,Tracking_Link__c  '+
            +'FROM Shipment_order__c WHERE Test_Account__c = false AND Shipping_Status__c NOT IN : statusToExclude AND '+fieldName+ ' =: userId AND (Lead_ICE__c IN : filtericeUsers OR ICE__c IN : filtericeUsers) order by Lastmodifieddate desc LIMIT 1000';
        }else{
            query = 'SELECT ID, Account__r.Name,ICE__r.Name, ICE__c,ETA_Days_Business__c,Account__c,Pressure_Point__c,Who_arranges_International_courier__c,Mapped_Shipping_status__c, Internal_Shipment_Type__c, Name, Ship_From_Country__c, Ship_to_Country_new__c,'+
            +'CPA_v2_0__r.Name,CPA_v2_0__r.Tracking_Term__c,CPA_v2_0__r.Green_Lane__c, Shipment_Value_USD__c,Shipping_Status__c,IOR_CSE__c, IOR_CSE__r.Name, Lead_ICE__r.Name, Sub_Status_Update__c,ETA_Days__c,(SELECT Id, Name,Pickup_availability_Ready__c,Create_Pickup__c FROM Freight__r LIMIT 1),'+
            +'(SELECT Id,Description__c,Date_Time__c,Task_State__c FROM SO_Travel_History__r ORDER By lastmodifieddate desc LIMIT 1),'+
            +'Customs_Compliance__c,Shipping_Documents__c,Pick_up_Coordination__c,Invoice_Payment__c,Region__c,Domestic_Tracking_Link__c,Tracking_Link__c '+
            +'FROM Shipment_order__c WHERE Test_Account__c = false AND Shipping_Status__c NOT IN : statusToExclude AND '+fieldName+ ' =: userId '+subWhereClause+' order by Lastmodifieddate desc LIMIT 1000';
        }
        
        
        System.debug('query '+query);
        Map<Id, Shipment_Order__C> initialSORecords = new Map<Id, Shipment_Order__C>((List<Shipment_Order__C>)Database.query(query));        
      system.debug('initialSORecords '+initialSORecords.size());

      Map<Id, AggregateResult> countZKShipments = new Map<Id,AggregateResult>([Select Freight_Request__r.Shipment_order__c Id, count(Id) zkcount FROM zkmulti__MCShipment__c where Freight_Request__r.Shipment_Order__c =: initialSORecords.keyset() GROUP BY Freight_Request__r.Shipment_Order__c]);
        Map<Id, AggregateResult> totalFDaForEachSO = new Map<Id,AggregateResult>([Select Shipment_Order__C Id, count(Id) fdCount from Final_Delivery__c WHERE Shipment_Order__c =: initialSORecords.keyset()  Group By Shipment_Order__C]);
        Map<Id, AggregateResult> totalPODRecFDaForEachSO = new Map<Id,AggregateResult>([Select Shipment_Order__C Id, count(Id) fdCount from Final_Delivery__c WHERE Delivery_Status__c = 'POD Received' AND Shipment_Order__c =: initialSORecords.keyset() Group By Shipment_Order__C]);
        //Getting ICE users

        List<User> iceUsers = [SELECT Id, Name, UserRole.Name FROM User WHERE (UserRole.Name = 'TecEx ICE Team Leader' OR UserRole.Name = 'TecEx ICE Team') ORDER BY Name];
        System.debug('iceUsers' +iceUsers);
       List<Option> iceUsersForFilters = new List<Option>();
       for(User userobj : iceUsers){
           Option opt = new Option();
           opt.label = userobj.Name;
           opt.value = userobj.id;
           iceUsersForFilters.add(opt);
       }

                                                                        
        Integer noOfComplianceSO = 0;
        Integer noOfTrackingSO = 0;
        Integer noOfCompleteSO = 0;

      
        Integer unresolvedTasksBTS = 0;
        Integer unresolvedTasks = 0;
        Integer AvgTimeUntilApprovedToShip = 0;

       
        Integer avgTimeUntilDelivered  = 0;
        Integer countSOAwatingPOD = 0;

        Set<Id> soIdsInComplianceTab = new Set<Id>();
        Set<Id> soIdsInTrackingTab = new Set<Id>();
        



        DetailDashBoardWrapper DDBWrap = new DetailDashBoardWrapper();
        DetailDashBoardSection ddbsCompliancePending =  new DetailDashBoardSection();
        ddbsCompliancePending.sectionName = 'COMPLIANCE PENDING';
        DetailDashBoardSection ddbsshipmentAwatingPkup =  new DetailDashBoardSection();
        ddbsshipmentAwatingPkup.sectionName = 'SHIPMENTS AWAITING PICKUP';
        DetailDashBoardSection ddbsOnHoldShipments =  new DetailDashBoardSection();
        ddbsOnHoldShipments.sectionName = 'ON HOLD SHIPMENTS';
        DetailDashBoardSection ddbsDAP =  new DetailDashBoardSection();
        ddbsDAP.sectionName = 'General';
        DetailDashBoardSection ddbsE2E =  new DetailDashBoardSection();
        ddbsE2E.sectionName = 'E2E';
        DetailDashBoardSection ddbsD2D =  new DetailDashBoardSection();
        ddbsD2D.sectionName = 'D2D';
        DetailDashBoardSection ddbsShipmentAwatingPOD =  new DetailDashBoardSection();
        ddbsShipmentAwatingPOD.sectionName = 'Shipments Awaiting PODs';
        
        map<Id, AggregateResult> userOutstandingTasksDNBATS = new Map<Id, AggregateResult>([SELECT Shipment_order__c Id, Count(Id) taskcount FROM Task 
                                                                                 WHERE (State__c != 'Resolved' AND State__c != 'Client Pending')
                                                                                 AND IsNotApplicable__c  = false
                                                                                 AND Inactive__c = false
                                                                                 AND Shipment_order__c IN : initialSORecords.keyset()
                                                                                 AND TecEx_Assigned_To__c =: tecexAssignedTo
                                                                                 AND Blocks_Approval_to_Ship__c = false
                                                                                 GROUP BY Shipment_order__c]);

        map<Id, AggregateResult> userOutstandingTasksBTS = new Map<Id, AggregateResult>([SELECT Shipment_order__c Id, Count(Id) taskcount FROM Task 
                                                                                 WHERE (State__c != 'Resolved' AND State__c != 'Client Pending')
                                                                                 AND IsNotApplicable__c  = false
                                                                                 AND Inactive__c = false
                                                                                 AND Shipment_order__c IN : initialSORecords.keyset()
                                                                                 AND TecEx_Assigned_To__c =: tecexAssignedTo
                                                                                 AND Blocks_Approval_to_Ship__c = true
                                                                                 GROUP BY Shipment_order__c]);

                                                                                 
        map<Id, AggregateResult> clientOutstandingTasks = new Map<Id, AggregateResult>([SELECT Shipment_order__c Id, Count(Id) taskcount FROM Task 
                                                                                 WHERE State__c = 'Client Pending'
                                                                                 AND IsNotApplicable__c  = false
                                                                                 AND Inactive__c = false
                                                                                 AND Shipment_order__c IN : initialSORecords.keyset()
                                                                                 
                                                                                 GROUP BY Shipment_order__c]);
                
        for(Shipment_Order__c soObj : initialSORecords.values()){
            rowDetail row = new rowDetail();
            row.client = soObj.Account__r.Name;
            row.clientId = soObj.Account__c;
            row.Id = soObj.Id;
            row.internalShipmentType = soObj.Internal_Shipment_Type__c;
            row.name = soObj.Name;
            row.shipFrom = soObj.Ship_From_Country__c;
            row.shipTo = soObj.Ship_to_Country_new__c;
            row.CPAId = soObj.CPA_v2_0__c;
            row.CPA = soObj.CPA_v2_0__r.Name;
            row.shipmentValue = soObj.Shipment_Value_USD__c;
            if(soObj.Shipment_Value_USD__c > 100000){
                row.shipmentValueColour = 'color: red;width:100px;';
            }else{
                row.shipmentValueColour = 'width:100px;';
            }
            row.amId = soObj.IOR_CSE__c;
            row.am = soObj.IOR_CSE__r.Name;
            row.iceId = soOBj.ICE__c;
            row.ice = soOBj.ICE__r.Name;
            if(soObj.Sub_Status_Update__c != 'NONE'){
                row.subStatus = soObj.Sub_Status_Update__c;
            }

            if(countZKShipments.containsKey(soObj.id)){
                row.trackingLinkColor = (Decimal)countZKShipments.get(soObj.id).get('zkcount') > 0 ? 'color:green;' :'color:#4a7dcc;';
            }
            if(soObj.Tracking_Link__c != null){
                String track = soObj.Tracking_Link__c.split('"')[1];
                        if(track.contains('&amp;')){
                            track = track.replaceAll('&amp;','&');
                        }
                        row.trackingLink = track;
               
            }else if(soObj.Domestic_Tracking_Link__c != null){
                String track = soObj.Domestic_Tracking_Link__c.split('"')[1];
                        if(track.contains('&amp;')){
                            track = track.replaceAll('&amp;','&');
                        }
                        row.trackingLink = track;
              
            }
            row.mappedShippingStatus = soObj.Mapped_Shipping_status__c;
            if(!String.isblank(soObj.ETA_Days__c)){
                if(soObj.ETA_Days__c.contains('<')){
                    row.etaflag = soObj.ETA_Days__c.split('"')[1];
                }
                
                if(soObj.Mapped_Shipping_status__c == 'Compliance Pending'){
                    row.etaDays = soObj.ETA_Days_Business__c.split('<',2)[0];
                }else{
                    row.etaDays = soObj.ETA_Days__c.split('<',2)[0];
                }
                if(row.etaDays.contains('0 Days')){
                    row.etaDaysColour = 'color: red;width:130px;';
                }
               if(row.etaDays.contains('+')){
                    
                    row.plusSignETADays = '+';
                    row.etaDays = row.etaDays.right(row.etaDays.length() - 2);
                }
                String s = row.etaDays.left(row.etaDays.length() - (row.etaDays.length() - 2));
                  if(s.contains(' ')){
                        s = s.left(1);
                    }
                    row.eta = Integer.valueof(s);   
            }
            
            row.whoDoesFreight = soObj.Who_arranges_International_courier__c;
            row.userOutstandingTasksBTS = userOutstandingTasksBTS.containsKey(soObj.Id) ? (Decimal)userOutstandingTasksBTS.get(soObj.Id).get('taskcount') : 0;
            row.userOutstandingTasksDNBATS = userOutstandingTasksDNBATS.containsKey(soObj.Id) ? (Decimal)userOutstandingTasksDNBATS.get(soObj.Id).get('taskcount') : 0;
            row.clientOutstandingTasks = clientOutstandingTasks.containsKey(soObj.Id) ? (Decimal)clientOutstandingTasks.get(soObj.Id).get('taskcount') : 0;
            if(soObj.Freight__r.size() > 0){
            row.pickupAvailabilityReady = soObj.Freight__r[0].Pickup_availability_Ready__c;
            row.createPickup = soObj.Freight__r[0].Create_Pickup__c;
            if(soObj.Freight__r[0].Pickup_availability_Ready__c != null){
                row.pickupScheduled = soObj.Freight__r[0].Pickup_availability_Ready__c.date();
                row.pickupTime = soObj.Freight__r[0].Pickup_availability_Ready__c.Time(); 
                if(soObj.Freight__r[0].Pickup_availability_Ready__c.date() < Date.today()){
                    row.pickupScheduledDateColor = 'color: red;width:130px;';
                }
            }
            }
            
            row.complianceProgress = new List<String>{'background-color:'+soObj.Customs_Compliance__c,'background-color:'+soObj.Shipping_Documents__c,'background-color:'+soObj.Pick_up_Coordination__c,'background-color:'+soObj.Invoice_Payment__c};
          String complianceProgressHover;
          integer i = 0;
          List<String> lst = new List<String>{soObj.Customs_Compliance__c,soObj.Shipping_Documents__c, soObj.Pick_up_Coordination__c,soObj.Invoice_Payment__c};
          for(i=0;i<4;i++){
              if(i == 0){
                complianceProgressHover = 'Compliance Pending : ';
              }else if(i == 1){
                complianceProgressHover = complianceProgressHover + 'Shipping Documents : ';
            }else if(i == 2){
                complianceProgressHover = complianceProgressHover + 'Pickup Coordination : ' ;
            }else if(i == 3){
                complianceProgressHover = complianceProgressHover + 'Invoice Documents : ';
            }

            if(lst[i] == 'Orange'){
                complianceProgressHover = complianceProgressHover+'Tecex in progress, ';
                }else if(lst[i] == 'Red'){
                complianceProgressHover = complianceProgressHover+'Blocking, ';
            }else if(lst[i] == 'Green'){
                complianceProgressHover = complianceProgressHover+'Approved, ';
            }
        
          }
          row.complianceProgressHover = complianceProgressHover;  
                
                
                
            if(soObj.SO_Travel_History__r.size() > 0){
            row.soLog = soObj.SO_Travel_History__r[0].Description__c;
            row.soLogTime = soObj.SO_Travel_History__r[0].Date_Time__c;
          }
            
           row.businessUnit = soObj.Region__c; 
           row.trackingTerm = soObj.CPA_v2_0__r.Tracking_Term__c;
           List<String> stars = new List<String>();
           
           if(soObj.Internal_Shipment_Type__c != null && soObj.Internal_Shipment_Type__c.contains('1st Time Shippers') ){
            stars.add('yellow');
           }
           if(soObj.Internal_Shipment_Type__c != null && soObj.Internal_Shipment_Type__c.contains('Sensitive')){
            stars.add('pink');
           }
           if(soObj.Pressure_Point__c){
            stars.add('red'); 
           }
           row.stars = stars;
           if(soObj.Mapped_Shipping_status__c == 'In-Transit'){
           soIdsInTrackingTab.add(soObj.Id);
           }
           
           if(soObj.Mapped_Shipping_status__c == 'Compliance Pending' && userOutstandingTasksBTS.containsKey(soObj.Id)){
            unresolvedTasksBTS = unresolvedTasksBTS + (Integer)userOutstandingTasksBTS.get(soObj.Id).get('taskcount');
           }
           if(soObj.Mapped_Shipping_status__c == 'Compliance Pending' && userOutstandingTasksDNBATS.containsKey(soObj.Id)){
            unresolvedTasks = unresolvedTasks + (Integer)userOutstandingTasksDNBATS.get(soObj.Id).get('taskcount');
           }
            system.debug('soObj.Mapped_Shipping_status__c :'+soObj.Mapped_Shipping_status__c);
            
           if(soObj.Sub_Status_Update__c == 'On Hold' && soObj.Shipping_Status__c != 'POD Received' && soObj.Shipping_Status__c != 'Customs Clearance Docs Received'){
                   
                List<rowDetail> rd = new List<rowDetail>(ddbsOnHoldShipments.sectionRows); 
                rd.add(row);
                ddbsOnHoldShipments.sectionRows = rd;
                //ddbsOnHoldShipments.showshippingstatus = false;
                ddbsOnHoldShipments.showCPA = false;
                ddbsOnHoldShipments.showComplianceProgress = false;
              
                ddbsOnHoldShipments.showetaDays = false;
                ddbsOnHoldShipments.showPickupScheduleTime = false;
                ddbsOnHoldShipments.showClientOutstandingTasks = false;
                ddbsOnHoldShipments.showBusinessUnit = false;
                ddbsOnHoldShipments.showWhoDoesFreight = false;

                ddbsOnHoldShipments.showCountFdPODRec = false;
                ddbsOnHoldShipments.showCountTotalFDA = false;
                ddbsOnHoldShipments.showTrackingLink = false;
                ddbsOnHoldShipments.showTrackingTerm = false;

                    noOfComplianceSO = noOfComplianceSO +1;

                
            }else if(soObj.Mapped_Shipping_status__c == 'Compliance Pending'){                    
                    List<rowDetail> rd = new List<rowDetail>(ddbsCompliancePending.sectionRows); 
                    rd.add(row);
                    ddbsCompliancePending.sectionRows = rd;
                   
                    ddbsCompliancePending.showshippingstatus = false;
                    ddbsCompliancePending.showPickupScheduleTime = false;
                    ddbsCompliancePending.showBusinessUnit = false;
                    ddbsCompliancePending.showWhoDoesFreight = false;

                    ddbsCompliancePending.showCountFdPODRec = false;
                    ddbsCompliancePending.showCountTotalFDA = false;
                    ddbsCompliancePending.showTrackingLink = false;
                    ddbsCompliancePending.showTrackingTerm = false;
                    noOfComplianceSO = noOfComplianceSO +1;
                
                
                
            }else if(soObj.Mapped_Shipping_status__c == 'Approved to Ship'){                
                     
                    List<rowDetail> rd = new List<rowDetail>(ddbsshipmentAwatingPkup.sectionRows); 
                    rd.add(row);
                    ddbsshipmentAwatingPkup.sectionRows = rd;
                
                    ddbsshipmentAwatingPkup.showCPA = false;
                    ddbsshipmentAwatingPkup.showshippingstatus = false;
                    ddbsshipmentAwatingPkup.showClientOutstandingTasks = false;
                    ddbsshipmentAwatingPkup.showUserOuntstandingTask = false;
                    ddbsshipmentAwatingPkup.showUserOutstandingTasksDNBATS = false;
                    ddbsshipmentAwatingPkup.showComplianceProgress = false;
                  
                    ddbsshipmentAwatingPkup.showetaDays = false;
                    ddbsshipmentAwatingPkup.showBusinessUnit = false;
                    ddbsshipmentAwatingPkup.showCountFdPODRec = false;
                    ddbsshipmentAwatingPkup.showCountTotalFDA = false; 
                    ddbsshipmentAwatingPkup.showTrackingTerm = false; 
                    noOfComplianceSO = noOfComplianceSO +1;
               
            }
            
            if((soObj.Mapped_Shipping_status__c == 'In-Transit' || soObj.Mapped_Shipping_status__c == 'Arrived in Country' || soObj.Mapped_Shipping_status__c == 'Cleared Customs' || soObj.Mapped_Shipping_status__c == 'Final Delivery In transit') && soObj.CPA_v2_0__r.Green_Lane__c ){
                List<rowDetail> rd = new List<rowDetail>(ddbsE2E.sectionRows);
                rd.add(row);
                ddbsE2E.sectionRows = rd;
                ddbsE2E.etaColname = 'ETA Until Delivered';
                
                ddbsE2E.showShipmentValue = false;
                ddbsE2E.showWhoDoesFreight = false;
                
                ddbsE2E.showBusinessUnit = false;
                ddbsE2E.showClientOutstandingTasks = false;
                ddbsE2E.showUserOuntstandingTask = false;
                ddbsE2E.showComplianceProgress = false;
                ddbsE2E.showPickupScheduleTime = false;
                
                ddbsE2E.showCountFdPODRec = false;
                ddbsE2E.showCountTotalFDA = false;  
               

                noOfTrackingSO = noOfTrackingSO+1;             
            }else if((soObj.Mapped_Shipping_status__c == 'In-Transit' || soObj.Mapped_Shipping_status__c == 'Arrived in Country' || soObj.Mapped_Shipping_status__c == 'Cleared Customs' || soObj.Mapped_Shipping_status__c == 'Final Delivery In transit') && ( soObj.CPA_v2_0__r.Tracking_Term__c=='DAP - Supplier' || soObj.CPA_v2_0__r.Tracking_Term__c == 'DDP' || soObj.CPA_v2_0__r.Tracking_Term__c == 'DAP - End User')){
                List<rowDetail> rd = new List<rowDetail>(ddbsDAP.sectionRows);
                rd.add(row);
                ddbsDAP.sectionRows = rd;

                ddbsDAP.etaColname = 'ETA Until Delivered';
                ddbsDAP.showShipmentValue = false;
                ddbsDAP.showWhoDoesFreight = false;
               
                ddbsDAP.showBusinessUnit = false;
                ddbsDAP.showClientOutstandingTasks = false;
                ddbsDAP.showUserOuntstandingTask = false;
                ddbsDAP.showComplianceProgress = false;
                ddbsDAP.showPickupScheduleTime = false;
                ddbsDAP.showCountFdPODRec = false;
                ddbsDAP.showCountTotalFDA = false;
               

                noOfTrackingSO = noOfTrackingSO+1;             
            }
            
            /*else if((soObj.Mapped_Shipping_status__c == 'In-Transit' || soObj.Mapped_Shipping_status__c == 'Arrived in Country' || soObj.Mapped_Shipping_status__c == 'Cleared Customs') && ()){
                List<rowDetail> rd = new List<rowDetail>(ddbsD2D.sectionRows);
                rd.add(row);
                ddbsD2D.sectionRows = rd;
                ddbsD2D.etaColname = 'ETA Until Delivered';
                
                ddbsD2D.showShipmentValue = false;
                ddbsD2D.showWhoDoesFreight = false;
                
                ddbsD2D.showBusinessUnit = false;
                ddbsD2D.showClientOutstandingTasks = false;
                ddbsD2D.showUserOuntstandingTask = false;
                ddbsD2D.showComplianceProgress = false;
                ddbsD2D.showPickupScheduleTime = false;

                ddbsD2D.showCountFdPODRec = false;
                ddbsD2D.showCountTotalFDA = false;
                

                noOfTrackingSO = noOfTrackingSO+1;             
                 
            }*/
           
            if(soObj.Shipping_Status__c == 'Awaiting POD'){
                List<rowDetail> rd = new List<rowDetail>(ddbsShipmentAwatingPOD.sectionRows);
                rd.add(row);
                ddbsShipmentAwatingPOD.sectionRows = rd;
    
                
                ddbsShipmentAwatingPOD.showShipmentValue = false;
                ddbsShipmentAwatingPOD.showWhoDoesFreight = false;
                ddbsShipmentAwatingPOD.showAM = false;
                ddbsShipmentAwatingPOD.showICE = false;
                ddbsShipmentAwatingPOD.showetaDays = false;
                ddbsShipmentAwatingPOD.showBusinessUnit = false;
                ddbsShipmentAwatingPOD.showClientOutstandingTasks = false;
                ddbsShipmentAwatingPOD.showUserOuntstandingTask = false;
                ddbsShipmentAwatingPOD.showComplianceProgress = false;
                ddbsShipmentAwatingPOD.showPickupScheduleTime = false;
                ddbsShipmentAwatingPOD.showSOLog = false;
                ddbsShipmentAwatingPOD.showSOLogTime = false;
                ddbsShipmentAwatingPOD.showCPA = false;
                ddbsShipmentAwatingPOD.showTrackingLink = false;
                ddbsShipmentAwatingPOD.showTrackingTerm = false;
            
                noOfCompleteSO = noOfCompleteSO+1;
       
                row.countFdPODRec = totalPODRecFDaForEachSO.containsKey(soObj.Id) ? (Integer)totalPODRecFDaForEachSO.get(soObj.Id).get('fdCount') : 0;
                
                row.countTotalFDA = totalFDaForEachSO.containsKey(soObj.Id) ? (Integer)totalFDaForEachSO.get(soObj.Id).get('fdCount') : 0;
                if(totalPODRecFDaForEachSO.containsKey(soObj.Id)){
                    countSOAwatingPOD = countSOAwatingPOD+1;
                }
            }

            }        
            Map<Id, Shipment_Order__C> soForDateAvgCompliancetab = new Map<Id, Shipment_Order__C>([SELECT Id,Go_Live_Date__c, Date_Client_Approved_to_Ship__c 
                                                                        FROM Shipment_Order__C 
                                                                        WHERE Date_Client_Approved_to_Ship__c = LAST_N_DAYS:30 
                                                                        AND Date_Client_Approved_to_Ship__c != null 
                                                                        AND IOR_CSE__c =: userDetails.Id 
                                                                        AND Id =: soIdsInComplianceTab
                                                                        order by Lastmodifieddate desc 
                                                                        LIMIT 1000]);
        

        Integer dateSum = 0;
        for(Shipment_Order__C soObj : soForDateAvgCompliancetab.values()){
            dateSum = dateSum + (soObj.Go_Live_Date__c.day() - soObj.Date_Client_Approved_to_Ship__c.day());
        }

        if(soForDateAvgCompliancetab.values().size() > 0){
            AvgTimeUntilApprovedToShip = dateSum/ soForDateAvgCompliancetab.values().size();
        }

        Map<Id, Shipment_Order__C> soForDateAvgTrackingtab = new Map<Id, Shipment_Order__C>([SELECT Id,Go_Live_Date__c, Date_Client_Approved_to_Ship__c,Final_Delivery_Date__c 
                                                                        FROM Shipment_Order__C 
                                                                        WHERE Final_Delivery_Date__C != null
                                                                        AND Id =: soIdsInTrackingTab
                                                                        order by Lastmodifieddate desc 
                                                                        LIMIT 1000]);
        

        Integer dateSum1 = 0;
        for(Shipment_Order__C soObj : soForDateAvgTrackingtab.values()){
            dateSum = dateSum + (soObj.Go_Live_Date__c.day() - soObj.Final_Delivery_Date__C.day());
        }

        if(soForDateAvgCompliancetab.values().size() > 0){
            avgTimeUntilDelivered = dateSum/ soForDateAvgCompliancetab.values().size();
        }

        
        DDBWrap.complianceTab = new List<DetailDashBoardSection>{ddbsCompliancePending, ddbsshipmentAwatingPkup,ddbsOnHoldShipments};
        DDBWrap.trackingTab = new List<DetailDashBoardSection>{ddbsDAP,ddbsE2E}; //, ddbsE2E,ddbsD2D
        DDBWrap.completeTab = new List<DetailDashBoardSection>{ddbsShipmentAwatingPOD}; //, ddbsE2E,ddbsD2D
        DDBWrap.descriptionCountSOOfEachTab = 'You Have '+noOfComplianceSO+' shipments in compliance ; '+noOfTrackingSO+' in Tracking and '+noOfCompleteSO+' in Delivered';
        
        DDBWrap.isAmUser = isAmUser;
        DDBWrap.iceUsersForFilters = iceUsersForFilters;

       
        DDBWrap.SOsInCompliancePending = noOfComplianceSO;
        DDBWrap.unresolvedTasksBTS = unresolvedTasksBTS;
        DDBWrap.unresolvedTasks = unresolvedTasks;
        DDBWrap.AvgTimeUntilApprovedToShip = AvgTimeUntilApprovedToShip;
        DDBWrap.SOsInTracking = noOfTrackingSO;
        DDBWrap.avgTimeUntilDelivered = avgTimeUntilDelivered;
        DDBWrap.countSOAwatingPOD = noOfCompleteSO;
        

        return DDBWrap;
    }
     
    public class  DetailDashBoardWrapper{
        @Auraenabled public List<DetailDashBoardSection> complianceTab;
        @Auraenabled public List<DetailDashBoardSection> trackingTab;
        @Auraenabled public List<DetailDashBoardSection> completeTab;
        @Auraenabled public List<Option> iceUsersForFilters;
        @Auraenabled public Boolean isAmUser;
   
        @AuraEnabled public String descriptionCountSOOfEachTab;
        
        @AuraEnabled public Integer SOsInCompliancePending;
        @AuraEnabled public Integer unresolvedTasksBTS;
        @AuraEnabled public Integer unresolvedTasks;
        @AuraEnabled public Integer AvgTimeUntilApprovedToShip;

        @AuraEnabled public Integer SOsInTracking;
        @AuraEnabled public Integer avgTimeUntilDelivered;
        
        @AuraEnabled public Integer countSOAwatingPOD;

       

        
    }
    public class DetailDashBoardSection{
        @Auraenabled  public  String sectionName;
        @Auraenabled  public  List<rowDetail> sectionRows = new List<rowDetail>();
        
        @Auraenabled public  Boolean showCPA = true;
        @Auraenabled public  Boolean showshippingstatus = true;
        @Auraenabled public  Boolean showshippingSubstatus = true;
        @Auraenabled public  Boolean showetaDays = true;
        @Auraenabled public  Boolean showUserOuntstandingTask = true;
        @Auraenabled public  Boolean showUserOutstandingTasksDNBATS = true;
        @Auraenabled public  Boolean showClientOutstandingTasks = true;
        @Auraenabled public  Boolean showComplianceProgress = true;
        @Auraenabled public  Boolean showSOLog = true;
        @Auraenabled public  Boolean showBusinessUnit = true;
        @Auraenabled public  Boolean showPickupScheduleTime = true;
        @Auraenabled public  Boolean showWhoDoesFreight = true;
        
        @Auraenabled public  Boolean showInternalShipmentType = true;
        @Auraenabled public  Boolean showShipmentValue = true;
        @Auraenabled public  Boolean showAM = true;
        @Auraenabled public  Boolean showICE = true;
        @Auraenabled public  Boolean showSOLogTime = true;
        
        @Auraenabled public  Boolean showTrackingLink = true;

        @Auraenabled public  Boolean showCountFdPODRec = true;
        @Auraenabled public  Boolean showCountTotalFDA = true;
        @Auraenabled public  String etaColname = 'ETA Until ATS';
        @AuraEnabled public Boolean showTrackingTerm = true;


    }
    public class rowDetail{
        @Auraenabled public Boolean showStar;
        @Auraenabled public  String client;
        @Auraenabled public  String clientId;
        @Auraenabled public  String internalShipmentType;
        @Auraenabled public  String name;
        @Auraenabled public  String Id;
        @Auraenabled public  String shipFrom;
        @Auraenabled public  String shipTo;
        @Auraenabled public  String CPA;
        @Auraenabled public  String CPAId;
        @Auraenabled public string subStatus;
        @Auraenabled public  Decimal shipmentValue;
        @Auraenabled public  String shipmentValueColour;
        @Auraenabled public  String am;
        @Auraenabled public  String amId;
        @Auraenabled public  String ice;
        @Auraenabled public  String iceId;
        @Auraenabled public  String etaDays;

        @Auraenabled public  String etaDaysColour = 'width:130px;';
        @Auraenabled public  String plusSignETADays;
        @Auraenabled public  String etasymbol;
        @Auraenabled public  String etaflag;
        @Auraenabled public  Decimal eta;
        @Auraenabled public  String whoDoesFreight; 
        @Auraenabled public  Decimal userOutstandingTasksBTS; 
        @Auraenabled public  Decimal userOutstandingTasksDNBATS; 
        @Auraenabled public  Decimal clientOutstandingTasks; 
        @Auraenabled public  DateTime pickupAvailabilityReady;
        @Auraenabled public  Boolean createPickup;
        @Auraenabled public  List<String> complianceProgress;
        @Auraenabled public  String soLog;
        @Auraenabled public  String businessUnit;
        @Auraenabled public  Date pickupScheduled;
        @Auraenabled public  String pickupScheduledDateColor = 'width:130px;';
        @Auraenabled public  Time pickupTime;
        @Auraenabled public  String mappedShippingStatus;
        @Auraenabled public  DateTime soLogTime;
        @Auraenabled public  String trackingLink;
        @Auraenabled public  Integer countFdPODRec;
        @Auraenabled public  Integer countTotalFDA;
        @Auraenabled public String trackingTerm; 
        @Auraenabled public String trackingLinkColor = 'color:#4a7dcc;'; 
        @Auraenabled public String complianceProgressHover;  
        @Auraenabled public List<String> stars;  
    }
    public class option{
        @Auraenabled public String label;
        @Auraenabled public String value;
    }
}