public class UTIL_GlobalVariableManifest
{
	public static Integer LeadLimit
	{
		get { return Integer.valueOf(UTIL_GlobalVariable.get()); }
		set { UTIL_GlobalVariable.put(String.valueOf(value)); }
	}
	
	private static final String VAR_FACTOR = 'Warn User at percentage';
	private static final Decimal DEF_FACTOR = 0.8; 

	public static Decimal WarningFactor
	{
		get { return Decimal.valueOf(UTIL_LeadWarn.get(VAR_FACTOR, String.valueOf(DEF_FACTOR))); }
		set { UTIL_LeadWarn.put(VAR_FACTOR, String.valueOf(value)); }
	}	
}