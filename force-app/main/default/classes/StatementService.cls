/**
 * Created by Caro on 2019-04-08.
 */

public with sharing class StatementService {

    public static DMN_Statement.StatementBody generateBody(Id customerId){

        DMN_Statement.StatementBody statementBody = new DMN_Statement.StatementBody();

        if (customerId == null) {
            return statementBody;
        }
        List<DMN_Statement.StatementLineItem> statementLineItems;

        statementLineItems = new List<DMN_Statement.StatementLineItem>();
        Set<Id> shipmentOrderIds = new Set<Id>();
        List<Customer_Invoice__c> customerInvoices = new DAL_CustomerInvoice().selectOpenInvoicesByCustomerId(customerId);
        for (Customer_Invoice__c customerInvoice : customerInvoices){
            shipmentOrderIds.add(customerInvoice.Shipment_Order__c);
        }
        Map<Id,Shipment_Order__c> relatedShipmentOrders = new Map<Id,Shipment_Order__c>();
        relatedShipmentOrders.putAll(new DAL_ShipmentOrder().selectById(shipmentOrderIds));
        for (Customer_Invoice__c customerInvoice : new DAL_CustomerInvoice().selectOpenInvoicesByCustomerId(customerId)){
            DMN_Statement.StatementLineItem invoiceItem = new DMN_Statement.StatementLineItem(
                    customerInvoice.Invoice_Sent_Date__c,
                    relatedShipmentOrders.get(customerInvoice.Shipment_Order__c).Name,
                    customerInvoice.Invoice_Type__c == 'Cash Outlay Invoice' ? '' : customerInvoice.Name,
                    customerInvoice.Client_PO_ReferenceNumber__c,
                    customerInvoice.Ship_To_Country__c,
                    customerInvoice.Invoice_Type__c,
                    customerInvoice.Due_Date__c,
                    customerInvoice.Invoice_Amount__c,
                    customerInvoice.BT__c
            );
            statementLineItems.add(invoiceItem);
        }

        statementBody.lineItems = statementLineItems;

        statementLineItems = new List<DMN_Statement.StatementLineItem>();

        for (Bank_transactions__c unappliedBankTransaction : new DAL_BankTransaction().selectUnappliedTransactionsByCustomerId(customerId)){
            DMN_Statement.StatementLineItem bankTransactionItem = new DMN_Statement.StatementLineItem(
                    unappliedBankTransaction.Date__c,
                    null,
                    unappliedBankTransaction.Name,
                    null,
                    null,
                    'Unapplied Payment',
                    null,
                    null,
                    -unappliedBankTransaction.Unapplied__c
            );
            statementLineItems.add(bankTransactionItem);
        }

        statementBody.lineItems.addAll(statementLineItems);

        statementBody.lineItems.sort();

        return statementBody;
    }

    public static Decimal generateUnappliedTotal(Id customerId) {
        return (Decimal) -(new DAL_BankTransaction().getTotalUnappliedShipmentAmountByCustomerId(customerId))+new DAL_CustomerInvoice().selectUnappliedCreditNotesTotalByCustomerId(customerId);
    }


    public static Map<String,String> generateHeader(){

        Map<String,String> statementHeader = new Map<String,String>();

        statementHeader.put('CompanyName','Tecex a division of VATit S.a.r.l');
        statementHeader.put('AddressStreet','14-16 Rue Phillippe II');
        statementHeader.put('AddressPostalCode','L-2340');
        statementHeader.put('Country','Luxembourg');
        statementHeader.put('MinimalCapital','Le montant du capital social: Euro 12 500');
        statementHeader.put('LLC','Sa Forme juridique: Societe a responsabilite limitee');
        statementHeader.put('VAT Number','LU 22944907');
        statementHeader.put('Reg No.','RCS Luxembourg RCS B136 565');

        return statementHeader;
    }

    public static  Map<String,String> generateFooter(){

        Map<String,String> statementFooter = new Map<String,String>();

        statementFooter.put('Wire transfers:','');
        statementFooter.put('US Details','');
        statementFooter.put('BANK:','Bank of the West');
        statementFooter.put('ROUTING #:','121100782');
        statementFooter.put('SWIFT #:','BWSTUS66');
        statementFooter.put('ACCOUNT:','046489191');
        statementFooter.put('ACCOUNT NAME:',' VATit USA INC');
        statementFooter.put('BANK ADDRESS:', '300 South Grand Avenue, 13th Floor Los Angeles, CA, 90071');
        statementFooter.put('PH#:','1-888-727-2692');


        return statementFooter;
    }

    public static List<DMN_Statement.AgingPeriod> generateAging(Id customerId){

        List<Customer_Invoice__c> customerInvoices = new DAL_CustomerInvoice().selectByCustomerId(new Set<Id>{customerId});
        for (Customer_Invoice__c customerInvoice : customerInvoices) {
            customerInvoice.Aging_At_Last_Statement__c = StatementService.calculateInvoiceAge(customerInvoice.Due_Date__c);
        }
        update customerInvoices;
        List<DMN_Statement.AgingPeriod> statementAging = new DAL_CustomerInvoice().getInvoiceAgingByCustomerId(customerId);
        Boolean outstandingAmount0 = false, outstandingAmount1 = false, outstandingAmount31 = false, outstandingAmount61 = false, outstandingAmount90  = false;
        for (DMN_Statement.AgingPeriod period : statementAging) {
            switch on period.periodLabel {
                when '0-Current' {
                    outstandingAmount0 = true;
                }
                when '1-30 Days' {
                    outstandingAmount1 = true;
                }
                when '31-60 Days' {
                    outstandingAmount31 = true;
                }
                when '61-90 Days' {
                    outstandingAmount61 = true;
                }
                when 'Over 90 Days' {
                    outstandingAmount90 = true;
                }
            }
        }
        if (!outstandingAmount0) {
            statementAging.add(new DMN_Statement.AgingPeriod('0-Current',0));
        }
        if (!outstandingAmount1) {
            statementAging.add(new DMN_Statement.AgingPeriod('1-30 Days',0));
        }
        if (!outstandingAmount31) {
            statementAging.add(new DMN_Statement.AgingPeriod('31-60 Days',0));
        }
        if (!outstandingAmount61) {
            statementAging.add(new DMN_Statement.AgingPeriod('61-90 Days',0));
        }
        if (!outstandingAmount90) {
            statementAging.add(new DMN_Statement.AgingPeriod('Over 90 Days',0));
        }

        statementAging.sort();

        return statementAging;
    }

    public static String calculateInvoiceAge(Date dueDate){

        String invoiceAgeText = '';
        if (dueDate == null) invoiceAgeText = 'Unable to Calculate Invoice Age';
        Integer InvoiceAge = dueDate.daysBetween(Date.today());
        if (InvoiceAge <= 0) invoiceAgeText = '0-Current';
        if (InvoiceAge > 0 && InvoiceAge <= 30) invoiceAgeText = '1-30 Days';
        if (InvoiceAge > 30 && InvoiceAge <= 60) invoiceAgeText = '31-60 Days';
        if (InvoiceAge > 60 && InvoiceAge <= 90) invoiceAgeText = '61-90 Days';
        if (InvoiceAge > 90) invoiceAgeText = 'Over 90 Days';

        return invoiceAgeText;
    }


    public static String getStatement (Id customerId, String customerName, String statementType) {

        String attachmentName = '';
        String statementId = '';
        if (statementType == 'General') {
            attachmentName= 'General_Statement_' + customerName + '_' + Date.today() + '.pdf';
        } else {
            attachmentName = 'Activity_Statement_' + customerName + '_' + Date.today() + '.pdf';
        }

        List<Attachment> existingStatements = new DAL_Attachment().selectByNameAndParent(attachmentName,customerId);

        if (existingStatements.size() > 0) {
            statementId = existingStatements[0].Id;
        }
        return statementId;
    }

    public static String attachStatement (Id customerId, String customerName, String statementType) {

        PageReference statementPdf = new PageReference(Site.getPathPrefix() + '/apex/CustomerStatement');
        statementPdf.getParameters().put('id', customerId);
        Blob pdfPageBlob = (Test.isRunningTest()) ?
                Blob.valueOf(String.valueOf('This is a test page')) : statementPdf.getContentAsPDF();
        Attachment statementAttachment = new Attachment();



        statementAttachment.Body = pdfPageBlob;


        if (statementType == 'General') {
            statementAttachment.Name = 'General_Statement_' + customerName + '_' + Date.today() + '.pdf';
        } else {
            statementAttachment.Name = 'Activity_Statement_' + customerName + '_' + Date.today() + '.pdf';
        }

        List<Attachment> existingStatements = new DAL_Attachment().selectByNameAndParent(statementAttachment.Name,customerId);

        delete existingStatements;

        statementAttachment.IsPrivate = false;

        statementAttachment.Body = pdfPageBlob;
        statementAttachment.ParentId = customerId;
        statementAttachment.Description = 'Customer Statement';
        insert statementAttachment;

        return statementAttachment.Id;
    }

    public static Boolean sendCustomerStatement (Id customerId, List<String> toAddress, String ccAddress, Id statementId) {

        Messaging.SingleEmailMessage email;

        Boolean isSent = false;

        if(toAddress.size()>0){

            List<EmailTemplate> emailTemplates = [
                    SELECT Subject, Body, HtmlValue
                    FROM EmailTemplate
                    WHERE DeveloperName = 'Statement_of_Account'];

            if(!emailTemplates.isEmpty()){
                email = new Messaging.SingleEmailMessage();

                EmailTemplate emailTemplate = emailTemplates[0];

                List<String> bodyMergeFields = new List<String>();
                List<Messaging.EmailFileAttachment> statementAttachments = new List<Messaging.EmailFileAttachment>();
                Messaging.EmailFileAttachment statementAttachment = new Messaging.EmailFileAttachment();

                Account client = new DAL_Account().selectById(customerId);

                Decimal statementTotal = StatementService.generateBody(customerId).statementTotal;

                bodyMergeFields.add(client.Name);
                bodyMergeFields.add(String.valueOf(Date.today()));
                bodyMergeFields.add(formatCurr(statementTotal));

                List<Attachment> fileAttachment = new DAL_Attachment().selectById(new Set<Id>{statementId});

                statementAttachment.setFileName(fileAttachment[0].Name);
                statementAttachment.setBody(fileAttachment[0].Body);
                statementAttachment.setContentType(fileAttachment[0].ContentType);

                statementAttachments.add(statementAttachment);

                String emailBodyHTML = String.format(emailTemplate.HtmlValue, bodyMergeFields);
                String emailBodyTxt = String.format(emailTemplate.Body, bodyMergeFields);

                email.setSubject(emailTemplate.Subject);
                email.setHtmlBody(emailBodyHTML);
                email.setPlainTextBody(emailBodyTxt);
                email.setToAddresses(toAddress);
                if (!String.isEmpty(ccAddress))
                email.setCcAddresses(new String[]{ccAddress});
                email.setFileAttachments(statementAttachments);

                List<Messaging.SendEmailResult> r = Messaging.sendEmail(new List<Messaging.SingleEmailMessage> {email});

                if (r[0].isSuccess()){
                    isSent = true;
                }
            }
        }
        return isSent;
    }

    static public string formatCurr(Decimal amount)
    {
        string regex = '(\\d)(?=(\\d{3})+(?!\\d))';
        Pattern objPt = Pattern.compile('(\\d)(?=(\\d{3})+(?!\\d))');
        Matcher regexMatcher = objPt.matcher(String.valueOf(amount));
        String formattedCur;
        if(regexMatcher.find()) {
            formattedCur = String.valueOf(amount).replaceAll(regex, '$1,');
        }
        return formattedCur;

    }
}