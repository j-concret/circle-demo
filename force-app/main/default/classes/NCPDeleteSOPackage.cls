@RestResource(urlMapping='/DeleteSOPackage/*')
Global class NCPDeleteSOPackage {
    
      @Httppost
    global static void NCPDeleteSOPackage(){
    
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        Blob body = req.requestBody;
    	res.addHeader('Content-Type', 'application/json');
        String requestString = body.toString();
        NCPDeleteSOPackagewra rw = (NCPDeleteSOPackagewra)JSON.deserialize(requestString,NCPDeleteSOPackagewra.class);
           try {
                Access_token__c At =[select status__c,Access_Token__c from Access_token__c where Access_Token__c=:rw.Accesstoken ];
            	if( at.status__C =='Active'){
                    List<ID>SOPIDs = new list<ID>();
                    list<Shipment_Order_Package__c> SOPs = new list<Shipment_Order_Package__c>();
                    For(Integer i=0;rw.sop.size()>i;i++){ SOPIDs.add(rw.sop[i].SOPID);}
          list<Shipment_Order_Package__c> SOP = [select id,Weight_Unit__c,Lithium_Batteries__c,Dimension_Unit__c,packages_of_same_weight_dims__c,Length__c,Height__c,Breadth__c,Actual_Weight__c from Shipment_Order_Package__c where id in:SOPIDs];
         
         For(Integer i=0;sop.size()>i;i++){ 
                      SOPs.add(SOP[i]);
             	   }
                        
            delete SOPs;
            
            JSONGenerator gen = JSON.createGenerator(true);
            gen.writeStartObject();
            
             gen.writeFieldName('Success');
             gen.writeStartObject();
            
             	if(SOPs.size()<1) {gen.writeNullField('Response');} else{gen.writeStringField('Response', 'Success-Shipment order packages  are deleted');}
            
            gen.writeEndObject();
            gen.writeEndObject();
            String jsonData = gen.getAsString();
            res.responseBody = Blob.valueOf(jsonData);
            res.addHeader('Content-Type', 'application/json');
            res.statusCode = 200;        
                    
                    
              	API_Log__c Al = New API_Log__c();
             //   Al.Account__c=rw.AccountID;
             //   Al.Login_Contact__c=rw.ContactID;
                Al.EndpointURLName__c='NCPDeleteSOPackage';
                Al.Response__c='Success- Shipment Order Package deleted';
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al; 
                }
            }
        	catch (Exception e){
                
              	String ErrorString ='Something went wrong while deleting Shipment order packages details, please contact support_SF@tecex.com';
               	res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
                 res.addHeader('Content-Type', 'application/json');
            	res.statusCode = 500;
                API_Log__c Al = New API_Log__c();
           //     Al.Account__c=rw.AccountID;
           //     Al.Login_Contact__c=rw.ContactID;
                Al.EndpointURLName__c='NCPDeleteSOPackage';
                Al.Response__c=e.getMessage()+' at line number: '+e.getLineNumber();
                Al.StatusCode__c=string.valueof(res.statusCode);
              	Insert Al;

            
        	}

        
        
    }

}