public class SOTravelCreation {
    
    public static Map<String,List<SO_Travel_History__c>> SoTravelMap = new Map<String,List<SO_Travel_History__c>>();
    
    public static void forRobotsChange(List<Shipment_Order__c> SOlist,Map<Id,String> DescriptionMap){
        for(Shipment_Order__c SO1: SOlist) {
            String approvalStatus = DescriptionMap.get(SO1.ID);
            List<SO_Travel_History__c> sothList = new List<SO_Travel_History__c>();
            if(SoTravelMap.containsKey(SO1.ID)){
                sothList = SoTravelMap.get(SO1.ID);
                
            }
            sothList.add( new SO_Travel_History__c (
                Source__c = 'SO Robots Changed',
                Date_Time__c =  System.now(),
                Location__c = '',
                Mapped_Status__C= 'Compliance Pending',
                SO_Shipping_Status__c  = SO1.Shipping_Status__c, 
                Description__c = 'All tasks within '+approvalStatus+' are completed. You will receive a separate notification when your goods are approved to  ship',
                Expected_Date_to_Next_Status__c=SO1.Expected_Date_to_Next_Status__c,
                Client_Notifications_Choice__c =SO1.Client_Contact_for_this_Shipment__r.Client_Notifications_Choice__c != null ? SO1.Client_Contact_for_this_Shipment__r.Client_Notifications_Choice__c : 'Not applicable',
                Major_Minor_notification__c = SO1.Client_Contact_for_this_Shipment__r.Major_Minor_notification__c != null ? SO1.Client_Contact_for_this_Shipment__r.Major_Minor_notification__c : '',
                Client_Tasks_notification_choice__c = SO1.Client_Contact_for_this_Shipment__r.Client_Tasks_notification_choice__c,
                Type_of_Update__c = 'MINOR',
                Approval_Channel__c = approvalStatus,
                TecEx_SO__c = SO1.ID
            ));
            SoTravelMap.put(SO1.ID,sothList);
            
            
        }
        
    }
    
    public static void forSOStatusChange(Map<Id,Shipment_Order__c> soMap,String Source){
        Map<String,String> CPAValuesMap = new Map<String,String>();
        Map<String,SO_Travel_History__c> SoTravelMap1 = new Map<String,SO_Travel_History__c>();
        String mappedStatus='',typeOfUpdate ='MAJOR';
        for(Shipment_Order__c SO1: soMap.values()) {
            CPAValuesMap.put(SO1.CPA_v2_0__c,SO1.Id);
            if(Source == 'SO Status Changed') {
                mappedStatus = 'Approved to Ship';
            }else{
                mappedStatus = 'Compliance Pending';
            }
            SoTravelMap1.put(SO1.ID,
                            new SO_Travel_History__c (
                                Source__c = Source,
                                Date_Time__c =  System.now(),
                                Location__c = '',
                                SO_Shipping_Status__c = SO1.Shipping_Status__c,
                                Mapped_Status__C= mappedStatus,
                                Description__c = 'NONE',
                                Expected_Date_to_Next_Status__c=SO1.Expected_Date_to_Next_Status__c,
                                Client_Notifications_Choice__c =SO1.Client_Contact_for_this_Shipment__r.Client_Notifications_Choice__c != null ? SO1.Client_Contact_for_this_Shipment__r.Client_Notifications_Choice__c : 'Not applicable',
                                Major_Minor_notification__c = SO1.Client_Contact_for_this_Shipment__r.Major_Minor_notification__c != null ? SO1.Client_Contact_for_this_Shipment__r.Major_Minor_notification__c : '',
                                Client_Tasks_notification_choice__c = SO1.Client_Contact_for_this_Shipment__r.Client_Tasks_notification_choice__c,
                                Type_of_Update__c = typeOfUpdate,
                                TecEx_SO__c = SO1.ID
                            )
                           );
            
        }
        
        for(SO_Status_Description__c SOSD : [SELECT Id, Name,Description_ETA__c,Expected_Days__C,Status__c,CPA_v2_0__c FROM SO_Status_Description__c WHERE CPA_v2_0__c IN :CPAValuesMap.keySet() AND Type__c = 'Shipping status']) {
            
            Shipment_Order__c shipmentOrder = soMap.get(CPAValuesMap.get(SOSD.CPA_v2_0__c));
            
            if(SOSD.Status__c == mappedStatus) {
                String des='';
                if(shipmentOrder.Ship_to_Country_new__c != null && shipmentOrder.Clearance_Destination__c != null){
                    des = (SOSD.Description_ETA__c).replace('[Ship to Country]',shipmentOrder.Ship_to_Country_new__c).replace('[Clearance Destination]',shipmentOrder.Clearance_Destination__c).replace('[Ship from Country]',shipmentOrder.Ship_From_Country__c );
                }
                
                SO_Travel_History__c soth = SoTravelMap1.get(shipmentOrder.Id);
                soth.Description__c = des;
                if(soth.Source__c == 'SO Creation')  {
                    soth.Expected_Date_to_Next_Status__c = datetime.now().adddays(integer.valueOf(SOSD.Expected_Days__C));
                }
                SoTravelMap1.put(shipmentOrder.Id,soth);
                shipmentOrder.Banner_Feed__c = des;
                shipmentOrder.Sub_Status_Update__c = 'NONE';
            }
            
        }
        
        for(Shipment_Order__c SO1: soMap.values()) {
		
              List<SO_Travel_History__c> sothList = new List<SO_Travel_History__c>();
            if(SoTravelMap.containsKey(SO1.Id)){
                sothList = SoTravelMap.get(SO1.Id);
                
            }
            sothList.add(SoTravelMap1.get(SO1.Id));
            SoTravelMap.put(SO1.Id,sothList);
		}
        
        
    }
    
    public static void forFDAStatusChange(List<Shipment_Order__c> SOlist,Map<Id,Final_Delivery__c> shipmentsWithFinalDelivery){
        
        
        For(Shipment_Order__C SO1: SOList){
            Final_Delivery__c finalDel = shipmentsWithFinalDelivery.get(SO1.ID);
            String description = '';
            if(finalDel.Delivery_Status__c == 'Awaiting POD' ) {
                description = 'The delivery to '+finalDel.Name+' has been completed. You will receive the proof of delivery as soon as it is available.';
            }else if(finalDel.Delivery_Status__c == 'POD Received') {
                description = 'The Proof of Delivery for '+finalDel.Name+' has been uploaded.';//Click View details below to see all details on the shipment page of the platform.';
            }
            List<SO_Travel_History__c> sothList = new List<SO_Travel_History__c>();
            if(SoTravelMap.containsKey(SO1.ID)){
                sothList = SoTravelMap.get(SO1.ID);
                
            }
            sothList.add( new SO_Travel_History__c (
                Source__c = 'FDA Status changed',
                Date_Time__c =  System.now(),
                Location__c = '',
                SO_Shipping_Status__c =(finalDel.Delivery_Status__c == 'Awaiting POD' || finalDel.Delivery_Status__c == 'POD Received' )? finalDel.Delivery_Status__c :  SO1.Shipping_Status__c,
                Mapped_Status__C= (finalDel.Delivery_Status__c == 'Awaiting POD' || finalDel.Delivery_Status__c == 'POD Received' )? 'Delivered' : SO1.Mapped_Shipping_status__c,
                Description__c = description,//'NONE',
                Expected_Date_to_Next_Status__c=SO1.Expected_Date_to_Next_Status__c,
                Client_Notifications_Choice__c =SO1.Client_Contact_for_this_Shipment__r.Client_Notifications_Choice__c != null ? SO1.Client_Contact_for_this_Shipment__r.Client_Notifications_Choice__c : 'Not applicable',
                Major_Minor_notification__c = SO1.Client_Contact_for_this_Shipment__r.Major_Minor_notification__c != null ? SO1.Client_Contact_for_this_Shipment__r.Major_Minor_notification__c : '',
                Client_Tasks_notification_choice__c = SO1.Client_Contact_for_this_Shipment__r.Client_Tasks_notification_choice__c,
                Type_of_Update__c = 'NONE',
                TecEx_SO__c = SO1.ID,
                FD_Id__c = finalDel.Id
            ));
            
            SoTravelMap.put(SO1.ID,sothList);
        }
        List<SO_Travel_History__c> sothList = new List<SO_Travel_History__c>();
        List<SO_Travel_History__c> completeList = new List<SO_Travel_History__c>();
        for(String key: SoTravelMap.keySet()){
            sothList = SoTravelMap.get(key);
            completeList.addAll(sothList);
        }
       // insert completeList;
       // SoTravelMap.clear();
        
    }
    
    public static Map<Id, Shipment_Order__c> forAllFDDeliveredStatusChange(List<Id> soIds){
        Map<Id, Shipment_Order__c> SOMap = new Map<Id, Shipment_Order__c>([Select Id,Service_Type__c,banner_feed__c,Ship_to_Country_new__c,Destination__c,Clearance_Destination__c,Client_Contact_for_this_Shipment__c,CPA_v2_0__c,Client_Contact_for_this_Shipment__r.Client_Notifications_Choice__c,Ship_From_Country__c, Shipping_Status__c FROM Shipment_Order__c
                                                                           WHERE Id IN: soIds]);
        Map<String,String> CPAValuesMap = new Map<String,String>();
        
        Map<String,SO_Travel_History__c> SoTravelMap1 = new Map<String,SO_Travel_History__c>();
        for(Shipment_Order__c SO1: SOMap.values()) {
            CPAValuesMap.put(SO1.CPA_v2_0__c,SO1.Id);
            SoTravelMap1.put(SO1.ID,
                             new SO_Travel_History__c (
                                 Source__c = 'Delivered Status Update',
                                 Date_Time__c =  System.now(),
                                 Location__c = '',
                                 Mapped_Status__C= 'Delivered',
                                 SO_Shipping_Status__c = SO1.Shipping_Status__c,
                                 Client_Notifications_Choice__c =SO1.Client_Contact_for_this_Shipment__r.Client_Notifications_Choice__c != null ? SO1.Client_Contact_for_this_Shipment__r.Client_Notifications_Choice__c : 'Not applicable',
                                 Type_of_Update__c = 'MAJOR',
                                 TecEx_SO__c = SO1.ID
                             )
                            );
        }
        For(SO_Status_Description__c SOSD : [SELECT Id , Name ,Description_ETA__c,CPA_v2_0__c
                                             FROM SO_Status_Description__c 
                                             where CPA_v2_0__r.Id in :CPAValuesMap.keySet()
                                             AND Type__c = 'Shipping status' AND Status__c='Delivered']){
                                                 String des='';
                                                 Shipment_Order__c shipmentOrder = soMap.get(CPAValuesMap.get(SOSD.CPA_v2_0__c));
                                                 if(shipmentOrder.Ship_to_Country_new__c != null && shipmentOrder.Clearance_Destination__c != null){
                                                    
                                                       des = (SOSD.Description_ETA__c).replace('[Ship to Country]',shipmentOrder.Ship_to_Country_new__c).replace('[Clearance Destination]',shipmentOrder.Clearance_Destination__c).replace('[Ship from Country]',shipmentOrder.Ship_From_Country__c );   
                                                    
                                                 }
                                                 
                                                 shipmentOrder.banner_feed__c = des;
                                                 shipmentOrder.Sub_Status_Update__c = 'NONE';
                                                 SO_Travel_History__c soth = SoTravelMap1.get(CPAValuesMap.get(SOSD.CPA_v2_0__c));
                                                 soth.Description__c = des;
                                             }
        
        for(Id soid: soIds){
            List<SO_Travel_History__c> sothList = new List<SO_Travel_History__c>();
            if(SoTravelMap.containsKey(soid)){
                sothList = SoTravelMap.get(soid);
                
            }
            sothList.add(SoTravelMap1.get(soid));
            SoTravelMap.put(soid,sothList);
        }
        return soMap;
        
        // insert SoTravelMap.values();
        //SoTravelMap.clear();
    }
    
    
    public static void forManualStatusChange(String source,String location,String description,Boolean StatusChanged,Boolean SubStatusChanged,Boolean textUpdate,String SOID){
        
        System.debug('SubStatusChanged '+SubStatusChanged);
        
        String typeOfUpdate = 'NONE';
        
        
        Shipment_Order__c shipmentOrder = [SELECT Id,Service_Type__c,Destination__c,Time_Shipment_Live__c,Time_CSE_Approved_Pack__c,Time_ICE_Submitted_to_Supplier__c,Time_Customs_Approved__c,Time_Client_Approved_to_Ship__c,Shipment_Date__c,
                                           Arrived_at_Hub__c,Cancelled_Date__c,Date_Departed_Hub__c,Arrived_in_Customs_Date__c,Customs_Cleared_Date__c,Final_Delivery_Date__c,POD_Date__c,Customs_Clearance_Docs_Received_Date__c,Ship_From_Country__c,  
                                           Shipping_Status__c,Sub_Status_Update__c ,banner_feed__c,Ship_to_Country_new__c, CPA_v2_0__c,Clearance_Destination__c,Client_Contact_for_this_Shipment__r.Client_Notifications_Choice__c,Client_Contact_for_this_Shipment__r.Major_Minor_notification__c,Client_Contact_for_this_Shipment__r.Client_Tasks_notification_choice__c ,Mapped_Shipping_status__c, Expected_Date_to_Next_Status__c FROM Shipment_Order__c WHERE Id = :SOID limit 1];
          String des  = '';
        if(shipmentOrder.Ship_to_Country_new__c != null && shipmentOrder.Clearance_Destination__c != null){
            
                                                       des = description.replace('[Ship to Country]',shipmentOrder.Ship_to_Country_new__c).replace('[Clearance Destination]',shipmentOrder.Clearance_Destination__c).replace('[Ship from Country]',shipmentOrder.Ship_From_Country__c );
                                                   
            
        }
        System.debug('Manual Status Updated '+SubStatusChanged);
        System.debug('Manual Status des'+(des == shipmentOrder.banner_feed__c));
        if(shipmentOrder.Mapped_Shipping_status__c == 'Compliance Pending' && des == shipmentOrder.banner_feed__c && !SubStatusChanged){
            System.debug('No Solog to send');
        }else{
            List<String> MajorStatusList = new List<String>{'In Transit to Hub','Arrived at Hub','In transit to country','Client Approved to ship','Awaiting POD','POD Received','Customs Clearance Docs Received','Cancelled - With fees/costs','Cancelled - No fees/costs'};
            List<String> MinorStatusList = new List<String>{'Arrived in Country, Awaiting Customs Clearance','Cleared Customs','Final Delivery in Progress'};
                if(StatusChanged){
                    if(MajorStatusList.contains(shipmentOrder.Shipping_Status__c )){
                        typeOfUpdate = 'MAJOR';
                    }else if(MinorStatusList.contains(shipmentOrder.Shipping_Status__c )){
                        typeOfUpdate = 'MINOR';
                    }else{
                        typeOfUpdate = 'NONE';
                    }
                    If(shipmentOrder.Shipping_status__c== 'CI, PL and DS Received' && shipmentOrder.Time_Shipment_Live__c ==null){shipmentOrder.Time_Shipment_Live__c=system.now();}else
                        if(shipmentOrder.Shipping_status__c== 'AM Approved CI,PL and DS\'s' && shipmentOrder.Time_CSE_Approved_Pack__c ==null){shipmentOrder.Time_CSE_Approved_Pack__c=system.now();}else
                            if(shipmentOrder.Shipping_status__c== 'Complete Shipment Pack Submitted for Approval' && shipmentOrder.Time_ICE_Submitted_to_Supplier__c ==null){shipmentOrder.Time_ICE_Submitted_to_Supplier__c=system.now();}else
                                if(shipmentOrder.Shipping_status__c== 'Customs Approved (Pending Payment)' && shipmentOrder.Time_Customs_Approved__c ==null){shipmentOrder.Time_Customs_Approved__c=system.now();}else
                                    if(shipmentOrder.Shipping_status__c== 'Client Approved to ship' && shipmentOrder.Time_Client_Approved_to_Ship__c ==null){shipmentOrder.Time_Client_Approved_to_Ship__c=system.now();}else
                                        if(shipmentOrder.Shipping_status__c== 'In Transit to Hub' && shipmentOrder.Shipment_Date__c ==null){shipmentOrder.Shipment_Date__c=system.today();}else
                                            if(shipmentOrder.Shipping_status__c== 'Arrived at Hub' && shipmentOrder.Arrived_at_Hub__c ==null){shipmentOrder.Arrived_at_Hub__c=system.today();}else
                                                if(shipmentOrder.Shipping_status__c== 'Cancelled - No fees/costs' && shipmentOrder.Cancelled_Date__c ==null){shipmentOrder.Cancelled_Date__c=system.today();}else
                                                    if(shipmentOrder.Shipping_status__c== 'In transit to country' && shipmentOrder.Date_Departed_Hub__c ==null){shipmentOrder.Date_Departed_Hub__c=system.today();}else
                                                        if(shipmentOrder.Shipping_status__c== 'Arrived in Country, Awaiting Customs Clearance' && shipmentOrder.Arrived_in_Customs_Date__c ==null){shipmentOrder.Arrived_in_Customs_Date__c=system.today();}else
                                                            if(shipmentOrder.Shipping_status__c== 'Cleared Customs' && shipmentOrder.Customs_Cleared_Date__c ==null){shipmentOrder.Customs_Cleared_Date__c=system.today();}else
                                                                if(shipmentOrder.Shipping_status__c== 'Awaiting POD' && shipmentOrder.Final_Delivery_Date__c ==null){shipmentOrder.Final_Delivery_Date__c=system.today();}else
                                                                    if(shipmentOrder.Shipping_status__c== 'POD Received' && shipmentOrder.POD_Date__c ==null){shipmentOrder.POD_Date__c=system.today();}else
                                                                        if(shipmentOrder.Shipping_status__c== 'Customs Clearance Docs Received' && shipmentOrder.Customs_Clearance_Docs_Received_Date__c ==null){shipmentOrder.Customs_Clearance_Docs_Received_Date__c=system.today();}   
                    
                    
                }
            if(SubStatusChanged){
            if(shipmentOrder.Sub_Status_Update__c == 'On Hold' || shipmentOrder.Sub_Status_Update__c == 'In Storage' ){
                typeOfUpdate = 'MAJOR';
            }else if(shipmentOrder.Sub_Status_Update__c == 'In Transit to Hub' 
                     || shipmentOrder.Sub_Status_Update__c == 'Arrived at Hub'  
                     || shipmentOrder.Sub_Status_Update__c == 'Customs Clearance has started' 
                     || shipmentOrder.Sub_Status_Update__c == 'Awaiting Final Delivery Confirmation'){
                typeOfUpdate = 'MINOR';
                     }
            }
            if(textUpdate){
                typeOfUpdate = 'MAJOR';
            }
         List<String> ExcludedShippingStatus = new List<String>{'POD Received','Customs Clearance Docs Received','In transit to country','Arrived in Country, Awaiting Customs Clearance','Cleared Customs','Final Delivery in Progress'};
      //  public static List<String> PastExcludedShippingStatus = new List<String>{};
        List<SO_Travel_History__c> SoLogs = [select Id FROM SO_Travel_History__c where TecEx_SO__c =: shipmentOrder.Id AND SO_Shipping_Status__c IN: ExcludedShippingStatus];
          System.debug('SoLogs '+JSON.serialize(SoLogs));
            if(SoLogs.isEmpty()  || shipmentOrder.Shipping_status__c != 'Client Approved to ship'){
       List<SO_Travel_History__c> sothList = new List<SO_Travel_History__c>();
            if(SoTravelMap.containsKey(shipmentOrder.ID)){
                sothList = SoTravelMap.get(shipmentOrder.ID);
                
            }
            sothList.add(new SO_Travel_History__c (
            
            Source__c = source,
            Date_Time__c = System.now(),
            Location__c = location,
            Mapped_Status__c = shipmentOrder.Mapped_Shipping_status__c,
            Description__c = des,
            Expected_Date_to_Next_Status__c = shipmentOrder.Expected_Date_to_Next_Status__c,
            Type_of_Update__c = typeOfUpdate,
            TecEx_SO__c = shipmentOrder.Id,
            SO_Shipping_Status__c = shipmentOrder.Shipping_Status__c,
            Sub_Status_Update__c = shipmentOrder.Sub_Status_Update__c,
            Client_Notifications_Choice__c =shipmentOrder.Client_Contact_for_this_Shipment__r.Client_Notifications_Choice__c != null ? shipmentOrder.Client_Contact_for_this_Shipment__r.Client_Notifications_Choice__c :'Not applicable',
            Major_Minor_notification__c = shipmentOrder.Client_Contact_for_this_Shipment__r.Major_Minor_notification__c != null ? shipmentOrder.Client_Contact_for_this_Shipment__r.Major_Minor_notification__c :'',
            Client_Tasks_notification_choice__c = shipmentOrder.Client_Contact_for_this_Shipment__r.Client_Tasks_notification_choice__c
        ));
        SoTravelMap.put(shipmentOrder.ID,sothList);
           }
        //insert soth;
        shipmentOrder.banner_feed__c = des;
        update shipmentOrder;
        }
    }
    
}