global class CalculateTaxes2{

@AuraEnabled
    webservice static void getTaxes(Id SO){
    system.debug('shipmentOrderId? : '+ SO);  
    
    
    shipment_Order__c shipmentOrder = [Select Id, Name, Ship_to_Country_new__c, Shipment_Value_USD__c,IOR_Price_List__r.Destination__c, Total_CIF_Duties__c, Total_FOB_Duties__c, Ship_to_Country__r.Clearance_Destination__c, Total_CIF_1_Duties__c from Shipment_Order__c where Id =: SO][0];
    
     
        //get country and hs code record
        List<Country__c> coutryList= [select Country__c, Name, CIF_Adjustment_Factor__c, CIF_Absolute_value_adjustment__c,
            (Select Amount__c,Tax_Type__c,Rate__c,Name ,Part_Specific__c, Max__c, Min__c, Order_Number__c,   
            Applied_to_Value__c,  Use_Total_From_Order_No__c, APPLIES_TO_ORDER__C, Additional_Percent__c   
            from Tax_Structure_Per_Country__r Order by Order_Number__c)
            from Country__c where Country__c =: shipmentOrder.Ship_to_Country__r.Clearance_Destination__c];
            
    
        List <Part__c>partList =[Select Id, Name, Rate__c, Total_Value__c,  Shipment_Order__c from Part__c 
        where Shipment_Order__c =: shipmentOrder.Id ];

      //System.debug('Did I get any countries back? : '+coutryList[0].HS_Codes_and_Associated_Details__r[0]);  
        System.debug('Ship to Country? : '+ shipmentOrder.Ship_to_Country__r.Clearance_Destination__c); 
        System.debug('Country : '+ coutryList[0].Country__c);
        System.debug('partList : '+ partList);          
        
        Integer taxStructureListSize = coutryList[0].Tax_Structure_Per_Country__r.size();
        
        //check tax structure
        List<Tax_Structure_Per_Country__c>  taxStr =coutryList[0].Tax_Structure_Per_Country__r;

        
        Double var1 =0;
        Double var2 =0;
        Double var3 =0;
        Double var4 =0;
        Double var5 =0;
        Double var6 =0;
        Double var7 =0;
        Double var8 =0;
        Double var9 =0;
        Double var10 =0;
        Double CIFValue = 0;
        Double FOBValue = 0;
                
        List<Tax_Calculation__c> taxCals = new List<Tax_Calculation__c>();
        
        for(Tax_Structure_Per_Country__c ts : coutryList[0].Tax_Structure_Per_Country__r){
        
        
        If(coutryList.size() > 0){
        
        Double appliesToOrderNumber = 0;
        
        String initalAppliesToOrder = ts.APPLIES_TO_ORDER__C;
         
               
        Double appliesToOrderValue = 0;
        
        Switch on ts.APPLIES_TO_ORDER__C{
               
           when '1,2,3,4'   { 
               
          appliesToOrderValue = var1+var2+var3+var4;
               
                         }
               
        when '1,2,3,4,5' {

        appliesToOrderValue = var1+var2+var3+var4+var5;
               
                         }
        when '1,2,3,4,6' {

        appliesToOrderValue = var1+var2+var3+var4+var6;
               
                         }
         when '1,2'      {

         appliesToOrderValue = var1+var2;
               
                         }
         when '1,3'      {

         appliesToOrderValue = var1+var3;
               
                         }
         when '2,3'      {

         appliesToOrderValue = var2+var3;
               
                         }
         when '2,3,4'      {

         appliesToOrderValue = var2+var3+var4;
               
                         }
         when '2'        {

         appliesToOrderValue = var2;
               
                         }
         when '1,2,3,4,5,7'{

         appliesToOrderValue = var1+var2+var3+var4+var5+var7;
               
                            }
         when '1,2,3'       {

         appliesToOrderValue = var1+var2+var3;
               
                            }
          when '3,4,5'      {

          appliesToOrderValue = var3+var4+var5;
             
                            }
          when '1'          {

          appliesToOrderValue = var1;
               
                             }
          when '1,2,3,5'     {

          appliesToOrderValue = var1+var2+var3+var5;
               
                             }  
          when '5'     {

          appliesToOrderValue = var5;
               
                             } 
          when '4'     {

          appliesToOrderValue = var4;
               
                             }
           when '1,2,3,4,5,6,7,8'     {

          appliesToOrderValue = var1+var2+var3+var4+var5+var6+var7+var8;
               
                             }
            when '2,3,4,5,6,7,8'     {

          appliesToOrderValue = var2+var3+var4+var5+var6+var7+var8;
               
                             }                   
          }
          
          
          CIFValue = (shipmentOrder.Shipment_Value_USD__c + coutryList[0].CIF_Absolute_value_adjustment__c) * (1 + (coutryList[0].CIF_Adjustment_Factor__c/100));
          FOBValue = shipmentOrder.Shipment_Value_USD__c;     
          
          Double appliedtoNumber = 0; 
          
          Double baseValue  = 0;
          
          

          
          String appliedToValue = ts.Applied_to_Value__c + ts.Part_Specific__c + (ts.Rate__c == null ? FALSE : TRUE)  +  (ts.Max__c == null ? FALSE : TRUE) +  (ts.Min__c == null ? FALSE : TRUE) + (ts.Amount__c == null ? FALSE : TRUE) +  (ts.APPLIES_TO_ORDER__C == null ? FALSE : TRUE) + (ts.Additional_Percent__c == null ? FALSE : TRUE) ;
               
               
          Switch on appliedToValue  {
          
          //cif and part specific
          
           when  'CIFtruefalsefalsefalsefalsefalsefalse'  {
                     
                 appliedtoNumber = shipmentOrder.Total_CIF_Duties__c;
                 baseValue = 0;
                        
                         }
                         
           //cif and has a rate              
                          
           when  'CIFfalsetruefalsefalsefalsefalsefalse' {
                     
                 appliedtoNumber =  CIFValue * ts.Rate__c/100 ; 
                 baseValue = CIFValue; 
                 
                          }
                          
            // cif and rate with applies to order              
           when  'CIFfalsetruefalsefalsefalsetruefalse'  {
           

                 appliedtoNumber =  (CIFValue + appliesToOrderValue )* ts.Rate__c/100; 
                 baseValue = (CIFValue + appliesToOrderValue );
                 
                          }
                          
            //cif and min and max plus rate
                          
          when  'CIFfalsetruetruetruefalsefalsefalse' {
                     
                 appliedtoNumber = (CIFValue * ts.Rate__c/100) < ts.Min__c ? ts.Min__c : ((CIFValue * ts.Rate__c/100) > ts.Max__c ? ts.Max__c :(CIFValue * ts.Rate__c/100))   ; 
                 baseValue = CIFValue;
                          }
                          
             // CIF and Amount            
          when  'CIFfalsefalsefalsefalsetruefalsefalse'  {
                     
                 appliedtoNumber = ts.Amount__c;
                 baseValue = CIFValue;
                         }
                         
           // CIF and Additional Percentage            
          when  'CIFfalsetruefalsefalsefalsefalsetrue'  {
                     
                 appliedtoNumber = (CIFValue + (CIFValue * ts.Additional_Percent__c))* ts.Rate__c/100 ;
                 baseValue = (CIFValue + (CIFValue * ts.Additional_Percent__c));
                         }
                         
                                     
           // CIF and Additional Percentage with Applied to Value           
          when  'CIFfalsetruefalsefalsefalsetruetrue'  {
                     
                 appliedtoNumber = (CIFValue + (CIFValue * ts.Additional_Percent__c) + appliesToOrderValue)* ts.Rate__c/100 ;
                 baseValue = (CIFValue + (CIFValue * ts.Additional_Percent__c));
                         }
                         
             // FOB and Part Specific
                         
           when 'FOBtruefalsefalsefalsefalsefalsefalse' {
                     
                 appliedtoNumber = shipmentOrder.Total_FOB_Duties__c ; 
                 baseValue = FOBValue;
                 
                         }
                         
           //FOB and has a rate              
                          
           when 'FOBfalsetruefalsefalsefalsefalsefalse' {
                    
                 appliedtoNumber = FOBValue* ts.Rate__c /100 ;
                 baseValue = FOBValue;
                 
                          }
                                        
                      
            //FOB and min and max plus rate
                          
          when  'FOBfalsetruetruetruefalsefalsefalsefalse' {
                     
                 appliedtoNumber =  ((FOBValue  * ts.Rate__c/100) < ts.Min__c ? ts.Min__c : ((FOBValue * ts.Rate__c/100) > ts.Max__c ? ts.Max__c :(FOBValue * ts.Rate__c/100))) ; 
                 baseValue = FOBValue;
                          }
                          
           // FOB and rate with applies to order              
           when  'FOBfalsetruefalsefalsefalsetruefalse' {
                     
                 appliedtoNumber =  (FOBValue + appliesToOrderValue) * ts.Rate__c/100; 
                 baseValue = FOBValue + appliesToOrderValue;
                          }
                          
            // FOB and Amount            
          when  'FOBfalsefalsefalsefalsetruefalsefalse' {
                     
                 appliedtoNumber = ts.Amount__c;
                 baseValue = FOBValue;
                         }
                         
          // FOB and Additional Percent and rate
                         
           when  'FOBfalsetruefalsefalsefalsefalsetrue' {
                     
                 appliedtoNumber = (FOBValue + (FOBValue * (ts.Additional_Percent__c/100))) * ts.Rate__c/100; 
                 baseValue = (FOBValue + (FOBValue * (ts.Additional_Percent__c/100)));        
                         }
                         
          // FOB and Additional Percent with Applies to Order
                         
           when  'FOBfalsetruefalsefalsefalsetruetrue' {
  
                 appliedtoNumber = (FOBValue + (FOBValue * (ts.Additional_Percent__c/100)) + appliesToOrderValue)  * ts.Rate__c/100 ; 
                 baseValue = (FOBValue + (FOBValue * (ts.Additional_Percent__c/100)) + appliesToOrderValue) ;        
                         }
                                             
                          
             //1 and has a rate              
                          
           when  '1falsetruefalsefalsefalsefalsefalse' {
                     
                 appliedtoNumber =  var1 * ts.Rate__c/100 ;
                 baseValue = var1; 
                 
                          }
                          
              //1 and has a rate and applied to order
                          
           when  '1falsetruefalsefalsefalsetruefalse' {
                     
                 appliedtoNumber =  (var1 + appliesToOrderValue) * ts.Rate__c/100 ;
                 baseValue = (var1 + appliesToOrderValue); 
                 
                          }
                          
           
                          
            //2 and has a rate                 
           when  '2falsetruefalsefalsefalsefalsefalse' {
                     
                 appliedtoNumber =  var2 * ts.Rate__c/100 ;
                 baseValue = var2; 
                          }
                          
            //2 and has a rate and applied to order
                          
           when  '2falsetruefalsefalsefalsetruefalse' {
                     
                 appliedtoNumber =  (var2 + appliesToOrderValue) * ts.Rate__c/100 ;
                 baseValue = (var2 + appliesToOrderValue); 
                 
                          }
                          
                          
           //3 and has a rate                 
           when  '3falsetruefalsefalsefalsefalsefalse' {
                     
                 appliedtoNumber =  var3 * ts.Rate__c/100 ;
                 baseValue = var3; 
                          }
          
           //3 and has a rate and applied to order
                          
           when  '3falsetruefalsefalsefalsetruefalse' {
                     
                 appliedtoNumber =  (var3 + appliesToOrderValue) * ts.Rate__c/100 ;
                 baseValue = (var3 + appliesToOrderValue); 
                 
                          }                
                          
            //4 and has a rate                 
           when  '4falsetruefalsefalsefalsefalsefalse' {
                     
                 appliedtoNumber =  var4 * ts.Rate__c/100 ;
                 baseValue = var4; 
                          }
                          
           //4 and has a rate and applied to order
                          
           when  '4falsetruefalsefalsefalsetruefalse' {
                     
                 appliedtoNumber =  (var4 + appliesToOrderValue) * ts.Rate__c/100 ;
                 baseValue = (var4 + appliesToOrderValue); 
                 
                          }                
            //5 and has a rate                 
           when  '5falsetruefalsefalsefalsefalsefalse' {
                     
                 appliedtoNumber =  var5 * ts.Rate__c/100 ;
                 baseValue = var5; 
                          }
           
            //5 and has a rate and applied to order
                          
           when  '5falsetruefalsefalsefalsetruefalse' {
                     
                 appliedtoNumber =  (var5 + appliesToOrderValue) * ts.Rate__c/100 ;
                 baseValue = (var5 + appliesToOrderValue); 
                 
                          }
                                         
            //6 and has a rate                 
           when  '6falsetruefalsefalsefalsefalsefalse' {
                     
                 appliedtoNumber =  var6 * ts.Rate__c/100 ;
                 baseValue = var6; 
                          }
                          
            //6 and has a rate and applied to order
                          
           when  '6falsetruefalsefalsefalsetruefalse' {
                     
                 appliedtoNumber =  (var6 + appliesToOrderValue) * ts.Rate__c/100 ;
                 baseValue = (var6 + appliesToOrderValue); 
                 
                          }               
                          
             //7 and has a rate                 
           when  '7falsetruefalsefalsefalsefalsefalse' {
                     
                 appliedtoNumber =  var7 * ts.Rate__c/100 ;
                 baseValue = var7; 
                          }
          
           //7 and has a rate and applied to order
                          
           when  '7falsetruefalsefalsefalsetruefalse' {
                     
                 appliedtoNumber =  (var7 + appliesToOrderValue) * ts.Rate__c/100 ;
                 baseValue = (var7 + appliesToOrderValue); 
                 
                          }
          
                         
            //8 and has a rate                 
           when  '8falsetruefalsefalsefalsefalsefalse' {
                     
                 appliedtoNumber =  var8 * ts.Rate__c/100 ;
                 baseValue = var8; 
                          }
            
           //8 and has a rate and applied to order
                          
           when  '8falsetruefalsefalsefalsetruefalse' {
                     
                 appliedtoNumber =  (var8 + appliesToOrderValue) * ts.Rate__c/100 ;
                 baseValue = (var8 + appliesToOrderValue); 
                 
                          }  
            
                          
             //9 and has a rate                 
           when  '9falsetruefalsefalsefalsefalsefalse' {
                     
                 appliedtoNumber =  var9 * ts.Rate__c/100 ;
                 baseValue = var9; 
                          }
         
          //9 and has a rate and applied to order
                          
           when  '9falsetruefalsefalsefalsetruefalse' {
                     
                 appliedtoNumber =  (var9 + appliesToOrderValue) * ts.Rate__c/100 ;
                 baseValue = (var9 + appliesToOrderValue); 
                 
                          }
                         
    }
    
     system.debug('appliedToValue : '+appliedToValue);
     system.debug('appliedtoNumber : '+appliedtoNumber);
     system.debug('shipmentOrderValue : '+shipmentOrder.Shipment_Value_USD__c);
     system.debug('appliesToOrderValue : '+appliesToOrderValue );
     system.debug('CIFValue : '+CIFValue );
     system.debug('FOBValue : '+FOBValue );
    
   
        
        
        If(ts.Order_Number__c == '1' ) {
              
               
               
               Tax_Calculation__c taxCal = new Tax_Calculation__c();   
               taxCal.Shipment_Order__c = shipmentOrder.Id;
               taxCal.Tax_Name__c  = ts.Name;
               taxCal.Value__c = appliedtoNumber; 
               taxCal.Order_Number__c = ts.Order_Number__c; 
               taxCal.Applied_to_Value_Type__c = ts.Applied_to_Value__c;
               taxCal.Rate__c = ts.Rate__c;
               taxCal.Applied_To_Value__c = baseValue; 
               taxCal.Tax_Type__c = ts.Tax_Type__c;
               taxCal.Part_Specific__c = ts.Part_Specific__c;
               taxCal.Applied_to_Order_Number__c = ts.Applies_to_Order__c;
               taxCals.add(taxCal);  
               var1 = taxCal.Value__c; 
               
               system.debug('appliedToValue : '+appliedToValue);
               system.debug('appliedtoNumber : '+appliedtoNumber); 
               system.debug('amount : '+ts.Amount__c); 
              
                             
            }  
            
            
            If(ts.Order_Number__c == '2'){
  
               Tax_Calculation__c taxCal = new Tax_Calculation__c();   
               taxCal.Shipment_Order__c = shipmentOrder.Id;
               taxCal.Tax_Name__c  = ts.Name;
               taxCal.Value__c = appliedtoNumber; 
               taxCal.Applied_to_Value_Type__c = ts.Applied_to_Value__c;
               taxCal.Order_Number__c = ts.Order_Number__c; 
               taxCal.Rate__c = ts.Rate__c;
               taxCal.Applied_To_Value__c = baseValue;
               taxCal.Tax_Type__c = ts.Tax_Type__c;
               taxCal.Part_Specific__c = ts.Part_Specific__c;
               taxCal.Applied_to_Order_Number__c = ts.Applies_to_Order__c; 
               taxCals.add(taxCal);  
               var2 = taxCal.Value__c;   
              
                             
            }  
            
            
            If(ts.Order_Number__c == '3'){
  
               Tax_Calculation__c taxCal = new Tax_Calculation__c();   
               taxCal.Shipment_Order__c = shipmentOrder.Id;
               taxCal.Tax_Name__c  = ts.Name;
               taxCal.Value__c = appliedtoNumber; 
               taxCal.Applied_to_Value_Type__c = ts.Applied_to_Value__c;
               taxCal.Order_Number__c = ts.Order_Number__c; 
               taxCal.Rate__c = ts.Rate__c;
               taxCal.Applied_To_Value__c = baseValue;
               taxCal.Tax_Type__c = ts.Tax_Type__c;
               taxCal.Part_Specific__c = ts.Part_Specific__c;
               taxCal.Applied_to_Order_Number__c = ts.Applies_to_Order__c; 
               taxCals.add(taxCal);  
               var3 = taxCal.Value__c;   
              
                             
            }  
            
            
             If(ts.Order_Number__c == '4'){
  
               Tax_Calculation__c taxCal = new Tax_Calculation__c();   
               taxCal.Shipment_Order__c = shipmentOrder.Id;
               taxCal.Tax_Name__c  = ts.Name;
               taxCal.Value__c = appliedtoNumber; 
               taxCal.Applied_to_Value_Type__c = ts.Applied_to_Value__c;
               taxCal.Order_Number__c = ts.Order_Number__c; 
               taxCal.Rate__c = ts.Rate__c;
               taxCal.Applied_To_Value__c = baseValue;
               taxCal.Tax_Type__c = ts.Tax_Type__c;
               taxCal.Part_Specific__c = ts.Part_Specific__c;
               taxCal.Applied_to_Order_Number__c = ts.Applies_to_Order__c; 
               taxCals.add(taxCal);  
               var4 = taxCal.Value__c;   
              
                             
            }  
            
            
             If(ts.Order_Number__c == '5'){
  
               Tax_Calculation__c taxCal = new Tax_Calculation__c();   
               taxCal.Shipment_Order__c = shipmentOrder.Id;
               taxCal.Tax_Name__c  = ts.Name;
               taxCal.Value__c = appliedtoNumber; 
               taxCal.Applied_to_Value_Type__c = ts.Applied_to_Value__c;
               taxCal.Order_Number__c = ts.Order_Number__c; 
               taxCal.Rate__c = ts.Rate__c;
               taxCal.Applied_To_Value__c = baseValue;
               taxCal.Tax_Type__c = ts.Tax_Type__c;
               taxCal.Part_Specific__c = ts.Part_Specific__c;
               taxCal.Applied_to_Order_Number__c = ts.Applies_to_Order__c; 
               taxCals.add(taxCal);  
               var5 = taxCal.Value__c;   
              
                             
            }  
            
             If(ts.Order_Number__c == '6'){
  
               Tax_Calculation__c taxCal = new Tax_Calculation__c();   
               taxCal.Shipment_Order__c = shipmentOrder.Id;
               taxCal.Tax_Name__c  = ts.Name;
               taxCal.Value__c = appliedtoNumber; 
               taxCal.Applied_to_Value_Type__c = ts.Applied_to_Value__c;
               taxCal.Order_Number__c = ts.Order_Number__c; 
               taxCal.Rate__c = ts.Rate__c;
               taxCal.Applied_To_Value__c = baseValue;
               taxCal.Tax_Type__c = ts.Tax_Type__c;
               taxCal.Part_Specific__c = ts.Part_Specific__c;
               taxCal.Applied_to_Order_Number__c = ts.Applies_to_Order__c; 
               taxCals.add(taxCal);  
               var6 = taxCal.Value__c;   
              
                             
            } 
            
            
             If(ts.Order_Number__c == '7'){
  
               Tax_Calculation__c taxCal = new Tax_Calculation__c();   
               taxCal.Shipment_Order__c = shipmentOrder.Id;
               taxCal.Tax_Name__c  = ts.Name;
               taxCal.Value__c = appliedtoNumber; 
               taxCal.Applied_to_Value_Type__c = ts.Applied_to_Value__c;
               taxCal.Order_Number__c = ts.Order_Number__c; 
               taxCal.Rate__c = ts.Rate__c;
               taxCal.Applied_To_Value__c = baseValue;
               taxCal.Tax_Type__c = ts.Tax_Type__c;
               taxCal.Part_Specific__c = ts.Part_Specific__c;
               taxCal.Applied_to_Order_Number__c = ts.Applies_to_Order__c; 
               taxCals.add(taxCal);  
               var7 = taxCal.Value__c;   
              
                             
            }  
    
    
               If(ts.Order_Number__c == '8'){
  
               Tax_Calculation__c taxCal = new Tax_Calculation__c();   
               taxCal.Shipment_Order__c = shipmentOrder.Id;
               taxCal.Tax_Name__c  = ts.Name;
               taxCal.Value__c = appliedtoNumber; 
               taxCal.Applied_to_Value_Type__c = ts.Applied_to_Value__c;
               taxCal.Order_Number__c = ts.Order_Number__c; 
               taxCal.Rate__c = ts.Rate__c;
               taxCal.Applied_To_Value__c = baseValue;
               taxCal.Tax_Type__c = ts.Tax_Type__c;
               taxCal.Part_Specific__c = ts.Part_Specific__c;
               taxCal.Applied_to_Order_Number__c = ts.Applies_to_Order__c; 
               taxCals.add(taxCal);  
               var8 = taxCal.Value__c;   
              
                             
            }  
   
   
    If(ts.Order_Number__c == '9'){
  
               Tax_Calculation__c taxCal = new Tax_Calculation__c();   
               taxCal.Shipment_Order__c = shipmentOrder.Id;
               taxCal.Tax_Name__c  = ts.Name;
               taxCal.Value__c = appliedtoNumber; 
               taxCal.Applied_to_Value_Type__c = ts.Applied_to_Value__c;
               taxCal.Order_Number__c = ts.Order_Number__c; 
               taxCal.Rate__c = ts.Rate__c;
               taxCal.Applied_To_Value__c = baseValue;
               taxCal.Tax_Type__c = ts.Tax_Type__c;
               taxCal.Part_Specific__c = ts.Part_Specific__c;
               taxCal.Applied_to_Order_Number__c = ts.Applies_to_Order__c; 
               taxCals.add(taxCal);  
               var9 = taxCal.Value__c;   
              
                             
            }    
            
     If(ts.Order_Number__c == '10'){
  
               Tax_Calculation__c taxCal = new Tax_Calculation__c();   
               taxCal.Shipment_Order__c = shipmentOrder.Id;
               taxCal.Tax_Name__c  = ts.Name;
               taxCal.Value__c = appliedtoNumber; 
               taxCal.Applied_to_Value_Type__c = ts.Applied_to_Value__c;
               taxCal.Order_Number__c = ts.Order_Number__c; 
               taxCal.Rate__c = ts.Rate__c;
               taxCal.Applied_To_Value__c = baseValue;
               taxCal.Tax_Type__c = ts.Tax_Type__c;
               taxCal.Part_Specific__c = ts.Part_Specific__c;
               taxCal.Applied_to_Order_Number__c = ts.Applies_to_Order__c; 
               taxCals.add(taxCal);  
               var10 = taxCal.Value__c;   
              
                             
            }    
        
}

}

     //If something else 
        //insert duties
       upsert taxCals;
       system.debug('tax cals: '+taxCals.size());

}

}