@RestResource(urlMapping='/NCPGetDefaults/*')
Global class NCPDefaultsGet5 {
    
@Httpget
   global static void NCPDefaultsGet5(){
        
        String jsonData='Test';
        RestRequest req = RestContext.request;
    	RestResponse res = RestContext.response;
        res.addHeader('Content-Type', 'application/json');
        String ClientID = req.params.get('ClientAccId');
     
        Id recordTypeId = Schema.SObjectType.Client_Address__c.getRecordTypeInfosByDeveloperName().get('PickUp').getRecordTypeId();
        Id FinrecordTypeId = Schema.SObjectType.Client_Address__c.getRecordTypeInfosByDeveloperName().get('Final_Destinations').getRecordTypeId();
        Account acc=[select id,Freight_preference__c from account where id =:ClientID];
        List<CPDefaults__c> CPD = [Select ID,Ship_from__c,Name,currency__c,Order_Type__c,Client_Account__c,Chargeable_Weight_Units__c,Li_ion_Batteries__c,Package_Dimensions__c,Package_Weight_Units__c,Second_hand_parts__c,Service_Type__c,TecEx_to_handle_freight__c from CPDefaults__c where Client_Account__c=:ClientID and Order_Type__c='Final Quote'];
        List<Client_Address__c> ABs = [select id,RecordType.ID,Name,Pickup_Preference__c,Address_Status__c,default__c,Comments__c,CompanyName__c,AdditionalContactNumber__c,Contact_Full_Name__c,Contact_Email__c,Contact_Phone_Number__c,Address__c,Address2__c,City__c,Province__c,Postal_Code__c,All_Countries__c,Client__c    from Client_Address__c where Client__c =:ClientID  and ( RecordType.ID =:recordTypeId OR RecordType.ID =:FinrecordTypeId) ORDER BY All_Countries__c desc];
           
        
        try {
        
             
             JSONGenerator gen = JSON.createGenerator(true);
             gen.writeStartObject();
             
             
         // If(!CPD.isEmpty()){
             //Creating Array & Object - Client Defaults
             gen.writeFieldName('ClientDefaultvalues');
             	gen.writeStartArray();
               If(!CPD.isEmpty()){
     		         for(CPDefaults__c CPD1 :CPD){
        				//Start of Object - SClient Defaults	
                         gen.writeStartObject();
                          
                          if(CPD1.Id == null) {gen.writeNullField('Id');} else{gen.writeStringField('Id', CPD1.Id);}
                          if(CPD1.Name == null) {gen.writeNullField('DefaultName');} else{gen.writeStringField('DefaultName', CPD1.Name);}
                          if(CPD1.Chargeable_Weight_Units__c == null) {gen.writeNullField('ChargeableWeightUnits');} else{gen.writeStringField('ChargeableWeightUnits', CPD1.Chargeable_Weight_Units__c);}
                          if(CPD1.Client_Account__c == null) {gen.writeNullField('ClientAccount');} else{gen.writeStringField('ClientAccount', CPD1.Client_Account__c);}
                          if(CPD1.Currency__c == null) {gen.writeNullField('Currency');} else{gen.writeStringField('Currency', CPD1.Currency__c);}
                          if(CPD1.Li_ion_Batteries__c == null) {gen.writeNullField('LiionBatteries');} else{gen.writeStringField('LiionBatteries', CPD1.Li_ion_Batteries__c);}
                          if(CPD1.Order_Type__c == null) {gen.writeNullField('OrderType');} else{gen.writeStringField('OrderType', CPD1.Order_Type__c);}
                          if(CPD1.Package_Dimensions__c == null) {gen.writeNullField('PackageDimensions');} else{gen.writeStringField('PackageDimensions', CPD1.Package_Dimensions__c);}
                          if(CPD1.Package_Weight_Units__c == null) {gen.writeNullField('PackageWeightUnits');} else{gen.writeStringField('PackageWeightUnits', CPD1.Package_Weight_Units__c);}
                          if(CPD1.Second_hand_parts__c == null) {gen.writeNullField('Secondhandparts');} else{gen.writeStringField('Secondhandparts', CPD1.Second_hand_parts__c);}
                          if(CPD1.Service_Type__c == null) {gen.writeNullField('ServiceType');} else{gen.writeStringField('ServiceType', CPD1.Service_Type__c);}
                         if(CPD1.TecEx_to_handle_freight__c == null) {gen.writeNullField('TecExtohandlefreight');} else{gen.writeStringField('TecExtohandlefreight', CPD1.TecEx_to_handle_freight__c);}
                         // if(Acc.Freight_preference__c == null) {gen.writeNullField('TecExtohandlefreight');} else{gen.writeStringField('TecExtohandlefreight', Acc.Freight_preference__c);}
                         if(CPD1.ship_From__c == null) {gen.writeNullField('ShipFromCountry');} else{gen.writeStringField('ShipFromCountry', CPD1.ship_From__c);}             
        				 if(CPD1.Currency__c == null) {gen.writeNullField('DefaultCurrency');} else{gen.writeStringField('DefaultCurrency', CPD1.currency__c);}             
        				
                         gen.writeEndObject();
                         //End of Object - Client Defaults
    					}
               }
    		gen.writeEndArray();
                //End of Array - Client Defaults 
            
             
             
             
             // If(!ABs.isEmpty()){
             //Creating Array & Object - PIckUp Address
             gen.writeFieldName('PickupAddress');
             	gen.writeStartArray();
            If(!ABs.isEmpty()){
     		         for(Client_Address__c ABS1 :ABs){
                         If(ABS1.RecordType.ID==recordTypeId){
        				//Start of Object - Pickup Address	
                         gen.writeStartObject();
                          
                          if(ABS1.Id == null) {gen.writeNullField('Id');} else{gen.writeStringField('Id', ABS1.Id);}
                          if(ABS1.Name == null) {gen.writeNullField('PickupaddressName');} else{gen.writeStringField('PickupaddressName', ABS1.Name);}
                          if(ABS1.default__c == null) {gen.writeNullField('Default');} else{gen.writeBooleanField('Default', ABS1.default__c);}
                          if(ABS1.Comments__c == null) {gen.writeNullField('Comments');} else{gen.writeStringField('Comments', ABS1.Comments__c);}
                          if(ABS1.CompanyName__c == null) {gen.writeNullField('CompanyName');} else{gen.writeStringField('CompanyName', ABS1.CompanyName__c);}
                          if(ABS1.AdditionalContactNumber__c == null) {gen.writeNullField('AdditionalContactNumber');} else{gen.writeStringField('AdditionalContactNumber', ABS1.AdditionalContactNumber__c);}
                          if(ABS1.Contact_Full_Name__c == null) {gen.writeNullField('ContactFullName');} else{gen.writeStringField('ContactFullName', ABS1.Contact_Full_Name__c);}
                          if(ABS1.Contact_Email__c == null) {gen.writeNullField('ContactEmail');} else{gen.writeStringField('ContactEmail', ABS1.Contact_Email__c);}
                          if(ABS1.Contact_Phone_Number__c == null) {gen.writeNullField('ContactPhoneNumber');} else{gen.writeStringField('ContactPhoneNumber', ABS1.Contact_Phone_Number__c);}
                          if(ABS1.Address__c == null) {gen.writeNullField('Address');} else{gen.writeStringField('Address', ABS1.Address__c);}
                          if(ABS1.Address2__c == null) {gen.writeNullField('Address2');} else{gen.writeStringField('Address2', ABS1.Address2__c);}
                          if(ABS1.City__c == null) {gen.writeNullField('City');} else{gen.writeStringField('City', ABS1.City__c);}
                          if(ABS1.Province__c == null) {gen.writeNullField('Province');} else{gen.writeStringField('Province', ABS1.Province__c);}
                          if(ABS1.Postal_Code__c == null) {gen.writeNullField('PostalCode');} else{gen.writeStringField('PostalCode', ABS1.Postal_Code__c);}
                          if(ABS1.All_Countries__c == null) {gen.writeNullField('Country');} else{gen.writeStringField('Country', ABS1.All_Countries__c);}
                          if(ABS1.Address_Status__c == null) {gen.writeNullField('AddressStatus');} else{gen.writeStringField('AddressStatus', ABS1.Address_Status__c);}
                          if(ABS1.Client__c == null) {gen.writeNullField('Client');} else{gen.writeStringField('Client', ABS1.Client__c);}
                          if(ABS1.Pickup_Preference__c == null) {gen.writeNullField('PickupPreference');} else{gen.writeStringField('PickupPreference', ABS1.Pickup_Preference__c);}
                          
                         
        					gen.writeEndObject();
                         //End of Object - Pickup Address
                         }
    					}
            }
    		gen.writeEndArray();
                //End of Array - Pickup Address 
                
                  
                  //Creating Array & Object - PIckUp Address
             gen.writeFieldName('FinalDestinationsAddress');
             	gen.writeStartArray();
            If(!ABs.isEmpty()){
     		         for(Client_Address__c ABS1 :ABs){
                         If(ABS1.RecordType.ID==FinrecordTypeId){
        				//Start of Object - Pickup Address	
                         gen.writeStartObject();
                          
                          if(ABS1.Id == null) {gen.writeNullField('Id');} else{gen.writeStringField('Id', ABS1.Id);}
                          if(ABS1.Name == null) {gen.writeNullField('PickupaddressName');} else{gen.writeStringField('PickupaddressName', ABS1.Name);}
                          if(ABS1.default__c == null) {gen.writeNullField('Default');} else{gen.writeBooleanField('Default', ABS1.default__c);}
                          if(ABS1.Comments__c == null) {gen.writeNullField('Comments');} else{gen.writeStringField('Comments', ABS1.Comments__c);}
                          if(ABS1.CompanyName__c == null) {gen.writeNullField('CompanyName');} else{gen.writeStringField('CompanyName', ABS1.CompanyName__c);}
                          if(ABS1.AdditionalContactNumber__c == null) {gen.writeNullField('AdditionalContactNumber');} else{gen.writeStringField('AdditionalContactNumber', ABS1.AdditionalContactNumber__c);}
                          if(ABS1.Contact_Full_Name__c == null) {gen.writeNullField('ContactFullName');} else{gen.writeStringField('ContactFullName', ABS1.Contact_Full_Name__c);}
                          if(ABS1.Contact_Email__c == null) {gen.writeNullField('ContactEmail');} else{gen.writeStringField('ContactEmail', ABS1.Contact_Email__c);}
                          if(ABS1.Contact_Phone_Number__c == null) {gen.writeNullField('ContactPhoneNumber');} else{gen.writeStringField('ContactPhoneNumber', ABS1.Contact_Phone_Number__c);}
                          if(ABS1.Address__c == null) {gen.writeNullField('Address');} else{gen.writeStringField('Address', ABS1.Address__c);}
                          if(ABS1.Address2__c == null) {gen.writeNullField('Address2');} else{gen.writeStringField('Address2', ABS1.Address2__c);}
                          if(ABS1.City__c == null) {gen.writeNullField('City');} else{gen.writeStringField('City', ABS1.City__c);}
                          if(ABS1.Province__c == null) {gen.writeNullField('Province');} else{gen.writeStringField('Province', ABS1.Province__c);}
                          if(ABS1.Postal_Code__c == null) {gen.writeNullField('PostalCode');} else{gen.writeStringField('PostalCode', ABS1.Postal_Code__c);}
                          if(ABS1.All_Countries__c == null) {gen.writeNullField('Country');} else{gen.writeStringField('Country', ABS1.All_Countries__c);}
                          if(ABS1.Address_Status__c == null) {gen.writeNullField('AddressStatus');} else{gen.writeStringField('AddressStatus', ABS1.Address_Status__c);}
                          if(ABS1.Client__c == null) {gen.writeNullField('Client');} else{gen.writeStringField('Client', ABS1.Client__c);}
                          if(ABS1.Pickup_Preference__c == null) {gen.writeNullField('PickupPreference');} else{gen.writeStringField('PickupPreference', ABS1.Pickup_Preference__c);}
                          
                         
        					gen.writeEndObject();
                         //End of Object - Pickup Address
                         }
    					}
            }
    		gen.writeEndArray();
                //End of Array - Pickup Address 
               
           
             
             
             
             gen.writeEndObject();
    		jsonData = gen.getAsString();
             
             
              res.responseBody = Blob.valueOf(jsonData);
              res.addHeader('Content-Type', 'application/json');
              res.statusCode = 200;
                
                API_Log__c Al = New API_Log__c();
                Al.Account__c=clientID;
                Al.EndpointURLName__c='GetDefaults';
                Al.Response__c='Success - Client Defaults are sent';
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al;
             
             
         
            }
        
        catch(exception e){
            System.debug('Line number ====> '+e.getLineNumber() + 'Exception Message =====> '+e.getMessage());
            
           		  String ErrorString ='Default values are not found';
               	  res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
               res.addHeader('Content-Type', 'application/json');
            	res.statusCode = 500;
                API_Log__c Al = New API_Log__c();
                Al.Account__c=ClientID;
                Al.EndpointURLName__c='GetDefaults';
                Al.Response__c=e.getMessage()+' at line number: '+e.getLineNumber();
                Al.StatusCode__c=string.valueof(res.statusCode);
              	Insert Al;
            
            
        }
   }

}