public class XF_Dragon_Request {
    // Field Maps
    private static map<String, String> fClientFieldMap; 
    public static map<String, String> ClientFieldMap {
            get {
                if (fClientFieldMap == null) 
                    fClientFieldMap = new map<String, String> { 
                        'Id' => 'SalesForceID', 
                        'Name' => 'ClientName',        
                        'Client_Code__c' => 'ClientCode',
                        'Portal_Name__c' => 'PortalName',
                        'Previous_Name__c' => 'PreviousName',
                        'Local_Name__c' => 'LocalName',
                        'Branch_PL__c' => 'BranchCode',
                        'Nature_of_Business__c' => 'NatureOfBusiness',
                        'VAT_Office__c' => 'VATOffice',
                        'VAT_Number__c' => 'VATNumber',
                        'Client_Status__c' => 'ClientStatus',
                        'Country__c' => 'CountryCode',
                        'Date_Signed__c' => 'DateSigned',
                        'CreatedDate' => 'ValidationDate', //Change this to Validation_Date__c when dragon 2 changes are done
                        'Registered_on_Portal__c' => 'RegisteredOnPortal'
                  };
                return fClientFieldMap; 
            } 
        } 
    private static map<String, String> fContactFieldMap; 
    public static map<String, String> ContactFieldMap {
        get {
            if (fContactFieldMap == null)
                fContactFieldMap = new map<String, String> {
                    'Id' => 'SFContactId',
                    'AccountId' => 'SFOwnerId',
                    'Contact_Role__c' => 'RelationshipTypeName',
                    'Salutation' => 'Salutation',
                    'FirstName' => 'Name',
                    'LastName' => 'Surname',
                    'Email' => 'Email',
                    'Phone' => 'Telephone',
                    'Fax' => 'Fax',
                    'MobilePhone' => 'Mobile'                   
                };
            return fContactFieldMap; 
        }
    } 
    private static map<String, String> fAddressFieldMap; 
    public static map<String, String> AddressFieldMap {
        get {
            if (fAddressFieldMap == null)
                fAddressFieldMap = new map<String, String> {
                    'Id' => 'SFAddressId',
                    'Client__c' => 'SFOwnerId',
                    'Type__c' => 'AddressType',
                    'Address__c' => 'Address1',
                    'City__c' => 'City',
                    'Postal_Code__c' => 'PostalCode',
                    'Province__c' => 'Province',
                    'Country__c' => 'Country'
                };
            return fAddressFieldMap; 
        }
    } 
    public static final String XML_DATE_TIME_FORMAT = 'yyyy-MM-dd\'T\'HH:mm:ss.SSS\'Z\'';
    public static final String XML_DATE_FORMAT = 'yyyy-MM-dd';
    
    private XmlStreamWriter X;
    
    public XF_Dragon_Request() {
        X = new XmlStreamWriter();  
    }
    
    private static final String NS_ENV = 'http://schemas.xmlsoap.org/soap/envelope/';  
    private static final String P_ENV = 'soapenv';  
    private static final String NS_WYV = 'http://vatit.com-dragon';  
    private static final String P_WYV = 'wyv';  

    private void beginDoc(String methodName, String rootElement) {
        X.writeStartDocument('utf-8', '1.0');
        X.writeStartElement(P_ENV, 'Envelope', NS_ENV);
        X.writeNamespace(P_ENV, NS_ENV);
        X.writeStartElement(P_ENV, 'Body', NS_ENV);
        X.writeNamespace('xsi', 'http://www.w3.org/2001/XMLSchema-instance');
        X.writeNamespace('xsd', 'http://www.w3.org/2001/XMLSchema');
        X.writeNamespace(P_WYV, NS_WYV);
        X.writeStartElement(P_WYV, methodName, NS_WYV);
        X.writeAttribute(P_ENV, NS_ENV, 'encodingStyle', 'http://schemas.xmlsoap.org/soap/encoding/');
        if (!SysUtils.empty(rootElement))
            writeStartElement(rootElement);
    }

    private String endDoc() {
        X.writeEndDocument();
        return X.getXmlString();        
    }   
    
    private void writeStartElement(String name) {
        X.writeStartElement(null, name, null);
    }
    
    private void writeEndElement() {
        X.writeEndElement();
    }
    
    private void writeElement(String name, String value, Boolean writeNullValue) {
        if (writeNullValue || !SysUtils.empty(value)) {
            X.writeStartElement(null, name, null);
            X.writeCharacters(SysUtils.getOrElse(value, ''));
            X.writeEndElement();
        }
    }
    
    private void writeElement(String name, String value) {
        writeElement(name, value, false);
    }
    
    private void writeElement(String name, Boolean value, Boolean writeNullValue) {
        writeElement(name, String.valueOf(value), writeNullValue);
    }
    private void writeElement(String name, Boolean value) {
        writeElement(name, value, false);
    }
    
    private void writeElements(map<String, String> fieldList, map<String, String> translation) {
        writeElements(fieldList, translation, false);   
    }
    
    private void writeElements(map<String, String> fieldList, map<String, String> translation, Boolean writeNullValues) {
        for (String field: fieldList.keySet()) {
            String translatedField = translation.get(field); 
            if (!SysUtils.empty(translatedField))
                writeElement(translatedField, fieldList.get(field), writeNullValues);     
        }
    }    
    
    private String deleteGeneric(list<ID> items, String methodName, String rootElement) {
        beginDoc(methodName, rootElement);
        for (ID item: items) {
            writeStartElement('Obj');
            writeElement('SFId', item);
            writeEndElement();
        }
        return endDoc();
    }
    
    public String updateClientEntity(list<SimpleObjectMap> items)
    {
        beginDoc('updateClientEntity', 'ClientEntity');
        for (SimpleObjectMap item: items) {
            try {
                //throw new ApplicationException('Test throwing an exception');
                writeStartElement('ClientCore');
                writeElement('SalesForceID', item.Id);      
                writeElements(item.Fields, ClientFieldMap);     
                writeEndElement();
                writeStartElement('OldClientCore');
                writeElements(item.OldFields, ClientFieldMap);      
                writeEndElement();
            } catch (Exception e) {
                SysUtils.processException(e, item.item);
                throw e;
            }
        }
        return endDoc();
    }
    
    public String insertClients(list<SimpleObjectMap> items)
    {
        beginDoc('insertClients', 'ClientEntity');
        for (SimpleObjectMap item: items) 
            try {
                writeStartElement('ClientCore');
                writeElements(item.Fields, ClientFieldMap);     
            } catch (Exception e) {
                SysUtils.processException(e, item.item);
                throw e;
            }
        return endDoc();
    }
    
    public String upsertContacts(list<SimpleObjectMap> items, Boolean isNew)
    {
        beginDoc('upsertClientContact', 'Contacts');
        for (SimpleObjectMap item: items) 
            try {
                writeStartElement('Contact');
                writeElement('Merge', isNew);  
                writeElements(item.Fields, ContactFieldMap);  
                writeEndElement();
            } catch (Exception e) {
                SysUtils.processException(e, item.item);
                throw e;
            }
        return endDoc();
    }
    
    public String upsertAddresses(list<SimpleObjectMap> items, Boolean isNew)
    {
        beginDoc('upsertAddress', 'Addresses');
        for (SimpleObjectMap item: items) {
            try {
                writeStartElement('Address');
                writeElement('Merge', isNew);  
                writeElements(item.Fields, AddressFieldMap);  
                writeEndElement();
            } catch (Exception e) {
                SysUtils.processException(e, item.item);
                throw e;
            }
        } 
        return endDoc();
    } 
    
    public String deleteContacts(list<ID> items)
    {
        return deleteGeneric(items, 'deleteClientContact', 'Contact');
    }
    
    public String deleteAddresses(list<ID> items)
    {
        return deleteGeneric(items, 'deleteClientAddress', 'Address');
    }
    
}