@isTest
public class FeedItem_TriggerV1_Test {
    static testMethod void  ProfileWithoutTecexCustomer(){
        
        Case caseObj = new Case(Subject='Test', Status = 'New');
        insert caseObj;
        FeedItem f = new FeedItem(Body= 'legal test',parentID = caseObj.Id);
        insert f;
        Task t = new Task(Subject='Donni',Status='Not Started',Priority='Normal');
        insert t;
        FeedItem f1 = new FeedItem(Body= 'legal test',parentID = t.Id);
        insert f1;
        
        
    }
    
    static testMethod void  ProfileWithTecexCustomer(){
        
        User admin = [SELECT Id, Username, UserRoleId FROM User WHERE Profile.Name = 'System Administrator' And IsActive=true LIMIT 1];
        User usr;
        System.runAs(admin) {
            Profile profileId = [SELECT Id FROM Profile WHERE Name = 'Tecex Customer' LIMIT 1];
            UserRole userrole = [Select Id, DeveloperName From UserRole Where DeveloperName = 'Head_Administrator' Limit 1];
            Account account = new Account(name='Acme1');
            insert account;
            
            Contact contact = new Contact ( lastname ='Testing Individual',  AccountId = account.Id); 
            insert contact;  
            
            usr = new User(LastName = 'LIVESTON',
                           FirstName='JASON',
                           Alias = 'jliv',
                           Email = 'jason.liveston@asdf.com',
                           Username = 'jason.liveston@asdf.com',
                           ProfileId = profileId.id,
                           ContactId = contact.Id,
                           TimeZoneSidKey = 'GMT',
                           LanguageLocaleKey = 'en_US',
                           EmailEncodingKey = 'UTF-8',
                           LocaleSidKey = 'en_US'
                          );
            insert usr;
        }
        Test.startTest();
        System.runAs(usr) {
            Case caseObj = new Case(Subject='Test', Status = 'New');
            insert caseObj;
            FeedItem f = new FeedItem(Body= 'legal test',parentID = caseObj.Id);
            insert f;
            Task t = new Task(Subject='Donni',Status='Not Started',Priority='Normal');
            insert t;
            FeedItem f1 = new FeedItem(Body= 'legal test',parentID = t.Id);
            insert f1;
        }
        Test.stopTest();
        
    }
    
    
    
}