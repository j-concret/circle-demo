public with sharing class SOCostEstimateController{


public List<Shipment_Order__c> shipmentOrder{get;set;}
public List<cShipmentOrder> costList {get; set;}
public Boolean hasSelAcct {get;set;}



public SOCostEstimateController(ApexPages.StandardController controller){
this.shipmentOrder= [ Select Id, Cost_Estimate_Number_HyperLink__c, Who_arranges_International_courier__c, Total_Invoice_Amount__c,  Shipping_Status__c, Shipment_Value_USD__c, Name, IOR_Price_List__c, Final_Delivery_Address__c, Delivery_Contact_Tel_number__c, 
Delivery_Contact_Name__c, Account__c, RecordType.Name, RecordTypeId FROM Shipment_Order__c
 where RecordTypeId = '0120Y0000009cEzQAI' limit 200];
    getCosts();
  
  }
 
  public List<cShipmentOrder> getCosts() {
     
        costList= new List<cShipmentOrder>();
        
        If( shipmentOrder != null)
        
        {
       
        
        for(Shipment_Order__c SO: [select Id, Cost_Estimate_Number_HyperLink__c, Who_arranges_International_courier__c,  Total_Invoice_Amount__c, Shipping_Status__c, Shipment_Value_USD__c, Name, IOR_Price_List__c, Final_Delivery_Address__c, Delivery_Contact_Tel_number__c, 
                                    Delivery_Contact_Name__c, Account__c, RecordType.Name, RecordTypeId 
                      FROM Shipment_Order__c WHERE RecordTypeId = '0120Y0000009cEzQAI' limit 200]) {
            // As each contact is processed we create a new cContact object and add it to the contactList
            costList.add(new cShipmentOrder (SO, FALSE));
        }
        return costList;
        
        }
        
        Else
        
        {
        
        return null;
        
        }
  }
  
  
 
 public PageReference ProcessSelectedEstimates() {

        //We create a new list of Contacts that we be populated only with Contacts if they are selected
        List<Shipment_Order__c> selectedCosts = new List<Shipment_Order__c>();

        //We will cycle through our list of cContacts and will check to see if the selected property is set to true, if it is we add the Contact to the selectedContacts list
        for(cShipmentOrder cCost: costList) {
            if(cCost.selected == true) {           
                selectedCosts.add(cCost.cSO);
            }
        }

        // Now we have our list of selected contacts and can perform any type of logic we want, sending emails, updating a field on the Contact, etc
        
        
        System.debug('These are the selected Contacts...'+ selectedCosts );
        
        List<Shipment_Order__c> c = new List<Shipment_Order__c>();
        for(Shipment_Order__c costs: selectedCosts) {
            costs.Shipping_Status__c = 'Shipment Pending';
            costs.RecordTypeId = '0120Y0000009cF0QAI';
            costs.Cost_Estimate_Accepted__c = TRUE;
            c.add(costs);            
        }
        
        try{
        
        update c;
        PageReference pageRef = new PageReference('/apex/SOCostEstimates');
        pageRef.setRedirect(FALSE);
        return pageRef;
       // costList =null; // we need this line if we performed a write operation  because getContacts gets a fresh list now
        
         }
        catch(Exception ex){
        ApexPages.addMessages(ex);
}
       
        
       
        
        return null;
    }
    
    
    public PageReference NewCostEstimates() {

      //PageReference leadPage = new PageReference('/apex/a0R/e?retURL=%2Fa0R%2Fo&RecordType=0120Y0000009cEz');
      PageReference leadPage = new PageReference('/a0R/e?retURL=https%3A%2F%2Ftecex--c.eu17.visual.force.com%2Fapex%2FSOCostEstimates&_CONFIRMATIONTOKEN=VmpFPSxNakF4T0Mwd09TMHlNMVF4TVRvek5EbzBPUzQxTkRKYSx4c3JWZm1YYU10ZzZtYnlLU3V2Mlg3LE5UUTRNemM1&common.udd.actions.ActionsUtilORIG_URI=%2Fa0R%2Fe&RecordType=0120Y0000009cEzQAI&0120Y0000009cEzQAI=a0R0D0000000g7g&ent=01I0Y000001M6ra&nooverride=1');
      leadPage.setRedirect(false);
      return leadPage;      
   }
        
        
     public PageReference NewCostEstimateFlow() {

      //PageReference leadPage = new PageReference('/apex/a0R/e?retURL=%2Fa0R%2Fo&RecordType=0120Y0000009cEz');
      PageReference flowPage= new PageReference('https://tecex--c.eu17.visual.force.com/flow/runtime.apexp?flowDevName=CostEstimate_Creation_for_Internal_Users');
      flowPage.setRedirect(false);
      return flowPage;      
   }
        
        public PageReference AcceptCostEstimate() {

      PageReference pageRef = new PageReference('/apex/acceptSOCostEstimatesTab');
      pageRef.setRedirect(true);
      return pageRef;
    
   }

 

  
 
        public class cShipmentOrder {
        public Shipment_Order__c cSO {get; set;}
        public Boolean selected {get; set;}
       
        public cShipmentOrder (Shipment_Order__c SO, Boolean sel) {
            cSO = SO;        
            selected = sel;
        }
    }
    
    


}