global class btNewMonthProcessSchedule implements Schedulable 
{
   global void execute(SchedulableContext SC) 
   {
      btNewMonthProcess UC = new btNewMonthProcess();
      database.executebatch(UC);

    }
}