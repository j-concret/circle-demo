@isTest(SeeAllData=true) 
private class TH_Account_BDD {
      
	// Check that all account fields are valid	
	static testMethod void testAccountChanged () 
	{
		
		Map<Account, Account> changes = new Map<Account, Account>();
		List<Account> items = Database.query(BDD_Utils.itemsQry('Account', XF_Dragon_Request.ClientFieldMap));
		Integer i = 0;
		for (Account item : items) {
			Account newItem = item.clone(true, false, true, true);
			if (i == 0) newItem.Last_API_Update__c = DateTime.now();
			newItem.Nature_of_Business__c = item.Nature_of_Business__c + ' modified by test';
			newItem.Referral_Effective_From__c = Date.today().addDays(1);
			newItem.Entity_Type__c = 'Division';
			newItem.Meeting_Coordinator__c = BDD_Utils.getRandomUserId();
			changes.put(item, newItem);
			i++;
		}	

		test.startTest();
		Test.setMock(HttpCalloutMock.class, new TX_Dragon_Mock());
		
		TH_Account handler = new TH_Account();
		
		handler.doAfterUpdate(changes);
		
		test.stopTest();		
	} 
	
	static testMethod void testAccountChangedError () {
		
		Map<Account, Account> changes = new Map<Account, Account>();
		List<Account> items = Database.query(BDD_Utils.itemsQry('Account', XF_Dragon_Request.ClientFieldMap));
		Integer i = 0;
		for (Account item : items) {
			Account newItem = item.clone(true, false, true, true);
			if (i == 0) newItem.Last_API_Update__c = DateTime.now();
			newItem.Nature_of_Business__c = item.Nature_of_Business__c + ' modified by test';
			newItem.Referral_Effective_From__c = Date.today().addDays(1);
			newItem.Entity_Type__c = 'Division';
			newItem.Meeting_Coordinator__c = BDD_Utils.getRandomUserId();
			changes.put(item, newItem);
			i++;
		}	

		test.startTest();
		Test.setMock(HttpCalloutMock.class, new TX_Dragon_Mock());
		
		TH_Account handler = new TH_Account();
		
		for (Account a : changes.keySet())
			changes.put(a, null);
		try { handler.doAfterUpdate(changes); } catch (Exception e) {} 
		
		test.stopTest();		
	} 
	
	static testMethod void testAccountAdded () {
		Test.setMock(HttpCalloutMock.class, new TX_Dragon_Mock());
		List<Account> items = Database.query(BDD_Utils.itemsQry('Account', XF_Dragon_Request.ClientFieldMap)); 
		if (items.size() > 0) items.get(0).Last_API_Update__c = DateTime.now();  
		if (items.size() > 1) items.get(1).Entity_Type__c = 'blah';  
		for (Account item : items) 
			item.BillingPostalCode = '1234';
		
		TH_Account handler = new TH_Account();
		
		test.startTest();
		
		handler.doAfterInsert(items);
		
		// Throws an exception when adding an address
		items.get(0).ID = null;
		try { handler.doAfterInsert(items); } catch (Exception e) {}
		// throws an exception when populating the change sets
		items.set(0, null);
		try { handler.doAfterInsert(items); } catch (Exception e) {}
		 
		test.stopTest();		
	}
}