@IsTest
public class CPPickUpaddCreationwrapper_Test {
    
    static testMethod void testParse() {
		String json = '{'+
		''+
		'  \"PickUpAddress\": ['+
		'    {'+
		'      '+
		'    \"Name\": \"Test\",'+
		'    \"Contact_Full_Name\": \"Test Full Name\",'+
		'    \"Contact_Email\": \"test@test.com\",'+
		'    \"Contact_Phone_Number\": \"3589624216545120\",'+
        '    \"AdditionalNumber\": \"3589624216545120\",'+
		'    \"Address1\": \"TestAddress1\",'+
		'    \"Address2\": \"TestAddress2\",'+
        '    \"CompanyName\": \"TestAddress2\",'+
        '    \"Comments\": \"TestAddress2\",'+
		'    \"City\": \"TestCity\",'+
		'    \"Province\": \"TestProvince\",'+
		'    \"Postal_Code\": \"1234\",'+
		'    \"All_Countries\": \"South Africa\",'+
		'    \"ClientID\": \"0010Y00000PEqvHQAT\"'+
		'     '+
		'    },'+
		'    {'+
		'      '+
		'    \"Name\": \"Test\",'+
		'    \"Contact_Full_Name\": \"Test Full Name\",'+
		'    \"Contact_Email\": \"test@test.com\",'+
		'    \"Contact_Phone_Number\": \"3589624216545120\",'+
        '    \"AdditionalNumber\": \"3589624216545120\",'+
		'    \"Address1\": \"TestAddress1\",'+
		'    \"Address2\": \"TestAddress2\",'+
        '    \"CompanyName\": \"TestAddress2\",'+
        '    \"Comments\": \"TestAddress2\",'+
		'    \"City\": \"TestCity\",'+
		'    \"Province\": \"TestProvince\",'+
		'    \"Postal_Code\": \"1234\",'+
		'    \"All_Countries\": \"South Africa\",'+
		'    \"ClientID\": \"0010Y00000PEqvHQAT\"'+
		'   '+
		'    },'+
		'    {'+
		'    '+
		'    \"Name\": \"Test\",'+
		'    \"Contact_Full_Name\": \"Test Full Name\",'+
		'    \"Contact_Email\": \"test@test.com\",'+
		'    \"Contact_Phone_Number\": \"3589624216545120\",'+
        '    \"AdditionalNumber\": \"3589624216545120\",'+
		'    \"Address1\": \"TestAddress1\",'+
		'    \"Address2\": \"TestAddress2\",'+
        '    \"CompanyName\": \"TestAddress2\",'+
        '    \"Comments\": \"TestAddress2\",'+
		'    \"City\": \"TestCity\",'+
		'    \"Province\": \"TestProvince\",'+
		'    \"Postal_Code\": \"1234\",'+
		'    \"All_Countries\": \"South Africa\",'+
		'    \"ClientID\": \"0010Y00000PEqvHQAT\"'+
		'     '+
		'    },'+
		'    {'+
		'     '+
		'    \"Name\": \"Test\",'+
		'    \"Contact_Full_Name\": \"Test Full Name\",'+
		'    \"Contact_Email\": \"test@test.com\",'+
		'    \"Contact_Phone_Number\": \"3589624216545120\",'+
        '    \"AdditionalNumber\": \"3589624216545120\",'+
		'    \"Address1\": \"TestAddress1\",'+
		'    \"Address2\": \"TestAddress2\",'+
        '    \"CompanyName\": \"TestAddress2\",'+
        '    \"Comments\": \"TestAddress2\",'+
		'    \"City\": \"TestCity\",'+
		'    \"Province\": \"TestProvince\",'+
		'    \"Postal_Code\": \"1234\",'+
		'    \"All_Countries\": \"South Africa\",'+
		'    \"ClientID\": \"0010Y00000PEqvHQAT\"'+
		'     '+
		'    }'+
		''+
		'  ]'+
		''+
		'}';
		CPPickUpaddCreationwrapper obj = CPPickUpaddCreationwrapper.parse(json);
		System.assert(obj != null);
	}
    
    

}