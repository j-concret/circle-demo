@isTest
global class MockHttpResponseGenerator implements HttpCalloutMock {
 // Implement this interface method
    global HTTPResponse respond(HTTPRequest req) {
        // Optionally, only send a mock response for a specific endpoint
        // and method.
       
        System.assertEquals('POST', req.getMethod());
        
      if (req.getEndpoint().endsWith('uploadBatch')) {
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type','multipart/form-data');
        res.setBody('{"example":"test"}');
        res.setStatusCode(200);
        return res;
        
            } else if (req.getEndpoint().endsWith('getBatchStatus')) {
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type','multipart/form-data');
        res.setBody('{"example":"test"}');
        res.setStatusCode(200);
        return res;
            } else {
                System.assert(false, 'unexpected endpoint ' + req.getEndpoint());
            }
        
        return null;

        
    }
    
}