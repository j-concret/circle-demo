@RestResource(urlMapping='/NCPUpdateTask/*')
Global class NCPUpdateTask {

    @Httppost
    global static void NCPUpdateTask(){
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        Blob body = req.requestBody;
        res.addHeader('Content-Type', 'application/json');
        String requestString = body.toString();
        NCPUpdateTaskWra rw  = (NCPUpdateTaskWra)JSON.deserialize(requestString,NCPUpdateTaskWra.class);
        try{

            Access_token__c at =[SELECT status__c,Access_Token__c from Access_token__c WHERE Access_Token__c=:rw.Accesstoken ];
            if( at.status__c =='Active' && rw.TaskId != null ) {

                TaskDetailPageCtrl.updateSfRecord(rw.TaskId,JSON.serialize(rw.data));

                JSONGenerator gen = JSON.createGenerator(true);
                gen.writeStartObject();

                gen.writeFieldName('Success');
                gen.writeStartObject();
                gen.writeStringField('Response', 'Success-Task Updated - '+rw.TaskId);

                gen.writeEndObject();
                gen.writeEndObject();
                String jsonData = gen.getAsString();
                res.responseBody = Blob.valueOf(jsonData);
                res.addHeader('Content-Type', 'application/json');
                res.statusCode = 200;

                API_Log__c Al = New API_Log__c();
                Al.EndpointURLName__c='NCP - NCPUpdateTasks';
                Al.Response__c=rw.TaskId + 'Success - Task is updated';
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al;
            }
        }
        catch(Exception e) {
            String ErrorString ='Something went wrong please contact sfsupport@tecex.com';
            res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
            res.addHeader('Content-Type', 'application/json');
            res.statusCode = 400;

            API_Log__c Al = New API_Log__c();
            // Al.Account__c=rw.AccountID;

            Al.EndpointURLName__c='NCP - NCPUpdateTasks';
            Al.Response__c=e.getMessage()+' at line number: '+e.getLineNumber();
            Al.StatusCode__c=string.valueof(res.statusCode);
            Insert Al;

        }




    }

}