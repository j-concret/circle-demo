public class CPCreatePartsWrapper {
    
    	public String ClientID;
        public String ContactID;
	
    public class Parts {
		public String SOID;
        public String PartNumber;
		public String PartDescription;
		public Integer Quantity;
		public Double UnitPrice;
		public String HSCode;
		public String CountryOfOrigin;
        public String Type_of_Goods;
		public String Li_ion_Batteries;
		public String Li_ion_BatteryTypes;        
		public String ECCNNo;
	}

	public List<Parts> Parts;

	
	public static CPCreatePartsWrapper parse(String json) {
		return (CPCreatePartsWrapper) System.JSON.deserialize(json, CPCreatePartsWrapper.class);
	}

}