@isTest
public class ZCPcreatenewregistration_Test {
    
    @testSetup
    public static void setup(){
        Account acc = new Account (name='TecEx Prospective Client', Type ='Client',New_Invoicing_Structure__c = true);
        insert acc;
        
        Access_token__c accessTokenObj = new Access_token__c();
        accessTokenObj.Status__c = 'Active';
        accessTokenObj.Access_Token__c = 'asdfghj-sdfgfds-sfdfgf';
        
        Insert accessTokenObj;
    }
    
    public static testMethod void testCase1(){
        Account Acc = [SELECT Id FROM Account where name='TecEx Prospective Client' LIMIT 1];
        Access_token__c accessToken = [SELECT Id,Access_Token__c FROM Access_token__c LIMIT 1];
        ZCPcreatenewregistrationwra obj = new ZCPcreatenewregistrationwra();
        obj.Accesstoken = accessToken.Access_Token__c;
        obj.Name = 'Reg1';
        obj.VATnumber = '987Hu9';
        obj.RegisteredAddress = 't-91';
        obj.RegisteredAddress2 = 'Test Colony';
        obj.RegisteredAddressCity = 'Test City';
        obj.RegisteredAddressProvince = 'U.P.';
        obj.RegisteredAddressPostalCode = '987654';
        obj.RegisteredAddressCountry = 'Australia';
        obj.Typeofregistration ='EU established';
        obj.FinancecontactEmail = 'abc@tec.com';
        obj.FinancecontactName = 'Name 1';
        obj.FinancecontactPhone = '8976754';
        obj.Verified = true;
        obj.Companyname = Acc.Id;
        obj.toCountry = 'Australia';
         Blob body = Blob.valueOf(JSON.serialize(obj));
        
        RestRequest req1 = new RestRequest();
        RestResponse res1 = new RestResponse();
        req1.requestURI = '/services/apexrest/CreateAccRegistration'; 
        req1.httpMethod = 'POST';
        req1.requestBody = Blob.valueOf(JSON.serialize(obj));     
        RestContext.request = req1;
        RestContext.response = res1;
        RestContext.request.addHeader('Content-Type', 'application/json');
        
        Test.startTest(); 
        ZCPcreatenewregistration.ZCPcreatenewregistration();
        Test.stopTest();
    }
    
     public static testMethod void testCase2(){
        Account Acc = [SELECT Id FROM Account where name='TecEx Prospective Client' LIMIT 1];
        ZCPcreatenewregistrationwra obj = new ZCPcreatenewregistrationwra();
        obj.Accesstoken = 'ABC';
        obj.Name = 'Reg1';
        obj.VATnumber = '987Hu9';
        obj.RegisteredAddress = 't-91';
        obj.RegisteredAddress2 = 'Test Colony';
        obj.RegisteredAddressCity = 'Test City';
        obj.RegisteredAddressProvince = 'U.P.';
        obj.RegisteredAddressPostalCode = '987654';
        obj.RegisteredAddressCountry = 'Australia';
        obj.Typeofregistration ='EU established';
        obj.FinancecontactEmail = 'abc@tec.com';
        obj.FinancecontactName = 'Name 1';
        obj.FinancecontactPhone = '8976754';
        obj.Verified = true;
        obj.Companyname = Acc.Id;
        obj.toCountry = 'Australia';
         Blob body = Blob.valueOf(JSON.serialize(obj));
        
        RestRequest req1 = new RestRequest();
        RestResponse res1 = new RestResponse();
        req1.requestURI = '/services/apexrest/CreateAccRegistration'; 
        req1.httpMethod = 'POST';
        req1.requestBody = Blob.valueOf(JSON.serialize(obj));     
        RestContext.request = req1;
        RestContext.response = res1;
        RestContext.request.addHeader('Content-Type', 'application/json');
        
        Test.startTest(); 
        ZCPcreatenewregistration.ZCPcreatenewregistration();
        Test.stopTest();
    }
}