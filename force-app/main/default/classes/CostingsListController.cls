public class CostingsListController {
    
    @AuraEnabled
    public static Map<String,Object> getCostings(Id ShipmentId){
        Map<String,Object> response = new Map<String,Object> {'status'=>'OK'};
            try {    
                Shipment_Order__c shipmentOrders = [SELECT Id ,Name, Shipment_Value_USD__c ,IOR_Refund_of_Tax_Duty__c,Recharge_Tax_and_Duty__c,CPA_v2_0__r.Related_Costing_CPA__c,
                                                    Taxes_Calculated__c, SupplierlU__c ,SupplierlU__r.Name, CPA_v2_0__c, of_packages__c,
                                                    of_Line_Items__c , Total_Line_Item_Extended_Value__c ,
                                                    Chargeable_Weight__c ,of_Unique_Line_Items__c, Final_Deliveries_New__c , 
                                                    (SELECT Supplier_Name__c,Amount__c,Cost_Type__c,Amended__c ,Applied_to__c ,Supplier__r.Name,Supplier__r.Id,Name,Cost_Category__c,
                                                     Variable_threshold__c,Rate__c,Currency__c,
                                                     Amount_in_USD__c,Cost_Note__c,Invoice_amount_USD_New__c 
                                                     FROM CPA_Costing__r) 
                                                    FROM Shipment_Order__c 
                                                    WHERE Id = :ShipmentId];
                response.put('shipmentOrders',shipmentOrders);
                
            } catch (Exception e) {
                response.put('status','ERROR');
                response.put('msg',e.getMessage());
            }
        
        return response; 
    }
    
    @AuraEnabled
    public static Map<String,Object> getPicklistValues(String objectName, String fieldName) {
        Map<String,Object> response = new Map<String,Object> {'status'=>'OK'};
            List<Map<String,String>> values = new List<Map<String,String>>();
        String[] types = new String[] {objectName};
            try {  
                Schema.DescribeSobjectResult[] results = Schema.describeSObjects(types);
                for(Schema.DescribeSobjectResult res : results) {
                    for (Schema.PicklistEntry entry : res.fields.getMap().get(fieldName).getDescribe().getPicklistValues()) {
                        if (entry.isActive()) {
                            values.add(new Map<String,String>{'label'=>entry.getValue(),'value'=>entry.getValue()});
                        }
                    }
                }
                response.put('statuses',values);
            } catch (Exception e) {
                response.put('status','ERROR');
                response.put('msg',e.getMessage());
            }
        
        return response;
    }
    
    
    @AuraEnabled
    public static Map<String,Object> updateCostingsList(List<CPA_Costing__c> costingsToUpdate){
        Map<String,Object> response = new Map<String,Object> {'status'=>'OK'};
            try {  
                Set<String> allCurrencies = new Set<String>();
				Map<String,Decimal> MapOfCurrencies = new Map<String,Decimal>();
                for(CPA_Costing__c cost:costingsToUpdate){
                    allCurrencies.add(cost.Currency__c);
                }
                for(Currency_Management2__c CM : [select Currency__C, Conversion_rate__C from Currency_Management2__c where Currency__C IN :allCurrencies ]){
                    MapOfCurrencies.put(CM.Currency__C,CM.Conversion_rate__C);
                }
                
                for(CPA_Costing__c CPAC:costingsToUpdate){
               
                    CPAC.Exchange_rate_forecast__c  = MapOfCurrencies.get(CPAC.Currency__c);
                
                }
                
                System.debug('costingsToUpdate ==> ');
                System.debug(JSON.serializePretty(costingsToUpdate));
                
                upsert costingsToUpdate;
                
            } catch (Exception e) {
                response.put('status','ERROR');
                response.put('msg',e.getMessage());
            }
        
        return response; 
    }
    
    
}