@IsTest
public class NCPGetProfilepic_Test {

    static testMethod void NCPGetProfilepicTest(){
        NCPGetProfilepicWra wrapper = new NCPGetProfilepicWra();
        NCPGetProfilepicWra.UserIds userids = new NCPGetProfilepicWra.UserIds();
        
        Access_token__c at = new Access_token__c(status__c = 'Active', Access_Token__c = '123');
        insert at;
        
        Profile profileId = [SELECT Id FROM Profile WHERE Name = 'Tecex Customer' LIMIT 1];
        
        Account account = new Account(name='Acme1');
        insert account;
        
        Contact contact = new Contact ( lastname ='Testing Individual',  AccountId = account.Id); 
        insert contact;  
        
        User usr = new User(LastName = 'LIVESTON',
                            FirstName='JASON',
                            Alias = 'jliv',
                            Email = 'jason.liveston@asdf.com',
                            Username = 'jason.liveston@asdf.com',
                            ProfileId = profileId.id,
                            ContactId = contact.Id,
                            TimeZoneSidKey = 'GMT',
                            LanguageLocaleKey = 'en_US',
                            EmailEncodingKey = 'UTF-8',
                            LocaleSidKey = 'en_US'
                           );
        insert usr;
        
        wrapper.Accesstoken = at.Access_Token__c;
		userids.UserId = usr.Id;
        wrapper.UserIds = new List<NCPGetProfilepicWra.UserIds>{userids};
            
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
        req.requestURI = '/services/apexrest/GetUserPicture'; 
        req.httpMethod = 'Post';
        req.addHeader('Content-Type', 'application/json');
        req.requestBody = Blob.valueof(JSON.serialize(wrapper));
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
        NCPGetProfilepic.NCPGetProfilepic();
        Test.stopTest();
		
    }
    
    static testMethod void NCPGetProfilepicCatchTest(){
        NCPGetProfilepicWra wrapper = new NCPGetProfilepicWra();
        NCPGetProfilepicWra.UserIds userids = new NCPGetProfilepicWra.UserIds();
        
        Access_token__c at = new Access_token__c(status__c = 'Active', Access_Token__c = '123');
        insert at;
        
        Profile profileId = [SELECT Id FROM Profile WHERE Name = 'Tecex Customer' LIMIT 1];
        
        Account account = new Account(name='Acme1');
        insert account;
        
        Contact contact = new Contact ( lastname ='Testing Individual',  AccountId = account.Id); 
        insert contact;  
        
        User usr = new User(LastName = 'LIVESTON',
                            FirstName='JASON',
                            Alias = 'jliv',
                            Email = 'jason.liveston@asdf.com',
                            Username = 'jason.liveston@asdf.com',
                            ProfileId = profileId.id,
                            ContactId = contact.Id,
                            TimeZoneSidKey = 'GMT',
                            LanguageLocaleKey = 'en_US',
                            EmailEncodingKey = 'UTF-8',
                            LocaleSidKey = 'en_US'
                           );
        insert usr;
        
        wrapper.Accesstoken = at.Access_Token__c;
		//userids.UserId = usr.Id;
        //wrapper.UserIds = new List<NCPGetProfilepicWra.UserIds>{userids};
            
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
        req.requestURI = '/services/apexrest/GetUserPicture'; 
        req.httpMethod = 'Post';
        req.addHeader('Content-Type', 'application/json');
        req.requestBody = Blob.valueof(JSON.serialize(wrapper));
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
        NCPGetProfilepic.NCPGetProfilepic();
        Test.stopTest();
		
    }
    
     static testMethod void NCPGetProfilepicNoUserIdTest(){
        NCPGetProfilepicWra wrapper = new NCPGetProfilepicWra();
        NCPGetProfilepicWra.UserIds userids = new NCPGetProfilepicWra.UserIds();
        
        Access_token__c at = new Access_token__c(status__c = 'Active', Access_Token__c = '123');
        insert at;
        
        Profile profileId = [SELECT Id FROM Profile WHERE Name = 'Tecex Customer' LIMIT 1];
        
        Account account = new Account(name='Acme1');
        insert account;
        
        Contact contact = new Contact ( lastname ='Testing Individual',  AccountId = account.Id); 
        insert contact;  
        
        wrapper.Accesstoken = at.Access_Token__c;
		userids.UserId = '';
        wrapper.UserIds = new List<NCPGetProfilepicWra.UserIds>();
            
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
        req.requestURI = '/services/apexrest/GetUserPicture'; 
        req.httpMethod = 'Post';
        req.addHeader('Content-Type', 'application/json');
        req.requestBody = Blob.valueof(JSON.serialize(wrapper));
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
        NCPGetProfilepic.NCPGetProfilepic();
        Test.stopTest();
		
    }
}