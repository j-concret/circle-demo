public class TestUtils 
{
    //Create new Leads and assign them to the test queue or user. Number created depends on the Limit. 
    //Integer argument determines how much more than the Limit will  be created (can be negative number)
    public static List<Lead> createLeadsToOwner(Integer howMuch, Id owner)
    {
        List<Lead> leads = DAL_Lead.createLeads(howMuch);
        DAL_Lead.changeOwnership(leads, owner);
        return leads;
    } 
}