@isTest
public class LookupFieldAndRelatedCtrl_Test {
    
    
    public static testMethod void fetchMatchingRecords_Test(){
        
        String additionalFilter = '{"RecordTypeId" : ["'+ Schema.SObjectType.Account.getRecordTypeInfosByName().get('Manufacturer').getRecordTypeId() +'"]}';
        
        Test.startTest();
        LookupFieldAndRelatedCtrl.fetchMatchingRecords('Test', 'Account', 'Name', 'Address__c', additionalFilter, 'Name','Internal Testing Account');
        Test.stopTest();
    }   
    
    public static testMethod void getSelectedRecord_Test(){
        
        String additionalFilter = '{"RecordTypeId" : ["'+ Schema.SObjectType.Account.getRecordTypeInfosByName().get('Manufacturer').getRecordTypeId() +'"]}';
        Test.startTest();
        LookupFieldAndRelatedCtrl.getSelectedRecord('Internal Testing Account', 'Account', 'Name', 'Address__c', additionalFilter,'','');
        Test.stopTest();
    }
    
    

}