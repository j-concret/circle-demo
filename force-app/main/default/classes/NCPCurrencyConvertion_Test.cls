@IsTest
public class NCPCurrencyConvertion_Test {

    static testMethod void testPostMethod(){
        Access_token__c accessTokenObj = new Access_token__c();
        accessTokenObj.Status__c = 'Active';
        accessTokenObj.Access_Token__c = 'asdfghj-sdfgfds-sfdfgf';
        
        Insert accessTokenObj;
        
        Account acc = new Account(Name = 'Testing',type = 'Supplier',CSE_IOR__c = '0050Y000001LTZO', 
                                       Service_Manager__c = '0050Y000001LTZO', 
                                       Financial_Manager__c='0050Y000001LTZO', 
                                       Financial_Controller__c='0050Y000001LTZO');
        insert acc;
        
        Contact con = new Contact(FirstName = 'Testing',LastName = 'contact',AccountId = acc.Id);
        insert con;
        
        Currency_Management2__c currencyMgmntObj = new Currency_Management2__c();
        currencyMgmntObj.Conversion_Rate__c = 0.01;
        currencyMgmntObj.Currency__c = 'Indian Rupee (INR)';
        currencyMgmntObj.ISO_Code__c = 'INR';
        currencyMgmntObj.Name = 'Test Currency';
        insert currencyMgmntObj;
        
        NCPCurrencyConvertionWra wrapperObj = new NCPCurrencyConvertionWra();
        wrapperObj.Accesstoken = accessTokenObj.Access_Token__c;
        wrapperObj.AccountID = acc.Id;
        wrapperObj.ContactID = con.Id;
        wrapperObj.Curr = 'INR';
        
        RestRequest request = new RestRequest();
        RestResponse res = new RestResponse();
        request.requestUri ='/services/apexrest/api/CurrencyConvertion/';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueOf(JSON.serialize(wrapperObj));
        RestContext.request = request;
        RestContext.response = res;
        
        Test.startTest();
        NCPCurrencyConvertion.NCPCurrencyConvertion();
        Test.stopTest();
    }
    
    static testMethod void testPostMethod2(){
        Access_token__c accessTokenObj = new Access_token__c();
        accessTokenObj.Status__c = 'Active';
        accessTokenObj.Access_Token__c = 'asdfghj-sdfgfds-sfdfgf';
        
        Insert accessTokenObj;
        
        Account acc = new Account(Name = 'Testing',type = 'Supplier',CSE_IOR__c = '0050Y000001LTZO', 
                                       Service_Manager__c = '0050Y000001LTZO', 
                                       Financial_Manager__c='0050Y000001LTZO', 
                                       Financial_Controller__c='0050Y000001LTZO');
        insert acc;
        
        Contact con = new Contact(FirstName = 'Testing',LastName = 'contact',AccountId = acc.Id);
        insert con;
        
        Currency_Management2__c currencyMgmntObj = new Currency_Management2__c();
        currencyMgmntObj.Conversion_Rate__c = 0.01;
        currencyMgmntObj.Currency__c = 'Indian Rupee (INR)';
        currencyMgmntObj.ISO_Code__c = 'INR';
        currencyMgmntObj.Name = 'Test Currency';
        insert currencyMgmntObj;
        
        NCPCurrencyConvertionWra wrapperObj = new NCPCurrencyConvertionWra();
        wrapperObj.Accesstoken = accessTokenObj.Access_Token__c;
        wrapperObj.AccountID = acc.Id;
        wrapperObj.ContactID = con.Id;
        wrapperObj.Curr = '';
        
        RestRequest request = new RestRequest();
        RestResponse res = new RestResponse();
        request.requestUri ='/services/apexrest/api/CurrencyConvertion/';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueOf(JSON.serialize(wrapperObj));
        RestContext.request = request;
        RestContext.response = res;
        
        Test.startTest();
        NCPCurrencyConvertion.NCPCurrencyConvertion();
        Test.stopTest();
    }
}