@isTest
public class BankAccountCustomLookUpControllerTest {
    @testSetup static void setup() {
        
        Account account2 = new Account (name='Test Manufacturer', Type ='Supplier'); 
        insert account2;
        
    }
    
    @isTest static void testFetchLookUpValues() {
        Test.startTest();
        List<Account> accList = BankAccountCustomLookUpController.fetchLookUpValues('Test','Account');
        Test.stopTest();
        System.assert((accList[0].Name).containsIgnoreCase('Test'));
    }
}