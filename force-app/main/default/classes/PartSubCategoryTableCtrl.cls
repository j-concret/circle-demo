public with sharing class PartSubCategoryTableCtrl {

    @AuraEnabled
    public static Map<String,Object> getParts(String soId,String tskId){
        try{
            Map<String,Object> response = new Map<String,Object>();
            List<Id> partIds = new List<Id>();
            String query = 'SELECT '+String.join(new List<String>(Shipment_order__c.sObjectType.getDescribe().fields.getMap().keySet()),',');

            query += ',Account__r.'+String.join(new List<String>(Account.sObjectType.getDescribe().fields.getMap().keySet()), ',Account__r.');
            /* query += ',CPA_v2_0__r.'+String.join(new List<String>(CPA_v2_0__c.sObjectType.getDescribe().fields.getMap().keySet()), ',CPA_v2_0__r.'); */

            query += ',CPA_v2_0__r.Country__r.'+String.join(new List<String>(Country__c.sObjectType.getDescribe().fields.getMap().keySet()), ',CPA_v2_0__r.Country__r.');

            query += ',(SELECT '+ String.join(new List<String>(Part__c.sObjectType.getDescribe().fields.getMap().keySet()),',') +' FROM Parts__r)';
            query += ' FROM Shipment_Order__c WHERE Id=:soId';

            Shipment_order__c sobj = Database.query(query);

            Task task = [SELECT Id,Master_Task__c,Master_Task__r.Condition_Fields_API_Name__c,Master_Task__r.Condition__c FROM Task WHERE Id=:tskId];

            if(String.isNotBlank(task.Master_Task__r.Condition__c) && String.isNotBlank(task.Master_Task__r.Condition_Fields_API_Name__c)) {
              String relationshipName;
              Map<String,Object> variables = new Map<String,Object>();
              List<String> fields = task.Master_Task__r.Condition_Fields_API_Name__c.split(',');
              String ex = task.Master_Task__r.Condition__c;
              if(ex.contains('\'')) {
                  ex = ex.replaceAll('\'','\'\'');
              }
              String expression = String.format(ex,fields);

              for(String field : fields) {
                  field = field.trim();
                  Object val = null;
                  if(field.startsWithIgnoreCase('Country__c')) {
                      SObject cpaObj = sobj.getSObject('CPA_v2_0__r');
                      SObject countryObj = cpaObj == null ? null : cpaObj.getSObject('Country__r');
                      val = countryObj == null ? null :  getFieldValue(countryObj, field);
                  }
                  else
                      val = getFieldValue(sobj,field);
                  variables.put(field,val);
              }

              List<SObject> relatedList;
              if(task.Master_Task__r.Condition_Fields_API_Name__c.containsIgnoreCase('Part__c') )
                  relatedList = sobj.getSObjects('Parts__r');


              if(relatedList != null) {
                  for(SObject relatedObj : relatedList) {
                      Map<String,Object> vars = new Map<String,Object>(variables);
                      for(String field : fields) {
                          if(!vars.containsKey(field) || (vars.containsKey(field) && vars.get(field) == null)) {
                              Object val = getFieldValue(relatedObj, field);
                              vars.put(field,val);
                          }
                      }
                      Boolean result = Boolean.valueOf(ScriptEngine.getInstance().eval(expression, vars));
                      if(result) {
                        partIds.add(relatedObj.Id);
                      }
                  }
              }
            }

            if(partIds.isEmpty()) throw new CustomException('No Line items.');

            List<Part__c> parts = [SELECT Id,Name,Description_and_Functionality__c,Client_Provided_Category__c FROM Part__c WHERE Shipment_Order__c =: soid AND Id IN: partIds];
            response.put('parts',parts);
            response.put('options',getPicklistValues('Part__c','Client_Provided_Category__c'));
            return response;
        }catch(Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    @AuraEnabled
    public static void updatePartCategory(List<Part__c> parts){
        try {
            update parts;
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    public static List<Map<String,String> > getPicklistValues(String object_name, String field_name) {

        List<Map<String,String> > values = new List<Map<String,String> >();
        String[] types = new String[] {object_name};
        Schema.DescribeSobjectResult[] results = Schema.describeSObjects(types);
        for(Schema.DescribeSobjectResult res : results) {
            for (Schema.PicklistEntry entry : res.fields.getMap().get(field_name).getDescribe().getPicklistValues()) {
                if (entry.isActive()) {
                    values.add(new Map<String,String> {'label'=>entry.getLabel(),'value'=>entry.getValue()});
                }
            }
        }
        return values;
    }

    private static Object getFieldValue(Sobject sobj,String fieldName){

      List<String> field = fieldName.split('\\.');
      String objName = sobj.getSObjectType().getDescribe().getName();
      //List<String> objects = new List<String> {'shipment_order__c','freight__c','country__c','part__c','shipment_order_package__c'};//values in lowercase
      Map<String,String> relatedObjects = new Map<String,String> {'account'=>'Account__r','cpa_v2_0__c'=>'CPA_v2_0__r'};//keys in lowercase

      try{
          if(sobj.isSet(field[1].trim().toLowerCase()) && objName.equalsIgnoreCase(field[0].trim())) {
              return sobj.get(field[1].trim());
          }
      }catch(Exception e) {}
      if(objName == 'Shipment_Order__c' && relatedObjects.containsKey(field[0].trim().toLowerCase())) {
          SObject obj = sobj.getSObject(relatedObjects.get(field[0].trim().toLowerCase()));
          return obj== null ? null : obj.get(field[1].trim());
      }
      return null;
  }
    
    public class CustomException extends Exception{}
}