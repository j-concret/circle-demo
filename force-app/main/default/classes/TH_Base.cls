public abstract class TH_Base {

    protected FieldChangeMaps fieldMap; 
    protected abstract map<String, String> dragonFieldMap();
    protected XF_Dragon_Service dragonService;
    protected virtual Type fieldMapItemType() {
    	return FieldChangeMap.class;
    }
    
    public TH_Base() {
      fieldMap = new FieldChangeMaps(fieldMapItemType());
      dragonService = new XF_Dragon_Service();
    }
    
	protected list<FieldChangeMap> getChangedFields(SObject oldItem, SObject newItem) {
		fieldMap.clearItems();
		for (String key: dragonFieldMap().keySet()) {
			fieldMap.addIfChanged(oldItem, newItem, key);
		}
		return fieldMap.items;
	}
	
	protected map<String, String> getFields(SObject item) {
		map<String, String> result = new map<String, String>();
		for (String key: dragonFieldMap().keySet()) 
			if (item.get(key) != null)
				result.put(key, fieldMap.FieldXMLValue(item, key));
		return result;
	}
	
	protected Boolean hasChanges(SObject oldItem, SObject item) {
		list<FieldChangeMap> changes = getChangedFields(oldItem, item);
		if (changes.size() > 0) {
			String fieldNames = '';
			for (FieldChangeMap change: changes)
				fieldNames += change.fieldName + ' : ';
			System.debug('Dragon fields changed: ' + fieldNames);
			return true;
		} else {
			System.debug('No Dragon fields affected, there will be no call-out to Dragon for this record pair.');
			return false;
		}
	}
	
}