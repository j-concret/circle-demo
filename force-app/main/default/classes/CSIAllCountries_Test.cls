@IsTest
public class CSIAllCountries_Test {
static testMethod void testGetMethod()
    {     
        CountryofOriginMap__c origin = new CountryofOriginMap__c();
        origin.Country__c = 'Brazil';
        origin.Name = 'Brazil';
        origin.Two_Digit_Country_Code__c = 'BR';
        origin.Three_Digit_Country_Code__c = '';
        insert origin;
        
        RestRequest request = new RestRequest();
        RestResponse res = new RestResponse();
        request.requestUri ='/services/apexrest/api/MyShipFromcountries';
        request.httpMethod = 'GET';
        RestContext.request = request;
        RestContext.response = res;
        Test.startTest();
        CSIAllCountries.NCPAPIAllCountries();
        Test.stopTest();        
    }
}