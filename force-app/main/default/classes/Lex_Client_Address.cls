public class Lex_Client_Address {
//SELECT Id, Name FROM RecordType where Id = '0121v000000KsJPAA0'
 @AuraEnabled
    public static Map<String,Object> getrecordTypes(String recordTypeId){
        
        Map<String,Object> response = new Map<String,Object>{'status' => 'OK'};
            
            try{
                
                response.put('recordTypeId',recordTypeId);
                RecordType recordType = [SELECT Id, Name FROM RecordType where Id = :recordTypeId limit 1];
                response.put('recordType',recordType);
                
            }catch(Exception e){
                
                response.put('status','ERROR');
                response.put('message',e.getMessage());
                
            }
        
        return response;
    }
}