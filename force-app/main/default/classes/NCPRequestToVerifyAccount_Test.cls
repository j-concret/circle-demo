@IsTest
public class NCPRequestToVerifyAccount_Test {
     @testSetup
    static void setup(){
        
        Access_token__c accessTokenObj = new Access_token__c();
        accessTokenObj.Status__c = 'Active';
        accessTokenObj.Access_Token__c = 'asdfghj-sdfgfds-sfdfgf';
        
        Insert accessTokenObj;
        
        String accountRID = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Client (E-commerce)').getRecordTypeId();
        Account account5 = new Account (name='E-commerce Quick Quote Demo', RecordTypeId = accountRID,Type ='Client',New_Invoicing_Structure__c = true);
        insert account5;
        
        Contact con2 = new Contact(FirstName = 'Testing',LastName = 'E-Commerce - Don\'t Delete',AccountId = account5.Id);
        insert con2;
        
    }
    
    static testMethod void testNCPQuickInvalidEmail(){
        Access_token__c accessTokenObj = [SELECT Access_Token__c FROM Access_token__c LIMIT 1];
        Account account = [select Id from Account where name='E-commerce Quick Quote Demo'];
        Contact con = [select id from contact where Accountid=:account.Id AND LastName = 'E-Commerce - Don\'t Delete'];
        NCPRequestToVerifyAccountwra wrapperObj = new NCPRequestToVerifyAccountwra();
        wrapperObj.Accesstoken = accessTokenObj.Access_Token__c; 
        wrapperObj.AccountID = account.Id;
        wrapperObj.ContactID = con.Id;
        RestRequest request = new RestRequest();
        RestResponse res = new RestResponse();
        request.requestUri ='/services/apexrest/api/VerifyAccount/';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueOf(JSON.serialize(wrapperObj));
        RestContext.request = request;
        RestContext.response = res;
        
        Test.startTest();
        NCPRequestToVerifyAccount.NCPRequestToVerifyAccount();
        Test.stopTest();
    }
    
    static testMethod void testNCPVerifyAuthFailed(){
        Access_token__c accessTokenObj = [SELECT Access_Token__c FROM Access_token__c LIMIT 1];
        Account account = [select Id from Account where name='E-commerce Quick Quote Demo'];
        Contact con = [select id from contact where Accountid=:account.Id AND LastName = 'E-Commerce - Don\'t Delete'];
        NCPRequestToVerifyAccountwra wrapperObj = new NCPRequestToVerifyAccountwra();
        wrapperObj.Accesstoken = accessTokenObj.Access_Token__c; 
        wrapperObj.AccountID = 'ABC';
        wrapperObj.ContactID = con.Id;
        RestRequest request = new RestRequest();
        RestResponse res = new RestResponse();
        request.requestUri ='/services/apexrest/api/VerifyAccount/';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueOf(JSON.serialize(wrapperObj));
        RestContext.request = request;
        RestContext.response = res;
        
        Test.startTest();
        NCPRequestToVerifyAccount.NCPRequestToVerifyAccount();
        Test.stopTest();
    }

}