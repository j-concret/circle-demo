public with sharing class AddPickupAndFinalDeliveryAddCtrl {
    
    @AuraEnabled
    public static Map<String,Object> getPicklistValue(String objectName,String fieldName){
        Map<String,Object> response = new Map<String,Object>();
        List<String> picklistLabels = new List<String>();
        picklistLabels.add('NONE');
        try {
            Schema.SObjectType s = Schema.getGlobalDescribe().get(objectName) ;
            Schema.DescribeSObjectResult r = s.getDescribe() ;
            Map<String,Schema.SObjectField> fields = r.fields.getMap() ;
            Schema.DescribeFieldResult fieldResult = fields.get(fieldName).getDescribe();
            List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
            for( Schema.PicklistEntry pickListVal : ple){
                System.debug(pickListVal.getLabel() +' '+pickListVal.getValue());
                response.put(pickListVal.getLabel(),pickListVal.getValue());
                picklistLabels.add(pickListVal.getLabel());
            }
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
        response.put('labels',picklistLabels);
        return response;
    }
    
    @AuraEnabled
    public static Map<String, Object> getsfRecords(String soId,Boolean isPickup){
        try {
            Map<String, Object> result = new Map<String, Object>();
            
            Boolean hideNewFinalDeliveryButton = false;
            
            Set<Id> accountIds = new Set<Id>();
            
            
            Shipment_Order__c so = [SELECT Id,Account__c,Service_Type__c, Ship_From_Country__c,Destination__c FROM Shipment_Order__c WHERE Id =: soId];
            
            accountIds.add(so.Account__c);
            
            String country;
            
            if(so.Service_Type__c == 'IOR'|| so.Service_Type__c == 'Shipping ONLY'){
                country = isPickup ? so.Ship_From_Country__c : so.Destination__c;
            }else if(so.Service_Type__c == 'EOR'){
                country = isPickup ? so.Destination__c : so.Ship_From_Country__c;
            }
            if(country != Null){
                Id devRecordTypeId = Schema.SObjectType.Client_Address__c.getRecordTypeInfosByName().get(isPickup ? 'PickUp' : 'Final Destinations').getRecordTypeId();
                
                if(!isPickup){
                    String eComProspectiveAccId = '';
                    Boolean isZeeAccount = false;
                    //String query = 'select Id,name,CaseID__c,CompanyName__c,AdditionalContactNumber__c,Contact_Email__c,Contact_Full_Name__c,Contact_Phone_Number__c,Province__c,City__c,Postal_Code__c,All_Countries__c';
                    
                    
                    List<Account> accountList = [Select Id, Name,RecordType.Name, Allow_Customised_Final_Deliveries__c FROM Account
                                                 WHERE Id = :so.Account__c OR Name = 'TecEx E-Commerce Prospective Client'];
                    
                    for(Account acc : accountList){
                        
                        //Collecting E-Commerce Prospective Client id
                        if(acc.Name == 'TecEx E-Commerce Prospective Client'){
                            eComProspectiveAccId = acc.Id;
                        }
                        
                        if(((acc.RecordType.Name == 'Client (E-commerce)' || acc.RecordType.Name == 'Zee') && acc.Name != 'TecEx E-Commerce Prospective Client')){
                            isZeeAccount = true;
                            if(!acc.Allow_Customised_Final_Deliveries__c){
                                accountIds.remove(acc.Id);
                                hideNewFinalDeliveryButton = true;                      
                            }
                        }
                        
                    }
                    
                    if(isZeeAccount){
                        accountIds.add(eComProspectiveAccId);
                    }  
                }   
                
                List<Client_Address__c> clientAddresses = [SELECT Id,Name,CaseID__c,Pickup_Preference__c ,CompanyName__c,AdditionalContactNumber__c,Contact_Email__c,Contact_Full_Name__c,Contact_Phone_Number__c,Province__c,City__c,Postal_Code__c,All_Countries__c FROM Client_Address__c WHERE Client__c IN :accountIds AND All_Countries__c=: country AND recordtypeID =: devRecordTypeId ];
                result.put('ClientAddress', clientAddresses);
                result.put('hideNewFinalDeliveryButton', hideNewFinalDeliveryButton);
                result.put('business', Util.getAppName());
                
            }
            
            return result;
            
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }
    
    @AuraEnabled
    public static String getAddressFrom(String SearchText){
        String APIKey = 'AIzaSyCdwojMVLcu2JbRoRheb9isbNLxC0wRwR4';
        String result = null;
        system.debug('SearchText is ' + SearchText);
        try{
            if(SearchText != null) {
                String APIUrl = 'https://maps.googleapis.com/maps/api/place/autocomplete/json?input=' + SearchText.replace(' ', '%20') + '&key=' + APIKey;
                system.debug('APIUrl is ' + APIUrl);
                HttpRequest req = new HttpRequest();
                req.setMethod('GET');
                req.setEndpoint(APIURL);
                Http http = new Http();
                HttpResponse res = http.send(req);
                Integer statusCode = res.getStatusCode();
                system.debug('statusCode is ' + statusCode);
                if(statusCode == 200) {
                    system.debug('API invoked successfully');
                    result = res.getBody();
                }
            }
        }
        catch(exception e) {
            system.debug(e.getMessage());
        }
        return result;
    }
    
    @AuraEnabled
    public static String getAddressDetailsByPlaceId(String PlaceID){
        String APIKey = 'AIzaSyCdwojMVLcu2JbRoRheb9isbNLxC0wRwR4';
        String result = null;
        system.debug('SearchText is ' + PlaceID);
        try{
            if(PlaceID != null) {
                String APIUrl = 'https://maps.googleapis.com/maps/api/place/details/json?placeid=' + PlaceId.replace(' ', '%20') + '&key=' + APIKey;
                system.debug('APIUrl is ' + APIUrl);
                HttpRequest req = new HttpRequest();
                req.setMethod('GET');
                req.setEndpoint(APIURL);
                Http http = new Http();
                HttpResponse res = http.send(req);
                Integer statusCode = res.getStatusCode();
                system.debug('statusCode is ' + statusCode);
                if(statusCode == 200) {
                    system.debug('API invoked successfully');
                    result = res.getBody();
                }
            }
        }
        catch(exception e) {
            //Handling exception
            system.debug(e.getMessage());
        }
        return result;
    }
    
    @AuraEnabled
    public static Map<String,Object> getZipCodeByCountry(String countryName){
        Map<String,Object> response = new Map<String,Object> {'status' => 'OK'};
            try{
                response.put('countryName',countryName);
                no_zip_code_countries__c nzp = [SELECT Id,Name,zip_code__c FROM no_zip_code_countries__c WHERE Name = :countryName limit 1];
                response.put('no_zip_code_countries',nzp);
                //response.put('countryName',countryName);
            }catch(Exception e) {
                response.put('status','ERROR');
                response.put('message',e.getMessage());
            }
        return response;
    }
    
    //Creating pickup address
    @AuraEnabled
    public static void createNewAddress(List<Client_Address__c> newAddressList, string soId,Boolean isPickup){
        try {
            Shipment_Order__c so = [SELECT Id,Account__c, Ship_From_Country__c,Destination__c FROM Shipment_Order__c WHERE Id =: soid];
            List<Client_Address__c> newAddList = new list<Client_Address__c>();
            Id devRecordTypeId = Schema.SObjectType.Client_Address__c.getRecordTypeInfosByName().get(isPickup ? 'PickUp' : 'Final Destinations').getRecordTypeId();
            
            for(Client_Address__c ca :newAddressList) {
                Client_Address__c newAdd = new Client_Address__c(
                    recordtypeID = devRecordTypeId,
                    Name = ca.Province__c,
                    Contact_Full_Name__c= ca.Contact_Full_Name__c,
                    Contact_Email__c = ca.Contact_Email__c,
                    Contact_Phone_Number__c = ca.Contact_Phone_Number__c,
                    CompanyName__c= ca.CompanyName__c,
                    Address__c=ca.Address__c,
                    Address2__c = ca.Address2__c,
                    City__c=ca.City__c,
                    Province__c=ca.Province__c,
                    Postal_Code__c=ca.Postal_Code__c,
                    All_Countries__c=ca.All_Countries__c,
                    Client__c=so.Account__c
                );
                
                if(isPickup) newAdd.Pickup_Preference__c = ca.Pickup_Preference__c;
                
                newAddList.add(newAdd);
            }
            Insert newAddList;
            
            System.debug('newAddList '+newAddList);
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }
    
    @AuraEnabled
    public static void updateFreight(List<Id> selectedAddIds,String soId, Boolean isPickup){
        try{System.debug('add--> '+selectedAddIds);
            List<Freight__c> freights = [SELECT Id,Ship_From_Address__c,Ship_to_Address__c FROM Freight__c WHERE Shipment_Order__c =: soId];
            
            if(freights.isEmpty()) throw new AuraHandledException('No Freight record found with this shipment order.');
            
            if(isPickup)
                freights[0].Ship_From_Address__c = selectedAddIds[0];
            else{
                freights[0].Ship_to_Address__c = selectedAddIds[0];
                List<Final_Delivery__c> finalDelAdd = new list<Final_Delivery__c>();
                
                for(Client_Address__c ca : [SELECT Id,Name,CaseID__c,CompanyName__c,Address__c,Address2__c,AdditionalContactNumber__c,Contact_Email__c,Contact_Full_Name__c,Contact_Phone_Number__c,Province__c,City__c,Postal_Code__c,All_Countries__c FROM Client_Address__c WHERE Id IN:selectedAddIds]) {
                    
                    Final_Delivery__c finalDel = new Final_Delivery__c(
                        Name = ca.Name,
                        /*Company_Name__c = ca.CompanyName__c,
Contact_name__c = ca.Contact_Full_Name__c,
Contact_email__c = ca.Contact_Email__c,
Contact_number__c = ca.Contact_Phone_Number__c,
Address_Line_1__c = ca.Address__c,
Address_Line_2__c = ca.Address2__c,
City__c = ca.City__c + ca.Province__c,
Zip__c = ca.Postal_Code__c,
Country__c = ca.All_Countries__c,*/
                        Ship_to_address__c = ca.Id,
                        // Created_From__c = 'Task component',
                        Shipment_Order__c = soId
                    );
                    finalDelAdd.add(finalDel);
                }
                Insert finalDelAdd;
            }
            ZenkraftOnFreightHandler.addPickupAndFD = true;
            update freights;
           }
        catch(Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }
}