global class ragStatusUpdate implements Database.Batchable<sObject>{

   global Database.QueryLocator start(Database.BatchableContext BC)

  {   string query = 'select id, Update_Rag_Status_Time__c from Shipment_Order__c  where Shipping_Status__c != \'POD Received\' AND Shipping_Status__c != \'On Hold\'AND Shipping_Status__c != \'Cancelled\'AND Shipping_Status__c != \'Shipment Pending\' AND Shipping_Status__c != \'Cancelled - With fees/costs\' AND Shipping_Status__c != \'Cancelled\'AND Shipping_Status__c != \'Cancelled - No fees/costs\' AND Shipping_Status__c != \'Customs Clearance Docs Received\' ';
      return Database.getQueryLocator(query);
   }

   global void execute(Database.BatchableContext BC, List<Shipment_Order__c>scope )
   {
list<Shipment_Order__c> SO = new list<Shipment_Order__c>();
        for(Shipment_Order__c s : scope)
           {
            s.Update_Rag_Status_Time__c  = TRUE;
            
             SO.add(s);
            }      
          update SO;
     }

   global void finish(Database.BatchableContext BC){

   }

}