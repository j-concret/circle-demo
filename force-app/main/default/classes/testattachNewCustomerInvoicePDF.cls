@isTest

public class testattachNewCustomerInvoicePDF {
    
   private static testMethod void  setUpData(){
         Account account = new Account (name='Acme1', Type ='Client', CSE_IOR__c = '0050Y000001LTZO', New_Invoicing_Structure__c = FALSE, Service_Manager__c = '0050Y000001LTZO');
        insert account;  
        
        Account account2 = new Account (name='Test Supplier', Type ='Supplier', ICE__c = '0050Y000001km5c', RecordTypeId = '0120Y0000006KjQQAU');
        insert account2;      
         
        Contact contact = new Contact ( lastname ='Testing Individual',  AccountId = account.Id);
        insert contact;  
        
        IOR_Price_List__c iorpl = new IOR_Price_List__c ( Client_Name__c = account.Id, Name = 'Brazil', IOR_Fee__c = 0.15, On_Charge_Mark_up__c = 0.15,
        TecEx_Shipping_Fee_Markup__c =0.40, IOR_Min_Fee__c = 100,  Admin_Fee__c =100, Set_up_fee__c =100, Bank_Fees__c =200, Tax_Rate__c = 0.20,
        Estimated_Customs_Brokerage__c =250, Estimated_Customs_Clearance__c =250, Estimated_Customs_Handling__c =250, Estimated_Import_License__c =250, Destination__c = 'Brazil' );
        insert iorpl; 
       
        country_price_approval__c cpa = new country_price_approval__c( name ='Brazil', Billing_term__c = 'DAP/CIF - IOR pays',  Airwaybill_Instructions__c = 'User IOR Address', Supplier__c = account2.Id, 
        In_Country_Specialist__c = '0050Y000001km5c');
        insert cpa;
        
        CPA_v2_0__c cpav2 = new CPA_v2_0__c(Name = 'Brazil Testing 2.0', Supplier__c = account2.Id, Service_Type__c = 'IOR/EOR', Related_Costing_CPA__c = 'a260Y00000EnDjr', Preferred_Supplier__c = TRUE,
                                           VAT_Rate__c = 0.1, Destination__c = 'Brazil');
        insert cpav2;
       
        List<CPA_Costing__c> costs = new List<CPA_Costing__c>();

       
       costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'Shipment value', Cost_Type__c = 'Variable',  Amount__c = null, Rate__c = 10, IOR_EOR__c = 'IOR', Min__c = 375, Max__c = 14000, Conditional_value__c = 'Shipment value', Condition__c = 'Between', Floor__c = 0, Ceiling__c = 10000));
       costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'Shipment value', Cost_Type__c = 'Variable',  Amount__c = null, Rate__c = 10, IOR_EOR__c = 'IOR', Min__c = 563, Max__c = 14000, Conditional_value__c = 'Shipment value', Condition__c = 'Between', Floor__c = 10000, Ceiling__c = 20000));
       costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'Shipment value', Cost_Type__c = 'Variable',  Amount__c = null, Rate__c = 10, IOR_EOR__c = 'IOR', Min__c = 750, Max__c = 14000, Conditional_value__c = 'Shipment value', Condition__c = '>', Floor__c = 20000));
       costs.add(new CPA_Costing__c(Cost_Category__c = 'International freight',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'Chargeable weight', Cost_Type__c = 'Variable',  Amount__c = null, Rate__c = 10, IOR_EOR__c = 'IOR', Min__c = 100, Max__c = 500, Conditional_value__c = 'Chargeable weight', Condition__c = '>', Floor__c = 100));
       costs.add(new CPA_Costing__c(Cost_Category__c = 'International freight',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'Chargeable weight', Cost_Type__c = 'Variable',  Amount__c = null, Rate__c = 10, IOR_EOR__c = 'IOR', Min__c = 55));
       costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'Shipment value', Cost_Type__c = 'Variable',  Amount__c = null, Rate__c = 10, IOR_EOR__c = 'IOR', Conditional_value__c = 'CIF value', Condition__c = '>', Floor__c = 20000));
       costs.add(new CPA_Costing__c(Cost_Category__c = 'Miscellaneous',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'CIF value', Cost_Type__c = 'Variable',  Amount__c = null, Rate__c = 10, IOR_EOR__c = 'IOR'));
       costs.add(new CPA_Costing__c(Cost_Category__c = 'Miscellaneous',CPA_v2_0__c = cpav2.Id, Applied_to__c = '# packages', Cost_Type__c = 'Variable', Amount__c = null, Rate__c = 10, IOR_EOR__c = 'IOR'));
       costs.add(new CPA_Costing__c(Cost_Category__c = 'Miscellaneous',CPA_v2_0__c = cpav2.Id, Applied_to__c = '# Parts (line items)', Cost_Type__c = 'Variable', Amount__c = null, Rate__c = 10, IOR_EOR__c = 'IOR'));
       costs.add(new CPA_Costing__c(Cost_Category__c = 'Miscellaneous',CPA_v2_0__c = cpav2.Id, Applied_to__c = '# Unique HS Codes', Cost_Type__c = 'Variable', Amount__c = null, Rate__c = 10, IOR_EOR__c = 'IOR'));
       costs.add(new CPA_Costing__c(Cost_Category__c = 'Miscellaneous',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'Total Taxes', Cost_Type__c = 'Variable', Amount__c = null, Rate__c = 10, IOR_EOR__c = 'IOR'));
       costs.add(new CPA_Costing__c(Cost_Category__c = 'Miscellaneous',CPA_v2_0__c = cpav2.Id, Variable_threshold__c = 10,  Applied_to__c = 'Shipment value', Cost_Type__c = 'Variable',  Amount__c = null, Rate__c = 10, IOR_EOR__c = 'IOR'));
       costs.add(new CPA_Costing__c(Cost_Category__c = 'Miscellaneous',CPA_v2_0__c = cpav2.Id, Variable_threshold__c = 100,  Applied_to__c = 'Chargeable weight', Cost_Type__c = 'Variable',  Amount__c = null, Rate__c = 10, IOR_EOR__c = 'IOR', VAT_applicable__c = TRUE));
       costs.add(new CPA_Costing__c(Cost_Category__c = 'Miscellaneous',CPA_v2_0__c = cpav2.Id, Cost_Type__c = 'Fixed',  Amount__c = 300, Rate__c = null, IOR_EOR__c = 'IOR'));
       costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'Shipment value', Cost_Type__c = 'Variable',  Amount__c = null, Rate__c = 10, IOR_EOR__c = 'EOR', Min__c = 375, Max__c = 14000, Conditional_value__c = 'Shipment value', Condition__c = 'Between', Floor__c = 0, Ceiling__c = 10000));
       costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'Shipment value', Cost_Type__c = 'Variable',  Amount__c = null, Rate__c = 10, IOR_EOR__c = 'EOR', Min__c = 563, Max__c = 14000, Conditional_value__c = 'Shipment value', Condition__c = 'Between', Floor__c = 10000, Ceiling__c = 20000));
       costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'Shipment value', Cost_Type__c = 'Variable',  Amount__c = null, Rate__c = 10, IOR_EOR__c = 'EOR', Min__c = 750, Max__c = 14000, Conditional_value__c = 'Shipment value', Condition__c = '>', Floor__c = 20000));
       costs.add(new CPA_Costing__c(Cost_Category__c = 'International freight',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'Chargeable weight', Cost_Type__c = 'Variable',  Amount__c = null, Rate__c = 10, IOR_EOR__c = 'EOR', Max__c = 100));
       costs.add(new CPA_Costing__c(Cost_Category__c = 'International freight',CPA_v2_0__c = cpav2.Id, Applied_to__c = '# Packages', Cost_Type__c = 'Variable',  Amount__c = null, Rate__c = 10, IOR_EOR__c = 'EOR', Conditional_value__c = '# of packages', Condition__c = '>', Floor__c = 2));
       costs.add(new CPA_Costing__c(Cost_Category__c = 'Miscellaneous',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'CIF value', Cost_Type__c = 'Variable',  Amount__c = null, Rate__c = 10, IOR_EOR__c = 'EOR'));
       costs.add(new CPA_Costing__c(Cost_Category__c = 'Miscellaneous',CPA_v2_0__c = cpav2.Id, Applied_to__c = '# packages', Cost_Type__c = 'Variable', Amount__c = null, Rate__c = 10, IOR_EOR__c = 'EOR'));
       costs.add(new CPA_Costing__c(Cost_Category__c = 'Miscellaneous',CPA_v2_0__c = cpav2.Id, Applied_to__c = '# Parts (line items)', Cost_Type__c = 'Variable', Amount__c = null, Rate__c = 10, IOR_EOR__c = 'EOR'));
       costs.add(new CPA_Costing__c(Cost_Category__c = 'Miscellaneous',CPA_v2_0__c = cpav2.Id, Applied_to__c = '# Unique HS Codes', Cost_Type__c = 'Variable', Amount__c = null, Rate__c = 10, IOR_EOR__c = 'EOR'));
       costs.add(new CPA_Costing__c(Cost_Category__c = 'Miscellaneous',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'Total Taxes', Cost_Type__c = 'Variable', Amount__c = null, Rate__c = 10, IOR_EOR__c = 'EOR'));
       costs.add(new CPA_Costing__c(Cost_Category__c = 'Miscellaneous',CPA_v2_0__c = cpav2.Id, Variable_threshold__c = 10,  Applied_to__c = 'Shipment value', Cost_Type__c = 'Variable',  Amount__c = null, Rate__c = 10, IOR_EOR__c = 'EOR'));
       costs.add(new CPA_Costing__c(Cost_Category__c = 'Miscellaneous',CPA_v2_0__c = cpav2.Id, Variable_threshold__c = 100,  Applied_to__c = 'Chargeable weight', Cost_Type__c = 'Variable',  Amount__c = null, Rate__c = 10, IOR_EOR__c = 'EOR'));
       costs.add(new CPA_Costing__c(Cost_Category__c = 'Miscellaneous',CPA_v2_0__c = cpav2.Id,  Cost_Type__c = 'Fixed',  Amount__c = 100, Rate__c = null, IOR_EOR__c = 'EOR'));
       insert costs;
        
        
        Shipment_Order__c shipmentOrder = new Shipment_Order__c(Account__c = account.id, Shipment_Value_USD__c =10000, CPA_v2_0__c = cpav2.Id,
        Client_Contact_for_this_Shipment__c = contact.Id, Who_arranges_International_courier__c ='Client', Tax_Treatment__c ='DAP/CIF - IOR pays', Ship_to_Country__c = cpa.Id,
        Destination__c = 'Brazil', Service_Type__c = 'IOR', IOR_Price_List__c = iorpl.Id, 
        CI_Admin_Fee__c  =100, CI_Bank_Fees__c  = 100, CI_Finance_Fee__c  = 100, CI_Insurance_Fee_USD__c  = 100, CI_International_Delivery_Fee__c  = 100, CI_IOR_and_Import_Compliance_Fee_USD__c  = 100, 
        CI_Miscellaneous_Fee__c  = 100, CI_Recharge_Tax_and_Duty__c  = 100, CI_Total_Clearance_Costs__c  = 100, CI_Total_Customs_Brokerage_Cost__c  = 100, CI_Total_Handling_Cost__c  = 100, 
        CI_Total_Licence_Cost__c  = 100, FC_Admin_Fee__c  = 100, FC_Bank_Fees__c  = 100, FC_Finance_Fee__c  = 100, FC_Insurance_Fee_USD__c   = 100, FC_International_Delivery_Fee__c  = 100, 
        FC_IOR_and_Import_Compliance_Fee_USD__c  = 100, FC_Miscellaneous_Fee__c  = 100, FC_Recharge_Tax_and_Duty__c  = 100, FC_Total_Clearance_Costs__c  = 100, 
        FC_Total_Customs_Brokerage_Cost__c  = 100, FC_Total_Handling_Costs__c  = 100, FC_Total_License_Cost__c  = 100, Total_CIF_Duties__c = 0, Total_FOB_Duties__c = 0);
        insert shipmentOrder; 
       
        Shipment_Order__c shipmentOrder2 = new Shipment_Order__c(Account__c = account.id, Shipment_Value_USD__c =10000, CPA_v2_0__c = cpav2.Id,
        Client_Contact_for_this_Shipment__c = contact.Id, Who_arranges_International_courier__c ='Client', Tax_Treatment__c ='DAP/CIF - IOR pays', Ship_to_Country__c = cpa.Id,
        Destination__c = 'Brazil', Service_Type__c = 'EOR', IOR_Price_List__c = iorpl.Id, of_Unique_Line_Items__c = 10, Total_Taxes__c= 1500, of_Line_Items__c= 10, Chargeable_Weight__c = 200,
        CI_Admin_Fee__c  =100, CI_Bank_Fees__c  = 100, CI_Finance_Fee__c  = 100, CI_Insurance_Fee_USD__c  = 100, CI_International_Delivery_Fee__c  = 100, CI_IOR_and_Import_Compliance_Fee_USD__c  = 100, 
        CI_Miscellaneous_Fee__c  = 100, CI_Recharge_Tax_and_Duty__c  = 100, CI_Total_Clearance_Costs__c  = 100, CI_Total_Customs_Brokerage_Cost__c  = 100, CI_Total_Handling_Cost__c  = 100, 
        CI_Total_Licence_Cost__c  = 100, FC_Admin_Fee__c  = 100, FC_Bank_Fees__c  = 100, FC_Finance_Fee__c  = 100, FC_Insurance_Fee_USD__c   = 100, FC_International_Delivery_Fee__c  = 100, 
        FC_IOR_and_Import_Compliance_Fee_USD__c  = 100, FC_Miscellaneous_Fee__c  = 100, FC_Recharge_Tax_and_Duty__c  = 100, FC_Total_Clearance_Costs__c  = 100, 
        FC_Total_Customs_Brokerage_Cost__c  = 100, FC_Total_Handling_Costs__c  = 100, FC_Total_License_Cost__c  = 100, Total_CIF_Duties__c = 0, Total_FOB_Duties__c = 0);
        insert shipmentOrder2; 
     
        Customer_Invoice__c customerInvoice = new Customer_Invoice__c(Client__c = account.id, Shipment_Order__c = shipmentOrder.Id, Invoice_Amount__c = 2000);
        insert customerInvoice; 
        
         Customer_Invoice__c customerInvoice1 = new Customer_Invoice__c(Client__c = account.id, Shipment_Order__c = shipmentOrder2.Id, Invoice_Amount__c = 2000, Invoice_Type__c = 'Top-up Invoice');
        insert customerInvoice1; 
              
              
 
        test.startTest();
       
        PageReference pageRef = Page.customerInvoicePDF2;
        
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('id',customerInvoice.id);

        ApexPages.StandardController stdCon= new ApexPages.StandardController(customerInvoice);
        attachNewCustomerInvoicePDF con = new attachNewCustomerInvoicePDF(stdCon);
        con.attachPDF();  
        
        
        
        
       
        PageReference pageRef1 = Page.NewCustomerInvoice;
        
        Test.setCurrentPage(pageRef1);
        pageRef1.getParameters().put('id',customerInvoice.id);

        ApexPages.StandardController stdCon1= new ApexPages.StandardController(customerInvoice);
        attachNewCustomerInvoicePDF con1 = new attachNewCustomerInvoicePDF(stdCon1);
        con1.attachPDF();  
        
        
        
        
        PageReference pageRef2 = Page.customerInvoicePDF2;
        
        Test.setCurrentPage(pageRef2);
        pageRef2.getParameters().put('id',customerInvoice1.id);

        ApexPages.StandardController stdCon2 = new ApexPages.StandardController(customerInvoice1);
        attachNewCustomerInvoicePDF con2 = new attachNewCustomerInvoicePDF (stdCon2);
        con2.attachPDF();  
        
        
       
        PageReference pageRef3 = Page.NewCustomerInvoice;
        
        Test.setCurrentPage(pageRef3);
        pageRef3.getParameters().put('id',customerInvoice1.id);

        ApexPages.StandardController stdCon3= new ApexPages.StandardController(customerInvoice1);
        attachNewCustomerInvoicePDF con3 = new attachNewCustomerInvoicePDF(stdCon3);
        con3.attachPDF();  
        
        
       
        test.stopTest();

     
       
    } 
    
    private static testMethod void  setUpData1(){
         Account account = new Account (name='Acme1', Type ='Client', CSE_IOR__c = '0050Y000001LTZO', New_Invoicing_Structure__c = TRUE, Service_Manager__c = '0050Y000001LTZO');
        insert account;  
        
        Account account2 = new Account (name='Test Supplier', Type ='Supplier', ICE__c = '0050Y000001km5c', RecordTypeId = '0120Y0000006KjQQAU');
        insert account2;      
         
        Contact contact = new Contact ( lastname ='Testing Individual',  AccountId = account.Id);
        insert contact;  
        
        IOR_Price_List__c iorpl = new IOR_Price_List__c ( Client_Name__c = account.Id, Name = 'Brazil', IOR_Fee__c = 0.15, On_Charge_Mark_up__c = 0.15,
        TecEx_Shipping_Fee_Markup__c =0.40, IOR_Min_Fee__c = 100,  Admin_Fee__c =100, Set_up_fee__c =100, Bank_Fees__c =200, Tax_Rate__c = 0.20,
        Estimated_Customs_Brokerage__c =250, Estimated_Customs_Clearance__c =250, Estimated_Customs_Handling__c =250, Estimated_Import_License__c =250, Destination__c = 'Brazil' );
        insert iorpl; 
       
        country_price_approval__c cpa = new country_price_approval__c( name ='Brazil', Billing_term__c = 'DAP/CIF - IOR pays',  Airwaybill_Instructions__c = 'User IOR Address', Supplier__c = account2.Id, 
        In_Country_Specialist__c = '0050Y000001km5c');
        insert cpa;
        
        CPA_v2_0__c cpav2 = new CPA_v2_0__c(Name = 'Brazil Testing 2.0', Supplier__c = account2.Id, Service_Type__c = 'IOR/EOR', Related_Costing_CPA__c = 'a260Y00000EnDjr', Preferred_Supplier__c = TRUE,
                                           VAT_Rate__c = 0.1, Destination__c = 'Brazil');
        insert cpav2;
       
        List<CPA_Costing__c> costs = new List<CPA_Costing__c>();

       
       costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'Shipment value', Cost_Type__c = 'Variable',  Amount__c = null, Rate__c = 10, IOR_EOR__c = 'IOR', Min__c = 375, Max__c = 14000, Conditional_value__c = 'Shipment value', Condition__c = 'Between', Floor__c = 0, Ceiling__c = 10000));
       costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'Shipment value', Cost_Type__c = 'Variable',  Amount__c = null, Rate__c = 10, IOR_EOR__c = 'IOR', Min__c = 563, Max__c = 14000, Conditional_value__c = 'Shipment value', Condition__c = 'Between', Floor__c = 10000, Ceiling__c = 20000));
       costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'Shipment value', Cost_Type__c = 'Variable',  Amount__c = null, Rate__c = 10, IOR_EOR__c = 'IOR', Min__c = 750, Max__c = 14000, Conditional_value__c = 'Shipment value', Condition__c = '>', Floor__c = 20000));
       costs.add(new CPA_Costing__c(Cost_Category__c = 'International freight',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'Chargeable weight', Cost_Type__c = 'Variable',  Amount__c = null, Rate__c = 10, IOR_EOR__c = 'IOR', Min__c = 100, Max__c = 500, Conditional_value__c = 'Chargeable weight', Condition__c = '>', Floor__c = 100));
       costs.add(new CPA_Costing__c(Cost_Category__c = 'International freight',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'Chargeable weight', Cost_Type__c = 'Variable',  Amount__c = null, Rate__c = 10, IOR_EOR__c = 'IOR', Min__c = 55));
       costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'Shipment value', Cost_Type__c = 'Variable',  Amount__c = null, Rate__c = 10, IOR_EOR__c = 'IOR', Conditional_value__c = 'CIF value', Condition__c = '>', Floor__c = 20000));
       costs.add(new CPA_Costing__c(Cost_Category__c = 'Miscellaneous',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'CIF value', Cost_Type__c = 'Variable',  Amount__c = null, Rate__c = 10, IOR_EOR__c = 'IOR'));
       costs.add(new CPA_Costing__c(Cost_Category__c = 'Miscellaneous',CPA_v2_0__c = cpav2.Id, Applied_to__c = '# packages', Cost_Type__c = 'Variable', Amount__c = null, Rate__c = 10, IOR_EOR__c = 'IOR'));
       costs.add(new CPA_Costing__c(Cost_Category__c = 'Miscellaneous',CPA_v2_0__c = cpav2.Id, Applied_to__c = '# Parts (line items)', Cost_Type__c = 'Variable', Amount__c = null, Rate__c = 10, IOR_EOR__c = 'IOR'));
       costs.add(new CPA_Costing__c(Cost_Category__c = 'Miscellaneous',CPA_v2_0__c = cpav2.Id, Applied_to__c = '# Unique HS Codes', Cost_Type__c = 'Variable', Amount__c = null, Rate__c = 10, IOR_EOR__c = 'IOR'));
       costs.add(new CPA_Costing__c(Cost_Category__c = 'Miscellaneous',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'Total Taxes', Cost_Type__c = 'Variable', Amount__c = null, Rate__c = 10, IOR_EOR__c = 'IOR'));
       costs.add(new CPA_Costing__c(Cost_Category__c = 'Miscellaneous',CPA_v2_0__c = cpav2.Id, Variable_threshold__c = 10,  Applied_to__c = 'Shipment value', Cost_Type__c = 'Variable',  Amount__c = null, Rate__c = 10, IOR_EOR__c = 'IOR'));
       costs.add(new CPA_Costing__c(Cost_Category__c = 'Miscellaneous',CPA_v2_0__c = cpav2.Id, Variable_threshold__c = 100,  Applied_to__c = 'Chargeable weight', Cost_Type__c = 'Variable',  Amount__c = null, Rate__c = 10, IOR_EOR__c = 'IOR', VAT_applicable__c = TRUE));
       costs.add(new CPA_Costing__c(Cost_Category__c = 'Miscellaneous',CPA_v2_0__c = cpav2.Id, Cost_Type__c = 'Fixed',  Amount__c = 300, Rate__c = null, IOR_EOR__c = 'IOR'));
       costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'Shipment value', Cost_Type__c = 'Variable',  Amount__c = null, Rate__c = 10, IOR_EOR__c = 'EOR', Min__c = 375, Max__c = 14000, Conditional_value__c = 'Shipment value', Condition__c = 'Between', Floor__c = 0, Ceiling__c = 10000));
       costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'Shipment value', Cost_Type__c = 'Variable',  Amount__c = null, Rate__c = 10, IOR_EOR__c = 'EOR', Min__c = 563, Max__c = 14000, Conditional_value__c = 'Shipment value', Condition__c = 'Between', Floor__c = 10000, Ceiling__c = 20000));
       costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'Shipment value', Cost_Type__c = 'Variable',  Amount__c = null, Rate__c = 10, IOR_EOR__c = 'EOR', Min__c = 750, Max__c = 14000, Conditional_value__c = 'Shipment value', Condition__c = '>', Floor__c = 20000));
       costs.add(new CPA_Costing__c(Cost_Category__c = 'International freight',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'Chargeable weight', Cost_Type__c = 'Variable',  Amount__c = null, Rate__c = 10, IOR_EOR__c = 'EOR', Max__c = 100));
       costs.add(new CPA_Costing__c(Cost_Category__c = 'International freight',CPA_v2_0__c = cpav2.Id, Applied_to__c = '# Packages', Cost_Type__c = 'Variable',  Amount__c = null, Rate__c = 10, IOR_EOR__c = 'EOR', Conditional_value__c = '# of packages', Condition__c = '>', Floor__c = 2));
       costs.add(new CPA_Costing__c(Cost_Category__c = 'Miscellaneous',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'CIF value', Cost_Type__c = 'Variable',  Amount__c = null, Rate__c = 10, IOR_EOR__c = 'EOR'));
       costs.add(new CPA_Costing__c(Cost_Category__c = 'Miscellaneous',CPA_v2_0__c = cpav2.Id, Applied_to__c = '# packages', Cost_Type__c = 'Variable', Amount__c = null, Rate__c = 10, IOR_EOR__c = 'EOR'));
       costs.add(new CPA_Costing__c(Cost_Category__c = 'Miscellaneous',CPA_v2_0__c = cpav2.Id, Applied_to__c = '# Parts (line items)', Cost_Type__c = 'Variable', Amount__c = null, Rate__c = 10, IOR_EOR__c = 'EOR'));
       costs.add(new CPA_Costing__c(Cost_Category__c = 'Miscellaneous',CPA_v2_0__c = cpav2.Id, Applied_to__c = '# Unique HS Codes', Cost_Type__c = 'Variable', Amount__c = null, Rate__c = 10, IOR_EOR__c = 'EOR'));
       costs.add(new CPA_Costing__c(Cost_Category__c = 'Miscellaneous',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'Total Taxes', Cost_Type__c = 'Variable', Amount__c = null, Rate__c = 10, IOR_EOR__c = 'EOR'));
       costs.add(new CPA_Costing__c(Cost_Category__c = 'Miscellaneous',CPA_v2_0__c = cpav2.Id, Variable_threshold__c = 10,  Applied_to__c = 'Shipment value', Cost_Type__c = 'Variable',  Amount__c = null, Rate__c = 10, IOR_EOR__c = 'EOR'));
       costs.add(new CPA_Costing__c(Cost_Category__c = 'Miscellaneous',CPA_v2_0__c = cpav2.Id, Variable_threshold__c = 100,  Applied_to__c = 'Chargeable weight', Cost_Type__c = 'Variable',  Amount__c = null, Rate__c = 10, IOR_EOR__c = 'EOR'));
       costs.add(new CPA_Costing__c(Cost_Category__c = 'Miscellaneous',CPA_v2_0__c = cpav2.Id,  Cost_Type__c = 'Fixed',  Amount__c = 100, Rate__c = null, IOR_EOR__c = 'EOR'));
       insert costs;
        
        
        Shipment_Order__c shipmentOrder = new Shipment_Order__c(Account__c = account.id, Shipment_Value_USD__c =10000, CPA_v2_0__c = cpav2.Id,
        Client_Contact_for_this_Shipment__c = contact.Id, Who_arranges_International_courier__c ='Client', Tax_Treatment__c ='DAP/CIF - IOR pays', Ship_to_Country__c = cpa.Id,
        Destination__c = 'Brazil', Service_Type__c = 'IOR', IOR_Price_List__c = iorpl.Id, 
        CI_Admin_Fee__c  =100, CI_Bank_Fees__c  = 100, CI_Finance_Fee__c  = 100, CI_Insurance_Fee_USD__c  = 100, CI_International_Delivery_Fee__c  = 100, CI_IOR_and_Import_Compliance_Fee_USD__c  = 100, 
        CI_Miscellaneous_Fee__c  = 100, CI_Recharge_Tax_and_Duty__c  = 100, CI_Total_Clearance_Costs__c  = 100, CI_Total_Customs_Brokerage_Cost__c  = 100, CI_Total_Handling_Cost__c  = 100, 
        CI_Total_Licence_Cost__c  = 100, FC_Admin_Fee__c  = 100, FC_Bank_Fees__c  = 100, FC_Finance_Fee__c  = 100, FC_Insurance_Fee_USD__c   = 100, FC_International_Delivery_Fee__c  = 100, 
        FC_IOR_and_Import_Compliance_Fee_USD__c  = 100, FC_Miscellaneous_Fee__c  = 100, FC_Recharge_Tax_and_Duty__c  = 100, FC_Total_Clearance_Costs__c  = 100, 
        FC_Total_Customs_Brokerage_Cost__c  = 100, FC_Total_Handling_Costs__c  = 100, FC_Total_License_Cost__c  = 100, Total_CIF_Duties__c = 0, Total_FOB_Duties__c = 0);
        insert shipmentOrder; 
       
        Shipment_Order__c shipmentOrder2 = new Shipment_Order__c(Account__c = account.id, Shipment_Value_USD__c =10000, CPA_v2_0__c = cpav2.Id,
        Client_Contact_for_this_Shipment__c = contact.Id, Who_arranges_International_courier__c ='Client', Tax_Treatment__c ='DAP/CIF - IOR pays', Ship_to_Country__c = cpa.Id,
        Destination__c = 'Brazil', Service_Type__c = 'EOR', IOR_Price_List__c = iorpl.Id, of_Unique_Line_Items__c = 10, Total_Taxes__c= 1500, of_Line_Items__c= 10, Chargeable_Weight__c = 200,
        CI_Admin_Fee__c  =100, CI_Bank_Fees__c  = 100, CI_Finance_Fee__c  = 100, CI_Insurance_Fee_USD__c  = 100, CI_International_Delivery_Fee__c  = 100, CI_IOR_and_Import_Compliance_Fee_USD__c  = 100, 
        CI_Miscellaneous_Fee__c  = 100, CI_Recharge_Tax_and_Duty__c  = 100, CI_Total_Clearance_Costs__c  = 100, CI_Total_Customs_Brokerage_Cost__c  = 100, CI_Total_Handling_Cost__c  = 100, 
        CI_Total_Licence_Cost__c  = 100, FC_Admin_Fee__c  = 100, FC_Bank_Fees__c  = 100, FC_Finance_Fee__c  = 100, FC_Insurance_Fee_USD__c   = 100, FC_International_Delivery_Fee__c  = 100, 
        FC_IOR_and_Import_Compliance_Fee_USD__c  = 100, FC_Miscellaneous_Fee__c  = 100, FC_Recharge_Tax_and_Duty__c  = 100, FC_Total_Clearance_Costs__c  = 100, 
        FC_Total_Customs_Brokerage_Cost__c  = 100, FC_Total_Handling_Costs__c  = 100, FC_Total_License_Cost__c  = 100, Total_CIF_Duties__c = 0, Total_FOB_Duties__c = 0);
        insert shipmentOrder2; 
     
        Customer_Invoice__c customerInvoice = new Customer_Invoice__c(Client__c = account.id, Shipment_Order__c = shipmentOrder.Id, Invoice_Amount__c = 2000);
        insert customerInvoice; 
        
        Customer_Invoice__c customerInvoice1 = new Customer_Invoice__c(Client__c = account.id, Shipment_Order__c = shipmentOrder2.Id, Invoice_Amount__c = 2000, Invoice_Type__c = 'Top-up Invoice');
        insert customerInvoice1; 
              
 
        test.startTest();
       
        PageReference pageRef = Page.customerInvoicePDF2;
        
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('id',customerInvoice.id);

        ApexPages.StandardController stdCon= new ApexPages.StandardController(customerInvoice);
        attachNewCustomerInvoicePDF con = new attachNewCustomerInvoicePDF (stdCon);
        con.attachPDF();  
        
        
        
        
       
        PageReference pageRef1 = Page.NewCustomerInvoice;
        
        Test.setCurrentPage(pageRef1);
        pageRef1.getParameters().put('id',customerInvoice.id);

        ApexPages.StandardController stdCon1= new ApexPages.StandardController(customerInvoice);
        attachNewCustomerInvoicePDF con1 = new attachNewCustomerInvoicePDF(stdCon1);
        con1.attachPDF();  
        
        
        
        
        PageReference pageRef2 = Page.customerInvoicePDF2;
        
        Test.setCurrentPage(pageRef2);
        pageRef2.getParameters().put('id',customerInvoice1.id);

        ApexPages.StandardController stdCon2 = new ApexPages.StandardController(customerInvoice1);
        attachNewCustomerInvoicePDF con2 = new attachNewCustomerInvoicePDF (stdCon2);
        con2.attachPDF();  
        
        
       
        PageReference pageRef3 = Page.NewCustomerInvoice;
        
        Test.setCurrentPage(pageRef3);
        pageRef3.getParameters().put('id',customerInvoice1.id);

        ApexPages.StandardController stdCon3= new ApexPages.StandardController(customerInvoice1);
        attachNewCustomerInvoicePDF con3 = new attachNewCustomerInvoicePDF(stdCon3);
        con3.attachPDF();  
        

       
       
      
       
        test.stopTest();

     
       
    } 

   }