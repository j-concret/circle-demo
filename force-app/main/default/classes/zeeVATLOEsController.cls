public class zeeVATLOEsController {
    
    public Registrations__c registrations {get;set;}
    public Shipment_Order__c shipmentOrder {get;set;}
    public Customs_Clearance_Documents__c customClearanceDocument {get;set;}
    public CPA_v2_0__c cpa {get;set;}    
    
    public zeeVATLOEsController(ApexPages.StandardController controller){
        
        this.shipmentOrder =  [ SELECT Id, Account__c, Destination__c FROM Shipment_Order__c 
                                WHERE Id = :ApexPages.currentPage().getParameters().get('id') LIMIT 1];
        
        if(shipmentOrder.Account__c != null && shipmentOrder.Destination__c != null){
            this.registrations = [Select Id, Name, Registered_Address__c, Registered_Address_2__c, 
                                  Registered_Address_City__c, Registered_Address_Province__c, 
                                  Registered_Address_Postal_Code__c, Registered_Address_Country__c, 
                                  VAT_number__c From Registrations__c Where Company_name__c = :shipmentOrder.Account__c AND Country__c =:shipmentOrder.Destination__c LIMIT 1];
            
             List<Customs_Clearance_Documents__c> ccd= [Select Date_of_clearance__c, Customs_Reference_Number__c from Customs_Clearance_Documents__c Where Customs_Clearance_Documents__c =:shipmentOrder.Id LIMIT 1];
            if(!ccd.isEmpty())
            this.customClearanceDocument = ccd[0];
        }
        
    }

}