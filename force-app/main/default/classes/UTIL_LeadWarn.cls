public class UTIL_LeadWarn 
{
	public static String get(String name)
    {
        String returnValue = null;
        
        if (String.isBlank(name))
			return returnValue;
			        
        final Map<String,Lead_Limit_Warn_Factor__c> allVariables = Lead_Limit_Warn_Factor__c.getAll();
        
        if(!allVariables.isEmpty() && allVariables.get(name) != null)
            returnValue = allVariables.get(name).Factor__c;
        
        return returnValue;
    }
    
    /*
    * @description Retrieve a global variable, if the variable doesn't exist, set it to the defaultvalue given.
    * param name The name of the variable to be retrieved
    * param defaultValue In the event that the variable doesn't exist, set it to the default value
    */
    public static String get(String name, String defaultValue)
    {
    	String returnValue = get(name);
    	if (returnValue == null)
    	{
    		returnValue = defaultValue;
    		put(name, defaultValue);
    	}
    	
    	return returnValue;
    }
    
    public static String put(String name, String value)
    {
        String returnValue = null; //the previous value
        
        if (String.isNotBlank(name) && String.isNotBlank(name.trim()))
        {
            Lead_Limit_Warn_Factor__c record = null;
            try
            {
                record = 
                [   
                    SELECT 
                    	Id, 
                    	Factor__c 
                    FROM Lead_Limit_Warn_Factor__c 
                    WHERE Name = :name
                ];
            }
            catch(QueryException e)
            {
            	
            }
            
            if(record == null)
                record = new Lead_Limit_Warn_Factor__c(Name = name, Factor__c = value);
            else 
                returnValue = record.Factor__c;
            
            record.Factor__c = value;
            
            if(record.id != null)
                update record;
            else 
                insert record;
        }
        
        return returnValue;
    } 
}