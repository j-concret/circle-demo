@isTest
private class Automated_Supplier_Email_Test_Class {
    public static String CRON_EXP = '0 0 0 15 3 ? 2022';
    static testmethod void test1()
    {
    	Test.startTest();
        String jobId = System.schedule('ScheduleApexClassTest', CRON_EXP, new Automated_Supplier_Email_Schedule());
        CronTrigger ct = [SELECT Id,CronExpression,TimesTriggered,NextFireTime FROM CronTrigger WHERE id = :jobId];
        System.assertEquals(CRON_EXP, ct.CronExpression);
        System.assertEquals(0, ct.TimesTriggered);
        System.assertEquals('2022-03-15 00:00:00', String.valueOf(ct.NextFireTime));
        Test.stopTest();
    }
    static testMethod void setUpData(){
    	Account account = new Account (Name='TecEx Prospective Client', Type='Client', CSE_IOR__c='0050Y000001LTZO', Service_Manager__c='0050Y000001LTZO', Financial_Manager__c='0050Y000001LTZO', Financial_Controller__c='0050Y000001LTZO', Tax_recovery_client__c=FALSE, Ship_From_Line_1__c='Ship_From_Line_1', Ship_From_Line_2__c='Ship_From_Line_2', Ship_From_Line_3__c='Ship_From_Line_3', Ship_From_City__c='Ship_From_City', Ship_From_Zip__c='Ship_From_Zip', Ship_From_Country__c='Ship_From_Country', Ship_From_Other_notes__c='Ship_From_Other', Ship_From_Contact_Name__c='Ship_From_Contact', Ship_From_Contact_Email__c='Ship_From_Email', Ship_From_Contact_Tel__c='Ship_From_Tel', Client_Address_Line_1__c='Client_Address_Line_1', Client_Address_Line_2__c='Client_Address_Line_2', Client_Address_Line_3__c='Client_Address_Line_3', Client_City__c='Client_City', Client_Zip__c='Client_Zip', Client_Country__c='Client_Country', Other_notes__c='Other_notes', Tax_Name__c='Tax_Name', Tax_ID__c='Tax_ID', Contact_Name__c='Contact_Name', Contact_Email__c='Contact_Email', Contact_Tel__c='Contact_Tel');
        insert account;
        Account account2 = new Account (Name='Test Supplier', Type='Supplier', ICE__c='0050Y000001km5c', RecordTypeId='0120Y0000006KjQQAU');
        insert account2;
        Contact contact = new Contact (LastName='Testing Individual', AccountId=account.Id, Include_in_invoicing_emails__c=TRUE, Email='anilshowreddy@gmail.com');
        insert contact;
        Contact contact2 = new Contact (LastName='Testing supplier', AccountId=account2.Id, Include_in_invoicing_emails__c=TRUE, Email='anilshowreddy@gmail.com');
        insert contact2;
        IOR_Price_List__c iorpl = new IOR_Price_List__c (Client_Name__c=account.Id, Name='Brazil', IOR_Fee__c=0.15, On_Charge_Mark_up__c=0.15, TecEx_Shipping_Fee_Markup__c=0.40, IOR_Min_Fee__c=100, Admin_Fee__c=100, Set_up_fee__c=100, Bank_Fees__c=200, Tax_Rate__c=0.20, Estimated_Customs_Brokerage__c=250, Estimated_Customs_Clearance__c=250, Estimated_Customs_Handling__c=250, Estimated_Import_License__c=250, Destination__c='Brazil');
        insert iorpl;
        country_price_approval__c cpa = new country_price_approval__c(Name='Brazil', Billing_term__c='DAP/CIF - IOR pays', Airwaybill_Instructions__c='User IOR Address', Supplier__c=account2.Id, In_Country_Specialist__c='0050Y000001km5c', Clearance_Destination__c='Brazil', Preferred_Supplier__c=TRUE, Destination__c='Brazil');
        insert cpa;
        Currency_Management2__c CM = new Currency_Management2__c (Name='US Dollar (USD)', Currency__c='US Dollar (USD)', ISO_Code__c='USD', Conversion_Rate__c=1);
        insert CM;
        Country__c country = new Country__c (Brokerage_Costs__c=618, CIF_Adjustment_Factor__c=0.035, CIF_Absolute_value_adjustment__c=200, Clearance_Costs__c=488, Country__c='Brazil', Name='Brazil', Handling_Costs__c=1180, License_Permit_Costs__c=0);
        insert country;
        Tax_Structure_Per_Country__c tax = new Tax_Structure_Per_Country__c (Applied_to_Value__c='CIF', Country__c=country.Id, Default_Duty_Rate__c=0.5625, Order_Number__c='1', Part_Specific__c=TRUE, Name='II', Tax_Type__c='Duties');
        insert tax;
        CPA_v2_0__c RelatedCPA = new CPA_v2_0__c (Name='Brazil', Country_Applied_Costings__c=TRUE);
        insert RelatedCPA;
		CPA_v2_0__c cpav2 = new CPA_v2_0__c (Name='Brazil Testing 2.0', Supplier__c=account2.Id, Service_Type__c='IOR/EOR', Related_Costing_CPA__c='a260Y00000EnDjr', Preferred_Supplier__c=TRUE, VAT_Rate__c=0.1, Destination__c='Brazil', VAT_Reclaim_Destination__c=FALSE, Final_Destination__c='Brazil', Country__c=country.id, 
                                             Default_Section_1_Address_Line_1__c='Address_Line_1',
                                             Default_Section_1_Address_Line_2__c='Address_Line_2',
                                             Default_Section_1_Address_Line_3__c='Address_Line_3',
                                             Default_Section_1_City__c='City',
                                             Default_Section_1_Zip__c='Zip',
                                             Default_Section_1_Country__c='Country',
                                             Default_Section_1_Other__c='Other',
                                             Default_Section_1_Tax_Name__c='Tax_Name',
                                             Default_Section_1_Tax_Id__c='Tax_Id',
                                             Default_Section_1_Contact_Name__c='Contact_Name',
                                             Default_Section_1_Contact_Email__c='Contact_Email',
                                             Default_Section_1_Contact_Tel__c='Contact_Tel',
                                             Alternate_Section_1_Address_Line_1__c='Address_Line_1',
                                             Alternate_Section_1_Address_Line_2__c='Address_Line_2',
                                             Alternate_Section_1_Address_Line_3__c='Address_Line_3',
                                             Alternate_Section_1_City__c='City',
                                             Alternate_Section_1_Zip__c='Zip',
                                             Alternate_Section_1_Country__c='Country',
                                             Alternate_Section_1_Other__c='Other',
                                             Alternate_Section_1_Tax_Name__c='Tax_Name',
                                             Alternate_Section_1_Tax_Id__c='Tax_Id',
                                             Alternate_Section_1_Contact_Name__c='Contact_Name',
                                             Alternate_Section_1_Contact_Email__c='Contact_Email',
                                             Alternate_Section_1_Contact_Tel__c='Contact_Tel',
                                             Default_Section_2_Address_Line_1__c='Address_Line_1',
                                             Default_Section_2_Address_Line_2__c='Address_Line_2',
                                             Default_Section_2_Address_Line_3__c='Address_Line_3',
                                             Default_Section_2_City__c='City',
                                             Default_Section_2_Zip__c='Zip',
                                             Default_Section_2_Country__c='Country',
                                             Default_Section_2_Other__c='Other',
                                             Default_Section_2_Tax_Name__c='Tax_Name',
                                             Default_Section_2_Tax_Id__c='Tax_Id',
                                             Default_Section_2_Contact_Name__c='Contact_Name',
                                             Default_Section_2_Contact_Email__c='Contact_Email',
                                             Default_Section_2_Contact_Tel__c='Contact_Tel',
                                             Alternate_Section_2_Address_Line_1__c='Address_Line_1',
                                             Alternate_Section_2_Address_Line_2__c='Address_Line_2',
                                             Alternate_Section_2_Address_Line_3__c='Address_Line_3',
                                             Alternate_Section_2_City__c='City',
                                             Alternate_Section_2_Zip__c='Zip',
                                             Alternate_Section_2_Country__c='Country',
                                             Alternate_Section_2_Other__c='Other',
                                             Alternate_Section_2_Tax_Name__c='Tax_Name',
                                             Alternate_Section_2_Tax_Id__c='Tax_Id',
                                             Alternate_Section_2_Contact_Name__c='Contact_Name',
                                             Alternate_Section_2_Contact_Email__c='Contact_Email',
                                             Alternate_Section_2_Contact_Tel__c='Contact_Tel',
                                             Default_Section_3_Address_Line_1__c='Address_Line_1',
                                             Default_Section_3_Address_Line_2__c='Address_Line_2',
                                             Default_Section_3_Address_Line_3__c='Address_Line_3',
                                             Default_Section_3_City__c='City',
                                             Default_Section_3_Zip__c='Zip',
                                             Default_Section_3_Country__c='Country',
                                             Default_Section_3_Other__c='Other',
                                             Default_Section_3_Tax_Name__c='Tax_Name',
                                             Default_Section_3_Tax_Id__c='Tax_Id',
                                             Default_Section_3_Contact_Name__c='Contact_Name',
                                             Default_Section_3_Contact_Email__c='Contact_Email',
                                             Default_Section_3_Contact_Tel__c='Contact_Tel',
                                             Alternate_Section_3_Address_Line_1__c='Address_Line_1',
                                             Alternate_Section_3_Address_Line_2__c='Address_Line_2',
                                             Alternate_Section_3_Address_Line_3__c='Address_Line_3',
                                             Alternate_Section_3_City__c='City',
                                             Alternate_Section_3_Zip__c='Zip',
                                             Alternate_Section_3_Country__c='Country',
                                             Alternate_Section_3_Other__c='Other',
                                             Alternate_Section_3_Tax_Name__c='Tax_Name',
                                             Alternate_Section_3_Tax_Id__c='Tax_Id',
                                             Alternate_Section_3_Contact_Name__c='Contact_Name',
                                             Alternate_Section_3_Contact_Email__c='Contact_Email',
                                             Alternate_Section_3_Contact_Tel__c='Contact_Tel',
                                             Default_Section_4_Address_Line_1__c='Address_Line_1',
                                             Default_Section_4_Address_Line_2__c='Address_Line_2',
                                             Default_Section_4_Address_Line_3__c='Address_Line_3',
                                             Default_Section_4_City__c='City',
                                             Default_Section_4_Zip__c='Zip',
                                             Default_Section_4_Country__c='Country',
                                             Default_Section_4_Other__c='Other',
                                             Default_Section_4_Tax_Name__c='Tax_Name',
                                             Default_Section_4_Tax_Id__c='Tax_Id',
                                             Default_Section_4_Contact_Name__c='Contact_Name',
                                             Default_Section_4_Contact_Email__c='Contact_Email',
                                             Default_Section_4_Contact_Tel__c='Contact_Tel',
                                             Alternate_Section_4_Address_Line_1__c='Address_Line_1',
                                             Alternate_Section_4_Address_Line_2__c='Address_Line_2',
                                             Alternate_Section_4_Address_Line_3__c='Address_Line_3',
                                             Alternate_Section_4_City__c='City',
                                             Alternate_Section_4_Zip__c='Zip',
                                             Alternate_Section_4_Country__c='Country',
                                             Alternate_Section_4_Other__c='Other',
                                             Alternate_Section_4_Tax_Name__c='Tax_Name',
                                             Alternate_Section_4_Tax_Id__c='Tax_Id',
                                             Alternate_Section_4_Contact_Name__c='Contact_Name',
                                             Alternate_Section_4_Contact_Email__c='Contact_Email',
                                             Alternate_Section_4_Contact_Tel__c='Contact_Tel'
                                            );
        insert cpav2;
        List<CPARules__c> CPARules = new List<CPARules__c>();
        CPARules.add(new CPARules__c (Country__c='Brazil', Criteria__c='Default', Chargable_weight__c='NONE', Courier_Responsibility__c='NONE', ECCN_No__c='NONE', NL_Product_User__c='NONE', Number_of_final_deliveries__c='NONE', ShipmentValue__c='NONE', Sum_of_quantity_of_matched_products__c='NONE', Manufacturer__c='NONE', DefaultCPA2__c=cpav2.Id, CPA2__c=cpav2.Id));
        insert CPARules;
		List<CPA_Costing__c> costs = new List<CPA_Costing__c>();
        costs.add(new CPA_Costing__c (Cost_Category__c='IOR Cost', CPA_v2_0__c=cpav2.Id, Applied_to__c='Shipment value', Cost_Type__c='Variable', Rate__c=10, IOR_EOR__c='IOR', Min__c=375, Max__c=14000, Conditional_value__c='Shipment value', Condition__c='Between', Floor__c=0, Ceiling__c=10000, Currency__c='US Dollar (USD)'));
        costs.add(new CPA_Costing__c (Cost_Category__c='IOR Cost', CPA_v2_0__c=cpav2.Id, Applied_to__c='Anil', Cost_Type__c='Variable', Rate__c=10, IOR_EOR__c='IOR', Currency__c='US Dollar (USD)', Variable_threshold__c=null));
        insert costs;
        Date myDate = Date.today();
     	Time myTime = Time.newInstance(8, 0, 0, 0);
    	DateTime dt1 = DateTime.newInstanceGMT(myDate, myTime);
		Shipment_Order__c shipmentOrder = new Shipment_Order__c (Account__c=account.id, Shipment_Value_USD__c=10000, Shipping_status__c='POD Received', SupplierlU__c=account2.Id, Actual_Total__c=0.00, 
                                                                 Client_Contact_for_this_Shipment__c=contact.Id, Who_arranges_International_courier__c='Client', Tax_Treatment__c='DAP/CIF - IOR pays', Ship_to_Country__c=cpa.Id, 
                                                                 Destination__c='Brazil', Service_Type__c='IOR', IOR_Price_List__c=iorpl.Id, of_Unique_Line_Items__c=10, Total_Taxes__c=1500, of_Line_Items__c=10, Chargeable_Weight__c=200, of_packages__c=20, Final_Deliveries_New__c=10, 
                                                                 CI_Admin_Fee__c=100, CI_Bank_Fees__c=100, CI_Finance_Fee__c=100, CI_Insurance_Fee_USD__c=100, CI_International_Delivery_Fee__c=100, CI_IOR_and_Import_Compliance_Fee_USD__c=100, 
                                                                 CI_Miscellaneous_Fee__c=100, CI_Recharge_Tax_and_Duty__c=100, CI_Total_Clearance_Costs__c=100, CI_Total_Customs_Brokerage_Cost__c=100, CI_Total_Handling_Cost__c=100, 
                                                                 CI_Total_Licence_Cost__c=100, FC_Admin_Fee__c=100, FC_Bank_Fees__c=100, FC_Finance_Fee__c=100, FC_Insurance_Fee_USD__c=100, FC_International_Delivery_Fee__c=100, 
                                                                 FC_IOR_and_Import_Compliance_Fee_USD__c=100, FC_Miscellaneous_Fee__c=100, FC_Recharge_Tax_and_Duty__c=100, FC_Total_Clearance_Costs__c=100, 
                                                                 FC_Total_Customs_Brokerage_Cost__c=100, FC_Total_Handling_Costs__c=100, FC_Total_License_Cost__c=100, Total_CIF_Duties__c=0, Total_FOB_Duties__c=0, Actual_Admin_Fee__c=0, 
                                                                 Actual_Bank_Fees__c=0, Actual_Exchange_gain_loss_on_payment__c=0, Actual_Finance_Fee__c=0, Actual_Insurance_Fee_USD__c=0, Actual_International_Delivery_Fee__c=0, 
                                                                 Actual_Miscellaneous_Fee__c=0, Actual_Total_Clearance_Costs__c=0, Actual_Total_Customs_Brokerage_Cost__c=0, Actual_Total_Handling_Costs__c=0, Actual_Total_IOR_EOR__c=0, 
                                                                 Actual_Total_License_Cost__c=0, Actual_Total_Tax_and_Duty__c=0, POD_Date__c=Date.newInstance(2016, 12, 9), CreatedDate=Date.newInstance(2020, 02, 05), Final_Supplier_Invoice_Received__c=FALSE
                                                                );
        insert shipmentOrder;
        shipmentOrder.Tax_Treatment__c = 'DAP/CIF - IOR pays';
        Update shipmentOrder;
        list<Shipment_Order__c> SOList = new List<Shipment_Order__c>();
        SOList.add(shipmentOrder);
        String SuupplierID=shipmentOrder.Account__c;
        String Email = 'anilshowreddy@gmail.com';
        Automated_Supplier_Email.sendEmailSummary(SOList,SuupplierID,Email);
	}
}