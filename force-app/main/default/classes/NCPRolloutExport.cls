public with sharing class NCPRolloutExport {
    
    
    //public Blob csvFileBody{get;set;}
    //public string csvAsString{get;set;}
    //public String[] csvFileLines{get;set;}
    Public Roll_Out__c rollout = new Roll_Out__c ();
    public List<Shipment_Order__c> shipmentOrder{get;set;}
    //public List<cShipmentOrder> costList {get; set;}
    //public Boolean hasSelAcct {get;set;}
    
    
    
    public NCPRolloutExport (ApexPages.StandardController controller){
        
        this.rollOut =  [Select Id, Name, Client_Name__c, RecordTypeId, Create_Roll_Out__c, Destinations__c, Shipment_Value_in_USD__c, Contact_For_The_Quote__c, File_Reference_Name__c  FROM Roll_Out__c WHERE Id =:ApexPages.currentPage().getParameters().get('id') Limit 1];
        this.shipmentOrder= [ Select Id, EOR_and_Export_Compliance_Fee_USD__c, Total_Taxes__c, of_Line_Items__c, Total_Cost_Range_Max__c, Total_Cost_Range_Min__c, Maximum_tax_range__c, Minimum_tax_range__c, Cost_Estimate_Number_HyperLink__c,Final_Deliveries_New__c, Who_arranges_International_courier__c, Total_Invoice_Amount__c,  Shipping_Status__c, Shipment_Value_USD__c, Name, IOR_Price_List__c, Final_Delivery_Address__c, Delivery_Contact_Tel_number__c, 
                             Delivery_Contact_Name__c, Account__c, RecordType.Name, RecordTypeId, IOR_Price_List__r.Name, Recharge_Tax_and_Duty__c, IOR_FEE_USD__c, Handling_and_Admin_Fee__c, Bank_Fees__c, Total_Customs_Brokerage_Cost__c, Total_clearance_Costs__c, Recharge_Handling_Costs__c, 
                             Recharge_License_Cost__c, International_Delivery_Fee__c, Insurance_Fee_USD__c,  Estimate_Transit_Time_Formula__c, Estimate_Customs_Clearance_Time_Formula__c, Shipping_Notes_Formula__c, Shipping_Notes_New__c, Account__r.Cash_outlay_fee_base__c,
                             Account__r.Finance_Charge__c, All_Taxes_and_Duties_Summary__c, Government_and_In_country_Summary__c, Chargeable_Weight__c, Tax_Cost__c FROM Shipment_Order__c where Roll_Out__c = :ApexPages.currentPage().getParameters().get('id')];
        
        
        //csvFileLines = new String[]{};
        
        
    }
    
}