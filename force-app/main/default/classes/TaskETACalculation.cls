public class TaskETACalculation {

    public static Map<String,TaskWrapper> existingTasks = new Map<String,TaskWrapper>();
    public static Map<Id,List<Id> > prerequisiteMaster = new Map<Id,List<Id> >();
    public static Map<Id,List<Id> > dependentMaster = new Map<Id,List<Id> >();
    public static Map<Id,Decimal> ETAOfCriticalMap = new Map<Id,Decimal>();
	public static Map<Id,Task> tasktoUpdate = new Map<Id,Task>();
    
    public static Map<String,ETAWrapper> calculate(List<Id> soIds) {
        System.debug('Calculating ETA');
        List<String> excludeState = new List<String> {'Resolved','Not Applicable'};
        Set<Id> masterTaskIds = new Set<Id>();

        for(Task extTask : [SELECT Id,Subject,TecEx_Pending_ETA_days__c,Slack__c,Under_Review_ETA_days__c,State__c,whatId,Manually_Override_Inactive_Field__c,Master_Task__r.Set_Default_to_Inactive__c,Master_Task__c,Inactive__c,Blocks_Approval_to_Ship__c FROM Task WHERE whatId IN:soIds AND State__c NOT IN:excludeState AND Master_Task__c != null AND IsNotApplicable__c = false AND Blocks_Approval_to_Ship__c = true ORDER BY Subject desc]) {
            masterTaskIds.add(extTask.Master_Task__c);
            existingTasks.put(extTask.Master_Task__c+'-'+extTask.whatId,new TaskWrapper(extTask));
            extTask.Slack__c = null;
            tasktoUpdate.put(extTask.Id,extTask);
        }


        for(Master_Task_Prerequisite__c mstPre : [SELECT id,Name, Master_Task_Dependent__c, Master_Task_Prerequisite__c FROM Master_Task_Prerequisite__c WHERE Master_Task_Dependent__c IN: masterTaskIds]) {

            if(prerequisiteMaster.containsKey(mstPre.Master_Task_Dependent__c))
                prerequisiteMaster.get(mstPre.Master_Task_Dependent__c).add(mstPre.Master_Task_Prerequisite__c);
            else
                prerequisiteMaster.put( mstPre.Master_Task_Dependent__c,new List<Id> {mstPre.Master_Task_Prerequisite__c});

            if(dependentMaster.containsKey(mstPre.Master_Task_Prerequisite__c))
                dependentMaster.get(mstPre.Master_Task_Prerequisite__c).add(mstPre.Master_Task_Dependent__c);
            else
                dependentMaster.put( mstPre.Master_Task_Prerequisite__c,new List<Id> {mstPre.Master_Task_Dependent__c});
        }

        //calculation for early start and early finish
        for(String key : existingTasks.keySet()) {
            String masterId = key.split('-')[0];
            String whatId = key.split('-')[1];
            if(existingTasks.get(key).earlyFinish == null) {
                Decimal maxEarlyFinish = calculatePre(masterId, whatId);
                if(!ETAOfCriticalMap.containsKey(whatId) || (ETAOfCriticalMap.containsKey(whatId) && ETAOfCriticalMap.get(whatId) < maxEarlyFinish))
                    ETAOfCriticalMap.put(whatId,maxEarlyFinish);
            }
        }

        //calculation for late finish and late start
        for(String key : existingTasks.keySet()) {
            String masterId = key.split('-')[0];
            String whatId = key.split('-')[1];
            if(existingTasks.get(key).lateStart == null) {
                Decimal maxEarlyFinish = calculateDepe(masterId, whatId);
                
            }
        }
		
        Map<String,ETAWrapper> soETA = new Map<String,ETAWrapper>();
        //fetching ETA from critical path
        System.debug('existingTasks');
        System.debug(JSON.serialize(existingTasks));
        for(String key : existingTasks.keySet()) {
            String masterId = key.split('-')[0];
            String whatId = key.split('-')[1];
            TaskWrapper tskWrap = existingTasks.get(key);

            if(tskWrap.slack == 0) {
                if(soETA.containsKey(whatId)) {
                    if(soETA.get(whatId).eta < tskWrap.earlyFinish) soETA.get(whatId).eta = tskWrap.earlyFinish;
                    if(!soETA.get(whatId).halt && !tskWrap.hasPre) soETA.get(whatId).halt = tskWrap.state == 'Client Pending' || tskWrap.inActive;
                }
                else{
                    soETA.put(whatId,new ETAWrapper(tskWrap.earlyFinish, (!tskWrap.hasPre && (tskWrap.state == 'Client Pending' || tskWrap.inActive))));
                }
            }
        }
        System.debug(JSON.serialize(existingTasks));
        // System.debug(JSON.serialize(soETA));
        RecusrsionHandler.isTaskTriggerSkip = true;
        update tasktoUpdate.values();
        
        return soETA;
    }

    public static Decimal calculateDepe(String masterId,String whatId){
        String key = masterId+'-'+whatId;
        if(existingTasks.containsKey(key)) {
            TaskWrapper tskWrap = existingTasks.get(key);
            tskWrap.lateFinish = ETAOfCriticalMap.get(whatId);
            if(dependentMaster.containsKey(masterId)) {
                Decimal minFinish;
                for(Id depId : dependentMaster.get(masterId)) {
                    Decimal mn = calculateDepe(depId,whatId);
                    if(minFinish > mn || minFinish == null) minFinish = mn;
                }
                tskWrap.lateFinish = minFinish;
            }
            //roundoff
            Decimal decTask = tskWrap.taskETA;
        	//Integer taskETARounded = Integer.valueOf((decTask).round(System.RoundingMode.UP));
            tskWrap.lateStart = tskWrap.lateFinish - decTask;
            tskWrap.slack = tskWrap.lateFinish - tskWrap.earlyFinish;
            
            /** Start update slack value for ETA Countdown **/
            Task tks = new Task(id = tskWrap.Id,Slack__c = tskWrap.slack);
            
           	tasktoUpdate.put(tks.Id,tks);
            //update tks;
            
            /** END update slack value for ETA Countdown **/
            
            return tskWrap.lateStart;
        }
        return 0;
    }

    public static Decimal calculatePre(String masterId,String whatId){
        String key = masterId+'-'+whatId;
        TaskWrapper tskWrap = existingTasks.get(key);
        if(prerequisiteMaster.containsKey(masterId)) {
            Decimal maxFinish = 0;
            for(Id preId : prerequisiteMaster.get(masterId)) {
                if(existingTasks.containsKey(preId+'-'+whatId)) {
                    tskWrap.hasPre = true;
                    if(!tskWrap.manulOverride && !tskWrap.inActive) {
                        tskWrap.inActive = true;
                    }
                    Decimal mx = calculatePre(preId,whatId);
                    if(maxFinish < mx) maxFinish = mx;
                }
            }
            tskWrap.earlyStart = maxFinish;
        }
        //roundoff
        Decimal decTask = tskWrap.taskETA;
        //Integer taskETARounded = Integer.valueOf((decTask).round(System.RoundingMode.UP));
        tskWrap.earlyFinish = tskWrap.earlyStart + decTask;
        return tskWrap.earlyFinish;
    }

    public class ETAWrapper {
        public Decimal eta;
        public Boolean halt;
        public ETAWrapper(Decimal eta, Boolean halt){
            this.eta = eta;
            this.halt = halt;
        }
    }

    public class TaskWrapper {
        public Decimal earlyStart;
        public Decimal earlyFinish;
        public Decimal lateFinish;
        public Decimal lateStart;
        public Decimal slack;
        public Double taskETA;
        public String state;
        public String Id;
        public String subject;
        public Boolean inActive;
        public Boolean manulOverride;
        public Boolean hasPre;
        //public Task task;
        Map<String,Integer> statePriority = new Map<String,Integer>{'Not Started'=>0,'TecEx Pending'=>1,'Client Pending'=>2,'Under Review'=>3};

        public TaskWrapper(Task task){
            this.subject = task.Subject;
            this.Id = task.Id;
            this.hasPre = false;
            this.earlyStart = 0;
            this.state = task.State__c;
            this.inActive = task.Manually_Override_Inactive_Field__c ? task.Inactive__c : task.Master_Task__r.Set_Default_to_Inactive__c;
            this.manulOverride = task.Manually_Override_Inactive_Field__c;
            //this.task = task;
            if(statePriority.get(this.state) <=1)
              this.taskETA = Double.valueOf(task.TecEx_Pending_ETA_days__c+task.Under_Review_ETA_days__c);
            else
              this.taskETA = Double.valueOf(task.Under_Review_ETA_days__c);
        }
    }
}