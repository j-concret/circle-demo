@IsTest
public class ProcessRollOutSOBatch_Test {

    @testSetup
    static void setupData(){
        User userObj = [Select Id, Name, userName, Profile.Name, UserRole.name From user where Profile.Name = 'System Administrator' LIMIT 1];
		Id EORRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('IOR/EOR Supplier').getRecordTypeId();
         
        CountryandRegionMap__c crm = new CountryandRegionMap__c();
            crm.Name = 'Brazil';
            crm.Country__c = 'Brazil';
            crm.European_Union__c = 'No';
            crm.Region__c = 'South America';
            
            insert crm;
            
            CountryandRegionMap__c crm1 = new CountryandRegionMap__c();
            crm1.Name = 'United States';
            crm1.Country__c = 'United States';
            crm1.European_Union__c = 'No';
            crm1.Region__c = 'Oceania';
            
            insert crm1;
            
            DefaultFreightValues__c df = new DefaultFreightValues__c();
            df.Name = 'Test';
            df.Courier_Base_Rate_KG__c = 12;
            df.Courier_Dangerous_Goods_premium__c = 12;
            df.Courier_Fixed_Charge__c = 12;
            df.Courier_Non_stackable_premium__c = 12;
            df.Courier_Oversized_premium__c = 12;
            df.FF_Base_Rate_KG__c = 12;
            df.FF_Dangerous_Goods_Fixed_Charge_Premium__c = 12;
            df.FF_Dangerous_Goods_premium__c = 12;
            df.FF_Dedicated_Pickup__c = 12;
            df.FF_Fixed_Charge__c = 12;
            df.FF_Non_stackable_Fixed_Charge_Premium__c = 12;
            df.FF_Non_stackable_premium__c = 12;
            df.FF_Oversized_Fixed_Charge_Premium__c = 12;
            df.FF_Oversized_premium__c = 12;
            insert df;
            
            RegionalFreightValues__c rfm = new RegionalFreightValues__c();
            rfm.Name = 'Oceania to South America';
            rfm.Courier_Discount_Premium__c = 12;
            rfm.Courier_Fixed_Premium__c = 10;
            rfm.Destination__c = 'Brazil';
            rfm.FF_Fixed_Premium__c = 123;
            rfm.FF_Regional_Discount_Premium__c = 12;
            rfm.Origin__c = 'United States';
            rfm.Preferred_Courier_Id__c = 'test';
            rfm.Preferred_Courier_Name__c = 'test';
            rfm.Preferred_Freight_Forwarder_Id__c = 'test';
            rfm.Preferred_Freight_Forwarder_Name__c = 'Fedex';
            rfm.Risk_Adjustment_Factor__c = 1;
            insert rfm;
            
        //Account record
        Account account = new Account (name='Acme1', 
                                       Type ='Client', 
                                       CSE_IOR__c = userObj.Id, 
                                       Service_Manager__c = userObj.Id, 
                                       Tax_recovery_client__c  = FALSE);
         
        
        Account account2 = new Account (name='Test Supplier', Type ='Supplier', ICE__c = userObj.Id, RecordTypeId = EORRecordTypeId);
        Account account3 = new Account (name='TecEx Prospective Client', Type ='Client',New_Invoicing_Structure__c = true);
        
        List<Account> acclist = new List<Account>{account,account2,account3};
        insert acclist;    
            
        Contact contact = new Contact ( lastname ='Testing Individual',  AccountId = acclist[0].Id,Client_Notifications_Choice__c='Opt-In');
        insert contact;  
        
        IOR_Price_List__c iorpl = new IOR_Price_List__c ( Client_Name__c = acclist[0].Id, Name = 'Brazil', IOR_Fee__c = 0.15, On_Charge_Mark_up__c = 0.15,
                                                         TecEx_Shipping_Fee_Markup__c =0.40, IOR_Min_Fee__c = 100,  Admin_Fee__c =100, Set_up_fee__c =100, 
                                                         Bank_Fees__c =200, Tax_Rate__c = 0.20,Estimated_Customs_Brokerage__c =250, Estimated_Customs_Clearance__c =250,
                                                         Estimated_Customs_Handling__c =250, Estimated_Import_License__c =250, Destination__c = 'Brazil' );
        insert iorpl; 
        
        country_price_approval__c cpa = new country_price_approval__c( name ='Brazil', Billing_term__c = 'DAP/CIF - IOR pays',  Airwaybill_Instructions__c = 'User IOR Address', Supplier__c = acclist[1].Id, 
                                                                      In_Country_Specialist__c = userObj.Id, Clearance_Destination__c = 'Brazil', Preferred_Supplier__c = TRUE, Destination__c = 'Brazil' );
        insert cpa;
        
        Currency_Management2__c CM = new Currency_Management2__c (Name = 'US Dollar (USD)', Currency__c = 'US Dollar (USD)', ISO_Code__c = 'USD', Conversion_Rate__c = 1);
        insert CM; 
        
        Country__c country = new Country__c(Brokerage_Costs__c = 618, CIF_Adjustment_Factor__c = 0.035, CIF_Absolute_value_adjustment__c = 200,
                                            Clearance_Costs__c = 488,Country__c = 'Brazil', Name = 'Brazil', Handling_Costs__c = 1180, License_Permit_Costs__c = 0);
        insert country;
        
        Tax_Structure_Per_Country__c tax = new Tax_Structure_Per_Country__c(Applied_to_Value__c = 'CIF', Country__c = country.Id, Default_Duty_Rate__c = 0.5625 , 
                                                                            Order_Number__c = '1', Part_Specific__c = TRUE, Name = 'II', Tax_Type__c = 'Duties');
        insert tax;
        CPA_v2_0__c RelatedCPA = new CPA_v2_0__c(Name = 'Brazil', Country_Applied_Costings__c=true );
        insert RelatedCPA;
        CPA_v2_0__c cpav2 = new CPA_v2_0__c(Name = 'Brazil Testing 2.0', Supplier__c = acclist[1].Id, Service_Type__c = 'IOR/EOR', Related_Costing_CPA__c = RelatedCPA.Id, Preferred_Supplier__c = TRUE,
                                            VAT_Rate__c = 0.1, Destination__c = 'Brazil',  
                                            Default_Section_1_Naming_Convention__c = 'Client c/o CPA Details as Below',
                                            Default_Section_2_Naming_Convention__c = 'Use CPA Details as per Below',
                                            Default_Section_3_Naming_Convention__c = 'VAT Claiming Entity (Tax#) c/o with CPA Details as Below',
                                            Default_Section_4_Naming_Convention__c = 'Client c/o CPA Details as Below',
                                            Alternate_Section_1_Naming_Convention__c = 'CPA c/o with Final Delivery Details',
                                            Alternate_Section_2_Naming_Convention__c = 'Client c/o CPA c/o with Final Delivery Details',
                                            Alternate_Section_3_Naming_Convention__c = 'Use Final Delivery Details',
                                            Alternate_Section_4_Naming_Convention__c = 'VAT Claiming Entity (Tax#) Details',
                                            VAT_Reclaim_Destination__c = FALSE,
                                            Country__c = country.id,
                                            CIF_Freight_and_Insurance__c ='Insurance Amount'
                                            
                                           );
        insert cpav2;
        
        List<CPARules__c> CPARules = new List<CPARules__c>();
        CPARules.add(new CPARules__c(Country__c = 'Brazil',Criteria__c ='Default', Chargable_weight__c = 'NONE', Courier_Responsibility__c = 'NONE',
                                     ECCN_No__c = 'NONE', NL_Product_User__c = 'NONE', Number_of_final_deliveries__c = 'NONE', ShipmentValue__c ='NONE',
                                     Sum_of_quantity_of_matched_products__c = 'NONE', Manufacturer__c = 'NONE', DefaultCPA2__c = cpav2.Id, CPA2__c =  cpav2.Id));
        insert CPARules;
        
    }
 
    @isTest
    public static void testCreateRolloutWithServiceType(){
        
   		Account clientAcc = [SELECT Id FROM Account where Type ='Client' LIMIT 1];
        Contact clientcon = [SELECT Id FROM Contact where AccountId=: clientAcc.Id];
        Country_price_approval__c cpa = [SELECT Id FROM Country_price_approval__c];
        CPA_v2_0__c cpav2 = [SELECT Id,Lead_ICE__c FROM CPA_v2_0__c where Destination__c='Brazil' LIMIT 1];
        User usr = [Select Id, Name, userName, Profile.Name, UserRole.name From user where Profile.Name = 'System Administrator' LIMIT 1];

        
        Roll_Out__c new_roll_out = new Roll_Out__c( Client_Name__c=clientAcc.id,
                                                          Contact_For_The_Quote__c=clientcon.id,
                                                          Shipment_Value_in_USD__c=200,
                                                          Destinations__c='Brazil',
                                                          Number_of_Cost_Estimates__c =0
                                                         );
     
      	Insert new_roll_out;
        List<Shipment_Order__c> shipmentList = new List<Shipment_Order__c>();
        Shipment_Order__c shipmentOrder = new Shipment_Order__c(Account__c = clientAcc.Id, Shipment_Value_USD__c = 2000, CPA_v2_0__c = cpav2.Id,Total_Taxes__c=20,of_Line_Items__c = 0,Roll_Out__c = new_roll_out.Id,
                                                                Client_Contact_for_this_Shipment__c = clientcon.Id, Who_arranges_International_courier__c ='Client', Tax_Treatment__c ='DAP/CIF - IOR pays', 
                                                                Ship_to_Country__c = cpa.Id, Destination__c = 'Brazil', Service_Type__c = 'EOR', Chargeable_Weight__c = 200,Ship_From_Country__c = 'Australia',  
                                                                Shipping_Status__c = 'Roll-Out',Roll_Up_Costings_A__c = false,Populate_Invoice__c = false,Li_ion_Batteries__c = 'No',
                                                                Loss_Making_Shipments__c = false,Pressure_Point__c = false,Insurance_Fee__c = 0,IOR_CSE__c = usr.id, ICE__c = usr.Id,
                                                               IOR_Refund_of_Tax_and_Duty__c=10.0,FC_Admin_Fee__c=10.0,FC_Bank_Fees__c=11.0,FC_Finance_Fee__c=11.0,
                                                               FC_Insurance_Fee_USD__c=11.0,FC_International_Delivery_Fee__c=11.0);
        Test.startTest();
        insert shipmentOrder;
       
        Database.executeBatch(new ProcessRollOutSOBatch(new List<Id>{new_roll_out.Id}),1);
        Test.stopTest();
        new ProcessRollOutSOBatch().createLogs(shipmentOrder.Id, 'Error', 'Error' );
        //System.assertEquals(shipmentOrder.Id, [SELECT Shipment_Order__c From Freight__c where Shipment_Order__c = :shipmentOrder.Id].Shipment_Order__c);

    }
    
    @isTest
    public static void testCreateRolloutWithServiceType1(){
        
   		Account clientAcc = [SELECT Id FROM Account where Type ='Client' LIMIT 1];
        Contact clientcon = [SELECT Id FROM Contact where AccountId=: clientAcc.Id];
        Country_price_approval__c cpa = [SELECT Id FROM Country_price_approval__c];
        CPA_v2_0__c cpav2 = [SELECT Id,Lead_ICE__c FROM CPA_v2_0__c where Destination__c='Brazil' LIMIT 1];
        User usr = [Select Id, Name, userName, Profile.Name, UserRole.name From user where Profile.Name = 'System Administrator' LIMIT 1];

        
        Roll_Out__c new_roll_out = new Roll_Out__c( Client_Name__c=clientAcc.id,
                                                          Contact_For_The_Quote__c=clientcon.id,
                                                          Shipment_Value_in_USD__c=200,
                                                          Destinations__c='Brazil',
                                                          Number_of_Cost_Estimates__c =0
                                                         );
     
      	Insert new_roll_out;
        List<Shipment_Order__c> shipmentList = new List<Shipment_Order__c>();
        Shipment_Order__c shipmentOrder = new Shipment_Order__c(Account__c = clientAcc.Id, Shipment_Value_USD__c = 2000, CPA_v2_0__c = cpav2.Id,Total_Taxes__c=20,of_Line_Items__c = 0,Roll_Out__c = new_roll_out.Id,
                                                                Client_Contact_for_this_Shipment__c = clientcon.Id, Who_arranges_International_courier__c ='Client', Tax_Treatment__c ='DAP/CIF - IOR pays', 
                                                                Ship_to_Country__c = cpa.Id, Destination__c = 'Brazil', Service_Type__c = 'IOR', Chargeable_Weight__c = 200,Ship_From_Country__c = 'Australia',  
                                                                Shipping_Status__c = 'Roll-Out',Roll_Up_Costings_A__c = false,Populate_Invoice__c = false,Li_ion_Batteries__c = 'No',
                                                                Loss_Making_Shipments__c = false,Pressure_Point__c = false,Insurance_Fee__c = 0,IOR_CSE__c = usr.id, ICE__c = usr.Id,
                                                               IOR_Refund_of_Tax_and_Duty__c=10.0,FC_Admin_Fee__c=10.0,FC_Bank_Fees__c=11.0,FC_Finance_Fee__c=11.0,
                                                               FC_Insurance_Fee_USD__c=11.0,FC_International_Delivery_Fee__c=11.0);
       Test.startTest();
 		insert shipmentOrder;
        
        
        Database.executeBatch(new ProcessRollOutSOBatch(new List<Id>{new_roll_out.Id}),1);
        Test.stopTest();
        new ProcessRollOutSOBatch().createLogs(shipmentOrder.Id, 'Error', 'Error' );
        //System.assertEquals(shipmentOrder.Id, [SELECT Shipment_Order__c From Freight__c where Shipment_Order__c = :shipmentOrder.Id].Shipment_Order__c);

    }
   
}