@RestResource(urlMapping='/ZCPGetSuggestedProvider/*')
Global class ZCPGetSuggestedProvider {

    @Httppost
    global static void ZCPGetSuggestedProvider(){

        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        Blob body = req.requestBody;
        res.addHeader('Content-Type', 'application/json');
        String requestString = body.toString();
        ZCPGetSuggestedProviderWra rw = (ZCPGetSuggestedProviderWra)JSON.deserialize(requestString,ZCPGetSuggestedProviderWra.class);
        try {
            list<Access_token__c> At =[select status__c,Access_Token__c from Access_token__c where Access_Token__c=:rw.Accesstoken ];
            //if( at.status__C =='Active'){
            if( at.size() >0) {
              
                List<Suggested_Provider__c> ZSPs = new list<Suggested_Provider__c>();
              

                ZSPs = [SELECT Id,Provider_Account__c,
                       Provider_Account__r.name,
                       Provider_Account__r.Preferred_shipping_method__c,
                       Provider_Account__r.Preferred_ranking__c,
                       Provider_Account__r.Link__c,
                       Provider_Account__r.Additional_contact_details__c,
                       Provider_Account__r.Description,
                       Related_Zee_CPA__c,Related_Zee_CPA__r.name FROM Suggested_Provider__c WHERE Related_Zee_CPA__c =:rw.CPAID];

                if(ZSPs.size() >0){
                JSONGenerator gen = JSON.createGenerator(true);
                gen.writeStartObject();
                gen.writeFieldName('ServiceProviders');
                gen.writeStartArray();
                    For(Suggested_Provider__c ZSP : ZSPs){
                        
                         gen.writeStartObject();
                           if(ZSP.ID == null) {gen.writeNullField('ServiceProviderID');} else{gen.writeStringField('ServiceProviderID', +ZSP.ID );}
                           if(ZSP.Provider_Account__c == null) {gen.writeNullField('ProviderAccountID');} else{gen.writeStringField('ProviderAccountID', +ZSP.Provider_Account__C );}
                           if(ZSP.Provider_Account__c == null) {gen.writeNullField('ProviderAccountName');} else{gen.writeStringField('ProviderAccountName', +ZSP.Provider_Account__r.name );}
                           if(ZSP.Provider_Account__r.Preferred_shipping_method__c == null) {gen.writeNullField('Preferredshippingmethod');} else{gen.writeStringField('Preferredshippingmethod', +ZSP.Provider_Account__r.Preferred_shipping_method__c );}
                           if(ZSP.Provider_Account__r.Preferred_ranking__c == null) {gen.writeNullField('Preferredranking');} else{gen.writeNumberField('Preferredranking', +ZSP.Provider_Account__r.Preferred_ranking__c );}
                           if(ZSP.Provider_Account__r.Link__c == null) {gen.writeNullField('Link');} else{gen.writeStringField('Link', +ZSP.Provider_Account__r.Link__c );}
                           if(ZSP.Provider_Account__r.Additional_contact_details__c == null) {gen.writeNullField('Additionalcontactdetails');} else{gen.writeStringField('Additionalcontactdetails', +ZSP.Provider_Account__r.Additional_contact_details__c );}
                           if(ZSP.Provider_Account__r.Description == null) {gen.writeNullField('Description');} else{gen.writeStringField('Description', +ZSP.Provider_Account__r.Description );}
                           if(ZSP.Related_Zee_CPA__r.name == null) {gen.writeNullField('RelatedZeeCPAname');} else{gen.writeStringField('RelatedZeeCPAname', +ZSP.Related_Zee_CPA__r.name );}
                           if(ZSP.Related_Zee_CPA__c == null) {gen.writeNullField('RelatedZeeCPAID');} else{gen.writeStringField('RelatedZeeCPAID', +ZSP.Related_Zee_CPA__c );}
                         
                        gen.writeEndObject();
               //				 gen.writeStringField('Response', 'Suggested Providers are not availble');
                    }
                gen.writeEndArray(); 
                gen.writeEndObject();
                String jsonData = gen.getAsString();
                res.responseBody = Blob.valueOf(jsonData);
                res.addHeader('Content-Type', 'application/json');
                res.statusCode = 200;
                }
                else{  
                JSONGenerator gen = JSON.createGenerator(true);
                gen.writeStartObject();

                gen.writeFieldName('Message');
                gen.writeStartObject();

                gen.writeStringField('Response', 'Suggested Providers are not availble');

                gen.writeEndObject();
                gen.writeEndObject();
                String jsonData = gen.getAsString();
                res.responseBody = Blob.valueOf(jsonData);
                res.addHeader('Content-Type', 'application/json');
                res.statusCode = 400;
                    }
              
            }
            else{
                JSONGenerator gen = JSON.createGenerator(true);
                gen.writeStartObject();

                gen.writeFieldName('Message');
                gen.writeStartObject();

                gen.writeStringField('Response', 'Access token Expired');

                gen.writeEndObject();
                gen.writeEndObject();
                String jsonData = gen.getAsString();
                res.responseBody = Blob.valueOf(jsonData);
                res.addHeader('Content-Type', 'application/json');
                res.statusCode = 400;
            }
        }
        catch(exception e) {
            res.responseBody = Blob.valueOf(e.getMessage());
            res.addHeader('Content-Type', 'application/json');
            res.statusCode = 500;
          /*  API_Log__c Al = New API_Log__c();
            Al.Account__c=rw.AccountID;
            Al.Login_Contact__c=rw.ContactId;
            Al.EndpointURLName__c='ZCPGetSuggestedProvider';
            Al.Response__c=e.getMessage();
            Al.StatusCode__c=string.valueof(res.statusCode);
            Insert Al;
        */
        }
    }
}