public class Outstanding_CCD_Email_Summary{
    public static void sendEmailSummary(List<Shipment_Order__c> SOList, String SupplierId, String newEmailAddress){
        List <Shipment_Order__c> soListToSend = SOList;
        String ccdContactAlias;
        String ccdContactEmail;
        if(SupplierId != null){
            soListToSend = Outstanding_CCD_Email_Summary.getSOList(SupplierId);
        }
        Map <Id,List<Shipment_Order__c>> soMap = new Map <Id,List<Shipment_Order__c>>();
        Map <Id,List<String>> soTableMap = new Map <Id,List<String>>();
        List <Account> allAccounts;
        if(soListToSend.size()>0){
            for(Shipment_Order__c currentSO:soListToSend){
                String soName = currentSO.Name;
                String shipStatus = '';
                if(currentSO.Shipping_Status__c != null){
                    shipStatus = currentSO.Shipping_Status__c;
                }
                String dateCustomsClearance = '';
                if(currentSO.Customs_Cleared_Date__c != null){
                    dateCustomsClearance = String.valueOf(currentSO.Customs_Cleared_Date__c);
                }
                String clientAccount = '';
                if(currentSO.Customs_Cleared_Date__c != null){
                    clientAccount = currentSO.Account__r.Name;
                }
                String soDestination = '';
                if(currentSO.Destination__c != null){
                    soDestination = currentSO.Destination__c;
                }
                if(soMap.containsKey(currentSO.SupplierlU__c)){
                    List<Shipment_Order__c> contactSO = soMap.get(currentSO.SupplierlU__c);
                    List<String> contactSOHtml = soTableMap.get(currentSO.SupplierlU__c);
                    contactSO.add(currentSO);
                    String htmlRows = '<tr><td style="padding:5px;border: 1px solid black;"">'+soName+'</td><td style="padding:5px;border: 1px solid black;"">'+shipStatus+'</td><td style="padding:5px;border: 1px solid black;">'+dateCustomsClearance+'</td><td style="padding:5px;border: 1px solid black;">'+clientAccount+'</td><td style="padding:5px;border: 1px solid black;">'+soDestination+'</td></tr>';
                    contactSOHtml.add(htmlRows);
                    soTableMap.put(currentSO.SupplierlU__c,contactSOHtml);
                    soMap.put(currentSO.SupplierlU__c,contactSO);
                }else{
                    List<Shipment_Order__c> contactSO = new List <Shipment_Order__c>();
                    List<String> contactSOHtml = new List <String>();
                    contactSO.add(currentSO);
                    String htmlRows = '<tr><td style="padding:5px;border: 1px solid black;"">'+soName+'</td><td style="padding:5px;border: 1px solid black;"">'+shipStatus+'</td><td style="padding:5px;border: 1px solid black;">'+dateCustomsClearance+'</td><td style="padding:5px;border: 1px solid black;">'+clientAccount+'</td><td style="padding:5px;border: 1px solid black;">'+soDestination+'</td></tr>';
                    contactSOHtml.add(htmlRows);
                    soTableMap.put(currentSO.SupplierlU__c,contactSOHtml);
                    soMap.put(currentSO.SupplierlU__c,contactSO);
                }
            }
            Set <Id> AccountSet = new Set <Id>();
            allAccounts = [Select Id,Name,Account_Alias__c,CCD_Contact_Email__c from Account where Id in :soMap.KeySet()];
            if(allAccounts.size()>0){
                for(Account currentAccount : allAccounts){
                    AccountSet.add(currentAccount.Id);
                }
            }
            List<String> emailAddressesToSend = new List<String>();
            List <Account> FinalallAccounts1 = new List<Account>();
            List <Contact> contactRecs = [Select Id,AccountId,Email,CCD_Contact__c from Contact where AccountId in :accountSet AND CCD_Contact__c = TRUE];
            map<id,Account> FinalAccounts = new map<id,Account>([Select Id,Account_Alias__c,CCD_Contact_Email__c,Name from account where id in :accountset]);
            System.debug('FinalAccounts---->'+FinalAccounts);
            System.debug('contactRecs---->'+contactRecs);
            System.debug('contactRecs.size()---->'+contactRecs.size());
            System.debug('FinalAccounts.get(contactRecs[0].AccountId);---->'+FinalAccounts.get(contactRecs[0].AccountId));
            if(contactRecs.size()>0){
                for(Contact ccdContact : contactRecs){
                    System.debug('ccdContact.AccountId---->'+ccdContact.AccountId);
                    emailAddressesToSend.add(ccdContact.Email);
                    FinalallAccounts1.add(FinalAccounts.get(ccdContact.AccountId));
                }
            }
            System.debug('FinalallAccounts---->'+FinalallAccounts1);
            System.debug('emailAddressesToSend---->'+emailAddressesToSend);
            String allToEmails = string.join(emailAddressesToSend, ';');
            System.debug('allToEmails---->'+allToEmails);
            Set<Account> Accset = new set<Account>(FinalallAccounts1);
            List <Account> FinalallAccounts = new List<Account>(Accset);
            List <Messaging.SingleEmailMessage> mails = new List <Messaging.SingleEmailMessage>();
            if(FinalallAccounts.size()>0){
                OrgWideEmailAddress[] owea = [Select Id from OrgWideEmailAddress where Address = 'taxteam@tecex.com'];
                //for(Account currentAccount : FinalallAccounts){
                for(Integer i=0;FinalallAccounts.size()>i;i++){
                    for(Integer J=0;contactRecs.size()>j;J++){
                        if(FinalallAccounts[i].id == contactRecs[j].accountid){
                            
                            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                            System.debug('FinalallAccounts inside loop -->'+FinalallAccounts[i]);
                            System.debug('contactRecs inside loop -->'+contactRecs[J]);
                            System.debug('Acc IDs -->'+FinalallAccounts[i].id+'Con IDs -->'+contactRecs[j].accountid);
                            String Email=contactRecs[j].email;
                            ccdContactAlias = FinalallAccounts[i].Account_Alias__c;
                            ccdContactEmail = FinalallAccounts[i].CCD_Contact_Email__c;
                            //Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                            if(owea.size()>0){
                                mail.setOrgWideEmailAddressId(owea.get(0).Id);
                            }
                            System.Debug(owea.get(0).Id);
                            string body = '<font face="Calibri">Dear '+ccdContactAlias+',<br/><br/>Please see the table below, showing the outstanding customs clearance documents for your account. Kindly upload as soon as possible.';
                            body += '<br/><br/><table cellspacing=0 style="font-family:Calibri;border: 1px solid black;border-collapse: collapse;"><tr><th style="padding:5px;border: 1px solid black;">Shipment Number</th><th style="padding:5px;border: 1px solid black;">Shipment Status</th><th style="padding:5px;border: 1px solid black;">Date of Customs Clearance</th><th style="padding:5px;border: 1px solid black;">Account</th><th style="padding:5px;border: 1px solid black;">Destination</th></tr>';
                            List<String> mailTable=soTableMap.get(FinalallAccounts[i].ID);
                            if(mailTable.size()>0){
                                for(String currentItem: mailTable){
                                    body += currentItem;
                                }
                            }
                            body += '</table><br/>Please visit the TecEx Supplier Portal at https://tecex.force.com/SupplierPortal to upload. Should you require any further information, please contact taxteam@tecex.com.<br/><br/>Regards,<br/>The TecEx Tax Team<br/><br/></font>';
                            mail.setSubject('Outstanding Customs Clearance Documents for '+FinalallAccounts[i].Name);
                            String[] toAddresses = new String[] {Email}; //ccdContactEmail};
                            //String[] toAddresses = new String[] {'pieterr@tecex.com'};
                            mail.setToAddresses(toAddresses);
                            mail.setSaveAsActivity(false);
                            mail.setWhatId(FinalallAccounts[i].Id);
                            mail.setHtmlBody(body);
                            mails.add(mail);
                           // System.Debug(body);
                            
                        }
                    }
                }
                
                Messaging.sendEmail(mails);
            }
        }
    }
    public static List<Shipment_Order__c> getSOList(String SupplierId){
        List <Shipment_Order__c> soRecords = [Select Id,Name,SupplierlU__c,Account__r.Name,Destination__c,Shipping_Status__c,Customs_Cleared_Date__c,CreatedDate from Shipment_Order__c where Account__c = :SupplierId AND (Shipping_Status__c = 'Cleared Customs' OR Shipping_Status__c = 'Final Delivery in Progress' OR Shipping_Status__c = 'Awaiting POD' OR Shipping_Status__c = 'POD Received') AND (Tax_Treatment__c != 'DDP - Client Account') AND Total_Taxes_CCD__c = null AND (CreatedDate >= 2019-07-01T00:00:00Z)];
        //  List <Shipment_Order__c> soRecords = [Select Id,Name,SupplierlU__c,Account__r.Name,Destination__c,Shipping_Status__c,Customs_Cleared_Date__c,CreatedDate from Shipment_Order__c where Account__c = :SupplierId AND (Shipping_Status__c = 'Cleared Customs' OR Shipping_Status__c = 'Final Delivery in Progress' OR Shipping_Status__c = 'Awaiting POD' OR Shipping_Status__c = 'POD Received') AND (Tax_Treatment__c != 'DDP - Client Account') ];
       
        if(soRecords.size()>0){
            return soRecords;
        }else{
            return null;
        }
    }
}