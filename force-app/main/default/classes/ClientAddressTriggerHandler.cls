public class ClientAddressTriggerHandler {
    public static void validateAddress(List<Client_Address__c> clientAddresses){
        
        Id pickupRecordType = Schema.getGlobalDescribe().get('Client_Address__c').getDescribe().getRecordTypeInfosByName().get('PickUp').getRecordTypeId();
        Id finalDestination = Schema.getGlobalDescribe().get('Client_Address__c').getDescribe().getRecordTypeInfosByName().get('Final Destinations').getRecordTypeId();
        
        for(Client_Address__c clientAdd : clientAddresses){
            
            if(clientAdd.RecordTypeId != null && (clientAdd.RecordTypeId == pickupRecordType || clientAdd.RecordTypeId == finalDestination)){ 
                if(clientAdd.Contact_Full_Name__c == null || isTextLimitExceed(clientAdd.Contact_Full_Name__c, 35)){
                    clientAdd.addError('Contact Full name required and cannot exceed 35 characters.');
                }
                if(clientAdd.Contact_Email__c == null || String.isBlank(clientAdd.Contact_Email__c)){
                    clientAdd.addError('Contact email required.');
                }
                if(clientAdd.Contact_Phone_Number__c == null || !isValidPhone(clientAdd.Contact_Phone_Number__c)){
                    clientAdd.addError('Valid Phone Number Required And Cannot Contain Letters.');
                }
                if(clientAdd.CompanyName__c == null || isTextLimitExceed(clientAdd.CompanyName__c, 35)){
                    clientAdd.addError('Company name required and cannot exceed 35 characters.');
                }
                if(clientAdd.Address__c == null || isTextLimitExceed(clientAdd.Address__c, 35)){
                    clientAdd.addError('Address 1 required and cannot exceed 35 characters.');
                }
                if(clientAdd.Address2__c != null && isTextLimitExceed(clientAdd.Address2__c, 35)){
                    clientAdd.addError('Address 2 cannot exceed 35 characters.');
                }
                if(clientAdd.City__c == null || String.isBlank(clientAdd.City__c)){
                    clientAdd.addError('City required.');
                }
                if(clientAdd.Postal_Code__c == null || String.isBlank(clientAdd.Postal_Code__c)){
                    clientAdd.addError('Postal Code required.');
                }
                if(clientAdd.Province__c == null || String.isBlank(clientAdd.Province__c)){
                    clientAdd.addError('Province required.');
                }
                if(clientAdd.All_Countries__c == null || String.isBlank(clientAdd.All_Countries__c)){
                    clientAdd.addError('Country required.');
                }
                
            }
        }
    }
    
    
    public static boolean isTextLimitExceed(String value, Integer count){
        return (value.length() > count);
    }
    
    public static Boolean isValidPhone(String phone) {
        return Pattern.matches('[0-9(),./\\-]+', phone);
    }
}