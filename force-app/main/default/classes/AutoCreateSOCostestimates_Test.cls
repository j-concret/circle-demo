@isTest
public class AutoCreateSOCostestimates_Test {
         
    
    private static testMethod void  setUpData(){
        Account account = new Account (name='TecEx Prospective Client', Type ='Client', CSE_IOR__c = '0050Y000001LTZO',New_Invoicing_Structure__c = TRUE, Service_Manager__c = '0050Y000001LTZO');
        insert account;   
        
        Account account2 = new Account (name='Test Supplier', Type ='Supplier', ICE__c = '0050Y000001km5c', RecordTypeId = '0120Y0000006KjQQAU');
        insert account2;      
         
        Contact contact = new Contact ( lastname ='Testing Individual',  AccountId = account.Id);
        insert contact;  
        
         IOR_Price_List__c iorpl = new IOR_Price_List__c ( Client_Name__c = account.Id, Name = 'Brazil', IOR_Fee__c = 0.15, On_Charge_Mark_up__c = 0.15,
        TecEx_Shipping_Fee_Markup__c =0.40, IOR_Min_Fee__c = 100,  Admin_Fee__c =100, Set_up_fee__c =100, Bank_Fees__c =200, Tax_Rate__c = 0.20,
        Estimated_Customs_Brokerage__c =250, Estimated_Customs_Clearance__c =250, Estimated_Customs_Handling__c =250, Estimated_Import_License__c =250, Destination__c = 'Brazil' );
        insert iorpl; 
        
        country_price_approval__c cpa = new country_price_approval__c( name ='Brazil', Billing_term__c = 'DAP/CIF - IOR pays',  Airwaybill_Instructions__c = 'User IOR Address', Supplier__c = account2.Id, 
        In_Country_Specialist__c = '0050Y000001km5c',Destination__c = 'Brazil',Preferred_Supplier__c=TRUE,Shipping_Notes_New__c='test');
        insert cpa;
        
        CPA_v2_0__c RelatedCPA = new CPA_v2_0__c(Name = 'Brazil', Country_Applied_Costings__c=true );
        insert RelatedCPA; 
       
        CPA_v2_0__c cpav2 = new CPA_v2_0__c(Name = 'Brazil Testing 2.0', Supplier__c = account2.Id, Country_Applied_Costings__c=false, Service_Type__c = 'IOR/EOR', Related_Costing_CPA__c = RelatedCPA.Id, Preferred_Supplier__c = TRUE,
                                           VAT_Rate__c = 0.1, Destination__c = 'Brazil');
        insert cpav2;
        
        string Destinations1 = 'Brazil';
        string Destinations2 = 'Brazil;Brazil';
          string ShipFrom1 = 'Algeria';
          Double SoValue1 = 1;
          Double Chweight1 = 1;
          string Courierresp1 = 'Client';
          string REf11 = 'Client';
          string Ref21 = 'Client';
         
          Roll_Out__c new_roll_out = new Roll_Out__c( Client_Name__c=account.Id,
                                                          Contact_For_The_Quote__c=contact.Id,
                                                          Shipment_Value_in_USD__c=SOValue1,
                                                          Destinations__c=Destinations1,
                                                     	  Create_Roll_Out__c=TRUE,
                                                    	  Client_Reference__c = REf11
                                                         );  
         Insert new_roll_out;
        
         Shipment_Order__c shipmentOrder2 = new Shipment_Order__c(Account__c = account.id, Shipment_Value_USD__c =10000, CPA_v2_0__c = cpav2.Id,
        Client_Contact_for_this_Shipment__c = contact.Id, Who_arranges_International_courier__c ='Client', Tax_Treatment__c ='DAP/CIF - IOR pays', Ship_to_Country__c = cpa.Id,Service_Manager__c = '0050Y000001km5cQAA',Shipping_Notes_New__c=cpa.Shipping_notes__c,
        Destination__c = 'Brazil', Service_Type__c = 'EOR', IOR_Price_List__c = iorpl.Id, of_Unique_Line_Items__c = 10, Total_Taxes__c = 1500, of_Line_Items__c= 10, Chargeable_Weight__c = 200, of_packages__c = 20, Final_Deliveries_New__c = 10,Shipping_Status__c = 'Roll-Out',
        CI_Admin_Fee__c  =100, CI_Bank_Fees__c  = 100, CI_Finance_Fee__c  = 100, CI_Insurance_Fee_USD__c  = 100, CI_International_Delivery_Fee__c  = 100, CI_IOR_and_Import_Compliance_Fee_USD__c  = 100, Roll_Out__c=new_roll_out.Id,
        CI_Miscellaneous_Fee__c  = 100, CI_Recharge_Tax_and_Duty__c  = 100, CI_Total_Clearance_Costs__c  = 100, CI_Total_Customs_Brokerage_Cost__c  = 100, CI_Total_Handling_Cost__c  = 100, 
        CI_Total_Licence_Cost__c  = 100, FC_Admin_Fee__c  = 100, FC_Bank_Fees__c  = 100, FC_Finance_Fee__c  = 100, FC_Insurance_Fee_USD__c   = 100, FC_International_Delivery_Fee__c  = 100, 
        FC_IOR_and_Import_Compliance_Fee_USD__c  = 100, FC_Miscellaneous_Fee__c  = 100, FC_Recharge_Tax_and_Duty__c  = 100, FC_Total_Clearance_Costs__c  = 100, 
        FC_Total_Customs_Brokerage_Cost__c  = 100, FC_Total_Handling_Costs__c  = 100, FC_Total_License_Cost__c  = 100, Total_CIF_Duties__c = 0, Total_FOB_Duties__c = 0);
      // insert shipmentOrder2; 
        
        Roll_out__c RO = [select ID,Destinations__c from Roll_Out__c where id =: new_roll_out.Id];
            Ro.Destinations__c = Destinations2;
        RO.Create_Roll_Out__c = TRUE;
        Update RO;
        
         Shipment_Order__c shipmentOrder3 = new Shipment_Order__c(Account__c = account.id, Shipment_Value_USD__c =10000, CPA_v2_0__c = cpav2.Id,
        Client_Contact_for_this_Shipment__c = contact.Id, Who_arranges_International_courier__c ='Client', Tax_Treatment__c ='DAP/CIF - IOR pays', Ship_to_Country__c = cpa.Id,Service_Manager__c = '0050Y000001km5cQAA',Shipping_Notes_New__c=cpa.Shipping_notes__c,
        Destination__c = 'Brazil', Service_Type__c = 'EOR', IOR_Price_List__c = iorpl.Id, of_Unique_Line_Items__c = 10, Total_Taxes__c = 1500, of_Line_Items__c= 10, Chargeable_Weight__c = 200, of_packages__c = 20, Final_Deliveries_New__c = 10,Shipping_Status__c = 'Roll-Out',
        CI_Admin_Fee__c  =100, CI_Bank_Fees__c  = 100, CI_Finance_Fee__c  = 100, CI_Insurance_Fee_USD__c  = 100, CI_International_Delivery_Fee__c  = 100, CI_IOR_and_Import_Compliance_Fee_USD__c  = 100, Roll_Out__c=RO.Id,
        CI_Miscellaneous_Fee__c  = 100, CI_Recharge_Tax_and_Duty__c  = 100, CI_Total_Clearance_Costs__c  = 100, CI_Total_Customs_Brokerage_Cost__c  = 100, CI_Total_Handling_Cost__c  = 100, 
        CI_Total_Licence_Cost__c  = 100, FC_Admin_Fee__c  = 100, FC_Bank_Fees__c  = 100, FC_Finance_Fee__c  = 100, FC_Insurance_Fee_USD__c   = 100, FC_International_Delivery_Fee__c  = 100, 
        FC_IOR_and_Import_Compliance_Fee_USD__c  = 100, FC_Miscellaneous_Fee__c  = 100, FC_Recharge_Tax_and_Duty__c  = 100, FC_Total_Clearance_Costs__c  = 100, 
        FC_Total_Customs_Brokerage_Cost__c  = 100, FC_Total_Handling_Costs__c  = 100, FC_Total_License_Cost__c  = 100, Total_CIF_Duties__c = 0, Total_FOB_Duties__c = 0);
      // insert shipmentOrder3; 
        
         }

}