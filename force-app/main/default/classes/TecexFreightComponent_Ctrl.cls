public class TecexFreightComponent_Ctrl {
    
    private static final String base64Chars = '' +
        'ABCDEFGHIJKLMNOPQRSTUVWXYZ' +
        'abcdefghijklmnopqrstuvwxyz' +
        '0123456789+/';
    
    @AuraEnabled
    public static Map<String,Object> getShipmentRelatedData(String recId){
        Map<String,Object> response = new Map<String,Object> {'status'=>'OK'};
            try{
                Shipment_Order__c shipmentOrder = [SELECT Id,Service_Type__c,Name,Account__r.CI_Statement__c,Beneficial_Owner_Company_Name__c,CPA_v2_0__c,CPA_v2_0__r.Name,SupplierlU__c,Client_Reference__c,Client_Reference_2__c,Destination__c,Account__r.Client_type__c,CPA_v2_0__r.CIF_Freight_and_Insurance__c,CPA_v2_0__r.Exporter_Header__c,CPA_v2_0__r.Specific_CI_Statement_1__c,CPA_v2_0__r.Default_CI_Currency__c,CPA_v2_0__r.Specific_CI_Statement_2__c,CPA_v2_0__r.Specific_CI_Statement_3__c,CPA_v2_0__r.Footer_Note_Override__c,CPA_v2_0__r.Ship_From_Header__c,CPA_v2_0__r.CI_Inco_Terms__c,Account__r.Name,Account__r.Registered_Name__c,Account__r.Client_Address_Line_1__c,Account__r.Client_Address_Line_2__c,Account__r.Client_Address_Line_3__c,Account__r.Client_City__c,Account__r.Client_Country__c,Account__r.Client_Zip__c,Account__r.Other_notes__c,Account__r.Tax_Name__c,Account__r.Tax_ID__c,Account__r.Contact_Name__c,Account__r.Contact_Email__c,Account__r.Contact_Tel__c,Shipment_Value_USD__c,Who_arranges_International_courier__c,Ship_From_Country__c,Insurance_Fee_USD__c FROM Shipment_Order__c WHERE Id =: recId];
                
                
                
                Freight__c freightRequest = [SELECT Id, Bill_Duties_and_Taxes_To__c, D_and_T_Courier_Account_name__c,AES_ITN_Number__c, DHL_Content__c, LITHIUM_BATTERIES_CELLS__c,International_freight_fee_invoiced__c,
                                             Pick_Up_Contact_Address1__c,Pick_Up_Contact_Address2__c,Pick_Up_Contact_Address3__c,Pick_Up_Contact_City__c,
                                             Pick_Up_Contact_Country__c,Pick_Up_Contact_ZIP__c,Pick_Up_Contact_Comments__c,Pick_up_Contact_Details__c,
                                             Pick_Up_Contact_Email__c,Pick_up_Contact_No__c, Oversized__c, Non_Stackable__c, Dangerous_Goods__c, Lift_Gate_Required__c,
                                             Notify_Address__r.Name, FedEx_Content__c, Courier_Remarks__c, Address_Is_Residential__c,
                                             Ship_To_Address__c, Ship_To_Address__r.Name, ST_Company__c,ST_Name__c,ST_Email__c,ST_Phone__c,ST_AddressLine1__c,ST_AddressLine2__c,ST_Street__c,
                                             ST_City__c, ST_State__c, ST_Country__c, ST_Postal_Code__c, Ship_From_Address__c, Ship_From_Address__r.Name, SF_Company__c, SF_Name__c, SF_Email__c,
                                             SF_Phone__c, SF_AddressLine1__c, SF_AddressLine2__c, SF_Street__c, SF_City__c, SF_State__c, SF_Country__c, SF_Postal_Code__c,
                                             AWB_Consignee_Naming_Conventions__c, Con_Company__c, Con_Name__c, Con_Email__c, Con_Phone__c,Con_AddressLine1__c, Con_AddressLine2__c,
                                             Con_Street__c, Con_City__c, Con_State__c,Con_Country__c, Con_Postal_Code__c,
                                             Notify_Address__c, Notify_Type__c,Notify_TaxID__c, Notify_TaxId_Type__c, Notify_Company__c, Notify_Name__c,
                                             Notify_Email__c, Notify_Phone__c, Notify_AddressLine1__c, Notify_AddressLine2__c, Notify_Street__c, 
                                             Notify_City__c, Notify_State__c,Notify_Country__c,Notify_Postal_Code__c
                                             FROM Freight__c WHERE Shipment_Order__c =:recId LIMIT 1];
                
                Map<String,Set<String> > ports = new Map<String,Set<String> >();
                
                List<Shipment_Order_Package__c> sopList = [SELECT Id,packages_of_same_weight_dims__c,Weight_Unit__c,
                                                           Name,Metal_PI970__c, ION_PI967__c, Metal_PI969__c, ION_PI966__c,
                                                           Dimension_Unit__c,Length__c, Actual_Weight__c,Breadth__c,
                                                           Height__c FROM Shipment_Order_Package__c WHERE Shipment_Order__c = :recId];
                /*List<SOPListWrapper> sopOptionList = new List<SOPListWrapper>();
for(Shipment_Order_Package__c sop : sopList){
SOPListWrapper obj = new SOPListWrapper();
obj.sop = sop;  
obj.sopWithOption = new List<Map<String,String>>();
obj.sopWithOption.add(new Map<String,String>{'label' => 'ION_PI966', 'value' => 'ION_PI966__c'});
obj.sopWithOption.add(new Map<String,String>{'label' => 'ION_PI967', 'value' => 'ION_PI967__c'});
obj.sopWithOption.add(new Map<String,String>{'label' => 'Metal_PI969', 'value' => 'Metal_PI969__c'});
obj.sopWithOption.add(new Map<String,String>{'label' => 'Metal_PI970', 'value' => 'Metal_PI970__c'});
obj.selectedValue = getSelectedBatteryType(sop);
sopOptionList.add(obj);
}*/
                
                map<String,String> portOfEntryWithId = new Map<String,String>();
                for(Client_Address__c cAdd : [SELECT Id,Name,VAT_NON_VAT_address__c, Port_of_Entry__c
                                              FROM Client_Address__c
                                              WHERE recordTypeId = :Schema.SObjectType.Client_Address__c.getRecordTypeInfosByName().get('Consignee Address').getRecordTypeId()
                                              AND Document_Type__c='AWB'
                                              AND Client__c=:shipmentOrder.SupplierlU__c
                                              AND CPA_v2_0__c =: shipmentOrder.CPA_v2_0__c
                                              AND address_status__c = 'Active']) {
                                                  
                                                  if(cAdd.Port_of_Entry__c == null) continue;
                                                  
                                                  portOfEntryWithId.put(cAdd.VAT_NON_VAT_address__c+'##'+cAdd.Port_of_Entry__c, cAdd.Id);
                                                  
                                                  if(ports.containsKey(cAdd.VAT_NON_VAT_address__c)) {
                                                      if(!(ports.get(cAdd.VAT_NON_VAT_address__c)).contains(cAdd.Port_of_Entry__c)) {
                                                          ports.get(cAdd.VAT_NON_VAT_address__c).add(cAdd.Port_of_Entry__c);
                                                      }
                                                      
                                                  }else{
                                                      ports.put(cAdd.VAT_NON_VAT_address__c,new Set<String> {cAdd.Port_of_Entry__c});
                                                  }
                                              }
                
                //response.put('missingDataList', checkMissingData(recId, shipmentOrder));
                response.put('shipmentOrder',shipmentOrder);
                response.put('freightRequest',freightRequest);
                response.put('ports',ports);
                response.put('sopList',sopList);
                //response.put('sopListWrapper',sopOptionList);
                response.put('dhlContentPickList', getPickListValuesIntoList('Freight__c', 'DHL_Content__c'));
                response.put('fedexContentPickList', getPickListValuesIntoList('Freight__c', 'FedEx_Content__c'));
                response.put('showCourierDetails', showCourierDetails(shipmentOrder));
                response.put('portOfEntryWithId', portOfEntryWithId);
                response.put('zkShipmentCount' , zenKraftShipmentCount(freightRequest.Id));
                system.debug('@@@ ' + getDependentPicklist('Freight__c', 'Bill_Duties_and_Taxes_To__c','D_and_T_Courier_Account_name__c'));
                response.put('BDPickListWrapper', getDependentPicklist('Freight__c', 'Bill_Duties_and_Taxes_To__c','D_and_T_Courier_Account_name__c'));
                
                
            }catch(Exception e) {
                response.put('status','ERROR');
                response.put('msg',e.getMessage());
            }
        return response;
    }
    
    /* private static String getSelectedBatteryType(Shipment_Order_Package__c sop){
if(sop.ION_PI966__c){
return 'ION_PI966__c';
}else if(sop.ION_PI967__c){
return 'ION_PI967__c';
}else if(sop.Metal_PI969__c){
return 'Metal_PI969__c';
}else if(sop.Metal_PI970__c){
return 'Metal_PI970__c';
}
return '';
} */
    
    public static List<String> getPickListValuesIntoList(String objectType, String selectedField){
        List<String> pickListValuesList = new List<String>();
        
        Schema.SObjectType convertToObj = Schema.getGlobalDescribe().get(objectType);
        Schema.DescribeSObjectResult res = convertToObj.getDescribe();
        Schema.DescribeFieldResult fieldResult = res.fields.getMap().get(selectedField).getDescribe();
        
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry pickListVal : ple){
            pickListValuesList.add(pickListVal.getLabel());
        }     
        return pickListValuesList;
    }
    
    
    public class SOPListWrapper{
        @AuraEnabled
        public String selectedValue{get;set;}
        @AuraEnabled
        public List<Map<String,String>> sopWithOption{get;set;}
        @AuraEnabled
        public Shipment_Order_Package__c sop{get;set;}
    } 
    
    @AuraEnabled
    public static Map<String,Object> getShipmentStatus(String recId){
        Map<String,Object> response = new Map<String,Object> {'status'=>'OK'};
            try{
                
                Shipment_order__C shipmentOrder = [Select id, Shipping_Status__c FROM Shipment_Order__c WHERE Id =: recId];
                response.put('shipmentStatus', shipmentOrder.Shipping_Status__c);  
            }catch(Exception e){
                response.put('status','ERROR');
                response.put('msg',e.getMessage());
            }
        return response;
    }
    
    @AuraEnabled
    public static Map<String,Object> getFreightData(String recId){
        
        List<String> profilesForAccess = new List<String>();
        profilesForAccess.add('TecEx IP & Automation Team');
        profilesForAccess.add('TecEx Freight Team');
        profilesForAccess.add('System Administrator');
        
        profilesForAccess.add('TecEx Freight Team');
        profilesForAccess.add('TecEx IOR Team');
        profilesForAccess.add('TecEx IOR Team Leader');
        profilesForAccess.add('TecEx Management');
        profilesForAccess.add('TecEx R&D Team');
        profilesForAccess.add('TecEx Service Managers');
        
        
        Map<String,Object> response = new Map<String,Object> {'status'=>'OK'};
            try{
                
                Freight__c freightRequest = [SELECT Id, Status__c
                                             FROM Freight__c 
                                             WHERE Shipment_Order__c =:recId LIMIT 1];
                
                Id profileId=userinfo.getProfileId();
                String profileName=[Select Id,Name from Profile where Id=:profileId].Name;
                
                //if(!profilesForAccess.contains(profileName)){
                //   response.put('freightStatus', 'unAuthorized'); 
                //   return response;
                //}
                
                if(freightRequest != NULL ){  
                    response.put('freightStatus', freightRequest.Status__c);
                } 
            }catch(Exception e){
                response.put('status','ERROR');
                response.put('msg',e.getMessage());
            }
        return response;
    }
    
    
    @AuraEnabled
    public static Map<String,Object> getRatesData(String recId){
        Map<String,Object> response = new Map<String,Object> {'status'=>'OK'};
            try{
                
                Freight__c freight = [Select Id FROM Freight__c WHERE Shipment_Order__c =:recId LIMIT 1];
                response = getCourierRateData(freight.Id);
                
            }catch(Exception e){
                response.put('status','ERROR');
                response.put('msg',e.getMessage());
            }
        return response;
    }
    
    @AuraEnabled
    public static Map<String,Object> getPickUpData(String recId){
        Map<String,Object> response = new Map<String,Object> {'status'=>'OK'};
            try{
                
                Freight__c freight = [SELECT Id, Create_Pickup__c,Courier_Remarks__c,status__c, Pickup_availability_Close__c, Pickup_availability_Ready__c, Lift_Gate_Required__c  FROM Freight__c WHERE Shipment_Order__c =:recId LIMIT 1];
                
                List<zkmulti__MCShipment__c> zkShipmentList = [select id, name from zkmulti__MCShipment__c WHERE zkmulti__Shipment_Deleted__c = false AND freight_request__c =: freight.Id];
                
                Boolean gateRequired = false;
                if(freight.Lift_Gate_Required__c == 'Yes'){
                    gateRequired = true;
                } 
                
                response.put('freight',freight);
                response.put('gateRequired', gateRequired); 
                response.put('zkShipmentList',zkShipmentList);  
                
                
            }catch(Exception e){
                response.put('status','ERROR');
                response.put('msg',e.getMessage());
            }
        return response;
    }
    
    
    
    @AuraEnabled
    public static Map<String,Object> getCourierRateData(String recId){
        Map<String,Object> response = new Map<String,Object> {'status'=>'OK'};
            try{
                List<String> serviceTypes = new List<String>();
                
                serviceTypes.add('INTERNATIONAL_PRIORITY');
                serviceTypes.add('INTERNATIONAL_ECONOMY');
                serviceTypes.add('express_worldwide_p');
                serviceTypes.add('express_worldwide_d');
                serviceTypes.add('INTERNATIONAL_ECONOMY_FREIGHT');
                serviceTypes.add('INTERNATIONAL_PRIORITY_FREIGHT');
                
                List<Courier_Rates__c> rates = [SELECT Id, Name, cost__c, CreatedDate, Freight_Request__c, Final_Rate__c, service_type__c, Status__c, Service_Type_Name__c, Origin__c, Carrier_Name__c, Preferred__c
                                                FROM Courier_Rates__c
                                                WHERE Freight_Request__c =: recId AND Status__C != 'Expired'
                                                AND Preferred__c = True
                                                Order By Status__C DESC];
                response.put('rates',rates);
            }catch(Exception e) {
                response.put('status','ERROR');
                response.put('msg',e.getMessage());
            }
        return response;
    }
    
    @AuraEnabled
    public static Map<String,Object> updateSOPBatteryType(String sop){
        Map<String,Object> response = new Map<String,Object> {'status'=>'OK'};
            try{
                
                List<Shipment_Order_Package__c> sopToUpdate = new List<Shipment_Order_Package__c>();
                List<SOPListWrapper> sopOptionList = (List<SOPListWrapper>) JSON.deserialize(sop, List<SOPListWrapper>.class);
                for(SOPListWrapper sopWrap : sopOptionList){
                    Shipment_Order_Package__c so = sopWrap.sop;
                    if(String.isNotBlank(sopWrap.selectedValue)){
                        resetSelectedBatteryTypes(so);
                        so.put(sopWrap.selectedValue, true);
                        sopToUpdate.add(so);
                    } 
                }
                system.debug(JSON.serializePretty(sopToUpdate));
                update sopToUpdate;
                
            }catch(Exception e) {
                response.put('status','ERROR');
                response.put('msg',e.getMessage());
            }
        return response;
    }
    
    
       @AuraEnabled
    public static Map<String,Object> schedulePickup(Freight__c freight, Boolean liftGateRequired){
        Map<String,Object> response = new Map<String,Object> {'status'=>'OK'};
            try{
                if(!Test.isRunningTest()){
                freight.Create_Pickup__c = false;
                }
                if(liftGateRequired){
                    freight.Lift_Gate_Required__c = 'Yes';   
                }else{
                    freight.Lift_Gate_Required__c = 'No'; 
                }
                
                update freight;
                
                if(!Test.isRunningTest()){
                freight.Create_Pickup__c = true;
                }
                
               update freight;
            }catch(Exception e) {
                system.debug('Error ' + e.getLineNumber());
                response.put('status','ERROR');
                response.put('msg',e.getMessage());
                throw new AuraHandledException(e.getMessage());
            }
        return response;
    }
    
     
   /* @AuraEnabled
    public static Map<String,Object> schedulePickup(Freight__c freight, Boolean liftGateRequired,String selectedZKs){
        
        System.debug('freight');
         System.debug(freight);
         System.debug('liftGateRequired');
         System.debug(liftGateRequired);
         System.debug('selectedZKs');
         System.debug(selectedZKs);
        Map<String,Object> response = new Map<String,Object> {'status'=>'OK'};
            List<zkmulti__MCShipment__c> zkShipmentList = new List<zkmulti__MCShipment__c>();
        List<zkmulti__MCPickup__c> zkPickupList = new List<zkmulti__MCPickup__c>();
        
        
        try{
            if(!Test.isRunningTest()){
                freight.Create_Pickup__c = false;
            }
            if(liftGateRequired){
                freight.Lift_Gate_Required__c = 'Yes';   
            }else{
                freight.Lift_Gate_Required__c = 'No'; 
            }
            
            update freight;
            
            for(zkmulti__MCPickup__c zkPickup : [SELECT Id , Name , Freight_Request__c,zkmulti__Cancel_Reason__c , zkmulti__Pickup_Cancelled__c  FROM zkmulti__MCPickup__c where Freight_Request__c='a237Z000000O0Y0QAK']){
                zkPickup.zkmulti__Pickup_Cancelled__c  = true;
                zkPickup.zkmulti__Cancel_Reason__c = 'Re-scheduling Pickup';
                zkPickupList.add(zkPickup);
            }
            update zkPickupList;
            for(zkmulti__MCShipment__c zkShipment:[select id, name ,zkmulti__Shipment_Deleted__c, zkmulti__Mod_Key__c ,zkmulti__Pickup__c 
                                                   from zkmulti__MCShipment__c 
                                                   WHERE zkmulti__Shipment_Deleted__c = false 
                                                   AND freight_request__c =: freight.Id 
                                                   AND Id != :selectedZKs
                                                  ]){
                                                      System.debug('zkShipment');
                                                      System.debug(JSON.serializePretty(zkShipment));
                                                      //zkShipment.zkmulti__Pickup__c = null;
                                                      zkShipment.zkmulti__Shipment_Deleted__c = true; 
                                                      zkShipment.zkmulti__Mod_Key__c = 'ZKMULTI2016';
                                                      zkShipmentList.add(zkShipment);
                                                      
                                                  }
            
             System.debug('zkShipmentList to update');
             System.debug(JSON.serializePretty(zkShipmentList));
            update zkShipmentList;
            
            // updating to cancel Pickup and removing from ZKshipment.
            
            if(!Test.isRunningTest()){
                freight.Create_Pickup__c = true;
            }
            
            update freight;
            
        }catch(Exception e) {
            system.debug('Error ' + e.getLineNumber()+e.getMessage()+e.getStackTraceString());
            response.put('status','ERROR');
            response.put('msg',e.getMessage());
            throw new AuraHandledException(e.getMessage());
        }
        return response;
    }
    */
    
    
    @AuraEnabled
    public static Map<String,Object> updateFreightData(Boolean oversized, Boolean nonStackable, Boolean dangerousGoods, Boolean liftGateRequired, String recordId, String selectedPort){
        Map<String,Object> response = new Map<String,Object> {'status'=>'OK'};
            try{
                
                ZenkraftOnFreightHandler.fetchRates = true;
                
                Freight__c freight = new Freight__c();
                freight.id = Id.valueOf(recordId);
                freight.Oversized__c = oversized;
                //freight.Notify_Address__c = selectedPort;
                freight.Status__c = 'Submitted';  
                freight.Non_Stackable__c = nonStackable;
                freight.Dangerous_Goods__c = dangerousGoods;
                if(liftGateRequired){
                    freight.Lift_Gate_Required__c = 'Yes';   
                }else{
                    freight.Lift_Gate_Required__c = 'No'; 
                }
                
                update freight;
            }catch(Exception e) {
                system.debug('Error ' + e.getLineNumber());
                response.put('status','ERROR');
                response.put('msg',e.getMessage());
            }
        return response;
    }
    
    
    @AuraEnabled
    public static Map<String,Object> updateAddresses(String recordId, String selectedPort, String shipTo, String shipFrom, String frieghtObjectString, Boolean updateConsignee){
        Map<String,Object> response = new Map<String,Object> {'status'=>'OK'};
            try{
                Freight__c freightToUpdate = new Freight__c();
                freightToUpdate.id = recordId;
                if(String.isNotBlank(shipTo)){
                    freightToUpdate.Ship_To_Address__c = shipTo;
                }
                if(String.isNotBlank(shipFrom)){
                    freightToUpdate.Ship_From_Address__c = shipFrom;
                }
                if(String.isNotBlank(selectedPort)){
                    freightToUpdate.Notify_Address__c = selectedPort;
                }
                if(!updateConsignee){
                    Freight__c freight =(Freight__c) JSON.deserialize(frieghtObjectString, Freight__c.class); 
                    freightToUpdate.Con_Company__c = freight.Con_Company__c;
                    freightToUpdate.Con_Name__c = freight.Con_Name__c;
                    freightToUpdate.Con_Email__c = freight.Con_Email__c;
                    freightToUpdate.Con_Phone__c = freight.Con_Phone__c;
                    freightToUpdate.Con_AddressLine1__c = freight.Con_AddressLine1__c;
                    freightToUpdate.Con_AddressLine2__c = freight.Con_AddressLine2__c;           
                }
                update freightToUpdate;
                
                Freight__c freightRequest = [SELECT Id, Bill_Duties_and_Taxes_To__c, D_and_T_Courier_Account_name__c, AES_ITN_Number__c, DHL_Content__c, LITHIUM_BATTERIES_CELLS__c,International_freight_fee_invoiced__c,
                                             Pick_Up_Contact_Address1__c,Pick_Up_Contact_Address2__c,Pick_Up_Contact_Address3__c,Pick_Up_Contact_City__c,
                                             Pick_Up_Contact_Country__c,Pick_Up_Contact_ZIP__c,Pick_Up_Contact_Comments__c,Pick_up_Contact_Details__c,
                                             Pick_Up_Contact_Email__c,Pick_up_Contact_No__c, Oversized__c, Non_Stackable__c, Dangerous_Goods__c, Lift_Gate_Required__c,
                                             Notify_Address__r.Name, FedEx_Content__c, Courier_Remarks__c, Address_Is_Residential__c,
                                             Ship_To_Address__c, Ship_To_Address__r.Name, ST_Company__c,ST_Name__c,ST_Email__c,ST_Phone__c,ST_AddressLine1__c,ST_AddressLine2__c,ST_Street__c,
                                             ST_City__c, ST_State__c, ST_Country__c, ST_Postal_Code__c, Ship_From_Address__c, Ship_From_Address__r.Name, SF_Company__c, SF_Name__c, SF_Email__c,
                                             SF_Phone__c, SF_AddressLine1__c, SF_AddressLine2__c, SF_Street__c, SF_City__c, SF_State__c, SF_Country__c, SF_Postal_Code__c,
                                             AWB_Consignee_Naming_Conventions__c, Con_Company__c, Con_Name__c, Con_Email__c, Con_Phone__c,Con_AddressLine1__c, Con_AddressLine2__c,
                                             Con_Street__c, Con_City__c, Con_State__c,Con_Country__c, Con_Postal_Code__c,
                                             Notify_Address__c, Notify_Type__c,Notify_TaxID__c, Notify_TaxId_Type__c, Notify_Company__c, Notify_Name__c,
                                             Notify_Email__c, Notify_Phone__c, Notify_AddressLine1__c, Notify_AddressLine2__c, Notify_Street__c, 
                                             Notify_City__c, Notify_State__c,Notify_Country__c,Notify_Postal_Code__c
                                             FROM Freight__c WHERE id =:recordId LIMIT 1];
                
                response.put('freightRequest',freightRequest);
            }catch(Exception e) {
                system.debug('Error ' + e.getLineNumber());
                response.put('status','ERROR');
                response.put('msg',e.getMessage());
            }
        return response;
    }
    
    @AuraEnabled
    public static Map<String,Object> getAWBCreationStatus(String freightId, Integer shipmentCount){
        Map<String,Object> response = new Map<String,Object> {'status'=>'OK'};
            try{
                Freight__c freightRecord = [Select Id, Zenkraft_Error_Success_Message__c,status__c 
                                            FROM Freight__c
                                            WHERE Id =:freightId LIMIT 1];
                
                Integer currentShipmentCount = zenKraftShipmentCount(freightId);
                if(currentShipmentCount > shipmentCount){
                    response.put('AWBStatus', 'Created');
                }else If(currentShipmentCount == shipmentCount && freightRecord.Zenkraft_Error_Success_Message__c != ''){
                    response.put('AWBStatus', 'Failed');
                }else{
                    response.put('AWBStatus', 'Manual Check');
                }
                
                
            }catch(Exception e) {
                system.debug('Error ' + e.getLineNumber());
                response.put('status','ERROR');
                response.put('msg',e.getMessage());
            }
        return response;    
    }
    
    public static Integer zenKraftShipmentCount(Id freight){
        return Database.countQuery('Select count() FROM zkmulti__MCShipment__c WHERE Freight_Request__c = :freight');
    }
    
    @AuraEnabled
    public static Map<String,Object> updateCourierDetails(String frieghtObjectString){
        Map<String,Object> response = new Map<String,Object> {'status'=>'OK'};
            try{
                Freight__c freight =(Freight__c) JSON.deserialize(frieghtObjectString, Freight__c.class); 
                
                Freight__c freightToUpdate = new Freight__c();
                freightToUpdate.Id = freight.id;
                freightToUpdate.AES_ITN_Number__c = freight.AES_ITN_Number__c;
                freightToUpdate.DHL_Content__c = freight.DHL_Content__c;
                freightToUpdate.FedEx_Content__c = freight.FedEx_Content__c;
                freightToUpdate.Courier_Remarks__c = freight.Courier_Remarks__c;
                freightToUpdate.Address_Is_Residential__c = freight.Address_Is_Residential__c;
                freightToUpdate.Bill_Duties_and_Taxes_To__c = freight.Bill_Duties_and_Taxes_To__c;
                freightToUpdate.D_and_T_Courier_Account_name__c = freight.D_and_T_Courier_Account_name__c;
                freightToUpdate.Status__c = 'Live';
                freightToUpdate.Zenkraft_Error_Success_Message__c = '';
                
                
                update freightToUpdate;
                
                //response.put('zkShipmentCount' , zenKraftShipmentCount(freightToUpdate.Id));
                
            }catch(Exception e) {
                system.debug('Error ' + e.getLineNumber());
                response.put('status','ERROR');
                response.put('msg',e.getMessage());
            }
        return response;
    }
    
    
    @AuraEnabled
    public static Map<String,Object> updateSOP(List<Shipment_Order_Package__c> sopList){
        Map<String,Object> response = new Map<String,Object> {'status'=>'OK'};
            try{
                update sopList;
                
            }catch(Exception e) {
                system.debug('Error ' + e.getLineNumber());
                response.put('status','ERROR');
                response.put('msg',e.getMessage());
            }
        return response;
    }
    
    @AuraEnabled
    public static Map<String,Object> updateSelectedCourierRate(String rates, String selectedId){
        Map<String,Object> response = new Map<String,Object> {'status'=>'OK'};
            try{
                
                if(String.isNotBlank(rates) && String.isNotBlank(selectedId)){
                    List<Courier_Rates__c> ratesToUpdate =(List<Courier_Rates__c>) JSON.deserialize(rates, List<Courier_Rates__c>.class);
                    for(Courier_Rates__c cr : ratesToUpdate){
                        if(selectedId.containsIgnoreCase(cr.Id)){
                            cr.Status__c = 'Selected';
                        }else{
                            cr.Status__c = 'Created';
                        }
                    }
                    update ratesToUpdate; 
                }
            }catch(Exception e) {
                system.debug('Error ' + e.getLineNumber());
                response.put('status','ERROR');
                response.put('msg',e.getMessage());
            }
        return response;
    }
    
    
    
    private static void resetSelectedBatteryTypes(Shipment_Order_Package__c so){
        so.put('ION_PI966__c',false);
        so.put('ION_PI967__c',false);
        so.put('Metal_PI969__c',false);
        so.put('Metal_PI970__c',false);   
    }
    
    
    
    
    
    private static Boolean showCourierDetails(Shipment_Order__c shipmentOrder){
        Boolean result = false;
        //if(shipmentOrder.Ship_From_Country__c == 'United States'){
        List<Part__c> parts = [SELECT Id, Commercial_Value__c, Total_Value__c ,US_HTS_Code__c  FROM Part__c WHERE Shipment_Order__c = :shipmentOrder.Id];
        
        map<String, Double> htsWithTotal = new map<String, Double>();
        
        for(Part__c part : parts){
            if(part.US_HTS_Code__c != NULL){
                if(htsWithTotal.containsKey(part.US_HTS_Code__c)){
                    Double totalAmount = htsWithTotal.get(part.US_HTS_Code__c);
                    totalAmount += part.Total_Value__c;
                    htsWithTotal.put(part.US_HTS_Code__c, totalAmount);
                }else{
                    htsWithTotal.put(part.US_HTS_Code__c, part.Total_Value__c);
                }
            }
        }
        
        for(Double totalValue : htsWithTotal.values()){
            if(totalValue > 2500){
                result = true;
                break;
            }
        }
        
        /*Decimal commericalValueSum = 0;
for(Part__c part : parts){


commericalValueSum += part.Commercial_Value__c;

//if(part.Commercial_Value__c > 2500){
//    result = true;
//    break;
//}
}
if(commericalValueSum > 2500){
result = true;
}
*/
        //}
        return result;
    }
    
    
    public static PicklistWrapper getDependentPicklist(String ObjectName, string parentField, string childField) {
        Map<String,List<String>> pickListMap = new Map<String,List<String>>();
        PicklistWrapper pw = new PicklistWrapper();
        pw.pickListMap = pickListMap;
        
        if (Schema.getGlobalDescribe().get(ObjectName) ==null || String.isBlank(parentField) || String.isBlank(ChildField)){
            return pw;
        }
        
        Schema.sObjectType objType = Schema.getGlobalDescribe().get(ObjectName).newSObject().getSObjectType();
        Map<String, Schema.SObjectField> objFieldMap = objType.getDescribe().fields.getMap();
        
        if (!objFieldMap.containsKey(parentField) || !objFieldMap.containsKey(childField)){
            return pw;     
        }
        
        List<PicklistEntryWrapper> depEntries = (List<PicklistEntryWrapper>)JSON.deserialize(JSON.serialize(objFieldMap.get(ChildField).getDescribe().getPicklistValues()), List<PicklistEntryWrapper>.class);
        List<String> controllingValues = new List<String>();
        
        for (Schema.PicklistEntry ple : objFieldMap.get(parentField).getDescribe().getPicklistValues()) {
            pickListMap.put(ple.getLabel(), new List<String>());
            controllingValues.add(ple.getLabel());
        }
        
        for (PicklistEntryWrapper plew : depEntries) {
            String validForBits = base64ToBits(plew.validFor);
            for (Integer i = 0; i < validForBits.length(); i++) {
                String bit = validForBits.mid(i, 1);
                if (bit == '1') {
                    pickListMap.get(controllingValues.get(i)).add(plew.label);
                }
            }
        }
        
        pw.pickListMap = pickListMap;
        pw.parentFieldLabel = objFieldMap.get(parentField).getDescribe().getLabel();
        pw.childFieldLabel = objFieldMap.get(childField).getDescribe().getLabel();
        return pw;
    }
    
    
    public static String decimalToBinary(Integer val) {
        String bits = '';
        while (val > 0) {
            Integer remainder = Math.mod(val, 2);
            val = Integer.valueOf(Math.floor(val / 2));
            bits = String.valueOf(remainder) + bits;
        }
        return bits;
    }
    
    public static String base64ToBits(String validFor) {
        if (String.isEmpty(validFor)) return '';
        
        String validForBits = '';
        
        for (Integer i = 0; i < validFor.length(); i++) {
            String thisChar = validFor.mid(i, 1);
            Integer val = base64Chars.indexOf(thisChar);
            String bits = decimalToBinary(val).leftPad(6, '0');
            validForBits += bits;
        }
        
        return validForBits;
    }
    
    
    public class PicklistWrapper{
        @AuraEnabled
        public Map<String, List<String>> pickListMap;
        @AuraEnabled
        public String parentFieldLabel;
        @AuraEnabled
        public String childFieldLabel;      
    }
    
    public class PicklistEntryWrapper{
        public String active;
        public String defaultValue;
        public String label;
        public String value;
        public String validFor;
        
    }
}