public class AttachmentWrapper{
    @AuraEnabled public Id id;
    @AuraEnabled public String parentId;
    @AuraEnabled public String fileName;
    @AuraEnabled public String ownerName;
    @AuraEnabled public String fileSize;
    @AuraEnabled public Date fileLastModifiedDate;

    public AttachmentWrapper(Id id, String parentId,String fileName,String ownerName, Integer fileSize, Date fileLastModifiedDate){
        this.id = id;
        this.parentId = parentId;
        this.fileName = fileName;
        this.ownerName = ownerName;
        this.fileSize = fileSizeToString(fileSize);
        this.fileLastModifiedDate = fileLastModifiedDate;
    }

    public String fileSizeToString(Long Value){
        /* string representation if a file's size, such as 2 KB, 4.1 MB, etc */
        if (Value < 1024)
            return string.valueOf(Value) + ' Bytes';
        else if (Value >= 1024 && Value < (1024*1024)){
            //KB
            Decimal kb = Decimal.valueOf(Value);
            kb = kb.divide(1024,2);
            return string.valueOf(kb) + ' KB';
        }
        else if (Value >= (1024*1024) && Value < (1024*1024*1024)){
            //MB
            Decimal mb = Decimal.valueOf(Value);
            mb = mb.divide((1024*1024),2);
            return string.valueOf(mb) + ' MB';
        }
        else{
            //GB
            Decimal gb = Decimal.valueOf(Value);
            gb = gb.divide((1024*1024*1024),2);
            return string.valueOf(gb) + ' GB';
        }
    }
}