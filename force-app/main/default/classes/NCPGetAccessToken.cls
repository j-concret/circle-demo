@RestResource(urlMapping='/GetAccessToken/*')
Global class NCPGetAccessToken {
    @HttpGet
    global static void GenerateAccessToken(){
     	 RestRequest req = RestContext.request;
         RestResponse res = RestContext.response;
       //  Blob body = req.requestBody;
         res.addHeader('Content-Type', 'application/json');
 		try {
     
     
      		String Accesstoken = GuidUtilCP.NewGuid();
            System.debug('Accesstoken'+Accesstoken);
            string Base64Accesstoken = EncodingUtil.base64Encode(Blob.valueof(Accesstoken));
            System.debug('Base64Accesstoken'+Base64Accesstoken);
                
                 Access_token__c At = New Access_token__c();
                	At.Access_Token__c=Accesstoken;
             		At.AccessToken_encoded__c=Base64Accesstoken;
                 Insert At;  
             
             
                
                JSONGenerator gen = JSON.createGenerator(true);
                gen.writeStartObject();                
                gen.writeFieldName('AccessToken');
                                          gen.writeStartObject();
                                          	if(at.AccessToken_encoded__c== null) {gen.writeNullField('AccessToken_encoded');} else{gen.writeStringField('AccessToken_encoded', at.AccessToken_encoded__c);}
                							
                 							gen.writeEndObject();
                 gen.writeEndObject();
                 string jsonData = gen.getAsString();
                
             
                res.responseBody = Blob.valueOf(jsonData);
            	res.addHeader('Content-Type', 'application/json');
        	 	res.statusCode = 200;

     		}
        Catch(Exception e){
            System.debug('Error-->'+e.getMessage()+' at line number: '+e.getLineNumber());
                
        	}
        
}
}