public class attachCostEstimatePDF
{
    public Account account {get;set;}
    public Shipment_Order__c shipmentOrder {get;set;}
    public List<String> proFormaList {get;set;}

    public attachCostEstimatePDF(ApexPages.StandardController controller) {
        
        this.shipmentOrder =  [ SELECT Id, Account__c,Reason_for_Pro_forma_quote__c, Who_arranges_International_courier__c, CPA_v2_0__r.ETA_Disclaimer__c, Miscellaneous_Fee_Name__c, Total_Taxes__c FROM Shipment_Order__c 
                                WHERE Id = :ApexPages.currentPage().getParameters().get('id') LIMIT 1];
		
        if(shipmentOrder.Account__c != null){
            this.account = [Select Id, Invoice_Timing__c,Client_type__c, Invoicing_Term_Parameters__c, Contract_Type_Signed__c From Account Where Id = :shipmentOrder.Account__c];
        }
        
        if(shipmentOrder != null && shipmentOrder.Reason_for_Pro_forma_quote__c != null){
            proFormaList = shipmentOrder.Reason_for_Pro_forma_quote__c.split('\\. ');
        }
        
    }


public shipment_order__C sorecord {set;get;}

    @InvocableMethod(label='AttachCostEstimatePDF' description='AttachCostEstimatePDFToNotes')
    public static void sendEmailWithAttachment(List<id> listofQuoteHeader)
    {
       Shipment_Order__C  SO =  [ Select Id,Name, Cost_Estimate_attached__c,Shipping_Status__c,Cost_Estimate_Number__c,Expiry_Date__c,Shipment_Value_USD__c,IOR_FEE_USD__c,Total_Customs_Brokerage_Cost__c,Total_clearance_Costs__c,Recharge_Handling_Costs__c,Recharge_License_Cost__c,Handling_and_Admin_Fee__c,Bank_Fees__c,International_Delivery_Fee__c,Insurance_Fee_USD__c,Tax_Cost__c,Total_Invoice_Amount_Excl_Taxes_Duties__c,Chargeable_Weight__c,Shipping_Notes_Formula__c, 
       New_Structure__c, Shipping_Notes_New__c FROM Shipment_Order__c where Id =: listofQuoteHeader];
  system.debug('SHipmentOrderrecord'+SO);
    
           for(Id QuoteHeaderid :listofQuoteHeader)
           {
           
           If( SO.New_Structure__c){
           
              PageReference pref= page.attachCostEstimatePDFVF2;
              // PageReference pref = new PageReference('/apex/attachCostEstimatePDFVF');
               pref.getParameters().put('id',QuoteHeaderid);
               pref.setRedirect(true);
               system.debug('SOID id'+ QuoteHeaderid);
               Attachment attachment = new Attachment();      
               Blob b = null;
               
                  
                 try {
               
                   b = pref.getContentAsPDF();
                   
                system.debug('Body--->'+ b);
               
   
        } catch (VisualforceException e) {
            system.debug('in the catch block');
             b= Blob.valueOf('Some Text');
        }
                           
               attachment.Body = b;
               
               If(SO.Shipping_Status__c == 'Cost Estimate'){               
               attachment.Name = 'CostEstimate' +so.name + '.pdf';
               }
               If(So.Shipping_Status__c == 'Shipment Pending'){               
               attachment.Name = 'Approved CostEstimate' +so.name + '.pdf';
               }
               attachment.IsPrivate = false;
               attachment.ParentId = QuoteHeaderid;
              
               try{
               insert attachment;
                  so.Cost_Estimate_attached__c = TRUE;
                   Update SO;
               }
               
               catch(Exception e){}
               
               /*
               Messaging.SingleEmailMessage semail= new Messaging.SingleEmailMessage();
               Messaging.EmailFileAttachment attach= new Messaging.EmailFileAttachment();
               attach.setBody(pref.getContentAsPDF());
               semail.setSubject('Quote Issued');
               String[] emailIds= new String[]{'anilshowreddy@gmail.com'};
               semail.setToAddresses(emailIds);
               semail.setPlainTextBody('Please find the attached quote details');
               semail.setFileAttachments(new Messaging.EmailFileAttachment[]{attach});
               Messaging.sendEmail(new Messaging.SingleEmailMessage[]{semail});
               
               */
           }
           
           
           Else {
           
            PageReference pref= page.attachCostEstimatePDFVF;
              // PageReference pref = new PageReference('/apex/attachCostEstimatePDFVF');
               pref.getParameters().put('id',(Id)QuoteHeaderid);
               pref.setRedirect(true);
               
               Attachment attachment = new Attachment();      
               Blob b;
               
                 try {
               
                   b = pref.getContentAsPDF();
                   
                   system.debug('Body--->'+ b);  
   
        } catch (VisualforceException e) {
            system.debug('in the catch block');
             b= Blob.valueOf('Some Text');
        }
               
             
               
             //  Blob b=pref.getContentAsPDF();
                           
               attachment.Body = b;
               
               If(SO.Shipping_Status__c == 'Cost Estimate'){               
               attachment.Name = 'CostEstimate' +so.name + '.pdf';
               }
               If(So.Shipping_Status__c == 'Shipment Pending'){               
               attachment.Name = 'Approved CostEstimate' +so.name + '.pdf';
               }
               attachment.IsPrivate = false;
               attachment.ParentId = QuoteHeaderid;
              
               try{
               insert attachment;
               Update SO;    
               }
               
               catch(Exception e){}
               
              }
           
           
    }

}

}