@isTest
public class NCPAddProductCatelog_Test {

    static testMethod void  testForNCPAddProductCatelog(){
        
        Access_token__c at = new Access_token__c();
        at.Access_Token__c = 'cc407192-e2bf-4ada-9ddd-dfa2e13aba9c';
        at.Status__c='Active';
        Insert at;
        
        Account acc = new Account(Name = 'Test Account');
        Insert acc;
        
        Contact con = new Contact(LastName = 'Test Contact',
                                  AccountId = acc.Id);
        Insert con;
        
        List<NCPAddProductCatelogWra.Attachment> attachmentList = new List<NCPAddProductCatelogWra.Attachment>();
        
        for(Integer i=0; i<=3; i++){
            
            NCPAddProductCatelogWra.Attachment attachementObj = new NCPAddProductCatelogWra.Attachment();
            attachementObj.filename = 'Test_File_'+ i;
            attachementObj.fileType = 'jpeg';
            attachementObj.filebody = Blob.valueOf('{"data": "wertyuiopaslkjhbcvhwertyuioe563745cv4b36cv5b"}');
            
            attachmentList.add(attachementObj);
        }
        
        NCPAddProductCatelogWra obj = new NCPAddProductCatelogWra();
        obj.Accesstoken = at.Access_Token__c;
        obj.AccountID = acc.Id;
        obj.ContactID = con.Id;
        obj.LineItems = attachmentList;
        
        RestRequest req1 = new RestRequest();
        RestResponse res1 = new RestResponse();
        req1.requestURI = '/services/apexrest/NCPAddProductCatelog/'; 
        req1.httpMethod = 'POST';
        req1.requestBody = Blob.valueOf(JSON.serialize(obj));     
        RestContext.request = req1;
        RestContext.response = res1;
        RestContext.request.addHeader('Content-Type', 'application/json');
        
        Test.startTest(); 
        	NCPAddProductCatelog.NCPSODetials();
        Test.stopTest();
    }
    
    static testMethod void  testErrorForNCPAddProductCatelog(){
        
        Access_token__c at = new Access_token__c();
        at.Access_Token__c = 'cc407192-e2bf-4ada-9ddd-dfa2e13aba9c';
        at.Status__c='Active';
        Insert at;

        NCPAddProductCatelogWra obj = new NCPAddProductCatelogWra();
        obj.Accesstoken = at.Access_Token__c;
        
        RestRequest req1 = new RestRequest();
        RestResponse res1 = new RestResponse();
        req1.requestURI = '/services/apexrest/NCPAddProductCatelog/'; 
        req1.httpMethod = 'POST';
        req1.requestBody = Blob.valueOf(JSON.serialize(obj));     
        RestContext.request = req1;
        RestContext.response = res1;
        RestContext.request.addHeader('Content-Type', 'application/json');
        
        Test.startTest(); 
        	NCPAddProductCatelog.NCPSODetials();
        Test.stopTest();
    }
}