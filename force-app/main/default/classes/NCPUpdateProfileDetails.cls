@RestResource(urlMapping='/NCPUpdateProfileDetails/*')
Global class NCPUpdateProfileDetails {
    
     @Httppost
        global static void NCPCreateCase(){
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        Blob body = req.requestBody;
    	res.addHeader('Content-Type', 'application/json');
        String requestString = body.toString();
        NCPUpdateProfileDetailsWra rw  = (NCPUpdateProfileDetailsWra)JSON.deserialize(requestString,NCPUpdateProfileDetailsWra.class);
          Try{
             
               Access_token__c At =[select status__c,Access_Token__c from Access_token__c where Access_Token__c=:rw.Accesstoken ];
            if( at.status__C =='Active'){
               Id CPrecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('ClientPlatform').getRecordTypeId();
                 
              Contact con = [Select Id,FirstName,Include_in_SO_Communication__c,LastName,email,Phone, name,Contact_Role__c,New_Task__c,Task_Completion__c,Quote_status_updates__c,Major_Updates__c,Minor_Updates__c,Include_in_invoicing_emails__c from Contact where id =:rw.ContactID limit 1];
                String ConEmail = con.Email;
                String ConPhone = Con.Phone;
                
                Case cs = new case();
                If(ConEmail != rw.email || ConPhone != rw.ContactNumber){
                    CS.RecordTypeId=CPrecordTypeId;
                    cs.Subject = 'Update user details';
                    cs.Reason = 'Client Portal';
                    cs.Description = 'Clinet Portal user requested to change his Email and Phone number and same has been updated in this contact '+con.Id+' Please update on User record if it is necessary.Client wants to update email with '+rw.email+'and phone number with '+rw.ContactNumber;
                    cs.Status = 'new';
                   // cs.AccountId=rw.AccountID;
                    cs.Contactid=rw.ContactId;
                   // cs.Linked_Record_Id__c=rw.ContactID;  
                    cs.Link_to_Record__c = 'https://tecex.lightning.force.com/'+rw.ContactID;                
                
                    
                }
                
                Con.Firstname=rw.firstname;
                Con.LastName=rw.lastname;
                Con.Email=rw.email;
                Con.Phone=rw.ContactNumber;
                Con.Contact_Role__c=rw.ContactRole;
                Con.New_Task__c=rw.Newtask == null? false :rw.Newtask;
                Con.Task_Completion__c=rw.taskcompletion == null? false :rw.taskcompletion;
              //  Con.Quote_status_updates__c=rw.Quotestatusupdate;
                Con.Major_Updates__c=rw.Majorupdate == null? false :rw.Majorupdate;
                Con.Minor_Updates__c=rw.Minorupdate == null? false :rw.Minorupdate;
                COn.Client_Push_Notifications_Choice__c=rw.ClientPushNotificationsChoice;
              //  Con.Include_in_invoicing_emails__c=rw.IncludeinInvoicingEmails;
                CON.Include_in_SO_Communication__c= rw.IncludeinSOCommunication == null? false :rw.IncludeinSOCommunication;
                Update Con;
               
               
                system.debug('ConEmail :'+ConEmail);
                system.debug('rw.email :'+rw.email);
                system.debug('ConPhone :'+ConPhone);
                system.debug('rw.ContactNumber :'+rw.ContactNumber);
            If(ConEmail != rw.email || ConPhone != rw.ContactNumber){
                insert cs;
                case cs1 = [select Id,CaseNumber from case where id=:cs.Id limit 1];
                
            JSONGenerator gen = JSON.createGenerator(true);
            gen.writeStartObject();
            
             gen.writeFieldName('Success');
             gen.writeStartObject();
            
             	if(cs.Id ==null) {gen.writeNullField('Response');} else{gen.writeStringField('Response', 'Details on contact updated and Case Created. Reference number: '+Cs1.CaseNumber);}
            
            gen.writeEndObject();
            gen.writeEndObject();
            String jsonData = gen.getAsString();
            res.responseBody = Blob.valueOf(jsonData);
            res.addHeader('Content-Type', 'application/json');
            res.statusCode = 200; 
                
               }   
                
                else{
                     JSONGenerator gen = JSON.createGenerator(true);
            gen.writeStartObject();
            
             gen.writeFieldName('Success');
             gen.writeStartObject();
            
             	gen.writeStringField('Response', 'Information Updated succesfully ');
            
            gen.writeEndObject();
            gen.writeEndObject();
            String jsonData = gen.getAsString();
            res.responseBody = Blob.valueOf(jsonData);
            res.addHeader('Content-Type', 'application/json');
            res.statusCode = 200; 
                    
                }
                
                 API_Log__c Al = New API_Log__c();
               // Al.Account__c=rw.AccountID;
                	Al.EndpointURLName__c='NCPUpdateProfileDetails';
                 	Al.Login_Contact__c=rw.contactID;
                	Al.Response__c='User requested to update Profile details. ' +con.Id ;
                	Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al;
                
              
            }
                }
          catch(Exception e){
                String ErrorString ='Something went wrong, please contact support_SF@tecex.com';
               		res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
            		res.addHeader('Content-Type', 'application/json');
        			res.statusCode = 400;
                
                API_Log__c Al = New API_Log__c();
               // Al.Account__c=rw.AccountID;
                Al.EndpointURLName__c='NCPUpdateProfileDetails';
                Al.Response__c=e.getMessage()+' at line number: '+e.getLineNumber();
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al;
            }
      }

}