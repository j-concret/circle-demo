public class CPCEProformaCreationWrapper {
    
    public String AccountID;
    public String ContactID;
	public double estimatedChargableweight;
	public String ServiceType;
	public String Courier_responsibility;
	public String Reference1;
	public String Reference2;
	public String ShipFrom;
	public String ShipTo;
	public Integer NumberOfFinalDel;
    public double ShipmentvalueinUSD;
    
    public static CPCEProformaCreationWrapper parse(String json) {
		return (CPCEProformaCreationWrapper) System.JSON.deserialize(json, CPCEProformaCreationWrapper.class);
	}
   
	
}