public with sharing class CTRL_generateAndSendInvoice {
    
    /* multiPicklist code */
    public static Map<String,String> availableFields = new Map<String,String>{
        'Invoice Number'=>'Name',
            'Ship To Country'=>'Ship_To_Country__c',
            'Shipment Order'=>'Shipment_Order__r.Name',
            'Total Amount Due (USD)'=>'Invoice_amount_USD__c',
            'Cash Disbursement Fee'=>'Cash_Outlay_Fee_USD__c',
            'Total Amount Due if paid after Due_Date__c'=>'Total_Amount_Incl_Potential_Cash_Outlay__c',
            'PO number'=>'PO_Number__c',
            'Final Delivery Address'=>'Shipment_Order__r.Final_Delivery_Address__c',
            'International Courier Tracking Number'=>'Shipment_Order__r.Int_Courier_Tracking_No__c',
            'Domestic Courier Tracking Number'=>'Shipment_Order__r.Domestic_Tracking_Number__c',
            'Client Reference'=>'Shipment_Order__r.Client_Reference__c',
            'Client Reference 2'=>'Shipment_Order__r.Client_Reference_2__c',
            'Ship From Country'=>'Shipment_Order__r.Ship_From_Country__c',
            'Potential Cash Outlay'=>'Possible_Cash_Outlay__c',
            'Invoice Type'=>'Invoice_Type__c'
            };
                
                //Get user Email address
                @AuraEnabled
                public static String getUserEmail() {
                    return UserInfo.getUserEmail();
                }
    
    //Get INvoice record
    @AuraEnabled
    public static Invoice_New__c getInvRecord(Id invId){
        
        return  [SELECT Id, RecordType.Name, Account__c, Name, Account__r.Default_Email_Template__c ,
                 Account__r.Invoice_Email_Fields__c, Freight_Request__r.Name, Due_Date__c,
                 Shipment_Order__r.Name, Possible_Cash_Outlay__c,PO_Number_Override__c  
                 FROM Invoice_New__c 
                 WHERE Id = :invId LIMIT 1];
    }
    
    @AuraEnabled
    public static Map<String,Object> getRelatedContacts(Id invId){
        system.debug('invId :'+invId);
        Map<String,Object> response = new Map<String,Object> {'status'=>'OK'};
            try {
                Invoice_New__c invRec = getInvRecord(invId);
                system.debug('invRec :'+invRec);
                Map<String,String> emailFields = new Map<String,String>();
                if(invRec.Account__r.Invoice_Email_Fields__c != Null){
                    List<String> invoiceEmailFields = (invRec.Account__r.Invoice_Email_Fields__c).split(';');
                    System.debug('Account Invocie fields--->'+invoiceEmailFields);
                    if(invoiceEmailFields.contains('Due Date')){
						invoiceEmailFields.remove(invoiceEmailFields.indexOf('Due Date'));
						}
                    for(String label : invoiceEmailFields){
                        
                        if( (label == 'Cash Disbursement Fee' || 
                             label == 'Total Amount Due if paid after Due_Date__c'
                            ) && (invRec.Possible_Cash_Outlay__c > 0 )){
                                emailFields.put(label,availableFields.get(label));
                            }else if(label == 'PO number'){
                                if(invRec.PO_Number_Override__c != null){
                                    emailFields.put(label,'PO_Number_Override__c');  
                                }else{
                                    emailFields.put(label,availableFields.get(label));
                                }
                            }else{
                                emailFields.put(label,availableFields.get(label));
                            }
                    }
                }
                
                List<Contact> contactList = [SELECT Id, Name, Email, Title , AccountId, Account.Name
                                             FROM Contact 
                                             WHERE AccountId = :invRec.Account__c  AND Email != null AND Include_in_invoicing_emails__c  = true];
                response.put('contactList',contactList);
                
                String resultFields =  String.join(emailFields.values(), ',');
                String resultFields1;
                if(resultFields.endsWith(',')){ resultFields1 = resultFields.removeEnd(',');} else{resultFields1=resultFields;}
                
				String query = 'select '+resultFields1+',Due_Date__c from Invoice_New__c WHERE Id = :invId LIMIT 1';
                
                System.debug('query--> '+query);
                
                Invoice_New__c invoice = Database.query(query);
                
                response.put('resultFields',resultFields);
                response.put('emailFields',emailFields);
                response.put('query',query);
                response.put('Invoice_New__c',invoice);
                response.put('defaultEmail',invRec.Account__r.Default_Email_Template__c);
                
                
            } catch (Exception e) {
                throw new AuraHandledException(e.getMessage());
                
            }
        return response;
    }
    
    @AuraEnabled
    public static Boolean sendInvoice(String invId, String emailType, List<String> toAddress, List<String> ccAddress, String bodyEmail, String subjectEmail){
        Invoice_New__c invRec2 = getInvRecord(invId);
        if(toAddress!=null && !toAddress.isEmpty()) {
            invRec2.Sent_From_System__c = true;
            DateTime dT = System.now();
            Date myDate = date.newinstance(dT.year(), dT.month(), dT.day());
            invRec2.Date_Time_Stamp__c = myDate;
            invRec2.User_Stamp__c = UserInfo.getName();
            update invRec2;
        }
        Boolean isSent = false;
        
        String invoiceName;
        String invoiceNumber;
        
        if(invRec2.Freight_Request__r.Name !=null){
            invoiceName = invRec2.Freight_Request__r.Name;
            invoiceNumber = invRec2.Name;
        }
        else{   invoiceName = invRec2.Shipment_Order__r.Name; 
             invoiceNumber = invRec2.Name;
            }
        
        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
        if(emailType == 'Custom'){
            
            if(ccAddress[0] !=''){
                email.setCcAddresses(ccAddress);
            }
            //Body and header
            
            email.setToAddresses(toAddress);
            email.setSubject(subjectEmail);
            email.setHtmlBody(bodyEmail);
            email.setSaveAsActivity(true);
            
            //Attachment
            PageReference invoicePdf = invRec2.RecordType.Name == 'Client (Zee)' ? new PageReference(Site.getPathPrefix() + '/apex/ZeeClientInvoice') : new PageReference(Site.getPathPrefix() + '/apex/tecexClientInvoiceNew');
            invoicePdf.getParameters().put('id', invId);
            Blob pdfPageBlob = (Test.isRunningTest()) ?
                Blob.valueOf(String.valueOf('This is a test page')) : invoicePdf.getContentAsPDF();
            //String customerName = client.Name;   
            Attachment InvoiceAttachment  = new Attachment();
            InvoiceAttachment.Body        = pdfPageBlob;
            InvoiceAttachment.Name        = invoiceName  + '-' +'INV-'+invoiceNumber+'-'+ (System.now()).date().format() + '.pdf';
            InvoiceAttachment.ParentId    = invId;
            InvoiceAttachment.IsPrivate   = false;
            InvoiceAttachment.Description = 'Client Invoice';
            
            
            List<Messaging.EmailFileAttachment> InvoiceAttachmentsList = new List<Messaging.EmailFileAttachment>();
            Messaging.EmailFileAttachment InvoiceAttachmentFile        = new Messaging.EmailFileAttachment();
            
            InvoiceAttachmentFile.setFileName(InvoiceAttachment.Name);
            InvoiceAttachmentFile.setBody(InvoiceAttachment.Body);
            InvoiceAttachmentFile.setContentType(InvoiceAttachment.ContentType);
            
            InvoiceAttachmentsList.add(InvoiceAttachmentFile);
            email.setFileAttachments(InvoiceAttachmentsList);
            
            List<Messaging.SendEmailResult> r = Messaging.sendEmail(new List<Messaging.SingleEmailMessage> {email});
            if (r[0].isSuccess()){
                isSent = true;
                insert InvoiceAttachment;
            }
        } 
        else if(emailType == 'Default'){
            List<Contact> contactList = (List<Contact>)getRelatedContacts(invId).get('contactList');
            Contact myCon = contactList[0];
            EmailTemplate et = [SELECT Id FROM EmailTemplate WHERE Name=:'Tecex Client Invoice'];
            
            //add template
            email.setTemplateId(et.Id);
            
            //set target object for merge fields
            email.setTargetObjectId(myCon.Id);
            
            email.setwhatId(invId);
            
            if(ccAddress[0] !=''){
                email.setCcAddresses(ccAddress);
            }
            //Body and header
            email.setToAddresses(toAddress);
            //email.setSubject(subjectEmail);
            //email.setHtmlBody(bodyEmail);
            
            //Attachment
            PageReference invoicePdf = invRec2.RecordType.Name == 'Client (Zee)' ? new PageReference(Site.getPathPrefix() + '/apex/ZeeClientInvoice') : new PageReference(Site.getPathPrefix() + '/apex/tecexClientInvoiceNew');
            invoicePdf.getParameters().put('id', invId);
            Blob pdfPageBlob = (Test.isRunningTest()) ?
                Blob.valueOf(String.valueOf('This is a test page')) : invoicePdf.getContentAsPDF();
            //String customerName = client.Name;   
            Attachment InvoiceAttachment  = new Attachment();
            InvoiceAttachment.Body        = pdfPageBlob;
            InvoiceAttachment.Name        = invoiceName  + '-' +'INV-'+invoiceNumber+'-'+ (System.now()).date().format() + '.pdf';
            InvoiceAttachment.ParentId    = invId;
            InvoiceAttachment.IsPrivate   = false;
            InvoiceAttachment.Description = 'Client Invoice';
            
            
            List<Messaging.EmailFileAttachment> InvoiceAttachmentsList = new List<Messaging.EmailFileAttachment>();
            Messaging.EmailFileAttachment InvoiceAttachmentFile = new Messaging.EmailFileAttachment();
            
            InvoiceAttachmentFile.setFileName(InvoiceAttachment.Name);
            InvoiceAttachmentFile.setBody(InvoiceAttachment.Body);
            InvoiceAttachmentFile.setContentType(InvoiceAttachment.ContentType);
            
            InvoiceAttachmentsList.add(InvoiceAttachmentFile);
            email.setFileAttachments(InvoiceAttachmentsList);
            
            List<Messaging.SendEmailResult> r = Messaging.sendEmail(new List<Messaging.SingleEmailMessage> {email});
            if (r[0].isSuccess()){
                isSent = true;
                insert InvoiceAttachment;
            }
        }
        
        return isSent;
        
    }
    
    //Save Invoice
    @AuraEnabled
    public static Boolean saveInvoice(Id invId){
        
        Boolean isSaved = false;   
        
        Invoice_New__c invRec2 = getInvRecord(invId);
        String invoiceName;
        String invoiceNumber = invRec2.Name;
        
        if(invRec2.Shipment_Order__c != null && invRec2.Shipment_Order__r.Name != null){
            invoiceName = invRec2.Shipment_Order__r.Name;
        }
        
        else if(invRec2.Freight_Request__c != null && invRec2.Freight_Request__r.Name !=null){ 
            invoiceName = invRec2.Freight_Request__r.Name;
        }
        else{ 
            invoiceName = '';
        }
        
        System.debug('invRec2.Shipment_Order__r.Name :'+invRec2.Shipment_Order__r.Name);
        try{  
            //#####Invoice attachment
            PageReference invoicePdf = invRec2.RecordType.Name == 'Client (Zee)' ? new PageReference(Site.getPathPrefix() + '/apex/ZeeClientInvoice') : new PageReference(Site.getPathPrefix() + '/apex/tecexClientInvoiceNew');
            invoicePdf.getParameters().put('id', invId);
            
            Blob pdfPageBlob = (Test.isRunningTest()) ? Blob.valueOf(String.valueOf('This is a test page')) : invoicePdf.getContentAsPDF();
            
            Attachment invoiceAttachment  = new Attachment();
            invoiceAttachment.Body        = pdfPageBlob;
            invoiceAttachment.Name        = invoiceName  + '-' +'INV-'+invoiceNumber+'-'+ (System.now()).date().format() + '.pdf';
            invoiceAttachment.ParentId    = invId;
            invoiceAttachment.IsPrivate   = false;
            invoiceAttachment.Description = 'Client Invoice';
            system.debug('invoiceAttachment :: '+invoiceAttachment);
            insert invoiceAttachment;
            system.debug('invoiceAttachment ID:: '+invoiceAttachment.Id);
            isSaved = true;
        }catch (Exception e){ System.debug('Error while trying to insert Invoice From The Apex Controller ###: ' + e.getMessage());}
        
        return isSaved;
    }
    
    
    @AuraEnabled
    public static Boolean NCPsendInvoice(String invId,  List<String> toAddress, List<String> ccAddress){
        
        Invoice_New__c invRec2 = getInvRecord(invId);
        
        Boolean isSent = false;
        
        String invoiceName;
        String invoiceNumber=invRec2.name;
        
        List<OrgWideEmailAddress> owea = [select Id from OrgWideEmailAddress where Address = 'app@tecex.com'];
        
        List<EmailTemplate> lstEmailTemplates = [SELECT Id,Body, Name, Subject FROM EmailTemplate WHERE Name ='Tecex Invoice'];
        
        Invoice_New__c invoice = [Select Id, Shipment_Order__r.Client_Contact_for_this_Shipment__c from Invoice_New__c Where Id=:invId LIMIT 1];
        
        if(!lstEmailTemplates.isEmpty() && invId != null && toAddress.size() > 0) {
            
            Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
            
            
            if(ccAddress[0] !=''){
                email.setCcAddresses(ccAddress);
            }
            //Body and header
            
            email.setToAddresses(toAddress);
            email.setTemplateId(lstEmailTemplates[0].Id);
            email.setSaveAsActivity(true);
            if ( owea.size() > 0 ) {
                email.setOrgWideEmailAddressId(owea[0].Id);
            }
            email.setWhatId(invId);
            email.setTargetObjectId(invoice.Shipment_Order__r.Client_Contact_for_this_Shipment__c);
            
            
            
            Attachment NCPInvoiceAttachment = [Select Id,parentId,name,body,contenttype from Attachment where parentid=:invId limit 1];
            if(NCPInvoiceAttachment !=null){
                
                List<Messaging.EmailFileAttachment> InvoiceAttachmentsList = new List<Messaging.EmailFileAttachment>();
                Messaging.EmailFileAttachment InvoiceAttachmentFile        = new Messaging.EmailFileAttachment();
                
                InvoiceAttachmentFile.setFileName(NCPInvoiceAttachment.Name);
                InvoiceAttachmentFile.setBody(NCPInvoiceAttachment.Body);
                InvoiceAttachmentFile.setContentType(NCPInvoiceAttachment.ContentType);
                
                InvoiceAttachmentsList.add(InvoiceAttachmentFile);
                email.setFileAttachments(InvoiceAttachmentsList);
                
                List<Messaging.SendEmailResult> r = Messaging.sendEmail(new List<Messaging.SingleEmailMessage> {email});
                if (r[0].isSuccess()){
                    isSent = true;
                }
                
                
            } 
        }
        return isSent;
        
        
    }
    @AuraEnabled
    public static Map<String,object> NCPDownloadInvoice(String invId){
        Map<String,object> invoiceMap = new Map<String,object>();
        
        Invoice_New__c invRec2 = getInvRecord(invId);
        
        invoiceMap.put('isSent',false);
        
        String invoiceNumber=invRec2.name;
        
        List<Attachment> NCPInvoiceAttachment = [Select Id,parentId,name,body,contenttype from Attachment where parentid=:invId limit 1];
        if(NCPInvoiceAttachment !=null && !NCPInvoiceAttachment.isEmpty()){
            
            invoiceMap.put('NCPInvoiceAttachment',NCPInvoiceAttachment[0]);
            invoiceMap.put('isSent',true);
            
        } 
        
        return invoiceMap;
        
        
    }
    
    @Future(callout=true)
    public static void SaveInvoiceUsingTrigger(Set<Id> invoiceIds){

        Map<String,Id> docIds = new Map<String,Id>();
        
        List<ContentVersion> docs = new List<ContentVersion>();
        
        for(ContentVersion cntVer : [SELECT Id,ContentDocumentId,Title,FirstPublishLocationId FROM ContentVersion WHERE FirstPublishLocationId IN:invoiceIds]) {
            docIds.put(cntVer.FirstPublishLocationId+''+cntVer.Title,cntVer.ContentDocumentId);
        }
       
        for(Invoice_New__c inv : [SELECT Id, Freight_Request__r.Name, Shipment_Order__r.Name, RecordType.Name, Name FROM Invoice_New__c WHERE Id IN:invoiceIds]) {
        String invoiceName;
        String invoiceNumber = inv.Name;
        
        if(inv.Shipment_Order__c != null && inv.Shipment_Order__r.Name != null){
            invoiceName = inv.Shipment_Order__r.Name;
        }
        
        else if(inv.Freight_Request__c != null && inv.Freight_Request__r.Name !=null){ 
            invoiceName = inv.Freight_Request__r.Name;
        }
        else{ 
            invoiceName = '';
        }
            
            String title= invoiceName  + '-' +'INV-'+invoiceNumber+'-'+ (System.now()).date().format();
            
            PageReference costEstimatePdf = inv.RecordType.Name == 'Client (Zee)' ? new PageReference(Site.getPathPrefix() + '/apex/ZeeClientInvoice') : new PageReference(Site.getPathPrefix() + '/apex/tecexClientInvoiceNew');
            costEstimatePdf.getParameters().put('id', inv.Id);
            Blob pdfPageBlob = (Test.isRunningTest()) ? Blob.valueOf(String.valueOf('This is a test page')) : costEstimatePdf.getContentAsPDF();
            
            ContentVersion contentVersion = new ContentVersion(
                versionData = pdfPageBlob,
                title = title,
                pathOnClient = title + '.pdf',
                origin = 'C');
            
            if(docIds.containsKey(inv.Id+''+title)) {
                contentVersion.ContentDocumentId = docIds.get(inv.Id+''+title);
            }else{
                contentVersion.FirstPublishLocationId =  inv.Id;
            }
            docs.add(contentVersion);
        }
        insert docs;
    }
    
}