@isTest
public class NCPExcelHeaders_Test {

    static testMethod void testGetMethod(){
        RestRequest request = new RestRequest();
        RestResponse res = new RestResponse();
        request.requestUri ='/services/apexrest/api/Excelheader/';
        request.httpMethod = 'GET';
        RestContext.request = request;
        RestContext.response = res;
        
        Test.startTest();
        NCPExcelHeaders.ExcelHeaders();
        Test.stopTest();
    }
}