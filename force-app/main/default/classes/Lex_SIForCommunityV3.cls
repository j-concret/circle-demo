public class Lex_SIForCommunityV3 {
    
        
      
    //2. Returns already created supplier invoices selected shipment order
    @AuraEnabled
   
         public static string getCreatedSIs(ID SOID1){
         
           String[] names = new String[] {};
            for (Invoice_New__c t : [select ID, name, Shipment_Order__c ,Freight_Request__c, Conversion_Rate__c, Invoice_Name__c 
                                     from Invoice_New__c 
                                     where Shipment_Order__c =: SOID1
                                     ])
           {
               names.add(t.Invoice_Name__c);
           }
             String CreatedSupplierInvoices = String.join(names, ', ');
             return CreatedSupplierInvoices;                      
            }

    //3. Create SUpplierInvoice and Return ID and This needs to be update to cater Freight and Both (SO and Fr) in later stages
    
    @AuraEnabled
   
         public static string CreateSI(ID PSelectedSOID,ID PAccID,ID PConID,String PCurr,Date PDate,String PInvNum){
         
             String CreatedSI= 'empty';
             System.debug('PSelectedSOID' +PSelectedSOID);
              System.debug('PAccID' +PAccID);
              System.debug('PConID' +PConID);
              System.debug('PCurr' +PCurr);
              System.debug('PDate' +PDate);
             System.debug('PInvNum' +PInvNum);
             
             Currency_Management2__c CM = [select Currency__C, Conversion_rate__C from Currency_Management2__c where Currency__C =:PCurr ];
             
             
             Shipment_Order__c SO = [Select POD_Date__c from  Shipment_Order__c where Id =:PSelectedSOID];
             
             Invoice_New__c SI = new Invoice_New__c( 
             Invoice_Name__c = PInvNum, 
             Account__c =PAccID,
             Invoice_Date__c = PDate, 
             Invoice_Currency__c = PCurr,
             Conversion_Rate__c = CM.Conversion_rate__C,
             Shipment_Order__c = PSelectedSOID, 
             Invoice_Type__c = 'Invoice', 
             Invoice_Status__c = 'New',
             POD_Date_New__c = SO.POD_Date__c,
             RecordTypeId = Schema.Sobjecttype.Invoice_New__c.getRecordTypeInfosByDeveloperName().get('Supplier').getRecordTypeId());
             Insert SI;
             CreatedSI = SI.Id;
             
             return CreatedSI;                      
            }
    
    //4. Returns related list of Costing
    @AuraEnabled
   
         public static List<CPA_Costing__c> getCostings(ID SupplierInvoiceID1){
         
           
             
          Invoice_New__c SI = [select ID, name,Shipment_Order__c, Account__c,Freight_Request__c, Conversion_Rate__c from Invoice_New__c where id =: SupplierInvoiceID1 limit 1];
             
             String SOId1 = SI.Shipment_Order__c;
             String Freight1 = SI.Freight_Request__c;
             String SupplierAcc = SI.Account__c;
             
             String RelatedSOid1 = SI.Shipment_Order__c;
             String RelatedFreight1 = SI.Freight_Request__c;
             
              
             
             If(SOId1 != null && Freight1 == null){
                         
                return [SELECT Additional_Percent__c, Invoice__c, Amended__c, Amount__c, Amount_USD__c, Applied_to__c, Ceiling__c, Condition__c, Conditional_value__c, Cost_Category__c, Cost_Note__c, Cost_Type__c, CostStatus__c, Name, CPA_v2_0__c, CreatedById, Currency__c, Exchange_rate_forecast__c, Exchange_rate_payment_date__c, Floor__c, Amount_in_USD__c, Freight_Request__c, Invoice_amount_local_currency__c, Invoice_amount_USD_New__c, Invoice_currency__c, IOR_EOR__c, LastModifiedById, Max__c, Min__c, Rate__c, RecordTypeId, Shipment_Order_Old__c, Status_Colour__c, Supplier_Invoice__c, Updating__c, Variable_threshold__c, VAT_applicable__c, VAT_Rate__c, VAT_Rate_Formula__c 
                FROM CPA_Costing__c where Shipment_Order_Old__c =: SOId1  and supplier__c = :SupplierAcc and (Invoice__c ='' or Invoice__c =: SupplierInvoiceID1)  LIMIT 200];
  
             }
             else if(Freight1 != null && SOId1 == null){
                         
                return [SELECT Additional_Percent__c, Invoice__c, Amended__c, Amount__c, Amount_USD__c, Applied_to__c, Ceiling__c, Condition__c, Conditional_value__c, Cost_Category__c, Cost_Note__c, Cost_Type__c, CostStatus__c, Name, CPA_v2_0__c, CreatedById, Currency__c, Exchange_rate_forecast__c, Exchange_rate_payment_date__c, Floor__c, Amount_in_USD__c, Freight_Request__c, Invoice_amount_local_currency__c, Invoice_amount_USD_New__c, Invoice_currency__c, IOR_EOR__c, LastModifiedById, Max__c, Min__c, Rate__c, RecordTypeId, Shipment_Order_Old__c, Status_Colour__c, Supplier_Invoice__c, Updating__c, Variable_threshold__c, VAT_applicable__c, VAT_Rate__c, VAT_Rate_Formula__c 
                FROM CPA_Costing__c where Freight_Request__c =: Freight1  and supplier__c = :SupplierAcc and (Invoice__c ='' or Invoice__c =: SupplierInvoiceID1) LIMIT 200];
  
             }
             else{
                return [SELECT Additional_Percent__c, Invoice__c, Amended__c, Amount__c, Amount_USD__c, Applied_to__c, Ceiling__c, Condition__c, Conditional_value__c, Cost_Category__c, Cost_Note__c, Cost_Type__c, CostStatus__c, Name, CPA_v2_0__c, CreatedById, Currency__c, Exchange_rate_forecast__c, Exchange_rate_payment_date__c, Floor__c, Amount_in_USD__c, Freight_Request__c, Invoice_amount_local_currency__c, Invoice_amount_USD_New__c, Invoice_currency__c, IOR_EOR__c, LastModifiedById, Max__c, Min__c, Rate__c, RecordTypeId, Shipment_Order_Old__c, Status_Colour__c, Supplier_Invoice__c, Updating__c, Variable_threshold__c, VAT_applicable__c, VAT_Rate__c, VAT_Rate_Formula__c 
                FROM CPA_Costing__c where Shipment_Order_Old__c =: RelatedSOid1 and Freight_Request__c =: RelatedFreight1  and supplier__c = :SupplierAcc  and (Invoice__c ='' or Invoice__c =: SupplierInvoiceID1) LIMIT 200];                 
             }
             //and Invoice_amount_local_currency__c = null
           }
    
    //5 updating related list of Costings
    
    @AuraEnabled
    public static void updateCostings(List<CPA_Costing__c> editedAccountList,string PCurr,ID PSIID ){
        try{
           list<CPA_Costing__c> CCO = new list<CPA_Costing__c> ();
            Currency_Management2__c CM = [select Currency__C, Conversion_rate__C from Currency_Management2__c where Currency__C =:PCurr ];
            
            system.debug('PSIID---->'+PSIID);
            
            For(integer i=0; i<editedAccountList.size(); i++){
                If(editedAccountList[i].Invoice_amount_local_currency__c != null){ 
                system.debug('PeditedAccountList[i]---->'+editedAccountList[i]);
                editedAccountList[i].Exchange_rate_payment_date__c = CM.Conversion_rate__C; //get it from SOQL
                editedAccountList[i].Invoice_currency__c = PCurr;
                editedAccountList[i].Invoice__c = PSIID;
                CCO.add(editedAccountList[i]);
                 system.debug('PeditedAccountList[i]---->'+editedAccountList[i]);
                }
            }
            
            update CCO;
         //   system.debug('editedAccountList - Before'+editedAccountList);
           
         //   For(integer i=0; cco.size()>i; i++){
                // below if condition to trigger rollups  on update costings     
             //       if(i==(CCO.size()-1)){   CCO[i].Trigger__c = true; } Else {  CCO[i].Trigger__c = false;  }
           //         system.debug('i-->'+i);system.debug('Size -1-->'+(CCO.size()-1));
                
        //      update CCO[i];
       //     }
            
    //        system.debug('editedAccountList - After'+CCO);
     
      //Added below 19072019      
     //  CPA_Costing__c CPACos = [select id,trigger__C from CPA_Costing__c where Trigger__C = TRUE  and ID in: CCO limit 1 ]; 
     //  system.debug('CPACos-->'+CPACos);
     //  CPACos.trigger__C=False; 
     //  Update CPACos;
      //upto here      
            
        } catch(Exception e){
           
        }
    }
    
    
    //6. Insert Costings from supplierInvoice - Costings Flow
     
    @AuraEnabled
    public static void saveCostings(List<CPA_Costing__c> accList, ID SupplierInvoiceID1, String PCurr ){
        
        list<CPA_Costing__c> CCO = new list<CPA_Costing__c> ();
        
         Currency_Management2__c CM = [select Currency__C, Conversion_rate__C from Currency_Management2__c where Currency__C =:PCurr ];
        
         Invoice_New__c SI = [select ID, name,Shipment_Order__c,Account__c,Freight_Request__c, Conversion_Rate__c from Invoice_New__c where id =: SupplierInvoiceID1 limit 1];
          
         Shipment_Order__c  SO = [Select Id,CPA_v2_0__r.Related_Costing_CPA__c,Service_Type__c from Shipment_Order__c where id=: SI.Shipment_Order__c];
         Id AppliedCPA = so.CPA_v2_0__r.Related_Costing_CPA__c;
       
        
        For(integer i=0; i<accList.size(); i++){
            
            accList[i].CPA_v2_0__c = AppliedCPA;
            accList[i].IOR_EOR__c = SO.Service_Type__c;
            accList[i].Cost_Type__c = 'Fixed';
            accList[i].Invoice_currency__c= PCurr;
            accList[i].Shipment_Order_Old__c =So.id;
          //  accList[i].Freight_Request__c = CCO1.Freight_Request__c;
            accList[i].Invoice__c = SupplierInvoiceID1;
            //accList[i].Exchange_rate_forecast__c = CM.Conversion_rate__C;
            accList[i].Exchange_rate_payment_date__c = CM.Conversion_rate__C;
            accList[i].RecordTypeId = '0120Y000000ytNmQAI';
             accList[i].Supplier__c = SI.Account__c;
            
            
             // below if condition to trigger rollups  on insert costings     
               //     if(i==(accList.size()-1)){  system.debug('in i-->'+i);  accList[i].Trigger__c = true; } Else {  accList[i].Trigger__c = false;  }
            CCO.add(accList[i]);
            
            system.debug('CCO-->'+CCO);
            
        }
        
        
        Insert CCO;
       // Insert accList;
             
        //Added below 19072019      
     //  CPA_Costing__c CPACos = [select id,trigger__C from CPA_Costing__c where Trigger__C = TRUE  and ID in: CCO limit 1 ]; 
     //  system.debug('CPACos-->'+CPACos);
     //  CPACos.trigger__C=False; 
    //   Update CPACos;
      //upto here  
       
    
}
    

}