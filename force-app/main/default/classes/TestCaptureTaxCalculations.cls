@IsTest
public class TestCaptureTaxCalculations {
    @isTest
    public static void getTaxCalcTest(){
        Test.startTest();
        Customs_Clearance_Documents__c customs_Clearance_Documents = TestCCDUtil.createSetUpData();
        Map<String,Object> recordMap = CaptureTaxCalculations.getTaxCalc(customs_Clearance_Documents.Id);
        Test.stopTest();
        System.assertEquals('OK', recordMap.get('status'));
    }
    @isTest
    public static void getPartsTest(){
        Test.startTest();
        Customs_Clearance_Documents__c customs_Clearance_Documents = TestCCDUtil.createSetUpData();
        Map<String,Object> recordMap = CaptureTaxCalculations.getParts(customs_Clearance_Documents.Id);
		Test.stopTest();
        System.assertEquals('OK', recordMap.get('status'));
    }
}