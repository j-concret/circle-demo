@RestResource(urlMapping='/GetRODetails/*')
Global class NCPRODetial {

    @Httppost
    global static void NCPSendAllClientSO(){

        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        Blob body = req.requestBody;
        res.addHeader('Content-Type', 'application/json');
        String requestString = body.toString();
        NCPRODetialsWra rw = (NCPRODetialsWra)JSON.deserialize(requestString,NCPRODetialsWra.class);
        try {
            Access_token__c At =[select status__c,Access_Token__c from Access_token__c where Access_Token__c=:rw.Accesstoken ];
            if( at.status__C =='Active') {
                roll_out__c ROs = [select id,name,Client_Name__r.Name,Contact_For_The_Quote__r.Name,Destinations__c from roll_out__C where ID =:rw.ROID ];
                List<Shipment_order__c> SOs = [SELECT Id,Total_including_estimated_duties_and_tax__c,ETA_Formula__c,CPA_V2_0__r.ETA_Disclaimer__c,NCP_Shipping_Status__C, Compliance_Process__c,Client_Reference__c, Service_Type__c,Expiry_Date__c,Total_Cost_Range_Min__c,Insurance_Fee__c,Total_Cost_Range_Max__c,Recharge_Tax_and_Duty_Other__c,Miscellaneous_Fee_Name__c,Miscellaneous_Fee__c,EOR_and_Export_Compliance_Fee_USD__c,Total_License_Cost__c,Cash_Outlay_Cost__c,of_Unique_Line_Items__c,Client_Contact_for_this_Shipment__r.Name,Ship_to_Country__r.Name,Account__r.Name,IOR_CSE__r.name,Invoice_Timing__c,Service_Manager__r.Name,Binding_Quote__c,IOR_CSE__c,IOR_CSE__r.email,Service_Manager__c,Service_Manager__r.email,International_Courier_Tracking_URL__c,Estimate_Pre_Approval_Time_Formula__c,of_Line_Items__c,NCP_Quote_Reference__c,CreatedDate,Potential_Cash_Outlay_Fee__c,Client_Taking_Liability_Cover__c,Client_Task__c,Quote_type__c,ETA__c,    Account__c,Account__r.Cash_outlay_fee_base__c,Account__r.Finance_Charge__c,All_Taxes_and_Duties_Summary__c,Bank_Fees__c,Chargeable_Weight__c,Cost_Estimate_Number_HyperLink__c,Delivery_Contact_Name__c,Delivery_Contact_Tel_number__c,Estimate_Customs_Clearance_Time_Formula__c,Estimate_Transit_Time_Formula__c,Final_Delivery_Address__c,Government_and_In_country_Summary__c,Handling_and_Admin_Fee__c,Insurance_Fee_USD__c,International_Delivery_Fee__c,IOR_FEE_USD__c,IOR_Price_List__c,IOR_Price_List__r.Name,Name,Recharge_Handling_Costs__c,Recharge_License_Cost__c,Recharge_Tax_and_Duty__c,RecordType.Name,RecordTypeId,Shipment_Value_USD__c,Shipping_Notes_Formula__c,Shipping_Notes_New__c,Shipping_Status__c,Tax_Cost__c,Total_clearance_Costs__c,Total_Customs_Brokerage_Cost__c,Total_Invoice_Amount__c,Who_arranges_International_courier__c FROM shipment_order__c WHERE Roll_out__c =:rw.ROID];
                JSONGenerator gen = JSON.createGenerator(true);
                gen.writeStartObject();
                //Creating Array & Object - Rollout
                gen.writeFieldName('RollOut');
                gen.writeStartObject();
                if(ROs.Id == null) {gen.writeNullField('Id');} else{gen.writeStringField('Id', ROs.Id);}
                if(ROs.name == null) {gen.writeNullField('name');} else{gen.writeStringField('name', ROs.name);}
                if(ROs.Client_Name__c == null) {gen.writeNullField('AccountName');} else{gen.writeStringField('AccountName', ROs.Client_Name__r.Name);}
                if(ROs.Contact_For_The_Quote__c == null) {gen.writeNullField('ContactName');} else{gen.writeStringField('ContactName', ROs.Contact_For_The_Quote__r.Name);}
                if(ROs.Destinations__c == null) {gen.writeNullField('Destinations');} else{gen.writeStringField('Destinations', ROs.Destinations__c);}

                gen.writeEndObject();
                //End of Array & Object - RollOut
                If(!SOs.isEmpty()){
                    //Creating Array & Object - Shipment Order
                    gen.writeFieldName('ShipmentOrders');
                    gen.writeStartArray();
                    for(Shipment_Order__c SOs1 :SOs) {
                        //Start of Object - Shipment Order
                        gen.writeStartObject();

                        if(SOs1.Id == null) {gen.writeNullField('Id');} else{gen.writeStringField('Id', SOs1.Id);}
                        if(SOs1.Compliance_Process__c == null) {gen.writeNullField('ComplianceProcess');} else{gen.writeStringField('ComplianceProcess', SOs1.Compliance_Process__c);}
                        if(SOs1.Expiry_Date__c == null) {gen.writeNullField('ExpiryDate');} else{gen.writeDateField('ExpiryDate', SOs1.Expiry_Date__c);}
                        if(SOs1.Quote_type__c == null) {gen.writeNullField('Quotetype');} else{gen.writeStringField('Quotetype', SOs1.Quote_type__c);}
                        if(SOs1.ETA__c == null) {gen.writeNullField('ETA');} else{gen.writeStringField('ETA', SOs1.ETA__c);}
                        if(SOs1.Client_Task__c == null) {gen.writeNullField('ClientTask');} else{gen.writeNumberField('ClientTask', SOs1.Client_Task__c);}
                        if(SOs1.Client_Taking_Liability_Cover__c == null) {gen.writeNullField('ClientTakingLiabilityCover');} else{gen.writeStringField('ClientTakingLiabilityCover', SOs1.Client_Taking_Liability_Cover__c);}
                        /* if(SOs1.Client_Cases__c == null) {gen.writeNullField('ClientCases');} else{gen.writeNumberField('ClientCases', SOs1.Client_Cases__c);} */
                        if(SOs1.CreatedDate == null) {gen.writeNullField('CreatedDate');} else{gen.writeDatetimeField('CreatedDate', SOs1.CreatedDate);}
                        if(SOs1.NCP_Quote_Reference__c == null) {gen.writeNullField('NCPQuoteReference');} else{gen.writeStringField('NCPQuoteReference', SOs1.NCP_Quote_Reference__c);}
                        if(SOs1.of_Line_Items__c == null) {gen.writeNullField('ofLineItems');} else{gen.writeNumberField('ofLineItems', SOs1.of_Line_Items__c);}
                        if(SOs1.Estimate_Pre_Approval_Time_Formula__c == null) {gen.writeNullField('EstimatePreApprovalTimeFormula');} else{gen.writeStringField('EstimatePreApprovalTimeFormula', SOs1.Estimate_Pre_Approval_Time_Formula__c);}
                        if(SOs1.International_Courier_Tracking_URL__c == null) {gen.writeNullField('InternationalCourierTrackingURL');} else{gen.writeStringField('InternationalCourierTrackingURL', SOs1.International_Courier_Tracking_URL__c);}
                        if(SOs1.IOR_Price_List__c == null) {gen.writeNullField('IORPriceListname');} else{gen.writeStringField('IORPriceListname', SOs1.IOR_Price_List__r.name);}
                        if(SOs1.Service_Manager__c == null) {gen.writeNullField('ServiceManagerName');} else{gen.writeStringField('ServiceManagerName', SOs1.Service_Manager__r.Name);}
                        if(SOs1.IOR_CSE__c == null) {gen.writeNullField('IORCSEname');} else{gen.writeStringField('IORCSEname', SOs1.IOR_CSE__r.name);}
                        if(SOs1.Service_Manager__c == null) {gen.writeNullField('ServiceManageremail');} else{gen.writeStringField('ServiceManageremail', SOs1.Service_Manager__r.email);}
                        if(SOs1.IOR_CSE__c == null) {gen.writeNullField('IORCSEemail');} else{gen.writeStringField('IORCSEemail', SOs1.IOR_CSE__r.email);}
                        if(SOs1.Binding_Quote__c == null) {gen.writeNullField('BindingQuote');} else{gen.writeBooleanField('BindingQuote', SOs1.Binding_Quote__c);}
                        if(SOs1.Invoice_Timing__c == null) {gen.writeNullField('InvoiceTiming');} else{gen.writeStringField('InvoiceTiming', SOs1.Invoice_Timing__c);}
                        if(SOs1.Client_Reference__c == null) {gen.writeNullField('ClientReference');} else{gen.writeStringField('ClientReference', SOs1.Client_Reference__c);}
                        if(SOs1.Service_Type__c == null) {gen.writeNullField('ServiceType');} else{gen.writeStringField('ServiceType', SOs1.Service_Type__c);}
                        if(SOs1.Account__c == null) {gen.writeNullField('AccountName');} else{gen.writeStringField('AccountName', SOs1.Account__r.Name);}
                        if(SOs1.Ship_to_Country__c == null) {gen.writeNullField('ShiptoCountryName');} else{gen.writeStringField('ShiptoCountryName', SOs1.Ship_to_Country__r.Name);}
                        if(SOs1.Client_Contact_for_this_Shipment__c == null) {gen.writeNullField('ClientContactforthisShipment');} else{gen.writeStringField('ClientContactforthisShipment', SOs1.Client_Contact_for_this_Shipment__r.Name);}
                        if(SOs1.of_Unique_Line_Items__c == null) {gen.writeNullField('ofUniqueLineItems');} else{gen.writeNumberField('ofUniqueLineItems', SOs1.of_Unique_Line_Items__c);}
                        if(SOs1.Name == null) {gen.writeNullField('Name');} else{gen.writeStringField('Name', SOs1.Name);}
                        if(SOs1.Shipment_value_USD__C == null) {gen.writeNullField('ShipmentvalueUSD');} else{gen.writeNumberField('ShipmentvalueUSD', SOs1.Shipment_value_USD__C);}
                        //new
                        if(SOs1.Handling_and_Admin_Fee__c == null) {gen.writeNullField('HandlingandAdminFee');} else{gen.writeNumberField('HandlingandAdminFee', SOs1.Handling_and_Admin_Fee__c);}
                        if(SOs1.Bank_Fees__c == null) {gen.writeNullField('BankFees');} else{gen.writeNumberField('BankFees', SOs1.Bank_Fees__c);}
                        if(SOs1.Cash_Outlay_Cost__c == null) {gen.writeNullField('CashOutlayCost');} else{gen.writeNumberField('CashOutlayCost', SOs1.Cash_Outlay_Cost__c);}
                        if(SOs1.Total_Customs_Brokerage_Cost__c == null) {gen.writeNullField('TotalCustomsBrokerageCost');} else{gen.writeNumberField('TotalCustomsBrokerageCost', SOs1.Total_Customs_Brokerage_Cost__c);}
                        if(SOs1.Total_clearance_Costs__c == null) {gen.writeNullField('TotalclearanceCosts');} else{gen.writeNumberField('TotalclearanceCosts', SOs1.Total_clearance_Costs__c);}
                        if(SOs1.Recharge_Handling_Costs__c == null) {gen.writeNullField('RechargeHandlingCosts');} else{gen.writeNumberField('RechargeHandlingCosts', SOs1.Recharge_Handling_Costs__c);}
                        if(SOs1.Total_License_Cost__c == null) {gen.writeNullField('TotalLicenseCost');} else{gen.writeNumberField('TotalLicenseCost', SOs1.Total_License_Cost__c);}
                        if(SOs1.EOR_and_Export_Compliance_Fee_USD__c == null) {gen.writeNullField('EORandExportComplianceFeeUSD');} else{gen.writeNumberField('EORandExportComplianceFeeUSD', SOs1.EOR_and_Export_Compliance_Fee_USD__c);}
                        if(SOs1.IOR_FEE_USD__c == null) {gen.writeNullField('IORFEEUSD');} else{gen.writeNumberField('IORFEEUSD', SOs1.IOR_FEE_USD__c);}
                        if(SOs1.Miscellaneous_Fee__c == null) {gen.writeNullField('MiscellaneousFee');} else{gen.writeNumberField('MiscellaneousFee', SOs1.Miscellaneous_Fee__c);}
                        if(SOs1.Miscellaneous_Fee_Name__c == null) {gen.writeNullField('MiscellaneousFeeName');} else{gen.writeStringField('MiscellaneousFeeName', SOs1.Miscellaneous_Fee_Name__c);}
                        if(SOs1.Recharge_Tax_and_Duty__c == null) {gen.writeNullField('RechargeTaxandDuty');} else{gen.writeNumberField('RechargeTaxandDuty', SOs1.Recharge_Tax_and_Duty__c);}
                        if(SOs1.Recharge_Tax_and_Duty_Other__c == null) {gen.writeNullField('RechargeTaxandDutyOther');} else{gen.writeNumberField('RechargeTaxandDutyOther', SOs1.Recharge_Tax_and_Duty_Other__c);}
                        if(SOs1.International_Delivery_Fee__c == null) {gen.writeNullField('InternationalDeliveryFee');} else{gen.writeNumberField('InternationalDeliveryFee', SOs1.International_Delivery_Fee__c);}
                        if(SOs1.Insurance_Fee__c == null) {gen.writeNullField('InsuranceFee');} else{gen.writeNumberField('InsuranceFee', SOs1.Insurance_Fee__c);}
                        if(SOs1.Total_Invoice_Amount__c == null) {gen.writeNullField('TotalInvoiceAmount');} else{gen.writeNumberField('TotalInvoiceAmount', SOs1.Total_Invoice_Amount__c);}
                        if(SOs1.Estimate_Transit_Time_Formula__c == null) {gen.writeNullField('EstimateTransitTimeFormula');} else{gen.writestringField('EstimateTransitTimeFormula', SOs1.Estimate_Transit_Time_Formula__c);}
                        if(SOs1.Estimate_Customs_Clearance_Time_Formula__c == null) {gen.writeNullField('EstimateCustomsClearanceTimeFormula');} else{gen.writeStringField('EstimateCustomsClearanceTimeFormula', SOs1.Estimate_Customs_Clearance_Time_Formula__c);}
                        if(SOs1.Total_Invoice_Amount__c == null) {gen.writeNullField('TotalInvoiceAmount');} else{gen.writeNumberField('TotalInvoiceAmount', SOs1.Total_Invoice_Amount__c);}
                        if(SOs1.Total_Cost_Range_Max__c == null) {gen.writeNullField('TotalCostRangeMax');} else{gen.writeNumberField('TotalCostRangeMax', SOs1.Total_Cost_Range_Max__c);}
                        if(SOs1.Total_Cost_Range_Min__c == null) {gen.writeNullField('TotalCostRangeMin');} else{gen.writeNumberField('TotalCostRangeMin', SOs1.Total_Cost_Range_Min__c);}
                        if(SOs1.Chargeable_Weight__c == null) {gen.writeNullField('ChargeableWeight');} else{gen.writeNumberField('ChargeableWeight', SOs1.Chargeable_Weight__c);}
                        if(SOs1.Who_arranges_International_courier__c == null) {gen.writeNullField('WhoarrangesInternationalcourier');} else{gen.writeStringField('WhoarrangesInternationalcourier', SOs1.Who_arranges_International_courier__c);}
                        if(SOs1.Client_Taking_Liability_Cover__c == null) {gen.writeNullField('ClientTakingLiabilityCover');} else{gen.writeStringField('ClientTakingLiabilityCover', SOs1.Client_Taking_Liability_Cover__c);}
                        if(SOs1.Shipping_Notes_New__c == null) {gen.writeNullField('ShippingNotesNew');} else{gen.writeStringField('ShippingNotesNew', SOs1.Shipping_Notes_New__c);}
                        if(SOs1.CPA_V2_0__r.ETA_Disclaimer__c == null) {gen.writeNullField('ETADisclaimer');} else{gen.writeStringField('ETADisclaimer', SOs1.CPA_V2_0__r.ETA_Disclaimer__c);}
                        if(SOs1.NCP_Shipping_Status__C == null) {gen.writeNullField('NCPShippingStatus');} else{gen.writeStringField('NCPShippingStatus', SOs1.NCP_Shipping_Status__C);}
                        if(SOs1.ETA_Formula__c == null) {gen.writeNullField('ETAFormula');} else{gen.writeStringField('ETAFormula', SOs1.ETA_Formula__c);}
                        if(SOs1.Total_including_estimated_duties_and_tax__c == null) {gen.writeNullField('Totalincludingestimateddutiesandtax');} else{gen.writeNumberField('Totalincludingestimateddutiesandtax', SOs1.Total_including_estimated_duties_and_tax__c);}

                        gen.writeEndObject();
                        //End of Object - Shipment Order
                    }
                    gen.writeEndArray();
                    //End of Array - Shipment
                }

                gen.writeEndObject();
                String jsonData = gen.getAsString();

                res.responseBody = Blob.valueOf(jsonData);
                res.statusCode = 200;


                API_Log__c Al = New API_Log__c();
                //  Al.Account__c=logincontact.AccountID;
                // Al.Login_Contact__c=logincontact.Id;
                Al.EndpointURLName__c='RORelatedObjects';
                Al.Response__c='Success - RO RelatedObject Details are sent';
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al;




            }
        }
        catch(Exception e) {


            String ErrorString ='RO Details are not sent. Something went wrong, please contact support_SF@tecex.com';
            res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
            res.addHeader('Content-Type', 'application/json');
            res.statusCode = 500;
            API_Log__c Al = New API_Log__c();
            // Al.Account__c=rw.AccountID;
            //Al.Login_Contact__c=rw.contactID;
            Al.EndpointURLName__c='RORelatedObjects';
            Al.Response__c='error RORelatedObjects ===>'+e.getMessage();
            Al.StatusCode__c=string.valueof(res.statusCode);
            Insert Al;
        }

    }
}