@RestResource(urlMapping='/NCPGetAlltasksfordashboard/*')
global class NCPGetAlltasksfordashboard {
    
    @Httppost
    global static void NCPGetAlltasks(){
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        Blob body = req.requestBody;
        res.addHeader('Content-Type', 'application/json');
        String requestString = body.toString();
        Map<String,Task> taskMapwithMasters = new Map<String,Task>();

        NCPGetAlltasksWra rw  = (NCPGetAlltasksWra)JSON.deserialize(requestString,NCPGetAlltasksWra.class);
        try{
            String jsonData='Test';
            Set<ID> Ids = New set<ID>();
            Access_token__c at = [SELECT status__c,Access_Token__c FROM Access_token__c WHERE Access_Token__c=:rw.Accesstoken];
            if(at.status__c =='Active') {


              List<String> INclude = New List<String>();
                  INclude.add('Shipment - Compliance Pending');
                  INclude.add('Shipment - Tracking');
               
               List<Shipment_Order__C> SOList = [Select Id from Shipment_Order__C where  account__C =: rw.accountid and NCP_Shipping_Status__c in :Include and Sub_Status_Update__c !='On Hold' ORDER BY createddate DESC LIMIT 1000];
                List<Task> tasks = [SELECT Id,accountID,Shipment_Order__r.NCP_Shipping_status__C,master_Task__r.client_visible__C,Master_Task__r.task_Subject__c,Task_Category__c,Manually_Override_Inactive_Field__c,Master_Task__r.Set_Default_to_Inactive__c,Master_Task__c,Inactive__c,IsNotApplicable__c,Shipment_Order__r.recordtype.name,Shipment_Order__r.NCP_Quote_Reference__c,Shipment_Order__r.Client_Reference__c,whoId,whatId,subject,State__c,OwnerId,Description,LastClientMsgTime__c,Last_Client_viewed_time__c,LastMessageTime__c,CreatedDate,Client_Assigned_To__c FROM Task WHERE ((Shipment_Order__c in:SOList and  State__c= 'Client Pending') or (accountID =:rw.AccountID and  (State__c= 'Under Review' or State__c= 'Client Pending') )) and Master_Task__c != null  and IsNotApplicable__c = FALSE and Inactive__c = FALSE AND Master_Task__r.Client_Visible__c=TRUE  ];
                           for(Task task :tasks) {
                    taskMapwithMasters.put(task.Master_Task__c,task);
                }
                /* START : Task inactive logic */

                Set<Id> prerequisiteIds = new Set<Id>();
                Map<Id,List<Id> > masterTaskPrerequisiteIds = new Map<Id,List<Id> >();
                //Set<Id> masterTaskInactive = new Set<Id>();
                Map<Id,Set<Id> > masterTaskInactive = new Map<Id,Set<Id> >();
                for(Master_Task_Prerequisite__c mstPre : [SELECT id,Name, Master_Task_Dependent__c, Master_Task_Prerequisite__c FROM Master_Task_Prerequisite__c WHERE Master_Task_Dependent__c IN: taskMapwithMasters.keySet()]) {
                    prerequisiteIds.add(mstPre.Master_Task_Prerequisite__c);
                    if(masterTaskPrerequisiteIds.containsKey(mstPre.Master_Task_Dependent__c))
                        masterTaskPrerequisiteIds.get(mstPre.Master_Task_Dependent__c).add(mstPre.Master_Task_Prerequisite__c);
                    else
                        masterTaskPrerequisiteIds.put(mstPre.Master_Task_Dependent__c,new List<Id> {mstPre.Master_Task_Prerequisite__c});
                }

                for(Task preTask : [SELECT Id,Master_Task__c,Shipment_Order__c FROM Task WHERE Master_Task__c IN:prerequisiteIds AND Shipment_Order__c IN: SOList AND State__c !='Resolved' AND State__c !='Not Applicable' AND IsNotApplicable__c = false]) {
                    for(Id mstId : masterTaskPrerequisiteIds.keySet()) {
                        if(masterTaskPrerequisiteIds.get(mstId).contains(preTask.Master_Task__c)) {
                            if(masterTaskInactive.containsKey(preTask.Shipment_Order__c)) {
                                masterTaskInactive.get(preTask.Shipment_Order__c).add(mstId);
                            }else{
                                masterTaskInactive.put(preTask.Shipment_Order__c,new Set<Id> {mstId});
                            }
                        }
                    }
                }


                /* END : Task inactive logic */


                list<Master_Task_Template__c> mtTemplate = [SELECT Id,State__c, Name, Master_Task__c, Task_Title__c, Displayed_to__c FROM Master_Task_Template__c where Displayed_to__c = 'Client'and Master_Task__c in:taskMapwithMasters.keySet()];

                List<FeedItem> FeedsonTask= new list<FeedItem>();
                Map<Id,List<FeedItem> > FeedItemTasks = new Map<Id,List<FeedItem> >();
                Map<String,String> contenetverIds = new Map<String,String>();
                Map<String,List<ContentVersion> > contenetDocIds = new map<String,List<ContentVersion> >();
                Set<ID> FeedIds = New set<ID>();

                for(FeedItem feedItem: [select Id,ParentId,InsertedById,Title,Body,LinkUrl,createddate,visibility,CommentCount,(select Id,Title,FeedEntityId,RecordId from FeedAttachments) from FeedItem where parentid=:tasks and visibility !='InternalUsers']) {

                    FeedsonTask.add(feedItem);
                    FeedIds.add(feedItem.Id);
                    if(FeedItemTasks.containsKey(feedItem.ParentId)) {

                        List<FeedItem> FeedsList = FeedItemTasks.get(feedItem.ParentId);
                        FeedsList.add(feedItem);
                        FeedItemTasks.put(feedItem.ParentId,FeedsList);

                    }else{

                        List<FeedItem> FeedsList = new  List<FeedItem>();
                        FeedsList.add(feedItem);
                        FeedItemTasks.put(feedItem.ParentId,FeedsList);

                    }
                }

                If(!FeedsonTask.isEmpty()){
                    // Feedatt =[SELECT Id, RecordId, FeedEntityId FROM FeedAttachment WHERE FeedEntityId  in: FeedIds];
                    for(FeedAttachment feed:[SELECT Id, RecordId, FeedEntityId FROM FeedAttachment WHERE FeedEntityId in: FeedIds]) {
                        contenetverIds.put(feed.RecordId,Feed.FeedEntityId);//068
                    }

                    If(!contenetverIds.isEmpty()){
                        for(ContentVersion CD:[SELECT Id,ContentDocumentId,Title from ContentVersion where Id in:contenetverIds.keyset() ]) {
                            If(contenetverIds.containskey(CD.ID)  ){
                                System.debug('contenetDocIds--> 1 IF  -->'+contenetDocIds);
                                If(contenetDocIds.containskey(contenetverIds.get(CD.ID))  ){
                                    list<ContentVersion> abc= contenetDocIds.get(contenetverIds.get(cd.ID));
                                    abc.add(cd);
                                    contenetDocIds.put(contenetverIds.get(CD.ID),abc); //069

                                }
                                else{

                                    contenetDocIds.put(contenetverIds.get(CD.ID),new list<ContentVersion> {CD});//069


                                }
                            }
                        }


                    }

                }

                Map<Id,List<Id> > mapsOfSubs = new  Map<Id,List<Id> >();
                Map<Id,List<User> > SubsOnTask = new  Map<Id,List<User> >();
                List<Id> SubsIdList = new List<Id>();
                for(EntitySubscription subs:[select SubscriberId,parentid from EntitySubscription where parentid In: tasks ]) {
                    //mapsOfSubs.put(subs.SubscriberId,subs.parentid);
                    if(mapsOfSubs.containsKey(subs.parentid)) {
                        List<Id> SubsIdList1 = mapsOfSubs.get(subs.parentid);
                        SubsIdList1.add(subs.SubscriberId);
                        mapsOfSubs.put(subs.parentid,SubsIdList1);

                    }else{
                        List<Id> SubsIdList1 = new List<Id>();//mapsOfSubs.get(subs.parentid);
                        SubsIdList1.add(subs.SubscriberId);
                        mapsOfSubs.put(subs.parentid,SubsIdList1);
                    }

                    Ids.add(subs.SubscriberId);
                }

                System.debug('Subids'+Ids);
                //For(Integer i =0;Subscribers.size()>i;i++){Ids.add(Subscribers[i].SubscriberId);}
                //list<User> Nofifyusers =new list<User>([Select id, Firstname,Lastname from user where id in:Ids ]);
                Map<Id,User> NotifyUsers =  new Map<Id,User>();  //[Select Id, Firstname,Lastname,fullphotoUrl from user where Id in:Ids ];
                for(User usr:[Select id, Firstname,Lastname,fullphotoUrl from user where id in:Ids ]) {
                    NotifyUsers.put(usr.Id,usr);
                    /* if(mapsOfSubs.containsKey(usr.Id)){
                         if(SubsOnTask.containsKey(mapsOfSubs.get(usr.Id))){

                             List<User> NotifyUsers = SubsOnTask.get(mapsOfSubs.get(usr.Id));
                             NotifyUsers.add(usr);
                             SubsOnTask.put(mapsOfSubs.get(usr.Id),NotifyUsers);

                         }else{

                             List<User> NotifyUsers = new  List<User>();
                             NotifyUsers.add(usr);
                             SubsOnTask.put(mapsOfSubs.get(usr.Id),NotifyUsers);

                         }
                       }*/

                }


                JSONGenerator gen = JSON.createGenerator(true);
                gen.writeStartObject();
                //Creating Array & Object - Tasks
                gen.writeFieldName('Tasks');
                gen.writeStartArray();
                for(Task tsk :tasks) {

                    String title;

                    For(Master_Task_Template__c Mtt: mtTemplate ){
                        if(tsk.State__c == Mtt.State__c  && tsk.Master_Task__c == MTT.Master_Task__c) {
                            title = Mtt.Task_Title__c;
                        }
                    }

                    if(!tsk.Manually_Override_Inactive_Field__c) {
                        tsk.Inactive__c = tsk.Master_Task__r.Set_Default_to_Inactive__c ? tsk.Master_Task__r.Set_Default_to_Inactive__c : masterTaskInactive.containsKey(tsk.Shipment_Order__c) && masterTaskInactive.get(tsk.Shipment_Order__c).contains(tsk.Master_Task__c);
                    }
if(tsk.Inactive__c == FALSE){
                    //Start of Object
                    gen.writeStartObject();

                    if(tsk.Id == null) {gen.writeNullField('Id');} else{gen.writeStringField('Id', tsk.Id);}
                    if(tsk.Task_Category__c == null) {gen.writeNullField('TaskCategory');} else{gen.writeStringField('TaskCategory', tsk.Task_Category__c);}
                    if(tsk.Master_Task__c == null) {gen.writeNullField('MasterTaskId');} else{gen.writeStringField('MasterTaskId', tsk.Master_Task__c);}
                    if(tsk.master_Task__r.client_visible__C == null) {gen.writeNullField('ClientVisibility');} else{gen.writeBooleanField('ClientVisibility', tsk.master_Task__r.client_visible__C);}

                    if(tsk.Inactive__c == null) {gen.writeNullField('Inactive');} else{gen.writeBooleanField('Inactive', tsk.Inactive__c);}
                    if(tsk.IsNotApplicable__c == null) {gen.writeNullField('IsNotApplicable');} else{gen.writeBooleanField('IsNotApplicable', tsk.IsNotApplicable__c);}
                    if(tsk.Shipment_Order__r.recordtype.name == null) {gen.writeNullField('RecordTypeName');} else{gen.writeStringField('RecordTypeName', tsk.Shipment_Order__r.recordtype.name);}
                    if(tsk.Shipment_Order__r.NCP_Quote_Reference__c == null) {gen.writeNullField('NCPQuoteReference');} else{gen.writeStringField('NCPQuoteReference', tsk.Shipment_Order__r.NCP_Quote_Reference__c);}
                    if(tsk.Shipment_Order__r.NCP_Shipping_status__C  == null) {gen.writeNullField('NCPShippingstatus');} else{gen.writeStringField('NCPShippingstatus', tsk.Shipment_Order__r.NCP_Shipping_status__C);}

                    if(tsk.Shipment_Order__r.Client_Reference__c == null) {gen.writeNullField('ClientReference');} else{gen.writeStringField('ClientReference', tsk.Shipment_Order__r.Client_Reference__c);}
                    if(tsk.whoId == null) {gen.writeNullField('whoId');} else{gen.writeStringField('whoId', tsk.whoId);}
                    if(tsk.whatId == null) {gen.writeNullField('whatId');} else{gen.writeStringField('whatId', tsk.whatId);}
                    if(tsk.subject == null) {gen.writeNullField('subject');} else{gen.writeStringField('subject', tsk.subject);}
                    if(tsk.State__c == null) {gen.writeNullField('State');} else{gen.writeStringField('State', tsk.State__c);}
                    if(tsk.OwnerId == null) {gen.writeNullField('OwnerId');} else{gen.writeStringField('OwnerId', tsk.OwnerId);}
                    if(tsk.Description == null) {gen.writeNullField('Description');} else{gen.writeStringField('Description', tsk.Description);}
                    if(tsk.LastClientMsgTime__c == null) {gen.writeNullField('LastClientMsgTime');} else{gen.writeDateTimeField('LastClientMsgTime', tsk.LastClientMsgTime__c);}
                    if(tsk.Last_Client_viewed_time__c == null) {gen.writeNullField('Last_Client_viewed_time');} else{gen.writeDateTimeField('Last_Client_viewed_time', tsk.Last_Client_viewed_time__c);}
                    if(tsk.LastMessageTime__c == null) {gen.writeNullField('LastMessageTime');} else{gen.writeDateTimeField('LastMessageTime', tsk.LastMessageTime__c);}
                    if(tsk.CreatedDate == null) {gen.writeNullField('CreatedDate');} else{gen.writeDateTimeField('CreatedDate', tsk.CreatedDate);}
                    if(title == null) {gen.writeNullField('TemplateTitle');} else{gen.writeStringField('TemplateTitle', title);}
                    //  if(title == null) {gen.writeNullField('TemplateTitle');} else{gen.writeStringField('TemplateTitle', tsk.master_task__r.task_Subject__c);}
                    If(!FeedsonTask.isEmpty()){
                        //Creating Array & Object - FeedsonTask
                        gen.writeFieldName('FeedsonTask');
                        gen.writeStartArray();
                        if(FeedItemTasks != null && FeedItemTasks.containsKey(tsk.Id)) {
                            List<FeedItem> taskFeeds = new List<FeedItem>();
                            taskFeeds = FeedItemTasks.get(tsk.Id);
                            for(FeedItem taskFeed :taskFeeds) {
                                //Start of Object - taskFeeds
                                gen.writeStartObject();

                                if(taskFeed.Id == null) {gen.writeNullField('Id');} else{gen.writeStringField('Id', taskFeed.Id);}
                                if(taskFeed.ParentId == null) {gen.writeNullField('ParentId');} else{gen.writeStringField('ParentId', taskFeed.ParentId);}
                                if(taskFeed.InsertedById == null) {gen.writeNullField('InsertedById');} else{gen.writeStringField('InsertedById', taskFeed.InsertedById);}
                                if(taskFeed.Title == null) {gen.writeNullField('Title');} else{gen.writeStringField('Title', taskFeed.Title);}
                                if(taskFeed.Body == null) {gen.writeNullField('Body');} else{gen.writeStringField('Body', taskFeed.Body);}
                                if(taskFeed.LinkUrl == null) {gen.writeNullField('LinkUrl');} else{gen.writeStringField('LinkUrl', taskFeed.LinkUrl);}
                                if(taskFeed.createddate == null) {gen.writeNullField('createddate');} else{ gen.writeDateTimeField('createddate', taskFeed.createddate);}
                                if(taskFeed.CommentCount== null) {gen.writeNullField('CommentCount');} else{gen.writeNumberField('CommentCount', taskFeed.CommentCount);}

                                //  gen.writeEndObject();
                                //End of Object - taskFeed

                                System.debug('contenetDocIds--> 2  -->'+contenetDocIds.containskey(taskFeed.id));

                                if(contenetDocIds.containskey(taskFeed.id)) {
                                    //Creating Array & Object - ContentDocId
                                    gen.writeFieldName('ContentDocId');
                                    gen.writeStartArray();

                                    for(ContentVersion CD1 :contenetDocIds.get(taskFeed.id)) {
                                        //Start of Object - ContentDocId
                                        gen.writeStartObject();

                                        if(CD1.Id == null) {gen.writeNullField('Title');} else{gen.writeStringField('Title',CD1.Title);}
                                        if(CD1.Id == null) {gen.writeNullField('ContentverId');} else{gen.writeStringField('ContentverId',CD1.ID);}
                                        if(CD1.ContentDocumentID == null) {gen.writeNullField('ContentdocId');} else{gen.writeStringField('ContentdocId',CD1.ContentDocumentID);}

                                        gen.writeEndObject();
                                        //End of Object - ContentDocId

                                    }

                                    gen.writeEndArray();
                                    //End of Array - ContentDocId
                                }


                                gen.writeEndObject();
                                //End of Object - taskFeed

                            }
                        }
                        gen.writeEndArray();
                        //End of Array - taskFeeds
                    }
                    If(!mapsOfSubs.isEmpty()){
                        //Creating Array & Object - FeedsonTask
                        gen.writeFieldName('Participants');
                        gen.writeStartArray();
                        if(mapsOfSubs != null && mapsOfSubs.containsKey(tsk.Id)) {
                            List<Id> UsersIds = new List<Id>();
                            UsersIds = mapsOfSubs.get(tsk.Id);
                            for(Id usrId :UsersIds) {
                                User usr = NotifyUsers.get(usrId);
                                //Start of Object - User
                                gen.writeStartObject();
                                //  id, Firstname,Lastname
                                if(usr.Id == null) {gen.writeNullField('Id');} else{gen.writeStringField('Id', usr.Id);}
                                if(usr.Firstname == null) {gen.writeNullField('Firstname');} else{gen.writeStringField('Firstname', usr.Firstname);}
                                if(usr.Lastname == null) {gen.writeNullField('Lastname');} else{gen.writeStringField('Lastname', usr.Lastname);}
                                if(usr.fullphotoUrl == null) {gen.writeNullField('fullphotoUrl');} else{gen.writeStringField('fullphotoUrl', usr.fullphotoUrl);}

                                //  gen.writeEndObject();
                                //End of Object - User




                                gen.writeEndObject();
                                //End of Object - notifyUsers

                            }
                        }
                        gen.writeEndArray();
                        //End of Array - SubsOnTask
                    }

                    gen.writeEndObject();
                    //End of Object


                }}
                gen.writeEndArray();
                gen.writeEndObject();

                jsonData = gen.getAsString();
                res.responseBody = Blob.valueOf(jsonData);
                res.addHeader('Content-Type', 'application/json');
                res.statusCode = 200;

                API_Log__c Al = New API_Log__c();

                Al.EndpointURLName__c='NCP - NCPGetAllTasks ';
                Al.Response__c='Success - All Tasks are sent';
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al;
            }

        }
        catch(Exception e) {

            String ErrorString ='Something went wrong please contact sfsupport@tecex.com';
            res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
            res.addHeader('Content-Type', 'application/json');
            res.statusCode = 400;

            API_Log__c Al = New API_Log__c();

            Al.EndpointURLName__c='NCP - NCPGetAllTasks';
            Al.Response__c=e.getMessage()+' at line number: '+e.getLineNumber();
            Al.StatusCode__c=string.valueof(res.statusCode);
            Insert Al;
        }
    }

   

}