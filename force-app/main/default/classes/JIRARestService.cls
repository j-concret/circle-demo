public with sharing class JIRARestService {

    @future (callout=true)
    public static void createJiraIssue(Set<Id> caseIds) {
        String jiraURL = 'https://vat-it.atlassian.net';
        String CREATE_ISSUE_URL = jiraURL+'/rest/api/3/issue/bulk';
        //String endpoint = jiraURL+'/rest/api/3/issue/bulk';

        Map<String,String> priority_mapping = new Map<String,String> {
            'Show Stopper'=>'Show Stopper',
            'Low'=>'Minor',
            'Urgent'=>'Major',
            'Medium'=>'Normal'
        };

        Map<String,String> status_mapping = new Map<String,String> {
            'Escalated'=>'Waiting for Support',
            'Working'=>'In Progress',
            'Waiting'=>'Waiting for customer',
            'Closed'=>'Closed'
        };

        List<Case> cases = [SELECT Id,Jira_Case_Number__c,Subject,description,Reason, Priority, Status FROM Case WHERE Id IN: caseIds AND RecordTypeId =:Schema.SObjectType.Case.getRecordTypeInfosByName().get('Tech Support').getRecordTypeId() AND Jira_Case_Number__c = null];

        List<JiraIssue> issueUpdates = new List<JiraIssue>();
        for(Case cs : cases) {
            /* Map<String,Object> fields = new Map<String,Object>();
            fields.put('project',new Map<String,Object> {'id'=>'13000'});
            fields.put('customfield_11405',new Map<String,Object> {'value'=>'Tec EX'});
            fields.put('customfield_11408',new Map<String,Object> {'value'=>cs.Reason});
            fields.put('priority',new Map<String,Object> {'name'=>priority_mapping.get(cs.Priority)});
            fields.put('description',cs.description);
            //fields.put('status',new Map<String,Object> {'name'=>status_mapping.get(cs.Status)});
            fields.put('issuetype',new Map<String,Object> {'name'=>'IT Issue'});
            fields.put('summary',cs.Subject);
            issueUpdates.add(new Map<String,Object> {'fields'=>fields});*/
            issueUpdates.add(new JiraIssue(cs.Subject,cs.Reason,priority_mapping.get(cs.Priority),cs.description));
        }
        Map<String,Object> data = new Map<String,Object> {'issueUpdates'=>issueUpdates};
        try{
            HttpResponse res = sendRequest('POST',CREATE_ISSUE_URL, data);
            JIRAIssueResponseWrapper response = (JIRAIssueResponseWrapper)JSON.deserialize(res.getbody(), JIRAIssueResponseWrapper.class);
            if(res.getStatusCode() == 201) {
                for(Integer i=0; i<response.issues.size(); i++) {
                    cases[i].Jira_Case_Number__c = response.issues[i].key;
                }
                RecusrsionHandler.isCaseTriggerSkip = true;
                update cases;
            }else{
                System.debug(res.getBody());
            }
        } catch(Exception e) {
            System.debug('ERROR:' + e);
        }
    }

    private static HttpResponse sendRequest(String method,String endpoint,Object data){
        String username = 'anilk@tecex.com';
        String api_token = 'qxay9B57x5NqLZNOQVnl149B';
        System.debug(JSON.serializePretty(data));

        //Construct HTTP request and response
        HttpRequest req = new HttpRequest();

        Http http = new Http();

        //Construct Authorization and Content header
        Blob headerValue = Blob.valueOf(username+':'+api_token);
        String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
        req.setHeader('Authorization', authorizationHeader);
        req.setHeader('Content-Type','application/json');

        //Set Method and Endpoint and Body
        req.setMethod(method);
        req.setEndpoint(endpoint);

        req.setBody(JSON.serialize(data));
        //Send endpoint to JIRA
        return http.send(req);
    }
}