@IsTest
public class NCPPickupAddressList_Test {

    static testMethod void testPostMethodElseCondition(){
        Access_token__c accessTokenObj = new Access_token__c();
        accessTokenObj.Status__c = 'Active';
        accessTokenObj.Access_Token__c = 'asdfghj-sdfgfds-sfdfgf';
        
        Insert accessTokenObj;
        
        Account acc = new Account(Name = 'Testing');
        insert acc;
        
        Id recordTypeId = Schema.SObjectType.Client_Address__c.getRecordTypeInfosByDeveloperName().get('PickUp').getRecordTypeId();

        Client_Address__c clientAddr1 = new Client_Address__c(Client__c=acc.Id,RecordTypeId =recordTypeId,Contact_Full_Name__c= 'TEstName',Contact_Email__c ='TEst@test.com',Contact_Phone_Number__c = '0722854399',CompanyName__c= 'CompanyName', Address__c='Adddress',All_Countries__c = 'Brazil',Province__c = 'Trest',Postal_Code__c='304309',City__c='City');
        insert clientAddr1;
        
        NCPFinalDestinationsWrap wrapperObj = new NCPFinalDestinationsWrap();
        wrapperObj.Accesstoken = accessTokenObj.Access_Token__c;
        wrapperObj.ClientAccId = acc.Id;
        wrapperObj.Country = 'Brazil';
        
        RestRequest request = new RestRequest();
        RestResponse res = new RestResponse();
        request.requestUri ='/services/apexrest/api/PickupAddressList/';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueOf(JSON.serialize(wrapperObj));
        RestContext.request = request;
        RestContext.response = res;
        
        Test.startTest();
        NCPPickupAddressList.NCPPickupAddressList();
        Test.stopTest(); 
        
    }
    
    static testMethod void testPostMethodIfCondition(){
        Access_token__c accessTokenObj = new Access_token__c();
        accessTokenObj.Status__c = 'Active';
        accessTokenObj.Access_Token__c = 'asdfghj-sdfgfds-sfdfgf';
        
        Insert accessTokenObj;
        
        Account acc = new Account(Name = 'Testing');
        insert acc;
        
        Id recordTypeId = Schema.SObjectType.Client_Address__c.getRecordTypeInfosByDeveloperName().get('PickUp').getRecordTypeId();

        Client_Address__c clientAddr1 = new Client_Address__c(Client__c=acc.Id,Contact_Full_Name__c= 'TEstName',Contact_Email__c ='TEst@test.com',Contact_Phone_Number__c = '0722854399',CompanyName__c= 'CompanyName', Address__c='Adddress',RecordTypeId =recordTypeId,All_Countries__c = 'Brazil',Address_Status__c = 'Active',Province__c = 'Trest',Postal_Code__c='304309' ,City__c='City');
        insert clientAddr1;
        
        NCPFinalDestinationsWrap wrapperObj = new NCPFinalDestinationsWrap();
        wrapperObj.Accesstoken = accessTokenObj.Access_Token__c;
        wrapperObj.ClientAccId = acc.Id;
        wrapperObj.Country = 'Brazil';
        
        RestRequest request = new RestRequest();
        RestResponse res = new RestResponse();
        request.requestUri ='/services/apexrest/api/PickupAddressList/';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueOf(JSON.serialize(wrapperObj));
        RestContext.request = request;
        RestContext.response = res;
        
        Test.startTest();
        NCPPickupAddressList.NCPPickupAddressList();
        Test.stopTest(); 
        
    }
}