@RestResource(urlMapping='/NCPGetTaskDetails/*')
Global class NCPGetTask {

    @Httppost
    global static void NCPGetAlltasks(){
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        Blob body = req.requestBody;
        res.addHeader('Content-Type', 'application/json');
        String requestString = body.toString();
        NCPGetTaskWrapper rw  = (NCPGetTaskWrapper)JSON.deserialize(requestString,NCPGetTaskWrapper.class);
        try{
            Set<ID> Ids = New set<ID>();
            Access_token__c at = [SELECT status__c,Access_Token__c FROM Access_token__c WHERE Access_Token__c=:rw.Accesstoken];
            if(at.status__c =='Active') {
                 Set<ID> FeedIds = New set<ID>(); 
                 Map<String,String> contenetverIds = new Map<String,String>(); 
                 Map<String,List<ContentVersion>> contenetDocIds = new Map<String,List<ContentVersion>>(); 
                 Map<String,Object> task = TaskDetailPageCtrl.getTaskRecord(rw.TaskID, true);

                  List<FeedItem> FeedsonTask= new list<FeedItem>([select Id,ParentId,InsertedById,Title,Body,visibility,LinkUrl,createddate,CommentCount,(select Id,Title,FeedEntityId,RecordId from FeedAttachments) from FeedItem where parentid=:rw.TaskID and visibility !='InternalUsers']);
                  List<Task> Soinfoontask= new list<Task>([select Id,Shipment_Order__c,Shipment_Order__r.name,Shipment_Order__r.NCP_Quote_Reference__c,
                                                           Shipment_Order__r.Client_Reference__c,
                                                           Shipment_Order__r.NCP_Shipping_Status__c, 
                                                           Shipment_Order__r.Customs_Compliance__c,
                                                           Shipment_Order__r.Shipping_Documents__c,
                                                           Shipment_Order__r.Pick_up_Coordination__c,
                                                           Shipment_Order__r.Invoice_Payment__c,
                                                           Shipment_Order__r.Cost_Estimate_Acceptance_Date__c,
                                                           Shipment_Order__r.Shipment_Value_USD__c,
                                                           Shipment_Order__r.Final_Deliveries_New__c,
                                                            Shipment_Order__r.Chargeable_Weight__c,
                                                            Shipment_Order__r.of_Line_Items__c,
                                                            Shipment_Order__r.Service_Type__c,
                                                            Shipment_Order__r.Ship_to_Country_new__c,
                                                            Shipment_Order__r.Shipping_Status__c,
                                                            Shipment_Order__r.Ship_From_Country__c,
                                                            Shipment_Order__r.Destination__c,
                                                            Shipment_Order__r.CPA_v2_0__r.Courier_AWB_Incoterm__c,
                                                           Shipment_Order__r.CPA_v2_0__r.FF_AWB_Incoterm__c
                                                           
                                                           from Task where id=:rw.TaskID]);
                 

                For(Integer i =0;FeedsonTask.size()>i;i++){FeedIds.add(FeedsonTask[i].ID);}
                
                
                     If(!FeedsonTask.isEmpty()){
                      
                     for(FeedAttachment feed:[SELECT Id, RecordId, FeedEntityId FROM FeedAttachment WHERE FeedEntityId  in: FeedIds]){
                                contenetverIds.put(feed.RecordId,Feed.FeedEntityId);//068
                         	}
                         
                     If(!contenetverIds.isEmpty()){
                          for(ContentVersion CD:[SELECT Id,ContentDocumentId,Title from ContentVersion where Id in:contenetverIds.keyset() ]){
                              If(contenetverIds.containskey(CD.ID)  ){
                                  System.debug('contenetDocIds--> 1 IF  -->'+contenetDocIds);
                                   If(contenetDocIds.containskey(contenetverIds.get(CD.ID))  ){
                                       list<ContentVersion> abc= contenetDocIds.get(contenetverIds.get(cd.ID));
                                       abc.add(cd);
                                       contenetDocIds.put(contenetverIds.get(CD.ID) ,abc);//069
                                      
                                   }
                                  else{
                                      
                                       contenetDocIds.put(contenetverIds.get(CD.ID) ,new list<ContentVersion>{CD});//069
                                      
                                      
                                  }
                              }
                         }
                         
                         
                     }
                     
                     }
                
                  List<EntitySubscription> Subscribers = new  List<EntitySubscription>([select SubscriberId  from EntitySubscription where  parentid=:rw.TaskID ]);
                    For(Integer i =0;Subscribers.size()>i;i++){Ids.add(Subscribers[i].SubscriberId);}
                    list<User> Nofifyusers =new list<User>([Select id, Firstname,Lastname,fullphotoUrl from user where id in:Ids ]);  
                
                
                if(!Soinfoontask.isEmpty()){
                    List<Object> Soinfo = new List<Object>();
                   		 for(Task tsk :Soinfoontask){
                             Map<String,Object> genMap1 = new Map<String,Object>();
                              if(tsk.Id != null) {genMap1.put('Id', tsk.Id);}
                              if(tsk.Shipment_Order__c != null) {genMap1.put('ShipmentOrderID', tsk.Shipment_Order__c);}
                              if(tsk.Shipment_Order__r.name != null) {genMap1.put('ShipmentOrdername', tsk.Shipment_Order__r.name);}
                              if(tsk.Shipment_Order__r.NCP_Quote_Reference__c != null) {genMap1.put('NCPQuoteReference', tsk.Shipment_Order__r.NCP_Quote_Reference__c);}
                              if(tsk.Shipment_Order__r.Client_Reference__c != null) {genMap1.put('ClientReference', tsk.Shipment_Order__r.Client_Reference__c);}
                              if(tsk.Shipment_Order__r.NCP_Shipping_Status__c != null) {genMap1.put('NCPShippingStatus', tsk.Shipment_Order__r.NCP_Shipping_Status__c);}
                              if(tsk.Shipment_Order__r.Customs_Compliance__c != null) {genMap1.put('CustomsCompliance', tsk.Shipment_Order__r.Customs_Compliance__c);}
                              if(tsk.Shipment_Order__r.Shipping_Documents__c != null) {genMap1.put('ShippingDocuments', tsk.Shipment_Order__r.Shipping_Documents__c);}
                              if(tsk.Shipment_Order__r.Pick_up_Coordination__c != null) {genMap1.put('PickupCoordination', tsk.Shipment_Order__r.Pick_up_Coordination__c);}
                              if(tsk.Shipment_Order__r.Invoice_Payment__c != null) {genMap1.put('InvoicePayment', tsk.Shipment_Order__r.Invoice_Payment__c);}
                              if(tsk.Shipment_Order__r.Cost_Estimate_Acceptance_Date__c != null) {genMap1.put('Cost_Estimate_AcceptanceDate', tsk.Shipment_Order__r.Cost_Estimate_Acceptance_Date__c);}
                              if(tsk.Shipment_Order__r.Shipment_Value_USD__c != null) {genMap1.put('ShipmentValueUSD', tsk.Shipment_Order__r.Shipment_Value_USD__c);}
                              if(tsk.Shipment_Order__r.Final_Deliveries_New__c != null) {genMap1.put('FinalDeliveriesNew', tsk.Shipment_Order__r.Final_Deliveries_New__c);}
                              if(tsk.Shipment_Order__r.Chargeable_Weight__c != null) {genMap1.put('ChargeableWeight', tsk.Shipment_Order__r.Chargeable_Weight__c);}
                             if(tsk.Shipment_Order__r.of_Line_Items__c != null) {genMap1.put('ofLineItems', tsk.Shipment_Order__r.of_Line_Items__c);}
                             if(tsk.Shipment_Order__r.Service_Type__c != null) {genMap1.put('ServiceType', tsk.Shipment_Order__r.Service_Type__c);}
                             if(tsk.Shipment_Order__r.Ship_to_Country_new__c != null) {genMap1.put('ShiptoCountrynew', tsk.Shipment_Order__r.Ship_to_Country_new__c);}
                             if(tsk.Shipment_Order__r.Ship_From_Country__c != null) {genMap1.put('ShipFromCountry', tsk.Shipment_Order__r.Ship_From_Country__c);}
                             if(tsk.Shipment_Order__r.Shipping_Status__c != null) {genMap1.put('ShippingStatus', tsk.Shipment_Order__r.Shipping_Status__c);}
                             if(tsk.Shipment_Order__r.Destination__C != null) {genMap1.put('Destination', tsk.Shipment_Order__r.Destination__c);}
							 if(tsk.Shipment_Order__r.CPA_v2_0__r.Courier_AWB_Incoterm__c != null) {genMap1.put('CourierAWBIncoterm', tsk.Shipment_Order__r.CPA_v2_0__r.Courier_AWB_Incoterm__c );}
 							 if(tsk.Shipment_Order__r.CPA_v2_0__r.FF_AWB_Incoterm__c != null) {genMap1.put('FFAWBIncoterm', tsk.Shipment_Order__r.CPA_v2_0__r.FF_AWB_Incoterm__c);}

                             Soinfo.add(genMap1);   
                   		 }
                     task.put('SOInfo',Soinfo) ; 
                
                    
                }
               
                   If(!FeedsonTask.isEmpty()){
                     //Creating Array & Object - Feedsoncase
                    
                       List<Object> taskFeedObj = new List<Object>();
                    
                     for(FeedItem taskFeed :FeedsonTask) {
                         //Start of Object - Feedsoncase
                         Map<String,Object> genMap = new Map<String,Object>();
							 
                         if(taskFeed.Id != null) {genMap.put('Id', taskFeed.Id);}
                         if(taskFeed.ParentId != null) {genMap.put('ParentId', taskFeed.ParentId);}
                         if(taskFeed.InsertedById != null) {genMap.put('InsertedById', taskFeed.InsertedById);}
                         if(taskFeed.Title != null) {genMap.put('Title', taskFeed.Title);}
                         if(taskFeed.Body != null) {genMap.put('Body', taskFeed.Body);}
                         if(taskFeed.LinkUrl != null) {genMap.put('LinkUrl', taskFeed.LinkUrl);}
                         if(taskFeed.createddate != null) {genMap.put('createddate', taskFeed.createddate);}
                         if(taskFeed.CommentCount != null) {genMap.put('CommentCount', taskFeed.CommentCount);}
                                            
                         System.debug('contenetDocIds--> 2  -->'+contenetDocIds.containskey(taskFeed.id));
                       
                         if(contenetDocIds.containskey(taskFeed.id)){
                             //Creating Array & Object - ContentDocId
                     			
                             List<Map<String,Object>> cntDcId = new List<Map<String,Object>>();
                             for(ContentVersion CD1 :contenetDocIds.get(taskFeed.id)) {
                                   Map<String,Object> genMapCV = new Map<String,Object>();
                                        if(CD1.Title != null){genMapCV.put('Title',CD1.Title);}
                                  		if(CD1.Id != null){genMapCV.put('ContentverId',CD1.ID);}
                         				if(CD1.ContentDocumentID != null){genMapCV.put('ContentdocId',CD1.ContentDocumentID);}
                                 
                                  cntDcId.add(genMapCV);
                         		//End of Object - ContentDocId
                                  
                              }
                             
                           genMap.put('ContentDocId',cntDcId);
                     //End of Array - ContentDocId 
                         }
                          //End of Object - Feedsoncase
                       taskFeedObj.add(genMap);
                     }
                     task.put('FeedsOnTask',taskFeedObj) ; 
                         If(!Nofifyusers.isEmpty()){
                             List<Map<String,Object>> UsersList = new List<Map<String,Object>>();
                             for(User usr : Nofifyusers){
                                  Map<String,Object> genMapUser = new Map<String,Object>();
								 if(usr.Id != null) {genMapUser.put('Id',usr.Id);} 
                                if(usr.Firstname != null) {genMapUser.put('Firstname',usr.Firstname);}
                                if(usr.Lastname != null) {genMapUser.put('Lastname',usr.Lastname);} 
                                if(usr.fullphotoUrl != null) {genMapUser.put('fullphotoUrl',usr.fullphotoUrl);} 
                                                               
							UsersList.add(genMapUser);
 
                             }
                            
						 task.put('Participants',UsersList);
							}
                     //End of Array - Feedsoncase
                 }
                 //jsonData = gen.getAsString();
                   //task.put('feedsONTask',gen);
               // task.put('template',taskTemp);
                res.responseBody = Blob.valueOf(JSON.serializePretty(task));
                res.addHeader('Content-Type', 'application/json');
                res.statusCode = 200;

                API_Log__c Al = New API_Log__c();
                //  Al.Account__c=rw.AccountID;

                Al.EndpointURLName__c='NCP - NCPGetTask';
                Al.Response__c='Success - Task sent';
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al;
            }
        }
        catch(Exception e) { System.debug('Error-->'+e.getMessage()+' at line number: '+e.getLineNumber());
            String ErrorString ='Something went wrong please contact sfsupport@tecex.com';
            res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
            res.addHeader('Content-Type', 'application/json');
            res.statusCode = 400;

            API_Log__c Al = New API_Log__c();
            // Al.Account__c=rw.AccountID;

            Al.EndpointURLName__c='NCP - NCPGetTask';
            Al.Response__c=e.getMessage()+' at line number: '+e.getLineNumber();
            Al.StatusCode__c=string.valueof(res.statusCode);
            Insert Al;
        }
    }
}