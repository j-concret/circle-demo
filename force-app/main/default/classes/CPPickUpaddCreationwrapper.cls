public class CPPickUpaddCreationwrapper {
     public List<PickUpAddress> PickUpAddress;

	public class PickUpAddress {
		public String Name;
		public String Contact_Full_Name;
		public String Contact_Email;
		public String Contact_Phone_Number;
		public String Address1;
		public String Address2;
		public String City;
		public String Province;
		public String Postal_Code;
		public String All_Countries;
		public String ClientID;
        public String AdditionalNumber;
     	public String Comments;
        public String CompanyName;
        public String DefaultAddress;
        public String AddressStatus;
	}

	
	public static CPPickUpaddCreationwrapper parse(String json) {
		return (CPPickUpaddCreationwrapper) System.JSON.deserialize(json, CPPickUpaddCreationwrapper.class);
	}
    
    

}