@RestResource(urlMapping='/NCPDownlodCostestimate/*')
Global class NCPDownloadCostestimate {
    
    @Httppost
    global static void NCPDownloadattachment(){
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        Blob body = req.requestBody;
        res.addHeader('Content-Type', 'application/json');
        String requestString = body.toString();
        System.debug('requestString :'+requestString);
        NCPDownloadattachmentwra rw  = (NCPDownloadattachmentwra)JSON.deserialize(requestString,NCPDownloadattachmentwra.class);
        Try{
            
            Access_token__c At =[select status__c,Access_Token__c from Access_token__c where Access_Token__c=:rw.Accesstoken ];
            if( at.status__C =='Active' ){
                
                Id quoteRecordTypeId = Schema.SObjectType.Shipment_Order__c.getRecordTypeInfosByName().get('Cost Estimate').getRecordTypeId();
                Id orderRecordTypeId = Schema.SObjectType.Shipment_Order__c.getRecordTypeInfosByName().get('Shipment Order').getRecordTypeId();
                
                Shipment_Order__C  ShipmentOrder =  [ Select Id,RecordTypeId FROM Shipment_Order__c WHERE Id =: rw.RecordID LIMIT 1];
                
                List<Attachment> att= new List<Attachment>();                    
                
                for(Attachment attach:[Select Id,ParentId, Name,body,ContentType From Attachment where ParentId=:rw.RecordID order by Createddate desc]){
                    if(ShipmentOrder.RecordTypeId == orderRecordTypeId &&
                       attach.Name.contains('Approved CostEstimate')){
                           att.add(attach);  
                           break;
                       }else if(ShipmentOrder.RecordTypeId == quoteRecordTypeId &&
                                !attach.Name.contains('Approved CostEstimate') &&
                                attach.Name.contains('CostEstimate')){
                                    att.add(attach);  
                                    break;
                                }
                }
                List<attmnt> oppatts = new list<attmnt>();
                
                if(att.size()>0 && att != null){
                    for(Attachment ats:att){
                        attmnt a=new attmnt();               
                        a.AttachmentType=ats.ContentType;
                        a.AttachmentName=ats.Name;
                        a.AttachmentId=ats.Id;
                        
                        // attachment body to convert to blob format.
                        string MyFile=EncodingUtil.base64Encode(ats.body);
                        Blob MyBlob=EncodingUtil.base64Decode(MyFile);
                        
                        a.AttachmentBody=MyBlob;
                        
                        oppatts.add(a);
                        
                    } 
                }    
                string Jsonstring=Json.serialize(oppatts);
                
                res.responseBody=Blob.valueOf(Jsonstring);
                res.statusCode=200;  
                API_Log__c Al = New API_Log__c();
                Al.EndpointURLName__c='NCPDownlodCostestimate';
                Al.StatusCode__c=string.valueof(res.statusCode);
                Al.Response__c = 'attachments are downloaded to --' +rw.recordID;
                Insert Al;
                
            }
        }
        catch(Exception e){
            String ErrorString ='Something went wrong, please contact Sfsupport@tecex.com';
            res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
            res.addHeader('Content-Type', 'application/json');
            res.statusCode = 400;
            
            API_Log__c Al = New API_Log__c();
            //Al.Account__c=rw.AccountID;
            //Al.Login_Contact__c=rw.contactId;
            Al.EndpointURLName__c='NCPDownlodCostestimate';
            Al.Response__c=e.getMessage()+' at line number: '+e.getLineNumber();
            Al.StatusCode__c=string.valueof(res.statusCode);
            Insert Al;
            
            
        }
        
        
    }
    
    global class attmnt{       
        public string AttachmentType{get;set;}
        public string AttachmentName{get;set;}
        public string AttachmentId{get;set;}
        public blob AttachmentBody{get;set;}
    }  
    
    
}