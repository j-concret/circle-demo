@IsTest
public class NCPCV_Test {
    
    static testMethod void testGetMethod()
    {   
        Country_Validations__c conVal = new Country_Validations__c(Action_type__c='Toaster - Error');
        insert conVal;
        
        RestRequest request = new RestRequest();
        RestResponse res = new RestResponse();
        request.requestUri ='/services/apexrest/api/NCPAPICountryValidations/*';
        request.httpMethod = 'GET';
        RestContext.request = request;
        RestContext.response = res;
        Test.startTest();
        NCPCV.NCPCV();
        Test.stopTest();
        
    }
}