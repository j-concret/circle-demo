public class getPowerBIRequestToken {
    
    public static String gettoken(){
        String bodyStr ='client_id='+Power_Bi_Requests__c.getInstance('PowerBi').client_id__c+'&resource='+Power_Bi_Requests__c.getInstance('PowerBi').resource__c+'&client_secret='+Power_Bi_Requests__c.getInstance('PowerBi').client_secret__c+'&grant_type='+Power_Bi_Requests__c.getInstance('PowerBi').grant_type__c ;
        
        String length = string.valueof(bodyStr.length());
        
        Http P=new Http();
        HttpRequest req = new HttpRequest();
                req.setEndpoint('https://login.microsoftonline.com/4167dd15-4d14-4264-85e4-e113f2f76e9f/oauth2/token');
        req.setbody(bodyStr);
        req.setMethod('POST');
        req.setHeader('Content-Type','application/x-www-form-urlencoded');
        req.setHeader('Content-Length', length);
        req.setTimeout(120000);
        HttpResponse response = P.send(req);
        
        system.debug('response'+response);
        system.debug('response'+response.getBody());
        String token='';
        JSONParser parser = JSON.createParser(response.getBody());
        while (parser.nextToken() != null) {
            if(parser.getCurrentName() == 'access_token') {
                parser.nextValue();
                token = parser.getText();
                break;
            }
        }
        return token; 
    }
    
    
    public static String getEmbedURL(String Tok){
        String Endpoint = 'https://api.powerbi.com/v1.0/myorg/groups/'+Power_Bi_Requests__c.getInstance('PowerBi').WorkSpace_ID__c+'/reports/'+Power_Bi_Requests__c.getInstance('PowerBi').reportId__c;    //  'client_id='+Power_Bi_Requests__c.getInstance('PowerBi').client_id__c+'&resource='+Power_Bi_Requests__c.getInstance('PowerBi').resource__c+'&client_secret='+Power_Bi_Requests__c.getInstance('PowerBi').client_secret__c+'&grant_type='+Power_Bi_Requests__c.getInstance('PowerBi').grant_type__c ;
        System.debug('Endpoint-->'+Endpoint);
        //  String length = string.valueof(bodyStr.length());
        
        Http P=new Http();
        HttpRequest req = new HttpRequest();
        
        req.setEndpoint(Endpoint);
        
        req.setMethod('GET');
        req.setHeader('Content-Type','application/x-www-form-urlencoded');
        req.setHeader('Authorization','Bearer '+Tok);
        //   req.setHeader('Content-Length', length);
        req.setTimeout(120000);
        HttpResponse response = P.send(req);
        
        system.debug('response'+response);
        system.debug('response'+response.getBody());
        String embeUrl='';
        JSONParser parser = JSON.createParser(response.getBody());
        while (parser.nextToken() != null) {
            if(parser.getCurrentName() == 'embedUrl') {
                parser.nextValue();
                embeUrl = parser.getText();
                break;
            }
        }
        
        
        return embeUrl; 
    }
    
    
    
    
}