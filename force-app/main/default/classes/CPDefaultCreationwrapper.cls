public class CPDefaultCreationwrapper {
    public List<DefaultPref> DefaultPref;

	public class DefaultPref {
		public String ClientAccID;
        public String ContactID;
		public String Chargeable_Weight_Units;
		public String ion_Batteries;
		public String Order_Type;
		public String Package_Dimensions;
		public String Package_Weight_Units;
		public String Second_hand_parts;
		public String Service_Type;
		public String TecEx_to_handle_freight;
        public String Country;
	}

	
	public static CPDefaultCreationwrapper parse(String json) {
		return (CPDefaultCreationwrapper) System.JSON.deserialize(json, CPDefaultCreationwrapper.class);
	}

}