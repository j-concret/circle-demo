public class BankTransactionProcessController {
    
    @AuraEnabled
    public static Map<String,Object> getRecordTypes(String objectName){
        Map<String,Object> response = new Map<String,Object> {'status'=>'OK'};
            try {
                User oUser = [select Id,Username,Alias,Country,Email,FirstName,LastName,IsActive FROM User Where Id =: userInfo.getUserId()];
                Map<Id,RecordType> recordTypeList = new Map<Id,RecordType>([select Id,Name from RecordType where sObjectType=:objectName]);
                response.put('recordTypeList',recordTypeList);
                response.put('user',oUser);
            } catch (Exception e) {
                response.put('status','ERROR');
                response.put('msg',e.getMessage());
            }
        return response;
    }
    
    @AuraEnabled
    public static Map<String,Object> createBankTransactions(String bankTransactionJson){
        List<Id> bankTransactionIds = new List<Id>();
        List<Id> supplierIds = new List<Id>();
        Map<String,Object> response = new Map<String,Object> {'status'=>'OK'};
            try {
                List<Bank_transactions__c> bankTransactionList = (List<Bank_transactions__c>)JSON.deserialize(bankTransactionJson,List<Bank_transactions__c>.class);
                upsert bankTransactionList;
                for(Bank_transactions__c bnkTransaction:bankTransactionList){
                    bankTransactionIds.add(bnkTransaction.Id);
                    supplierIds.add(bnkTransaction.Supplier__c);
                }
                response.put('data',getBTAndInvoiceDetails(bankTransactionIds,supplierIds));
            } catch (Exception e) {
                response.put('status','ERROR');
                response.put('msg',e.getMessage());
            }
        return response;
    }
    
    @AuraEnabled
    public static Map<String,Object> createBankTransactionsApplied(String bankTransactionId,String SupplierId,String bankTransactionJson){
        System.debug('bankTransactionJson ++> '+bankTransactionJson);
        Map<String,Object> response = new Map<String,Object> {'status'=>'OK'};
            try {
                List<Bank_Transaction_Applied__c> bankTransactionAppliedList = (List<Bank_Transaction_Applied__c>)JSON.deserialize(bankTransactionJson,List<Bank_Transaction_Applied__c>.class);
                List<Id> bankTransactionIds = new List<Id>{bankTransactionId};
                    List<Id> supplierIds = new List<Id>{SupplierId};
                        insert bankTransactionAppliedList;
                response.put('data',getBTAndInvoiceDetails(bankTransactionIds,supplierIds));
            } catch (Exception e) {
                response.put('status','ERROR');
                response.put('msg',e.getMessage());
            }
        return response;
    }
    
    @AuraEnabled
    public static Map<String,Object> getBankTransactions(Id bankAccountId){
        Map<String,Object> response = new Map<String,Object> {'status'=>'OK'};
            try {
                List<String> excludedBTStatuses = new List<String>{'Closed','Forex difference'};
                List<Bank_transactions__c> bankTransactionsList= [SELECT Id , Name, Supplier__c, Supplier__r.Name , Date__c , 
                                                              Amount_USD__c , Applied_amount__c , 
                                                              Unapplied__c , BT_Status__c  
                                                              FROM Bank_transactions__c 
                                                              where Supplier__c = :bankAccountId 
                                                              AND BT_Status__c NOT IN :excludedBTStatuses 
                                                              AND Unapplied__c != 0
                                                             ORDER BY Name];
                response.put('bankTransactionsList',bankTransactionsList);
            } catch (Exception e) {
                response.put('status','ERROR');
                response.put('msg',e.getMessage());
            }
        return response;
        
    }
    
    @AuraEnabled
    public static Map<String,Object> getBTAndInvoiceDetails(List<Id> bankTransactionIds,List<Id> supplierIds){
        Map<String,Object> response = new Map<String,Object>{'status'=>'OK'};
        List<Object> bTInvoiceList = new List<Object>();
        List<Account> accList = [select Id , Name , 
                                 (select Id , Name , RecordType.Id , RecordType.Name , Unapplied__c , Applied_amount__c , BT_Status__c 
                                  from Payments2__r  
                                  where Id IN :bankTransactionIds),
                                 (select Name , Id , RecordType.Id , RecordType.Name ,
                                  Invoice_Name__c , Amount_Outstanding__c , Penalty_Outstanding_Amount__c 
                                  from Invoice__r 
                                  where Invoice_Status__c IN ('Invoice Sent','FM Approved','Partial Payment','Paid via Stripe') 
                                  OR Penalty_Status__c = 'Approved' 
                                  ORDER BY Due_Date__c) 
                                 from Account 
                                 where Id IN :supplierIds];
        for(Account acc: accList){
            List<Bank_transactions__c> bankTransactionList = acc.Payments2__r;
            if(bankTransactionList != null){
                for(Bank_transactions__c bankTransaction:bankTransactionList){
                    Map<String,Object> bTInvoice = new Map<String,Object>();
                    bTInvoice.put('bankTransactionId', bankTransaction.Id);
                    bTInvoice.put('name', bankTransaction.Name);
                    bTInvoice.put('RecordTypeId', bankTransaction.RecordType.Id);
                    bTInvoice.put('RecordTypeName', bankTransaction.RecordType.Name);
                    bTInvoice.put('supplierId', acc.Id);
                    bTInvoice.put('supplierName', acc.Name);
                    bTInvoice.put('unapplied', bankTransaction.Unapplied__c);
                    bTInvoice.put('appliedAmount', bankTransaction.Applied_amount__c);
                    bTInvoice.put('btStatus', bankTransaction.BT_Status__c);
                    bTInvoice.put('isBTApplied', false);
                    if(acc.Invoice__r != null) bTInvoice.put('invoiceList', acc.Invoice__r);
                    bTInvoiceList.add(bTInvoice);
                }
            }
        }
        
        response.put('bTInvoiceList',bTInvoiceList);
        return response;
    }
    
    @AuraEnabled
    public static Map<String,Object> sendBankTransactions(String blobData){
        
        Map<String,Object> response = new Map<String,Object> {'status'=>'OK'};
            try {
                String currentDate = System.Now().format();
                Messaging.EmailFileAttachment csvAttc = new Messaging.EmailFileAttachment();
                blob csvBlob = EncodingUtil.base64Decode(blobData);
                string csvname= 'BT-'+currentDate+'.xls';
                csvAttc.setFileName(csvname);
                csvAttc.setBody(csvBlob);
                Messaging.SingleEmailMessage email =new Messaging.SingleEmailMessage();
                String[] toAddresses = new list<string> {UserInfo.getUserEmail()};
                    //String[] ccAddresses = new list<string> {'biancabe@tecex.com'};
                    String[] ccAddresses = new list<string> {'luhar.aadil@concret.io'};    
                    String subject ='Bank Transaction';
                email.setSubject(subject);
                email.setToAddresses( toAddresses );
                email.setCcAddresses(ccAddresses);
                email.setPlainTextBody('Please see the attached bank movement for '+System.Now().format('dd/MM/YYYY'));
                email.setFileAttachments(new Messaging.EmailFileAttachment[]{csvAttc});
                Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});
                
            } catch (Exception e) {
                response.put('status','ERROR');
                response.put('msg',e.getMessage());
            }
        return response;
    }
    
    
}