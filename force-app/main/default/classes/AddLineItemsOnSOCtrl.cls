public class AddLineItemsOnSOCtrl {

    @AuraEnabled
    public static String createLineItems(Boolean fromProCatalogue,String recordId,String lineItems){
        try{
            RecusrsionHandler.runTaskInQueueable = true;
            if(fromProCatalogue) {
                List<SObject> insertParts = new List<SObject>();
                Shipment_Order__c shipmentOrder = [SELECT Id,Account__c FROM Shipment_Order__c WHERE Id=:recordId];
                String accountId = shipmentOrder.Account__c;
                Map<String,LineItemWrapper> parts = new Map<String,LineItemWrapper>();
                List<LineItemWrapper> partList = (List<LineItemWrapper>)JSON.deserialize(lineItems,List<LineItemWrapper>.class);
                for(LineItemWrapper part : partList) {
                    parts.put(part.internal_reference,part);
                }

                Set<String> internalRefs = parts.keySet();

                String query = 'SELECT Id,Unit_Price__c,Client_Reference__c ';
                query += ',Part__r.'+String.join(new List<String>(Part__c.sObjectType.getDescribe().fields.getMap().keySet()), ',Part__r.');

                query += ' FROM Product_Catalogue__c WHERE Account__c =:accountId AND Client_Reference__c IN:internalRefs';
                for(SObject lineItem : Database.query(query)) {
                    SObject part = lineItem.getSObject('Part__r');
                    SObject clonedPart = part.clone(false, false, false, false);
                    String internalRef = String.valueOf(lineItem.get('Client_Reference__c'));
                    LineItemWrapper wrapper = parts.get(internalRef);
                    clonedPart.put('Quantity__c',wrapper.quantity);
                    if(wrapper.unit_price != null && wrapper.unit_price != 0)
                        clonedPart.put('Commercial_Value__c',wrapper.unit_price);
                    else
                        clonedPart.put('Commercial_Value__c',(Decimal)(lineItem.get('Unit_Price__c')));
                    clonedPart.put('Shipment_Order__c',recordId);
                    insertParts.add(clonedPart);
                }

                insert insertParts;
            }else{
                List<Part__c> parts = (List<Part__c>)JSON.deserialize(lineItems,List<Part__c>.class);
                insert parts;
            }

            return 'Line items created successfully';
        }catch(Exception e) {
            throw new AuraHandledException('Error Occurred: '+ e.getMessage());
        }
    }

    private class LineItemWrapper {
        public String internal_reference;
        public Integer quantity;
        public Decimal unit_price;
    }
}