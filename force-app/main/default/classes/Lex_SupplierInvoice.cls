public with sharing class Lex_SupplierInvoice {
    
     private static Set< String > m_profiles_set = new Set< String >{  'TecEx Customer Lightning Community', 'Tecex Customer','Test Tecex Community plususer','TecEx Supplier','Test Supplier'};

       /** this function will get the account and contact objects automatically
     *  @return ServerResponse object will be returned
     */
    @AuraEnabled
    public static ServerResponse getAccountAndContact(){

        ServerResponse response = new ServerResponse();           //create our server response object
        List< Account > account_list;                             //will hold accounts returned
        List< Contact > contact_list;                             //contact we are auto filling
        Id contact_id;                                              //get the contact ID

        //check if user doesnt have contact ID or should not autopupuate
        if( !shouldAutoPopulate() ){

            response.result_message = 'NA';             //user does'nt have contact
            return response;                            //return the response

        }//end of if-block

        response = getContactID();                      //get the contact ID

        //check if we have an error
        if( response.is_error )
            return response;                            //return the error message
        //check if no contact ID was returned
        else if( response.result_message == null ){

            response.is_error = true;                   //indicate we have an error
            response.result_message = 'No Contact Record Defined';              //indicate we have an error

            return response;                                //indicate response

        }//end of if-block

        contact_id = response.result_message;           //get the contact ID

        try{

            //select contact
            contact_list = [ SELECT Id, Name, AccountId
                             FROM Contact
                             WHERE Id =: contact_id ];

            //check if we have no values returned or no account linked
            if( ( contact_list.size() == 0 ) || ( contact_list[0].AccountId == null ) ){

                response.result_message = 'NA';         //contact not found
                return response;                        //return the response

            }//end of if-block

            //select account
            account_list = [ SELECT Id, Name
                             FROM Account
                             WHERE Id =: contact_list[0].AccountId ];

        }
        catch( QueryException query_exception ){

            System.debug( 'In function LEX_NewRollOutController.getAccountAndContact --- ' + query_exception.getMessage() );

            return getErrorResponse( query_exception.getMessage() );            //return the response

        }//end of try-catch

        response.sobject_list = new List< SObject >();                //create the list
        response.sobject_list.add( account_list[0] );                 //account list
        response.sobject_list.add( contact_list[0] );                 //add the contact
        response.result_message = 'auto-populate';                      //we gonna out populate

        return response;                //send the response

    }//end of function definition





    /**this function will determine if the user is a community user
     * @return will return true on success
     */
    @AuraEnabled
    public static ServerResponse isCommunityUser(){

        String user_profile;                            //this will hold the current user's profile
        ServerResponse response = new ServerResponse();     //will hold the server response

        try{

            user_profile = [ SELECT Name
                             FROM Profile
                             WHERE Id =: UserInfo.getProfileId() ].Name;

        }
        catch( QueryException query_exception ){

            System.debug( 'In Function LEX_NewRollOutController.isCommunityUser --- ' + query_exception.getMessage() );

            return getErrorResponse( query_exception.getMessage() );            //return the response object

        }//end of try-catch

        response.result_message = String.valueOf( m_profiles_set.contains( user_profile ) );        //set the string value
        return response;                        //return the response object

    }//end of function definition





    /** this function will check if the account and contact fields
     *  should be auto populated
     *  @return true if auto populate
     */
    private static Boolean shouldAutoPopulate(){

        String user_profile;                                                //this will hold the current user's profile

        try{

            user_profile = [ SELECT Name
                             FROM Profile
                             WHERE Id =: UserInfo.getProfileId() ].Name;

        }
        catch( QueryException query_exception ){

            System.debug( 'In Function LEX_NewRollOutController.shouldAutoPopulate --- ' + query_exception.getMessage() );
            return false;

        }//end of try-catch

        return m_profiles_set.contains( user_profile );         //return check

    }//end of function definition





    /** this function will get the associated contact record ID
     *  @return contact ID will be returned
     */
    private static ServerResponse getContactID(){

        ServerResponse response = new ServerResponse();                 //create our response object
        List< User > user_list;                                         //this will hold the list of users

        try{

            user_list = [ SELECT ContactID  //ContactID
                          FROM User
                          WHERE Id =: UserInfo.getUserId() ];

        }
        catch( QueryException query_exception ){

            System.debug( 'In function LEX_NewRollOutController --- ' + query_exception.getMessage() );         //print the exception

            return getErrorResponse( query_exception.getMessage() );

        }//end of try-catch block

        response.result_message = user_list[0].ContactID;                               //set the contact ID

        return response;                    //return the response object

    }//end of function definition





    /** this function will prepare our error response object
     *  @param messageP is the message we are returning to the user
     *  @return ServerResponse object will be returned
     */
    @TestVisible
    public static ServerResponse getErrorResponse( String messageP ){

        ServerResponse response = new ServerResponse();                         //this is our server response

        //setup our response object
        response.is_error = true;
        response.result_message = messageP;

        return response;                    //return the response

    }//end of function definition





    /** this is class for serializing return data
     *
     */
    public class ServerResponse{

        /** this will indicate if response is an error message
         */
        @AuraEnabled
        public Boolean is_error{ get; set; }

        /** this is message from server
         */
        @AuraEnabled
        public String result_message{ get; set; }

        /** this is list of SObjects returned
         */
        @AuraEnabled
        public List< SObject > sobject_list { get; set; }

        /** this is the account object we are sending back
         */


        /** this is default class constructor
         */
        public ServerResponse(){

            is_error = false;               //we assume no error by default
            result_message = '';            //empty result message
            sobject_list = null;          //set the list to null

        }//end of constructor definition

    }//end of class definition
    
      //1. Returns already created supplier invoices selected shipment order
    @AuraEnabled
   
         public static string getCreatedSIs(ID SOID1){
         
           String[] names = new String[] {};
            for (Supplier_Invoice__c t : [select ID, name,Shipment_Order_Name__c,Freight_Request__c from Supplier_Invoice__c where Shipment_Order_Name__c =: SOID1 ])
           {
               names.add(t.name);
           }
             String CreatedSupplierInvoices = String.join(names, ', ');
             
             return CreatedSupplierInvoices;                      
                         
             
            
         }

    

}