@isTest

public class btNewMonthProcess_Test {

   
    @isTest
    public static void btNewMonthProcessBatchTest(){
        
        List<Bank_transactions__c> btList = new List<Bank_transactions__c>();
        List<GL_Account__c> glAccountList = new List<GL_Account__c>();
        Id paymentRecordTypeId = Schema.SObjectType.Bank_transactions__c.getRecordTypeInfosByName().get('Payment').getRecordTypeId();
        Id receiptRecordTypeId = Schema.SObjectType.Bank_transactions__c.getRecordTypeInfosByName().get('Receipt').getRecordTypeId();
        Id accountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Client').getRecordTypeId();
        
                
        Accounting_Period__c accountingPeriod = new Accounting_Period__c(Name = '2020-11', 
                                                                         Start_Date__c = Date.newInstance(2020, 11, 1), 
                                                                         End_Date__c = Date.newInstance(2020, 11, 30), 
                                                                         Status__c = 'Open');
        Insert accountingPeriod;
     
        Dimension__c dimension = new Dimension__c(Name = 'IOR', Dimension_Type_Code__c = 'BRA', 
                                                  Dimension_Description__c = 'Testing', 
                                                  Short_Dimension_Description__c = 'IOR');
        Insert dimension;  
                
        Account acc = new Account(Name = 'Acme Test', Region__c = 'USA', Dimension__c = dimension.Id, 
                                  Invoicing_Term_Parameters__c = 'No Terms', IOR_Payment_Terms__c = 32, 
                                  Invoice_Timing__c = 'Upfront invoicing', RecordTypeId = accountRecordTypeId );
        
        Insert acc;
        
    GL_Account__c glAccount1 = new GL_Account__c(Name = '920600', GL_Account_Description__c = 'Revenue received in advance', 
                                                    GL_Account_Short_Description__c = 'RRIA', 
                                                    Currency__c = 'US Dollar (USD)');
        glAccountList.add(glAccount1);
        
        GL_Account__c glAccount2 = new GL_Account__c(Name = '611500', GL_Account_Description__c = 'Prepayments', 
                                                    GL_Account_Short_Description__c = 'Prepayments', 
                                                    Currency__c = 'US Dollar (USD)');
        glAccountList.add(glAccount2);
        
        GL_Account__c glAccount3 = new GL_Account__c(Name = '624256', GL_Account_Description__c = 'Testing the Batch', 
                                                    GL_Account_Short_Description__c = 'Testing the Batch', 
                                                    Currency__c = 'US Dollar (USD)');
        glAccountList.add(glAccount3);
        
           
        
        Insert glAccountList;
        
                    
        Bank_transactions__c BT1 = new Bank_transactions__c( Supplier__c = acc.Id, Date__c = Date.newInstance(2020,11,11), 
                                                            Type__c = 'Shipment', Amount__c = 2220.00, Conversion_Rate__c = 1.0, Bank_charges__c = 1000, Bank_Account_New__c = glAccount3.Id, Currency__c ='US Dollar (USD)' 
                                                           	,Accounting_Period_New__c = accountingPeriod.Id,RecordTypeId = paymentRecordTypeId);
        btList.add(BT1);
        
         Bank_transactions__c BT2 = new Bank_transactions__c( Supplier__c = acc.Id, Date__c = Date.newInstance(2020,11,11), 
                                                            Type__c = 'Shipment', Amount__c = 21220.00, Conversion_Rate__c = 1.0, Bank_charges__c = 1000, Bank_Account_New__c = glAccount3.Id ,Currency__c ='US Dollar (USD)' 
                                                            ,Accounting_Period_New__c = accountingPeriod.Id, RecordTypeId = receiptRecordTypeId);
        btList.add(BT2);       
        
            
        Insert btList;      
        
        Test.startTest();
        	btNewMonthProcess obj = new btNewMonthProcess();
        	Database.executeBatch(obj);
        Test.stopTest();
        
        List<Transaction_Line_New__c> transactionLineList = [Select Id, Description__c from Transaction_Line_New__c];
        
        System.assert(transactionLineList.size() > 0, 'Checking the size of the Transaction Line List.');
        System.assert(transactionLineList != null);


    }

}