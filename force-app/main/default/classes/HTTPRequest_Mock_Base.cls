/**
 * This implements the HTTPMock Callout interface and form the parent class of HTTPCalloutMocks
 * to allow us to test HTTP callouts
 */
global virtual class HTTPRequest_Mock_Base implements HttpCalloutMock
{
	protected HTTPRequest_Base request;
	  
	public HTTPRequest_Mock_Base()
	{
		System.Type requestClass = getHTTPRequestClass();
		System.assert(requestClass != null);
		request = (HTTPRequest_Base)requestClass.newInstance();
	}
	
	global HTTPResponse respond(HTTPRequest request) 
	{
		validateRequest(request);

		HttpResponse response = new HttpResponse();
        setResponse(request, response);
        return response;
    }
	
	protected virtual System.Type getHTTPRequestClass()
	{
		return HTTPRequest_Base.class;	
	}
	
	protected Set<String> getValidOrgIds()
	{
		return HTTPRequest_Base.VALID_ORG_IDS;	
	}
	
	protected String getValidEndPoint()
	{
		return request.GetWebServiceEndPoint();	
	}
	
	protected String getValidEncoding()
	{
		return request.getEncoding();	
	}
		
	protected virtual void validateRequest(HTTPRequest request)
    {
    	String httpEndPoint = request.getEndpoint();
		System.assert(String.isNotBlank(httpEndPoint));
		
		//Now check that the endpoints are ones that we expect
		System.assert(httpEndPoint.equals(getValidEndPoint()));

		String httpMethod = request.getMethod();
		System.assertEquals(httpMethod, HTTPRequest_Base.HTTP_VERB_POST);
		
		String httpContentType = request.getHeader(HTTPRequest_Base.HTTP_CONTENT_TYPE);
		System.assertEquals(httpContentType, getValidEncoding());
		
    }
    
    protected virtual void setResponse(HTTPRequest request, HttpResponse response)
    {
    	response.setHeader(HTTPRequest_Base.HTTP_CONTENT_TYPE, this.request.getEncoding());
        response.setStatusCode(HTTPRequest_Base.HTTP_OK);
    }
    
    protected void setErrorResponse(HttpResponse response)
    {
    	response.setStatusCode(HTTPRequest_Base.HTTP_INTERAL_SERVER_ERROR);
        response.setBody(HTTPRequest_Base.ERR_INTERNAL_SERVER);			
    }
 
}