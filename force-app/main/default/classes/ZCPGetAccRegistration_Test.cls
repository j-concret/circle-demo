@isTest
public class ZCPGetAccRegistration_Test {
    
    @testSetup
    public static void setup(){
        Account acc = new Account (name='TecEx Prospective Client', Type ='Client',New_Invoicing_Structure__c = true);
        insert acc;
        
        Registrations__c reg = new Registrations__c(Name='reg1',Company_name__c = acc.Id,Country__c='Australia');
        insert reg;
        
        Access_token__c accessTokenObj = new Access_token__c();
        accessTokenObj.Status__c = 'Active';
        accessTokenObj.Access_Token__c = 'asdfghj-sdfgfds-sfdfgf';
        
        Insert accessTokenObj;
    }
    
    public static testMethod void testCase1(){
        
        Account Acc = [SELECT Id FROM Account where name='TecEx Prospective Client' LIMIT 1];
        Access_token__c accessToken = [SELECT Id,Access_Token__c FROM Access_token__c LIMIT 1];
        
        ZCPGetRegistrationwra obj = new ZCPGetRegistrationwra();
        obj.Accesstoken = accessToken.Access_Token__c;
        obj.AccountID = Acc.Id;
        obj.toCountry = 'Australia';
        
        Blob body = Blob.valueOf(JSON.serialize(obj));
        
        RestRequest req1 = new RestRequest();
        RestResponse res1 = new RestResponse();
        req1.requestURI = '/services/apexrest/Getregistration'; 
        req1.httpMethod = 'POST';
        req1.requestBody = Blob.valueOf(JSON.serialize(obj));     
        RestContext.request = req1;
        RestContext.response = res1;
        RestContext.request.addHeader('Content-Type', 'application/json');
        
        Test.startTest(); 
        ZCPGetAccRegistration.ZCPGetAccRegistration();
        Test.stopTest();
    }
    
     public static testMethod void testCase2(){
        
        Account Acc = [SELECT Id FROM Account where name='TecEx Prospective Client' LIMIT 1];
        Access_token__c accessToken = [SELECT Id,Access_Token__c FROM Access_token__c LIMIT 1];
        
        ZCPGetRegistrationwra obj = new ZCPGetRegistrationwra();
        obj.Accesstoken = accessToken.Access_Token__c;
        obj.AccountID = 'ABC';
        obj.toCountry = 'Australia';
        
        Blob body = Blob.valueOf(JSON.serialize(obj));
        
        RestRequest req1 = new RestRequest();
        RestResponse res1 = new RestResponse();
        req1.requestURI = '/services/apexrest/Getregistration'; 
        req1.httpMethod = 'POST';
        req1.requestBody = Blob.valueOf(JSON.serialize(obj));     
        RestContext.request = req1;
        RestContext.response = res1;
        RestContext.request.addHeader('Content-Type', 'application/json');
        
        Test.startTest(); 
        ZCPGetAccRegistration.ZCPGetAccRegistration();
        Test.stopTest();
    }
}