@RestResource(urlMapping='/NCPattachdocuments/*')
Global  class NCPattachdocument {
    
       @Httppost
      global static void NCPattachdocument(){
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        Blob body = req.requestBody;
    	res.addHeader('Content-Type', 'application/json');
        String requestString = body.toString();
        NCPattachdocumentWra rw  = (NCPattachdocumentWra)JSON.deserialize(requestString,NCPattachdocumentWra.class);
          Try{
              
              Access_token__c At =[select status__c,Access_Token__c from Access_token__c where Access_Token__c=:rw.Accesstoken ];
            	if( at.status__C =='Active' ){
                    //Network nw= [Select Id, name  from Network where name = 'New-ClientPortal'];
                    Network nw= [select Id, name from network where name = 'TecEx App'];
                    	if(!rw.Atts.isempty()){
                                        list<Attachment> atchs=new List<Attachment>();
                             			list<ContentVersion> CVS=new List<ContentVersion>();//new
                            			list<ContentDocumentLink> CDLL= new List<ContentDocumentLink>(); //new
                            
                                        For(Integer i=0;rw.Atts.size()>i;i++){
                                            
                                            Attachment a = new Attachment (ParentId = rw.recordID, Body =rw.Atts[i].FileBody,                                                     
                                            Name =rw.Atts[i].FileName); //,contenttype=rw.LineItems[i].FileType
                                            atchs.add(a);
                                            
                                            //new start
                                             ContentVersion cv = new ContentVersion();
                                                   cv.ContentLocation = 'S';
                                                   cv.VersionData =rw.Atts[i].filebody;
                                                   cv.NetworkId=nw.Id;// acc2-0DB1q00000000ozGAA,Prod - 0DB1v000000kB98GAE
                                                     // cv.VersionData = EncodingUtil.base64Decode(base64Data);
                                                   cv.Title = rw.Atts[i].fileName;
                                                   cv.PathOnClient = rw.Atts[i].fileName;
                                            	 
                                                    CVS.add(CV);
                                            
                                            //New end
                                            
                                            
                                            
                                        }
                                       // insert atchs; 
                            			insert Cvs;
                            
                            
                            list<ContentVersion> InCVS=[Select ID,contentDocumentID from ContentVersion where id IN:Cvs];
                            if(!InCVS.Isempty()){ 				
                            					For(Integer i=0;InCVS.size()>i;i++){
                                                    ContentDocumentLink CDL= new ContentDocumentLink();
                                                    CDL.LinkedEntityId= rw.recordID; 
                                                    CDL.ContentDocumentId=InCVS[i].contentDocumentID;
                                                    CDL.ShareType='V';
                                                    
                                                    CDL.Visibility='ALLUsers';
                                                 CDLL.add(CDL);
                             						}
                                			Insert CDLL;
                            					}
                            
                                   } 
                
                 
                  	JSONGenerator gen = JSON.createGenerator(true);
            		gen.writeStartObject();
            
             		gen.writeFieldName('Success');
             		gen.writeStartObject();
            
             			if(rw.recordid == null) {gen.writeNullField('Response');} else{gen.writeStringField('Response','Attachements are added to  '+rw.recordID);}
            
            		gen.writeEndObject();
            		gen.writeEndObject();
            		String jsonData = gen.getAsString();
            		res.responseBody = Blob.valueOf(jsonData);
            		res.addHeader('Content-Type', 'application/json');
            		res.statusCode = 200;    
                API_Log__c Al = New API_Log__c();
                Al.EndpointURLName__c='NCPattachdocuments';
                Al.StatusCode__c=string.valueof(res.statusCode);
                Al.Response__c = 'attachments are added to --' +rw.recordID;
                Insert Al;
                
                  
                  
                  
              }        
          }
          Catch(Exception e){
              
              String ErrorString ='Something went wrong, please contact Sfsupport@tecex.com';
               		res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
            		res.addHeader('Content-Type', 'application/json');
        			res.statusCode = 400;
                
                API_Log__c Al = New API_Log__c();
                //Al.Account__c=rw.AccountID;
                //Al.Login_Contact__c=rw.contactId;
                Al.EndpointURLName__c='NCPattachdocuments';
                Al.Response__c=e.getMessage()+' at line number: '+e.getLineNumber();
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al;
                
              
              
          }



      }

}