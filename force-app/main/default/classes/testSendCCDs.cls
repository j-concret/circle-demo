@isTest
public class testSendCCDs {

   private static testMethod void  myTest()
   
   {
   
        Account account = new Account (name='Orange Business Services Test', Type ='Client', CSE_IOR__c = '0050Y000001km5c');
        insert account;
        
        Account account2 = new Account (name='Test Supplier', Type ='Supplier', ICE__c = '0050Y000001km5c');
        insert account2;      
          
        country_price_approval__c cpa = new country_price_approval__c( name ='Aruba', Billing_term__c = 'DAP/CIF - IOR pays',  Airwaybill_Instructions__c = 'User IOR Address', In_Country_Specialist__c = '0050Y000001km5c', Supplier__c = account2.Id );
        insert cpa;
        
        IOR_Price_List__c iorpl = new IOR_Price_List__c ( Client_Name__c = account.Id, Name = 'Aruba', IOR_Fee__c = 0.15, On_Charge_Mark_up__c = 0.15,
        TecEx_Shipping_Fee_Markup__c =0.40, IOR_Min_Fee__c = 100,  Admin_Fee__c =100, Set_up_fee__c =100, Bank_Fees__c =200, Tax_Rate__c = 0.20,
        Estimated_Customs_Brokerage__c =250, Estimated_Customs_Clearance__c =250, Estimated_Customs_Handling__c =250, Estimated_Import_License__c =250);
        insert iorpl; 
     
        Contact contact = new Contact ( lastname ='Testing Individual',  AccountId = account.Id, Email = 'testing@tecex.com');
        insert contact; 
        
        Opportunity opportunity = new Opportunity(AccountId = account.id, Name ='120-547', Ship_to_Country__c = cpa.Id, IOR_Price_List__c = iorpl.Id, Shipment_Value_USD__c =1000,
        Ship_From_Country__c = 'Algeria', CloseDate= system.today(), StageName ='Shipment Pending', Client_Contact_for_this_Shipment__c = contact.Id, Who_arranges_International_courier__c ='Client', Tax_Treatment__c ='DAP/CIF - IOR pays' );
        insert opportunity;
     
        Shipment_Order__c shipmentOrder = new Shipment_Order__c(Account__c = account.id, Ship_to_Country__c = cpa.Id, IOR_Price_List__c = iorpl.Id, Shipment_Value_USD__c =1000, Opportunity__c = opportunity.Id,
        Client_Contact_for_this_Shipment__c = contact.Id, Who_arranges_International_courier__c ='Client', Tax_Treatment__c ='DAP/CIF - IOR pays', Shipping_Status__c ='Shipment Pending', ICE__c = '0050Y000001km5c', IOR_CSE__c = '0050Y000001km5c'  );
        insert shipmentOrder;
        
        Customs_Clearance_Documents__c CCD = new Customs_Clearance_Documents__c(Name= 'Testing Documents',  Customs_Clearance_Documents__c = shipmentOrder.Id, Foreign_Exchange_Rate__c = 1,    Status__c = 'Uploaded',   CCD_Document_Sent__c = FALSE);
        insert CCD;
        
        Attachment attach = new Attachment();  
        attach.Name='Unit Test Attachment';
        Blob bodyBlob = Blob.valueOf('Unit Test Attachment Body');
        attach.body=bodyBlob;
        attach.parentId= CCD.id;
        insert attach;

        


          CCD.Status__c = 'Reviewed';
          Update CCD;      

  
   
     
       
    } 

   }