@RestResource(urlMapping='/GetDestinationlist/*')
Global class NCPAPIDestinations {
@Httpget
    global static void NCPAPIDestinations(){
         RestRequest req = RestContext.request;
         RestResponse res = RestContext.response;

        Schema.DescribeFieldResult F = Shipment_Order__c.Destination__c.getDescribe();
        Schema.sObjectField T = F.getSObjectField();
        List<PicklistEntry> entries = T.getDescribe().getPicklistValues();

                 res.responseBody = Blob.valueOf(JSON.serializePretty(entries));
        		 res.addHeader('Content-Type', 'application/json');
        	 	 res.statusCode = 200;

}
}