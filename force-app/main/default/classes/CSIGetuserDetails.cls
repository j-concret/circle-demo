@RestResource(urlMapping='/MyDetails/*')
global class CSIGetuserDetails {
    @Httppost
 global static void NCPGetuserDetails6(){	
    	String jsonData='Test';
        RestRequest req = RestContext.request;
    	RestResponse res = RestContext.response;
         Blob body = req.requestBody;
         String requestString = body.toString();
         CSIGetAccessTokenWra rw = (CSIGetAccessTokenWra)JSON.deserialize(requestString,CSIGetAccessTokenWra.class);
   // String ClientuserID = req.params.get('ClientuserID');//0050E0000076EKGQA2
    ID contactId = [Select contactid from User where id =: rw.ClientuserID].contactId;
    ID AccID  = [Select AccountID from Contact where id =: contactid].AccountId;
     
     Account AC = [Select Id,CSE_IOR__c,CSE_IOR__r.Firstname,CSE_IOR__r.Lastname,CSE_IOR__r.FullPhotoUrl from Account where ID =:AccID];
     
     try {
         
        // List<Contact> con = [Select id,firstname,lastname,email from contact where id =: contactId limit 1];
        user u= [select id,name,email,FullPhotoUrl,contactid from user where id =:rw.ClientuserID limit 1];
      //user u= [select id,name,email,FullPhotoUrl,contactid from user where id ='0050E0000076EKGQA2' limit 1]; 
            system.debug('FullPhotoUrl-->'+u.FullPhotoUrl);
         String Accesstoken = GuidUtilCP.NewGuid();
         	System.debug('Accesstoken'+Accesstoken);
            string Base64Accesstoken = EncodingUtil.base64Encode(Blob.valueof(Accesstoken));
            System.debug('Base64Accesstoken'+Base64Accesstoken);
                
                 Access_token__c At = New Access_token__c();
                	At.Access_Token__c=Accesstoken;
             		At.AccessToken_encoded__c=Base64Accesstoken;
                 Insert At;  
         
             JSONGenerator gen = JSON.createGenerator(true);
             gen.writeStartObject();
             
         
             //Creating Array & Object - Client Defaults
             gen.writeFieldName('ClientUserIds');
             	gen.writeStartArray();
     		        
        				//Start of Object - SClient Defaults	
                         gen.writeStartObject();
                          
                          if(contactId == null) {gen.writeNullField('ClientContactId');} else{gen.writeStringField('ClientContactId', contactId);}
          				  if(u == null) {gen.writeNullField('loginContactEmail');} else{gen.writeStringField('loginContactEmail', u.email);}
          				  if(u == null) {gen.writeNullField('loginContactName');} else{gen.writeStringField('loginContactName', u.name);}
         				 if(u == null) {gen.writeNullField('loginContactphotourl');} else{gen.writeStringField('loginContactphotourl', u.FullPhotoUrl);}
         
         				  if(AccID == null) {gen.writeNullField('ClientAccID');} else{gen.writeStringField('ClientAccID', AccID);}
         				  if(rw.ClientuserID == null) {gen.writeNullField('ClientUserID');} else{gen.writeStringField('ClientUserID', rw.ClientuserID);}
                          if(at.AccessToken_encoded__c== null) {gen.writeNullField('AccessToken');} else{gen.writeStringField('AccessToken', at.AccessToken_encoded__c);}
                		 //  if(ac.CSE_IOR__c== null) {gen.writeNullField('AccountManagerID');} else{gen.writeStringField('AccountManagerID', ac.CSE_IOR__c);}
             			 // if(ac.CSE_IOR__c== null) {gen.writeNullField('AccountManagerName');} else{gen.writeStringField('AccountManagerName', ac.CSE_IOR__r.Firstname+' '+ac.CSE_IOR__r.Lastname);}
                		 // if(ac.CSE_IOR__c== null) {gen.writeNullField('AccountManagerProfilepic');} else{gen.writeStringField('AccountManagerProfilepic', ac.CSE_IOR__r.FullPhotoUrl);}
         gen.writeEndObject();
                         //End of Object - Client Defaults
    				
    		gen.writeEndArray();
                //End of Array - Client Defaults 
             
            gen.writeEndObject();
    		jsonData = gen.getAsString();
             
             
              res.responseBody = Blob.valueOf(jsonData);
              res.addHeader('Content-Type', 'application/json');
              res.statusCode = 200;
                
                API_Log__c Al = New API_Log__c();
                Al.Account__c=AccID;
                Al.EndpointURLName__c='NCP-GetUserdetails';
                Al.Response__c='NCP-Success - User details are sent';
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al;
             
         
            }
     Catch (Exception e){
         
          System.debug('Line number ====> '+e.getLineNumber() + 'Exception Message =====> '+e.getMessage());
            
           		String ErrorString ='NCP- Error - User details are not sent';
               	res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
                res.addHeader('Content-Type', 'application/json');
            	res.statusCode = 500;
                API_Log__c Al = New API_Log__c();
                Al.Account__c=AccID;
                Al.EndpointURLName__c='NCP - GetUserDetails';
                Al.Response__c=e.getMessage()+' at line number: '+e.getLineNumber();
                Al.StatusCode__c=string.valueof(res.statusCode);
              	Insert Al;
           
     }
   }

}