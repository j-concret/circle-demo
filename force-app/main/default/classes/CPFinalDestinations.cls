@RestResource(urlMapping='/FinalDestinations/*')
global class CPFinalDestinations {
     public class My1Exception extends Exception {} 
    
     @Httpget
    global static void FinalDestinations(){
        
        RestRequest req = RestContext.request;
    	RestResponse res = RestContext.response;
     	res.addHeader('Content-Type', 'application/json');
        String ClientAccId = req.params.get('ClientAccId');
        String Country = req.params.get('Country');
        try {
            Id recordTypeId = Schema.SObjectType.Client_Address__c.getRecordTypeInfosByDeveloperName().get('Final_Destinations').getRecordTypeId();
             
                List<Client_Address__c> ABs = [select id,Name,Address_Status__c,Comments__c,CompanyName__c,AdditionalContactNumber__c,Contact_Full_Name__c,Contact_Email__c,Contact_Phone_Number__c,Address__c,Address2__c,City__c,Province__c,Postal_Code__c,All_Countries__c,Client__c      from Client_Address__c where Client__c =:ClientAccId  and All_Countries__c = : Country and RecordType.ID =:recordTypeId and address_status__c='Active' ];
                IF(!ABs.Isempty()){ 
                    res.responseBody = Blob.valueOf(JSON.serializePretty(ABs));
        			res.statusCode = 200;
                
                API_Log__c Al = New API_Log__c();
                Al.Account__c=ClientAccId;
                Al.EndpointURLName__c='FinalDestinations';
                Al.Response__c='Success - List of Final Destination Address are sent';
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al;    
        		}
         Else{
                String ErrorString = 'No Addresses found';
                res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
        		res.statusCode = 200;
                
                API_Log__c Al = New API_Log__c();
                Al.Account__c=ClientAccId;
                Al.EndpointURLName__c='FinalDestinations';
                Al.Response__c='Success - 0 Pickup Address are found';
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al;
                throw new My1Exception('First exception');
         
         
         }
        
        }
        
        
        Catch(My1Exception e){
                String ErrorString ='There are currently no addresses for this country. Please create a new address.';
               	res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
                res.statusCode = 500;
                API_Log__c Al = New API_Log__c();
                Al.Account__c=ClientAccId;
                Al.EndpointURLName__c='FinalDestinations';
                Al.Response__c=e.getMessage()+' at line number: '+e.getLineNumber();
                Al.StatusCode__c=string.valueof(res.statusCode);
              	Insert Al;
            
            
        }
        
        
        
        
        
    }
    

}