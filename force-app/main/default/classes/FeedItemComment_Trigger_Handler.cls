public class FeedItemComment_Trigger_Handler {
public static void updateCaseAndTask(List <FeedComment> feedCommentList){
        List<Case> casesToUpdate = new List<Case>();
        List<Task> tasksToUpdate = new List<Task>();
     	List<ID> FeedItemIds = new list<ID>();
        
        
        List<FeedComment> FeedItemList1 = [select CreatedBy.Profile.Name,ParentId,CreatedDate,InsertedById,commentbody 
                                        from FeedComment 
                                        where Id in :feedCommentList];
        
        user us=[select id, Name, fullphotourl from user where id =:FeedItemList1[0].InsertedById ];
        
        System.debug('FeedItemList1');
        System.debug(JSON.serializePretty(FeedItemList1));
        for (FeedComment fi : FeedItemList1) {
            
            String feedbody = '';
            if(fi !=null && fi.commentbody != null){
                feedbody = fi.commentbody.left(35);   
            }
            
             if (fi.ParentId.getSObjectType() == Case.SObjectType) {
                
                 if(fi.commentbody!=null) {FeedItemIds.add(Fi.Id); }
                 System.debug('Parent Case Object');
                casesToUpdate.add(
                    (fi.CreatedBy.Profile.Name == 'Tecex Customer')?
                    new Case(
                        Id = fi.ParentId,
                        LastClientMsgTime__c = fi.CreatedDate
                       // ClientlatestComment__c=feedbody,
                        
                    ):
                    new Case(
                        Id = fi.ParentId,
                        LastMessageTime__c = fi.CreatedDate,
                        FullphotoURL__C = us.fullphotourl
                       //  ClientlatestComment__c=feedbody
                    )
                );
            }
            if (fi.ParentId.getSObjectType() == Task.SObjectType) {
                 if(fi.commentbody!=null) {FeedItemIds.add(Fi.Id); }
                System.debug('Parent Task Object');
                tasksToUpdate.add((fi.CreatedBy.Profile.Name == 'Tecex Customer')?
                                  new Task(
                                      Id = fi.ParentId,
                                      LastClientMsgTime__c = fi.CreatedDate,
                                      ClientlatestComment__c=feedbody,
                                      ClientlatestCommentUser__c = us.Name
                                  ) :
                                  new Task(
                                      Id = fi.ParentId,
                                      LastMessageTime__c = fi.CreatedDate,
                                      ClientlatestComment__c=feedbody,
                                      ClientlatestCommentUser__c = us.Name
                                  )
                                 );
            }
          
        }
        if(casesToUpdate !=null && !casesToUpdate.isEmpty()){
            update casesToUpdate;
             
            if(!FeedItemIds.isempty()){
           	 FirebaseNotifications.NotifyFirebaseforcasefeeditem(FeedItemIds);
            }
        }
        if(tasksToUpdate !=null && !tasksToUpdate.isEmpty()){
            update tasksToUpdate;
            if(!FeedItemIds.isempty()){
           	 FirebaseNotifications.NotifyFirebasefortaskfeeditem(FeedItemIds);
            }
        }
   		if(!FeedItemList1.isEmpty()){
           feedItem_CaseMail.sendFeedCommentMail(feedCommentList);
        }
        System.debug('tasksToUpdate');
        System.debug(JSON.serializePretty(tasksToUpdate));
        System.debug('casesToUpdate');
        System.debug(JSON.serializePretty(casesToUpdate));
    }
}