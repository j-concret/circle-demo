@isTest
public class NCPupdateProfilePic_Test {
    
	public static testMethod void NCPCreateCase_Test(){
        
        //Test Data
        Access_token__c at = new Access_token__c(status__c = 'Active', Access_Token__c = '123');
        insert at;
        //Contact con = TestDataFactory.myContact();
        
        //Json Body
        String json = '{"Accesstoken":"'+at.Access_Token__c+'","UserID":"'+UserInfo.getUserId()+'","pic":"Null"}';
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
        req.requestURI = '/services/apexrest/NCPUpdateProfilepic'; 
        req.httpMethod = 'Post';
        req.addHeader('Content-Type', 'application/json');
        req.requestBody = Blob.valueof(json);
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
        NCPupdateProfilePic.NCPCreateCase();
        Test.stopTest(); 
    }
    
    //Covering catch exception
    public static testMethod void NCPCreateCase_Test1(){
        
        //Test Data
        Access_token__c at = new Access_token__c(status__c = 'Active', Access_Token__c = '123');
        insert at;
        //Contact con = TestDataFactory.myContact();
        
        //Json Body
        String json = '{"Accesstoken":"'+at.Access_Token__c+'","UserID":"123","pic":"Null"}';
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
        req.requestURI = '/services/apexrest/NCPUpdateProfilepic'; 
        req.httpMethod = 'Post';
        req.addHeader('Content-Type', 'application/json');
        req.requestBody = Blob.valueof(json);
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
        NCPupdateProfilePic.NCPCreateCase();
        Test.stopTest(); 
    }
}