public class FeedItem_Hndlr {
  public static void ShareFeed(List <FeedItem> contentList)
 {
     //Get the relevant Content links
     List <FeedItem> ContentToShare = new List <FeedItem>();
     Map <Id,FeedItem> contentMap = new Map <Id,FeedItem>();
     
     if(contentList.size()>0){
         for (FeedItem currentDoc : contentList){
             String linkedId = currentDoc.ParentId;
             if(linkedId.startsWith('a0R')){
                 contentMap.put(currentDoc.ParentId,currentDoc);
             }
         }
     }
     
     if(contentMap.size()>0){
         List <Opportunity> oppsList = [Select Id, Shipment_Order__c from Opportunity where Shipment_Order__c IN :contentMap.keySet()];
         
         if(oppsList.size()>0){
             for(Opportunity currentOpp : oppsList){
                 FeedItem newShare = contentMap.get(currentOpp.Shipment_Order__c);
                 newShare = newShare.clone();
                 newShare.ParentId = currentOpp.Id;
                 ContentToShare.add(newShare);
             }
         }
     }
     
     /*if(contentList.size()>0){
         for (FeedItem currentDoc : contentList){
             String linkedId = currentDoc.ParentId;
             if(linkedId.startsWith('006')){
                 contentMap.put(currentDoc.ParentId,currentDoc);
             }
         }
     }
     
     if(contentMap.size()>0){
         List <Opportunity> oppsList = [Select Id, Shipment_Order__c from Opportunity where Id IN :contentMap.keySet()];
         
         if(oppsList.size()>0){
             for(Opportunity currentOpp : oppsList){
                 if(!contentMap.containsKey(currentOpp.Shipment_Order__c)){
                 FeedItem newShare = contentMap.get(currentOpp.Id);
                 newShare = newShare.clone();
                 newShare.ParentId = currentOpp.Shipment_Order__c;
                 ContentToShare.add(newShare);
                 }
             }
         }
     }*/
     
     if(ContentToShare.size()>0){
         insert ContentToShare;
     }
 }
}