@IsTest
public class NCPSOADownload_Test {

    static testMethod void testNCPSOADownload(){
        Access_token__c accessTokenObj = new Access_token__c();
        accessTokenObj.Status__c = 'Active';
        accessTokenObj.Access_Token__c = 'asdfghj-sdfgfds-sfdfgf';
        
        Insert accessTokenObj;
        
        Account acc = new Account(Name = 'TecEx Prospective Client');
        
        Insert acc;
        
        NCPSOADownloadwra wrapper = new NCPSOADownloadwra();
        wrapper.Accesstoken = accessTokenObj.Access_Token__c;
        wrapper.AccountID = acc.Id;
        
        RestRequest req1 = new RestRequest();
        RestResponse res1 = new RestResponse();
        req1.requestURI = '/services/apexrest/SOADownload'; 
        req1.httpMethod = 'POST';
        req1.requestBody = Blob.valueOf(JSON.serialize(wrapper));     
        RestContext.request = req1;
        RestContext.response = res1;
        RestContext.request.addHeader('Content-Type', 'application/json');
        
        Test.startTest(); 
        NCPSOADownload.NCPSOADownload();
        Test.stopTest();
    }
    
    static testMethod void testNCPSOADownloadElse(){
        Access_token__c accessTokenObj = new Access_token__c();
        accessTokenObj.Status__c = 'Expired';
        accessTokenObj.Access_Token__c = 'asdfghj-sdfgfds-sfdfgf';
        
        Insert accessTokenObj;
        
        Account acc = new Account(Name = 'TecEx Prospective Client');
        
        Insert acc;
        
        NCPSOADownloadwra wrapper = new NCPSOADownloadwra();
        wrapper.Accesstoken = accessTokenObj.Access_Token__c;
        wrapper.AccountID = acc.Id;
        
        RestRequest req1 = new RestRequest();
        RestResponse res1 = new RestResponse();
        req1.requestURI = '/services/apexrest/SOADownload'; 
        req1.httpMethod = 'POST';
        req1.requestBody = Blob.valueOf(JSON.serialize(wrapper));     
        RestContext.request = req1;
        RestContext.response = res1;
        RestContext.request.addHeader('Content-Type', 'application/json');
        
        Test.startTest(); 
        NCPSOADownload.NCPSOADownload();
        Test.stopTest();
    }
    
 /*   static testMethod void testNCPSOADownloadCatchBlock(){
        Access_token__c accessTokenObj = new Access_token__c();
        accessTokenObj.Status__c = 'Active';
        accessTokenObj.Access_Token__c = 'asdfghj-sdfgfds-sfdfgf';
        
        Insert accessTokenObj;
        
        Account acc = new Account(Name = 'Acme Test');
        
        Insert acc;
        
        NCPSOADownloadwra wrapper = new NCPSOADownloadwra();
        wrapper.Accesstoken = accessTokenObj.Access_Token__c;
        wrapper.AccountID = '';
        
        RestRequest req1 = new RestRequest();
        RestResponse res1 = new RestResponse();
        req1.requestURI = '/services/apexrest/SOADownload'; 
        req1.httpMethod = 'POST';
        req1.requestBody = Blob.valueOf(JSON.serialize(wrapper));     
        RestContext.request = req1;
        RestContext.response = res1;
        RestContext.request.addHeader('Content-Type', 'application/json');
        
        Test.startTest(); 
        NCPSOADownload.NCPSOADownload();
        Test.stopTest();
    }
*/
}