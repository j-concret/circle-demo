public class cpa2ComponentController {

    
    @AuraEnabled
    public static CPA_v2_0__c getCpa ( Id recordId){
        
    CPA_v2_0__c cpa2 = [ select Id, Name,  Pre_Inspection_Required__c
                       FROM CPA_v2_0__c WHERE Id =: recordId];
        
        return cpa2;
        
        
        
    }
    
    
    
    @AuraEnabled
    public static User getModifiedBy( Id recordId){
        
      CPA_v2_0__c cpa2 = [Select Id, LastModifiedById from CPA_v2_0__c WHERE Id =: recordId];  
        
        
       User user = null; 
        
        If(cpa2.LastModifiedById != null){
            
            user = [Select Id, Name from User where Id =: cpa2.LastModifiedById];
        }
        
       return user; 
    }
    
    
    
    @AuraEnabled
    public static User getCreatedBy( Id recordId){
        
      CPA_v2_0__c cpa2 = [Select Id, CreatedById from CPA_v2_0__c WHERE Id =: recordId];  
        
        
       User user = null; 
        
        If(cpa2.CreatedById != null){
            
            user = [Select Id, Name from User where Id =: cpa2.CreatedById];
        }
        
       return user; 
    }
    
    
    
    @AuraEnabled
    public static Map<String, String> getLabels(Id recordId) {
 
        String type='CPA_v2_0__c';
        Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Schema.SObjectType leadSchema = schemaMap.get(type);
        Map<String, Schema.SObjectField> fieldMap = leadSchema.getDescribe().fields.getMap();
        Map<String, String> fieldsLabel = new Map<String, String>();
 
        for (String fieldName: fieldMap.keySet()) {
            fieldsLabel.put(fieldName, fieldMap.get(fieldname).getDescribe().getLabel());
        }
 
        return fieldsLabel;
    }
    
    
    
     @AuraEnabled
    public static String UpdateCpa(CPA_v2_0__c cpa2) {
 
        String mess = null;     
 
        try{
            update cpa2;
        }
        catch (DmlException e) {
            System.debug('+++++++++++++++++++ A DML exception has occurred: ' + e.getMessage());
            mess = e.getMessage();
            return mess;
        }
 
        return mess;
   
    }
    
   
    
}