public class Shipment_Order_TriggerHandler extends Base_Handler
{
    Private List<CPA_Costing__c> newCostings = new List<CPA_Costing__c>();
    Private List<ID> CPACostings_Delete = new List<ID>();
    Private List<Shipment_Order__c> newSOs = trigger.new;
    Private List<Shipment_Order__c> oldSOs = trigger.old;
    Private List<ID> SOsToUpdate = SOTriggerHelper.extractIDs(Trigger.new);
    private List<SOWrapper> newSOWrappers = new List<SOWrapper>();
    private Map<ID, CPA_v2_0__c> newSOIdCPA = new Map<ID, CPA_v2_0__c>();
    private Map<ID, IOR_Price_List__c> newSOIdIORPriceList = new Map<ID, IOR_Price_List__c>();

    public override void bulkBefore()
    {
        super.bulkBefore();


        if (trigger.isInsert || trigger.isUpdate)
        {
          //  list<IOR_Price_List__c> IORPriceLists = [SELECT Admin_Fee__c, Bank_Fees__c, IOR_Fee__c, IOR_Min_Fee__c, On_Charge_Mark_up__c, TecEx_Shipping_Fee_Markup__c FROM IOR_Price_List__c WHERE ID in: SOTriggerHelper.extractIDs('IOR_Price_List__c', newSOs)];

         //   if (IORPriceLists.size() > 0)
         //   {
          //     for (Shipment_Order__c SO : newSOs)
          //      {
           //        for (IOR_Price_List__c IOR_pl : IORPriceLists)
            //        {
             //          if (SO.IOR_Price_List__c == IOR_pl.ID)
             //          {
              //              newSOIdIORPriceList.put(SO.Id, IOR_pl);
               //            continue;
                //        }
                //    }
             //   }
        //   }
      //  }

      //  else 
        
        if (trigger.isDelete)
        {
            List<ID> SOIds_Delete = new List<ID>();
            for (Shipment_Order__c SO : oldSOs)
                SOIds_Delete.Add(SO.ID);

            for (CPA_Costing__c costing : [SELECT ID FROM CPA_Costing__c WHERE Shipment_Order_Old__c in:SOIds_Delete])
                CPACostings_Delete.Add(costing.ID);
       }
    }
        
    }

    public override void bulkAfter()
    {
        super.bulkAfter();
    }

    public override void beforeInsert(List<SObject> newlstObj)
   {
        super.beforeInsert(newlstObj);
        
        for (SObject SOObj : newlstObj)
        {
          Shipment_Order__c SO = (Shipment_Order__c)SOObj;
          
         System.debug('ShipmentOrder: ' + SO);
          
        If( SO.IOR_Price_List__c <> null){
        
        
        String destination = '';
        if(SO.IOR_Price_List__r.Destination__c == NULL){
            destination = [select Destination__c from IOR_Price_List__c where Id =: SO.IOR_Price_List__c].Destination__c;
            System.debug('If Part - Destination 1: '+destination );
        }else{
        
        
            destination = SO.IOR_Price_List__r.Destination__c;  
            System.debug('Else Part - Destination: '+destination);          
        }
        
        Double shipmentValue = 0;
        shipmentValue = SO.Shipment_Value_USD__c;
        
                
        List<country_price_approval__c> CPAs = [Select Id, Name, Destination__c, Preferred_Supplier__c, Max_Shipment_Value__c, Min_Shipment_Value__c From country_price_approval__c where Destination__c =: destination AND Preferred_Supplier__c = TRUE AND Min_Shipment_Value__c <= :shipmentValue AND Max_Shipment_Value__c >= :shipmentValue];
        List<country_price_approval__c> CPANoMins = [Select Id, Name, Destination__c, Preferred_Supplier__c From country_price_approval__c where Destination__c =: destination AND Preferred_Supplier__c = TRUE ];
        List<CPA_v2_0__c> CPA2s = [Select Id, Name, Destination__c, Preferred_Supplier__c,  Service_Type__c, Related_Costing_CPA__c From CPA_v2_0__c where Destination__c =: destination AND Preferred_Supplier__c = TRUE AND Service_Type__c = 'IOR/EOR' AND Related_Costing_CPA__c != NULL];
        
        
        
        
        If(!CPAs.IsEmpty() && SO.Ship_to_Country__c == null)
        {
            SO.Ship_to_Country__c = CPAs[0].Id;                
        }
        
        If(CPAs.IsEmpty() && !CPANoMins.IsEmpty() && SO.Ship_to_Country__c == null)
        {
         
          SO.Ship_to_Country__c = CPANoMins[0].Id; 
        }

        If(!CPA2s.IsEmpty() && SO.CPA_v2_0__c == null)
        {         
            SO.CPA_v2_0__c = CPA2s[0].Id;                
        }       
        
        }
        Else 
        
        If( SO.Destination__c <> null){
        
         Double shipmentValue = 0;
         shipmentValue = SO.Shipment_Value_USD__c;
        
         List<country_price_approval__c> CPAs = [Select Id, Name, Destination__c, Preferred_Supplier__c, Max_Shipment_Value__c, Min_Shipment_Value__c From country_price_approval__c where Destination__c =: SO.Destination__c AND Preferred_Supplier__c = TRUE AND Min_Shipment_Value__c <= :shipmentValue AND Max_Shipment_Value__c >= :shipmentValue];
         List<country_price_approval__c> CPANoMins = [Select Id, Name, Destination__c, Preferred_Supplier__c From country_price_approval__c where Destination__c =: SO.Destination__c AND Preferred_Supplier__c = TRUE ];
         List<CPA_v2_0__c> CPA2s = [Select Id, Name, Destination__c, Preferred_Supplier__c,  Service_Type__c From CPA_v2_0__c where Destination__c =: SO.Destination__c AND Preferred_Supplier__c = TRUE AND Service_Type__c = 'IOR/EOR' AND Related_Costing_CPA__c != NULL ];
         List<IOR_Price_List__c > PLs = [Select Id, Name, Destination__c, Client_Name__c From IOR_Price_List__c where Destination__c =: SO.Destination__c AND Client_Name__c  =: SO.Account__c ];
         
         
         
         If(!CPAs.IsEmpty() && SO.Ship_to_Country__c == null)
        {
            SO.Ship_to_Country__c = CPAs[0].Id;                
        }
        
        If(CPAs.IsEmpty() && !CPANoMins.IsEmpty() && SO.Ship_to_Country__c == null)
        {
         
          SO.Ship_to_Country__c = CPANoMins[0].Id; 
        }

        If(!CPA2s.IsEmpty() && SO.CPA_v2_0__c == null)
        {         
            SO.CPA_v2_0__c = CPA2s[0].Id;                
        } 
        
         If(!PLs.IsEmpty() && SO.IOR_Price_List__c  == null)
        {         
            SO.IOR_Price_List__c  = PLs[0].Id;                 
        } 

        
        }
        }
        
        }
         
    public override void beforeUpdate(List<SObject> newlstObj, List<SObject> oldlstObj, Map<Id, SObject> newMapObj, Map<Id, SObject> oldMapObj)
    {
        super.beforeUpdate(newlstObj, oldlstObj, newMapObj, oldMapObj);
        
         for (SObject SO : newlstObj)
        {
        
          for (SObject oldSO : oldlstObj)
          
       if (oldSO.ID == SO.ID && ((Shipment_Order__c)oldSO).CPA_v2_0__c == null && ((Shipment_Order__c)oldSO).IOR_Price_List__c <> null)
       
        {
        
        String destination = '';
        if(((Shipment_Order__c)SO).IOR_Price_List__r.Destination__c == NULL){
            destination = [select Destination__c from IOR_Price_List__c where Id =: ((Shipment_Order__c)oldSO).IOR_Price_List__c].Destination__c;
            System.debug('If Part - Destination 1: '+destination );
        }else{
        
        
            destination = ((Shipment_Order__c)SO).IOR_Price_List__r.Destination__c;  
            System.debug('Else Part - Destination: '+destination);          
        }
        
        
        System.debug('Pricelist Destination: ' + ((Shipment_Order__c)SO).IOR_Price_List__r.Destination__c);
        
        List<CPA_v2_0__c> CPA2s = [Select Id, Name, Destination__c, Preferred_Supplier__c,  Service_Type__c, Related_Costing_CPA__c From CPA_v2_0__c where Destination__c =: destination AND Preferred_Supplier__c = TRUE  AND Service_Type__c = 'IOR/EOR' AND Related_Costing_CPA__c != NULL];
        
        System.debug('CPA2s: '+CPA2s.size()); 
       

        If(!CPA2s.IsEmpty() )
        {         
            ((Shipment_Order__c)SO).CPA_v2_0__c = CPA2s[0].Id; 
            ((Shipment_Order__c)SO).Create_Costings__c = TRUE;
                         
        }       
      
    }
    
    }
    
    }

    public override void beforeDelete(List<SObject> oldlstObj, Map<Id, SObject> oldMapObj)
    {
        super.beforeDelete(oldlstObj, oldMapObj);
        Database.delete(CPACostings_Delete);
    }


  
  public override void afterInsert(List<SObject> newlstObj, Map<Id, SObject> newMapObj)
     {
        super.afterInsert(newlstObj, newMapObj);

      for (SObject SO : newlstObj)
     {            
            
           
            SOWrapper test = new SOWrapper((Shipment_Order__c)SO); 
           
            newSOWrappers.Add(test);
            
           System.debug('newSOTest: ' + test);
                  
       }
  }
    public override void afterUpdate(List<SObject> newlstObj, List<SObject> oldlstObj, Map<Id, SObject> newMapObj, Map<Id, SObject> oldMapObj)
    {
        super.afterUpdate(newlstObj, oldlstObj, newMapObj, oldMapObj);

        for (SObject SO : newlstObj)
        {
           
        
            boolean recalculateCosts = false;

            for(SObject oldSO : oldlstObj)
                if (oldSO.ID == SO.ID && ((Shipment_Order__c)oldSO).FC_Total__c != 0 && (((Shipment_Order__c)oldSO).Shipment_Value_USD__c != ((Shipment_Order__c)SO).Shipment_Value_USD__c || ((Shipment_Order__c)oldSO).Chargeable_Weight__c  != ((Shipment_Order__c)SO).Chargeable_Weight__c || ((Shipment_Order__c)oldSO).Service_Type__c != ((Shipment_Order__c)SO).Service_Type__c))
                {
                    recalculateCosts = true;
                    break;
                }

            if (recalculateCosts)
            {
                newSOWrappers.Add(new SOWrapper((Shipment_Order__c)SO, recalculateCosts));
            }    
           Else If(!recalculateCosts && ((Shipment_Order__c)SO).FC_Total__c ==0)
            
           {
            
            newSOWrappers.Add(new SOWrapper((Shipment_Order__c)SO));

           }
        }
    }

    public override void afterDelete(List<SObject> oldlstObj, Map<Id, SObject> oldMapObj)
    {
        super.afterDelete(oldlstObj, oldMapObj);
    }

    public override void andFinally()
    {

        super.andFinally();

        List<Shipment_Order__c> SOs = new List<Shipment_Order__c>();

        List<CPA_Costing__c> costingsToAddUpdate = new List<CPA_Costing__c>();

        List<Shipment_Order__c> updatedSOs =
        [SELECt Id, Name,  Insurance_Cost_CIF__c, Shipment_Value_USD__c, IOR_Fee_new__c, FC_IOR_and_Import_Compliance_Fee_USD__c, FC_Total_Customs_Brokerage_Cost__c,
        FC_Total_Handling_Costs__c, FC_Total_License_Cost__c, FC_Total_Clearance_Costs__c, FC_Insurance_Fee_USD__c, FC_Miscellaneous_Fee__c, On_Charge_Mark_up__c , FC_International_Delivery_Fee__c,
        TecEx_Shipping_Fee_Markup__c, CPA_v2_0__c, Chargeable_Weight__c    FROM Shipment_Order__c  WHERE ID in: SOsToUpdate];

        if (newSOWrappers.size() > 0 )
        {
            for (SOWrapper wrp : newSOWrappers)
            {
                Shipment_Order__c SO;
                for (Shipment_Order__c SOToUpdate : updatedSOs)
                    if (SOToUpdate.ID == wrp.SO.ID)
                    {
                        SO = SOToUpdate;
                        break;
                    }

                costingsToAddUpdate.AddAll(wrp.getApplicableCostings());
                // SOTriggerHelper.calculateSOTotalsMain(SO);

               // System.debug('newSOIdIORPriceList: ' + newSOIdIORPriceList);


          //      if (newSOIdIORPriceList.containsKey(SO.ID))
           //         SOTriggerHelper.CalculateSOForecastRevenueTotals(SO, newSOIdIORPriceList.get(SO.ID));
            //    SOs.Add(SO);
            }
            Upsert costingsToAddUpdate;

            update SOs;
        }
    }

    public class SOWrapper
    {
        public SOWrapper(Shipment_Order__c so, boolean recalculateCosts)
        {
            this.SO = so;
            this.CAP = [SELECT Related_Costing_CPA__c, VAT_Rate__c  FROM CPA_v2_0__c WHERE ID =: so.CPA_v2_0__c];
            this.Client = so.Account__r;

            if (recalculateCosts )
                recalculateCostings();
            else
                buildCostings();
        }

        public SOWrapper(Shipment_Order__c so)
        {
            this.SO = so;
            this.CAP = [SELECT Related_Costing_CPA__c, VAT_Rate__c FROM CPA_v2_0__c WHERE ID =: so.CPA_v2_0__c];
            this.Client = so.Account__r;

            if (so.CPA_v2_0__c != null)

            buildCostings();
        }

        public Shipment_Order__c SO { set; get; }
        public CPA_v2_0__c CAP { set; get; }
        public Account Client { set; get; }
        private List<CPA_Costing__c> relatedCostings;
        private List<CPA_Costing__c> relatedCostingsIOR;
        private List<CPA_Costing__c> relatedCostingsEOR;
        private List<CPA_Costing__c> addedCostings;
        private List<CPA_Costing__c> updatedCostings;

        private Id recordTypeId = Schema.SObjectType.CPA_Costing__c.getRecordTypeInfosByName()
                .get('Display').getRecordTypeId();

        public void buildCostings()
        {
            relatedCostingsIOR = [SELECT Name, Shipment_Order_Old__c, Amount__c, Applied_to__c, Ceiling__c, Conditional_value__c, Condition__c, Floor__c, Cost_Category__c, Cost_Type__c, Max__c, Min__c, Rate__c, CostStatus__c, Updating__c, RecordTypeID, IOR_EOR__c, VAT_applicable__c, Variable_threshold__c, VAT_Rate__c, CPA_v2_0__r.VAT_Rate__c  FROM CPA_Costing__c WHERE CPA_v2_0__c =: CAP.Id AND IOR_EOR__c = 'IOR' ];
            relatedCostingsEOR = [SELECT Name, Shipment_Order_Old__c, Amount__c, Applied_to__c, Ceiling__c, Conditional_value__c, Condition__c, Floor__c, Cost_Category__c, Cost_Type__c, Max__c, Min__c, Rate__c, CostStatus__c, Updating__c, RecordTypeID, IOR_EOR__c, VAT_applicable__c, Variable_threshold__c, VAT_Rate__c, CPA_v2_0__r.VAT_Rate__c  FROM CPA_Costing__c WHERE CPA_v2_0__c =: CAP.Id AND IOR_EOR__c = 'EOR'];       
    
         
         If(SO.Service_Type__c == 'IOR') {
          
            List<CPA_Costing__c>  SOCPAcostingsIOR = relatedCostingsIOR.deepClone();
           
            for(CPA_Costing__c cost : SOCPAcostingsIOR)
            
            {
           
                cost.Shipment_Order_Old__c = SO.ID;
                cost.CPA_v2_0__c = CAP.Related_Costing_CPA__c;
                cost.recordTypeID = recordTypeId;
                cost.VAT_Rate__c =  CAP.VAT_Rate__c;
                
                

                if (SO.Shipment_Value_USD__c != null)
                {
                    cost.Updating__c = !cost.Updating__c;
                    SOTriggerHelper.calculateSOCostingAmount(so, cost, false);
                }
            }
            addedCostings = SOCPAcostingsIOR;
        }
        
        Else If(SO.Service_Type__c == 'EOR') {
          
            List<CPA_Costing__c>  SOCPAcostingsEOR = relatedCostingsEOR.deepClone();
           
            for(CPA_Costing__c cost : SOCPAcostingsEOR)
            
            {
           
                cost.Shipment_Order_Old__c = SO.ID;
                cost.CPA_v2_0__c = CAP.Related_Costing_CPA__c;
                cost.recordTypeID = recordTypeId;
                cost.VAT_Rate__c =  CAP.VAT_Rate__c; 

                if (SO.Shipment_Value_USD__c != null && (SO.FC_Total__c == null || SO.FC_Total__c == 0))
                {
                    cost.Updating__c = !cost.Updating__c;
                    SOTriggerHelper.calculateSOCostingAmount(so, cost, false);
                }
            }
            addedCostings = SOCPAcostingsEOR;
        }
        
        }


         public void recalculateCostings()
        {
            
            relatedCostings = [SELECT Name, Shipment_Order_Old__c, IOR_EOR__c,Amount__c, Applied_to__c, Ceiling__c, Conditional_value__c, Condition__c, Floor__c, Cost_Category__c, Cost_Type__c, Max__c, Min__c, Rate__c, CostStatus__c, Updating__c, RecordTypeID, VAT_Rate__c, VAT_applicable__c, Variable_threshold__c FROM CPA_Costing__c WHERE Shipment_Order_Old__c =: SO.ID];
            System.debug('relatedCostings --- '+relatedCostings.size());
            
            List<CPA_Costing__c> delList = new List<CPA_Costing__c>();
            for(CPA_Costing__c var: relatedCostings )
            {
                if(var.Cost_Type__c == 'Variable'){
                    CPA_Costing__c del = new CPA_Costing__c();
                    del.id =var.id;
                    delList.add(del);                    
                }
            }
            

            
            updatedCostings = new List<CPA_Costing__c>();
            System.debug('updatedCostings --- '+updatedCostings.size());
            List<CPA_Costing__c>  relatedTemplateCPAcostings = [SELECT Name, IOR_EOR__c,Shipment_Order_Old__c, Amount__c, Applied_to__c, Ceiling__c, Conditional_value__c, Condition__c, Floor__c, Cost_Category__c, Cost_Type__c, Max__c, Min__c, Rate__c, CostStatus__c, Updating__c, RecordTypeID, CPA_v2_0__r.VAT_Rate__c, VAT_Rate__c, VAT_applicable__c, Variable_threshold__c FROM CPA_Costing__c WHERE CPA_v2_0__c =: CAP.Id ];
               
               List<CPA_Costing__c> updateCosting = new List<CPA_Costing__c >(); 
               Set<Id> ids = new Set<id>();
              for (CPA_Costing__c templateCost : relatedTemplateCPAcostings)
              {    
               
                  SOTriggerHelper.calculateSOCostingAmount(so, templateCost, false);                       
                  System.debug('The cost id; ----------# '+templateCost.Id+' Amount:- '+templateCost.Amount__c);
                  for(CPA_Costing__c cost : relatedCostings){
                      if (templateCost.Cost_Category__c == cost.Cost_Category__c && templateCost.Name != cost.Name && templateCost.Cost_Type__c == cost.Cost_Type__c && templateCost.Applied_to__c == cost.Applied_to__c && templateCost.IOR_EOR__c == cost.IOR_EOR__c){
                           System.debug(' Do update: '+cost.Id+' the amount '+templateCost.Amount__c+'  --- name '+ templateCost.Name);
                            if(!ids.contains(cost.id) && templateCost.Amount__c > -1){
                                cost.Id = cost.Id;  
                                cost.Name =  templateCost.Name;
                                cost.Amount__c =  templateCost.VAT_applicable__c == TRUE ? templateCost.Amount__c * (1 + (CAP.VAT_Rate__c/ 100)): templateCost.Amount__c;
                                cost.Ceiling__c = templateCost.Ceiling__c;
                                cost.Conditional_value__c = templateCost.Conditional_value__c;
                                cost.Condition__c = templateCost.Condition__c;
                                cost.Cost_Type__c = templateCost.Cost_Type__c;
                                cost.Floor__c = templateCost.Floor__c;
                                cost.Max__c = templateCost.Max__c;
                                cost.Min__c = templateCost.Min__c;
                                cost.Rate__c = templateCost.Rate__c;
                                cost.Variable_threshold__c = templateCost.Variable_threshold__c;
                                cost.VAT_Rate__c = templateCost.CPA_v2_0__r.VAT_Rate__c; 
                                cost.Cost_Category__c   = templateCost.Cost_Category__c ;                                  
                                cost.IOR_EOR__c = templateCost.IOR_EOR__c;                                
                                updateCosting.add(cost);
                                ids.add(cost.id);
                            }
                      }                   
                  }    
              }        
     if(!updateCosting.IsEmpty()){       
        update updateCosting;
     }
            List<CPA_Costing__c> updateCostList = new List<CPA_Costing__c>();
            for(CPA_Costing__c cost : relatedCostings)
            {
                cost.Updating__c = !cost.Updating__c;
                SOTriggerHelper.calculateSOCostingAmount(so, cost, false);
                  System.debug('The cost id; --------'+cost.Id+' Amount:- '+cost.Amount__c);
                  IF(cost.Amount__c == -1){
                                    

                  }
                System.debug(' --- cost '+cost.Amount__c);
                if (cost.Amount__c >= 0)
                {
                System.debug(' --- amount '+relatedTemplateCPAcostings);
                    for (CPA_Costing__c templateCost : relatedTemplateCPAcostings)
                    {
                    
                   
                    System.debug(' --- templateCost ');
                        if (templateCost.Cost_Category__c == cost.Cost_Category__c && templateCost.Name == cost.Name)
                        {
                            cost.Updating__c = !cost.Updating__c;
                            SOTriggerHelper.calculateSOCostingAmount(so, templateCost, false);
                             System.debug(' --- templateCost2  '+templateCost.Amount__c);

                            if (templateCost.Amount__c > 0)
                            {
                             System.debug(' ---inside  '+templateCost.Amount__c);
                                cost.Amount__c =  templateCost.VAT_applicable__c == TRUE ? templateCost.Amount__c * (1 + (CAP.VAT_Rate__c/ 100)): templateCost.Amount__c;
                                cost.Ceiling__c = templateCost.Ceiling__c;
                                cost.Conditional_value__c = templateCost.Conditional_value__c;
                                cost.Condition__c = templateCost.Condition__c;
                                cost.Cost_Type__c = templateCost.Cost_Type__c;
                                cost.Floor__c = templateCost.Floor__c;
                                cost.Max__c = templateCost.Max__c;
                                cost.Min__c = templateCost.Min__c;
                                cost.Rate__c = templateCost.Rate__c;
                                cost.Variable_threshold__c = templateCost.Variable_threshold__c;
                                cost.VAT_Rate__c = templateCost.CPA_v2_0__r.VAT_Rate__c;
                                
                              system.debug('THE COST ' +  cost.ID+' '+cost.Name);  
                                
                            }
                        }
                    }
                }

                updatedCostings.Add(cost);
            }
                       
            if(!updateCostList.IsEmpty()){
                    insert updateCostList;
            }
        }
        
        
        

        public List<CPA_Costing__c> getApplicableCostings()
        {
            List<CPA_Costing__c> applicableCostings = new List<CPA_Costing__c>();
            if (addedCostings != null)
                for (CPA_Costing__c cost : addedCostings)
                    if (cost.Amount__c > -1 && cost.Shipment_Order_Old__c != null)
                        applicableCostings.Add(cost);
            if (updatedCostings != null)
                for (CPA_Costing__c cost : updatedCostings)
                    if (cost.Amount__c > -1)
                        applicableCostings.Add(cost);

            return applicableCostings;
        }
    }
    
    }