@RestResource(urlMapping='/updateShippingFees/*')
Global class NCPupdateShippingFees {
      @Httppost
    global static void NCPupdateShippingFees(){
    
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        Blob body = req.requestBody;
    	res.addHeader('Content-Type', 'application/json');
        String requestString = body.toString();
        NCPupdateShippingFeesWra rw = (NCPupdateShippingFeesWra)JSON.deserialize(requestString,NCPupdateShippingFeesWra.class);
           try {
               
               
                Access_token__c At =[select status__c,Access_Token__c from Access_token__c where Access_Token__c=:rw.Accesstoken ];
            	if( at.status__C =='Active'){
                    
           Shipment_Order__c SO = [Select Id,account__c,Client_Taking_Liability_Cover__c,Who_arranges_International_courier__c from Shipment_Order__c where id = :rw.SOID];      
           
                    
                    IF(rw.shippingfee==true){ System.debug('shippingfee-->'+rw.shippingfee); So.Who_arranges_International_courier__c='Tecex';}
                    If(rw.shippingfee==false){System.debug('shippingfee-->'+rw.shippingfee); So.Who_arranges_International_courier__c='Client';}
                    
                    if(rw.liabilitycoverfee==true){System.debug('liabilitycoverfee-->'+rw.liabilitycoverfee); SO.Client_Taking_Liability_Cover__c='Yes';}
                    if(rw.liabilitycoverfee==false){System.debug('liabilitycoverfee-->'+rw.liabilitycoverfee); SO.Client_Taking_Liability_Cover__c='No';}
                    
            
            Update SO; 
                    
            JSONGenerator gen = JSON.createGenerator(true);
            gen.writeStartObject();
            
             gen.writeFieldName('Success');
             gen.writeStartObject();
            
             	if(SO.ID == null) {gen.writeNullField('Response');} else{gen.writeStringField('Response', 'Success- updateShippingFees Updated');}
            
            gen.writeEndObject();
            gen.writeEndObject();
            String jsonData = gen.getAsString();
            res.responseBody = Blob.valueOf(jsonData);
            res.addHeader('Content-Type', 'application/json');
            res.statusCode = 200;        
           
            
                API_Log__c Al = New API_Log__c();
                Al.Account__c=SO.Account__c;
                Al.EndpointURLName__c='NCP-updateShippingFees';
                Al.Response__c='Success- updateShippingFees Updated';
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al; 
                }
               
               Else{
             JSONGenerator gen = JSON.createGenerator(true);
            gen.writeStartObject();
            
             gen.writeFieldName('Error');
             gen.writeStartObject();
            
             	gen.writeStringField('Response', 'Accesstoken expired');
            
            gen.writeEndObject();
            gen.writeEndObject();
            String jsonData = gen.getAsString();
            res.responseBody = Blob.valueOf(jsonData);
            res.addHeader('Content-Type', 'application/json');
            res.statusCode = 400;        
                  
                   
               }
            }
        	catch (Exception e){
                
              	 String ErrorString ='Something went wrong while updating ShippingFees details, please contact support_SF@tecex.com';
               	res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
                res.addHeader('Content-Type', 'application/json');
                res.statusCode = 500;
                API_Log__c Al = New API_Log__c();
                //Al.Account__c=FD.Shipment_Order__r.Account__c;
                Al.EndpointURLName__c='NCP-updateShippingFees';
                Al.Response__c=e.getMessage()+' at line number: '+e.getLineNumber();
                Al.StatusCode__c=string.valueof(res.statusCode);
              	Insert Al;
		     	}

        
        
    }
 

}