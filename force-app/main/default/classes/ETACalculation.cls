public class ETACalculation {
    
    public static Boolean isETACalculated = false;
    
    public static Map<String, String> mappedStatusWithFieldsName = new Map<String, String>{
        'In-Transit' => 'Total_In_Transit_Halt_Days__c#Total_In_Transit_Halt_Business_Days__c',
            'Arrived in Country' => 'Total_Arrived_Halt_Days__c#Total_Arrived_Halt_Business_Days__c',
            'Cleared Customs' => 'Total_Cleared_Customs_Halt_Days__c#Total_Cleared_Customs_Halt_Business_Days__c',
            'Final Delivery in Transit' => 'Total_Cleared_Customs_Halt_Days__c#Total_Cleared_Customs_Halt_Business_Days__c'
            };
    
    public static void populateTotalHaltFields(Shipment_order__c shipmentOrder,String oldMappedStatus){
        
        if(mappedStatusWithFieldsName.containsKey(oldMappedStatus)){
            List<String> totalHaltFields = mappedStatusWithFieldsName.get(oldMappedStatus).split('#');
            if(shipmentOrder.TP_Halt__c && shipmentOrder.TP_Halt_Start_Date__c != null){
                calculateHaltDays(shipmentOrder);
            }
            
            if(shipmentOrder.get(totalHaltFields[0]) != null && shipmentOrder.get(totalHaltFields[0]) != 0){
                Integer currentHaltDays = Integer.valueOf(shipmentOrder.get(totalHaltFields[0]));
                shipmentOrder.put(totalHaltFields[0], currentHaltDays + shipmentOrder.Total_TP_Halt_Days__c);
            }else{
                shipmentOrder.put(totalHaltFields[0], shipmentOrder.Total_TP_Halt_Days__c);
            }
            
            if(shipmentOrder.get(totalHaltFields[1]) != null && shipmentOrder.get(totalHaltFields[1]) != 0){
                Integer currentHaltDays = Integer.valueOf(shipmentOrder.get(totalHaltFields[1]));
                shipmentOrder.put(totalHaltFields[1], currentHaltDays + shipmentOrder.Total_TP_Halt_Business_Days__c);
            }else{
                shipmentOrder.put(totalHaltFields[1], shipmentOrder.Total_TP_Halt_Business_Days__c);
            }
            
            
        }
        
    }
    
    public static void calculateHaltDays(Shipment_Order__c so){
        Integer currentHaltDays = ((so.TP_Halt_Start_Date__c).date()).daysBetween(system.today());        
        if(so.Total_TP_Halt_Days__c != null){
            so.Total_TP_Halt_Days__c += currentHaltDays; 
        }else{
            so.Total_TP_Halt_Days__c = currentHaltDays;
        }
        
        Integer haltDaysInBusiness = getHaltDaysinBusiness(so.TP_Halt_Start_Date__c,currentHaltDays);
        if(so.Total_TP_Halt_Business_Days__c != null){
            so.Total_TP_Halt_Business_Days__c += haltDaysInBusiness;
        }else{
            so.Total_TP_Halt_Business_Days__c = haltDaysInBusiness;
        }
    }
    
    public static void processETA(Map<Id,Shipment_Order__c> soMap) {
        List<Shipment_Order__c> finalSOlistToUpdate = new List<Shipment_order__c>();
        
        List<String> CPAConsiderShippingStatus = new List<String> {'In-Transit','Arrived in Country','Cleared Customs','Final Delivery in Transit','Delivered'};
            
            
            //Set<Id> cpaToFetch = new Set<Id>();
        List<Id> taskRollUpsToFetch = new List<Id>();
        List<Shipment_Order__c> soForSOETACalcuation = new List<Shipment_Order__c>();
        List<Shipment_Order__c> soForFreightCalculation = new List<Shipment_Order__c>();
        
        for(Shipment_Order__c so : soMap.values()) {
            if(so.CPA_v2_0__c == NULL ||
               so.Mapped_Shipping_status__c == NULL
              ) {
                  continue;
              }
            
            if((so.Mapped_Shipping_status__c == 'Compliance Pending' ||  so.Mapped_Shipping_status__c == 'Quote Created') && so.shipping_status__C != 'Client Approved to ship') {
                taskRollUpsToFetch.add(so.Id);
            }
            if(CPAConsiderShippingStatus.contains(so.Mapped_Shipping_status__c)) {
                soForSOETACalcuation.add(so);
            }
            if(so.Mapped_Shipping_status__c == 'Approved to Ship' || so.shipping_status__C == 'Client Approved to ship') {
                soForFreightCalculation.add(so);
            }
        }
        
        // Need to fetch SO log here.
        if(!soForSOETACalcuation.isEmpty()) {
            
            
            Map<String, String> mappedStatusToDateField = new Map<String, String>();
            mappedStatusToDateField.put('In-Transit', 'In_Transit_Date__c');
            mappedStatusToDateField.put('Cleared Customs', 'Customs_Cleared_Date__c');
            mappedStatusToDateField.put('Arrived in Country', 'Arrived_in_Customs_Date__c');
            mappedStatusToDateField.put('Final Delivery in Transit', 'Customs_Cleared_Date__c');
            // added delivered to always get negative date then it will be 0.
            mappedStatusToDateField.put('Delivered', 'In_Transit_Date__c');
            
            for(Shipment_Order__c so : soForSOETACalcuation) {
                
                Boolean isManualHalt = checkManualHalt(so);
                system.debug('@@@ ' + so.TP_Halt__c);
                
                system.debug('@@@ ' + isManualHalt);
                
                
                if(isManualHalt && !so.TP_Halt__c){
                    so.TP_Halt__c = true;
                    so.TP_Halt_Start_Date__c = system.now();
                }else if(isManualHalt && so.TP_Halt__c){
                    
                }else if(!isManualHalt && !so.TP_Halt__c){
                    
                }else if(!isManualHalt && so.TP_Halt__c){
                    
                    
                    //Integer currentHaltDays = (Date.valueOf(so.TP_Halt_Start_Date__c)).daysBetween(system.today()); 
                    calculateHaltDays(so);
            
                    so.TP_Halt__c = false;
                    so.TP_Halt_Start_Date__c = null;                    
                }
                
                Double maxDayCount = 0;
                if(so.Hub_Lead_Time_Decimal__c !=null && so.Mapped_Shipping_status__c == 'In-Transit') {
                    maxDayCount += Double.valueOf(so.Hub_Lead_Time_Decimal__c);
                }
                if(so.Transit_to_Destination_Lead_Time_Decimal__c !=null  && (so.Mapped_Shipping_status__c == 'In-Transit')) {
                    maxDayCount += Double.valueOf(so.Transit_to_Destination_Lead_Time_Decimal__c);
                }
                if(so.Customs_Clearance_Lead_Time_Decimal__c !=null  && (so.Mapped_Shipping_status__c == 'In-Transit' || so.Mapped_Shipping_status__c == 'Arrived in Country')) {
                    maxDayCount += Double.valueOf(so.Customs_Clearance_Lead_Time_Decimal__c);
                }
                if(so.Final_Delivery_Lead_Time_Decimal__c != null  && (
                    so.Mapped_Shipping_status__c == 'Arrived in Country' ||
                    so.Mapped_Shipping_status__c == 'Cleared Customs' ||
                    so.Mapped_Shipping_status__c == 'Final Delivery in Transit' ||
                    so.Mapped_Shipping_status__c == 'In-Transit')) {
                        maxDayCount += Double.valueOf(so.Final_Delivery_Lead_Time_Decimal__c);
                    }
                //here need to add roundoff logic
                Decimal maxDayC = maxDayCount;
                Integer MaxDays = Integer.valueOf((maxDayC).round(System.RoundingMode.UP)); //7 Today Date + 4 next date = find weekends(2)=7+2+todaydate
                
                
                
                DateTime statusChangedDate;
                if(so.get(mappedStatusToDateField.get(so.Mapped_Shipping_status__c)) != null){
                    statusChangedDate = (Datetime)so.get(mappedStatusToDateField.get(so.Mapped_Shipping_status__c));
                }else{
                    statusChangedDate = system.today();
                }
                
                
                so.Expected_Date_to_Next_Status__c = getExpecetedDate(statusChangedDate,MaxDays);
                if(so.Total_TP_Halt_Days__c != null && so.Total_TP_Halt_Days__c != 0){
                    so.Expected_Date_to_Next_Status__c = so.Expected_Date_to_Next_Status__c.addDays(Integer.valueOf(so.Total_TP_Halt_Days__c));
                }
                
                // resetting halt flags
                
                so.Halt__c = false;
                so.Halt_Start_Date__c = null;
            
                finalSOlistToUpdate.add(so);
            }
            
        }
        if(!taskRollUpsToFetch.isEmpty()) {
            
            Map<String,TaskETACalculation.ETAWrapper> soWithETAWrapper = TaskETACalculation.calculate(taskRollUpsToFetch);
            system.debug('soWithETAWrapper ' + soWithETAWrapper);
            for(String so : soWithETAWrapper.keySet()) {
                
                
                
                Shipment_Order__c shipment = soMap.get(so);
                Boolean isManualHalt = checkManualHalt(shipment);
                
                TaskETACalculation.ETAWrapper etaWrapper = soWithETAWrapper.get(so);
                //if already halt and new also halt then don't process.
                if( shipment.Halt__c && (etaWrapper.halt || isManualHalt)) {
                    System.debug('Inside halt true block: witheta halt true'+etaWrapper);
                    if(shipment.Mapped_Shipping_status__c == 'Compliance Pending') {
                        
                        Integer currentHaltDays = ((shipment.Halt_Start_Date__c).date()).daysBetween(system.today());
                        
                        
                        shipment.Total_Halt_Days__c += currentHaltDays;
                        // Total_Halt_Days__c in Business diff b/w Halt_Start_Date__c and system.today() in business Days
                        Integer haltDaysInBusiness = getHaltDaysinBusiness(shipment.Halt_Start_Date__c,currentHaltDays);
                        System.debug('haltDaysInBusiness '+haltDaysInBusiness); 
                        if(shipment.Total_Halt_Business_Days__c != null){
                            shipment.Total_Halt_Business_Days__c  += haltDaysInBusiness;
                        }else{
                            shipment.Total_Halt_Business_Days__c  = haltDaysInBusiness;
                        }
                    }
                    if(shipment.Shipment_Order_Compliance_Time_Decimal__c != Double.valueOf(etaWrapper.eta)
                       || shipment.Mapped_Shipping_status__c == 'Quote Created'
                       || shipment.Mapped_Shipping_status__c == 'Compliance Pending'
                       || shipment.cost_Estimate_acceptance_date__c == system.today()) {
                           shipment.Date_Of_New_CP__c = system.today();
                       }
                    
                    if(shipment.cost_Estimate_acceptance_date__c == system.today()
                       || shipment.Mapped_Shipping_status__c == 'Quote Created'
                       || shipment.Mapped_Shipping_status__c == 'Compliance Pending'){
                           shipment.Halt_Start_Date__c = system.today(); //Date.today().addDays(-1);
                       }
                    
                    DateTime goLiveDate = shipment.Date_Of_New_CP__c == NULL ? shipment.cost_Estimate_acceptance_date__c : shipment.Date_Of_New_CP__c;
                    shipment.Shipment_Order_Compliance_Time_Decimal__c = Double.valueOf(etaWrapper.eta);//1
                    
                    Integer etaDay = Integer.valueOf((etaWrapper.eta).round(System.RoundingMode.UP));
                    if(shipment.Mapped_Shipping_status__c == 'Quote Created') {
                        shipment.Compliance_Process__c = getMinMaxAsString(etaDay);
                    }
                    
                    shipment.Expected_Date_to_Next_Status__c =  getExpecetedDate(goLiveDate,etaDay);//goLiveDate.addDays(goLiveDate);
                }
                
                
                // if previously not halt and now halt then set the halt flags.
                if(!shipment.Halt__c && (etaWrapper.halt || isManualHalt)) {
                    System.debug('Inside halt false block: witheta halt true'+etaWrapper);
                    
     
                    
                    shipment.Date_Of_New_CP__c = system.today();
                    
                    DateTime goLiveDate = shipment.Date_Of_New_CP__c == NULL ? shipment.cost_Estimate_acceptance_date__c : shipment.Date_Of_New_CP__c;
                    shipment.Shipment_Order_Compliance_Time_Decimal__c = Double.valueOf(etaWrapper.eta);//1
                    Integer etaDay = Integer.valueOf((etaWrapper.eta).round(System.RoundingMode.UP));
                    if(shipment.Mapped_Shipping_status__c == 'Quote Created') {
                        shipment.Compliance_Process__c = getMinMaxAsString(etaDay);
                    }
                    shipment.Expected_Date_to_Next_Status__c = getExpecetedDate(goLiveDate,etaDay);//goLiveDate.addDays(etaDay);
                    shipment.Halt_Start_Date__c = system.today();
                    shipment.Halt__c = true;
                }
                // if previously halt and now not halt then reset halt flag and update current ETA
                if(shipment.Halt__c && !etaWrapper.halt && !isManualHalt) {
                    System.debug('Inside halt true block: witheta halt false'+etaWrapper);
                    System.debug('shipment.Halt_Start_Date__c '+shipment.Halt_Start_Date__c); 
                    
                    if(shipment.Mapped_Shipping_status__c == 'Compliance Pending') {
                        Integer currentHaltDays = ((shipment.Halt_Start_Date__c).date()).daysBetween(system.today());
                        System.debug('currentHaltDays '+currentHaltDays);    
                        
                        shipment.Total_Halt_Days__c += currentHaltDays;
                        // Total_Halt_Days__c in Business diff b/w Halt_Start_Date__c and system.today() in business Days
                        Integer haltDaysInBusiness = getHaltDaysinBusiness(shipment.Halt_Start_Date__c,currentHaltDays);
                        System.debug('haltDaysInBusiness '+haltDaysInBusiness); 
                        if(shipment.Total_Halt_Business_Days__c != null){
                            shipment.Total_Halt_Business_Days__c  += haltDaysInBusiness;
                        }else{
                            shipment.Total_Halt_Business_Days__c  = haltDaysInBusiness;
                        }
                    }
                    shipment.Date_Of_New_CP__c = system.today();

                    DateTime goLiveDate = shipment.Date_Of_New_CP__c == NULL ? shipment.cost_Estimate_acceptance_date__c : shipment.Date_Of_New_CP__c;
                    
                    shipment.Shipment_Order_Compliance_Time_Decimal__c = Double.valueOf(etaWrapper.eta);
                    
                    
                	Integer etaDay = Integer.valueOf((etaWrapper.eta).round(System.RoundingMode.UP));
                    if(shipment.Mapped_Shipping_status__c == 'Quote Created') {
                        shipment.Compliance_Process__c = getMinMaxAsString(etaDay);
                    }
                    shipment.Expected_Date_to_Next_Status__c = getExpecetedDate(goLiveDate,etaDay);//goLiveDate.addDays(etaDay);
                    
                    // resetting halt flags
                    shipment.Halt__c = false;
                    shipment.Halt_Start_Date__c = null;
                }
                
                //normal case when no halt before after.
                if(!shipment.Halt__c && !etaWrapper.halt && !isManualHalt) {
                    System.debug('Inside halt false block: witheta halt false'+etaWrapper);
                    if(shipment.Shipment_Order_Compliance_Time_Decimal__c != Double.valueOf(etaWrapper.eta)
                       || shipment.Mapped_Shipping_status__c == 'Quote Created'
                       || shipment.cost_Estimate_acceptance_date__c == system.today()) {
                           shipment.Date_Of_New_CP__c = system.today();
                       }
                    DateTime goLiveDate = shipment.Date_Of_New_CP__c == NULL ? shipment.cost_Estimate_acceptance_date__c : shipment.Date_Of_New_CP__c;
                    
                    Integer daysToAdd = 0;
                    if(shipment.Total_Halt_Days__c != NULL && shipment.Total_Halt_Days__c != 0) {
                        daysToAdd = Integer.valueOf(shipment.Total_Halt_Days__c);
                    }
                    shipment.Shipment_Order_Compliance_Time_Decimal__c = Double.valueOf(etaWrapper.eta);
                    //shipment.Expected_Date_to_Next_Status__c = goLiveDate.addDays(daysToAdd + etaWrapper.eta + getAdditonalActualDays(etaWrapper.eta));
                    Integer etaDay = Integer.valueOf((etaWrapper.eta).round(System.RoundingMode.UP));
                    if(shipment.Mapped_Shipping_status__c == 'Quote Created') {
                        shipment.Compliance_Process__c = getMinMaxAsString(etaDay);
                    }
                    shipment.Expected_Date_to_Next_Status__c = getExpecetedDate(goLiveDate,etaDay);//goLiveDate.addDays(etaDay);
                }
                
                System.debug(JSON.serialize('shipment ' + shipment));
 
            }
        }
        if(!soForFreightCalculation.isEmpty()) {
            Map<Id,Freight__c> freightSOMap = new Map<Id,Freight__c>();
            for(Freight__c ff : [SELECT Pickup_availability_Close__c,Shipment_Order__c from Freight__c WHERE Pickup_availability_Close__c != null AND Shipment_Order__c IN :soMap.keySet()]) {
                freightSOMap.put(ff.Shipment_Order__c,ff);
            }
            
            for(Shipment_Order__c shipmentOrder : soForFreightCalculation) {
                // resetting halt flags
                shipmentOrder.Halt__c = false;
                shipmentOrder.Halt_Start_Date__c = null;
                
                if(freightSOMap.containsKey(shipmentOrder.Id)) {
                    shipmentOrder.Expected_Date_to_Next_Status__c = freightSOMap.get(shipmentOrder.Id).Pickup_availability_Close__c;
                }else{
                    shipmentOrder.Expected_Date_to_Next_Status__c = system.now();
                }
            }
        }
        ETACalculation.isETACalculated = true;
    }
    
    public static Integer getAdditonalActualDays(Integer eta){
        if(eta == NULL && eta == 0) {
            return 0;
        }
        
        return Integer.valueOf((Math.ceil(Double.valueOf(eta)/5.0) * 2));
    }
    
    
    public static Boolean checkManualHalt(Shipment_Order__c so){
        return (so.Sub_Status_Update__c == 'On Hold' || 
                (so.Sub_Status_Update__c == 'In Transit to Hub' && so.Mapped_Shipping_status__c == 'Compliance Pending') ||
                so.Sub_Status_Update__c == 'In Storage' ||
                so.Sub_Status_Update__c == 'Awaiting Final Delivery Confirmation'
               );
    }
    
    public static void processQuoteETA(List<Shipment_order__c> soList){
        
        Set<Id> cpaToFetch = new Set<Id>();
        for(Shipment_order__c so : soList) {
            if(so.CPA_v2_0__c != NULL) {
                cpaToFetch.add(so.CPA_v2_0__c);
            }
        }
        if(!cpaToFetch.isEmpty()) {
            List<SO_Status_Description__c> soStatusDescList = [SELECT Id, CPA_v2_0__c, Status__c, Min_Days__c, Max_Days__c FROM SO_Status_Description__c
                                                               WHERE Type__c = 'Lead Time' AND CPA_v2_0__c IN :cpaToFetch
                                                               AND Status__c != NULL //AND Status__c != 'Final Delivery Lead Time'
                                                               AND Max_Days__c != NULL];
            
            Map<Id, List<SO_Status_Description__c> > cpaWithSOStaus = new Map<Id, List<SO_Status_Description__c> >();
            for(SO_Status_Description__c SOSD : soStatusDescList) {
                if(cpaWithSOStaus.containsKey(SOSD.CPA_v2_0__c)) {
                    cpaWithSOStaus.get(SOSD.CPA_v2_0__c).add(SOSD);
                }else{
                    cpaWithSOStaus.put(SOSD.CPA_v2_0__c, new List<SO_Status_Description__c> {SOSD});
                }
            }
            
            for(Shipment_Order__c so : soList) {
                if(cpaWithSOStaus.containsKey(so.CPA_v2_0__c)) {
                    
                    Decimal maxDays = 0;
                    Decimal complianceProcessesMax = 0;
                    Decimal transitTimeMax = 0;
                    Decimal customsClearanceMax = 0;
                    Decimal finalDeliveryLeadTimeMax =0;
                    for(SO_Status_Description__c sosd : cpaWithSOStaus.get(so.CPA_v2_0__c)) {
                        
                        
                        if(sosd.Status__c == 'Hub Lead Time' ||
                           sosd.Status__c == 'Transit to Destination Lead Time'
                          ) {
                              transitTimeMax += sosd.Max_Days__c;
                          }
                        if(sosd.Status__c == 'Customs Clearance Lead Time') {
                            customsClearanceMax += sosd.Max_Days__c;
                        }
                        if(sosd.Status__c == 'Final Delivery Lead Time'){
                            finalDeliveryLeadTimeMax += sosd.Max_Days__c;
                        }
                        
                    }
                    Integer transitTimeMaxRollup = Integer.valueOf((transitTimeMax).round(System.RoundingMode.UP));
                    Integer customsClearanceMaxRollup = Integer.valueOf((customsClearanceMax).round(System.RoundingMode.UP));
                    Integer finalDeliveryLeadTimeMaxRollup = Integer.valueOf((finalDeliveryLeadTimeMax).round(System.RoundingMode.UP));
                    so.Estimate_Transit_Time__c = getMinMaxAsString(transitTimeMaxRollup);
                    so.Estimate_Customs_Clearance_time__c = getMinMaxAsString(customsClearanceMaxRollup);
                    so.Estimate_Final_Delivery_Time__c = getMinMaxAsString(finalDeliveryLeadTimeMaxRollup);
                }
            }
        }
    }
    
    private static String getMinMaxAsString(Integer max){
        return String.valueOf(max) + ' business days';
    }
    
    private static Integer getHaltDaysinBusiness(Datetime startDate,Integer currentHaltDays ){
        Integer haltDays=0;
        for(Integer i = 1; i <= currentHaltDays; i++ ){
            String day = (startDate.addDays(i)).format('EEEE');
            if(day != 'Saturday' && day != 'Sunday'){
                haltDays++;
            }
        }
        System.debug('TotalBusinessDays '+haltDays);
        return haltDays;
    }
    
    @TestVisible
    public static DateTime getExpecetedDate(Datetime expectedDate,Integer businessDays){
        Datetime nextExpectedDate = expectedDate;
        String day2 = nextExpectedDate.format('EEEE');
        if(day2 == 'Saturday') {
            nextExpectedDate = nextExpectedDate.addDays(-1);
        }
        if(day2 == 'Sunday') {
            nextExpectedDate = nextExpectedDate.addDays(-2);
        }
        while(businessDays > 0) {
            String day = nextExpectedDate.format('EEEE');
            if(day != 'Saturday' && day != 'Sunday') {
                businessDays--;
            }
            nextExpectedDate = nextExpectedDate.addDays(1);
        }
        
        String day1 = nextExpectedDate.format('EEEE');
        if(day1 == 'Saturday') {
            nextExpectedDate = nextExpectedDate.addDays(2);
        }
        if(day1 == 'Sunday') {
            nextExpectedDate = nextExpectedDate.addDays(1);
        }
        system.debug('Final Date ' + nextExpectedDate);
        return nextExpectedDate;
    }
    
}