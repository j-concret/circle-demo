@RestResource(urlMapping='/NCPUpdateincludeaccountstatement/*')
Global class NCPUpdatecontact {
    
      @Httppost
        global static void NCPCreateCase(){
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        Blob body = req.requestBody;
    	res.addHeader('Content-Type', 'application/json');
        String requestString = body.toString();
        NCPUpdatecontactwra rw  = (NCPUpdatecontactwra)JSON.deserialize(requestString,NCPUpdatecontactwra.class);
          Try{
             
            Access_token__c At =[select status__c,Access_Token__c from Access_token__c where Access_Token__c=:rw.Accesstoken ];
            if( at.status__C =='Active'){
                
                 Contact con = [Select Id,FirstName,LastName,email,Phone,Include_in_SO_Communication__c, name,Contact_Role__c,New_Task__c,Task_Completion__c,Quote_status_updates__c,Major_Updates__c,Minor_Updates__c,Include_in_invoicing_emails__c from Contact where id =:rw.ContactID limit 1];
                 Con.Include_in_invoicing_emails__c = rw.IncludeinInvoicingEmails;
                 CON.Include_in_SO_Communication__c= rw.IncludeinSOCommunication == null? false :rw.IncludeinSOCommunication;
                 Update Con;
                
                
            JSONGenerator gen = JSON.createGenerator(true);
            gen.writeStartObject();
            
             gen.writeFieldName('Success');
             gen.writeStartObject();
            
                if(con.Id ==null) {gen.writeNullField('Response');} else{gen.writeStringField('Response', 'Contact updated');}
            
            gen.writeEndObject();
            gen.writeEndObject();
            String jsonData = gen.getAsString();
            res.responseBody = Blob.valueOf(jsonData);
            res.addHeader('Content-Type', 'application/json');
            res.statusCode = 200; 
                 API_Log__c Al = New API_Log__c();
                	Al.EndpointURLName__c='NCPUpdateincludeaccountstatement';
                 	Al.Login_Contact__c=rw.contactID;
                	Al.Response__c='Field updated: includeaccountstatement ' +con.Id ;
                	Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al;
                
            }
          
          
          }
            catch(Exception e){
                
                 String ErrorString ='Something went wrong, please contact support_SF@tecex.com';
               		res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
            		res.addHeader('Content-Type', 'application/json');
        			res.statusCode = 400;
                
                API_Log__c Al = New API_Log__c();
               
                Al.EndpointURLName__c='NCPUpdateincludeaccountstatement';
                Al.Response__c=e.getMessage()+' at line number: '+e.getLineNumber();
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al;
                
            }
  }
}