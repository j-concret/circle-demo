public class SObjectService {
    public static List<Schema.PicklistEntry> getPickListValues(String objectName, String fieldName){
        Schema.SObjectType objectDetails = getObjectInfo(objectName);
        Map<String, Schema.SObjectField> objectFieldsMap = getAllFieldsOfObject(objectDetails);
        Schema.DescribeFieldResult fieldResult = objectFieldsMap.get(fieldName).getDescribe();
        List<Schema.PicklistEntry> picklistEntries = fieldResult.getPicklistValues();
        Map<String, String> values = new Map<String, String>();
        
        for(Schema.PicklistEntry value : picklistEntries)
        {
            values.put(value.getLabel(), value.getValue());
        }
        
        return picklistEntries;//values;
    }
    public static Schema.SObjectType getObjectInfo(String objectName){
        Map<String, Schema.SObjectType> globalDescribe = Schema.getGlobalDescribe();
        
        return globalDescribe.containsKey(objectName) ? globalDescribe.get(objectName) : NULL;
    }
    public static Map<String, Schema.SObjectField> getAllFieldsOfObject(Schema.SObjectType objectDetails){
        return objectDetails.getDescribe().fields.getMap();
    }
}