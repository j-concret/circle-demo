@RestResource(urlMapping='/CreateAccRegistration/*')
global class ZCPcreatenewregistration {
    
   @Httppost
      global static void ZCPcreatenewregistration(){
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        Blob body = req.requestBody;
    	res.addHeader('Content-Type', 'application/json');
        String requestString = body.toString();
        ZCPcreatenewregistrationwra rw  = (ZCPcreatenewregistrationwra)JSON.deserialize(requestString,ZCPcreatenewregistrationwra.class);
          Try{
              Access_token__c At =[select status__c,Access_Token__c from Access_token__c where Access_Token__c=:rw.Accesstoken ];
              if( at.status__C =='Active' ){
                  
                  List<Registrations__c> CheckReg=[select Id from Registrations__c where Company_name__c=:rw.AccountID and Country__c=:rw.toCountry];
                  if(!CheckReg.isempty()){
                      
                      JSONGenerator gen = JSON.createGenerator(true);
            			gen.writeStartObject();
            			gen.writeFieldName('Message');
                        gen.writeStartObject();
                      
                  		gen.writeStringField('Message','Registraion already existing to selected country');
                        gen.writeEndObject();
                        gen.writeEndObject();
                    String jsonData = gen.getAsString();
            		res.responseBody = Blob.valueOf(jsonData);
            		res.addHeader('Content-Type', 'application/json');
            		res.statusCode = 400;
                      
                  }
                  else{
                  Registrations__c R = new Registrations__c();
                  r.Name=rw.name;
                  r.VAT_number__c=rw.VATnumber;
                  r.Registered_Address__c=rw.RegisteredAddress;
                  r.Registered_Address_2__c= rw.RegisteredAddress2;
                  r.Registered_Address_City__c= rw.RegisteredAddressCity;
                  r.Registered_Address_Province__c= rw.RegisteredAddressProvince;
                  r.Registered_Address_Postal_Code__c= rw.RegisteredAddressPostalCode;
                  r.Registered_Address_Country__c= rw.RegisteredAddressCountry;
                  r.Type_of_registration__c= rw.Typeofregistration;
                  r.Finance_contact_Email__c= rw.FinancecontactEmail;
                  r.Finance_contact_Name__c= rw.FinancecontactName;
                  r.Finance_contact_Phone__c= rw.FinancecontactPhone;
                  r.Verified__c= rw.Verified == null ? FALSE : rw.Verified ;
                  r.Company_name__c= rw.AccountID;
                  r.Country__c=rw.toCountry;
                 
                  Insert r;
                  
                  if(r.Id !=null){
                  Registrations__c re = [select id,name from Registrations__c where id =: r.Id];
                  
                   		JSONGenerator gen = JSON.createGenerator(true);
            			gen.writeStartObject();
            			gen.writeFieldName('Success');
                        gen.writeStartObject();
                        gen.writeStringField('RegistrationID',re.Id);
                  		gen.writeStringField('RegCompanyName',re.name);
                        gen.writeEndObject();
                        gen.writeEndObject();
                    String jsonData = gen.getAsString();
            		res.responseBody = Blob.valueOf(jsonData);
            		res.addHeader('Content-Type', 'application/json');
            		res.statusCode = 200;
                    
                    	API_Log__c Al = New API_Log__c();
                        Al.EndpointURLName__c='ZeeRegistrratiocreation';
                        Al.Response__c='Registrations created'+re.Id  ;
                        Al.StatusCode__c=string.valueof(res.statusCode);
                        Insert Al;
                  }
                  
              }
             }
          
          }
          catch(Exception e){
              
             		//String ErrorString ='Something went wrong, please contact support_SF@tecex.com';
               		String ErrorString=e.getMessage()+e.getStackTraceString();
              		res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
            		res.addHeader('Content-Type', 'application/json');
        			res.statusCode = 500;
                
                API_Log__c Al = New API_Log__c();
                Al.EndpointURLName__c='ZeeRegistrratiocreation';
                Al.Response__c=e.getMessage()+' at line number: '+e.getLineNumber();
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al;
          }

      }
}