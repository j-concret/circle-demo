public with sharing class PReInspectionCarrierChoiceCtrl {

    @AuraEnabled
    public static Task getTaskRecord(String taskId){
        try {
            return [SELECT Id,State__c,WhatId,Master_Task__c,Shipment_Order__r.Destination__c,Master_Task__r.Name,Master_Task__r.Set_Default_to_Inactive__c, Subject,Inactive__c,Blocks_Approval_to_Ship__c FROM Task WHERE Id=: taskId];
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    @AuraEnabled
    public static Map<String,Object> getTaskWithConsignee(String taskId){
        try {
            Map<String,Object> response = new Map<String,Object>();
            Task tsk = [SELECT Id,State__c,WhatId,Master_Task__c,Master_Task__r.Name,Master_Task__r.Set_Default_to_Inactive__c, Subject,Inactive__c,Blocks_Approval_to_Ship__c FROM Task WHERE Id =: taskId];

            List<Freight__c> freights = [SELECT Id,Shipment_Order__r.CPA_v2_0__r.Courier_AWB_Incoterm__c,Shipment_Order__r.CPA_v2_0__r.FF_AWB_Incoterm__c,FF_Notify_Type__c,FF_Notify_TaxID__c,FF_Notify_TaxId_Type__c,FF_Notify_Company__c,FF_Notify_AddressLine1__c,FF_Notify_AddressLine2__c,FF_Notify_City__c,FF_Notify_State__c,FF_Notify_Country__c,FF_Notify_Postal_Code__c,FF_Notify_Name__c,FF_Notify_Email__c,FF_Notify_Phone__c,FF_Con_Company__c,FF_Con_AddressLine1__c,FF_Con_AddressLine2__c,FF_Con_City__c,FF_Con_State__c,FF_Con_Country__c,FF_Con_Postal_Code__c,FF_Con_Name__c,FF_Con_Email__c,FF_Con_Phone__c,Con_Company__c,Con_AddressLine1__c,Con_AddressLine2__c,Con_City__c,Con_Country__c,Con_Name__c,Con_Email__c,Con_Phone__c,Con_Postal_Code__c,Con_State__c,Notify_Type__c,Notify_TaxID__c,Notify_TaxId_Type__c,Notify_Company__c,Notify_AddressLine1__c,Notify_AddressLine2__c,Notify_City__c,Notify_State__c,Notify_Country__c,Notify_Postal_Code__c,Notify_Name__c,Notify_Email__c,Notify_Phone__c FROM Freight__c WHERE Shipment_Order__c =: tsk.WhatId LIMIT 1];
            response.put('task',tsk);
            if(!freights.isEmpty()){
              response.put('freight',freights[0]);
            }
            return response;
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    @AuraEnabled
    public static void updateTask(String taskId,String state){
      try {
        update new Task(Id = taskId, State__c=state);
      } catch (Exception e) {
        throw new AuraHandledException(e.getMessage());
      }

    }
}