/*
 This implements the HTTPMock Callout interface to allow testing of Dragon Web Services 
*/  
global class TX_Dragon_Mock extends HTTPRequest_Mock_Base
{
	private static final String EMPTY_DRAGON_RESPONSE = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">' +
			'<soapenv:Body/>' + 
			'</soapenv:Envelope>';
			
	private static final String ERROR_DRAGON_RESPONSE = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">' +
			'<soapenv:Body>' + 
				'<soapenv:Fault>' + 
					'<faultcode>soapenv:Server.Application</faultcode>' + 
					'<faultstring>Application Error</faultstring>' + 
					'<detail>' + 
						'<error code="10060">Socket error: Could not bind the socket to the appropriate port and interface [trace 25] (Route: Dragon Server (Gilbert) - Address: rosetta:1687)</error>' +
					'</detail>' +
				'</soapenv:Fault>' +
			'</soapenv:Body>' + 
			'</soapenv:Envelope>';

	protected override void setResponse(HTTPRequest request, HttpResponse response)
    {
    	super.setResponse(request, response);
        
      	response.setBody(EMPTY_DRAGON_RESPONSE);
    }
    
    protected override System.Type getHTTPRequestClass()
	{
		return XF_Dragon.class;	
	}
}