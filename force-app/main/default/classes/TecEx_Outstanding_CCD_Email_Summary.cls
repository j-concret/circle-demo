global class TecEx_Outstanding_CCD_Email_Summary implements Database.Batchable<sObject>{
    global String Query;
    global TecEx_Outstanding_CCD_Email_Summary(){
        String sClearedCustoms = 'Cleared Customs';
        String sFD = 'Final Delivery in Progress';
        String sAwaitingPOD = 'Awaiting POD';
        String sPODReceived = 'POD Received';
        String sDDP = 'DDP - Client Account';
        Query = 'Select Id,Name,SupplierlU__c,Account__r.Name,Destination__c,Shipping_Status__c,Customs_Cleared_Date__c,CreatedDate,Test_Account__c from Shipment_Order__c where (Shipping_Status__c = \''+String.escapeSingleQuotes(sClearedCustoms)+'\' OR Shipping_Status__c = \''+String.escapeSingleQuotes(sFD)+'\' OR Shipping_Status__c = \''+String.escapeSingleQuotes(sAwaitingPOD)+'\' OR Shipping_Status__c = \''+String.escapeSingleQuotes(sPODReceived)+'\') AND (Tax_Treatment__c != \''+String.escapeSingleQuotes(sDDP)+'\') AND (Total_Taxes_CCD__c = null) AND (CreatedDate >= 2019-07-01T00:00:00Z) AND Test_Account__c = FALSE ORDER BY Name'; // where Account__c = :SupplierId
    }
    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(Query);
    }
    global void execute(Database.BatchableContext BC, List<Shipment_Order__c> scope){
        Outstanding_CCD_Email_Summary.sendEmailSummary(scope,null,null);
    }
    global void finish(Database.BatchableContext BC){
    
    }
}