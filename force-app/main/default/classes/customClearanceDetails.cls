public class customClearanceDetails {
    
    /** returns the  Customs_Clearance_Documents__c Object Detail for a specific record**/
    public static Customs_Clearance_Documents__c getCustomsClearanceDocumentsDetail(Id recordId){
        
        Customs_Clearance_Documents__c customs_Clearance_Document = [SELECT Customs_Clearance_Documents__r.Name,Name , 
                                                                     Tax_Treatment__c , Shipment_Order_Status__c ,
                                                                     VAT_Reclaim_Shipment__c , Ship_to_Country__c ,
                                                                     Region__c , Account__c , Invoice_Timing__c ,
                                                                     Shipment_Order_Value__c , Supplier__c , Tax_Currency__c , 
                                                                     Foreign_Exchange_Rate__c , Duties_and_Taxes_per_CCD__c ,
                                                                     Taxes_per_CCDs_USD__c , Taxes_per_Tax_Calculation__c ,
                                                                     Sum_of_Supplier_Duties_Taxes__c ,
                                                                     Sum_of_Supplier_Duties_and_Taxes__c , Supplier_Overcharge_Undercharge__c,
                                                                     Tax_Calculation_Difference__c , Percentage_Difference_Supplier__c ,
                                                                     Percentage_Difference__c , Percentage_Difference_CDC__c, Reason_For_Difference__c ,
                                                                     Reason_For_Difference_Supplier__c , Reason_For_Top_Up_Credit__c , 
                                                                     Reason_For_Supplier_Over_Undercharge__c,Status__c,Reason_For_Query__c,
                                                                     Elaboration_on_Queried_Status__c , Elaboration_On_Reason_CDC__c ,
                                                                     Reason_For_Difference_CDC__c,Review_Attachment__c, Calculated_CIF__c,Captured_CIF__c
                                                                     FROM Customs_Clearance_Documents__c where Id = : recordId];
        return customs_Clearance_Document;
        
    }
    /** returns the Map of customs_Clearance_Document details with Picklist Values **/
    @AuraEnabled    
    public static Map<String,Object> getCCDDetails(Id recordId){
        Map<String,Object> responseMap = new Map<String,Object>{
            'status'=>'OK'
                };
                    try{
                        Customs_Clearance_Documents__c customs_Clearance_Document = getCustomsClearanceDocumentsDetail(recordId);
                        List<Object> sectionsList = generateCCDDetailsWrapper.getDetails(customs_Clearance_Document);
                        responseMap.put('data',customs_Clearance_Document);
                        responseMap.put('Reason_For_Difference_CDC_Options',picklist_values('Customs_Clearance_Documents__c','Reason_For_Difference_CDC__c'));
                        responseMap.put('Reason_For_Difference_Supplier_Options',picklist_values('Customs_Clearance_Documents__c','Reason_For_Difference_Supplier__c'));
                        responseMap.put('Status_Options',picklist_values('Customs_Clearance_Documents__c','Status__c'));
                        responseMap.put('Reason_For_Query_Options',picklist_values('Customs_Clearance_Documents__c','Reason_For_Query__c'));
                    }catch(Exception e){
                        responseMap.put('status', 'ERROR');
                        responseMap.put('message', e.getMessage());
                    }
        return responseMap;
    }
    
    /** used to update the customsClearanceDocument record from lightning component **/
    @AuraEnabled 
    public static Map<String,Object> updateCCDDetails(Customs_Clearance_Documents__c customsClearanceDocument){
        if(customsClearanceDocument.Reason_For_Difference_CDC__c == 'None'){
            customsClearanceDocument.Reason_For_Difference_CDC__c = null;
        }
        if(customsClearanceDocument.Reason_For_Difference_Supplier__c == 'None'){
            customsClearanceDocument.Reason_For_Difference_Supplier__c = null;
        }
        if(customsClearanceDocument.Status__c == 'None'){
            customsClearanceDocument.Status__c = null;
        }else{
            customsClearanceDocument.Reviewed_By__c = UserInfo.getUserId();
        }
        if(customsClearanceDocument.Reason_For_Query__c == 'None'){
            customsClearanceDocument.Reason_For_Query__c = null;
        }
        Map<String,Object> responseMap = new Map<String,Object>{
            'status'=>'OK'
                };
                    try{
                        upsert customsClearanceDocument;
                        responseMap.put('data', customsClearanceDocument);
                    }catch(DmlException exp){
                        System.debug('exp '+exp.getDmlFieldNames(0));
                        responseMap.put('status', 'DMLERROR');
                         responseMap.put('field', exp.getDmlFieldNames(0));
                        responseMap.put('message', exp.getDmlMessage(0));
                    }catch(Exception e){
                        responseMap.put('status', 'ERROR');
                        responseMap.put('message', e.getMessage());
                    }
        return responseMap;
    }
    
    /** This Method returns the Picklist Values for the specific field in specific object **/
    public static Set<Object> picklist_values(String object_name, String field_name) {
        Set<Object> values = new Set<Object>{new Map<String,String>{'label'=>'None',
            'value'=>null}};
                Set<String> labels = new Set<String>{'None'};
                    String[] types = new String[] {object_name};
                        Schema.DescribeSobjectResult[] results = Schema.describeSObjects(types);
        for(Schema.DescribeSobjectResult res : results) {
            for (Schema.PicklistEntry entry : res.fields.getMap().get(field_name).getDescribe().getPicklistValues()) {
                if (entry.isActive()) {
                    values.add(
                        new Map<String,String>{
                            'label'=>entry.getLabel(),
                                'value'=>entry.getValue()}
                    );
                }
            }
        }
        return values;
    }
    
}