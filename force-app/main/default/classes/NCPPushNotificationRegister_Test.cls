@isTest
public class NCPPushNotificationRegister_Test {
    static testMethod void  setUpData(){
         Access_Token__c att = new Access_Token__c(
            AccessToken_encoded__c='zmM0MDcxOTItZTJiZi00YWRhLTlkZGQtZGZhMmUxM2FiYTlj',
            Access_Token__c='cc407192-e2bf-4ada-9ddd-dfa2e13aba9c',
            Status__c='Active'
            );
        insert att;
        
          Account account = new Account (name='Acme1', Type ='Client',
                                       CSE_IOR__c = '0050Y000001LTZO',
                                       Service_Manager__c = '0050Y000001LTZO',
                                       Financial_Manager__c='0050Y000001LTZO',
                                       Financial_Controller__c='0050Y000001LTZO',
                                       Tax_recovery_client__c  = FALSE,
                                       Ship_From_Line_1__c = 'Ship_From_Line_1',
                                       Ship_From_Line_2__c = 'Ship_From_Line_2',
                                       Ship_From_Line_3__c = 'Ship_From_Line_3',
                                       Ship_From_City__c = 'Ship_From_City',
                                       Ship_From_Zip__c = 'Ship_From_Zip',
                                       Ship_From_Country__c = 'Ship_From_Country',
                                       Ship_From_Other_notes__c = 'Ship_From_Other',
                                       Ship_From_Contact_Name__c = 'Ship_From_Contact',
                                       Ship_From_Contact_Email__c = 'Ship_From_Email',
                                       Ship_From_Contact_Tel__c = 'Ship_From_Tel',
                                       Client_Address_Line_1__c = 'Client_Address_Line_1',
                                       Client_Address_Line_2__c = 'Client_Address_Line_2',
                                       Client_Address_Line_3__c = 'Client_Address_Line_3',
                                       Client_City__c = 'Client_City',
                                       Client_Zip__c = 'Client_Zip',
                                       Client_Country__c = 'Algeria',
                                       Other_notes__c = 'Other_notes',
                                       Tax_Name__c = 'Tax_Name',
                                       Tax_ID__c = 'Tax_ID',
                                       Contact_Name__c = 'Contact_Name',
                                       Contact_Email__c = 'Contact_Email',
                                       Contact_Tel__c = 'Contact_Tel',
                                       NL_Product_User__c='Yes'
                                       );
        insert account;
         Contact contact = new Contact ( lastname ='Testing Individual', Client_Notifications_Choice__c='Opt-In', AccountId = account.Id);
        insert contact;
        
         APP_Tokens__c AT = new APP_Tokens__c ( Contact__C =contact.id, Device_Name__c='Opt-In', Registration_Token__c = 'Test Token');
        insert AT;
        
        
        NCPPushNotificationRegisterWra outerObj = new NCPPushNotificationRegisterWra();
         outerObj.Accesstoken= ATT.Access_Token__c;
         outerObj.ContactID= string.valueof(contact.Id);
         outerObj.UserID= string.valueof(contact.Id);
         outerObj.Registertoken= AT.Registration_Token__c;
         outerObj.DeviceName= AT.Device_Name__c;
        
        
        
        string jsonReq = json.serialize(outerObj);
                  
   		RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
       
       
        req.requestURI = '/services/apexrest/Pushnotificationregister'; 
         req.requestBody = Blob.valueOf(jsonReq);
       // req.addParameter('FinaldeliveryID', FDid);
        req.httpMethod = 'POST';
        req.addHeader('Content-Type', 'application/json');
        
        RestContext.request = req;
        RestContext.response = res;
        
         Test.startTest();
         NCPPushNotificationRegister.NCPPushNotificationRegister();
         Test.stopTest();
         

        
    }
    static testMethod void  setUpData1(){
         Access_Token__c att = new Access_Token__c(
            AccessToken_encoded__c='zmM0MDcxOTItZTJiZi00YWRhLTlkZGQtZGZhMmUxM2FiYTlj',
            Access_Token__c='cc407192-e2bf-4ada-9ddd-dfa2e13aba9c',
            Status__c='Active'
            );
        insert att;
        
          Account account = new Account (name='Acme1', Type ='Client',
                                       CSE_IOR__c = '0050Y000001LTZO',
                                       Service_Manager__c = '0050Y000001LTZO',
                                       Financial_Manager__c='0050Y000001LTZO',
                                       Financial_Controller__c='0050Y000001LTZO',
                                       Tax_recovery_client__c  = FALSE,
                                       Ship_From_Line_1__c = 'Ship_From_Line_1',
                                       Ship_From_Line_2__c = 'Ship_From_Line_2',
                                       Ship_From_Line_3__c = 'Ship_From_Line_3',
                                       Ship_From_City__c = 'Ship_From_City',
                                       Ship_From_Zip__c = 'Ship_From_Zip',
                                       Ship_From_Country__c = 'Ship_From_Country',
                                       Ship_From_Other_notes__c = 'Ship_From_Other',
                                       Ship_From_Contact_Name__c = 'Ship_From_Contact',
                                       Ship_From_Contact_Email__c = 'Ship_From_Email',
                                       Ship_From_Contact_Tel__c = 'Ship_From_Tel',
                                       Client_Address_Line_1__c = 'Client_Address_Line_1',
                                       Client_Address_Line_2__c = 'Client_Address_Line_2',
                                       Client_Address_Line_3__c = 'Client_Address_Line_3',
                                       Client_City__c = 'Client_City',
                                       Client_Zip__c = 'Client_Zip',
                                       Client_Country__c = 'Algeria',
                                       Other_notes__c = 'Other_notes',
                                       Tax_Name__c = 'Tax_Name',
                                       Tax_ID__c = 'Tax_ID',
                                       Contact_Name__c = 'Contact_Name',
                                       Contact_Email__c = 'Contact_Email',
                                       Contact_Tel__c = 'Contact_Tel',
                                       NL_Product_User__c='Yes'
                                       );
        insert account;
         Contact contact = new Contact ( lastname ='Testing Individual', Client_Notifications_Choice__c='Opt-In', AccountId = account.Id);
        insert contact;
        
           Contact contact2 = new Contact ( lastname ='Testing Individual', Client_Notifications_Choice__c='Opt-In', AccountId = account.Id);
        insert contact2;
        
         APP_Tokens__c AT = new APP_Tokens__c ( Contact__C =contact.id, Device_Name__c='Opt-In', Registration_Token__c = 'Test Token');
        insert AT;
        
      
        NCPPushNotificationRegisterWra outerObj = new NCPPushNotificationRegisterWra();
         outerObj.Accesstoken= 'Test';
         outerObj.ContactID= string.valueof(contact2.Id);
         outerObj.UserID= string.valueof(contact.Id);
         outerObj.Registertoken= AT.Registration_Token__c;
         outerObj.DeviceName= AT.Device_Name__c;
        
        
        
        string jsonReq = json.serialize(outerObj);
                  
   		RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
       
       
        req.requestURI = '/services/apexrest/Pushnotificationregister'; 
         req.requestBody = Blob.valueOf(jsonReq);
       // req.addParameter('FinaldeliveryID', FDid);
        req.httpMethod = 'POST';
        req.addHeader('Content-Type', 'application/json');
        
        RestContext.request = req;
        RestContext.response = res;
        
         Test.startTest();
         NCPPushNotificationRegister.NCPPushNotificationRegister();
         Test.stopTest();
         

        
    }

}