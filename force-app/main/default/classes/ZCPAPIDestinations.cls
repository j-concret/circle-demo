@RestResource(urlMapping='/GetZeeDestinationslist/*')
global class ZCPAPIDestinations {
@Httpget
    global static void ZCPAPIDestinations(){
         RestRequest req = RestContext.request;
         RestResponse res = RestContext.response;

        Schema.DescribeFieldResult F = Registrations__c.Country__c.getDescribe();
        Schema.sObjectField T = F.getSObjectField();
        List<PicklistEntry> entries = T.getDescribe().getPicklistValues();

                 res.responseBody = Blob.valueOf(JSON.serializePretty(entries));
        		 res.addHeader('Content-Type', 'application/json');
        	 	 res.statusCode = 200;

}
}