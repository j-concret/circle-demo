@RestResource(urlMapping='/Getregistration/*')
Global class ZCPGetAccRegistration {
    
    @Httppost
    global static void ZCPGetAccRegistration(){
         RestRequest req = RestContext.request;
         RestResponse res = RestContext.response;
         Blob body = req.requestBody;
        res.addHeader('Content-Type', 'application/json');
        String requestString = body.toString();
        
 
        ZCPGetRegistrationwra rw = (ZCPGetRegistrationwra)JSON.deserialize(requestString,ZCPGetRegistrationwra.class);
         try {
            List<Access_token__c> at = [SELECT status__c,Access_Token__c FROM Access_token__c WHERE Access_Token__c =: rw.Accesstoken and status__c = 'Active' LIMIT 1];
            If(!at.isEmpty()){
                 List<Registrations__c> rec= new List<Registrations__c>();
                if(rw.toCountry !=null){
                    rec = [SELECT id,Name,
                                             VAT_number__c,
                                             Registered_Address__c,
                                             Registered_Address_2__c,
                                             Registered_Address_City__c,
                                             Registered_Address_Province__c,
                                             Registered_Address_Postal_Code__c,
                                             Registered_Address_Country__c,
                                             Type_of_registration__c,
                                             Finance_contact_Email__c,
                                             Finance_contact_Name__c,
                                             Finance_contact_Phone__c,
                                             Verified__c,
                                             Company_name__c,
                                             Company_name__r.name,
                                             Country__c                                             
                                              FROM Registrations__c where  Company_name__c =:rw.AccountID and Country__c=:rw.toCountry];
               
                   } 
                else{
                    rec = [SELECT id,Name,
                                             VAT_number__c,
                                             Registered_Address__c,
                                             Registered_Address_2__c,
                                             Registered_Address_City__c,
                                             Registered_Address_Province__c,
                                             Registered_Address_Postal_Code__c,
                                             Registered_Address_Country__c,
                                             Type_of_registration__c,
                                             Finance_contact_Email__c,
                                             Finance_contact_Name__c,
                                             Finance_contact_Phone__c,
                                             Verified__c,
                                             Company_name__c,
                                             Company_name__r.name,
                                             Country__c                                             
                                             FROM Registrations__c where  Company_name__c =:rw.AccountID];
               
                }
                
                			if(!rec.isEmpty()){
                			res.responseBody = Blob.valueOf(JSON.serializePretty(rec));
                  			res.addHeader('Content-Type', 'application/json');
        		  			res.statusCode = 200;
                        API_Log__c Al = New API_Log__c();
                        Al.EndpointURLName__c='GetZeeAccregistration';
                        Al.Response__c='Success - Registration Details are sent';
                        Al.StatusCode__c=string.valueof(res.statusCode);
                        Insert Al;
                			}
                else {
                    
        		    JSONGenerator gen = JSON.createGenerator(true);
            			gen.writeStartObject();
            			gen.writeFieldName('Success');
                        gen.writeStartObject();
                        gen.writeStringField('Message','Registrations are not availble');
                        gen.writeEndObject();
                        gen.writeEndObject();
                    String jsonData = gen.getAsString();
            		res.responseBody = Blob.valueOf(jsonData);
            		res.addHeader('Content-Type', 'application/json');
            		res.statusCode = 400;
                    
                    	API_Log__c Al = New API_Log__c();
                        Al.EndpointURLName__c='GetZeeAccregistration';
                        Al.Response__c='Registrations are not availble'+rw.AccountID  ;
                        Al.StatusCode__c=string.valueof(res.statusCode);
                        Insert Al;
                    
                }
            } 
		
			}
        catch(Exception e){
             		String ErrorString ='Something went wrong, please contact support_SF@tecex.com';
               		res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
            		res.addHeader('Content-Type', 'application/json');
        			res.statusCode = 500;
                
                API_Log__c Al = New API_Log__c();
                //Al.Account__c=rw.AccountID;
                //Al.Login_Contact__c=rw.contactId;
                Al.EndpointURLName__c='GetAccRegisration';
                Al.Response__c=e.getMessage()+' at line number: '+e.getLineNumber();
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al;
            
            
        }
                
       
       
}
    

}