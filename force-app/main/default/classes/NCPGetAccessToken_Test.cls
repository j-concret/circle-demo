@isTest
public class NCPGetAccessToken_Test {

    static testMethod void  testForNCPGetAccessToken(){
        
        RestRequest req1 = new RestRequest();
        RestResponse res1 = new RestResponse();
        req1.requestURI = '/services/apexrest/GetAccessToken/'; 
        req1.httpMethod = 'GET';
        req1.requestBody = Blob.valueOf(JSON.serialize('{"value":"Test Response"}'));     
        RestContext.request = req1;
        RestContext.response = res1;
        RestContext.request.addHeader('Content-Type', 'application/json');
        
        Test.startTest(); 
        	NCPGetAccessToken.GenerateAccessToken();
        Test.stopTest();
    }
}