@RestResource(urlMapping='/ChangeDefaultAddress/*')
Global class CPChangeDefaultAddress {
    
     @Httppost
    global static void CPPickupAddressCreation(){
     	RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        Blob body = req.requestBody;
    	res.addHeader('Content-Type', 'application/json');
        String requestString = body.toString();
        CPChangeDefaultAddressWrapper rw = (CPChangeDefaultAddressWrapper)JSON.deserialize(requestString,CPChangeDefaultAddressWrapper.class);
     		try {
                
                 If(!rw.ChangeDefaultAddress.isEmpty()){
            			Id PickuprecordTypeId = Schema.SObjectType.Client_Address__c.getRecordTypeInfosByDeveloperName().get('PickUp').getRecordTypeId();
            			list<Client_Address__c> PickaddL = new list<Client_Address__c>();
                    
                  	List<Client_Address__c> AB =[select id,Name,default__C,Address_status__c,Comments__c,All_Countries__c,Client__c  from Client_Address__c where All_Countries__c= :rw.ChangeDefaultAddress[0].All_Countries and default__C = true and Client__c =:rw.ChangeDefaultAddress[0].ClientId and recordtypeID=:PickuprecordTypeId];
                 
                  for(Integer i=0; AB.size()>i;i++) { AB[i].Default__c =FALSE;} Update AB;
                     
                     Client_Address__c AB1 =[select id,Name,default__C,Address_status__c,Comments__c,All_Countries__c,Client__c  from Client_Address__c where ID= :rw.ChangeDefaultAddress[0].NewAddressID ];
                     AB1.Default__c = TRUE;
                     Update AB1;
                     
           		String ASD = 'Success-Addrerss updated to default';	
            	res.responseBody = Blob.valueOf(JSON.serializePretty(ASD));
            	res.statusCode = 200;
              	API_Log__c Al = New API_Log__c();
                Al.Account__c=AB1.Client__c;
                Al.EndpointURLName__c='ChangeDefaultAddress';
                Al.Response__c='Success - ChangeDefaultAddress'+rw.ChangeDefaultAddress[0].NewAddressID;
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al; 

                     
                 }
                
                else{
                String ASD = 'Something went wrong while updating address, please contact support_SF@tecex.com';	
            	res.responseBody = Blob.valueOf(JSON.serializePretty(ASD));
            	res.statusCode = 400;
              	API_Log__c Al = New API_Log__c();
                //Al.Account__c=rw.ChangeDefaultAddress[0].ClientID;
                Al.EndpointURLName__c='ChangeDefaultAddress';
                Al.Response__c='Success - ChangeDefaultAddress';
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al; 
                    
                }
                  
     			}
        catch(Exception e){
            String ASD = 'Something went wrong while updating address, please contact support_SF@tecex.com';	
            	res.responseBody = Blob.valueOf(JSON.serializePretty(ASD));
            	res.statusCode = 500;
              	API_Log__c Al = New API_Log__c();
                Al.Account__c=rw.ChangeDefaultAddress[0].ClientID;
                Al.EndpointURLName__c='ChangeDefaultAddress';
                Al.Response__c='Success - ChangeDefaultAddress';
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al; 
            
        }
    
    

}
    
}