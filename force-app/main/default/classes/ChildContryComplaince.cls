public class ChildContryComplaince {
    
     @AuraEnabled
        public static Compliance_Document__c getcomplaincedocument(String PID){
             Compliance_Document__c CD = [select ID,name,Certification_number__c,Compliance_status__c,Compliance_type__c,Country__r.name,Country_Name__c,Name__c,Date_of_issue__c,Expiry__c,Factory_name__c,General_Comments__c  from Compliance_Document__c where id =:PID];
             return CD;
           }
    
         @AuraEnabled
        public static string CreateCC(List<Country_Compliance__c> PCCList,String PID,String PName){
            
             System.debug('PCCList-->'+PCCList);
            System.debug('PID-->'+PID);
            System.debug('PName-->'+PName);
            list<Country_Compliance__c> CCtoInsert = new list<Country_Compliance__c>();
            For(integer i=0;PCCList.size()>i;i++)
            {
                if(PCCList[i].product__c !=null || PCCList[i].HS_Code_and_Associated_Details__c !=null ){
                    
                      PCCList[i].Country_Compliance__c=PID;
                      PCCList[i].name=PName;
                      CCtoInsert.add(PCCList[i]);
                }
                              
                
            }
            
             insert CCtoInsert;
             System.debug('Partstoupdate'+CCtoInsert);
          	 String returnstring =  CCtoInsert.size()+' records inserted'  ;
            return returnstring;
           }

}