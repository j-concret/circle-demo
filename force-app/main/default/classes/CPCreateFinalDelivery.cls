@RestResource(urlMapping='/CreateFinalDelivery/*')
global class CPCreateFinalDelivery {
     @Httppost
    global static void CreateFinalDelivery(){
    
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        Blob body = req.requestBody;
    	res.addHeader('Content-Type', 'application/json');
        String requestString = body.toString();
        CPCreateFinalDeliveryWrapper rw = (CPCreateFinalDeliveryWrapper)JSON.deserialize(requestString,CPCreateFinalDeliveryWrapper.class);
         
        try{
        If(!rw.Final_Delivery.isEmpty()){
           
              
          list<Final_Delivery__c> FDL = new list<Final_Delivery__c>();
           		
              for(Integer i=0; rw.Final_Delivery.size()>i;i++) {
              
          Final_Delivery__c FD = new Final_Delivery__c(
            
            Ship_to_address__c=Rw.Final_Delivery[i].AddressID,
            Shipment_Order__c = Rw.Final_Delivery[i].SOID,
               name= rw.Final_Delivery[i].Name,
               Contact_name__c= rw.Final_Delivery[i].ContactName,
               Contact_email__c= rw.Final_Delivery[i].ContactEmail,
               Contact_number__c= rw.Final_Delivery[i].ContactNumber,
               Address_Line_1__c= rw.Final_Delivery[i].ContactAddreaaLine1,
               Address_Line_2__c= rw.Final_Delivery[i].ContactAddreaaLine2,
               City__c= rw.Final_Delivery[i].City,
               Zip__c= rw.Final_Delivery[i].ZIP,
               Country__c= rw.Final_Delivery[i].Country   );
            
           
              		FDL.add(FD); 
                   
             }
              Insert FDL;
                 
                 String ASD = 'Success- FinalDelivery Created';	
           		 res.responseBody = Blob.valueOf(JSON.serializePretty(ASD));
           		 res.statusCode = 200;
              	API_Log__c Al = New API_Log__c();
               // Al.Account__c=FD.Shipment_Order__r.Account__c;
                Al.EndpointURLName__c='CreateFinalDelivery';
                Al.Response__c='Success- FinalDelivery Created';
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al; 
            
            
        } 
        }
        	catch (Exception e){
                
              	String ErrorString ='Something went wrong while creating Final deliveries, please contact support_SF@tecex.com';
               	res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
            	res.statusCode = 500;
                API_Log__c Al = New API_Log__c();
               // Al.Account__c=FD.Shipment_Order__r.Account__c;
                Al.EndpointURLName__c='UpdateFinalDelivery';
                Al.Response__c=e.getMessage()+' at line number: '+e.getLineNumber();
                Al.StatusCode__c=string.valueof(res.statusCode);
              	Insert Al;

            
        	}

        
        
    }
    

}