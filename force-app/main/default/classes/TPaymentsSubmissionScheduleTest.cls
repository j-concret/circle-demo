@isTest
public class TPaymentsSubmissionScheduleTest implements HttpCalloutMock{
private static HttpResponse resp;
    
    public TPaymentsSubmissionScheduleTest(String testBody,Boolean isTest) {
        resp = new HttpResponse();
        resp.setBody(testBody);        
        if(isTest == true){
            resp.setStatus('OK');
            resp.setStatusCode(200);
        }else{
            resp.setStatus('Error');
            resp.setStatusCode(404);
        }
    }
    
    public HTTPResponse respond(HTTPRequest req) {
        return resp;
    }
    
    public static testMethod void testschedule() {
        
        String testBody = '<?xml version="1.0" encoding="utf-8"?><soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><soap:Body><ProcessPaymentsResponse xmlns="http://Tipalti.org/"><ProcessPaymentsResult><errorMessage>OK</errorMessage><errorCode>0</errorCode><linesResults><ExpandedLineResult><status>Warning</status><paymentOrderStatus>Deferred</paymentOrderStatus><message>Fees exceed amount to be paid</message><refCode>08-14-50-08</refCode><lineErrorOrWarningType>0</lineErrorOrWarningType></ExpandedLineResult></linesResults><summary><TipaltiProcessPaymentsSummary><accountType>7</accountType><accountCurrency>USD</accountCurrency><fundsRequired>0</fundsRequired><outstandingAmount>0</outstandingAmount><tipaltiSummaryAmounts><TipaltiSummaryAmounts><summaryRowCurrency>USD</summaryRowCurrency><totalAmount>10</totalAmount><immediateAmount>10</immediateAmount><payableAmount>0</payableAmount><payerFees>0</payerFees><scheduledAmount>0</scheduledAmount><outstandingAmount>0</outstandingAmount><withholdingAmount>0</withholdingAmount><estimatedProviderFees>0</estimatedProviderFees></TipaltiSummaryAmounts></tipaltiSummaryAmounts></TipaltiProcessPaymentsSummary></summary></ProcessPaymentsResult></ProcessPaymentsResponse></soap:Body></soap:Envelope>';
        
        HttpCalloutMock mock = new TPaymentsSubmissionScheduleTest(testBody,false);
        Test.setMock(HttpCalloutMock.class, mock);
        
        Test.startTest();
       // TPaymentSchedule  b = new TPaymentSchedule ();
        TPaymentsSubmissionSchedule.execute(null); 
        Test.stopTest();
        System.assertEquals(404, resp.getStatusCode());        
        System.assertEquals(resp.getBody(), testBody );
        
    }
}