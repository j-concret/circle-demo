@RestResource(urlMapping='/ActivateFirsttimepassword/*')
Global class CPActivateFirsttimepassword {
// Reset Password
    @Httppatch
    global static void ActivateFirsttimepassword(){
        
    	RestRequest req = RestContext.request;
    	RestResponse res = RestContext.response;
        res.addHeader('Content-Type', 'application/json');  
        String Username = req.params.get('Username');
        String NewPassword = req.params.get('NewPassword');
          
        
        try {
             Contact logincontact = [select Id,account.id, lastname,Email,UserName__c,Password__c,Contact_Status__c from Contact where UserName__c =:username and FirstTimeloginClientPortal__c = FALSE limit 1];
            
            
               if(logincontact.UserName__c == Username && logincontact.Contact_Status__c =='Active'){
                logincontact.Password__c = NewPassword;
                logincontact.FirstTimeloginClientPortal__c = TRUE;   
                Update logincontact;
                
            		String ErrorString ='Password Updated successfully. New password : '+NewPassword;
                    res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
        			res.statusCode = 200;
               
                API_Log__c Al = New API_Log__c();
                Al.Account__c=logincontact.AccountID;
                Al.Login_Contact__c=logincontact.Id;
                Al.EndpointURLName__c='ResetPassword';
                Al.Response__c=ErrorString;
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al;
                
                
            	  }
            else {
                    String ErrorString ='Password not updated. Please retry or contact support_SF@tecex.com';
               		res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
        			res.statusCode = 400;
                
                API_Log__c Al = New API_Log__c();
                Al.Account__c=logincontact.AccountID;
                Al.Login_Contact__c=logincontact.Id;
                Al.EndpointURLName__c='ActivateFirsttimepassword';
                Al.Response__c=ErrorString;
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al;
                
            	}
        
            }
            catch (Exception e) {
                    String ErrorString ='Failed, Please check username or link expired contact support_SF@tecex.com';
               		res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
            	res.statusCode = 500;
                API_Log__c Al = New API_Log__c();
                Al.EndpointURLName__c='ActivateFirsttimepassword';
                Al.Response__c=e.getMessage()+ ' - incorrect email - '+Username;
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al;
            } 

	}    
    

}