@istest
public class NCPGetAlltasksfordashboard_Test {
    
    
    static testMethod void  testForNCPGetAlltasks(){
        
        Id shipmentOrderRecTypeId = Schema.SObjectType.shipment_order__c.getRecordTypeInfosByName().get('Cost Estimate').getRecordTypeId();
        Id accountRecTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('IOR/EOR Supplier').getRecordTypeId();

        
        Access_token__c accessTokenObj = new Access_token__c();
        accessTokenObj.Status__c = 'Active';
        accessTokenObj.Access_Token__c = 'asdfghj-sdfgfds-sfdfgf';
        
        Insert accessTokenObj;
        
        Account acc = new Account(Name = 'Acme Test',RecordTypeId = accountRecTypeId);
        
        Insert acc;
        
        Account acc1 = new Account(Name = 'TecEx Prospective Client',RecordTypeId = accountRecTypeId);
        
        Insert acc1;
        
        Contact contact = new Contact ( lastname ='Testing Individual',  AccountId = acc.Id,Client_Notifications_Choice__c='Opt-In');
        insert contact; 
        
        
        Country__c country = new Country__c(Brokerage_Costs__c = 618, CIF_Adjustment_Factor__c = 0.035, CIF_Absolute_value_adjustment__c = 200, Clearance_Costs__c = 488, 
                                            Country__c = 'Brazil', Name = 'Brazil', Handling_Costs__c = 1180, License_Permit_Costs__c = 0);
        insert country;
        
        CPA_v2_0__c RelatedCPA = new CPA_v2_0__c(Name = 'Brazil', Country_Applied_Costings__c=true );
        insert RelatedCPA;
        CPA_v2_0__c cpav2 = new CPA_v2_0__c(Name = 'Brazil Testing 2.0', Supplier__c = acc.Id, Service_Type__c = 'IOR/EOR', Related_Costing_CPA__c = 'a260Y00000EnDjr', Preferred_Supplier__c = TRUE,
                                            VAT_Rate__c = 0.1, Destination__c = 'Brazil',  
                                            
                                            Default_Section_1_Address_Line_1__c= 'Address_Line_1' ,
                                            Default_Section_1_Address_Line_2__c= 'Address_Line_2' ,
                                            Default_Section_1_Address_Line_3__c= 'Address_Line_3' ,
                                            Default_Section_1_City__c= 'City' ,
                                            Default_Section_1_Zip__c= 'Zip' ,
                                            Default_Section_1_Country__c = 'Country' , 
                                            Default_Section_1_Other__c= 'Other' ,
                                            Default_Section_1_Tax_Name__c= 'Tax_Name' ,
                                            Default_Section_1_Tax_Id__c= 'Tax_Id' ,
                                            Default_Section_1_Contact_Name__c= 'Contact_Name' ,
                                            Default_Section_1_Contact_Email__c= 'Contact_Email' ,
                                            Default_Section_1_Contact_Tel__c= 'Contact_Tel' ,
                                            Alternate_Section_1_Address_Line_1__c= 'Address_Line_1' ,
                                            Alternate_Section_1_Address_Line_2__c= 'Address_Line_2' ,
                                            Alternate_Section_1_Address_Line_3__c= 'Address_Line_3' ,
                                            Alternate_Section_1_City__c= 'City' ,
                                            Alternate_Section_1_Zip__c= 'Zip' ,
                                            Alternate_Section_1_Country__c = 'Country' ,
                                            Alternate_Section_1_Other__c = 'Other' ,
                                            Alternate_Section_1_Tax_Name__c = 'Tax_Name' ,
                                            Alternate_Section_1_Tax_Id__c = 'Tax_Id' ,
                                            Alternate_Section_1_Contact_Name__c= 'Contact_Name' ,
                                            Alternate_Section_1_Contact_Email__c= 'Contact_Email' ,
                                            Alternate_Section_1_Contact_Tel__c= 'Contact_Tel', 
                                            Default_Section_2_Address_Line_1__c= 'Address_Line_1' ,
                                            Default_Section_2_Address_Line_2__c= 'Address_Line_2' ,
                                            Default_Section_2_Address_Line_3__c= 'Address_Line_3' ,
                                            Default_Section_2_City__c= 'City' ,
                                            Default_Section_2_Zip__c= 'Zip' ,
                                            Default_Section_2_Country__c = 'Country' , 
                                            Default_Section_2_Other__c= 'Other' ,
                                            Default_Section_2_Tax_Name__c= 'Tax_Name' ,
                                            Default_Section_2_Tax_Id__c= 'Tax_Id' ,
                                            Default_Section_2_Contact_Name__c= 'Contact_Name' ,
                                            Default_Section_2_Contact_Email__c= 'Contact_Email' ,
                                            Default_Section_2_Contact_Tel__c= 'Contact_Tel' ,
                                            Alternate_Section_2_Address_Line_1__c= 'Address_Line_1' ,
                                            Alternate_Section_2_Address_Line_2__c= 'Address_Line_2' ,
                                            Alternate_Section_2_Address_Line_3__c= 'Address_Line_3' ,
                                            Alternate_Section_2_City__c= 'City' ,
                                            Alternate_Section_2_Zip__c= 'Zip' ,
                                            Alternate_Section_2_Country__c = 'Country' ,
                                            Alternate_Section_2_Other__c = 'Other' ,
                                            Alternate_Section_2_Tax_Name__c = 'Tax_Name' ,
                                            Alternate_Section_2_Tax_Id__c = 'Tax_Id' ,
                                            Alternate_Section_2_Contact_Name__c= 'Contact_Name' ,
                                            Alternate_Section_2_Contact_Email__c= 'Contact_Email',
                                            Alternate_Section_2_Contact_Tel__c= 'Contact_Tel',
                                            Default_Section_3_Address_Line_1__c= 'Address_Line_1' ,
                                            Default_Section_3_Address_Line_2__c= 'Address_Line_2' ,
                                            Default_Section_3_Address_Line_3__c= 'Address_Line_3' ,
                                            Default_Section_3_City__c= 'City' ,
                                            Default_Section_3_Zip__c= 'Zip' ,
                                            Default_Section_3_Country__c = 'Country' , 
                                            Default_Section_3_Other__c= 'Other' ,
                                            Default_Section_3_Tax_Name__c= 'Tax_Name' ,
                                            Default_Section_3_Tax_Id__c= 'Tax_Id' ,
                                            Default_Section_3_Contact_Name__c= 'Contact_Name' ,
                                            Default_Section_3_Contact_Email__c= 'Contact_Email' ,
                                            Default_Section_3_Contact_Tel__c= 'Contact_Tel' ,
                                            Alternate_Section_3_Address_Line_1__c= 'Address_Line_1' ,
                                            Alternate_Section_3_Address_Line_2__c= 'Address_Line_2' ,
                                            Alternate_Section_3_Address_Line_3__c= 'Address_Line_3' ,
                                            Alternate_Section_3_City__c= 'City' ,
                                            Alternate_Section_3_Zip__c= 'Zip' ,
                                            Alternate_Section_3_Country__c = 'Country' ,
                                            Alternate_Section_3_Other__c = 'Other' ,
                                            Alternate_Section_3_Tax_Name__c = 'Tax_Name' ,
                                            Alternate_Section_3_Tax_Id__c = 'Tax_Id' ,
                                            Alternate_Section_3_Contact_Name__c= 'Contact_Name' ,
                                            Alternate_Section_3_Contact_Email__c= 'Contact_Email' ,
                                            Alternate_Section_3_Contact_Tel__c= 'Contact_Tel',
                                            Default_Section_4_Address_Line_1__c= 'Address_Line_1' ,
                                            Default_Section_4_Address_Line_2__c= 'Address_Line_2' ,
                                            Default_Section_4_Address_Line_3__c= 'Address_Line_3' ,
                                            Default_Section_4_City__c= 'City' ,
                                            Default_Section_4_Zip__c= 'Zip' ,
                                            Default_Section_4_Country__c = 'Country' , 
                                            Default_Section_4_Other__c= 'Other' ,
                                            Default_Section_4_Tax_Name__c= 'Tax_Name' ,
                                            Default_Section_4_Tax_Id__c= 'Tax_Id' ,
                                            Default_Section_4_Contact_Name__c= 'Contact_Name' ,
                                            Default_Section_4_Contact_Email__c= 'Contact_Email' ,
                                            Default_Section_4_Contact_Tel__c= 'Contact_Tel' ,
                                            Alternate_Section_4_Address_Line_1__c= 'Address_Line_1' ,
                                            Alternate_Section_4_Address_Line_2__c= 'Address_Line_2' ,
                                            Alternate_Section_4_Address_Line_3__c= 'Address_Line_3' ,
                                            Alternate_Section_4_City__c= 'City' ,
                                            Alternate_Section_4_Zip__c= 'Zip' ,
                                            Alternate_Section_4_Country__c = 'Country' ,
                                            Alternate_Section_4_Other__c = 'Other' ,
                                            Alternate_Section_4_Tax_Name__c = 'Tax_Name' ,
                                            Alternate_Section_4_Tax_Id__c = 'Tax_Id' ,
                                            Alternate_Section_4_Contact_Name__c= 'Contact_Name' ,
                                            Alternate_Section_4_Contact_Email__c= 'Contact_Email' ,
                                            Alternate_Section_4_Contact_Tel__c= 'Contact_Tel' ,
                                            VAT_Reclaim_Destination__c = FALSE,
                                            Final_Destination__c ='Brazil',
                                            Country__c = country.id,
                                            CIF_Freight_and_Insurance__c  = 'CIF Amount'
                                           );
        insert cpav2;
        
        
        CpaRules__c cpaRule = new CpaRules__c();
        cpaRule.Country__c = 'Finland';
        cpaRule.Criteria__c = 'Default';
        cpaRule.DefaultCPA2__c = cpav2.Id;
        cpaRule.CPA2__c =  cpav2.Id;
        Insert cpaRule;
        
        
        
        IOR_Price_List__c priceList = new IOR_Price_List__c();
        priceList.Destination__c = 'Finland';
        priceList.Client_Name__c = acc.Id;
        priceList.IOR_Fee__c = 10;
        
        Insert priceList;
        
        shipment_order__c shipment = new shipment_order__c();
        shipment.RecordTypeId = shipmentOrderRecTypeId;
        shipment.Destination__c = 'Finland';
        shipment.IOR_Price_List__c = priceList.Id;
        shipment.Account__c = acc.Id;
        shipment.CPA_v2_0__c = cpav2.Id;
        shipment.shipping_status__c='In Transit to Hub';
        Insert shipment;
        
        Master_Task__c masterTask = new Master_Task__c(Name = 'Demo Task',Client_Visible__c=TRUE, Shipment_Order_Record_Type__c = 'TecEx');
        Insert masterTask;
         
         Master_Task_Template__c mstTaskTemp = new Master_Task_Template__c(Displayed_to__c = 'TecEx',
                                                                          Master_Task__c = masterTask.Id,
                                                                          State__c = 'TecEx Pending',Task_Title__c = 'Demo',
                                                                          Task_Description__c = 'Testing {0} {1}',
                                                                          Task_Description_Fields__c='Shipment_Order__c.Name,Task.Subject',
                                                                          Complete_Button_Text__c = 'Submit',Complete_Button_Action__c = 'Yes',
                                                                          Complete_Button_Target_Object_API_Name__c = 'Task',
                                                                          Complete_Button_Target_Field_API_Name__c = 'State__c',
                                                                          Complete_Button_Target_Field_Set_Value__c = 'Resolved',
                                                                          Description__c = 'Testing {0} {1}',
                                                                          Description_Fields__c = 'Shipment_Order__c.Name,Task.Subject',
                                                                          Building_Block_Type__c = 'Instruction');
        insert mstTaskTemp;
        
         Master_Task_Prerequisite__c prequisite = new Master_Task_Prerequisite__c(
            Master_Task_Dependent__c = masterTask.Id,
            Master_Task_Prerequisite__c = masterTask.Id
        );
        insert prequisite;
        
         ContentVersion cv = new ContentVersion();
        cv.Title = 'Test Document';
        cv.PathOnClient = 'TestDocument.pdf';
        cv.VersionData = Blob.valueOf('Test Content');
        cv.IsMajorVersion = true;
        
        Insert cv;
        Id TaskRecordTypeId = Schema.SObjectType.Task.getRecordTypeInfosByName().get('MasterTaskRelated').getRecordTypeId();
        Task tk = new Task(status = 'In Progress',
                           priority = 'Normal', State__c= 'Client Pending',
                           WhatId = acc.Id,RecordTypeId = TaskRecordTypeId,
                           Master_Task__c = masterTask.Id,shipment_Order__c = shipment.id,
                           IsNotApplicable__c = false,Inactive__c=false);
        Insert tk;

        FeedItem fd = new FeedItem(Title = 'Hello',ParentId = tk.Id, body='body',visibility='AllUsers');
        
        insert fd;
        
        FeedAttachment fda = new FeedAttachment(type='Content',FeedEntityId = fd.id, recordId=cv.Id);
        
        insert fda;
        NCPGetAlltasksWra obj = new NCPGetAlltasksWra();
        obj.Accesstoken = accessTokenObj.Access_Token__c;
        obj.AccountID = acc.id;
       
        
        RestRequest req1 = new RestRequest();
        RestResponse res1 = new RestResponse();
        req1.requestURI = '/services/apexrest/NCPGetAlltasksfordashboard/'; 
        req1.httpMethod = 'POST';
        req1.requestBody = Blob.valueOf(JSON.serialize(obj));     
        RestContext.request = req1;
        RestContext.response = res1;
        RestContext.request.addHeader('Content-Type', 'application/json');
        
        Test.startTest(); 
        	NCPGetAlltasksfordashboard.NCPGetAlltasks();
        Test.stopTest();
    }
    
    static testMethod void  testForNCPGetAlltasks1(){
        
        Id shipmentOrderRecTypeId = Schema.SObjectType.shipment_order__c.getRecordTypeInfosByName().get('Cost Estimate').getRecordTypeId();
        Id accountRecTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('IOR/EOR Supplier').getRecordTypeId();

        
        Access_token__c accessTokenObj = new Access_token__c();
        accessTokenObj.Status__c = 'Active';
        accessTokenObj.Access_Token__c = 'asdfghj-sdfgfds-sfdfgf';
        
        Insert accessTokenObj;
        
        Account acc = new Account(Name = 'Acme Test',RecordTypeId = accountRecTypeId);
        
        Insert acc;
        
        Account acc1 = new Account(Name = 'TecEx Prospective Client',RecordTypeId = accountRecTypeId);
        
        Insert acc1;
        
        Contact contact = new Contact ( lastname ='Testing Individual',  AccountId = acc.Id,Client_Notifications_Choice__c='Opt-In');
        insert contact; 
        
        
        Country__c country = new Country__c(Brokerage_Costs__c = 618, CIF_Adjustment_Factor__c = 0.035, CIF_Absolute_value_adjustment__c = 200, Clearance_Costs__c = 488, 
                                            Country__c = 'Brazil', Name = 'Brazil', Handling_Costs__c = 1180, License_Permit_Costs__c = 0);
        insert country;
        
        CPA_v2_0__c RelatedCPA = new CPA_v2_0__c(Name = 'Brazil', Country_Applied_Costings__c=true );
        insert RelatedCPA;
        CPA_v2_0__c cpav2 = new CPA_v2_0__c(Name = 'Brazil Testing 2.0', Supplier__c = acc.Id, Service_Type__c = 'IOR/EOR', Related_Costing_CPA__c = 'a260Y00000EnDjr', Preferred_Supplier__c = TRUE,
                                            VAT_Rate__c = 0.1, Destination__c = 'Brazil',  
                                            
                                            Default_Section_1_Address_Line_1__c= 'Address_Line_1' ,
                                            Default_Section_1_Address_Line_2__c= 'Address_Line_2' ,
                                            Default_Section_1_Address_Line_3__c= 'Address_Line_3' ,
                                            Default_Section_1_City__c= 'City' ,
                                            Default_Section_1_Zip__c= 'Zip' ,
                                            Default_Section_1_Country__c = 'Country' , 
                                            Default_Section_1_Other__c= 'Other' ,
                                            Default_Section_1_Tax_Name__c= 'Tax_Name' ,
                                            Default_Section_1_Tax_Id__c= 'Tax_Id' ,
                                            Default_Section_1_Contact_Name__c= 'Contact_Name' ,
                                            Default_Section_1_Contact_Email__c= 'Contact_Email' ,
                                            Default_Section_1_Contact_Tel__c= 'Contact_Tel' ,
                                            Alternate_Section_1_Address_Line_1__c= 'Address_Line_1' ,
                                            Alternate_Section_1_Address_Line_2__c= 'Address_Line_2' ,
                                            Alternate_Section_1_Address_Line_3__c= 'Address_Line_3' ,
                                            Alternate_Section_1_City__c= 'City' ,
                                            Alternate_Section_1_Zip__c= 'Zip' ,
                                            Alternate_Section_1_Country__c = 'Country' ,
                                            Alternate_Section_1_Other__c = 'Other' ,
                                            Alternate_Section_1_Tax_Name__c = 'Tax_Name' ,
                                            Alternate_Section_1_Tax_Id__c = 'Tax_Id' ,
                                            Alternate_Section_1_Contact_Name__c= 'Contact_Name' ,
                                            Alternate_Section_1_Contact_Email__c= 'Contact_Email' ,
                                            Alternate_Section_1_Contact_Tel__c= 'Contact_Tel', 
                                            Default_Section_2_Address_Line_1__c= 'Address_Line_1' ,
                                            Default_Section_2_Address_Line_2__c= 'Address_Line_2' ,
                                            Default_Section_2_Address_Line_3__c= 'Address_Line_3' ,
                                            Default_Section_2_City__c= 'City' ,
                                            Default_Section_2_Zip__c= 'Zip' ,
                                            Default_Section_2_Country__c = 'Country' , 
                                            Default_Section_2_Other__c= 'Other' ,
                                            Default_Section_2_Tax_Name__c= 'Tax_Name' ,
                                            Default_Section_2_Tax_Id__c= 'Tax_Id' ,
                                            Default_Section_2_Contact_Name__c= 'Contact_Name' ,
                                            Default_Section_2_Contact_Email__c= 'Contact_Email' ,
                                            Default_Section_2_Contact_Tel__c= 'Contact_Tel' ,
                                            Alternate_Section_2_Address_Line_1__c= 'Address_Line_1' ,
                                            Alternate_Section_2_Address_Line_2__c= 'Address_Line_2' ,
                                            Alternate_Section_2_Address_Line_3__c= 'Address_Line_3' ,
                                            Alternate_Section_2_City__c= 'City' ,
                                            Alternate_Section_2_Zip__c= 'Zip' ,
                                            Alternate_Section_2_Country__c = 'Country' ,
                                            Alternate_Section_2_Other__c = 'Other' ,
                                            Alternate_Section_2_Tax_Name__c = 'Tax_Name' ,
                                            Alternate_Section_2_Tax_Id__c = 'Tax_Id' ,
                                            Alternate_Section_2_Contact_Name__c= 'Contact_Name' ,
                                            Alternate_Section_2_Contact_Email__c= 'Contact_Email',
                                            Alternate_Section_2_Contact_Tel__c= 'Contact_Tel',
                                            Default_Section_3_Address_Line_1__c= 'Address_Line_1' ,
                                            Default_Section_3_Address_Line_2__c= 'Address_Line_2' ,
                                            Default_Section_3_Address_Line_3__c= 'Address_Line_3' ,
                                            Default_Section_3_City__c= 'City' ,
                                            Default_Section_3_Zip__c= 'Zip' ,
                                            Default_Section_3_Country__c = 'Country' , 
                                            Default_Section_3_Other__c= 'Other' ,
                                            Default_Section_3_Tax_Name__c= 'Tax_Name' ,
                                            Default_Section_3_Tax_Id__c= 'Tax_Id' ,
                                            Default_Section_3_Contact_Name__c= 'Contact_Name' ,
                                            Default_Section_3_Contact_Email__c= 'Contact_Email' ,
                                            Default_Section_3_Contact_Tel__c= 'Contact_Tel' ,
                                            Alternate_Section_3_Address_Line_1__c= 'Address_Line_1' ,
                                            Alternate_Section_3_Address_Line_2__c= 'Address_Line_2' ,
                                            Alternate_Section_3_Address_Line_3__c= 'Address_Line_3' ,
                                            Alternate_Section_3_City__c= 'City' ,
                                            Alternate_Section_3_Zip__c= 'Zip' ,
                                            Alternate_Section_3_Country__c = 'Country' ,
                                            Alternate_Section_3_Other__c = 'Other' ,
                                            Alternate_Section_3_Tax_Name__c = 'Tax_Name' ,
                                            Alternate_Section_3_Tax_Id__c = 'Tax_Id' ,
                                            Alternate_Section_3_Contact_Name__c= 'Contact_Name' ,
                                            Alternate_Section_3_Contact_Email__c= 'Contact_Email' ,
                                            Alternate_Section_3_Contact_Tel__c= 'Contact_Tel',
                                            Default_Section_4_Address_Line_1__c= 'Address_Line_1' ,
                                            Default_Section_4_Address_Line_2__c= 'Address_Line_2' ,
                                            Default_Section_4_Address_Line_3__c= 'Address_Line_3' ,
                                            Default_Section_4_City__c= 'City' ,
                                            Default_Section_4_Zip__c= 'Zip' ,
                                            Default_Section_4_Country__c = 'Country' , 
                                            Default_Section_4_Other__c= 'Other' ,
                                            Default_Section_4_Tax_Name__c= 'Tax_Name' ,
                                            Default_Section_4_Tax_Id__c= 'Tax_Id' ,
                                            Default_Section_4_Contact_Name__c= 'Contact_Name' ,
                                            Default_Section_4_Contact_Email__c= 'Contact_Email' ,
                                            Default_Section_4_Contact_Tel__c= 'Contact_Tel' ,
                                            Alternate_Section_4_Address_Line_1__c= 'Address_Line_1' ,
                                            Alternate_Section_4_Address_Line_2__c= 'Address_Line_2' ,
                                            Alternate_Section_4_Address_Line_3__c= 'Address_Line_3' ,
                                            Alternate_Section_4_City__c= 'City' ,
                                            Alternate_Section_4_Zip__c= 'Zip' ,
                                            Alternate_Section_4_Country__c = 'Country' ,
                                            Alternate_Section_4_Other__c = 'Other' ,
                                            Alternate_Section_4_Tax_Name__c = 'Tax_Name' ,
                                            Alternate_Section_4_Tax_Id__c = 'Tax_Id' ,
                                            Alternate_Section_4_Contact_Name__c= 'Contact_Name' ,
                                            Alternate_Section_4_Contact_Email__c= 'Contact_Email' ,
                                            Alternate_Section_4_Contact_Tel__c= 'Contact_Tel' ,
                                            VAT_Reclaim_Destination__c = FALSE,
                                            Final_Destination__c ='Brazil',
                                            Country__c = country.id,
                                            CIF_Freight_and_Insurance__c  = 'CIF Amount'
                                           );
        insert cpav2;
        
        
        CpaRules__c cpaRule = new CpaRules__c();
        cpaRule.Country__c = 'Finland';
        cpaRule.Criteria__c = 'Default';
        cpaRule.DefaultCPA2__c = cpav2.Id;
        cpaRule.CPA2__c =  cpav2.Id;
        Insert cpaRule;
        
        
        
        IOR_Price_List__c priceList = new IOR_Price_List__c();
        priceList.Destination__c = 'Finland';
        priceList.Client_Name__c = acc.Id;
        priceList.IOR_Fee__c = 10;
        
        Insert priceList;
        
        shipment_order__c shipment = new shipment_order__c();
        shipment.RecordTypeId = shipmentOrderRecTypeId;
        shipment.Destination__c = 'Finland';
        shipment.IOR_Price_List__c = priceList.Id;
        shipment.Account__c = acc.Id;
        shipment.CPA_v2_0__c = cpav2.Id;
        shipment.shipping_status__c='In Transit to Hub';
        Insert shipment;
        
        Master_Task__c masterTask = new Master_Task__c(Name = 'Demo Task',Client_Visible__c=TRUE, Shipment_Order_Record_Type__c = 'TecEx');
        Insert masterTask;
         
         Master_Task_Template__c mstTaskTemp = new Master_Task_Template__c(Displayed_to__c = 'TecEx',
                                                                          Master_Task__c = masterTask.Id,
                                                                          State__c = 'TecEx Pending',Task_Title__c = 'Demo',
                                                                          Task_Description__c = 'Testing {0} {1}',
                                                                          Task_Description_Fields__c='Shipment_Order__c.Name,Task.Subject',
                                                                          Complete_Button_Text__c = 'Submit',Complete_Button_Action__c = 'Yes',
                                                                          Complete_Button_Target_Object_API_Name__c = 'Task',
                                                                          Complete_Button_Target_Field_API_Name__c = 'State__c',
                                                                          Complete_Button_Target_Field_Set_Value__c = 'Resolved',
                                                                          Description__c = 'Testing {0} {1}',
                                                                          Description_Fields__c = 'Shipment_Order__c.Name,Task.Subject',
                                                                          Building_Block_Type__c = 'Instruction');
        insert mstTaskTemp;
        
         ContentVersion cv = new ContentVersion();
        cv.Title = 'Test Document';
        cv.PathOnClient = 'TestDocument.pdf';
        cv.VersionData = Blob.valueOf('Test Content');
        cv.IsMajorVersion = true;
        
        Insert cv;
        Id TaskRecordTypeId = Schema.SObjectType.Task.getRecordTypeInfosByName().get('MasterTaskRelated').getRecordTypeId();
        Task tk = new Task(status = 'In Progress',
                           priority = 'Normal', State__c= 'Client Pending',
                           WhatId = acc.Id,RecordTypeId = TaskRecordTypeId,
                           Master_Task__c = masterTask.Id,shipment_Order__c = shipment.id,
                           IsNotApplicable__c = false,Inactive__c=false);
        Insert tk;

        FeedItem fd = new FeedItem(Title = 'Hello',ParentId = tk.Id, body='body',visibility='AllUsers');
        
        insert fd;
        
        FeedAttachment fda = new FeedAttachment(type='Content',FeedEntityId = fd.id, recordId=cv.Id);
        
        insert fda;
        NCPGetAlltasksWra obj = new NCPGetAlltasksWra();
        obj.Accesstoken = accessTokenObj.Access_Token__c;
        obj.AccountID = acc.id;
       
        
        RestRequest req1 = new RestRequest();
        RestResponse res1 = new RestResponse();
        req1.requestURI = '/services/apexrest/NCPGetAlltasksfordashboard/'; 
        req1.httpMethod = 'POST';
        req1.requestBody = Blob.valueOf(JSON.serialize(obj));     
        RestContext.request = req1;
        RestContext.response = res1;
        RestContext.request.addHeader('Content-Type', 'application/json');
        
        Test.startTest(); 
        	NCPGetAlltasksfordashboard.NCPGetAlltasks();
        Test.stopTest();
    }
    
    static testMethod void  testErrorForNCPGetAlltasks(){
        
        RestRequest req1 = new RestRequest();
        RestResponse res1 = new RestResponse();
        req1.requestURI = '/services/apexrest/NCPGetAlltasksfordashboard/'; 
        req1.httpMethod = 'POST';
        req1.requestBody = Blob.valueOf('{"value":"Test Response"}');     
        RestContext.request = req1;
        RestContext.response = res1;
        RestContext.request.addHeader('Content-Type', 'application/json');
        
        Test.startTest(); 
        	NCPGetAlltasksfordashboard.NCPGetAlltasks();
        Test.stopTest();
    }
}