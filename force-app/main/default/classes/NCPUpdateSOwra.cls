public class NCPUpdateSOwra {
    public String Accesstoken;
    public String Id;
    public String Reference1;
    public String Reference2;
    public Double Shipment_Value_USD;
    public String LionBatteries;
    public String LionBatteriestype;
    public String TypeOfGoods;
    public String ClientContactForShipment;

    public String ServiceType;
    public String ShipFromCountry;
    public Double ChargeableWeight;
    public integer FinalDeliveries;
    public String BuyerAccount;
    public String NCPClientNotes;
    public String ShippingStatus;
    public String ReasonforCancel;
    public String NCPCancelQuoteReason;
    public String preferredFreight;
    public String AppName;//TecEx,Zee,Medical
    public String Intention_with_Goods;
    public String Contact_name;
    public String Beneficial_Owner_Company_Address;
    public String Intention_with_Goods_Other;
    public String Buyer_Retains_Ownership;
    public Boolean Buyer_or_BO_Part_of_EU;
    public String Buyer_or_BO_VAT_Number;
    public String Beneficial_Owner_Country;
}