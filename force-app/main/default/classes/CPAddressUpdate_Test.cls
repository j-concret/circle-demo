@isTest
public class CPAddressUpdate_Test {
    
    static testmethod void setupdata(){
   
     Account account = new Account (name='Acme1', Type ='Client', 
                                       CSE_IOR__c = '0050Y000001LTZO', 
                                       Service_Manager__c = '0050Y000001LTZO', 
                                       Tax_recovery_client__c  = FALSE,
                                       Ship_From_Line_1__c = 'Ship_From_Line_1',
                                       Ship_From_Line_2__c = 'Ship_From_Line_2',
                                       Ship_From_Line_3__c = 'Ship_From_Line_3',
                                       Ship_From_City__c = 'Ship_From_City', 
                                       Ship_From_Zip__c = 'Ship_From_Zip',
                                       Ship_From_Country__c = 'Ship_From_Country' , 
                                       Ship_From_Other_notes__c = 'Ship_From_Other',
                                       Ship_From_Contact_Name__c = 'Ship_From_Contact',
                                       Ship_From_Contact_Email__c = 'Ship_From_Email',
                                       Ship_From_Contact_Tel__c = 'Ship_From_Tel',
                                       Client_Address_Line_1__c = 'Client_Address_Line_1',
                                       Client_Address_Line_2__c = 'Client_Address_Line_2',
                                       Client_Address_Line_3__c = 'Client_Address_Line_3',
                                       Client_City__c = 'Client_City',
                                       Client_Zip__c = 'Client_Zip',
                                       Client_Country__c = 'Client_Country', 
                                       Other_notes__c = 'Other_notes',
                                       Tax_Name__c = 'Tax_Name',
                                       Tax_ID__c = 'Tax_ID',
                                       Contact_Name__c = 'Contact_Name',
                                       Contact_Email__c = 'Contact_Email',
                                       Contact_Tel__c = 'Contact_Tel'
                                      );
        insert account;  
        
            
         
        Contact contact = new Contact ( lastname ='Testing Individual',  email = 'anilk@tecex.com', Password__c='Test',contact_Status__C = 'Active', AccountId = account.Id);
        insert contact;  
        Id PickuprecordTypeId = Schema.SObjectType.Client_Address__c.getRecordTypeInfosByDeveloperName().get('PickUp').getRecordTypeId();
        Id FinalDestrecTypeId = Schema.SObjectType.Client_Address__c.getRecordTypeInfosByDeveloperName().get('Final_Destinations').getRecordTypeId();
    
        Client_Address__c PickAdd1 = new Client_Address__c(RecordTypeID=PickuprecordTypeId,
         												Name='Name',
                                                        Contact_Full_Name__c='FullName',
                                                        Contact_Email__c='anilk@teccex.com',
                                                        Contact_Phone_Number__c='2563563',
                                                        Address__c='Add1',
                                                        Address2__c='Add2',
                                                        City__c='City',
                                                        Province__c='Proince',
                                                        Postal_Code__c='5236',
                                                        All_Countries__c= 'South Africa',
                                                        Client__c=account.id ) ;  
    
     Client_Address__c PickAdd2 = new Client_Address__c(RecordTypeID=PickuprecordTypeId,
         												Name='Name',
                                                        Contact_Full_Name__c='FullName',
                                                        Contact_Email__c='anilk@teccex.com',
                                                        Contact_Phone_Number__c='2563563',
                                                        Address__c='Add1',
                                                        Address2__c='Add2',
                                                        City__c='City',
                                                        Province__c='Proince',
                                                        Postal_Code__c='5236',
                                                        All_Countries__c= 'South Africa',
                                                        Default__C=TRUE,
                                                        Client__c=account.id ) ;  
     Client_Address__c FinalDelAdd3 = new Client_Address__c(RecordTypeID=FinalDestrecTypeId,
         												Name='Name',
                                                        Contact_Full_Name__c='FullName',
                                                        Contact_Email__c='anilk@teccex.com',
                                                        Contact_Phone_Number__c='2563563',
                                                        Address__c='Add1',
                                                        Address2__c='Add2',
                                                        City__c='City',
                                                        Province__c='Proince',
                                                        Postal_Code__c='5236',
                                                        All_Countries__c= 'South Africa',
                                                        Client__c=account.id ) ;  
     Client_Address__c FinalDelAdd4 = new Client_Address__c(RecordTypeID=FinalDestrecTypeId,
         												Name='Name',
                                                        Contact_Full_Name__c='FullName',
                                                        Contact_Email__c='anilk@teccex.com',
                                                        Contact_Phone_Number__c='2563563',
                                                        Address__c='Add1',
                                                        Address2__c='Add2',
                                                        City__c='City',
                                                        Province__c='Proince',
                                                        Postal_Code__c='5236',
                                                        All_Countries__c= 'South Africa',
                                                        Client__c=account.id ) ;  
    
    Insert PickAdd1;
    Insert PickAdd2;
    Insert FinalDelAdd3;
    Insert FinalDelAdd4;
    Update PickAdd1;
    Update PickAdd2;
    Update FinalDelAdd3;
    Update FinalDelAdd4;
    
    
      CPAddressBookEditWrapper outerObj = new CPAddressBookEditWrapper();
		outerObj.ID= PickAdd1.Id;
		outerObj.Name= 'name';
		outerObj.Contact_Full_Name='sadfasdf';
		outerObj.Contact_Email='anilk@tecex.com';
		outerObj.Contact_Phone_Number='12365';
		outerObj.AddressLine1= 'Ref1';
		outerObj.AddressLine1= 'Ref2';
        outerObj.City= 'City';
        outerObj.Province= 'Province';
        outerObj.Postal_Code= '1234';
        outerObj.DefaultAddress='TRUE';
		outerObj.All_Countries= 'South Africa';
	    
   string jsonReq = json.serialize(outerObj);
     
    RestRequest req1 = new RestRequest();
    RestResponse res1 = new RestResponse();
    req1.requestURI = 'https://alpha-testxyz.cs108.force.com/services/apexrest/AddressUpdate'; 
    req1.httpMethod = 'PUT';
    req1.requestBody = Blob.valueOf(jsonReq);
    RestContext.request = req1;
    RestContext.response = res1;
     RestContext.request.addHeader('Content-Type', 'application/json');
    
    Test.startTest();
       
        CPAddressUpdate.CPPickupAddress();

    Test.stopTest();

    }
    static testmethod void setupdataEl(){
   
     Account account = new Account (name='Acme1', Type ='Client', 
                                       CSE_IOR__c = '0050Y000001LTZO', 
                                       Service_Manager__c = '0050Y000001LTZO', 
                                       Tax_recovery_client__c  = FALSE,
                                       Ship_From_Line_1__c = 'Ship_From_Line_1',
                                       Ship_From_Line_2__c = 'Ship_From_Line_2',
                                       Ship_From_Line_3__c = 'Ship_From_Line_3',
                                       Ship_From_City__c = 'Ship_From_City', 
                                       Ship_From_Zip__c = 'Ship_From_Zip',
                                       Ship_From_Country__c = 'Ship_From_Country' , 
                                       Ship_From_Other_notes__c = 'Ship_From_Other',
                                       Ship_From_Contact_Name__c = 'Ship_From_Contact',
                                       Ship_From_Contact_Email__c = 'Ship_From_Email',
                                       Ship_From_Contact_Tel__c = 'Ship_From_Tel',
                                       Client_Address_Line_1__c = 'Client_Address_Line_1',
                                       Client_Address_Line_2__c = 'Client_Address_Line_2',
                                       Client_Address_Line_3__c = 'Client_Address_Line_3',
                                       Client_City__c = 'Client_City',
                                       Client_Zip__c = 'Client_Zip',
                                       Client_Country__c = 'Client_Country', 
                                       Other_notes__c = 'Other_notes',
                                       Tax_Name__c = 'Tax_Name',
                                       Tax_ID__c = 'Tax_ID',
                                       Contact_Name__c = 'Contact_Name',
                                       Contact_Email__c = 'Contact_Email',
                                       Contact_Tel__c = 'Contact_Tel'
                                      );
        insert account;  
         Account account2 = new Account (name='Test Supplier', Type ='Supplier', ICE__c = '0050Y000001km5c', RecordTypeId = '0120Y0000006KjQQAU');
        insert account2; 
        
            
         
        Contact contact = new Contact ( lastname ='Testing Individual',  email = 'anilk@tecex.com', Password__c='Test',contact_Status__C = 'Active', AccountId = account.Id);
        insert contact;  
        Id PickuprecordTypeId = Schema.SObjectType.Client_Address__c.getRecordTypeInfosByDeveloperName().get('PickUp').getRecordTypeId();
        Id FinalDestrecTypeId = Schema.SObjectType.Client_Address__c.getRecordTypeInfosByDeveloperName().get('Final_Destinations').getRecordTypeId();
    
        Client_Address__c PickAdd1 = new Client_Address__c(RecordTypeID=PickuprecordTypeId,
         												Name='Name',
                                                        Contact_Full_Name__c='FullName',
                                                        Contact_Email__c='anilk@teccex.com',
                                                        Contact_Phone_Number__c='2563563',
                                                        Address__c='Add1',
                                                        Address2__c='Add2',
                                                        City__c='City',
                                                        Province__c='Proince',
                                                        Postal_Code__c='5236',
                                                        All_Countries__c= 'South Africa',
                                                        Client__c=account.id ) ;  
    
     Client_Address__c PickAdd2 = new Client_Address__c(RecordTypeID=PickuprecordTypeId,
         												Name='Name',
                                                        Contact_Full_Name__c='FullName',
                                                        Contact_Email__c='anilk@teccex.com',
                                                        Contact_Phone_Number__c='2563563',
                                                        Address__c='Add1',
                                                        Address2__c='Add2',
                                                        City__c='City',
                                                        Province__c='Proince',
                                                        Postal_Code__c='5236',
                                                        All_Countries__c= 'South Africa',
                                                        Client__c=account.id ) ;  
     Client_Address__c FinalDelAdd3 = new Client_Address__c(RecordTypeID=FinalDestrecTypeId,
         												Name='Name',
                                                        Contact_Full_Name__c='FullName',
                                                        Contact_Email__c='anilk@teccex.com',
                                                        Contact_Phone_Number__c='2563563',
                                                        Address__c='Add1',
                                                        Address2__c='Add2',
                                                        City__c='City',
                                                        Province__c='Proince',
                                                        Postal_Code__c='5236',
                                                        All_Countries__c= 'South Africa',
                                                        Client__c=account.id ) ;  
     Client_Address__c FinalDelAdd4 = new Client_Address__c(RecordTypeID=FinalDestrecTypeId,
         												Name='Name',
                                                        Contact_Full_Name__c='FullName',
                                                        Contact_Email__c='anilk@teccex.com',
                                                        Contact_Phone_Number__c='2563563',
                                                        Address__c='Add1',
                                                        Address2__c='Add2',
                                                        City__c='City',
                                                        Province__c='Proince',
                                                        Postal_Code__c='5236',
                                                        All_Countries__c= 'South Africa',
                                                        Client__c=account.id ) ;  
    
    Insert PickAdd1;
    //Insert PickAdd2;
   // Insert FinalDelAdd3;
   // Insert FinalDelAdd4;
   // Update PickAdd1;
   // Update PickAdd2;
  //  Update FinalDelAdd3;
   // Update FinalDelAdd4;
    
    
      CPAddressBookEditWrapper outerObj = new CPAddressBookEditWrapper();
		outerObj.ID= PickAdd1.Id;
		outerObj.Name= 'name';
		outerObj.Contact_Full_Name='sadfasdf';
		outerObj.Contact_Email='anilk@tecex.com';
		outerObj.Contact_Phone_Number='123652222222222222222222222222222222222222222222222222222';
		outerObj.AddressLine1= 'Ref1';
		outerObj.AddressLine1= 'Ref2';
        outerObj.City= 'City';
        outerObj.Province= 'Province';
        outerObj.Postal_Code= '1234';
         outerObj.DefaultAddress='TRUE';
		outerObj.All_Countries= 'South Africa';
	    
   string jsonReq = json.serialize(outerObj);
     
    RestRequest req1 = new RestRequest();
    RestResponse res1 = new RestResponse();
    req1.requestURI = 'https://alpha-testxyz.cs108.force.com/services/apexrest/AddressUpdate'; 
    req1.httpMethod = 'PUT';
    req1.requestBody = Blob.valueOf(jsonReq);
    RestContext.request = req1;
    RestContext.response = res1;
     RestContext.request.addHeader('Content-Type', 'application/json');
    
    Test.startTest();
       
        CPAddressUpdate.CPPickupAddress();

    Test.stopTest();

    }
    static testmethod void setupdataEx(){
   
     Account account = new Account (name='Acme1', Type ='Client', 
                                       CSE_IOR__c = '0050Y000001LTZO', 
                                       Service_Manager__c = '0050Y000001LTZO', 
                                       Tax_recovery_client__c  = FALSE,
                                       Ship_From_Line_1__c = 'Ship_From_Line_1',
                                       Ship_From_Line_2__c = 'Ship_From_Line_2',
                                       Ship_From_Line_3__c = 'Ship_From_Line_3',
                                       Ship_From_City__c = 'Ship_From_City', 
                                       Ship_From_Zip__c = 'Ship_From_Zip',
                                       Ship_From_Country__c = 'Ship_From_Country' , 
                                       Ship_From_Other_notes__c = 'Ship_From_Other',
                                       Ship_From_Contact_Name__c = 'Ship_From_Contact',
                                       Ship_From_Contact_Email__c = 'Ship_From_Email',
                                       Ship_From_Contact_Tel__c = 'Ship_From_Tel',
                                       Client_Address_Line_1__c = 'Client_Address_Line_1',
                                       Client_Address_Line_2__c = 'Client_Address_Line_2',
                                       Client_Address_Line_3__c = 'Client_Address_Line_3',
                                       Client_City__c = 'Client_City',
                                       Client_Zip__c = 'Client_Zip',
                                       Client_Country__c = 'Client_Country', 
                                       Other_notes__c = 'Other_notes',
                                       Tax_Name__c = 'Tax_Name',
                                       Tax_ID__c = 'Tax_ID',
                                       Contact_Name__c = 'Contact_Name',
                                       Contact_Email__c = 'Contact_Email',
                                       Contact_Tel__c = 'Contact_Tel'
                                      );
        insert account;  
         Account account2 = new Account (name='Test Supplier', Type ='Supplier', ICE__c = '0050Y000001km5c', RecordTypeId = '0120Y0000006KjQQAU');
        insert account2; 
        
            
         
        Contact contact = new Contact ( lastname ='Testing Individual',  email = 'anilk@tecex.com', Password__c='Test',contact_Status__C = 'Active', AccountId = account.Id);
        insert contact;  
        Id PickuprecordTypeId = Schema.SObjectType.Client_Address__c.getRecordTypeInfosByDeveloperName().get('PickUp').getRecordTypeId();
        Id FinalDestrecTypeId = Schema.SObjectType.Client_Address__c.getRecordTypeInfosByDeveloperName().get('Final_Destinations').getRecordTypeId();
    
        Client_Address__c PickAdd1 = new Client_Address__c(RecordTypeID=PickuprecordTypeId,
         												Name='Name',
                                                        Contact_Full_Name__c='FullName',
                                                        Contact_Email__c='anilk@teccex.com',
                                                        Contact_Phone_Number__c='2563563',
                                                        Address__c='Add1',
                                                        Address2__c='Add2',
                                                        City__c='City',
                                                        Province__c='Proince',
                                                        Postal_Code__c='5236',
                                                        All_Countries__c= 'South Africa',
                                                        Client__c=account.id ) ;  
    
     Client_Address__c PickAdd2 = new Client_Address__c(RecordTypeID=PickuprecordTypeId,
         												Name='Name',
                                                        Contact_Full_Name__c='FullName',
                                                        Contact_Email__c='anilk@teccex.com',
                                                        Contact_Phone_Number__c='2563563',
                                                        Address__c='Add1',
                                                        Address2__c='Add2',
                                                        City__c='City',
                                                        Province__c='Proince',
                                                        Postal_Code__c='5236',
                                                        All_Countries__c= 'South Africa',
                                                        Client__c=account.id ) ;  
     Client_Address__c FinalDelAdd3 = new Client_Address__c(RecordTypeID=FinalDestrecTypeId,
         												Name='Name',
                                                        Contact_Full_Name__c='FullName',
                                                        Contact_Email__c='anilk@teccex.com',
                                                        Contact_Phone_Number__c='2563563',
                                                        Address__c='Add1',
                                                        Address2__c='Add2',
                                                        City__c='City',
                                                        Province__c='Proince',
                                                        Postal_Code__c='5236',
                                                        All_Countries__c= 'South Africa',
                                                        Client__c=account.id ) ;  
     Client_Address__c FinalDelAdd4 = new Client_Address__c(RecordTypeID=FinalDestrecTypeId,
         												Name='Name',
                                                        Contact_Full_Name__c='FullName',
                                                        Contact_Email__c='anilk@teccex.com',
                                                        Contact_Phone_Number__c='2563563',
                                                        Address__c='Add1',
                                                        Address2__c='Add2',
                                                        City__c='City',
                                                        Province__c='Proince',
                                                        Postal_Code__c='5236',
                                                        All_Countries__c= 'South Africa',
                                                        Client__c=account.id ) ;  
    
    Insert PickAdd1;
    Insert PickAdd2;
    Insert FinalDelAdd3;
    Insert FinalDelAdd4;
    Update PickAdd1;
    Update PickAdd2;
    Update FinalDelAdd3;
    Update FinalDelAdd4;
    
    
      CPAddressBookEditWrapper outerObj = new CPAddressBookEditWrapper();
		outerObj.ID= PickAdd1.Id;
		outerObj.Name= 'name';
		outerObj.Contact_Full_Name='sadfasdf';
		outerObj.Contact_Email='anilk@tecex.com';
		outerObj.Contact_Phone_Number='12365';
		outerObj.AddressLine1= 'Ref1';
		outerObj.AddressLine1= 'Ref2';
        outerObj.City= 'City';
        outerObj.Province= 'Province';
        outerObj.Postal_Code= '1234';
         outerObj.DefaultAddress='TRUE';
		outerObj.All_Countries= 'South Africa';
	    
   string jsonReq = json.serialize(outerObj);
     
    RestRequest req1 = new RestRequest();
    RestResponse res1 = new RestResponse();
    req1.requestURI = '/services/apexrest/AddressUpdate'; 
    req1.httpMethod = 'PUT';
    req1.requestBody = Blob.valueOf(jsonReq);
    RestContext.request = req1;
    RestContext.response = res1;
     RestContext.request.addHeader('Content-Type', 'application/json');
    
    Test.startTest();
       
        
         CPAddressUpdate.CPPickupAddress();

    Test.stopTest();

    }


}