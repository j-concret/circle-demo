@isTest
public class NCPAddParts_Test {

    static testMethod void testPostMethod(){
        Id shipmentOrderRecTypeId = Schema.SObjectType.shipment_order__c.getRecordTypeInfosByName().get('Cost Estimate').getRecordTypeId();
        
        CpaRules__c cpaRule = new CpaRules__c();
        cpaRule.Country__c = 'Finland';
        cpaRule.Criteria__c = 'Default';
        
        Insert cpaRule;
        
        Access_token__c accessTokenObj = new Access_token__c();
        accessTokenObj.Status__c = 'Active';
        accessTokenObj.Access_Token__c = 'asdfghj-sdfgfds-sfdfgf';
        
        Insert accessTokenObj;
        
        Account acc = new Account(Name = 'Acme Test');
        
        Insert acc;
        
        IOR_Price_List__c priceList = new IOR_Price_List__c();
        priceList.Destination__c = 'Finland';
        priceList.Client_Name__c = acc.Id;
        priceList.IOR_Fee__c = 10;
        
        Insert priceList;
        
        shipment_order__c shipment = new shipment_order__c();
        shipment.RecordTypeId = shipmentOrderRecTypeId;
        shipment.Destination__c = 'Finland';
        shipment.IOR_Price_List__c = priceList.Id;
        shipment.Account__c = acc.Id;
        
        Insert shipment;
                
        List<NCPAddPartsWrapper.Parts> partsList = new List<NCPAddPartsWrapper.Parts>();
            NCPAddPartsWrapper.Parts partsObj = new NCPAddPartsWrapper.Parts();
            NCPAddPartsWrapper obj = new NCPAddPartsWrapper();
            
            for(Integer i=0; i<=10; i++){
                
                partsObj.PartDescription = 'part '+ i +' Description for Test';
                partsObj.PartNumber = 'part '+ i;
                partsObj.Quantity = i;
                partsObj.UnitPrice = 500.00 + i;
                partsObj.SOID = shipment.Id;
                
                partsList.add(partsObj);
            }
            
            obj.Parts = partsList;
            obj.Accesstoken = 'asdfghj-sdfgfds-sfdfgf';
            obj.AccountID = acc.Id;
            
            Blob body = Blob.valueOf(JSON.serialize(obj));
    
    RestRequest req1 = new RestRequest();
    RestResponse res1 = new RestResponse();
    req1.requestURI = '/services/apexrest/NCPAddParts'; 
    req1.httpMethod = 'POST';
    req1.requestBody = Blob.valueOf(JSON.serialize(obj));     
    RestContext.request = req1;
    RestContext.response = res1;
    RestContext.request.addHeader('Content-Type', 'application/json');

 	Test.startTest(); 
       NCPAddParts.CreateParts();
    Test.stopTest();
    }
    
    static testMethod void testPostMethodCatchBlock(){
        
    
    RestRequest req1 = new RestRequest();
    RestResponse res1 = new RestResponse();
    req1.requestURI = '/services/apexrest/NCPAddParts'; 
    req1.httpMethod = 'POST';
    req1.requestBody = Blob.valueOf('{"value":"Test response"}');     
    RestContext.request = req1;
    RestContext.response = res1;
    RestContext.request.addHeader('Content-Type', 'application/json');

 	Test.startTest(); 
       NCPAddParts.CreateParts();
    Test.stopTest();
    }
}