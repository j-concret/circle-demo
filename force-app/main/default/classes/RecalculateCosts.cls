public class RecalculateCosts
{

    @InvocableMethod
    public static void recreateSOCosts(List<Id> SOIds)  

    {
        List<SOWrapperRecalculate> newSOWrappers = new List<SOWrapperRecalculate>();
        List<ID> SOsToUpdate = new List<Id>();
        
        shipment_Order__c shipmentOrder = [Select Id, Name,  Insurance_Cost_CIF__c, Shipment_Value_USD__c, IOR_Fee_new__c, FC_IOR_and_Import_Compliance_Fee_USD__c, FC_Total_Customs_Brokerage_Cost__c,
        FC_Total_Handling_Costs__c, FC_Total_License_Cost__c, FC_Total_Clearance_Costs__c, FC_Insurance_Fee_USD__c, FC_Miscellaneous_Fee__c, On_Charge_Mark_up__c , FC_International_Delivery_Fee__c,
        TecEx_Shipping_Fee_Markup__c, CPA_v2_0__c, Chargeable_Weight__c, Account__c, Service_Type__c, Total_Taxes__c, of_Line_Items__c, of_Unique_Line_Items__c, FC_Total__c FROM Shipment_Order__c where id IN :SOIds];
        
        List<Shipment_Order__c> SOs = new List<Shipment_Order__c>();

        List<CPA_Costing__c> costingsToAddUpdate = new List<CPA_Costing__c>();

        List<Shipment_Order__c> updatedSOs =
        [SELECt Id, Name,  Insurance_Cost_CIF__c, Shipment_Value_USD__c, IOR_Fee_new__c, FC_IOR_and_Import_Compliance_Fee_USD__c, FC_Total_Customs_Brokerage_Cost__c,
        FC_Total_Handling_Costs__c, FC_Total_License_Cost__c, FC_Total_Clearance_Costs__c, FC_Insurance_Fee_USD__c, FC_Miscellaneous_Fee__c, On_Charge_Mark_up__c , FC_International_Delivery_Fee__c,
        TecEx_Shipping_Fee_Markup__c, CPA_v2_0__c, Chargeable_Weight__c, FC_Total__c    FROM Shipment_Order__c  WHERE ID in: SOsToUpdate];

        newSOWrappers.Add(new SOWrapperRecalculate((Shipment_Order__c)shipmentOrder));

       
        
        
        
       }
            
                                                   
        
    }