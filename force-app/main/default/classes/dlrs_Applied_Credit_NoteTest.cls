/**
 * Auto Generated and Deployed by the Declarative Lookup Rollup Summaries Tool package (dlrs)
 **/
@IsTest
private class dlrs_Applied_Credit_NoteTest
{
    @IsTest
    private static void testTrigger()
    {
        // Force the dlrs_Applied_Credit_NoteTrigger to be invoked, fails the test if org config or other Apex code prevents this.
        dlrs.RollupService.testHandler(new Applied_Credit_Note__c());
    }
}