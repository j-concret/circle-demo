@isTest
public class CPActivateFirsttimepassword_Test {
     static testmethod void testAccount(){ 
        Test.startTest();
        Account account = new Account (name='Acme1', Type ='Client', 
                                       CSE_IOR__c = '0050Y000001LTZO', 
                                       Service_Manager__c = '0050Y000001LTZO', 
                                       Tax_recovery_client__c  = FALSE,
                                       Ship_From_Line_1__c = 'Ship_From_Line_1',
                                       Ship_From_Line_2__c = 'Ship_From_Line_2',
                                       Ship_From_Line_3__c = 'Ship_From_Line_3',
                                       Ship_From_City__c = 'Ship_From_City', 
                                       Ship_From_Zip__c = 'Ship_From_Zip',
                                       Ship_From_Country__c = 'Ship_From_Country' , 
                                       Ship_From_Other_notes__c = 'Ship_From_Other',
                                       Ship_From_Contact_Name__c = 'Ship_From_Contact',
                                       Ship_From_Contact_Email__c = 'Ship_From_Email',
                                       Ship_From_Contact_Tel__c = 'Ship_From_Tel',
                                       Client_Address_Line_1__c = 'Client_Address_Line_1',
                                       Client_Address_Line_2__c = 'Client_Address_Line_2',
                                       Client_Address_Line_3__c = 'Client_Address_Line_3',
                                       Client_City__c = 'Client_City',
                                       Client_Zip__c = 'Client_Zip',
                                       Client_Country__c = 'Client_Country', 
                                       Other_notes__c = 'Other_notes',
                                       Tax_Name__c = 'Tax_Name',
                                       Tax_ID__c = 'Tax_ID',
                                       Contact_Name__c = 'Contact_Name',
                                       Contact_Email__c = 'Contact_Email',
                                       Contact_Tel__c = 'Contact_Tel'
                                      );
        insert account;  
        
        Contact contact = new Contact ( lastname ='Testing Individual',  AccountId = account.Id,Email='anilk@tecex.com',username__c='anilk@tecex.com',Password__c='Test',Contact_status__C = 'Active',FirstTimeloginClientPortal__c = FALSE);
        insert contact;  
               
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
       
       
        req.requestURI = '/services/apexrest/ActivateFirsttimepassword'; 
        req.addParameter('Username', 'anilk@tecex.com');
        req.addParameter('NewPassword', 'Test');
        req.httpMethod = 'PATCH';
        req.addHeader('Content-Type', 'application/json');
        
        RestContext.request = req;
        RestContext.response = res;

         CPActivateFirsttimepassword.ActivateFirsttimepassword();
    
    Test.stopTest();


	}
    static testmethod void testAccount1(){ 
        Test.startTest();
        Account account = new Account (name='Acme1', Type ='Client', 
                                       CSE_IOR__c = '0050Y000001LTZO', 
                                       Service_Manager__c = '0050Y000001LTZO', 
                                       Tax_recovery_client__c  = FALSE,
                                       Ship_From_Line_1__c = 'Ship_From_Line_1',
                                       Ship_From_Line_2__c = 'Ship_From_Line_2',
                                       Ship_From_Line_3__c = 'Ship_From_Line_3',
                                       Ship_From_City__c = 'Ship_From_City', 
                                       Ship_From_Zip__c = 'Ship_From_Zip',
                                       Ship_From_Country__c = 'Ship_From_Country' , 
                                       Ship_From_Other_notes__c = 'Ship_From_Other',
                                       Ship_From_Contact_Name__c = 'Ship_From_Contact',
                                       Ship_From_Contact_Email__c = 'Ship_From_Email',
                                       Ship_From_Contact_Tel__c = 'Ship_From_Tel',
                                       Client_Address_Line_1__c = 'Client_Address_Line_1',
                                       Client_Address_Line_2__c = 'Client_Address_Line_2',
                                       Client_Address_Line_3__c = 'Client_Address_Line_3',
                                       Client_City__c = 'Client_City',
                                       Client_Zip__c = 'Client_Zip',
                                       Client_Country__c = 'Client_Country', 
                                       Other_notes__c = 'Other_notes',
                                       Tax_Name__c = 'Tax_Name',
                                       Tax_ID__c = 'Tax_ID',
                                       Contact_Name__c = 'Contact_Name',
                                       Contact_Email__c = 'Contact_Email',
                                       Contact_Tel__c = 'Contact_Tel'
                                      );
        insert account;  
        
        Contact contact = new Contact ( lastname ='Testing Individual',  AccountId = account.Id,Email='anilk@tecex.com',username__c='anilk@tecex.com',Password__c='Test',Contact_status__C = 'Inactive',FirstTimeloginClientPortal__c = FALSE);
        insert contact;  
               
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
       
       
        req.requestURI = '/services/apexrest/ActivateFirsttimepassword'; 
        req.addParameter('Username', 'anilk@tecex.com');
        req.addParameter('NewPassword', 'Test');
        req.httpMethod = 'PATCH';
        req.addHeader('Content-Type', 'application/json');
        
        RestContext.request = req;
        RestContext.response = res;

         CPActivateFirsttimepassword.ActivateFirsttimepassword();
    
    Test.stopTest();


	}
 static testmethod void testAccount2(){ 
        Test.startTest();
        Account account = new Account (name='Acme1', Type ='Client', 
                                       CSE_IOR__c = '0050Y000001LTZO', 
                                       Service_Manager__c = '0050Y000001LTZO', 
                                       Tax_recovery_client__c  = FALSE,
                                       Ship_From_Line_1__c = 'Ship_From_Line_1',
                                       Ship_From_Line_2__c = 'Ship_From_Line_2',
                                       Ship_From_Line_3__c = 'Ship_From_Line_3',
                                       Ship_From_City__c = 'Ship_From_City', 
                                       Ship_From_Zip__c = 'Ship_From_Zip',
                                       Ship_From_Country__c = 'Ship_From_Country' , 
                                       Ship_From_Other_notes__c = 'Ship_From_Other',
                                       Ship_From_Contact_Name__c = 'Ship_From_Contact',
                                       Ship_From_Contact_Email__c = 'Ship_From_Email',
                                       Ship_From_Contact_Tel__c = 'Ship_From_Tel',
                                       Client_Address_Line_1__c = 'Client_Address_Line_1',
                                       Client_Address_Line_2__c = 'Client_Address_Line_2',
                                       Client_Address_Line_3__c = 'Client_Address_Line_3',
                                       Client_City__c = 'Client_City',
                                       Client_Zip__c = 'Client_Zip',
                                       Client_Country__c = 'Client_Country', 
                                       Other_notes__c = 'Other_notes',
                                       Tax_Name__c = 'Tax_Name',
                                       Tax_ID__c = 'Tax_ID',
                                       Contact_Name__c = 'Contact_Name',
                                       Contact_Email__c = 'Contact_Email',
                                       Contact_Tel__c = 'Contact_Tel'
                                      );
        insert account;  
        
        Contact contact = new Contact ( lastname ='Testing Individual',  AccountId = account.Id,Email='anilk@tecex.com',username__c='anilk@tecex.com',Password__c='Test',Contact_status__C = 'Active',FirstTimeloginClientPortal__c = TRUE);
        insert contact;  
               
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
       
       
        req.requestURI = '/services/apexrest/ActivateFirsttimepassword'; 
        req.addParameter('Username', 'anilk@tecex.com');
        req.addParameter('NewPassword', 'Test');
        req.httpMethod = 'PATCH';
        req.addHeader('Content-Type', 'application/json');
        
        RestContext.request = req;
        RestContext.response = res;

         CPActivateFirsttimepassword.ActivateFirsttimepassword();
    
    Test.stopTest();


	}
    
    
}