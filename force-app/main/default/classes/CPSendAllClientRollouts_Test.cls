@isTest
public class CPSendAllClientRollouts_Test {
    static testmethod void setupdata(){
        
         Account account = new Account (name='Acme1', Type ='Client', 
                                       CSE_IOR__c = '0050Y000001LTZO', 
                                       Service_Manager__c = '0050Y000001LTZO', 
                                       Tax_recovery_client__c  = FALSE,
                                       Ship_From_Line_1__c = 'Ship_From_Line_1',
                                       Ship_From_Line_2__c = 'Ship_From_Line_2',
                                       Ship_From_Line_3__c = 'Ship_From_Line_3',
                                       Ship_From_City__c = 'Ship_From_City', 
                                       Ship_From_Zip__c = 'Ship_From_Zip',
                                       Ship_From_Country__c = 'Ship_From_Country' , 
                                       Ship_From_Other_notes__c = 'Ship_From_Other',
                                       Ship_From_Contact_Name__c = 'Ship_From_Contact',
                                       Ship_From_Contact_Email__c = 'Ship_From_Email',
                                       Ship_From_Contact_Tel__c = 'Ship_From_Tel',
                                       Client_Address_Line_1__c = 'Client_Address_Line_1',
                                       Client_Address_Line_2__c = 'Client_Address_Line_2',
                                       Client_Address_Line_3__c = 'Client_Address_Line_3',
                                       Client_City__c = 'Client_City',
                                       Client_Zip__c = 'Client_Zip',
                                       Client_Country__c = 'Client_Country', 
                                       Other_notes__c = 'Other_notes',
                                       Tax_Name__c = 'Tax_Name',
                                       Tax_ID__c = 'Tax_ID',
                                       Contact_Name__c = 'Contact_Name',
                                       Contact_Email__c = 'Contact_Email',
                                       Contact_Tel__c = 'Contact_Tel'
                                      );
        insert account;  
        
            
         
        Contact contact = new Contact ( lastname ='Testing Individual',  email = 'anilk@tecex.com', Password__c='Test',contact_Status__C = 'Active', AccountId = account.Id);
        insert contact;  
        
        Roll_Out__c new_roll_out = new Roll_Out__c( Client_Name__c=account.id,
                                                          Contact_For_The_Quote__c=Contact.id,
                                                          Shipment_Value_in_USD__c=200,
                                                          Destinations__c='Brazil',
                                                          Number_of_Cost_Estimates__c =0
                                                         );
     
      	Insert new_roll_out;
        Roll_Out__c new_roll_out1 = new Roll_Out__c( Client_Name__c=account.id,
                                                          Contact_For_The_Quote__c=Contact.id,
                                                          Shipment_Value_in_USD__c=200,
                                                          Destinations__c='Brazil',
                                                          Number_of_Cost_Estimates__c =0
                                                         );
     
      	Insert new_roll_out1;
         Roll_Out__c new_roll_out2 = new Roll_Out__c( Client_Name__c=account.id,
                                                          Contact_For_The_Quote__c=Contact.id,
                                                          Shipment_Value_in_USD__c=200,
                                                          Destinations__c='Brazil',
                                                          Number_of_Cost_Estimates__c =0
                                                         );
     
      	Insert new_roll_out2;
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
       
       
        req.requestURI = '/services/apexrest/SendAllClientRollouts'; 
        req.addParameter('Username', 'anilk@tecex.com');
        req.addParameter('Password', 'Test');
        req.addParameter('ClientAccId', Account.id);
        req.httpMethod = 'GET';
        req.addHeader('Content-Type', 'application/json');
        
        RestContext.request = req;
        RestContext.response = res;
        Test.startTest();
        
        CPSendAllClientRollouts.CPSendAllClientRollout();
        Test.stopTest();
    
    
    }
    static testmethod void setupdata1(){
        
         Account account = new Account (name='Acme1', Type ='Client', 
                                       CSE_IOR__c = '0050Y000001LTZO', 
                                       Service_Manager__c = '0050Y000001LTZO', 
                                       Tax_recovery_client__c  = FALSE,
                                       Ship_From_Line_1__c = 'Ship_From_Line_1',
                                       Ship_From_Line_2__c = 'Ship_From_Line_2',
                                       Ship_From_Line_3__c = 'Ship_From_Line_3',
                                       Ship_From_City__c = 'Ship_From_City', 
                                       Ship_From_Zip__c = 'Ship_From_Zip',
                                       Ship_From_Country__c = 'Ship_From_Country' , 
                                       Ship_From_Other_notes__c = 'Ship_From_Other',
                                       Ship_From_Contact_Name__c = 'Ship_From_Contact',
                                       Ship_From_Contact_Email__c = 'Ship_From_Email',
                                       Ship_From_Contact_Tel__c = 'Ship_From_Tel',
                                       Client_Address_Line_1__c = 'Client_Address_Line_1',
                                       Client_Address_Line_2__c = 'Client_Address_Line_2',
                                       Client_Address_Line_3__c = 'Client_Address_Line_3',
                                       Client_City__c = 'Client_City',
                                       Client_Zip__c = 'Client_Zip',
                                       Client_Country__c = 'Client_Country', 
                                       Other_notes__c = 'Other_notes',
                                       Tax_Name__c = 'Tax_Name',
                                       Tax_ID__c = 'Tax_ID',
                                       Contact_Name__c = 'Contact_Name',
                                       Contact_Email__c = 'Contact_Email',
                                       Contact_Tel__c = 'Contact_Tel'
                                      );
        insert account;  
        
            
         
        Contact contact = new Contact ( lastname ='Testing Individual',  email = 'anilk@tecex.com', Password__c='Test',contact_Status__C = 'Active', AccountId = account.Id);
        insert contact;  
        
        Roll_Out__c new_roll_out = new Roll_Out__c( Client_Name__c=account.id,
                                                          Contact_For_The_Quote__c=Contact.id,
                                                          Shipment_Value_in_USD__c=200,
                                                          Destinations__c='Brazil',
                                                          Number_of_Cost_Estimates__c =0
                                                         );
     
      	Insert new_roll_out;
        Roll_Out__c new_roll_out1 = new Roll_Out__c( Client_Name__c=account.id,
                                                          Contact_For_The_Quote__c=Contact.id,
                                                          Shipment_Value_in_USD__c=200,
                                                          Destinations__c='Brazil',
                                                          Number_of_Cost_Estimates__c =0
                                                         );
     
      	Insert new_roll_out1;
         Roll_Out__c new_roll_out2 = new Roll_Out__c( Client_Name__c=account.id,
                                                          Contact_For_The_Quote__c=Contact.id,
                                                          Shipment_Value_in_USD__c=200,
                                                          Destinations__c='Brazil',
                                                          Number_of_Cost_Estimates__c =0
                                                         );
     
      	Insert new_roll_out2;
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
       
       
        req.requestURI = '/services/apexrest/SendAllClientRollouts'; 
        req.addParameter('Username', 'anilk@tecex.com');
        req.addParameter('Password', 'Test123');
        req.addParameter('ClientAccId', Account.id);
        req.httpMethod = 'GET';
        req.addHeader('Content-Type', 'application/json');
        
        RestContext.request = req;
        RestContext.response = res;
        Test.startTest();
        
        CPSendAllClientRollouts.CPSendAllClientRollout();
        Test.stopTest();
    
    
    }
     static testmethod void setupdata2(){
        
         Account account = new Account (name='Acme1', Type ='Client', 
                                       CSE_IOR__c = '0050Y000001LTZO', 
                                       Service_Manager__c = '0050Y000001LTZO', 
                                       Tax_recovery_client__c  = FALSE,
                                       Ship_From_Line_1__c = 'Ship_From_Line_1',
                                       Ship_From_Line_2__c = 'Ship_From_Line_2',
                                       Ship_From_Line_3__c = 'Ship_From_Line_3',
                                       Ship_From_City__c = 'Ship_From_City', 
                                       Ship_From_Zip__c = 'Ship_From_Zip',
                                       Ship_From_Country__c = 'Ship_From_Country' , 
                                       Ship_From_Other_notes__c = 'Ship_From_Other',
                                       Ship_From_Contact_Name__c = 'Ship_From_Contact',
                                       Ship_From_Contact_Email__c = 'Ship_From_Email',
                                       Ship_From_Contact_Tel__c = 'Ship_From_Tel',
                                       Client_Address_Line_1__c = 'Client_Address_Line_1',
                                       Client_Address_Line_2__c = 'Client_Address_Line_2',
                                       Client_Address_Line_3__c = 'Client_Address_Line_3',
                                       Client_City__c = 'Client_City',
                                       Client_Zip__c = 'Client_Zip',
                                       Client_Country__c = 'Client_Country', 
                                       Other_notes__c = 'Other_notes',
                                       Tax_Name__c = 'Tax_Name',
                                       Tax_ID__c = 'Tax_ID',
                                       Contact_Name__c = 'Contact_Name',
                                       Contact_Email__c = 'Contact_Email',
                                       Contact_Tel__c = 'Contact_Tel'
                                      );
        insert account;  
        
            
         
        Contact contact = new Contact ( lastname ='Testing Individual',  email = 'anilk@tecex.com', Password__c='Test',contact_Status__C = 'Active', AccountId = account.Id);
        insert contact;  
        
        Roll_Out__c new_roll_out = new Roll_Out__c( Client_Name__c=account.id,
                                                          Contact_For_The_Quote__c=Contact.id,
                                                          Shipment_Value_in_USD__c=200,
                                                          Destinations__c='Brazil',
                                                          Number_of_Cost_Estimates__c =0
                                                         );
     
      	Insert new_roll_out;
        Roll_Out__c new_roll_out1 = new Roll_Out__c( Client_Name__c=account.id,
                                                          Contact_For_The_Quote__c=Contact.id,
                                                          Shipment_Value_in_USD__c=200,
                                                          Destinations__c='Brazil',
                                                          Number_of_Cost_Estimates__c =0
                                                         );
     
      	Insert new_roll_out1;
         Roll_Out__c new_roll_out2 = new Roll_Out__c( Client_Name__c=account.id,
                                                          Contact_For_The_Quote__c=Contact.id,
                                                          Shipment_Value_in_USD__c=200,
                                                          Destinations__c='Brazil',
                                                          Number_of_Cost_Estimates__c =0
                                                         );
     
      	Insert new_roll_out2;
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
       
       
        req.requestURI = '/services/apexrest/SendAllClientRollouts'; 
        req.addParameter('Username', 'anilk@tecex.co');
        req.addParameter('Password', 'Test');
        req.addParameter('ClientAccId', Account.id);
        req.httpMethod = 'GET';
        req.addHeader('Content-Type', 'application/json');
        
        RestContext.request = req;
        RestContext.response = res;
        
        String ErrorString ='List has no rows for assignment to SObject';     
        blob b =Blob.valueof(ErrorString);
         
         
         Test.startTest();
        
        CPSendAllClientRollouts.CPSendAllClientRollout();
         String Asd = res.responseBody.toString();
         system.debug('Asd-->'+Asd);
              
       
       //  System.assertEquals('Something went wrong, please contact support_SF@tecex.com',Asd);
        Test.stopTest();
    
    
    }

}