public with Sharing class GenerateZeeDocuments {

    @AuraEnabled
    public static Boolean generateClearanceLetter(Id soId){

        try {
                List<Shipment_order__c> shipments =  [SELECT Id, Name, ZeeClearanceLetter__c, Cost_Estimate_Number__c FROM Shipment_Order__c WHERE Id =:soId];
            
            if(shipments.isEmpty()) throw new AuraHandledException('Not a valid record ,please check its status!');
            
            String soCENumber = shipments[0].Name;
            
            PageReference clearnceLetterPdf = new PageReference(Site.getPathPrefix() + '/apex/zeeVATLOEs');
            clearnceLetterPdf.getParameters().put('id', soId);
            Blob pdfPageBlob = (Test.isRunningTest()) ? Blob.valueOf(String.valueOf('This is a test page')) : clearnceLetterPdf.getContentAsPDF();
            
            
            String title = 'Clearance Letter:'+ soCENumber;
            List<ContentVersion> contVers = [SELECT Id,ContentDocumentId FROM ContentVersion WHERE FirstPublishLocationId=:SoId AND Title=: title LIMIT 1];
            
            ContentVersion contentVersion = new ContentVersion(
                versionData = pdfPageBlob,
                title = title,
                pathOnClient = title + '.pdf',
                origin = 'C');
            
            if(!contVers.isEmpty()) {
                contentVersion.ContentDocumentId = contVers[0].ContentDocumentId;
            }else{
                contentVersion.FirstPublishLocationId =  soId;
            }
            insert contentVersion;
            
            shipments[0].ZeeClearanceLetter__c = true;
            update shipments[0];
            
            return true;
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }
    
    @AuraEnabled
    public static Boolean generateProofOfValuation(Id soId){

        try {
                List<Shipment_order__c> shipments =  [SELECT Id, Name, Cost_Estimate_Number__c FROM Shipment_Order__c WHERE Id =:soId];
            
            if(shipments.isEmpty()) throw new AuraHandledException('Not a valid record ,please check its status!');
            
            String soCENumber = shipments[0].Name;
            List<PageReference> pageList;
            PageReference ALetterPdf = new PageReference(Site.getPathPrefix() + '/apex/ZeeValuationSupport_A');
            PageReference BLetterPdf = new PageReference(Site.getPathPrefix() + '/apex/valuationsCalculationAppendix2');
            pageList = new List<PageReference>{ALetterPdf,BLetterPdf};
            
                List<Blob> pdfPageBlob = new List<Blob>();
                for(PageReference pageref : pageList){
                    pageref.getParameters().put('id', soId);
            		pdfPageBlob.add((Test.isRunningTest()) ? Blob.valueOf(String.valueOf('This is a test page')) : pageref.getContent()); 
                }    
            
            
            List<String> title = new List<String>{'Proof Of Valuation:'+ soCENumber + 'Appendix-A','Proof Of Valuation:'+ soCENumber + 'Appendix-B'};
            Map<String,ContentVersion>contVers = new Map<String,ContentVersion>();
            for(ContentVersion var : [SELECT Id,Title,ContentDocumentId FROM ContentVersion WHERE FirstPublishLocationId=:SoId AND Title IN :title]){
                contVers.put(var.title,var);
            }
            List<ContentVersion> pageAsAttachment = new List<ContentVersion>();
            
            for(integer i=0; i<title.size(); i++){
                system.debug(JSON.serialize(title[i]));
                ContentVersion contentVersion = new ContentVersion(
                    versionData = pdfPageBlob[i],
                    title = title[i],
                    pathOnClient = title[i] + '.pdf',
                    origin = 'C');
                
                if(!contVers.isEmpty() && contVers.containsKey(title[i])) {
                    contentVersion.ContentDocumentId = contVers.get(title[i]).ContentDocumentId;
                }else{
                    contentVersion.FirstPublishLocationId =  soId;
                }
                system.debug(JSON.serialize(contentVersion));
                pageAsAttachment.add(contentVersion);
            }
            
            insert pageAsAttachment;
            
            shipments[0].ZeePOV__c = true;
            update shipments[0];
            
            return true;
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }
}