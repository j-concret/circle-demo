@isTest
public class TestData {

   // Gl Account
	public static List<GL_Account__c> testGlAccount (Boolean doInsert){

		List<GL_Account__c> glAccounts = new List<GL_Account__c>();
        glAccounts.add(new GL_Account__c(Name = '70001', GL_Account_Description__c = 'Prepaid Expenses', GL_Account_Short_Description__c = 'PE'));
        glAccounts.add(new GL_Account__c(Name = '70000', GL_Account_Description__c = 'Revenue Received In Advance', GL_Account_Short_Description__c = 'RRIA'));
        glAccounts.add(new GL_Account__c(Name = '123456', GL_Account_Description__c = 'Forex', GL_Account_Short_Description__c = 'Forex'));
        glAccounts.add(new GL_Account__c(Name = 'Net Taxes', GL_Account_Description__c = 'Net Taxes', GL_Account_Short_Description__c = 'Net Taxes'));
        glAccounts.add(new GL_Account__c(Name = '999999', GL_Account_Description__c = 'Revenue Received In Advance', GL_Account_Short_Description__c = 'RevRecInAdv'));
        glAccounts.add(new GL_Account__c(Name = '110000', GL_Account_Description__c = 'Revenue', GL_Account_Short_Description__c = 'Revenue'));
        glAccounts.add(new GL_Account__c(Name = '120000', GL_Account_Description__c = 'Cost of Sales', GL_Account_Short_Description__c = 'Cost of Sale'));
        glAccounts.add(new GL_Account__c(Name = '314500', GL_Account_Description__c = 'Bank Charges', GL_Account_Short_Description__c = 'Bank Charges'));
        glAccounts.add(new GL_Account__c(Name = '610000', GL_Account_Description__c = 'Customer Control Account', GL_Account_Short_Description__c = 'AR Control'));
        glAccounts.add(new GL_Account__c(Name = '910500', GL_Account_Description__c = 'Unallocated Receipts', GL_Account_Short_Description__c = 'Unallocated'));
        glAccounts.add(new GL_Account__c(Name = '910501', GL_Account_Description__c = 'Unallocated Receipts Customers', GL_Account_Short_Description__c = 'UnallocatedC'));
        glAccounts.add(new GL_Account__c(Name = '920000', GL_Account_Description__c = 'Supplier Control Account', GL_Account_Short_Description__c = 'AP Control'));
		glAccounts.add(new GL_Account__c(Name = '921000', GL_Account_Description__c = 'Accruals', GL_Account_Short_Description__c = 'Accruals'));
		
		if(doInsert){insert glAccounts;}
        return glAccounts;
	}
	//Dimension List 
	public static List<Dimension__c> testDimension(Boolean doInsert){

		List<Dimension__c> dimensionList = new List<Dimension__c>();
							 dimensionList.add(new Dimension__c(Name = 'MASELIM',  Dimension_Description__c = 'Test 610000', Short_Dimension_Description__c = 'Test 610000', Dimension_Type_Code__c = 'PRO'));
							
		if(doInsert){insert dimensionList;}
		return dimensionList;

	}
	
	//Client record
    public static Account testClient(Boolean doInsert){
		List<Dimension__c> dimList = testDimension(true);

		//Requeired Fields: Name and Type
		//RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Client').getRecordTypeId(),
		Account account = new Account (name='Acme1',  Type ='Client',  CSE_IOR__c = '0050Y000001LTZO',  Service_Manager__c = '0050Y000001LTZO',  Tax_recovery_client__c  = FALSE, Ship_From_Line_1__c = 'Ship_From_Line_1',Ship_From_Line_2__c = 'Ship_From_Line_2', Ship_From_Line_3__c = 'Ship_From_Line_3', Ship_From_City__c = 'Ship_From_City',  Ship_From_Zip__c = 'Ship_From_Zip', Ship_From_Country__c = 'Ship_From_Country' ,  Ship_From_Other_notes__c = 'Ship_From_Other', Ship_From_Contact_Name__c = 'Ship_From_Contact', Ship_From_Contact_Email__c = 'Ship_From_Email', Ship_From_Contact_Tel__c = 'Ship_From_Tel', Client_Address_Line_1__c = 'Client_Address_Line_1', Client_Address_Line_2__c = 'Client_Address_Line_2', Client_Address_Line_3__c = 'Client_Address_Line_3',Client_City__c = 'Client_City', Client_Zip__c = 'Client_Zip', Client_Country__c = 'Client_Country',  Other_notes__c = 'Other_notes',  Tax_Name__c = 'Tax_Name', Tax_ID__c = 'Tax_ID',
        Contact_Name__c = 'Contact_Name', Contact_Email__c = 'Contact_Email', Contact_Tel__c = 'Contact_Tel', Default_invoicing_currency__c = 'US Dollar (USD)', IOR_Payment_Terms__c = 7,
        Invoice_Timing__c = 'Upfront invoicing', Invoicing_Term_Parameters__c = 'No Terms', Dimension__c = dimList[0].ID, Region__c = 'USA', New_Invoicing_Structure__c = TRUE ); 
		
		if(doInsert){insert account;}
        return account;
    }

    //Supplier record
    public static Account testSupplier(Boolean doInsert){

         //Requeired Fields: Name and Type
        Account testSupplierAccount = new Account(
        //Account Information
        RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('IOR/EOR Supplier').getRecordTypeId(), 
        Name= 'Supplier', Type = 'Supplier', Company_Registration_Number__c = 'just Testing relax!', Supplier_type__c = 'IOR Provider', 
        Supplier_Inactive__c = false, Region_Bianca__c = 'AMERICAS', ICE__c = '0050Y000001km5c',
        //Finance Information
        Default_invoicing_currency__c = 'US Dollar (USD)',
        //Banking details
        Account_Holder_s_name__c = 'Supplier', Bank_Name__c = 'HSBC MEXICO, S.A. INSTITUCION DE BA', SWIFT_BIC_Code__c = 'BIMEMXMM', IBAN_Number__c = '6405729991',
        //Accounting
        AcctSeed__Accounting_Type__c = 'Vendor');

        if(doInsert){insert testSupplierAccount;}
        return testSupplierAccount;
    }

    //Client Contact
    public static Contact testClientContact(Boolean doInsert){

       return testClientContact(doInsert, testClient(true).Id);
    }

    public static Contact testClientContact(Boolean doInsert, Id accId){

        Contact clientContact = new Contact( AccountId = accId, FirstName = 'testContactFirstName', LastName = 'testContactLastName', Email = 'FirstName@test.com' + Math.floor(Math.random() * 1000),
        Include_in_invoicing_emails__c = true );

        if(doInsert){insert clientContact;}
        return clientContact;
    }

    //Supplier Contact
    public static Contact testSupplierContact(Boolean doInsert){

       return testSupplierContact(doInsert, testSupplier(true).Id);
    }

    public static Contact testSupplierContact(Boolean doInsert, Id accId){

        Contact supplierContact = new Contact( AccountId = accId, FirstName = 'testContactFirstName', LastName = 'testContactLastName', Email = 'FirstName@test.com' + Math.floor(Math.random() * 1000),
        Include_in_invoicing_emails__c = true );

        if(doInsert){insert supplierContact;}
        return supplierContact;
    }

    //IOR Price List Record
    public static IOR_Price_List__c testIORPriceList(Boolean doInsert){

        return testIORPriceList(doInsert, testClient(true).Id);
    }

    public static IOR_Price_List__c testIORPriceList(Boolean doInsert, Id accId){

        IOR_Price_List__c iorpl = new IOR_Price_List__c ( Client_Name__c = accId, Name = 'Brazil', IOR_Fee__c = 0.15, On_Charge_Mark_up__c = 0.15,
                                TecEx_Shipping_Fee_Markup__c =0.40, IOR_Min_Fee__c = 100,  Admin_Fee__c =100, Set_up_fee__c =100, Bank_Fees__c =200, Tax_Rate__c = 0.20,
                                Estimated_Customs_Brokerage__c =250, Estimated_Customs_Clearance__c =250, Estimated_Customs_Handling__c =250, Estimated_Import_License__c =250, 
                                Destination__c = 'Brazil' );
		
		if(doInsert){insert iorpl;}
        return iorpl;
    }

    //CPA1 record
    public static country_price_approval__c testCPA1(Boolean doInsert){

        return testCPA1(doInsert, testSupplier(true).Id);
    }

    public static country_price_approval__c testCPA1(Boolean doInsert, Id accId){

        country_price_approval__c cpa = new country_price_approval__c( name ='Brazil', Billing_term__c = 'DAP/CIF - IOR pays',  Airwaybill_Instructions__c = 'User IOR Address', Supplier__c = accId, 
                                                                        In_Country_Specialist__c = '0050Y000001km5c', Clearance_Destination__c = 'Brazil', Preferred_Supplier__c = TRUE, 
                                                                        Destination__c = 'Brazil' );

        if(doInsert){insert cpa;}
        return cpa;
    }

    // Currency Management record
    public static Currency_Management2__c testCurrencyManagement(Boolean doInsert){

        Currency_Management2__c conversion = new Currency_Management2__c(Name = 'US Dollar (USD)', Conversion_Rate__c = 1, Currency__c = 'US Dollar (USD)', ISO_Code__c = 'USD');
		
		if(doInsert){insert conversion;}
        return conversion;
    }

    //Country record
    public static Country__c testCountry(Boolean doInsert){

        Country__c country = new Country__c(Brokerage_Costs__c = 618, CIF_Adjustment_Factor__c = 0.035, CIF_Absolute_value_adjustment__c = 200, Clearance_Costs__c = 488, 
                                            Country__c = 'Brazil', Name = 'Brazil', Handling_Costs__c = 1180, License_Permit_Costs__c = 0); 

        if(doInsert){insert country;}
        return country;
    }

    //Tax Structure record
    public static List<Tax_Structure_Per_Country__c> testTaxStructure(Boolean doInsert){

        return testTaxStructure(doInsert, testCountry(true).Id);
    }
    
    public static List<Tax_Structure_Per_Country__c> testTaxStructure(Boolean doInsert, Id countryId){

        List<Tax_Structure_Per_Country__c> taxStructure = new List<Tax_Structure_Per_Country__c>();
                taxStructure.add(new Tax_Structure_Per_Country__c(Country__c = countryId, Applied_to_Value__c = 'CIF', Name = 'I', Order_Number__c = '1', Part_Specific__c = TRUE, Tax_Type__c = 'Duties', Default_Duty_Rate__c = 0.5625)); 
                taxStructure.add(new Tax_Structure_Per_Country__c(Country__c = countryId, Applied_to_Value__c = 'CIF', Name = 'II', Order_Number__c = '2', Part_Specific__c = FALSE, Tax_Type__c = 'Other', Rate__c = 0.40, Additional_Percent__c = 0.01 ));
                taxStructure.add(new Tax_Structure_Per_Country__c(Country__c = countryId, Applied_to_Value__c = 'CIF', Name = 'III', Order_Number__c = '3', Part_Specific__c = FALSE, Tax_Type__c = 'VAT', Rate__c = 0.40, Applies_to_Order__c = '1'));
                taxStructure.add(new Tax_Structure_Per_Country__c(Country__c = countryId, Applied_to_Value__c = 'CIF', Name = 'IV', Order_Number__c = '4', Part_Specific__c = FALSE, Tax_Type__c = 'Other', Amount__c = 400));     
                taxStructure.add(new Tax_Structure_Per_Country__c(Country__c = countryId, Applied_to_Value__c = 'CIF', Name = 'V', Order_Number__c = '5', Part_Specific__c = FALSE, Tax_Type__c = 'Other', Rate__c = 0.40, Applies_to_Order__c = '1,2,3'));
                taxStructure.add(new Tax_Structure_Per_Country__c(Country__c = countryId, Applied_to_Value__c = 'CIF', Name = 'VI', Order_Number__c = '6', Part_Specific__c = FALSE, Tax_Type__c = 'Other', Rate__c = 0.40, Applies_to_Order__c = '1,2,3,4', Additional_Percent__c = 0.01));
                taxStructure.add(new Tax_Structure_Per_Country__c(Country__c = countryId, Applied_to_Value__c = 'CIF', Name = 'VII', Order_Number__c = '7', Part_Specific__c = FALSE, Tax_Type__c = 'Other', Rate__c = 0.40, Max__c = 1000, Min__c = 50));       
                taxStructure.add(new Tax_Structure_Per_Country__c(Country__c = countryId, Applied_to_Value__c = 'CIF', Name = 'VIII', Order_Number__c = '8', Part_Specific__c = FALSE, Tax_Type__c = 'Other', Rate__c = 0.40, Applies_to_Order__c = '1,2,3,4,5,7'));
                taxStructure.add(new Tax_Structure_Per_Country__c(Country__c = countryId, Applied_to_Value__c = 'CIF', Name = 'IX', Order_Number__c = '9', Part_Specific__c = FALSE, Tax_Type__c = 'Other', Rate__c = 0.40, Applies_to_Order__c = '1,2,3,4,6'));
                taxStructure.add(new Tax_Structure_Per_Country__c(Country__c = countryId, Applied_to_Value__c = 'CIF', Name = 'X', Order_Number__c = '10', Part_Specific__c = FALSE, Tax_Type__c = 'Other', Rate__c = 0.40)); 
        
        if(doInsert){insert taxStructure;}
        return taxStructure;
    }


	//Related CPA costing
	public static CPA_v2_0__c testRelatedCPA(Boolean doInsert){

		CPA_v2_0__c aCpav = new CPA_v2_0__c(Name = 'Brazil', Country_Applied_Costings__c = TRUE);
		if(doInsert){insert aCpav;}
        return aCpav;
	}
	
	// CPA2 record
    public static CPA_v2_0__c testCPA2(Boolean doInsert){

		return testCPA2(doInsert, testSupplier(true).Id, testCountry(true).Id, testRelatedCPA(true).Id);
		
    }
	
	public static CPA_v2_0__c testCPA2(Boolean doInsert, Id accId, Id countryId, Id relCPAId){

        CPA_v2_0__c cpav2 = new CPA_v2_0__c(Name = 'Brazil Testing 2.0', Supplier__c = accId, Service_Type__c = 'IOR/EOR', Related_Costing_CPA__c = relCPAId, 
                                            Preferred_Supplier__c = TRUE, VAT_Rate__c = 0.1, Destination__c = 'Brazil',  Final_Destination__c = 'Brazil', 
                                            VAT_Reclaim_Destination__c = FALSE, Country__c = countryId, Lead_ICE__c = '0050Y000001km5c');
            if(doInsert){insert cpav2;}
            return cpav2;
    }

    //Costing records
    public static List<CPA_Costing__c> testCostsList(Boolean doInsert){

        return testCostsList(doInsert, testCPA2(true).Id);
    }

    public static List<CPA_Costing__c> testCostsList(Boolean doInsert, Id cpa2Id){
		
		List<CPA_Costing__c> costs = new List<CPA_Costing__c>();

		costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpa2Id, Applied_to__c = 'Shipment value', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'IOR', Min__c = 375, Max__c = 14000, Conditional_value__c = 'Shipment value', Condition__c = 'Between', Floor__c = 0, Ceiling__c = 10000, Currency__c = 'US Dollar (USD)'));
		costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpa2Id, Applied_to__c = 'Anil', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'IOR', Currency__c = 'US Dollar (USD)',Variable_threshold__c = null));
        
        if(doInsert){insert costs;}
        return costs;
    }

    //Roll-out record
    public static Roll_Out__c testRollOut(Boolean doInsert){

        return testRollOut(doInsert, testClient(true).Id, testClientContact(true).Id);
    }
    
    public static Roll_Out__c testRollOut(Boolean doInsert, Id AccountId, Id ConId){

        Roll_Out__c RollOut = new Roll_Out__c( Client_Name__c=AccountId, Contact_For_The_Quote__c=ConId, Shipment_Value_in_USD__c=200, Destinations__c='Brazil', Number_of_Cost_Estimates__c =0
                                                         );
     
      	if(doInsert){insert RollOut;}
        return RollOut;
    }

    //#######################################Shipment Order record ############################################
    public static Shipment_Order__c testShipmentOrder(Boolean doInsert){

        return testShipmentOrder(doInsert, testClient(true).Id, testClientContact(true).Id, testIORPriceList(true).Id, testCPA1(true).Id, testCPA2(true).Id);
    }
    
    public static Shipment_Order__c testShipmentOrder(Boolean doInsert, Id accId, Id conId, Id iorplId, Id cpa1Id, Id cpa2Id){

        Shipment_Order__c shipmentOrder = new Shipment_Order__c(Account__c = accId, Shipment_Value_USD__c = 2000, CPA_v2_0__c = cpa2Id,
											Client_Contact_for_this_Shipment__c = conId, Who_arranges_International_courier__c ='Tecex', Tax_Treatment__c ='DAP/CIF - IOR pays', 
											Ship_to_Country__c = cpa1Id, Destination__c = 'Brazil', Service_Type__c = 'EOR', IOR_Price_List__c = iorplId, Chargeable_Weight__c = 200,  
											Shipping_Status__c = 'Cost Estimate', Miscellaneous_Fee__c = 400,  Miscellaneous_Fee_Name__c = 'Ad Hoc Charge', FC_Admin_Fee__c=100,
											FC_Bank_Fees__c=100, FC_Finance_Fee__c=100, FC_IOR_and_Import_Compliance_Fee_USD__c=100, FC_Insurance_Fee_USD__c=100, 
											FC_International_Delivery_Fee__c=100, FC_Miscellaneous_Fee__c=100, FC_Recharge_Tax_and_Duty__c=500, FC_Total_Clearance_Costs__c=100, 
											FC_Total_Customs_Brokerage_Cost__c=100, FC_Total_Handling_Costs__c=100, FC_Total_License_Cost__c=100,  POD_Date__c =date.today(),
											Tax_Cost__c = 5, International_Delivery_Cost__c = 5 );

        if(doInsert){insert shipmentOrder;}
        return shipmentOrder;
    }
	
	//Client  Invoice
	public static Invoice_New__c testClientInvoice (Boolean doInsert){

		return testClientInvoice(doInsert, testClient(true).Id, testShipmentOrder(true).Id);
	}

	public static Invoice_New__c testClientInvoice(Boolean doInsert, Id accId, Id soId){

		Invoice_New__c SI = new Invoice_New__c( Account__c = accId,   Admin_Fees__c = 100, Bank_Fee__c = 100, Cash_Outlay_Fee__c = 100, Collection_Administration_Fee__c = 100,  Customs_Brokerage_Fees__c = 100,
    	Customs_Clearance_Fees__c = 100, Customs_Handling_Fees__c = 100, Customs_License_In__c = 100, EOR_Fees__c = 100, IOR_Fees__c = 100, International_Freight_Fee__c = 100, Invoice_Amount_Local_Currency__c = 2000,
        Invoice_Currency__c = 'US Dollar (USD)', Invoice_Date__c = date.today(), Invoice_Status__c = 'Invoice Sent',  Invoice_Type__c = 'Invoice', Invoice_amount_USD__c = 2000, Liability_Cover_Fee__c = 100,
        Miscellaneous_Fee__c = 100, Recharge_Tax_and_Duty_Other__c = 100, RecordTypeId = Schema.Sobjecttype.Invoice_New__c.getRecordTypeInfosByDeveloperName().get('Client').getRecordTypeId(),
        Shipment_Order__c = soId , Actual_Admin_Costs__c = 100 , Actual_Bank_Costs__c = 100, Actual_Customs_Brokerage_Costs__c = 100, Actual_Customs_Clearance_Costs__c = 100,
        Actual_Customs_Handling_Costs__c = 100, Actual_Customs_License_Costs__c = 100, Actual_Duties_and_Taxes_Other__c = 100, Actual_Duties_and_Taxes__c = 100, Actual_Finance_Costs__c = 100, Actual_IOREOR_Costs__c = 100,
        Actual_Insurance_Costs__c = 100, Actual_International_Delivery_Costs__c = 100,  Actual_Miscellaneous_Costs__c = 100, Due_Date__c = date.today() - 2, Taxes_and_Duties__c = 100, Overdue__c = TRUE, 
		New_Penalty_Date__c = date.today() - 8 );
		
		if(doInsert){insert SI;}
        return SI;
	}
     //#########################################################Finance####################################################################
        
   
}