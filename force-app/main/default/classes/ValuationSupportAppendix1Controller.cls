public with sharing class ValuationSupportAppendix1Controller {
    public Shipment_Order__c shipment {get;set;}
    public Map<String,String> partImageMap {get;set;}
    public Map<String,Boolean> partImageMapRendered {get;set;}
	public boolean hasData {get;set;}
    
    public ValuationSupportAppendix1Controller(ApexPages.StandardController controller) {
        Id currentRecordId  = ApexPages.CurrentPage().getparameters().get('id');//'a0R9E000007U41eUAC';
        partImageMap = new Map<String,String>();
        partImageMapRendered = new Map<String,Boolean>();
        Set<String> partIds = new Set<String>();
		hasData = false;
        
        shipment = [SELECT Id,CPA_v2_0__r.CI_Currency__c,NCP_Quote_Reference__c,
                        (SELECT Quantity__c,Retail_Method_Sales_Price_per_unit__c,Retail_Method_Extended_Value__c,
                                Retail_Method_Unit_Price__c,Retail_Method_VAT_per_unit__c,Retail_Method_Amazon_Fees_per_unit__c,Name 
                        FROM Parts__r) 
                        FROM Shipment_Order__c WHERE Id =:currentRecordId ];
        System.debug('shipment  '+ JSON.serialize(shipment));

        for (Part__c variable : shipment.Parts__r) {
            partIds.add(variable.Id);
        }
        System.debug('partIds  '+ JSON.serialize(partIds));
        /*for(ContentVersion cntVersion : [Select VersionData, FirstPublishLocationId from ContentVersion Where isLatest = true AND Title='Attachment A' AND FirstPublishLocationId IN :partIds]){
            partImageMap.put(cntVersion.FirstPublishLocationId, cntVersion.Id);
        }*/
        Map<String,String> cntDocumentids = new Map<String,String>();
        for(ContentDocumentLink cnt : [SELECT Id, LinkedEntityId, ContentDocumentId FROM ContentDocumentLink Where LinkedEntityId =: partIds]){
            cntDocumentids.put(cnt.ContentDocumentId,cnt.LinkedEntityId);
        }
        System.debug('cntDocumentids  '+ JSON.serialize(cntDocumentids));
        if(!cntDocumentids.isEmpty()){
           for(ContentVersion cntVersion : [SELECT Id, FirstPublishLocationId,ContentDocumentId, Title FROM ContentVersion where ContentDocumentId IN:cntDocumentids.keySet() AND isLatest = true AND Title LIKE '%Attachment A%' order by CreatedDate]){
            	partImageMap.put(cntDocumentids.get(cntVersion.ContentDocumentId), cntVersion.Id);
               	//hasData = true;
        	}            
        }
        System.debug('partImageMap  '+ JSON.serialize(partImageMap));
        for(Id key : partIds){
                if(partImageMap.containsKey(key)){
                   partImageMapRendered.put(key,true); 
                }else{
                   partImageMapRendered.put(key, false); 
                }
            }
        System.debug('partImageMapRendered  '+ JSON.serialize(partImageMapRendered));
      
    }
}