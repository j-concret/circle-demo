public class NCPQuickQuoteCreationWrap {
    public String Accesstoken;
    public String shipFrom;
    public String shipTo;
    public Decimal shipmentValueUSD;
    public Decimal chargableWeight;
    public String weightUnit;
    public String typeOfGoods;
    public String aboutShipment;
    public String companyName;
    public String companyLocation;
    public String FirstName;
    public String LastName;
    //public String Name;
    public String countryCode;
    public String contactNumber;
    public String emailAddress;
    public String recaptchatoken;
}