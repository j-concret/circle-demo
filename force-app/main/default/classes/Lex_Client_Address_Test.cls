@isTest
public class Lex_Client_Address_Test {

    public testmethod static void testGetRecordTypes() {
        String clientAddressRecordId = Schema.Sobjecttype.Client_Address__c.getRecordTypeInfosByDeveloperName().get('PickUp').getRecordTypeId();
    	Lex_Client_Address.getrecordTypes(clientAddressRecordId);
        Lex_Client_Address.getrecordTypes(null);
    }
    
}