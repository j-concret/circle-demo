@RestResource(urlMapping='/GetIPNStatus/*')
global class TIPNBilllines {
    
    @Httppost
    global static void TIPNBilllines(){
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        
        try{
            system.debug('res-->'+req.params);
            Map<string,string> ipnparam = req.params;
            Blob fullbody = req.requestBody;
            String FullURL = req.requestURI;
            System.debug('ipnparam --->'+ipnparam);
            System.debug('ipnparam Msg--->'+JSON.serializePretty(ipnparam));
            String reqBody = '';
            String reqBody1 = '';
            system.debug('before');
            if(ipnparam.get('type') == 'payee_details_changed'){
                system.debug(' in payee_details_changed');    
                
                reqBody = ((ipnparam.get('key') != null) ? 'key='+EncodingUtil.urlEncode(ipnparam.get('key'), 'UTF-8') : '')+
                    ((ipnparam.get('c_date') != null) ? '&c_date='+EncodingUtil.urlEncode(ipnparam.get('c_date'), 'UTF-8') : '')+
                    ((ipnparam.get('payee_id') != null) ? '&payee_id='+EncodingUtil.urlEncode(ipnparam.get('payee_id'), 'UTF-8') : '')+
                    ((ipnparam.get('first_name') != null) ? '&first_name='+EncodingUtil.urlEncode(ipnparam.get('first_name'), 'UTF-8') : '')+
                    ((ipnparam.get('middle_name') != null) ? '&middle_name='+EncodingUtil.urlEncode(ipnparam.get('middle_name'), 'UTF-8') : '')+
                    ((ipnparam.get('last_name') != null) ? '&last_name='+EncodingUtil.urlEncode(ipnparam.get('last_name') , 'UTF-8'): '')+
                    ((ipnparam.get('street1') != null) ? '&street1='+EncodingUtil.urlEncode(ipnparam.get('street1') , 'UTF-8'): '')+
                    ((ipnparam.get('street2') != null) ? '&street2='+EncodingUtil.urlEncode(ipnparam.get('street2'), 'UTF-8') : '')+
                    ((ipnparam.get('city') != null) ? '&city='+EncodingUtil.urlEncode(ipnparam.get('city'), 'UTF-8') : '')+
                    ((ipnparam.get('zip_code') != null) ? '&zip_code='+EncodingUtil.urlEncode(ipnparam.get('zip_code') , 'UTF-8'): '')+
                    ((ipnparam.get('country') != null) ? '&country='+EncodingUtil.urlEncode(ipnparam.get('country'), 'UTF-8') : '')+
                    ((ipnparam.get('payment_country') != null) ? '&payment_country='+EncodingUtil.urlEncode(ipnparam.get('payment_country'), 'UTF-8') : '')+
                    ((ipnparam.get('payment_method') != null) ? '&payment_method='+EncodingUtil.urlEncode(ipnparam.get('payment_method'), 'UTF-8') : '')+
                    ((ipnparam.get('payment_currency') != null) ? '&payment_currency='+EncodingUtil.urlEncode(ipnparam.get('payment_currency'), 'UTF-8') : '')+
                    ((ipnparam.get('is_payable') != null) ? '&is_payable='+EncodingUtil.urlEncode(ipnparam.get('is_payable') , 'UTF-8'): '')+
                    ((ipnparam.get('company') != null) ? '&company='+EncodingUtil.urlEncode(ipnparam.get('company'), 'UTF-8') : '')+
                    ((ipnparam.get('email') != null) ? '&email='+EncodingUtil.urlEncode(ipnparam.get('email'), 'UTF-8') : '')+
                    ((ipnparam.get('phone') != null) ? '&phone='+EncodingUtil.urlEncode(ipnparam.get('phone'), 'UTF-8') : '')+
                    ((ipnparam.get('intern_swift') != null) ? '&intern_swift='+EncodingUtil.urlEncode(ipnparam.get('intern_swift'), 'UTF-8') : '')+
                    ((ipnparam.get('intern_bank_name') != null) ? '&intern_bank_name='+EncodingUtil.urlEncode(ipnparam.get('intern_bank_name'), 'UTF-8') : '')+
                    ((ipnparam.get('intern_account_number') != null) ? '&intern_account_number='+EncodingUtil.urlEncode(ipnparam.get('intern_account_number'), 'UTF-8') : '')+
                 	((ipnparam.get('payee_status') != null) ? '&payee_status='+EncodingUtil.urlEncode(ipnparam.get('payee_status'), 'UTF-8') : '')+
                    ((ipnparam.get('blocked_automatically') != null) ? '&blocked_automatically='+EncodingUtil.urlEncode(ipnparam.get('blocked_automatically'), 'UTF-8') : '')+
                    ((ipnparam.get('payment_method_token') != null) ? '&payment_method_token='+EncodingUtil.urlEncode(ipnparam.get('payment_method_token') , 'UTF-8'): '')+
                    ((ipnparam.get('bank_beneficiary_name') != null) ? '&bank_beneficiary_name='+EncodingUtil.urlEncode(ipnparam.get('bank_beneficiary_name') , 'UTF-8'): '')+
                    ((ipnparam.get('iban') != null) ? '&iban='+EncodingUtil.urlEncode(ipnparam.get('iban'), 'UTF-8') : '')+
                    ((ipnparam.get('swift') != null) ? '&swift='+EncodingUtil.urlEncode(ipnparam.get('swift'), 'UTF-8') : '')+
                    ((ipnparam.get('bank_code') != null) ? '&bank_code='+EncodingUtil.urlEncode(ipnparam.get('bank_code'), 'UTF-8') : '')+
                    ((ipnparam.get('branch_code') != null) ? '&branch_code='+EncodingUtil.urlEncode(ipnparam.get('branch_code'), 'UTF-8') : '')+
                    ((ipnparam.get('bank_name') != null) ? '&bank_name='+EncodingUtil.urlEncode(ipnparam.get('bank_name') , 'UTF-8'): '')+
                    ((ipnparam.get('bank_branch_name') != null) ? '&bank_branch_name='+EncodingUtil.urlEncode(ipnparam.get('bank_branch_name') , 'UTF-8'): '')+
                    ((ipnparam.get('bank_address') != null) ? '&bank_address='+EncodingUtil.urlEncode(ipnparam.get('bank_address') , 'UTF-8'): '')+
                    ((ipnparam.get('bank_address2') != null) ? '&bank_address2='+EncodingUtil.urlEncode(ipnparam.get('bank_address2') , 'UTF-8'): '')+
                    ((ipnparam.get('bank_city') != null) ? '&bank_city='+EncodingUtil.urlEncode(ipnparam.get('bank_city') , 'UTF-8'): '')+
                    ((ipnparam.get('account_number') != null) ? '&account_number='+EncodingUtil.urlEncode(ipnparam.get('account_number'), 'UTF-8'): '')+
                    ((ipnparam.get('ach_account_type') != null) ? '&ach_account_type='+EncodingUtil.urlEncode(ipnparam.get('ach_account_type'), 'UTF-8') : '')+
                    ((ipnparam.get('tax_form_entity_type') != null) ? '&tax_form_entity_type='+EncodingUtil.urlEncode(ipnparam.get('tax_form_entity_type'), 'UTF-8') : '')+
                    ((ipnparam.get('tax_form_type') != null) ? '&tax_form_type='+EncodingUtil.urlEncode(ipnparam.get('tax_form_type'), 'UTF-8'): '')+
                    ((ipnparam.get('tax_form_status') != null) ? '&tax_form_status='+EncodingUtil.urlEncode(ipnparam.get('tax_form_status'), 'UTF-8') : '')+
                    ((ipnparam.get('type') != null) ? '&type='+EncodingUtil.urlEncode(ipnparam.get('type'), 'UTF-8') : '');
                
                
                
                
                
                
                
                
            }
            else if(ipnparam.get('type') == 'payment_submitted'){
                
                reqBody = ((ipnparam.get('key') != null) ? 'key='+EncodingUtil.urlEncode(ipnparam.get('key'), 'UTF-8'): '')+
                    ((ipnparam.get('c_date') != null) ?'&c_date='+EncodingUtil.urlEncode(ipnparam.get('c_date'), 'UTF-8'): '')+
                    ((ipnparam.get('type') != null) ?'&type='+EncodingUtil.urlEncode(ipnparam.get('type'), 'UTF-8'): '')+
                    ((ipnparam.get('ref_code') != null) ?'&ref_code='+EncodingUtil.urlEncode(ipnparam.get('ref_code'), 'UTF-8'): '')+
                    ((ipnparam.get('seq_ref_code') != null) ?'&seq_ref_code='+EncodingUtil.urlEncode(ipnparam.get('seq_ref_code'), 'UTF-8'): '')+
                    ((ipnparam.get('payee_id') != null) ?'&payee_id='+EncodingUtil.urlEncode(ipnparam.get('payee_id'), 'UTF-8'): '')+
                    ((ipnparam.get('submit_date') != null) ?'&submit_date='+EncodingUtil.urlEncode(ipnparam.get('submit_date'), 'UTF-8'): '')+
                    ((ipnparam.get('amount_submitted') != null) ?'&amount_submitted='+EncodingUtil.urlEncode(ipnparam.get('amount_submitted'), 'UTF-8'): '')+
                    ((ipnparam.get('currency_submitted') != null) ?'&currency_submitted='+EncodingUtil.urlEncode(ipnparam.get('currency_submitted'), 'UTF-8'): '')+
                    ((ipnparam.get('payment_method') != null) ?'&payment_method='+EncodingUtil.urlEncode(ipnparam.get('payment_method'), 'UTF-8'): '')+
                    ((ipnparam.get('payee_fees') != null) ?'&payee_fees='+EncodingUtil.urlEncode(ipnparam.get('payee_fees'), 'UTF-8'): '')+
                    ((ipnparam.get('payee_fees_currency') != null) ?'&payee_fees_currency='+EncodingUtil.urlEncode(ipnparam.get('payee_fees_currency'), 'UTF-8'): '')+
                    ((ipnparam.get('is_finalized') != null) ?'&is_finalized='+EncodingUtil.urlEncode(ipnparam.get('is_finalized'), 'UTF-8'): '')+
                    ((ipnparam.get('provider') != null) ?'&provider='+EncodingUtil.urlEncode(ipnparam.get('provider'), 'UTF-8'): '')+
                    ((ipnparam.get('account_identifier') != null) ?'&account_identifier='+EncodingUtil.urlEncode(ipnparam.get('account_identifier'), 'UTF-8'): '')+
                    ((ipnparam.get('payer_exchange_rate') != null) ?'&payer_exchange_rate='+EncodingUtil.urlEncode(ipnparam.get('payer_exchange_rate'), 'UTF-8'): '')+
                    ((ipnparam.get('invoicenumber') != null) ?'&invoicenumber='+EncodingUtil.urlEncode(ipnparam.get('invoicenumber'), 'UTF-8'): '')+
                    ((ipnparam.get('invoicenumber2') != null) ?'&invoicenumber2='+EncodingUtil.urlEncode(ipnparam.get('invoicenumber2'), 'UTF-8'): '')+
                    ((ipnparam.get('invoicenumber3') != null) ?'&invoicenumber3='+EncodingUtil.urlEncode(ipnparam.get('invoicenumber3'), 'UTF-8'): '')+
                    ((ipnparam.get('transaction_date') != null) ?'&transaction_date='+EncodingUtil.urlEncode(ipnparam.get('transaction_date'), 'UTF-8'): '');
               
                
            }
            else if(ipnparam.get('type') == 'deferred'){
                String deferredReason = ((ipnparam.get('deferred_reasons') != null) ? EncodingUtil.urlEncode(ipnparam.get('deferred_reasons'), 'UTF-8'): '');
                System.debug('deferredReason');
                System.debug(deferredReason);
                reqBody = ((ipnparam.get('key') != null) ? 'key='+EncodingUtil.urlEncode(ipnparam.get('key'), 'UTF-8'): '')+
                    ((ipnparam.get('c_date') != null) ? '&c_date='+EncodingUtil.urlEncode(ipnparam.get('c_date'), 'UTF-8'): '')+
                    ((ipnparam.get('type') != null) ? '&type='+EncodingUtil.urlEncode(ipnparam.get('type'), 'UTF-8'): '')+
                    ((ipnparam.get('amount_submitted') != null) ? '&amount_submitted='+EncodingUtil.urlEncode(ipnparam.get('amount_submitted'), 'UTF-8'): '')+
                    ((ipnparam.get('currency_submitted') != null) ? '&currency_submitted='+EncodingUtil.urlEncode(ipnparam.get('currency_submitted'), 'UTF-8'): '')+
                    ((ipnparam.get('payee_id') != null) ? '&payee_id='+EncodingUtil.urlEncode(ipnparam.get('payee_id'), 'UTF-8'): '')+
                    ((ipnparam.get('ref_code') != null) ? '&ref_code='+EncodingUtil.urlEncode(ipnparam.get('ref_code'), 'UTF-8'): '')+
                    ((ipnparam.get('deferred_date') != null) ? '&deferred_date='+EncodingUtil.urlEncode(ipnparam.get('deferred_date'), 'UTF-8'): '')+
                    '&deferred_reasons='+deferredReason+
                    ((ipnparam.get('invoicenumber') != null) ? '&invoicenumber='+EncodingUtil.urlEncode(ipnparam.get('invoicenumber'), 'UTF-8'): '')+
                    ((ipnparam.get('invoicenumber2') != null) ?'&invoicenumber2='+EncodingUtil.urlEncode(ipnparam.get('invoicenumber2'), 'UTF-8'): '')+
                    ((ipnparam.get('invoicenumber3') != null) ?'&invoicenumber3='+EncodingUtil.urlEncode(ipnparam.get('invoicenumber3'), 'UTF-8'): '');
                    
            }
            else if(ipnparam.get('type') == 'error'){
                System.debug('Error Code ==>');
                System.debug(ipnparam.get('error_code'));	
                reqBody = ((ipnparam.get('key') != null) ?'key='+EncodingUtil.urlEncode(ipnparam.get('key'), 'UTF-8'): '')+
                    ((ipnparam.get('c_date') != null) ?'&c_date='+EncodingUtil.urlEncode(ipnparam.get('c_date'), 'UTF-8'): '')+
                    ((ipnparam.get('type') != null) ?'&type='+EncodingUtil.urlEncode(ipnparam.get('type'), 'UTF-8'): '')+
                    ((ipnparam.get('error') != null) ?'&error='+EncodingUtil.urlEncode(ipnparam.get('error'), 'UTF-8'): '')+
                    ((ipnparam.get('error_code') != null) ?'&error_code='+EncodingUtil.urlEncode(ipnparam.get('error_code'), 'UTF-8'): '')+
                    ((ipnparam.get('amount_submitted') != null) ?'&amount_submitted='+EncodingUtil.urlEncode(ipnparam.get('amount_submitted'), 'UTF-8'): '')+
                    ((ipnparam.get('error_date') != null) ?'&error_date='+EncodingUtil.urlEncode(ipnparam.get('error_date'), 'UTF-8'): '')+
                    ((ipnparam.get('payee_id') != null) ?'&payee_id='+EncodingUtil.urlEncode(ipnparam.get('payee_id'), 'UTF-8'): '')+
                    ((ipnparam.get('ref_code') != null) ?'&ref_code='+EncodingUtil.urlEncode(ipnparam.get('ref_code'), 'UTF-8'): '')+
                    ((ipnparam.get('seq_ref_code') != null) ?'&seq_ref_code='+EncodingUtil.urlEncode(ipnparam.get('seq_ref_code'), 'UTF-8'): '')+
                    ((ipnparam.get('currency_submitted') != null) ?'&currency_submitted='+EncodingUtil.urlEncode(ipnparam.get('currency_submitted'), 'UTF-8'): '')+
                    ((ipnparam.get('payer_fees') != null) ?'&payer_fees='+EncodingUtil.urlEncode(ipnparam.get('payer_fees'), 'UTF-8'): '')+
                    ((ipnparam.get('invoicenumber') != null) ?'&invoicenumber='+EncodingUtil.urlEncode(ipnparam.get('invoicenumber'), 'UTF-8'): '')+
                    ((ipnparam.get('invoicenumber2') != null) ?'&invoicenumber2='+EncodingUtil.urlEncode(ipnparam.get('invoicenumber2'), 'UTF-8'): '')+
                    ((ipnparam.get('invoicenumber3') != null) ?'&invoicenumber3='+EncodingUtil.urlEncode(ipnparam.get('invoicenumber3'), 'UTF-8'): '');
                
            }
            else if(ipnparam.get('type') == 'payment_cancelled'){
                
                reqBody = ((ipnparam.get('key') != null) ?'key='+EncodingUtil.urlEncode(ipnparam.get('key'), 'UTF-8'): '')+
                    ((ipnparam.get('c_date') != null) ?'&c_date='+EncodingUtil.urlEncode(ipnparam.get('c_date'), 'UTF-8'): '')+
                    ((ipnparam.get('type') != null) ?'&type='+EncodingUtil.urlEncode(ipnparam.get('type'), 'UTF-8'): '')+
                    ((ipnparam.get('ref_code') != null) ?'&ref_code='+EncodingUtil.urlEncode(ipnparam.get('ref_code'), 'UTF-8'): '')+
                    ((ipnparam.get('payee_id') != null) ?'&payee_id='+EncodingUtil.urlEncode(ipnparam.get('payee_id'), 'UTF-8'): '')+
                    ((ipnparam.get('cancelled_date') != null) ?'&cancelled_date='+EncodingUtil.urlEncode(ipnparam.get('cancelled_date'), 'UTF-8'): '')+
                    ((ipnparam.get('amount_submitted') != null) ?'&amount_submitted='+EncodingUtil.urlEncode(ipnparam.get('amount_submitted'), 'UTF-8'): '')+
                    ((ipnparam.get('currency_submitted') != null) ?'&currency_submitted='+EncodingUtil.urlEncode(ipnparam.get('currency_submitted'), 'UTF-8'): '')+
                    ((ipnparam.get('invoicenumber') != null) ?'&invoicenumber='+EncodingUtil.urlEncode(ipnparam.get('invoicenumber'), 'UTF-8'): '')+
                    ((ipnparam.get('invoicenumber2') != null) ?'&invoicenumber2='+EncodingUtil.urlEncode(ipnparam.get('invoicenumber2'), 'UTF-8'): '')+
                    ((ipnparam.get('invoicenumber3') != null) ?'&invoicenumber3='+EncodingUtil.urlEncode(ipnparam.get('invoicenumber3'), 'UTF-8'): '');
            }
            else if(ipnparam.get('type') == 'completed'){
                
                reqBody = ((ipnparam.get('key') != null) ?'key='+EncodingUtil.urlEncode(ipnparam.get('key'), 'UTF-8'): '')+
                    ((ipnparam.get('c_date') != null) ?'&c_date='+EncodingUtil.urlEncode(ipnparam.get('c_date'), 'UTF-8'): '')+
                    ((ipnparam.get('completed') != null) ?'&completed='+EncodingUtil.urlEncode(ipnparam.get('completed'), 'UTF-8'): '')+
                    ((ipnparam.get('type') != null) ?'&type='+EncodingUtil.urlEncode(ipnparam.get('type'), 'UTF-8'): '')+
                    ((ipnparam.get('ref_code') != null) ?'&ref_code='+EncodingUtil.urlEncode(ipnparam.get('ref_code'), 'UTF-8'): '')+
                    ((ipnparam.get('seq_ref_code') != null) ?'&seq_ref_code='+EncodingUtil.urlEncode(ipnparam.get('seq_ref_code'), 'UTF-8'): '')+
                    ((ipnparam.get('payee_id') != null) ?'&payee_id='+EncodingUtil.urlEncode(ipnparam.get('payee_id'), 'UTF-8'): '')+
                    ((ipnparam.get('submit_date') != null) ?'&submit_date='+EncodingUtil.urlEncode(ipnparam.get('submit_date'), 'UTF-8'): '')+
                    ((ipnparam.get('amount_submitted') != null) ?'&amount_submitted='+EncodingUtil.urlEncode(ipnparam.get('amount_submitted'), 'UTF-8'): '')+
                    ((ipnparam.get('currency_submitted') != null) ?'&currency_submitted='+EncodingUtil.urlEncode(ipnparam.get('currency_submitted'), 'UTF-8'): '')+
                    ((ipnparam.get('payment_method') != null) ?'&payment_method='+EncodingUtil.urlEncode(ipnparam.get('payment_method'), 'UTF-8'): '')+
                    ((ipnparam.get('payer_fees') != null) ?'&payer_fees='+EncodingUtil.urlEncode(ipnparam.get('payer_fees'), 'UTF-8'): '')+
                    ((ipnparam.get('payee_fees') != null) ?'&payee_fees='+EncodingUtil.urlEncode(ipnparam.get('payee_fees'), 'UTF-8'): '')+
                    ((ipnparam.get('provider') != null) ?'&provider='+EncodingUtil.urlEncode(ipnparam.get('provider'), 'UTF-8'): '')+
                    ((ipnparam.get('account_identifier') != null) ?'&account_identifier='+EncodingUtil.urlEncode(ipnparam.get('account_identifier'), 'UTF-8'): '')+
                    ((ipnparam.get('payer_exchange_rate') != null) ?'&payer_exchange_rate='+EncodingUtil.urlEncode(ipnparam.get('payer_exchange_rate'), 'UTF-8'): '')+
                    ((ipnparam.get('submitted_currency_to_payee_currency_fx_rate') != null) ? '&submitted_currency_to_payee_currency_fx_rate='+EncodingUtil.urlEncode(ipnparam.get('submitted_currency_to_payee_currency_fx_rate'), 'UTF-8'): '')+
                    ((ipnparam.get('payment_amount_in_withdraw_currency') != null) ?'&payment_amount_in_withdraw_currency='+EncodingUtil.urlEncode(ipnparam.get('payment_amount_in_withdraw_currency'), 'UTF-8'): '')+
                    ((ipnparam.get('payment_amount') != null) ?'&payment_amount='+EncodingUtil.urlEncode(ipnparam.get('payment_amount'), 'UTF-8'): '')+
                    ((ipnparam.get('payment_currency') != null) ?'&payment_currency='+EncodingUtil.urlEncode(ipnparam.get('payment_currency'), 'UTF-8'): '')+
                    ((ipnparam.get('value_date') != null) ?'&value_date='+EncodingUtil.urlEncode(ipnparam.get('value_date'), 'UTF-8'): '')+
                    ((ipnparam.get('transaction_ref') != null) ?'&transaction_ref='+EncodingUtil.urlEncode(ipnparam.get('transaction_ref'), 'UTF-8'): '')+
                    ((ipnparam.get('payment_method_token') != null) ?'&payment_method_token='+EncodingUtil.urlEncode(ipnparam.get('payment_method_token'), 'UTF-8'): '')+
                    ((ipnparam.get('bank_beneficiary_name') != null) ?'&bank_beneficiary_name='+EncodingUtil.urlEncode(ipnparam.get('bank_beneficiary_name'), 'UTF-8'): '')+
                    ((ipnparam.get('iban') != null) ?'&iban='+EncodingUtil.urlEncode(ipnparam.get('iban'), 'UTF-8'): '')+
                    ((ipnparam.get('swift') != null) ?'&swift='+EncodingUtil.urlEncode(ipnparam.get('swift'), 'UTF-8'): '')+
                    ((ipnparam.get('bank_code') != null) ?'&bank_code='+EncodingUtil.urlEncode(ipnparam.get('bank_code'), 'UTF-8'): '')+
                    ((ipnparam.get('branch_code') != null) ?'&branch_code='+EncodingUtil.urlEncode(ipnparam.get('branch_code'), 'UTF-8'): '')+
                    ((ipnparam.get('bank_name') != null) ?'&bank_name='+EncodingUtil.urlEncode(ipnparam.get('bank_name'), 'UTF-8'): '')+
                    ((ipnparam.get('bank_branch_name') != null) ?'&bank_branch_name='+EncodingUtil.urlEncode(ipnparam.get('bank_branch_name'), 'UTF-8'): '')+
                    ((ipnparam.get('bank_address') != null) ?'&bank_address='+EncodingUtil.urlEncode(ipnparam.get('bank_address'), 'UTF-8'): '')+
                    ((ipnparam.get('bank_address2') != null) ?'&bank_address2='+EncodingUtil.urlEncode(ipnparam.get('bank_address2'), 'UTF-8'): '')+
                    ((ipnparam.get('bank_city') != null) ?'&bank_city='+EncodingUtil.urlEncode(ipnparam.get('bank_city'), 'UTF-8'): '')+
                    ((ipnparam.get('routing_number') != null) ?'&routing_number='+EncodingUtil.urlEncode(ipnparam.get('routing_number'), 'UTF-8'): '')+
					((ipnparam.get('account_number') != null) ?'&account_number='+EncodingUtil.urlEncode(ipnparam.get('account_number'), 'UTF-8'): '')+
                    ((ipnparam.get('ach_account_type') != null) ?'&ach_account_type='+EncodingUtil.urlEncode(ipnparam.get('ach_account_type'), 'UTF-8'): '')+
                    ((ipnparam.get('intern_swift') != null) ?'&intern_swift='+EncodingUtil.urlEncode(ipnparam.get('intern_swift'), 'UTF-8'): '')+
                    ((ipnparam.get('intern_bank_name') != null) ?'&intern_bank_name='+EncodingUtil.urlEncode(ipnparam.get('intern_bank_name'), 'UTF-8'): '')+
                    ((ipnparam.get('intern_account_number') != null) ?'&intern_account_number='+EncodingUtil.urlEncode(ipnparam.get('intern_account_number'), 'UTF-8'): '')+
                    ((ipnparam.get('beneficiary_id') != null) ?'&beneficiary_id='+EncodingUtil.urlEncode(ipnparam.get('beneficiary_id'), 'UTF-8'): '')+
					((ipnparam.get('tax_id') != null) ?'&tax_id='+EncodingUtil.urlEncode(ipnparam.get('tax_id'), 'UTF-8'): '')+
                    ((ipnparam.get('withdraw_amount') != null) ?'&withdraw_amount='+EncodingUtil.urlEncode(ipnparam.get('withdraw_amount'), 'UTF-8'): '')+
                    ((ipnparam.get('withdraw_currency') != null) ?'&withdraw_currency='+EncodingUtil.urlEncode(ipnparam.get('withdraw_currency'), 'UTF-8'): '')+
                    ((ipnparam.get('invoicenumber') != null) ?'&invoicenumber='+EncodingUtil.urlEncode(ipnparam.get('invoicenumber'), 'UTF-8'): '')+
                    ((ipnparam.get('invoicenumber2') != null) ?'&invoicenumber2='+EncodingUtil.urlEncode(ipnparam.get('invoicenumber2'), 'UTF-8'): '')+
                    ((ipnparam.get('invoicenumber3') != null) ?'&invoicenumber3='+EncodingUtil.urlEncode(ipnparam.get('invoicenumber3'), 'UTF-8'): '')+
                    ((ipnparam.get('transaction_date') != null) ?'&transaction_date='+EncodingUtil.urlEncode(ipnparam.get('transaction_date'), 'UTF-8'): '');
                
            }
                        
            
            
            System.debug('String Created reqBody');
            
                        
            
            Http P=new Http();
            HttpRequest httpReq = new HttpRequest();
            
            
            String Replacedencodedrequest = reqBody.replace('%26', '&').replace('%3D','=').replace('%2F','%2f').replace('%3A','%3a').replace('%2B','%2b').replace('%2C','%2c').replace('%3B','%3b');
            
            System.debug('Replaced and ncoded reqBody ==>');
            System.debug(Replacedencodedrequest);
            
            // httpReq.setEndpoint('https://console.sandbox.tipalti.com/notif/ipn.aspx'); //Sandbox
            httpReq.setEndpoint('https://console.tipalti.com/notif/ipn.aspx'); //Production
            httpReq.setMethod('POST');
            httpReq.setHeader('Content-Type','application/x-www-form-urlencoded');//charset=utf-8
            httpReq.setHeader('Content-Length', string.valueOf(Replacedencodedrequest.length()));
            httpReq.setBody(Replacedencodedrequest);
            
            System.debug('reqBody ==>');
            System.debug(Replacedencodedrequest);
            
            
            
            HttpResponse response = P.send(httpReq);
            system.debug('status code is >>>>'+ response.getStatusCode() + ' ' + response.getStatus());
            system.debug('status code is >>>>'+ response.getStatusCode() + ' ' + response.getStatus());
            System.debug('Body is >>>>'+response.getBody());
            System.debug('IPn is Test >> '+ipnparam.get('invoicenumber'));
            
            
            if (response.getStatus()=='OK' && response.getBody() == 'VERIFIED') {
                
                //String invoiceLike = Test.isRunningTest() ? 'a681v000000LRhOAAW' : '%'+ipnparam.get('invoicenumber')+'%';
                //System.debug('IPn is invoiceLike >> '+invoiceLike);
                String invoiceList = ipnparam.get('invoicenumber');
                
                if(ipnparam.get('invoicenumber2') != null){
                    invoiceList += ';'+ipnparam.get('invoicenumber2');
                }
                 if(ipnparam.get('invoicenumber3') != null){
                    invoiceList += ';'+ipnparam.get('invoicenumber3');
                }
                
                //'a681v000000LRhOAAW;a681v000000LRhOAAW;a681v000000LRhOAAW';
                //List<String> lstAlpha = invoiceList.split(';');
                List<Tipalti_Payment_History__c> TPHList = new List<Tipalti_Payment_History__c>();
                //System.debug(lstAlpha);
                /*          List<String> invoices = new List<String>();
for(String invoice: invoiceList.split(';')){
invoices.add(invoice);
}
*/       
                if(ipnparam.get('type') == 'payee_details_changed'){
                    
                    Tipalti_Payment_Submission__c tpsObj = new Tipalti_Payment_Submission__c(
                        Account__c = ipnparam.get('payee_id'),
                        Idap__c = ipnparam.get('payee_id'),
                        Updated_payee_details__c = reqBody
                        
                    );
                    
                    insert tpsObj;
                    
                }
                if(invoiceList != null){
                    
                    
                    
                    Tipalti_Payment_Submission__c tps = [SELECT Id FROM Tipalti_Payment_Submission__c where Invoiced_Ids__c = :invoiceList  limit 1];
                    Set<Id> invoiceIds = new Set<Id>();
                    Map<Id,String> bankRefMap = new Map<Id,String>();
                    for(String invoice: invoiceList.split(';')){
                        String invoiceLike = Test.isRunningTest() ? 'a681v000000kWvaAAE' : '%'+invoice+'%';
                        //Tipalti_Payment_Submission__c tps = [SELECT Id FROM Tipalti_Payment_Submission__c where Reference_Number__c = :ipnparam.get('ref_code') limit 1];
                        if(ipnparam.get('type') == 'completed')
                        {   
                            invoiceIds.add(invoice);
							bankRefMap.put(invoice,ipnparam.get('payment_method'));
                        }
                        System.debug('TPS ID >> '+tps.Id);
                        Tipalti_Payment_History__c tph = new Tipalti_Payment_History__c(
                            account_identifier__c = ipnparam.get('account_identifier'),account_number__c  = ipnparam.get('account_number'),
                            ach_account_type__c  = ipnparam.get('ach_account_type'),amount_submitted__c  = (ipnparam.get('amount_submitted') != null) ?Decimal.valueOf(ipnparam.get('amount_submitted').replace(',','')) : 0,
                            ap_account_name__c  = ipnparam.get('ap_account_name'),ap_account_number__c  = ipnparam.get('ap_account_number'),
                            bank_address__c  = ipnparam.get('bank_address'),bank_address2__c  = ipnparam.get('bank_address2'),
                            bank_beneficiary_name__c  = ipnparam.get('bank_beneficiary_name'),bank_branch_code__c  = ipnparam.get('bank_branch_code'),
                            bank_branch_name__c = ipnparam.get('bank_branch_name'),bank_city__c = ipnparam.get('bank_city'),
                            bank_code__c = ipnparam.get('bank_code'),bank_name__c = ipnparam.get('bank_name'),
                            bank_zip_code__c = ipnparam.get('bank_zip_code'),beneficiary_id__c  = ipnparam.get('beneficiary_id'),
                            bill_refcode__c  = ipnparam.get('bill_refcode'),c_date__c = (ipnparam.get('c_date') != null ) ? convertToDateTime(ipnparam.get('c_date')) : null,
                            cancelled_date__c = (ipnparam.get('cancelled_date') != null ) ? convertToDateTime(ipnparam.get('cancelled_date')):null,check_zip_code__c  = ipnparam.get('check_zip_code'),
                            check_address__c  = ipnparam.get('check_address'),check_address2__c = ipnparam.get('check_address2'),
                            check_city__c = ipnparam.get('check_city'),check_state__c  = ipnparam.get('check_state'),
                            currency_submitted__c = ipnparam.get('currency_submitted'),deferred_date__c = (ipnparam.get('deferred_date') != null ) ?convertToDateTime(ipnparam.get('deferred_date')):null,
                            deferred_reasons__c  = ipnparam.get('deferred_reasons'),early_payment_fee__c =  (ipnparam.get('early_payment_fee') != null) ? Decimal.valueOf(ipnparam.get('early_payment_fee').replace(',','')) : 0,
                            error__c  = ipnparam.get('error'),error_code__c = ipnparam.get('error_code'),
                            error_date__c =(ipnparam.get('error_date') != null ) ?   convertToDateTime(ipnparam.get('error_date')):null,estimated_provider_fees__c = (ipnparam.get('estimated_provider_fees') != null) ? Decimal.valueOf(ipnparam.get('estimated_provider_fees').replace(',','')): 0,
                            ewallet_account__c = ipnparam.get('ewallet_account'),iban__c = ipnparam.get('iban'),
                            income_code__c = ipnparam.get('income_code'),income_type__c = ipnparam.get('income_type'),
                            Interm_account_number__c = ipnparam.get('Interm_account_number'),Interm_bank_name__c = ipnparam.get('Interm_bank_name'),
                            Interm_swift__c = ipnparam.get('Interm_swift'),invoicenumber__c = invoice,//invoicenumber__c = ipnparam.get('invoicenumber'),//invoicenumber__c = invoice,
                            is_finalized__c = (ipnparam.get('is_finalized')!=null) ? Boolean.valueOf(ipnparam.get('is_finalized')) :false,is_test__c = (ipnparam.get('is_test') != null )? Boolean.valueOf(ipnparam.get('is_test')): false,
                            Key__c = ipnparam.get('key'),lifting_fees__c = (ipnparam.get('lifting_fees') != null) ? Decimal.valueOf(ipnparam.get('lifting_fees').replace(',','')) : 0,
                            name_on_check__c = ipnparam.get('name_on_check'),paid_amount__c = (ipnparam.get('paid_amount') != null) ? Decimal.valueOf(ipnparam.get('paid_amount').replace(',','')):0,
                            payee_fees__c = (ipnparam.get('payee_fees') != null) ? Decimal.valueOf(ipnparam.get('payee_fees')):0,payee_fees_currency__c = ipnparam.get('payee_fees_currency'),
                            payee_fx_fee_currency__c = ipnparam.get('payee_fx_fee_currency'),payee_fx_fees__c  = (ipnparam.get('payee_fx_fees') != null) ? Decimal.valueOf(ipnparam.get('payee_fx_fees').replace(',','')):0,
                            payee_id__c = ipnparam.get('payee_id'),payer_exchange_rate__c = (ipnparam.get('payer_exchange_rate') != null) ? Decimal.valueOf(ipnparam.get('payer_exchange_rate').replace(',','')):0,
                            payer_fees__c = (ipnparam.get('payer_fees') != null) ? Decimal.valueOf(ipnparam.get('payer_fees')) : 0,payer_fx_fee_currency__c = ipnparam.get('payer_fx_fee_currency'),
                            payer_fx_fees__c = (ipnparam.get('payer_fx_fees') != null) ? Decimal.valueOf(ipnparam.get('payer_fx_fees').replace(',','')):0,payment_amount__c = (ipnparam.get('payment_amount') != null) ? Decimal.valueOf(ipnparam.get('payment_amount').replace(',','')):0,
                            payment_amount_in_withdraw_currency__c = (ipnparam.get('payment_amount_in_withdraw_currency') != null) ?Decimal.valueOf(ipnparam.get('payment_amount_in_withdraw_currency').replace(',','')):0,payment_currency__c = ipnparam.get('payment_currency'),
                            payment_method__c = ipnparam.get('payment_method'),payment_method_token__c = ipnparam.get('payment_method_token'),
                            payment_type__c = ipnparam.get('payment_type'),preferred_payer_entity__c = ipnparam.get('preferred_payer_entity'),
                            provider__c = ipnparam.get('provider'),reason__c = ipnparam.get('reason'),
                            reason_code__c = ipnparam.get('reason_code'),ref_code__c = ipnparam.get('ref_code'),
                            related_bills__c = ipnparam.get('related_bills'),related_invoice_ref_code__c = ipnparam.get('related_invoice_ref_code'),
                            routing_number__c = ipnparam.get('routing_number'),seq_ref_code__c = ipnparam.get('seq_ref_code'),
                            submit_date__c = (ipnparam.get('submit_date') != null ) ?  convertToDateTime(ipnparam.get('submit_date')): null,submitted_currency_to_payee_currency_fx__c = (ipnparam.get('submitted_currency_to_payee_currency_fx') != null) ? Decimal.valueOf(ipnparam.get('submitted_currency_to_payee_currency_fx').replace(',','')):0,
                            Supplier_Invoice__c = invoice,//Supplier_Invoice__c = ipnparam.get('invoicenumber'),// 
                            swift__c = ipnparam.get('swift'),
                            tax_id__c = ipnparam.get('tax_id'),transaction_date__c = (ipnparam.get('transaction_date') != null ) ?  convertToDateTime(ipnparam.get('transaction_date')) : null,
                            transaction_ref__c = ipnparam.get('transaction_ref'),treaty_country__c = ipnparam.get('treaty_country'),
                            Type__c = ipnparam.get('type'),value_date__c = (ipnparam.get('value_date') != null ) ?  convertToDateTime(ipnparam.get('value_date')) : null,
                            withdraw_amount__c = (ipnparam.get('withdraw_amount') != null) ? Decimal.valueOf(ipnparam.get('withdraw_amount').replace(',','')):0,withdraw_currency__c = ipnparam.get('withdraw_currency'),
                            withholding_amount__c = (ipnparam.get('withholding_amount') != null) ? Decimal.valueOf(ipnparam.get('withholding_amount').replace(',','')):0,withholding_amount_USD__c = (ipnparam.get('withholding_amount_USD') != null) ? Decimal.valueOf(ipnparam.get('withholding_amount_USD').replace(',','')) : 0,
                            withholding_rate__c = ipnparam.get('withholding_rate'), Tipalti_Payment_Submission__c = tps.Id    
                        );
                        TPHList.add(tph);
                        
                    }
                    insert TPHList;
                    
                    
                    if(invoiceIds != null && !invoiceIds.isEmpty()){
                        List<Invoice_New__c> invoiceTOUpdate = new List<Invoice_New__c>();
                        for(Id invoiceId: invoiceIds){
                            Invoice_New__c  invoice = new Invoice_New__c(
                                Id = invoiceId,
                                Invoice_Status__c  = 'Paid by Tipalti',
                                Paid_Date_Tipalti__c = system.today(),
                                Bank_reference__c = bankRefMap.get(invoiceId)
                            );
                            invoiceTOUpdate.add(invoice);
                        }
                        update invoiceTOUpdate;
                        
                    }
                    
                    
                }
                //API_Log__c apiLog1 = new API_Log__c (Response__c =response.getBody() , Calling_Method_Name__c  = 'IPN success Status');
                // insert apiLog1;
            }else{
                API_Log__c apiLog1 = new API_Log__c (Response__c = response.getBody(), Calling_Method_Name__c  = 'IPN failure Status');
                insert apiLog1;
            }
            
            /* API_Log__c apiLog1 = new API_Log__c (Response__c = Rkey+'/'+Rc_date+'/'+Rtype+'/'+Ris_test, Calling_Method_Name__c  = 'https://account2-testxyz.cs129.force.com/services/apexrest/GetIPNStatus');
API_Log__c apiLog2 = new API_Log__c (Response__c = 'ipnparam-->'+ipnparam, Calling_Method_Name__c  = 'https://account2-testxyz.cs129.force.com/services/apexrest/GetIPNStatus');
API_Log__c apiLog3 = new API_Log__c (Response__c = 'fullbody Msg-->'+fullbody, Calling_Method_Name__c  = 'https://account2-testxyz.cs129.force.com/services/apexrest/GetIPNStatus');
API_Log__c apiLog4 = new API_Log__c (Response__c = 'FullURL Msg--->'+FullURL, Calling_Method_Name__c  = 'https://account2-testxyz.cs129.force.com/services/apexrest/GetIPNStatus');

insert apiLog1;
insert apiLog2;
insert apiLog3;
insert apiLog4;*/
            
        }
        catch(Exception e){
            String ErrorString =e.getMessage();
            
            API_Log__c apiLog = new API_Log__c (Response__c = ErrorString, Calling_Method_Name__c  = 'https://account2-testxyz.cs129.force.com/services/apexrest/GetIPNStatus');
            
            insert apiLog;
            System.debug('IPN Exception Msg--->'+ErrorString);
            
        }
        
        
    }
    
    public static DateTime convertToDateTime(String date1){
        String[]  part = date1.split('\\ ');
        String[] date3 = part[0].split('\\/');
        String[] date4 = part[1].split('\\:');
        String stringDate = date3[2] + '-' + date3[0]
            + '-' + date3[1] + ' ' + date4[0] + ':' +
            date4[1] + ':' + date4[2].split('\\.')[0];
        DateTime myDate = DateTime.valueOf(stringDate);
        return myDate;
    }
    
    /* public  static String generateHmacSHA256Signature(String Keyvalue,string ClientsecreteValue)
// public  static String generateHmacSHA256Signature(String Keyvalue)
{    
String algorithmName = 'HmacSHA256';
Blob hmacData = Crypto.generateMac(algorithmName,  Blob.valueOf(KeyValue),Blob.valueOf(ClientsecreteValue));
String hextext = EncodingUtil.convertToHex(hmacData);

return hextext;
}*/
}