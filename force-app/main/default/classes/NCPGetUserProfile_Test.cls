@isTest
public class NCPGetUserProfile_Test {
	
    public static testMethod void NCPGetCourierServices_Test1(){
        
        //Test Data
        //Test Data
        Access_token__c at = new Access_token__c(status__c = 'Active', Access_Token__c = '123');
        Insert at;
        
        Contact con = new Contact(LastName='TestCon');
        Insert con;
        
		NCPGetUserProfileWra obj = new NCPGetUserProfileWra();
		obj.Accesstoken = at.Access_Token__c;
		obj.ContactID = con.Id;        
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
        req.requestURI = '/services/apexrest/GetUserProfile'; 
        req.httpMethod = 'Post';
        req.addHeader('Content-Type', 'application/json');
        req.requestBody = Blob.valueof(JSON.serialize(obj));
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
        	NCPGetUserProfile.NCPGetCourierServices();
        Test.stopTest(); 
    }

    public static testMethod void NCPGetCourierServices_Test2(){
        
        //Test Data
        //Test Data
        Access_token__c at = new Access_token__c(status__c = 'Active', Access_Token__c = '123');
        Insert at;
        
        Contact con = new Contact(LastName='TestCon');
        Insert con;
        
		NCPGetUserProfileWra obj = new NCPGetUserProfileWra();
		obj.Accesstoken = at.Access_Token__c;
		obj.ContactID = con.Id;        
        
     
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
        req.requestURI = '/services/apexrest/GetUserProfile'; 
        req.httpMethod = 'Post';
        req.addHeader('Content-Type', 'application/json');
        req.requestBody = Blob.valueof(JSON.serialize(obj));
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
        	NCPGetUserProfile.NCPGetCourierServices();
        Test.stopTest(); 
    }
    
    //Covering catch exception
    public static testMethod void NCPGetCourierServices_Test3(){
        
        //Test Data
        Access_token__c at = new Access_token__c(status__c = 'Active', Access_Token__c = '123');
        insert at;
        
        //Json Body
        String json = '{"Accesstoken": "123"}';
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
        req.requestURI = '/services/apexrest/GetUserProfile'; 
        req.httpMethod = 'Post';
        req.addHeader('Content-Type', 'application/json');
        req.requestBody = Blob.valueof(json);
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
        NCPGetUserProfile.NCPGetCourierServices();
        Test.stopTest(); 
    }

    
    

}