@isTest
public class AccountTriggerTest {
    
    @isTest
    public static void testMasterTaskCreation(){
        User userObj = [Select Id, Name, userName, Profile.Name, UserRole.name From user where Profile.Name = 'System Administrator' LIMIT 1];
        Master_Task__c masterTask = new Master_Task__c(Name = 'Demo Task', States__c = 'TecEx Pending;Resolved',
                                                       Operational__c = true, Condition__c = '({0} == FALSE)',
                                                       Condition_Fields_API_Name__c = 'Account.Pre_onboarding_complete__c',
                                                       Assess_Condition_on_Creation_of__c = 'Account',Ship_To_Country__c = 'All');
        Insert masterTask;
        
        insert new Account (name='Acme1', Type ='Client',
                                                     CSE_IOR__c = userObj.Id,
                                                     Service_Manager__c = userObj.Id,
                                                     Financial_Manager__c= userObj.Id,
                                                     Financial_Controller__c= userObj.Id,
                                                     Tax_recovery_client__c  = FALSE,
                                                     Ship_From_Line_1__c = 'Ship_From_Line_1',
                                                     Ship_From_Line_2__c = 'Ship_From_Line_2',
                                                     Ship_From_Line_3__c = 'Ship_From_Line_3',
                                                     Ship_From_City__c = 'Ship_From_City',
                                                     Ship_From_Zip__c = 'Ship_From_Zip',
                                                     Ship_From_Country__c = 'Ship_From_Country',
                                                     Ship_From_Other_notes__c = 'Ship_From_Other',
                                                     Ship_From_Contact_Name__c = 'Ship_From_Contact',
                                                     Ship_From_Contact_Email__c = 'Ship_From_Email',
                                                     Ship_From_Contact_Tel__c = 'Ship_From_Tel',
                                                     Client_Address_Line_1__c = 'Client_Address_Line_1',
                                                     Client_Address_Line_2__c = 'Client_Address_Line_2',
                                                     Client_Address_Line_3__c = 'Client_Address_Line_3',
                                                     Client_City__c = 'Client_City',
                                                     Client_Zip__c = 'Client_Zip',
                                                     Client_Country__c = 'Client_Country',
                                                     Other_notes__c = 'Other_notes',
                                                     Tax_Name__c = 'Tax_Name',
                                                     Tax_ID__c = 'Tax_ID',
                                                     Contact_Name__c = 'Contact_Name',
                                                     Contact_Email__c = 'Contact_Email',
                                                     Contact_Tel__c = 'Contact_Tel'
                                                    );
    }
}