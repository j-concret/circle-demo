@isTest
private class RagStatusTest {

public static String CRON_EXP = '0 0 0 15 3 ? 2022';

static testMethod void testMethod1() {
    
        
        Account testClient = new account(Name='TestingClient',Client_Status__c = 'Prospect',Estimated_Vat__c = 123,CSE_IOR__c='0050Y000001LTZO');

        insert testClient;

        Contact testcontact = new Contact(LastName='Maseli',AccountId=testClient.Id);

        insert testcontact;
        
        country_price_approval__c cpa = new country_price_approval__c( name ='Serbia', Billing_term__c = 'DAP/CIF - IOR pays',  Airwaybill_Instructions__c = 'User IOR Address');
        insert cpa;
        
        IOR_Price_List__c testIOR = new IOR_Price_List__c();

        testIOR.Client_Name__c = testClient.Id;
        testIOR.Name = 'Serbia';
        testIOR.IOR_Fee__c = 1;
        
        insert testIOR;

        Shipment_Order__c shipmentOrder = new Shipment_Order__c();
        shipmentOrder.Account__c = testClient.Id;
        shipmentOrder.IOR_Price_List__c = testIOR.Id;
        shipmentOrder.Client_Contact_for_this_Shipment__c = testcontact.Id;
        shipmentOrder.Shipment_Value_USD__c = 123;
        shipmentOrder.Who_arranges_International_courier__c ='Client'; 
        shipmentOrder.Tax_Treatment__c ='DAP/CIF - IOR pays';
        shipmentOrder.Ship_to_Country__c = cpa.Id;
        shipmentOrder.Update_Rag_Status_Time__c = TRUE;
        shipmentOrder.Shipping_Status__c = 'Client Approved to ship';
        insert shipmentOrder;

        
        
        Test.startTest();
        
        String query = 'select id, Update_Rag_Status_Time__c from Shipment_Order__c  where Shipping_Status__c != \'POD Received\' AND Shipping_Status__c != \'On Hold\'AND Shipping_Status__c != \'Cancelled\'AND Shipping_Status__c != \'Shipment Pending\' AND Shipping_Status__c != \'Cancelled - With fees/costs\' AND Shipping_Status__c != \'Cancelled\'AND Shipping_Status__c != \'Cancelled - No fees/costs\' AND Shipping_Status__c != \'Customs Clearance Docs Received\' AND Account__c = \'0012600000GCKSz\'';
    

        
        // Schedule the test job
        String jobId = System.schedule('ScheduleApexClassTest',
            CRON_EXP, 
            new ragStatusSchedule());
        // Get the information from the CronTrigger API object
        CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, 
        NextFireTime
        FROM CronTrigger WHERE id = :jobId];

          // Verify the expressions are the same
          System.assertEquals(CRON_EXP, 
            ct.CronExpression);

          // Verify the job has not run
          System.assertEquals(0, ct.TimesTriggered);

          // Verify the next time the job will run
          System.assertEquals('2022-03-15 00:00:00', 
            String.valueOf(ct.NextFireTime));

        ragStatusUpdate batch = new ragStatusUpdate(); 
        database.executebatch(batch);
       
          
          

          Test.stopTest();
      }

    }