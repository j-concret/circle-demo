@RestResource(urlMapping='/NCPInvoiceDetails/*')
Global class NCPInvoiceDetails {
    
     @Httppost
    global static void logEmailStatementRequest(){
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        res.addHeader('Content-Type', 'application/json');
        Blob body = req.requestBody;
        String requestString = body.toString();
        RequestWrapper reqBody;
     	
          try {
        reqBody = (RequestWrapper)JSON.deserialize(requestString,RequestWrapper.class);
            List<Access_token__c> at = [SELECT status__c,Access_Token__c FROM Access_token__c WHERE Access_Token__c =: reqBody.Accesstoken and status__c = 'Active' LIMIT 1];

            if(!at.isEmpty()) {
                
                  List<INvoice_New__c> Inv = [SELECT Id,Stripe_Link__C,Invoice_Status__c,Invoice_amount_USD__c,Collection_Administration_Fee__c ,Total_Value_Applied__c,Total_Invoice_Amount_Formula__c,IOR_Fees_USD__c,Admin_Fees_USD__c,Taxes_and_Duties_USD__c,Customs_Brokerage_Fees_USD__c,Customs_Clearance_Fees_USD__c,Customs_Handling_Fees_USD__c,Customs_License_Fees_USD__c,EOR_Fees_USD__c,International_Freight_Fee_USD__c,Bank_Fees_USD__c,Cash_Outlay_Fee_USD__c,Miscellaneous_Fee_Name__c,Miscellaneous_Fee_USD__c,Recharge_Tax_and_Duty_Other_USD__c,Liability_Cover_Fee_USD__c,Amount_Outstanding__c,Due_Date__c  FROM INvoice_New__c WHERE ID =: reqBody.InvoiceID LIMIT 1];
 				
                res.responseBody = Blob.valueOf(JSON.serializePretty(Inv));
                res.addHeader('Content-Type', 'application/json');
                res.statusCode = 200;
                
                API_Log__c Al = New API_Log__c();
                Al.Account__c=reqBody.AccountID;
                Al.EndpointURLName__c='NCPInvoiceDetails';
                Al.Response__c='Success - InvoiceDetails are sent';
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al; 
                
                
                
            }
          }
        
        catch(Exception e){
            
             String jsonData = Util.getResponseString('Error','Something went wrong while logging email account statement request, please contact support_SF@tecex.com');
            res.responseBody = Blob.valueOf(jsonData);
            res.statusCode = 500;
            insert new API_Log__c(
                Account__c = (reqBody != null && String.isNotBlank(reqBody.AccountID)) ? reqBody.AccountID : null,
                EndpointURLName__c = 'NCPInvoiceDetails',
                Response__c = 'Error - NCPInvoiceDetails :'+e.getMessage(),
                StatusCode__c = String.valueof(res.statusCode)
                );
            
           }
    
    }
    
    public class RequestWrapper {
        public String Accesstoken;
        public String AccountID;
        public String InvoiceID;
       
    }
}