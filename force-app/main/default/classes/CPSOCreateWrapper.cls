public class CPSOCreateWrapper {

	public String AccountID;
    public String ContactID;
    //public Integer estimatedChargableweight;
    public double estimatedChargableweight;
    
    public String ServiceType;
	public String Courier_responsibility;
	public String Reference1;
	public String Reference2;
	public String ShipFrom;
	public String ShipTo;
	//public Integer ShipmentvalueinUSD;
	public double ShipmentvalueinUSD;
	public String PONumber;
	public String Type_of_Goods;
	public String Li_ion_Batteries;
	public String Li_ion_BatteryTypes;
	public String FRContact_Full_Name;
	public String FRContact_Email;
	public String FRContact_Phone_Number;
	public String FRAddress1;
	public String FRAddress2;
	public String FRCity;
	public String FRProvince;
	public String FRPostal_Code;
	public String FRAll_Countries;
	public List<ShipmentOrder_Packages> ShipmentOrder_Packages;
	public List<Parts> Parts;
	public List<FinalDelivieries> FinalDelivieries;

	public class Parts {
		public String PartNumber;
		public String PartDescription;
		public Integer Quantity;
		public Double UnitPrice;
		public String HSCode;
		public String CountryOfOrigin;
		public String ECCNNo;
        public String Type_of_Goods;
		public String Li_ion_Batteries;
		public String Li_ion_BatteryTypes;
	}

	public class FinalDelivieries {
        public String AddressID;
		public String Name;
		public String Contact_Full_Name;
		public String Contact_Email;
		public String Contact_Phone_Number;
		public String Address1;
		public String Address2;
		public String City;
		public String Province;
		public String Postal_Code;
		public String All_Countries;
		//public String ClientID;
	}
    
   
	public class ShipmentOrder_Packages {
		public String Weight_Unit;
		public String Dimension_Unit;
		public Integer Packages_of_Same_Weight;
		public Double Length;
		public Double Height;
		public Double Breadth;
		public Double Actual_Weight;
	}

	
	public static CPSOCreateWrapper parse(String json) {
		return (CPSOCreateWrapper) System.JSON.deserialize(json, CPSOCreateWrapper.class);
	}
}