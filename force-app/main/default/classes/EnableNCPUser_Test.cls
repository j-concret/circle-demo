@IsTest
public class EnableNCPUser_Test {
    
    @testSetup
    static void setup(){
         UserRole r = new UserRole(DeveloperName = 'MyCustomRole', Name = 'My Role');
        insert r;
        
        User usr = new User(
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id,
            LastName = 'Berkeley',
            Email = 'testingUser@asdf.com',
            Username = 'testingUser@asdf.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            FirstName='Vat',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            UserRoleId = r.Id
        );
        insert usr;
                
        
        System.runAs(usr) {
        Account account = new Account (name='Test Supplier', Type ='Supplier', ICE__c = '0050Y000001km5c', RecordTypeId = '0120Y0000006KjQQAU');
        insert account;        
        Contact contact = new Contact ( firstName='Testing',lastname ='Individual',  AccountId = account.Id, Email = 'abc01@Tecex.com');
        insert contact;  
        }
    }
    static testMethod void  setUpData1(){
       
             User usr = [SELECT Id FROM User where Email ='testingUser@asdf.com' LIMIT 1];
         System.runAs(usr) {
        Account acc = [SELECT Id FROM Account Limit 1];
        List<Contact> lstCon = [SELECT Id, firstName, Name, lastname,Email, Client_Notifications_Choice__c ,AccountId,
                                Client_Tasks_notification_choice__c,Client_Push_Notifications_Choice__c
                                FROM Contact];
        EnableNCPUser.createNCPUser(lstCon);
        EnableNCPUser.getContacts(acc.Id);
        EnableNCPUser.saveContacts(lstCon);
        EnableNCPUser.activeAndDeactivateUsers(lstCon, true);
             EnableNCPUser.NCPRegistrationaddPM(usr.Id);
        }
    }
    
    
}