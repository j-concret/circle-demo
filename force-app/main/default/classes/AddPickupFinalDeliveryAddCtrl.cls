public with sharing class AddPickupFinalDeliveryAddCtrl {

     @AuraEnabled
    public static Map<String,Object> getPicklistValue(String objectName,String fieldName){
        Map<String,Object> response = new Map<String,Object>();
        List<String> picklistLabels = new List<String>();
        picklistLabels.add('NONE');
        try {
            Schema.SObjectType s = Schema.getGlobalDescribe().get(objectName) ;
            Schema.DescribeSObjectResult r = s.getDescribe() ;
            Map<String,Schema.SObjectField> fields = r.fields.getMap() ;
            Schema.DescribeFieldResult fieldResult = fields.get(fieldName).getDescribe();
            List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
            for( Schema.PicklistEntry pickListVal : ple){
                System.debug(pickListVal.getLabel() +' '+pickListVal.getValue());
                response.put(pickListVal.getLabel(),pickListVal.getValue());
                picklistLabels.add(pickListVal.getLabel());
            }
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
        response.put('labels',picklistLabels);
        return response;
    }
    
    @AuraEnabled
    public static Map<String, Object> getAddressRecords(String country,String accountId,Boolean isPickup){
        try {
            Boolean hideNewFinalDeliveryButton = false;
			
            Map<String, Object> result = new Map<String, Object>();
            
            Id devRecordTypeId = Schema.SObjectType.Client_Address__c.getRecordTypeInfosByName().get(isPickup ? 'PickUp' : 'Final Destinations').getRecordTypeId();
            
            //Id devRecordTypeId = Schema.SObjectType.Client_Address__c.getRecordTypeInfosByName().get('Final Destinations').getRecordTypeId();
            
            Set<Id> accountIds = new Set<Id>();
            accountIds.add(accountId);
            
            if(!isPickup){
                System.debug('Inside isPickup');
                String eComProspectiveAccId = '';
                Boolean isZeeAccount = false;
                //String query = 'select Id,name,CaseID__c,CompanyName__c,AdditionalContactNumber__c,Contact_Email__c,Contact_Full_Name__c,Contact_Phone_Number__c,Province__c,City__c,Postal_Code__c,All_Countries__c';
                
                
                List<Account> accountList = [Select Id, Name,RecordType.Name, Allow_Customised_Final_Deliveries__c FROM Account
                                             WHERE Id = :accountId OR Name = 'TecEx E-Commerce Prospective Client'];
                
                for(Account acc : accountList){
                    
                    //Collecting E-Commerce Prospective Client id
                    if(acc.Name == 'TecEx E-Commerce Prospective Client'){
                        eComProspectiveAccId = acc.Id;
                    }
                    
                    if(((acc.RecordType.Name == 'Client (E-commerce)' || acc.RecordType.Name == 'Zee') && acc.Name != 'TecEx E-Commerce Prospective Client')){
                        isZeeAccount = true;
                        if(!acc.Allow_Customised_Final_Deliveries__c){
                            System.debug('Inside Allow Account');
                            accountIds.remove(acc.Id);
                             System.debug('Inside Allow Account 12');
                            hideNewFinalDeliveryButton = true;                      
                        }
                        
                    }
                    
                }
                
                if(isZeeAccount && String.isNotBlank(eComProspectiveAccId)){
                    accountIds.add(eComProspectiveAccId);
                }  
            }
            List<Client_Address__c> clientAddresses = [select Id,name,CaseID__c,CompanyName__c,Pickup_Preference__c,AdditionalContactNumber__c,Address__c,Address2__c,Contact_Email__c,Contact_Full_Name__c,Contact_Phone_Number__c,Province__c,City__c,Postal_Code__c,All_Countries__c
                   from Client_Address__c
                   where Client__c IN :accountIds and All_Countries__c=:country and recordtypeID =:devRecordTypeId ];
            
            
            result.put('ClientAddress', clientAddresses);
            result.put('hideNewFinalDeliveryButton', hideNewFinalDeliveryButton);
            
            return result;
            
            
            //return [SELECT Id,Name,CaseID__c,CompanyName__c,AdditionalContactNumber__c,Contact_Email__c,Contact_Full_Name__c,Contact_Phone_Number__c,Province__c,City__c,Postal_Code__c,All_Countries__c FROM Client_Address__c WHERE Client__c =: accountId AND All_Countries__c=: country AND recordtypeID =: devRecordTypeId ];
            
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    @AuraEnabled
    public static String getAddressFrom(String SearchText){
        String APIKey = 'AIzaSyCdwojMVLcu2JbRoRheb9isbNLxC0wRwR4';
        String result = null;
        system.debug('SearchText is ' + SearchText);
        try{
            if(SearchText != null) {
                String APIUrl = 'https://maps.googleapis.com/maps/api/place/autocomplete/json?input=' + SearchText.replace(' ', '%20') + '&key=' + APIKey;
                system.debug('APIUrl is ' + APIUrl);
                HttpRequest req = new HttpRequest();
                req.setMethod('GET');
                req.setEndpoint(APIURL);
                Http http = new Http();
                HttpResponse res = http.send(req);
                Integer statusCode = res.getStatusCode();
                system.debug('statusCode is ' + statusCode);
                if(statusCode == 200) {
                    system.debug('API invoked successfully');
                    result = res.getBody();
                }
            }
        }
        catch(exception e) {
            system.debug(e.getMessage());
        }
        return result;
    }

    @AuraEnabled
    public static String getAddressDetailsByPlaceId(String PlaceID){
        String APIKey = 'AIzaSyCdwojMVLcu2JbRoRheb9isbNLxC0wRwR4';
        String result = null;
        system.debug('SearchText is ' + PlaceID);
        try{
            if(PlaceID != null) {
                String APIUrl = 'https://maps.googleapis.com/maps/api/place/details/json?placeid=' + PlaceId.replace(' ', '%20') + '&key=' + APIKey;
                system.debug('APIUrl is ' + APIUrl);
                HttpRequest req = new HttpRequest();
                req.setMethod('GET');
                req.setEndpoint(APIURL);
                Http http = new Http();
                HttpResponse res = http.send(req);
                Integer statusCode = res.getStatusCode();
                system.debug('statusCode is ' + statusCode);
                if(statusCode == 200) {
                    system.debug('API invoked successfully');
                    result = res.getBody();
                }
            }
        }
        catch(exception e) {
            //Handling exception
            system.debug(e.getMessage());
        }
        return result;
    }

    @AuraEnabled
    public static Map<String,Object> getZipCodeByCountry(String countryName){
        Map<String,Object> response = new Map<String,Object> {'status' => 'OK'};
        try{
            response.put('countryName',countryName);
            no_zip_code_countries__c nzp = [SELECT Id,Name,zip_code__c FROM no_zip_code_countries__c WHERE Name = :countryName limit 1];
            response.put('no_zip_code_countries',nzp);
            //response.put('countryName',countryName);
        }catch(Exception e) {
            response.put('status','ERROR');
            response.put('message',e.getMessage());
        }
        return response;
    }

    //Creating pickup address
    @AuraEnabled
    public static void createNewAddress(List<Client_Address__c> newAddressList, string accountId,Boolean isPickup){
        try {
            List<Client_Address__c> newAddList = new list<Client_Address__c>();
            Id devRecordTypeId = Schema.SObjectType.Client_Address__c.getRecordTypeInfosByName().get(isPickup ? 'PickUp' : 'Final Destinations').getRecordTypeId();

            for(Client_Address__c ca :newAddressList) {
                Client_Address__c newAdd = new Client_Address__c(
                    recordtypeID = devRecordTypeId,
                    Name = ca.Province__c,
                    Contact_Full_Name__c= ca.Contact_Full_Name__c,
                    Contact_Email__c = ca.Contact_Email__c,
                    Contact_Phone_Number__c = ca.Contact_Phone_Number__c,
                    CompanyName__c= ca.CompanyName__c,
                    Address__c=ca.Address__c,
                    Address2__c = ca.Address2__c,
                    City__c=ca.City__c,
                    Province__c=ca.Province__c,
                    Postal_Code__c=ca.Postal_Code__c,
                    All_Countries__c=ca.All_Countries__c,
                    Client__c=accountId
                    );
                 if(isPickup) newAdd.Pickup_Preference__c = ca.Pickup_Preference__c;
                newAddList.add(newAdd);
            }
            Insert newAddList;
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }
}