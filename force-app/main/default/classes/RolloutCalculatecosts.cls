public class RolloutCalculatecosts {



  @Future
   public static void createSOCosts(List<Id> SOIds)  
       

    
    {
    
        Shipment_Order__c  SO = [Select Id, Name,  Insurance_Cost_CIF__c, Shipment_Value_USD__c, IOR_Fee_new__c, FC_IOR_and_Import_Compliance_Fee_USD__c, FC_Total_Customs_Brokerage_Cost__c,
        FC_Total_Handling_Costs__c, FC_Total_License_Cost__c, FC_Total_Clearance_Costs__c, FC_Insurance_Fee_USD__c, FC_Miscellaneous_Fee__c, On_Charge_Mark_up__c , FC_International_Delivery_Fee__c,
        TecEx_Shipping_Fee_Markup__c, CPA_v2_0__c, Chargeable_Weight__c, Account__c, Service_Type__c, Total_Taxes__c, of_Line_Items__c, FC_Total__c, of_Unique_Line_Items__c, CPA_Costings_Added__c,
        of_packages__c, Final_Deliveries_New__c, IOR_Refund_of_Tax_Duty__c  FROM Shipment_Order__c where Id IN :SOIds LIMIT 1];
        
        system.debug('CPA2--->'+SO.CPA_v2_0__c);
        system.debug('Id--->'+SO.Id);
         
        List<CPA_v2_0__c> CAP = [SELECT Related_Costing_CPA__c, VAT_Rate__c, Supplier__c FROM CPA_v2_0__c WHERE ID =: SO.CPA_v2_0__c LIMIT 1];
         
        List<CPA_Costing__c > relatedCostingsIOR = [SELECT Name, Shipment_Order_Old__c, Amount__c, Applied_to__c, Ceiling__c, Conditional_value__c, Condition__c, Floor__c, Cost_Category__c, Cost_Type__c, Max__c, Min__c, Rate__c, CostStatus__c, Updating__c, RecordTypeID, IOR_EOR__c, VAT_applicable__c, Variable_threshold__c, VAT_Rate__c, CPA_v2_0__r.VAT_Rate__c, Currency__c, Additional_Percent__c, Supplier__c, Tax_Sub_Category__c  FROM CPA_Costing__c WHERE CPA_v2_0__c =: CAP[0].Id AND IOR_EOR__c = 'IOR' ];
        List<CPA_Costing__c > relatedCostingsEOR = [SELECT Name, Shipment_Order_Old__c, Amount__c, Applied_to__c, Ceiling__c, Conditional_value__c, Condition__c, Floor__c, Cost_Category__c, Cost_Type__c, Max__c, Min__c, Rate__c, CostStatus__c, Updating__c, RecordTypeID, IOR_EOR__c, VAT_applicable__c, Variable_threshold__c, VAT_Rate__c, CPA_v2_0__r.VAT_Rate__c, Currency__c, Additional_Percent__c, Supplier__c FROM CPA_Costing__c WHERE CPA_v2_0__c =: CAP[0].Id AND IOR_EOR__c = 'EOR'];   
       
      //  List<CPA_Costing__c> addedCostings; 
        
         List<CPA_Costing__c> addedCostings= new List<CPA_Costing__c>();
        
         Id recordTypeId = Schema.SObjectType.CPA_Costing__c.getRecordTypeInfosByName().get('Display').getRecordTypeId();
         
        List<CPA_Costing__c> applicableCostings = new List<CPA_Costing__c>();
        
        List<CPA_Costing__c> costingsToAddUpdate = new List<CPA_Costing__c>();
        
        
         If(SO.Service_Type__c == 'IOR') {
          
           List<CPA_Costing__c>  SOCPAcostingsIOR = relatedCostingsIOR.deepClone();
           
             
             system.debug('SOCPAcostingsIOR--->'+SOCPAcostingsIOR.size());
           
            for(CPA_Costing__c cost : SOCPAcostingsIOR)
            
            {
           
                cost.Shipment_Order_Old__c = SO.ID;
                cost.CPA_v2_0__c = CAP[0].Related_Costing_CPA__c;
                cost.recordTypeID = recordTypeId;
                cost.VAT_Rate__c =  CAP[0].VAT_Rate__c;
                cost.Additional_Percent__c =  cost.Additional_Percent__c;
                cost.Updating__c = !cost.Updating__c;
                cost.Supplier__c = CAP[0].Supplier__c ;
                cost.Tax_Sub_Category__c = cost.Tax_Sub_Category__c;
               
                
                SOTriggerHelper.calculateSOCostingAmount(so, cost, false, cost.Currency__c);
                
                addedCostings.add(cost);
                system.debug('addedCostings--->'+ addedCostings.size());
                 
                }
                
                
                
              
            }
         
        
        Else If(SO.Service_Type__c == 'EOR' ) {
          
            List<CPA_Costing__c>  SOCPAcostingsEOR = relatedCostingsEOR.deepClone();
           
            for(CPA_Costing__c cost : SOCPAcostingsEOR)
            
            {
           
               cost.Shipment_Order_Old__c = SO.ID;
                cost.CPA_v2_0__c = CAP[0].Related_Costing_CPA__c;
                cost.recordTypeID = recordTypeId;
                cost.VAT_Rate__c =  CAP[0].VAT_Rate__c;
                cost.Additional_Percent__c =  cost.Additional_Percent__c;
                cost.Updating__c = !cost.Updating__c;
                cost.Supplier__c = CAP[0].Supplier__c ;
                
                SOTriggerHelper.calculateSOCostingAmount(so, cost, false, cost.Currency__c);
                
                addedCostings.add(cost);
                system.debug('addedCostings--->'+addedCostings.size());
               
              }
              
              
              
             
              
            }
           
        
        

            if (addedCostings != null){
            
                for (CPA_Costing__c cost : addedCostings){
                
                    if (cost.Amount__c > -1 && cost.Shipment_Order_Old__c != null){
                    
                        applicableCostings.Add(cost);

                   

        }
        
        
        }
        Insert applicableCostings;
       
    
    }
    
    }
    
    }
    
      //  List<SOCalculateCosts> newSOWrappers = new List<SOCalculateCosts>();

        
      //  List<Shipment_Order__c> shipmentOrder = [Select Id, Name,  Insurance_Cost_CIF__c, Shipment_Value_USD__c, IOR_Fee_new__c, FC_IOR_and_Import_Compliance_Fee_USD__c, FC_Total_Customs_Brokerage_Cost__c,
      //  FC_Total_Handling_Costs__c, FC_Total_License_Cost__c, FC_Total_Clearance_Costs__c, FC_Insurance_Fee_USD__c, FC_Miscellaneous_Fee__c, On_Charge_Mark_up__c , FC_International_Delivery_Fee__c,
      //  TecEx_Shipping_Fee_Markup__c, CPA_v2_0__c, Chargeable_Weight__c, Account__c, Service_Type__c, Total_Taxes__c, of_Line_Items__c, FC_Total__c, of_Unique_Line_Items__c, CPA_Costings_Added__c  FROM Shipment_Order__c where id IN :SOIds];
        
     //   List<Shipment_Order__c> SOs = new List<Shipment_Order__c>();

     //   List<CPA_Costing__c> costingsToAddUpdate = new List<CPA_Costing__c>();

        
        
   //     for (Shipment_Order__c sho : shipmentOrder) {newSOWrappers.Add(new SOCalculateCosts(sho));
    //    }

    //    if (newSOWrappers.size() > 0 && shipmentOrder.size() > 0 )
     //  {
       //     for (SOCalculateCosts wrp : newSOWrappers)
       //    {
            //    Shipment_Order__c SO;
              

            //   costingsToAddUpdate.AddAll(wrp.getApplicableCostings());
                 
         //   }
            
           

            
       // }
     //    Insert costingsToAddUpdate;
     //   insert SOs;