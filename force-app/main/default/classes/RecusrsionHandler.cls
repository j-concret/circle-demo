public with sharing class RecusrsionHandler {
    public static Boolean isBeforeUpdate = false;
    public static Boolean isAfterUpdate = false;
    public static Boolean isBeforeInsert = false;
    public static Boolean taxesCalculated = false;
    public static Boolean tasksCreated = false;
    public static Boolean Leadsonactivity = false;
    public static Boolean taskTriggerRun = false;
    public static Boolean runTaskInQueueable = false;
    public static Boolean runTaskIsQueued = false;
    public static Boolean isTaskTriggerSkip = false;
    public static Boolean isCaseTriggerSkip = false;
    public static Boolean skipBeforeLeadInsert = false;

    public static Boolean skipTriggerExecution = false;
    public static Boolean freightUpdateFromSO = false;
    public static Boolean partUpdateFromSO = false;
    public static Boolean skipFDTriggerExecutionOnSOCreation = false;
    public static Boolean skipSOPTriggerExecutionOnSOCreation = false;
    public static Boolean soCreationProcessStarted = false;
    public static Boolean SOPCalled = false;

    public static Integer count = 0;
    public static Integer count2 = 0;
    public static void updateRecusrsionHandler(List<Id> soid){
        isBeforeUpdate = true;
    }

    public class CustomException extends Exception{}
}