@isTest

public with sharing class TestInvoiceLineItemDeleteAllController {
    public TestInvoiceLineItemDeleteAllController() {

    }

    @isTest(SeeAllData=true)
    static void setupTestData() {       
 
        Account account = new Account (name='Acme1', Type ='Client', CSE_IOR__c = '0050Y000001LTZO');
        insert account;  
        
        Account account2 = new Account (name='Test Supplier', Type ='Supplier', ICE__c = '0050Y000001km5c');
        insert account2;      
         
        Contact contact = new Contact ( lastname ='Testing Individual',  AccountId = account.Id);
        insert contact;  
        
         IOR_Price_List__c iorpl = new IOR_Price_List__c ( Client_Name__c = account.Id, Name = 'Brazil', IOR_Fee__c = 0.15, On_Charge_Mark_up__c = 0.15,
        TecEx_Shipping_Fee_Markup__c =0.40, IOR_Min_Fee__c = 100,  Admin_Fee__c =100, Set_up_fee__c =100, Bank_Fees__c =200, Tax_Rate__c = 0.20,
        Estimated_Customs_Brokerage__c =250, Estimated_Customs_Clearance__c =250, Estimated_Customs_Handling__c =250, Estimated_Import_License__c =250, Destination__c = 'Brazil' );
        insert iorpl;   
        
        Shipment_Order__c masterObj = new Shipment_Order__c(Account__c = account.id, Shipment_Value_USD__c =10000, 
        Client_Contact_for_this_Shipment__c = contact.Id, Who_arranges_International_courier__c ='Client', Tax_Treatment__c ='DAP/CIF - IOR pays', 
        Destination__c = 'Brazil', Service_Type__c = 'IOR', IOR_Price_List__c = iorpl.Id, of_Unique_Line_Items__c = 10, Total_Taxes__c= 1500, of_Line_Items__c= 10, Chargeable_Weight__c = 200 );
        insert masterObj ; 

        

        Part__c detailObj1 = new Part__c();
        
        detailObj1.Name ='CAB-ETH-S-RJ45'; 
        detailObj1.Part_Type__c ='Server'; 
        detailObj1.Part_Number__c ='AB-ETH-S-RJ45'; 
        detailObj1.Quantity__c= 1; 
        detailObj1.Commercial_Value__c = 2000; 
        detailObj1.US_HTS_Code__c = '123123.123123.123.12';
        detailObj1.Shipment_Order__c = masterObj.Id;
        detailObj1.ECCN_NO__c= '5A002a Test';
        detailObj1.Description_and_Functionality__c = 'Test PALO ALTO 3060 FIREWALL';
        insert detailObj1;

   
        List<Shipment_Order__c> invoiceList = [SELECT Id FROM Shipment_Order__c where Id =: masterObj.Id ];
        PageReference deleteAllPage = Page.InvoiceLineItemDeleteAll;
        deleteAllPage.getParameters().put('id',invoiceList.get(0).Id);
        Test.setCurrentPage(deleteAllPage);
        Test.startTest();
        InvoiceLineItemDeleteAllController deleteAllController = new InvoiceLineItemDeleteAllController();
        deleteAllController.deleteAll();
        Test.stopTest();
        System.assertEquals([SELECT Id FROM Part__c LIMIT 1].size() == 0, false);
    }
}