public class FieldDefinition {
	public DescribeFieldResult Field;
	public DescribeSObjectResult ReferencedObject;
	public String Name { get { return Field.getLocalName(); } }
	
	public Boolean References(string objName) {
		return (ReferencedObject != null) && (ReferencedObject.getLocalName() == objName); 
	}

	public FieldDefinition(DescribeFieldResult fieldDef) {
		Field = fieldDef;
		if (Field.getType() == Schema.DisplayType.Reference) {
			List<Schema.sObjectType> refTo = field.getReferenceTo();
			if (!refTo.IsEmpty())
				ReferencedObject = refTo.get(0).getDescribe();		
		}
	}
}