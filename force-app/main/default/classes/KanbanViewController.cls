public with sharing class KanbanViewController {

  private static Map<String,String> nextKeys = new Map<String,String>{
    'role' => 'shipmentStatuses',
    'shipmentStatuses' => 'status',
    'status' => 'tasks',
    'tasks' => 'flagType',
    'flagType' =>'partProdKey'
  };

  private static List<String> fields = new List<String>{'role','status','flagType','partProdKey'};
  private static Map<String,List<Object>> otherFields = new Map<String,List<Object>>{
    'shipmentStatuses' => new List<Object>{
      new Map<String,String>{'jsonKey'=>'title','apiName'=> 'role'},
      new Map<String,String>{'jsonKey'=>'name','apiName' => 'ownerName'}
    },
    'tasks' => new List<Object>{
      new Map<String,String>{'jsonKey'=>'status' , 'apiName'=>'status'},
      new Map<String,String>{'jsonKey'=>'isFlag' , 'apiName'=>'status'}
    }
  };

    @AuraEnabled
    public static Map<String,Object> getKanbanViewData(Id recordId){
      Map<String,Object> response = new Map<String,Object>{'status'=>'OK'};
      try{
        Map<String,Object> data = new Map<String,Object>();
        Set<String> flagPicklistVals = FlagSummaryController.picklist_values('Task','Flag_Type__c');
        Set<String> statusPicklistVals = FlagSummaryController.picklist_values('Task','Status');
        statusPicklistVals.remove('Not Started'); // Remove unwanted status value from 'Status picklist field values'
       // Shipment_Order__c shipment_order = [SELECT Id,Name FROM Shipment_Order__c where Id=:recordId];
        for (Task tsk : [SELECT Id,Reason_Not_Applicable__c,Rule_Name__c,CPA_v2_0__c,Part__r.Name,Part__c, Product__r.Name,Product__c,CPA_v2_0__r.Name,Owner.Name,Status,Subject,Assigned_to_Role__c,Priority_Level__c,Flag_Type__c FROM Task where WhatId=:recordId and Status != 'Not Started']) {
          if(tsk.Assigned_to_Role__c != null && tsk.Status != null){
            recursion(data,'role',new TaskWrapper(tsk),'');
          }
        }
        response.put('data',data);
        response.put('flagVals',flagPicklistVals);
        response.put('statusvals',statusPicklistVals);
      }catch(Exception e){
        response.put('status','ERROR');
        response.put('msg', e.getMessage());
      }
      return response;
    }

    private static void recursion(Map<String,Object> data,String key,TaskWrapper tsk,String prevKey){
      if(key == null) return;
      String nextKey = (key == 'tasks' && tsk.status != 'In Progress') ? 'partProdKey' : nextKeys.get(key);
      String k = fields.contains(key) ? String.valueOf(tsk.get(key)) : key;

      if(data.containsKey(k)) {
        if(key == 'partProdKey'){
          List<Object> innerData = (List<Object>) data.get(k);
          innerData.add(tsk);
        }else{
          Map<String,Object> innerData = (Map<String,Object>) data.get(k);
          recursion(innerData,nextKey,tsk,k);
        }
      }else{
        if(key == 'partProdKey'){
            data.put(k,new List<Object>{tsk});
            //populateOtherFields(data,key,tsk);
        }
        else{
            Map<String,Object> innerData = new Map<String,Object>();
            data.put(k,innerData);
            populateOtherFields(data,key,tsk);
            recursion(innerData,nextKey,tsk,k);
        }
      }
    }

    private static void populateOtherFields(Map<String,Object> data,String key,TaskWrapper tsk){
      if(otherFields.containsKey(key) ){
        for(Object obj : otherFields.get(key)){
          Map<String,String> fieldObj = (Map<String,String>)obj;
          if(fieldObj.get('jsonKey') == 'isFlag')
            data.put(fieldObj.get('jsonKey'),String.valueOf(tsk.get(fieldObj.get('apiName'))) == 'In Progress');
          else
            data.put(fieldObj.get('jsonKey'),tsk.get(fieldObj.get('apiName')));
        }
      }
    }

    @AuraEnabled
    public static Map<String,Object> updateTask(Id taskId,String status,String reason){
      Map<String,Object> response = new Map<String,Object>{'status'=>'OK'};
      try {
        update new Task(Id=taskId,Status=status,Completed_By_User__c = (status != 'In Progress'),Reason_Not_Applicable__c = String.isNotBlank(reason) ? reason : null);
      } catch(System.DmlException e) {
           response.put('status','ERROR');
        	response.put('msg',e.getDmlMessage(0));
      }catch (Exception e) {
        response.put('status','ERROR');
        response.put('msg',e.getMessage());
      }
      return response;
    }


    public class TaskWrapper{
      @AuraEnabled public String id;
      @AuraEnabled public String role;
      @AuraEnabled public String status;
      @AuraEnabled public String flagType;
      @AuraEnabled public String subject;
      @AuraEnabled public String ruleName;
      @AuraEnabled public String ownerName;
      @AuraEnabled public String priorityLevel;
      @AuraEnabled public String CPAId;
      @AuraEnabled public String CPAName;
      @AuraEnabled public String hyperLinkLabel;
      @AuraEnabled public String partProdKey;
      @AuraEnabled public String partProdLabel;
      @AuraEnabled public String partProdId;
      @AuraEnabled public String partProdName;
      @AuraEnabled public Integer colorCode;
      @AuraEnabled public String reasonForNotApplicable;

      public TaskWrapper(Task tsk){
        this.reasonForNotApplicable = tsk.Reason_Not_Applicable__c;
        this.id = tsk.id;
        this.role = tsk.Assigned_to_Role__c;
        this.status = tsk.Status;
        this.flagType = tsk.Flag_Type__c;
        this.ruleName = tsk.Rule_Name__c;
        this.ownerName = tsk.Owner.Name;
        this.priorityLevel = tsk.Priority_Level__c;
        this.CPAId  = tsk.CPA_v2_0__c;
        this.CPAName = tsk.CPA_v2_0__c != null ? tsk.CPA_v2_0__r.Name : '';
        this.hyperLinkLabel = tsk.CPA_v2_0__c != null ? 'CPA' :'';
        this.partProdKey = tsk.Product__c != null ? 'products'+this.ruleName : (tsk.Part__c != null ? 'parts'+this.ruleName : 'others');
        this.partProdLabel = tsk.Product__c != null ? 'Product' : (tsk.Part__c != null ? 'Part' : 'others');
        this.partProdId = tsk.Product__c != null ? tsk.Product__c : (tsk.Part__c != null ? tsk.Part__c : null);
        this.partProdName = tsk.Product__c != null ? tsk.Product__r.Name : (tsk.Part__c != null ? tsk.Part__r.Name : null);
        this.colorCode = tsk.Priority_Level__c == 'Shipment Stop' ? 1 : tsk.Priority_Level__c == 'Shipment Block' ? 2 : tsk.Priority_Level__c == 'Shipment Alert' ? 3 : 4 ;
      }

      public Object get(String param_name){
        String json_instance = Json.serialize(this);
        Map<String, Object> untyped_instance = (Map<String, Object>)JSON.deserializeUntyped(json_instance);
        return untyped_instance.get(param_name);
      }

    }
}