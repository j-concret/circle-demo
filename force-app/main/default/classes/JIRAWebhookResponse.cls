public class JIRAWebhookResponse {

    public class Assignee {
        public String displayName;
    }

    public class CustomfieldGenral {
        public String value;
    }

    public class Fields {
        public Assignee assignee;
        public IssueGeneral issuetype;
        public Object duedate;
        public IssueGeneral status;
        public IssueGeneral resolution;
        public Assignee creator;
        public CustomfieldGenral customfield_11405;
        public CustomfieldGenral customfield_11408;
        public IssueGeneral priority;
        public String summary;
        public String description;
    }

    public class Issue {
        public String id;
        public String key;
        public Fields fields;
    }

    public Long timestamp;
    public String webhookEvent;
    public String issue_event_type_name;
    public Issue issue;

    public class IssueGeneral {
        public String name;
    }
}