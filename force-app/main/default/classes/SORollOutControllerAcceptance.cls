public class SORollOutControllerAcceptance{

public Blob csvFileBody{get;set;}
public string csvAsString{get;set;}
public String[] csvFileLines{get;set;}
Public Roll_Out__c rollout = new Roll_Out__c ();
public List<Shipment_Order__c> shipmentOrder{get;set;}
public List<cShipmentOrder> costList {get; set;}
public Boolean hasSelAcct {get;set;}



public SORollOutControllerAcceptance (ApexPages.StandardController controller){
this.rollOut =  [Select Id, Name, Client_Name__c, RecordTypeId, Create_Roll_Out__c, Destinations__c, Shipment_Value_in_USD__c, Contact_For_The_Quote__c, File_Reference_Name__c  FROM Roll_Out__c WHERE Id = :ApexPages.currentPage().getParameters().get('id') Limit 1];
this.shipmentOrder= [ Select Id, EOR_and_Export_Compliance_Fee_USD__c, Total_Taxes__c, of_Line_Items__c, Total_Cost_Range_Max__c, Total_Cost_Range_Min__c, Maximum_tax_range__c, Minimum_tax_range__c, Cost_Estimate_Number_HyperLink__c,Final_Deliveries_New__c, Who_arranges_International_courier__c, Total_Invoice_Amount__c,  Shipping_Status__c, Shipment_Value_USD__c, Name, IOR_Price_List__c, Final_Delivery_Address__c, Delivery_Contact_Tel_number__c, 
Delivery_Contact_Name__c, Account__c, RecordType.Name, RecordTypeId, IOR_Price_List__r.Name, Recharge_Tax_and_Duty__c, IOR_FEE_USD__c, Handling_and_Admin_Fee__c, Bank_Fees__c, Total_Customs_Brokerage_Cost__c, Total_clearance_Costs__c, Recharge_Handling_Costs__c, 
Recharge_License_Cost__c, International_Delivery_Fee__c, Insurance_Fee_USD__c,  Estimate_Transit_Time_Formula__c, Estimate_Customs_Clearance_Time_Formula__c, Shipping_Notes_Formula__c, Shipping_Notes_New__c, Account__r.Cash_outlay_fee_base__c,
Account__r.Finance_Charge__c, All_Taxes_and_Duties_Summary__c, Government_and_In_country_Summary__c, Chargeable_Weight__c, Tax_Cost__c FROM Shipment_Order__c where Roll_Out__c = :rollOut.Id];
    csvFileLines = new String[]{};
    getCosts();
  
  }
 
  public List<cShipmentOrder> getCosts() {
      
      String costEstimateRecordTypeId = Schema.Sobjecttype.Shipment_Order__c.getRecordTypeInfosByDeveloperName().get('Cost_Estimate').getRecordTypeId();
	  


     
        costList= new List<cShipmentOrder>();
        
        If( shipmentOrder != null)
        
        {
       
        
        for(Shipment_Order__c SO: [Select Id, Cost_Estimate_Number_HyperLink__c, Final_Deliveries_New__c,Who_arranges_International_courier__c, Total_Invoice_Amount__c,  Shipping_Status__c, Shipment_Value_USD__c, Name, IOR_Price_List__c, Final_Delivery_Address__c, Delivery_Contact_Tel_number__c, 
                                   Delivery_Contact_Name__c, Account__c, RecordType.Name, RecordTypeId,  IOR_Price_List__r.Name, Recharge_Tax_and_Duty__c, IOR_FEE_USD__c, Handling_and_Admin_Fee__c, Bank_Fees__c, Total_Customs_Brokerage_Cost__c, Total_clearance_Costs__c, 
                                   Recharge_Handling_Costs__c, Recharge_License_Cost__c, International_Delivery_Fee__c, Insurance_Fee_USD__c,  Estimate_Transit_Time_Formula__c, Estimate_Customs_Clearance_Time_Formula__c, Shipping_Notes_Formula__c, Shipping_Notes_New__c,
                                   Account__r.Cash_outlay_fee_base__c, Account__r.Finance_Charge__c, All_Taxes_and_Duties_Summary__c, Government_and_In_country_Summary__c, Chargeable_Weight__c, Tax_Cost__c
                      FROM Shipment_Order__c where Roll_Out__c = :ApexPages.currentPage().getParameters().get('id') and RecordTypeId = :costEstimateRecordTypeId]) {
            // As each contact is processed we create a new cContact object and add it to the contactList
            costList.add(new cShipmentOrder (SO, True));
        }
        return costList;
        
        }
        
        Else
        
        {
        
        return null;
        
        }
  }
  
  
 
 public PageReference processSelected() {

        //We create a new list of Contacts that we be populated only with Contacts if they are selected
        List<Shipment_Order__c> selectedCosts = new List<Shipment_Order__c>();
     String ShipmentOrderRecordTypeId = Schema.Sobjecttype.Shipment_Order__c.getRecordTypeInfosByDeveloperName().get('Shipment_Order').getRecordTypeId();

        //We will cycle through our list of cContacts and will check to see if the selected property is set to true, if it is we add the Contact to the selectedContacts list
        for(cShipmentOrder cCost: costList) {
            if(cCost.selected == true) {           
                selectedCosts.add(cCost.cSO);
            }
        }

        // Now we have our list of selected contacts and can perform any type of logic we want, sending emails, updating a field on the Contact, etc
        
        
        System.debug('These are the selected Contacts...'+ selectedCosts );
        
        List<Shipment_Order__c> c = new List<Shipment_Order__c>();
        for(Shipment_Order__c costs: selectedCosts) {
            costs.Shipping_Status__c = 'Shipment Pending';
            costs.RecordTypeId = ShipmentOrderRecordTypeId;
            costs.Cost_Estimate_Accepted__c = TRUE;
            c.add(costs);            
        }
        
        try{
        
        update c;
        PageReference pageRef = new ApexPages.StandardController(rollout).view();
        pageRef.setRedirect(true);
        return pageRef;
       // costList =null; // we need this line if we performed a write operation  because getContacts gets a fresh list now
        
         }
        catch(Exception ex){
        ApexPages.addMessages(ex);
}
       
        
       
        
        return null;
    }
 
 
 
 // public Pagereference exportAll(){
 //   return new Pagereference('/apex/ExportSORollOut');
 //   PageReference pageRef = ApexPages.currentPage();
//    pageRef.setRedirect(true);
//    return pageRef;
    
//    }
 
 
        public class cShipmentOrder {
        public Shipment_Order__c cSO {get; set;}
        public Boolean selected {get; set;}
       
        public cShipmentOrder (Shipment_Order__c SO, Boolean sel) {
            cSO = SO;        
            selected = sel;
        }
    }
    
    


}