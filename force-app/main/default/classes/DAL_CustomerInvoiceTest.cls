@isTest
private class DAL_CustomerInvoiceTest {
    
    @isTest static void TestDAL_CustomerInvoice() {
	
	    //Prepare testing data data:

        //Account records
        Account myClient   = TestDataFactory.myClient();
        Account mySupplier = TestDataFactory.mySupplier();
    
        //Invoice records
        Customer_Invoice__c creditNoteInvoiceRecord   = TestDataFactory.myCreditNoteInvoice();
        Customer_Invoice__c firstInvoiceInvoiceRecord = TestDataFactory.myFirstInvoice();
    
        //Invoice record(s) Id's
        Set<Id> idSet = new Set<Id>{creditNoteInvoiceRecord.Id};
    
        //Customer(Account) records Id's
        Id customerId         = myClient.Id;
        Id customerId2        = mySupplier.Id;
        Set<Id> customerIdSet = new Set<Id>{customerId, customerId2};

	    Test.startTest();
    
        //Class Instantiation
        DAL_CustomerInvoice dalCustomerInvoice     = new DAL_CustomerInvoice();
    
        // getSObjectFieldList
        List<Schema.SObjectField> sobjectFieldList = dalCustomerInvoice.getSObjectFieldList();
    
        //getSObjectType
        Schema.SObjectType sobjectType = dalCustomerInvoice.getSObjectType();
    
        //selectById
        List<Customer_Invoice__c> customerInvoiceById = dalCustomerInvoice.selectById(idSet);

        //selectUnappliedCreditNotesTotalByCustomerId
        Decimal unappliedCreditNotesTotalByCustomerId = dalCustomerInvoice.selectUnappliedCreditNotesTotalByCustomerId(customerId);

        //getTotalUnappliedAmountByCustomerId
        List<DMN_Statement.AgingPeriod> totalUnappliedAmountByCustomerId = dalCustomerInvoice.getTotalUnappliedAmountByCustomerId(customerId);

        // getTotalUnappliedAmountByCustomerId
        Map<Id,List<DMN_Statement.AgingPeriod>> totalUnappliedAmountByCustomerIdMap = dalCustomerInvoice.getTotalUnappliedAmountByCustomerId(customerIdSet);
    
        //selectUnappliedCreditNotesTotalByCustomerId
        List<Customer_Invoice__c> creditNotesTotalByCustomerId = dalCustomerInvoice.selectUnappliedCreditNotesTotalByCustomerId(customerIdSet);
    
        //selectOpenInvoicesByCustomerId
        List<Customer_Invoice__c> openInvoicesByCustomerId = dalCustomerInvoice.selectOpenInvoicesByCustomerId(customerId);

        //selectByCustomerId
        List<Customer_Invoice__c> selectByCustomerIdSet = dalCustomerInvoice.selectByCustomerId(customerIdSet);
        List<Customer_Invoice__c> selectByCustomerId    = dalCustomerInvoice.selectByCustomerId(customerId);

        //getInvoiceAgingByCustomerId
        List<DMN_Statement.AgingPeriod> getInvoiceAgingByCustomerId            = dalCustomerInvoice.getInvoiceAgingByCustomerId(customerId);
        Map<Id,List<DMN_Statement.AgingPeriod>> getInvoiceAgingByCustomerIdMap =  dalCustomerInvoice.getInvoiceAgingByCustomerId(customerIdSet);

        Test.stopTest();
    
        //Assert getSObjectFieldList
        System.assertEquals(16, sobjectFieldList .size());
   
        //Assert getSObjectType
        System.assert(creditNoteInvoiceRecord.getsObjectType() == sobjectType);

        // selectById assert
        System.assertEquals(1, customerInvoiceById.size());
    }
}