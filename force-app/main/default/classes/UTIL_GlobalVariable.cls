public class UTIL_GlobalVariable 
{
 
    public static String get()
    {
        String returnValue = null;
			        
        final Features_Switches__c allVariables = Features_Switches__c.getInstance();        
   
        returnValue = allVariables.value__c;               
        
        return returnValue;
    }
    
    public static String put(String value)
    {
        String returnValue = null; //the previous value        

        Features_Switches__c record = Features_Switches__c.getInstance();        
            
        record.Value__c = value;            
        
        if(record.id != null)
            update record;
        else 
            insert record;

        returnValue = record.Value__c;        
        
        return returnValue;
    } 
}