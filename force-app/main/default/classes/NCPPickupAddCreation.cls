@RestResource(urlMapping='/PickupAddresscreate/*')
global class NCPPickupAddCreation {
    
     @Httppost
    global static void NCPPickupAddCreation(){
     	RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        Blob body = req.requestBody;
    	res.addHeader('Content-Type', 'application/json');
        String requestString = body.toString();
        NCPPickUpaddCreationwra rw = (NCPPickUpaddCreationwra)JSON.deserialize(requestString,NCPPickUpaddCreationwra.class);
     try {
              
 		Access_token__c At =[select status__c,Access_Token__c from Access_token__c where Access_Token__c=:rw.PickUpAddress[0].Accesstoken ];
        if( at.status__C =='Active'){
         
          If(!rw.PickUpAddress.isEmpty()){
            Id PickuprecordTypeId = Schema.SObjectType.Client_Address__c.getRecordTypeInfosByDeveloperName().get('PickUp').getRecordTypeId();
            list<Client_Address__c> PickaddL = new list<Client_Address__c>();
              
              Boolean Isdefaultexists;   
               
              for(Integer i=0; rw.PickUpAddress.size()>i;i++) {
                  if(rw.PickUpAddress[i].DefaultAddress == 'TRUE') { Isdefaultexists = TRUE;}
               }
					system.debug('Isdefaultexists-->'+Isdefaultexists);
              
              IF(Isdefaultexists==TRUE){
                  
                  	List<Client_Address__c> AB =[select id,Name,default__C,Address_status__c,Comments__c,All_Countries__c,Client__c  from Client_Address__c where All_Countries__c= :rw.PickUpAddress[0].All_Countries and default__C = true and Client__c =:rw.PickUpAddress[0].AccountID and recordtypeID=:PickuprecordTypeId];
                 
                  for(Integer i=0; AB.size()>i;i++) { AB[i].Default__c =FALSE;
                                                    } Update AB;
                  
                                  
              }
              
              
                for(Integer i=0; rw.PickUpAddress.size()>i;i++) {    
                  				Client_Address__c Pickadd = new Client_Address__c(
            
            									recordtypeID=PickuprecordTypeId,   
            									Name = Rw.PickUpAddress[i].Name,
                                                Contact_Full_Name__c= rw.PickUpAddress[i].Contact_Full_Name,
                                                Contact_Email__c = rw.PickUpAddress[i].Contact_Email,
                                                Contact_Phone_Number__c = rw.PickUpAddress[i].Contact_Phone_Number,
                                                Address__c=rw.PickUpAddress[i].Address1,
                                                Address2__c=rw.PickUpAddress[i].Address2,
                                                City__c=rw.PickUpAddress[i].City,
                                                Province__c=rw.PickUpAddress[i].Province,
                                                Postal_Code__c=rw.PickUpAddress[i].Postal_Code,
                                                Client__c=rw.PickUpAddress[i].AccountID,
                                                Comments__c= rw.PickUpAddress[i].Comments,
                                                CompanyName__c= rw.PickUpAddress[i].CompanyName,
                                                AdditionalContactNumber__c= rw.PickUpAddress[i].AdditionalNumber,
                                                Default__c= Boolean.valueOf(rw.PickUpAddress[i].DefaultAddress),   
                                                Address_Status__c= rw.PickUpAddress[i].AddressStatus,
                                     			Pickup_Preference__c= rw.PickUpAddress[i].PickupPreference,
                                                All_Countries__c=rw.PickUpAddress[i].All_Countries);
                           PickaddL.add(Pickadd);
                                                      
                  }
              
              system.debug('PickaddL.size-->'+PickaddL.size());
              Insert PickaddL;
              
                res.responseBody = Blob.valueOf(JSON.serializePretty(PickaddL));
               res.addHeader('Content-Type', 'application/json');
            	res.statusCode = 200;
              	API_Log__c Al = New API_Log__c();
                Al.Account__c=rw.PickUpAddress[0].AccountID;
                Al.EndpointURLName__c='PickupAddresscreation';
           //  Al.Response__c =  String.valueOf(URL.getCurrentRequestUrl()).toLowerCase();  
              Al.Response__c='Success-Pickup Addrerss Created';
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al; 
            }
           }
            }
        	catch (Exception e){
                
              	String ErrorString ='Something went wrong while creating pickup address, please contact support_SF@tecex.com';
               	res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
                res.addHeader('Content-Type', 'application/json');
            	res.statusCode = 500;
                API_Log__c Al = New API_Log__c();
                Al.Account__c=rw.PickUpAddress[0].AccountID;
                Al.EndpointURLName__c='PickupAddresscreation';
                Al.Response__c=e.getMessage()+' at line number: '+e.getLineNumber();
                Al.StatusCode__c=string.valueof(res.statusCode);
              	Insert Al;

              	}
        
        
    }

}