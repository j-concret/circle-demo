@RestResource(urlMapping='/NCPGetProductCatelog/*')
Global class NCPGetProductCatelog {
    @Httppost
      global static void NCPGetProductCatelog(){
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        Blob body = req.requestBody;
    	res.addHeader('Content-Type', 'application/json');
        String requestString = body.toString();
        NCPGetProductCatelogWrap rw  = (NCPGetProductCatelogWrap)JSON.deserialize(requestString,NCPGetProductCatelogWrap.class);
          try{
              
                Access_token__c At =[select status__c,Access_Token__c from Access_token__c where Access_Token__c=:rw.Accesstoken ];
            	if( at.status__C =='Active'){
               
                  list<Product_Catalogue__c> Pcs = [Select ID,Client_Reference__c,Part__r.name,Part__r.Product_HS_Code__c,Part__r.Country_of_Origin__c,Part__r.Country_of_Origin2__c,Part_Description__c,Part_Mapped__c,Unit_Price__c,Number_of_times_shipped__c from Product_Catalogue__c where Account__c =:rw.AccountId ];
                  res.responseBody = Blob.valueOf(JSON.serializePretty(Pcs));
                  res.addHeader('Content-Type', 'application/json');
        		  res.statusCode = 200;
                
                API_Log__c Al = New API_Log__c();
                Al.Account__c=rw.AccountID;
               
                Al.EndpointURLName__c='NCP - NCPGetProductCatelog';
                Al.Response__c='Success - Product Catelog Details are sent';
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al;
                 
                    
                }
              
          }
          catch(Exception e){
              
                String ErrorString ='Something went wrong please contact sfsupport@tecex.com';
               		res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
            		res.addHeader('Content-Type', 'application/json');
        			res.statusCode = 400;
                
                API_Log__c Al = New API_Log__c();
                Al.Account__c=rw.AccountID;
               
                Al.EndpointURLName__c='NCP - NCPGetProductCatelog';
                Al.Response__c=e.getMessage()+' at line number: '+e.getLineNumber();
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al;
              
          }
          
      }

}