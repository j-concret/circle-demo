public with sharing class tecexCIComponent_CTRL_V2 {

    @AuraEnabled
    public static Map<String,Object> getShipmentRelatedData(String recId){
        Map<String,Object> response = new Map<String,Object> {'status'=>'OK'};
        try{
            Shipment_Order__c shipmentOrder = [SELECT Id,Name,CI_Input_Freight__c,RecordType.Name, Service_Type__c ,CI_Input_Liability_Cover__c,Account__r.CI_Statement__c,CPA_v2_0__r.Valuation_Method__c,Beneficial_Owner_Company_Name__c,CPA_v2_0__c,CPA_v2_0__r.Name,CPA_v2_0__r.CI_Name__c,SupplierlU__c,Client_Reference__c,CPA_v2_0__r.CI_Currency__c, CPA_v2_0__r.Section_2_Header__c,Client_Reference_2__c,Destination__c,Account__r.Client_type__c,CPA_v2_0__r.CIF_Freight_and_Insurance__c,CPA_v2_0__r.Exporter_Header__c,CPA_v2_0__r.Specific_CI_Statement_1__c,CPA_v2_0__r.Default_CI_Currency__c,CPA_v2_0__r.Specific_CI_Statement_2__c,CPA_v2_0__r.Specific_CI_Statement_3__c,CPA_v2_0__r.Footer_Note_Override__c,CPA_v2_0__r.Ship_From_Header__c,CPA_v2_0__r.CI_Inco_Terms__c,Account__r.Name,Account__r.Registered_Name__c,Account__r.Client_Address_Line_1__c,Account__r.Client_Address_Line_2__c,Account__r.Client_Address_Line_3__c,Account__r.Client_City__c,Account__r.Client_Country__c,Account__r.Client_Zip__c,Account__r.Other_notes__c,Account__r.Tax_Name__c,Account__r.Tax_ID__c,Account__r.Contact_Name__c,Account__r.Contact_Email__c,Account__r.Contact_Tel__c,Shipment_Value_USD__c,Who_arranges_International_courier__c,Insurance_Fee_USD__c FROM Shipment_Order__c WHERE Id =: recId];

            Freight__c freightRequest = [SELECT Id,International_freight_fee_invoiced__c,Pick_Up_Contact_Address1__c,Pick_Up_Contact_Address2__c,Pick_Up_Contact_Address3__c,Pick_Up_Contact_City__c,Pick_Up_Contact_Country__c,Pick_Up_Contact_ZIP__c,Pick_Up_Contact_Comments__c,Pick_up_Contact_Details__c,Pick_Up_Contact_Email__c,Pick_up_Contact_No__c,
                                         SF_Company__c,SF_AddressLine1__c ,SF_AddressLine2__c ,SF_City__c ,SF_Country__c ,SF_Postal_Code__c ,SF_Name__c , SF_Email__c ,SF_Phone__c  FROM Freight__c WHERE Shipment_Order__c =:recId LIMIT 1];

            Map<String,Set<String> > ports = new Map<String,Set<String> >();
            
           
            for(Client_Address__c cAdd : [SELECT Name,VAT_NON_VAT_address__c, Port_of_Entry__c FROM Client_Address__c WHERE recordTypeId = :Schema.SObjectType.Client_Address__c.getRecordTypeInfosByName().get('Consignee Address').getRecordTypeId() AND Document_Type__c='CI' AND Client__c=:shipmentOrder.SupplierlU__c AND CPA_v2_0__c =: shipmentOrder.CPA_v2_0__c ]) {
                
                if(cAdd.Port_of_Entry__c == null) continue;
                if(ports.containsKey(cAdd.VAT_NON_VAT_address__c)) {
                    if(!(ports.get(cAdd.VAT_NON_VAT_address__c)).contains(cAdd.Port_of_Entry__c)) {
                        ports.get(cAdd.VAT_NON_VAT_address__c).add(cAdd.Port_of_Entry__c);
                    }

                }else{
                    ports.put(cAdd.VAT_NON_VAT_address__c,new Set<String> {cAdd.Port_of_Entry__c});
                }
            }
		
            //response.put('missingDataList', checkMissingData(recId, shipmentOrder));
            response.put('shipmentOrder',shipmentOrder);
            response.put('freightRequest',freightRequest);
            response.put('ports',ports);

        }catch(Exception e) {
            response.put('status','ERROR');
            response.put('msg',e.getMessage());
        }
        return response;
    }

    // This method will check all required data missing and show the same on CI creation process.
    @AuraEnabled
    public static Map<String,Object> checkMissingData(String recId,String vatValue,String portValue){
        Map<String,Object> response = new Map<String,Object> {'status'=>'OK'};
        Set<String> missingDataSet = new Set<String>();

        Boolean checkForMissingBeneficial = false;

        try{
            
            Shipment_Order__c shipmentOrder = [SELECT Id,Account__c,RecordTypeId, Destination__c,Name,CPA_v2_0__c,Beneficial_Owner_Company_Name__c,Contact_name__c,SupplierlU__c,Account__r.Tax_recovery_client__c,Account__r.Registered_Name__c,CPA_v2_0__r.Name,CPA_v2_0__r.VAT_Reclaim_Destination__c, Account__r.Name FROM Shipment_Order__c WHERE Id=:recId];
            
            
            //Here we are checking if the Naming_Conventions__c contains Beneficial Owner then only check the Beneficial_Owner_Company_Name__c on shipment.
            for(Client_Address__c c_add : [SELECT Id,Naming_Conventions__c,Port_of_Entry__c,Address_Type__c FROM Client_Address__c WHERE recordTypeId = :Schema.SObjectType.Client_Address__c.getRecordTypeInfosByName().get('Consignee Address').getRecordTypeId() AND Document_Type__c='CI' AND Client__c=:shipmentOrder.SupplierlU__c AND CPA_v2_0__c =: shipmentOrder.CPA_v2_0__c AND VAT_NON_VAT_address__c=:vatValue AND Address_Type__c != null order by Address_Type__c]) {
                system.debug('VAT_NON_VAT_address__c ' + c_add);
                if(c_add.Port_of_Entry__c != portValue && c_add.Address_Type__c == 'Section 1') continue;
                if(c_add.Naming_Conventions__c != NULL && c_add.Naming_Conventions__c.contains('Beneficial Owner')) {
                    checkForMissingBeneficial = true;
                }
            }

          //  if(checkForMissingBeneficial && shipmentOrder.Beneficial_Owner_Company_Name__c == NULL || shipmentOrder.Beneficial_Owner_Company_Name__c == '') {
             if(checkForMissingBeneficial && (shipmentOrder.Contact_name__c == NULL || shipmentOrder.Contact_name__c == '')) {   // commented above line and Added by anil 07202020
            missingDataSet.add('Beneficial Owner Name Missing.');
            }

            // Here we are checking line items count.
            Integer lineItemCount = Database.countQuery('select count() from Part__c WHERE Shipment_Order__c = :recId');
            if(lineItemCount == 0) {
                missingDataSet.add('Line Items Missing.');
            }

            // Here we are checking Shipment Order Packages count.
            Integer SOPCount = Database.countQuery('select count() from Shipment_Order_Package__c WHERE Shipment_Order__c = :recId');
            if(SOPCount == 0) {
                missingDataSet.add('Shipment Order Packages Missing.');
            }

            // Here we are checking Final Delivery Address count.
            Integer finalDeliveryCount = Database.countQuery('select count() from Final_Delivery__c WHERE Shipment_Order__c = :recId');
            if(finalDeliveryCount == 0) {
                missingDataSet.add('Final Delivery Address Missing.');
            }
            
            Integer registrationRecordCount = Database.countQuery('select count() from Registrations__c WHERE Company_name__c = \''+shipmentOrder.Account__c+'\' AND Country__c = \''+shipmentOrder.Destination__c + '\'');
            if(registrationRecordCount == 0 && isSORecordTypeIsZee(shipmentOrder.RecordTypeId)) {
                missingDataSet.add('Client Registration Address Missing.');
            }
            
            response.put('missingDataList',missingDataSet);
        }catch(Exception e) {
            response.put('status','ERROR');
            response.put('msg',e.getMessage());
        }
        return response;
    }


    @AuraEnabled
    public static Map<String,Object> getShipmentPckgLineItem(String recId,String vatValue,String portValue){
        List<Id> shipmentRecordIds = new List<Id>{Schema.Sobjecttype.Shipment_Order__c.getRecordTypeInfosByDeveloperName().get('Zee_Cost_Estimate').getRecordTypeId(),
            Schema.Sobjecttype.Shipment_Order__c.getRecordTypeInfosByDeveloperName().get('Zee_Shipment_Order').getRecordTypeId()};
                String appName = 'TecEx';
        		Double currencyConvertor = 1;
        List<Registrations__c> regt = new List<Registrations__c>();
                Map<String,Object> response = new Map<String,Object> {'status'=>'OK'};
                    String currencyUnit = '';    
        List<String> logos = new List<String> {'colombia - tekstar (*)','denali advanced integration, inc','dimension data usa','cloudflare inc','akamai international b.v.'};
            try{
                Shipment_Order__c shipmentOrder = [SELECT Id, Account__c, Destination__c, Name, RecordTypeId, CPA_v2_0__c, CPA_v2_0__r.CI_Currency__c,CPA_v2_0__r.Valuation_Method__c ,Beneficial_Owner_Company_Name__c,SupplierlU__c,Account__r.Tax_recovery_client__c,Account__r.Registered_Name__c,CPA_v2_0__r.Name,CPA_v2_0__r.VAT_Reclaim_Destination__c, Account__r.Name, Contact_name__c FROM Shipment_Order__c WHERE Id=:recId];
                
                List<Part__c> parts = [SELECT Id,  Name, Net_Weight_per_Line_Item__c,  Quantity__c,  Retail_Method_Unit_Price__c, Description_and_Functionality__c,  COO_2_digit__c,  ECCN_NO__c,  US_HTS_Code__c,  Commercial_Value__c,  Total_Value__c, Shipment_Order__c,additional_CI_Columns__c FROM Part__c WHERE Shipment_Order__c = :recId];
                
                if(shipmentRecordIds.contains(shipmentOrder.RecordTypeId) && shipmentOrder.CPA_v2_0__c !=null){
                    
                    regt = [SELECT Id, Name, Company_name__c, Country__c, Finance_contact_Email__c, 
                            Finance_contact_Name__c, Finance_contact_Phone__c, Registered_Address_2__c, 
                            Registered_Address_City__c, Registered_Address_Country__c, 
                            Registered_Address_Postal_Code__c, Registered_Address_Province__c, 
                            Registered_Address__c, Type_of_registration__c, VAT_number__c, Verified__c 
                            FROM Registrations__c
                           Where Company_name__c = :shipmentOrder.Account__c AND Country__c = : shipmentOrder.Destination__c];
                    
                    appName = 'Zee';
                    Currency_Management2__c cmList = new Currency_Management2__c();
                    
                    if(shipmentOrder.CPA_v2_0__r.CI_Currency__c != null){
                        currencyUnit = shipmentOrder.CPA_v2_0__r.CI_Currency__c;
                        currencyUnit = currencyUnit.subString(currencyUnit.indexOf('(') + 1,currencyUnit.indexOf(')'));
                        cmList = [SELECT id,Conversion_Rate__c,ISO_Code__c FROM Currency_Management2__c WHERE ISO_Code__c =:currencyUnit];
                    }
                    if(shipmentOrder.CPA_v2_0__r.Valuation_Method__c == 'Cost Method'){
                        
                        if(cmList.Conversion_Rate__c != null && currencyUnit != 'USD'){
                            
                            for(Part__c prt : parts){
                                prt.Commercial_Value__c =  (prt.Commercial_Value__c / cmList.Conversion_Rate__c).setScale(2) ;
                            }
                            
                            Formula.recalculateFormulas(parts);
                            currencyConvertor = cmList.Conversion_Rate__c;
                            
                        }
                    }
                    else if(shipmentOrder.CPA_v2_0__r.Valuation_Method__c == 'Retail Method'){
                        
                        for(Part__c prt : parts){
                            prt.Commercial_Value__c = (prt.Retail_Method_Unit_Price__c).setScale(2);
                        }
                        
                        Formula.recalculateFormulas(parts);
                    }
                    
                }
                
                List<Shipment_Order_Package__c> sopList = [SELECT Id,  Name,  Actual_Weight_KGs_Per_Package__c ,packages_of_same_weight_dims__c,  Actual_Weight_KGs__c,  Breadth__c,  Height__c,  Length__c, Actual_Weight__c, Dimension_Unit__c,  Shipment_Order__c FROM Shipment_Order_Package__c WHERE Shipment_Order__c = :recId];
                
                Decimal numberOfPackages = 0;
                Decimal packagesWeight = 0;
                for(Shipment_Order_Package__c pckg : sopList) {
                    if(pckg.Dimension_Unit__c != NULL && pckg.Dimension_Unit__c == 'INs'){
                        pckg.Length__c = (pckg.Length__c * 2.54).setScale(2);
                        pckg.Breadth__c = (pckg.Breadth__c * 2.54).setScale(2);
                        pckg.Height__c = (pckg.Height__c * 2.54).setScale(2);
                    }
                    
                    
                    numberOfPackages += pckg.packages_of_same_weight_dims__c;
                    //  packagesWeight += (pckg.packages_of_same_weight_dims__c*pckg.Actual_Weight_KGs__c); //Anil 08032020 changed field
                    packagesWeight += (1*pckg.Actual_Weight_KGs__c);
                    
                    
                }
                
                List<Final_Delivery__c> finalDeliveries = [SELECT Id, Name, Address_Line_1__c, Address_Line_2__c, Address_Line_3__c, City__c, Contact_email__c,
                                                           Contact_name__c, Contact_number__c, Country__c, Delivery_address__c, Delivery_date__c,
                                                           Delivery_Status__c, Other__c,Shipment_Order__c, Tax_Id__c, Tax_Name__c, Zip__c, Company_Name__c,
                                                           Ship_to_address__r.CompanyName__c, Ship_to_address__r.Address__c,Ship_to_address__r.Address2__c,
                                                           Ship_to_address__r.City__c, Ship_to_address__r.Postal_Code__c,  Ship_to_address__r.All_Countries__c,
                                                           Ship_to_address__r.Contact_Full_Name__c,  Ship_to_address__r.Contact_Email__c,
                                                           Ship_to_address__r.Contact_Phone_Number__c,  Ship_to_address__r.Tax_Name_Number__c   
                                                           FROM Final_Delivery__c
                                                           WHERE Shipment_Order__c =:shipmentOrder.Id LIMIT 1];
                
                List<Client_Address__c> cAddresses = new List<Client_Address__c>();
                
                for(Client_Address__c c_add : [SELECT Id,Naming_Conventions__c, Address__c, Name, City__c, All_Countries__c, Postal_Code__c, Province__c, Address2__c, CompanyName__c, Contact_Full_Name__c, Contact_Phone_Number__c, Contact_Email__c, AdditionalContactNumber__c, Tax_Name_Number__c,VAT_NON_VAT_address__c,Address_Type__c,Port_of_Entry__c,Section_Headers__c FROM Client_Address__c WHERE recordTypeId = :Schema.SObjectType.Client_Address__c.getRecordTypeInfosByName().get('Consignee Address').getRecordTypeId() AND Document_Type__c='CI' AND Client__c=:shipmentOrder.SupplierlU__c AND CPA_v2_0__c =: shipmentOrder.CPA_v2_0__c AND VAT_NON_VAT_address__c=:vatValue AND Address_Type__c != null order by Address_Type__c]) {
                    
                    if(c_add.Port_of_Entry__c != portValue && c_add.Address_Type__c == 'Section 1') continue;
                    cAddresses.add(c_add);
                }
                String logoName;
                if(String.isNotBlank(shipmentOrder.Account__r.Name) && logos.contains(shipmentOrder.Account__r.Name.toLowerCase())) {
                    logoName = '/logos/'+(shipmentOrder.Account__r.Name.toLowerCase()).replace(' ','_')+'.jpg';
                }else if(String.isNotBlank(shipmentOrder.CPA_v2_0__r.Name) && logos.contains(shipmentOrder.CPA_v2_0__r.Name.toLowerCase())) {
                    logoName = '/logos/'+(shipmentOrder.CPA_v2_0__r.Name.toLowerCase()).replace(' ','_')+'.jpg';
                }
                
                system.debug('cAddresses ' + JSON.serialize(cAddresses));
                
                //This method will transform values of client address records as per the naming convention logic.
                namingConventionTransformation(cAddresses, finalDeliveries, shipmentOrder, regt);
                
                system.debug('cAddresses after ' + JSON.serialize(cAddresses));
                response.put('ciCount', Database.countQuery('SELECT count() FROM ContentVersion WHERE Title like \'%Commercial Invoice%\' AND FirstPublishLocationId =:recId LIMIT 1'));
                response.put('logoName',logoName);
                response.put('cAddresses',cAddresses);
                response.put('lineItems',parts);
                response.put('sopList',sopList);
                response.put('numberOfPackages',numberOfPackages);
                response.put('packagesWeight',packagesWeight);
                response.put('business', appName);
                response.put('currencyConvertor', currencyConvertor);
               // response.put('registrations', !regt.isEmpty() ? regt[0] : new Registrations__c());
            }catch(Exception e) {
                response.put('status','ERROR');
                response.put('msg',e.getMessage());
            }
        return response;
    }

    
    public static void namingConventionTransformation( List<Client_Address__c> cAddresses,List<Final_Delivery__c> finalDeliveries, Shipment_Order__c shipmentOrder, List<Registrations__c> regt){
        
      
        for(Client_Address__c cAddress : cAddresses) {
            if(String.isNotBlank(cAddress.Naming_Conventions__c)) {
                switch on cAddress.Naming_Conventions__c {

                    when 'Use Final Delivery Details' {
                        if(!finalDeliveries.isEmpty()) {
                            cAddress.CompanyName__c = finalDeliveries[0].Ship_to_address__r.CompanyName__c;
                            copyAddressFromFinal(cAddress,finalDeliveries[0]);
                        }
                    }
                    when 'CPA c/o Final Delivery Details' {
                        cAddress.CompanyName__c += ' c/o ';
                        if(!finalDeliveries.isEmpty()) {
                            cAddress.CompanyName__c += finalDeliveries[0].Ship_to_address__r.CompanyName__c;
                            copyAddressFromFinal(cAddress,finalDeliveries[0]);
                        }
                    }

                    when 'Client c/o CPA Details as per Below' {
                        cAddress.CompanyName__c = shipmentOrder.Account__r.Name + ' c/o';
                    }

                        /*when 'Client c/o CPA c/o Final Delivery Details' {
                        cAddress.CompanyName__c = shipmentOrder.Account__r.Name + ' c/o ' + cAddress.CompanyName__c + ' c/o ';
                        if(!finalDeliveries.isEmpty()){
                           cAddress.CompanyName__c += finalDeliveries[0].Ship_to_address__r.CompanyName__c;
                        }
                    }*/
                    
                     //when 'Client c/o CPA c/o Final Delivery Details' {
                        when 'Client c/o CPA c/o with Final Delivery Details' {
                        cAddress.CompanyName__c = shipmentOrder.Account__r.Name + ' c/o ' + cAddress.CompanyName__c + ' c/o ';
                        if(!finalDeliveries.isEmpty()){
                           cAddress.CompanyName__c += finalDeliveries[0].Ship_to_address__r.CompanyName__c;
                            System.debug('finalDeliveries[0] -->'+JSON.serializePretty(finalDeliveries[0]));
                            System.debug('cAddress -->'+JSON.serializePretty(cAddress));
                            copyAddressFromFinal(cAddress,finalDeliveries[0]);
                        }
                        }

                    when 'Beneficial Owner c/o Final Delivery Details' {
                        cAddress.CompanyName__c = shipmentOrder.Contact_name__c + ' c/o ';
                        if(!finalDeliveries.isEmpty()) {
                            cAddress.CompanyName__c += finalDeliveries[0].Ship_to_address__r.CompanyName__c;
                            copyAddressFromFinal(cAddress,finalDeliveries[0]);
                        }
                    }

                    when 'Beneficial Owner c/o Use CPA Details as per Below' {
                        //cAddress.CompanyName__c = shipmentOrder.Contact_name__c + ' c/o '; previous values
                        cAddress.CompanyName__c = shipmentOrder.Contact_name__c + ' c/o '+cAddress.CompanyName__c;
                    }

                    // Story Id : 4246
                    when 'VAT Claiming Entity c/o with CPA details below' {
                        //cAddress.CompanyName__c = 'VAT Claiming Entity c/o';
                        cAddress.CompanyName__c = 'VAT Claiming Entity c/o ' + cAddress.CompanyName__c;
                        //if(!finalDeliveries.isEmpty()){
                        //    cAddress.CompanyName__c += finalDeliveries[0].Ship_to_address__r.CompanyName__c;
                        //}
                        
                    }

                    when 'VAT Claiming Entity (Tax#) c/o with Final Delivery Details' {
                        cAddress.CompanyName__c = 'VAT Claiming Entity c/o ';
                        if(!finalDeliveries.isEmpty()) {
                             cAddress.CompanyName__c += finalDeliveries[0].Ship_to_address__r.CompanyName__c;
                            copyAddressFromFinal(cAddress,finalDeliveries[0]);
                        }
                    }

                    when 'VAT Claiming Entity (Tax#) c/o CPA c/o with Final Delivery Details' {
                        cAddress.CompanyName__c = 'VAT Claiming Entity c/o ' + cAddress.CompanyName__c + ' c/o ';
                        if(!finalDeliveries.isEmpty()) {
                            cAddress.CompanyName__c += finalDeliveries[0].Ship_to_address__r.CompanyName__c;
                            copyAddressFromFinal(cAddress,finalDeliveries[0]);
                        }
                    }
                    
                    //added CH 13108
                    when 'CPA Final Delivery Details' {
                        if(!finalDeliveries.isEmpty()) {
                            cAddress.CompanyName__c += ' '+ finalDeliveries[0].Ship_to_address__r.CompanyName__c;
                            copyAddressFromFinal(cAddress,finalDeliveries[0]);
                        }
                    }
                    //end CH 13108

                    when 'TecEx c/o Final Delivery Company' {
                        cAddress.CompanyName__c = 'TecEx c/o ';
                        if(!finalDeliveries.isEmpty()) {
                            cAddress.CompanyName__c += finalDeliveries[0].Ship_to_address__r.CompanyName__c;
                            copyAddressFromFinal(cAddress,finalDeliveries[0]);
                        }
                    }
when 'Client Registration Address' {
                        
                        if(!regt.isEmpty()){
                            cAddress.CompanyName__c = regt[0].Name;
                            copyAddressFromRegistrations(cAddress, regt[0]);
                        }
                    }
                    when 'Client Registration, Tax # c/o Final Delivery Details' {
                        
                        if(!regt.isEmpty()){
                            cAddress.CompanyName__c = regt[0].Name + ' (' + regt[0].VAT_number__c + ') c/o ';
                            if(!finalDeliveries.isEmpty()){
                             	copyAddressFromFinal(cAddress, finalDeliveries[0]);   
                            }
                        }                        
                    }
                }
            }
        }
    }

    //this method will copy values in clientadddress from final address.
    public static void copyAddressFromFinal(Client_Address__c clientAddress, Final_Delivery__c finalAddress){

        clientAddress.Address__c = getNullSafeValue(finalAddress.Ship_to_address__r.Address__c);
        clientAddress.Address2__c = getNullSafeValue(finalAddress.Ship_to_address__r.Address2__c);
        clientAddress.City__c = getNullSafeValue(finalAddress.Ship_to_address__r.City__c);
        clientAddress.Province__c = '';// This field not present on finalAddress object.
        clientAddress.Postal_Code__c = getNullSafeValue(finalAddress.Ship_to_address__r.Postal_Code__c);
        clientAddress.All_Countries__c = getNullSafeValue(finalAddress.Ship_to_address__r.All_Countries__c);
        clientAddress.Contact_Full_Name__c = getNullSafeValue(finalAddress.Ship_to_address__r.Contact_Full_Name__c);
        clientAddress.Contact_Email__c = getNullSafeValue(finalAddress.Ship_to_address__r.Contact_Email__c);
        clientAddress.Contact_Phone_Number__c = getNullSafeValue(finalAddress.Ship_to_address__r.Contact_Phone_Number__c);
        clientAddress.AdditionalContactNumber__c = '';
        clientAddress.Tax_Name_Number__c = getNullSafeValue(finalAddress.Ship_to_address__r.Tax_Name_Number__c);
    }
    
	//this method will copy values in clientadddress from registrations address.
	public static void copyAddressFromRegistrations(Client_Address__c clientAddress, Registrations__c regtAddress){
        
        clientAddress.Address__c = getNullSafeValue(regtAddress.Registered_Address__c);
        clientAddress.Address2__c = getNullSafeValue(regtAddress.Registered_Address_2__c);
        clientAddress.City__c = getNullSafeValue(regtAddress.Registered_Address_City__c);
        clientAddress.Province__c = getNullSafeValue(regtAddress.Registered_Address_Province__c);
        clientAddress.Postal_Code__c = getNullSafeValue(regtAddress.Registered_Address_Postal_Code__c);
        clientAddress.All_Countries__c = ''; // no field on Registrations__c object
        clientAddress.Contact_Full_Name__c = '';
        clientAddress.Contact_Email__c = '';
        clientAddress.Contact_Phone_Number__c = '';
        clientAddress.AdditionalContactNumber__c = '';
        clientAddress.Tax_Name_Number__c = 'VAT No: '+ getNullSafeValue(regtAddress.VAT_number__c);
    }    
    
    // this method will take associated client addresses and transform as per the naming convention logic.
    /*public static void namingConventionTransformation( List<Client_Address__c> cAddresses,List<Final_Delivery__c> finalDeliveries, Shipment_Order__c shipmentOrder){
        
      
        for(Client_Address__c cAddress : cAddresses) {
            if(String.isNotBlank(cAddress.Naming_Conventions__c)) {
                switch on cAddress.Naming_Conventions__c {

                    when 'Use Final Delivery Details' {
                        if(!finalDeliveries.isEmpty()) {
                            cAddress.CompanyName__c = finalDeliveries[0].Company_Name__c;
                            copyAddressFromFinal(cAddress,finalDeliveries[0]);
                        }
                    }
                    when 'CPA c/o Final Delivery Details' {
                        cAddress.CompanyName__c += ' c/o ';
                        if(!finalDeliveries.isEmpty()) {
                            cAddress.CompanyName__c += finalDeliveries[0].Company_Name__c;
                            copyAddressFromFinal(cAddress,finalDeliveries[0]);
                        }
                    }

                    when 'Client c/o CPA Details as per Below' {
                        cAddress.CompanyName__c = shipmentOrder.Account__r.Name + ' c/o';
                    }

                    when 'Client c/o CPA c/o Final Delivery Details' {
                        cAddress.CompanyName__c = shipmentOrder.Account__r.Name + ' c/o ' + cAddress.CompanyName__c + ' c/o ';
                        if(!finalDeliveries.isEmpty()){
                           cAddress.CompanyName__c += finalDeliveries[0].Company_Name__c;
                        }
                    }

                    when 'Beneficial Owner c/o Final Delivery Details' {
                        cAddress.CompanyName__c = shipmentOrder.Contact_name__c + ' c/o ';
                        if(!finalDeliveries.isEmpty()) {
                            cAddress.CompanyName__c += finalDeliveries[0].Company_Name__c;
                            copyAddressFromFinal(cAddress,finalDeliveries[0]);
                        }
                    }

                    when 'Beneficial Owner c/o Use CPA Details as per Below' {
                        cAddress.CompanyName__c = shipmentOrder.Contact_name__c + ' c/o';
                    }

                    when 'VAT Claiming Entity c/o with CPA details below' {
                        cAddress.CompanyName__c = 'VAT Claiming Entity c/o ';
                        if(!finalDeliveries.isEmpty()){
                            cAddress.CompanyName__c += finalDeliveries[0].Company_Name__c;
                        }
                    }

                    when 'VAT Claiming Entity (Tax#) c/o with Final Delivery Details' {
                        cAddress.CompanyName__c = 'VAT Claiming Entity c/o ';
                        if(!finalDeliveries.isEmpty()) {
                             cAddress.CompanyName__c += finalDeliveries[0].Company_Name__c;
                            copyAddressFromFinal(cAddress,finalDeliveries[0]);
                        }
                    }

                    when 'VAT Claiming Entity (Tax#) c/o CPA c/o with Final Delivery Details' {
                        cAddress.CompanyName__c = 'VAT Claiming Entity c/o ' + cAddress.CompanyName__c + ' c/o ';
                        if(!finalDeliveries.isEmpty()) {
                            cAddress.CompanyName__c += finalDeliveries[0].Company_Name__c;
                            copyAddressFromFinal(cAddress,finalDeliveries[0]);
                        }
                    }

                    when 'TecEx c/o Final Delivery Company' {
                        cAddress.CompanyName__c = 'TecEx c/o ';
                        if(!finalDeliveries.isEmpty()) {
                            cAddress.CompanyName__c += finalDeliveries[0].Company_Name__c;
                            copyAddressFromFinal(cAddress,finalDeliveries[0]);
                        }
                    }
                }
            }
        }
    }

    //this method will copy values in clientadddress from final address.
    public static void copyAddressFromFinal(Client_Address__c clientAddress, Final_Delivery__c finalAddress){

        clientAddress.Address__c = getNullSafeValue(finalAddress.Address_Line_1__c);
        clientAddress.Address2__c = getNullSafeValue(finalAddress.Address_Line_2__c) + (finalAddress.Address_Line_3__c != NULL ? ', ' + finalAddress.Address_Line_3__c : '');// Address 3 not found on clientAddress so concatination done.
        clientAddress.City__c = getNullSafeValue(finalAddress.City__c);
        clientAddress.Province__c = '';// This field not present on finalAddress object.
        clientAddress.Postal_Code__c = getNullSafeValue(finalAddress.Zip__c);
        clientAddress.All_Countries__c = getNullSafeValue(finalAddress.Country__c);
        clientAddress.Contact_Full_Name__c = getNullSafeValue(finalAddress.Contact_name__c);
        clientAddress.Contact_Email__c = getNullSafeValue(finalAddress.Contact_email__c);
        clientAddress.Contact_Phone_Number__c = getNullSafeValue(finalAddress.Contact_number__c);
        clientAddress.AdditionalContactNumber__c = '';
        clientAddress.Tax_Name_Number__c = getNullSafeValue(finalAddress.Tax_Name__c) + (finalAddress.Tax_Id__c != NULL ? ' - ' + finalAddress.Tax_Id__c : '');
    }
*/

    public static string getNullSafeValue(String val){
        if(val != null) {
            return val;
        }
        return '';
    }

    private static Boolean isSORecordTypeIsZee(Id RecordTypeId){
        
        List<Id> zeeRecordTypes = new List<Id>{
            Schema.SObjectType.Shipment_Order__c.getRecordTypeInfosByDeveloperName().get('Zee_Cost_Estimate').getRecordTypeId(),
                Schema.SObjectType.Shipment_Order__c.getRecordTypeInfosByDeveloperName().get('Zee_Shipment_Order').getRecordTypeId()
                };
                    
                    if(zeeRecordTypes.contains(RecordTypeId)){
                        return true;
                    }
        else{
            return false;
        }
    }
    
    @AuraEnabled
    public static Map<String,Object> saveCI(String blobData,String soId,String soNumber,String type,String additionalColumnData, String vatValue, String portValue){

        
        Map<String,Object> response = new Map<String,Object> {'status'=>'OK','msg'=>'Attachment saved successfully!'};
        try{
            Blob fileContent;

            if(String.isNotBlank(additionalColumnData)){
              List<Part__c> lineItemsToUpdate = (List<Part__c>) JSON.deserialize(additionalColumnData, List<Part__c>.class);
              update lineItemsToUpdate;
            }
            Set<ContentDocument> listToDelete = new Set<ContentDocument>();
            for(ContentVersion conver : [SELECT Id,ContentDocumentId FROM ContentVersion WHERE Title like '%CI-%' AND FirstPublishLocationId =:soId]) {
                listToDelete.add(new ContentDocument( Id = conver.ContentDocumentId));
            }
            delete new List<ContentDocument>(listToDelete);

            if(type == 'pdf'){
               savePDFAttchment(soId, vatValue, portValue, soNumber);
            }else{
                //Commercial Invoice
                fileContent = EncodingUtil.base64Decode(blobData);
                ContentVersion contentVersion = new ContentVersion(
                    versionData = fileContent,
                    title = 'CI-' + soNumber +'-'+(System.now()).date().format(),
                    pathOnClient = 'CI-' + soNumber +'-'+(System.now()).date().format()+'.'+type,
                    FirstPublishLocationId =  soId);
                insert contentVersion;  
            }

           
        }catch(Exception e) {
            response.put('status','ERROR');
            response.put('msg',e.getMessage());
        }
        return response;
    }

    @Future(callout=true)
    public static void savePDFAttchment(String soId, String vatValue, String portValue, String soNumber) {   
        System.PageReference pageRef = new System.PageReference('/apex/tecexCI_V2');
        pageRef.getParameters().put('id',soId);
        pageRef.getParameters().put('vatValue',vatValue);
        pageRef.getParameters().put('portValue',portValue);  
        
        Blob fileContent;
        if(Test.isRunningTest()) { 
            fileContent = blob.valueOf('Unit.Test');
        } else {
            fileContent = pageRef.getContentAsPDF();
        }
        
        ContentVersion contentVersion = new ContentVersion(
                versionData = fileContent,
                title = 'CI-' + soNumber +'-'+(System.now()).date().format(),
                pathOnClient = 'CI-' + soNumber +'-'+(System.now()).date().format()+'.'+'pdf',
                FirstPublishLocationId =  soId);
       insert contentVersion;
        
    }
    
}