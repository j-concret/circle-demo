/**
 * Created by Chibuye Kunda on 2018/12/21.
 */

/** this class is controller for our picklist component
 */
public with sharing class LEX_PickListController {

    /** this function will return picklist values
     *  @param objectP is the object which has the picklist
     *  @param field_nameP is the name of the picklist field
     */
    @AuraEnabled
    public static List< String > getPickListValues( sObject objectP, String field_nameP ){

        List< String > picklist_value_list = new List< String >();              //this is list of picklist values
        Schema.sObjectType object_type = objectP.getSObjectType();              //get the sObject type
        Schema.DescribeSObjectResult object_describe = object_type.getDescribe();       //get object description
        Map< String, Schema.SObjectField > field_map = object_describe.fields.getMap();         //get the field map
        List< Schema.PicklistEntry > values_list = field_map.get( field_nameP ).getDescribe().getPicklistValues();      //get picklist values

        //add values to picklist value list
        for( Schema.PicklistEntry current_entry : values_list )
            picklist_value_list.add( current_entry.getValue() );

        picklist_value_list.sort();

        return picklist_value_list;

    }//end of function definition

}//end of class definition