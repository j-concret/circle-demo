@RestResource(urlMapping='/UpdateSOOrderPackage/*')
Global class CPUpdateSOOrderPackage {
    
     @Httpput
    global static void UpdateSOOrderPackage(){
    
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        Blob body = req.requestBody;
    	res.addHeader('Content-Type', 'application/json');
        String requestString = body.toString();
        CPUpdateSOOrderPackWrapper rw = (CPUpdateSOOrderPackWrapper)JSON.deserialize(requestString,CPUpdateSOOrderPackWrapper.class);
        Shipment_Order_Package__c SOP = [select id,Weight_Unit__c,Dimension_Unit__c,packages_of_same_weight_dims__c,Length__c,Height__c,Breadth__c,Actual_Weight__c from Shipment_Order_Package__c where id=:rw.SOPID];
           try {
           
            SOP.Weight_Unit__c= rw.Weight_Unit;
            SOP.Dimension_Unit__c = rw.Dimension_Unit;
            SOP.packages_of_same_weight_dims__c = rw.Packages_of_Same_Weight;
            SOP.Length__c=decimal.Valueof(rw.Length);
            SOP.Height__c=decimal.Valueof(rw.Height);
            SOP.Breadth__c=decimal.Valueof(rw.Breadth);
            SOP.Actual_Weight__c=decimal.Valueof(rw.Actual_Weight);
                        
            Update SOP;
            
            String ASD = 'Success- Shipment Order Package Updated';	
            res.responseBody = Blob.valueOf(JSON.serializePretty(ASD));
            res.statusCode = 200;
              	API_Log__c Al = New API_Log__c();
                Al.Account__c=rw.ClientID;
                Al.Login_Contact__c=rw.ContactID;
                Al.EndpointURLName__c='UpdateSOOrderPackage';
                Al.Response__c='Success- Shipment Order Package Updated';
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al; 
            
            }
        	catch (Exception e){
                
              	String ErrorString ='Something went wrong while updating Shipment order packages details, please contact support_SF@tecex.com';
               	res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
            	res.statusCode = 500;
                API_Log__c Al = New API_Log__c();
                Al.Account__c=rw.ClientID;
                Al.Login_Contact__c=rw.ContactID;
                Al.EndpointURLName__c='UpdateSOOrderPackage';
                Al.Response__c=e.getMessage()+' at line number: '+e.getLineNumber();
                Al.StatusCode__c=string.valueof(res.statusCode);
              	Insert Al;

            
        	}

        
        
    }
    

}