public with sharing class AttachmentUploadController {


   public Shipment_Order__c shipmentOrder {get;set;}
   public String Title {get;set;}
   public Blob file_Body {get;set;}
   public String file_Name {get;set;}
   public String body {get;set;}
  
    
    //constructor
    
    public AttachmentUploadController (ApexPages.StandardController controller) {
        this.shipmentOrder =  (Shipment_Order__c)controller.getRecord();
        //this.attachmentObject = [select id, Initial_HTTP_Response__c, Validation_HTTP_Response__c  from attachment__c where Shipment_Order__c =: shipmentOrder.Id Limit 1];                           
    }
   String recordId = null;

 // creates a new Attachment__c record
    private Database.SaveResult saveCustomAttachment() {
        Attachment__c obj = new Attachment__c ();
        obj.Shipment_Order__c = shipmentOrder.Id; 
        Database.SaveResult obj1 = Database.insert(obj);
        recordId = obj.Id;
        
        System.Debug('----var attachmentId:'+recordId);
        return obj1;  
    }
    
    // create an actual Attachment record with the Attachment__c as parent
    private Database.SaveResult saveStandardAttachment(Id parentId) {
        Database.SaveResult result;
        Attachment attachment = new Attachment();
        attachment.body = this.file_Body;
        attachment.name = this.file_Name;
        attachment.parentId = parentId;
        // insert the attachment
        result = Database.insert(attachment);
        // reset the file for the view state
        file_Body = Blob.valueOf('attachment.body');

        return result;
    }
    
    
    /**
    * Upload process is:
    *  1. Insert new Contact_Attachment__c record
    *  2. Insert new Attachment with the new Contact_Attachment__c record as parent
    *  3. Update the Contact_Attachment__c record with the ID of the new Attachment
    **/
    public PageReference processUpload() {
            Database.SaveResult customAttachmentResult = saveCustomAttachment();
        
            if (customAttachmentResult == null || !customAttachmentResult.isSuccess()) {
                ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 
                  'Could not save attachment.'));
                return null;
            }
        
            Database.SaveResult attachmentResult = saveStandardAttachment(customAttachmentResult.getId());
        
            if (attachmentResult == null || !attachmentResult.isSuccess()) {
                ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 
                  'Could not save attachment.'));            
                return null;
            } else {
                // update the custom attachment record with some attachment info
                Attachment__c customAttachment = [select id from Attachment__c where id = :customAttachmentResult.getId()];
                
            update customAttachment;
           }
            
           
      return new PageReference('/apex/Attachment?id='+shipmentOrder.Id +recordId);
    }
    
    
    public PageReference Ephesoft() {
    Attachment__c newAttachment = [select id, Initial_HTTP_Response__c, Validation_HTTP_Response__c  from attachment__c where Id =:recordId ]; 
    
    String serviceResBody = newAttachment.Initial_HTTP_Response__c;
    String  newXMLString = serviceResBody.replace('UploadResponse', 'BatchStatusRequest');
    System.debug('The new string::: '+newXMLString ); 
        
       
     HttpRequest req = new HttpRequest();
      req.setHeader('Content-Type','application/xml');
      req.setMethod('POST');
      req.setEndpoint('http://34.244.26.115:8080/tecexsfws/getBatchStatus');
      req.setTimeout(120000);
      req.setbody(newXMLString );
      

     Http http = new Http();
          HTTPResponse res = http.send(req);
          system.debug('Response====>>>>'+res);
          system.debug('ResponseBody=====>>>>>'+res.getBody());
          
            updateEphesoftResponse.updateAtt(recordId , res.getBody());

return null;
    
    }    
    
   

}