public class CTRL_ShipmentOrderOld_Body_Component
{
    public ID SOID {get; set;}
    public Shipment_Order__c SO
    {
        get;
        set
        {
            if (SO == null)
            {
                SO = value;
                initialize();
            }
        }
    }
    public List<Wrapper> costs {get; set;}

    public CTRL_ShipmentOrderOld_Body_Component()
    {
    }

   private void initialize()
    {
        List<Shipment_Order__c> shipmentOrders = getShipmentOrder(SO.Id);
        Shipment_Order__c shipmentOrder = shipmentOrders[0];
        costs = new List<Wrapper>();

        for (String breakDown : breakDowns.keySet())
        {
            Decimal amount = breakDowns.get(breakDown) == '' || breakDowns.get(breakDown) == null ? 0 : (Decimal) shipmentOrder.get(breakDowns.get(breakDown));
            Decimal amountSupplierActual = breakDownsSupplierActuals.get(breakDown) == '' || breakDownsSupplierActuals.get(breakDown) == null ? 0 : (Decimal) shipmentOrder.get(breakDownsSupplierActuals.get(breakDown));
            Decimal amountCI = breakDownsCI.get(breakDown) == '' || breakDownsCI.get(breakDown) == null ? 0 : (Decimal) shipmentOrder.get(breakDownsCI.get(breakDown));
            Decimal amountFR = breakDownsFR.get(breakDown) == '' || breakDownsFR.get(breakDown) == null ? 0  : (Decimal) shipmentOrder.get(breakDownsFR.get(breakDown));

            if (amountCI == null)
                amountCI = 0;

            if (breakDown.equals('Total:'))
            {
                System.debug('Getting totals');
                Decimal amountTotal = 0;
                Decimal amountSupplierActualTotal = 0;
                Decimal amountCIamountTotal = 0;
                Decimal amountFRamountTotal = 0;
                
                for (Wrapper wrp : costs)
                {
                    amountTotal += Decimal.valueOf(extractAmountOnly(wrp.amount));
                    amountSupplierActualTotal += Decimal.valueOf(extractAmountOnly(wrp.amountSupplierActual));
                    amountCIamountTotal += Decimal.valueOf(extractAmountOnly(wrp.amountCI));
                    amountFRamountTotal += Decimal.valueOf(extractAmountOnly(wrp.amountFR));
                }
                costs.Add(new Wrapper(breakDown, amountTotal, amountSupplierActualTotal, amountCIAmountTotal, amountFRAmountTotal));
            }
            else
                costs.Add(new Wrapper(breakDown, amount, amountSupplierActual, amountCI, amountFR));

        }
    }

    private String extractAmountOnly(String amount)
    {
        return amount.replace('$', '').replace(',','').remove(' ');
    }


    private Map<String, String> breakDowns = new Map<String, String>
    {
        'IOR/EOR and Import Compliance Fee' => 'FC_IOR_and_Import_Compliance_Fee_USD__c',
        'Admin Fee' => 'FC_Admin_Fee__c',
        'Bank Fees' => 'FC_Bank_Fees__c',
        'Total - Customs Brokerage Cost' => 'FC_Total_Customs_Brokerage_Cost__c',
        'Total - Handling Costs' => 'FC_Total_Handling_Costs__c',
        'Total - Clearance Costs' => 'FC_Total_Clearance_Costs__c',
        'Total - License Cost' => 'FC_Total_License_Cost__c',
        'International Delivery Fee' => 'FC_International_Delivery_Fee__c',
        'Liability Cover Fee' => 'FC_Insurance_Fee_USD__c',
        'Recharge - Tax and Duty' => 'IOR_Refund_of_Tax_Duty__c',
        'Cash Outlay Fee' => 'FC_Finance_Fee__c',
        'Miscellaneous Fee' => 'FC_Miscellaneous_Fee__c',
        'Collection Administration Fee' => '',
        'Tax Recovery Premium' => '',     
        'Total:' => 'FC_Total__c'

    };

    private Map<String, String> breakDownsSupplierActuals = new Map<String, String>
    {
        'IOR/EOR and Import Compliance Fee' => 'Actual_Total_IOR_EOR__c',
        'Admin Fee' => 'Actual_Admin_Fee__c',
        'Bank Fees' => 'Actual_Bank_Fees__c',
        'Total - Customs Brokerage Cost' => 'Actual_Total_Customs_Brokerage_Cost__c',
        'Total - Handling Costs' => 'Actual_Total_Handling_Costs__c',
        'Total - Clearance Costs' => 'Actual_Total_Clearance_Costs__c',
        'Total - License Cost' => 'Actual_Total_License_Cost__c',
        'International Delivery Fee' => 'Actual_International_Delivery_Fee__c',
        'Liability Cover Fee' => 'Actual_Insurance_Fee_USD__c',
        'Recharge - Tax and Duty' => 'Actual_Total_Tax_and_Duty__c',
        'Cash Outlay Fee' => 'Actual_Finance_Fee__c',
        'Miscellaneous Fee' => 'Actual_Miscellaneous_Fee__c',
        'Collection Administration Fee' => '',
        'Tax Recovery Premium' => '', 
        'Total:' => 'Total_actual_costs__c'
    };

    private Map<String, String> breakDownsCI = new Map<String, String>
    {
        'IOR/EOR and Import Compliance Fee' => 'CI_IOR_and_Import_Compliance_Fee_USD__c',
        'Admin Fee' => 'CI_Admin_Fee__c',
        'Bank Fees' => 'CI_Bank_Fees__c',
        'Total - Customs Brokerage Cost' => 'CI_Total_Customs_Brokerage_Cost__c',
        'Total - Handling Costs' => 'CI_Total_Handling_Cost__c',
        'Total - Clearance Costs' => 'CI_Total_Clearance_Costs__c',
        'Total - License Cost' => 'CI_Total_Licence_Cost__c',
        'International Delivery Fee' => 'CI_International_Delivery_Fee__c',
        'Liability Cover Fee' => 'CI_Insurance_Fee_USD__c',
        'Recharge - Tax and Duty' => 'CI_Recharge_Tax_and_Duty__c',
        'Cash Outlay Fee' => 'CI_Finance_Fee__c',
        'Miscellaneous Fee' => 'CI_Miscellaneous_Fee__c',
        'Collection Administration Fee' => 'CI_Collection_Administration_Fee__c',
        'Tax Recovery Premium' => '', 
        'Total:' => 'CI_Total__c'
    };

    private Map<String, String> breakDownsFR = new Map<String, String>
    {
        'IOR/EOR and Import Compliance Fee' => 'IOR_FEE_USD__c',
        'Admin Fee' => 'Handling_and_Admin_Fee__c',
        'Bank Fees' => 'Bank_Fees__c',
        'Total - Customs Brokerage Cost' => 'Total_Customs_Brokerage_Cost__c',
        'Total - Handling Costs' => 'Recharge_Handling_Costs__c',
        'Total - Clearance Costs' => 'Total_clearance_Costs__c',
        'Total - License Cost' => 'Recharge_License_Cost__c',
        'International Delivery Fee' => 'International_Delivery_Fee__c',
        'Liability Cover Fee' => 'Insurance_Fee_USD__c',
        'Recharge - Tax and Duty' => 'Recharge_Tax_and_Duty__c',
        'Cash Outlay Fee' => 'Finance_Fee__c',
        'Miscellaneous Fee' => 'Miscellaneous_Fee__c',
        'Collection Administration Fee' => 'Collection_Administration_Fee__c',
        'Tax Recovery Premium' => '', 
        'Total:' => 'Total_Invoice_Amount__c'
    };

    public class Wrapper
    {
        public String breakDown {get; set;}
        
        
        
        public String amount {get; set;}
        public String amountSupplierActual {get; set;}
        public String amountCI {get; set;}
        public String amountFR {get; set;}
        public String amountForecastFees {get; set;}
        public String amountDifference1{get; set;}
        public String amountDifference2{get; set;}
        public String amountDifference3{get; set;}
        public String profit{get; set;}
        public String invoiceCheck {get; set;}
        public String actualNetFees {get; set;}

        public Wrapper(String breakDown, Decimal amount, Decimal amountSupplierActual, Decimal amountCI, Decimal amountFR)
        {
            system.debug(breakDown + ' - ' + amount  + ' - ' +  amountSupplierActual  + ' - ' +  amountCI);
            this.breakDown = breakDown;
            this.amount = (amount == null ? format(0) : format(amount.setscale(0)));
            this.amountSupplierActual = (amountSupplierActual == null ? format(0) : format(amountSupplierActual.setscale(0)));
            this.amountCI =  (amountCI == null ? format(0) : format(amountCI.setscale(0)));
            this.amountFR =  (amountFR == null ? format(0) : format(amountFR.setscale(0)));
            Decimal twoMinusOne =   (amountSupplierActual == null ? 0 : amountSupplierActual ) - (amount == null ? 0 : amount); 
            this.amountDifference1 = format(twoMinusOne.setscale(0));
            Decimal fourMinusThree =   (amountCI == null ? 0 : amountCI) - (amountFR == null ? 0 : amountFR ); 
            this.amountDifference2 = format(fourMinusThree.setscale(0));
            Decimal forecastFees = (amountFR == null ? 0 : amountFR) - (amount == null ? 0 : amount);
            this.amountForecastFees = format(forecastFees.setscale(0));

            System.debug('amountForecastFees: ' + amountForecastFees);
            Decimal actualNetFees = (amountCI == null ? 0: amountCI) - (amountSupplierActual == null ? 0 : amountSupplierActual);
            this.actualNetFees = format(actualNetFees.setscale(0));
            System.debug('actualNetFees: ' + actualNetFees);

            Decimal invoiceCheck = (amountCI == null ? 0 : amountCI) - (amountFR == null ? 0 : amountFR);
            this.invoiceCheck = format(invoiceCheck);
            System.debug('invoiceCheck: ' + invoiceCheck);
            
            Decimal sixMinusFive =   (actualNetFees == null ? 0 : actualNetFees ) - (forecastFees == null ? 0 : forecastFees ); 
            this.amountDifference3 = format(sixMinusFive.setscale(0));
            
            Decimal profitCalculation = amountSupplierActual != 0 && amountSupplierActual != null ?  (actualNetFees / amountSupplierActual) * 100 :  actualNetFees == 0 && amountSupplierActual  == 0 ? 0 :100;
            this.profit = percentageFormat(profitCalculation.setscale(0));
        }

        private String format(Decimal amount)
        {
            return '$ ' + amount.format();
        }
        
        
         private String percentageFormat(Decimal amount)
        {
            return  amount.format() + '%';
        }
    }

    private List<Shipment_Order__c> getShipmentOrder(ID SOID)
    {
        return [
                SELECT
                FC_Admin_Fee__c,
                FC_Bank_Fees__c,
                Total_IOR_Costs__c ,
                FC_Total_Customs_Brokerage_Cost__c,
                FC_Total_Handling_Costs__c,
                FC_Total_Clearance_Costs__c,
                FC_Total_License_Cost__c,
                FC_International_Delivery_Fee__c,
                FC_Insurance_Fee_USD__c ,
                SupplierInvoice_Total_IOR_Cost__c,
                SupplierInvoice_Total_Customs_Brokerage__c,
                SupplierInvoice_Total_Customs_Handling__c,
                SupplierInvoice_Total_Customs_Clearance__c,
                SupplierInvoice_Total_License_Inspection__c,
                SupplierInvoice_Total_Int_l_Freight__c,
                SupplierInvoice_Total_Insurance__c,
                FC_Miscellaneous_Fee__c,
                Sum_of_Duties_and_Taxes_Invoiced__c,
                Sum_of_First_Invoices__c,
                Sum_of_Sent_or_Paid_First_Invoices__c,
                Top_up_Invoice_Credits__c,
                Total_Receipted__c,
                SupplierInvoice_Total_Tax_and_Duties__c,
                CI_Admin_Fee__c,
                CI_Bank_Fees__c,
                CI_delivery_address__c,
                CI_Finance_Fee__c,
                CI_Insurance_Fee_USD__c,
                CI_International_Delivery_Fee__c,
                CI_IOR_and_Import_Compliance_Fee_USD__c,
                CI_Miscellaneous_Fee__c,
                CI_Recharge_Tax_and_Duty__c,
                CI_Total_Clearance_Costs__c,
                CI_Total_Customs_Brokerage_Cost__c,
                CI_Total_Handling_Cost__c,
                CI_Total_Licence_Cost__c,
                IOR_FEE_USD__c,
                Handling_and_Admin_Fee__c,
                Bank_Fees__c,
                Total_Customs_Brokerage_Cost__c,
                Total_clearance_Costs__c,
                Recharge_License_Cost__c,
                International_Delivery_Fee__c,
                Insurance_Fee_USD__c,
                Recharge_Tax_and_Duty__c,
                Finance_Fee__c,
                Miscellaneous_Fee__c,
                Total_Invoice_Amount__c,
                Total_Supplier_Cost__c,
                CI_Total__c, 
                FC_Total__c,
                FC_IOR_and_Import_Compliance_Fee_USD__c,
                FC_Finance_Fee__c,
                FC_Recharge_Tax_and_Duty__c,
                CI_Collection_Administration_Fee__c,
                Collection_Administration_Fee__c,
                IOR_Refund_of_Tax_Duty__c,
                Actual_Admin_Fee__c , 
                Actual_Bank_Fees__c , 
                Actual_Exchange_gain_loss_on_payment__c , 
                Actual_Finance_Fee__c , 
                Actual_Insurance_Fee_USD__c , 
                Actual_International_Delivery_Fee__c , 
                Actual_Miscellaneous_Fee__c , 
                Actual_Total_Clearance_Costs__c , 
                Actual_Total_Customs_Brokerage_Cost__c , 
                Actual_Total_Handling_Costs__c , 
                Actual_Total_IOR_EOR__c , 
                Actual_Total_License_Cost__c , 
                Actual_Total_Tax_and_Duty__c ,
                Total_actual_costs__c,
                Recharge_Handling_Costs__c

                
                FROM Shipment_Order__c WHERE Id =: SOID];
    }

}