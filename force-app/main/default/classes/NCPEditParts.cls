@RestResource(urlMapping='/NCPEditParts/*')
Global class NCPEditParts {
    
    @Httppost
    global static void CreateParts(){
        
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        Blob body = req.requestBody;
        res.addHeader('Content-Type', 'application/json');
        String requestString = body.toString();
        NCPEditPartsWrapper rw = (NCPEditPartsWrapper)JSON.deserialize(requestString,NCPEditPartsWrapper.class);
        try {
            List<Access_token__c> at = [SELECT status__c,Access_Token__c FROM Access_token__c WHERE Access_Token__c =: rw.Accesstoken and status__c = 'Active' LIMIT 1];
            
            If(!rw.Parts.isEmpty() && !at.isEmpty()){
                
                list<Part__c > ExistingParts = new list<Part__c >();
                list<Part__c > NewParts = new list<Part__c >();
                
                
                
                ExistingParts = [Select Name,shipment_order__c,Description_and_Functionality__c,Quantity__c,Commercial_Value__c,US_HTS_Code__c,Country_of_Origin2__c,ECCN_NO__c,Type_of_Goods__c,Li_ion_Batteries__c,Lithium_Battery_Types__c from Part__c  where shipment_order__c =:rw.SOID ];
                
                if(ExistingParts.size()>0) {
                    RecusrsionHandler.skipTriggerExecution=true;
                    delete ExistingParts;
                }
                
                for(Integer i=0;rw.Parts.size()>i;i++) {
                    
                    Part__c PA = new Part__c(
                        
                        Name =rw.Parts[i].PartNumber,
                        Description_and_Functionality__c =rw.Parts[i].PartDescription,
                        Quantity__c =rw.Parts[i].Quantity,
                        Commercial_Value__c =rw.Parts[i].UnitPrice,
                        Shipment_Order__c=rw.SOID,
                        Client_Provided_Category__c=rw.Parts[i].category
                    );
                    
                    NewParts.add(PA);
                    
                }
                RecusrsionHandler.skipTriggerExecution = false;
                insert NewParts;
                
                List<Part__C> PAL1=[select Id,Name,Client_Provided_Category__c,Description_and_Functionality__c,Quantity__c,Commercial_Value__c,Shipment_Order__c,US_HTS_Code__c from Part__C where Id in :NewParts];
                res.responseBody = Blob.valueOf(JSON.serializePretty(PAL1));
                res.statusCode = 200;
                
                API_Log__c Al = New API_Log__c();
                Al.Account__c=rw.accountID;
                // Al.Login_Contact__c=So.Client_Contact_for_this_Shipment__c;
                Al.EndpointURLName__c='NCPEditParts';
                Al.Response__c='Success - NCP - Parts updated';
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al;
            }
            
        }
        Catch(Exception e){
            
            System.debug('Line number ====> '+e.getLineNumber() + 'Exception Message =====> '+e.getMessage());
            
            String ErrorString ='Something went wrong, Please contact sf';
            res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
            res.statusCode = 500;
            API_Log__c Al = New API_Log__c();
            Al.Account__c=rw.accountID;
            //Al.Login_Contact__c=So.Client_Contact_for_this_Shipment__c;
            Al.EndpointURLName__c='NCPEditParts';
            Al.Response__c='error - Parts not updated'+e.getLineNumber()+e.getMessage();
            Al.StatusCode__c=string.valueof(res.statusCode);
            Insert Al;
        }
    }
}