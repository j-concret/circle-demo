public class ZCPAddpartswra {
   Public string Accesstoken;
   Public string SOID;
   Public list<LineItem> LineItems;
   public class LineItem {
		public String partNumber;
		public String description;
        public Integer quantity;
        public Double unitPrice;
        public String hsCode;
        public String countryOfOrigin;
	}

}