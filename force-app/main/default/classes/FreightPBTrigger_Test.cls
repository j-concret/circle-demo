@isTest
public class FreightPBTrigger_Test {
    
    @testSetup
    public static void setUpData(){
        
        User userObj = [Select Id, Name, userName, Profile.Name, UserRole.name From user where Profile.Name = 'System Administrator' LIMIT 1];
        
        DefaultFreightValues__c df = new DefaultFreightValues__c();
        df.Name = 'Test';
        df.Courier_Base_Rate_KG__c = 12;
        df.Courier_Dangerous_Goods_premium__c = 12;
        df.Courier_Fixed_Charge__c = 12;
        df.Courier_Non_stackable_premium__c = 12;
        df.Courier_Oversized_premium__c = 12;
        df.FF_Base_Rate_KG__c = 12;
        df.FF_Dangerous_Goods_Fixed_Charge_Premium__c = 12;
        df.FF_Dangerous_Goods_premium__c = 12;
        df.FF_Dedicated_Pickup__c = 12;
        df.FF_Fixed_Charge__c = 12;
        df.FF_Non_stackable_Fixed_Charge_Premium__c = 12;
        df.FF_Non_stackable_premium__c = 12;
        df.FF_Oversized_Fixed_Charge_Premium__c = 12;
        df.FF_Oversized_premium__c = 12;
        insert df;
        
        CountryandRegionMap__c crm = new CountryandRegionMap__c();
        crm.Name = 'Brazil';
        crm.Country__c = 'Brazil';
        crm.European_Union__c = 'No';
        crm.Region__c = 'South America';
        
        insert crm;
        
        CountryandRegionMap__c crm1 = new CountryandRegionMap__c();
        crm1.Name = 'United States';
        crm1.Country__c = 'United States';
        crm1.European_Union__c = 'No';
        crm1.Region__c = 'Oceania';
        
        insert crm1;
        
        RegionalFreightValues__c rfm = new RegionalFreightValues__c();
        rfm.Name = 'Oceania to South America';
        rfm.Courier_Discount_Premium__c = 12;
        rfm.Courier_Fixed_Premium__c = 10;
        rfm.Destination__c = 'Brazil';
        rfm.FF_Fixed_Premium__c = 123;
        rfm.FF_Regional_Discount_Premium__c = 12;
        rfm.Origin__c = 'United States';
        rfm.Preferred_Courier_Id__c = 'test';
        rfm.Preferred_Courier_Name__c = 'test';
        rfm.Preferred_Freight_Forwarder_Id__c = 'test';
        rfm.Preferred_Freight_Forwarder_Name__c = 'test';
        rfm.Risk_Adjustment_Factor__c = 1;
        insert rfm;
        
        List<Account> accountToInsert = new List<Account>();
        accountToInsert.add(new Account (name='Acme1', Type ='Client',
                                         CSE_IOR__c = userObj.Id,
                                         Service_Manager__c = userObj.Id,
                                         Financial_Manager__c= userObj.Id,
                                         Financial_Controller__c= userObj.Id,
                                         Tax_recovery_client__c  = FALSE,
                                         Ship_From_Line_1__c = 'Ship_From_Line_1',
                                         Ship_From_Line_2__c = 'Ship_From_Line_2',
                                         Ship_From_Line_3__c = 'Ship_From_Line_3',
                                         Ship_From_City__c = 'Ship_From_City',
                                         Ship_From_Zip__c = 'Ship_From_Zip',
                                         Ship_From_Country__c = 'Ship_From_Country',
                                         Ship_From_Other_notes__c = 'Ship_From_Other',
                                         Ship_From_Contact_Name__c = 'Ship_From_Contact',
                                         Ship_From_Contact_Email__c = 'Ship_From_Email',
                                         Ship_From_Contact_Tel__c = 'Ship_From_Tel',
                                         Client_Address_Line_1__c = 'Client_Address_Line_1',
                                         Client_Address_Line_2__c = 'Client_Address_Line_2',
                                         Client_Address_Line_3__c = 'Client_Address_Line_3',
                                         Client_City__c = 'Client_City',
                                         Client_Zip__c = 'Client_Zip',
                                         Client_Country__c = 'Client_Country',
                                         Other_notes__c = 'Other_notes',
                                         Tax_Name__c = 'Tax_Name',
                                         Tax_ID__c = 'Tax_ID',
                                         Contact_Name__c = 'Contact_Name',
                                         Contact_Email__c = 'Contact_Email',
                                         Contact_Tel__c = 'Contact_Tel'
                                         ));



        accountToInsert.add(new Account (name='TecEx Prospective Client', Type ='Supplier', ICE__c = userObj.Id, RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('IOR/EOR Supplier').getRecordTypeId()));

        insert accountToInsert;

        List<Contact> ContactToInsert = new List<Contact>();
        ContactToInsert.add(new Contact (Client_Notifications_Choice__c = 'opt-in', email = 'test@test.com', lastname ='Testing Individual',  AccountId = accountToInsert[0].Id));


        ContactToInsert.add(new Contact (Client_Notifications_Choice__c = 'opt-in', email = 'test@test.com', lastname ='Testing supplier',  AccountId = accountToInsert[1].Id));
        insert ContactToInsert;

        IOR_Price_List__c iorpl = new IOR_Price_List__c ( Client_Name__c = accountToInsert[0].Id, Name = 'Brazil', IOR_Fee__c = 0.15, On_Charge_Mark_up__c = 0.15,
                                                          TecEx_Shipping_Fee_Markup__c =0.40, IOR_Min_Fee__c = 100,  Admin_Fee__c =100, Set_up_fee__c =100, Bank_Fees__c =200, Tax_Rate__c = 0.20,
                                                          Estimated_Customs_Brokerage__c =250, Estimated_Customs_Clearance__c =250, Estimated_Customs_Handling__c =250, Estimated_Import_License__c =250, Destination__c = 'Brazil' );
        insert iorpl;

        country_price_approval__c cpa = new country_price_approval__c( name ='Brazil', Billing_term__c = 'DAP/CIF - IOR pays',  Airwaybill_Instructions__c = 'User IOR Address', Supplier__c = accountToInsert[1].Id,
                                                                       In_Country_Specialist__c = userObj.Id, Clearance_Destination__c = 'Brazil', Preferred_Supplier__c = TRUE, Destination__c = 'Brazil' );
        insert cpa;

        Currency_Management2__c CM = new Currency_Management2__c (Name = 'US Dollar (USD)', Currency__c = 'US Dollar (USD)', ISO_Code__c = 'EUR', Conversion_Rate__c = 1);
        insert CM;

        Country__c country = new Country__c(Brokerage_Costs__c = 618, CIF_Adjustment_Factor__c = 0.035, CIF_Absolute_value_adjustment__c = 200, Clearance_Costs__c = 488,
                                            Country__c = 'Brazil', Name = 'Brazil', Handling_Costs__c = 1180, License_Permit_Costs__c = 0);
        insert country;

        Tax_Structure_Per_Country__c tax = new Tax_Structure_Per_Country__c(Applied_to_Value__c = 'CIF', Country__c = country.Id, Default_Duty_Rate__c = 0.5625,
                                                                            Order_Number__c = '1', Part_Specific__c = TRUE, Name = 'II', Tax_Type__c = 'Duties');
        insert tax;



        CPA_v2_0__c relatedCPA = new CPA_v2_0__c(Name = 'Brazil', Country_Applied_Costings__c=true );
        insert relatedCPA; 

        CPA_v2_0__c cpav2 = new CPA_v2_0__c(Name = 'Brazil Testing 2.0', Supplier__c = accountToInsert[1].Id, Service_Type__c = 'IOR/EOR', Related_Costing_CPA__c = relatedCPA.Id, Preferred_Supplier__c = TRUE,
                                            VAT_Rate__c = 0.1, Destination__c = 'Brazil',

                                            Default_Section_1_Address_Line_1__c= 'Address_Line_1',
                                            Default_Section_1_Address_Line_2__c= 'Address_Line_2',
                                            Default_Section_1_Address_Line_3__c= 'Address_Line_3',
                                            Default_Section_1_City__c= 'City',
                                            Default_Section_1_Zip__c= 'Zip',
                                            Default_Section_1_Country__c = 'Country',
                                            Default_Section_1_Other__c= 'Other',
                                            Default_Section_1_Tax_Name__c= 'Tax_Name',
                                            Default_Section_1_Tax_Id__c= 'Tax_Id',
                                            Default_Section_1_Contact_Name__c= 'Contact_Name',
                                            Default_Section_1_Contact_Email__c= 'Contact_Email',
                                            Default_Section_1_Contact_Tel__c= 'Contact_Tel',
                                            Alternate_Section_1_Address_Line_1__c= 'Address_Line_1',
                                            Alternate_Section_1_Address_Line_2__c= 'Address_Line_2',
                                            Alternate_Section_1_Address_Line_3__c= 'Address_Line_3',
                                            Alternate_Section_1_City__c= 'City',
                                            Alternate_Section_1_Zip__c= 'Zip',
                                            Alternate_Section_1_Country__c = 'Country',
                                            Alternate_Section_1_Other__c = 'Other',
                                            Alternate_Section_1_Tax_Name__c = 'Tax_Name',
                                            Alternate_Section_1_Tax_Id__c = 'Tax_Id',
                                            Alternate_Section_1_Contact_Name__c= 'Contact_Name',
                                            Alternate_Section_1_Contact_Email__c= 'Contact_Email',
                                            Alternate_Section_1_Contact_Tel__c= 'Contact_Tel',
                                            Default_Section_2_Address_Line_1__c= 'Address_Line_1',
                                            Default_Section_2_Address_Line_2__c= 'Address_Line_2',
                                            Default_Section_2_Address_Line_3__c= 'Address_Line_3',
                                            Default_Section_2_City__c= 'City',
                                            Default_Section_2_Zip__c= 'Zip',
                                            Default_Section_2_Country__c = 'Country',
                                            Default_Section_2_Other__c= 'Other',
                                            Default_Section_2_Tax_Name__c= 'Tax_Name',
                                            Default_Section_2_Tax_Id__c= 'Tax_Id',
                                            Default_Section_2_Contact_Name__c= 'Contact_Name',
                                            Default_Section_2_Contact_Email__c= 'Contact_Email',
                                            Default_Section_2_Contact_Tel__c= 'Contact_Tel',
                                            Alternate_Section_2_Address_Line_1__c= 'Address_Line_1',
                                            Alternate_Section_2_Address_Line_2__c= 'Address_Line_2',
                                            Alternate_Section_2_Address_Line_3__c= 'Address_Line_3',
                                            Alternate_Section_2_City__c= 'City',
                                            Alternate_Section_2_Zip__c= 'Zip',
                                            Alternate_Section_2_Country__c = 'Country',
                                            Alternate_Section_2_Other__c = 'Other',
                                            Alternate_Section_2_Tax_Name__c = 'Tax_Name',
                                            Alternate_Section_2_Tax_Id__c = 'Tax_Id',
                                            Alternate_Section_2_Contact_Name__c= 'Contact_Name',
                                            Alternate_Section_2_Contact_Email__c= 'Contact_Email',
                                            Alternate_Section_2_Contact_Tel__c= 'Contact_Tel',
                                            Default_Section_3_Address_Line_1__c= 'Address_Line_1',
                                            Default_Section_3_Address_Line_2__c= 'Address_Line_2',
                                            Default_Section_3_Address_Line_3__c= 'Address_Line_3',
                                            Default_Section_3_City__c= 'City',
                                            Default_Section_3_Zip__c= 'Zip',
                                            Default_Section_3_Country__c = 'Country',
                                            Default_Section_3_Other__c= 'Other',
                                            Default_Section_3_Tax_Name__c= 'Tax_Name',
                                            Default_Section_3_Tax_Id__c= 'Tax_Id',
                                            Default_Section_3_Contact_Name__c= 'Contact_Name',
                                            Default_Section_3_Contact_Email__c= 'Contact_Email',
                                            Default_Section_3_Contact_Tel__c= 'Contact_Tel',
                                            Alternate_Section_3_Address_Line_1__c= 'Address_Line_1',
                                            Alternate_Section_3_Address_Line_2__c= 'Address_Line_2',
                                            Alternate_Section_3_Address_Line_3__c= 'Address_Line_3',
                                            Alternate_Section_3_City__c= 'City',
                                            Alternate_Section_3_Zip__c= 'Zip',
                                            Alternate_Section_3_Country__c = 'Country',
                                            Alternate_Section_3_Other__c = 'Other',
                                            Alternate_Section_3_Tax_Name__c = 'Tax_Name',
                                            Alternate_Section_3_Tax_Id__c = 'Tax_Id',
                                            Alternate_Section_3_Contact_Name__c= 'Contact_Name',
                                            Alternate_Section_3_Contact_Email__c= 'Contact_Email',
                                            Alternate_Section_3_Contact_Tel__c= 'Contact_Tel',
                                            Default_Section_4_Address_Line_1__c= 'Address_Line_1',
                                            Default_Section_4_Address_Line_2__c= 'Address_Line_2',
                                            Default_Section_4_Address_Line_3__c= 'Address_Line_3',
                                            Default_Section_4_City__c= 'City',
                                            Default_Section_4_Zip__c= 'Zip',
                                            Default_Section_4_Country__c = 'Country',
                                            Default_Section_4_Other__c= 'Other',
                                            Default_Section_4_Tax_Name__c= 'Tax_Name',
                                            Default_Section_4_Tax_Id__c= 'Tax_Id',
                                            Default_Section_4_Contact_Name__c= 'Contact_Name',
                                            Default_Section_4_Contact_Email__c= 'Contact_Email',
                                            Default_Section_4_Contact_Tel__c= 'Contact_Tel',
                                            Alternate_Section_4_Address_Line_1__c= 'Address_Line_1',
                                            Alternate_Section_4_Address_Line_2__c= 'Address_Line_2',
                                            Alternate_Section_4_Address_Line_3__c= 'Address_Line_3',
                                            Alternate_Section_4_City__c= 'City',
                                            Alternate_Section_4_Zip__c= 'Zip',
                                            Alternate_Section_4_Country__c = 'Country',
                                            Alternate_Section_4_Other__c = 'Other',
                                            Alternate_Section_4_Tax_Name__c = 'Tax_Name',
                                            Alternate_Section_4_Tax_Id__c = 'Tax_Id',
                                            Alternate_Section_4_Contact_Name__c= 'Contact_Name',
                                            Alternate_Section_4_Contact_Email__c= 'Contact_Email',
                                            Alternate_Section_4_Contact_Tel__c= 'Contact_Tel',
                                            VAT_Reclaim_Destination__c = FALSE,
                                            Final_Destination__c ='Brazil',
                                            Country__c = country.id

                                            );
        insert cpav2;

        List<CPARules__c> CPARules = new List<CPARules__c>();
        CPARules.add(new CPARules__c(Country__c = 'Brazil',Criteria__c ='Default', Chargable_weight__c = 'NONE', Courier_Responsibility__c = 'NONE',   ECCN_No__c = 'NONE', NL_Product_User__c = 'NONE', Number_of_final_deliveries__c = 'NONE', ShipmentValue__c ='NONE', Sum_of_quantity_of_matched_products__c = 'NONE', Manufacturer__c = 'NONE', DefaultCPA2__c = cpav2.Id, CPA2__c =  cpav2.Id));
        insert CPARules;

        List<CPA_Costing__c> costs = new List<CPA_Costing__c>();


        costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'Shipment value', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'IOR', Min__c = 375, Max__c = 14000, Conditional_value__c = 'Shipment value', Condition__c = 'Between', Floor__c = 0, Ceiling__c = 10000, Currency__c = 'US Dollar (USD)'));
        costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'Anil', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'IOR', Currency__c = 'US Dollar (USD)',Variable_threshold__c = null));


        insert costs;



        Shipment_Order__c shipmentOrder = new Shipment_Order__c(Account__c = accountToInsert[0].Id, Shipment_Value_USD__c =10000,Shipping_status__C ='Shipment Pending',
                                                                Client_Contact_for_this_Shipment__c = ContactToInsert[0].Id, Who_arranges_International_courier__c ='Client', Tax_Treatment__c ='DAP/CIF - IOR pays', Ship_to_Country__c = cpa.Id,
                                                                Destination__c = 'Brazil', Service_Type__c = 'IOR', IOR_Price_List__c = iorpl.Id, of_Unique_Line_Items__c = 10, Total_Taxes__c = 1500, of_Line_Items__c= 10, of_packages__c = 20, Final_Deliveries_New__c = 10,
                                                                CI_Admin_Fee__c  =100, CI_Bank_Fees__c  = 100, CI_Finance_Fee__c  = 100, CI_Insurance_Fee_USD__c  = 100, CI_International_Delivery_Fee__c  = 100, CI_IOR_and_Import_Compliance_Fee_USD__c  = 100,
                                                                CI_Miscellaneous_Fee__c  = 100, CI_Recharge_Tax_and_Duty__c  = 100, CI_Total_Clearance_Costs__c  = 100, CI_Total_Customs_Brokerage_Cost__c  = 100, CI_Total_Handling_Cost__c  = 100,
                                                                CI_Total_Licence_Cost__c  = 100, FC_Admin_Fee__c  = 100, FC_Bank_Fees__c  = 100, FC_Finance_Fee__c  = 100, FC_Insurance_Fee_USD__c   = 100, FC_International_Delivery_Fee__c  = 100,
                                                                FC_IOR_and_Import_Compliance_Fee_USD__c  = 100, FC_Miscellaneous_Fee__c  = 100, FC_Recharge_Tax_and_Duty__c  = 100, FC_Total_Clearance_Costs__c  = 100,
                                                                FC_Total_Customs_Brokerage_Cost__c  = 100, FC_Total_Handling_Costs__c  = 100, FC_Total_License_Cost__c  = 100, Total_CIF_Duties__c = 0, Total_FOB_Duties__c = 0, Actual_Admin_Fee__c = 15,
                                                                Actual_Bank_Fees__c = 15, Actual_Exchange_gain_loss_on_payment__c = 15, Actual_Finance_Fee__c = 15, Actual_Insurance_Fee_USD__c = 15, Actual_International_Delivery_Fee__c = 15,
                                                                Actual_Miscellaneous_Fee__c = 15, Actual_Total_Clearance_Costs__c = 15, Actual_Total_Customs_Brokerage_Cost__c = 15, Actual_Total_Handling_Costs__c = 15, Actual_Total_IOR_EOR__c = 15,
                                                                Actual_Total_License_Cost__c = 15, Actual_Total_Tax_and_Duty__c = 15,source__c = 'Internal',Ship_From_Country__c = 'Australia', IOR_CSE__c = userObj.Id, ICE__c = userObj.Id);

        insert shipmentOrder;


        Id PickuprecordTypeId = Schema.SObjectType.Client_Address__c.getRecordTypeInfosByDeveloperName().get('PickUp').getRecordTypeId();
        List<Client_Address__c> clientAddressList = new List<Client_Address__c>();
        Client_Address__c Pickadd = new Client_Address__c(

            recordtypeID=PickuprecordTypeId,
            Name = 'Test',
            Contact_Full_Name__c= 'TEstName',
            Contact_Email__c ='TEst@test.com',
            Contact_Phone_Number__c = '0722854399',
            CompanyName__c= 'CompanyName',
            Address__c='Adddress',
            City__c='City',
            Province__c='Province',
            Postal_Code__c='1235',
            All_Countries__c='Algeria',
            Client__c=accountToInsert[1].Id,
            Naming_Conventions__c = 'Use CPA Details as per the below'
            );

        clientAddressList.add(Pickadd);
        Client_Address__c Pickadd2 = new Client_Address__c(

            recordtypeID=PickuprecordTypeId,
            Name = 'Test',
            Contact_Full_Name__c= 'TEstName',
            Contact_Email__c ='TEst@test.com',
            Contact_Phone_Number__c = '0722854399',
            CompanyName__c= 'CompanyName',
            Address__c='Adddress',
            City__c='City',
            Province__c='Province',
            Postal_Code__c='1235',
            All_Countries__c='Algeria',
            Client__c=accountToInsert[1].Id,
            Naming_Conventions__c = 'Beneficial Owner c/o Final Delivery Details'
            );

        clientAddressList.add(Pickadd2);
        Client_Address__c Pickadd3 = new Client_Address__c(

            recordtypeID=PickuprecordTypeId,
            Name = 'Test',
            Contact_Full_Name__c= 'TEstName',
            Contact_Email__c ='TEst@test.com',
            Contact_Phone_Number__c = '0722854399',
            CompanyName__c= 'CompanyName',
            Address__c='Adddress',
            City__c='City',
            Province__c='Province',
            Postal_Code__c='1235',
            All_Countries__c='Algeria',
            Client__c=accountToInsert[1].Id,
            Naming_Conventions__c = 'Beneficial Owner c/o Use CPA Details as per Below'
            );

        clientAddressList.add(Pickadd3);
        Client_Address__c Pickadd4 = new Client_Address__c(

            recordtypeID=PickuprecordTypeId,
            Name = 'Test',
            Contact_Full_Name__c= 'TEstName',
            Contact_Email__c ='TEst@test.com',
            Contact_Phone_Number__c = '0722854399',
            CompanyName__c= 'CompanyName',
            Address__c='Adddress',
            City__c='City',
            Province__c='Province',
            Postal_Code__c='1235',
            All_Countries__c='Algeria',
            Client__c=accountToInsert[1].Id,
            Naming_Conventions__c = 'Client c/o CPA Details as per Below'
            );

        clientAddressList.add(Pickadd4);

        Client_Address__c Pickadd6 = new Client_Address__c(

            recordtypeID=PickuprecordTypeId,
            Name = 'Test',
            Contact_Full_Name__c= 'TEstName',
            Contact_Email__c ='TEst@test.com',
            Contact_Phone_Number__c = '0722854399',
            CompanyName__c= 'CompanyName',
            Address__c='Adddress',
            City__c='City',
            Province__c='Province',
            Postal_Code__c='1235',
            All_Countries__c='Algeria',
            Client__c=accountToInsert[1].Id,
            Naming_Conventions__c = 'TecEx c/o Final Delivery Company'
            );

        clientAddressList.add(Pickadd6);
        Client_Address__c Pickadd7 = new Client_Address__c(

            recordtypeID=PickuprecordTypeId,
            Name = 'Test',
            Contact_Full_Name__c= 'TEstName',
            Contact_Email__c ='TEst@test.com',
            Contact_Phone_Number__c = '0722854399',
            CompanyName__c= 'CompanyName',
            Address__c='Adddress',
            City__c='City',
            Province__c='Province',
            Postal_Code__c='1235',
            All_Countries__c='Algeria',
            Client__c=accountToInsert[1].Id,
            Naming_Conventions__c = 'Use Final Delivery Details'
            );

        clientAddressList.add(Pickadd7);
        Client_Address__c Pickadd8 = new Client_Address__c(

            recordtypeID=PickuprecordTypeId,
            Name = 'Test',
            Contact_Full_Name__c= 'TEstName',
            Contact_Email__c ='TEst@test.com',
            Contact_Phone_Number__c = '0722854399',
            CompanyName__c= 'CompanyName',
            Address__c='Adddress',
            City__c='City',
            Province__c='Province',
            Postal_Code__c='1235',
            All_Countries__c='Algeria',
            Client__c=accountToInsert[1].Id,
            Naming_Conventions__c = 'VAT Claiming Entity c/o with CPA details below'
            );
        clientAddressList.add(Pickadd8);
        Client_Address__c Pickadd9 = new Client_Address__c(

            recordtypeID=PickuprecordTypeId,
            Name = 'Test',
            Contact_Full_Name__c= 'TEstName',
            Contact_Email__c ='TEst@test.com',
            Contact_Phone_Number__c = '0722854399',
            CompanyName__c= 'CompanyName',
            Address__c='Adddress',
            City__c='City',
            Province__c='Province',
            Postal_Code__c='1235',
            All_Countries__c='Algeria',
            Client__c=accountToInsert[1].Id,
            Naming_Conventions__c = 'CPA c/o Final Delivery Details'
            );
        clientAddressList.add(Pickadd9);
        insert clientAddressList;
        List<Freight__c> frightList = new List<Freight__c>();
        for(Client_Address__c pickAddress: clientAddressList) {
            Freight__c Fr = New Freight__c(status__c='Cancel',Packages_of_Same_Weight_Dimensions__c=1,
                                           Actual_Weight_KGs__c=40,Logistics_provider__c=accountToInsert[1].Id,
                                           Li_Ion_Batteries__c='No',Shipment_Order__c=shipmentOrder.Id,
                                           Ship_Froma__c='Angola',Ship_From__c='Angola',Courier_Rate_kg__c = 20,
                                           Ship_To__c='Australia',Chargeable_weight_in_KGs_packages__c=40,
                                           Estimated_chargeable_weight__c=40, Ship_To_Address__c = pickAddress.Id,
                                           Ship_From_Address__c = pickAddress.Id,Notify_Address__c= pickAddress.Id,Client__c=accountToInsert[1].Id, FF_Consignee_Address__c = pickAddress.Id,
                                          Suggested_Supplier_Name__c='TECEX');
            frightList.add(Fr);
        }

        frightList.add(New Freight__c(status__c='Cancel',Packages_of_Same_Weight_Dimensions__c=1,
                                      Actual_Weight_KGs__c=40,Logistics_provider__c=accountToInsert[1].Id,
                                      Li_Ion_Batteries__c='No',Shipment_Order__c=shipmentOrder.Id,Courier_Rate_kg__c = 20,
                                      Ship_Froma__c='Angola',Ship_From__c='Angola',
                                      Ship_To__c='Australia',Chargeable_weight_in_KGs_packages__c=40,
                                      Estimated_chargeable_weight__c=40,Suggested_Supplier_Name__c='TECEX'));
        
        zkmulti__MCShipmate_Preference__c shipmatePref = new zkmulti__MCShipmate_Preference__c(zkmulti__Label_Image_Type_Default__c='PNG');
        insert shipmatePref;

        list<Carrier_Account__c> caList = new list<Carrier_Account__c>();

        caList.add(new Carrier_Account__c(Carrier_Name__c = 'FedEx',Account__c=accountToInsert[1].Id,ID__c = shipmatePref.Id ));
        caList.add(new Carrier_Account__c(Carrier_Name__c = 'DHL',Account__c=accountToInsert[1].Id,ID__c = shipmatePref.Id ));

        Insert caList;
        Insert frightList;
        
        Shipment_Order_Package__c SOP = new Shipment_Order_Package__c(packages_of_same_weight_dims__c=2,Weight_Unit__c='LBS',Actual_Weight__c=60,
                                                                      Dimension_Unit__c = 'CMs',
                                                                      Height__c=2,
                                                                      Length__c=3,
                                                                      Breadth__c=2,
                                                                      Shipment_Order__c=shipmentOrder.Id,
                                                                      Freight__c = frightList[0].Id,
                                                                      Created_From__c='FromRecord'
                                                                      );
        Insert SOP;
    }
    
    public static testMethod void updateSo_Test(){
        
        Shipment_Order__c SO = [Select Id, Shipping_Status__C from Shipment_Order__c LIMIT 1];
        
        List<Freight__c> freightList = [Select status__c, Packages_of_Same_Weight_Dimensions__c,
                                           Actual_Weight_KGs__c,Logistics_provider__c,
                                           Li_Ion_Batteries__c,Shipment_Order__c,
                                           Ship_Froma__c,Ship_From__c,Courier_Rate_kg__c,
                                           Ship_To__c,Chargeable_weight_in_KGs_packages__c,
                                           Estimated_chargeable_weight__c, Ship_To_Address__c,
                                           Ship_From_Address__c,Notify_Address__c,Client__c, FF_Consignee_Address__c,
                                          Suggested_Supplier_Name__c, Reason_For_Manual_Override__c, Manual_Cost_Override__c from Freight__c LIMIT 1];
        
        Test.startTest();
        
        freightList[0].Manual_Cost_Override__c = 101.00;
        freightList[0].Reason_For_Manual_Override__c = 'Testing the Class.';
        freightList[0].Status__c = 'Submitted';
        
        UPDATE freightList;
        FreightPBTrigger.updateSo(freightList);
        
        Test.stopTest();
        
        Shipment_Order__c soObj = [Select Id, Freight_Fee_Calculated_Freight_Request__c from Shipment_order__c LIMIT 1];
        
        System.assert(soObj.Freight_Fee_Calculated_Freight_Request__c != null);
        
    }
    
    public static testMethod void updateSupplier_Test(){
        
        List<Freight__c> freightList = [Select Status__c, Packages_of_Same_Weight_Dimensions__c,
                                           Actual_Weight_KGs__c,Logistics_provider__c,
                                           Li_Ion_Batteries__c,Shipment_Order__c,
                                           Ship_Froma__c,Ship_From__c,Courier_Rate_kg__c,
                                           Ship_To__c,Chargeable_weight_in_KGs_packages__c,
                                           Estimated_chargeable_weight__c, Ship_To_Address__c,
                                           Ship_From_Address__c,Notify_Address__c,Client__c, FF_Consignee_Address__c,
                                          Suggested_Supplier_Name__c from Freight__c];
        
        Test.startTest();
        
        freightList[0].Suggested_Supplier_Name__c = 'TNT';
        
        UPDATE freightList;
        
        FreightPBTrigger.updateSupplier(freightList);
        
        Test.stopTest();
        
        Freight__c freight = [Select Id, Logistics_provider__c from Freight__c LIMIT 1];
        
        System.assert(freight.Logistics_provider__c != null);
    }
    
    public static testMethod void wrappers_Test(){
        
        FreightPBTrigger.ShipToFromCountryWrapper shipToFromObj = new FreightPBTrigger.ShipToFromCountryWrapper('USA','South Africa');
        FreightPBTrigger.ShipToFromRegionWrapper ShipToFromRegionObj = new FreightPBTrigger.ShipToFromRegionWrapper('South Africa','USA');
        FreightPBTrigger.FreightVariablesWrapper FreightVariablesObj = new FreightPBTrigger.FreightVariablesWrapper(34.00, true);
        
        
    }

}