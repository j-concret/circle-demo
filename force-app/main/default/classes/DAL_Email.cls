public class DAL_Email 
{
    public static final String EMIL_TEMPLATE_PLACEHOLDER = '{Number}';
	
	public static OrgWideEmailAddress getAnyOrgWideAddress()
	{
		return 
			[
				SELECT Id, 
						DisplayName, 
						Address 
				FROM OrgWideEmailAddress 
				LIMIT 1
			];
	}

	public static EmailTemplate getTemplateByName(String templateName)
	{
        List<EmailTemplate> templates = [
									        SELECT Id, 
											       Body, 
											       HtmlValue 
									        FROM EmailTemplate 
									        WHERE Name = :templateName 									        
								        ];

        if(templates.isEmpty())
        	return null;
    	else
    		//Name is unique
    		return templates[0];
    }	
}