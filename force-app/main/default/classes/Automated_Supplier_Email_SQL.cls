global class Automated_Supplier_Email_SQL implements Database.Batchable<sObject>{
    global String Query;
    global Automated_Supplier_Email_SQL(){
        String sPODReceived = 'POD Received';
        String sCCDReceived = 'Customs Clearance Docs Received';
        String sStream = 'Stream (supplier)';
        String sKWE = 'Kintetsu World Express South Africa Proprietary Limited';
        Query = 'Select Id,Name,SupplierlU__c,SupplierlU__r.Name,Account__r.Name,Destination__c,Shipping_Status__c,POD_Date__c,Days_Since_POD_Date__c,CreatedDate,Test_Account__c,Final_Supplier_Invoice_Received__c from Shipment_Order__c where (Shipping_Status__c = \''+String.escapeSingleQuotes(sPODReceived)+'\' OR Shipping_Status__c = \''+String.escapeSingleQuotes(sCCDReceived)+'\') AND (Actual_Total__c = 0.00) AND (CreatedDate >= 2020-01-01T00:00:00Z) AND (Days_Since_POD_Date__c > 2) AND (Test_Account__c = FALSE) AND (Final_Supplier_Invoice_Received__c = FALSE) AND (SupplierlU__r.Name != \''+String.escapeSingleQuotes(sStream)+'\') AND (SupplierlU__r.Name != \''+String.escapeSingleQuotes(sKWE)+'\') ORDER BY POD_Date__c ASC';
    }
    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(Query);
    }
    global void execute(Database.BatchableContext BC, List<Shipment_Order__c> scope){
        Automated_Supplier_Email.sendEmailSummary(scope,null,null);
    }
    global void finish(Database.BatchableContext BC){
    
    }
}