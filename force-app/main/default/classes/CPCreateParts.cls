@RestResource(urlMapping='/CPCreateParts/*')
Global class CPCreateParts {

     @Httppost
    global static void CreateParts(){
        
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        Blob body = req.requestBody;
    	res.addHeader('Content-Type', 'application/json');
        String requestString = body.toString();
        CPCreatePartsWrapper rw = (CPCreatePartsWrapper)JSON.deserialize(requestString,CPCreatePartsWrapper.class);
        
     try {
         
      //Parts creation
     
 		If(!rw.Parts.isEmpty()){
                
 					list<Part__c> PAL = new list<Part__c>();                
                      for(Integer l=0; rw.Parts.size()>l;l++) {
                         
                             Part__c PA = new Part__c(
                         	 
                                  Name =rw.Parts[l].PartNumber,
                                  Description_and_Functionality__c =rw.Parts[l].PartDescription,
                                  Quantity__c =rw.Parts[l].Quantity,
                                  Commercial_Value__c =rw.Parts[l].UnitPrice,
                                  US_HTS_Code__c =rw.Parts[l].HSCode,
                                  Country_of_Origin2__c =rw.Parts[l].CountryOfOrigin,
                                  ECCN_NO__c =rw.Parts[l].ECCNNo,
                                  Type_of_Goods__c=rw.Parts[l].Type_of_Goods,
                                  Li_ion_Batteries__c=rw.Parts[l].Li_ion_Batteries,
                                  Lithium_Battery_Types__c=rw.Parts[l].Li_ion_BatteryTypes,
                                  Shipment_Order__c=rw.Parts[l].SOID
                         
                         );
                           PAL.add(PA);
                          }
            system.debug('Parts list--->'+PAL); 
            system.debug('Parts list Parts.size()--->'+rw.Parts.size());
            Insert PAL;
			
            
            
     
            res.responseBody = Blob.valueOf(JSON.serializePretty(PAL));
            res.statusCode = 200;
              	API_Log__c Al = New API_Log__c();
                Al.Account__c=rw.ClientID;
                Al.Login_Contact__c=rw.ContactID;
                Al.EndpointURLName__c='CPCreateParts';
                Al.Response__c='Success - Parts created from Record';
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al; 
            }
        }
    Catch(Exception e){
        
        		String ErrorString ='Something went wrong while creating parts, please contact support_SF@tecex.com';
               	res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
            	res.statusCode = 500;
                API_Log__c Al = New API_Log__c();
                Al.Account__c=rw.ClientID;
                Al.Login_Contact__c=rw.ContactID;
                Al.EndpointURLName__c='CPCreateParts';
                Al.Response__c=e.getMessage()+' at line number: '+e.getLineNumber();
                Al.StatusCode__c=string.valueof(res.statusCode);
              	Insert Al;
        
        
       }
        
        
    
    }
    
}