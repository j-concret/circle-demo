@RestResource(urlMapping='/SODetails/*')
Global class NCPSODetials {

    @Httppost
    global static void NCPSODetials(){
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        Blob body = req.requestBody;
        res.addHeader('Content-Type', 'application/json');
        String requestString = body.toString();
        NCPSODetialsWra rw = (NCPSODetialsWra)JSON.deserialize(requestString,NCPSODetialsWra.class);
        try {
            Access_token__c At =[select status__c,Access_Token__c from Access_token__c where Access_Token__c=:rw.Accesstoken ];
            if( at.status__C =='Active') {

                Shipment_order__c SOs = [SELECT Preferred_Freight_Method__c,Client_Invoice_Status_Roll_up__c,Total_VAT_GST__c,Cost_Estimate_Taxes_and_Duties_Other__c,Total_VAT_GST_with_buffer__c, Total_Duties_and_Other_Taxes_Excl_VAT__c, CPA_v2_0__r.Estimated_Transit_Time__c, CPA_v2_0__r.Estimate_Road_Transit_Time__c,CPA_v2_0__r.Estimate_Sea_Transit_Time__c,portal_tracking_number__C,Reason_for_Pro_forma_quote__c,ETA_Days_Business__c,ETA_Formula__c,Shipment_Order_Compliance_Time_Decimal__c,Estimate_Final_Delivery_Time__c,Final_Delivery_Lead_Time_Decimal__c,Transit_to_Destination_Lead_Time_Decimal__c,Hub_Lead_Time_Decimal__c,Customs_Clearance_Lead_Time_Decimal__c,CPA_v2_0__r.ETA_Disclaimer__c,POD_Date__C,recordtypeID,recordtype.name,Client_Contact_for_this_Shipment__r.name,NCP_Shipping_Status__c,Potential_Liability_cover_fee_USD__c,Sub_Status_Update__c,Freight_Fee_Calculated_Freight_Request__c,ETA_Text_per_Status__c,Final_Delivery_Date__c,banner_feed__c,Mapped_Shipping_status__c,Insurance_Fee__c,Cost_Estimate_Acceptance_Date__c,NCP_Client_Notes__c,Miscellaneous_Fee_Name__c,ETA_Days__c,Admin_TecEx_Freight__c,Buyer_Account__r.Name,Client_Type__c,roll_out__C,rolloutname__c,Minimum_tax_range__c,Maximum_tax_range__c,Invoice_Payment__c,Tax_Cost__c,Pick_up_Coordination__c,Shipping_Documents__c,Client_Task__c,Customs_Compliance__c,Compliance_Process__c,Total_Cost_Range_Min__c,Total_Cost_Range_Max__c,Quote_type__c,ETA__c,Client_Taking_Liability_Cover__c,CreatedDate,NCP_Quote_Reference__c,of_Line_Items__c,Estimate_Pre_Approval_Time_Formula__c,IOR_Price_List__c,Service_Manager__c,IOR_CSE__c,Binding_Quote__c,Invoice_Timing__c,Account__c,Ship_to_Country__c,Client_Contact_for_this_Shipment__c,of_Unique_Line_Items__c,Name,Actual_Weight_KGs__c,Number_Delivered__c,Add_Supplier_Invoice_Costings__c,Handling_and_Admin_Fee__c,Bank_Fees__c,Branch__c,CasesafeId__c,Finance_Fee__c,Chargeable_Weight__c,CI_Total__c,Client_Payment_Received__c,Client_Reference__c,Client_Reference_2__c,Closed_Down_SO__c,Collection_Administration_Fee__c,Confirm_Liability_Cover_Decline__c,Cost_Estimate_Accepted__c,Cost_Estimate_attached__c,Cost_Estimate_Number_HyperLink__c,Cost_Estimate_Number__c,Cost_Estimate_Subject__c,CPA_Costings_Added__c,CPA_v2_0__c,Create_Costings__c,CreatedByOpp__c,Create_Roll_Out__c,Destination__c,EOR_and_Export_Compliance_Fee_USD__c,Estimate_Customs_Clearance_time__c,Estimate_Customs_Clearance_Time_Formula__c,Estimate_Transit_Time__c,Estimate_Transit_Time_Formula__c,Expiry_Date__c,Non_TecEx_VAT_recovery__c,Finance_Team__c,Financial_Controller__c,Financial_Manager__c,First_Invoice_Created__c,Hub_shipping_in_CPA_costs__c,ICE_Name__c,Who_arranges_International_courier__c,International_Delivery_Fee__c,Invisible__c,IOR_FEE_USD__c,Lead_AM__c,Lead_ICE__c,Insurance_Fee_USD__c,Liability_Cover_Fee_Added__c,Li_ion_Batteries__c,Miscellaneous_Fee__c,New_Status_Date__c,New_Structure__c,Next_Step_by__c,NON_Pickup_freight_request_created__c,Non_VAT_recovery__c,Final_Deliveries_New__c,of_packages__c,Opportunity__c,Packages_override__c,POD_Month__c,Populate_Invoice__c,Client_PO_ReferenceNumber__c,Purchase_orders_required__c,Recharge_Tax_and_Duty__c,Regenerate_First_Invoice__c,Request_Email_Estimate__c,Service_Type__c,Ship_From_Country__c,Shipment_Value_USD__c,Shipping_Notes_Formula__c,Shipping_Notes_New__c,Shipping_Status__c,Ship_to_Country_new__c,Taxes_Calculated__c,Tax_Recovery__c,Tax_recovery_Premium_Fee__c,Vat_Claim_Country__c,Tax_Treatment__c,Tax_Treatment_Formula__c,VAT_shipment__c,Time_At_Status__c,Top_up_Invoice_Credits_Roll_up__c,TopUp_Invoice_Created__c,Total_clearance_Costs__c,Total_Customs_Brokerage_Cost__c,Recharge_Handling_Costs__c,Total_including_estimated_duties_and_tax__c,Total_Invoice_Amount__c,Total_Invoice_Amount_Excl_Taxes_Duties__c,Recharge_License_Cost__c,Total_License_Cost__c,Total_Line_Item_Extended_Value__c,Tracking_estimate__c,Trigger_Time_05__c,Trigger_Time_1_Minute__c,Type_of_Goods__c,VAT_Claiming_Country__c,Id,Account__r.Name,Intention_with_Goods__c,Intention_with_Goods_Other__c,Contact_name__c,Beneficial_Owner_Country__c,Buyer_or_BO_Part_of_EU__c,Beneficial_Owner_Company_Address__c,Buyer_or_BO_VAT_Number__c, Buyer_Retains_Ownership__c FROM shipment_order__c WHERE Id =:rw.SOID ];
                //	String stringAcc = JSON.serialize(SOs);
                //    Map<String, Object> mapSOs = (Map<String, Object>)JSON.deserializeUntyped(stringAcc);
                //    mapSOs.put('Demo','Testing');

                if(rw.AccountID == SOS.Account__c) {
                    res.responseBody = Blob.valueOf(JSON.serializePretty(SOs));  //mapSOs
                    res.addHeader('Content-Type', 'application/json');
                    res.statusCode = 200;
                }
                else {
                    res.responseBody = Blob.valueOf(' "You are not authorised to view details" ');
                    res.addHeader('Content-Type', 'application/json');
                    res.statusCode = 200;
                }
                API_Log__c Al = New API_Log__c();
                Al.Account__c=rw.AccountID;
                Al.Login_Contact__c=rw.ContactId;
                Al.EndpointURLName__c='NCP - ShipmentOrderDetails';
                Al.Response__c='Success - shipment orders Details are sent';
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al;
            }
        }
        Catch(Exception e ){
            String ErrorString ='Authentication failed, Please check username and password';
            res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
            res.addHeader('Content-Type', 'application/json');
            res.statusCode = 400;

            API_Log__c Al = New API_Log__c();
            Al.Account__c=rw.AccountID;
            Al.Login_Contact__c=rw.contactId;
            Al.EndpointURLName__c='ShipmentOrderDetails';
            Al.Response__c=e.getMessage()+' at line number: '+e.getLineNumber();
            Al.StatusCode__c=string.valueof(res.statusCode);
            Insert Al;
        }
    }
}