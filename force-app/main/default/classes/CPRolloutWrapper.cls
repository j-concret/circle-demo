public class CPRolloutWrapper {
    public String Account_Name;
	public String Contact_Name;
	public Double Shipment_Value_USD;
	public Double Chargable_Weight;
	public String Courier_responsibility;
    public String ServiceType;
	public String Reference1;
	public String Reference2;
	public String ShipFrom;
	public String ShipTo;
	public List<ShipmentOrder_Packages> ShipmentOrder_Packages;

	public class ShipmentOrder_Packages {
		
		
		public String Weight_Unit;
       	public String Dimension_Unit;
		public Integer Packages_of_Same_Weight;
		public Double Length;
		public Double Height;
		public Double Breadth;
		public Double Actual_Weight;
		
		
		
	}

	
	public static CPRolloutWrapper parse(String json) {
		return (CPRolloutWrapper) System.JSON.deserialize(json, CPRolloutWrapper.class);
	}
}