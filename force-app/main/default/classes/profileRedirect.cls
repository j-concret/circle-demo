global without sharing class profileRedirect {
    
    @AuraEnabled
    global static Map<String,Object> getUserInfo(){ 
        Map<String,Object> response = new Map<String,Object>();
        response.put('Status','OK');
        String email = UserInfo.getUserEmail();
        response.put('InternalUser',email.contains('tecex.com'));
        response.put('email',email);
        return response;
    }
    @AuraEnabled
    global static Map<String,String> getShipmentId(String IdToSql,Boolean isTask){ 
        Map<String,String> response = new Map<String,String>();
        response.put('Status','Okay');
        response.put('UserInfo',UserInfo.getUserEmail());
        response.put('UserInfoName',UserInfo.getUserName());
        try {
            if(isTask){
                
                Task[] taskList = [SELECT Id, Shipment_Order__c,Shipment_Order__r.Shipping_Status__c FROM Task where Id =: IdToSql];
                if (taskList.size() > 0){
                    IdToSql = taskList[0].Id;
                    response.put('shipId',taskList[0].Shipment_Order__c);  
                    response.put('shipStatus',taskList[0].Shipment_Order__r.Shipping_Status__c); 
                }
                
            }else {
                
                if(IdToSql.startsWith('0D5')){
                    FeedItem[]  FeedItemList =  [SELECT Id, ParentId FROM FeedItem where Id =:IdToSql limit 1];
                    
                   // System.debug('FeedCommentList '+JSON.serializePretty(FeedItemList));
                        if(FeedItemList.size() > 0){
                        IdToSql = FeedItemList[0].ParentId;
                    }
                }
                
                if(IdToSql.startsWith('0D7')){
                    FeedComment[]  FeedCommentList =  [SELECT ParentId, Id, FeedItemId FROM FeedComment where Id =:IdToSql limit 1];
                    
                    System.debug('FeedCommentList '+JSON.serializePretty(FeedCommentList));
                        if(FeedCommentList.size() > 0){
                        IdToSql = FeedCommentList[0].ParentId;
                    }
                }
                
                if(IdToSql.startsWith('00T')){
                    Task[] taskList = [SELECT Id, Shipment_Order__c,Shipment_Order__r.Shipping_Status__c FROM Task where Id =: IdToSql];
                if (taskList.size() > 0){
                    IdToSql = taskList[0].Id;
                    response.put('shipId',taskList[0].Shipment_Order__c);  
                    response.put('shipStatus',taskList[0].Shipment_Order__r.Shipping_Status__c); 
                } 
                }else if(IdToSql.startsWith('500')){
                Case[] caseList = [SELECT Id, Shipment_Order__c,Shipment_Order__r.Shipping_Status__c, Origin ,Linked_Record_Id__c  FROM Case where Id =: IdToSql];
                
                    System.debug('caseList '+JSON.serializePretty(caseList));
                if (caseList.size() > 0){
                    IdToSql = caseList[0].Id;
                  //  Id linkedId = caseList[0].Linked_Record_Id__c;
                    //Id shipId = caseList[0].Shipment_Order__c;
                    response.put('shipId',caseList[0].Shipment_Order__c); 
                    response.put('shipStatus',caseList[0].Shipment_Order__r.Shipping_Status__c);  
                    response.put('Origin',caseList[0].Origin);  
                    response.put('LinkedRecordId',caseList[0].Linked_Record_Id__c);  
                }
                }
            }
             response.put('IdToSql',IdToSql);
        }
        catch (Exception ex) {
            response.put('error',ex.getMessage());
        }
        System.debug('response '+JSON.serializePretty(response));
        return response;
    }
}