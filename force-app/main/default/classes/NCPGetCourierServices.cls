@RestResource(urlMapping='/GetCourierrates/*')
Global class NCPGetCourierServices {
        
      @Httppost
      global static void NCPGetCourierServices(){
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        Blob body = req.requestBody;
    	res.addHeader('Content-Type', 'application/json');
        String requestString = body.toString();
        NCPGetCourierServiceswra rw  = (NCPGetCourierServiceswra)JSON.deserialize(requestString,NCPGetCourierServiceswra.class);
          try{
              //accesstoken check has to be done here
           list< Courier_Rates__c> CR = [SELECT Id, Name, CreatedDate,Preferred__C, Freight_Request__c, Final_Rate__c, service_type__c, Status__c, Service_Type_Name__c, Origin__c, Carrier_Name__c FROM Courier_Rates__c where Status__c !='Expired'  and Preferred__C=TRUE and  Freight_Request__c=:rw.frid  ORDER BY Final_Rate__c ASC limit 5];
           if(!CR.isempty()){
             res.responseBody = Blob.valueOf(JSON.serializePretty(CR));
             res.addHeader('Content-Type', 'application/json');
             res.statusCode = 200;
                
                API_Log__c Al = New API_Log__c();
                Al.EndpointURLName__c='GetCourierrates';
                Al.StatusCode__c=string.valueof(res.statusCode);
                AL.Response__c = 'Success - Courierrates are sent';
                Insert Al;  
              }
              Else{
                  
            JSONGenerator gen = JSON.createGenerator(true);
            gen.writeStartObject();
            
             gen.writeFieldName('Success');
             gen.writeStartObject();
            
             	if(rw.FRID == null) {gen.writeNullField('Response');} else{gen.writeStringField('Response', 'Courier rates are not available');}
            
            gen.writeEndObject();
            gen.writeEndObject();
            String jsonData = gen.getAsString();
            res.responseBody = Blob.valueOf(jsonData);
            res.addHeader('Content-Type', 'application/json');
            res.statusCode = 400;        
            
                  
                  
              }
              
              
          }
          catch(Exception e){
              
                String ErrorString ='Authentication failed, Please check username and password';
               		res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
        			res.statusCode = 400;
                
                API_Log__c Al = New API_Log__c();
                //Al.Account__c=logincontact.AccountID;
                //Al.Login_Contact__c=logincontact.Id;
                Al.EndpointURLName__c='GetCourierrates';
                Al.Response__c=ErrorString;
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al;
                
              
          }
          
    
          
    }
   
}