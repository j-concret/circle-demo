@RestResource(urlMapping='/GetDestinations/*')
Global with sharing class CPAPIDestinations {
 @Httpget
    global static void Destinations(){
         RestRequest req = RestContext.request;
         RestResponse res = RestContext.response;

        Schema.DescribeFieldResult F = Shipment_Order__c.Destination__c.getDescribe();
        Schema.sObjectField T = F.getSObjectField();
        List<PicklistEntry> entries = T.getDescribe().getPicklistValues();

                 res.responseBody = Blob.valueOf(JSON.serializePretty(entries));
        	 	 res.statusCode = 200;

}
}