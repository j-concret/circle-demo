public class CSIQuotecreationWra {
    public String AccountID;
	public String ContactID;
	public String AccessToken;
	public List<Quotes> Quotes;

	public class FinalDelivieries {
		public String Name;
		public String FinalDestinationAddressID;
	}

	public class Quotes {
		public Double estimatedChargableweight;
		public String ServiceType;
		public String Courier_responsibility;
		public String Reference1;
		public String Reference2;
		public String BuyerID;
		public String ShipFrom;
		public String ShipTo;
		public Double ShipmentvalueinUSD;
		public String PONumber;
		public String Type_of_Goods;
		public String Li_ion_Batteries;
		public String Li_ion_BatteryTypes;
		public Integer NumberOfFinaldeliveries;
	/*	public String FRContact_Full_Name;
		public String FRContact_Email;
		public String FRContact_Phone_Number;
		public String FRAddress1;
		public String FRAddress2;
		public String FRCity;
		public String FRProvince;
		public String FRPostal_Code;
		public String FRAll_Countries;
*/
		public List<ShipmentOrder_Packages> ShipmentOrder_Packages;
		public List<FinalDelivieries> FinalDelivieries;
        public List<Parts> Parts;
	}

	public class ShipmentOrder_Packages {
		public String Weight_Unit;
		public String Dimension_Unit;
		public Integer Packages_of_Same_Weight;
		public Double Length;
		public Double Height;
		public Double Breadth;
		public Double Actual_Weight;
	}
    public class Parts {
		public String PartNumber;
		public String PartDescription;
		public Integer Quantity;
		public Double UnitPrice;
        Public string ClientpartReferencenumber;
	}

	
/*	public static CSIQuotecreationWra parse(String json) {
		return (CSIQuotecreationWra) System.JSON.deserialize(json, CSIQuotecreationWra.class);
	}
*/
}