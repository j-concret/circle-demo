@IsTest(SeeAllData=true) 
public class TestShipmentToInvoice{

    public static testMethod void testCreateInvoice(){
        
        
        TestUtil.addDefaultSettings();
        AppUtils.IS_API_CALL_OVERRIDE = true;
        Test.setMock(HttpCalloutMock.class, new TX_Dragon_Mock());
        List<Account> accts = TestUtil.createAccounts(1, true);
        
        /* 
        Shipment_Order__c shipmentOrder = new Shipment_Order__c();
        shipmentOrder.Account__c = accts[0].id;
        shipmentOrder.Shipment_Value_USD__c = 99.99;
        shipmentOrder.Set_up_Fee__c = 9.99;
        shipmentOrder.Handling_and_Admin_Fee__c = 9.99;
        shipmentOrder.Bank_Fees__c = 9.99;         
        shipmentOrder.Supplier_Admin_Cost__c = 9.99;
        shipmentOrder.TecEx_In_house_Fee__c = 9.99; 
        //shipmentOrder.Int_Courier_Fee__c = 9.99; 
        shipmentOrder.Insurance_Fee__c = 10; 
        
        insert shipmentOrder;
        
        shipmentOrder.License_Permit_Cost__c = 9.99;
        //shipmentOrder.Customs_Cost_Other__c = 9.99;
        shipmentOrder.Customs_Brokerage_Cost_fixed__c = 9.99;
        shipmentOrder.Customs_Brokerage_Cost_CIF__c = 1;
        shipmentOrder.Customs_Clearance_Cost_b__c = 9.99;
        shipmentOrder.Customs_Handling_Cost__c = 9.99;
        shipmentOrder.Delvier_to_IOR_Warehouse_Cost__c = 9.99; 
        
        //shipmentOrder.IOR_Fee_new__c = 10;
        
        update shipmentOrder;
        */
        
        Shipment_Order__c shipmentOrder = [SELECT id,Name FROM Shipment_Order__c LIMIT 1];
        
        test.startTest();
        ShipmentOrderToInvoice shipToInv = new ShipmentOrderToInvoice();
        fw1__Invoice__c inv =  shipToInv.createInvoice(shipmentOrder.id);
        shipToInv.savePdf(shipmentOrder.id);
        
        PageReference pageRef = Page.EmailShipmentInvoicePDF;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('ids',shipmentOrder.id);
        EmailShipmentInvoicePDFController pdfCntlr = new EmailShipmentInvoicePDFController();
        System.AssertNotEquals('',pdfCntlr.body);
        
        
        ApexPages.StandardController con = new ApexPages.StandardController(shipmentOrder);
        ShipmentOrderToInvoiceController shipToInvCntlr = new ShipmentOrderToInvoiceController(con);
        shipToInvCntlr.createInvoice();
        
        test.stopTest();
        
        System.AssertNotEquals(null,inv);
        
    }
    
     

    
    

}