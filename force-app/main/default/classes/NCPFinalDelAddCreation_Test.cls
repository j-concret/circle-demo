@IsTest
public class NCPFinalDelAddCreation_Test {

    static testMethod void testPostMethod(){
        Access_token__c accessTokenObj = new Access_token__c();
        accessTokenObj.Status__c = 'Active';
        accessTokenObj.Access_Token__c = 'asdfghj-sdfgfds-sfdfgf';
        
        Insert accessTokenObj;
        
        Account acc = new Account(Name = 'Testing',type = 'Supplier',CSE_IOR__c = '0050Y000001LTZO', 
                                       Service_Manager__c = '0050Y000001LTZO', 
                                       Financial_Manager__c='0050Y000001LTZO', 
                                       Financial_Controller__c='0050Y000001LTZO');
        insert acc;

        Contact con = new Contact(FirstName = 'Testing',LastName = 'contact',AccountId = acc.Id,Email = 'test@test.com',Phone = '123456789');
        insert con;
        
         Client_Address__c clientaddrObj = new Client_Address__c(Name='TestAddress',default__C=true,Address_status__c='Active',Comments__c='',All_Countries__c='Brazil',Client__c=acc.Id,City__c='Rio',CompanyName__c = 'Testing');
        insert clientaddrObj;
        
		NCPFinalDestaddCreationwra.FianlDelAddress delAddressObj = new NCPFinalDestaddCreationwra.FianlDelAddress();
        delAddressObj.Accesstoken = accessTokenObj.Access_Token__c;
		delAddressObj.Name = '';
		delAddressObj.Contact_Full_Name = con.Name;
		delAddressObj.Contact_Email = con.Email;
		delAddressObj.Contact_Phone_Number = con.Phone;
		delAddressObj.Address1 = 'Address1';
		delAddressObj.Address2 = 'Address2';
		delAddressObj.City = 'Rio';
		delAddressObj.Province = 'Northern';
		delAddressObj.Postal_Code = '12345';
		delAddressObj.All_Countries = clientaddrObj.All_Countries__c;
		delAddressObj.AccountID = acc.Id;
        delAddressObj.AdditionalNumber = '8855220046';
     	delAddressObj.Comments = clientaddrObj.Comments__c;
        delAddressObj.CompanyName = clientaddrObj.CompanyName__c;
        delAddressObj.DefaultAddress = String.valueOf(clientaddrObj.Default__c);
        delAddressObj.AddressStatus = clientaddrObj.Address_Status__c;
        
        List<NCPFinalDestaddCreationwra.FianlDelAddress> finalDelAddressList = new List<NCPFinalDestaddCreationwra.FianlDelAddress>();
        finalDelAddressList.add(delAddressObj);
        
        NCPFinalDestaddCreationwra wrapper = new NCPFinalDestaddCreationwra();
        wrapper.FianlDelAddress = finalDelAddressList;
        
        RestRequest request = new RestRequest();
        RestResponse res = new RestResponse();
        request.requestUri ='/services/apexrest/api/FinalDelAddresscreate/';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueOf(JSON.serialize(wrapper));
        RestContext.request = request;
        RestContext.response = res;
        
        Test.startTest();
        NCPFinalDelAddCreation.CPFinalDelAddressCreation();
        Test.stopTest();
    }
    
    static testMethod void testPostMethodCatchBlock(){
        
        RestRequest request = new RestRequest();
        RestResponse res = new RestResponse();
        request.requestUri ='/services/apexrest/api/FinalDelAddresscreate/';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueOf('{"value":"Test response"}');
        RestContext.request = request;
        RestContext.response = res;
        
        Test.startTest();
        NCPFinalDelAddCreation.CPFinalDelAddressCreation();
        Test.stopTest();
    }
}