@isTest
public class TAuthenticationTest {
    
    public testmethod static void AuthAndPaymentTest() {
        
       	Account account = new Account (name='Acme1',  Type ='Client',  CSE_IOR__c = '0050Y000001km5c',  Service_Manager__c = '0050Y000001km5c',  Tax_recovery_client__c  = FALSE, Ship_From_Line_1__c = 'Ship_From_Line_1',
                                       Ship_From_Line_2__c = 'Ship_From_Line_2', Ship_From_Line_3__c = 'Ship_From_Line_3', Ship_From_City__c = 'Ship_From_City',  Ship_From_Zip__c = 'Ship_From_Zip',
                                       Ship_From_Country__c = 'Ship_From_Country' ,  Ship_From_Other_notes__c = 'Ship_From_Other', Ship_From_Contact_Name__c = 'Ship_From_Contact', Ship_From_Contact_Email__c = 'Ship_From_Email',
                                       Ship_From_Contact_Tel__c = 'Ship_From_Tel', Client_Address_Line_1__c = 'Client_Address_Line_1', Client_Address_Line_2__c = 'Client_Address_Line_2', Client_Address_Line_3__c = 'Client_Address_Line_3',
                                       Client_City__c = 'Client_City', Client_Zip__c = 'Client_Zip', Client_Country__c = 'Client_Country',  Other_notes__c = 'Other_notes',  Tax_Name__c = 'Tax_Name', Tax_ID__c = 'Tax_ID',
                                       Contact_Name__c = 'Contact_Name', Contact_Email__c = 'Contact_Email', Contact_Tel__c = 'Contact_Tel', Financial_Controller__c = '0050Y000001km5c' ); 
		insert account;
        
        Contact contact1 = new Contact(AccountID =account.Id,Eligible_to_access_Tipalti__c = true,LastName='Test');
        insert contact1;
        
        Test.startTest();
        
        TAuthentication.TAuth();
        TAuthentication.PaymentDetails();
        
        Test.stopTest();
    }
    public testmethod static void AuthAndPaymentTest1() {
        
       
        
        Test.startTest();
        
        TAuthentication.TAuth();
        TAuthentication.PaymentDetails();
        
        Test.stopTest();
    }
}