@IsTest
public with sharing class Lex_SupplierInvoice_Test {

    /** this is the name of our test record
     */
    private static final String TEST_LASTNAME = 'Test User';

    /** this is the name of our community test record
     */
    private static final String TEST_COMMUNITY_LASTNAME = 'Test Comm User';

    /** this is the last name of the contact
     */
    private static final String CONTACT_LASTNAME = 'CONTACT LASTNAME';

    /** this is the account name
     */
    private static final String ACCOUNT_NAME = 'ACCOUNT NAME';

    /** this is the role name
     */
    private static final String ROLE_NAME = 'Role Name';



    /** this function will initialise our test data
     */
    @TestSetup
    static void setupData(){

        createRole();                   //create the user role
        createUser( 'TecEx IOR Team' );                 //create the user

        User test_user = getUser( TEST_LASTNAME );      //get the test user

        System.runAs( test_user ) {

            createAccountAndContact( test_user.Id );

            Account test_account = getAccount( ACCOUNT_NAME );
            country_price_approval__c cpa = new country_price_approval__c( Name = 'Aruba', Billing_term__c = 'DAP/CIF - IOR pays',
                                                                           Airwaybill_Instructions__c = 'User IOR Address' );
            insert cpa;                            //insert the CPA

            IOR_Price_List__c iorpl = new IOR_Price_List__c ( Client_Name__c = test_account.Id, Name = 'Aruba', IOR_Fee__c = 0.15, On_Charge_Mark_up__c = 0.15,
                                                              TecEx_Shipping_Fee_Markup__c =0.40, IOR_Min_Fee__c = 100,  Admin_Fee__c =100, Set_up_fee__c =100,
                                                              Bank_Fees__c =200, Tax_Rate__c = 0.20, Estimated_Customs_Brokerage__c =250,
                                                              Estimated_Customs_Clearance__c =250, Estimated_Customs_Handling__c =250, Estimated_Import_License__c =250 );
            insert iorpl;           //insert the iorpl

        }//end of run as block

        Contact test_contact = getContact( CONTACT_LASTNAME );

        createCommunityUser( test_contact.Id );

    }//end of function definition






    /** this function will test the function that get destinations from
     */
    @IsTest
    static void testGetDestinationValues(){

        User usr = getUser( TEST_LASTNAME );

        System.runAs( usr ){

        //    LEX_SupplierInvoice.getDestinationValues();

        }//end of runAs block

    }//end of function definition





    /** this function will get list view ID
     */
    @IsTest
    static void testGetRecordListViewID(){

        User usr = getUser( TEST_LASTNAME );

        //run in test user context
        System.runAs( usr ){

   //         LEX_SupplierInvoice.getRecordListViewID();

        }//end of runAs block

    }//end of function definition





    @IsTest
    static void testCreateRolloutRecord(){

        User usr = getUser( TEST_LASTNAME );

        //run in test internal user context
        System.runAs( usr ){

            Id account_id = [ SELECT Id
                              FROM Account
                              WHERE Name =: ACCOUNT_NAME  ].Id;

            Id contact_id = [ SELECT Id
                              FROM Contact
                              WHERE LastName =: CONTACT_LASTNAME ].Id;

            List< String > destination_list = new List< String >{ 'Brazil' };
     //       LEX_SupplierInvoice.createRolloutRecord( account_id, contact_id, '12345', 128, destination_list, 'false' );

        }//end of runAs-block

        usr = getUser( TEST_COMMUNITY_LASTNAME );

        //run in test community user context
        System.runAs( usr ){

            Id account_id = [ SELECT Id
                              FROM Account
                              WHERE Name =: ACCOUNT_NAME  ].Id;

            Id contact_id = [ SELECT Id
                              FROM Contact
                              WHERE LastName =: CONTACT_LASTNAME ].Id;

            List< String > destination_list = new List< String >{ 'Brazil', 'Zambia' };
    //        LEX_SupplierInvoice.createRolloutRecord( account_id, contact_id, '12345', 128, destination_list, 'false' );

        }//end of runAs-block


    }//end of function definition




    @IsTest
    static void testGetAccountAndContact(){

        User usr = getUser( TEST_COMMUNITY_LASTNAME );

        //run in test user context
        System.runAs( usr ){

            LEX_SupplierInvoice.getAccountAndContact();

        }//end of runAs-block

    }




    @IsTest
    static void testIsCommunityUser(){

        User usr = getUser( TEST_LASTNAME );

        //run in test user context
        System.runAs( usr ){

            LEX_SupplierInvoice.isCommunityUser();

        }//end of runAs-block

    }





    @IsTest
    static void testGetErrorResponse(){

        LEX_SupplierInvoice.getErrorResponse( 'Testing' );


    }





    /**this function will create a test user
     * @param profile_nameP is the profile we are setting on the user
     * @return will return a User record
     */
    private static void createUser( String profile_nameP ){

        User test_user = new User();                    //this will hold our user
        UserRole user_role = getUserRole( ROLE_NAME );          //get the user role

        //setup the user attributes
        test_user.LastName = TEST_LASTNAME;
        test_user.email = 'test@test.com';
        test_user.Username = 'testwwww@test.com.test1';
        test_user.CommunityNickname = 'testing';
        test_user.Alias = 'testing';
        test_user.EmailEncodingKey = 'ISO-8859-1';
        test_user.TimeZoneSidKey = 'America/Los_Angeles';
        test_user.LocaleSidKey = 'en_US';
        test_user.LanguageLocaleKey = 'en_US';
        test_user.UserRoleId = user_role.Id;

        test_user.ProfileId = [ SELECT Id
                                FROM Profile
                                WHERE Name =: profile_nameP ].Id;

        insert test_user;

    }//end of function definition





    private static void createCommunityUser( String contact_idP  ){

        User test_user = new User();                    //this will hold our user

        //setup the user attributes
        test_user.LastName = TEST_COMMUNITY_LASTNAME;
        test_user.email = 'test2@test.com';
        test_user.Username = 'testwwww@test2.com.test1';
        test_user.CommunityNickname = 'testing2';
        test_user.Alias = 'testing2';
        test_user.EmailEncodingKey = 'ISO-8859-1';
        test_user.TimeZoneSidKey = 'America/Los_Angeles';
        test_user.LocaleSidKey = 'en_US';
        test_user.LanguageLocaleKey = 'en_US';
        test_user.ContactId = contact_idP;
        test_user.isActive = true;

        test_user.ProfileId = [ SELECT Id
                                FROM Profile
                                WHERE Name =: 'Tecex Customer' ].Id;

        insert test_user;

    }//end of function definition





    private static void createAccountAndContact( String user_idP ){

        Account test_account = new Account( name = ACCOUNT_NAME, Type = 'Client', CSE_IOR__c = user_idP );              //this is our test account
        insert test_account;

        Contact test_contact = new Contact( LastName = CONTACT_LASTNAME, AccountId = test_account.Id );                        //this is our test contact
        insert test_contact;

    }




    private static void createRole(){

        UserRole test_role = new UserRole( DeveloperName='DeveloperName', Name=ROLE_NAME );
        insert test_role;

    }//end of function definition




    private static Account getAccount( String account_nameP ){

        Account acc = [ SELECT Id, Name, Type, CSE_IOR__c
                        FROM Account
                        WHERE Name =: account_nameP ];

        return acc;

    }





    private static Contact getContact( String contact_last_nameP ){

        Contact con = [ SELECT Id, LastName, AccountId
                        FROM Contact
                        WHERE LastName =: contact_last_nameP ];

        return con;

    }





    /** this function will return User record
     *  @param last_nameP is the last name of the user
     */
    private static User getUser( String last_nameP ){

        //select the user
        User usr = [ SELECT Id
                     FROM User
                     WHERE LastName =: last_nameP ];

        return usr;             //return the user object

    }//end of function definition





    /** this function will return a user role
     *  @ role_nameP is the name of the role
     */
    private static UserRole getUserRole( String role_nameP ){

        UserRole user_role = [ SELECT Id
                               FROM UserRole
                               WHERE Name =: role_nameP ];

        return user_role;

    }
    
    
    private static testMethod void  setUpData1(){
        Account account = new Account (name='Acme1', Type ='Client', CSE_IOR__c = '0050Y000001LTZO', Service_Manager__c = '0050Y000001LTZO');
        insert account;  
        
        Account account2 = new Account (name='Test Supplier', Type ='Supplier', ICE__c = '0050Y000001km5c', RecordTypeId = '0120Y0000006KjQQAU');
        insert account2;      
         
        Contact contact = new Contact ( lastname ='Testing Individual',  AccountId = account.Id);
        insert contact;  
        
        IOR_Price_List__c iorpl = new IOR_Price_List__c ( Client_Name__c = account.Id, Name = 'Brazil', IOR_Fee__c = 0.15, On_Charge_Mark_up__c = 0.15,
        TecEx_Shipping_Fee_Markup__c =0.40, IOR_Min_Fee__c = 100,  Admin_Fee__c =100, Set_up_fee__c =100, Bank_Fees__c =200, Tax_Rate__c = 0.20,
        Estimated_Customs_Brokerage__c =250, Estimated_Customs_Clearance__c =250, Estimated_Customs_Handling__c =250, Estimated_Import_License__c =250, Destination__c = 'Brazil' );
        insert iorpl; 
       
        country_price_approval__c cpa = new country_price_approval__c( name ='Brazil', Billing_term__c = 'DAP/CIF - IOR pays',  Airwaybill_Instructions__c = 'User IOR Address', Supplier__c = account2.Id, 
        In_Country_Specialist__c = '0050Y000001km5c');
        insert cpa;
       
        Currency_Management2__c CM = new Currency_Management2__c (Name = 'US Dollar (USD)', Currency__c = 'US Dollar (USD)', ISO_Code__c = 'USD', Conversion_Rate__c = 1);
        insert CM; 
        
        
        
       
        CPA_v2_0__c RelatedCPA = new CPA_v2_0__c(Name = 'Brazil', Country_Applied_Costings__c=true );
        insert RelatedCPA;
       
        system.debug('cpav21-->'+RelatedCPA);
       
       
       CPA_v2_0__c cpav2 = new CPA_v2_0__c(Name = 'Brazil Testing 2.0', Supplier__c = account2.Id, Country_Applied_Costings__c=false, Service_Type__c = 'IOR/EOR', Related_Costing_CPA__c = RelatedCPA.Id, Preferred_Supplier__c = TRUE,
                                           VAT_Rate__c = 0.1, Destination__c = 'Brazil');
        insert cpav2;

         List<CPA_Costing__c> costs = new List<CPA_Costing__c>();
       
       
      costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'Shipment value', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'IOR', Min__c = 375, Max__c = 14000, Conditional_value__c = 'Shipment value', Condition__c = 'Between', Floor__c = 0, Ceiling__c = 10000, Currency__c = 'US Dollar (USD)'));
      costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'CIF value', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'IOR', Max__c = 50000, Conditional_value__c = 'Shipment value', Condition__c = 'Between', Floor__c = 0, Ceiling__c = 10000, Currency__c = 'US Dollar (USD)'));
      costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'Total Taxes', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'IOR', Min__c = 14000, Conditional_value__c = 'Shipment value', Condition__c = '<', Floor__c = 20000,  Currency__c = 'US Dollar (USD)'));
      costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'Shipment value', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'IOR', Conditional_value__c = 'CIF value', Condition__c = '>=', Floor__c = 5000, Currency__c = 'US Dollar (USD)'));

      costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'Shipment value', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'IOR', Min__c = 375, Max__c = 14000, Currency__c = 'US Dollar (USD)', Variable_threshold__c = 1000));
      costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'CIF value', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'IOR', Max__c = 50000, Currency__c = 'US Dollar (USD)', Variable_threshold__c = 5000));
      costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'Total Taxes', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'IOR', Min__c = 14000, Currency__c = 'US Dollar (USD)', Variable_threshold__c = 500));
      costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'Shipment value', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'IOR', Currency__c = 'US Dollar (USD)', Variable_threshold__c = 9000));

      
      costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'Chargeable weight', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'IOR', Min__c = 375, Max__c = 500, Conditional_value__c = 'Chargeable weight', Condition__c = 'Between', Floor__c = 0, Ceiling__c = 500, Currency__c = 'US Dollar (USD)'));
      costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = '# packages', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'IOR', Max__c = 200, Conditional_value__c = '# of packages', Condition__c = '>', Floor__c = 5,  Currency__c = 'US Dollar (USD)'));
      costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = '# Parts (line items)', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'IOR', Min__c = 100, Currency__c = 'US Dollar (USD)'));
      costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = '# Unique HS Codes', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'IOR', Currency__c = 'US Dollar (USD)'));

      costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'Chargeable weight', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'IOR', Min__c = 375, Max__c = 500, Currency__c = 'US Dollar (USD)', Variable_threshold__c = 100));
      costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = '# packages', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'IOR', Max__c = 200, Currency__c = 'US Dollar (USD)', Variable_threshold__c = 4));
      costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = '# Parts (line items)', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'IOR', Min__c = 100, Currency__c = 'US Dollar (USD)', Variable_threshold__c = 2));
      costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = '# Final Deliveries', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'IOR', Currency__c = 'US Dollar (USD)',  Variable_threshold__c = 2));
	  
      costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Cost_Type__c = 'Fixed',  Amount__c = 800, IOR_EOR__c = 'IOR', Min__c = 375, Max__c = 14000, Currency__c = 'US Dollar (USD)'));
      costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Cost_Type__c = 'Fixed',  Amount__c = 900, IOR_EOR__c = 'IOR', Max__c = 1000, Currency__c = 'US Dollar (USD)'));
      costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Cost_Type__c = 'Fixed',  Amount__c = 100, IOR_EOR__c = 'IOR', Min__c = 200, Currency__c = 'US Dollar (USD)'));
      costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Cost_Type__c = 'Fixed',  Amount__c = 350, IOR_EOR__c = 'IOR', Currency__c = 'US Dollar (USD)'));
      
       
      costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'Shipment value', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'EOR', Min__c = 375, Max__c = 14000, Conditional_value__c = 'Shipment value', Condition__c = 'Between', Floor__c = 0, Ceiling__c = 10000, Currency__c = 'US Dollar (USD)'));
      costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'CIF value', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'EOR', Max__c = 50000, Conditional_value__c = 'Shipment value', Condition__c = 'Between', Floor__c = 0, Ceiling__c = 10000, Currency__c = 'US Dollar (USD)'));
      costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'Total Taxes', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'EOR', Min__c = 14000, Conditional_value__c = 'Shipment value', Condition__c = '<', Floor__c = 20000,  Currency__c = 'US Dollar (USD)'));
      costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'Shipment value', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'EOR', Conditional_value__c = 'Shipment value', Condition__c = '>', Floor__c = 20000, Currency__c = 'US Dollar (USD)'));

      costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'Shipment value', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'EOR', Min__c = 375, Max__c = 14000, Currency__c = 'US Dollar (USD)', Variable_threshold__c = 1000));
      costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'CIF value', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'EOR', Max__c = 50000, Currency__c = 'US Dollar (USD)', Variable_threshold__c = 5000));
      costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'Total Taxes', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'EOR', Min__c = 14000, Currency__c = 'US Dollar (USD)', Variable_threshold__c = 500));
      costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'Shipment value', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'EOR', Currency__c = 'US Dollar (USD)', Variable_threshold__c = 9000));

      
      costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'Chargeable weight', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'EOR', Min__c = 375, Max__c = 500, Currency__c = 'US Dollar (USD)'));
      costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = '# packages', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'EOR', Max__c = 200, Currency__c = 'US Dollar (USD)'));
      costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = '# Parts (line items)', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'EOR', Min__c = 100, Currency__c = 'US Dollar (USD)'));
      costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = '# Unique HS Codes', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'EOR', Currency__c = 'US Dollar (USD)'));

      costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'Chargeable weight', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'EOR', Min__c = 375, Max__c = 500, Currency__c = 'US Dollar (USD)', Variable_threshold__c = 100));
      costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = '# packages', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'EOR', Max__c = 200, Currency__c = 'US Dollar (USD)', Variable_threshold__c = 4));
      costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = '# Parts (line items)', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'EOR', Min__c = 100, Currency__c = 'US Dollar (USD)', Variable_threshold__c = 2));
      costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = '# Final Deliveries', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'EOR', Currency__c = 'US Dollar (USD)',  Variable_threshold__c = 2));
	  
      costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Cost_Type__c = 'Fixed',  Amount__c = 800, IOR_EOR__c = 'EOR', Min__c = 375, Max__c = 14000, Currency__c = 'US Dollar (USD)'));
      costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Cost_Type__c = 'Fixed',  Amount__c = 900, IOR_EOR__c = 'EOR', Max__c = 1000, Currency__c = 'US Dollar (USD)'));
      costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Cost_Type__c = 'Fixed',  Amount__c = 100, IOR_EOR__c = 'EOR', Min__c = 200, Currency__c = 'US Dollar (USD)'));
      costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Cost_Type__c = 'Fixed',  Amount__c = 350, IOR_EOR__c = 'EOR', Currency__c = 'US Dollar (USD)'));
      
      costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'Anil', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'IOR', Min__c = 375, Max__c = 500, Currency__c = 'US Dollar (USD)', Variable_threshold__c = null));
      costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'Anil', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'IOR', Max__c = 200, Currency__c = 'US Dollar (USD)', Variable_threshold__c = null));
      costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'Anil', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'IOR', Min__c = 100, Currency__c = 'US Dollar (USD)', Variable_threshold__c = null));
      costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'Anil', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'IOR', Currency__c = 'US Dollar (USD)',  Variable_threshold__c = null));
       
       
      
       
       
        insert costs;
       
        
        
        Shipment_Order__c shipmentOrder = new Shipment_Order__c(Account__c = account.id, Shipment_Value_USD__c =10000, CPA_v2_0__c = cpav2.Id,
        Client_Contact_for_this_Shipment__c = contact.Id, Who_arranges_International_courier__c ='Client', Tax_Treatment__c ='DAP/CIF - IOR pays', Ship_to_Country__c = cpa.Id,
        Destination__c = 'Brazil', Service_Type__c = 'IOR', IOR_Price_List__c = iorpl.Id, of_Unique_Line_Items__c = 10, Total_Taxes__c = 1500, of_Line_Items__c= 10, Chargeable_Weight__c = 200, of_packages__c = 20, Final_Deliveries_New__c = 10,
        CI_Admin_Fee__c  =100, CI_Bank_Fees__c  = 100, CI_Finance_Fee__c  = 100, CI_Insurance_Fee_USD__c  = 100, CI_International_Delivery_Fee__c  = 100, CI_IOR_and_Import_Compliance_Fee_USD__c  = 100, 
        CI_Miscellaneous_Fee__c  = 100, CI_Recharge_Tax_and_Duty__c  = 100, CI_Total_Clearance_Costs__c  = 100, CI_Total_Customs_Brokerage_Cost__c  = 100, CI_Total_Handling_Cost__c  = 100, 
        CI_Total_Licence_Cost__c  = 100, FC_Admin_Fee__c  = 100, FC_Bank_Fees__c  = 100, FC_Finance_Fee__c  = 100, FC_Insurance_Fee_USD__c   = 100, FC_International_Delivery_Fee__c  = 100, 
        FC_IOR_and_Import_Compliance_Fee_USD__c  = 100, FC_Miscellaneous_Fee__c  = 100, FC_Recharge_Tax_and_Duty__c  = 100, FC_Total_Clearance_Costs__c  = 100, 
        FC_Total_Customs_Brokerage_Cost__c  = 100, FC_Total_Handling_Costs__c  = 100, FC_Total_License_Cost__c  = 100, Total_CIF_Duties__c = 0, Total_FOB_Duties__c = 0);
        insert shipmentOrder; 
       
        Shipment_Order__c shipmentOrder2 = new Shipment_Order__c(Account__c = account.id, Shipment_Value_USD__c =10000, CPA_v2_0__c = cpav2.Id,
        Client_Contact_for_this_Shipment__c = contact.Id, Who_arranges_International_courier__c ='Client', Tax_Treatment__c ='DAP/CIF - IOR pays', Ship_to_Country__c = cpa.Id,
        Destination__c = 'Brazil', Service_Type__c = 'EOR', IOR_Price_List__c = iorpl.Id, of_Unique_Line_Items__c = 10, Total_Taxes__c = 1500, of_Line_Items__c= 10, Chargeable_Weight__c = 200, of_packages__c = 20, Final_Deliveries_New__c = 10,
        CI_Admin_Fee__c  =100, CI_Bank_Fees__c  = 100, CI_Finance_Fee__c  = 100, CI_Insurance_Fee_USD__c  = 100, CI_International_Delivery_Fee__c  = 100, CI_IOR_and_Import_Compliance_Fee_USD__c  = 100, 
        CI_Miscellaneous_Fee__c  = 100, CI_Recharge_Tax_and_Duty__c  = 100, CI_Total_Clearance_Costs__c  = 100, CI_Total_Customs_Brokerage_Cost__c  = 100, CI_Total_Handling_Cost__c  = 100, 
        CI_Total_Licence_Cost__c  = 100, FC_Admin_Fee__c  = 100, FC_Bank_Fees__c  = 100, FC_Finance_Fee__c  = 100, FC_Insurance_Fee_USD__c   = 100, FC_International_Delivery_Fee__c  = 100, 
        FC_IOR_and_Import_Compliance_Fee_USD__c  = 100, FC_Miscellaneous_Fee__c  = 100, FC_Recharge_Tax_and_Duty__c  = 100, FC_Total_Clearance_Costs__c  = 100, 
        FC_Total_Customs_Brokerage_Cost__c  = 100, FC_Total_Handling_Costs__c  = 100, FC_Total_License_Cost__c  = 100, Total_CIF_Duties__c = 0, Total_FOB_Duties__c = 0);
        insert shipmentOrder2; 
       
      
       
     
       
       
       Freight__c Fr = New Freight__c(Logistics_provider__c=account2.Id,Li_Ion_Batteries__c='No',Shipment_Order__c=shipmentOrder.Id,Ship_Froma__c='Angola',Ship_From__c='Angola',Ship_To__c='Australia',Chargeable_weight_in_KGs_packages__c=1,Estimated_chargeable_weight__c=1);
       Insert Fr;
       
       
       
       Supplier_Invoice__c SI = New Supplier_invoice__C(Name='SI001',Due_date__c= date.today(),Supplier_Name__c=account2.Id,Invoice_Currency__c='US Dollar (USD)',Shipment_Order_Name__c=shipmentOrder.id);
       insert SI;
       Supplier_Invoice__c SI2 = New Supplier_invoice__C(Name='SI002',Due_date__c= date.today(),Supplier_Name__c=account2.Id,Invoice_Currency__c='US Dollar (USD)');
       insert SI2;
       Supplier_Invoice__c SI3 = New Supplier_invoice__C(Name='SI003',Due_date__c= date.today(),Supplier_Name__c=account2.Id,Invoice_Currency__c='US Dollar (USD)',Freight_request__C=fr.id);
       insert SI3;
     
       List<CPA_Costing__c> costs2 = new List<CPA_Costing__c>();
       costs2.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',Invoice_amount_local_currency__c =123, CPA_v2_0__c = RelatedCPA.Id, Applied_to__c = 'Shipment value', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'IOR', Min__c = 375, Max__c = 14000, Conditional_value__c = 'Shipment value', Condition__c = 'Between', Floor__c = 0, Ceiling__c = 10000, Currency__c = 'US Dollar (USD)',Supplier_Invoice__c=si.Id,RecordTypeId='0120Y000000ytNmQAI'));
       costs2.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',Invoice_amount_local_currency__c =1234,CPA_v2_0__c = RelatedCPA.Id, Applied_to__c = 'CIF value', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'IOR', Max__c = 50000, Conditional_value__c = 'Shipment value', Condition__c = 'Between', Floor__c = 0, Ceiling__c = 10000, Currency__c = 'US Dollar (USD)',Supplier_Invoice__c=si.Id,RecordTypeId='0120Y000000ytNmQAI'));
       costs2.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',Invoice_amount_local_currency__c =null,CPA_v2_0__c = RelatedCPA.Id, Applied_to__c = 'Total Taxes', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'IOR', Min__c = 14000, Conditional_value__c = 'Shipment value', Condition__c = '<', Floor__c = 20000,  Currency__c = 'US Dollar (USD)',Supplier_Invoice__c=si.Id,RecordTypeId='0120Y000000ytNmQAI'));
       costs2.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = RelatedCPA.Id, Applied_to__c = 'Shipment value', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'IOR', Conditional_value__c = 'CIF value', Condition__c = '>=', Floor__c = 5000, Currency__c = 'US Dollar (USD)',Supplier_Invoice__c=si.Id,RecordTypeId='0120Y000000ytNmQAI'));
       Insert costs2; 
       
       
       List<CPA_Costing__c> costs1 = new List<CPA_Costing__c>();
       costs1.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',Invoice_amount_local_currency__c =123, Applied_to__c = 'Shipment value', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'IOR', Min__c = 375, Max__c = 14000, Conditional_value__c = 'Shipment value', Condition__c = 'Between', Floor__c = 0, Ceiling__c = 10000, Currency__c = 'US Dollar (USD)',Supplier_Invoice__c=si.Id));
       costs1.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',Invoice_amount_local_currency__c =1234, Applied_to__c = 'CIF value', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'IOR', Max__c = 50000, Conditional_value__c = 'Shipment value', Condition__c = 'Between', Floor__c = 0, Ceiling__c = 10000, Currency__c = 'US Dollar (USD)',Supplier_Invoice__c=si.Id));
       costs1.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',Invoice_amount_local_currency__c =null, Applied_to__c = 'Total Taxes', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'IOR', Min__c = 14000, Conditional_value__c = 'Shipment value', Condition__c = '<', Floor__c = 20000,  Currency__c = 'US Dollar (USD)',Supplier_Invoice__c=si.Id));
       costs1.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost', Applied_to__c = 'Shipment value', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'IOR', Conditional_value__c = 'CIF value', Condition__c = '>=', Floor__c = 5000, Currency__c = 'US Dollar (USD)',Supplier_Invoice__c=si.Id));
    //  Insert costs1; 
      
           
       List<CPA_Costing__c> CCO1 = [Select Additional_Percent__c, Amended__c, Amount__c, Amount_USD__c, Applied_to__c, Ceiling__c, Condition__c, Conditional_value__c, Cost_Category__c, Cost_Note__c, Cost_Type__c, CostStatus__c, Name, CPA_v2_0__c, CreatedById, Currency__c, Exchange_rate_forecast__c, Exchange_rate_payment_date__c, Floor__c, Amount_in_USD__c, Freight_Request__c, Invoice_amount_local_currency__c, Invoice_amount_USD_New__c, Invoice_currency__c, IOR_EOR__c, LastModifiedById, Max__c, Min__c, Rate__c, RecordTypeId, Shipment_Order_Old__c, Status_Colour__c, Supplier_Invoice__c, Updating__c, Variable_threshold__c, VAT_applicable__c, VAT_Rate__c, VAT_Rate_Formula__c 
       from CPA_Costing__c where Supplier_Invoice__c =: SI.Id limit 1];
       
       
              
        List<Id> soIds = new List<Id>();
        soIds.add(shipmentOrder.id); 
       
        List<Id> soIds2 = new List<Id>();
        soIds2.add(shipmentOrder2.id); 


                  
     test.startTest();
       
       
       //   CalculateCosts.createSOCosts(soIds);
       // CalculateCosts.createSOCosts(soIds2);
       // shipmentOrder.Shipment_Value_USD__c = 20000;
       // UPSERT shipmentOrder;
        
       Lex_SupplierInvoice.getCreatedSIs(SI.ID);
      
       


  


       test.stopTest();
       
    } 




}