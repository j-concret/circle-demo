@isTest (seeAllData = true)
private class ShipToPicklist_Ext_Test {
    
    
    //Test Lead the is ready to convert
    @isTest static void test_Positve_TestCase() {
    
        Account testClient = new account(Name='TestClient',Client_Status__c = 'Prospect',Estimated_Vat__c = 123);
        
        insert testClient;
        
        Product2 testProduct = new Product2(Name='Japan');
        insert testProduct;
        
        Pricebook2 testPricebook = new Pricebook2(Name='TestClient',isActive = True);
        insert testPricebook;
        
        Pricebook2 testPricebookStd = [Select Id from Pricebook2 where Name = 'Standard Price Book'];
        
        PricebookEntry testEntry = new PricebookEntry(Product2Id = testProduct.Id, Pricebook2Id = testPricebookStd.Id, IsActive = true, UnitPrice = 1,IOR_Fee__c =10);
        insert testEntry;
        
        PricebookEntry testEntry2 = new PricebookEntry(Product2Id = testProduct.Id, Pricebook2Id = testPricebook.Id, IsActive = true, UnitPrice = 1,IOR_Fee__c =10);
        insert testEntry2;
        
        IOR_Price_List__c testIOR = new IOR_Price_List__c();
            
          testIOR.Client_Name__c = testClient.Id;
            testIOR.Name = 'Japan';
            testIOR.IOR_Fee__c = 1;
        
        insert testIOR;
        
        Opportunity testOpp = new opportunity(Name='testOpp',AccountId = testClient.Id, StageName ='Meeting Scheduled', closeDate = System.today(), Pricebook2Id = testPricebook.Id, Shipment_Value_USD__c = 1000, IOR_Price_List__c = testIOR.Id,product_ID__c=testEntry2.Id);
        insert testOpp;
        
        //Standard controller and extension instance to test
        Test.startTest();
        ApexPages.StandardController sc = new ApexPages.StandardController(testOpp);
        ShipToPicklist_Ext testProductList = new ShipToPicklist_Ext(sc);
        
        PageReference pageRef = Page.OpportunityPageOverride_VF;
        Test.setCurrentPage(pageRef);
        
        List<SelectOption> testList = testProductList.getPicklistOptions();
        
        PageReference testPageRedirect = testProductList.PbEntryCreate();
        Test.stopTest();
        
        System.assertNotEquals(null, testPageRedirect);
        
    }
}