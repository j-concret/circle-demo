@RestResource(urlMapping='/ValidateEmail/*')
Global class ValidateEmail {

    //Get Request
    @HttpGet
    global static void Validate(){
        RestRequest req = RestContext.request;
    	RestResponse res = RestContext.response;
        res.addHeader('Content-Type', 'application/json');
        String Username = req.params.get('Username');
        
        try {
        Contact logincontact = [select Id,Account.id, lastname,Email,Password__c,Contact_Status__c from Contact where UserName__c =:username];
        
                    	
            //List<List<SObject> > logincontact = [FIND :Password IN ALL FIELDS Returning Contact (Id,password__c)];
            //List<List<SObject> > logincontact = [FIND :Username IN email FIELDS Returning Contact (Id,name,email,password__c where email =: Username )];
            if(logincontact.Contact_status__C == 'Active'){
               
                    res.responseBody = Blob.valueOf(JSON.serializePretty(logincontact));
        			res.statusCode = 200;
                API_Log__c Al = New API_Log__c();
                Al.Account__c=logincontact.AccountID;
                Al.Login_Contact__c=logincontact.Id;
                Al.EndpointURLName__c='ValidateEmail';
                Al.Response__c='Email validated succesfully';
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al;
                     
            
              }
            else{
                    String ErrorString ='Validation failed, Contact not active';
               		res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
        			res.statusCode = 400;
                API_Log__c Al = New API_Log__c();
                Al.Account__c=logincontact.AccountId;
                Al.Login_Contact__c=logincontact.Id;
                Al.EndpointURLName__c='ValidateEmail';
                Al.Response__c='Validation failed, Contact not active';
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al;
                
            	}
        
            }
            catch (Exception e) {
            String ErrorString ='Something went wrong while validating username, please contact support_SF@tecex.com';
               	res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
            res.statusCode = 500;
                API_Log__c Al = New API_Log__c();
              
                Al.EndpointURLName__c='ValidateEmail';
                Al.Response__c=e.getMessage()+'- Exception - ' +Username ;
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al;
                
            
              
            } 
        
        
    }
    
    
}