@RestResource(urlMapping='/UpdateFinalDeliveryAddressV2/*')
Global class NCPUpdateFinalDeliveryV2 {
    
   @Httppost
    global static void UpdateFinalDelivery(){
    
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        Blob body = req.requestBody;
    	res.addHeader('Content-Type', 'application/json');
        String requestString = body.toString();
        NCPUpdateFinalDeliveryWraV2 rw = (NCPUpdateFinalDeliveryWraV2)JSON.deserialize(requestString,NCPUpdateFinalDeliveryWraV2.class);
        Final_Delivery__c FD = [select id,ship_to_Address__C,Shipment_Order__r.account__c,Name,Contact_name__c,Contact_email__c,Contact_number__c,Address_Line_1__c,Address_Line_2__c,City__c,Zip__c,Country__c from Final_Delivery__c where id=:rw.ID];
           try {
               
               
                Access_token__c At =[select status__c,Access_Token__c from Access_token__c where Access_Token__c=:rw.Accesstoken ];
            	if( at.status__C =='Active'){
                    
               Client_Address__C CA = [Select Id,Name,Contact_Full_Name__c,Contact_Email__c,Contact_Phone_Number__c,Address__c,Address2__c,City__c,Province__c,Postal_Code__c,All_Countries__c,CompanyName__c from Client_Address__c where id = :rw.ShiptoaddressID];      
            
            CA.Name = Rw.Name;
            CA.Contact_Full_Name__c= rw.ContactName;
            CA.Contact_Email__c = rw.ContactEmail;
            CA.Contact_Phone_Number__c = rw.ContactNumber;
            CA.Address__c=rw.ContactAddreaaLine1;
            CA.Address2__c=rw.ContactAddreaaLine2;
            CA.City__c=rw.City;
            CA.Province__c=rw.Province;
            CA.Postal_Code__c=rw.ZIP;
            CA.All_Countries__c=rw.Country;
            CA.CompanyName__c=rw.CompanyName;
            
            Update CA;  
                    
            FD.ship_to_Address__C = rw.ShiptoaddressID;        
            Update FD;        
                    
         /*           
            FD.name = Rw.Name;
            FD.Contact_name__c= rw.ContactName;
            FD.Company_name__c= rw.CompanyName;
            FD.Contact_email__c = rw.ContactEmail;
            FD.Contact_number__c = rw.ContactNumber;
            FD.Address_Line_1__c=rw.ContactAddreaaLine1;
            FD.Address_Line_2__c=rw.ContactAddreaaLine2;
            FD.City__c=rw.City;
            FD.Zip__c=rw.ZIP;
            FD.Country__c=rw.Country;
            
            
            Update FD;
           */ 
            JSONGenerator gen = JSON.createGenerator(true);
            gen.writeStartObject();
            
             gen.writeFieldName('Success');
             gen.writeStartObject();
            
             	if(rw.ID == null) {gen.writeNullField('Response');} else{gen.writeStringField('Response', 'Success-Final Delivery Updated');}
            
            gen.writeEndObject();
            gen.writeEndObject();
            String jsonData = gen.getAsString();
            res.responseBody = Blob.valueOf(jsonData);
            res.addHeader('Content-Type', 'application/json');
            res.statusCode = 200;        
           
            
                API_Log__c Al = New API_Log__c();
                Al.Account__c=FD.Shipment_Order__r.Account__c;
                Al.EndpointURLName__c='NCP-UpdateFinalDelivery';
                Al.Response__c='Success- FinalDelivery Updated';
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al; 
                }
               
               Else{
             JSONGenerator gen = JSON.createGenerator(true);
            gen.writeStartObject();
            
             gen.writeFieldName('Error');
             gen.writeStartObject();
            
             	if(rw.ID == null) {gen.writeNullField('Response');} else{gen.writeStringField('Response', 'Accesstoken expired');}
            
            gen.writeEndObject();
            gen.writeEndObject();
            String jsonData = gen.getAsString();
            res.responseBody = Blob.valueOf(jsonData);
            res.addHeader('Content-Type', 'application/json');
            res.statusCode = 400;        
                  
                   
               }
            }
        	catch (Exception e){
                
              	 String ErrorString ='Something went wrong while updating final delivery details, please contact support_SF@tecex.com';
               	res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
                res.addHeader('Content-Type', 'application/json');
                res.statusCode = 500;
                API_Log__c Al = New API_Log__c();
                Al.Account__c=FD.Shipment_Order__r.Account__c;
                Al.EndpointURLName__c='NCP-UpdateFinalDelivery';
                Al.Response__c=e.getMessage()+' at line number: '+e.getLineNumber();
                Al.StatusCode__c=string.valueof(res.statusCode);
              	Insert Al;
		     	}

        
        
    }
       

}