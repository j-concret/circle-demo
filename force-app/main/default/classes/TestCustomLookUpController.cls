@isTest
public class TestCustomLookUpController {
    
    @testSetup static void setup() {
        
        Account account2 = new Account (name='Test Manufacturer', Type ='Manufacturer', ICE__c = '0050Y000001km5c', RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Manufacturer').getRecordTypeId()); 
        insert account2;
        List<Product2> productList = new List<Product2>();
        
        for(Integer i=0;i<10;i++){
            
            productList.add(new Product2(Name = 'Test '+i,
                                         Manufacturer__c = account2.Id,
                                         Family = 'Hardware'));
            
        }
        
        insert productList;
    }
    
    @isTest static void testFetchLookUpValues() {
        List<Product2> productList = customLookUpController.fetchLookUpValues('Test','Product2','productNumber');
        System.assert((productList[0].Name).containsIgnoreCase('Test'));
    }
}