/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* ******class.
*.
* methods:
*           getRelatedContacts(),getUserEmail(),sendStatement(),saveChunk()
*
*           
*           
*
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Khonology01
* @version        *.*
* @created        2019/04/17
* @systemLayer    ******* ( Invocation )
* ──────────────────────────────────────────────────────────────────────────────────────────────────
*
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
public with sharing class CTRL_EmailRelatedContact {

    @AuraEnabled
    public static List<Contact> getRelatedContacts(String clientId){
        return [SELECT Id, Name, Email, Title from Contact where AccountId = :clientId];
    }

    @AuraEnabled
    public static String getUserEmail() {
        return UserInfo.getUserEmail();
    }

    @AuraEnabled
    public static Id generateStatement(Id clientId, String customerName, String statementType) {
        Id statementId;
        try {
            statementId = StatementService.attachStatement(clientId,customerName,statementType);
        } catch (Exception e){
            throw new AuraHandledException('Unable to attach statement! '+e.getMessage());
        }

        return statementId;
    }

    @AuraEnabled
    public static Boolean sendStatement(Id customerId, List<String> toAddress, String ccAddress,Id statementId) {

        return StatementService.sendCustomerStatement(customerId,toAddress,ccAddress,statementId);
    }

    @AuraEnabled
    public static String getStatement(Id clientId, String customerName, String statementType) {

        return StatementService.getStatement(clientId, customerName, statementType);
    }


    @AuraEnabled
    public static Id saveChunk(Id parentId, String fileName, String base64Data, String contentType, String fileId) {

        if (fileId == '') {
            fileId = saveTheFile(parentId, fileName, base64Data, contentType);
        } else {
            appendToFile(fileId, base64Data);
        }

        return Id.valueOf(fileId);
    }

     @TestVisible private static Id saveTheFile(Id parentId, String fileName, String base64Data, String contentType) {
        base64Data = EncodingUtil.urlDecode(base64Data, 'UTF-8');

        Attachment oAttachment = new Attachment();
        oAttachment.parentId = parentId;

        oAttachment.Body = EncodingUtil.base64Decode(base64Data);
        oAttachment.Name = fileName;
        oAttachment.ContentType = contentType;

        insert oAttachment;

        return oAttachment.Id;
    }

   @TestVisible private static void appendToFile(Id fileId, String base64Data) {
        base64Data = EncodingUtil.urlDecode(base64Data, 'UTF-8');

        Attachment a = [
                SELECT Id, Body
                FROM Attachment
                WHERE Id =: fileId
        ];

        String existingBody = EncodingUtil.base64Encode(a.Body);

        a.Body = EncodingUtil.base64Decode(existingBody + base64Data);

        update a;
    }
}