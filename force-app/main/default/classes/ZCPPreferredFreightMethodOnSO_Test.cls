@isTest
public class ZCPPreferredFreightMethodOnSO_Test{
    
    public static testMethod void testCase1(){
        
        Access_token__c accessTokenObj = new Access_token__c();
        accessTokenObj.Status__c = 'Active';
        accessTokenObj.Access_Token__c = 'asdfghj-sdfgfds-sfdfgf';
        
        Insert accessTokenObj;
        
        
        ZCPPreferredFreightMethodOnSOwra obj = new ZCPPreferredFreightMethodOnSOwra();
        
        obj.ShipFromcountry = 'United States';
        obj.ShipTocountry = 'Australia';
        obj.Accesstoken = accessTokenObj.Access_Token__c;
        
        Blob body = Blob.valueOf(JSON.serialize(obj));
        
        RestRequest req1 = new RestRequest();
        RestResponse res1 = new RestResponse();
        req1.requestURI = '/services/apexrest/ZCPPreferredFreightMethod'; 
        req1.httpMethod = 'POST';
        req1.requestBody = Blob.valueOf(JSON.serialize(obj));     
        RestContext.request = req1;
        RestContext.response = res1;
        RestContext.request.addHeader('Content-Type', 'application/json');
        
        Test.startTest(); 
        ZCPPreferredFreightMethodOnSO.ZCPPreferredFreightMethodOnSO();
        Test.stopTest();
    }
    
    public static testMethod void testCase2(){
        
        ZCPPreferredFreightMethodOnSOwra obj = new ZCPPreferredFreightMethodOnSOwra();
        
        obj.ShipFromcountry = 'United States';
        obj.ShipTocountry = 'Australia';
        obj.Accesstoken = 'Abc';
        
        Blob body = Blob.valueOf(JSON.serialize(obj));
        
        RestRequest req1 = new RestRequest();
        RestResponse res1 = new RestResponse();
        req1.requestURI = '/services/apexrest/ZCPPreferredFreightMethod'; 
        req1.httpMethod = 'POST';
        req1.requestBody = Blob.valueOf(JSON.serialize(obj));     
        RestContext.request = req1;
        RestContext.response = res1;
        RestContext.request.addHeader('Content-Type', 'application/json');
        
        Test.startTest(); 
        ZCPPreferredFreightMethodOnSO.ZCPPreferredFreightMethodOnSO();
        Test.stopTest();
    }
    
}