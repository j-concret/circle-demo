public with sharing class ProductCatalogueTiggerHandler {

  Public static Boolean isRecursive = false;

    public static void process(Set<Id> ids) {
        List<String> partNames = new List<String>();
        Set<Id> accountIds = new Set<Id>();
        Map<String,Product_Catalogue__c> catalogues = new Map<String,Product_Catalogue__c>();
        List<Product_Catalogue__c> updateCatalogues = new List<Product_Catalogue__c>();

        for(Product_Catalogue__c pc :[SELECT Id,Account__c,Number_of_times_shipped__c,Part__r.Name FROM Product_Catalogue__c WHERE Id IN:ids]) {
            partNames.add(pc.Part__r.Name);
            accountIds.add(pc.Account__c);
            catalogues.put(pc.Part__r.Name+pc.Account__c,pc);
        }

        for(AggregateResult a :  [SELECT Name, count(Shipment_Order__c) total, Shipment_Order__r.Account__c client FROM Part__c Where Name IN:partNames AND Shipment_Order__r.Account__c IN: accountIds GROUP BY Name, Shipment_Order__r.Account__c]) {

            String key = String.valueOf(a.get('Name'))+String.valueOf(a.get('client'));
            if(catalogues.containsKey(key)) {
                catalogues.get(key).Number_of_times_shipped__c = (Decimal) a.get('total');
                updateCatalogues.add(catalogues.get(key));
            }
        }
        update updateCatalogues;
    }
}