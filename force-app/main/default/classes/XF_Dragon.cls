public class XF_Dragon extends HTTPRequest_Base {
    public static final String NAMESPACE_GILBERT = 'urn:vatit-com:dragon-gilbert';
    public static final String NAMESPACE_NASEEM = 'urn:vatit-com:dragon-naseem';
    public static final String NAMESPACE_DRAGON = 'urn:vatit-com:dragon';
    
    private static final String DEV_AORTA_END_POINT = 'https://nod.synthesis.co.za/scripts/aorta.dll';
    private static final String UAT_AORTA_END_POINT = 'https://uat.dragon.vatit.com/scripts/aorta.dll';
    private static final String PROD_AORTA_END_POINT = 'https://api.dragon.vatit.com/scripts/aorta.dll';
  
    private static final String HTTP_ENCODING_XML_UTF_8 = HTTPRequest_Base.HTTP_ENCODING_XML + '; charset=UTF-8';
    private static final Integer WEB_SERVICE_TIME_OUT = 120000;//2 minutes
    private static final String HTTP_SF_USER = 'SF-User';
    
    public override String GetWebServiceEndPoint () {
        String organisationId = Userinfo.getOrganizationId();
        
        if (organisationId.startsWith(PROD_ORG_ID)) 
          return PROD_AORTA_END_POINT;
        else if (organisationId.startsWith(UAT_ORG_ID))
          return UAT_AORTA_END_POINT;
        else
          return DEV_AORTA_END_POINT;
    }
    
    public override String getEncoding()
    {
        return HTTP_ENCODING_XML_UTF_8; 
    }
    
    public override Integer getTimeout()
    {
        return WEB_SERVICE_TIME_OUT;    
    }
    
    private String getNamespace() {
      String result = '';
      if (GetWebServiceEndPoint() != PROD_AORTA_END_POINT)
        result = [SELECT Dragon_API_Namespace__c FROM User WHERE id = :UserInfo.getUserId()].Dragon_API_Namespace__c;
      if (result == '' || result == null)
        result = NAMESPACE_DRAGON;
      System.debug('Namespace: ' + result);
      return result;    
    }
    public override HttpResponse createAndSendRequest(String body)
    {
    	System.debug('Sending request: ' + body);
        HttpRequest request = createRequest(body);  
        request.setHeader(HTTPRequest_Base.HTTP_SOAP_ACTION, getNamespace());
        request.setHeader(HTTP_SF_USER, UserInfo.getUserName());
        HttpResponse response = sendRequest(request);
    	System.debug('Response received: ' + response.getBody());
    	return response;
    }

}