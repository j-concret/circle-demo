@isTest(SeeAllData=false)
public class AddPickupAndFinalDeliveryAddCtrl_Test implements HttpCalloutMock {

    public HTTPResponse respond(HTTPRequest req) {
        HttpResponse resp = new HttpResponse();
        resp.setBody('testBody');
        resp.setStatus('OK');
        resp.setStatusCode(200);
        return resp;
    }

    @TestSetup
    static void makeData(){
        no_zip_code_countries__c nzp = new no_zip_code_countries__c(
            Name='Angola',
            zip_code__c='12345');
        insert nzp;
        
        DefaultFreightValues__c df = new DefaultFreightValues__c();
        df.Name = 'Test';
        df.Courier_Base_Rate_KG__c = 12;
        df.Courier_Dangerous_Goods_premium__c = 12;
        df.Courier_Fixed_Charge__c = 12;
        df.Courier_Non_stackable_premium__c = 12;
        df.Courier_Oversized_premium__c = 12;
        df.FF_Base_Rate_KG__c = 12;
        df.FF_Dangerous_Goods_Fixed_Charge_Premium__c = 12;
        df.FF_Dangerous_Goods_premium__c = 12;
        df.FF_Dedicated_Pickup__c = 12;
        df.FF_Fixed_Charge__c = 12;
        df.FF_Non_stackable_Fixed_Charge_Premium__c = 12;
        df.FF_Non_stackable_premium__c = 12;
        df.FF_Oversized_Fixed_Charge_Premium__c = 12;
        df.FF_Oversized_premium__c = 12;
        insert df;
        
        CountryandRegionMap__c crm = new CountryandRegionMap__c();
        crm.Name = 'Brazil';
        crm.Country__c = 'Brazil';
        crm.European_Union__c = 'No';
        crm.Region__c = 'South America';
        
        insert crm;
        
        CountryandRegionMap__c crm1 = new CountryandRegionMap__c();
        crm1.Name = 'Australia';
        crm1.Country__c = 'Australia';
        crm1.European_Union__c = 'No';
        crm1.Region__c = 'Oceania';
        
        insert crm1;
        CountryandRegionMap__c crm2 = new CountryandRegionMap__c();
        crm2.Name = 'United States';
        crm2.Country__c = 'United States';
        crm2.European_Union__c = 'No';
        crm2.Region__c = 'North America';
        
        insert crm2;
        
        RegionalFreightValues__c rfm = new RegionalFreightValues__c();
        rfm.Name = 'North America to South America';
        rfm.Courier_Discount_Premium__c = 12;
        rfm.Courier_Fixed_Premium__c = 10;
        rfm.Destination__c = 'Brazil';
        rfm.FF_Fixed_Premium__c = 123;
        rfm.FF_Regional_Discount_Premium__c = 12;
        rfm.Origin__c = 'United States';
        rfm.Preferred_Courier_Id__c = 'TECEX';
        rfm.Preferred_Courier_Name__c = 'TECEX';
        rfm.Preferred_Freight_Forwarder_Id__c = 'TECEX';
        rfm.Preferred_Freight_Forwarder_Name__c = 'TECEX';
        rfm.Risk_Adjustment_Factor__c = 1;
        insert rfm;
 
        
        User user = [Select Id, Name, userName, Profile.Name, UserRole.name From user where Profile.Name = 'System Administrator' LIMIT 1];
        
        Id accManufRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Manufacturer').getRecordTypeId();
        Id ecommRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Client (E-commerce)').getRecordTypeId();
        Id accSupplierRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('IOR/EOR Supplier').getRecordTypeId();
   
        
        List<Account> accounts = new List<Account>();

        accounts.add(new Account (name='Acme1', Type ='Client',
                                  CSE_IOR__c = user.Id,
                                  Service_Manager__c = user.Id,
                                  Financial_Manager__c=user.Id,
                                  Financial_Controller__c=user.Id,
                                  Tax_recovery_client__c  = FALSE,
                                  Ship_From_Line_1__c = 'Ship_From_Line_1',
                                  Ship_From_Line_2__c = 'Ship_From_Line_2',
                                  Ship_From_Line_3__c = 'Ship_From_Line_3',
                                  Ship_From_City__c = 'Ship_From_City',
                                  Ship_From_Zip__c = 'Ship_From_Zip',
                                  Ship_From_Country__c = 'Ship_From_Country',
                                  Ship_From_Other_notes__c = 'Ship_From_Other',
                                  Ship_From_Contact_Name__c = 'Ship_From_Contact',
                                  Ship_From_Contact_Email__c = 'Ship_From_Email',
                                  Ship_From_Contact_Tel__c = 'Ship_From_Tel',
                                  Client_Address_Line_1__c = 'Client_Address_Line_1',
                                  Client_Address_Line_2__c = 'Client_Address_Line_2',
                                  Client_Address_Line_3__c = 'Client_Address_Line_3',
                                  Client_City__c = 'Client_City',
                                  Client_Zip__c = 'Client_Zip',
                                  Client_Country__c = 'Client_Country',
                                  Other_notes__c = 'Other_notes',
                                  Tax_Name__c = 'Tax_Name',
                                  Tax_ID__c = 'Tax_ID',
                                  Contact_Name__c = 'Contact_Name',
                                  Contact_Email__c = 'Contact_Email',
                                  Contact_Tel__c = 'Contact_Tel'
                                  ));
        accounts.add(new Account (name='TecEx Prospective Client', Type ='Supplier', ICE__c = user.Id, RecordTypeId = accSupplierRecordTypeId));
        accounts.add(new Account (name='Test Manufacturer', Type ='Manufacturer', RecordTypeId = accManufRecordTypeId, Manufacturer_Alias__c = 'Tester,Who' ));
        accounts.add(new Account (name='TecEx E-Commerce Prospective Client', Type ='Client', ICE__c = user.Id, RecordTypeId = ecommRecordTypeId));

        insert accounts;

        Contact contact = new Contact (Client_Notifications_Choice__c = 'opt-in',email = 'test@test.com', lastname ='Testing Individual',  AccountId = accounts[0].Id);
        insert contact;

        IOR_Price_List__c iorpl = new IOR_Price_List__c ( Client_Name__c = accounts[0].Id, Name = 'Brazil', IOR_Fee__c = 0.15, On_Charge_Mark_up__c = 0.15,
                                                          TecEx_Shipping_Fee_Markup__c =0.40, IOR_Min_Fee__c = 100,  Admin_Fee__c =100, Set_up_fee__c =100, Bank_Fees__c =200, Tax_Rate__c = 0.20,
                                                          Estimated_Customs_Brokerage__c =250, Estimated_Customs_Clearance__c =250, Estimated_Customs_Handling__c =250, Estimated_Import_License__c =250, Destination__c = 'Brazil' );
        insert iorpl;

        country_price_approval__c cpa = new country_price_approval__c( name ='Brazil', Billing_term__c = 'DAP/CIF - IOR pays',  Airwaybill_Instructions__c = 'User IOR Address', Supplier__c = accounts[1].Id, In_Country_Specialist__c = user.Id, Clearance_Destination__c = 'Brazil', Preferred_Supplier__c = TRUE, Destination__c = 'Brazil' );
        insert cpa;

        Currency_Management2__c CM = new Currency_Management2__c (Name = 'US Dollar (USD)', Currency__c = 'US Dollar (USD)', ISO_Code__c = 'USD', Conversion_Rate__c = 1);
        insert CM;

        Country__c country = new Country__c(Brokerage_Costs__c = 618, CIF_Adjustment_Factor__c = 0.035, CIF_Absolute_value_adjustment__c = 200, Clearance_Costs__c = 488,
                                            Country__c = 'Brazil', Name = 'Brazil', Handling_Costs__c = 1180, License_Permit_Costs__c = 0);
        insert country;

        Tax_Structure_Per_Country__c tax = new Tax_Structure_Per_Country__c(Applied_to_Value__c = 'CIF', Country__c = country.Id, Default_Duty_Rate__c = 0.5625,
                                                                            Order_Number__c = '1', Part_Specific__c = TRUE, Name = 'II', Tax_Type__c = 'Duties');
        insert tax;

        CPA_v2_0__c RelatedCPA = new CPA_v2_0__c(Name = 'Brazil', Country_Applied_Costings__c=true );
        insert RelatedCPA;
        CPA_v2_0__c cpav2 = new CPA_v2_0__c(Name = 'Brazil Testing 2.0', Supplier__c = accounts[1].Id, Service_Type__c = 'IOR/EOR', Related_Costing_CPA__c = RelatedCPA.Id, Preferred_Supplier__c = TRUE,
                                            VAT_Rate__c = 0.1, Destination__c = 'Brazil',

                                            Default_Section_1_Address_Line_1__c= 'Address_Line_1',
                                            Default_Section_1_Address_Line_2__c= 'Address_Line_2',
                                            Default_Section_1_Address_Line_3__c= 'Address_Line_3',
                                            Default_Section_1_City__c= 'City',
                                            Default_Section_1_Zip__c= 'Zip',
                                            Default_Section_1_Country__c = 'Country',
                                            Default_Section_1_Other__c= 'Other',
                                            Default_Section_1_Tax_Name__c= 'Tax_Name',
                                            Default_Section_1_Tax_Id__c= 'Tax_Id',
                                            Default_Section_1_Contact_Name__c= 'Contact_Name',
                                            Default_Section_1_Contact_Email__c= 'Contact_Email',
                                            Default_Section_1_Contact_Tel__c= 'Contact_Tel',
                                            Alternate_Section_1_Address_Line_1__c= 'Address_Line_1',
                                            Alternate_Section_1_Address_Line_2__c= 'Address_Line_2',
                                            Alternate_Section_1_Address_Line_3__c= 'Address_Line_3',
                                            Alternate_Section_1_City__c= 'City',
                                            Alternate_Section_1_Zip__c= 'Zip',
                                            Alternate_Section_1_Country__c = 'Country',
                                            Alternate_Section_1_Other__c = 'Other',
                                            Alternate_Section_1_Tax_Name__c = 'Tax_Name',
                                            Alternate_Section_1_Tax_Id__c = 'Tax_Id',
                                            Alternate_Section_1_Contact_Name__c= 'Contact_Name',
                                            Alternate_Section_1_Contact_Email__c= 'Contact_Email',
                                            Alternate_Section_1_Contact_Tel__c= 'Contact_Tel',
                                            Default_Section_2_Address_Line_1__c= 'Address_Line_1',
                                            Default_Section_2_Address_Line_2__c= 'Address_Line_2',
                                            Default_Section_2_Address_Line_3__c= 'Address_Line_3',
                                            Default_Section_2_City__c= 'City',
                                            Default_Section_2_Zip__c= 'Zip',
                                            Default_Section_2_Country__c = 'Country',
                                            Default_Section_2_Other__c= 'Other',
                                            Default_Section_2_Tax_Name__c= 'Tax_Name',
                                            Default_Section_2_Tax_Id__c= 'Tax_Id',
                                            Default_Section_2_Contact_Name__c= 'Contact_Name',
                                            Default_Section_2_Contact_Email__c= 'Contact_Email',
                                            Default_Section_2_Contact_Tel__c= 'Contact_Tel',
                                            Alternate_Section_2_Address_Line_1__c= 'Address_Line_1',
                                            Alternate_Section_2_Address_Line_2__c= 'Address_Line_2',
                                            Alternate_Section_2_Address_Line_3__c= 'Address_Line_3',
                                            Alternate_Section_2_City__c= 'City',
                                            Alternate_Section_2_Zip__c= 'Zip',
                                            Alternate_Section_2_Country__c = 'Country',
                                            Alternate_Section_2_Other__c = 'Other',
                                            Alternate_Section_2_Tax_Name__c = 'Tax_Name',
                                            Alternate_Section_2_Tax_Id__c = 'Tax_Id',
                                            Alternate_Section_2_Contact_Name__c= 'Contact_Name',
                                            Alternate_Section_2_Contact_Email__c= 'Contact_Email',
                                            Alternate_Section_2_Contact_Tel__c= 'Contact_Tel',
                                            Default_Section_3_Address_Line_1__c= 'Address_Line_1',
                                            Default_Section_3_Address_Line_2__c= 'Address_Line_2',
                                            Default_Section_3_Address_Line_3__c= 'Address_Line_3',
                                            Default_Section_3_City__c= 'City',
                                            Default_Section_3_Zip__c= 'Zip',
                                            Default_Section_3_Country__c = 'Country',
                                            Default_Section_3_Other__c= 'Other',
                                            Default_Section_3_Tax_Name__c= 'Tax_Name',
                                            Default_Section_3_Tax_Id__c= 'Tax_Id',
                                            Default_Section_3_Contact_Name__c= 'Contact_Name',
                                            Default_Section_3_Contact_Email__c= 'Contact_Email',
                                            Default_Section_3_Contact_Tel__c= 'Contact_Tel',
                                            Alternate_Section_3_Address_Line_1__c= 'Address_Line_1',
                                            Alternate_Section_3_Address_Line_2__c= 'Address_Line_2',
                                            Alternate_Section_3_Address_Line_3__c= 'Address_Line_3',
                                            Alternate_Section_3_City__c= 'City',
                                            Alternate_Section_3_Zip__c= 'Zip',
                                            Alternate_Section_3_Country__c = 'Country',
                                            Alternate_Section_3_Other__c = 'Other',
                                            Alternate_Section_3_Tax_Name__c = 'Tax_Name',
                                            Alternate_Section_3_Tax_Id__c = 'Tax_Id',
                                            Alternate_Section_3_Contact_Name__c= 'Contact_Name',
                                            Alternate_Section_3_Contact_Email__c= 'Contact_Email',
                                            Alternate_Section_3_Contact_Tel__c= 'Contact_Tel',
                                            Default_Section_4_Address_Line_1__c= 'Address_Line_1',
                                            Default_Section_4_Address_Line_2__c= 'Address_Line_2',
                                            Default_Section_4_Address_Line_3__c= 'Address_Line_3',
                                            Default_Section_4_City__c= 'City',
                                            Default_Section_4_Zip__c= 'Zip',
                                            Default_Section_4_Country__c = 'Country',
                                            Default_Section_4_Other__c= 'Other',
                                            Default_Section_4_Tax_Name__c= 'Tax_Name',
                                            Default_Section_4_Tax_Id__c= 'Tax_Id',
                                            Default_Section_4_Contact_Name__c= 'Contact_Name',
                                            Default_Section_4_Contact_Email__c= 'Contact_Email',
                                            Default_Section_4_Contact_Tel__c= 'Contact_Tel',
                                            Alternate_Section_4_Address_Line_1__c= 'Address_Line_1',
                                            Alternate_Section_4_Address_Line_2__c= 'Address_Line_2',
                                            Alternate_Section_4_Address_Line_3__c= 'Address_Line_3',
                                            Alternate_Section_4_City__c= 'City',
                                            Alternate_Section_4_Zip__c= 'Zip',
                                            Alternate_Section_4_Country__c = 'Country',
                                            Alternate_Section_4_Other__c = 'Other',
                                            Alternate_Section_4_Tax_Name__c = 'Tax_Name',
                                            Alternate_Section_4_Tax_Id__c = 'Tax_Id',
                                            Alternate_Section_4_Contact_Name__c= 'Contact_Name',
                                            Alternate_Section_4_Contact_Email__c= 'Contact_Email',
                                            Alternate_Section_4_Contact_Tel__c= 'Contact_Tel',
                                            VAT_Reclaim_Destination__c = FALSE,
                                            Final_Destination__c ='Brazil',
                                            Country__c = country.id
                                            );
        insert cpav2;

        List<CPARules__c> cpaRules = new List<CPARules__c>();
        cpaRules.add(new CPARules__c(Country__c = 'Brazil',Criteria__c ='Default', Chargable_weight__c = 'NONE', Courier_Responsibility__c = 'NONE',   ECCN_No__c = 'NONE', NL_Product_User__c = 'NONE', Number_of_final_deliveries__c = 'NONE', ShipmentValue__c ='NONE', Sum_of_quantity_of_matched_products__c = 'NONE', Manufacturer__c = 'NONE', DefaultCPA2__c = cpav2.Id, CPA2__c =  cpav2.Id));
        insert cpaRules;

        List<CPA_Costing__c> costs = new List<CPA_Costing__c>();
        costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'Shipment value', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'IOR', Min__c = 375, Max__c = 14000, Conditional_value__c = 'Shipment value', Condition__c = 'Between', Floor__c = 0, Ceiling__c = 10000, Currency__c = 'US Dollar (USD)'));
        costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'Anil', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'IOR', Currency__c = 'US Dollar (USD)',Variable_threshold__c = null));
        insert costs;


        List<Product2> products = new List<Product2>();
        products.add(new Product2(Name = 'CAB-ETH-S-RJ45', US_HTS_Code__c='85444210',description='Anil', ProductCode = 'CAB-ETH-S-RJ45', Manufacturer__c = accounts[2].Id));
        products.add(new Product2(Name = 'CAB-ETH-S-RJ46',US_HTS_Code__c='85444210', ProductCode = 'CAB-ETH-S-RJ46', Manufacturer__c = accounts[2].Id));
        products.add(new Product2(Name = 'CAB-ETH-S', US_HTS_Code__c='85444210',ProductCode = 'CAB-ETH-S', Manufacturer__c = accounts[2].Id));

        insert products;

        Shipment_Order__c shipmentOrder = new Shipment_Order__c(Account__c = accounts[0].id, Shipment_Value_USD__c = 2000, CPA_v2_0__c = cpav2.Id,
                                                                Client_Contact_for_this_Shipment__c = contact.Id, Who_arranges_International_courier__c ='Client', Tax_Treatment__c ='DAP/CIF - IOR pays',
                                                                Ship_to_Country__c = cpa.Id, Destination__c = 'Brazil', Service_Type__c = 'IOR', IOR_Price_List__c = iorpl.Id, Chargeable_Weight__c = 200,
                                                                Shipping_Status__c = 'Cost Estimate Abandoned',Ship_From_Country__c='United States' );  insert shipmentOrder;


        Freight__c FR = new Freight__c(Shipment_Order__c =shipmentOrder.Id, Service_type__c ='Courier');
        insert FR;
        Client_Address__c newAdd1 = new Client_Address__c(
            Province__c = 'XYZ',
            Contact_Full_Name__c= 'Demo',
            Contact_Email__c = 'abc@xyz.com',
            Contact_Phone_Number__c = '9787676',
            CompanyName__c= 'xyz',
            Address__c='add1',
            Address2__c = 'add1',
            City__c='xy',
            Postal_Code__c='1243',
            All_Countries__c='United States',
            Client__c=shipmentOrder.Account__c
            );
        insert newAdd1;
    }


    @isTest
    public static void testAddPickupAndFinalDeliveryAddCtrl() {
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new AddPickupAndFinalDeliveryAddCtrl_Test());

        AddPickupAndFinalDeliveryAddCtrl.getAddressDetailsByPlaceId('12345');
        AddPickupAndFinalDeliveryAddCtrl.getAddressFrom('AbC');
        AddPickupAndFinalDeliveryAddCtrl.getZipCodeByCountry('Angola');
        Test.stopTest();

    }

    @isTest
    public static void testCreateNewAddress() {
        Test.startTest();
        Shipment_order__c shipmentOrder = [SELECT Id FROM Shipment_Order__c limit 1];
        List<Client_Address__c> clientAddList = new List<Client_Address__c>();
        Client_Address__c newAdd = new Client_Address__c(
            Province__c = 'XYZ',
            Contact_Full_Name__c= 'Demo',
            Contact_Email__c = 'abc@xyz.com',
            Contact_Phone_Number__c = '9787676',
            CompanyName__c= 'xyz',
            Address__c='add1',
            Address2__c = 'add1',
            City__c='xy',
            Postal_Code__c='1243',
            All_Countries__c='United States'
            );
        clientAddList.add(newAdd);
        AddPickupAndFinalDeliveryAddCtrl.createNewAddress(clientAddList,shipmentOrder.Id,True);
        Test.stopTest();
    }

    @isTest
    public static void testUpdateFreight() {
        Test.startTest();
        Shipment_order__c shipmentOrder = [SELECT Id FROM Shipment_Order__c limit 1];
        Client_Address__c cAdd = [SELECT Id FROM Client_Address__c limit 1];
        AddPickupAndFinalDeliveryAddCtrl.updateFreight(new List<Id> {cAdd.Id},shipmentOrder.Id,false);
        Test.stopTest();
    }

    @isTest
    public static void testGetsfRecords() {
        Test.startTest();
        Shipment_order__c shipmentOrder = [SELECT Id FROM Shipment_Order__c limit 1];
        AddPickupAndFinalDeliveryAddCtrl.getsfRecords(shipmentOrder.Id,true);
        AddPickupAndFinalDeliveryAddCtrl.getsfRecords(shipmentOrder.Id,false);
        Test.stopTest();
    }
    
    @isTest
    public static void testgetPicklist() {
        Test.startTest();
        AddPickupAndFinalDeliveryAddCtrl.getPicklistValue('Client_Address__c','Pickup_Preference__c');
        Test.stopTest();
    }
   
}