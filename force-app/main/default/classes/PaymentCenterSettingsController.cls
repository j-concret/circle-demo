public with sharing class PaymentCenterSettingsController {
  public fw1__Payment_Center_Setting__c settings {
    get {
      return [
          SELECT EU_Bank_Details__c, Notes__c, US_Bank_Details__c, Euro_Conversion_Rate__c,
          fw1__Default_Email_Message__c, fw1__Email_Footer__c, fw1__Email_Currency_Symbol__c            
          FROM fw1__Payment_Center_Setting__c
          WHERE Name = 'Default Settings'
          LIMIT 1]; 
    }
    set;
  }
  
  public static testmethod void testPaymentCenterSettings(){
    Test.startTest();   
    fw1__Payment_Center_Setting__c defaultSetting = new fw1__Payment_Center_Setting__c();
    defaultSetting.Name = 'Default Settings';
    insert defaultSetting;
    
    PaymentCenterSettingsController s = new PaymentCenterSettingsController();
    fw1__Payment_Center_Setting__c s1 = s.settings;
    Test.stopTest();
  }
}