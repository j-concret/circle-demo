@RestResource(urlMapping='/NCPUpdateCase/*')
Global class NCPUpdateCase {
    
      @Httppost
      global static void NCPUpdateCase(){
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        Blob body = req.requestBody;
    	res.addHeader('Content-Type', 'application/json');
        String requestString = body.toString();
        NCPUpdateCaseWra rw  = (NCPUpdateCaseWra)JSON.deserialize(requestString,NCPUpdateCaseWra.class);
          Try{
              
              Access_token__c At =[select status__c,Access_Token__c from Access_token__c where Access_Token__c=:rw.Accesstoken ];
            	if( at.status__C =='Active' ){
                           
                    if(rw.OwnerID !=null){
                     Case cs = [Select ID,OwnerID from case where Id=:rw.CaseID limit 1  ];
                    // cs.Last_Client_viewed_time__c =datetime.now();
                      cs.OwnerId=rw.OwnerID;
                     Update cs;
                    }
                    
                    if(!rw.NotifyContacts.isempty()){
                     List<EntitySubscription> entitySubToBeInsert = new List<EntitySubscription>();
                        For(Integer i=0;rw.NotifyContacts.size()>i;i++){
                        	EntitySubscription follow = new EntitySubscription(parentId = rw.CaseID, subscriberid =rw.NotifyContacts[i].NotifyContactID,networkId ='0DB1v000000kB98GAE'); //  acc2-0DB1q00000000ozGAA,Prod - 0DB1v000000kB98GAE
                			entitySubToBeInsert.add(follow);
                        }
                        
                        Insert entitySubToBeInsert;
                    }
                
                 	case cs1 =[Select Id,CaseNumber from case where id=:rw.CaseID];
                  	JSONGenerator gen = JSON.createGenerator(true);
            		gen.writeStartObject();
            
             		gen.writeFieldName('Success');
             		gen.writeStartObject();
            
             			if(rw.CaseID == null) {gen.writeNullField('Response');} else{gen.writeStringField('Response','Case updated successfully. Casse Reference number is '+cs1.CaseNumber);}
            
            		gen.writeEndObject();
            		gen.writeEndObject();
            		String jsonData = gen.getAsString();
            		res.responseBody = Blob.valueOf(jsonData);
            		res.addHeader('Content-Type', 'application/json');
            		res.statusCode = 200;    
                    API_Log__c Al = New API_Log__c();
                    Al.EndpointURLName__c='NCPCreateCase';
                    Al.StatusCode__c=string.valueof(res.statusCode);
                    Al.Response__c = 'Created case' +rw.CaseID;
                    Insert Al;
                
                  
                  
                  
              }        
          }
          Catch(Exception e){
              
              String ErrorString ='You are trying to add duplicate participants or Something went wrong, please contact Sfsupport@tecex.com';
               		res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
            		res.addHeader('Content-Type', 'application/json');
        			res.statusCode = 400;
                
                API_Log__c Al = New API_Log__c();
                Al.Account__c=rw.AccountID;
                Al.Login_Contact__c=rw.contactId;
                Al.EndpointURLName__c='NCPUpdateCase';
                Al.Response__c=e.getMessage()+' at line number: '+e.getLineNumber();
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al;
                
              
              
          }



      }

}