@IsTest
global class CSICommunityLoginAPI_Test {

    static testMethod void login(){
         // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
                
        RestRequest req1 = new RestRequest();
        RestResponse res1 = new RestResponse();
        req1.requestURI = '/services/apexrest/PortalLogin'; 
        req1.httpMethod = 'POST';
        req1.addParameter('username','Test');
        req1.addParameter('password','Test');
        req1.addParameter('domain','login');
        RestContext.request = req1;
        RestContext.response = res1;
        RestContext.request.addHeader('Content-Type', 'application/json');
        Test.startTest();
        CSICommunityLoginAPI.LoginResponse response  = CSICommunityLoginAPI.login();
        Integer code = response.statusCode;
        System.assertEquals(200, code);
        Test.stopTest();
    }
    
    
global class MockHttpResponseGenerator implements HttpCalloutMock {
    
    global HTTPResponse respond(HTTPRequest req) {
        System.assertEquals('https://login.salesforce.com/services/Soap/u/22.0', req.getEndpoint());
        System.assertEquals('POST', req.getMethod());
        
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody('{"example":"test"}');
        res.setStatusCode(200);
        return res;
    }
}
}