public class CalculateCosts {

    @InvocableMethod
    public static void createSOCosts(List<Id> SOIds){
        Shipment_Order__c SO = [SELECT Id, Name,  Insurance_Cost_CIF__c, Shipment_Value_USD__c, IOR_Fee_new__c, FC_IOR_and_Import_Compliance_Fee_USD__c, FC_Total_Customs_Brokerage_Cost__c,FC_Total_Handling_Costs__c, SupplierlU__c, FC_Total_License_Cost__c, FC_Total_Clearance_Costs__c, FC_Insurance_Fee_USD__c, FC_Miscellaneous_Fee__c, On_Charge_Mark_up__c, FC_International_Delivery_Fee__c, TecEx_Shipping_Fee_Markup__c, CPA_v2_0__c, Chargeable_Weight__c, Account__c, Service_Type__c, Total_Taxes__c, of_Line_Items__c, FC_Total__c, of_Unique_Line_Items__c, CPA_Costings_Added__c, of_packages__c, Final_Deliveries_New__c, IOR_Refund_of_Tax_Duty__c FROM Shipment_Order__c where Id IN :SOIds LIMIT 1];

        List<CPA_v2_0__c> CAP = [SELECT Related_Costing_CPA__c, VAT_Rate__c, Supplier__c FROM CPA_v2_0__c WHERE Id =: SO.CPA_v2_0__c LIMIT 1];

        List<CPA_Costing__c > relatedCostings_IOR_EOR = [SELECT Name,Shipment_Order_Old__c,Invoice__c,isManualCosting__c,Inactive__c, Amount__c, Applied_to__c, Ceiling__c, Conditional_value__c, Condition__c, Floor__c,Cost_Category__c, Cost_Type__c, Max__c, Min__c, Rate__c, CostStatus__c, Updating__c, RecordTypeID, IOR_EOR__c, VAT_applicable__c, Threshold_Percentage__c, Within_Threshold__c, Variable_threshold__c, VAT_Rate__c, CPA_v2_0__r.VAT_Rate__c, Currency__c, Additional_Percent__c,Supplier__c, Cost_Note__c FROM CPA_Costing__c WHERE CPA_v2_0__c =: CAP[0].Id AND IOR_EOR__c =: SO.Service_Type__c];

        List<String> currencies = new List<String>();
        for(CPA_Costing__c cost : relatedCostings_IOR_EOR){
          currencies.add(cost.Currency__c);
        }
        Map<String,Currency_Management2__c> costingCurrency = new Map<String,Currency_Management2__c>();
        for(Currency_Management2__c curr : [SELECT Name, Conversion_Rate__c, ISO_Code__c, Currency__c FROM Currency_Management2__c WHERE Currency__c IN: currencies]){
          costingCurrency.put(curr.Currency__c,curr);
        }

        List<CPA_Costing__c> deleteCostings = new List<CPA_Costing__c>();
        Map<String,CPA_Costing__c> costingNameIdMap = new Map<String,CPA_Costing__c>();

        for(CPA_Costing__c relatedCost : [SELECT Id, Name, isManualCosting__c,Invoice__c, Supplier__c, DeleteAll__c, Shipment_Order_Old__c,Inactive__c, IOR_EOR__c,Amount__c, Applied_to__c, Ceiling__c, Conditional_value__c, Condition__c,Floor__c,Cost_Category__c, Cost_Type__c, Max__c, Min__c, Rate__c, CostStatus__c, Updating__c, RecordTypeID, VAT_Rate__c, VAT_applicable__c,Variable_threshold__c,Threshold_Percentage__c, Within_Threshold__c, Currency__c, Supplier_Invoice__c, Amended__c, Cost_Note__c FROM CPA_Costing__c WHERE Shipment_Order_Old__c =: SO.Id ]) {
            costingNameIdMap.put(relatedCost.Name,relatedCost);
        }

        List<CPA_Costing__c> addedCostings = new List<CPA_Costing__c>();
        MAP<String,CPA_Costing__c> finalUpdateCostingMap = new MAP<String,CPA_Costing__c>();
        MAP<Id,CPA_Costing__c> deleteCostingWithNegAmount = new MAP<Id,CPA_Costing__c>();
        Id recordTypeId = Schema.SObjectType.CPA_Costing__c.getRecordTypeInfosByName().get('Transaction').getRecordTypeId();
        List<CPA_Costing__c> applicableCostings = new List<CPA_Costing__c>();

        if(!relatedCostings_IOR_EOR.isEmpty()) {
            List<CPA_Costing__c>  culledCostings = SOTriggerHelper.excludeAllCosts(so, relatedCostings_IOR_EOR);
            List<CPA_Costing__c>  SOCPAcostingsIOR = culledCostings.deepClone();

            for(CPA_Costing__c cost : SOCPAcostingsIOR) {
                if(costingNameIdMap.containsKey(cost.Name)) {
                    if(costingNameIdMap.get(cost.Name).Amended__c) continue;
                    cost.Inactive__c  = false;
                    cost.Id = costingNameIdMap.get(cost.Name).Id;
                    cost.Within_Threshold__c = costingNameIdMap.get(cost.Name).Within_Threshold__c;
                    //cost.Amount__c = cost.Amount__c;//costingNameIdMap.get(cost.Name).VAT_applicable__c ? cost.Amount__c : costingNameIdMap.get(cost.Name).Amount__c;
                }
                cost.Shipment_Order_Old__c = SO.ID;
                cost.CPA_v2_0__c = CAP[0].Related_Costing_CPA__c;
                cost.recordTypeID = recordTypeId;
                cost.VAT_Rate__c =  !cost.VAT_applicable__c ? 0 : CAP[0].VAT_Rate__c == null ? 0 : CAP[0].VAT_Rate__c;
                cost.VAT_applicable__c =  cost.VAT_applicable__c;
                cost.Additional_Percent__c =  cost.Additional_Percent__c == null ? 0 : cost.Additional_Percent__c;
                cost.Variable_threshold__c =  cost.Variable_threshold__c == null ? 0 : cost.Variable_threshold__c;
                cost.Threshold_Percentage__c = cost.Threshold_Percentage__c;
                cost.Within_Threshold__c = null;
                cost.Cost_Note__c =  cost.Cost_Note__c;
                cost.Updating__c = !cost.Updating__c;
                if(cost.Invoice__c == null) {
                    cost.Supplier__c = CAP[0].Supplier__c;
                    cost.Ceiling__c = cost.Ceiling__c;
                    cost.Floor__c = cost.Floor__c;
                    cost.Condition__c = cost.Condition__c;
                    cost.Conditional_value__c = cost.Conditional_value__c;
                    cost.Amount__c = cost.Amount__c;
                }
                SOTriggerHelper.calculateSOCostingAmount(so, cost, costingCurrency.get(cost.Currency__c));
                if(cost.Inactive__c) {
                    if(cost.Id != null)
                        deleteCostingWithNegAmount.put(cost.Id,cost);
                }else{
                    addedCostings.add(cost);
                }
            }

            Map<String,CPA_Costing__c> costingNameIdMapReverse = new Map<String,CPA_Costing__c>();
            for(CPA_Costing__c cost : addedCostings) {
                costingNameIdMapReverse.put(cost.Name,cost);
            }
            for(CPA_Costing__c cost : costingNameIdMap.values()) {
                if(!costingNameIdMapReverse.containsKey(cost.Name)) {
                    if(!cost.isManualCosting__c) {
                        if(cost.Invoice__c == null && cost.Amended__c == FALSE) {
                            cost.Inactive__c = true;
                            if(cost.Inactive__c) {
                                if(cost.Id != null) {
                                    deleteCostingWithNegAmount.put(cost.Id,cost);
                                }
                            }
                        }
                        // addedCostings.add(cost);
                    }
                }
            }
        }
        if(!addedCostings.isEmpty()) {
            for(Integer i=0; i<addedCostings.size(); i++) {
                if(i==(addedCostings.size()-1)) {
                    addedCostings[i].Trigger__c = true;
                } else {
                    addedCostings[i].Trigger__c = false;
                }
                String key = addedCostings[i].Id == null ? 'Id'+i : addedCostings[i].Id;
                if(!finalUpdateCostingMap.containsKey(key))
                    finalUpdateCostingMap.put(key,addedCostings[i]);

            }
            upsert finalUpdateCostingMap.values();
            // Added "UpdateTaxCosting.updateCosting(SOIds)" as we were having issue like Tax and duties recharge was
            // not getting updated when we create a new Quote, it was only showing default value which is 1.
            UpdateTaxCosting.updateCosting(SOIds);
        }
        if(!deleteCostingWithNegAmount.isEmpty()) {
            delete deleteCostingWithNegAmount.values();
        }
    }
}