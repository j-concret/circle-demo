public class CPCreateFinalDeliveryWrapper {
    
    
    
    public class Final_Delivery {
    public String AddressID;
    public String SOID;
	public String Name;
	public String ContactName;
	public String ContactEmail;
	public String ContactNumber;
	public String ContactAddreaaLine1;
	public String ContactAddreaaLine2;
	public String City;
	public String ZIP;
	public String Country;
    }
    
    public List<Final_Delivery> Final_Delivery;
	
	public static CPCreateFinalDeliveryWrapper parse(String json) {
		return (CPCreateFinalDeliveryWrapper) System.JSON.deserialize(json, CPCreateFinalDeliveryWrapper.class);
	}


}