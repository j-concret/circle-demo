public class Lex_SupplierInvoiceDataTable {
    
    
     //1. Returns related list of Costing
    @AuraEnabled
   
         public static List<CPA_Costing__c> getCostings(ID SupplierInvoiceID1){
         
           
             
          Supplier_Invoice__c SI = [select ID, name,Shipment_Order_Name__c,Freight_Request__c from Supplier_Invoice__c where id =: SupplierInvoiceID1 limit 1];
             
             String SOId1 = SI.Shipment_Order_Name__c;
             String Freight1 = SI.Freight_Request__c;
             
             String RelatedSOid1 = SI.Shipment_Order_Name__c;
             String RelatedFreight1 = SI.Freight_Request__c;
             
                        
             
          
             
             If(SOId1 != null && Freight1 == null){
                         
                return [SELECT Additional_Percent__c, Amended__c, Amount__c, Amount_USD__c, Applied_to__c, Ceiling__c, Condition__c, Conditional_value__c, Cost_Category__c, Cost_Note__c, Cost_Type__c, CostStatus__c, Name, CPA_v2_0__c, CreatedById, Currency__c, Exchange_rate_forecast__c, Exchange_rate_payment_date__c, Floor__c, Amount_in_USD__c, Freight_Request__c, Invoice_amount_local_currency__c, Invoice_amount_USD_New__c, Invoice_currency__c, IOR_EOR__c, LastModifiedById, Max__c, Min__c, Rate__c, RecordTypeId, Shipment_Order_Old__c, Status_Colour__c, Supplier_Invoice__c, Updating__c, Variable_threshold__c, VAT_applicable__c, VAT_Rate__c, VAT_Rate_Formula__c 
                FROM CPA_Costing__c where Shipment_Order_Old__c =: SOId1 and Supplier_Invoice__c =: SupplierInvoiceID1  LIMIT 200];
  
             }
             else if(Freight1 != null && SOId1 == null){
                         
                return [SELECT Additional_Percent__c, Amended__c, Amount__c, Amount_USD__c, Applied_to__c, Ceiling__c, Condition__c, Conditional_value__c, Cost_Category__c, Cost_Note__c, Cost_Type__c, CostStatus__c, Name, CPA_v2_0__c, CreatedById, Currency__c, Exchange_rate_forecast__c, Exchange_rate_payment_date__c, Floor__c, Amount_in_USD__c, Freight_Request__c, Invoice_amount_local_currency__c, Invoice_amount_USD_New__c, Invoice_currency__c, IOR_EOR__c, LastModifiedById, Max__c, Min__c, Rate__c, RecordTypeId, Shipment_Order_Old__c, Status_Colour__c, Supplier_Invoice__c, Updating__c, Variable_threshold__c, VAT_applicable__c, VAT_Rate__c, VAT_Rate_Formula__c 
                FROM CPA_Costing__c where Freight_Request__c =: Freight1 and Supplier_Invoice__c =: SupplierInvoiceID1 LIMIT 200];
  
             }
             else{
                return [SELECT Additional_Percent__c, Amended__c, Amount__c, Amount_USD__c, Applied_to__c, Ceiling__c, Condition__c, Conditional_value__c, Cost_Category__c, Cost_Note__c, Cost_Type__c, CostStatus__c, Name, CPA_v2_0__c, CreatedById, Currency__c, Exchange_rate_forecast__c, Exchange_rate_payment_date__c, Floor__c, Amount_in_USD__c, Freight_Request__c, Invoice_amount_local_currency__c, Invoice_amount_USD_New__c, Invoice_currency__c, IOR_EOR__c, LastModifiedById, Max__c, Min__c, Rate__c, RecordTypeId, Shipment_Order_Old__c, Status_Colour__c, Supplier_Invoice__c, Updating__c, Variable_threshold__c, VAT_applicable__c, VAT_Rate__c, VAT_Rate_Formula__c 
                FROM CPA_Costing__c where (Shipment_Order_Old__c =: RelatedSOid1 or Freight_Request__c =: RelatedFreight1) and Supplier_Invoice__c =: SupplierInvoiceID1 LIMIT 200];                 
             }
             
                      
             
             
             
             
             //  return [SELECT Additional_Percent__c, Amended__c, Amount__c, Amount_USD__c, Applied_to__c, Ceiling__c, Condition__c, Conditional_value__c, Cost_Category__c, Cost_Note__c, Cost_Type__c, CostStatus__c, Name, CPA_v2_0__c, CreatedById, Currency__c, Exchange_rate_forecast__c, Exchange_rate_payment_date__c, Floor__c, Amount_in_USD__c, Freight_Request__c, Invoice_amount_local_currency__c, Invoice_amount_USD_New__c, Invoice_currency__c, IOR_EOR__c, LastModifiedById, Max__c, Min__c, Rate__c, RecordTypeId, Shipment_Order_Old__c, Status_Colour__c, Supplier_Invoice__c, Updating__c, Variable_threshold__c, VAT_applicable__c, VAT_Rate__c, VAT_Rate_Formula__c 
   // FROM CPA_Costing__c where Shipment_Order_Old__c =: SOId1]; && SOId1 <> null) or ( Freight_Request__c =: Freight1 && Freight1 <> null) and (Shipment_Order_Old__c =: RelatedSOid1 and Freight_Request__c =: RelatedFreight1)
             
         }
    
    //1.1 updating related list of Costings
    @AuraEnabled
    public static void updateCostings(List<CPA_Costing__c> editedAccountList){
        try{
            system.debug('editedAccountList - Before'+editedAccountList);
            update editedAccountList;
            system.debug('editedAccountList - After'+editedAccountList);
            
        } catch(Exception e){
           
        }
    }
    
    
    
   //2. Insert Costings from supplierInvoice - Costings Flow
     
    @AuraEnabled
    public static void saveCostings(List<CPA_Costing__c> accList, ID SupplierInvoiceID1, ID SeleSOID ){
        
        list<CPA_Costing__c> CCO = new list<CPA_Costing__c> ();
        
        CPA_Costing__c CCO1 = [Select Additional_Percent__c, Amended__c, Amount__c, Amount_USD__c, Applied_to__c, Ceiling__c, Condition__c, Conditional_value__c, Cost_Category__c, Cost_Note__c, Cost_Type__c, CostStatus__c, Name, CPA_v2_0__c, CreatedById, Currency__c, Exchange_rate_forecast__c, Exchange_rate_payment_date__c, Floor__c, Amount_in_USD__c, Freight_Request__c, Invoice_amount_local_currency__c, Invoice_amount_USD_New__c, Invoice_currency__c, IOR_EOR__c, LastModifiedById, Max__c, Min__c, Rate__c, RecordTypeId, Shipment_Order_Old__c, Status_Colour__c, Supplier_Invoice__c, Updating__c, Variable_threshold__c, VAT_applicable__c, VAT_Rate__c, VAT_Rate_Formula__c 
 from CPA_Costing__c where Shipment_Order_Old__c =: SeleSOID limit 1];
        
        System.debug('CCO1'+CCO1);
        
        For(integer i=0; i<accList.size(); i++){
            
            accList[i].CPA_v2_0__c = CCO1.CPA_v2_0__c;
            accList[i].IOR_EOR__c = CCO1.IOR_EOR__c;
            accList[i].Cost_Type__c = 'Fixed';
          //  accList[i].Currency__c = CCO1.Currency__c;
            accList[i].Invoice_currency__c= CCO1.Invoice_currency__c;
            accList[i].Shipment_Order_Old__c = CCO1.Shipment_Order_Old__c;
            accList[i].Freight_Request__c = CCO1.Freight_Request__c;
            accList[i].Supplier_Invoice__c = SupplierInvoiceID1;
            accList[i].Exchange_rate_forecast__c = CCO1.Exchange_rate_forecast__c;
            accList[i].Exchange_rate_payment_date__c = CCO1.Exchange_rate_forecast__c;
            accList[i].RecordTypeId = cco1.RecordTypeId;
            CCO.add(accList[i]);
            
        }
        
        
        Insert CCO;
       // Insert accList;
    
}
    
    
      //3. Return supplier invoice info to summary page
    @AuraEnabled
   
         public static List<Supplier_Invoice__c> getSupplierInvoice(ID SupplierInvoiceID1){
         
           
             
          return [select Total_Invoice_Amount_USD__c,Invoice_amount_local_currency__c,Actual_Admin_Fee__c,Actual_Bank_Fees__c,Actual_Brokerage_Costs__c,Actual_Clearance_Costs__c,Actual_Duties_and_Taxes__c,Actual_Finance_Fee__c,Actual_Handling_Costs__c,Actual_Insurance_Fee_USD__c,Actual_International_Delivery_Fee__c,Actual_IOR_EOR_Cost__c,Actual_License_Permits_Cost__c,Actual_Miscellaneous_Fee__c,Approval_Status__c,Bank_Account__c,Bank_Reference__c,Beta_Total_amount_paid__c,BT_Amount_Outstanding__c,CasesafeId__c,Change_override__c,Chargeable_Weight__c,CreatedById,Created_By_Profile_Name__c,Date_Paid__c,Due_date_1__c,FC_Admin_Fee__c,FC_Bank_Fees__c,FC_Finance_Fee__c,FC_Insurance_Fee_USD__c,FC_International_Delivery_Fee__c,FC_Miscellaneous_Fee__c,Foreign_Exchange_Gain_Loss__c,Foreign_Exchange_gain_loss_on_payment__c,Forex_Rate_Used_Euro_Dollar__c,Freight_Request__c,Invoice_Amount_Due__c,Amount_Paid__c,Invoice_Currency__c,Due_date__c,Name,Invoice_Paid_vs_Total_USD__c,IOR_Cost__c,LastModifiedById,On_charges_costs__c,Broker_Cost__c,OwnerIdOld__c,Payment_Status__c,Rate_adjustment__c,RecordTypeId,Reviewed_by__c,sfoldid__c,Shipment_Order_Name__c,Spot_Rate__c,Supplier_Name__c,Tax_Cost__c,Total_Applied_to_other_invoices__c,Total_Brokerage_Costs__c,Total_Cash_Applied__c,Total_Clearance_Costs__c,Total_Credits_Applied__c,Total_Customs_Brokerage__c,Total_Customs_Clearance__c,Total_Customs_Handling__c,Total_Duties_and_Taxes__c,Total_Handling_Costs__c,Total_Insurance__c,Total_Int_l_Freight__c,Total_Invoice_Amount_Invoice_Currency__c,Total_Invoice_Amount__c,Total_Invoice_Amount_Excl_Forex__c,Total_IOR_Cost__c,Total_IOR_EOR__c,Total_License_Inspection__c,Total_License_Permits_Cost__c,Total_Local_Delivery__c,Total_Tax_and_Duties__c,Total_Value_Applied__c,Total_value_per_Invoice__c,USD_converted_at_spot__c
 from Supplier_Invoice__c where id =: SupplierInvoiceID1 limit 1];
            
         }
    
    

}