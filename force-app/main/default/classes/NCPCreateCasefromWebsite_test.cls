@isTest
public class NCPCreateCasefromWebsite_test {

     static testMethod void  NCPCreateCasefromWebsite(){
           
        RestRequest req1 = new RestRequest();
        RestResponse res1 = new RestResponse();
        req1.requestURI = '/services/apexrest/casecreation/*';
          req1.params.put('Name','2digNA');
          req1.params.put('Surname','2digNA');
          req1.params.put('Email','as@as.com');
          req1.params.put('Title','2digNA');
          req1.params.put('Role','Sales');
          req1.httpMethod = 'POST';
        RestContext.request = req1;
        RestContext.response = res1;
        RestContext.request.addHeader('Content-Type', 'application/json');
        
       
        Test.startTest(); 
        	NCPCreateCasefromWebsite.NCPCreateCasefromWebsite();
        Test.stopTest();
         }
}