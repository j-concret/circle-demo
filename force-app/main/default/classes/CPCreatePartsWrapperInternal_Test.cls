@Istest
public class CPCreatePartsWrapperInternal_Test {
 static testMethod void testParse() {
		String json = '{'+
		'\"Parts\": ['+
		'    {'+
		'      '+
        '      \"SOID\": \"a0R1l0000022JeM\",'+
        '      \"ClientID\": \"001D000001kdDDLIA2\",'+
        '      \"ContactID\": \"0031v00001hY02H\",'+
		'      \"PartNumber\": \"Test\",'+
		'      \"PartDescription\": \"Test\",'+
		'      \"Quantity\": 1,'+
		'      \"UnitPrice\": 1.00,'+
		'      \"HSCode\": \"8456879658\",'+
		'      \"CountryOfOrigin\": \"USA\",'+
		'      \"ECCNNo\": \"1234\"'+
		'     '+
		'    },'+
		'    {'+
		'      '+
        '      \"SOID\": \"a0R1l0000022JeM\",'+
        '      \"ClientID\": \"001D000001kdDDLIA2\",'+
        '      \"ContactID\": \"0031v00001hY02H\",'+
		'      \"PartNumber\": \"Test\",'+
		'      \"PartDescription\": \"Test\",'+
		'      \"Quantity\": 1,'+
		'      \"UnitPrice\": 1.00,'+
		'      \"HSCode\": \"8456879658\",'+
		'      \"CountryOfOrigin\": \"USA\",'+
		'      \"ECCNNo\": \"1234\"'+
		'     '+
		'    },'+
		'    {'+
		'      '+
        '      \"SOID\": \"a0R1l0000022JeM\",'+
        '      \"ClientID\": \"001D000001kdDDLIA2\",'+
        '      \"ContactID\": \"0031v00001hY02H\",'+
		'      \"PartNumber\": \"Test\",'+
		'      \"PartDescription\": \"Test\",'+
		'      \"Quantity\": 1,'+
		'      \"UnitPrice\": 1.00,'+
		'      \"HSCode\": \"8456879658\",'+
		'      \"CountryOfOrigin\": \"USA\",'+
		'      \"ECCNNo\": \"1234\"'+
		'     '+
		'    }'+
		''+
		'  ]'+
		'}';
		CPCreatePartsWrapperInternal obj = CPCreatePartsWrapperInternal.parse(json);
		System.assert(obj != null);
	}
    
    
}