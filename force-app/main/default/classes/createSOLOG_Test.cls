@IsTest
public class createSOLOG_Test {
    
    static testMethod void testCreateSOLogWith2DigitCountryCode(){
        
        
        CountryandRegionMap__c crm = new CountryandRegionMap__c();
        crm.Name = 'Brazil';
        crm.Country__c = 'Brazil';
        crm.European_Union__c = 'No';
        crm.Region__c = 'South America';
        
        insert crm;
        
        CountryandRegionMap__c crm1 = new CountryandRegionMap__c();
        crm1.Name = 'United States';
        crm1.Country__c = 'United States';
        crm1.European_Union__c = 'No';
        crm1.Region__c = 'Oceania';
        
        insert crm1;
        
        DefaultFreightValues__c df = new DefaultFreightValues__c();
        df.Name = 'Test';
        df.Courier_Base_Rate_KG__c = 12;
        df.Courier_Dangerous_Goods_premium__c = 12;
        df.Courier_Fixed_Charge__c = 12;
        df.Courier_Non_stackable_premium__c = 12;
        df.Courier_Oversized_premium__c = 12;
        df.FF_Base_Rate_KG__c = 12;
        df.FF_Dangerous_Goods_Fixed_Charge_Premium__c = 12;
        df.FF_Dangerous_Goods_premium__c = 12;
        df.FF_Dedicated_Pickup__c = 12;
        df.FF_Fixed_Charge__c = 12;
        df.FF_Non_stackable_Fixed_Charge_Premium__c = 12;
        df.FF_Non_stackable_premium__c = 12;
        df.FF_Oversized_Fixed_Charge_Premium__c = 12;
        df.FF_Oversized_premium__c = 12;
        insert df;
        
        RegionalFreightValues__c rfm = new RegionalFreightValues__c();
        rfm.Name = 'Oceania to South America';
        rfm.Courier_Discount_Premium__c = 12;
        rfm.Courier_Fixed_Premium__c = 10;
        rfm.Destination__c = 'Brazil';
        rfm.FF_Fixed_Premium__c = 123;
        rfm.FF_Regional_Discount_Premium__c = 12;
        rfm.Origin__c = 'Australia';
        rfm.Preferred_Courier_Id__c = 'test';
        rfm.Preferred_Courier_Name__c = 'test';
        rfm.Preferred_Freight_Forwarder_Id__c = 'test';
        rfm.Preferred_Freight_Forwarder_Name__c = 'test';
        rfm.Risk_Adjustment_Factor__c = 1;
        insert rfm;
        List<CountryofOriginMap__c> twoDigitCodeList = new List<CountryofOriginMap__c>();
        for(Integer i=0;i<=1;i++){
            CountryofOriginMap__c comSetting = new CountryofOriginMap__c();
            comSetting.Country__c = 'California';
            comSetting.Name = 'California'+i;
            comSetting.Currency_ISO_Code__c = 'USD';
            comSetting.Three_Digit_Country_Code__c = '';
            comSetting.Two_Digit_Country_Code__c = 'CA';
            twoDigitCodeList.add(comSetting);
        }
        
        insert twoDigitCodeList;
        
        Tracking_rules__c trackRulesObj = new Tracking_rules__c();
        trackRulesObj.Name = 'TestTracking Rule';
        trackRulesObj.Tracking_term__c = 'NONE';
        trackRulesObj.Checkpoint_Message_Contains__c = 'NONE';
        trackRulesObj.Checkpoint_Location_country_contains__c = 'NONE';
        trackRulesObj.Current_Status__c = 'NONE';
        trackRulesObj.Mapped_Status__c = 'In-Transit';
        trackRulesObj.Description__c = '';
        trackRulesObj.Type_of_Update__c = 'NONE';
        trackRulesObj.Eligible_Carriers__c = 'FedEx';
        insert trackRulesObj;
        
        
        Account account2 = new Account (name='TecEx Prospective Client', Type ='Supplier', ICE__c = '0050Y000001km5c', RecordTypeId = '0120Y0000006KjQQAU',Insurance_Fee__c=10);
        insert account2;
        
        Contact contact = new Contact ( lastname ='Testing Individual',  email = 'anilk@tecex.com',UserName__c = 'anilk@tecex.com', Password__c = 'Test', contact_Status__C = 'Active', AccountId = account2.Id);
        insert contact;
        
        Id shipmentOrderRecTypeId = Schema.SObjectType.shipment_order__c.getRecordTypeInfosByName().get('Cost Estimate').getRecordTypeId();
        
        CpaRules__c cpaRule = new CpaRules__c();
        cpaRule.Country__c = 'Brazil';
        cpaRule.Criteria__c = 'Default';
        
        Insert cpaRule;
        
        Country__c country = new Country__c(Brokerage_Costs__c = 618, CIF_Adjustment_Factor__c = 0.035, CIF_Absolute_value_adjustment__c = 200, Clearance_Costs__c = 488,
                                            Country__c = 'Brazil', Name = 'Brazil', Handling_Costs__c = 1180, License_Permit_Costs__c = 0);
        insert country;
        
        CPA_v2_0__c RelatedCostingCPA = new CPA_v2_0__c(Name = 'Brazil',Country_Applied_Costings__c =TRUE);
        Insert RelatedCostingCPA;
        
        CPA_v2_0__c cpav2 = new CPA_v2_0__c(Name = 'Brazil Testing 2.0', Supplier__c = account2.Id, Service_Type__c = 'IOR/EOR', Related_Costing_CPA__c = RelatedCostingCPA.Id, Preferred_Supplier__c = TRUE,
                                            VAT_Rate__c = 0.1, Destination__c = 'Brazil',
                                            Default_Section_1_Address_Line_1__c= 'Address_Line_1' ,
                                            Default_Section_1_Address_Line_2__c= 'Address_Line_2' ,
                                            Default_Section_1_Address_Line_3__c= 'Address_Line_3' ,
                                            Default_Section_1_City__c= 'City' ,
                                            Default_Section_1_Zip__c= 'Zip' ,
                                            Default_Section_1_Country__c = 'Country' ,
                                            Default_Section_1_Other__c= 'Other' ,
                                            Default_Section_1_Tax_Name__c= 'Tax_Name' ,
                                            Default_Section_1_Tax_Id__c= 'Tax_Id' ,
                                            Default_Section_1_Contact_Name__c= 'Contact_Name' ,
                                            Default_Section_1_Contact_Email__c= 'Contact_Email' ,
                                            Default_Section_1_Contact_Tel__c= 'Contact_Tel' ,
                                            Alternate_Section_1_Address_Line_1__c= 'Address_Line_1' ,
                                            Alternate_Section_1_Address_Line_2__c= 'Address_Line_2' ,
                                            Alternate_Section_1_Address_Line_3__c= 'Address_Line_3' ,
                                            Alternate_Section_1_City__c= 'City' ,
                                            Alternate_Section_1_Zip__c= 'Zip' ,
                                            Alternate_Section_1_Country__c = 'Country' ,
                                            Alternate_Section_1_Other__c = 'Other' ,
                                            Alternate_Section_1_Tax_Name__c = 'Tax_Name' ,
                                            Alternate_Section_1_Tax_Id__c = 'Tax_Id' ,
                                            Alternate_Section_1_Contact_Name__c= 'Contact_Name' ,
                                            Alternate_Section_1_Contact_Email__c= 'Contact_Email' ,
                                            Alternate_Section_1_Contact_Tel__c= 'Contact_Tel',
                                            Default_Section_2_Address_Line_1__c= 'Address_Line_1' ,
                                            Default_Section_2_Address_Line_2__c= 'Address_Line_2' ,
                                            Default_Section_2_Address_Line_3__c= 'Address_Line_3' ,
                                            Default_Section_2_City__c= 'City' ,
                                            Default_Section_2_Zip__c= 'Zip' ,
                                            Default_Section_2_Country__c = 'Country' ,
                                            Default_Section_2_Other__c= 'Other' ,
                                            Default_Section_2_Tax_Name__c= 'Tax_Name' ,
                                            Default_Section_2_Tax_Id__c= 'Tax_Id' ,
                                            Default_Section_2_Contact_Name__c= 'Contact_Name' ,
                                            Default_Section_2_Contact_Email__c= 'Contact_Email' ,
                                            Default_Section_2_Contact_Tel__c= 'Contact_Tel' ,
                                            Alternate_Section_2_Address_Line_1__c= 'Address_Line_1' ,
                                            Alternate_Section_2_Address_Line_2__c= 'Address_Line_2' ,
                                            Alternate_Section_2_Address_Line_3__c= 'Address_Line_3' ,
                                            Alternate_Section_2_City__c= 'City' ,
                                            Alternate_Section_2_Zip__c= 'Zip' ,
                                            Alternate_Section_2_Country__c = 'Country' ,
                                            Alternate_Section_2_Other__c = 'Other' ,
                                            Alternate_Section_2_Tax_Name__c = 'Tax_Name' ,
                                            Alternate_Section_2_Tax_Id__c = 'Tax_Id' ,
                                            Alternate_Section_2_Contact_Name__c= 'Contact_Name' ,
                                            Alternate_Section_2_Contact_Email__c= 'Contact_Email',
                                            Alternate_Section_2_Contact_Tel__c= 'Contact_Tel',
                                            Default_Section_3_Address_Line_1__c= 'Address_Line_1' ,
                                            Default_Section_3_Address_Line_2__c= 'Address_Line_2' ,
                                            Default_Section_3_Address_Line_3__c= 'Address_Line_3' ,
                                            Default_Section_3_City__c= 'City' ,
                                            Default_Section_3_Zip__c= 'Zip' ,
                                            Default_Section_3_Country__c = 'Country' ,
                                            Default_Section_3_Other__c= 'Other' ,
                                            Default_Section_3_Tax_Name__c= 'Tax_Name' ,
                                            Default_Section_3_Tax_Id__c= 'Tax_Id' ,
                                            Default_Section_3_Contact_Name__c= 'Contact_Name' ,
                                            Default_Section_3_Contact_Email__c= 'Contact_Email' ,
                                            Default_Section_3_Contact_Tel__c= 'Contact_Tel' ,
                                            Alternate_Section_3_Address_Line_1__c= 'Address_Line_1' ,
                                            Alternate_Section_3_Address_Line_2__c= 'Address_Line_2' ,
                                            Alternate_Section_3_Address_Line_3__c= 'Address_Line_3' ,
                                            Alternate_Section_3_City__c= 'City' ,
                                            Alternate_Section_3_Zip__c= 'Zip' ,
                                            Alternate_Section_3_Country__c = 'Country' ,
                                            Alternate_Section_3_Other__c = 'Other' ,
                                            Alternate_Section_3_Tax_Name__c = 'Tax_Name' ,
                                            Alternate_Section_3_Tax_Id__c = 'Tax_Id' ,
                                            Alternate_Section_3_Contact_Name__c= 'Contact_Name' ,
                                            Alternate_Section_3_Contact_Email__c= 'Contact_Email' ,
                                            Alternate_Section_3_Contact_Tel__c= 'Contact_Tel',
                                            Default_Section_4_Address_Line_1__c= 'Address_Line_1' ,
                                            Default_Section_4_Address_Line_2__c= 'Address_Line_2' ,
                                            Default_Section_4_Address_Line_3__c= 'Address_Line_3' ,
                                            Default_Section_4_City__c= 'City' ,
                                            Default_Section_4_Zip__c= 'Zip' ,
                                            Default_Section_4_Country__c = 'Country' ,
                                            Default_Section_4_Other__c= 'Other' ,
                                            Default_Section_4_Tax_Name__c= 'Tax_Name' ,
                                            Default_Section_4_Tax_Id__c= 'Tax_Id' ,
                                            Default_Section_4_Contact_Name__c= 'Contact_Name' ,
                                            Default_Section_4_Contact_Email__c= 'Contact_Email' ,
                                            Default_Section_4_Contact_Tel__c= 'Contact_Tel' ,
                                            Alternate_Section_4_Address_Line_1__c= 'Address_Line_1' ,
                                            Alternate_Section_4_Address_Line_2__c= 'Address_Line_2' ,
                                            Alternate_Section_4_Address_Line_3__c= 'Address_Line_3' ,
                                            Alternate_Section_4_City__c= 'City' ,
                                            Alternate_Section_4_Zip__c= 'Zip' ,
                                            Alternate_Section_4_Country__c = 'Country' ,
                                            Alternate_Section_4_Other__c = 'Other' ,
                                            Alternate_Section_4_Tax_Name__c = 'Tax_Name' ,
                                            Alternate_Section_4_Tax_Id__c = 'Tax_Id' ,
                                            Alternate_Section_4_Contact_Name__c= 'Contact_Name' ,
                                            Alternate_Section_4_Contact_Email__c= 'Contact_Email' ,
                                            Alternate_Section_4_Contact_Tel__c= 'Contact_Tel' ,
                                            VAT_Reclaim_Destination__c = FALSE,
                                            Final_Destination__c ='Brazil',
                                            Country__c = country.id
                                            
                                           );
        insert cpav2;
        
        
        IOR_Price_List__c priceList = new IOR_Price_List__c();
        priceList.Destination__c = 'Brazil';
        priceList.Client_Name__c = account2.Id;
        priceList.IOR_Fee__c = 10;
        
        Insert priceList;
        IOR_Price_List__c iorpl = new IOR_Price_List__c ( Client_Name__c = account2.Id, Name = 'Brazil', IOR_Fee__c = 0.15, On_Charge_Mark_up__c = 0.15,
                                                         TecEx_Shipping_Fee_Markup__c =0.40, IOR_Min_Fee__c = 100,  Admin_Fee__c =100, Set_up_fee__c =100, Bank_Fees__c =200, Tax_Rate__c = 0.20,
                                                         Estimated_Customs_Brokerage__c =250, Estimated_Customs_Clearance__c =250, Estimated_Customs_Handling__c =250, Estimated_Import_License__c =250, Destination__c = 'Brazil' );
        
        insert iorpl;
        IOR_Price_List__c iorpl1 = new IOR_Price_List__c ( Client_Name__c = account2.Id, Name = 'Belgium', IOR_Fee__c = 0.15, On_Charge_Mark_up__c = 0.15,
                                                          TecEx_Shipping_Fee_Markup__c =0.40, IOR_Min_Fee__c = 100,  Admin_Fee__c =100, Set_up_fee__c =100, Bank_Fees__c =200, Tax_Rate__c = 0.20,
                                                          Estimated_Customs_Brokerage__c =250, Estimated_Customs_Clearance__c =250, Estimated_Customs_Handling__c =250, Estimated_Import_License__c =250, Destination__c = 'Belgium' );
        
        insert iorpl1;
         
        shipment_order__c shipmentOrder = new shipment_order__c();
        shipmentOrder.RecordTypeId = shipmentOrderRecTypeId;
        shipmentOrder.Destination__c = 'Brazil';
        shipmentOrder.Ship_From_Country__c = 'Australia';
        shipmentOrder.IOR_Price_List__c = priceList.Id;
        shipmentOrder.Account__c = account2.Id;
        shipmentOrder.CPA_v2_0__c = cpav2.Id;
        shipmentOrder.Shipping_Status__c = 'Shipping status';
        shipmentOrder.Shipment_Value_USD__c =100;
        shipmentOrder.xxxTotal_Invoice_Amount__c =100;
        shipmentOrder.Finance_Fee__c =100;
        shipmentOrder.Insurance_Cost_CIF__c =100;
        shipmentOrder.Li_ion_Batteries__c = 'Yes';
        shipmentOrder.Client_Taking_Liability_Cover__c = 'Yes';
        shipmentOrder.Liability_Cover_Fee_Added__c = true;
        shipmentOrder.Special_Client_Invoicing__c = true;
        shipmentOrder.Clear_out_all_costings__c = false;
        shipmentOrder.FC_Total_Customs_Brokerage_Cost__c = 10.0;
        shipmentOrder.Minimum_Brokerage_Costs__c = 5.0;
        shipmentOrder.FC_Total_Clearance_Costs__c = 10.0;
        shipmentOrder.Minimum_Clearance_Costs__c = 5.0;
        shipmentOrder.Minimum_License_Permit_Costs__c = 5.0;
        shipmentOrder.FC_Total_Handling_Costs__c = 10.0;
        shipmentOrder.Minimum_Handling_Costs__c = 5.0;
        shipmentOrder.FC_Total_License_Cost__c = 10.0;
        shipmentOrder.Customs_Brokerage_Cost_fixed__c = 0.0;
        shipmentOrder.Insurance_Cost_CIF__c =100;
        shipmentOrder.IOR_and_Import_Compliance_Fee_USD_numb__c =100;
        shipmentOrder.Handling_and_Admin_Fee__c =100;
        shipmentOrder.Bank_Fees__c =100;
        shipmentOrder.EOR_and_Export_Compliance_Fee_USD_numb__c =100;
        shipmentOrder.FC_IOR_and_Import_Compliance_Fee_USD__c =100;
        shipmentOrder.Forecast_Override__c =true;
        shipmentOrder.IOR_Cost_Custom_b__c =100;
        shipmentOrder.IOR_Cost_If_in_USD_2__c =100;
        shipmentOrder.FC_Miscellaneous_Fee__c = 0.0;
       
        Insert shipmentOrder;
        
        SO_Status_Description__c SOSD = new SO_Status_Description__c(CPA_v2_0__c = cpav2.Id,Status__c='In-Transit',Type__c = 'Shipping status',Max_Days__c = 1,Min_Days__c = 1);
        SO_Status_Description__c SOSD1 = new SO_Status_Description__c(CPA_v2_0__c = cpav2.Id,Status__c='In-Transit',Type__c = 'Shipping status',Max_Days__c = 1,Min_Days__c = 1);
        SO_Status_Description__c SOSD2 = new SO_Status_Description__c(CPA_v2_0__c = cpav2.Id,Status__c='In-Transit',Type__c = 'Shipping status',Max_Days__c = 1,Min_Days__c = 1);
        SO_Status_Description__c SOSD3 = new SO_Status_Description__c(CPA_v2_0__c = cpav2.Id,Status__c='In-Transit',Type__c = 'Shipping status',Max_Days__c = 1,Min_Days__c = 1);
        List<SO_Status_Description__c> SOSDList = new List<SO_Status_Description__c>();
        SOSDList.add(SOSD);
        SOSDList.add(SOSD1);
        SOSDList.add(SOSD2);
        SOSDList.add(SOSD3);
        insert SOSDList;
        
        Freight__c FR = new Freight__c();
        FR.Pick_up_Contact_Details__c='Test Full Name';
        FR.Ship_From_Country__c = 'Australia';
        FR.Ship_To__c = 'Brazil';
        FR.Client__c = account2.Id;
        FR.Suggested_Supplier_Name__c = 'Test Supplier';
        Fr.Shipment_Order__c = shipmentOrder.Id;
        
        Insert FR;
        
        
        
        zkmulti__MCShipment__c shipment = new zkmulti__MCShipment__c();
        shipment.zkmulti__Carrier__c = 'FedEx';
        shipment.Freight_Request__c = FR.Id;
        insert shipment;
        
        zkmulti__MCCheckpoint__c mcCheckpointObj = new zkmulti__MCCheckpoint__c();
        mcCheckpointObj.zkmulti__Address__c = '1600 Parkway CA';
        mcCheckpointObj.zkmulti__Shipment__c = shipment.Id;
        insert mcCheckpointObj;
    }
    
    static testMethod void testCreateSOLogWith3DigitCountryCode(){
        
        
        CountryandRegionMap__c crm = new CountryandRegionMap__c();
        crm.Name = 'Brazil';
        crm.Country__c = 'Brazil';
        crm.European_Union__c = 'No';
        crm.Region__c = 'South America';
        
        insert crm;
        
        CountryandRegionMap__c crm1 = new CountryandRegionMap__c();
        crm1.Name = 'United States';
        crm1.Country__c = 'United States';
        crm1.European_Union__c = 'No';
        crm1.Region__c = 'Oceania';
        
        insert crm1;
        
        DefaultFreightValues__c df = new DefaultFreightValues__c();
        df.Name = 'Test';
        df.Courier_Base_Rate_KG__c = 12;
        df.Courier_Dangerous_Goods_premium__c = 12;
        df.Courier_Fixed_Charge__c = 12;
        df.Courier_Non_stackable_premium__c = 12;
        df.Courier_Oversized_premium__c = 12;
        df.FF_Base_Rate_KG__c = 12;
        df.FF_Dangerous_Goods_Fixed_Charge_Premium__c = 12;
        df.FF_Dangerous_Goods_premium__c = 12;
        df.FF_Dedicated_Pickup__c = 12;
        df.FF_Fixed_Charge__c = 12;
        df.FF_Non_stackable_Fixed_Charge_Premium__c = 12;
        df.FF_Non_stackable_premium__c = 12;
        df.FF_Oversized_Fixed_Charge_Premium__c = 12;
        df.FF_Oversized_premium__c = 12;
        insert df;
        
        RegionalFreightValues__c rfm = new RegionalFreightValues__c();
        rfm.Name = 'Oceania to South America';
        rfm.Courier_Discount_Premium__c = 12;
        rfm.Courier_Fixed_Premium__c = 10;
        rfm.Destination__c = 'Brazil';
        rfm.FF_Fixed_Premium__c = 123;
        rfm.FF_Regional_Discount_Premium__c = 12;
        rfm.Origin__c = 'Australia';
        rfm.Preferred_Courier_Id__c = 'test';
        rfm.Preferred_Courier_Name__c = 'test';
        rfm.Preferred_Freight_Forwarder_Id__c = 'test';
        rfm.Preferred_Freight_Forwarder_Name__c = 'test';
        rfm.Risk_Adjustment_Factor__c = 1;
        insert rfm;
        List<CountryofOriginMap__c> threeDigitCodeList = new List<CountryofOriginMap__c>();
        
        for(Integer i=0;i<=1;i++){
            CountryofOriginMap__c comSetting = new CountryofOriginMap__c();
            comSetting.Country__c = 'California';
            comSetting.Name = 'California'+i;
            comSetting.Currency_ISO_Code__c = 'USD';
            comSetting.Three_Digit_Country_Code__c = 'CAA';
            comSetting.Two_Digit_Country_Code__c = '';
            threeDigitCodeList.add(comSetting);
        }
        insert threeDigitCodeList;
        
        Tracking_rules__c trackRulesObj = new Tracking_rules__c();
        trackRulesObj.Name = 'TestTracking Rule';
        trackRulesObj.Tracking_term__c = 'NONE';
        trackRulesObj.Checkpoint_Message_Contains__c = 'NONE';
        trackRulesObj.Checkpoint_Location_country_contains__c = 'NONE';
        trackRulesObj.Current_Status__c = 'NONE';
        trackRulesObj.Mapped_Status__c = 'In-Transit';
        trackRulesObj.Description__c = '';
        trackRulesObj.Type_of_Update__c = 'NONE';
        trackRulesObj.Eligible_Carriers__c = 'DHL Express';
        insert trackRulesObj;
        
        
        Account account2 = new Account (name='TecEx Prospective Client', Type ='Supplier', ICE__c = '0050Y000001km5c', RecordTypeId = '0120Y0000006KjQQAU',Insurance_Fee__c=10);
        insert account2;
        
        Contact contact = new Contact ( lastname ='Testing Individual',  email = 'anilk@tecex.com',UserName__c = 'anilk@tecex.com', Password__c = 'Test', contact_Status__C = 'Active', AccountId = account2.Id);
        insert contact;
        IOR_Price_List__c iorpl = new IOR_Price_List__c ( Client_Name__c =account2.Id, Name = 'Brazil', IOR_Fee__c = 0.15, On_Charge_Mark_up__c = 0.15,
                                                         TecEx_Shipping_Fee_Markup__c =0.40, IOR_Min_Fee__c = 100,  Admin_Fee__c =100, Set_up_fee__c =100, Bank_Fees__c =200, Tax_Rate__c = 0.20,
                                                         Estimated_Customs_Brokerage__c =250, Estimated_Customs_Clearance__c =250, Estimated_Customs_Handling__c =250, Estimated_Import_License__c =250, Destination__c = 'Brazil' );
        
        insert iorpl;
        IOR_Price_List__c iorpl1 = new IOR_Price_List__c ( Client_Name__c =account2.Id, Name = 'Belgium', IOR_Fee__c = 0.15, On_Charge_Mark_up__c = 0.15,
                                                          TecEx_Shipping_Fee_Markup__c =0.40, IOR_Min_Fee__c = 100,  Admin_Fee__c =100, Set_up_fee__c =100, Bank_Fees__c =200, Tax_Rate__c = 0.20,
                                                          Estimated_Customs_Brokerage__c =250, Estimated_Customs_Clearance__c =250, Estimated_Customs_Handling__c =250, Estimated_Import_License__c =250, Destination__c = 'Belgium' );
        
        insert iorpl1;
        Id shipmentOrderRecTypeId = Schema.SObjectType.shipment_order__c.getRecordTypeInfosByName().get('Cost Estimate').getRecordTypeId();
        
        CpaRules__c cpaRule = new CpaRules__c();
        cpaRule.Country__c = 'Brazil';
        cpaRule.Criteria__c = 'Default';
        
        Insert cpaRule;
        
        Country__c country = new Country__c(Brokerage_Costs__c = 618, CIF_Adjustment_Factor__c = 0.035, CIF_Absolute_value_adjustment__c = 200, Clearance_Costs__c = 488,
                                            Country__c = 'Brazil', Name = 'Brazil', Handling_Costs__c = 1180, License_Permit_Costs__c = 0);
        insert country;
        
        CPA_v2_0__c RelatedCostingCPA = new CPA_v2_0__c(Name = 'Brazil',Country_Applied_Costings__c =TRUE);
        Insert RelatedCostingCPA;
        
        CPA_v2_0__c cpav2 = new CPA_v2_0__c(Name = 'Brazil Testing 2.0', Supplier__c = account2.Id, Service_Type__c = 'IOR/EOR', Related_Costing_CPA__c = RelatedCostingCPA.Id, Preferred_Supplier__c = TRUE,
                                            VAT_Rate__c = 0.1, Destination__c = 'Brazil',
                                            Default_Section_1_Address_Line_1__c= 'Address_Line_1' ,
                                            Default_Section_1_Address_Line_2__c= 'Address_Line_2' ,
                                            Default_Section_1_Address_Line_3__c= 'Address_Line_3' ,
                                            Default_Section_1_City__c= 'City' ,
                                            Default_Section_1_Zip__c= 'Zip' ,
                                            Default_Section_1_Country__c = 'Country' ,
                                            Default_Section_1_Other__c= 'Other' ,
                                            Default_Section_1_Tax_Name__c= 'Tax_Name' ,
                                            Default_Section_1_Tax_Id__c= 'Tax_Id' ,
                                            Default_Section_1_Contact_Name__c= 'Contact_Name' ,
                                            Default_Section_1_Contact_Email__c= 'Contact_Email' ,
                                            Default_Section_1_Contact_Tel__c= 'Contact_Tel' ,
                                            Alternate_Section_1_Address_Line_1__c= 'Address_Line_1' ,
                                            Alternate_Section_1_Address_Line_2__c= 'Address_Line_2' ,
                                            Alternate_Section_1_Address_Line_3__c= 'Address_Line_3' ,
                                            Alternate_Section_1_City__c= 'City' ,
                                            Alternate_Section_1_Zip__c= 'Zip' ,
                                            Alternate_Section_1_Country__c = 'Country' ,
                                            Alternate_Section_1_Other__c = 'Other' ,
                                            Alternate_Section_1_Tax_Name__c = 'Tax_Name' ,
                                            Alternate_Section_1_Tax_Id__c = 'Tax_Id' ,
                                            Alternate_Section_1_Contact_Name__c= 'Contact_Name' ,
                                            Alternate_Section_1_Contact_Email__c= 'Contact_Email' ,
                                            Alternate_Section_1_Contact_Tel__c= 'Contact_Tel',
                                            Default_Section_2_Address_Line_1__c= 'Address_Line_1' ,
                                            Default_Section_2_Address_Line_2__c= 'Address_Line_2' ,
                                            Default_Section_2_Address_Line_3__c= 'Address_Line_3' ,
                                            Default_Section_2_City__c= 'City' ,
                                            Default_Section_2_Zip__c= 'Zip' ,
                                            Default_Section_2_Country__c = 'Country' ,
                                            Default_Section_2_Other__c= 'Other' ,
                                            Default_Section_2_Tax_Name__c= 'Tax_Name' ,
                                            Default_Section_2_Tax_Id__c= 'Tax_Id' ,
                                            Default_Section_2_Contact_Name__c= 'Contact_Name' ,
                                            Default_Section_2_Contact_Email__c= 'Contact_Email' ,
                                            Default_Section_2_Contact_Tel__c= 'Contact_Tel' ,
                                            Alternate_Section_2_Address_Line_1__c= 'Address_Line_1' ,
                                            Alternate_Section_2_Address_Line_2__c= 'Address_Line_2' ,
                                            Alternate_Section_2_Address_Line_3__c= 'Address_Line_3' ,
                                            Alternate_Section_2_City__c= 'City' ,
                                            Alternate_Section_2_Zip__c= 'Zip' ,
                                            Alternate_Section_2_Country__c = 'Country' ,
                                            Alternate_Section_2_Other__c = 'Other' ,
                                            Alternate_Section_2_Tax_Name__c = 'Tax_Name' ,
                                            Alternate_Section_2_Tax_Id__c = 'Tax_Id' ,
                                            Alternate_Section_2_Contact_Name__c= 'Contact_Name' ,
                                            Alternate_Section_2_Contact_Email__c= 'Contact_Email',
                                            Alternate_Section_2_Contact_Tel__c= 'Contact_Tel',
                                            Default_Section_3_Address_Line_1__c= 'Address_Line_1' ,
                                            Default_Section_3_Address_Line_2__c= 'Address_Line_2' ,
                                            Default_Section_3_Address_Line_3__c= 'Address_Line_3' ,
                                            Default_Section_3_City__c= 'City' ,
                                            Default_Section_3_Zip__c= 'Zip' ,
                                            Default_Section_3_Country__c = 'Country' ,
                                            Default_Section_3_Other__c= 'Other' ,
                                            Default_Section_3_Tax_Name__c= 'Tax_Name' ,
                                            Default_Section_3_Tax_Id__c= 'Tax_Id' ,
                                            Default_Section_3_Contact_Name__c= 'Contact_Name' ,
                                            Default_Section_3_Contact_Email__c= 'Contact_Email' ,
                                            Default_Section_3_Contact_Tel__c= 'Contact_Tel' ,
                                            Alternate_Section_3_Address_Line_1__c= 'Address_Line_1' ,
                                            Alternate_Section_3_Address_Line_2__c= 'Address_Line_2' ,
                                            Alternate_Section_3_Address_Line_3__c= 'Address_Line_3' ,
                                            Alternate_Section_3_City__c= 'City' ,
                                            Alternate_Section_3_Zip__c= 'Zip' ,
                                            Alternate_Section_3_Country__c = 'Country' ,
                                            Alternate_Section_3_Other__c = 'Other' ,
                                            Alternate_Section_3_Tax_Name__c = 'Tax_Name' ,
                                            Alternate_Section_3_Tax_Id__c = 'Tax_Id' ,
                                            Alternate_Section_3_Contact_Name__c= 'Contact_Name' ,
                                            Alternate_Section_3_Contact_Email__c= 'Contact_Email' ,
                                            Alternate_Section_3_Contact_Tel__c= 'Contact_Tel',
                                            Default_Section_4_Address_Line_1__c= 'Address_Line_1' ,
                                            Default_Section_4_Address_Line_2__c= 'Address_Line_2' ,
                                            Default_Section_4_Address_Line_3__c= 'Address_Line_3' ,
                                            Default_Section_4_City__c= 'City' ,
                                            Default_Section_4_Zip__c= 'Zip' ,
                                            Default_Section_4_Country__c = 'Country' ,
                                            Default_Section_4_Other__c= 'Other' ,
                                            Default_Section_4_Tax_Name__c= 'Tax_Name' ,
                                            Default_Section_4_Tax_Id__c= 'Tax_Id' ,
                                            Default_Section_4_Contact_Name__c= 'Contact_Name' ,
                                            Default_Section_4_Contact_Email__c= 'Contact_Email' ,
                                            Default_Section_4_Contact_Tel__c= 'Contact_Tel' ,
                                            Alternate_Section_4_Address_Line_1__c= 'Address_Line_1' ,
                                            Alternate_Section_4_Address_Line_2__c= 'Address_Line_2' ,
                                            Alternate_Section_4_Address_Line_3__c= 'Address_Line_3' ,
                                            Alternate_Section_4_City__c= 'City' ,
                                            Alternate_Section_4_Zip__c= 'Zip' ,
                                            Alternate_Section_4_Country__c = 'Country' ,
                                            Alternate_Section_4_Other__c = 'Other' ,
                                            Alternate_Section_4_Tax_Name__c = 'Tax_Name' ,
                                            Alternate_Section_4_Tax_Id__c = 'Tax_Id' ,
                                            Alternate_Section_4_Contact_Name__c= 'Contact_Name' ,
                                            Alternate_Section_4_Contact_Email__c= 'Contact_Email' ,
                                            Alternate_Section_4_Contact_Tel__c= 'Contact_Tel' ,
                                            VAT_Reclaim_Destination__c = FALSE,
                                            Final_Destination__c ='Brazil',
                                            Country__c = country.id
                                            
                                           );
        insert cpav2;
        
        
        IOR_Price_List__c priceList = new IOR_Price_List__c();
        priceList.Destination__c = 'Brazil';
        priceList.Client_Name__c = account2.Id;
        priceList.IOR_Fee__c = 10;
        
        Insert priceList;
        
        
        
       
        shipment_order__c shipmentOrder = new shipment_order__c();
        shipmentOrder.RecordTypeId = shipmentOrderRecTypeId;
        shipmentOrder.Destination__c = 'Brazil';
        shipmentOrder.Ship_From_Country__c = 'Australia';
        shipmentOrder.IOR_Price_List__c = priceList.Id;
        shipmentOrder.Account__c = account2.Id;
        shipmentOrder.CPA_v2_0__c = cpav2.Id;
        shipmentOrder.Shipping_Status__c = 'Shipping status';
        shipmentOrder.Shipment_Value_USD__c =100;
        shipmentOrder.xxxTotal_Invoice_Amount__c =100;
        shipmentOrder.Finance_Fee__c =100;
        shipmentOrder.Insurance_Cost_CIF__c =100;
        shipmentOrder.Li_ion_Batteries__c = 'Yes';
        shipmentOrder.Client_Taking_Liability_Cover__c = 'Yes';
        shipmentOrder.Liability_Cover_Fee_Added__c = true;
        shipmentOrder.Special_Client_Invoicing__c = true;
        shipmentOrder.Clear_out_all_costings__c = false;
        shipmentOrder.FC_Total_Customs_Brokerage_Cost__c = 10.0;
        shipmentOrder.Minimum_Brokerage_Costs__c = 5.0;
        shipmentOrder.FC_Total_Clearance_Costs__c = 10.0;
        shipmentOrder.Minimum_Clearance_Costs__c = 5.0;
        shipmentOrder.Minimum_License_Permit_Costs__c = 5.0;
        shipmentOrder.FC_Total_Handling_Costs__c = 10.0;
        shipmentOrder.Minimum_Handling_Costs__c = 5.0;
        shipmentOrder.FC_Total_License_Cost__c = 10.0;
        shipmentOrder.Customs_Brokerage_Cost_fixed__c = 0.0;
        shipmentOrder.Insurance_Cost_CIF__c =100;
        shipmentOrder.IOR_and_Import_Compliance_Fee_USD_numb__c =100;
        shipmentOrder.Handling_and_Admin_Fee__c =100;
        shipmentOrder.Bank_Fees__c =100;
        shipmentOrder.EOR_and_Export_Compliance_Fee_USD_numb__c =100;
        shipmentOrder.FC_IOR_and_Import_Compliance_Fee_USD__c =100;
        shipmentOrder.Forecast_Override__c =true;
        shipmentOrder.IOR_Cost_Custom_b__c =100;
        shipmentOrder.IOR_Cost_If_in_USD_2__c =100;
        shipmentOrder.FC_Miscellaneous_Fee__c = 0.0;
       
        Insert shipmentOrder;
        
        SO_Status_Description__c SOSD = new SO_Status_Description__c(CPA_v2_0__c = cpav2.Id,Status__c='In-Transit',Type__c = 'Shipping status',Max_Days__c = 1,Min_Days__c = 1);
        SO_Status_Description__c SOSD1 = new SO_Status_Description__c(CPA_v2_0__c = cpav2.Id,Status__c='In-Transit',Type__c = 'Shipping status',Max_Days__c = 1,Min_Days__c = 1);
        SO_Status_Description__c SOSD2 = new SO_Status_Description__c(CPA_v2_0__c = cpav2.Id,Status__c='In-Transit',Type__c = 'Shipping status',Max_Days__c = 1,Min_Days__c = 1);
        SO_Status_Description__c SOSD3 = new SO_Status_Description__c(CPA_v2_0__c = cpav2.Id,Status__c='In-Transit',Type__c = 'Shipping status',Max_Days__c = 1,Min_Days__c = 1);
        List<SO_Status_Description__c> SOSDList = new List<SO_Status_Description__c>();
        SOSDList.add(SOSD);
        SOSDList.add(SOSD1);
        SOSDList.add(SOSD2);
        SOSDList.add(SOSD3);
        insert SOSDList;
        
        Freight__c FR = new Freight__c();
        FR.Pick_up_Contact_Details__c='Test Full Name';
        FR.Ship_From_Country__c = 'Australia';
        FR.Ship_To__c = 'Brazil';
        FR.Client__c = account2.Id;
        Fr.Shipment_Order__c = shipmentOrder.Id;
        FR.Suggested_Supplier_Name__c = 'Test Supplier';
        Insert FR;
        
        
        
        zkmulti__MCShipment__c shipment = new zkmulti__MCShipment__c();
        shipment.zkmulti__Carrier__c = 'DHL Express';
        shipment.Freight_Request__c = FR.Id;
        insert shipment;
        
        zkmulti__MCCheckpoint__c mcCheckpointObj = new zkmulti__MCCheckpoint__c();
        mcCheckpointObj.zkmulti__Address__c = '1600 Parkway CAA';
        mcCheckpointObj.zkmulti__Shipment__c = shipment.Id;
        insert mcCheckpointObj;
    }
}