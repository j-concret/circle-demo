public class Lex_AutoFill_Address {
    // AutoPlace Code
    //Method to call google API and fetch the address details by addressID 
    @AuraEnabled
    public static String getAddressDetailsByPlaceId(String PlaceID){
        String APIKey = 'AIzaSyCdwojMVLcu2JbRoRheb9isbNLxC0wRwR4';
        String result = null;
        system.debug('SearchText is ' + PlaceID);
        try{
            if(PlaceID != null){
                String APIUrl = 'https://maps.googleapis.com/maps/api/place/details/json?placeid=' + PlaceId.replace(' ', '%20') + '&key=' + APIKey; 
                system.debug('APIUrl is ' + APIUrl);
                HttpRequest req = new HttpRequest();
                req.setMethod('GET');
                req.setEndpoint(APIURL);
                Http http = new Http();
                HttpResponse res = http.send(req);
                Integer statusCode = res.getStatusCode();
                system.debug('statusCode is ' + statusCode);
                if(statusCode == 200){
                    system.debug('API invoked successfully');
                    result = res.getBody();
                }
            }
        }
        catch(exception e){
            //Handling exception
            system.debug(e.getMessage());
        }
        return result;
    }
    //Method to call google API and fetch the address recommendations 
    @AuraEnabled
    public static String getAddressFrom(String SearchText){
        String APIKey = 'AIzaSyCdwojMVLcu2JbRoRheb9isbNLxC0wRwR4';
        String result = null;
        system.debug('SearchText is ' + SearchText);
        try{
            if(SearchText != null){
                String APIUrl = 'https://maps.googleapis.com/maps/api/place/autocomplete/json?input=' + SearchText.replace(' ', '%20') + '&key=' + APIKey; 
                system.debug('APIUrl is ' + APIUrl);
                HttpRequest req = new HttpRequest();
                req.setMethod('GET');
                req.setEndpoint(APIURL);
                Http http = new Http();
                HttpResponse res = http.send(req);
                Integer statusCode = res.getStatusCode();
                system.debug('statusCode is ' + statusCode);
                if(statusCode == 200){
                    system.debug('API invoked successfully');
                    result = res.getBody();
                }
            }
        }
        catch(exception e){
            //Handling exception
            system.debug(e.getMessage());
        }
        return result;
        //        return 'abc';
    }
    
    //Method to get ZIP Code from Custom Setting Object(no_zip_code_countries__c) for COuntries with No ZIP Code
    @AuraEnabled
    public static Map<String,Object> getZipCodeByCountry(String countryName){
        
        Map<String,Object> response = new Map<String,Object>{'status' => 'OK'};
            
            try{
                
                response.put('countryName',countryName);
                no_zip_code_countries__c nzp = [SELECT Id,Name,zip_code__c FROM no_zip_code_countries__c WHERE Name = :countryName limit 1];
                response.put('no_zip_code_countries',nzp);
                
            }catch(Exception e){
                
                response.put('status','ERROR');
                response.put('message',e.getMessage());
                
            }
        
        return response;
    }
}