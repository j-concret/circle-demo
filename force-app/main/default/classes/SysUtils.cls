/* 
**   General Utility Class which will contain common routines that can be resused within the system
*/

public class SysUtils {

	public enum ListOp {ALL, SOME}
	
	public static String logException(Exception error)
    {
     	if (error instanceof System.DMLException)
    	{
    		System.DMLException dmlError = (System.DMLException)error;
    		for (Integer i = 0; i < dmlError.getNumDml(); i++) {
        		System.Debug(dmlError.getDmlMessage(i));
        		return dmlError.getDmlMessage(i);
    		}
    		return '';
    	}
    	else {
    		System.debug(error);
    		return error.getMessage();	
    	}
    }
    
   	public static void processException(Exception error, SObject so)
    {
    	logException(error);
    	
    	if ((so != null) && (error instanceof System.DMLException))
    	{
    		System.DMLException dmlError = (System.DMLException)error;
    		
        	if (dmlError.getNumDml() > 0)
        		so.addError(dmlError.getDmlMessage(0), false);
        	else
        		so.addError(error, false);
    	}
    	else
    		so.addError(error, false);
    }
    
    public static Boolean empty(String x) {
    	return (x == null || x ==''); 
    }
    
    public static Boolean empty(ListOp op, list<String> items) {
    	for (String i: items)
    		if ((op == ListOp.SOME) && empty(i))
    			return true;
    		else if ((op == ListOp.ALL) && !empty(i))
    			return false;
    	return op == ListOp.ALL ? true : false; 
    }
    
    public static String getOrElse(String val, String def) {
    	if (val == null)
    		return def;
    	else
    		return val; 
    }
    
    public static String join(set<String> items, String delim) {
    	String result = '';
    	for (String s: items) {
    		if (result != '') result += delim;
    		result += s; 
    	}
    	return result;
    }
    
    //// SOQL Stuff
    public static String selectFrom(String tableName, List<String> fieldList) {
    	String result = 'SELECT ';
    	for (String item : fieldList)
    		result += item + ', ';
    	result = result.subString(0, result.length() - 2) + ' FROM ' + tableName; 
    	System.Debug(result);
    	return result;
    }
    public static String selectFrom(String tableName, Set<String> fieldList, List<String> otherFields) {
    	for (String item : fieldList)
    		otherFields.add(item);
    	return selectFrom(tableName, otherFields);
    }
    
}