public class TPaymentsSubmission {
    @future(callout = True)
    Public static  void submit(){
        
        string result;
        string resbody;
        string resstatus;
        //List<Invoice_New__c> opps= new List<Invoice_New__c>(); 
        String supplierRTypeId = Schema.Sobjecttype.Invoice_New__c.getRecordTypeInfosByDeveloperName().get('Supplier').getRecordTypeId();
        
        
        Map<String, String> xmlDataMap = new Map<String,String>();
        Map<String, Object> invoiceMap = new Map<String,Object>();
        //  String masterKey = 'j2O2HSydXz/YqW6fLD1ZDXupBI8Q6yTf5euM3dRZD7IYfwodhvC12N8M/Gc5lckO'; //sandbox
        String masterKey = '6/AruxtA1M+gP+g5fZRtpYm0jXJdaQWZNv6k8oJu6DBYEH5papLWbnJGtc3o1SVg'; // Production
        String Payername = 'TecEx';
        //DateTime dt = Datetime.now();
        //String GroupTitle = dt.format('yyyMMdd');
        String paymentGroupTitle = 'TecExPayments';
        System.debug('paymentGroupTitle'+paymentGroupTitle);
        
        Decimal Timestampheader1 = datetime.now().gettime()/1000;
        System.debug('Timestampheader1-->'+Timestampheader1);
        String untime=string.valueOf(Timestampheader1);
        
        
        String params= Payername+untime+paymentGroupTitle;
        String FinalKey = generateHmacSHA256Signature(params,masterKey);
        System.debug('params-->'+params);
        System.debug('FinalKey-->'+FinalKey);
        
        //opps=new list<Invoice_New__c>();
        
        map<id,string> accidoppids=new map<id,string>();              
        list<Invoice_New__c> opps=[select id,Account__c from Invoice_New__c where Due_Date__c = TODAY];
        if(opps.size()>0){
            for(Invoice_New__c op:opps){
                if(accidoppids.containskey(op.Account__c)){
                    string opstr=accidoppids.get(op.Account__c)+';'+op.id;
                    accidoppids.remove(op.Account__c);
                    accidoppids.put(op.Account__c,opstr);
                }else{
                    accidoppids.put(op.Account__c,op.id);  
                }
            }
        }
        
        
        Dom.Document doc = new Dom.Document();
        dom.XmlNode envelope = doc.createRootElement('soap:Envelope', null, null);
        envelope.setAttribute('xmlns:xsi','http://www.w3.org/2001/XMLSchema-instance');
        envelope.setAttribute('xmlns:xsd','http://www.w3.org/2001/XMLSchema');
        envelope.setAttribute('xmlns:soap','http://schemas.xmlsoap.org/soap/envelope/');
        
        dom.XmlNode body = envelope.addChildElement('soap:Body',null,null);
        
        Dom.Xmlnode ProcessPaymentsNode = body.addChildElement('ProcessPayments', null, null);
        ProcessPaymentsNode.setAttribute('xmlns','http://Tipalti.org/');
        Dom.Xmlnode PayerNamenode = ProcessPaymentsNode.addChildElement('payerName', null, null);
        PayerNamenode.addTextNode(Payername) ; 
        
        Dom.Xmlnode paymentGroupTitlenode = ProcessPaymentsNode.addChildElement('paymentGroupTitle', null, null);
        paymentGroupTitlenode.addTextNode(paymentGroupTitle); 
        
        Dom.Xmlnode tipaltiPaymentsOrdersnode = ProcessPaymentsNode.addChildElement('tipaltiPaymentsOrders', null, null);
        
        List<Tipalti_Payment_Submission__c>  tipaltiPaymentSubmissions = new List<Tipalti_Payment_Submission__c>();
        List<Invoice_New__c> invoiceList = [SELECT Account__c, Name, Invoice_Currency__c , Amount_Outstanding_Local_Currency__c
                                            FROM Invoice_New__c 
                                            where RecordTypeId = :supplierRTypeId
                                            AND Due_Date__c = TODAY
                                            AND Submitted_to_Tipalti__c = false 
                                            AND Account__r.Eligible_for_Tipalti_Autopay__c = true 
                                            AND Auto_Pay__c = 'Yes'
                                            AND ( Payment_Tracking__c = 'Ready For Payment'  or Payment_Tracking__c = 'Urgent') 
                                            AND (Invoice_Status__c = 'FM Approved' OR Invoice_Status__c = 'Partial Payment') 
                                            AND Tipalti_validation_completed__c = true
                                           ];
        
        for(Invoice_New__c invoice:invoiceList){
            if(invoiceMap.containsKey(invoice.Account__c)){
                Map<String,String> invoiceDetail = (Map<String,String>)invoiceMap.get(invoice.Account__c);
                String includedInvoices = invoiceDetail.get('includedInvoices')+';'+invoice.Name;
                String includedInvoiceIds = invoiceDetail.get('includesInvoiceIds')+';'+invoice.Id;
                double sumOfInvoices =0;
                if(invoice.Amount_Outstanding_Local_Currency__c != null)
                    sumOfInvoices = (Double.valueOf(invoiceDetail.get('sumOfInvoices')))+invoice.Amount_Outstanding_Local_Currency__c;
                Map<String,String> invoiceDetailMap = new Map<String,String>{
                    'includedInvoices' => includedInvoices,
                        'includesInvoiceIds' => includedInvoiceIds,
                        'sumOfInvoices'=> String.valueOf(sumOfInvoices),
                        'currency'=> (invoice.Invoice_Currency__c).substringBetween('(', ')')
                        };
                            invoiceMap.put(invoice.Account__c,invoiceDetailMap);
            }else{
                
                Map<String,String> invoiceDetail = new Map<String,String>{
                    'includedInvoices' => invoice.Name,
                        'includesInvoiceIds' => invoice.Id,
                        'sumOfInvoices'=> (invoice.Amount_Outstanding_Local_Currency__c != null) ? String.valueOf(invoice.Amount_Outstanding_Local_Currency__c): '0',
                            'currency'=>  (invoice.Invoice_Currency__c).substringBetween('(', ')')
                            };
                                invoiceMap.put(invoice.Account__c,invoiceDetail);
            }
        }
        //  System.debug('Updateing the invoiceMap to TRUE');
        // System.debug(JSON.serializePretty(invoiceMap));
        integer i=0;
        for(String account:invoiceMap.keyset()){
            
            Map<String,String> invoiceDetail = (Map<String,String>)invoiceMap.get(account);
            String currenyCode = invoiceDetail.get('currency');
            dom.XmlNode TipaltiPaymentOrderItemnode=tipaltiPaymentsOrdersnode.addChildElement('TipaltiPaymentOrderItem',null, null);
            Datetime Timestampheader2 = datetime.now();
            String Refcodefinal = Timestampheader2.format('MM-ddmmss')+string.valueof(i);
            i=i+1;
            system.debug('Refcodefinal-->'+Refcodefinal);
            //id accid=(id)ag.get('Account__c');
            double amt=Double.valueOf(invoiceDetail.get('sumOfInvoices'));
            TipaltiPaymentOrderItemnode.addChildElement('Idap', null, null).addTextNode(account);
            TipaltiPaymentOrderItemnode.addChildElement('Amount', null, null).addTextNode(string.valueof(amt));
            TipaltiPaymentOrderItemnode.addChildElement('RefCode', null, null).addTextNode(Refcodefinal);
            //string str=accidoppids.get(accid);
            TipaltiPaymentOrderItemnode.addChildElement('IgnoreThresholds', null, null).addTextNode('true');
            TipaltiPaymentOrderItemnode.addChildElement('Currency', null, null).addTextNode(currenyCode);
            
            // Custom fieldnodes start
            dom.XmlNode CustomFieldnode=TipaltiPaymentOrderItemnode.addChildElement('CustomFields',null, null);
            String invIds = invoiceDetail.get('includesInvoiceIds'); 
            System.debug('complete Invoice List');
            System.debug(invIds);
            if(invIds.length() > 94){
                String invWithinFirst = invIds.mid(0, 94);
                dom.XmlNode Keyvaluepairnode=CustomFieldnode.addChildElement('KeyValuePair',null, null);
                Keyvaluepairnode.addChildElement('Key',null, null).addTextNode('invoicenumber');
                Keyvaluepairnode.addChildElement('Value',null, null).addTextNode(invWithinFirst);
                System.debug('at line 138 invWithinFirst');
                System.debug(invWithinFirst);
                if(invIds.length() > 189){
                    String invWithinSecond = invIds.mid(95, 94);
                    System.debug('at line 142 invWithinSecond');
                    System.debug(invWithinSecond);
                    dom.XmlNode Keyvaluepairnode1=CustomFieldnode.addChildElement('KeyValuePair',null, null);
                    Keyvaluepairnode1.addChildElement('Key',null, null).addTextNode('invoicenumber2');
                    Keyvaluepairnode1.addChildElement('Value',null, null).addTextNode(invWithinSecond);
                    if(invIds.length() > 284){
                        String invWithinThree = invIds.mid(190, 94);
                        System.debug('at line 149 invWithinThree');
                        System.debug(invWithinThree);
                        dom.XmlNode Keyvaluepairnode2=CustomFieldnode.addChildElement('KeyValuePair',null, null);
                        Keyvaluepairnode2.addChildElement('Key',null, null).addTextNode('invoicenumber3');
                        Keyvaluepairnode2.addChildElement('Value',null, null).addTextNode(invWithinThree);
                        
                    }else{
                        String invWithinThree = invIds.mid(190, (invIds.length()-190));
                        System.debug('at line 157 else of invWithinThree');
                        System.debug(invWithinThree);
                        dom.XmlNode Keyvaluepairnode2=CustomFieldnode.addChildElement('KeyValuePair',null, null);
                        Keyvaluepairnode2.addChildElement('Key',null, null).addTextNode('invoicenumber3');
                        Keyvaluepairnode2.addChildElement('Value',null, null).addTextNode(invWithinThree);
                    }
                }else{
                    String invWithinSecond = invIds.mid(95, (invIds.length()-95));
                    System.debug('at line 164 else of invWithinSecond');
                    System.debug(invWithinSecond);
                    dom.XmlNode Keyvaluepairnode1=CustomFieldnode.addChildElement('KeyValuePair',null, null);
                    Keyvaluepairnode1.addChildElement('Key',null, null).addTextNode('invoicenumber2');
                    Keyvaluepairnode1.addChildElement('Value',null, null).addTextNode(invWithinSecond);
                }
                
            }else{
                System.debug('at line 173 else of invWithinFirst');
                System.debug(invIds);
                dom.XmlNode Keyvaluepairnode=CustomFieldnode.addChildElement('KeyValuePair',null, null);
                Keyvaluepairnode.addChildElement('Key',null, null).addTextNode('invoicenumber');
                Keyvaluepairnode.addChildElement('Value',null, null).addTextNode(invIds);
            }
            
            
            // Custom fieldnodes End
            
            tipaltiPaymentSubmissions.add(new Tipalti_Payment_Submission__c(
                Account__c = account,Amount__c = amt, Idap__c = account, 
                Invoiced_Ids__c = invoiceDetail.get('includesInvoiceIds'),
                Invoiced_Names__c = invoiceDetail.get('includedInvoices')));
            
        }
        // Decimal Timestampheader = datetime.now().gettime()/1000;
        
        ProcessPaymentsNode.addChildElement('timeStamp', null, null).addTextNode(string.valueof(Timestampheader1));
        ProcessPaymentsNode.addChildElement('key', null, null).addTextNode(FinalKey);     
        String xmlString =doc.toXmlString();
        
        
        System.debug('xmlString =' + xmlString);
        result=xmlString;
        Integer strlength=xmlString.length();
        string length=string.valueOf(strlength);
        Http P=new Http();
        HttpRequest req = new HttpRequest();
        
        // req.setEndpoint('https://api.sandbox.tipalti.com/v9/payerfunctions.asmx'); //Sandbox
        req.setEndpoint('https://api.tipalti.com/v9/PayerFunctions.asmx'); //Production
        req.setMethod('POST');
        req.setTimeout(120000);
        req.setHeader('Content-Type','text/xml;charset=utf-8');
        req.setHeader('Content-Length', length);
        req.setHeader('SOAPAction', 'http://Tipalti.org/ProcessPayments');
        req.setBody(xmlString);
        
        
        
        HttpResponse response = P.send(req);
        system.debug('status code is >>>>'+ response.getStatusCode() + ' ' + response.getStatus());
        system.debug('status code is >>>>'+ response.getStatusCode() + ' ' + response.getStatus());
        System.debug('Body is >>>>'+response.getBody());
        
        if (response.getStatus()=='OK') {
            API_Log__c apiLog1 = new API_Log__c (Response__c = xmlString, Calling_Method_Name__c  = 'Submited Payment XML - in Success block');
            insert apiLog1;
            
            System.debug('The status code returned was expected: ');
            system.debug('status code is >>>>'+ response.getStatusCode() + ' ' + response.getStatus());
            System.debug('Body is >>>>'+response.getBody());
            resbody=response.getBody();
            resstatus=response.getStatus();
            generateAPILogs(response,'Payment submitted - success Response');
            markInvoiceTipulti();
            System.debug('tipaltiPaymentSubmissions::');
            System.debug(JSON.serializePretty(tipaltiPaymentSubmissions));
            System.debug('response::');
            System.debug(response);
            System.debug('resbody::');
            System.debug(response.getBody());
            parseResponse(response.getBody(),tipaltiPaymentSubmissions);
            
        } else {
            API_Log__c apiLog1 = new API_Log__c (Response__c = xmlString, Calling_Method_Name__c  = 'Submited Payment XML - in Fail block');
            insert apiLog1;
            System.debug('Body is >>>>'+response.getBody());
            system.debug('status code is >>>>'+ response.getStatusCode() + ' ' + response.getStatus());
            resbody=response.getBody();
            resstatus=response.getStatus();
            generateAPILogs(response,'Payment submitted -  failure Response');
        }   
        
        //API_Log__c apiLog = new API_Log__c (Response__c = xmlString, Calling_Method_Name__c  = 'Before Submit, XML');
        //insert apiLog;
        
    }
    
    //Generate Signature
    public  static String generateHmacSHA256Signature(String Keyvalue,string ClientsecreteValue) 
    {     
        String algorithmName = 'HmacSHA256';
        Blob hmacData = Crypto.generateMac(algorithmName,  Blob.valueOf(KeyValue), Blob.valueOf(ClientsecreteValue));
        String hextext = EncodingUtil.convertToHex(hmacData);
        
        return hextext;
    }
    
    public static void generateAPILogs(HttpResponse response,String url){
        API_Log__c apiLog = new API_Log__c (StatusCode__c = String.valueOf((Integer)response.getStatusCode()),Response__c = response.getBody(), Calling_Method_Name__c  = url);
        insert apiLog;
    }
    
    public static void markInvoiceTipulti(){
        String supplierRTypeId = Schema.Sobjecttype.Invoice_New__c.getRecordTypeInfosByDeveloperName().get('Supplier').getRecordTypeId();
        
        List<Invoice_New__c> invoiceList = new List<Invoice_New__c>();
        For(Invoice_New__c invoice : [SELECT Account__c,Submitted_to_Tipalti__c,
                                      Amount_Paid_via_Tipalti_Local_Currency__c,Amount_Outstanding_Local_Currency__c 
                                      FROM Invoice_New__c 
                                      where  RecordTypeId = :supplierRTypeId
                                      AND Due_Date__c = TODAY
                                      AND Submitted_to_Tipalti__c = false 
                                      AND Account__r.Eligible_for_Tipalti_Autopay__c = true
                                      AND Auto_Pay__c = 'Yes'
                                      AND ( Payment_Tracking__c = 'Ready For Payment'  or Payment_Tracking__c = 'Urgent') 
                                      AND (Invoice_Status__c = 'FM Approved' OR Invoice_Status__c = 'Partial Payment') 
                                      AND Tipalti_validation_completed__c = true]){
                                          invoice.Submitted_to_Tipalti__c = true;
                                          invoice.Amount_Paid_via_Tipalti_Local_Currency__c = invoice.Amount_Outstanding_Local_Currency__c;
                                          invoiceList.add(invoice);       
                                      }
        update invoiceList;
    }
    public static void parseResponse(String responseXML,List<Tipalti_Payment_Submission__c>  tipaltiPaymentSubmissions){
        //String responseXML = '<?xml version="1.0" encoding="utf-8"?><soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><soap:Body><ProcessPaymentsResponse xmlns="http://Tipalti.org/"><ProcessPaymentsResult><errorMessage>OK</errorMessage><errorCode>0</errorCode><linesResults><ExpandedLineResult><status>Warning</status><paymentOrderStatus>Deferred</paymentOrderStatus><message>Fees exceed amount to be paid</message><refCode>08-14-50-08</refCode><lineErrorOrWarningType>0</lineErrorOrWarningType></ExpandedLineResult></linesResults><summary><TipaltiProcessPaymentsSummary><accountType>7</accountType><accountCurrency>USD</accountCurrency><fundsRequired>0</fundsRequired><outstandingAmount>0</outstandingAmount><tipaltiSummaryAmounts><TipaltiSummaryAmounts><summaryRowCurrency>USD</summaryRowCurrency><totalAmount>10</totalAmount><immediateAmount>10</immediateAmount><payableAmount>0</payableAmount><payerFees>0</payerFees><scheduledAmount>0</scheduledAmount><outstandingAmount>0</outstandingAmount><withholdingAmount>0</withholdingAmount><estimatedProviderFees>0</estimatedProviderFees></TipaltiSummaryAmounts></tipaltiSummaryAmounts></TipaltiProcessPaymentsSummary></summary></ProcessPaymentsResult></ProcessPaymentsResponse></soap:Body></soap:Envelope>';
        // String responseXML = 
        List<Tipalti_Payment_History__c> tipaltiHistories = new List<Tipalti_Payment_History__c>();
        DOM.Document doc = new DOM.Document();
        String toParse = responseXML;
        doc.load(toParse);
        String errorCode;
        String errorMessage;
        String refCode;
        DOM.XMLNode root = doc.getRootElement();
        
        String nms = root.getNameSpace();
        
        System.Debug('namespace: ' + nms);
        
        DOM.XMLNode body = root.getChildElement('Body', nms);
        
        System.Debug('body: ' + body);
        
        List<DOM.XMLNode> bodyChildrenList = body.getChildElements();
        
        for (DOM.XMLNode passThroughReqResponse : bodyChildrenList) {
            System.Debug('passThroughReqResponse: ' + passThroughReqResponse.getName());
            
            List<DOM.XMLNode> passThroughReqResultList = passThroughReqResponse.getChildElements();
            
            for (DOM.XMLNode passThroughReqResult : passThroughReqResultList) {
                System.Debug('passThroughReqResult: ' + passThroughReqResult.getName());
                
                List<DOM.XMLNode> pickResponseList = passThroughReqResult.getChildElements();
                
                for (DOM.XMLNode pickResponse : pickResponseList) {
                    System.Debug('pickResponse: ' + pickResponse.getName());
                    if(pickResponse.getName() != null && pickResponse.getName() == 'errorCode'){
                        errorCode = pickResponse.getText();
                    }
                    if(pickResponse.getName() != null && pickResponse.getName() == 'errorMessage'){
                        errorMessage = pickResponse.getText();
                    }
                    List<DOM.XMLNode> filesList = pickResponse.getChildElements();
                    
                    for (DOM.XMLNode files : filesList) {
                        System.Debug('files: ' + files.getName());
                        
                        List<DOM.XMLNode> fileList = files.getChildElements();
                        
                        for (DOM.XMLNode file : fileList) {
                            System.Debug('file: ' + file.getName());
                            if(file.getName() != null && file.getName() == 'refCode'){
                                refCode = file.getText();
                            }
                            
                            List<DOM.XMLNode> itemList = file.getChildElements();
                            
                            for (DOM.XMLNode item : itemList) {
                                System.Debug('item: ' + item.getName());
                                
                                List<DOM.XMLNode> aList = item.getChildElements();
                                
                                for (DOM.XMLNode a : aList) {
                                    System.Debug('a: ' + a.getName());
                                    System.Debug('text: ' + a.getText());
                                }
                            }
                        }
                    }
                }
            }
        }
        
        System.debug('errorCode::');
        System.debug(errorCode);
        System.debug('errorMessage::');
        System.debug(errorMessage);
        System.debug('refCode::');
        System.debug(refCode);
        if(errorCode == '0' && errorMessage == 'OK' && refCode != null){
            System.debug('Do tipalti thing');
            //  List<Tipalti_Payment_Submission__c>  
            for(Tipalti_Payment_Submission__c tps : tipaltiPaymentSubmissions){
                tps.Reference_Number__c = refCode;
            }
            if(tipaltiPaymentSubmissions!=null && !tipaltiPaymentSubmissions.isEmpty()) {
                insert tipaltiPaymentSubmissions;
                for(Tipalti_Payment_Submission__c tps : tipaltiPaymentSubmissions){
                    for(String invoiceNumber: (tps.Invoiced_Ids__c).split('\\;')){
                        tipaltiHistories.add(new Tipalti_Payment_History__c(
                            Supplier_Invoice__c = invoiceNumber,Tipalti_Payment_Submission__c = tps.Id,
                            Type__c = 'Submitted to Tipalti'
                        ));
                    }
                }
            }
            if(tipaltiHistories!=null && !tipaltiHistories.isEmpty()) {
                System.debug('tipaltiHistories');
                System.debug(JSON.serializePretty(tipaltiHistories));
                insert tipaltiHistories;
            }
            
        }
        
        System.debug('tipaltiPaymentSubmissions::');
        System.debug(JSON.serializePretty(tipaltiPaymentSubmissions));
    }
}