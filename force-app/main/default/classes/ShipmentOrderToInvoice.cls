public class ShipmentOrderToInvoice {
    private Shipment_Order__c shipmentOrder;
    private fw1__Invoice__c inv = new fw1__Invoice__c();
    private fw1__Payment_Center_Setting__c settings;
    
    public fw1__Invoice__c createInvoice(string shipmentOrderId) {
        if (String.isEmpty(shipmentOrderId))
            return null;
            
        settings = getDefaultSetting();    
        shipmentOrder = getShipmentOrder(shipmentOrderId);
        insertInvoiceHeader();
        insertInvoiceLines();
        
        return inv;
    }
    
    private Shipment_Order__c getShipmentOrder(Id shipmentOrderId){
        return [SELECT Name,Id,Account__c,Ship_From_Country__c,Ship_to_Country__c,
                    Shipment_Value_USD__c,No_of_Parts__c,Final_Delivery_Date__c,
                    Int_Courier_Name__c,Int_Courier_Tracking_No__c, //IOR_Fee__c,
                    Set_up_Fee__c,Handling_and_Admin_Fee__c,Bank_Fees__c,Total_Customs_Brokerage_Cost__c,
                    //Recharge_License_Cost__c,Int_Courier_Fee__c,
                    //Ancillary_Costs__c,Recharge_Local_Deliver_Costs__c,
                    TecEx_In_house_Fee__c,Insurance_Fee_USD__c,
                    IOR_FEE_USD__c                   
                FROM Shipment_Order__c
                WHERE Id = :shipmentOrderId];
    }
    
    private void insertInvoiceHeader(){
        inv.fw1__Account__c = shipmentOrder.Account__c;
        inv.Shipment_Order__c = shipmentOrder.id;
        //inv.Ship_to_Country__c = shipmentOrder.Ship_to_Country__c;
        inv.fw1__Alternate_Invoice_Number__c = shipmentOrder.Name;
        inv.US_Bank_Details__c = settings.US_Bank_Details__c;
        inv.EU_Bank_Details__c = settings.EU_Bank_Details__c;
        inv.Notes__c = settings.Notes__c;
        inv.Euro_Conversion_Rate__c = settings.Euro_Conversion_Rate__c;
        
        Account acct = getAccount(shipmentOrder.Account__c);
        
        inv.fw1__Billing_Street__c = acct.BillingStreet;
        inv.fw1__Billing_City__c = acct.BillingCity;
        inv.fw1__Billing_State__c = acct.BillingState;
        inv.fw1__Billing_Country__c = acct.BillingCountry;
        inv.fw1__Billing_Zip__c = acct.BillingPostalCode;
        
        
        for (Contact c : acct.Contacts) {
            inv.fw1__Contact__c = c.Id;
            inv.fw1__Recipient_Email__c = c.Email; 
        } 
        
        insert inv;
        
    }
    
    private Account getAccount(Id accountId){
        return [
            SELECT BillingStreet, BillingCity, BillingState, 
                   BillingPostalCode,BillingCountry, (
                SELECT Id,Email, Contact_Role__c, Inactive__c
                FROM Contacts
                WHERE Inactive__c = false
                LIMIT 1)
            FROM Account
            WHERE Id = :accountId ];
    }
    
    private fw1__Payment_Center_Setting__c getDefaultSetting(){
        return [
            SELECT US_Bank_Details__c,EU_Bank_Details__c,Notes__c,Euro_Conversion_Rate__c
            FROM fw1__Payment_Center_Setting__c
            WHERE Name = 'Default Settings'
            LIMIT 1];
    }
    
    private void insertInvoiceLines(){
        List<fw1__Invoice_Line__c> invLines = new List<fw1__Invoice_Line__c>();
        
        if(shipmentOrder.IOR_FEE_USD__c != null && shipmentOrder.IOR_FEE_USD__c > 0) {
            invLines.add(getIORFee());
        }
        
        if(shipmentOrder.Set_up_Fee__c != null && shipmentOrder.Set_up_Fee__c > 0) {
            invLines.add(getSetupFee());
        }
        
        if(shipmentOrder.Handling_and_Admin_Fee__c != null && shipmentOrder.Handling_and_Admin_Fee__c > 0) {
            invLines.add(getHandlingFee());
        }
        
        if(shipmentOrder.Bank_Fees__c != null && shipmentOrder.Bank_Fees__c > 0) {
            invLines.add(getBankFee());
        }
        
        //if(shipmentOrder.Recharge_License_Cost__c != null && shipmentOrder.Recharge_License_Cost__c > 0) {
        //    invLines.add(getLicenseCost());
        //}
        
        if(shipmentOrder.Total_Customs_Brokerage_Cost__c != null && shipmentOrder.Total_Customs_Brokerage_Cost__c > 0) {
            invLines.add(getCustomsCost());
        }
        
        //if(shipmentOrder.Recharge_Local_Deliver_Costs__c != null && shipmentOrder.Recharge_Local_Deliver_Costs__c > 0) {
        //    invLines.add(getDeliverCost());
        //}
        
        //if(shipmentOrder.Ancillary_Costs__c != null && shipmentOrder.Ancillary_Costs__c > 0) {
        //    invLines.add(getAncillaryCost());
        //}
        
        if(shipmentOrder.TecEx_In_house_Fee__c != null && shipmentOrder.TecEx_In_house_Fee__c > 0) {
            invLines.add(getInHouseFee());
        }
        
        //if(shipmentOrder.Int_Courier_Fee__c != null && shipmentOrder.Int_Courier_Fee__c > 0) {
        //    invLines.add(getCourierFee());
        //}
        
        if(shipmentOrder.Insurance_Fee_USD__c != null && shipmentOrder.Insurance_Fee_USD__c > 0) {
            invLines.add(getInsuranceFee());
        }
        
        insert invLines;
    }
    
    private fw1__Invoice_Line__c getIORFee(){
        fw1__Invoice_Line__c l = new fw1__Invoice_Line__c();
        l.fw1__Invoice__c = inv.Id;
        l.Name = 'IOR Fee';
        l.fw1__Quantity__c = 1;
        l.fw1__Unit_Price__c = shipmentOrder.IOR_FEE_USD__c;   
        return l;
    }
    
    private fw1__Invoice_Line__c getSetupFee(){
        fw1__Invoice_Line__c l = new fw1__Invoice_Line__c();
        l.fw1__Invoice__c = inv.Id;
        l.Name = 'Setup Fee';
        l.fw1__Quantity__c = 1;
        l.fw1__Unit_Price__c = shipmentOrder.Set_up_Fee__c;   
        return l;
    }
    
    private fw1__Invoice_Line__c getHandlingFee(){
        fw1__Invoice_Line__c l = new fw1__Invoice_Line__c();
        l.fw1__Invoice__c = inv.Id;
        l.Name = 'Handling and Admin Fee';
        l.fw1__Quantity__c = 1;
        l.fw1__Unit_Price__c = shipmentOrder.Handling_and_Admin_Fee__c;   
        return l;
    }
    
    private fw1__Invoice_Line__c getBankFee(){
        fw1__Invoice_Line__c l = new fw1__Invoice_Line__c();
        l.fw1__Invoice__c = inv.Id;
        l.Name = 'Bank Fees';
        l.fw1__Quantity__c = 1;
        l.fw1__Unit_Price__c = shipmentOrder.Bank_Fees__c;   
        return l;
    }
    /*
    private fw1__Invoice_Line__c getLicenseCost(){
        fw1__Invoice_Line__c l = new fw1__Invoice_Line__c();
        l.fw1__Invoice__c = inv.Id;
        l.Name = 'Recharge - License Cost';
        l.fw1__Quantity__c = 1;
        l.fw1__Unit_Price__c = shipmentOrder.Recharge_License_Cost__c;   
        return l;
    }
    */
    private fw1__Invoice_Line__c getCustomsCost(){
        fw1__Invoice_Line__c l = new fw1__Invoice_Line__c();
        l.fw1__Invoice__c = inv.Id;
        l.Name = 'Recharge - Customs Brokerage Cost';
        l.fw1__Quantity__c = 1;
        l.fw1__Unit_Price__c = shipmentOrder.Total_Customs_Brokerage_Cost__c;   
        return l;
    }
    /*
    private fw1__Invoice_Line__c getDeliverCost(){
        fw1__Invoice_Line__c l = new fw1__Invoice_Line__c();
        l.fw1__Invoice__c = inv.Id;
        l.Name = 'Recharge - Local Deliver Costs';
        l.fw1__Quantity__c = 1;
        l.fw1__Unit_Price__c = shipmentOrder.Recharge_Local_Deliver_Costs__c;   
        return l;
    }
    
    private fw1__Invoice_Line__c getAncillaryCost(){
        fw1__Invoice_Line__c l = new fw1__Invoice_Line__c();
        l.fw1__Invoice__c = inv.Id;
        l.Name = 'Recharge - Ancillary Costs';
        l.fw1__Quantity__c = 1;
        l.fw1__Unit_Price__c = shipmentOrder.Ancillary_Costs__c;   
        return l;
    }
    */
    private fw1__Invoice_Line__c getInHouseFee(){
        fw1__Invoice_Line__c l = new fw1__Invoice_Line__c();
        l.fw1__Invoice__c = inv.Id;
        l.Name = 'TecEx In-house Fee';
        l.fw1__Quantity__c = 1;
        l.fw1__Unit_Price__c = shipmentOrder.TecEx_In_house_Fee__c;   
        return l;
    }
    /*
    private fw1__Invoice_Line__c getCourierFee(){
        fw1__Invoice_Line__c l = new fw1__Invoice_Line__c();
        l.fw1__Invoice__c = inv.Id;
        l.Name = 'International Courier Fee';
        l.fw1__Quantity__c = 1;
        l.fw1__Unit_Price__c = shipmentOrder.Int_Courier_Fee__c;   
        return l;
    }
    */
    
    private fw1__Invoice_Line__c getInsuranceFee(){
        fw1__Invoice_Line__c l = new fw1__Invoice_Line__c();
        l.fw1__Invoice__c = inv.Id;
        l.Name = 'Insurance Fee';
        l.fw1__Quantity__c = 1;
        l.fw1__Unit_Price__c = shipmentOrder.Insurance_Fee_USD__c;   
        return l;
    }
    
    public void savePdf(string shipmentOrderId) {
    
        shipmentOrder = getShipmentOrder(shipmentOrderId);
 
        PageReference pdf = new PageReference('/apex/EmailShipmentInvoicePDF?ids=' + shipmentOrderId);
     
        // create the new attachment
        Attachment attach = new Attachment();
     
        // the contents of the attachment from the pdf
        Blob body;
       
        try {
            // returns the output of the page as a PDF
            body = pdf.getContentAsPDF();
     
        // need to pass unit test -- current bug    
        } catch (VisualforceException e) {
            body = Blob.valueOf('Some Text');
        }
     
        attach.Body = body;
        // add the custom file name
        attach.Name = shipmentOrder.Name + '.pdf';
        attach.IsPrivate = false;
        // attach the pdf to the account
        attach.ParentId = shipmentOrderId;
        insert attach;
 
    }
    
    public string getShipmentTemplate() {
       EmailTemplate emailtemp = [SELECT Id FROM EmailTemplate WHERE Name = 'Shipment Template'];
       return emailtemp.Id;
    }
    

}