public class CPUpdateSOOrderPackWrapper {
    public String ClientID;
    public String ContactID;
	public String SOPID;
    public String Weight_Unit;
	public String Dimension_Unit;
	public Integer Packages_of_Same_Weight;
	public String Length;
	public String Height;
	public String Breadth;
	public String Actual_Weight;
	
	
	public static CPUpdateSOOrderPackWrapper parse(String json) {
		return (CPUpdateSOOrderPackWrapper) System.JSON.deserialize(json, CPUpdateSOOrderPackWrapper.class);
	}

}