@IsTest (SeeAllData=true)
private class TestNewRollOutController{

static testmethod void TestNewRollOut(){
   
   Account account = new Account (name='Acme5', Type ='Client', CSE_IOR__c = '0050Y000001LTZO');
   insert account; 
   
   Contact contact = new Contact ( lastname ='Testing Person',  AccountId = account.Id);
   insert contact; 
   
   country_price_approval__c cpa = new country_price_approval__c( name ='Aruba', Billing_term__c = 'DAP/CIF - IOR pays',  Airwaybill_Instructions__c = 'User IOR Address');
        insert cpa;
        
        IOR_Price_List__c iorpl = new IOR_Price_List__c ( Client_Name__c = account.Id, Name = 'Aruba', IOR_Fee__c = 0.15, On_Charge_Mark_up__c = 0.15,
        TecEx_Shipping_Fee_Markup__c =0.40, IOR_Min_Fee__c = 100,  Admin_Fee__c =100, Set_up_fee__c =100, Bank_Fees__c =200, Tax_Rate__c = 0.20,
        Estimated_Customs_Brokerage__c =250, Estimated_Customs_Clearance__c =250, Estimated_Customs_Handling__c =250, Estimated_Import_License__c =250);
        insert iorpl; 
   
   Roll_Out__c rollout = new Roll_Out__c( Client_Name__c = account.Id,   Contact_For_The_Quote__c = contact.Id);
   insert rollout;
  
   test.startTest();
   
 PageReference pageRef = Page.NewRollOutPage;
 Test.setCurrentPage(pageRef);
 pageRef.getParameters().put('id',rollout.id);
  

       
newRollOutController testNew = new newRollOutController(new ApexPages.StandardController(rollOut));  
    
    testNew.MCSV();
    testNew.MCVV();
    testNew.SaveAndNew();
    
   testNew.showMCSV =True;
   testNew.method1();
   System.assertEquals(False, testNew.showMCSV);  

  
      
    
     test.stopTest();
     }
}