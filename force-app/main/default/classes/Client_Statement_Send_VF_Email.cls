global class  Client_Statement_Send_VF_Email implements Schedulable
{
    private static final string newLine = '\n';

    global void execute(SchedulableContext SC)
    {
        sendStatementEmailToAM();
    }

    public void sendStatementEmailToAM()
    {
        PageReference ref = Page.CustomStatement;//Get the VF used to generate the statements

        Map<String, List<SenderWrapper>> accounts = getAccounts(); //Get all accounts that have outstanding amounts

        Messaging.SingleEmailMessage[] emails = new Messaging.SingleEmailMessage[]{}; //Collection of emails to be sent out

       OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where Address = 'finance@tecex.com']; //Get global email address ID for finance

        for (String accManager : accounts.keySet()) //Accounts is made up of Am as the key and a list of wrapper objects as the value.
        {
            List<SenderWrapper> wrappers = accounts.get(accManager); //Get all the wrapper records for this AM
        if (wrappers.size() == 0) //if there are no wrappers continue with the next AM and dont do anything further
        continue;

            Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage(); //The one email msg to be sent out to the AM

            List<Account> includedAccounts = new List<Account>(); //All accounts that will be mentioned in the email
            List<String> ToEmailIds = new List<String>(); //To 
            List<String> CcEmailIds = new List<String>();// CC

            ToEmailIds.add(wrappers[0].AM); //Add the AM email address in TO 
            CcEmailIds.add(wrappers[0].Finance);

            List<Messaging.EmailFileAttachment> attachments = new List<Messaging.EmailFileAttachment>(); //List of statements for all the  included accounts
            for (SenderWrapper wrapper : wrappers)
            {
                ref.getParameters().put('id', wrapper.Acc.Id);
                includedAccounts.Add(wrapper.Acc);
                Blob b;
                if(!test.isRunningTest())
                    b = ref.getContentAsPDF();
                else
                    b = Blob.valueof('TEST BODY');                    

                Messaging.EmailFileAttachment efa1 = new Messaging.EmailFileAttachment();
                efa1.setFileName('Outstanding Statement - ' + wrapper.Acc.Name + '.pdf');
                efa1.setBody(b);

                attachments.add(efa1);
        }


            email.setToAddresses(ToEmailIds);
            email.setccAddresses(CcEmailIds);
            email.setReplyTo(wrappers[0].Finance);
            if ( owea.size() > 0 )
            {
                email.setOrgWideEmailAddressId(owea.get(0).Id);
            }
            email.setFileAttachments(attachments);
            email.setSubject('Statements');

            emails.add(email);
            email.setPlainTextBody(buildEmailBody(accManager, includedAccounts));
        

    }
        if(!test.isRunningTest())
            Messaging.SendEmailResult [] r = Messaging.sendEmail(emails);
    
}

    private static String buildEmailBody(String AccountManger,List<Account> accounts)
    {
        String body = '';

        body += 'Good day ' + AccountManger.split(' ')[0] + ',' + newLine + newLine;

        body += 'Please see attached the statements as at ' + format(Date.today()) + ' for your following clients:' + newLine;

        for (account acc : accounts)
        {
            body += '*  ' + Acc.Name + newLine;
        }

        body += newLine + 'Please review the attached and reply to finance@tecex.com with a list of your queries and concerns regarding the statements.'  + newLine + newLine;

        body += 'If we receive no comment by ' + format(Date.today().addDays(2)) +  ', we will take the statement as reviewed and correct, and we will send the statement as is to the client on ' + format(Date.today().addDays(3)) + newLine + newLine + newLine + newLine;

        return body;
    }

    private static string format(Date d)
    {
        return d.year() + '-' + (d.month() < 10 ? '0' + d.month() : d.month().format()) + '-' + d.day();
    }

  
    private static Map<String, List<SenderWrapper>> getAccounts()
    {
        List<Account> accounts = [SELECT Name, Signed_By__c, Signed_By__r.Name, Signed_By__r.Email, CSE_IOR__c, CSE_IOR__r.Name, CSE_IOR__r.Email, Balance_outstanding__c, Total_Amount_Outstanding__c, Type FROM ACCOUNT WHERE Name NOT IN ('Integrated Group', 'Demo', 'Tecex Test Client 2') AND  Total_Amount_Outstanding__c > 5 AND CSE_IOR__c != null AND Type = 'Client' ORDER BY CSE_IOR__c];

        Map<String, List<SenderWrapper>> mapAccountEmails = new Map<String, List<SenderWrapper>>();

        for (Account acc : accounts)
        {
            if (!mapAccountEmails.containsKey(acc.CSE_IOR__r.Name))
                mapAccountEmails.put(acc.CSE_IOR__r.Name, new List<SenderWrapper>());

            mapAccountEmails.get(acc.CSE_IOR__r.Name).add(new SenderWrapper(acc, acc.Signed_By__r.Email, acc.CSE_IOR__r.Email, ''));
        }

        return mapAccountEmails;
    }

    public class SenderWrapper
    {
        Account Acc {get; set;}
        public String AM {get; set;}
        public String BM {get; set;}
        public String Finance { get; set;}

        public SenderWrapper(Account acc, String bm, String am, String finance)
        {
            this.AM = am;
            this.BM = bm;
            this.Finance = 'finance@tecex.com';
            this.Acc = acc;
        }
    }
}