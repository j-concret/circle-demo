@IsTest
public class NCPAPIAllCountries_Test {
	static testMethod void testGetMethod()
    {     
        RestRequest request = new RestRequest();
        RestResponse res = new RestResponse();
        request.requestUri ='/services/apexrest/api/GetAllCountrieslist';
        request.httpMethod = 'GET';
        RestContext.request = request;
        RestContext.response = res;
        Test.startTest();
        NCPAPIAllCountries.NCPAPIAllCountries();
        Test.stopTest();        
    }
}