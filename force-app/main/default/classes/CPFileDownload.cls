@RestResource(urlMapping='/FileDownload/*')
global class CPFileDownload {
    
     @Httpget
    global static void FileDownload(){
     	RestRequest req = RestContext.request;
    	RestResponse res = RestContext.response;
      //  res.addHeader('Content-Type', 'application/json');
        String FID = req.params.get('FID');
       
         try {
              Attachment a = [select ID,name,body,contenttype,parentId from attachment where id=:FID];
             res.addHeader('Content-Type',a.ContentType);
             res.AddHeader('Content-Disposition', 'attachment; filename=' + a.Name);
             res.responseBody = a.body;
             res.statusCode = 200;
             
            }
        catch(exception e){
           		res.responseBody = Blob.valueOf(e.getMessage());
            	res.statusCode = 500;
                
            	API_Log__c Al = New API_Log__c();
                //Al.Account__c=logincontact.AccountID;
                //Al.Login_Contact__c=logincontact.Id;
                Al.EndpointURLName__c='Filedownload';
                Al.Response__c=e.getMessage()+'----File ID>'+FID;
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al;
            
        }
    
    }

}