@isTest
public class NCPPickupAddCreation_Test {

    static testMethod void testPostMethod(){
        Access_token__c accessTokenObj = new Access_token__c();
        accessTokenObj.Status__c = 'Active';
        accessTokenObj.Access_Token__c = 'asdfghj-sdfgfds-sfdfgf';
        
        Insert accessTokenObj;
        
        Account acc = new Account(Name = 'Testing',type = 'Supplier',CSE_IOR__c = '0050Y000001LTZO', 
                                       Service_Manager__c = '0050Y000001LTZO', 
                                       Financial_Manager__c='0050Y000001LTZO', 
                                       Financial_Controller__c='0050Y000001LTZO');
        insert acc;

        Contact con = new Contact(FirstName = 'Testing',LastName = 'contact',AccountId = acc.Id,Email = 'test@test.com',Phone = '123456789');
        insert con;
        
        Id PickuprecordTypeId = Schema.SObjectType.Client_Address__c.getRecordTypeInfosByDeveloperName().get('PickUp').getRecordTypeId();

        Client_Address__c clientaddrObj = new Client_Address__c(Name='TestAddress',default__C=true,Address_status__c='Active',Comments__c='',All_Countries__c='Brazil',Client__c=acc.Id,City__c='Rio',CompanyName__c = 'Testing', Address__c='Adddress',Province__c = 'Trest',Postal_Code__c='304309',Contact_Phone_Number__c = '0722854399',Contact_Email__c ='TEst@test.com',Contact_Full_Name__c= 'TEstName');
        insert clientaddrObj;
        
        NCPPickUpaddCreationwra.PickUpAddress pickupAddrObj = new NCPPickUpaddCreationwra.PickUpAddress();
        pickupAddrObj.Accesstoken = accessTokenObj.Access_Token__c;
		pickupAddrObj.Name = '';
		pickupAddrObj.Contact_Full_Name = con.Name;
		pickupAddrObj.Contact_Email = con.Email;
		pickupAddrObj.Contact_Phone_Number = con.Phone;
		pickupAddrObj.Address1 = 'Address1';
		pickupAddrObj.Address2 = 'Address2';
		pickupAddrObj.City = 'Rio';
		pickupAddrObj.Province = 'Northern';
		pickupAddrObj.Postal_Code = '12345';
		pickupAddrObj.All_Countries = clientaddrObj.All_Countries__c;
		pickupAddrObj.AccountID = acc.Id;
        pickupAddrObj.AdditionalNumber = '8855220046';
     	pickupAddrObj.Comments = clientaddrObj.Comments__c;
        pickupAddrObj.CompanyName = clientaddrObj.CompanyName__c;
        pickupAddrObj.DefaultAddress = String.valueOf(clientaddrObj.Default__c);
        pickupAddrObj.AddressStatus = clientaddrObj.Address_Status__c;
        
        List<NCPPickUpaddCreationwra.PickUpAddress> pickupAddrList = new List<NCPPickUpaddCreationwra.PickUpAddress>();
        pickupAddrList.add(pickupAddrObj);
        
        NCPPickUpaddCreationwra wrapper = new NCPPickUpaddCreationwra();
        wrapper.PickUpAddress = pickupAddrList;
        
        RestRequest request = new RestRequest();
        RestResponse res = new RestResponse();
        request.requestUri ='/services/apexrest/api/PickupAddresscreate/';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueOf(JSON.serialize(wrapper));
        RestContext.request = request;
        RestContext.response = res;
        
        Test.startTest();
        NCPPickupAddCreation.NCPPickupAddCreation();
        Test.stopTest();
    }
    
    static testMethod void testPostMethodException(){
        Access_token__c accessTokenObj = new Access_token__c();
        accessTokenObj.Status__c = 'Active';
        accessTokenObj.Access_Token__c = 'asdfghj-sdfgfds-sfdfgf';
        
        Insert accessTokenObj;
        
        Account acc = new Account(Name = 'Testing',type = 'Supplier',CSE_IOR__c = '0050Y000001LTZO', 
                                       Service_Manager__c = '0050Y000001LTZO', 
                                       Financial_Manager__c='0050Y000001LTZO', 
                                       Financial_Controller__c='0050Y000001LTZO');
        insert acc;

        Contact con = new Contact(FirstName = 'Testing',LastName = 'contact',AccountId = acc.Id,Email = 'test@test.com',Phone = '123456789');
        insert con;
        
        Id PickuprecordTypeId = Schema.SObjectType.Client_Address__c.getRecordTypeInfosByDeveloperName().get('PickUp').getRecordTypeId();

        Client_Address__c clientaddrObj = new Client_Address__c(Name='TestAddress',default__C=true,Address_status__c='',Comments__c='',All_Countries__c='Brazil',Client__c=acc.Id,City__c='Rio',CompanyName__c = 'Testing', Address__c='Adddress',Province__c = 'Trest',Postal_Code__c='304309',Contact_Phone_Number__c = '0722854399',Contact_Email__c ='TEst@test.com',Contact_Full_Name__c= 'TEstName');
        insert clientaddrObj;
        
        NCPPickUpaddCreationwra.PickUpAddress pickupAddrObj = new NCPPickUpaddCreationwra.PickUpAddress();
        pickupAddrObj.Accesstoken = accessTokenObj.Access_Token__c;
		pickupAddrObj.Name = '';
		pickupAddrObj.Contact_Full_Name = con.Name;
		pickupAddrObj.Contact_Email = con.Email;
		pickupAddrObj.Contact_Phone_Number = con.Phone;
		pickupAddrObj.Address1 = 'Address1';
		pickupAddrObj.Address2 = 'Address2';
		pickupAddrObj.City = 'Rio';
		pickupAddrObj.Province = 'Northern';
		pickupAddrObj.Postal_Code = '12345';
		pickupAddrObj.All_Countries = clientaddrObj.All_Countries__c;
		pickupAddrObj.AccountID = acc.Id;
        pickupAddrObj.AdditionalNumber = '8855220046';
     	pickupAddrObj.Comments = clientaddrObj.Comments__c;
        pickupAddrObj.CompanyName = clientaddrObj.CompanyName__c;
        pickupAddrObj.AddressStatus = clientaddrObj.Address_Status__c;
        
        List<NCPPickUpaddCreationwra.PickUpAddress> pickupAddrList = new List<NCPPickUpaddCreationwra.PickUpAddress>();
        pickupAddrList.add(pickupAddrObj);
        
        NCPPickUpaddCreationwra wrapper = new NCPPickUpaddCreationwra();
        wrapper.PickUpAddress = pickupAddrList;
        
        RestRequest request = new RestRequest();
        RestResponse res = new RestResponse();
        request.requestUri ='/services/apexrest/api/PickupAddresscreate/';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueOf(JSON.serialize(wrapper));
        RestContext.request = request;
        RestContext.response = res;
        
        Test.startTest();
        NCPPickupAddCreation.NCPPickupAddCreation();
        Test.stopTest();
    }
}