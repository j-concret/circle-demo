@IsTest
public class TestCustomClearanceDetails {
    @isTest
    public static void getCCDDetails(){
        Test.startTest();
        Customs_Clearance_Documents__c customs_Clearance_Documents = TestCCDUtil.createSetUpData();
        Map<String,Object> recordMap = customClearanceDetails.getCCDDetails(customs_Clearance_Documents.Id);
        customs_Clearance_Documents.Name = 'Test CCD 1';
        Map<String,Object> updateMap = customClearanceDetails.updateCCDDetails(customs_Clearance_Documents);
        customs_Clearance_Documents.Review_Attachment__c = true;
        Map<String,Object> errorMap = customClearanceDetails.updateCCDDetails(customs_Clearance_Documents);
		Test.stopTest();
		System.assertEquals('OK', recordMap.get('status'));
        System.assertEquals('OK', updateMap.get('status'));
        System.assertEquals('DMLERROR', errorMap.get('status'));
    }
}