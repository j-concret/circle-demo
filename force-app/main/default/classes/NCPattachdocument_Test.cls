@isTest
public class NCPattachdocument_Test {
    
    static testMethod void  testErrorForNCPattachdocument(){
        
        RestRequest req1 = new RestRequest();
        RestResponse res1 = new RestResponse();
        req1.requestURI = '/services/apexrest/NCPattachdocuments/'; 
        req1.httpMethod = 'POST';
        req1.requestBody = Blob.valueOf('{"value":"Test response"}');     
        RestContext.request = req1;
        RestContext.response = res1;
        RestContext.request.addHeader('Content-Type', 'application/json');
        
        Test.startTest(); 
        	NCPattachdocument.NCPattachdocument();
        Test.stopTest();
    }

    static testMethod void  testForNCPattachdocument(){
        
        Access_token__c at = new Access_token__c(Status__c = 'Active',
            												 Access_Token__c = 'asdfghj-sdfgfds-sfdfgf');
        Insert at;
        
        Account acc = new Account(Name = 'Test Account');
        Insert acc;
        
        NCPattachdocumentWra.Attachment attachment = new NCPattachdocumentWra.Attachment();
        attachment.filename = 'Test file';
        attachment.filebody = Blob.valueOf('data of attachment');
        
        List<NCPattachdocumentWra.Attachment> attachmentList = new List<NCPattachdocumentWra.Attachment>();
        attachmentList.add(attachment);
        
        NCPattachdocumentWra obj = new NCPattachdocumentWra();
        obj.Accesstoken = at.Access_Token__c;
        obj.RecordID = acc.Id;
        obj.Atts = attachmentList;
        
        RestRequest req1 = new RestRequest();
        RestResponse res1 = new RestResponse();
        req1.requestURI = '/services/apexrest/NCPattachdocuments/'; 
        req1.httpMethod = 'POST';
        req1.requestBody = Blob.valueOf(JSON.serialize(obj));     
        RestContext.request = req1;
        RestContext.response = res1;
        RestContext.request.addHeader('Content-Type', 'application/json');
        
        Test.startTest(); 
        	NCPattachdocument.NCPattachdocument();
        Test.stopTest();
    }
}