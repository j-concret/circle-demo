@isTest
public class NCPDownloadCostestimateV2_Test {
    
	public static testMethod void  NCPDownloadattachment_Test1(){
        
        //Test Data
       Id shipmentOrderRecTypeId = Schema.SObjectType.shipment_order__c.getRecordTypeInfosByName().get('Cost Estimate').getRecordTypeId();
        
        CpaRules__c cpaRule = new CpaRules__c();
        cpaRule.Country__c = 'Finland';
        cpaRule.Criteria__c = 'Default';
        
        Insert cpaRule;
        
        Access_token__c accessTokenObj = new Access_token__c();
        accessTokenObj.Status__c = 'Active';
        accessTokenObj.Access_Token__c = 'asdfghj-sdfgfds-sfdfgf';
        
        Insert accessTokenObj;
        
        Account acc = new Account(Name = 'TecEx Prospective Client');
        
        Insert acc;
       Contact contact = new Contact (Client_Notifications_Choice__c = 'opt-in', email = 'test@test.com', lastname ='Testing Individual',  AccountId = acc.Id);
        
        Insert contact;
        
        IOR_Price_List__c priceList = new IOR_Price_List__c();
        priceList.Destination__c = 'Finland';
        priceList.Client_Name__c = acc.Id;
        priceList.IOR_Fee__c = 10;
        
        Insert priceList;
        
        shipment_order__c shipment = new shipment_order__c();
        shipment.RecordTypeId = shipmentOrderRecTypeId;
        shipment.Destination__c = 'Finland';
        shipment.IOR_Price_List__c = priceList.Id;
        shipment.Shipment_Value_USD__c = 600;
        shipment.Account__c = acc.Id;
        shipment.Client_Contact_for_this_Shipment__c =contact.Id ;
        shipment.Email_Estimate_To__c='abc@xyz.com';
        Insert shipment;
        
        Access_token__c at = new Access_token__c(status__c = 'Active', Access_Token__c = '123');
        insert at;
        Attachment att = new Attachment(ParentId = shipment.Id, Name = 'CostEstimate', body = Blob.valueOf('Unit Test Attachment Body'));
        insert att;
        
        //Json Body
        String json =  '{"Accesstoken" : "123","RecordID":"'+shipment.Id+'","Identifier":"0"}';
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
        req.requestURI = '/services/apexrest/NCPDownlodCostestimates'; 
        req.httpMethod = 'Post';
        req.addHeader('Content-Type', 'application/json');
        req.requestBody = Blob.valueof(json);
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
       		 NCPDownloadCostestimateV2.NCPDownloadattachment();
        Test.stopTest(); 
    }
	public static testMethod void  NCPDownloadattachment_Test2(){
        
        //Test Data
       Id shipmentOrderRecTypeId = Schema.SObjectType.shipment_order__c.getRecordTypeInfosByName().get('Cost Estimate').getRecordTypeId();
        
        CpaRules__c cpaRule = new CpaRules__c();
        cpaRule.Country__c = 'Finland';
        cpaRule.Criteria__c = 'Default';
        
        Insert cpaRule;
        
        Access_token__c accessTokenObj = new Access_token__c();
        accessTokenObj.Status__c = 'Active';
        accessTokenObj.Access_Token__c = 'asdfghj-sdfgfds-sfdfgf';
        
        Insert accessTokenObj;
        
        Account acc = new Account(Name = 'TecEx Prospective Client');
        
        Insert acc;
       Contact contact = new Contact (Client_Notifications_Choice__c = 'opt-in', email = 'test@test.com', lastname ='Testing Individual',  AccountId = acc.Id);
        
        Insert contact;
        
        IOR_Price_List__c priceList = new IOR_Price_List__c();
        priceList.Destination__c = 'Finland';
        priceList.Client_Name__c = acc.Id;
        priceList.IOR_Fee__c = 10;
        
        Insert priceList;
        
        shipment_order__c shipment = new shipment_order__c();
        shipment.RecordTypeId = shipmentOrderRecTypeId;
        shipment.Destination__c = 'Finland';
        shipment.IOR_Price_List__c = priceList.Id;
        shipment.Shipment_Value_USD__c = 600;
        shipment.Account__c = acc.Id;
        shipment.Client_Contact_for_this_Shipment__c =contact.Id ;
        shipment.Email_Estimate_To__c='abc@xyz.com';
        Insert shipment;
        
        Access_token__c at = new Access_token__c(status__c = 'Active', Access_Token__c = '123');
        insert at;
        Attachment att = new Attachment(ParentId = shipment.Id, Name = 'CostEstimate', body = Blob.valueOf('Unit Test Attachment Body'));
        insert att;

 
//Create Document
ContentVersion cv = new ContentVersion();
cv.Title = 'Test Document';
cv.PathOnClient = 'TestDocument.pdf';
cv.VersionData = Blob.valueOf('Test Content');
cv.IsMajorVersion = true;
Insert cv;
 
//Get Content Documents
Id conDocId = [SELECT ContentDocumentId FROM ContentVersion WHERE Id =:cv.Id].ContentDocumentId;
 
//Create ContentDocumentLink 
ContentDocumentLink cdl = New ContentDocumentLink();
cdl.LinkedEntityId = shipment.Id;
cdl.ContentDocumentId = conDocId;
cdl.shareType = 'V';
Insert cdl;
        //Json Body
        String json =  '{"Accesstoken" : "123","RecordID":"'+shipment.Id+'","Identifier":"1"}';
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
        req.requestURI = '/services/apexrest/NCPDownlodCostestimates'; 
        req.httpMethod = 'Post';
        req.addHeader('Content-Type', 'application/json');
        req.requestBody = Blob.valueof(json);
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
       		 NCPDownloadCostestimateV2.NCPDownloadattachment();
        Test.stopTest(); 
    }
    public static testMethod void  NCPDownloadattachment_Test(){
        
        //Test Data
        Access_token__c at = new Access_token__c(status__c = 'Active', Access_Token__c = '123');
        insert at;
        Attachment att = new Attachment(ParentId = at.Id, Name = '123Approved123', body = Blob.valueOf('Unit Test Attachment Body'));
        insert att;
        
        //Json Body
        String json =  '{"Accesstoken" : "xyz","RecordID":"'+at.Id+'","Identifier":"1"}';
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
        req.requestURI = '/services/apexrest/NCPDownlodCostestimates'; 
        req.httpMethod = 'Post';
        req.addHeader('Content-Type', 'application/json');
        req.requestBody = Blob.valueof(json);
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
       		 NCPDownloadCostestimateV2.NCPDownloadattachment();
        Test.stopTest(); 
    }
     public static testMethod void  NCPDownloadattachment_Test3(){
        
        //Test Data
        Access_token__c at = new Access_token__c(status__c = 'Active', Access_Token__c = '123');
        insert at;
        Attachment att = new Attachment(ParentId = at.Id, Name = '123Approved123', body = Blob.valueOf('Unit Test Attachment Body'));
        insert att;
        
        //Json Body
        String json =  '{"Accesstoken" : "123","RecordID":"'+at.Id+'","Identifier":"1"}';
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
        req.requestURI = '/services/apexrest/NCPDownlodCostestimates'; 
        req.httpMethod = 'Post';
        req.addHeader('Content-Type', 'application/json');
        req.requestBody = Blob.valueof(json);
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
       		 NCPDownloadCostestimateV2.NCPDownloadattachment();
        Test.stopTest(); 
    }

}