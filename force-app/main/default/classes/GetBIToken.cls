@RestResource(urlMapping='/GetBIToken/*')
Global class GetBIToken {
    @Httppost
    global static void GetBIToken(){
        
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        System.debug('req');
        System.debug(JSON.serializePretty(req));
        Blob body = req.requestBody;
        res.addHeader('Content-Type', 'application/json');
        String requestString = body.toString();
        GetBITokenwra rw = (GetBITokenwra)JSON.deserialize(requestString,GetBITokenwra.class);
        try {
            Access_token__c At =[select status__c,Access_Token__c from Access_token__c where Access_Token__c=:rw.Accesstoken ];
            if( at.status__C =='Active'){
                
                String Finaltoken;
                
                String requestBIToken = getPowerBIRequestToken.gettoken();
               
                                            
                if(requestBIToken != null){
                    
              String abcURL=  getPowerBIRequestToken.getEmbedURL(requestBIToken);     
              String abcTOK=  generateToken(requestBIToken,rw);
                    
                    
            JSONGenerator gen = JSON.createGenerator(true);
            gen.writeStartObject();
            
             gen.writeFieldName('Success');
             gen.writeStartObject();
            
                if(abcTOK==null) {gen.writeNullField('BIToken');} else{gen.writeStringField('BIToken', abcTOK);}
                if(abcURL==null) {gen.writeNullField('EmbedURL');} else{gen.writeStringField('EmbedURL', abcURL);}
            
            gen.writeEndObject();
            gen.writeEndObject();
            String jsonData = gen.getAsString();
            res.responseBody = Blob.valueOf(jsonData);
            res.addHeader('Content-Type', 'application/json');
            res.statusCode = 200; 
                 API_Log__c Al = New API_Log__c();
                    Al.EndpointURLName__c='GetBIToken';
                    Al.Login_Contact__c=rw.contactID;
                    Al.Response__c='BITOKEN--> ' +abcTOK +' // EmbedURL--> ' +abcURL    ;
                    Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al;
                    
                }
                
                
            }
            
            
            
            
            
        }
        
        catch(exception e){
           
                            res.responseBody = Blob.valueOf(e.getMessage());
                            res.addHeader('Content-Type', 'application/json');
                            
                            res.statusCode = 500;
                            API_Log__c Al = New API_Log__c();
                            //Al.Account__c=rw.AccountID;
                            //Al.Login_Contact__c=rw.ContactId;
                            Al.EndpointURLName__c='NCP-GetBIToken';
                            Al.Response__c=e.getMessage();
                            Al.StatusCode__c=string.valueof(res.statusCode);
                            Insert Al;

            
        }
        
        
    }
    
    public static String generateToken(String requestedToken,GetBITokenwra rw){
        
         system.debug('token-->'+requestedToken);
         system.debug('GetBITokenwra-->'+rw);
        
        
        // String bodyStr ='client_id=03e8300e-2059-4580-b1a6-94bee02e8fb5&resource=https://analysis.windows.net/powerbi/api&client_secret=jKuyGTWB8G9-Q2v4vE1r4-.4e0__-wdo1C&grant_type=client_credentials' ;
        // String length = string.valueof(bodyStr.length());
        String bodyStr = getjsonBody(rw);
        Http P=new Http();
        HttpRequest req = new HttpRequest();
        
        req.setEndpoint('https://api.powerbi.com/v1.0/myorg/GenerateToken');
        req.setbody(bodyStr);
        req.setMethod('POST');
        req.setHeader('Content-Type','application/json');
        req.setHeader('Authorization','Bearer '+requestedToken);
        // req.setTimeout(120000);
        HttpResponse response = P.send(req);
        
        system.debug('response'+response);
        system.debug('response of Generate Token '+response.getBody()  );
        
       // return string.valueOf(response.getBody());
         String token='';
        JSONParser parser = JSON.createParser(response.getBody());
        while (parser.nextToken() != null) {
            if(parser.getCurrentName() == 'token') {
                parser.nextValue();
                token = parser.getText();
                break;
            }
        }
       return token;                    
        
        
    }
    
    public static String getjsonBody(GetBITokenwra rw){
       
        String jsonData='';
        JSONGenerator gen = JSON.createGenerator(true);
        gen.writeStartObject();
        
        gen.writeFieldName('datasets');
        gen.writeStartArray();
        gen.writeStartObject();
        gen.writeStringField('id',Power_Bi_Requests__c.getInstance('PowerBi').datasetId__c);
        gen.writeEndObject();
        gen.writeEndArray();
        
        gen.writeFieldName('reports');
        gen.writeStartArray();
        gen.writeStartObject();
        gen.writeStringField('id',Power_Bi_Requests__c.getInstance('PowerBi').reportId__c);
        gen.writeEndObject();
        gen.writeEndArray();
        
        gen.writeFieldName('identities');
        gen.writeStartArray();
        gen.writeStartObject();
        
        gen.writeStringField('username',rw.ContactID);
        
        gen.writeFieldName('roles');
        gen.writeStartArray();
        gen.writeString('PortalUser');
        gen.writeEndArray();
        
        gen.writeFieldName('datasets');
        gen.writeStartArray();
        gen.writeString(Power_Bi_Requests__c.getInstance('PowerBi').datasetId__c);
        gen.writeEndArray();
       
        gen.writeEndObject();
        gen.writeEndArray();
        
        gen.writeEndObject();
        jsonData = gen.getAsString();
        
        System.debug('JSON String');
        System.debug(jsonData);
        return jsonData;
    }
    
    
    
    
    
}