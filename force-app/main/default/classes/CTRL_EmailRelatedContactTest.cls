@isTest
public class CTRL_EmailRelatedContactTest {
    
    @isTest static void test_method_one(){
        
        Contact cnt            = TestDataFactory.myContact();
        Attachment att         = TestDataFactory.myAttachment();
        Account acc            = TestDataFactory.myClient();
        String statementType   = 'Test Statement';
        List<String> toAddress = new List<String>{'dayeniwear@gmail.com'};
        String ccAddress       = '';
        Id statementId         = att.Id;
        Id customerId          = acc.Id;
        Id parentId            = att.ParentId;
        String customerName    = acc.Name;
        String fileName        = 'Test File';
        String base64Data      = 'aabbcc';
        String contentType     = 'application/pdf';
        
        String custName        = cnt.LastName;
       
        String clientTestId    = cnt.id;
        Id fileId              = att.Id;
        
        Test.startTest();
            
        CTRL_EmailRelatedContact.getRelatedContacts(clientTestId);
        CTRL_EmailRelatedContact.generateStatement(clientTestId, custName, statementType);
        CTRL_EmailRelatedContact.sendStatement(customerId, toAddress, ccAddress, statementId);
        CTRL_EmailRelatedContact.getStatement(clientTestId, customerName, statementType);
        CTRL_EmailRelatedContact.getUserEmail();
        CTRL_EmailRelatedContact.saveChunk(parentId, fileName, base64Data, contentType, fileId);
        CTRL_EmailRelatedContact.appendToFile(fileId, base64Data);
        CTRL_EmailRelatedContact.saveTheFile(parentId, fileName, base64Data, contentType);
        
         system.assertNotEquals(null,CTRL_EmailRelatedContact.getRelatedContacts(clientTestId));
         system.assertNotEquals(null,CTRL_EmailRelatedContact.generateStatement(clientTestId, custName, statementType));
         system.assertNotEquals(null, CTRL_EmailRelatedContact.sendStatement(customerId, toAddress, ccAddress, statementId));
         system.assertNotEquals(null, CTRL_EmailRelatedContact.getStatement(clientTestId, customerName, statementType)); 
         system.assertNotEquals(null,CTRL_EmailRelatedContact.getUserEmail());
         system.assertNotEquals(null, CTRL_EmailRelatedContact.saveChunk(parentId, fileName, base64Data, contentType, fileId));
         system.assertNotEquals(null,CTRL_EmailRelatedContact.getRelatedContacts(clientTestId));
        
        Test.stopTest();
    }
  }