@RestResource(urlMapping='/SOandAllTecexUsers/*')
Global class NCPSOandAllTecexUsers {
    
    @Httppost
        global static void getRelatedContacts(){
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        res.addHeader('Content-Type', 'application/json');
        Blob body = req.requestBody;
        String requestString = body.toString();
        RequestWrapper reqBody;
        try {
            reqBody = (RequestWrapper)JSON.deserialize(requestString,RequestWrapper.class);
            List<Access_token__c> at = [SELECT status__c,Access_Token__c FROM Access_token__c WHERE Access_Token__c =: reqBody.Accesstoken and status__c = 'Active' LIMIT 1];
            
            if(!at.isEmpty()) {
                if(String.isBlank(reqBody.AccountID)) {
                    res.responseBody = Blob.valueOf(Util.getResponseString('Error','Invalid Account id.'));
                    res.statusCode = 400;
                }
                
                else{
                    Set<ID> CONIDS = new Set<ID>();
                    Set<ID> SOTecexpeople = new Set<ID>();
                    Set<ID> ACTecexpeople = new Set<ID>();
                    Map<ID,String> SOFields = new map<ID,string>();
                    Map<ID,String> ConFields = new map<ID,string>();
                   // MAP<string,String> StandByUserID = new Map<string,string>();
                    
                    String type='Account';
                    String Stype='Shipment_Order__c';
                    
                    Map<String, Schema.SObjectType> cschemaMap = Schema.getGlobalDescribe();
                    Schema.SObjectType cleadSchema = cschemaMap.get(type);
                    Map<String, Schema.SObjectField> ContactfieldMap = cleadSchema.getDescribe().fields.getMap();
                    
                    Map<String, Schema.SObjectType> SOschemaMap = Schema.getGlobalDescribe();
                    Schema.SObjectType SOleadSchema = SOschemaMap.get(Stype);
                    Map<String, Schema.SObjectField> SOfieldMap = SoleadSchema.getDescribe().fields.getMap();
                    
                    
                    List<Contact> contacts = [SELECT Id,Include_in_SO_Communication__c,Account.Service_Manager__c,Account.Lead_AM__c,Account.CSE_IOR__c,Account.Financial_Manager__c,Account.Financial_Controller__c,Account.Compliance__c,Account.Operations_Manager__c,Account.Sales_Executive__c,
                                              Email,Name,Include_in_invoicing_emails__c ,Contact_Role__c  FROM Contact Where AccountId =:reqBody.AccountID];
                    
                    For(Integer i=0;contacts.size()>i;i++){
                        CONIDS.add(contacts[i].id);
                        ACTecexpeople.add(contacts[i].Account.Lead_AM__c);
                        ACTecexpeople.add(contacts[i].Account.CSE_IOR__c);
                        ACTecexpeople.add(contacts[i].Account.Financial_Manager__c);
                        ACTecexpeople.add(contacts[i].Account.Financial_Controller__c);
                        ACTecexpeople.add(contacts[i]. Account.Operations_Manager__c);
                        ACTecexpeople.add(contacts[i].Account.Sales_Executive__c);
                        ACTecexpeople.add(contacts[i].Account.Compliance__c);
                        ACTecexpeople.add(contacts[i].Account.Service_Manager__c);
                        
                        //ConFields.put (contacts[i].Account.Service_Manager__c,ContactfieldMap.get('Service_Manager__c').getDescribe().getLabel());
                        ConFields.put (contacts[i].Account.Lead_AM__c,ContactfieldMap.get('Lead_AM__c').getDescribe().getLabel());
                        ConFields.put (contacts[i].Account.CSE_IOR__c,ContactfieldMap.get('CSE_IOR__c').getDescribe().getLabel());
                        ConFields.put (contacts[i].Account.Financial_Manager__c,ContactfieldMap.get('Financial_Manager__c').getDescribe().getLabel());
                        ConFields.put (contacts[i].Account.Financial_Controller__c,ContactfieldMap.get('Financial_Controller__c').getDescribe().getLabel());
                        ConFields.put (contacts[i].Account.Operations_Manager__c,ContactfieldMap.get('Operations_Manager__c').getDescribe().getLabel());
                        ConFields.put (contacts[i].Account.Sales_Executive__c,ContactfieldMap.get('Sales_Executive__c').getDescribe().getLabel());
                        ConFields.put (contacts[i].Account.Compliance__c,ContactfieldMap.get('Compliance__c').getDescribe().getLabel());
                        ConFields.put (contacts[i].Account.Service_Manager__c,ContactfieldMap.get('Service_Manager__c').getDescribe().getLabel());
                        
                        
                    }
                    
                    List<Shipment_order__C> SOL = [Select Id,Operations_Manager__c,Service_Manager__c,Lead_AM__c,IOR_CSE__c,Financial_Manager__c,Financial_Controller__c,Compliance_Team__c from Shipment_Order__c where Id=:reqbody.SOID];
                    
                    //System.debug('SOL');
                    //  System.debug(JSON.serializePretty(SOL));
                    
                    For(Integer i=0;SOL.size()>i;i++){
                        
                        
                        SOTecexpeople.add(SOL[i].Lead_AM__c);
                        SOTecexpeople.add(SOL[i].IOR_CSE__c);
                        SOTecexpeople.add(SOL[i].Financial_Manager__c);
                        SOTecexpeople.add(SOL[i].Financial_Controller__c);
                        SOTecexpeople.add(SOL[i].Compliance_Team__c);
                        SOTecexpeople.add(SOL[i].Operations_Manager__c);
                        SOTecexpeople.add(SOL[i].Service_Manager__c);
                        
                        
                        SOFields.put (SOL[i].Lead_AM__c,SOfieldMap.get('Lead_AM__c').getDescribe().getLabel());
                        SOFields.put (SOL[i].IOR_CSE__c,SOfieldMap.get('IOR_CSE__c').getDescribe().getLabel());
                        SOFields.put (SOL[i].Financial_Manager__c,SOfieldMap.get('Financial_Manager__c').getDescribe().getLabel());
                        SOFields.put (SOL[i].Financial_Controller__c,SOfieldMap.get('Financial_Controller__c').getDescribe().getLabel());
                        SOFields.put (SOL[i].Compliance_Team__c,SOfieldMap.get('Compliance_Team__c').getDescribe().getLabel());
                        SOFields.put (SOL[i].Operations_Manager__c,SOfieldMap.get('Operations_Manager__c').getDescribe().getLabel());
                        SOFields.put (SOL[i].Service_Manager__c,SOfieldMap.get('Service_Manager__c').getDescribe().getLabel());
                        
                    }
                    
                    List<user> AllUsers = [select Id,Stand_by_user__c, Stand_by_user__r.name,FirstName,Lastname,name,email,contact.Include_in_invoicing_emails__c,contact.Include_in_SO_Communication__c,contact.Contact_Role__c, LastLoginDate ,isactive,  ContactId , AccountId , FullPhotoUrl  from User where isactive=true and (Username like '%tecex%' or contactid in:CONIDS)  ];
                   // for(user us:Allusers){  StandByUserID.put(us.id,us.name);
                   //   }
                  // system.debug( StandByUserID.get('0051v000005XSSSAA4'));
                   //  System.debug(JSON.serializePretty(StandByUserID));
                    
                    Map<String,OutOfOffice> UsersOutOfOffice = new  Map<String,OutOfOffice>();
                    
                    for(OutOfOffice outof:[SELECT Id, UserId, IsEnabled, Message, StartDate, EndDate FROM OutOfOffice where UserId in :AllUsers]){
                        UsersOutOfOffice.put(outof.UserId,outof);
                    }
                    //  System.debug('AllUsers');
                    // System.debug(JSON.serializePretty(AllUsers));
                    
                    //  System.debug('SOTecexpeople');
                    //  System.debug(JSON.serializePretty(SOTecexpeople));
                    JSONGenerator gen = JSON.createGenerator(true);
                    gen.writeStartObject();
                    
                    
                    //Creating Array & Object - Client Defaults
                    gen.writeFieldName('ClientUsers');
                    gen.writeStartArray();
                    For(Integer i=0;AllUsers.size()>i;i++ ){
                         If(CONIDS.contains(AllUsers[i].ContactId)){
                            //Start of Object - SClient Defaults	
                            gen.writeStartObject();
                            OutOfOffice outof;
                            if(UsersOutOfOffice.containsKey(AllUsers[i].ID) ){
                                outof = UsersOutOfOffice.get(AllUsers[i].ID);
                            }
                            if(AllUsers[i].ID == null) {gen.writeNullField('UserID');} else{gen.writeStringField('UserID', AllUsers[i].ID);}
                            if(outof != null){
                                if(outof.IsEnabled == null) {gen.writeNullField('OutOfOfficeIsEnabled');} else{gen.writeBooleanField('OutOfOfficeIsEnabled', outof.IsEnabled);}
                                if(outof.Message == null) {gen.writeNullField('OutOfOfficeMessage');} else{gen.writeStringField('OutOfOfficeMessage', outof.Message);}
                                if(outof.StartDate == null) {gen.writeNullField('OutOfOfficeStartDate');} else{gen.writeDatetimeField('OutOfOfficeStartDate', outof.StartDate);}
                                if(outof.EndDate == null) {gen.writeNullField('OutOfOfficeEndDate');} else{gen.writeDatetimeField('OutOfOfficeEndDate',outof.EndDate);}
                                if(AllUsers[i].Stand_by_user__c == null) {gen.writeNullField('StandByUserID');} else{gen.writeStringField('StandbyUserID',AllUsers[i].Stand_by_user__c);}
                                if(AllUsers[i].Stand_by_user__c == null) {gen.writeNullField('StandByUserName');} else{gen.writeStringField('StandbyUserName',AllUsers[i].Stand_by_user__r.name);}
                                
                            }
                            if(AllUsers[i].FirstName == null) {gen.writeNullField('FirstName');} else{gen.writeStringField('FirstName', AllUsers[i].FirstName);}
                            if(AllUsers[i].LastName == null) {gen.writeNullField('LastName');} else{gen.writeStringField('LastName', AllUsers[i].LastName);}
                            if(AllUsers[i].name == null) {gen.writeNullField('Username');} else{gen.writeStringField('Username', AllUsers[i].name);}
                            if(AllUsers[i].email == null) {gen.writeNullField('Useremail');} else{gen.writeStringField('Useremail', AllUsers[i].email);}
                            if(AllUsers[i].contact.Include_in_invoicing_emails__c == null) {gen.writeNullField('Includeininvoicingemails');} else{gen.writeBooleanField('Includeininvoicingemails', AllUsers[i].contact.Include_in_invoicing_emails__c);}
                            if(AllUsers[i].contact.Include_in_SO_Communication__c == null) {gen.writeNullField('IncludeinSOCommunication');} else{gen.writeBooleanField('IncludeinSOCommunication', AllUsers[i].contact.Include_in_SO_Communication__c);}
                            
                             if(AllUsers[i].contact.Contact_Role__c == null) {gen.writeNullField('ContactRole');} else{gen.writeStringField('ContactRole', AllUsers[i].contact.Contact_Role__c);}
                            if(AllUsers[i].ContactId == null) {gen.writeNullField('ContactId');} else{gen.writeStringField('ContactId', AllUsers[i].ContactId);}
                            if(AllUsers[i].AccountId == null) {gen.writeNullField('AccountId');} else{gen.writeStringField('AccountId', AllUsers[i].AccountId);}
                            if(AllUsers[i].FullPhotoUrl == null) {gen.writeNullField('FullPhotoUrl');} else{gen.writeStringField('FullPhotoUrl', AllUsers[i].FullPhotoUrl);}
                            if(AllUsers[i].LastLoginDate == null) {gen.writeNullField('LastLoginDate');} else{gen.writeDatetimeField('LastLoginDate', AllUsers[i].LastLoginDate);}
                            
                            
                            gen.writeEndObject();
                            //End of Object - Client Defaults
                        }
                    }
                    gen.writeEndArray();
                    //End of Array - Client Defaults 
                    
                    ////Creating Array & Object - SOTecexPeople
                    gen.writeFieldName('SOTecexPeople');
                    gen.writeStartArray();
                    
                    
                    For(Integer i=0;AllUsers.size()>i;i++ ){
                        If(SOTecexpeople.contains(AllUsers[i].ID)){
                            //Start of Object - SOTecexPeople
                            OutOfOffice outof;
                            if(UsersOutOfOffice.containsKey(AllUsers[i].ID) ){
                                outof = UsersOutOfOffice.get(AllUsers[i].ID);
                            }
                            
                            gen.writeStartObject();
                            if(AllUsers[i].ID == null) {gen.writeNullField('UserID');} else{gen.writeStringField('UserID', AllUsers[i].ID);}
                            if(outof != null){
                                if(outof.IsEnabled == null) {gen.writeNullField('OutOfOfficeIsEnabled');} else{gen.writeBooleanField('OutOfOfficeIsEnabled', outof.IsEnabled);}
                                if(outof.Message == null) {gen.writeNullField('OutOfOfficeMessage');} else{gen.writeStringField('OutOfOfficeMessage', outof.Message);}
                                if(outof.StartDate == null) {gen.writeNullField('OutOfOfficeStartDate');} else{gen.writeDatetimeField('OutOfOfficeStartDate', outof.StartDate);}
                                if(outof.EndDate == null) {gen.writeNullField('OutOfOfficeEndDate');} else{gen.writeDatetimeField('OutOfOfficeEndDate',outof.EndDate);}
                             	//if(AllUsers[i].StandByUserID__c == null) {gen.writeNullField('StandByUserID');} else{gen.writeStringField('StandbyUserID',AllUsers[i].StandByUserID__c);}
                               // if(AllUsers[i].StandByUserID__c == null) {gen.writeNullField('StandByUserName');} else{gen.writeStringField('StandbyUserName',StandByUserID.get(AllUsers[i].StandByUserID__c));}
                                if(AllUsers[i].Stand_by_user__c == null) {gen.writeNullField('StandByUserID');} else{gen.writeStringField('StandbyUserID',AllUsers[i].Stand_by_user__c);}
                                if(AllUsers[i].Stand_by_user__c == null) {gen.writeNullField('StandByUserName');} else{gen.writeStringField('StandbyUserName',AllUsers[i].Stand_by_user__r.name);}
                               
                            }
                            if(AllUsers[i].ID == null) {gen.writeNullField('Role');} else{gen.writeStringField('Role',SOFields.get(AllUsers[i].ID));}
                            if(AllUsers[i].FirstName == null) {gen.writeNullField('FirstName');} else{gen.writeStringField('FirstName', AllUsers[i].FirstName);}
                            if(AllUsers[i].LastName == null) {gen.writeNullField('LastName');} else{gen.writeStringField('LastName', AllUsers[i].LastName);}
                            if(AllUsers[i].name == null) {gen.writeNullField('Username');} else{gen.writeStringField('Username', AllUsers[i].name);}
                            if(AllUsers[i].email == null) {gen.writeNullField('Useremail');} else{gen.writeStringField('Useremail', AllUsers[i].email);}
                            if(AllUsers[i].contact.Include_in_invoicing_emails__c == null) {gen.writeNullField('Includeininvoicingemails');} else{gen.writeBooleanField('Includeininvoicingemails', AllUsers[i].contact.Include_in_invoicing_emails__c);}
                           if(AllUsers[i].contact.Include_in_SO_Communication__c == null) {gen.writeNullField('IncludeinSOCommunication');} else{gen.writeBooleanField('IncludeinSOCommunication', AllUsers[i].contact.Include_in_SO_Communication__c);}
                            
                            if(AllUsers[i].contact.Contact_Role__c == null) {gen.writeNullField('ContactRole');} else{gen.writeStringField('ContactRole', AllUsers[i].contact.Contact_Role__c);}
                            if(AllUsers[i].ContactId == null) {gen.writeNullField('ContactId');} else{gen.writeStringField('ContactId', AllUsers[i].ContactId);}
                            if(AllUsers[i].AccountId == null) {gen.writeNullField('AccountId');} else{gen.writeStringField('AccountId', AllUsers[i].AccountId);}
                            if(AllUsers[i].FullPhotoUrl == null) {gen.writeNullField('FullPhotoUrl');} else{gen.writeStringField('FullPhotoUrl', AllUsers[i].FullPhotoUrl);}
                            if(AllUsers[i].LastLoginDate == null) {gen.writeNullField('LastLoginDate');} else{gen.writeDatetimeField('LastLoginDate', AllUsers[i].LastLoginDate);}
                            gen.writeEndObject();
                            //End of Object - SOTecexPeople
                        }
                        
                    }
                    
                    
                    
                    
                    gen.writeEndArray();
                    //End of Array -  SOTecexPeople
                    //
                    //////Creating Array & Object - ACTecexPeople
                    gen.writeFieldName('ACTecexPeople');
                    gen.writeStartArray();
                    
                    
                    For(Integer i=0;AllUsers.size()>i;i++ ){
                        If(ACTecexpeople.contains(AllUsers[i].ID)){
                            //Start of Object - AcTecexPeople
                            OutOfOffice outof;
                            if(UsersOutOfOffice.containsKey(AllUsers[i].ID) ){
                                outof = UsersOutOfOffice.get(AllUsers[i].ID);
                            }
                            
                            gen.writeStartObject();
                            if(AllUsers[i].ID == null) {gen.writeNullField('UserID');} else{gen.writeStringField('UserID', AllUsers[i].ID);}
                            if(outof != null){
                                if(outof.IsEnabled == null) {gen.writeNullField('OutOfOfficeIsEnabled');} else{gen.writeBooleanField('OutOfOfficeIsEnabled', outof.IsEnabled);}
                                if(outof.Message == null) {gen.writeNullField('OutOfOfficeMessage');} else{gen.writeStringField('OutOfOfficeMessage', outof.Message);}
                                if(outof.StartDate == null) {gen.writeNullField('OutOfOfficeStartDate');} else{gen.writeDatetimeField('OutOfOfficeStartDate', outof.StartDate);}
                                if(outof.EndDate == null) {gen.writeNullField('OutOfOfficeEndDate');} else{gen.writeDatetimeField('OutOfOfficeEndDate',outof.EndDate);}
                           		 if(AllUsers[i].Stand_by_user__c == null) {gen.writeNullField('StandByUserID');} else{gen.writeStringField('StandbyUserID',AllUsers[i].Stand_by_user__c);}
                                if(AllUsers[i].Stand_by_user__c == null) {gen.writeNullField('StandByUserName');} else{gen.writeStringField('StandbyUserName',AllUsers[i].Stand_by_user__r.name);}
                               
                            
                            }
                            if(AllUsers[i].ID == null) {gen.writeNullField('Role');} else{gen.writeStringField('Role',ConFields.get(AllUsers[i].ID));}
                            if(AllUsers[i].FirstName == null) {gen.writeNullField('FirstName');} else{gen.writeStringField('FirstName', AllUsers[i].FirstName);}
                            if(AllUsers[i].LastName == null) {gen.writeNullField('LastName');} else{gen.writeStringField('LastName', AllUsers[i].LastName);}
                            if(AllUsers[i].name == null) {gen.writeNullField('Username');} else{gen.writeStringField('Username', AllUsers[i].name);}
                            if(AllUsers[i].email == null) {gen.writeNullField('Useremail');} else{gen.writeStringField('Useremail', AllUsers[i].email);}
                            if(AllUsers[i].contact.Include_in_invoicing_emails__c == null) {gen.writeNullField('Includeininvoicingemails');} else{gen.writeBooleanField('Includeininvoicingemails', AllUsers[i].contact.Include_in_invoicing_emails__c);}
                           if(AllUsers[i].contact.Include_in_SO_Communication__c == null) {gen.writeNullField('IncludeinSOCommunication');} else{gen.writeBooleanField('IncludeinSOCommunication', AllUsers[i].contact.Include_in_SO_Communication__c);}
                            
                            if(AllUsers[i].contact.Contact_Role__c == null) {gen.writeNullField('ContactRole');} else{gen.writeStringField('ContactRole', AllUsers[i].contact.Contact_Role__c);}
                            if(AllUsers[i].ContactId == null) {gen.writeNullField('ContactId');} else{gen.writeStringField('ContactId', AllUsers[i].ContactId);}
                            if(AllUsers[i].AccountId == null) {gen.writeNullField('AccountId');} else{gen.writeStringField('AccountId', AllUsers[i].AccountId);}
                            if(AllUsers[i].FullPhotoUrl == null) {gen.writeNullField('FullPhotoUrl');} else{gen.writeStringField('FullPhotoUrl', AllUsers[i].FullPhotoUrl);}
                            if(AllUsers[i].LastLoginDate == null) {gen.writeNullField('LastLoginDate');} else{gen.writeDatetimeField('LastLoginDate', AllUsers[i].LastLoginDate);}
                            gen.writeEndObject();
                            //End of Object - AcTecexPeople
                        }
                        
                    }
                    
                    
                    
                    
                    gen.writeEndArray();
                    //End of Array -  AcTecexPeople
                    
                    ////Creating Array & Object - AllTecexPeople
                    gen.writeFieldName('AllTecexPeople');
                    gen.writeStartArray();
                    
                    
                    For(Integer i=0;AllUsers.size()>i;i++ ){
                        If(!SOTecexpeople.contains(AllUsers[i].ID) && !CONIDS.contains(AllUsers[i].ContactId) &&!AcTecexpeople.contains(AllUsers[i].ID)){
                            //Start of Object - AllTecexPeople
                            OutOfOffice outof;
                            if(UsersOutOfOffice.containsKey(AllUsers[i].ID) ){
                                outof = UsersOutOfOffice.get(AllUsers[i].ID);
                            }
                            
                            gen.writeStartObject();
                            if(AllUsers[i].ID == null) {gen.writeNullField('UserID');} else{gen.writeStringField('UserID', AllUsers[i].ID);}
                            if(outof != null){
                                if(outof.IsEnabled == null) {gen.writeNullField('OutOfOfficeIsEnabled');} else{gen.writeBooleanField('OutOfOfficeIsEnabled', outof.IsEnabled);}
                                if(outof.Message == null) {gen.writeNullField('OutOfOfficeMessage');} else{gen.writeStringField('OutOfOfficeMessage', outof.Message);}
                                if(outof.StartDate == null) {gen.writeNullField('OutOfOfficeStartDate');} else{gen.writeDatetimeField('OutOfOfficeStartDate', outof.StartDate);}
                                if(outof.EndDate == null) {gen.writeNullField('OutOfOfficeEndDate');} else{gen.writeDatetimeField('OutOfOfficeEndDate',outof.EndDate);}
                                if(AllUsers[i].Stand_by_user__c == null) {gen.writeNullField('StandByUserID');} else{gen.writeStringField('StandbyUserID',AllUsers[i].Stand_by_user__c);}
                                if(AllUsers[i].Stand_by_user__c == null) {gen.writeNullField('StandByUserName');} else{gen.writeStringField('StandbyUserName',AllUsers[i].Stand_by_user__r.name);}
                               
                            }
                            if(AllUsers[i].FirstName == null) {gen.writeNullField('FirstName');} else{gen.writeStringField('FirstName', AllUsers[i].FirstName);}
                            if(AllUsers[i].LastName == null) {gen.writeNullField('LastName');} else{gen.writeStringField('LastName', AllUsers[i].LastName);}
                            if(AllUsers[i].name == null) {gen.writeNullField('Username');} else{gen.writeStringField('Username', AllUsers[i].name);}
                            if(AllUsers[i].email == null) {gen.writeNullField('Useremail');} else{gen.writeStringField('Useremail', AllUsers[i].email);}
                            if(AllUsers[i].contact.Include_in_invoicing_emails__c == null) {gen.writeNullField('Includeininvoicingemails');} else{gen.writeBooleanField('Includeininvoicingemails', AllUsers[i].contact.Include_in_invoicing_emails__c);}
                           if(AllUsers[i].contact.Include_in_SO_Communication__c == null) {gen.writeNullField('IncludeinSOCommunication');} else{gen.writeBooleanField('IncludeinSOCommunication', AllUsers[i].contact.Include_in_SO_Communication__c);}
                            
                            if(AllUsers[i].contact.Contact_Role__c == null) {gen.writeNullField('ContactRole');} else{gen.writeStringField('ContactRole', AllUsers[i].contact.Contact_Role__c);}
                            if(AllUsers[i].ContactId == null) {gen.writeNullField('ContactId');} else{gen.writeStringField('ContactId', AllUsers[i].ContactId);}
                            if(AllUsers[i].AccountId == null) {gen.writeNullField('AccountId');} else{gen.writeStringField('AccountId', AllUsers[i].AccountId);}
                            if(AllUsers[i].FullPhotoUrl == null) {gen.writeNullField('FullPhotoUrl');} else{gen.writeStringField('FullPhotoUrl', AllUsers[i].FullPhotoUrl);}
                            if(AllUsers[i].LastLoginDate == null) {gen.writeNullField('LastLoginDate');} else{gen.writeDatetimeField('LastLoginDate', AllUsers[i].LastLoginDate);}
                            gen.writeEndObject();
                            //End of Object - SOTecexPeople
                        }
                        
                    }
                    
                    gen.writeEndArray();
                    //End of Array -  SOTecexPeople
                    
                    gen.writeEndObject();
                    String jsonData = gen.getAsString();
                    
                    // res.responseBody = Blob.valueOf(JSON.serializePretty(AllUsers));
                    res.responseBody = Blob.valueOf(jsonData);
                    res.statusCode = 200;
                    
                    insert new API_Log__c(
                        Account__c = reqBody.AccountID,
                        EndpointURLName__c = 'NCPAccountRelatedContacts',
                        Response__c='Success - Related Contacts sent for this account '+reqBody.AccountID,
                        StatusCode__c=string.valueof(res.statusCode)
                    );
                }
            }
            else{
                res.responseBody = Blob.valueOf(Util.getResponseString('Error','Access Token is invalid or expired.'));
                res.statusCode = 400;
                insert new API_Log__c(
                    Account__c = String.isNotBlank(reqBody.AccountID) ? reqBody.AccountID : null,
                    EndpointURLName__c = 'SOandAllTecexUsers',
                    Response__c='Error - Invalid AccessToken',
                    StatusCode__c=string.valueof(res.statusCode)
                );
            }
        }catch(Exception e) {
          //  String jsonData = Util.getResponseString('Error','Something went wrong while sending related contacts of Account, please contact support_SF@tecex.com');
            String jsonData= e.getLineNumber()+e.getStackTraceString()+e.getMessage();
            res.responseBody = Blob.valueOf(jsonData);
            res.statusCode = 500;
            insert new API_Log__c(
                Account__c = reqBody != null && String.isNotBlank(reqBody.AccountID) ? reqBody.AccountID : null,
                EndpointURLName__c = 'SOandAllTecexUsers',
                Response__c = 'Error - SOandAllTecexUsers : '+e.getMessage()+e.getLineNumber(),
                StatusCode__c = string.valueof(res.statusCode)
            );
        }
    }
    
    public class RequestWrapper {
        public String Accesstoken;
        public String AccountID;
        public String SOID;
    }
    
    
}