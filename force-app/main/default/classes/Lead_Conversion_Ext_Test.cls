/*****************************************************************************************************\
@ Func Area     : Leads
@ Date          : 12/2015
@ Class testing : Lead_Conversion_Ext  
@ Additional    :    
-----------------------------------------------------------------------------
@ Last Modified By  : 
@ Last Modified On  :   
@ Last Modified Reason  : 
----------------------------------------------------------------------------- 
******************************************************************************************************/
@isTest
private class Lead_Conversion_Ext_Test {
    
    //Variables needed for lead limits
    private static final Integer SMALL_LIMIT = 10;
    private static final Integer LARGE_LIMIT = 110;	
    
    //Test Lead the is ready to convert
    /*@isTest static void test_Positve_TestCase() {
        //Running user needed for SF lead limits
        User thisUser = [ Select Id from User where Id = :UserInfo.getUserId() ];
        
        System.runAs( thisUser ){
            
            UTIL_GlobalVariableManifest.LeadLimit = SMALL_LIMIT - 5; 
        }
        
        //Test Lead
        Lead testLead = new Lead ();
        testLead.LastName = 'Binks';
        testLead.Company = 'All of it';
        testLead.Country = 'South Africa';
        testLead.Email = 'jorel@vatit.com';
        testLead.CSE_New__c = UserInfo.getUserId();
        testLead.Sales_Executive__c = UserInfo.getUserId();
        testLead.Street = 'Test';
        testLead.City = 'Test';
        testLead.Rates_Approved__c = True;
        testLead.Re_issue_Fee__c = 0.5;
        testLead.Restyle_Fee__c = 0.1;
        testLead.Date_Signed__c = System.Today();
        testLead.Contract_Expiration_Date__c = System.Today() + 300;
        insert testLead;
        
        //Test Attachment
        Attachment testAttachment = new Attachment();     
        testAttachment.Name='Test Attachment';
        Blob bodyBlob=Blob.valueOf('THE BLOB');
        testAttachment.body=bodyBlob;
        testAttachment.parentId=testLead.id;
        insert testAttachment;
        
        //Standard controller and extension instance to test
        Test.startTest();
        ApexPages.StandardController sc = new ApexPages.StandardController(testLead);
        Lead_Conversion_Ext testLeadConvert = new Lead_Conversion_Ext(sc);
        
        PageReference pageRef = Page.CustomLeadConversion_VF;
        pageRef.getParameters().put('id', String.valueOf(testLead.Id));
        Test.setCurrentPage(pageRef);
        
        PageReference testPageRedirect = testLeadConvert.leadConversionCheck();
        Test.stopTest();
        
        System.assertNotEquals(null, testPageRedirect);
        
    }
    
    //Test missing lead values
    @isTest static void test_Negative_TestCase() {
        //Running user needed for SF lead limits
        User thisUser = [ Select Id from User where Id = :UserInfo.getUserId() ];
        
        System.runAs( thisUser ){
            
            UTIL_GlobalVariableManifest.LeadLimit = SMALL_LIMIT - 5; 
        }
        
        //Test Lead
        Lead testLead = new Lead ();
        testLead.LastName = 'Binks';
        testLead.Company = 'All of it';
        testLead.Country = 'South Africa';
        testLead.Email = 'jorel@vatit.com';
        testLead.CSE_New__c = UserInfo.getUserId();
        testLead.Sales_Executive__c = UserInfo.getUserId();
        testLead.Street = 'Test';
        testLead.City = 'Test';
        testLead.Rates_Approved__c = True;
        testLead.Re_issue_Fee__c = 0.5;
        testLead.Restyle_Fee__c = 0.1;
        testLead.Contract_Expiration_Date__c = System.Today() + 300;
        insert testLead;
        
        //Test Attachment
        Attachment testAttachment = new Attachment();     
        testAttachment.Name = 'Test Attachment';
        Blob bodyBlob = Blob.valueOf('THE BLOB');
        testAttachment.body = bodyBlob;
        testAttachment.parentId = testLead.id;
        insert testAttachment;
        
        //Standard controller and extension instance to test
        Test.startTest();
        ApexPages.StandardController sc = new ApexPages.StandardController(testLead);
        Lead_Conversion_Ext testLeadConvert = new Lead_Conversion_Ext(sc);
        
        PageReference pageRef = Page.CustomLeadConversion_VF;
        pageRef.getParameters().put('id', String.valueOf(testLead.Id));
        Test.setCurrentPage(pageRef);
        
        PageReference testPageRedirect = testLeadConvert.leadConversionCheck();
        Test.stopTest();
        
        //System.assertEquals(null,testPageRedirect);
    }
    
    //No Attachment test
    @isTest static void test_Negative_TestCase2() {
        //Running user needed for SF lead limits
        User thisUser = [ Select Id from User where Id = :UserInfo.getUserId() ];
        
        System.runAs( thisUser ){
            UTIL_GlobalVariableManifest.LeadLimit = SMALL_LIMIT - 5; 
        }
        //Test Lead
        Lead testLead = new Lead ();
        testLead.LastName = 'Binks';
        testLead.Company = 'All of it';
        testLead.Country = 'South Africa';
        testLead.Email = 'jorel@vatit.com';
        testLead.CSE_New__c = UserInfo.getUserId();
        testLead.Sales_Executive__c = UserInfo.getUserId();
        testLead.Street = 'Test';
        testLead.City = 'Test';
        testLead.Rates_Approved__c = True;
        testLead.Re_issue_Fee__c = 0.5;
        testLead.Restyle_Fee__c = 0.1;
        testLead.Date_Signed__c = System.Today();
        testLead.Contract_Expiration_Date__c = System.Today() + 300;
        insert testLead;
        
        //Standard controller and extension instance to test        
        Test.startTest();
        ApexPages.StandardController sc = new ApexPages.StandardController(testLead);
        Lead_Conversion_Ext testLeadConvert = new Lead_Conversion_Ext(sc);
        
        PageReference pageRef = Page.CustomLeadConversion_VF;
        pageRef.getParameters().put('id', String.valueOf(testLead.Id));
        Test.setCurrentPage(pageRef);
        
        PageReference testPageRedirect = testLeadConvert.leadConversionCheck();
        Test.stopTest();
        
        //System.assertEquals(null,testPageRedirect);
    }*/
    
}