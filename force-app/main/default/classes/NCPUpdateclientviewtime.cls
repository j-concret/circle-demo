@RestResource(urlMapping='/NCPUpdateclientviewtime/*')
Global class NCPUpdateclientviewtime {
    
     @Httppost
      global static void NCPUpdateCase(){
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        Blob body = req.requestBody;
    	res.addHeader('Content-Type', 'application/json');
        String requestString = body.toString();
        NCPUpdateclientviewtimeWra rw  = (NCPUpdateclientviewtimeWra)JSON.deserialize(requestString,NCPUpdateclientviewtimeWra.class);
          Try{
              
              Access_token__c At =[select status__c,Access_Token__c from Access_token__c where Access_Token__c=:rw.Accesstoken ];
            	if( at.status__C =='Active' ){
                        
                    if(rw.RecordID.left(3)=='500'){
                    
                       Case cs = [Select ID,Last_Client_viewed_time__c from case where Id=:rw.RecordID limit 1  ];
                       cs.Last_Client_viewed_time__c =datetime.now();
                       Update cs;
                        
                    }
                    if(rw.RecordID.left(3)=='00T'){
                    
                       Task cs = [Select ID,Last_Client_viewed_time__c from task where Id=:rw.RecordID limit 1  ];
                       cs.Last_Client_viewed_time__c =datetime.now();
                       Update cs;
                        
                    }
                    
                    
                    
               
                  	JSONGenerator gen = JSON.createGenerator(true);
            		gen.writeStartObject();
            
             		gen.writeFieldName('Success');
             		gen.writeStartObject();
            
             			gen.writeStringField('Response','Record updated successfully.');
            
            		gen.writeEndObject();
            		gen.writeEndObject();
            		String jsonData = gen.getAsString();
            		res.responseBody = Blob.valueOf(jsonData);
            		res.addHeader('Content-Type', 'application/json');
            		res.statusCode = 200;    
                    API_Log__c Al = New API_Log__c();
                    Al.EndpointURLName__c='NCPUpdateclientviewtime';
                    Al.StatusCode__c=string.valueof(res.statusCode);
                    Al.Response__c = 'Updateclientviewtime time updated' ;
                    Insert Al;
                
                  
                  
                  
              }        
          }
          Catch(Exception e){
              
              String ErrorString ='Something went wrong, please contact Sfsupport@tecex.com';
               		res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
            		res.addHeader('Content-Type', 'application/json');
        			res.statusCode = 400;
                
                API_Log__c Al = New API_Log__c();
               // Al.Account__c=rw.AccountID;
                //Al.Login_Contact__c=rw.contactId;
                Al.EndpointURLName__c='NCPUpdateclientviewtime';
                Al.Response__c=e.getMessage()+' at line number: '+e.getLineNumber();
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al;
                
              
              
          }



      }

}