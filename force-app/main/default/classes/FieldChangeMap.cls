public virtual class FieldChangeMap {
	private Object oldValue;
	private Object newValue;
	private FieldDefinition fField;
	protected FieldDefinition Field { get { return fField; } }

	protected virtual String getValue(Object value) {
		if (value == null) return null;
		
		if (Field.Field.getSOAPType() == Schema.Soaptype.Date) 
			return ((datetime)value).formatGmt(XF_Dragon_Request.XML_DATE_FORMAT);
		else if (Field.Field.getSOAPType() == Schema.Soaptype.DateTime) 
			return ((datetime)value).formatGmt(XF_Dragon_Request.XML_DATE_TIME_FORMAT);  
	    else
		  	return String.valueOf(value);
	}
	
	public String fieldName {
		get { return Field.Field.getLocalName(); } 
	}
	public String XMLOldValue {
		get { return getValue(oldValue); }
	}
	public String XMLNewValue {
		get { return getValue(newValue); }
	}
	
	public FieldChangeMap() {} // needed for newInstance creation
	
    public FieldChangeMap Initialize(SObject newItem, FieldDefinition fieldDef) {
		fField = fieldDef;
		newValue = newItem.get(fieldName);
        return this;
    }
    
    public FieldChangeMap Initialize(SObject oldItem, SObject newItem, FieldDefinition fieldDef) {
		fField = fieldDef;
		oldValue = oldItem.get(fieldName);
		newValue = newItem.get(fieldName);
        System.debug('OldValue: ' + oldValue + ', New Value: ' + newValue + ', Field Name: ' + Field.Field.getLabel() + ', Field Type: ' + Field.Field.getType());
        return this;
    } 
}