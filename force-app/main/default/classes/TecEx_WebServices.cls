global class TecEx_WebServices {

  webservice static String sendEmailSummary(List<Shipment_Order__c> SOList, String ClientId, String newEmailAddress){
      Shipping_Order_Email_Summary.sendEmailSummary(null,ClientId,newEmailAddress);
      return 'Email Sent';
    }
}