@isTest
private class Chatter_Feed_Sync_Test {
    
    @isTest static void test_method_one() {
        
        Id currentUserId = UserInfo.getUserId();

        Account testClient = new account(Name='TestClient',Client_Status__c = 'Prospect',Estimated_Vat__c = 123, CSE_IOR__c = UserInfo.getUserId());
        insert testClient;

        IOR_Price_List__c testIOR = new IOR_Price_List__c();
            
          testIOR.Client_Name__c = testClient.Id;
            testIOR.Name = 'Tokyo';
            testIOR.IOR_Fee__c = 1;
        
        insert testIOR;

        Id soRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('TecEx').getRecordTypeId();

        Opportunity testOpp = new opportunity(Name='testOpp',AccountId = testClient.Id,IOR_Price_List__c = testIOR.Id, StageName ='Closed Won', closeDate = System.today(), recordTypeId = soRecordTypeId, Shipment_Value_USD__c = 12);
        insert testOpp;

        Shipment_Order__c testSO =  new Shipment_Order__c(Account__c = testClient.Id,IOR_Price_List__c = testIOR.Id, Shipment_Value_USD__c = 12);
        Insert testSO;
        
        FeedItem testPost = new FeedItem(ParentId = testOpp.Id,title = 'Test2',Body = 'Test');
        Insert testPost;

        FeedItem testPost2 = new FeedItem(ParentId = testSO.Id,title = 'Test',Body = 'Test');
        Insert testPost2;

        string before = 'Testing base 64 encode';            
        Blob beforeblob = Blob.valueOf(before);
        ContentVersion cv = new ContentVersion();
            cv.title = 'test content trigger';      
            cv.PathOnClient ='test';           
            cv.VersionData =beforeblob;          
        insert cv;         

        ContentVersion testContent = [SELECT id, ContentDocumentId FROM ContentVersion where Id = :cv.Id];

        Shipment_Order__c testSO2 = new Shipment_Order__c(Account__c = testClient.Id,IOR_Price_List__c = testIOR.Id, Shipment_Value_USD__c = 12);
        Insert testSO2;
        
        //[Select Id from Shipment_Order__c Limit 1];

        FeedItem feeditem = new FeedItem();
            feedItem.Type = 'ContentPost';
            feedItem.Body = 'Place holder file. Upload attachment using Upload new version.';
            feedItem.ContentData = beforeblob;
            feedItem.ContentFileName = 'Test File';
            feedItem.ParentId = testSO2.id;
        insert feedItem;

        System.debug ('SO ID'+testSO2.id);

        ContentDocumentLink newFileShare = new ContentDocumentLink();
            newFileShare.contentdocumentid = testcontent.contentdocumentid;
            newFileShare.LinkedEntityId = testSO2.id;
            newFileShare.ShareType= 'V';
        insert newFileShare;


    }
    
    
}