@RestResource(urlMapping='/RemoveNotifyparties/*')
Global class RemoveNotifyParties {
      @Httppost
    global static void RemoveNotifyParties(){
        
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        Blob body = req.requestBody;
        res.addHeader('Content-Type', 'application/json');
        String requestString = body.toString();
        RemoveNotifyPartieswra rw = (RemoveNotifyPartieswra)JSON.deserialize(requestString,RemoveNotifyPartieswra.class);
        
        try {
            String Message;
            Access_token__c At =[select status__c,Access_Token__c from Access_token__c where Access_Token__c=:rw.Accesstoken ];
            if( at.status__C =='Active' ){
              
                List<Notify_Parties__c> notifypartiestoremove = new list<Notify_Parties__c>();
                
               notifypartiestoremove=[SELECT Id, Contact__c, Shipment_Order__c, Name FROM Notify_Parties__c where id in:rw.notifyparties];
                if(notifypartiestoremove.isempty()){
                    message='There is no notifypaties found';
                }
                else{
                   delete notifypartiestoremove;
                 message='Notify parties are deleted';
                  } 
                 }
        
                
                
                JSONGenerator gen = JSON.createGenerator(true);
                gen.writeStartObject();
                
                gen.writeFieldName('Success');
                gen.writeStartObject();
                
                if(Message == null) {gen.writeNullField('Message');} else{gen.writeStringField('Message', +Message );}
                
                gen.writeEndObject();
                gen.writeEndObject();
                String jsonData = gen.getAsString();
                res.responseBody = Blob.valueOf(jsonData);
                res.addHeader('Content-Type', 'application/json');
                res.statusCode = 200;        
                
                API_Log__c Al = New API_Log__c();
                Al.EndpointURLName__c='RemoveNotifyParties';
                Al.Response__c= message;
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al; 
            }
        
        catch (Exception e){
            String ErrorString ='Something went wrong while updating cost estimate, please contact support_SF@tecex.com'+e.getMessage();
            res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
            res.addHeader('Content-Type', 'application/json');
            res.statusCode = 500;
            API_Log__c Al = New API_Log__c();
            Al.EndpointURLName__c='RemoveNotifyParties';
            Al.Response__c=e.getMessage()+' at line number: '+e.getLineNumber();
            Al.StatusCode__c=string.valueof(res.statusCode);
            Insert Al;
        }
    }

}