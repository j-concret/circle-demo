public class FeedItem_CaseMail {
    
    public static final String FEED_TYPE_TEXTPOST = 'TextPost';
    public static final String FEED_TYPE_CONTENTPOST = 'ContentPost';
    public static final String CLIENT_USER_ID = [Select Id from Profile where Name = 'Tecex Customer'].Id;
    
    public static void sendFeedCommentMail(List<FeedComment> FeedItemList1){
        
        Set<Id> userIds;
        Set<Id> feedTaggedUserId;
        Set<Id> commentTaggedUserId;
        List<Id> sObjectIds = new List<Id>();
        Set<String> feedItemId = new Set<String>();
        Map<Id, FeedItem> feedItemMap = new Map<Id, FeedItem>();
        Map<Id, List<FeedComment>> feedCommentsMap = new Map<Id, List<FeedComment>>();
        List <Messaging.SingleEmailMessage> mails = new List <Messaging.SingleEmailMessage>();
        Map<id, SObject> sObjectMap;
        String mailBody;
        String subject;
        String commentBody;
        String feedCreatedBy;
        String commentCreatedBy;
        String feedItemLink;
        
        for(FeedComment item : FeedItemList1){
            sObjectIds.add(item.ParentId);
            feedItemId.add(item.FeedItemId);
        }
        
        Schema.sObjectType objType = sObjectIds[0].getSObjectType();
        String objName = objType.getDescribe().getName();
        
        if(objName == 'Case'){
            sObjectMap = new Map<Id, SObject>([Select Id, Subject, Shipment_Order__c,Shipment_Order__r.NCP_Quote_Reference__c, Shipment_Order__r.Client_Reference__c, Shipment_Order__r.Client_Reference_2__c from Case where Id IN :sObjectIds]); 
        }
        else if(objName == 'Task'){
            sObjectMap = new Map<Id, SObject>([Select Id, Subject, Shipment_Order__c,Shipment_Order__r.NCP_Quote_Reference__c, Shipment_Order__r.Client_Reference__c, Shipment_Order__r.Client_Reference_2__c from Task where Id IN :sObjectIds]);
        }
        else if(objName == 'Shipment_Order__c'){
            sObjectMap = new Map<Id, SObject>([Select Id, Name, NCP_Quote_Reference__c, Client_Reference__c, Client_Reference_2__c from Shipment_Order__c where Id IN :sObjectIds]);            
        }        
        
        feedItemMap = getFeedItemById(feedItemId, sObjectIds);
        
        feedCommentsMap = getFeedComments(feedItemId, sObjectIds);
        
        for(FeedComment fi : FeedItemList1){
            
            if(fi.ParentId.getSObjectType() == Case.SObjectType 
               || fi.ParentId.getSObjectType() == Task.SObjectType 
               || fi.ParentId.getSObjectType() == Shipment_Order__c.SObjectType){
                   
                   mailBody = '';
                   subject = '';
                   commentBody = '';
                   feedCreatedBy = '';
                   commentCreatedBy = '';
                   feedItemLink = '';
                   
                   userIds = new Set<Id>();
                   feedTaggedUserId = new Set<Id>();
                   commentTaggedUserId = new Set<Id>();
                   
                   
                   SObject sobj = sObjectMap.get(fi.ParentId); 
                   
                   FeedItem fItem = feedItemMap.get(fi.FeedItemId);
                   
                   feedCreatedBy = fItem.CreatedBy.ProfileId == CLIENT_USER_ID ? fItem.CreatedBy.Name + '<span style="color:#888888;">&nbsp;(Customer)</span>' : fItem.CreatedBy.Name + '<span style="color:#888888;">&nbsp;(TecEx)</span>';
                   
                   List<FeedComment> fComment = feedCommentsMap.get(fi.FeedItemId);
                   
                   for(Integer i = fComment.size() - 1 ; i>=0 ; i--){
                       
                       if(i > 2){
                           continue;
                       }
                       
                       commentCreatedBy = fComment[i].CreatedBy.ProfileId == CLIENT_USER_ID ? fComment[i].CreatedBy.Name + '<span style="color:#888888;">&nbsp;(Customer)</span>' : fComment[i].CreatedBy.Name + '<span style="color:#888888;">&nbsp;(TecEx)</span>';
                       
                       if(fComment[i].CreatedBy.ProfileId == CLIENT_USER_ID){
                           commentBody += '<br/><tr> <td width="10%">&nbsp;</td> <td colspan="2"><div style="text-align:right;font-size:15px;font-weight: 500;">' + commentCreatedBy + '</div></td><td width="15%"></td> </tr> <tr> <td width="10%">&nbsp;</td> <td colspan="2"><div style="text-align:right; font-size:13px;">' + fComment[i].CommentBody.replaceAll('<p>', '<p style="margin:0;">') + '</div></td><td width="15%"></td> </tr> <tr> <td width="10%">&nbsp;</td> <td colspan="2"><div style="text-align:right;font-size:11px;">' + fComment[i].CreatedDate.format('EEEE, MMMM d, yyyy HH:mm a') + '</div></td><td width="15%"></td> </tr>';
                       }
                       else{
                           commentBody += '<br/><tr> <td width="10%">&nbsp;</td> <td width="5%"></td> <td colspan="2"><div style="font-size:15px;font-weight: 500;">' + commentCreatedBy + '</div></td> </tr> <tr> <td width="10%">&nbsp;</td> <td width="5%"></td> <td colspan="2"><div style="font-size:13px;">' + fComment[i].CommentBody.replaceAll('<p>', '<p style="margin:0;">') + '</div></td> </tr> <tr> <td width="10%">&nbsp;</td> <td width="5%"></td> <td colspan="2"><div style="font-size:11px;">' + fComment[i].CreatedDate.format('EEEE, MMMM d, yyyy HH:mm a') + '</div></td> </tr>';                        
                       }
                   }
                   
                   feedItemLink = 'https://staging-tecex.cs174.force.com/ncpo/s/notification?id='+fItem.Id;
                   
                   commentCreatedBy = fComment[0].CreatedBy.ProfileId == CLIENT_USER_ID ? fComment[0].CreatedBy.Name + '<span style="color:#888888;">&nbsp;(Customer)</span>' : fComment[0].CreatedBy.Name + '<span style="color:#888888;">&nbsp;(TecEx)</span>';
                   
                   
                   mailBody = '<table style="color:#2a94d6;background-color:#2a94d6" width="100%" cellpadding="0" cellspacing="0" border="0"> <tbody> <tr> <td style="line-height:6px;font-size:6px" bgcolor="#2A94D6" height="6">&nbsp; </td> </tr> </tbody> </table> <table style="color:#f2f2f2;background-color:#f2f2f2" width="100%" cellpadding="0" cellspacing="0" border="0"> <tbody> <tr> <td style="line-height:25px;font-size:25px" bgcolor="#F2F2F2" height="25">&nbsp; </td> </tr> </tbody> </table> <table border="0" cellpadding="0" cellspacing="0" bgcolor="#F2F2F2" width="100%" align="center"> <tbody><tr> <td align="center"> <table style="background-color:#ffffff;width: 600px;padding-top: 15px;padding-bottom:20px;" cellpadding="0" cellspacing="0" border="0" align="center" bgcolor="#FFFFFF"> <tr> <td width="10%">&nbsp;</td> <td colspan="3"> <div style="font-size:15px; font-weight:500;">' + commentCreatedBy + '</div> </td> </tr> <br/> <tr> <td width="10%">&nbsp;</td> <td colspan="3"><div style="font-size:13px;">' + fComment[0].CommentBody.replaceAll('<p>', '<p style="margin:0;">') + '</div></td> </tr> <br/> <br/> <tr> <td width="10%">&nbsp;</td> <td colspan="3"><a style="text-decoration:none; padding: 10px;color:white; background-color: #0176d3;" href="' + feedItemLink +'">View/Comment</a></td> </tr><tr> <td width="10%" style="padding-top: 7px; "> </td> <td colspan="3" style="padding-top: 7px; color: #808080; font-size:10px;">Please do not reply to this email</td> </tr> <br/> <br/>';
                   
                   mailBody += '<tr> <td width="10%">&nbsp;</td> <td colspan="3"><div style="font-size:18px;font-weight: bold;">Original Post</div></td> </tr> <br/> <tr> <td width="10%">&nbsp;</td> <td colspan="3"><div style="font-size:15px;font-weight: 500;">' + feedCreatedBy + '</div></td> </tr> <tr> <td width="10%">&nbsp;</td> <td colspan="3"><div style="font-size:11px;">' + fItem.CreatedDate.format('EEEE, MMMM d, yyyy HH:mm a') + '</div></td> </tr> <br/> <tr> <td width="10%">&nbsp;</td> <td colspan="3"><div style="font-size:13px;">' + fItem.Body.replaceAll('<p>', '<p style="margin:0;">') + '</div></td> </tr> <br/> <tr> <td width="10%">&nbsp;</td><td width="5%"></td> <td colspan="3"><div style="font-size:18px; font-weight:bold;">Comments</div></td> </tr>';
                   
                   mailBody +=commentBody;
                   
                   mailBody += '</table> </td> </tr> <tr> <td align="center"> <table style="background-color:#F2F2F2;width: 600px;" cellpadding="0" cellspacing="0" border="0" align="center"> <div style="font-size: 9px;">4th floor, 23 Melrose Boulevard, Melrose Arch, Johannesburg, South Africa, 2196</div> <img src="https://tecex.com/img/email-tecex-logo-hd-1.png" width="20%" /> <div style="font-size: 9px; font-weight: bold;">Confidentiality Notice</div> <div style="font-size: 9px; font-weight: bold;">This email (and any attachments) is confidential and is intended for the person/entity to whom it is addressed. If received in error, the reading, use, disclosure and transmission of its contents is prohibited. In such instance please immediately notify the sender (and copy confidentialitynotice@tecex.com) by return email and delete this email (and any attachments) from your system.</div> </table> </td> </tr> </tbody> </table> <table style="color:#f2f2f2;background-color:#f2f2f2" width="100%" cellpadding="0" cellspacing="0" border="0"> <tbody> <tr> <td style="line-height:25px;font-size:25px" bgcolor="#F2F2F2" height="25">&nbsp; </td> </tr> </tbody> </table>';
                   
                   commentTaggedUserId = getTaggedUserForFeedComment('Comment', fi.Id);
                   
                   feedTaggedUserId = getTaggedUserForFeedComment('FeedElement', fItem.Id);
                   
                   subject = getEmailSubject(sObj, objName);
                   
                   if(!commentTaggedUserId.isEmpty()){
                       userIds.addAll(commentTaggedUserId);
                   }
                   
                   if(!feedTaggedUserId.isEmpty()){                    
                       userIds.addAll(feedTaggedUserId);                    
                   }
                   
                   if(userIds.contains(fi.CreatedById)){
                       userIds.remove(fi.CreatedById);
                   }
                   
                   Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                   mail.setSubject(subject);
                   if(!Test.isRunningTest()){
                       mail.setToAddresses(new List<Id>(userIds)); 
                   }
                   else{
                       mail.setToAddresses(new List<String>{'Test@Test.io'});
                   }
                   mail.setReplyTo('noreplyapp@tecex.com');
                   mail.setSaveAsActivity(false);
                   mail.setWhatId(fi.ParentId);
                   mail.setHtmlBody(mailBody);
                   
                   if(!userIds.isEmpty()){
                       mails.add(mail);
                   }
                   
               }           
        }       
        
        Messaging.sendEmail(mails);
    }
    
    public static void sendFeedItemMail(List<FeedItem> FeedItemList1){
        
        List<Id> userIds;
        Set<Id> feedTaggedUserId;
        List<Id> sObjectIds = new List<Id>();
        Set<String> feedItemIds = new Set<String>();
        Map<Id, FeedItem> feedItemMap = new Map<Id, FeedItem>();
        List <Messaging.SingleEmailMessage> mails = new List <Messaging.SingleEmailMessage>();
        Map<id, SObject> sObjectMap;
        String mailBody;
        String subject;
        String feedCreatedBy;
        String feedLink;
        
        for(FeedItem item : FeedItemList1){
            sObjectIds.add(item.ParentId);
            feedItemIds.add(item.Id);
        }
        
        Schema.sObjectType objType = sObjectIds[0].getSObjectType();
        String objName = objType.getDescribe().getName();
        
        if(objName == 'Case'){
            sObjectMap = new Map<Id, SObject>([Select Id, Subject, Shipment_Order__c,Shipment_Order__r.NCP_Quote_Reference__c, Shipment_Order__r.Client_Reference__c, Shipment_Order__r.Client_Reference_2__c from Case where Id IN :sObjectIds]); 
        }
        else if(objName == 'Task'){
            sObjectMap = new Map<Id, SObject>([Select Id, Subject, Shipment_Order__c,Shipment_Order__r.NCP_Quote_Reference__c, Shipment_Order__r.Client_Reference__c, Shipment_Order__r.Client_Reference_2__c from Task where Id IN :sObjectIds]);
        }
        else if(objName == 'Shipment_Order__c'){
            sObjectMap = new Map<Id, SObject>([Select Id, Name, NCP_Quote_Reference__c, Client_Reference__c, Client_Reference_2__c from Shipment_Order__c where Id IN :sObjectIds]);            
        }
        
        
        feedItemMap = getFeedItemById(feedItemIds, sObjectIds);
        
        for(FeedItem fi : FeedItemList1){
            
            if(fi.ParentId.getSObjectType() == Case.SObjectType 
               || fi.ParentId.getSObjectType() == Task.SObjectType 
               || fi.ParentId.getSObjectType() == Shipment_Order__c.SObjectType){
                   
                   mailBody = '';
                   subject = '';
                   feedCreatedBy = '';
                   feedLink = '';
                   
                   userIds = new List<Id>();
                   feedTaggedUserId = new Set<Id>();
                   
                   SObject sobj = sObjectMap.get(fi.ParentId); 
                   
                   FeedItem feed = feedItemMap.get(fi.Id);
                   
                   feedCreatedBy = feed.CreatedBy.ProfileId == CLIENT_USER_ID ? feed.CreatedBy.Name + '<span style="color:#888888;">&nbsp;(Customer)</span>' : feed.CreatedBy.Name + '<span style="color:#888888;">&nbsp;(TecEx)</span>';
                   
                   feedLink = 'https://staging-tecex.cs174.force.com/ncpo/s/notification?id='+feed.Id;
                   
                   mailBody = '<table style="color:#2a94d6;background-color:#2a94d6" width="100%" cellpadding="0" cellspacing="0" border="0"> <tbody> <tr> <td style="line-height:6px;font-size:6px" bgcolor="#2A94D6" height="6">&nbsp; </td> </tr> </tbody> </table> <table style="color:#f2f2f2;background-color:#f2f2f2" width="100%" cellpadding="0" cellspacing="0" border="0"> <tbody> <tr> <td style="line-height:25px;font-size:25px" bgcolor="#F2F2F2" height="25">&nbsp; </td> </tr> </tbody> </table> <table border="0" cellpadding="0" cellspacing="0" bgcolor="#F2F2F2" width="100%" align="center"> <tbody><tr> <td align="center"> <table style="background-color:#ffffff;width: 600px;padding-top: 20px;" cellpadding="0" cellspacing="0" border="0" align="center" bgcolor="#FFFFFF"> <tr> <td width="10%">&nbsp;</td> <td> <div style="font-size:15px;font-weight:500;">' + feedCreatedBy + '</div> </td> </tr> <br/> <tr> <td width="10%">&nbsp;</td> <td><div style="font-size:13px;">' +  feed.Body.replaceAll('<p>', '<p style="margin:0;">') + '</div></td> </tr> <br/><br/> <tr> <td width="10%">&nbsp;</td> <td><a style="text-decoration:none;border-style:solid;border-width:8px 16px;border-color:#0176d3;border-radius:3px;color:white; background-color: #0176d3;" href="' + feedLink + '">View/Comment</a></td> </tr><tr> <td width="10%" style=" padding-top: 5px; "> </td> <td style=" padding-top: 5px; color: #808080;  font-size:10px;">Please do not reply to this email</td> </tr> <br/> <br/> </table> </td> </tr> <tr> <td align="center"> <table style="background-color:#F2F2F2;width: 600px;" cellpadding="0" cellspacing="0" border="0" align="center"> <div style="font-size: 9px;">4th floor, 23 Melrose Boulevard, Melrose Arch, Johannesburg, South Africa, 2196</div> <img src="https://tecex.com/img/email-tecex-logo-hd-1.png" width="20%" /> <div style="font-size: 9px; font-weight: bold;">Confidentiality Notice</div> <div style="font-size: 9px; font-weight: bold;">This email (and any attachments) is confidential and is intended for the person/entity to whom it is addressed. If received in error, the reading, use, disclosure and transmission of its contents is prohibited. In such instance please immediately notify the sender (and copy confidentialitynotice@tecex.com) by return email and delete this email (and any attachments) from your system.</div> </table> </td> </tr> </tbody> </table> <table style="color:#f2f2f2;background-color:#f2f2f2" width="100%" cellpadding="0" cellspacing="0" border="0"> <tbody> <tr> <td style="line-height:25px;font-size:25px" bgcolor="#F2F2F2" height="25">&nbsp; </td> </tr> </tbody> </table>';
                   
                   feedTaggedUserId = getTaggedUserForFeedComment('FeedElement', fi.Id);
                   
                   subject = getEmailSubject(sObj, objName);
                   
                   if(!feedTaggedUserId.isEmpty()){
                       userIds.addAll(feedTaggedUserId);
                   }
                   
                   if(userIds.contains(fi.CreatedById)){
                       userIds.remove(userIds.indexOf(fi.CreatedById));
                   }   
                   
                   Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                   mail.setSubject(subject);
                   if(!Test.isRunningTest() && userIds !=null){
                       mail.setToAddresses(userIds); 
                   }
                   else{
                       mail.setToAddresses(new List<String>{'Test@Test.io'});
                   }
                   mail.setReplyTo('noreplyapp@tecex.com');
                   mail.setSaveAsActivity(false);
                   mail.setWhatId(fi.ParentId);
                   mail.setHtmlBody(mailBody);
                   if(!userIds.isEmpty()){
                       mails.add(mail);
                   }                 
               }           
        }       
        
        Messaging.sendEmail(mails);
    }
    
    public static Map<Id, List<FeedComment>> getFeedComments(Set<String> feedItemIds, List<Id> parentId) {
        
        Map<Id, List<FeedComment>> feedCommentMap = new Map<Id, List<FeedComment>>();
        
        List<FeedComment> feedCommentList = [ SELECT Id, FeedItemId, ParentId, CreatedDate, CreatedById, CreatedBy.Name, CreatedBy.ProfileId, CommentBody, RelatedRecordId FROM FeedComment WHERE FeedItemId IN :feedItemIds AND ParentId = :parentId ORDER BY CreatedDate DESC LIMIT 50000];
        
        for(FeedComment fc : feedCommentList){
            
            if(feedCommentMap.containsKey(fc.FeedItemId)){
                feedCommentMap.get(fc.FeedItemId).add(fc);
            }
            else{
                feedCommentMap.put(fc.FeedItemId, new List<FeedComment>{fc});
            }
            
        }
        
        if(feedCommentMap.isEmpty()){
            return null;
        }
        
        return feedCommentMap;
        
    }
    
    public static Map<Id,FeedItem> getFeedItemById(Set<String> feedItemId, List<Id> parentId) {
        Map<Id,FeedItem> feeditemsMap = new Map<Id,FeedItem>([
            SELECT
            Id,
            Body,
            Type,
            ParentId,
            CreatedById,
            CreatedBy.Name,
            CreatedBy.ProfileId,
            CreatedDate,
            LikeCount
            FROM
            FeedItem
            WHERE
            Id = :feedItemId AND
            ParentId = :parentId AND 
            Type IN (:FEED_TYPE_TEXTPOST, :FEED_TYPE_CONTENTPOST) 
            LIMIT 50000
        ]);
        if (feeditemsMap.isEmpty()) {
            return null;
        }
        return feeditemsMap;
    }
    
    public static Set<Id> getTaggedUserForFeedComment(String feedType, Id feedId){
        
        Set<Id> userIds = new Set<Id>();
        
        if(feedType == 'FeedElement'){
            ConnectApi.FeedElement feedItem = ConnectApi.ChatterFeeds.getFeedElement(null, feedId);
            List<ConnectApi.MessageSegment> messageSegments =feedItem.body.messageSegments;
            for (ConnectApi.MessageSegment messageSegment : messageSegments) {
                if (messageSegment instanceof ConnectApi.MentionSegment) {
                    ConnectApi.MentionSegment mentionSegment = (ConnectApi.MentionSegment) messageSegment;
                    userIds.add(mentionSegment.record.id);
                    
                }
            }
        }
        else if(feedType == 'Comment'){
            ConnectApi.Comment feedItem = ConnectApi.ChatterFeeds.getComment(null, feedId);
            List<ConnectApi.MessageSegment> messageSegments =feedItem.body.messageSegments;
            for (ConnectApi.MessageSegment messageSegment : messageSegments) {
                if (messageSegment instanceof ConnectApi.MentionSegment) {
                    ConnectApi.MentionSegment mentionSegment = (ConnectApi.MentionSegment) messageSegment;
                    userIds.add(mentionSegment.record.id);
                    
                }
            }
        }
        
        return userIds;
        
    }
    
    public static String getEmailSubject(SObject sObj, String objName){
        
        String Subject = '';
        
        if(objName == 'Shipment_Order__c'){
            subject = (String)sobj.get('Name');
        }
        else{
            SObject shipmentObj = sobj.getSObject('Shipment_Order__r');
            
            subject = (String)sobj.get('Subject');
            if(sobj.get('Shipment_Order__c') != null){
                subject += shipmentObj.get('NCP_Quote_Reference__c') != null && shipmentObj.get('NCP_Quote_Reference__c') != '' ? '-'+ shipmentObj.get('NCP_Quote_Reference__c') : '';
                subject += shipmentObj.get('Client_Reference__c') != null && shipmentObj.get('Client_Reference__c') != '' ? '-'+ shipmentObj.get('Client_Reference__c') : '';
                subject += shipmentObj.get('Client_Reference_2__c') != null && shipmentObj.get('Client_Reference_2__c') != '' ? '-'+ shipmentObj.get('Client_Reference_2__c') : '';
            }                    
        }
        return subject;
        
    }
    
    
}