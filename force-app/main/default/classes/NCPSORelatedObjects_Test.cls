@isTest
public class NCPSORelatedObjects_Test {
    
    static testMethod void  testForNCPSORelatedObjects(){
          UserRole r = new UserRole(DeveloperName = 'MyCustomRole', Name = 'My Role');
        insert r;
        
        User u = new User(
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id,
            LastName = 'Berkeley',
            Email = 'puser000@amamama.com',
            Username = 'puser000@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            FirstName='Vat',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            UserRoleId = r.Id
        );
        insert u;
        System.runAs(u) {
        
	Access_token__c accessTokenObj = new Access_token__c(Status__c = 'Active',
            												 Access_Token__c = 'asdfghj-sdfgfds-sfdfgf');
        Insert accessTokenObj;
        
        DefaultFreightValues__c df = new DefaultFreightValues__c();
            df.Name = 'Test';
            df.Courier_Base_Rate_KG__c = 12;
            df.Courier_Dangerous_Goods_premium__c = 12;
            df.Courier_Fixed_Charge__c = 12;
            df.Courier_Non_stackable_premium__c = 12;
            df.Courier_Oversized_premium__c = 12;
            df.FF_Base_Rate_KG__c = 12;
            df.FF_Dangerous_Goods_Fixed_Charge_Premium__c = 12;
            df.FF_Dangerous_Goods_premium__c = 12;
            df.FF_Dedicated_Pickup__c = 12;
            df.FF_Fixed_Charge__c = 12;
            df.FF_Non_stackable_Fixed_Charge_Premium__c = 12;
            df.FF_Non_stackable_premium__c = 12;
            df.FF_Oversized_Fixed_Charge_Premium__c = 12;
            df.FF_Oversized_premium__c = 12;
            insert df;
        
        RegionalFreightValues__c rfm = new RegionalFreightValues__c();
        rfm.Name = 'Oceania to South America';
        rfm.Courier_Discount_Premium__c = 12;
        rfm.Courier_Fixed_Premium__c = 10;
        rfm.Destination__c = 'Brazil';
        rfm.FF_Fixed_Premium__c = 123;
        rfm.FF_Regional_Discount_Premium__c = 12;
        rfm.Origin__c = 'Australia';
        rfm.Preferred_Courier_Id__c = 'TECEX';
        rfm.Preferred_Courier_Name__c = 'TECEX';
        rfm.Preferred_Freight_Forwarder_Id__c = 'TECEX';
        rfm.Preferred_Freight_Forwarder_Name__c = 'TECEX';
        rfm.Risk_Adjustment_Factor__c = 1;
        insert rfm;
        
        CountryandRegionMap__c crm = new CountryandRegionMap__c();
        crm.Name = 'Brazil';
        crm.Country__c = 'Brazil';
        crm.European_Union__c = 'No';
        crm.Region__c = 'South America';
        
        insert crm;
        
        CountryandRegionMap__c crm1 = new CountryandRegionMap__c();
        crm1.Name = 'United States';
        crm1.Country__c = 'United States';
        crm1.European_Union__c = 'No';
        crm1.Region__c = 'Oceania';
        
        insert crm1;
        
        Account account = new Account (name='TecEx Prospective Client', Type ='Client', 
                                       CSE_IOR__c = '0050Y000001LTZO', 
                                       Service_Manager__c = '0050Y000001LTZO', 
                                       Financial_Manager__c='0050Y000001LTZO', 
                                       Financial_Controller__c='0050Y000001LTZO', 
                                       Tax_recovery_client__c  = FALSE,
                                       Ship_From_Line_1__c = 'Ship_From_Line_1',
                                       Ship_From_Line_2__c = 'Ship_From_Line_2',
                                       Ship_From_Line_3__c = 'Ship_From_Line_3',
                                       Ship_From_City__c = 'Ship_From_City', 
                                       Ship_From_Zip__c = 'Ship_From_Zip',
                                       Ship_From_Country__c = 'Ship_From_Country' , 
                                       Ship_From_Other_notes__c = 'Ship_From_Other',
                                       Ship_From_Contact_Name__c = 'Ship_From_Contact',
                                       Ship_From_Contact_Email__c = 'Ship_From_Email',
                                       Ship_From_Contact_Tel__c = 'Ship_From_Tel',
                                       Client_Address_Line_1__c = 'Client_Address_Line_1',
                                       Client_Address_Line_2__c = 'Client_Address_Line_2',
                                       Client_Address_Line_3__c = 'Client_Address_Line_3',
                                       Client_City__c = 'Client_City',
                                       Client_Zip__c = 'Client_Zip',
                                       Client_Country__c = 'Client_Country', 
                                       Other_notes__c = 'Other_notes',
                                       Tax_Name__c = 'Tax_Name',
                                       Tax_ID__c = 'Tax_ID',
                                       Contact_Name__c = 'Contact_Name',
                                       Contact_Email__c = 'Contact_Email',
                                       Contact_Tel__c = 'Contact_Tel'
                                      );
        insert account;  
        
        Account account2 = new Account (name='Test Supplier', Type ='Supplier', ICE__c = '0050Y000001km5c', RecordTypeId = '0120Y0000006KjQQAU');
        insert account2;   
        
        Account account3 = new Account (name='Test Manufacturer', Type ='Manufacturer', RecordTypeId = '0120Y0000002wa0QAA', Manufacturer_Alias__c = 'Tester,Who' ); insert account3;
        
        Account account4 = new Account (name='ACME', Type ='Manufacturer', RecordTypeId = '0120Y0000002wa0QAA', Manufacturer_Alias__c = 'Buggs,Bunny' ); insert account4;
        
        
        Contact contact = new Contact ( lastname ='Testing Individual',Client_Notifications_Choice__c='Opt-In',Email = 'Test@test.com',  AccountId = account.Id,Include_in_SO_Communication__c = true);
        insert contact;  
        
        Contact contact2 = new Contact ( lastname ='Testing supplier',Client_Notifications_Choice__c='Opt-In',Email = 'Test@test.com',  AccountId = account2.Id);
        insert contact2; 
        
        IOR_Price_List__c iorpl = new IOR_Price_List__c ( Client_Name__c = account.Id, Name = 'Brazil', IOR_Fee__c = 0.15, On_Charge_Mark_up__c = 0.15,
                                                         TecEx_Shipping_Fee_Markup__c =0.40, IOR_Min_Fee__c = 100,  Admin_Fee__c =100, Set_up_fee__c =100, Bank_Fees__c =200, Tax_Rate__c = 0.20,
                                                         Estimated_Customs_Brokerage__c =250, Estimated_Customs_Clearance__c =250, Estimated_Customs_Handling__c =250, Estimated_Import_License__c =250, Destination__c = 'Brazil' );
        insert iorpl; 
        
        country_price_approval__c cpa = new country_price_approval__c( name ='Brazil', Billing_term__c = 'DAP/CIF - IOR pays',  Airwaybill_Instructions__c = 'User IOR Address', Supplier__c = account2.Id, 
                                                                      In_Country_Specialist__c = '0050Y000001km5c', Clearance_Destination__c = 'Brazil', Preferred_Supplier__c = TRUE, Destination__c = 'Brazil' );
        insert cpa;
        
        Currency_Management2__c CM = new Currency_Management2__c (Name = 'US Dollar (USD)', Currency__c = 'US Dollar (USD)', ISO_Code__c = 'EUR', Conversion_Rate__c = 1);
        insert CM; 
        
        Country__c country = new Country__c(Brokerage_Costs__c = 618, CIF_Adjustment_Factor__c = 0.035, CIF_Absolute_value_adjustment__c = 200, Clearance_Costs__c = 488, 
                                            Country__c = 'Brazil', Name = 'Brazil', Handling_Costs__c = 1180, License_Permit_Costs__c = 0);
        insert country;
        
        Tax_Structure_Per_Country__c tax = new Tax_Structure_Per_Country__c(Applied_to_Value__c = 'CIF', Country__c = country.Id, Default_Duty_Rate__c = 0.5625 , 
                                                                            Order_Number__c = '1', Part_Specific__c = TRUE, Name = 'II', Tax_Type__c = 'Duties');
        insert tax;
        
        CPA_v2_0__c RelatedCPA = new CPA_v2_0__c(Name = 'Brazil', Country_Applied_Costings__c=true );
        insert RelatedCPA;
        CPA_v2_0__c cpav2 = new CPA_v2_0__c(Name = 'Brazil Testing 2.0', Supplier__c = account2.Id, Service_Type__c = 'IOR/EOR', Related_Costing_CPA__c = RelatedCPA.Id, Preferred_Supplier__c = TRUE,
                                            VAT_Rate__c = 0.1, Destination__c = 'Brazil',
                                            VAT_Reclaim_Destination__c = FALSE,
                                            Final_Destination__c ='Brazil',
                                            Country__c = country.id
                                            
                                           );
        insert cpav2;
        
        List<CPARules__c> CPARules = new List<CPARules__c>();
        CPARules.add(new CPARules__c(Country__c = 'Brazil',Criteria__c ='Default', Chargable_weight__c = 'NONE', Courier_Responsibility__c = 'NONE',   ECCN_No__c = 'NONE', NL_Product_User__c = 'NONE', Number_of_final_deliveries__c = 'NONE', ShipmentValue__c ='NONE', Sum_of_quantity_of_matched_products__c = 'NONE', Manufacturer__c = 'NONE', DefaultCPA2__c = cpav2.Id, CPA2__c =  cpav2.Id));
        insert CPARules;
        
        List<CPA_Costing__c> costs = new List<CPA_Costing__c>();
        costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'Shipment value', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'IOR', Min__c = 375, Max__c = 14000, Conditional_value__c = 'Shipment value', Condition__c = 'Between', Floor__c = 0, Ceiling__c = 10000, Currency__c = 'US Dollar (USD)'));
        costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'Anil', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'IOR', Currency__c = 'US Dollar (USD)',Variable_threshold__c = null));
        insert costs;
        
        
        Product2 product = new Product2(Name = 'CAB-ETH-S-RJ45', description='Anil', ProductCode = 'CAB-ETH-S-RJ45', Manufacturer__c = account3.Id);insert product;
        Product2 product2 = new Product2(Name = 'CAB-ETH-S-RJ46', ProductCode = 'CAB-ETH-S-RJ46', Manufacturer__c = account3.Id);insert product2;
        Product2 product3 = new Product2(Name = 'CAB-ETH-S', ProductCode = 'CAB-ETH-S', Manufacturer__c = account3.Id);insert product3;
        
        List<Product_matching_Rule__c> PMR = new  List<Product_matching_Rule__c>();
        PMR.add(new Product_matching_Rule__c(Contains__c='Anil',Does_Not_Contains__c='Test',Product_to_be_match__c=product.id));
        PMR.add(new Product_matching_Rule__c(Contains__c='Anil',Does_Not_Contains__c='Test',Product_to_be_match__c=product.id));
        Insert PMR;
        
        
        HS_Codes_and_Associated_Details__c hs = new HS_Codes_and_Associated_Details__c(Name = '85444210', Destination_HS_Code__c = '85444210', Description__c = 'Something that does something',
                                                                                       Rate__c = 0.2, Country__c = country.id, US_HTS_Code__c = '');insert hs;
        
        HS_Codes_and_Associated_Details__c hs2 = new HS_Codes_and_Associated_Details__c(Name = '10562231145', Destination_HS_Code__c = '10562231145', Description__c = 'Something that does something',
                                                                                        Rate__c = 0.2, Country__c = country.id, US_HTS_Code__c = '85478123');insert hs2;
        
        HS_Codes_and_Associated_Details__c hs3 = new HS_Codes_and_Associated_Details__c(Name = '8345124587', Destination_HS_Code__c = '8345124587', Description__c = 'Something that does something',
                                                                                        Rate__c = 0.2, Country__c = country.id, US_HTS_Code__c = '');insert hs3;
        
        HS_Codes_and_Associated_Details__c hs4 = new HS_Codes_and_Associated_Details__c(Name = '8471512459', Destination_HS_Code__c = '8471512459', Description__c = 'Something that does something',
                                                                                        Rate__c = 0.2, Country__c = country.id, US_HTS_Code__c = '');insert hs4;
        
        HS_Codes_and_Associated_Details__c hs5 = new HS_Codes_and_Associated_Details__c(Name = '10562231156', Destination_HS_Code__c = '10562231156', Description__c = 'Something that does something',
                                                                                        Rate__c = 0.5, Country__c = country.id, US_HTS_Code__c = '85478123');insert hs5;
        
        List<Tax_Structure_Per_Country__c> taxStructure = new List<Tax_Structure_Per_Country__c>();
        taxStructure.add(new Tax_Structure_Per_Country__c(Country__c = country.Id, Applied_to_Value__c = 'CIF', Name = 'I', Order_Number__c = '1', Part_Specific__c = TRUE, Tax_Type__c = 'Duties', Default_Duty_Rate__c = 0.5625)); 
        taxStructure.add(new Tax_Structure_Per_Country__c(Country__c = country.Id, Applied_to_Value__c = 'CIF', Name = 'II', Order_Number__c = '2', Part_Specific__c = FALSE, Tax_Type__c = 'Other', Rate__c = 0.40, Additional_Percent__c = 0.01 ));
        taxStructure.add(new Tax_Structure_Per_Country__c(Country__c = country.Id, Applied_to_Value__c = 'CIF', Name = 'III', Order_Number__c = '3', Part_Specific__c = FALSE, Tax_Type__c = 'VAT', Rate__c = 0.40, Applies_to_Order__c = '1'));
        taxStructure.add(new Tax_Structure_Per_Country__c(Country__c = country.Id, Applied_to_Value__c = 'CIF', Name = 'IV', Order_Number__c = '4', Part_Specific__c = FALSE, Tax_Type__c = 'Other', Amount__c = 400));     
        taxStructure.add(new Tax_Structure_Per_Country__c(Country__c = country.Id, Applied_to_Value__c = 'CIF', Name = 'V', Order_Number__c = '5', Part_Specific__c = FALSE, Tax_Type__c = 'Other', Rate__c = 0.40, Applies_to_Order__c = '1,2,3'));
        taxStructure.add(new Tax_Structure_Per_Country__c(Country__c = country.Id, Applied_to_Value__c = 'CIF', Name = 'VI', Order_Number__c = '6', Part_Specific__c = FALSE, Tax_Type__c = 'Other', Rate__c = 0.40, Applies_to_Order__c = '1,2,3,4', Additional_Percent__c = 0.01));
        taxStructure.add(new Tax_Structure_Per_Country__c(Country__c = country.Id, Applied_to_Value__c = 'CIF', Name = 'VII', Order_Number__c = '7', Part_Specific__c = FALSE, Tax_Type__c = 'Other', Rate__c = 0.40, Max__c = 1000, Min__c = 50));       
        taxStructure.add(new Tax_Structure_Per_Country__c(Country__c = country.Id, Applied_to_Value__c = 'CIF', Name = 'VIII', Order_Number__c = '8', Part_Specific__c = FALSE, Tax_Type__c = 'Other', Rate__c = 0.40, Applies_to_Order__c = '1,2,3,4,5,7'));
        taxStructure.add(new Tax_Structure_Per_Country__c(Country__c = country.Id, Applied_to_Value__c = 'CIF', Name = 'IX', Order_Number__c = '9', Part_Specific__c = FALSE, Tax_Type__c = 'Other', Rate__c = 0.40, Applies_to_Order__c = '1,2,3,4,6'));
        taxStructure.add(new Tax_Structure_Per_Country__c(Country__c = country.Id, Applied_to_Value__c = 'CIF', Name = 'X', Order_Number__c = '10', Part_Specific__c = FALSE, Tax_Type__c = 'Other', Rate__c = 0.40)); 
        insert taxStructure;
        
        Id pid = [SELECT Id FROM Profile WHERE Name = 'System Administrator' LIMIT 1].Id;
        User usr = [SELECT Id FROM User WHERE ProfileId=:pid LIMIT 1];
        Shipment_Order__c shipment = new Shipment_Order__c(Account__c = account.id, Shipment_Value_USD__c = 2000, CPA_v2_0__c = cpav2.Id,
                                                                Client_Contact_for_this_Shipment__c = contact.Id, Who_arranges_International_courier__c ='Client', Tax_Treatment__c ='DAP/CIF - IOR pays', 
                                                                Ship_to_Country__c = cpa.Id, Destination__c = 'Brazil', Service_Type__c = 'IOR', IOR_Price_List__c = iorpl.Id, Chargeable_Weight__c = 200,  
                                                                Shipping_Status__c = 'Cost Estimate Abandoned',IOR_CSE__c = usr.id, ICE__c = usr.Id  );  
 		Test.startTest();
		insert shipment;
        Notify_Parties__c np = new Notify_Parties__c(Shipment_Order__c = shipment.Id,Contact__c =contact.Id);
        insert np;
        List<SO_Travel_History__c> SOTHList = new List<SO_Travel_History__c>();
 		SO_Travel_History__c SOTH =  new SO_Travel_History__c (
                                Source__c = 'FDA Status changed',
                                Date_Time__c =  System.now(),
                                Location__c = '',
                                Description__c = 'NONE',
                                Expected_Date_to_Next_Status__c=System.now(),
                               Type_of_Update__c = 'NONE',
                                TecEx_SO__c = shipment.ID
                            );
        SOTHList.add(SOTH);
        SO_Travel_History__c SOTH1 =  new SO_Travel_History__c (
                                Source__c = 'Task (internal)',
                                Date_Time__c =  System.now(),
                                Location__c = '',
                                Description__c = 'NONE',
                                Expected_Date_to_Next_Status__c=System.now(),
                               Type_of_Update__c = 'NONE',
                                TecEx_SO__c = shipment.ID
                            );
        SOTHList.add(SOTH1);
        
        insert SOTHList;
        
		System.debug('shipment Obj Id '+shipment.Id);
        System.debug('shipment Obj ');
        System.debug(JSON.serializePretty(shipment));
        Shipment_Order_Package__c shipmentPackage = new Shipment_Order_Package__c(packages_of_same_weight_dims__c = 21,
                                                                                  shipment_order__c = shipment.Id);
        Insert shipmentPackage;
        
        Final_Delivery__c finalDelivery = new Final_Delivery__c(shipment_order__c = shipment.Id);
        Insert finalDelivery;
        
        Attachment attach = new Attachment(ParentId = shipment.Id,
                                          name = 'attachment Name',
                                          body = Blob.valueOf('data for the attachment'));
        Insert attach;
        
        Part__c part = new Part__c(shipment_order__c = shipment.Id,
                                   Name = 'Test Part',
                                   Description_and_Functionality__c = 'Test description of Part');
        Insert part;
        
        Freight__c freight = new Freight__c(shipment_order__c = shipment.Id,
                                            Chargeable_weight_in_KGs_packages__c = 12,
                                            Ship_From_Country__c = 'United States',
                                            Status__c = 'Submitted');
        Insert freight;
        Master_Task__c masterTask = new Master_Task__c(Name = 'Demo Task', Client_visible__c = true);
        Insert masterTask;
        
        Master_Task_Prerequisite__c prequisite = new Master_Task_Prerequisite__c(
            Master_Task_Dependent__c = masterTask.Id,
            Master_Task_Prerequisite__c = masterTask.Id
            );
        insert prequisite;
                
        Task tk = new Task(recordtypeId=Schema.Sobjecttype.task.getRecordTypeInfosByDeveloperName().get('SalesTasks').getRecordTypeId(),WhatId = shipment.Id,status = 'Completed',priority = 'Normal',Master_Task__c = masterTask.Id,Subject='ABC');
        Insert tk;
        
        
        ContentVersion cv = new ContentVersion();
        cv.Title = 'Test Document';
        cv.PathOnClient = 'TestDocument.pdf';
        cv.VersionData = Blob.valueOf('Test Content');
        cv.IsMajorVersion = true;
        
        Insert cv;
        
        FeedItem fd = new FeedItem(Title = 'Hello',ParentId = tk.Id, body='body',visibility='AllUsers');
        
        insert fd;
        
        FeedAttachment fda = new FeedAttachment(type='Content',FeedEntityId = fd.id, recordId=cv.Id);
        
        insert fda;
        
        NCPSORelatedObjectsWra obj = new NCPSORelatedObjectsWra();
        obj.AccessToken = accessTokenObj.Access_Token__c;
        obj.AccountID = account.Id;
        obj.SOID = shipment.Id;
        
        RestRequest req1 = new RestRequest();
        RestResponse res1 = new RestResponse();
        req1.requestURI = '/services/apexrest/NCPSORelatedObjects/'; 
        req1.httpMethod = 'POST';
        req1.requestBody = Blob.valueOf(JSON.serialize(obj));     
        RestContext.request = req1;
        RestContext.response = res1;
        RestContext.request.addHeader('Content-Type', 'application/json');
        
        
        	NCPSORelatedObjects.NCPSORelatedObjects();
        Test.stopTest();
        }
    }
    
    static testMethod void  testErrorForNCPSORelatedObjects(){
        UserRole r = new UserRole(DeveloperName = 'MyCustomRole', Name = 'My Role');
        insert r;
        
        User u = new User(
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id,
            LastName = 'Berkeley',
            Email = 'puser000@amamama.com',
            Username = 'puser000@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            FirstName='Vat',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            UserRoleId = r.Id
        );
        insert u;
        System.runAs(u) {
        
        Access_token__c accessTokenObj = new Access_token__c(Status__c = 'Active',
            												 Access_Token__c = 'asdfghj-sdfgfds-sfdfgf');
        Insert accessTokenObj;
        
        NCPSORelatedObjectsWra obj = new NCPSORelatedObjectsWra();
        obj.AccessToken = accessTokenObj.Access_Token__c;
        
        RestRequest req1 = new RestRequest();
        RestResponse res1 = new RestResponse();
        req1.requestURI = '/services/apexrest/NCPSORelatedObjects/'; 
        req1.httpMethod = 'POST';
        req1.requestBody = Blob.valueOf(JSOn.serialize(obj));     
        RestContext.request = req1;
        RestContext.response = res1;
        RestContext.request.addHeader('Content-Type', 'application/json');
        
        Test.startTest(); 
        	NCPSORelatedObjects.NCPSORelatedObjects();
        Test.stopTest();
        }
    }
}