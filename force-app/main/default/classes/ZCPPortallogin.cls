/* this is not for front end, It is is used for Postman login flow only
 *  
 */
@RestResource(urlMapping='/ZCPPortalLogin/*')
Global class ZCPPortallogin {
    @Httppost
      global static void NCPCreateCase(){
          
      LoginResponse objResponse = new LoginResponse();
          map<String,string> Finalresponse= new map<string,string>();
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        Blob body = req.requestBody;
    	res.addHeader('Content-Type', 'application/json');
        String requestString = body.toString();
        ZCPPortalloginWra rw  = (ZCPPortalloginWra)JSON.deserialize(requestString,ZCPPortalloginWra.class);
          
          try{
          
        string LoginXML='<?xml version="1.0" encoding="UTF-8"?>';
        loginXML += '<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns1="urn:partner.soap.sforce.com">';
          loginXML += '<SOAP-ENV:Header>';
              loginXML +='<ns1:LoginScopeHeader>';
               loginXML += '<ns1:organizationId>00D7Z00000056Um</ns1:organizationId>'; // This has to change for Prod - 00D0Y000001KRPf, Account2-00D1q0000008awq
               loginXML += '</ns1:LoginScopeHeader>';
             loginXML += '</SOAP-ENV:Header>';
             loginXML += '<SOAP-ENV:Body>';
               loginXML += '<ns1:login>';
                loginXML +='<ns1:username>'+ rw.Username+'</ns1:username>';
               loginXML += '<ns1:password>'+rw.password+'</ns1:password>';
               loginXML += '</ns1:login>';
            loginXML += '</SOAP-ENV:Body>';
            loginXML +='</SOAP-ENV:Envelope>';            
            
            String endpoint='https://'+rw.domain+'.salesforce.com/services/Soap/u/22.0'; //For sandbox domain =test,  for prod --> domain=  login
           // String endpoint='https://login.salesforce.com/services/Soap/u/22.0'; //prod   login
           
            HttpRequest request = new HttpRequest();
            request.setEndpoint(endpoint);
            request.setTimeout(60000); 
            request.setMethod('POST');
            request.setHeader('SOAPAction','""');
            request.setHeader('Content-Type', 'text/xml;charset=UTF-8');
            request.setBody(loginXML);       
            HttpResponse response = new HttpResponse();
           
            response = new Http().send(request);
           
            String responseBody = response.getBody();
            system.debug('responseBody-->'+responseBody);
            String sessionId = getValueFromXMLString(responseBody, 'sessionId');
             
            objResponse.statusMessage = response.getStatus();
            objResponse.statusCode = response.getStatusCode();
             
            if(string.isNotBlank(sessionId)){
                objResponse.isSuccess = true;
                objResponse.sessionId = sessionId;
                 
            }else{
                objResponse.isSuccess = false;
            }
              if(objresponse.issuccess==true){
              // Finalresponse=CommunityLoginController.getUserDetails();
                  Finalresponse.put('SessionID',objresponse.sessionId);
                   String Accesstoken = GuidUtilCP.NewGuid();
                   string Base64Accesstoken = EncodingUtil.base64Encode(Blob.valueof(Accesstoken));
                   Access_token__c At = New Access_token__c();
                    At.Access_Token__c=Accesstoken;
                    At.AccessToken_encoded__c=Base64Accesstoken;
                 	Insert At; 
                 // Finalresponse.put('usid',UserInfo.getUserId());
           			 Id userId = UserInfo.getUserId();
                   User u = [select Id, contactId from User where username =:rw.Username];
                  // response.put('User',u);
                    if(u.contactId != null){
                    Id getContactId = u.contactId;
                    Contact contact = [SELECT Id , Name , Account.Id , Account.Name FROM Contact where Id =:getContactId];
                    Finalresponse.put('AccountId',contact.Account.Id);
                    Finalresponse.put('ContactId',contact.Id);    
                   Finalresponse.put('Accesstoken',at.Access_Token__c);
                        Finalresponse.put('usid',U.id);
                  
                 
              			}
              }
              
                    res.responseBody = Blob.valueOf(JSON.serializePretty(Finalresponse));
            		res.addHeader('Content-Type', 'application/json');
        			res.statusCode = 200;
              
          }
          catch(Exception e){
            //  String ErrorString ='Something went wrong, please contact support_SF@tecex.com';
              string Errorstring= e.getStackTraceString()+e.getLineNumber()+e.getMessage(); 		
              res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
            		res.addHeader('Content-Type', 'application/json');
        			res.statusCode = 500;
              
          }
          
        // map<string,string> finalresponse=CommunityLoginController.getUserDetails();
      
      
      }
     public static string getValueFromXMLString(string xmlString, string keyField){
        String xmlKeyValue = '';
        if(xmlString.contains('<' + keyField + '>')){
            try{
                xmlKeyValue = xmlString.substring(xmlString.indexOf('<' + keyField + '>')+keyField.length() + 2, xmlString.indexOf('</' + keyField + '>'));   
            }catch (exception e){
                 
            }            
        }
        return xmlKeyValue;
    }
   global class LoginResponse {
        public String sessionId {get; set;}
        public Boolean isSuccess {get; set;}
        public String statusMessage {get; set;}
        public Integer statusCode {get; set;}
    }

}