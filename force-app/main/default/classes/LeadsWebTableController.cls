/**
* @description       :
* @author            : ChangeMeIn@UserSettingsUnder.SFDoc
* @group             :
* @last modified on  : 10-12-2021
* @last modified by  : Jitender Yadav
* Modifications Log
* Ver   Date         Author                               Modification
* 1.0   03-02-2021   ChangeMeIn@UserSettingsUnder.SFDoc   Initial Version
**/
public class LeadsWebTableController {
    @AuraEnabled
    public static Map<String,Object> getPicklistsValues(String object_name, List<String> field_names){
        Map<String, Set<String>> mapName = new Map<String, Set<String>>();
        String[] types = new String[] {object_name};
            Schema.DescribeSobjectResult[] results = Schema.describeSObjects(types);
        for (String field_name : field_names) {
            mapName.put(field_name,getPicklistValues(results,field_name));
        }
        return mapName;
    }

    private static Set<String> getPicklistValues(List<Schema.DescribeSobjectResult> results, String field_name) {
        Set<String> values = new Set<String>();
        for(Schema.DescribeSobjectResult res : results) {
            for (Schema.PicklistEntry entry : res.fields.getMap().get(field_name).getDescribe().getPicklistValues()) {
                if (entry.isActive()) {
                    values.add(entry.getValue());
                }
            }
        }
        return values;
    }

    @AuraEnabled
    public static  List<String> checkLeadsData(List<String> firstName, List<String> lastName, List<String> email, List<String> company){
        try {

            List<String> records = new List<String>();
            for (Lead variable : [SELECT FirstName, LastName, Company, Email
                                  FROM Lead
                                  WHERE (FirstName IN :firstName AND LastName IN : lastName AND Company IN : company) OR Email IN : email]) {
                                      records.add(variable.Company+'-'+variable.FirstName+'-'+variable.LastName);
                                      records.add(variable.Email);

                                  }
            for (Contact variable : [SELECT FirstName, LastName, Account_Text_Formula__c, Email
                                     FROM Contact
                                     WHERE (FirstName IN :firstName AND LastName IN : lastName AND Account_Text_Formula__c IN : company) OR Email IN : email]) {
                                         records.add(variable.Account_Text_Formula__c+'-'+variable.FirstName+'-'+variable.LastName);
                                         records.add(variable.Email);                }

            return records;
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    @AuraEnabled
    public static Map<String,object> newLeadInsert(String newLeads){
        try {
            List<Map<String,String>> listName = (List<Map<String,String>>)JSON.deserialize(newLeads,List<Map<String,String>>.class);
            List<Lead> newLeadList = new List<Lead>();
            List<String> leadIdsInserted = new List<String>();
            List<String> company = new List<String>();
            List<String> country = new List<String>();

            Map<String,CountryandRegionMap__c> countryRegionMap = CountryandRegionMap__c.getall();

            for (Map<String,String>variable : listName) {

                Lead newlead = new Lead();
                newlead.LeadSource = variable.get('Xant/Not Xant') == 'XANT'? 'Other' : '';
                newlead.Company = variable.get('Company');
                newlead.Client_Type__c = variable.get('Client Type');
                newlead.FirstName = variable.get('First Name');
                newlead.LastName = variable.get('Last Name');
                newlead.Country__c = variable.get('Country');
                newlead.Title = variable.get('Title');
                newlead.Email = variable.get('Email');
                newlead.Phone = variable.get('Phone');
                newlead.Phone_Extension__c =variable.get('Phone Extension');
                newlead.Partner_Referral__c = variable.get('Partner Referral');
                newlead.Competitors__c = variable.get('Competitors');
                newlead.Website = variable.get('Website');
                newlead.OwnerId = variable.get('Lead Owner');
                newlead.Status = 'New Lead';
                newlead.Price_List_Type__c = 'Standard Price List';
                if (variable.containsKey('Related_Company')) {
                    newlead.Related_Company__c = variable.get('Related_Company');
                }
                newLeadList.add(newLead);
                company.add(variable.get('Company'));
                country.add(variable.get('Country'));
            }
            //System.debug('new Leads ==>'+newLeadList);
            Map<String,object> resultMap = new Map<String,object>();
            //insert newLeadList;
            RecusrsionHandler.skipBeforeLeadInsert = true;
            Database.SaveResult[] srList = Database.insert(newLeadList, false);
            for(Integer i=0;i<srList.size();i++){
                if (srList[i].isSuccess()) {
                    // Operation was successful, so get the ID of the record that was processed
                    System.debug('Successfully inserted account. Lead ID: ' + srList[i].getId());
                    leadIdsInserted.add(srList[i].getId());
                }
                else {
                    List<String> errors = new List<String>();
                    // Operation failed, so get all errors
                    for(Database.Error err : srList[i].getErrors()) {
                        errors.add(err.getMessage());
                        System.debug('The following error has occurred.');
                        System.debug(err.getStatusCode() + ': ' + err.getMessage());
                        System.debug('Lead fields that affected this error: ' + err.getFields());
                    }
                    resultMap.put(listName[i].get('ind'),errors);
                }
            }
            if(!leadIdsInserted.isEmpty()){

                List<Company__c> companyToUpdate = [SELECT Id,Number_of_Leads__c FROM Company__c WHERE Name IN :company AND Country__c IN :country];
                if (!companyToUpdate.isEmpty()) {
                    for(Company__c com : companyToUpdate){
                        com.Number_of_Leads__c += 1;
                    }
                    update companyToUpdate;
                }

            }
            return resultMap;

        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    @AuraEnabled
    public static Map<String,String> getLeadOwners(){
        try {
            Map<String,String> userMap = new Map<String,String>();
            List<String> validCompany = new List<String>();
            List<String> validDomains = new List<String>();

            List<User> users = [SELECT Id,Name,UserName FROM User WHERE UserName LIKE '%@tecex.com%'];
            for (User variable : users) {
                userMap.put(variable.Name.toLowercase(),variable.Id);
            }

            return userMap;
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    @AuraEnabled
    public static Map<String,Object> getValidDomains(List<String> countries,List<String> domains,String ownersWithCompany){
        Map<String,String> ownersWithCompanyMap = (Map<String,String>)JSON.deserialize(ownersWithCompany,Map<String,String>.class);
        Set<String> validDomains = new Set<String>();
        Set<String> validRegionCountries = new Set<String>();
        Set<String> companyWithOwners = new Set<String>();
        try {
            Map<String,CountryandRegionMap__c> countryRegionMapCS = CountryandRegionMap__c.getall();
            Map<String,Set<String>> countryRegionMap = new Map<String,Set<String>>();
            Map<String,SObject> DomainRegionMap = new Map<String,SObject> ();
            //system.debug('countryRegionMap'+countryRegionMap.get('Canada').Region__c);
            for (String variable : countries) {
                if (countryRegionMapCS.containsKey(variable) && countryRegionMap.containsKey(countryRegionMapCS.get(variable).Region_for_Company__c)) {
                    countryRegionMap.get(countryRegionMapCS.get(variable).Region_for_Company__c).add(variable);
                }else if (countryRegionMapCS.containsKey(variable)) {
                    countryRegionMap.put(countryRegionMapCS.get(variable).Region_for_Company__c,new Set<String>{variable});
                }

            }

            for(Domain__c domainObj : [SELECT Id,Name,Company__c,Company__r.Name,Company__r.Country__c,Company__r.Region__c,Company__r.Number_of_Leads__c,Company__r.Extra_Leads_Approved__c
                                            FROM Domain__c
                                            WHERE Name IN :domains]){

                    DomainRegionMap.put(domainObj.Name.toLowercase(), domainObj);

                    String Region_for_Company = countryRegionMapCS.get(domainObj.Company__r.Country__c)?.Region_for_Company__c;

                    if (Region_for_Company != null && countryRegionMap.containsKey(Region_for_Company) ) {
                        validRegionCountries.addAll(countryRegionMap.get(Region_for_Company));
                    }
            }
            //System.debug('DomainRegionMap'+JSON.serialize(DomainRegionMap));

            //System.debug('ownersWithCompanyMap values'+String.join(ownersWithCompanyMap.values(),',').split(','));
            //System.debug('ownersWithCompanyMap Keys'+ownersWithCompanyMap.keySet());
            for(Company__c companyRecords : [SELECT Id,Name,Owner.Name
                                                FROM Company__c
                                                WHERE Name IN :String.join(ownersWithCompanyMap.values(),',').split(',') AND OwnerId IN :ownersWithCompanyMap.keySet()]){
                    companyWithOwners.add(companyRecords.Name+'-'+companyRecords.Owner.Name);
            }
            for(Company_User_Assignment__c companyUserAssignRecords : [SELECT Id,Company__r.Name,User__r.Name
                                                FROM Company_User_Assignment__c
                                                WHERE Company__r.Name IN :String.join(ownersWithCompanyMap.values(),',').split(',') AND User__c IN :ownersWithCompanyMap.keySet()]){
                    companyWithOwners.add(companyUserAssignRecords.Company__r.Name+'-'+companyUserAssignRecords.User__r.Name);
            }

            //System.debug('companyWithOwners '+companyWithOwners);

            return new Map<String,Object>{'validRegionCountries'=>validRegionCountries,'companyWithOwners' => companyWithOwners,'DomainRegionMap' => DomainRegionMap};
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

}