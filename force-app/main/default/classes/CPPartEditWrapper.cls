public class CPPartEditWrapper {

    public List<Parts> Parts;
    public class Parts {
    	public String SOID;
        public String PartID;
		public String PartNumber;
		public String PartDescription;
		public Integer Quantity;
		public Double UnitPrice;
		public String HSCode;
		public String CountryOfOrigin;
		public String ECCNNo;
        public String Type_of_Goods;
		public String Li_ion_Batteries;
		public String Li_ion_BatteryTypes;
	}
    
    public static CPPartEditWrapper parse(String json) {
		return (CPPartEditWrapper) System.JSON.deserialize(json, CPPartEditWrapper.class);
	}
}