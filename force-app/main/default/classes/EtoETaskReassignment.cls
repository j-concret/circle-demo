public class EtoETaskReassignment {
    @InvocableMethod(label='Task Re-assignmets' description='Task Re-assignmets from ICE to AM depending upon E2E process active is ON')

    public static void taskReassignment(List<Id> shipmentOrderIds) {

        /* List<Task> taskList = new List<Task>();
           Set<Shipment_Order__c> shipmentSet = new Set<Shipment_Order__c>();

           Map<Id,Shipment_Order__c> shipmentMap = new Map<Id, Shipment_Order__c>([SELECT Id, Name, E2E_Process_active__c,
                                                                                ICE__c,IOR_CSE__c, E2E_Ice_Swop_ID__c
                                                                                FROM Shipment_Order__c
                                                                                WHERE Id IN: shipmentOrderIds]);
           for(Task task : [SELECT Id, OwnerId, WhatId, E2E_process_checked__c,
                         E2E_Ice_Swop_ID__c,Assigned_to_Role__c
                         FROM Task
                         WHERE WhatId IN :shipmentOrderIds]) {
            Shipment_Order__c shipOrder = shipmentMap.get(task.WhatId);
            if(task.Assigned_to_Role__c == 'ICE'  && !task.E2E_process_checked__c) {
                task.Assigned_to_Role__c = 'AM';
                if(shipOrder.IOR_CSE__c != null) {
                    task.OwnerId = shipOrder.IOR_CSE__c;
                }
                if(shipOrder.E2E_Ice_Swop_ID__c != null) {
                    task.E2E_Ice_Swop_ID__c = shipOrder.E2E_Ice_Swop_ID__c;
                }
                task.E2E_process_checked__c = true;
            }
            else if(task.Assigned_to_Role__c == 'AM' && task.E2E_process_checked__c) {
                task.Assigned_to_Role__c = 'ICE';
                if(task.E2E_Ice_Swop_ID__c != null) {
                    task.OwnerId = task.E2E_Ice_Swop_ID__c;
                }
                task.E2E_process_checked__c = false;
                if(shipOrder.E2E_Ice_Swop_ID__c != null) {
                    shipOrder.ICE__c = shipOrder.E2E_Ice_Swop_ID__c;
                    shipmentSet.add(shipOrder);
                }
            }
            taskList.add(task);
           }
           if(taskList!=null && !taskList.isEmpty()) {
            update taskList;
           }
           if(shipmentSet!=null && !shipmentSet.isEmpty()) {
            List<Shipment_Order__c> shipmentList = new List<Shipment_Order__c>(shipmentSet);
            update shipmentList;
           } */
    }

}