@RestResource(urlMapping='/NCPDownlodCostestimates/*')
Global class NCPDownloadCostestimateV2 {
    
    @Httppost
    global static void NCPDownloadattachment(){
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        Blob body = req.requestBody;
        res.addHeader('Content-Type', 'application/json');
        String requestString = body.toString();
        System.debug('requestString :'+requestString);
        NCPDownloadattachmentv2wra rw  = (NCPDownloadattachmentv2wra)JSON.deserialize(requestString,NCPDownloadattachmentv2wra.class);
        String communityId = Network.getNetworkId();
        Try{
            
            Access_token__c At =[select status__c,Access_Token__c from Access_token__c where Access_Token__c=:rw.Accesstoken ];
            if( at.status__C =='Active' && rw.Identifier==0){
                
                try{
                    string Jsonstring='Creation of document is under progress, this will take 5 sec. Pls retry with Identifier == 1';
                    
                    SO_GenerateCostEstimate.generateCostEstimateForAPI(rw.recordID);
                    JSONGenerator gen = JSON.createGenerator(true);
                    gen.writeStartObject();
                    gen.writeFieldName('Success');
                    gen.writeStartObject();
                    
                    gen.writeStringField('Response', Jsonstring);
                    gen.writeEndObject();
                    gen.writeEndObject();
                    String jsonData = gen.getAsString();
                    res.responseBody = Blob.valueOf(jsonData);
                    
                    
                    
                    res.statusCode=200;  
                    API_Log__c Al = New API_Log__c();
                    Al.EndpointURLName__c='NCPDownlodCostestimate';
                    Al.StatusCode__c=string.valueof(res.statusCode);
                    Al.Response__c = 'Attachments are downloaded to --' +rw.recordID;
                    Insert Al;
                } catch (Exception e) {
                    
                    String ErrorString ='Something went wrong, please contact Sfsupport@tecex.com'+e.getMessage() + e.getLineNumber();
                    
                    res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
                    res.addHeader('Content-Type', 'application/json');
                    res.statusCode = 400;
                    
                    API_Log__c Al = New API_Log__c();
                    Al.EndpointURLName__c='NCPDownlodCostestimate';
                    Al.Response__c=e.getMessage()+' at line number: '+e.getLineNumber();
                    Al.StatusCode__c=string.valueof(res.statusCode);
                    Insert Al;
                    
                }
                
            }
            else  if( at.status__C =='Active' && rw.Identifier==1){
                
                try{
                    //  List<ContentDocumentLink> contDocs=    [SELECT Id,contentdocumentID,contentdocument.title,contentdocument.createddate,contentdocument.lastmodifieddate,ContentDocument.LatestPublishedVersion.VersionData FROM ContentDocumentLink WHERE LinkedEntityId =:rw.RecordID order by contentdocument.lastmodifieddate desc limit 1];
                    
                    ContentDocumentLink cdl = [SELECT Id,contentdocumentID,contentdocument.title,contentdocument.createddate,contentdocument.lastmodifieddate,ContentDocument.LatestPublishedVersion.VersionData FROM ContentDocumentLink WHERE LinkedEntityId =:rw.RecordID order by contentdocument.lastmodifieddate desc limit 1];
                    ContentVersion cv = [select id from contentversion where contentdocumentid = :cdl.contentdocument.id and IsLatest =TRUE];
                    ContentDistribution cd = new ContentDistribution();
                    cd.Name = 'Test'; // Please have document title
                    cd.ContentVersionId = cv.id;
                    cd.PreferencesAllowViewInBrowser= true;
                    cd.PreferencesLinkLatestVersion=true;
                    cd.PreferencesNotifyOnVisit=false;
                    cd.PreferencesPasswordRequired=false;
                    cd.PreferencesAllowOriginalDownload= true;
                    insert cd;
                    ContentDistribution contentDist = [SELECT Id, CreatedDate, LastModifiedDate, Name,ContentVersionId, ContentDocumentId, DistributionPublicUrl, ContentDownloadUrl, PdfDownloadUrl FROM ContentDistribution where Id=:cd.Id order by createddate desc limit 1 ];         
                    
                                   
                    JSONGenerator gen = JSON.createGenerator(true);
                   gen.writeStartObject();
                    gen.writeFieldName('Success');
                    gen.writeStartObject();
                    
                    if(contentDist.Id == null) {gen.writeNullField('ContentDistributionId');} else{gen.writeIdField('ContentDistributionId',+contentDist.Id);}
                    if(contentDist.ContentDocumentId == null) {gen.writeNullField('ContentDocumentId');} else{gen.writeIdField('ContentDocumentId',+contentDist.ContentDocumentId);}
                 //   if(contentDist.DistributionPublicUrl == null) {gen.writeNullField('DistributionPublicUrl');} else{gen.writeStringField('DistributionPublicUrl',+contentDist.DistributionPublicUrl);}
                    if(contentDist.ContentDownloadUrl == null) {gen.writeNullField('ContentDownloadUrl');} else{gen.writeStringField('ContentDownloadUrl',+contentDist.ContentDownloadUrl);}
                    if(contentDist.PdfDownloadUrl == null) {gen.writeNullField('PdfDownloadUrl');} else{gen.writeStringField('PdfDownloadUrl',+contentDist.PdfDownloadUrl);}
                    
                    gen.writeEndObject();
                    gen.writeEndObject();
                    String jsonData = gen.getAsString();
                    res.responseBody = Blob.valueOf(jsonData);
                    
                    
                    
                    res.statusCode=200;  
                   
                    API_Log__c Al = New API_Log__c();
                    Al.EndpointURLName__c='NCPDownlodCostestimate';
                    Al.StatusCode__c=string.valueof(res.statusCode);
                    Al.Response__c = 'Attachments are downloaded to --' +rw.recordID;
                    Insert Al;
                } catch (Exception e) {
                    
                    String ErrorString ='Something went wrong, please contact Sfsupport@tecex.com'+e.getMessage() + e.getLineNumber();
                    
                    res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
                    res.addHeader('Content-Type', 'application/json');
                    res.statusCode = 400;
                    
                    API_Log__c Al = New API_Log__c();
                    Al.EndpointURLName__c='NCPDownlodCostestimate';
                    String emsg = e.getMessage(); 
                    Al.Response__c= emsg+' at line number: '+e.getLineNumber();
                    Al.StatusCode__c=string.valueof(res.statusCode);
                    Insert Al;
                    
                }
                
            }
            
            
        }
        catch(Exception e){
            
            String ErrorString ='Something went wrong, please contact Sfsupport@tecex.com'+e.getMessage() + e.getLineNumber();
            
            res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
            res.addHeader('Content-Type', 'application/json');
            res.statusCode = 500;
            
            API_Log__c Al = New API_Log__c();
            Al.EndpointURLName__c='NCPDownlodCostestimate';
            Al.Response__c=e.getMessage()+' at line number: '+e.getLineNumber();
            Al.StatusCode__c=string.valueof(res.statusCode);
            Insert Al;
            
        }
        
    }
}