@isTest
public class NCPUpdatecontact_Test {
public static testMethod void NCPUpdatecontactTest(){
        
        //Test Data
        Access_token__c at = new Access_token__c(status__c = 'Active', Access_Token__c = '123');
        insert at;
        //Contact con = TestDataFactory.myContact();
        Account account2 = new Account (name='Test Supplier', Type ='Supplier', ICE__c = '0050Y000001km5c', RecordTypeId = '0120Y0000006KjQQAU');
        insert account2;        
        
        Contact contact = new Contact ( lastname ='Testing Individual',  AccountId = account2.Id);
        insert contact; 
        //Json Body
        String json = '{"Accesstoken":"'+at.Access_Token__c+'","ContactID":"'+contact.Id+'","IncludeinInvoicingEmails":true}';
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
        req.requestURI = '/services/apexrest/NCPUpdateincludeaccountstatement'; 
        req.httpMethod = 'Post';
        req.addHeader('Content-Type', 'application/json');
        req.requestBody = Blob.valueof(json);
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
        NCPUpdatecontact.NCPCreateCase();
        Test.stopTest(); 
    }
    
    //Covering catch exception
    public static testMethod void NCPUpdatecontact_Test1(){
        
        //Test Data
        Access_token__c at = new Access_token__c(status__c = 'Active', Access_Token__c = '123');
        insert at;
        //Contact con = TestDataFactory.myContact();
        
        //Json Body
        String json = '{"Accesstoken":"'+at.Access_Token__c+'","ContactID":"null","IncludeinInvoicingEmails":true}';
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
         req.requestURI = '/services/apexrest/NCPUpdateincludeaccountstatement'; 
        req.httpMethod = 'Post';
        req.addHeader('Content-Type', 'application/json');
        req.requestBody = Blob.valueof(json);
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
        NCPUpdatecontact.NCPCreateCase();
        Test.stopTest(); 
    }
}