@RestResource(urlMapping='/CreateSOPackage/*')
Global class CSICreateSOP {

    @Httppost
    global static void CSICreateSOP(){
        
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        Blob body = req.requestBody;
        res.addHeader('Content-Type', 'application/json');
        String requestString = body.toString();
        CSICreateSOPwra rw = (CSICreateSOPwra)JSON.deserialize(requestString,CSICreateSOPwra.class);
        try {
            Access_token__c At =[select status__c,Access_Token__c from Access_token__c where Access_Token__c=:rw.Accesstoken ];
            if( at.status__C =='Active'){
                list<Shipment_Order_Package__c> SOPs = new list<Shipment_Order_Package__c>();
              
               
                    for(Integer j=0;rw.sop.size()>j;j++){
                       Shipment_Order_Package__c SOP = new Shipment_Order_Package__c(
                            Shipment_Order__c = rw.SOID,
                            Weight_Unit__c= rw.sop[j].Weight_Unit,
                            Dimension_Unit__c = rw.sop[j].Dimension_Unit,
                            packages_of_same_weight_dims__c =rw.sop[j].Packages_of_Same_Weight,
                            //Length__c=decimal.Valueof(rw.sop[j].Length),
                            //Height__c=decimal.Valueof(rw.sop[j].Height),
                            //Breadth__c=decimal.Valueof(rw.sop[j].Breadth),
                            //Actual_Weight__c=decimal.Valueof(rw.sop[j].Actual_Weight),
                            
                            Length__c=rw.sop[j].Length,
                            Height__c=rw.sop[j].Height,
                            Breadth__c=rw.sop[j].Breadth,
                            Actual_Weight__c=rw.sop[j].Actual_Weight,
                            
                            Lithium_Batteries__c=rw.sop[j].LithiumBatteries !=null ? rw.sop[j].LithiumBatteries : false,
                            Contains_Batteries__c=rw.sop[j].Contains_Batteries !=null ? rw.sop[j].Contains_Batteries : false,
                            ION_PI966__c = rw.sop[j].ION_PI966 != null ? rw.sop[j].ION_PI966 : false,
                            ION_PI967__c = rw.sop[j].ION_PI967 != null ? rw.sop[j].ION_PI967 : false,
                            Metal_PI969__c = rw.sop[j].Metal_PI969 != null ? rw.sop[j].Metal_PI969 : false,
                            Metal_PI970__c = rw.sop[j].Metal_PI970 != null ? rw.sop[j].Metal_PI970 : false
                           
                           );
                            SOPs.add(SOP);
                        
                    }
                    
                
                
                insert SOPs;
                
                JSONGenerator gen = JSON.createGenerator(true);
                gen.writeStartObject();
                
                gen.writeFieldName('Success');
                gen.writeStartObject();
                
                if(SOPs.size()<1) {gen.writeNullField('Response');} else{gen.writeStringField('Response', 'Success-Shipment order package created');}
                
                gen.writeEndObject();
                gen.writeEndObject();
                String jsonData = gen.getAsString();
                res.responseBody = Blob.valueOf(jsonData);
                res.addHeader('Content-Type', 'application/json');
                res.statusCode = 200;        
                
                
                API_Log__c Al = New API_Log__c();
               // Al.Account__c=rw.AccountID;
                //Al.Login_Contact__c=rw.ContactID;
                Al.EndpointURLName__c='CSI-UpdateSOOrderPackage';
                Al.Response__c='Success- Shipment Order Package Updated';
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al; 
            }
        }
        catch (Exception e){
            String ErrorString ='Something went wrong while updating Shipment order packages details, please contact support_SF@tecex.com';
           // String ErrorString =e.getLineNumber()+e.getLineNumber()+e.getStackTraceString();
            res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
            res.addHeader('Content-Type', 'application/json');
            res.statusCode = 500;
            API_Log__c Al = New API_Log__c();
            //Al.Account__c=rw.AccountID;
            //Al.Login_Contact__c=rw.ContactID;
            Al.EndpointURLName__c='UpdateSOOrderPackage';
            Al.Response__c=e.getMessage()+' at line number: '+e.getLineNumber();
            Al.StatusCode__c=string.valueof(res.statusCode);
            Insert Al;
        }
    }
    
}