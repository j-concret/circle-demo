public class Controller_InvoicePage {

    public final Withholding_Tax__c a;
    public final Id currentID = ApexPages.currentPage().getParameters().get('aId');


    public Controller_InvoicePage() {

        a = [SELECT Client__c,Client__r.Name,Client__r.VAT_Number__c,Claim_Currency__c,Claim_Value__c,CPD__c,Disbursement__c,Disbursements__c,Exchange_Rate__c,FeeValue__c,Fee__c,Invoice_Date__c,
                    Invoice_Number__c,InvNumber1__c,Invoicing_Entity__c,Payment_Currency__c,Reference__c,Refund_Value__c,Sub_Total__c,Total__c,VATValue__c,VAT__c, Address__c, City__c, Country__c,
                    Postal_Code__c, Province__c,Invoicing_Entity_Address__c,Invoicing_Entity_City__c,Invoicing_Entity_Country__c,Invoicing_Entity_Postal_Code__c,Invoicing_Entity_Province__c,
                    Invoicing_Entity_VAT_Number__c,Bank_Charges__c, ClientAddress__c,Payable_to_Client__c,Refunded_In_PC__c,Beneficial_Owner__c,VATitFee__c
                    FROM Withholding_Tax__c 
                    WHERE Id=:currentID];
        
    }

    public Withholding_Tax__c getA() {
        return a;
    }
    
}