@isTest
public class ZCPFinaldeliveryAddress_Test {
    @testSetup
    public static void setup(){
        Account acc = new Account (name='TecEx E-Commerce Prospective Client', Type ='Client',New_Invoicing_Structure__c = true);
        insert acc;
        
        Access_token__c accessTokenObj = new Access_token__c();
        accessTokenObj.Status__c = 'Active';
        accessTokenObj.Access_Token__c = 'asdfghj-sdfgfds-sfdfgf';
        
        Insert accessTokenObj;
        
    }
    public static testMethod void testCase1(){
        Account Acc = [SELECT Id FROM Account where name='TecEx E-Commerce Prospective Client' LIMIT 1];
        Access_token__c accessToken = [SELECT Id,Access_Token__c FROM Access_token__c LIMIT 1];
        
        ZCPFinaldeliveryAddresswra obj = new ZCPFinaldeliveryAddresswra();
        obj.Accesstoken = accessToken.Access_Token__c;
        obj.AccountID = Acc.Id;
        obj.Shiptocountry = 'Brazil';
        
        Blob body = Blob.valueOf(JSON.serialize(obj));
        
        RestRequest req1 = new RestRequest();
        RestResponse res1 = new RestResponse();
        req1.requestURI = '/services/apexrest/GetZeeFinaldeliveryAddress'; 
        req1.httpMethod = 'POST';
        req1.requestBody = Blob.valueOf(JSON.serialize(obj));     
        RestContext.request = req1;
        RestContext.response = res1;
        RestContext.request.addHeader('Content-Type', 'application/json');
        
        Test.startTest(); 
        ZCPFinaldeliveryAddress.ZCPFinaldeliveryAddress();
        Test.stopTest();
    }
    
    public static testMethod void testCase2(){
        Account Acc = [SELECT Id FROM Account where name='TecEx E-Commerce Prospective Client' LIMIT 1];
        Access_token__c accessToken = [SELECT Id,Access_Token__c FROM Access_token__c LIMIT 1];
        
        ZCPFinaldeliveryAddresswra obj = new ZCPFinaldeliveryAddresswra();
        obj.Accesstoken = 'ABC';
        obj.AccountID = Acc.Id;
        obj.Shiptocountry = 'Brazil';
        
        Blob body = Blob.valueOf(JSON.serialize(obj));
        
        RestRequest req1 = new RestRequest();
        RestResponse res1 = new RestResponse();
        req1.requestURI = '/services/apexrest/GetZeeFinaldeliveryAddress'; 
        req1.httpMethod = 'POST';
        req1.requestBody = Blob.valueOf(JSON.serialize(obj));     
        RestContext.request = req1;
        RestContext.response = res1;
        RestContext.request.addHeader('Content-Type', 'application/json');
        
        Test.startTest(); 
        ZCPFinaldeliveryAddress.ZCPFinaldeliveryAddress();
        Test.stopTest();
    }
}