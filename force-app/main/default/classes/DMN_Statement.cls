/**
 * Created by Caro on 2019-04-08.
 */

public with sharing class DMN_Statement {

    public class AgingPeriod implements Comparable {
        public String periodLabel {get;set;}
        public Decimal periodAmount {get;set;}

        public AgingPeriod(String period, Decimal amount){
            this.periodAmount = amount;
            this.periodLabel = period;
        }

        public Integer compareTo(Object compareTo){

            AgingPeriod compareToEmp = (AgingPeriod)compareTo;
            if (periodLabel == compareToEmp.periodLabel) return 0;
            if (periodLabel > compareToEmp.periodLabel) return 1;
            return -1;

        }
    }

    public class StatementBody {

        public List<StatementLineItem> lineItems {get;set;}
        public Decimal statementTotal {
            get {
                if (statementTotal == null){
                    statementTotal = 0;
                    for (StatementLineItem lineItem : this.lineItems) {
                        statementTotal += lineItem.statementBalance;
                }
            }
            return statementTotal;
        } set;}
        public String statementType {get;set;}

    }

    public class StatementLineItem implements Comparable{
        public Date lineItemDate {get;set;}
        public String shipmentNumber {get;set;}
        public String receipt_InvoiceNumber {get;set;}
        public String purchaseOrderNumber {get;set;}
        public String shipToCountry {get;set;}
        public String lineItemType {get;set;}
        public Date dueDate {get;set;}
        public Decimal invoiceTotal {get;set;}
        public Decimal statementBalance {get;set;}

        public StatementLineItem(Date lineItemDate, String shipmentNumber, String receipt_InvoiceNumber, String purchaseOrderNumber, String shipToCountry, String lineItemType, Date dueDate, Decimal invoiceTotal, Decimal statementBalance){
            this.lineItemDate = lineItemDate;
            this.shipmentNumber = shipmentNumber;
            this.receipt_InvoiceNumber = receipt_InvoiceNumber;
            this.purchaseOrderNumber = purchaseOrderNumber == null ? '' : purchaseOrderNumber;
            this.shipToCountry = shipToCountry;
            this.lineItemType = lineItemType;
            this.dueDate = dueDate;
            this.invoiceTotal = invoiceTotal;
            this.statementBalance = statementBalance;

        }

        public Integer compareTo(Object compareTo){

            StatementLineItem compareToLineItem = (StatementLineItem)compareTo;
            if (shipmentNumber == compareToLineItem.shipmentNumber) {
                if (dueDate == compareToLineItem.dueDate) return 0;
                if (dueDate > compareToLineItem.dueDate) return 1;
            }
            if (shipmentNumber > compareToLineItem.shipmentNumber) return 1;
            return -11;

        }
    }

}