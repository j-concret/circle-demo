public class CaptureTaxCalculations {
    
    /** This method returns the List of Tax_Calculations for a particular shipmentOrderId **/
    public static List<Object> getTaxCalculation(Id ShipmentOrderId){
        Shipment_Order__c shipment_Order = [SELECT Id,(SELECT Name,Order_Number__c,Tax_Name__c,Tax_Type__c,Part_Specific__c,Applied_To_Value__c,Applied_to_Value_Type__c,Value__c,Rate__c FROM Tax_Calculations__r) FROM Shipment_Order__c where Id =:ShipmentOrderId];
        List<Object> taxCalcList = new List<Object>();
        for(Tax_Calculation__c tax_Calculation:shipment_Order.Tax_Calculations__r){
           taxCalcList.add(new Map<String,Object>{
                'TAX_CALCULATION_NAME'=> tax_Calculation.Name,
                    'ORDER'=>tax_Calculation.Order_Number__c,
                    'TAX_NAME'=>tax_Calculation.Tax_Name__c ,
                    'TAX_TYPE'=>tax_Calculation.Tax_Type__c,
                    'PART_SPECIFIC'=>tax_Calculation.Part_Specific__c,
                    'APPLIED_TO_TYPE'=>tax_Calculation.Applied_to_Value_Type__c,
                    'APPLIED_TO_VALUE'=>tax_Calculation.Applied_To_Value__c.setScale(2),
                    'VALUE'=>tax_Calculation.Value__c.setScale(2),
                    'RATE'=>tax_Calculation.Rate__c
                    });
        }
        return taxCalcList;    
    }
    
    /** returns the Map of Tax_Calculations to the lighning component **/
    @AuraEnabled
    public static Map<String,Object> getTaxCalc(Id recordId){
        Map<String,Object> responseMap = new Map<String,Object>();
        responseMap.put('status','OK');
        try{
            Customs_Clearance_Documents__c customs_Clearance_Documents = CCDListFiles.getCustomsClearanceDocument(recordId);
            List<Object> taxCalcList = getTaxCalculation(customs_Clearance_Documents.Customs_Clearance_Documents__r.Id);
            responseMap.put('data',taxCalcList);
        }catch(Exception e){
            responseMap.put('status','Error');  
            responseMap.put('message',e.getMessage()); 
        }
        
        return responseMap;
    }
    /** This Method returns the List of Part__c to the Tax_Calculations Table where Line_Item_Duty__c > 0 **/
     @AuraEnabled
    public static Map<String,Object> getParts(Id recordId){
        Map<String,Object> responseMap = new Map<String,Object>();
        responseMap.put('status','OK');
        try{
            Customs_Clearance_Documents__c customs_Clearance_Documents = CCDListFiles.getCustomsClearanceDocument(recordId);
            List<Part__c> partList = [SELECT Name ,US_HTS_Code__c,Matched_HS_Code2__c,Matched_HS_Code2__r.Name, Rate__c , Line_Item_Duty__c , Part_Specific_Rate_1__c , Part_Specific_Rate_2__c , Part_Specific_Rate_3__c , Part_Specific_Tax_1__c , Part_Specific_Tax_2__c , Part_Specific_Tax_3__c FROM Part__c where Shipment_Order__c=:customs_Clearance_Documents.Customs_Clearance_Documents__r.Id ];
            responseMap.put('data',partList);
        }catch(Exception e){
            responseMap.put('status','Error');  
            responseMap.put('message',e.getMessage()); 
        }
        
        return responseMap;
    }
}