@RestResource(urlMapping='/NCPAddProductCatelog/*')
Global class NCPAddProductCatelog {
 @Httppost
    global static void NCPSODetials(){
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        Blob body = req.requestBody;
    	res.addHeader('Content-Type', 'application/json');
        String requestString = body.toString();
        NCPAddProductCatelogWra rw  = (NCPAddProductCatelogWra)JSON.deserialize(requestString,NCPAddProductCatelogWra.class);
        {
            try{
             List<Access_token__c> at = [SELECT status__c,Access_Token__c FROM Access_token__c WHERE Access_Token__c =: rw.Accesstoken and status__c = 'Active' LIMIT 1];

            if(!at.isEmpty()) {
                   Account   Ac = [Select Id,Service_Manager__c,Compliance__c from Account where id =:rw.AccountID limit 1];
                  Id CPrecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('ClientPlatform').getRecordTypeId();
                 
                
                 Case cs = new case();
                    CS.RecordTypeId=CPrecordTypeId;
                    cs.Subject = 'lineitems to add in product catelog';
                    cs.Reason = 'Client Portal';
                    cs.Description ='Please find attaced file in attachements to add lineitems in client product catelog - Client account record ID: '+rw.AccountID;
                    cs.Status = 'new';
                    cs.AccountId=rw.AccountID;
                    cs.Contactid=rw.ContactId;
                    cs.Assigned_to__c=Ac.Compliance__c;
                 //	cs.Linked_Record_Id__c=rw.AccountID;
                 cs.Link_to_Record__c = 'https://tecex.lightning.force.com/'+rw.AccountID;
                    	
                
                insert cs;
                
                 			if(!rw.LineItems.isempty()){
                                        list<Attachment> atts=new List<Attachment>();
                                        For(Integer i=0;rw.LineItems.size()>i;i++){
                                            Attachment a = new Attachment (ParentId = cs.id, Body =rw.LineItems[i].FileBody,                                                     
                                            Name =rw.LineItems[i].FileName,contenttype=rw.LineItems[i].FileType);
                                             if(rw.LineItems[i].FileType != 'application/x-msdos-program'){
                                                atts.add(a);
                                            }
                                        }
                                        insert atts; 
                                   } 
                
                 	case cs1 =[Select Id,CaseNumber from case where id=:cs.Id];
                  	JSONGenerator gen = JSON.createGenerator(true);
            		gen.writeStartObject();
            
             		gen.writeFieldName('Success');
             		gen.writeStartObject();
            
             			if(cs.id == null) {gen.writeNullField('Response');} else{gen.writeStringField('Response','Case created successfully. Casse Reference number is '+cs1.CaseNumber);}
            
            		gen.writeEndObject();
            		gen.writeEndObject();
            		String jsonData = gen.getAsString();
            		res.responseBody = Blob.valueOf(jsonData);
            		res.addHeader('Content-Type', 'application/json');
            		res.statusCode = 200;    
                API_Log__c Al = New API_Log__c();
                Al.EndpointURLName__c='NCPAddProductCatelog - Casecreation';
                Al.StatusCode__c=string.valueof(res.statusCode);
                Al.Response__c = 'Created case' +cs.id;
                Insert Al;
                
                
            }
                
                
            }
            catch(Exception e){
                 String ErrorString ='Something went wrong, please contact Sfsupport@tecex.com';
               		res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
            		res.addHeader('Content-Type', 'application/json');
        			res.statusCode = 400;
                
                API_Log__c Al = New API_Log__c();
                Al.Account__c=rw.AccountID;
                Al.Login_Contact__c=rw.contactId;
                Al.EndpointURLName__c='NCPAddProductCatelog';
                Al.Response__c=e.getMessage()+' at line number: '+e.getLineNumber();
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al;
                
            }
            
            
        }
    
    }
      
    
}