public class NCPDefaultupdatewrapper {
   public List<DefaultPref> DefaultPref;

	public class DefaultPref {
        public String CPDefaultRecordID;
		public String ClientID;
		public String Chargeable_Weight_Units;
		public String ion_Batteries;
		public String Order_Type;
		public String Package_Dimensions;
		public String Package_Weight_Units;
		public String Second_hand_parts;
		public String Service_Type;
		public String TecEx_to_handle_freight;
        public String Country;
        public String DefaultCurrency;
        
	}
   
}