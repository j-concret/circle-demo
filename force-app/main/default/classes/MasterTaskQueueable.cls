public class MasterTaskQueueable implements Queueable {
    Set<Id> soidList = new Set<Id>();
    public MasterTaskQueueable(Set<Id> soids){
        this.soidList = soids;
    }
    public void execute(QueueableContext context) {
        Map<Id,Shipment_Order__c> soMap = new Map<Id,Shipment_Order__c>([SELECT Id,TP_Halt__c, TP_Halt_Start_Date__c, Total_TP_Halt_Days__c, Total_TP_Halt_Business_Days__c,Total_Halt_Business_Days__c,Sub_Status_Update__c,Customs_Cleared_Date__c, Arrived_in_Customs_Date__c,Final_Delivery_Lead_Time_Decimal__c,
                                                                         Customs_Clearance_Lead_Time_Decimal__c,Hub_Lead_Time_Decimal__c,Transit_to_Destination_Lead_Time_Decimal__c,
                                                                         of_Line_Items__c,banner_feed__c,Clearance_Destination__c,Ship_to_Country_new__c,Invoice_Timing__c,
                                                                         Shipping_Status__c, Halt__c, Halt_Start_Date__c, Total_Halt_Days__c, Expected_Date_to_Next_Status__c,
                                                                         In_Transit_Date__c,cost_Estimate_acceptance_date__c,CPA_v2_0__r.Final_Destination__c,
                                                                         CPA_v2_0__r.Tracking_Term__c,Client_Contact_for_this_Shipment__r.Client_Notifications_Choice__c,
                                                                         Client_Contact_for_this_Shipment__r.Major_Minor_notification__c,
                                                                         Client_Contact_for_this_Shipment__r.Client_Tasks_notification_choice__c,
                                                                         Shipment_Order_Compliance_Approval_Time__c,Shipment_Order_Compliance_Time_Decimal__c,
                                                                         Date_Of_New_CP__c,Invoice_Payment__c,Pick_up_Coordination__c,Shipping_Documents__c,
                                                                         Customs_Compliance__c,Mapped_Shipping_status__c,Hub_Lead_Time__c,Transit_to_Destination_Lead_Time__c,
                                                                         Customs_Clearance_Lead_Time__c,Final_Delivery_Lead_Time__c,CPA_v2_0__r.Comments_On_Why_No_Final_Quote_Possible__c,
                                                                         CPA_v2_0__c,(SELECT Pickup_availability_Close__c from Freight__r WHERE Pickup_availability_Close__c != NULL)
                                                                         FROM Shipment_Order__c WHERE Id IN: soidList]);
        ShipmentOrderTriggerHandler.processAfterUpdate(soMap);
    }
}