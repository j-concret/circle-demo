public with sharing class DataCenterAndFTZCtrl {
    /*
* @description fetch the list of FInal Delivery records and related CLient address.
* @param  soId  Id of the shipment order. 
* @return List<Final_Delivery__c>
*/
    @AuraEnabled
    public static List<Final_Delivery__c> getFinalDeliveries(String soId){
        try{
            return [SELECT Id, Name, Ship_to_address__r.CompanyName__c, Ship_to_address__r.Address__c, Ship_to_address__r.Address2__c, Ship_to_address__r.City__c, Ship_to_address__r.Province__c, Ship_to_address__r.Country__c, Ship_to_address__r.Address_is_a_Datacenter__c , Ticket_Number__c,Ship_to_address__r.Address_is_in_a_FTZ__c,Shipment_Order__r.CPA_v2_0__r.Free_Trade_Zone__c FROM Final_Delivery__c WHERE Shipment_Order__c =: soid];
        }catch(Exception e) {throw new AuraHandledException(e.getMessage());
                            }
    }
    
    /*
* @description update the Final_Delivery__c records related CLient address.
* @param  finalDeliveries  List of Final Delivery Address to update
* @param  componentType  String to identify if compoenent is FTZ or Data Center
*/
    @AuraEnabled
    public static void updateFinalDeliveries(List<Final_Delivery__c> finalDeliveries,String componentType){
        List<Client_Address__c> listClientAddresses =  new List<Client_Address__c>();
        String soId= finalDeliveries[0].Shipment_Order__c;
        
        for(Final_Delivery__c objFinalDelivery:finalDeliveries){

            Client_Address__c objClinetAddress = New Client_Address__c();
            objClinetAddress.id= objFinalDelivery.Ship_to_address__c;
            if(componentType == 'Data Center'){
                objClinetAddress.Address_is_a_Datacenter__c= objFinalDelivery.Ship_to_address__r.Address_is_a_Datacenter__c;
            }else{
                objClinetAddress.Address_is_in_a_FTZ__c= objFinalDelivery.Ship_to_address__r.Address_is_in_a_FTZ__c;
            }
            
            listClientAddresses.add(objClinetAddress);
        }
        
        try {
            update listClientAddresses;
        } catch (Exception e) { throw new AuraHandledException(e.getMessage());
                              }
        
        if(componentType == 'Data Center'){
            try {
                update finalDeliveries;
            } catch (Exception e) { throw new AuraHandledException(e.getMessage());
                                  }
        }else{
            update new Shipment_Order__c(id=soId);        
        }        

    }
}