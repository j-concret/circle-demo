@IsTest
public class ChildContryComplaince_test {
			static testMethod void  setUpData(){
			
             Country__c country = new Country__c(Brokerage_Costs__c = 618, CIF_Adjustment_Factor__c = 0.035, CIF_Absolute_value_adjustment__c = 200, Clearance_Costs__c = 488, 
                                            Country__c = 'Brazil', Name = 'Brazil', Handling_Costs__c = 1180, License_Permit_Costs__c = 0);
        	insert country;
                
                Compliance_Document__c CCD = new Compliance_Document__c(Name__c='BIS',Country__c=country.Id,Compliance_type__c='Document');
                Insert CCD;
                String PID = CCD.ID;
                ChildContryComplaince.getcomplaincedocument(PID);
                
            }
    	   
        static testMethod void  setUpData1(){
			
             Country__c country = new Country__c(Brokerage_Costs__c = 618, CIF_Adjustment_Factor__c = 0.035, CIF_Absolute_value_adjustment__c = 200, Clearance_Costs__c = 488, 
                                            Country__c = 'Brazil', Name = 'Brazil', Handling_Costs__c = 1180, License_Permit_Costs__c = 0);
        	 insert country;
                
                Compliance_Document__c CCD = new Compliance_Document__c(Name__c='BIS',Country__c=country.Id,Compliance_type__c='Document');
                Insert CCD;
              	HS_Codes_and_Associated_Details__c HSC = new HS_Codes_and_Associated_Details__c(Name='95865858585',Country__c=country.Id);
                Insert HSC;
            
            
            
                String PID = CCD.ID;
            String PName = 'asdfff';
            
                list<Country_Compliance__c> CCOM = new list<Country_Compliance__c>();
            		CCOM.add(new Country_Compliance__c(Country_Compliance__c=CCD.Id,Name='asdf'));
            		CCOM.add(new Country_Compliance__c(Country_Compliance__c=CCD.Id,Name='asdf',HS_Code_and_Associated_Details__c=HSC.ID));
            		CCOM.add(new Country_Compliance__c(Country_Compliance__c=CCD.Id,Name='asdf'));
            
            ChildContryComplaince.CreateCC(CCOM,PID,PName);
                
            }
    }