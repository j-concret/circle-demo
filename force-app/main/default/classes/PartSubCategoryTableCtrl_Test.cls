@isTest(SeeAllData=false)
public class PartSubCategoryTableCtrl_Test {

    @Testsetup
    static void setupData(){

        List<Account> accounts = new List<Account>();
        accounts.add(new Account (name='TecEx Prospective Client', Type ='Client',
                                  CSE_IOR__c = '0050Y000001LTZO',
                                  Service_Manager__c = '0050Y000001LTZO',
                                  Financial_Manager__c='0050Y000001LTZO',
                                  Financial_Controller__c='0050Y000001LTZO',
                                  Tax_recovery_client__c  = FALSE,
                                  Ship_From_Line_1__c = 'Ship_From_Line_1',
                                  Ship_From_Line_2__c = 'Ship_From_Line_2',
                                  Ship_From_Line_3__c = 'Ship_From_Line_3',
                                  Ship_From_City__c = 'Ship_From_City',
                                  Ship_From_Zip__c = 'Ship_From_Zip',
                                  Ship_From_Country__c = 'Ship_From_Country',
                                  Ship_From_Other_notes__c = 'Ship_From_Other',
                                  Ship_From_Contact_Name__c = 'Ship_From_Contact',
                                  Ship_From_Contact_Email__c = 'Ship_From_Email',
                                  Ship_From_Contact_Tel__c = 'Ship_From_Tel',
                                  Client_Address_Line_1__c = 'Client_Address_Line_1',
                                  Client_Address_Line_2__c = 'Client_Address_Line_2',
                                  Client_Address_Line_3__c = 'Client_Address_Line_3',
                                  Client_City__c = 'Client_City',
                                  Client_Zip__c = 'Client_Zip',
                                  Client_Country__c = 'Client_Country',
                                  Other_notes__c = 'Other_notes',
                                  Tax_Name__c = 'Tax_Name',
                                  Tax_ID__c = 'Tax_ID',
                                  Contact_Name__c = 'Contact_Name',
                                  Contact_Email__c = 'Contact_Email',
                                  Contact_Tel__c = 'Contact_Tel'
                                  ));
        accounts.add(new Account (name='Test Supplier', Type ='Supplier', ICE__c = '0050Y000001km5c', RecordTypeId = '0120Y0000006KjQQAU'));
        accounts.add(new Account (name='Test Manufacturer', Type ='Manufacturer', RecordTypeId = '0120Y0000002wa0QAA', Manufacturer_Alias__c = 'Tester,Who'));
        insert accounts;

        Contact contact = new Contact ( lastname ='Testing Individual',Email='test@dfsd.com', AccountId = accounts[0].Id, Client_Notifications_Choice__c ='Opt-In');
        insert contact;

        IOR_Price_List__c iorpl = new IOR_Price_List__c ( Client_Name__c = accounts[0].Id, Name = 'Brazil', IOR_Fee__c = 0.15, On_Charge_Mark_up__c = 0.15,
                                                          TecEx_Shipping_Fee_Markup__c =0.40, IOR_Min_Fee__c = 100,  Admin_Fee__c =100, Set_up_fee__c =100, Bank_Fees__c =200, Tax_Rate__c = 0.20,
                                                          Estimated_Customs_Brokerage__c =250, Estimated_Customs_Clearance__c =250, Estimated_Customs_Handling__c =250, Estimated_Import_License__c =250, Destination__c = 'Brazil' );
        insert iorpl;

        country_price_approval__c cpa = new country_price_approval__c( name ='Brazil', Billing_term__c = 'DAP/CIF - IOR pays',  Airwaybill_Instructions__c = 'User IOR Address', Supplier__c = accounts[1].Id,
                                                                       In_Country_Specialist__c = '0050Y000001km5c', Clearance_Destination__c = 'Brazil', Preferred_Supplier__c = TRUE, Destination__c = 'Brazil' );
        insert cpa;

        Currency_Management2__c CM = new Currency_Management2__c (Name = 'US Dollar (USD)', Currency__c = 'US Dollar (USD)', ISO_Code__c = 'USD', Conversion_Rate__c = 1);
        insert CM;

        Country__c country = new Country__c(Brokerage_Costs__c = 618, CIF_Adjustment_Factor__c = 0.035, CIF_Absolute_value_adjustment__c = 200, Clearance_Costs__c = 488,
                                            Country__c = 'Brazil', Name = 'Brazil', Handling_Costs__c = 1180, License_Permit_Costs__c = 0);
        insert country;

        Tax_Structure_Per_Country__c tax = new Tax_Structure_Per_Country__c(Applied_to_Value__c = 'CIF', Country__c = country.Id, Default_Duty_Rate__c = 0.5625,
                                                                            Order_Number__c = '1', Part_Specific__c = TRUE, Name = 'II', Tax_Type__c = 'Duties');
        insert tax;

        CPA_v2_0__c RelatedCPA = new CPA_v2_0__c(Name = 'Brazil', Country_Applied_Costings__c=true,CPA_Minimum_Brokerage_Surcharge__c=10 );
        insert RelatedCPA;
        CPA_v2_0__c cpav2 = new CPA_v2_0__c(Name = 'Brazil Testing 2.0', CPA_Minimum_Brokerage_Surcharge__c=10,Supplier__c = accounts[1].Id, Service_Type__c = 'IOR/EOR', Related_Costing_CPA__c = RelatedCPA.Id, Preferred_Supplier__c = TRUE,
                                            VAT_Rate__c = 0.1, Destination__c = 'Brazil',

                                            Default_Section_1_Address_Line_1__c= 'Address_Line_1',
                                            Default_Section_1_Address_Line_2__c= 'Address_Line_2',
                                            Default_Section_1_Address_Line_3__c= 'Address_Line_3',
                                            Default_Section_1_City__c= 'City',
                                            Default_Section_1_Zip__c= 'Zip',
                                            Default_Section_1_Country__c = 'Country',
                                            Default_Section_1_Other__c= 'Other',
                                            Default_Section_1_Tax_Name__c= 'Tax_Name',
                                            Default_Section_1_Tax_Id__c= 'Tax_Id',
                                            Default_Section_1_Contact_Name__c= 'Contact_Name',
                                            Default_Section_1_Contact_Email__c= 'Contact_Email',
                                            Default_Section_1_Contact_Tel__c= 'Contact_Tel',
                                            Alternate_Section_1_Address_Line_1__c= 'Address_Line_1',
                                            Alternate_Section_1_Address_Line_2__c= 'Address_Line_2',
                                            Alternate_Section_1_Address_Line_3__c= 'Address_Line_3',
                                            Alternate_Section_1_City__c= 'City',
                                            Alternate_Section_1_Zip__c= 'Zip',
                                            Alternate_Section_1_Country__c = 'Country',
                                            Alternate_Section_1_Other__c = 'Other',
                                            Alternate_Section_1_Tax_Name__c = 'Tax_Name',
                                            Alternate_Section_1_Tax_Id__c = 'Tax_Id',
                                            Alternate_Section_1_Contact_Name__c= 'Contact_Name',
                                            Alternate_Section_1_Contact_Email__c= 'Contact_Email',
                                            Alternate_Section_1_Contact_Tel__c= 'Contact_Tel',
                                            Default_Section_2_Address_Line_1__c= 'Address_Line_1',
                                            Default_Section_2_Address_Line_2__c= 'Address_Line_2',
                                            Default_Section_2_Address_Line_3__c= 'Address_Line_3',
                                            Default_Section_2_City__c= 'City',
                                            Default_Section_2_Zip__c= 'Zip',
                                            Default_Section_2_Country__c = 'Country',
                                            Default_Section_2_Other__c= 'Other',
                                            Default_Section_2_Tax_Name__c= 'Tax_Name',
                                            Default_Section_2_Tax_Id__c= 'Tax_Id',
                                            Default_Section_2_Contact_Name__c= 'Contact_Name',
                                            Default_Section_2_Contact_Email__c= 'Contact_Email',
                                            Default_Section_2_Contact_Tel__c= 'Contact_Tel',
                                            Alternate_Section_2_Address_Line_1__c= 'Address_Line_1',
                                            Alternate_Section_2_Address_Line_2__c= 'Address_Line_2',
                                            Alternate_Section_2_Address_Line_3__c= 'Address_Line_3',
                                            Alternate_Section_2_City__c= 'City',
                                            Alternate_Section_2_Zip__c= 'Zip',
                                            Alternate_Section_2_Country__c = 'Country',
                                            Alternate_Section_2_Other__c = 'Other',
                                            Alternate_Section_2_Tax_Name__c = 'Tax_Name',
                                            Alternate_Section_2_Tax_Id__c = 'Tax_Id',
                                            Alternate_Section_2_Contact_Name__c= 'Contact_Name',
                                            Alternate_Section_2_Contact_Email__c= 'Contact_Email',
                                            Alternate_Section_2_Contact_Tel__c= 'Contact_Tel',
                                            Default_Section_3_Address_Line_1__c= 'Address_Line_1',
                                            Default_Section_3_Address_Line_2__c= 'Address_Line_2',
                                            Default_Section_3_Address_Line_3__c= 'Address_Line_3',
                                            Default_Section_3_City__c= 'City',
                                            Default_Section_3_Zip__c= 'Zip',
                                            Default_Section_3_Country__c = 'Country',
                                            Default_Section_3_Other__c= 'Other',
                                            Default_Section_3_Tax_Name__c= 'Tax_Name',
                                            Default_Section_3_Tax_Id__c= 'Tax_Id',
                                            Default_Section_3_Contact_Name__c= 'Contact_Name',
                                            Default_Section_3_Contact_Email__c= 'Contact_Email',
                                            Default_Section_3_Contact_Tel__c= 'Contact_Tel',
                                            Alternate_Section_3_Address_Line_1__c= 'Address_Line_1',
                                            Alternate_Section_3_Address_Line_2__c= 'Address_Line_2',
                                            Alternate_Section_3_Address_Line_3__c= 'Address_Line_3',
                                            Alternate_Section_3_City__c= 'City',
                                            Alternate_Section_3_Zip__c= 'Zip',
                                            Alternate_Section_3_Country__c = 'Country',
                                            Alternate_Section_3_Other__c = 'Other',
                                            Alternate_Section_3_Tax_Name__c = 'Tax_Name',
                                            Alternate_Section_3_Tax_Id__c = 'Tax_Id',
                                            Alternate_Section_3_Contact_Name__c= 'Contact_Name',
                                            Alternate_Section_3_Contact_Email__c= 'Contact_Email',
                                            Alternate_Section_3_Contact_Tel__c= 'Contact_Tel',
                                            Default_Section_4_Address_Line_1__c= 'Address_Line_1',
                                            Default_Section_4_Address_Line_2__c= 'Address_Line_2',
                                            Default_Section_4_Address_Line_3__c= 'Address_Line_3',
                                            Default_Section_4_City__c= 'City',
                                            Default_Section_4_Zip__c= 'Zip',
                                            Default_Section_4_Country__c = 'Country',
                                            Default_Section_4_Other__c= 'Other',
                                            Default_Section_4_Tax_Name__c= 'Tax_Name',
                                            Default_Section_4_Tax_Id__c= 'Tax_Id',
                                            Default_Section_4_Contact_Name__c= 'Contact_Name',
                                            Default_Section_4_Contact_Email__c= 'Contact_Email',
                                            Default_Section_4_Contact_Tel__c= 'Contact_Tel',
                                            Alternate_Section_4_Address_Line_1__c= 'Address_Line_1',
                                            Alternate_Section_4_Address_Line_2__c= 'Address_Line_2',
                                            Alternate_Section_4_Address_Line_3__c= 'Address_Line_3',
                                            Alternate_Section_4_City__c= 'City',
                                            Alternate_Section_4_Zip__c= 'Zip',
                                            Alternate_Section_4_Country__c = 'Country',
                                            Alternate_Section_4_Other__c = 'Other',
                                            Alternate_Section_4_Tax_Name__c = 'Tax_Name',
                                            Alternate_Section_4_Tax_Id__c = 'Tax_Id',
                                            Alternate_Section_4_Contact_Name__c= 'Contact_Name',
                                            Alternate_Section_4_Contact_Email__c= 'Contact_Email',
                                            Alternate_Section_4_Contact_Tel__c= 'Contact_Tel',
                                            VAT_Reclaim_Destination__c = FALSE,
                                            Final_Destination__c ='Brazil',
                                            Country__c = country.id

                                            );
        insert cpav2;

        List<CPARules__c> CPARules = new List<CPARules__c>();
        CPARules.add(new CPARules__c(Country__c = 'Brazil',Criteria__c ='Default', Chargable_weight__c = 'NONE', Courier_Responsibility__c = 'NONE',   ECCN_No__c = 'NONE', NL_Product_User__c = 'NONE', Number_of_final_deliveries__c = 'NONE', ShipmentValue__c ='NONE', Sum_of_quantity_of_matched_products__c = 'NONE', Manufacturer__c = 'NONE', DefaultCPA2__c = cpav2.Id, CPA2__c =  cpav2.Id));
        insert CPARules;

        List<CPA_Costing__c> costs = new List<CPA_Costing__c>();
        costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'Shipment value', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'IOR', Min__c = 375, Max__c = 14000, Conditional_value__c = 'Shipment value', Condition__c = 'Between', Floor__c = 0, Ceiling__c = 10000, Currency__c = 'US Dollar (USD)'));
        costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'Anil', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'IOR', Currency__c = 'US Dollar (USD)',Variable_threshold__c = null));
        insert costs;


        List<Product2> products = new List<Product2>();
        products.add(new Product2(Name = 'CAB-ETH-S-RJ45', US_HTS_Code__c='85444210',description='Anil', ProductCode = 'CAB-ETH-S-RJ45', Manufacturer__c = accounts[2].Id));
        products.add(new Product2(Name = 'CAB-ETH-S-RJ46',US_HTS_Code__c='85444211', ProductCode = 'CAB-ETH-S-RJ46', Manufacturer__c = accounts[2].Id));
        products.add(new Product2(Name = 'CAB-ETH-S', US_HTS_Code__c='834512879',ProductCode = 'CAB-ETH-S', Manufacturer__c = accounts[2].Id));
        insert products;

        Master_Task__c masterTask = new Master_Task__c(Name = 'Demo Task', States__c = 'TecEx Pending;Resolved',
                                                       Operational__c = true, Condition__c = '{0} > 1',
                                                       Condition_Fields_API_Name__c = 'Part__c.Quantity__c',
                                                       Assess_Condition_on_Creation_of__c = 'Shipment_Order__c',Ship_To_Country__c = 'All');
        Insert masterTask;


        Shipment_Order__c shipmentOrder = new Shipment_Order__c(Account__c = accounts[0].id, Shipment_Value_USD__c = 6000, CPA_v2_0__c = cpav2.Id,IOR_CSE__c = '0050Y000001km5c', ICE__c = '0050Y000001km5c', Financial_Controller__c = '0050Y000001km5c',
                                                                Client_Contact_for_this_Shipment__c = contact.Id, Who_arranges_International_courier__c ='Client', Tax_Treatment__c ='DAP/CIF - IOR pays',
                                                                Ship_to_Country__c = cpa.Id, Destination__c = 'Brazil', Service_Type__c = 'IOR', IOR_Price_List__c = iorpl.Id, Chargeable_Weight__c = 200,
                                                                Shipping_Status__c = 'Cost Estimate',CPA2_Override__c=true);
        RecusrsionHandler.taskTriggerRun = true;
        insert shipmentOrder;
        insert new Task(Subject=masterTask.Name,Shipment_Order__c=shipmentOrder.Id,whatID = shipmentOrder.Id,Status = 'Not Started',
                            Auto_Resolved__c = false,Master_Task__c = masterTask.Id,IsNotApplicable__c = false); 


        List<Part__c> partList = new List<Part__c>();

        partList.add(new Part__c(Name ='CAB-ETH-S-RJ45',  Quantity__c= 3, Product__c=products[0].Id, Commercial_Value__c = 15000, US_HTS_Code__c = '85444210',
                                 Country_of_Origin2__c = 'China', Shipment_Order__c = shipmentOrder.Id, Description_and_Functionality__c ='Anil'));

        
        insert partList;


    }

    static testMethod void getPartsTest(){
        Test.startTest();
        Shipment_Order__c shipmentOrder = [SELECT Account__c, Shipment_Value_USD__c, CPA_v2_0__c,
                                           Client_Contact_for_this_Shipment__c, Who_arranges_International_courier__c, Tax_Treatment__c,
                                           Ship_to_Country__c, Destination__c, Service_Type__c, IOR_Price_List__c, Chargeable_Weight__c,
                                           Shipping_Status__c FROM Shipment_Order__c LIMIT 1];
        Task tsk = [SELECT Id FROM Task WHERE Shipment_Order__c =:shipmentOrder.Id limit 1];
        Map<String,Object> rPart = PartSubCategoryTableCtrl.getParts(shipmentOrder.Id,tsk.Id);
        System.assert (((List<Part__c>)rPart.get('parts')).size() > 0);
        List<Part__c> partList = (List<Part__c>)rPart.get('parts');
        partList[0].Name = 'CAB-ETH-S';
        PartSubCategoryTableCtrl.updatePartCategory(partList);
        Test.stopTest();
    }

}