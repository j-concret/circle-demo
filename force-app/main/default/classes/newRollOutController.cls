public class newRollOutController {
 
 Public Roll_Out__c rollout = new Roll_Out__c ();
 public Boolean showMCSV{get;set;}
 public Roll_Out__c getrollout()
    {
        return rollout;
    }
   public ApexPages.StandardController controller; 
   
 public newRollOutController (ApexPages.StandardController Controller)
    {
        rollout = (Roll_Out__c)Controller.getRecord(); 
        this.controller=Controller;
        showMCSV= False; 
    }   
    
    public void method1(){
         if(showMCSV == True){
             showMCSV= False;
         }else{
             showMCSV= True;
         }      
    }
    
   
 public PageReference MCSV(){
 rollout.Create_Roll_Out__c = True;
 Upsert rollout;
 rollout.Destinations__c = '';
 rollout.Shipment_Value_in_USD__c = 0;
 rollout.Client_Reference__c = '';
  Update rollout;
 
    PageReference opportunityPage = new ApexPages.StandardController(rollout).view();
            opportunityPage.setRedirect(true);
            return opportunityPage; 


    
    }
    
     public PageReference SaveAndNew(){
     rollout.Create_Roll_Out__c = True;
     Upsert rollout;
     rollout.Destinations__c = '';
     rollout.Shipment_Value_in_USD__c = 0;
     rollout.Client_Reference__c = '';
     Update rollout;
     PageReference opportunityPage = new PageReference('/apex/NewRollOutPage?id='+rollout.Id);
     opportunityPage.setRedirect(False);
     return opportunityPage; 


    
    }
    
public PageReference MCVV(){

Upsert rollout;
PageReference pageRef = new PageReference('/apex/uploadCSVCostEstimate2?id='+rollout.Id);
            return pageRef; 



    
    }
 


}