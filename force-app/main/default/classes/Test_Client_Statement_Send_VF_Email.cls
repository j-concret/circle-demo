@isTest(SeeAllData=true)
private class Test_Client_Statement_Send_VF_Email 
{
    private static String CRON_EXP = '0 0 0 15 3 ? 2022';
    @isTest static void myTest() 
    {
        Test.startTest();

         Client_Statement_Send_VF_Email testClass = new Client_Statement_Send_VF_Email();

        // Schedule the test job
        
        String jobId = System.schedule('ScheduleApexClassTest',
                        CRON_EXP, testClass);
        
        // Get the information from the CronTrigger API object
      CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, 
         NextFireTime
         FROM CronTrigger WHERE id = :jobId];

      // Verify the expressions are the same
      System.assertEquals(CRON_EXP, 
         ct.CronExpression);

      // Verify the job has not run
      System.assertEquals(0, ct.TimesTriggered);

      // Verify the next time the job will run
      System.assertEquals('2022-03-15 00:00:00', 
         String.valueOf(ct.NextFireTime));

      // Verify the scheduled job hasn't run yet.
      Integer beforeInvocations = Limits.getEmailInvocations();
      System.assertEquals(0,beforeInvocations,'no email sent');
        
        Test.stopTest();
    }

}