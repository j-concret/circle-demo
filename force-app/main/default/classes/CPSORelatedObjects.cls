@RestResource(urlMapping='/SOAllRelatedObjects/*')
Global class CPSORelatedObjects {

    @Httpget
    global static void SORelatedObjects(){
        
        String jsonData='Test';
        RestRequest req = RestContext.request;
    	RestResponse res = RestContext.response;
     	res.addHeader('Content-Type', 'application/json');
        String Username = req.params.get('Username');
        String Password = req.params.get('Password');
        String SOID = req.params.get('SOID');
       	Contact logincontact = [select Id,Account.id,lastname,Email,Password__c,UserName__c,Contact_Status__c from Contact where UserName__c =:username and Contact_status__c = 'Active' ];
        try {
            
             if(logincontact.Password__c == Password && logincontact.UserName__c == Username && logincontact.Contact_status__C == 'Active' ){
            
                 //shipment_order__c SOs = [Select Account__r.Name,Ship_to_Country__r.Name,Client_Contact_for_this_Shipment__r.Name,of_Unique_Line_Items__c,Account__c,Account_frozen__c,Accounting_Seed_Net_Equity__c,Accounting_Seed_Net_Profit__c,Actual_Admin_Fee__c,Actual_Bank_Fees__c,Actual_Cash_Outlay_Fee__c,Actual_Duties_and_Taxes_Other__c,Actual_Exchange_gain_loss_on_payment__c,Actual_Finance_Fee__c,Actual_Insurance_Fee_USD__c,Actual_International_Delivery_Fee__c,Actual_Liability_Cover__c,Actual_Miscellaneous_Fee__c,Cash_Net_Profit__c,Total_Supplier_Cost__c,Actual_Taxes_Forex_Spread__c,Actual_Tax_Recovery__c,Actual_Total__c,Actual_Total_Clearance_Costs__c,Actual_Total_Customs_Brokerage_Cost__c,Actual_Total_Formula_field__c,Actual_Total_Handling_Costs__c,Actual_Total_IOR_EOR__c,Actual_Total_License_Cost__c,Actual_Total_Tax_and_Duty__c,Actual_Weight_KGs__c,Number_Delivered__c,Additional_Parts__c,Add_Supplier_Invoice_Costings__c,Handling_and_Admin_Fee__c,Airwaybill_Instructions__c,All_Taxes_and_Duties_Summary__c,IOR_CSE__c,AM_Incentive_Date__c,Amount_Receipted__c,Applied_discount__c,Arrived_at_Hub__c,Arrived_in_Customs_Date__c,Awaiting_export_license__c,AWB_consign_to__c,AWB_Type__c,Balance_outstanding__c,Bank_Account__c,Bank_Cost_b__c,Bank_Fees__c,Beneficial_Owner_Company_Address__c,Beneficial_Owner_Company_Name__c,Contact_email__c,Contact_name__c,Contact_number__c,Blank_Space__c,Branch__c,Calculated_Hub_Shipping_Cost__c,Calculated_International_Delivery_Cost__c,Calculate_quoted_fields__c,Calculate_Taxes__c,CasesafeId__c,Cash_Outlay_Cost__c,Finance_Fee__c,Finance_fee_percentage__c,Finance_Fee_Formula__c,Cash_Outlay_Fee_Profit__c,Chargeable_Weight__c,Total_Chargeable_Weight_as_per_Final_AWB__c,CI_Admin_Fee__c,CI_Bank_Fees__c,CI_Cash_Outlay_Fee__c,CI_Collection_Administration_Fee__c,CI_delivery_address__c,CI_Finance_Fee__c,CI_Insurance_Fee_USD__c,CI_International_Delivery_Fee__c,CI_IOR_and_Import_Compliance_Fee_USD__c,CI_Liability_Cover__c,CI_Miscellaneous_Fee__c,CI_Recharge_Tax_and_Duty__c,CI_Tax_Recovery__c,CI_Total__c,CI_Total_Clearance_Costs__c,CI_Total_Customs_Brokerage_Cost__c,CI_Total_Handling_Cost__c,CI_Total_Licence_Cost__c,Customs_Cleared_Date__c,Client_Contact_for_this_Shipment__c,Client_Payment_Received__c,Client_Reference__c,Client_Reference_2__c,Closed_Down_Date__c,Closed_Down_SO__c,Collection_Administration_Fee__c,Collection_Administration_Profit__c,Commercial_Invoice_Instructions__c,Confirm_Liability_Cover_Decline__c,ContactEmail__c,Cost_Estimate__c,Cost_Estimate_Accepted__c,Cost_Estimate_attached__c,Cost_Estimate_Number_HyperLink__c,Cost_Estimate_Number__c,Cost_Estimate_Subject__c,Ship_to_Country__c,Courier_Handover_Cost__c,CPA_Costings_Added__c,CPA_v2_0__c,Create_Costings__c,CreatedByOpp__c,Create_Roll_Out__c,CSE_IOR__c,Customs_Brokerage_Cost_CIF__c,Customs_Brokerage_Cost_fixed__c,Customs_Clearance_Cost_b__c,Customs_Clearance_Docs_Received_Date__c,Customs_Handling_Cost__c,Customs_Inspection_Cost__c,Date_Client_Approved_to_Ship__c,Date_Client_Pack_Submitted_to_IOR__c,Date_IOR_Approval_Received__c,Date_of_escalation__c,Date_of_last_follow_up__c,Date_Updated__c,Delvier_to_IOR_Warehouse_Cost__c,Delivery_Order_Cost__c,Date_Departed_Hub__c,Destination__c,DHL_Courier_cost__c,Discount_Percentage__c,Domestic_Courier__c,Domestic_Tracking_Link__c,Domestic_Tracking_Number__c,Duty_and_Taxes_Billed_to__c,Duty_Cost__c,Email_Estimate_To__c,EOR_and_Export_Compliance_Fee_USD__c,EOR_and_Export_Compliance_Fee_USD_numb__c,EOR_Fee__c,EOR_Fee_new__c,EOR_Min_USD__c,EOR_Min_USD_new__c,Estimate_Customs_Clearance_time__c,Estimate_Customs_Clearance_Time_Formula__c,Est_Delivery_Date__c,Tax_Cost__c,Estimated_Tax__c,Estimate_Pre_Approval_Time__c,Estimate_Transit_Time__c,Estimate_Transit_Time_Formula__c,Tax_Rate__c,Examination_Cost__c,Expiry_Date__c,Export_license_details__c,Extended_Pre_Approval_Time_Required__c,Non_TecEx_VAT_recovery__c,FC_Admin_Fee__c,FC_Bank_Fees__c,FC_Finance_Fee__c,FC_Insurance_Fee_USD__c,FC_International_Delivery_Fee__c,FC_IOR_and_Import_Compliance_Fee_USD__c,FC_Miscellaneous_Fee__c,FC_Recharge_Tax_and_Duty__c,FC_Total__c,FC_Total_Clearance_Costs__c,FC_Total_Customs_Brokerage_Cost__c,FC_Total_Handling_Costs__c,FC_Total_License_Cost__c,FF_Est_Cost__c,Final_Invoice_Amount__c,Final_client_Name__c,Final_Delivery_Address__c,Final_delivery_contact_email_address__c,Delivery_Contact_Name__c,Delivery_Contact_Tel_number__c,Final_Delivery_Date__c,Final_Invoice_Number__c,Final_Supplier_Invoice_Received__c,Final_Supplier_Invoice_Paid_Date__c,Finance_Team__c,Financial_Controller__c,Financial_Manager__c,First_Invoice_Created__c,Fixed_IOR_Cost__c,Flag__c,Forecast_Invoiced_Fees__c,Forecast_Net_Profit_v3__c,Forecast_Net_Fees_Formulas__c,Forecast_Net_Profitv2_del__c,Forecast_Override__c,Salesforce_Supplier_Cost_v2__c,Forecast_Supplier_Cost__c,Forex_Rate_Used_Euro_Dollar__c,Freight_and_Related_Costs_Summary__c,Freight_co_ordinator__c,Freight_coordinator_comments__c,Freight_Fee_Calculated_Freight_Request__c,Freight_Referrer__c,Freight_Supplier__c,Cost_Estimate_Acceptance_Date__c,Go_Live_Date__c,Government_and_In_country_Summary__c,Government_and_In_country_Summary_No_Tax__c,Handling_other_costs__c,Handling_Other_costs_Details__c,Held_in_Storage__c,HS_Code_Check__c,Hub_Country__c,Hub_Ship_From_Zone__c,Hub_Shipment__c,Hub_Shipment_Formula__c,Hub_Shipping_Cost__c,Hub_shipping_in_CPA_costs__c,Hub_Shipping_Rate_KG__c,Hub_to_Destination_Shipping_Cost__c,ICE__c,In_Country_Expert_ICE__c,ICE_Incentive_Date__c,ICE_Name__c,I_have_checked_the_HS_Codes__c,Import_Declaration_Cost__c,Included_in_OBS_billing__c,Inco_Terms__c,Internal_Comments1__c,Internal_Notes__c,Shipping_Notes__c,Internal_Status1__c,Who_arranges_International_courier__c,Int_Courier_Name__c,Int_Courier_Picklist__c,Tracking_Link__c,Int_Courier_Tracking_No__c,International_Delivery_Cost__c,International_Delivery_Cost_Formula__c,International_Delivery_Fee__c,International_Delivery_Fee_Test__c,Invisible__c,Invoice_Amount__c,Invoiced_Tax_and_Duty_Other__c,Invoice_Sent_to_Customer__c,IOR_EOR_Compliance_Summary__c,IOR_Address__c,IOR_FEE_USD__c,IOR_and_Import_Compliance_Fee_USD_numb__c,IOR_Cost_b__c,IOR_Cost_Custom_b__c,IOR_Cost_If_in_USD_2__c,IOR_Cost_If_in_USD_Formula__c,IOR_Fee_new__c,IOR_Gross_Profit__c,IOR_Min_USD__c,IOR_Name__c,IOR_Reference_No__c,IOR_Refund_of_Tax_Duty__c,IOR_Refund_of_Tax_and_Duty__c,Lead_AM__c,Lead_ICE__c,Lead_Pod_Email__c,Insurance_Cost_CIF__c,Insurance_Cost_U__c,Insurance_Fee__c,Insurance_Fee_USD__c,Liability_Cover_Fee_Added__c,Liability_Cover_Profit__c,License_Permit_Cost__c,Li_ion_Batteries__c,Lithium_Battery_Types__c,Loading_Cost__c,Local_Delivery_Cost__c,Loss_Making_Reason__c,Loss_Making_Shipments__c,Loss_Making_SO_Checked_by__c,Loss_Making_SO_checked_Date__c,Management_comments__c,Freight_overide_reason__c,Manual_Hub_Shipping_Cost__c,Manual_International_Delivery_Cost__c,Minimum_Brokerage_Costs__c,Minimum_Clearance_Costs__c,Minimum_Handling_Costs__c,Minimum_License_Permit_Costs__c,Miscellaneous_Costs__c,Miscellaneous_Fee__c,Miscellaneous_Fee_Name__c,Net_Profit_on_Shipment__c,New_Status_Date__c,New_Structure__c,New_Taxes_post_Forex__c,New_Tax_Structure_Active__c,Next_Step_by__c,No_further_client_invoices_required__c,NON_Pickup_freight_request_created__c,Non_VAT_recovery__c,Final_Deliveries_New__c,of_packages__c,No_of_Parts__c,On_Charge_Mark_up__c,On_charges_Profit__c,Open_and_Repack_Cost__c,Opportunity__c,Original_IOR_Fee__c,Other_Fees__c,Outstanding__c,Over_60_days__c,Override_Calculated_Taxes__c,Override_Tax_Treatment__c,OwnerFirstName__c,OwnerIdOld__c,Packages_override__c,Paid_Amount__c,Part_1a__c,Part_10__c,Part_11__c,Part_12__c,Part_13__c,Part_14__c,Part_15__c,Part_16__c,Part_17__c,Part_18__c,Part_19__c,Part_2a__c,Part_20__c,Part_3__c,Part_4__c,Part_5__c,Part_6__c,Part_7__c,Part_8__c,Part_9__c,Percentage_Time_At_Status__c,Percentage_Time_At_Status_Number__c,Pick_Up_Address__c,Pick_Up_Address_Lookup__c,Pick_Up_Contact__c,Pick_Up_contact_Number__c,Pick_Up_Contact_Number_New__c,POD_Date__c,Pod_Email_Formula__c,POD_Month__c,POD_signed_by__c,Populate_Invoice__c,Pre_Approval_Instructions__c,Pressure_Point__c,Pressure_Point_Comment__c,Pull_CPA_and_IOR_PL__c,Client_PO_ReferenceNumber__c,Purchase_orders_required__c,Quarantine_cost__c,Quoted_Admin_Fee__c,Quoted_Bank_Fee__c,Quoted_Brokerage_Fee__c,Quoted_Clearance_Fee__c,Quoted_Handling_Fee__c,Quoted_IOR_Fee__c,Quoted_Licence_Fee__c,Rag_Status__c,Reason_for_cancellation__c,Reason_for_rejection__c,Rebate_Due__c,Recalculate_Costings__c,Recalculate_Taxes__c,Receipt_Date__c,Receipt_Notes__c,Recharge_Tax_and_Duty__c,Recharge_Tax_and_Duty_Other__c,Referral_Fee__c,Referral_Fee_Formula__c,Referral_Fee_Percentage__c,Referrer_Branch__c,Regenerate_First_Invoice__c,Request_Email_Estimate__c,Roll_Out__c,RolloutName__c,Roll_Up_Costings__c,Sales_Commission__c,Sales_Commission_Branch__c,Sales_Commission_Formula__c,Sales_Commission_Percentage__c,Salesforce_Supplier_Cost__c,Service_Manager__c,Service_Type__c,Set_up_Costs__c,Set_up_Fee__c,sfoldid__c,Ship_From_Country__c,Ship_From_Zone1__c,Ship_from_zone__c,Shipment_Date__c,Date_Shipment_Live__c,Shipment_Value_USD__c,Shipping_co_ordinator2__c,Shipping_Method__c,Shipping_Notes_Formula__c,Shipping_Notes_New__c,Shipping_and_Insurance_Profit__c,Shipping_Status__c,Shipment_Status_Notes__c,IOR_Price_List__c,Ship_to_Country_new__c,Ship_to__c,From_Hub_Destination__c,Status__c,Storage_Cost__c,Storage_From_Date__c,Storage_Notes__c,Total_Invoiced_Amount__c,Sum_of_Duties_and_Taxes_Invoiced__c,Sum_of_First_Invoices__c,Sum_of_Intl_Freight_Invoices__c,Sum_of_Paid_First_Invoice__c,Sum_of_Sent_or_Paid_First_Invoices__c,Sum_of_SO_Face_Invoices__c,SupplierlU__c,Supplier_Admin_Cost__c,Supplier_Cost_Difference__c,Supplier_Costings_added__c,Dispute_Details__c,SupplierInvoice_Total_Customs_Brokerage__c,SupplierInvoice_Total_Customs_Clearance__c,SupplierInvoice_Total_Customs_Handling__c,SupplierInvoice_Total_Forex_Gain_Loss__c,SupplierInvoice_Total_Insurance__c,SupplierInvoice_Total_Int_l_Freight__c,SupplierInvoice_Total_IOR_Cost__c,SupplierInvoice_Total_License_Inspection__c,SupplierInvoice_Total_Local_Delivery__c,SupplierInvoice_Total_Tax_and_Duties__c,Supplier_Reference__c,Supplier_Shipment_Status__c,Supplier__c,Final_cost_vs_Invoice_Cost_difference__c,Tax_and_Duty_Cost_Formula__c,Taxes_Calculated__c,Tax_Recovery__c,Tax_recovery_Premium_Fee__c,Vat_Claim_Country__c,Tax_recovery_Premium_Rate__c,Tax_Treatment__c,Tax_Treatment_Formula__c,TecEx_In_house_Fee__c,TecEx_In_House_Profit__c,TecEx_Invoice_Number__c,TecEx_person_responsible__c,TecEx_Shipping_Fee_Markup__c,VAT_shipment__c,Terminal_Charges_Cost__c,Time_At_Status__c,Time_Shipment_Live__c,Time_Client_Approved_to_Ship__c,Time_CSE_Approved_Pack__c,Time_Customs_Approved__c,Time_ICE_Submitted_to_Supplier__c,Top_up_Invoice_Credits_Roll_up__c,TopUp_Invoice_Created__c,Total_Accounts_Receivable__c,Total_Accrual_expense__c,Total_actual_costs__c,Total_Bank_Charges__c,Total_Bank_USD_USD_BoW__c,Total_CIF_1_Duties__c,Total_CIF_Duties__c,Total_clearance_Costs__c,Total_Customs_Brokerage_Cost__c,Total_Customs_Brokerage_Cost1__c,Total_FOB_Duties__c,Total_Foreign_Exchange_Gain_Loss__c,Recharge_Handling_Costs__c,Total_including_estimated_duties_and_tax__c,Total_Invoice_Amount__c,Total_Invoice_Amount_Excl_Taxes_Duties__c,Total_IOR_Costs__c,Recharge_License_Cost__c,Total_License_Cost__c,Total_Line_Item_Extended_Value__c,Total_On_Charges_Costs__c,Total_Prepaid_Expenses__c,Total_Receipted__c,Total_Revenue_Financing_Fees__c,Total_Revenue_Collection_Admin_Fees__c,Total_Revenue_EOR_Fees__c,Total_Revenue_IOR_fees__c,Total_Revenue_Insurance_fees__c,Total_Revenue_Miscellaneous__c,Total_Revenue_On_charges__c,Total_Revenue_Received_In_Advance__c,Total_Revenue_Shipping_Insurance__c,Total_Revenue_Shipping_Fees__c,Total_Revenue_Tax__c,Total_Revenue_VAT_Deferral_Fees__c,Total_Shipping_Insurance_Costs__c,Total_Suppliers__c,Total_Tax_Control_Account__c,Total_Tax_Costs__c,Total_Taxes_CCD__c,Total_Taxes__c,Total_VAT_IT_GMLS__c,Total_VAT_IT_Luxembourg_DHL__c,Tracking_estimate__c,Trigger_Time_05__c,Trigger_Time_1_Minute__c,Type_of_Goods__c,Update_Forecast_Fees__c,update_Net_profit__c,Update_Rag_Status_Time__c,Value_Hub_Shipping_Cost__c,Value_International_Delivery_Cost__c,Variable_Brokerage_Cost__c,VAT_Amount__c,VAT_Claiming_Country__c,VAT_Recovered__c,xxxTotal_Invoice_Amount__c,Internal_Comments__c,Internal_Status__c,Top_up_Invoice_Credits__c from shipment_Order__C where ID =:SOID ];
                 shipment_order__c SOs = [Select id from shipment_Order__C where ID =:SOID ];
                
                 List<Shipment_Order_Package__c> SOP = [select id,Shipment_Order__c,Weight_Unit__c,Volumetric_Weight_KGs__c,Height__c,Length__c,Oversized__c,Freight__c,Freight_Linked_To_SO__c,Chargeable_Weight_KGs__c,Dimension_Unit__c,Name,packages_of_same_weight_dims__c,Actual_Weight__c,Actual_Weight_KGs__c,Breadth__c from Shipment_Order_Package__c where Shipment_Order__c =: SOID ];
                 List<Final_Delivery__c> FD = [select id,Ship_to_address__C,Ship_to_address__r.Name,Ship_to_address__r.Contact_Full_Name__c,Ship_to_address__r.Contact_Email__c,Ship_to_address__r.Contact_Phone_Number__c,
			   Ship_to_address__r.CompanyName__c,Ship_to_address__r.Address__c,Ship_to_address__r.Address2__c,
			    Ship_to_address__r.City__c,Ship_to_address__r.Postal_Code__c,Ship_to_address__r.All_Countries__c,
                                               Address_Line_1__c,Address_Line_2__c,Address_Line_3__c,City__c,Company_Name__c,Contact_email__c,Contact_name__c,Contact_number__c,Country__c,Delivery_address__c,Delivery_date__c,Name,Delivery_Status__c,Other__c,Shipment_Order__c,Tax_Id__c,Tax_Name__c,Zip__c from Final_Delivery__c where Shipment_Order__c =: SOID ];
                 List<Attachment> Attmnts = [select Id,Name,ParentId,Body,BodyLength,ContentType, Parent.Type FROM Attachment where ParentId =: SOID ];
                 List<Part__c> Parts = [select Id,Shipment_Order__c,Name,Description_and_Functionality__c,Quantity__c,Commercial_Value__c,US_HTS_Code__c,Country_of_Origin2__c,ECCN_NO__c,Type_of_Goods__c,Li_ion_Batteries__c,Lithium_Battery_Types__c FROM Part__c where Shipment_Order__c =: SOID ];
                  
                                          
                                          
             JSONGenerator gen = JSON.createGenerator(true);
             gen.writeStartObject();
                                          //Creating Array & Object - Shipment Order
                                          gen.writeFieldName('ShipmentOrder');
                                          gen.writeStartObject();
                                          if(SOs.Id == null) {gen.writeNullField('Id');} else{gen.writeStringField('Id', SOs.Id);}
                                          gen.writeEndObject();
                                          //End of Array & Object - Shipment Order
             
         If(!SOP.isEmpty()){
             //Creating Array & Object - Shipment Order package
             gen.writeFieldName('ShipmentOrder Packages');
             	gen.writeStartArray();
     		         for(Shipment_Order_Package__c SOP1 :SOP){
        				//Start of Object - Shipment Order package	
                         gen.writeStartObject();
                          
                         if(SOP1.Id == null) {gen.writeNullField('Id');} else{gen.writeStringField('Id', SOP1.Id);}
                          if(SOP1.Name == null) {gen.writeNullField('SO Package Name');} else{gen.writeStringField('SO Package Name', SOP1.Name);}
                          if(SOP1.Weight_Unit__c == null) {gen.writeNullField('Weight Unit');} else{gen.writeStringField('Weight Unit', SOP1.Weight_Unit__c);}
                          if(SOP1.Dimension_Unit__c == null) {gen.writeNullField('Dimension Unit');} else{gen.writeStringField('Dimension Unit', SOP1.Dimension_Unit__c);}
                          if(SOP1.packages_of_same_weight_dims__c == null) {gen.writeNullField('# Packages of Same Weight/Dimensions');} else{gen.writeNumberField('# Packages of Same Weight/Dimensions', SOP1.packages_of_same_weight_dims__c);}
                          if(SOP1.Length__c == null) {gen.writeNullField('Length');} else{gen.writeNumberField('Length', SOP1.Length__c);}
                          if(SOP1.Height__c == null) {gen.writeNullField('Height');} else{gen.writeNumberField('Height', SOP1.Height__c);}
                          if(SOP1.Breadth__c == null) {gen.writeNullField('Breadth');} else{gen.writeNumberField('Breadth', SOP1.Breadth__c);}
                          if(SOP1.Actual_Weight__c == null) {gen.writeNullField('Actual Weight');} else{ gen.writeNumberField('Actual Weight', SOP1.Actual_Weight__c);}
                          if(SOP1.Shipment_Order__c== null) {gen.writeNullField('Shipment Order');} else{gen.writeStringField('Shipment Order', SOP1.Shipment_Order__c);}
                          if(SOP1.Freight__c== null) {gen.writeNullField('Freight');} else{gen.writeStringField('Freight', SOP1.Freight__c);}
                          if(SOP1.Chargeable_Weight_KGs__c== null) {gen.writeNullField('Chargeable Weight (KGs)');} else{gen.writeNumberField('Chargeable Weight (KGs)', SOP1.Chargeable_Weight_KGs__c);}
                          if(SOP1.Volumetric_Weight_KGs__c== null) {gen.writeNullField('Volumetric Weight (KGs)');} else{gen.writeNumberField('Volumetric Weight (KGs)', SOP1.Volumetric_Weight_KGs__c);}
                          if(SOP1.Actual_Weight_KGs__c== null) {gen.writeNullField('Actual Weight (KGs)');} else{ gen.writeNumberField('Actual Weight (KGs)', SOP1.Actual_Weight_KGs__c);}
                          if(SOP1.Freight_Linked_To_SO__c== null) {gen.writeNullField('Freight Linked To SO?');} else{gen.writeBooleanField('Freight Linked To SO?', SOp1.Freight_Linked_To_SO__c);}
                          if(SOP1.Oversized__c== null) {gen.writeNullField('Oversized?');} else{gen.writeBooleanField('Oversized?', SOP1.Oversized__c);}
                                       
        					gen.writeEndObject();
                         //End of Object - Shipment Order package
    					}
    		gen.writeEndArray();
                //End of Array - Shipment Order package 
         }   
               
                 
             If(!Parts.isEmpty()){
             //Creating Array & Object - Parts
             gen.writeFieldName('Parts');
             	gen.writeStartArray();
     		         for(Part__c Parts1 :Parts){
        				//Start of Object - Parts	
                         gen.writeStartObject();
                          
                         if(Parts1.Id == null) {gen.writeNullField('Id');} else{gen.writeStringField('Id', Parts1.Id);}
                          if(Parts1.Name == null) {gen.writeNullField('Part Number');} else{gen.writeStringField('Part Number', Parts1.Name);}
                          if(Parts1.Description_and_Functionality__c == null) {gen.writeNullField('Part Description');} else{gen.writeStringField('Part Description', Parts1.Description_and_Functionality__c);}
                          if(Parts1.Quantity__c == null) {gen.writeNullField('Quantity');} else{gen.writeNumberField('Quantity', Parts1.Quantity__c);}
                          if(Parts1.Commercial_Value__c == null) {gen.writeNullField('UnitPrice');} else{gen.writeNumberField('UnitPrice', Parts1.Commercial_Value__c);}
                          if(Parts1.US_HTS_Code__c == null) {gen.writeNullField('USHTSCode');} else{gen.writeStringField('USHTSCode', Parts1.US_HTS_Code__c);}
                          if(Parts1.Country_of_Origin2__c == null) {gen.writeNullField('CountryOfOrigin');} else{ gen.writeStringField('CountryOfOrigin', Parts1.Country_of_Origin2__c);}
                          if(Parts1.ECCN_NO__c== null) {gen.writeNullField('ECCNNo');} else{gen.writeStringField('ECCNNo', Parts1.ECCN_NO__c);}
                          if(Parts1.Type_of_Goods__c== null) {gen.writeNullField('TypeofGoods');} else{gen.writeStringField('TypeofGoods', Parts1.Type_of_Goods__c);}
                          if(Parts1.Li_ion_Batteries__c== null) {gen.writeNullField('LiIonBatteries');} else{gen.writeStringField('LiIonBatteries', Parts1.Li_ion_Batteries__c);}
                          if(Parts1.Lithium_Battery_Types__c== null) {gen.writeNullField('LithiumBatteryTypes');} else{gen.writeStringField('LithiumBatteryTypes', Parts1.Lithium_Battery_Types__c);}
             
        					gen.writeEndObject();
                         //End of Object - Shipment Order package
    					}
    		gen.writeEndArray();
                //End of Array - Shipment Order package 
         } 
                 
                 
                 
              If(!Attmnts.isEmpty()){
             //Creating Array & Object - Attachements
             gen.writeFieldName('Attachments');
             	gen.writeStartArray();
     		         for(Attachment Attmnts1 :Attmnts){
        				//Start of Object - Attachements	
                         gen.writeStartObject();
                          
                          if(Attmnts1.Id == null) {gen.writeNullField('Id');} else{gen.writeStringField('Id', Attmnts1.Id);}
                          if(Attmnts1.Name == null) {gen.writeNullField('Name');} else{gen.writeStringField('Name', Attmnts1.Name);}
                          if(Attmnts1.ContentType == null) {gen.writeNullField('ContentType');} else{gen.writeStringField('ContentType', Attmnts1.ContentType);}
                         
                                    
        					gen.writeEndObject();
                         //End of Object - Attachements
    					}
    		gen.writeEndArray();
                //End of Array - Attachementse 
         }   
                 
            If(!FD.isEmpty()){                               
              //Creating Object - Final Delivery
             gen.writeFieldName('Final Deliveries');
             	gen.writeStartArray();
     		         for(Final_Delivery__c FD1 :FD){
                         //Start of Object - Final Delivery
        					gen.writeStartObject();
        				   gen.writeStringField('Id', FD1.Id);
                         if(FD1.Ship_to_address__r.Name== null) {gen.writeNullField('Delivery Reference');} else{gen.writeStringField('Delivery Reference', FD1.Ship_to_address__r.Name);}
                          if(FD1.Ship_to_address__C== null) {gen.writeNullField('Shiptoaddress');} else{gen.writeStringField('Shiptoaddress', FD1.Ship_to_address__C);} 
                          if(FD1.Ship_to_address__C== null) {gen.writeNullField('ShiptoaddressName');} else{gen.writeStringField('ShiptoaddressName', FD1.Ship_to_address__r.Name );} 
                          if(FD1.Delivery_address__c== null) {gen.writeNullField('Delivery Address');} else{gen.writeStringField('Delivery Address', FD1.Delivery_address__c);}
                         if(FD1.Delivery_Status__c== null) {gen.writeNullField('Delivery Status');} else{gen.writeStringField('Delivery Status', FD1.Delivery_Status__c);}
                         if(FD1.Contact_number__c== null) {gen.writeNullField('Contact Number');} else{gen.writeStringField('Contact Number', FD1.Contact_number__c);}
                         if(FD1.Shipment_Order__c== null) {gen.writeNullField('Shipment Order');} else{gen.writeStringField('Shipment Order', FD1.Shipment_Order__c);}
                         if(FD1.Delivery_date__c== null) {gen.writeNullField('Delivery Date');} else{gen.writeDateField('Delivery Date', FD1.Delivery_date__c);}
                          if(FD1.Other__c== null) {gen.writeNullField('Other');} else{gen.writeStringField('Other', FD1.Other__c);}
                          if(FD1.Tax_Name__c== null) {gen.writeNullField('Tax Name');} else{gen.writeStringField('Tax Name', FD1.Tax_Name__c);}
                          if(FD1.Tax_Id__c== null) {gen.writeNullField('Tax Id');} else{gen.writeStringField('Tax Id', FD1.Tax_Id__c);}
                         
                        // Change below details to get from Client address.
                       //  if(FD1.Ship_to_address__r.Name== null) {gen.writeNullField('Name');} else{gen.writeStringField('Name', FD1.Ship_to_address__r.Name);}
                         if(FD1.Ship_to_address__r.Contact_Full_Name__c== null) {gen.writeNullField('Contact Name');} else{gen.writeStringField('Contact Name', FD1.Ship_to_address__r.Contact_Full_Name__c);}
                         if(FD1.Ship_to_address__r.Contact_Email__c== null) {gen.writeNullField('Contact Email');} else{gen.writeStringField('Contact Email', FD1.Ship_to_address__r.Contact_Email__c);}
                         if(FD1.Ship_to_address__r.CompanyName__c== null) {gen.writeNullField('Company Name');} else{gen.writeStringField('Company Name', FD1.Ship_to_address__r.CompanyName__c);}
                         if(FD1.Ship_to_address__r.City__c== null) {gen.writeNullField('City');} else{gen.writeStringField('City', FD1.Ship_to_address__r.City__c);}
                         if(FD1.Address_Line_3__c== null) {gen.writeNullField('Address Line 3');} else{gen.writeNullField('Address Line 3');}
                         if(FD1.Ship_to_address__r.Address2__c== null) {gen.writeNullField('Address Line 2');} else{gen.writeStringField('Address Line 2', FD1.Ship_to_address__r.Address2__c);}
                         if(FD1.Ship_to_address__r.Address__c== null) {gen.writeNullField('Address Line 1');} else{gen.writeStringField('Address Line 1', FD1.Ship_to_address__r.Address__c);}
                         if(FD1.Ship_to_address__r.Postal_Code__c== null) {gen.writeNullField('Zip');} else{gen.writeStringField('Zip', FD1.Ship_to_address__r.Postal_Code__c);}
                         if(FD1.Ship_to_address__r.All_Countries__c== null) {gen.writeNullField('Country');} else{gen.writeStringField('Country', FD1.Ship_to_address__r.All_Countries__c);}
                       
                         
        					gen.writeEndObject();
                         //End of Object - Final Delivery
    					}
    		gen.writeEndArray();
                //End of Array - Final Delivery 
                                                                                             
            }                            
                                          
    		gen.writeEndObject();
    		jsonData = gen.getAsString();
               
                  res.responseBody = Blob.valueOf(jsonData);
        	 	  res.statusCode = 200;
                
                API_Log__c Al = New API_Log__c();
                Al.Account__c=logincontact.AccountID;
                Al.Login_Contact__c=logincontact.Id;
                Al.EndpointURLName__c='SORelatedObjects';
                Al.Response__c='Success - SO RelatedObject Details are sent';
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al;
               
           
                 
             }
              
              else{
                  
                  String ErrorString ='Authentication failed, Please check username and password';
               	  res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
        	  	  res.statusCode = 400;
                
                API_Log__c Al = New API_Log__c();
                Al.Account__c=logincontact.AccountID;
                Al.Login_Contact__c=logincontact.Id;
                Al.EndpointURLName__c='SORelatedObjects';
                Al.Response__c=ErrorString;
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al;
               
                                              
                 }
            
            
        }
        Catch(exception e){
            
            System.debug('Line number ====> '+e.getLineNumber() + 'Exception Message =====> '+e.getMessage());
            
           		  String ErrorString ='Failed, Please check username and password';
               	  res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
            res.statusCode = 500;
                API_Log__c Al = New API_Log__c();
                Al.Account__c=logincontact.AccountID;
                Al.Login_Contact__c=logincontact.Id;
                Al.EndpointURLName__c='SORelatedObjects';
                Al.Response__c=e.getMessage()+' at line number: '+e.getLineNumber();
                Al.StatusCode__c=string.valueof(res.statusCode);
              	Insert Al;
          
            
        }
        
    }
        
}