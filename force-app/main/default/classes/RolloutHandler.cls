public class RolloutHandler {
    // public static  boolean firstRun = true;
    
    public static void isBeforeInsert()
    {
        RolloutHandler.manageInsert();
        
    }
    public static void isbeforeUpdate()
    {
        
        RolloutHandler.manageCostings();
    }
    /*      public static void deleteCosting()
{

RolloutHandler.deleteCostings();
}

*/
    
    public static void Updatefromlookups()
    {
        
        RolloutHandler.LookupFieldUpdates();
    }
    
    
    
    
  
    public static void manageInsert(){
        
        list<Shipment_Order__c> SO = New list<Shipment_Order__c>();
        // List<ID> IORPL1 = New List<ID>();
        // List<ID> CPA11 = New List<ID>();
        // List<ID> CPA21 = New List<ID>();
        
        ID IORPL;
        ID CPA1;
        ID CPA2;
        Ior_Price_list__C ShiptoRecord;
        
        for(Shipment_Order__c SOO :(List<Shipment_Order__c >) trigger.new){
            
            system.debug('SOO ' + SOO);
            
            // Step 2: Getting ShiptoCountry (IOR_Price_list__C)   
            If(SOO.IOR_Price_list__C == NUll){
                ShiptoRecord = [select ID,Client_Name__r.name, Destination__c from IOR_price_List__C where (Active__c = TRUE and Client_Name__c = : SOO.Account__c and Destination__c = : SOO.Destination__c) or (Active__c = TRUE and Client_Name__r.name ='TecEx Prospective Client' and Destination__c = : SOO.Destination__c) ORDER BY ID DESC limit 1];
                
                if(ShiptoRecord.Client_Name__r.name != 'TecEx Prospective Client' ){
                    //  IORPL1.add(ShiptoRecord.ID);
                    IORPL=ShiptoRecord.ID;
                }
                Else{
                    Ior_Price_list__C MasterShiptoRecord = [select ID,Destination__c,Estimate_Customs_Clearance_Time__c,Estimate_Transit_time__c,Estimated_Customs_Brokerage__c,Estimated_Customs_Clearance__c,Estimated_Customs_Handling__c,Estimated_Import_License__c,IOR_Fee__c,IOR_Fee_as_of__c,Tax_Rate__c,Admin_Fee__c,Bank_Fees__c,TecEx_Shipping_Fee_Markup__c,On_Charge_Mark_up__c,IOR_Min_Fee__c,Set_up_fee__c,Name from IOR_price_List__C where Active__c = TRUE and Client_Name__r.name ='TecEx Prospective Client' and Destination__c =: SOO.Destination__c  limit 1];
                    //    Ior_Price_list__C MasterShiptoRecord = [select ID,Estimate_Customs_Clearance_Time__c,Estimate_Transit_time__c,Estimated_Customs_Brokerage__c,Estimated_Customs_Clearance__c,Estimated_Customs_Handling__c,Estimated_Import_License__c,IOR_Fee__c,IOR_Fee_as_of__c,Tax_Rate__c,Admin_Fee__c,Bank_Fees__c,TecEx_Shipping_Fee_Markup__c,On_Charge_Mark_up__c,IOR_Min_Fee__c,Set_up_fee__c,Name from IOR_price_List__C where Active__c = TRUE and Client_Name__c ='0010Y00000PEnzVQAT' and Destination__c IN : Destinations  limit 1]; 
                    Ior_Price_list__C CreateNewClientShiptoRecord = new Ior_Price_list__C( 
                        Admin_Fee__c=MasterShiptoRecord.Admin_Fee__c,
                        Bank_Fees__c=MasterShiptoRecord.Bank_Fees__c,
                        Client_Name__c=SOO.account__C,
                        Destination__c=MasterShiptoRecord.Destination__c,
                        Estimate_Customs_Clearance_Time__c=MasterShiptoRecord.Estimate_Customs_Clearance_Time__c,
                        Estimated_Customs_Brokerage__c=MasterShiptoRecord.Estimated_Customs_Brokerage__c,
                        Estimated_Customs_Clearance__c=MasterShiptoRecord.Estimated_Customs_Clearance__c,
                        Estimated_Customs_Handling__c=MasterShiptoRecord.Estimated_Customs_Handling__c,
                        Estimated_Import_License__c=MasterShiptoRecord.Estimated_Import_License__c,
                        IOR_Fee__c=MasterShiptoRecord.IOR_Fee__c,
                        IOR_Fee_as_of__c=MasterShiptoRecord.IOR_Fee_as_of__c,
                        IOR_Min_Fee__c=MasterShiptoRecord.IOR_Min_Fee__c,
                        Name=MasterShiptoRecord.Name,
                        On_Charge_Mark_up__c=MasterShiptoRecord.On_Charge_Mark_up__c,
                        Set_up_fee__c=MasterShiptoRecord.Set_up_fee__c,
                        Tax_Rate__c=MasterShiptoRecord.Tax_Rate__c,
                        TecEx_Shipping_Fee_Markup__c=MasterShiptoRecord.TecEx_Shipping_Fee_Markup__c    
                    ); 
                    insert CreateNewClientShiptoRecord;
                    
                    // IORPL1.add(CreateNewClientShiptoRecord.ID);
                    IORPL=CreateNewClientShiptoRecord.ID;
                    
                }
                
                
            }
            
            // Step 3: Getting ShiptoCountry (Ship_to_Country__c)   
            If(SOO.Ship_to_Country__c == NUll){
                Country_Price_Approval__C CPARecord =[select ID,Name,Destination__C from Country_Price_Approval__C where (Destination__C =: SOO.Destination__c and Preferred_Supplier__c = TRUE and Max_Shipment_Value__c >= :SOO.Shipment_Value_USD__c and Min_Shipment_Value__c <= :SOO.Shipment_Value_USD__c) or (Destination__C =: SOO.Destination__c and Preferred_Supplier__c = TRUE ) Limit 1 ]; 
                //  Country_Price_Approval__C CPARecord =[select ID,Name,Destination__C from Country_Price_Approval__C where (Destination__C IN  :Destinations and Preferred_Supplier__c = TRUE and Max_Shipment_Value__c >= :SOO.Shipment_Value_USD__c and Min_Shipment_Value__c <= :SOO.Shipment_Value_USD__c) or (Destination__C IN : Destinations and Preferred_Supplier__c = TRUE ) Limit 1 ];     
                system.debug('CPA1 Query record--->'+CPARecord);
                if(CPARecord.Id != Null )
                {
                    //   CPA11.add(CPARecord.ID);
                    CPA1 =CPARecord.ID;
                }
                
                
            }
            
            // Step 4: Getting CPA2.0 (CPA_v2_0__c)   
            If(SOO.CPA_v2_0__c == NUll){
                
                // CPA2=Id.valueOf(FindCPA2.FindCPA2(SOO));
                
                
                
                
                /* 
CPA_v2_0__c CPA2Record =[select ID,Destination__C from CPA_v2_0__c where Destination__C =: SOO.Destination__c and Country_Applied_Costings__c = FALSE and Preferred_Supplier__c = TRUE Limit 1 ];   
if(CPA2Record.Id != Null )
{
//   CPA21.add(CPA2Record.ID);
CPA2=CPA2Record.ID;

}
*/
                
            }   
            
            
            
            
        }
        
        
        
        
        for(Shipment_Order__c SO1 :(List<Shipment_Order__c >) trigger.new){
            
            if(ShiptoRecord.Destination__c != NULL && SO1.Destination__c != NULL && ShiptoRecord.Destination__c == SO1.Destination__c){
            
            //Step2: update ShiptoCountry (IOR_Price_list__C)
            SO1.IOR_Price_list__C=IORPL;
            
            // Step 3: update CPA1 (Ship_to_Country__c) 
            SO1.Ship_to_Country__c =CPA1;
            
            // Step 3: update CPA2 (CPA_v2_0__c) 
            SO1.CPA_v2_0__c =CPA2;
            
            
            }
            
            
            
        }
        
        
    }
    
    Public static void manageCostings(){
        
        
        for(Shipment_Order__c SOO :(List<Shipment_Order__c >) trigger.new){ 
            List<ID> SOID= new List<Id>();
            SOID.add(SOO.Id);
            
            
            
            RolloutCalculatecosts.createSOCosts(SOID);
            
        }
        
        
        
    }
    
    public static void LookupFieldUpdates(){
        
        for(Shipment_Order__c SOO :(List<Shipment_Order__c >) trigger.new){ 
            List<ID> SOID= new List<Id>();
            SOID.add(SOO.Id);
            
            
            
            SOLookupFieldUpdates.Updates(SOID);
            
            
        }
        
        
        
    }
    
    
}