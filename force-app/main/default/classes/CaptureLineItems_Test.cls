@IsTest
public class CaptureLineItems_Test{
	@isTest
    public static void getLineItemsTest(){
        Test.startTest();
        Customs_Clearance_Documents__c customs_Clearance_Documents = TestCCDUtil.createSetUpData();
        Map<String,Object> recordMap = CaptureLineItems.getLineItems(customs_Clearance_Documents.Id);
        List<Part__c> parts = (List<Part__c>)recordMap.get('data');
        //parts[0].Captured_HS_code__c = 50;
        Map<String,Object> partsMap = CaptureLineItems.updateParts(parts);
		Test.stopTest();
        System.assertEquals('OK', recordMap.get('status'));
        System.assertEquals('OK', partsMap.get('status'));
    }
    
    @isTest
    public static void TestNegativeCaseLineItems(){
        Test.startTest();
        //Customs_Clearance_Documents__c customs_Clearance_Documents = TestCCDUtil.createSetUpData();
        Map<String,Object> recordMap = CaptureLineItems.getLineItems('a1T0E000000XC5aUAG');
		Test.stopTest();
        System.assertEquals('Error', recordMap.get('status'));
    }
    
    @isTest
    public static void TestNegativeCaseUpdatePart(){
        Test.startTest();
        //Customs_Clearance_Documents__c customs_Clearance_Documents = TestCCDUtil.createSetUpData();
        Map<String,Object> partsMap = CaptureLineItems.updateParts(null);
		Test.stopTest();
        System.assertEquals('ERROR', partsMap.get('status'));
    }
}