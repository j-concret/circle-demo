@RestResource(urlMapping='/NCPAddParticipants/*')
Global class NCPAddPartcipants {
    
      @Httppost
      global static void NCPAddPartcipants(){
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        Blob body = req.requestBody;
    	res.addHeader('Content-Type', 'application/json');
        String requestString = body.toString();
        NCPAddPartcipantsWra rw  = (NCPAddPartcipantsWra)JSON.deserialize(requestString,NCPAddPartcipantsWra.class);
          Try{
              
              list<Access_token__c> At =[select status__c,Access_Token__c from Access_token__c where Access_Token__c=:rw.Accesstoken ];
            	if(!at.isempty() ){
                           
                 //Network nw= [select Id, name from network where name = 'New-ClientPortal'];
           		Network nw= [select Id, name from network where name = 'TecEx App'];
                    
                    if(!rw.NotifyContacts.isempty()){
                     List<EntitySubscription> entitySubToBeInsert = new List<EntitySubscription>();
                        For(Integer i=0;rw.NotifyContacts.size()>i;i++){
                        	EntitySubscription follow = new EntitySubscription(parentId = rw.RecordID, subscriberid =rw.NotifyContacts[i].NotifyContactID,networkId =nw.ID); //  acc2-0DB1q00000000ozGAA,Prod - 0DB1v000000kB98GAE
                			entitySubToBeInsert.add(follow);
                        }
                        
                        Insert entitySubToBeInsert;
                    }
                
                 	
                  	JSONGenerator gen = JSON.createGenerator(true);
            		gen.writeStartObject();
            
             		gen.writeFieldName('Success');
             		gen.writeStartObject();
            
             			gen.writeStringField('Response','Participants are added succesfully');
            
            		gen.writeEndObject();
            		gen.writeEndObject();
            		//String jsonData = gen.getAsString();
            		res.responseBody = Blob.valueOf(gen.getAsString());
            		res.addHeader('Content-Type', 'application/json');
            		res.statusCode = 200;    
                    API_Log__c Al = New API_Log__c(EndpointURLName__c='NCPAddParticipants',StatusCode__c=string.valueof(res.statusCode),Response__c = 'Participants are added succesfully');
                    //Al.EndpointURLName__c='NCPAddParticipants';
                   // Al.StatusCode__c=string.valueof(res.statusCode);
                    //Al.Response__c = 'Participants are added succesfully';
                    Insert Al;
                
                  
                  
                  
              } 
              
               else{
                  
                  JSONGenerator gen = JSON.createGenerator(true);
            gen.writeStartObject();
            
             gen.writeFieldName('Error');
             gen.writeStartObject();
            
              gen.writeStringField('Response', 'Access Token Expired or Invlid');
            
            gen.writeEndObject();
            gen.writeEndObject();
            String jsonData = gen.getAsString();
            res.responseBody = Blob.valueOf(jsonData);
            res.addHeader('Content-Type', 'application/json');
            res.statusCode = 400; 
                
                API_Log__c Al = New API_Log__c();
                Al.Account__c=rw.AccountID;
                Al.Login_Contact__c=rw.contactId;
                Al.EndpointURLName__c='NCPAddParticipants';
                Al.Response__c='Access Token Expired or Invlid';
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al;
            } 
          }
          Catch(Exception e){
              
              String ErrorString ='You are trying to add duplicate participants or Something went wrong, please contact Sfsupport@tecex.com';
               		res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
            		res.addHeader('Content-Type', 'application/json');
        			res.statusCode = 500;
                
                API_Log__c Al = New API_Log__c();
                Al.Account__c=rw.AccountID;
                 Al.Login_Contact__c=rw.contactId;
                Al.EndpointURLName__c='NCPAddParticipants';
                Al.Response__c=e.getMessage()+' at line number: '+e.getLineNumber();
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al;
                
              
              
          }



      }
    

}