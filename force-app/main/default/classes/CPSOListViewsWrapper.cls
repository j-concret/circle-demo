public class CPSOListViewsWrapper {
    
	public String AccountId;
	public String ContactId;
	public String ShippingStatus;
	public String shipfrom;
	public String shipto;
	public String servicetype;
	public String CourierResponsibility;

	
	public static CPSOListViewsWrapper parse(String json) {
		return (CPSOListViewsWrapper) System.JSON.deserialize(json, CPSOListViewsWrapper.class);
	}

}