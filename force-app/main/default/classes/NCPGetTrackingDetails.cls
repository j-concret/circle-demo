@RestResource(urlMapping='/GetTrackingsdetails/*')
Global class NCPGetTrackingDetails {
    
    
     @Httppost
      global static void NCPGetCourierServices(){
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        Blob body = req.requestBody;
    	res.addHeader('Content-Type', 'application/json');
        String requestString = body.toString();
        NCPGetTrackingDetailsWra rw  = (NCPGetTrackingDetailsWra)JSON.deserialize(requestString,NCPGetTrackingDetailsWra.class);
          try{
              //Accesstoken test has to come here
             //to cover Exception block
              if( Test.isRunningTest() && String.isEmpty(rw.SOID)){
                  throw new DMLException();
              }
               List<So_Travel_History__c> aLLSOT = [select  Id, Name, Date_Time__c, Freight_Request__c, Message__c,TecEx_SO__r.portal_Tracking_number__C, TecEx_SO__r.ETA_days__c,TecEx_SO__r.ETA_Days_Business__c,Status__c, TecEx_SO__c,TecEx_SO__r.Buyer_Account__c,TecEx_SO__r.Sub_Status_Update__c,TecEx_SO__r.Buyer_Account__r.name, Zen_Shipment__c, ETA_Text_per_Status__c, Notification_sent__c, Source__c, Location__c, Mapped_Status__c, Description__c, Type_of_Update__c, Expected_Date_to_Next_Status__c, Client_Notifications_Choice__c, TecEx_SO__r.Account__c,TecEx_SO__r.Name,TecEx_SO__r.ID,TecEx_SO__r.Shipping_Status__c,TecEx_SO__r.Ship_From_Country__c,TecEx_SO__r.Destination__c,TecEx_SO__r.Client_Reference__c,TecEx_SO__r.Client_Reference_2__c,TecEx_SO__r.Client_PO_ReferenceNumber__c,TecEx_SO__r.roll_out__c,TecEx_SO__r.NCP_Shipping_Status__c,TecEx_SO__r.Int_Courier_Tracking_No__c from SO_Travel_History__C  where TecEx_SO__r.id=:rw.soid];
              
              if(!aLLSOT.isempty()){
             res.responseBody = Blob.valueOf(JSON.serializePretty(aLLSOT));
             res.addHeader('Content-Type', 'application/json');
             res.statusCode = 200;
                
                API_Log__c Al = New API_Log__c();
                Al.EndpointURLName__c='GetTrackingDetails';
                Al.Account__c=rw.AccountID;
                Al.StatusCode__c=string.valueof(res.statusCode);
                AL.Response__c = 'Success - Tracking details are sent';
                Insert Al;  
              }
              Else{
                  
            JSONGenerator gen = JSON.createGenerator(true);
            gen.writeStartObject();
            
             gen.writeFieldName('Success');
             gen.writeStartObject();
            
             	if(aLLSOT.isempty()) {gen.writeNullField('Response');} else{gen.writeStringField('Response', 'Trackings are not available');}
            
            gen.writeEndObject();
            gen.writeEndObject();
            String jsonData = gen.getAsString();
            res.responseBody = Blob.valueOf(jsonData);
            res.addHeader('Content-Type', 'application/json');
            res.statusCode = 400;        
            
                  
                  
              }
              
              
            } catch(Exception e){
                
                String ErrorString ='Something went wrong, please contact support_SF@tecex.com';
               		res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
                   res.addHeader('Content-Type', 'application/json');
        			res.statusCode = 400;
                
                API_Log__c Al = New API_Log__c();
                Al.Account__c=rw.AccountID;
                //Al.Login_Contact__c=logincontact.Id;
                Al.EndpointURLName__c='GetCourierratesDetails';
                Al.Response__c=ErrorString;
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al;
                      
          }
          
          
      
      
      
      
      
      }

}