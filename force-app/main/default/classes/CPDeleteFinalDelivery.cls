@RestResource(urlMapping='/DeleteFinalDelivery/*')
Global class CPDeleteFinalDelivery {
       @Httppost
    global static void CPDeleteFinalDelivery(){
    
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        Blob body = req.requestBody;
    	res.addHeader('Content-Type', 'application/json');
        String requestString = body.toString();
        CPDeleteFinalDeliverywra rw = (CPDeleteFinalDeliverywra)JSON.deserialize(requestString,CPDeleteFinalDeliverywra.class);
        try{ 
     	Final_Delivery__c  FD=  [SELECT Id, Name FROM Final_Delivery__c where Id =: rw.FinaldeliveryID limit 1];
        Delete FD;
        
         		String ASD = 'Success- Final delivery deleted';	
            	res.responseBody = Blob.valueOf(JSON.serializePretty(ASD));
            	res.statusCode = 200;
              	API_Log__c Al = New API_Log__c();
               // Al.Account__c=rw.ClientID;
               // Al.Login_Contact__c=rw.ContactID;
                Al.EndpointURLName__c='DeleteFinalDelivery';
                Al.Response__c='Success- Final delivery deleted';
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al; 
        }
        catch(Exception e){
            
            	String ErrorString ='Something went wrong while updating Shipment order packages details, please contact support_SF@tecex.com';
               	res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
            	res.statusCode = 500;
                API_Log__c Al = New API_Log__c();
                // Al.Account__c=rw.ClientID;
                //Al.Login_Contact__c=rw.ContactID;
                Al.EndpointURLName__c='DeleteFinalDelivery';
                Al.Response__c=e.getMessage()+' at line number: '+e.getLineNumber();
                Al.StatusCode__c=string.valueof(res.statusCode);
              	Insert Al;
        }
   
    
    }
    

}