public class SOTriggerHelper {

    public static void calculateSOCostingAmount(Shipment_Order__c SO, CPA_Costing__c cost, Currency_Management2__c costingCurrency){

        Decimal chargeableAmount = 0;
        boolean costStatus = true;

        switch on cost.Applied_to__c {
            when 'CIF value' {
                if (SO.Shipment_Value_USD__c == null)
                    cost.CostStatus__c = Constants.Status.Warning.ordinal();
                else if (SO.Shipment_Value_USD__c != null || cost.CostStatus__c == Constants.Status.Warning.ordinal())
                    cost.CostStatus__c = Constants.Status.Good.ordinal();

                chargeableAmount = ((SO.Shipment_Value_USD__c + 250) * 1.05);
            }
            when 'Shipment value' { // when block 2
                if (SO.Shipment_Value_USD__c == null)
                    cost.CostStatus__c = Constants.Status.Warning.ordinal();
                else if (SO.Shipment_Value_USD__c != null ||cost.CostStatus__c == Constants.Status.Warning.ordinal())
                    cost.CostStatus__c = Constants.Status.Good.ordinal();
                chargeableAmount = SO.Shipment_Value_USD__c;
            }
            when 'Chargeable weight' { // when block 3
                if (SO.Chargeable_Weight__c == null || SO.Chargeable_Weight__c == 0)
                    cost.CostStatus__c = Constants.Status.Warning.ordinal();
                else if (SO.Chargeable_Weight__c > 0 ||cost.CostStatus__c == Constants.Status.Warning.ordinal())
                    cost.CostStatus__c = Constants.Status.Good.ordinal();
                chargeableAmount = SO.Chargeable_Weight__c == null ? 0 : SO.Chargeable_Weight__c;
            }
            when '# Packages' { // when block 3
                if (SO.of_packages__c == null || SO.of_packages__c == 0)
                    cost.CostStatus__c = Constants.Status.Warning.ordinal();
                else if (SO.of_packages__c > 0 || cost.CostStatus__c == Constants.Status.Warning.ordinal())
                    cost.CostStatus__c = Constants.Status.Good.ordinal();
                chargeableAmount = SO.of_packages__c == null ? 0 : SO.of_packages__c;
            }
            when '# Parts (line items)' { // when block 3
                if (SO.of_Line_Items__c  == null || SO.of_Line_Items__c == 0)
                    cost.CostStatus__c = Constants.Status.Warning.ordinal();
                else if (SO.of_Line_Items__c > 0 || cost.CostStatus__c == Constants.Status.Warning.ordinal())
                    cost.CostStatus__c = Constants.Status.Good.ordinal();
                chargeableAmount = SO.of_Line_Items__c == null ? 0 : SO.of_Line_Items__c;
            }
            when '# Unique HS Codes' { // when block 3
                if (SO.of_Unique_Line_Items__c == null || SO.of_Unique_Line_Items__c == 0)
                    cost.CostStatus__c = Constants.Status.Warning.ordinal();
                else if (SO.of_Unique_Line_Items__c > 0 || cost.CostStatus__c == Constants.Status.Warning.ordinal())
                    cost.CostStatus__c = Constants.Status.Good.ordinal();
                chargeableAmount = SO.of_Unique_Line_Items__c == null ? 0 : SO.of_Unique_Line_Items__c;
            }
            when 'Total Taxes' { // when block 3
                if (SO.IOR_Refund_of_Tax_Duty__c == null || SO.IOR_Refund_of_Tax_Duty__c == 0)
                    cost.CostStatus__c = Constants.Status.Warning.ordinal();
                else if (SO.IOR_Refund_of_Tax_Duty__c > 0 || cost.CostStatus__c == Constants.Status.Warning.ordinal())
                    cost.CostStatus__c = Constants.Status.Good.ordinal();
                chargeableAmount = SO.IOR_Refund_of_Tax_Duty__c == null ? 0 : SO.IOR_Refund_of_Tax_Duty__c;
            }
            when '# of Final Deliveries' { // when block 3
                if (SO.Final_Deliveries_New__c == null || SO.Final_Deliveries_New__c == 0)
                    cost.CostStatus__c = Constants.Status.Warning.ordinal();
                else if (SO.Final_Deliveries_New__c > 0 || cost.CostStatus__c == Constants.Status.Warning.ordinal())
                    cost.CostStatus__c = Constants.Status.Good.ordinal();

                chargeableAmount = SO.Final_Deliveries_New__c == null ? 0 : SO.Final_Deliveries_New__c;

            }
            when else{ // when else block, optional
                if (SO.Shipment_Value_USD__c == null || SO.Shipment_Value_USD__c == 0)
                    cost.CostStatus__c = Constants.Status.Warning.ordinal();
                else if (SO.Shipment_Value_USD__c > 0 || cost.CostStatus__c == Constants.Status.Warning.ordinal())
                    cost.CostStatus__c = Constants.Status.Good.ordinal();

                chargeableAmount = SO.Shipment_Value_USD__c;
            }
        }
        System.debug('cost.Name :: '+cost.Name);
        cost.Inactive__c = cost.Variable_threshold__c > 0 && chargeableAmount < cost.Variable_threshold__c;

        if (cost.Variable_threshold__c == 0 && cost.Max__c != null && cost.Min__c != null && cost.Rate__c != null && (cost.Applied_to__c == 'Shipment value' || cost.Applied_to__c == 'CIF value' || cost.Applied_to__c == 'Total Taxes')) {
            Decimal amount = (cost.Rate__c / 100) * chargeableAmount;
            Decimal calculatedAmount = amount > cost.Min__c ? (amount < cost.Max__c ? amount : cost.Max__c) : cost.Min__c;
            Decimal amountWithVat = cost.VAT_applicable__c == TRUE && cost.VAT_Rate__c != null ? calculatedAmount * (1 + (cost.VAT_Rate__c / 100)) : calculatedAmount;
            cost.Amount__c = cost.Additional_Percent__c != null ? amountWithVat * (1 + (cost.Additional_Percent__c / 100)) : amountWithVat;
            cost.Exchange_rate_forecast__c = costingCurrency.Conversion_Rate__c;
            cost.Amount_USD__c = cost.Amount__c * costingCurrency.Conversion_Rate__c;
        }
        else if (cost.Variable_threshold__c == 0 && cost.Max__c == null && cost.Min__c != null && cost.Rate__c != null && (cost.Applied_to__c == 'Shipment value' || cost.Applied_to__c == 'CIF value' || cost.Applied_to__c == 'Total Taxes')) {
            Decimal amount = (cost.Rate__c / 100) * chargeableAmount;
            Decimal calculatedAmount = amount < cost.Min__c ? cost.Min__c : amount;
            Decimal amountWithVat = cost.VAT_applicable__c == TRUE && cost.VAT_Rate__c != null ? calculatedAmount * (1 + (cost.VAT_Rate__c / 100)) : calculatedAmount;
            cost.Amount__c = cost.Additional_Percent__c != null ? amountWithVat * (1 + (cost.Additional_Percent__c / 100)) : amountWithVat;
            cost.Exchange_rate_forecast__c = costingCurrency.Conversion_Rate__c;
            cost.Amount_USD__c = cost.Amount__c * costingCurrency.Conversion_Rate__c;
        }
        else if (cost.Variable_threshold__c == 0 && cost.Max__c != null && cost.Min__c == null && cost.Rate__c != null && (cost.Applied_to__c == 'Shipment value' || cost.Applied_to__c == 'CIF value' || cost.Applied_to__c == 'Total Taxes')) {
            Decimal amount = (cost.Rate__c / 100) * chargeableAmount;
            Decimal calculatedAmount = amount < cost.Max__c ? amount : cost.Max__c;
            Decimal amountWithVat = cost.VAT_applicable__c == TRUE && cost.VAT_Rate__c != null ? calculatedAmount * (1 + (cost.VAT_Rate__c / 100)) : calculatedAmount;
            cost.Amount__c = cost.Additional_Percent__c != null ? amountWithVat * (1 + (cost.Additional_Percent__c / 100)) : amountWithVat;
            cost.Exchange_rate_forecast__c = costingCurrency.Conversion_Rate__c;
            cost.Amount_USD__c = cost.Amount__c * costingCurrency.Conversion_Rate__c;
        }
        else if (cost.Variable_threshold__c == 0 && cost.Max__c == null && cost.Min__c == null && cost.Rate__c != null && (cost.Applied_to__c == 'Shipment value' || cost.Applied_to__c == 'CIF value' || cost.Applied_to__c == 'Total Taxes')) {
            System.debug('Inside block');
            Decimal amount = (cost.Rate__c / 100) * chargeableAmount;
            Decimal calculatedAmount = amount;
            Decimal amountWithVat = cost.VAT_applicable__c == TRUE && cost.VAT_Rate__c != null ? calculatedAmount * (1 + (cost.VAT_Rate__c / 100)) : calculatedAmount;
            cost.Amount__c = cost.Additional_Percent__c != null ? amountWithVat * (1 + (cost.Additional_Percent__c / 100)) : amountWithVat;
            cost.Exchange_rate_forecast__c = costingCurrency.Conversion_Rate__c;
            cost.Amount_USD__c = cost.Amount__c * costingCurrency.Conversion_Rate__c;
            System.debug('cost.Amount__c :: '+cost.Amount__c);
        }
        else if (cost.Variable_threshold__c > 0 && cost.Max__c != null && cost.Min__c != null && cost.Rate__c != null && (cost.Applied_to__c == 'Shipment value' || cost.Applied_to__c == 'CIF value' || cost.Applied_to__c == 'Total Taxes')) {
            Decimal amount = (cost.Rate__c / 100) * (chargeableAmount - cost.Variable_threshold__c);
            Decimal calculatedAmount = amount > cost.Min__c ? (amount < cost.Max__c ? amount : cost.Max__c) : cost.Min__c;
            Decimal amountWithVat = cost.VAT_applicable__c == TRUE && cost.VAT_Rate__c != null ? calculatedAmount * (1 + (cost.VAT_Rate__c / 100)) : calculatedAmount;
            cost.Amount__c = cost.Additional_Percent__c != null ? amountWithVat * (1 + (cost.Additional_Percent__c / 100)) : amountWithVat;
            cost.Exchange_rate_forecast__c = costingCurrency.Conversion_Rate__c;
            cost.Amount_USD__c = cost.Amount__c * costingCurrency.Conversion_Rate__c;
        }
        else if (cost.Variable_threshold__c > 0 && cost.Max__c == null && cost.Min__c != null && cost.Rate__c != null && (cost.Applied_to__c == 'Shipment value' || cost.Applied_to__c == 'CIF value' || cost.Applied_to__c == 'Total Taxes')) {
            Decimal amount = (cost.Rate__c / 100) * (chargeableAmount - cost.Variable_threshold__c);
            Decimal calculatedAmount = amount < cost.Min__c ? cost.Min__c : amount;
            Decimal amountWithVat = cost.VAT_applicable__c == TRUE && cost.VAT_Rate__c != null ? calculatedAmount * (1 + (cost.VAT_Rate__c / 100)) : calculatedAmount;
            cost.Amount__c = cost.Additional_Percent__c != null ? amountWithVat * (1 + (cost.Additional_Percent__c / 100)) : amountWithVat;
            cost.Exchange_rate_forecast__c = costingCurrency.Conversion_Rate__c;
            cost.Amount_USD__c = cost.Amount__c * costingCurrency.Conversion_Rate__c;
        }
        else if (cost.Variable_threshold__c > 0 && cost.Max__c != null && cost.Min__c == null && cost.Rate__c != null && (cost.Applied_to__c == 'Shipment value' || cost.Applied_to__c == 'CIF value' || cost.Applied_to__c == 'Total Taxes')) {
            Decimal amount = (cost.Rate__c / 100) * (chargeableAmount - cost.Variable_threshold__c);
            Decimal calculatedAmount = amount < cost.Max__c ? amount : cost.Max__c;
            Decimal amountWithVat = cost.VAT_applicable__c == TRUE && cost.VAT_Rate__c != null ? calculatedAmount * (1 + (cost.VAT_Rate__c / 100)) : calculatedAmount;
            cost.Amount__c = cost.Additional_Percent__c != null ? amountWithVat * (1 + (cost.Additional_Percent__c / 100)) : amountWithVat;
            cost.Exchange_rate_forecast__c = costingCurrency.Conversion_Rate__c;
            cost.Amount_USD__c = cost.Amount__c * costingCurrency.Conversion_Rate__c;
        }
        else if (cost.Variable_threshold__c > 0 && cost.Max__c == null && cost.Min__c == null && cost.Rate__c != null && (cost.Applied_to__c == 'Shipment value' || cost.Applied_to__c == 'CIF value' || cost.Applied_to__c == 'Total Taxes')) {
            Decimal amount = (cost.Rate__c / 100) * (chargeableAmount - cost.Variable_threshold__c);
            Decimal calculatedAmount = amount;
            Decimal amountWithVat = cost.VAT_applicable__c == TRUE && cost.VAT_Rate__c != null ? calculatedAmount * (1 + (cost.VAT_Rate__c / 100)) : calculatedAmount;
            cost.Amount__c = cost.Additional_Percent__c != null ? amountWithVat * (1 + (cost.Additional_Percent__c / 100)) : amountWithVat;
            cost.Exchange_rate_forecast__c = costingCurrency.Conversion_Rate__c;
            cost.Amount_USD__c = cost.Amount__c * costingCurrency.Conversion_Rate__c;
        }
        else if (cost.Variable_threshold__c == 0 && cost.Max__c != null && cost.Min__c != null && cost.Rate__c != null && (cost.Applied_to__c == 'Chargeable weight' || cost.Applied_to__c == '# Parts (line items)'  || cost.Applied_to__c == '# Unique HS Codes' || cost.Applied_to__c == '# Packages' || cost.Applied_to__c == '# of Final Deliveries')) {
            Decimal amount = cost.Rate__c  * chargeableAmount;
            Decimal calculatedAmount = amount > cost.Min__c ? (amount < cost.Max__c ? amount : cost.Max__c) : cost.Min__c;
            Decimal amountWithVat = cost.VAT_applicable__c == TRUE && cost.VAT_Rate__c != null ? calculatedAmount * (1 + (cost.VAT_Rate__c / 100)) : calculatedAmount;
            cost.Amount__c = cost.Additional_Percent__c != null ? amountWithVat * (1 + (cost.Additional_Percent__c / 100)) : amountWithVat;
            cost.Exchange_rate_forecast__c = costingCurrency.Conversion_Rate__c;
            cost.Amount_USD__c = cost.Amount__c * costingCurrency.Conversion_Rate__c;
        }
        else if (cost.Variable_threshold__c == 0 && cost.Max__c == null && cost.Min__c != null && cost.Rate__c != null && (cost.Applied_to__c == 'Chargeable weight' || cost.Applied_to__c == '# Parts (line items)'  || cost.Applied_to__c == '# Unique HS Codes' || cost.Applied_to__c == '# Packages' || cost.Applied_to__c == '# of Final Deliveries')) {
            Decimal amount = cost.Rate__c  * chargeableAmount;
            Decimal calculatedAmount = amount < cost.Min__c ? cost.Min__c : amount;
            Decimal amountWithVat = cost.VAT_applicable__c == TRUE && cost.VAT_Rate__c != null ? calculatedAmount * (1 + (cost.VAT_Rate__c / 100)) : calculatedAmount;
            cost.Amount__c = cost.Additional_Percent__c != null ? amountWithVat * (1 + (cost.Additional_Percent__c / 100)) : amountWithVat;
            cost.Exchange_rate_forecast__c = costingCurrency.Conversion_Rate__c;
            cost.Amount_USD__c = cost.Amount__c * costingCurrency.Conversion_Rate__c;
        }
        else if (cost.Variable_threshold__c == 0 && cost.Max__c != null && cost.Min__c == null && cost.Rate__c != null && (cost.Applied_to__c == 'Chargeable weight' || cost.Applied_to__c == '# Parts (line items)'  || cost.Applied_to__c == '# Unique HS Codes' || cost.Applied_to__c == '# Packages' || cost.Applied_to__c == '# of Final Deliveries')) {
            Decimal amount = cost.Rate__c  * chargeableAmount;
            Decimal calculatedAmount = amount < cost.Max__c ? amount : cost.Max__c;
            Decimal amountWithVat = cost.VAT_applicable__c == TRUE && cost.VAT_Rate__c != null ? calculatedAmount * (1 + (cost.VAT_Rate__c / 100)) : calculatedAmount;
            cost.Amount__c = cost.Additional_Percent__c != null ? amountWithVat * (1 + (cost.Additional_Percent__c / 100)) : amountWithVat;
            cost.Exchange_rate_forecast__c = costingCurrency.Conversion_Rate__c;
            cost.Amount_USD__c = cost.Amount__c * costingCurrency.Conversion_Rate__c;
        }
        else if (cost.Variable_threshold__c == 0 && cost.Max__c == null && cost.Min__c == null && cost.Rate__c != null && (cost.Applied_to__c == 'Chargeable weight' || cost.Applied_to__c == '# Parts (line items)'  || cost.Applied_to__c == '# Unique HS Codes' || cost.Applied_to__c == '# Packages' || cost.Applied_to__c == '# of Final Deliveries')) {
            Decimal amount = cost.Rate__c  * chargeableAmount;
            Decimal calculatedAmount = amount;
            Decimal amountWithVat = cost.VAT_applicable__c == TRUE && cost.VAT_Rate__c != null ? calculatedAmount * (1 + (cost.VAT_Rate__c / 100)) : calculatedAmount;
            cost.Amount__c = cost.Additional_Percent__c != null ? amountWithVat * (1 + (cost.Additional_Percent__c / 100)) : amountWithVat;
            cost.Exchange_rate_forecast__c = costingCurrency.Conversion_Rate__c;
            cost.Amount_USD__c = cost.Amount__c * costingCurrency.Conversion_Rate__c;
        }
        else if (cost.Variable_threshold__c > 0 && cost.Max__c != null && cost.Min__c != null && cost.Rate__c != null && (cost.Applied_to__c == 'Chargeable weight' || cost.Applied_to__c == '# Parts (line items)'  || cost.Applied_to__c == '# Unique HS Codes' || cost.Applied_to__c == '# Packages' || cost.Applied_to__c == '# of Final Deliveries')) {
            Decimal amount = cost.Rate__c  * (chargeableAmount - cost.Variable_threshold__c);
            Decimal calculatedAmount = amount > cost.Min__c ? (amount < cost.Max__c ? amount : cost.Max__c) : cost.Min__c;
            Decimal amountWithVat = cost.VAT_applicable__c == TRUE && cost.VAT_Rate__c != null ? calculatedAmount * (1 + (cost.VAT_Rate__c / 100)) : calculatedAmount;
            cost.Amount__c = cost.Additional_Percent__c != null ? amountWithVat * (1 + (cost.Additional_Percent__c / 100)) : amountWithVat;
            cost.Exchange_rate_forecast__c = costingCurrency.Conversion_Rate__c;
            cost.Amount_USD__c = cost.Amount__c * costingCurrency.Conversion_Rate__c;
        }
        else if (cost.Variable_threshold__c > 0 && cost.Max__c == null && cost.Min__c != null && cost.Rate__c != null && (cost.Applied_to__c == 'Chargeable weight' || cost.Applied_to__c == '# Parts (line items)'  || cost.Applied_to__c == '# Unique HS Codes' || cost.Applied_to__c == '# Packages' || cost.Applied_to__c == '# of Final Deliveries')) {
            Decimal amount = cost.Rate__c  * (chargeableAmount - cost.Variable_threshold__c);
            Decimal calculatedAmount = amount < cost.Min__c ? cost.Min__c : amount;
            Decimal amountWithVat = cost.VAT_applicable__c == TRUE && cost.VAT_Rate__c != null ? calculatedAmount * (1 + (cost.VAT_Rate__c / 100)) : calculatedAmount;
            cost.Amount__c = cost.Additional_Percent__c != null ? amountWithVat * (1 + (cost.Additional_Percent__c / 100)) : amountWithVat;
            cost.Exchange_rate_forecast__c = costingCurrency.Conversion_Rate__c;
            cost.Amount_USD__c = cost.Amount__c * costingCurrency.Conversion_Rate__c;
        }
        else if (cost.Variable_threshold__c > 0 && cost.Max__c != null && cost.Min__c == null && cost.Rate__c != null && (cost.Applied_to__c == 'Chargeable weight' || cost.Applied_to__c == '# Parts (line items)'  || cost.Applied_to__c == '# Unique HS Codes' || cost.Applied_to__c == '# Packages' || cost.Applied_to__c == '# of Final Deliveries')) {
            Decimal amount = cost.Rate__c  * (chargeableAmount - cost.Variable_threshold__c);
            Decimal calculatedAmount = amount < cost.Max__c ? amount : cost.Max__c;
            Decimal amountWithVat = cost.VAT_applicable__c == TRUE && cost.VAT_Rate__c != null ? calculatedAmount * (1 + (cost.VAT_Rate__c / 100)) : calculatedAmount;
            cost.Amount__c = cost.Additional_Percent__c != null ? amountWithVat * (1 + (cost.Additional_Percent__c / 100)) : amountWithVat;
            cost.Exchange_rate_forecast__c = costingCurrency.Conversion_Rate__c;
            cost.Amount_USD__c = cost.Amount__c * costingCurrency.Conversion_Rate__c;
        }
        else if (cost.Variable_threshold__c > 0 && cost.Max__c == null && cost.Min__c == null && cost.Rate__c != null && (cost.Applied_to__c == 'Chargeable weight' || cost.Applied_to__c == '# Parts (line items)'  || cost.Applied_to__c == '# Unique HS Codes' || cost.Applied_to__c == '# Packages' || cost.Applied_to__c == '# of Final Deliveries')) {
            Decimal amount = cost.Rate__c  * (chargeableAmount - cost.Variable_threshold__c);
            Decimal calculatedAmount = amount;
            Decimal amountWithVat = cost.VAT_applicable__c == TRUE && cost.VAT_Rate__c != null ? calculatedAmount * (1 + (cost.VAT_Rate__c / 100)) : calculatedAmount;
            cost.Amount__c = cost.Additional_Percent__c != null ? amountWithVat * (1 + (cost.Additional_Percent__c / 100)) : amountWithVat;
            cost.Exchange_rate_forecast__c = costingCurrency.Conversion_Rate__c;
            cost.Amount_USD__c = cost.Amount__c * costingCurrency.Conversion_Rate__c;
        }
        else if (cost.Variable_threshold__c == 0 && cost.Max__c != null && cost.Min__c != null  && cost.Applied_to__c == null && cost.Amount__c != null) {
            Decimal amount = cost.Amount__c;
            Decimal calculatedAmount = amount > cost.Min__c ? (amount < cost.Max__c ? amount : cost.Max__c) : cost.Min__c;
            Decimal amountWithVat = cost.VAT_applicable__c == TRUE && cost.VAT_Rate__c != null ? calculatedAmount * (1 + (cost.VAT_Rate__c / 100)) : calculatedAmount;
            cost.Amount__c = cost.Additional_Percent__c != null ? amountWithVat * (1 + (cost.Additional_Percent__c / 100)) : amountWithVat;
            cost.Exchange_rate_forecast__c = costingCurrency.Conversion_Rate__c;
            cost.Amount_USD__c = cost.Amount__c * costingCurrency.Conversion_Rate__c;
        }
        else if (cost.Variable_threshold__c == 0 && cost.Max__c == null && cost.Min__c != null  && cost.Applied_to__c == null && cost.Amount__c != null) {
            Decimal amount = cost.Amount__c;
            Decimal calculatedAmount = amount < cost.Min__c ? cost.Min__c : amount;
            Decimal amountWithVat = cost.VAT_applicable__c == TRUE && cost.VAT_Rate__c != null ? calculatedAmount * (1 + (cost.VAT_Rate__c / 100)) : calculatedAmount;
            cost.Amount__c = cost.Additional_Percent__c != null ? amountWithVat * (1 + (cost.Additional_Percent__c / 100)) : amountWithVat;
            cost.Exchange_rate_forecast__c = costingCurrency.Conversion_Rate__c;
            cost.Amount_USD__c = cost.Amount__c * costingCurrency.Conversion_Rate__c;
        }
        else if (cost.Variable_threshold__c == 0 && cost.Max__c != null && cost.Min__c == null && cost.Applied_to__c == null && cost.Amount__c != null) {
            Decimal amount = cost.Amount__c;
            Decimal calculatedAmount = amount < cost.Max__c ? amount : cost.Max__c;
            Decimal amountWithVat = cost.VAT_applicable__c == TRUE && cost.VAT_Rate__c != null ? calculatedAmount * (1 + (cost.VAT_Rate__c / 100)) : calculatedAmount;
            cost.Amount__c = cost.Additional_Percent__c != null ? amountWithVat * (1 + (cost.Additional_Percent__c / 100)) : amountWithVat;
            cost.Exchange_rate_forecast__c = costingCurrency.Conversion_Rate__c;
            cost.Amount_USD__c = cost.Amount__c * costingCurrency.Conversion_Rate__c;
        }
        else if (cost.Variable_threshold__c == 0 && cost.Max__c == null && cost.Min__c == null && cost.Applied_to__c == null && cost.Amount__c != null) {
            Decimal amount = cost.Amount__c;
            Decimal calculatedAmount = amount;
            Decimal amountWithVat = cost.VAT_applicable__c == TRUE && cost.VAT_Rate__c != null ? calculatedAmount * (1 + (cost.VAT_Rate__c / 100)) : calculatedAmount;
            cost.Amount__c = cost.Additional_Percent__c != null ? amountWithVat * (1 + (cost.Additional_Percent__c / 100)) : amountWithVat;
            cost.Exchange_rate_forecast__c = costingCurrency.Conversion_Rate__c;
            cost.Amount_USD__c = cost.Amount__c * costingCurrency.Conversion_Rate__c;
        }
        else if(cost.Cost_Type__c == 'Fixed') {
            //cost.Amount__c = cost.Amount__c;
            cost.Exchange_rate_forecast__c = costingCurrency.Conversion_Rate__c;
            cost.Amount_USD__c = cost.Amount__c * costingCurrency.Conversion_Rate__c;

        }else {
            Decimal amountWithVat = cost.VAT_applicable__c == TRUE && cost.VAT_Rate__c != null ? (cost.Rate__c / 100 * chargeableAmount) * (1 + (cost.VAT_Rate__c / 100)) : (cost.Rate__c / 100 * chargeableAmount);
            cost.Amount__c = cost.Additional_Percent__c != null ? amountWithVat * (1 + (cost.Additional_Percent__c / 100)) : amountWithVat;
            cost.Exchange_rate_forecast__c = costingCurrency.Conversion_Rate__c;
            cost.Amount_USD__c = cost.Amount__c * costingCurrency.Conversion_Rate__c;
        }
        System.debug('cost.Amount__c :: '+cost.Amount__c);
        if (cost.condition__c != null) {
            if(String.isBlank(cost.Conditional_logic__c)) cost.Amount__c = -1;
            else{
                Boolean result = evaluateCostConditions(so,cost);
                if(!result) cost.Amount__c = -1;
            }
        }
    }

    public static List<CPA_Costing__c> excludeAllCosts(Shipment_Order__c SO, List<CPA_Costing__c> allCosts){
        List<CPA_Costing__c> costingList = new List<CPA_Costing__c>();

        for(CPA_Costing__c cost : allCosts) {
            System.debug('Cost.Id'+cost.Id);
            if(String.isBlank(cost.Conditional_logic__c)) costingList.add(cost);
            else if(evaluateCostConditions(so,cost)) {
                System.debug('Cost Result '+true);
                costingList.add(cost);
            }
        }
        return costingList;
    }

    private static Boolean evaluateCostConditions(Shipment_Order__c SO,CPA_Costing__c cost){
        String condition_logic = cost.Conditional_logic__c;

        Map<String,Boolean> conditionResults = new Map<String,Boolean>();

        //calculate condition 1
        if(String.isBlank(cost.Condition__c) || String.isBlank(cost.Conditional_value__c)) {
            conditionResults.put('1',false);
        }
        else {
            conditionResults.put('1',calculateCondition(so,cost.Conditional_value__c,cost.condition__c,cost.Value__c));
        }

        //calculate condition 2
        if(String.isBlank(cost.Condition_2__c) || String.isBlank(cost.Conditional_value_2__c)) {
            conditionResults.put('2',false);
        }
        else {
            conditionResults.put('2',calculateCondition(so,cost.Conditional_value_2__c,cost.Condition_2__c,cost.Value_2__c));
        }

        //calculate condition 3
        if(String.isBlank(cost.Condition_3__c) || String.isBlank(cost.Conditional_value_3__c)) {
            conditionResults.put('3',false);
        }
        else {
            conditionResults.put('3',calculateCondition(so,cost.Conditional_value_3__c,cost.Condition_3__c,cost.Value_3__c));
        }

        //calculate condition 4
        if(String.isBlank(cost.Condition_4__c) || String.isBlank(cost.Conditional_value_4__c)) {
            conditionResults.put('4',false);
        }
        else {
            conditionResults.put('4',calculateCondition(so,cost.Conditional_value_4__c,cost.Condition_4__c,cost.Value_4__c));
        }

        //calculate condition 5
        if(String.isBlank(cost.Condition_5__c) || String.isBlank(cost.Conditional_value_5__c)) {
            conditionResults.put('5',false);
        }
        else {
            conditionResults.put('5',calculateCondition(so,cost.Conditional_value_5__c,cost.Condition_5__c,cost.Value_5__c));
        }

        System.debug('conditionResults :: '+conditionResults);

        for(String condNumber : conditionResults.keySet()) {
            condition_logic = condition_logic.replace(condNumber,String.valueOf(conditionResults.get(condNumber)));
        }
        return BooleanExpression.eval(condition_logic.toUpperCase());
    }

    private static Boolean calculateCondition(Shipment_Order__c SO,String conditional_value,String condition,String value){
        Object fieldValue;
        System.debug('cost.Conditional_value__c:: '+conditional_value);
        switch on conditional_value {
            when 'CIF value' {
                fieldValue = ((SO.Shipment_Value_USD__c + 250) * 1.05);
            }
            when  'Shipment value' {
                fieldValue = SO.Shipment_Value_USD__c;
            }
            when 'Chargeable weight' {
                fieldValue = SO.Chargeable_Weight__c;
            }
            when '# of packages' {
                fieldValue = SO.of_packages__c;
            }
            when 'Total Taxes' {
                fieldValue = SO.IOR_Refund_of_Tax_Duty__c;
            }
            when 'Ship from country' {
                fieldValue = SO.Ship_From_Country__c;
            }
            when 'Account name' {
                fieldValue = SO.Account__r.Name;
            }
            when 'Client Type' {
                fieldValue = SO.Account__r.Client_type__c;
            }
            when 'Staging Warehouse Preference' {
                fieldValue = SO.Account__r.Staging_Warehouse_Preferred_Method__c;
            }
            when 'Pre-Inspection Responsibility Preference (Client)' {
                fieldValue = SO.Account__r.Pre_Inspection_Responsibility_Preference__c;
            }
            when '# of line items' {
                fieldValue = SO.of_Line_Items__c;
            }
            when '# of unique HS codes' {
                fieldValue = SO.of_Unique_Line_Items__c;
            }
            when '# of Final Deliveries' {
                fieldValue = SO.Final_Deliveries_New__c;
            }
            when 'Freight Responsibility' {
                fieldValue = SO.Who_arranges_International_courier__c;
            }
            when 'Invoice Timing' {
                fieldValue = SO.Invoice_Timing__c;
            }
            when 'Contains li-ion battery products' {
                fieldValue = SO.Li_ion_Batteries__c;
            }
            when 'Contains Batteries - Auto' {
                fieldValue = SO.Contains_Batteries_Auto__c;
            }
            when 'Contains Batteries - Client' {
                fieldValue = SO.Contains_Batteries_Client__c;
            }
            when 'Contains Wireless Goods - Auto' {
                fieldValue = SO.Contains_Wireless_Goods_Auto__c;
            }
            when 'Contains Wireless Goods - Client' {
                fieldValue = SO.Contains_Wireless_Goods_Client__c;
            }
            when 'Contains Encrypted Goods - Auto' {
                fieldValue = SO.Contains_Encrypted_Goods_Auto__c;
            }
            when 'Contains Encrypted Goods - Client' {
                fieldValue = SO.Contains_Encrypted_Goods_Client__c;
            }
            when 'Pre Inspection Responsibility (SO)' {
                fieldValue = SO.Pre_inspection_Responsibility__c;
            }
            when 'FFer only lane' {
                fieldValue = SO.CPA_v2_0__r.Freight_Only_Lane__c;
            }
            when 'Shipping sub status' {
                fieldValue = SO.Sub_Status_Update__c;
            }
            when 'Stuck Shipment' {
                fieldValue = SO.Stuck_Shipment__c;
            }
            when 'Shipping Complexity' {
                fieldValue = SO.Shipping_complexity__c;
            }
            when else{
                fieldValue = SO.Shipment_Value_USD__c;
            }
        }
        System.debug('condition:: '+condition);
        System.debug('fieldValue:: '+fieldValue);
        System.debug('value:: '+Value);

        if(fieldValue != null && value != null && condition == Constants.greater_Than && (Decimal)fieldValue > Decimal.valueOf(value)) {
            return true;
        }
        if(fieldValue != null && value != null && condition == Constants.greater_Equals_Than && (Decimal)fieldValue >= Decimal.valueOf(value)) {
            return true;
        }
        if(fieldValue != null && value != null && condition == Constants.Less_Equals_Than && (Decimal)fieldValue <= Decimal.valueOf(value)) {
            return true;
        }
        if( fieldValue != null && value != null && condition == Constants.Less_Than && (Decimal)fieldValue < Decimal.valueOf(value)) {
            return true;
        }
        if(condition == Constants.EQUALS) {
            if(fieldValue != null && value != null && fieldValue instanceOf Decimal && value.isNumeric() && (Decimal)fieldValue == Decimal.valueOf(value))
                return true;
            else return String.valueOf(fieldValue) == value;
        }
        if(condition == Constants.DOES_NOT_EQUAL && fieldValue != value) {
            return true;
        }
        if(fieldValue != null && value != null && condition == Constants.CONTAINS && String.valueOf(fieldValue).contains(value)) {
            return true;
        }
        if(fieldValue != null && value != null && condition == Constants.DOES_NOT_CONTAINS && !String.valueOf(fieldValue).contains(value)) {
            return true;
        }
        return false;
    }
}