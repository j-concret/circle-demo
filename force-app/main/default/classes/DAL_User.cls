public class DAL_User 
{
    public static final String QUEUE_TYPE = 'Queue';
    public static final String USER_TYPE_STANDARD = 'Standard';

	public static List<Group> getAllQueues()
	{
		return 
        [
            SELECT Id, DeveloperName
            FROM Group
            WHERE Type = :QUEUE_TYPE
        ];
	}

    //Random user that has access to Leads
    public static User getRandomConsultantUser()
    {
        List<User> users = 
        [            
            SELECT Id, Name 
            FROM User 
            WHERE IsActive = true
            AND UserType = :USER_TYPE_STANDARD
            AND Id IN 
                (
                    SELECT UserOrGroupId
                    FROM GroupMember 
                    WHERE Group.Name =:DAL_Lead.LEAD_POOL_NAME
                )
        ];
            
        System.assert(!users.isEmpty());
        Integer index = al.RandomUtils.nextInteger(users.size() - 1);
        return users[index];
    }

    public static Group getGroupByName(String name)
    {    	 
        List<Group> groups =
        [
            SELECT Id, Name
            FROM Group 
            WHERE TYPE = :QUEUE_TYPE 
            AND Name = :name
            LIMIT 1
        ];

        if(!groups.isEmpty())
            return groups[0];
        else
            return null;
    }

    public static User getDifferentUser(User userToNotUse)
    {
    	User alternateUser;

    	do {
            alternateUser = getRandomConsultantUser();
        } 
        while 
            (alternateUser.Id == userToNotUse.Id);

        return alternateUser;
    }
}