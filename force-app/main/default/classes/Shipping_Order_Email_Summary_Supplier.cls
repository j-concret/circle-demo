public class Shipping_Order_Email_Summary_Supplier {

    public static void sendEmailSummary(List<Shipment_Order__c> SOList, String SupplierId, String newEmailAddress){
        List <Shipment_Order__c> soListToSend = SOList;


        if(SupplierId!= null){

            soListToSend =  Shipping_Order_Email_Summary_Supplier.getSOList(SupplierId);
        }       

        Map <Id,List<Shipment_Order__c>> soMap = new Map <Id,List<Shipment_Order__c>>();
        Map <Id,List<String>> soTableMap = new Map <Id,List<String>>();
        
        List <Account> allAccounts;
        
        if(soListToSend.size()>0){
            for(Shipment_Order__c currentSO:soListToSend){
                String soName = currentSO.Name;
                String soAccountName = currentSO.Account__r.Name;
                String shipTo = currentSO.IOR_Price_List__r.Name;
                String shipVal = String.valueOf(currentSO.Shipment_Value_USD__c);
                String shipStatus = '';
                if(currentSO.Shipping_Status__c != null){
                    shipStatus = currentSO.Shipping_Status__c;
               }
                String shipCourier = currentSO.Int_Courier_Picklist__c;
                String shipTrackingNo = currentSO.Int_Courier_Tracking_No__c;
                
                String shipFinalClient= currentSO.Final_client_Name__c;
                String shipFinalAdd = currentSO.Final_Delivery_Address__c;
                String shipFinalContName = currentSO.Delivery_Contact_Name__c;
                String shipFinalContNumber = currentSO.Delivery_Contact_Tel_number__c;
                
                                

                if(soMap.containsKey(currentSO.SupplierlU__c)){
                    List<Shipment_Order__c> contactSO = soMap.get(currentSO.SupplierlU__c);
                    List<String> contactSOHtml = soTableMap.get(currentSO.SupplierlU__c);
                    contactSO.add(currentSO);
                    String htmlRows = '<tr><td style="padding:5px;border: 1px solid black;"">'+soName+'</td><td style="padding:5px;border: 1px solid black;"">'+soAccountName+'</td><td style="padding:5px;border: 1px solid black;"">'+shipTo +'</td><td style="padding:5px;border: 1px solid black;">'+shipVal+'</td><td style="padding:5px;border: 1px solid black;">'+shipStatus+'</td><td style="padding:5px;border: 1px solid black;" >'+shipCourier+'</td><td style="padding:5px;border: 1px solid black;">'+shipTrackingNo+'</td><td style="padding:5px;border: 1px solid black;">'+shipFinalClient+'</td><td style="padding:5px;border: 1px solid black;">'+shipFinalContName+'</td><td style="padding:5px;border: 1px solid black;">'+shipFinalContNumber+'</td><td style="padding:5px;border: 1px solid black;">'+shipFinalAdd+'&nbsp</td></tr>';
                    contactSOHtml.Add(htmlRows);
                    soTableMap.put(currentSO.SupplierlU__c,contactSOHtml);
                    soMap.put(currentSO.SupplierlU__c,contactSO);
                    }else{
                        List<Shipment_Order__c> contactSO = new List <Shipment_Order__c>();
                        List<String> contactSOHtml = new List <String>();
                        contactSO.add(currentSO);
                        String htmlRows = '<tr><td style="padding:5px;border: 1px solid black;border-collapse: collapse;">'+soName+'</td><td style="padding:5px;border: 1px solid black;border-collapse: collapse;">'+soAccountName+'</td><td style="padding:5px;border: 1px solid black;"">'+shipTo +'</td><td style="padding:5px;border: 1px solid black;">'+shipVal+'</td><td style="padding:5px;border: 1px solid black;">'+shipStatus+'</td><td style="padding:5px;border: 1px solid black;">'+shipCourier+'</td><td style="padding:5px;border: 1px solid black;">'+shipTrackingNo+'</td><td style="padding:5px;border: 1px solid black;">'+shipFinalClient+'</td><td style="padding:5px;border: 1px solid black;">'+shipFinalContName+'</td><td style="padding:5px;border: 1px solid black;">'+shipFinalContNumber+'</td><td style="padding:5px;border: 1px solid black;">'+shipFinalAdd+'&nbsp</td></tr>';
                        contactSOHtml.add(htmlRows);

                        soTableMap.put(currentSO.SupplierlU__c,contactSOHtml);
                        soMap.put(currentSO.SupplierlU__c,contactSO);
                    }
                }   

                Set <Id> accountSet = new Set <Id>();
                Map<Id,String> accountContactMap = new Map <id,String>();       

                allAccounts = [Select Id, Name, ICE__r.FirstName, ICE__r.LastName, ICE__r.Email from Account where Id IN :soMap.KeySet()];

                if(allAccounts.size()>0){
                  for(Account currentAccount : allAccounts) {
                    accountSet.add(currentAccount.Id);
                }
            }

            List <Contact> contactRecs = [Select Id,AccountId from Contact where AccountId in :accountSet];

            if(contactRecs.size()>0){
              for(Contact currentContact : contactRecs) {
                accountContactMap.put(currentContact.AccountId,currentContact.Id);
            }
        }

        If(allAccounts.size()>0){

          if(allAccounts.size()==1){

          }

          List <Messaging.SingleEmailMessage> mails = new List <Messaging.SingleEmailMessage>();
          for(Account currentAccount : allAccounts) {
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            string body = '<font face="Calibri">Hi,<br/><br/> Kindly see the consolidated updates as per COB today.';
            body += '<br/><br/><table cellspacing=0 style="font-family:Calibri;border: 1px solid black;border-collapse: collapse;"><tr><th style="padding:5px;border: 1px solid black;">Shipment Number</th><th style="padding:5px;border: 1px solid black;">Client Name</th><th style="padding:5px;border: 1px solid black;">Ship To</th><th style="padding:5px;border: 1px solid black;">Value (USD)</th><th style="padding:5px;border: 1px solid black;">Status</th><th style="padding:5px;border: 1px solid black;">Int Courier Name</th><th style="padding:5px;border: 1px solid black;">Int courier tracking number</th><th style="padding:5px;border: 1px solid black;">Final Delivery Client</th><th style="padding:5px;border: 1px solid black;">Final Delivery Client Contact</th><th style="padding:5px;border: 1px solid black;">Final Delivery Contact Number</th><th style="padding:5px;border: 1px solid black;">Final Delivery Address</th></tr>';
            List<String> mailTable= soTableMap.get(currentAccount.ID);
            if(mailTable.size()>0){
                for(String currentItem: mailTable){
                    body +=  currentItem;
                }
            }
            body += '</table><br/><br/>Please let me know if you have any queries in this regard.<br/><br/>Regards,<br/>The TecEx Team<br/><br/></font>';
            mail.setSubject('Current shipment orders for '+currentAccount.Name);
            if(newEmailAddress != null && newEmailAddress != ''){
              String[] toAddresses = new String[] {newEmailAddress};
              String[] ccAddresses = new String[] {currentAccount.ICE__r.Email};                            
              mail.setToAddresses(toAddresses);
              mail.setCcAddresses(ccAddresses);
              }else{
                 String[] toAddresses = new String[] {currentAccount.ICE__r.Email};
                 mail.setToAddresses(toAddresses);
             }
             mail.setSaveAsActivity(false);
             mail.setWhatId(currentAccount.Id);
             mail.setHtmlBody(body); 
             mails.add(mail);
             System.debug(body);
         }
         Messaging.sendEmail(mails);
     }
 }
}

public static List<Shipment_Order__c> getSOList(String SupplierId){

  List <Shipment_Order__c> soRecords = [Select Total_Invoiced_Amount__c,Total_Receipted__c,Int_Courier_Tracking_No__c,Id,Name,Client_Reference__c, SupplierlU__r.Name,Account__r.Name,Client_Contact_for_this_Shipment__c,Shipment_Status_Notes__c,Ship_From_Country__c,IOR_Price_List__r.Name ,Shipment_Value_USD__c,No_of_Parts__c, Shipping_Status__c,Final_Delivery_Date__c, Final_client_Name__c,Final_Delivery_Address__c, Delivery_Contact_Name__c,Delivery_Contact_Tel_number__c, Int_Courier_Picklist__c    from  Shipment_Order__c where Shipping_Status__c != 'POD Received' AND Shipping_Status__c != 'Cancelled - No fees/costs' AND Shipping_Status__c != 'Cancelled - No fees'  AND Shipping_Status__c != 'Cancelled - With fees/costs' AND Shipping_Status__c != 'Customs Clearance Docs Received' AND Shipping_Status__c != 'Cost Estimate' AND  Shipping_Status__c != 'Shipment Abandoned' AND Shipping_Status__c != 'Cost Estimate Abandoned' AND Shipping_Status__c != 'Roll-Out' AND Shipping_Status__c != 'Cost Estimate Rejected' AND SupplierlU__c = :SupplierId];

  if(soRecords.size()>0){
    return soRecords;
    }else{
        return null;
    }
}
}