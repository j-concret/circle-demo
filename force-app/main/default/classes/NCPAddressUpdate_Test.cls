@isTest
public class NCPAddressUpdate_Test {

    static testMethod void testPostMethod(){
        Access_token__c accessTokenObj = new Access_token__c();
        accessTokenObj.Status__c = 'Active';
        accessTokenObj.Access_Token__c = 'asdfghj-sdfgfds-sfdfgf';
        
        Insert accessTokenObj;
        
        Account acc = new Account(Name = 'Testing',type = 'Supplier',CSE_IOR__c = '0050Y000001LTZO', 
                                       Service_Manager__c = '0050Y000001LTZO', 
                                       Financial_Manager__c='0050Y000001LTZO', 
                                       Financial_Controller__c='0050Y000001LTZO');
        insert acc;

        Contact con = new Contact(FirstName = 'Testing',LastName = 'contact',AccountId = acc.Id,Email = 'test@test.com',Phone = '123456789');
        insert con;
        
        Id PickuprecordTypeId = Schema.SObjectType.Client_Address__c.getRecordTypeInfosByDeveloperName().get('PickUp').getRecordTypeId();

        Client_Address__c clientaddrObj = new Client_Address__c(Name='TestAddress',default__C=true,Address_status__c='Active',Comments__c='',All_Countries__c='Brazil',Client__c=acc.Id,City__c='Rio',CompanyName__c = 'Testing', Address__c='Adddress',Province__c = 'Trest',Postal_Code__c='304309',Contact_Phone_Number__c = '0722854399',Contact_Email__c ='TEst@test.com',Contact_Full_Name__c= 'TEstName');
        insert clientaddrObj;
        
        NCPAddressEditWra wrapper = new NCPAddressEditWra();
        wrapper.Accesstoken = accessTokenObj.Access_Token__c;
		wrapper.Name = '';
		wrapper.Contact_Full_Name = 'Testing';//con.Name;
		wrapper.Contact_Email = con.Email;
		wrapper.Contact_Phone_Number = con.Phone;
		wrapper.AddressLine1 = 'Address1';
		wrapper.AddressLine2 = 'Address2';
		wrapper.City = 'Rio';
		wrapper.Province = 'Northern';
		wrapper.Postal_Code = '12345';
		wrapper.All_Countries = clientaddrObj.All_Countries__c;
		wrapper.Id = clientaddrObj.Id;
        wrapper.AdditionalNumber = '8855220046';
     	wrapper.Comments = clientaddrObj.Comments__c;
        wrapper.CompanyName = clientaddrObj.CompanyName__c;
        wrapper.DefaultAddress = 'True';//String.valueOf(clientaddrObj.Default__c);
        wrapper.AddressStatus = clientaddrObj.Address_Status__c;
        
              
        RestRequest request = new RestRequest();
        RestResponse res = new RestResponse();
        request.requestUri ='/services/apexrest/api/UpdateAddress/';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueOf(JSON.serialize(wrapper));
        RestContext.request = request;
        RestContext.response = res;
        
        Test.startTest();
        NCPAddressUpdate.CPPickupAddress();
        Test.stopTest();
    }
    
    static testMethod void testPostMethodCatchBlock(){
        Access_token__c accessTokenObj = new Access_token__c();
        accessTokenObj.Status__c = 'Active';
        accessTokenObj.Access_Token__c = 'asdfghj-sdfgfds-sfdfgf';
        
        Insert accessTokenObj;
        
        Account acc = new Account(Name = 'Testing',type = 'Supplier',CSE_IOR__c = '0050Y000001LTZO', 
                                       Service_Manager__c = '0050Y000001LTZO', 
                                       Financial_Manager__c='0050Y000001LTZO', 
                                       Financial_Controller__c='0050Y000001LTZO');
        insert acc;

        Contact con = new Contact(FirstName = 'Testing',LastName = 'contact',AccountId = acc.Id,Email = 'test@test.com',Phone = '123456789');
        insert con;
        
        Id PickuprecordTypeId = Schema.SObjectType.Client_Address__c.getRecordTypeInfosByDeveloperName().get('PickUp').getRecordTypeId();

        Client_Address__c clientaddrObj = new Client_Address__c(Name='TestAddress',default__C=true,Address_status__c='Active',Comments__c='',All_Countries__c='Brazil',Client__c=acc.Id,City__c='Rio',CompanyName__c = 'Testing', Address__c='Adddress',Province__c = 'Trest',Postal_Code__c='304309',Contact_Phone_Number__c = '0722854399',Contact_Email__c ='TEst@test.com',Contact_Full_Name__c= 'TEstName');
        insert clientaddrObj;
        
        
        NCPAddressEditWra wrapper = new NCPAddressEditWra();
        wrapper.Accesstoken = accessTokenObj.Access_Token__c;
		wrapper.Name = '';
		wrapper.Contact_Full_Name = 'Testing';//con.Name;
		wrapper.Contact_Email = con.Email;
		wrapper.Contact_Phone_Number = con.Phone;
		wrapper.AddressLine1 = 'Address1';
		wrapper.AddressLine2 = 'Address2';
		wrapper.City = 'Rio';
		wrapper.Province = 'Northern';
		wrapper.Postal_Code = '12345';
		wrapper.All_Countries = clientaddrObj.All_Countries__c;
		wrapper.Id = clientaddrObj.Id;
        wrapper.AdditionalNumber = '8855220046';
     	wrapper.Comments = clientaddrObj.Comments__c;
        wrapper.CompanyName = clientaddrObj.CompanyName__c;
        wrapper.AddressStatus = clientaddrObj.Address_Status__c;
        
              
        RestRequest request = new RestRequest();
        RestResponse res = new RestResponse();
        request.requestUri ='/services/apexrest/api/UpdateAddress/';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueOf(JSON.serialize(wrapper));
        RestContext.request = request;
        RestContext.response = res;
        
        Test.startTest();
        NCPAddressUpdate.CPPickupAddress();
        Test.stopTest();
    }
}