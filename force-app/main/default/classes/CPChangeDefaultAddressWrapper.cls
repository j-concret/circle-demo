public class CPChangeDefaultAddressWrapper {
    public List<ChangeDefaultAddress> ChangeDefaultAddress;

	public class ChangeDefaultAddress {
		Public String All_Countries;
		public String ClientID;
        public String NewAddressID;
     	
	}

	
	public static CPChangeDefaultAddressWrapper parse(String json) {
		return (CPChangeDefaultAddressWrapper) System.JSON.deserialize(json, CPChangeDefaultAddressWrapper.class);
	}

}