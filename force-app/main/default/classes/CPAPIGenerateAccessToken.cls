@RestResource(urlMapping='/GenerateAccessToken/*')
Global class CPAPIGenerateAccessToken {
    @Httppost
    global static void GenerateAccessToken(){
     	 RestRequest req = RestContext.request;
         RestResponse res = RestContext.response;
         Blob body = req.requestBody;
    	 res.addHeader('Content-Type', 'application/json');
         String requestString = body.toString();
         CPAPIGenerateAccessTokenWra rw = (CPAPIGenerateAccessTokenWra)JSON.deserialize(requestString,CPAPIGenerateAccessTokenWra.class);

        try {
           Contact con =[select id, username__c from contact where username__c =:rw.Username and id =: rw.UserID];
            If(con.username__c == rw.Username && con.ID == rw.UserID ){
            String Accesstoken = GuidUtilCP.NewGuid();
            System.debug('Accesstoken'+Accesstoken);
            string Base64Accesstoken = EncodingUtil.base64Encode(Blob.valueof(Accesstoken));
            System.debug('Base64Accesstoken'+Base64Accesstoken);
                
                 Access_token__c At = New Access_token__c();
                	At.Access_Token__c=Accesstoken;
             		At.AccessToken_encoded__c=Base64Accesstoken;
                 Insert At;  
             
             
                
                 JSONGenerator gen = JSON.createGenerator(true);
                        gen.writeStartObject();                
                gen.writeFieldName('AccessToken');
                                          gen.writeStartObject();
                                          	if(at.AccessToken_encoded__c== null) {gen.writeNullField('AccessToken_encoded');} else{gen.writeStringField('AccessToken_encoded', at.AccessToken_encoded__c);}
                							
                 							gen.writeEndObject();
                 gen.writeEndObject();
                 string jsonData = gen.getAsString();
                
             
                res.responseBody = Blob.valueOf(jsonData);
        	 	res.statusCode = 200;
                
                
                
            }
            
            Else{
                
                 string ErrorString = 'Please check Username,Password and UserID';
                 res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
        	 	 res.statusCode = 400;
                
                
            }
        
            
        }
        Catch(exception e){
                string ErrorString = 'Please check Username,Password and UserID';
                 res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
        	 	 res.statusCode = 500;
        
        
        }
                
        
    }

}