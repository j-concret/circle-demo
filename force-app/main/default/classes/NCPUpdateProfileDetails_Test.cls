@isTest
public class NCPUpdateProfileDetails_Test {
	
    public static testMethod void NCPCreateCase_Test1(){
        
        //Test Data
        Access_token__c at = new Access_token__c(status__c = 'Active', Access_Token__c = '123');
        insert at;
        Contact con = TestDataFactory.myContact();
        
        //Json Body
        String json = '{"Accesstoken":"'+at.Access_Token__c+'","ContactID":"'+con.Id+'","Firstname":"'+con.FirstName+'","Lastname":"'+con.LastName+'","email":"'+con.Email+'",'+
            +'"ContactNumber":"1234","ContactRole":"'+con.Contact_Role__c+'","Newtask":true,"taskcompletion":true,"Quotestatusupdate":true,'+
            +'"Majorupdate":true,"Minorupdate":true}';
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
        req.requestURI = '/services/apexrest/NCPUpdateProfileDetails'; 
        req.httpMethod = 'Post';
        req.addHeader('Content-Type', 'application/json');
        req.requestBody = Blob.valueof(json);
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
        NCPUpdateProfileDetails.NCPCreateCase();
        Test.stopTest(); 
    }
    
    public static testMethod void NCPCreateCase_Test2(){
        
        //Test Data
        Access_token__c at = new Access_token__c(status__c = 'Active', Access_Token__c = '123');
        insert at;
        
        Contact con             = new Contact();
        con.LastName            = 'Testing1';
        con.Email               = 'dayeniofficial@gmail.com';
        con.Contact_Role__c     = 'Executive';
        con.Phone = '123456';
        con.Email = '123@gmail.com';
        insert con;
        
        //Json Body
        String json = '{"Accesstoken":"'+at.Access_Token__c+'","ContactID":"'+con.Id+'","Firstname":"'+con.FirstName+'","Lastname":"'+con.LastName+'","email":"'+con.Email+'",'+
            +'"ContactNumber":"'+con.Phone+'","ContactRole":"'+con.Contact_Role__c+'","Newtask":true,"taskcompletion":true,"Quotestatusupdate":true,'+
            +'"Majorupdate":true,"Minorupdate":true}';
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
        req.requestURI = '/services/apexrest/NCPUpdateProfileDetails'; 
        req.httpMethod = 'Post';
        req.addHeader('Content-Type', 'application/json');
        req.requestBody = Blob.valueof(json);
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
        NCPUpdateProfileDetails.NCPCreateCase();
        Test.stopTest(); 
    }
}