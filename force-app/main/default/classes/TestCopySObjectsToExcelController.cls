/**
* ApexClass   	: TestCopySObjectsToExcelController.cls
* Description	: Test class for the CopyExcelToSObjectsController
*/
@isTest
public with sharing class TestCopySObjectsToExcelController {
    
    /**
* @description : Setting data for the test method.
*/
    public static void setupData(){
        //Create the custom setting record
        InvoiceExcelCopyPasteConfiguration__c config = new InvoiceExcelCopyPasteConfiguration__c();
        config.Name='Invoice_Line_Item__c';        
        config.DisplayFieldset__c = 'InvoiceLineItemDisplayFieldSet';
        config.ParentFieldSet__c = 'InvoiceLineItemParentFieldSet';
        config.MaxRows__c = 25;        
        insert config;
    }
    
    /**
* @description : This verifies the records are creating successfully on suppliying the data from the page.
*/
    @isTest public static void copySObjectsToExcelControllerTest(){
        PageReference copyExcelToSObjects = Page.InvoiceLineItemQuickEntry;
        Invoice__c invoiceTest = new Invoice__c(Name='TestBillxyz7890');
        insert invoiceTest;
        //put the parameter
        copyExcelToSObjects.getParameters().put('sobjectName','Invoice_Line_Item__c');
        copyExcelToSObjects.getParameters().put('id',invoiceTest.Id);
        setupData();
        Test.setCurrentPage(copyExcelToSObjects);
        Test.startTest();
        List < Invoice_Line_Item__c > invoiceLineItemObjList = new List < Invoice_Line_Item__c > {
            new Invoice_Line_Item__c()
                };
                    ApexPages.StandardSetController stdController = new ApexPages.StandardSetController(invoiceLineItemObjList);
        InvoiceLineItemQuickEntryController ctrl = new InvoiceLineItemQuickEntryController(stdController);
        
        ctrl.record.put('Invoice__c',invoiceTest.Id); 
        ctrl.defaultFields.clear();
        ctrl.defaultFields.add('Invoice__c');
        System.debug(ctrl.columnsSerialized);//this will call its getter
        String jsonData = '[["USA","2","PALO ALTO 3060 FIREWALL","Palo Alto PA-3060","5A002a","8517.62.0050","20,520.00"]]';
        copyExcelToSObjects.getParameters().put('jsonData',jsonData);
        copyExcelToSObjects.getParameters().put('isColumnHeaderCustomized','false');
        //save records
        ctrl.saveRecords();
        //clear records
        ctrl.clearRecords();
        Test.stopTest();
        System.assert([SELECT Id From Invoice_Line_Item__c].size()>0);//This verifies that records are created.
    }
    
    /**
* @description : This will check exception handling in saveRecords
*/
    @isTest public static void copySObjectsToExcelControllerExceptonTest(){
        PageReference copyExcelToSObjects = Page.InvoiceLineItemQuickEntry;
        Invoice__c invoiceTest = new Invoice__c(Name='TestBillxyz7890');
        insert invoiceTest;
        //put the parameter
        copyExcelToSObjects.getParameters().put('sobjectName','Invoice_Line_Item__c');
        copyExcelToSObjects.getParameters().put('id',invoiceTest.Id);
        setupData();
        Test.setCurrentPage(copyExcelToSObjects);
        Test.startTest();
        List < Invoice_Line_Item__c > invoiceLineItemObjList = new List < Invoice_Line_Item__c > {
            new Invoice_Line_Item__c()
                };
                    ApexPages.StandardSetController stdController = new ApexPages.StandardSetController(invoiceLineItemObjList);
        InvoiceLineItemQuickEntryController ctrl = new InvoiceLineItemQuickEntryController(stdController);
        
        ctrl.record.put('Invoice__c',invoiceTest.Id); 
        ctrl.defaultFields.clear();
        ctrl.defaultFields.add('Invoice__c');
        System.debug(ctrl.columnsSerialized);//this will call its getter
        String jsonData = '[["USA","2","PALO ALTO 3060 FIREWALL","Palo Alto PA-3060","5A002a","8517.62.0050","20,520.00"]]';
        copyExcelToSObjects.getParameters().put('jsonData',jsonData);
        ctrl.SObjectType = null;//this will generate null pointer exception
        //save records
        ctrl.saveRecords();
        //clear records
        ctrl.clearRecords();
        Test.stopTest();
        System.assert([SELECT Id From Invoice_Line_Item__c].size()==0);//This verifies that no records are created.
    }
    
    /**
* @description : This will check exception handling in constructor
*/
    @isTest public static void copySObjectsToExcelControllerConstructorExceptonTest(){
        PageReference copyExcelToSObjects = Page.InvoiceLineItemQuickEntry;
        Invoice__c invoiceTest = new Invoice__c(Name='TestBillxyz7890');
        insert invoiceTest;
        //put the parameter
        copyExcelToSObjects.getParameters().put('sobjectName','Invoice__c');
        copyExcelToSObjects.getParameters().put('id',invoiceTest.Id);
        
        //data is not setup. so contructor will give exception
        Test.setCurrentPage(copyExcelToSObjects);
        Test.startTest();
        InvoiceLineItemQuickEntryController ctrl = new InvoiceLineItemQuickEntryController();            
        //save records
        ctrl.saveRecords();
        //clear records
        ctrl.clearRecords();
        Test.stopTest();
        System.assert([SELECT Id From Invoice_Line_Item__c].size()==0);//This verifies that no records are created.
    }
    
    
    /**
* @description : This will check exception handling in saveRecords
*/
    @isTest public static void invalidDataTest(){
        PageReference copyExcelToSObjects = Page.InvoiceLineItemQuickEntry;
        Invoice__c invoiceTest = new Invoice__c(Name='TestBillxyz7890');
        insert invoiceTest;
        //put the parameter
        copyExcelToSObjects.getParameters().put('sobjectName','Invoice_Line_Item__c');
        copyExcelToSObjects.getParameters().put('id',invoiceTest.Id);
        setupData();
        Test.setCurrentPage(copyExcelToSObjects);
        Test.startTest();
        List < Invoice_Line_Item__c > invoiceLineItemObjList = new List < Invoice_Line_Item__c > {
            new Invoice_Line_Item__c()
                };
                    ApexPages.StandardSetController stdController = new ApexPages.StandardSetController(invoiceLineItemObjList);
        InvoiceLineItemQuickEntryController ctrl = new InvoiceLineItemQuickEntryController(stdController);
        
        ctrl.record.put('Invoice__c',invoiceTest.Id); 
        ctrl.defaultFields.clear();
        ctrl.defaultFields.add('Invoice__c');
        System.debug(ctrl.columnsSerialized);//this will call its getter
        String jsonData = '[["USA","2","PALO ALTO 3060 FIREWALL","Palo Alto PA-3060","5A002a","8517.62.0050","20,520.00"]]';
        copyExcelToSObjects.getParameters().put('jsonData',jsonData);
        ctrl.SObjectType = null;//this will generate null pointer exception
        //save records
        ctrl.saveRecords();
        //clear records
        ctrl.clearRecords();
        Test.stopTest();
        System.assert([SELECT Id From Invoice_Line_Item__c].size()==0);//This verifies that no records are created.
    }
    
    
    /**
* @description : This will check exception handling in saveRecords
*/
    @isTest public static void invalidPageReference(){
        PageReference copyExcelToSObjects = Page.InvoiceLineItemQuickEntry;
        Invoice__c invoiceTest = new Invoice__c(Name='TestBillxyz7890');
        insert invoiceTest;
        setupData();
        System.assert([SELECT Id From Invoice_Line_Item__c].size()==0);//This verifies that no records are created.
    }

    /**
* @description : Controller run for manually selected columns
*/
    @isTest public static void copySObjectsToExcelManualColumnTest(){
        PageReference copyExcelToSObjects = Page.InvoiceLineItemQuickEntry;
        Invoice__c invoiceTest = new Invoice__c(Name='TestBillxyz7890');
        insert invoiceTest;
        //put the parameter
        copyExcelToSObjects.getParameters().put('sobjectName','Invoice_Line_Item__c');
        copyExcelToSObjects.getParameters().put('id',invoiceTest.Id);
        setupData();
        Test.setCurrentPage(copyExcelToSObjects);
        Test.startTest();
        List < Invoice_Line_Item__c > invoiceLineItemObjList = new List < Invoice_Line_Item__c > {
            new Invoice_Line_Item__c()
                };
                    ApexPages.StandardSetController stdController = new ApexPages.StandardSetController(invoiceLineItemObjList);
        InvoiceLineItemQuickEntryController ctrl = new InvoiceLineItemQuickEntryController(stdController);
        
        ctrl.record.put('Invoice__c',invoiceTest.Id); 
        ctrl.defaultFields.clear();
        ctrl.defaultFields.add('Invoice__c');
        System.debug(ctrl.columnsSerialized);//this will call its getter
        String jsonData = '[["USA","2","8517.62.0050","20,520.00"]]';
        String newCustomizedColumn = '["Country of Origin","Qty","HTS Code","Unit Value"]';
        copyExcelToSObjects.getParameters().put('jsonData',jsonData);
        copyExcelToSObjects.getParameters().put('isColumnHeaderCustomized','true');
        copyExcelToSObjects.getParameters().put('newCustomizedColumn',newCustomizedColumn);
        //save records
        ctrl.saveRecords();
        //clear records
        ctrl.clearRecords();
        Test.stopTest();
        List<Invoice_Line_Item__c> lineItemList = [SELECT Id,HTS_Code__c From Invoice_Line_Item__c ];
        System.assert(lineItemList.size()>0);//This verifies that records are created.

        System.assert('8517620050'.equals(lineItemList.get(0).HTS_Code__c),true);//This verifies that records are created.
    }
}