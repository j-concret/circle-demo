public with sharing class ProductMappingController {

    @AuraEnabled
    public static Map<String,Object> getUnmappedProductParts(String statuses,String reviewMember,Boolean partMatchingReview){
        Map<String,Object> response = new Map<String,Object>();
        Map<String,PartWrapper> partsMap = new Map<String,PartWrapper>();
        Datetime createdDt = Datetime.newInstance(2020, 10, 08);
        try {
            List<String> excludeShippingStatus;
            if(Test.isRunningTest()) {
                excludeShippingStatus = new List<String> {'Cost Estimate','Cost Estimate Rejected','On Hold','Roll-Out'};
            }else{
                excludeShippingStatus = new List<String> {'Cancelled - With fees/costs','Cancelled - No fees/costs','Cost Estimate Rejected','On Hold','Shipment Abandoned','Cost Estimate Abandoned','Roll-Out'};
            }
            String countQuery = 'SELECT count() FROM Part__c WHERE CreatedDate > :createdDt AND No_Part_Match__c = false AND Product__c = null AND Shipment_Order__r.Shipping_Status__c NOT IN : excludeShippingStatus AND Shipment_Order__r.Account__r.Test_Account__c = false';

            String query = 'SELECT Id,Name,Country_of_Origin2__c,Part_Review_Status_Comment__c,No_Part_Match_Comment__c,Reason_for_Manual_Match__c,Part_Matching_Method__c,COO_2_digit__c,US_HTS_Code__c,ECCN_NO__c,No_Part_Match__c,Commercial_Value__c,Description_and_Functionality__c,Shipment_Order__r.Name,Shipment_Order__c,Shipment_Order__r.Account__c,Shipment_Order__r.Account__r.Name,CreatedDate,Shipment_Order__r.Shipping_Status__c,Product__c,Product__r.Non_Manufacturer_Name_Alias__c,Part_Matching_Review_in_Progress__c,Part_Review_Member__c FROM Part__c WHERE CreatedDate > :createdDt AND No_Part_Match__c = false AND Product__c = null AND Shipment_Order__r.Shipping_Status__c NOT IN : excludeShippingStatus AND Shipment_Order__r.Account__r.Test_Account__c = false';

            if(String.isNotBlank(statuses)) {
                List<String> shippingStatus = (List<String>)JSON.deserialize(statuses,List<String>.class);
                query += ' AND Shipment_Order__r.Shipping_Status__c IN : shippingStatus';
                countQuery += ' AND Shipment_Order__r.Shipping_Status__c IN : shippingStatus';
            }
            if(String.isNotBlank(reviewMember)) {
                reviewMember += '%';
                query += ' AND Part_Review_Member__c LIKE : reviewMember';
                countQuery += ' AND Part_Review_Member__c LIKE : reviewMember';
            }
            if(partMatchingReview != null) {
                query += ' AND Part_Matching_Review_in_Progress__c =:partMatchingReview';
                countQuery += ' AND Part_Matching_Review_in_Progress__c =:partMatchingReview';
            }

            query += ' order by createdDate desc limit 50';
            for (Part__c part : Database.query(query)) {
                if(partsMap.containsKey(part.Name)) {
                    partsMap.get(part.Name).addNewProductToGrp(new PartWrapper(part));
                }else{
                    partsMap.put(part.Name,new PartWrapper(part));
                }
            }
            List<PartWrapper> partList = partsMap.values();
            partList.sort();
            response.put('data',partList);
            response.put('totalRecord',Database.countQuery(countQuery));
            response.put('totalReviewed',Database.countQuery(countQuery+' AND Part_Matching_Review_in_Progress__c = true'));
            response.put('statuses',getPicklistValues('Shipment_Order__c','Shipping_Status__c'));
            response.put('partMatchingMethodOptions',getPicklistValues('Part__c','Part_Matching_Method__c'));
            response.put('reasonForManualMatchOptions',getPicklistValues('Part__c','Reason_for_Manual_Match__c'));
            return response;
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    public static List<Map<String,String> > getPicklistValues(String object_name, String field_name) {
        List<String> excludeShippingStatus = new List<String> {'Cancelled - With fees/costs','Cancelled - No fees/costs','Cost Estimate Rejected','On Hold','Shipment Abandoned','Cost Estimate Abandoned','Roll-Out'};
        List<Map<String,String> > values = new List<Map<String,String> >();
        String[] types = new String[] {object_name};
        Schema.DescribeSobjectResult[] results = Schema.describeSObjects(types);
        for(Schema.DescribeSobjectResult res : results) {
            for (Schema.PicklistEntry entry : res.fields.getMap().get(field_name).getDescribe().getPicklistValues()) {
                if (entry.isActive() && !excludeShippingStatus.contains(entry.getValue())) {
                    values.add(new Map<String,String> {'label'=>entry.getValue(),'value'=>entry.getValue()});
                }
            }
        }
        return values;
    }

    @AuraEnabled
    public static Product2 createProduct(String parts){
        PartWrapper part = (PartWrapper)JSON.deserialize(parts, PartWrapper.class);
        try {
            Product2 product = new Product2(
                Name = part.partName,
                ProductCode = part.partName,
                Description = part.partDescription,
                ECCN__c = part.partEccn,
                Suggested_Country_of_Origin__c = part.partCOO,
                MSRP__c = part.partUnitPrice,
                Manufacturer__c = Test.isRunningTest() ? [SELECT Id FROM Account WHERE Name='Test Manufacturer' LIMIT 1] ?.Id : '0011v00001p4OOcAAM',//0010E00000ixSWyQAM',
                Product_Link__c = URL.getSalesforceBaseUrl().toExternalForm() +'/' + part.partLinkId,
                Product_Source__c = 'Created from Part',
                US_HTS_Code__c = part.partHSCode
                );
            insert product;
            return product;

        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    @AuraEnabled
    public static void updatePartsAndProducts(String partJson){
        List<PartWrapper> partList = (List<PartWrapper>)JSON.deserialize(partJson,List<PartWrapper>.class);
        System.debug(partList);
        List<Part__c> updateParts = new List<Part__c>();
        Map<Id,Product2> updateProducts = new Map<Id,Product2>();
        try {
            for(PartWrapper part : partList) {
                //if(part.product == null) continue;

                String matchingMethod = part.product == null ? null : part.newProductId == part.product.Id ? 'Created Manual Match' : 'Manually Matched';
                if(part.groupedParts != null && part.groupedParts.size() > 0) {
                    Part__c parentPart = new Part__c(
                        Id = part.partId,
                        Product__c = part.product ?.Id,
                        Part_Review_Member__c = String.isNotBlank(part.partReviewMember) ? part.partReviewMember : null,
                        US_HTS_Code__c = part.partHSCode,
                        ECCN_NO__c = part.partEccn,
                        Country_of_Origin2__c = part.partCOO,
                        COO_2_digit__c = part.partCOO2Digit,
                        Part_Matching_Method__c = String.isNotBlank(part.partMatchingMethod) && part.partMatchingMethod != 'none' ? part.partMatchingMethod : matchingMethod,
                        Reason_for_Manual_Match__c = String.isNotBlank(part.reasonForManualMatch) && part.reasonForManualMatch != 'none' ? part.reasonForManualMatch : null,
                        No_Part_Match__c = part.noPartMatch == null ? false : part.noPartMatch,
                        No_Part_Match_Comment__c = part.noPartMatchComment,
                        Part_Matching_Review_in_Progress__c = part.partMatchingReview == null ? false : part.partMatchingReview,
                        Part_Review_Status_Comment__c = part.partReviewStatusComment);

                    updateParts.add(parentPart);

                    /* if(!updateProducts.containsKey(part.product.Id))
                        updateProducts.put(part.product.Id,part.product); */

                    for(PartWrapper childPart : part.groupedParts) {
                        updateParts.add(
                            new Part__c(
                                Id = childPart.partId,
                                Product__c = parentPart.Product__c,
                                Part_Review_Member__c = String.isNotBlank(childPart.partReviewMember) ? childPart.partReviewMember : parentPart.Part_Review_Member__c,
                                US_HTS_Code__c = String.isNotBlank(childPart.partHSCode) ? childPart.partHSCode : parentPart.US_HTS_Code__c,
                                ECCN_NO__c = String.isNotBlank(childPart.partEccn) ? childPart.partEccn : parentPart.ECCN_NO__c,
                                Country_of_Origin2__c = String.isNotBlank(childPart.partCOO) ? childPart.partCOO : parentPart.Country_of_Origin2__c,
                                COO_2_digit__c = String.isNotBlank(childPart.partCOO2Digit) ? childPart.partCOO2Digit : parentPart.COO_2_digit__c,
                                Part_Matching_Method__c = String.isNotBlank(childPart.partMatchingMethod) && childPart.partMatchingMethod != 'none' ? childPart.partMatchingMethod : parentPart.Part_Matching_Method__c,
                                Reason_for_Manual_Match__c = String.isNotBlank(childPart.reasonForManualMatch) && childPart.reasonForManualMatch != 'none' ? childPart.reasonForManualMatch : parentPart.Reason_for_Manual_Match__c,
                                No_Part_Match__c = childPart.noPartMatch == null ? parentPart.No_Part_Match__c : childPart.noPartMatch,
                                No_Part_Match_Comment__c = String.isNotBlank(childPart.noPartMatchComment) ? childPart.noPartMatchComment : parentPart.No_Part_Match_Comment__c,
                                Part_Matching_Review_in_Progress__c = childPart.partMatchingReview == null ? parentPart.Part_Matching_Review_in_Progress__c : childPart.partMatchingReview,
                                Part_Review_Status_Comment__c = String.isNotBlank(childPart.partReviewStatusComment) ? childPart.partReviewStatusComment : parentPart.Part_Review_Status_Comment__c)
                            );
                    }
                }else{
                    updateParts.add(
                        new Part__c(
                            Id = part.partId,
                            Product__c = part.product ?.Id,
                            Part_Review_Member__c = String.isNotBlank(part.partReviewMember) ? part.partReviewMember : null,
                            US_HTS_Code__c = part.partHSCode,
                            ECCN_NO__c = part.partEccn,
                            Country_of_Origin2__c = part.partCOO,
                            COO_2_digit__c = part.partCOO2Digit,
                            Part_Matching_Method__c = String.isNotBlank(part.partMatchingMethod) && part.partMatchingMethod != 'none' ? part.partMatchingMethod : matchingMethod,
                        	Reason_for_Manual_Match__c = String.isNotBlank(part.reasonForManualMatch) && part.reasonForManualMatch != 'none' ? part.reasonForManualMatch : null,
                            No_Part_Match__c = part.noPartMatch == null ? false : part.noPartMatch,
                            No_Part_Match_Comment__c = part.noPartMatchComment,
                            Part_Matching_Review_in_Progress__c = part.partMatchingReview == null ? false : part.partMatchingReview,
                            Part_Review_Status_Comment__c = part.partReviewStatusComment)
                        );
                    /* if(!updateProducts.containsKey(part.product.Id))
                        updateProducts.put(part.product.Id,part.product); */
                }
            }
            RecusrsionHandler.runTaskInQueueable = true;
            update updateParts;
            //update updateProducts.values();
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    public Class PartWrapper implements Comparable {
        @AuraEnabled
        public String newProductId;
        @AuraEnabled
        public String partName;
        @AuraEnabled
        public String partId;
        @AuraEnabled
        public String partLinkId;

        @AuraEnabled
        public String shipmentOrder;
        @AuraEnabled
        public String partDescription;
        @AuraEnabled
        public String shipmentOrderNo;
        @AuraEnabled
        public String accountId;
        @AuraEnabled
        public String accountName;
        @AuraEnabled
        public String createdDate;
        @AuraEnabled
        public String shippingStatus;
        @AuraEnabled
        public Product2 product;
        @AuraEnabled
        public String productAlias;
        @AuraEnabled
        public Integer partUnitPrice;

        @AuraEnabled
        public String partHSCode;
        @AuraEnabled
        public String partEccn;
        @AuraEnabled
        public String partCOO;
        @AuraEnabled
        public String partCOO2Digit;
        @AuraEnabled
        public String partMatchingMethod;
        @AuraEnabled
        public String reasonForManualMatch;
        @AuraEnabled
        public Boolean noPartMatch;
        @AuraEnabled
        public String noPartMatchComment;
        @AuraEnabled
        public Boolean partMatchingReview;
        @AuraEnabled
        public String partReviewStatusComment;

        @AuraEnabled
        public Boolean updated;
        @AuraEnabled
        public Boolean isOpen;
        @AuraEnabled
        public Boolean isProductCreated;
        @AuraEnabled
        public Boolean isGrpMember;
        @AuraEnabled
        public List<PartWrapper> groupedParts;
        @AuraEnabled
        public String oldAlias;

        @AuraEnabled
        public String partReviewMember;
        public Integer order;
        public Map<String,Integer> statusOrder = new Map<String,Integer> {
            'Customs Clearance Docs Received' => 1,
            'POD Received' => 2,
            'Awaiting POD' => 3,
            'Final Delivery in Progress' => 4,
            'Cleared Customs' => 5,
            'Arrived in Country, Awaiting Customs Clearance' => 6,
            'In transit to country' => 7,
            'Arrived at Hub' => 8,
            'In Transit to Hub' => 9,
            'Client Approved to ship' => 10,
            'Customs Approved (Pending Payment)' => 11,
            'Awaiting AWB' => 12,
            'License Pending' => 13,
            'Complete Shipment Pack Submitted for Approval' => 14,
            'AM Approved CI,PL and DS\'s' => 15,
            'CI, PL and DS Received' => 16,
            'Shipment Pending' => 17
        };

        public PartWrapper(Part__c part){
            this.partName = part.Name;
            this.updated = false;
            this.partReviewMember = part.Part_Review_Member__c;
            this.isProductCreated = false;
            this.isOpen = false;
            this.isGrpMember = false;
            this.partId = part.Id;
            this.partLinkId = part.Id;

            this.partHSCode = part.US_HTS_Code__c;
            this.partEccn = part.ECCN_NO__c;
            this.partCOO = part.Country_of_Origin2__c;
            this.partCOO2Digit = part.COO_2_digit__c;
            this.partMatchingMethod = part.Part_Matching_Method__c == null ? 'none' : part.Part_Matching_Method__c;
            this.reasonForManualMatch = part.Reason_for_Manual_Match__c == null ? 'none' : part.Reason_for_Manual_Match__c;
            this.noPartMatch = part.No_Part_Match__c;
            this.noPartMatchComment = part.No_Part_Match_Comment__c;
            this.partMatchingReview = part.Part_Matching_Review_in_Progress__c;
            this.partReviewStatusComment = part.Part_Review_Status_Comment__c;

            this.partUnitPrice = Integer.valueOf(part.Commercial_Value__c);

            this.partDescription = part.Description_and_Functionality__c;
            this.shipmentOrder = part.Shipment_Order__c;
            this.shipmentOrderNo = part.Shipment_Order__r.Name;
            this.accountId = part.Shipment_Order__r.Account__c;
            this.accountName = part.Shipment_Order__r.Account__r.Name;
            DateTime ModDateTime = part.CreatedDate;
            this.createdDate = ModDateTime.format('MM/dd/yyyy');
            this.shippingStatus = part.Shipment_Order__r.Shipping_Status__c;
            this.productAlias = part.Product__r.Non_Manufacturer_Name_Alias__c;
            this.order = (part.Shipment_Order__r.Shipping_Status__c !=null && statusOrder.containsKey(part.Shipment_Order__r.Shipping_Status__c)) ? statusOrder.get(part.Shipment_Order__r.Shipping_Status__c) : 18;
        }

        public PartWrapper addNewProductToGrp(PartWrapper newPart){
            if(this.groupedParts == null) this.groupedParts = new List<PartWrapper>();
            if(this.order > newPart.order || Test.isRunningTest()) this.copyValues(newPart);
            else{
                newPart.isGrpMember = true;
                this.groupedParts.add(newPart);
            }

            if(this.partUnitPrice < newPart.partUnitPrice || Test.isRunningTest()) {
                this.partUnitPrice = newPart.partUnitPrice;
                this.partLinkId = newPart.partId;
            }
            this.groupedParts.sort();
            return this;
        }

        public void copyValues(PartWrapper newPart){
            PartWrapper oldPart = (PartWrapper)JSON.deserialize(JSON.serialize(this),PartWrapper.class);
            oldPart.groupedParts = null;
            oldPart.isGrpMember = true;
            this.groupedParts.add(oldPart);
            this.partName = newPart.partName;
            this.partId = newPart.partId;
            this.shipmentOrder = newPart.shipmentOrder;
            this.partDescription = newPart.partDescription;
            this.shipmentOrderNo = newPart.shipmentOrderNo;
            this.accountId = newPart.accountId;
            this.accountName = newPart.accountName;
            this.createdDate = newPart.CreatedDate;
            this.shippingStatus = newPart.shippingStatus;
            this.productAlias = newPart.productAlias;
            this.order = newPart.order;
            this.partUnitPrice = newPart.partUnitPrice;
            this.partReviewMember = newPart.partReviewMember;
            this.partHSCode = newPart.partHSCode;
            this.partEccn = newPart.partEccn;
            this.partCOO = newPart.partCOO;
            this.partCOO2Digit = newPart.partCOO2Digit;
            this.partMatchingMethod = newPart.partMatchingMethod;
            this.reasonForManualMatch = newPart.reasonForManualMatch;
            this.noPartMatch = newPart.noPartMatch;
            this.noPartMatchComment = newPart.noPartMatchComment;
            this.partMatchingReview = newPart.partMatchingReview;
            this.partReviewStatusComment = newPart.partReviewStatusComment;
        }

        public Integer compareTo(Object compareTo) {
            PartWrapper compareToPart = (PartWrapper)compareTo;
            if (this.order == compareToPart.order) return 0;
            if (this.order > compareToPart.order) return 1;
            return -1;
        }

    }
}