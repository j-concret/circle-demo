public class ZCPQuoteCreationWrap {
	public String AccountID;
	public String ContactID;
    PUblic String UserID;
	public String AccessToken;
	public List<Quotes> Quotes;

	public class FinalDelivieries {
		public String Name;
		public String FinalDestinationAddressID;
	}
    
  /*  public class LineItem {
		public String partNumber;
		public String description;
        public Integer quantity;
        public Double unitPrice;
        public String hsCode;
        public String countryOfOrigin;
	}*/

	public class Quotes {
		public Double estimatedChargableweight;
		public String ServiceType;
		public String Courier_responsibility;
		public String Reference1;
		public String Reference2;
		public String BuyerID;
		public String ShipFrom;
		public String ShipTo;
		public Double ShipmentvalueinUSD;
		public String PONumber;
		public String Type_of_Goods;
		public String Li_ion_Batteries;
		public String Li_ion_BatteryTypes;
		public Integer NumberOfFinaldeliveries;
		//public String FRContact_Full_Name;
		//public String FRContact_Email;
		//public String FRContact_Phone_Number;
		//public String FRAddress1;
		//public String FRAddress2;
		//public String FRCity;
		//public String FRProvince;
		//public String FRPostal_Code;
		Public String RegistrationID;
		public String FRAll_Countries;
		public String PickUpAddressId;
        public String preferredFreight;
		public List<ShipmentOrder_Packages> ShipmentOrder_Packages;
		public List<FinalDelivieries> FinalDelivieries;
//        public List<LineItem> LineItems;
	}

	public class ShipmentOrder_Packages {
		public String Weight_Unit;
		public String Dimension_Unit;
		public Integer Packages_of_Same_Weight;
		public Double Length;
		public Double Height;
		public Double Breadth;
		public Double Actual_Weight;
        Public Boolean LithiumBatteries;
        Public Boolean DangerousGoods;
        
	}

	
	public static NCPQuoteCreationWrapV2 parse(String json) {
		return (NCPQuoteCreationWrapV2) System.JSON.deserialize(json, NCPQuoteCreationWrapV2.class);
	}
}