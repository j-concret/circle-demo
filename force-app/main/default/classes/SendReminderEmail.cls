global class SendReminderEmail implements Schedulable {
    
    
    public SendReminderEmail(){
        
    }
    
    global void execute(SchedulableContext SC) {
        Map<String,List<Invoice_New__c>> accountInvoicesMap = new Map<String,List<Invoice_New__c>>();
        Map <Id,List<String>> soTableMap = new Map <Id,List<String>>();
        
        String clientRTypeId = Schema.Sobjecttype.Invoice_New__c.getRecordTypeInfosByDeveloperName().get('Client').getRecordTypeId();
        String AccClientRTypeId = Schema.Sobjecttype.Account.getRecordTypeInfosByDeveloperName().get('Client').getRecordTypeId();
        
        List<Invoice_New__c> allInvoices = [SELECT Id,Shipment_Order__c,Shipment_Order__r.Name,Name,Amount_Outstanding__c,Due_Date__c,Account__c,Account__r.RecordTypeId, RecordTypeId, Send_Reminder_Mail_Formula__c   
                                            FROM Invoice_New__c
                                            where Invoice_New__c.Send_Reminder_Mail_Formula__c  = true 
                                            AND Invoice_New__c.RecordTypeId =: clientRTypeId 
                                            AND Invoice_New__c.Account__r.RecordTypeId =: AccClientRTypeId 
                                            AND Invoice_New__c.Account__r.Debt_Reminder_Days_Before_Due_Date__c !=null 
                                            AND Invoice_New__c.Account__r.Debt_Reminder_Days_Before_Due_Date__c != 0 ORDER BY Due_Date__c ASC];
        
        
        if(!allInvoices.isEmpty()){
            for(Invoice_New__c invoice : allInvoices){
                if(accountInvoicesMap.containsKey(invoice.Account__c)){
                    accountInvoicesMap.get(invoice.Account__c).add(invoice);
                }else{
                    accountInvoicesMap.put(invoice.Account__c,new List<Invoice_New__c>{invoice});
                } 
            }
            
            List <Contact> contactRecs = [Select Id,AccountId,Email,CCD_Contact__c 
                                          from Contact 
                                          where AccountId in :accountInvoicesMap.keySet() AND Include_in_invoicing_emails__c = true AND Email !=null];
            
            Map<Id,List<String>>AccountContactEmailMap = new Map<Id,List<String>>();
            
            for(Contact con : contactRecs){
                if(AccountContactEmailMap.containsKey(con.AccountId)){
                   AccountContactEmailMap.get(con.AccountId).add(con.Email); 
                }else{
                    AccountContactEmailMap.put(con.AccountId,new List<String>{con.Email});
                }
                
            }
            List<Account> FinalAccounts = [Select Id,Account_Alias__c,Financial_Manager__c,Name,Financial_Controller__c,Debt_Reminder_Days_Before_Due_Date__c 
                                                                 from account 
                                                                 where id in :accountInvoicesMap.keySet()];
            
            List <Messaging.SingleEmailMessage> mails = new List <Messaging.SingleEmailMessage>();
            
            for(Account acc : FinalAccounts){
                if(accountInvoicesMap.containsKey(acc.id)){
                    String HTMLData = '';
                        for(Invoice_New__c invoice : accountInvoicesMap.get(acc.id)){
                            HTMLData += '<tr><td style="padding:5px;border: 1px solid black;"">'+invoice.Shipment_Order__r.Name+'</td><td style="padding:5px;border: 1px solid black;"">'+invoice.Name+'</td><td style="padding:5px;border: 1px solid black;">'+invoice.Amount_Outstanding__c+'</td><td style="padding:5px;border: 1px solid black;">'+String.valueOf(invoice.Due_Date__c).split(' ')[0]+'</td></tr>';
                        }
                    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                        System.debug(acc.Financial_Controller__c);
                        System.debug(acc.Financial_Manager__c);
                        
                        if(acc.Financial_Controller__c != null && acc.Financial_Manager__c !=null)
                        	mail.setCcAddresses(new List<String>{acc.Financial_Controller__c, acc.Financial_Manager__c});
                        mail.setSubject('Upcoming Invoices Due for '+acc.Name);
                        String mailBody = '<font face="Calibri">Disclaimer: Do not reply to this email as this inbox is not monitored. Please contact finance@tecex.com should you have any queries. <br/><br/>Dear '+acc.Name +' Team,<br/>'+'This is a friendly reminder that the following invoice(s) are due in the next '+acc.Debt_Reminder_Days_Before_Due_Date__c +' days';
                        mailBody += '<br/><br/><table cellspacing=0 style="font-family:Calibri;border: 1px solid black;border-collapse: collapse;"><tr><th style="padding:5px;border: 1px solid black;">Shipment Order</th><th style="padding:5px;border: 1px solid black;">Invoice Number</th><th style="padding:5px;border: 1px solid black;">Amount Outstanding(USD)</th><th style="padding:5px;border: 1px solid black;">Due Date</th></tr>';
                        mailBody +=  HTMLData+'</table><br/>';
                        mailBody += 'Please ensure that these invoices are paid before their due dates to avoid incurring any late payment fees.<br/>Have a great day further.<br/>TecEx';
                        
                    
                    mail.setToAddresses(AccountContactEmailMap.get(acc.Id));//
                        mail.setSaveAsActivity(false);
                        mail.setWhatId(acc.Id);
                        mail.setHtmlBody(mailBody);
                        mails.add(mail);
                }
            }
            
            Messaging.sendEmail(mails); 
        }
        
    }
}