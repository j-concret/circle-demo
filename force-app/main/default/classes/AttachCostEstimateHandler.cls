public class AttachCostEstimateHandler {

    public static Integer count = 0;
    public static boolean getSummaryCalledFromTaskTrigger = false;
    public static void processAfterUpdate(Map<Id,Shipment_Order__c> soMap) {

        List<Id> soidList = new List<Id>();
        Set<Id> cpaIds = new Set<Id>();
        for(shipment_order__c so : soMap.values()) {
            if(so.Shipping_Status__c !='Shipment Abandoned' && so.Shipping_Status__c !='Cost Estimate Abandoned' && so.Shipping_Status__c !='Roll-Out') {
                soidList.add(so.Id);
                cpaIds.add(so.CPA_v2_0__c);
            }
        }


        if(!soidList.isEmpty()) {
            RecusrsionHandler.taskTriggerRun = true;
            MasterTaskProcess.taskCreation(soidList);
            getSummaryCalledFromTaskTrigger = true;
            //ETACalculation.processETA(soidList, soMap.values());

            for(AggregateResult a : [SELECT count(Id) client_tasks,Shipment_Order__c from Task where Master_Task__c != null AND State__c='Client Pending' AND IsNotApplicable__c = false AND Shipment_Order__c IN: soMap.keySet() group by Shipment_Order__c]) {
                String sId = String.valueOf(a.get('Shipment_Order__c'));
                soMap.get(sId).Client_Task__c = (Decimal) a.get('client_tasks');
            }

            bindingQoute(soMap,cpaIds);
            SoLogOnTaskHelper.getSummary(soMap);
        }

    }

    public static void bindingQoute(Map<Id,Shipment_Order__c> soMap,Set<Id> cpaIds){
        Map<Id,List<String> > bindingQuoteFalseSO = new Map<Id,List<String> >();

        Map<Id,CPA_v2_0__c> cpas = new Map<Id,CPA_v2_0__c>([SELECT Id,Comments_On_Why_No_Final_Quote_Possible__c FROM CPA_v2_0__c WHERE Id IN: cpaIds]);

        for(Task tsk : [SELECT Id,State__c,Shipment_Order__c,Shipment_Order__r.CPA_v2_0__c,Master_Task__c,Master_Task__r.Name,Master_Task__r.Reason_for_Binding_Quote_Exempted__c,IsNotApplicable__c,Binding_Quote_Exempted_V2__c FROM Task WHERE Shipment_Order__c IN:soMap.keySet() AND Master_Task__c != null]) {

            if( (tsk.Binding_Quote_Exempted_V2__c == 'Always Exempt' && !tsk.IsNotApplicable__c) || (tsk.Binding_Quote_Exempted_V2__c == 'Exempt until resolved' && tsk.State__c !='Resolved' && !tsk.IsNotApplicable__c)) {

                String reason;
                if(tsk.Master_Task__r.Name == 'No Binding Quote Available') {
                    reason = cpas.get(tsk.Shipment_Order__r.CPA_v2_0__c).Comments_On_Why_No_Final_Quote_Possible__c;
                }else{
                    reason = tsk.Master_Task__r.Reason_for_Binding_Quote_Exempted__c;
                }

                if(bindingQuoteFalseSO.containsKey(tsk.Shipment_Order__c))
                    bindingQuoteFalseSO.get(tsk.Shipment_Order__c).add(reason == null ? '' : reason);
                else
                    bindingQuoteFalseSO.put(tsk.Shipment_Order__c,new List<String> {reason == null ? '' : reason});
            }
        }

        for(Id soId : soMap.keySet()) {
            List<String> reasons = new List<String>();

            if(soMap.get(soId).of_Line_Items__c == 0) {
                reasons.add('Tax calculations are not accurate because line-item details have not been provided');
            }
            if(soMap.get(soId).Invoice_Timing__c == 'Post-CCD Invoicing') {
                reasons.add('You have selected to be on a post-CCD invoice structure. TecEx will invoice you after the order has cleared customs based on the actual CCD amounts');
            }
            if(bindingQuoteFalseSO.containsKey(soId)) {
                reasons.addAll(bindingQuoteFalseSO.get(soId));
            }

            if(reasons.isEmpty()) {
                soMap.get(soId).Binding_Quote__c = true;
                soMap.get(soId).Reason_for_Pro_forma_quote__c = null;
            }else{
                soMap.get(soId).Binding_Quote__c = false;
                soMap.get(soId).Reason_for_Pro_forma_quote__c = String.join(reasons,';');
            }
        }
    }
}