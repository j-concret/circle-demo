@RestResource(urlMapping='/ZCPupdateAccRegistration/*')
Global class ZCPupdateRegistration {
    
     @Httppost
      global static void ZCPcreatenewregistration(){
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        Blob body = req.requestBody;
    	res.addHeader('Content-Type', 'application/json');
        String requestString = body.toString();
        ZCPupdateRegistrationwra rw  = (ZCPupdateRegistrationwra)JSON.deserialize(requestString,ZCPupdateRegistrationwra.class);
          Try{
              Access_token__c At =[select status__c,Access_Token__c from Access_token__c where Access_Token__c=:rw.Accesstoken ];
              if( at.status__C =='Active' ){
                  Registrations__c R = [select ID,name,VAT_number__c,Registered_Address__c,Registered_Address_2__c,Registered_Address_City__c,
                                        Registered_Address_Province__c,Registered_Address_Postal_Code__c,Registered_Address_Country__c,
                                        Type_of_registration__c,Finance_contact_Email__c,Finance_contact_Name__c,Finance_contact_Phone__c,
                                        Verified__c,Company_name__c,Country__c,Company_name__r.name
                                        
                                        from Registrations__c where id =: rw.RegistrationID limit 1 ];
                  r.Name=rw.name;
                  r.VAT_number__c=rw.VATnumber;
                  r.Registered_Address__c=rw.RegisteredAddress;
                  r.Registered_Address_2__c= rw.RegisteredAddress2;
                  r.Registered_Address_City__c= rw.RegisteredAddressCity;
                  r.Registered_Address_Province__c= rw.RegisteredAddressProvince;
                  r.Registered_Address_Postal_Code__c= rw.RegisteredAddressPostalCode;
                  r.Registered_Address_Country__c= rw.RegisteredAddressCountry;
                  r.Type_of_registration__c= rw.Typeofregistration;
                  r.Finance_contact_Email__c= rw.FinancecontactEmail;
                  r.Finance_contact_Name__c= rw.FinancecontactName;
                  r.Finance_contact_Phone__c= rw.FinancecontactPhone;
                  r.Verified__c= rw.Verified == null ? FALSE : rw.Verified ;
                  r.Company_name__c= rw.AccountID;
                  r.Country__c=rw.toCountry;
                 
                  update r;
                  
                  if(r.Id !=null){
                  Registrations__c re = [select ID,name,VAT_number__c,Registered_Address__c,Registered_Address_2__c,Registered_Address_City__c,
                                        Registered_Address_Province__c,Registered_Address_Postal_Code__c,Registered_Address_Country__c,
                                        Type_of_registration__c,Finance_contact_Email__c,Finance_contact_Name__c,Finance_contact_Phone__c,
                                        Verified__c,Company_name__c,Country__c,Company_name__r.name from Registrations__c where id =: r.Id and Company_name__c=:rw.AccountID];
                  
                   		JSONGenerator gen = JSON.createGenerator(true);
            			gen.writeStartObject();
            			gen.writeFieldName('Success');
                        gen.writeStartObject();
                        if(re.id == null){gen.writenullField('RegistrationID');}else{ gen.writeStringField('RegistrationID',re.Id);}
                  		if(re.name == null){gen.writenullField('RegCompanyName');}else{  gen.writeStringField('RegCompanyName',re.name);}
                  		if(re.VAT_number__c == null){gen.writenullField('VATnumber');}else{ gen.writeStringField('VATnumber',re.VAT_number__c);}
                  		if(re.Registered_Address__c == null){gen.writenullField('RegisteredAddress');}else{ gen.writeStringField('RegisteredAddress',re.Registered_Address__c);}
                  		if(re.Registered_Address_2__c == null){gen.writenullField('RegisteredAddress2');}else{ gen.writeStringField('RegisteredAddress2',re.Registered_Address_2__c);}
                  		if(re.Registered_Address_City__c == null){gen.writenullField('RegisteredAddressCity');}else{ gen.writeStringField('RegisteredAddressCity',re.Registered_Address_City__c);}
                  		if(re.Registered_Address_Province__c == null){gen.writenullField('RegisteredAddressProvince');}else{ gen.writeStringField('RegisteredAddressProvince',re.Registered_Address_Province__c);}
                  		if(re.Registered_Address_Postal_Code__c == null){gen.writenullField('RegisteredAddressPostalCode');}else{gen.writeStringField('RegisteredAddressPostalCode',re.Registered_Address_Postal_Code__c);}
                  		if(re.Registered_Address_Country__c == null){gen.writenullField('RegisteredAddressCountry');}else{gen.writeStringField('RegisteredAddressCountry',re.Registered_Address_Country__c);}
                  		if(re.Type_of_registration__c == null){gen.writenullField('Typeofregistration');}else{gen.writeStringField('Typeofregistration',re.Type_of_registration__c);}
                        if(re.Finance_contact_Email__c == null){gen.writenullField('FinancecontactEmail');}else{gen.writeStringField('FinancecontactEmail',re.Finance_contact_Email__c);}
                        if(re.Finance_contact_Name__c == null){gen.writenullField('FinancecontactName');}else{gen.writeStringField('FinancecontactName',re.Finance_contact_Name__c);}
                        if(re.Finance_contact_Phone__c == null){gen.writenullField('FinancecontactPhone');}else{gen.writeStringField('FinancecontactPhone',re.Finance_contact_Phone__c);}
                        if(re.Verified__c == null){gen.writenullField('Verified');}else{gen.writeBooleanField('Verified',re.Verified__c);}
                     
                     	
                      	gen.writeStringField('Companyname',re.Company_name__r.name);
                      	gen.writeStringField('Country',re.Country__c);
                      
                      
                        gen.writeEndObject();
                        gen.writeEndObject();
                    String jsonData = gen.getAsString();
            		res.responseBody = Blob.valueOf(jsonData);
            		res.addHeader('Content-Type', 'application/json');
            		res.statusCode = 200;
                    
                    	API_Log__c Al = New API_Log__c();
                        Al.EndpointURLName__c='ZeeRegistrratiocreation';
                        Al.Response__c='Registrations Updated'+re.Id  ;
                        Al.StatusCode__c=string.valueof(res.statusCode);
                        Insert Al;
                  }
                  
              }
          
          }
          catch(Exception e){
              
             		String ErrorString ='Something went wrong, please contact support_SF@tecex.com';
               		res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
            		res.addHeader('Content-Type', 'application/json');
        			res.statusCode = 500;
                
                API_Log__c Al = New API_Log__c();
                Al.EndpointURLName__c='ZeeRegistrratiocreation';
                Al.Response__c=e.getMessage()+' at line number: '+e.getLineNumber();
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al;
          }

      }

}