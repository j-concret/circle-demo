@isTest
public class ComplianceShipmentControllerTest {
    @testSetup
    static void setupData() {
        Account account = new Account (name='TecEx Prospective Client',  Type ='Client',  CSE_IOR__c = '0050Y000001km5c',  Service_Manager__c = '0050Y000001km5c',  Tax_recovery_client__c  = FALSE, Ship_From_Line_1__c = 'Ship_From_Line_1',
                                       Ship_From_Line_2__c = 'Ship_From_Line_2', Ship_From_Line_3__c = 'Ship_From_Line_3', Ship_From_City__c = 'Ship_From_City',  Ship_From_Zip__c = 'Ship_From_Zip',
                                       Ship_From_Country__c = 'Ship_From_Country',  Ship_From_Other_notes__c = 'Ship_From_Other', Ship_From_Contact_Name__c = 'Ship_From_Contact', Ship_From_Contact_Email__c = 'Ship_From_Email',
                                       Ship_From_Contact_Tel__c = 'Ship_From_Tel', Client_Address_Line_1__c = 'Client_Address_Line_1', Client_Address_Line_2__c = 'Client_Address_Line_2', Client_Address_Line_3__c = 'Client_Address_Line_3',
                                       Client_City__c = 'Client_City', Client_Zip__c = 'Client_Zip', Client_Country__c = 'Client_Country',  Other_notes__c = 'Other_notes',  Tax_Name__c = 'Tax_Name', Tax_ID__c = 'Tax_ID',
                                       Contact_Name__c = 'Contact_Name', Contact_Email__c = 'Contact_Email', Contact_Tel__c = 'Contact_Tel' ); insert account;


        Account account2 = new Account (name='Test Supplier', Type ='Supplier', ICE__c = '0050Y000001km5c', RecordTypeId = '0120Y0000006KjQQAU');
        insert account2;

        Contact contact = new Contact ( lastname ='Testing Individual',  AccountId = account.Id);
        insert contact;

        IOR_Price_List__c iorpl = new IOR_Price_List__c ( Client_Name__c = account.Id, Name = 'Brazil', IOR_Fee__c = 0.15, On_Charge_Mark_up__c = 0.15,
                                                          TecEx_Shipping_Fee_Markup__c =0.40, IOR_Min_Fee__c = 100,  Admin_Fee__c =100, Set_up_fee__c =100, Bank_Fees__c =200, Tax_Rate__c = 0.20,
                                                          Estimated_Customs_Brokerage__c =250, Estimated_Customs_Clearance__c =250, Estimated_Customs_Handling__c =250, Estimated_Import_License__c =250,
                                                          Destination__c = 'Brazil' );
        insert iorpl;

        country_price_approval__c cpa = new country_price_approval__c( name ='Brazil', Billing_term__c = 'DAP/CIF - IOR pays',  Airwaybill_Instructions__c = 'User IOR Address', Supplier__c = account2.Id,In_Country_Specialist__c = '0050Y000001km5c', Clearance_Destination__c = 'Brazil', Preferred_Supplier__c = TRUE,Destination__c = 'Brazil');
        insert cpa;



        Country__c country = new Country__c(Brokerage_Costs__c = 618, CIF_Adjustment_Factor__c = 0.035, CIF_Absolute_value_adjustment__c = 200, Clearance_Costs__c = 488,
                                            Country__c = 'Brazil', Name = 'Brazil', Handling_Costs__c = 1180, License_Permit_Costs__c = 0);
        insert country;

        CPA_v2_0__c aCpav = new CPA_v2_0__c(Name = 'Brazil', Country_Applied_Costings__c = TRUE);
        insert aCpav;

        CPA_v2_0__c cpav2 = new CPA_v2_0__c(Name = 'Brazil Testing 2.0', Supplier__c = account2.Id, Service_Type__c = 'IOR/EOR', Related_Costing_CPA__c = aCpav.Id,
                                            Preferred_Supplier__c = TRUE, VAT_Rate__c = 0.1, Destination__c = 'Brazil',  Final_Destination__c = 'Brazil',
                                            VAT_Reclaim_Destination__c = FALSE, Country__c = country.id, Lead_ICE__c = '0050Y000001km5c');
        insert cpav2;


        Currency_Management2__c CM = new Currency_Management2__c (Name = 'US Dollar (USD)', Currency__c = 'US Dollar (USD)', ISO_Code__c = 'USD', Conversion_Rate__c = 1);
        insert CM;

        Shipment_Order__c shipmentOrder = new Shipment_Order__c(Account__c = account.id, Shipment_Value_USD__c = 2000, CPA_v2_0__c = cpav2.Id,
                                                                Client_Contact_for_this_Shipment__c = contact.Id, Who_arranges_International_courier__c ='Client', Tax_Treatment__c ='DAP/CIF - IOR pays',
                                                                Ship_to_Country__c = cpa.Id, Destination__c = 'Brazil', Service_Type__c = 'IOR', IOR_Price_List__c = iorpl.Id, Chargeable_Weight__c = 200,
                                                                Shipping_Status__c = 'Cost Estimate Abandoned', Ship_From_Country__c = 'Angola' );
        insert shipmentOrder;

        ContentVersion cv1 = new Contentversion();
        cv1.Title = 'CZDSTOU';
        cv1.PathOnClient = 'test1';
        cv1.FirstPublishLocationId = shipmentOrder.Id;
        cv1.VersionData = EncodingUtil.base64Decode('Unit Test Attachment Body2');

        ContentVersion cv2 = new Contentversion();
        cv2.Title = 'CZDSTOURER';
        cv2.PathOnClient = 'test2';
        cv2.FirstPublishLocationId = shipmentOrder.Id;
        cv2.VersionData = EncodingUtil.base64Decode('Unit Test Attachment Body2');

        ContentVersion cv3 = new Contentversion();
        cv3.Title = 'CZDSTOURER';
        cv3.PathOnClient = 'test2';
        cv3.FirstPublishLocationId = shipmentOrder.Id;
        cv3.VersionData = EncodingUtil.base64Decode('Unit Test Attachment Body2');

        ContentVersion cv4 = new Contentversion();
        cv4.Title = 'CZDSTOURER';
        cv4.PathOnClient = 'test2';
        cv4.FirstPublishLocationId = shipmentOrder.Id;
        cv4.VersionData = EncodingUtil.base64Decode('Unit Test Attachment Body2');

        insert new List<ContentVersion> {cv1,cv2,cv3,cv4};
    }

    @isTest
    public static void getShipmentDetailsTest(){
        Shipment_Order__c shipmentOrderObj = [SELECT Id FROM Shipment_Order__c WHERE Destination__c = 'Brazil' LIMIT 1];
        Test.startTest();
        Map<String, Object> responseMap = ComplianceShipmentController.getShipmentDetails(shipmentOrderObj.Id);
        Test.stopTest();
        System.assertEquals(responseMap.get('userName'), UserInfo.getName());
    }
    
    @isTest
    public static void getShipmentDetailsNegativeTest(){
        Test.startTest();
        Map<String, Object> responseMap = ComplianceShipmentController.getShipmentDetails(null);
        Test.stopTest();
        System.assertEquals('ERROR',responseMap.get('status'));
    }

    @isTest
    public static void updateShipmentOrderReviewTest(){
        Shipment_Order__c shipmentOrderObj = [SELECT Id FROM Shipment_Order__c WHERE Destination__c = 'Brazil' LIMIT 1];
        Test.startTest();
        Map<String, Object> responseMap = ComplianceShipmentController.updateShipmentOrderReview(shipmentOrderObj.Id, true);
        Test.stopTest();
        System.assertEquals('OK',responseMap.get('status'));
    }
    
    @isTest
    public static void updateShipmentOrderReviewNegativeTest(){
        Test.startTest();
        Map<String, Object> responseMap = ComplianceShipmentController.updateShipmentOrderReview(null, true);
        Test.stopTest();
        System.assertEquals('ERROR',responseMap.get('status'));
    }
}