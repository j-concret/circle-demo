@IsTest
public class CSIDestinationCountries_Test {

    static testMethod void testNCPAPIDestinations(){
        RestRequest request = new RestRequest();
        RestResponse res = new RestResponse();
        request.requestUri ='/services/apexrest/api/MyDestinationCountrieslist/*';
        request.httpMethod = 'GET';
        RestContext.request = request;
        RestContext.response = res;
        Test.startTest();
        CSIDestinationCountries.NCPAPIDestinations();
        Test.stopTest();
    }
}