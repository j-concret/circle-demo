/*Change Orgid and Endpoint for Production
*/
@RestResource(urlMapping='/PortalLogin/*')
global without sharing class CSICommunityLoginAPI { 
    @HttpPost
    global static LoginResponse login() {
         
        LoginResponse objResponse = new LoginResponse();
         
        String username = RestContext.request.params.get('username');
        String password = RestContext.request.params.get('password');
        String domain = RestContext.request.params.get('domain');
         
        try{
        string LoginXML='<?xml version="1.0" encoding="UTF-8"?>';
        loginXML += '<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns1="urn:partner.soap.sforce.com">';
          loginXML += '<SOAP-ENV:Header>';
              loginXML +='<ns1:LoginScopeHeader>';
               loginXML += '<ns1:organizationId>00D7Z00000056Um</ns1:organizationId>'; // This has to change for Prod - 00D0Y000001KRPf, Account2-00D1q0000008awq
               loginXML += '</ns1:LoginScopeHeader>';
             loginXML += '</SOAP-ENV:Header>';
             loginXML += '<SOAP-ENV:Body>';
               loginXML += '<ns1:login>';
                loginXML +='<ns1:username>'+ userName+'</ns1:username>';
               loginXML += '<ns1:password>'+password+'</ns1:password>';
               loginXML += '</ns1:login>';
            loginXML += '</SOAP-ENV:Body>';
            loginXML +='</SOAP-ENV:Envelope>';
            
            
            String endpoint='https://'+domain+'.salesforce.com/services/Soap/u/22.0'; //For sandbox domain =test,  for prod --> domain=  login
           // String endpoint='https://login.salesforce.com/services/Soap/u/22.0'; //prod   login
           
            HttpRequest request = new HttpRequest();
            request.setEndpoint(endpoint);
            request.setTimeout(60000); 
            request.setMethod('POST');
            request.setHeader('SOAPAction','""');
            request.setHeader('Content-Type', 'text/xml;charset=UTF-8');
            request.setBody(loginXML);       
            HttpResponse response = new HttpResponse();
           
            response = new Http().send(request);
           
            String responseBody = response.getBody();
            system.debug('responseBody-->'+responseBody);
            String sessionId = getValueFromXMLString(responseBody, 'sessionId');
             
            objResponse.statusMessage = response.getStatus();
            objResponse.statusCode = response.getStatusCode();
             
            if(string.isNotBlank(sessionId)){
                objResponse.isSuccess = true;
                objResponse.sessionId = sessionId;
                 
            }else{
                objResponse.isSuccess = false;
            }
        }
        catch(System.Exception ex){
            objResponse.isSuccess = false;
            objResponse.statusMessage = ex.getMessage();
        }
        system.debug('objResponse-' + objResponse);
        return objResponse;
    }
     
    /*
* Get XML tag value from XML string
* @param xmlString : String XML
* @param keyField : XML key tag
* @return String : return XML tag key value
*/
    public static string getValueFromXMLString(string xmlString, string keyField){
        String xmlKeyValue = '';
        if(xmlString.contains('<' + keyField + '>')){
            try{
                xmlKeyValue = xmlString.substring(xmlString.indexOf('<' + keyField + '>')+keyField.length() + 2, xmlString.indexOf('</' + keyField + '>'));   
            }catch (exception e){
                 
            }            
        }
        return xmlKeyValue;
    }
     
    global class LoginResponse {
        public String sessionId {get; set;}
        public Boolean isSuccess {get; set;}
        public String statusMessage {get; set;}
        public Integer statusCode {get; set;}
    }
                                
                                
}