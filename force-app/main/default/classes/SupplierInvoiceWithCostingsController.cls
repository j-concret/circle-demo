public class SupplierInvoiceWithCostingsController {
    
    @AuraEnabled
    public static Map<String,Object> isCommunity(Id shipmentId){
        Map<String,Object> response = new Map<String,Object> {'status'=>'OK'};
            try {  
                Id siteId = Site.getSiteId();
                if(shipmentId != null){
                Shipment_Order__c shipmentOrder = [SELECT Id , Name , SupplierlU__r.Id , SupplierlU__r.Name , Account__r.Id , Account__r.Name FROM Shipment_Order__c where Id = :shipmentId limit 1];
                response.put('comShip',shipmentOrder);
                }
                if (siteId != null) {
                    Id userId = UserInfo.getUserId();
                   User u = [select Id, contactId from User where Id = : userId];
                   response.put('User',u);
                    if(u.contactId != null){
                    Id getContactId = u.contactId;
                    Contact contact = [SELECT Id , Name , Account.Id , Account.Name FROM Contact where Id =:getContactId];
                    response.put('comContact',contact);
                    }
                     response.put('isCommunityOpen',true);
                }else{
                    response.put('isCommunityOpen',false);
                }
            } catch (Exception e) {
                response.put('status','ERROR');
                response.put('msg',e.getMessage());
            }
        
        return response;
    }
    
    @AuraEnabled
    public static Map<String,Object> getPicklistValues(String objectName, String fieldName) {
        Map<String,Object> response = new Map<String,Object> {'status'=>'OK'};
            List<Map<String,String>> values = new List<Map<String,String>>();
        String[] types = new String[] {objectName};
            try {  
                Schema.DescribeSobjectResult[] results = Schema.describeSObjects(types);
                for(Schema.DescribeSobjectResult res : results) {
                    for (Schema.PicklistEntry entry : res.fields.getMap().get(fieldName).getDescribe().getPicklistValues()) {
                        if (entry.isActive()) {
                            values.add(new Map<String,String>{'label'=>entry.getValue(),'value'=>entry.getValue()});
                        }
                    }
                }
                response.put('statuses',values);
            } catch (Exception e) {
                response.put('status','ERROR');
                response.put('msg',e.getMessage());
            }
        
        return response;
    }
    
    @AuraEnabled
    public static Map<String,Object> getInvoiceDetail(Id invoiceId){
        Map<String,Object> response = new Map<String,Object> {'status'=>'OK'};
            try {  
                Id siteId = Site.getSiteId();
                if (siteId != null) {
                    response.put('isCommunityOpen',true);
                }else{
                    response.put('isCommunityOpen',false);
                }
                Invoice_New__c invoice = [select Id, Invoice_Name__c,Invoice_Status__c,Account__c,
                                          Invoice_amount_USD__c,Invoice_Date__c,Invoice_Currency__c,
                                          Invoice_Amount_Local_Currency__c,Shipment_Order__c,Shipment_Order__r.Name
                                          from Invoice_New__c where Id=:invoiceId];
                
                response.put('invoice',invoice);
            } catch (Exception e) {
                response.put('status','ERROR');
                response.put('msg',e.getMessage());
            }
        
        return response;                    
    }
    
    @AuraEnabled
    public static Map<String,Object> getCreatedInvoicesOnShipment(Id shipmentId,Id accountId){
        Map<String,Object> response = new Map<String,Object> {'status'=>'OK'};
            try {  
                Id supplierRecordType = Schema.Sobjecttype.Invoice_New__c.getRecordTypeInfosByDeveloperName().get('Supplier').getRecordTypeId();
                
                List<Invoice_New__c> invoicesList = [select Id, Invoice_Name__c,Invoice_Status__c,
                                                     Invoice_amount_USD__c,Invoice_Currency__c,Invoice_Amount_Local_Currency__c
                                                     from Invoice_New__c 
                                                     where Shipment_Order__c =: shipmentId 
                                                     AND Account__c =:accountId
                                                     AND RecordTypeId =:supplierRecordType
                                                     ORDER BY createdDate DESC];
                response.put('invoicesList',invoicesList);
            } catch (Exception e) {
                response.put('status','ERROR');
                response.put('msg',e.getMessage());
            }
        
        return response;                    
    }
    
    @AuraEnabled
    public static Map<String,Object> CreateSInvoices(Id shipmentId,Id accId,String curr,Date invDate,String invNum, Decimal totalInvPerDoc){
        Map<String,Object> response = new Map<String,Object> {'status'=>'OK'};
            try {  
                
                Currency_Management2__c CM = [select Currency__C, Conversion_rate__C 
                                              from Currency_Management2__c where Currency__C =:curr ];
                
                Shipment_Order__c SO = [Select POD_Date__c from  Shipment_Order__c where Id =:shipmentId];
                
                
                Invoice_New__c SI = new Invoice_New__c( 
                    Invoice_Name__c = invNum, 
                    Account__c =accId,
                    Invoice_Date__c = invDate, 
                    Invoice_Currency__c = curr,
                    Conversion_Rate__c = CM.Conversion_rate__C,
                    Shipment_Order__c = shipmentId, 
                    Invoice_Type__c = 'Invoice', 
                    Invoice_Status__c = 'New',
                    POD_Date_New__c = SO.POD_Date__c,
                    Total_Invoice_Amount_per_Document_LC__c = totalInvPerDoc,
                    RecordTypeId = Schema.Sobjecttype.Invoice_New__c.getRecordTypeInfosByDeveloperName().get('Supplier').getRecordTypeId());
               
                Insert SI;
                response.put('createdInvoice',SI);
            } catch (Exception e) {
                response.put('status','ERROR');
                response.put('msg',e.getMessage());
            }
        
        return response;  
    }
    
    @AuraEnabled
    public static Map<String,Object> getCostingsForInvoices(String PCurr,Id SupplierInvoiceId){
        //
        Map<String,Object> response = new Map<String,Object> {'status'=>'OK'};
            try {  
                //Id SupplierInvoiceId = 'a680Q000000Tj8NQAS';
                //String PCurr='US Dollar (USD)';
                Currency_Management2__c CM = [select Currency__C, Conversion_rate__C from Currency_Management2__c where Currency__C =:PCurr ];
                List<CPA_Costing__c> costingLists = new List<CPA_Costing__c>();
                Invoice_New__c SupplierInvoice = [select ID, name,Shipment_Order__c, Account__c,
                                                  Freight_Request__c, Conversion_Rate__c from Invoice_New__c 
                                                  where Id =: SupplierInvoiceId limit 1];
                
                String ShipmentOrderId = SupplierInvoice.Shipment_Order__c;
                String FreightRequestId = SupplierInvoice.Freight_Request__c;
                String SupplierId = SupplierInvoice.Account__c;
                String query = 'SELECT Additional_Percent__c, Comment__c , Invoice__c, Amended__c,'+
                    'Amount__c, Amount_USD__c, Applied_to__c, Ceiling__c,'+ 
                    ' Condition__c, Conditional_value__c, Cost_Category__c,'+ 
                    ' Cost_Note__c, Cost_Type__c, CostStatus__c, Name, CPA_v2_0__c,'+ 
                    ' CreatedById, Currency__c, Exchange_rate_forecast__c,'+ 
                    'Exchange_rate_payment_date__c, Floor__c, Amount_in_USD__c,'+ 
                    'Freight_Request__c, Invoice_amount_local_currency__c, '+
                    'Invoice_amount_USD_New__c, Invoice_currency__c, IOR_EOR__c, '+
                    'LastModifiedById, Max__c, Min__c, Rate__c, RecordTypeId, '+
                    ' Shipment_Order_Old__c, Status_Colour__c, Supplier_Invoice__c,'+ 
                    'Updating__c, Variable_threshold__c, VAT_applicable__c, VAT_Rate__c,Threshold_Percentage__c,Within_Threshold__c,'+ 
                    'VAT_Rate_Formula__c FROM CPA_Costing__c';
                String emptyInvoice = '';
                String condition ='';
                If(ShipmentOrderId != null && FreightRequestId == null){
                    condition = ' where Inactive__c = false and Shipment_Order_Old__c = :ShipmentOrderId and supplier__c = :SupplierId and (Invoice__c = :emptyInvoice or Invoice__c =: SupplierInvoiceId)  LIMIT 200';
                }
                else if(FreightRequestId != null && ShipmentOrderId == null){
                    condition = ' where Inactive__c = false and Freight_Request__c =: FreightRequestId and supplier__c = :SupplierId and (Invoice__c = :emptyInvoice or Invoice__c =: SupplierInvoiceId) LIMIT 200';
                }
                else{
                    condition = ' where Inactive__c = false and Shipment_Order_Old__c =:ShipmentOrderId and Freight_Request__c =: FreightRequestId  and supplier__c = :SupplierId  and (Invoice__c = :emptyInvoice or Supplier_Invoice__c =: SupplierInvoiceId) LIMIT 200';
                }
                query = query+condition;
                response.put('costingLists',Database.query(query));
                response.put('ShipmentOrderId',ShipmentOrderId);
                response.put('SupplierId',SupplierId);
                response.put('emptyInvoice',emptyInvoice);
                response.put('SupplierInvoiceId',SupplierInvoiceId);
                response.put('currencyFields',CM);
                response.put('query',query);
            } catch (Exception e) {
                response.put('status','ERROR');
                response.put('msg',e.getMessage());
            }
        
        return response;  
    }
    
    @AuraEnabled
    public static Map<String,Object> updateCostingLists(String PCurr,Id SupplierInvoiceId,List<CPA_Costing__c> updateCostings){
        //
        //String PCurr='US Dollar (USD)';
        //Id SupplierInvoiceId = 'a680Q000000Tj8NQAS';
        Map<String,Object> response = new Map<String,Object> {'status'=>'OK'};
            try{
                list<CPA_Costing__c> CCO = new list<CPA_Costing__c> ();
                Currency_Management2__c CM = [select Currency__C, Conversion_rate__C from Currency_Management2__c where Currency__C =:PCurr ];
                
                For(integer i=0; i<updateCostings.size(); i++){
                    If(updateCostings[i].Invoice_amount_local_currency__c != null){  
                        updateCostings[i].Exchange_rate_payment_date__c = CM.Conversion_rate__C;
                        updateCostings[i].Invoice_currency__c = PCurr;
                        updateCostings[i].Invoice__c = SupplierInvoiceId;
                        CCO.add(updateCostings[i]);
                    }
                }
                update CCO;
                response.put('CCO',CCO);
            } catch (Exception e) {
                response.put('status','ERROR');
                response.put('msg',e.getMessage());
            }
        
        return response;  
    }
    
    @AuraEnabled
    public static Map<String,Object> saveCostings(String PCurr,Id SupplierInvoiceId,List<CPA_Costing__c> costingsList){
        //
        //String PCurr='US Dollar (USD)';
        //Id SupplierInvoiceId = 'a680Q000000Tj8NQAS';
        Map<String,Object> response = new Map<String,Object> {'status'=>'OK'};
            try{
                list<CPA_Costing__c> CCO = new list<CPA_Costing__c> ();
                
                Currency_Management2__c CM = [select Currency__C, Conversion_rate__C 
                                              from Currency_Management2__c where Currency__C =:PCurr ];
                
                Invoice_New__c SI = [select ID, name,Shipment_Order__c,Account__c,
                                     Freight_Request__c, Conversion_Rate__c 
                                     from Invoice_New__c where id =: SupplierInvoiceId limit 1];
                
                Shipment_Order__c  SO = [Select Id,CPA_v2_0__r.Related_Costing_CPA__c,
                                         Service_Type__c from Shipment_Order__c 
                                         where id=: SI.Shipment_Order__c];
                
                Id AppliedCPA = so.CPA_v2_0__r.Related_Costing_CPA__c;
                
                
                For(integer i=0; i<costingsList.size(); i++){
                    
                    costingsList[i].CPA_v2_0__c = AppliedCPA;
                    costingsList[i].IOR_EOR__c = SO.Service_Type__c;
                    costingsList[i].Cost_Type__c = 'Fixed';
                    costingsList[i].Invoice_currency__c= PCurr;
                    costingsList[i].Shipment_Order_Old__c =So.id;
                    costingsList[i].Invoice__c = SupplierInvoiceId;
                    costingsList[i].Exchange_rate_payment_date__c = CM.Conversion_rate__C;
                    costingsList[i].RecordTypeId = Schema.Sobjecttype.CPA_Costing__c.getRecordTypeInfosByDeveloperName().get('Display').getRecordTypeId();
                    costingsList[i].Supplier__c = SI.Account__c;
                    costingsList[i].isManualCosting__c = true;
                    costingsList[i].Within_Threshold__c = 'No';
                    CCO.add(costingsList[i]);
                    
                }
                Insert CCO;
            } catch (Exception e) {
                response.put('status','ERROR');
                response.put('msg',e.getMessage());
            }
        
        return response;  
    }
    
    @AuraEnabled
    public static Map<String,Object> autoPayInvoices(Id supplierId,Id shipmentOrderId){
        Map<String,Object> response = new Map<String,Object> {'status'=>'OK'};
            
            try{
                Shipment_Order__c shipOrder =[SELECT Id , Name , CPA_v2_0__r.Related_Costing_CPA__c , CPA_v2_0__r.Threshold_Percentage__c, 
                                              (SELECT Amount_in_USD__c FROM CPA_Costing__r WHERE Supplier__c = :supplierId), 
                                              (SELECT Id,Name,Invoice_amount_USD__c,Invoice_Name__c 
                                               FROM Invoices2__r WHERE Account__c= :supplierId 
                                               ORDER BY CreatedDate ASC) 
                                              FROM Shipment_Order__c WHERE Id = :shipmentOrderId];
                
                List<Invoice_New__c> invoicesCost = [SELECT (SELECT Id,Invoice_amount_USD_New__c FROM Costings__r) FROM Invoice_New__c where Id in :shipOrder.Invoices2__r ORDER BY CreatedDate ASC];
                
                Decimal AmountInUSD = 0;
                for(CPA_Costing__c cpaCost:shipOrder.CPA_Costing__r){
                    
                    AmountInUSD +=  cpaCost.Amount_in_USD__c;
                    
                }
                Decimal InvoiceAmount = 0;
                List<Invoice_New__c> invoicesToUpdate = new List<Invoice_New__c>();
                Decimal calculationPercentage = 0;
                for(Invoice_New__c invoice: invoicesCost){
                    Decimal sumOfCostInvoiceAmount = 0 ;
                    for(CPA_Costing__c  cost:invoice.Costings__r){
                        sumOfCostInvoiceAmount += cost.Invoice_amount_USD_New__c;
                    }
                    InvoiceAmount +=  sumOfCostInvoiceAmount;
                    
                    System.debug('InvoiceAmount '+InvoiceAmount);
                    System.debug('AmountInUSD '+AmountInUSD);
                    calculationPercentage = null;
                    if(AmountInUSD != 0){
                        calculationPercentage = ((InvoiceAmount-AmountInUSD)/AmountInUSD)*100; 
                    }
                    System.debug('calculationPercentage '+calculationPercentage);
                    System.debug('shipOrder.CPA_v2_0__r.Threshold_Percentage__c '+shipOrder.CPA_v2_0__r.Threshold_Percentage__c);
                    if(calculationPercentage == null){
                        invoice.Auto_Pay__c = 'No';
                    }
                    else if(calculationPercentage !=0){
                        if(shipOrder.CPA_v2_0__r.Threshold_Percentage__c < calculationPercentage){
                            System.debug('AutoPay No ');
                            invoice.Auto_Pay__c = 'No';
                        }else{
                            System.debug('AutoPay Yes ');
                            invoice.Auto_Pay__c = 'Yes';
                        }
                    }
                    else{
                        System.debug('AutoPay Yes ');
                        invoice.Auto_Pay__c = 'Yes';
                        
                    }
                    invoice.Auto_pay_Threshold__c = shipOrder.CPA_v2_0__r.Threshold_Percentage__c;
                    invoice.Auto_Pay_Total_Forecast_Amount__c = AmountInUSD;
                    invoice.Auto_Pay_Total_Invoiced_Amount__c = InvoiceAmount;
                    invoicesToUpdate.add(invoice);  
                }
                update invoicesToUpdate;
            } catch (Exception e) {
                response.put('status','ERROR');
                response.put('msg',e.getMessage());
            }
        
        return response;  
    }
}