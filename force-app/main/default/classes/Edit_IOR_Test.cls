@isTest
public class Edit_IOR_Test {
    @isTest static void test_Edit_IOR() {
        Account testClient = new account(Name='TestClient',Client_Status__c = 'Prospect',Estimated_Vat__c = 123);
        
        insert testClient;
        
        IOR_Price_List__c testIOR = new IOR_Price_List__c();
            
        	testIOR.Client_Name__c = testClient.Id;
            testIOR.Name = 'Tokyo';
            testIOR.IOR_Fee__c = 1;
        
        insert testIOR;
        
        Test.startTest();
        
        PageReference pageRef = Page.EditIORPricelist;
        	
        pageRef.getParameters().put('id', String.valueOf(testClient.Id));
        
        Test.setCurrentPage(pageRef);
        
        	Edit_IOR_Cont testIOREdit = new Edit_IOR_Cont();
        
        	PageReference testPageRedirectSave = testIOREdit.save();
        
        	PageReference testPageRedirectCancel = testIOREdit.plsCancel();
        
        Test.stopTest();
    }
}