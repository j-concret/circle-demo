public class clientAddressLookUpController {
    @AuraEnabled
    public static List < sObject > fetchLookUpValues(String searchKeyWord, String ObjectName, String selectedField, String addressType) {
        
        String searchKey = searchKeyWord + '%';
        
        List < sObject > returnList = new List < sObject > ();
        String queryPart = 'select id, Name from ';
        
        String recordTypeCondition = '';
        String recordType = '';
        if(addressType.equalsIgnoreCase('pickup')){
            recordType = Schema.SObjectType.Client_Address__c.getRecordTypeInfosByName().get('PickUp').getRecordTypeId();
        }else{
            recordType = Schema.SObjectType.Client_Address__c.getRecordTypeInfosByName().get('Final Destinations').getRecordTypeId();
        }
        recordTypeCondition = ' recordTypeId =: recordType AND';
        
        // Create a Dynamic SOQL Query For Fetch Record List with LIMIT 5   
        String sQuery =  queryPart + ObjectName
            + ' where'+ recordTypeCondition +' Name ' 
            +' LIKE: searchKey limit 10';
        
        system.debug('sQuery ' + sQuery);
        
        List < sObject > lstOfRecords = Database.query(sQuery);
        
        for (sObject obj: lstOfRecords) {
            returnList.add(obj);
        }
        return returnList;
    }
    
}