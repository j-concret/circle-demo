public class LeadsHelper {
    
    public static void ZeeLeadsRoundRobin(List<Lead> Leads){
        // Get the queue user details
        List<Group> queues = [
            SELECT Id,
            (Select Id, UserOrGroupId FROM GroupMembers Order By ID ASC)
            FROM Group
            Where Type = 'Queue'AND DeveloperName = 'Zee_Lead_Queue'
        ];
        Boolean isLeadUpdated = false;
        System.debug('-->queues<--');
        System.debug(JSOn.serializePretty(queues));
        // Get the index of the last lead assigned user in the queue
        Lead_Round_Robin_Assignment__c lrr = Lead_Round_Robin_Assignment__c.getOrgDefaults();
        Integer userIndex = (lrr.get('User_Index__c') == null || Integer.valueOf(lrr.get('User_Index__c')) < -1) 
            ? -1 : Integer.valueOf(lrr.get('User_Index__c'));
        System.debug('-->Lead_Round_Robin_Assignment__c<--');
        System.debug(JSOn.serializePretty(lrr));
        String zeeRecordType = Schema.Sobjecttype.Lead.getRecordTypeInfosByDeveloperName().get('Client_E_commerce').getRecordTypeId();
        if (queues.size() > 0 && queues.get(0).GroupMembers.size() > 0) {
            Id queueId = queues.get(0).Id;
            Integer groupMemberSize = queues.get(0).GroupMembers.size();
            for (Lead l : Leads) {
                if (l.RecordTypeId == zeeRecordType) {
                    Integer leadUserIndex =  (userIndex + 1) >= groupMemberSize ? 0 : userIndex + 1;
                    l.OwnerId = queues.get(0).GroupMembers.get(leadUserIndex).UserOrGroupId;
                    userIndex = leadUserIndex;
                    isLeadUpdated = true;
                }
                
             }
            
            // Update the custom settings user index with the last lead assigned user
            lrr.User_Index__c = userIndex;
            System.debug('-->Updating Leads ');
            System.debug('LRR.ID---> '+lrr.id);
            System.debug(JSOn.serializePretty(lrr));
            if(isLeadUpdated)
            update lrr;
        }
    }
    
}