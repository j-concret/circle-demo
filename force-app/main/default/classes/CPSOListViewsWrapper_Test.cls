@IsTest
public class CPSOListViewsWrapper_Test {
    static testMethod void testParse() {
		String json = '{'+
		'\"AccountId\":\"0010Y00000PEqvH\",'+
		'\"ContactId\":\"0031v00001hY02H\",'+
		'\"ShippingStatus\":\"\'Cost Estimate\',\'Shipment Pending\',\'CI, PL and DS Received\'\",'+
		'\"shipfrom\":\"\'Afghanistan\',\'Antigua and Barbuda\',\'Falkland Islands (Islas Malvinas)\',\'Congo, Democratic Republic of the\'\",'+
		'\"shipto\":\"\'Afghanistan\',\'Antigua and Barbuda\',\'Falkland Islands (Islas Malvinas)\',\'Congo, Democratic Republic of the\'\",'+
		'\"servicetype\":\"\'IOR\',\'EOR\'\",'+
		'\"CourierResponsibility\":\"\'Client\',\'TecEx\'\"'+
		''+
		'}';
		CPSOListViewsWrapper obj = CPSOListViewsWrapper.parse(json);
		System.assert(obj != null);
	}

}