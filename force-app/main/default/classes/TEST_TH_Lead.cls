@isTest(seeAllData = false)
private class TEST_TH_Lead
{
    private static final Integer SMALL_LIMIT = 10;
    private static final Integer LARGE_LIMIT = 110;
    private static User testUser;
    private static Group testQueue;

    /*
    static testMethod void exceptionNotThrownForDifferentUsersWithDifferentLimits()
    {    
        User alternateTestUser;         

        testUser = DAL_User.getRandomConsultantUser();        
        alternateTestUser = DAL_User.getDifferentUser(testUser);

        Test.startTest();

        System.runAs(testUser)
        {
            UTIL_GlobalVariableManifest.LeadLimit = SMALL_LIMIT - 5;               
                         
            List<Lead> userLeads = TestUtils.createLeadsToOwner(SMALL_LIMIT - 5, testUser.Id);

            try 
            {
                insert userLeads;         
            } 
            catch(Exception e) 
            {
                System.assert(false, 'Trigger should not throw exception if user is under the limit: ' + e.getMessage());
            }
        }       

        //Using a different user, set his hierarchy custom setting to a different value and re-test
        System.runAs(alternateTestUser)
        {
            UTIL_GlobalVariableManifest.LeadLimit = SMALL_LIMIT;

            List<Lead> alternateUserLeads = DAL_Lead.createLeads(SMALL_LIMIT);
            DAL_Lead.changeOwnership(alternateUserLeads, alternateTestUser.Id);

            try 
            {
                insert alternateUserLeads;        
            } 
            catch(Exception e) 
            {
                System.assert(false, 'Trigger should not throw exception if user is under the limit: ' + e.getMessage());
            }
        }

        Test.stopTest();        
    }
    */

    //Test that an exception will be thrown if more Leads than the Limit are inserted 
    /*static testMethod void exceptionThrown()
    {   
           
        String actualMessage = null;        
        String expectedErrorMessage = String.format(DAL_Lead.ERROR_LEAD_ALLOCATION, new String[]{String.valueOf(SMALL_LIMIT)});         

        testQueue = DAL_User.getGroupByName(DAL_Lead.LEAD_POOL_NAME);        
        testUser = DAL_User.getRandomConsultantUser(); 

        Test.startTest();  

        System.runAs(testUser)
        {
            UTIL_GlobalVariableManifest.LeadLimit = SMALL_LIMIT;            
            List<Lead> userLeads = TestUtils.createLeadsToOwner(SMALL_LIMIT + 2, testUser.Id);

            try 
            {
                insert userLeads;
                System.assert(false, 'Trigger did not throw exception on insert');          
            } 
            catch(Exception e) 
            {
                actualMessage = e.getMessage();
                System.assertEquals(true, actualMessage.contains(expectedErrorMessage), 'Wrong error thrown: ' + actualMessage);
            }         

            //Test that an exception will be thrown if more Leads than the Limit are claimed by the user
            List<Lead> queueLeads = TestUtils.createLeadsToOwner(SMALL_LIMIT + 2, testQueue.Id);
            insert queueLeads;

            DAL_Lead.changeOwnership(queueLeads, testUser.Id);

            try 
            {
                update queueLeads;
                System.assert(false, 'Trigger did not throw exception on update');          
            } 
            catch(Exception e) 
            {
                actualMessage = e.getMessage();
                System.assertEquals(true, actualMessage.contains(expectedErrorMessage), 'Wrong error thrown: ' + actualMessage);
            }  
        }  

        Test.stopTest();        
    }

    static testMethod void exceptionNotThrownWithConvertedUpdate()
    {
        testQueue = DAL_User.getGroupByName(DAL_Lead.LEAD_POOL_NAME);        
        testUser = DAL_User.getRandomConsultantUser();  

        System.runAs(testUser)
        {
            //Ensure we switch off API Calls when doing lead conversions
            AppUtils.IS_API_CALL_OVERRIDE = true; 
            UTIL_GlobalVariableManifest.LeadLimit = SMALL_LIMIT;       
            
            DAL_Lead.addConvertedLeadsToUser(testUser);

            List<Lead> queueLeads = TestUtils.createLeadsToOwner(SMALL_LIMIT, testQueue.Id);        
            insert queueLeads;

            DAL_Lead.changeOwnership(queueLeads, testUser.Id);

            Test.startTest();

            try 
            {            
                update queueLeads;        
            } 
            catch(Exception e) 
            {
                System.assert(false, 'Trigger should not throw exception when user only breaches the limit if converted Leads are counted: ' + e.getMessage());
            }   

            Test.stopTest();
        }               
    }

    static testMethod void exceptionNotThrownWithConvertedInsert()
    {                  
        testQueue = DAL_User.getGroupByName(DAL_Lead.LEAD_POOL_NAME);        
        testUser = DAL_User.getRandomConsultantUser();

        System.runAs(testUser)
        {
            //Ensure we switch off API Calls when doing lead conversions
            AppUtils.IS_API_CALL_OVERRIDE = true;
            UTIL_GlobalVariableManifest.LeadLimit = SMALL_LIMIT; 
            
            DAL_Lead.addConvertedLeadsToUser(testUser);
            
            List<Lead> userLeads = TestUtils.createLeadsToOwner(SMALL_LIMIT, testUser.Id);

            Test.startTest();

            try 
            {
                insert userLeads;         
            } 
            catch(Exception e) 
            {
                System.assert(false, 'Trigger should not throw exception exception when user only breaches the limit if converted Leads are counted: ' + e.getMessage());
            } 

            Test.stopTest(); 
        }            
    }  

    static testMethod void exceptionNotThrown()
    {               
        testQueue = DAL_User.getGroupByName(DAL_Lead.LEAD_POOL_NAME);        
        testUser = DAL_User.getRandomConsultantUser();   

        System.runAs(testUser)
        {
            UTIL_GlobalVariableManifest.LeadLimit = SMALL_LIMIT;          
            List<Lead> queueLeads = TestUtils.createLeadsToOwner(SMALL_LIMIT, testQueue.Id);
            insert queueLeads;

            DAL_Lead.changeOwnership(queueLeads, testUser.Id);

            Test.startTest(); 

            try 
            {
                update queueLeads;        
            } 
            catch(Exception e) 
            {
                System.assert(false, 'Trigger should not throw exception on update when user is under the limit: ' + e.getMessage());
            }     

            //Return Leads to queue to test the insert
            DAL_Lead.changeOwnership(queueLeads, testQueue.Id);
            update queueLeads;          

            List<Lead> userLeads = TestUtils.createLeadsToOwner(SMALL_LIMIT, testUser.Id);

            try 
            {
                insert userLeads;         
            } 
            catch(Exception e) 
            {
                System.assert(false, 'Trigger should not throw exception on insert when user is under the limit: ' + e.getMessage());
            } 

            Test.stopTest();   
        }                  
    }

    //Assumption is being made that no user will have a limit of 1 lead or the factor will be set low enough to trigger the mechanism
    static testMethod void exceptionNotThrownWarningNotSent()
    {               
        testQueue = DAL_User.getGroupByName(DAL_Lead.LEAD_POOL_NAME);        
        testUser = DAL_User.getRandomConsultantUser();   

        System.runAs(testUser)
        {
            UTIL_GlobalVariableManifest.LeadLimit = SMALL_LIMIT;          
            List<Lead> queueLeads = TestUtils.createLeadsToOwner(1, testQueue.Id);
            insert queueLeads;

            DAL_Lead.changeOwnership(queueLeads, testUser.Id);

            Test.startTest(); 

            try 
            {
                update queueLeads;        
            } 
            catch(Exception e) 
            {
                System.assert(false, 'Trigger should not throw exception on update when user is under the limit: ' + e.getMessage());
            }     

            Test.stopTest(); 

            System.assertEquals(0, TH_Lead.EMAIL_SENT, 'No emails sent');
            System.assertEquals(null, TH_Lead.EMAIL_TEXT, 'Email body has been set, this should not happen');   
        }                  
    }

    static testMethod void emailSent()
    {   
             
        testUser = DAL_User.getRandomConsultantUser();

        Test.startTest(); 

        System.runAs(testUser)
        {
            UTIL_GlobalVariableManifest.LeadLimit = SMALL_LIMIT;             
            List<Lead> userLeads = TestUtils.createLeadsToOwner(SMALL_LIMIT, testUser.Id);

            try 
            {
                insert userLeads;         
            } 
            catch(Exception e) 
            {
                System.assert(false, 'Trigger should not throw exception: ' + e.getMessage());
            }   
        }

        Test.stopTest();    

        //Static variables on the handler class are set during test if the email creation/send loop is entered
        System.assertNotEquals(0, TH_Lead.EMAIL_SENT, 'No emails sent');
        System.assertNotEquals(true, TH_Lead.EMAIL_TEXT.contains(DAL_Email.EMIL_TEMPLATE_PLACEHOLDER), 'Email body not constructed properly'); 
    }  

    static testMethod void limitDisabled()
    {   
        testQueue = DAL_User.getGroupByName(DAL_Lead.LEAD_POOL_NAME);        
        testUser = DAL_User.getRandomConsultantUser();  

        System.runAs(testUser)
        {
            UTIL_GlobalVariableManifest.LeadLimit = 0;
            List<Lead> userLeads = TestUtils.createLeadsToOwner(10, testUser.Id);
            List<Lead> queueLeads = TestUtils.createLeadsToOwner(10, testQueue.Id);

            Test.startTest();

            try 
            {
                insert userLeads;         
            } 
            catch(Exception e) 
            {
                System.assert(false, 'Trigger should not throw exception on insert when user\'s limit is 0: ' + e.getMessage());
            }
            
            insert queueLeads;

            DAL_Lead.changeOwnership(queueLeads, testUser.Id);

            try 
            {
                update queueLeads;        
            } 
            catch(Exception e) 
            {
                System.assert(false, 'Trigger should not throw exception on update when user\'s limit is 0: ' + e.getMessage());
            }

            Test.stopTest(); 
        }               
    }  */
    
}