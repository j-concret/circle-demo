@isTest
public class NCPupdateFreightAddress_Test {
	
    public static testMethod void CPUpdateCostEstimates_Test(){
        
        //Test Data
        
        Access_token__c accessTokenObj = new Access_token__c();
        accessTokenObj.Status__c = 'Active';
        accessTokenObj.Access_Token__c = '123';
        
        Insert accessTokenObj;
        
        Account acc = new Account(Name = 'Acme Test');
        
        Insert acc;
        
        Client_Address__c clientAddress = new Client_Address__c();
        clientAddress.Name = 'My Address';
        clientAddress.client__c = acc.Id;
        
        insert clientAddress;
        
        Freight__c freightObj = new Freight__c();
		
        insert freightObj;
        
        
        //Json Body
        String json = '{"FRID":"'+freightObj.Id+'","Accesstoken":"'+accessTokenObj.Access_Token__c+'","PickupaddressID":"'+clientAddress.Id+'"}';
        

        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
        req.requestURI = '/services/apexrest/NCPupdateFreightPickupAddress'; 
        req.httpMethod = 'Post';
        req.addHeader('Content-Type', 'application/json');
        req.requestBody = Blob.valueof(json);
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
        NCPupdateFreightAddress.NCPupdateFreightAddress();
        Test.stopTest(); 
    }
    
    public static testMethod void CPUpdateCostEstimates_Test1(){
        
        //Test Data
        
        Access_token__c accessTokenObj = new Access_token__c();
        accessTokenObj.Status__c = 'Expired';
        accessTokenObj.Access_Token__c = '123';
        
        Insert accessTokenObj;
        
        Account acc = new Account(Name = 'Acme Test');
        
        Insert acc;
        
        Client_Address__c clientAddress = new Client_Address__c();
        clientAddress.Name = 'My Address';
        clientAddress.client__c = acc.Id;
        
        insert clientAddress;
        
        Freight__c freightObj = new Freight__c();
		
        insert freightObj;
        
        
        //Json Body
        String json = '{"FRID":"'+freightObj.Id+'","Accesstoken":"'+accessTokenObj.Access_Token__c+'","PickupaddressID":"'+clientAddress.Id+'"}';
        

        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
        req.requestURI = '/services/apexrest/NCPupdateFreightPickupAddress'; 
        req.httpMethod = 'Post';
        req.addHeader('Content-Type', 'application/json');
        req.requestBody = Blob.valueof(json);
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
        NCPupdateFreightAddress.NCPupdateFreightAddress();
        Test.stopTest(); 
    }
    
    public static testMethod void CPUpdateCostEstimates_Test2(){
        
        //Test Data
        
        Access_token__c accessTokenObj = new Access_token__c();
        accessTokenObj.Status__c = 'Active';
        accessTokenObj.Access_Token__c = '123';
        
        Insert accessTokenObj;
        
        Account acc = new Account(Name = 'Acme Test');
        
        Insert acc;
        
        Freight__c freightObj = new Freight__c();
		
        insert freightObj;
        
        
        //Json Body
        String json = '{"FRID":"'+freightObj.Id+'","Accesstoken":"'+accessTokenObj.Access_Token__c+'","PickupaddressID":"Null"}';
        

        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
        req.requestURI = '/services/apexrest/NCPupdateFreightPickupAddress'; 
        req.httpMethod = 'Post';
        req.addHeader('Content-Type', 'application/json');
        req.requestBody = Blob.valueof(json);
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
        NCPupdateFreightAddress.NCPupdateFreightAddress();
        Test.stopTest(); 
    }
}