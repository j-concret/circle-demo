public class QueueableUpdateCE implements Queueable {

    Shipment_Order__c SO;
    public QueueableUpdateCE(shipment_Order__c SO){
        this.SO = SO;
    }

    public void execute(QueueableContext context) {
        
        Shipment_Order__c shipmentOrder = [Select Id, Shipping_Status__c FROM Shipment_Order__c WHERE Id = :SO.Id ];
        shipmentOrder.Shipping_Status__c = 'Cost Estimate';
        Update shipmentOrder;
        
        Map<Id,Freight__c> fids = new Map<Id,Freight__c>([SELECT Id from Freight__c WHERE shipment_order__c=: SO.Id]);
        if(!fids.isEmpty() && !Test.isRunningTest()) {
            System.enqueueJob(new CreateCourierratesV2(new List<Id>(fids.keySet())));
        }
    }
}