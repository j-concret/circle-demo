public class Lex_SIEditRecord2 {
    
    
      
     //1. Returns related list of Costing
    @AuraEnabled
   
         public static List<CPA_Costing__c> getCostings(ID SupplierInvoiceID1){

             
          Invoice_New__c SI = [select ID, name, Shipment_Order__c ,Freight_Request__c, Conversion_Rate__c, Invoice_Name__c  from Invoice_New__c where id =: SupplierInvoiceID1 limit 1];
             
             String SOId1 = SI.Shipment_Order__c;
             String Freight1 = SI.Freight_Request__c;
             
             String RelatedSOid1 = SI.Shipment_Order__c;
             String RelatedFreight1 = SI.Freight_Request__c;
             
                        
            
               return [SELECT Additional_Percent__c, Amended__c, Amount__c, Amount_USD__c, Applied_to__c, Ceiling__c, Condition__c, Conditional_value__c, Cost_Category__c, Cost_Note__c, 
                       Cost_Type__c, CostStatus__c, Name, CPA_v2_0__c, CreatedById, Currency__c, Exchange_rate_forecast__c, Exchange_rate_payment_date__c, Floor__c, Amount_in_USD__c, 
                       Freight_Request__c, Invoice_amount_local_currency__c, Invoice_amount_USD_New__c, Invoice_currency__c, IOR_EOR__c, LastModifiedById, Max__c, Min__c, Rate__c, 
                       RecordTypeId, Shipment_Order_Old__c, Status_Colour__c, Supplier_Invoice__c, Updating__c, Variable_threshold__c, VAT_applicable__c, VAT_Rate__c, VAT_Rate_Formula__c, Invoice__c 
                       FROM CPA_Costing__c where Invoice__c =: SupplierInvoiceID1 limit 200];
             
         }
    
    //1.1 updating related list of Costings
    @AuraEnabled
    public static void updateCostings(List<CPA_Costing__c> editedAccountList){
        try{
            
           list<CPA_Costing__c> CCO = new list<CPA_Costing__c> ();
            For(integer i=0; editedAccountList.size()>i; i++){
                if(i==(editedAccountList.size()-1)){   editedAccountList[i].Trigger__c = true; } Else {  editedAccountList[i].Trigger__c = false;  }
                    system.debug('i-->'+i);system.debug('Size -1-->'+(editedAccountList.size()-1));
                     CCO.add(editedAccountList[i]);
            }
            system.debug('editedAccountList - Before'+editedAccountList);
          //  update editedAccountList;
            update CCO;
            system.debug('editedAccountList - After'+editedAccountList);
       CPA_Costing__c CPACos = [select id,trigger__C from CPA_Costing__c where Trigger__C = TRUE  and ID in: CCO limit 1 ]; 
       system.debug('CPACos-->'+CPACos);
       CPACos.trigger__C=False; 
       Update CPACos;
            
        } catch(Exception e){
           
        }
    }
    
    
    
   //2. Insert Costings from supplierInvoice - Costings Flow
     
    @AuraEnabled
    public static void saveCostings(List<CPA_Costing__c> accList, ID SupplierInvoiceID1 ){
        
        list<CPA_Costing__c> CCO = new list<CPA_Costing__c> ();
        
        CPA_Costing__c CCO1 = [Select Additional_Percent__c, Amended__c, Amount__c, Amount_USD__c, Applied_to__c, Ceiling__c, Condition__c, Conditional_value__c, Cost_Category__c, Cost_Note__c, 
                               Cost_Type__c, CostStatus__c, Name, CPA_v2_0__c, CreatedById, Currency__c, Exchange_rate_forecast__c, Exchange_rate_payment_date__c, Floor__c, Amount_in_USD__c, 
                               Freight_Request__c, Invoice_amount_local_currency__c, Invoice_amount_USD_New__c, Invoice_currency__c, IOR_EOR__c, LastModifiedById, Max__c, Min__c, Rate__c, 
                               RecordTypeId, Shipment_Order_Old__c, Status_Colour__c, Supplier_Invoice__c, Updating__c, Variable_threshold__c, VAT_applicable__c, VAT_Rate__c, VAT_Rate_Formula__c, Invoice__c
                               from CPA_Costing__c 
                               where Invoice__c =: SupplierInvoiceID1
                               limit 1];
        
        System.debug('CCO1'+CCO1);
        
        For(integer i=0; i<accList.size(); i++){
            
            accList[i].CPA_v2_0__c = CCO1.CPA_v2_0__c;
           // accList[i].IOR_EOR__c = CCO1.IOR_EOR__c;
            accList[i].Cost_Type__c = 'Fixed';
          //  accList[i].Currency__c = CCO1.Currency__c;
            accList[i].Invoice_currency__c= CCO1.Invoice_currency__c;
            accList[i].Shipment_Order_Old__c = CCO1.Shipment_Order_Old__c;
            accList[i].Freight_Request__c = CCO1.Freight_Request__c;
            accList[i].Invoice__c = SupplierInvoiceID1;
            accList[i].Exchange_rate_forecast__c = CCO1.Exchange_rate_forecast__c;
            accList[i].Exchange_rate_payment_date__c = CCO1.Exchange_rate_payment_date__c;
            accList[i].RecordTypeId = cco1.RecordTypeId;
            // below if condition to trigger rollups  on insert costings     
                    if(i==(accList.size()-1)){   accList[i].Trigger__c = true; } Else {  accList[i].Trigger__c = false;  }
            CCO.add(accList[i]);
            
        }
        
        
        Insert CCO;
       // Insert accList;
       CPA_Costing__c CPACos = [select id,trigger__C from CPA_Costing__c where Trigger__C = TRUE and Invoice__c =:SupplierInvoiceID1  and ID in: CCO limit 1 ]; 
       system.debug('CPACos-->'+CPACos);
       CPACos.trigger__C=False; 
        Update CPACos;
    
}
    
    
      //3. Return supplier invoice info to summary page
    @AuraEnabled
   
         public static List<Invoice_New__c> getSupplierInvoice(ID SupplierInvoiceID1){
         
           
             
          return [select Id, Actual_Admin_Costs__c, Actual_Bank_Costs__c, Actual_Customs_Brokerage_Costs__c, Actual_Customs_Clearance_Costs__c, Actual_Customs_Handling_Costs__c,
                  Actual_Customs_License_Costs__c, Actual_Duties_and_Taxes__c, Actual_Duties_and_Taxes_Other__c, Actual_Finance_Costs__c, Actual_Insurance_Costs__c, 
                  Actual_International_Delivery_Costs__c, Actual_IOREOR_Costs__c, Actual_Miscellaneous_Costs__c, FC_Customs_Brokerage_Costs__c, FC_Customs_Clearance_Costs__c, 
                  FC_Customs_Handling_Costs__c, FC_Customs_License_Costs__c, FC_Duties_and_Taxes__c, FC_Duties_and_Taxes_Other__c, FC_Finance_Costs__c, FC_Insurance_Costs__c, 
                  FC_International_Delivery_Costs__c, FC_IOR_Costs__c, FC_Miscellaneous_Costs__c, Conversion_Rate__c
                  from Invoice_New__c where id =: SupplierInvoiceID1 limit 1];
            
         }
    

}