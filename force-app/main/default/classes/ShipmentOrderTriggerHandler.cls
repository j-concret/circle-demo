public class ShipmentOrderTriggerHandler {
    
    public static Boolean IsZeeInvoiceProcessfromSO = false;
    
    public static Boolean isRecurssion = false;
    
    public static Map<Id, Shipment_Order__c> soMapFromInvoiceTrigger = new Map<Id, Shipment_Order__c>();
    
    public static Map<Id,Shipment_Order__c> soMapFromPartTrigger = new Map<Id,Shipment_Order__c>();
    
    public static void processAfterUpdate(Map<Id,Shipment_Order__c> soMap) {
        Map<Id,Shipment_Order__c> soToCalculateETA = new Map<Id,Shipment_Order__c>();
        Map<Id,Shipment_Order__c> soIdsToCreateTask = new Map<Id,Shipment_Order__c>();
        Map<Id,Task> tasks = new Map<Id,Task>();
        Set<String> mappedStatusAllowed = new Set<String> {'Quote Created','Approved to Ship','Compliance Pending','In-Transit','Arrived in Country','Cleared Customs','Final Delivery in Transit','Delivered'};
            
            for(Shipment_Order__c shipment : soMap.values()) {
                
                //MasterTask creation
                if(shipment.Shipping_Status__c !='Shipment Abandoned' && shipment.Shipping_Status__c !='Cost Estimate Abandoned' && shipment.Shipping_Status__c !='Roll-Out') {
                    soIdsToCreateTask.put(shipment.Id,shipment);
                }
                
                // FOR ETA calculation
                if(shipment.Mapped_Shipping_status__c != '' && mappedStatusAllowed.contains(shipment.Mapped_Shipping_status__c)) {
                    soToCalculateETA.put(shipment.Id,shipment);
                }
            }
        
        if(!soIdsToCreateTask.isEmpty() && !RecusrsionHandler.tasksCreated) {
            RecusrsionHandler.tasksCreated = true;
            MasterTaskProcess.taskCreation(new List<Id>(soIdsToCreateTask.keySet()));
            
            tasks = new Map<Id,Task>([SELECT Id,State__c,Shipment_Order__c,Shipment_Order__r.CPA_v2_0__c,Master_Task__c,Master_Task__r.Name,Master_Task__r.Reason_for_Binding_Quote_Exempted__c,IsNotApplicable__c,Binding_Quote_Exempted_V2__c FROM Task WHERE Shipment_Order__c IN: soMap.keySet() AND Master_Task__c != null]);
            
            bindingQoute(soIdsToCreateTask,tasks); // Binding Quote update field logic
            
            SoLogOnTaskHelper.getSummary(soIdsToCreateTask);
            List<AggregateResult> aggs = [SELECT count(Id) client_tasks,Shipment_Order__c FROM Task where Master_Task__c != null AND State__c='Client Pending' AND IsNotApplicable__c = false AND Shipment_Order__c IN: soMap.keySet() group by Shipment_Order__c];
            if(aggs.isEmpty()){
                for(Id sId : soMap.keySet()){
                	soMap.get(sId).Client_Task__c = 0;
                }
            }else{
                for(AggregateResult a : aggs) {
                    String sId = String.valueOf(a.get('Shipment_Order__c'));
                    soMap.get(sId).Client_Task__c = (Decimal) a.get('client_tasks');
                }
            }
        }
        
        if((!ETACalculation.isETACalculated && !soToCalculateETA.isEmpty())) {
            ETACalculation.processETA(soToCalculateETA);
        }
        
        RecusrsionHandler.skipTriggerExecution = true;
        update soMap.values();
        
        if(SOTravelCreation.SoTravelMap != null && !SOTravelCreation.SoTravelMap.isEmpty()) {
            List<SO_Travel_History__c> sothList = new List<SO_Travel_History__c>();
            List<SO_Travel_History__c> completeList = new List<SO_Travel_History__c>();
            for(String key: (SOTravelCreation.SoTravelMap).keySet()){
                sothList = (SOTravelCreation.SoTravelMap).get(key);
                completeList.addAll(sothList);
            }
            insert completeList;
            //insert SOTravelCreation.SoTravelMap.values();
            SOTravelCreation.SoTravelMap.clear();
        }
        //send platform event
        if(!soIdsToCreateTask.isEmpty()) {
            TaskChange__e tchange = new TaskChange__e(ShipmentOrderIds__c = String.join(new List<Id>(soIdsToCreateTask.keySet()),','),
                                                      TaskIds__c = String.join(new List<Id>(tasks.keySet()),','));
            
            Eventbus.publish(new List<TaskChange__e> {
                tchange
                    });
        }
        
        //Generate so document
        //if(UserInfo.getProfileId() != '00e0Y000000INg1' && ((System.isQueueable() || Trigger.isExecuting) && !System.isBatch())) SO_GenerateCostEstimate.generateCostEstimateDocAsync(soMap.keySet());
    }
    
    public static void bindingQoute(Map<Id,Shipment_Order__c> soMap,Map<Id,Task> tasks){
        Map<Id,List<String> > bindingQuoteFalseSO = new Map<Id,List<String> >();
        
        for(Task tsk : tasks.values()) {
            
            if( (tsk.Binding_Quote_Exempted_V2__c == 'Always Exempt' && !tsk.IsNotApplicable__c) || (tsk.Binding_Quote_Exempted_V2__c == 'Exempt until resolved' && tsk.State__c !='Resolved' && !tsk.IsNotApplicable__c)) {
                
                String reason;
                if(tsk.Master_Task__r.Name == 'No Binding Quote Available') {
                    reason = soMap.get(tsk.Shipment_Order__c).CPA_v2_0__r.Comments_On_Why_No_Final_Quote_Possible__c;
                }else{
                    reason = tsk.Master_Task__r.Reason_for_Binding_Quote_Exempted__c;
                }
                
                if(bindingQuoteFalseSO.containsKey(tsk.Shipment_Order__c))
                    bindingQuoteFalseSO.get(tsk.Shipment_Order__c).add(reason == null ? '' : reason);
                else
                    bindingQuoteFalseSO.put(tsk.Shipment_Order__c,new List<String> {reason == null ? '' : reason});
            }
        }
        
        for(Id soId : soMap.keySet()) {
            List<String> reasons = new List<String>();
            
            if(soMap.get(soId).of_Line_Items__c == 0) {
                reasons.add('Tax calculations are not accurate because line-item details have not been provided.');
            }
            if(soMap.get(soId).Invoice_Timing__c == 'Post-CCD Invoicing') {
                reasons.add('You have selected to be on a post-CCD invoice structure. TecEx will invoice you after the order has cleared customs based on the actual CCD amounts.');
            }
            if(bindingQuoteFalseSO.containsKey(soId)) {
                reasons.addAll(bindingQuoteFalseSO.get(soId));
            }
            
            if(reasons.isEmpty()) {
                soMap.get(soId).Binding_Quote__c = true;
                soMap.get(soId).Reason_for_Pro_forma_quote__c = null;
            }else{
                soMap.get(soId).Binding_Quote__c = false;
                soMap.get(soId).Reason_for_Pro_forma_quote__c = String.join(reasons,' '); // changed from ';'
            }
        }
    }
    
    @future(callout=true)
    public static void sendCEMail(Id ShipmentId){
        
        //network nt= [select Id, name from network where name = 'New-ClientPortal'];
        network nt= [select Id, name from network where name = 'TecEx App']; 
        List<EmailTemplate> lstEmailTemplates = [SELECT Id,Body, Name, Subject FROM EmailTemplate WHERE Name ='Tecex Cost Estimate'];
        Shipment_Order__c shipment = [SELECT Id,source__C,Client_Contact_for_this_Shipment__c,Request_Email_Estimate__c,Cost_Estimate_Number__c,Email_Estimate_To__c FROM Shipment_Order__c WHERE Id=:ShipmentId];
        if(!lstEmailTemplates.isEmpty() && shipment != null) {
            String title = 'Cost Estimate No: '+ shipment.Cost_Estimate_Number__c+'.pdf';
            Messaging.EmailFileAttachment csvAttc = new Messaging.EmailFileAttachment();
            PageReference costEstimatePdf = new PageReference(Site.getPathPrefix() + '/apex/acePDF');
            costEstimatePdf.getParameters().put('id', shipment.Id);
            Blob pdfPageBlob = (Test.isRunningTest()) ? Blob.valueOf(String.valueOf('This is a test page')) : costEstimatePdf.getContentAsPDF();
            
            //Blob csvBlob = EncodingUtil.base64Decode(pdfPageBlob);
            
            csvAttc.setFileName(title);
            csvAttc.setContentType('application/pdf');
            csvAttc.setBody(pdfPageBlob);
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setTemplateId(lstEmailTemplates[0].Id);
            mail.setSaveAsActivity(false);
            mail.setTargetObjectId(shipment.Client_Contact_for_this_Shipment__c);
            mail.setToAddresses(new List<String>{shipment.Email_Estimate_To__c});
            mail.setWhatId(shipment.Id);
            mail.setFileAttachments(new Messaging.EmailFileAttachment[]{csvAttc});
            Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});
            // attach new version
            
            ContentVersion contentVersion;
            try {
                // String title = 'Cost Estimate No:'+ soCENumber;
                List<ContentVersion> contVers = [SELECT Id,ContentDocumentId FROM ContentVersion WHERE FirstPublishLocationId=:ShipmentId AND Title=: title LIMIT 1];
                
                contentVersion = new ContentVersion(
                    versionData = pdfPageBlob,
                    title = title,
                    pathOnClient = title + '.pdf',
                    IsTaskDoc_fileupload__c='Yes',
                    origin = 'C');
                
                if(!contVers.isEmpty()) {
                    contentVersion.ContentDocumentId = contVers[0].ContentDocumentId;
                }else{
                    contentVersion.FirstPublishLocationId =  ShipmentId;
                }
                
                if(String.isNotBlank(nt.id) && String.valueOf(URL.getCurrentRequestUrl()).toLowerCase().contains('apexrest')) contentVersion.NetworkId =nt.id;
                
                insert contentVersion;
                
            } catch (Exception e) {
                throw new AuraHandledException(e.getMessage());
            }
            
        }
    }  
    
    @future(callout=true)
    public static void NCPSendCEMail(Id ShipmentId){
        
        //network nt= [select Id, name from network where name = 'New-ClientPortal'];
         network nt= [select Id, name from network where name = 'TecEx App']; 
        List<OrgWideEmailAddress> owea = [select Id from OrgWideEmailAddress where Address = 'app@tecex.com'];
        List<EmailTemplate> lstEmailTemplates = [SELECT Id,Body, Name, Subject FROM EmailTemplate WHERE Name ='Tecex Cost Estimate'];
        Shipment_Order__c shipment = [SELECT Id,source__C,Client_Contact_for_this_Shipment__c,Request_Email_Estimate__c,Cost_Estimate_Number__c,Email_Estimate_To__c FROM Shipment_Order__c WHERE Id=:ShipmentId];
        if(!lstEmailTemplates.isEmpty() && shipment != null) {
            String title = 'Cost Estimate No: '+ shipment.Cost_Estimate_Number__c+'.pdf';
            Messaging.EmailFileAttachment csvAttc = new Messaging.EmailFileAttachment();
            PageReference costEstimatePdf = new PageReference(Site.getPathPrefix() + '/apex/acePDF');
            costEstimatePdf.getParameters().put('id', shipment.Id);
            Blob pdfPageBlob = (Test.isRunningTest()) ? Blob.valueOf(String.valueOf('This is a test page')) : costEstimatePdf.getContentAsPDF();
            
            //Blob csvBlob = EncodingUtil.base64Decode(pdfPageBlob);
            
            csvAttc.setFileName(title);
            csvAttc.setContentType('application/pdf');
            csvAttc.setBody(pdfPageBlob);
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setTemplateId(lstEmailTemplates[0].Id);
            mail.setSaveAsActivity(false);
            if ( owea.size() > 0 ) {
                mail.setOrgWideEmailAddressId(owea[0].Id);
            }
            mail.setTargetObjectId(shipment.Client_Contact_for_this_Shipment__c);
            mail.setToAddresses(new List<String>{shipment.Email_Estimate_To__c});
            mail.setWhatId(shipment.Id);
            mail.setFileAttachments(new Messaging.EmailFileAttachment[]{csvAttc});
            Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});
            // attach new version
            
            ContentVersion contentVersion;
            try {
                // String title = 'Cost Estimate No:'+ soCENumber;
                List<ContentVersion> contVers = [SELECT Id,ContentDocumentId FROM ContentVersion WHERE FirstPublishLocationId=:ShipmentId AND Title=: title LIMIT 1];
                
                contentVersion = new ContentVersion(
                    versionData = pdfPageBlob,
                    title = title,
                    pathOnClient = title + '.pdf',
                    IsTaskDoc_fileupload__c='Yes',
                    origin = 'C');
                
                if(!contVers.isEmpty()) {
                    contentVersion.ContentDocumentId = contVers[0].ContentDocumentId;
                }else{
                    contentVersion.FirstPublishLocationId =  ShipmentId;
                }
               
                if(String.isNotBlank(nt.id)) contentVersion.NetworkId =nt.id;
                
                insert contentVersion;
                
            } catch (Exception e) {
              //  system.debug('Exception block-->' +e.getMessage()+e.getStackTraceString()+e.getLineNumber());
                throw new AuraHandledException(e.getMessage());
            }
            
        }
    }  
    
    
  /*  public static void generateChatterFeed(Shipment_Order__c shipmentOrder){
        
        List<String> clientProfiles = new List<String>();
        for(User user: [select ProfileId from User where Profile.Name IN ('New-ClientPortal Profile','ClientPortal Profile')]){
            clientProfiles.add(user.ProfileId);
        } 
        System.debug('UserInfo.getProfileId() '+UserInfo.getProfileId());
        System.debug('UserInfo.getName() '+UserInfo.getName());
        if(clientProfiles != null && !clientProfiles.contains(UserInfo.getProfileId())) {
            ConnectApi.FeedItemInput feedItemInput = new ConnectApi.FeedItemInput();
            ConnectApi.MentionSegmentInput mentionIORCSEInput = new ConnectApi.MentionSegmentInput();
            ConnectApi.MentionSegmentInput mentionICEInput = new ConnectApi.MentionSegmentInput();
            ConnectApi.MentionSegmentInput mentionFinancialManagerInput = new ConnectApi.MentionSegmentInput();
            ConnectApi.MentionSegmentInput mentionFinancialControllerInput = new ConnectApi.MentionSegmentInput();
            ConnectApi.MessageBodyInput messageBodyInput = new ConnectApi.MessageBodyInput();
            ConnectApi.TextSegmentInput textSegmentInput = new ConnectApi.TextSegmentInput();
            ConnectApi.TextSegmentInput textforDesignatedSpace = new ConnectApi.TextSegmentInput();
            ConnectApi.TextSegmentInput textforKindRegards = new ConnectApi.TextSegmentInput();
            ConnectApi.TextSegmentInput textforSpace = new ConnectApi.TextSegmentInput();
            
            
            messageBodyInput.messageSegments = new List<ConnectApi.MessageSegmentInput>();
            
            if(shipmentOrder.IOR_CSE__c != null){
                mentionIORCSEInput.id = shipmentOrder.IOR_CSE__c;//'0051w000005VmQGAA0';
                messageBodyInput.messageSegments.add(mentionIORCSEInput);
            }
            
            if(shipmentOrder.IOR_CSE__c != null && shipmentOrder.ICE__c != null){
                textSegmentInput.text = ' & ';
                messageBodyInput.messageSegments.add(textSegmentInput);
            }
            
            if(shipmentOrder.ICE__c != null){
                mentionICEInput.id = shipmentOrder.ICE__c;//'0051w000005VmQGAA0';
                messageBodyInput.messageSegments.add(mentionICEInput);
            }
            
            textforDesignatedSpace.text = '\r\n\r\n Please use this designated space for all your finance Inquiries. \r\n\r\n ';
            messageBodyInput.messageSegments.add(textforDesignatedSpace);
            
            textforKindRegards.text = ' Kind Regards ';
            messageBodyInput.messageSegments.add(textforKindRegards);
            
            if(shipmentOrder.Financial_Manager__c != null){
                mentionFinancialManagerInput.id = shipmentOrder.Financial_Manager__c;//'0051w000005VmQGAA0';
                messageBodyInput.messageSegments.add(mentionFinancialManagerInput);
            }
            
            textforSpace.text = ' ';
            messageBodyInput.messageSegments.add(textforSpace);
            
            if(shipmentOrder.Financial_Controller__c != null){
                mentionFinancialControllerInput.id = shipmentOrder.Financial_Controller__c;//'0051w000005VmQGAA0';
                messageBodyInput.messageSegments.add(mentionFinancialControllerInput);
            }
            
            feedItemInput.body = messageBodyInput;
            feedItemInput.feedElementType = ConnectApi.FeedElementType.FeedItem;
            feedItemInput.subjectId = shipmentOrder.Id;
            
            ConnectApi.FeedElement feedElement = ConnectApi.ChatterFeeds.postFeedElement(null, feedItemInput);
        }
    }*/
    
    public static void createNotifyParties(Map<Id,Set<Id>> ShipmentWithAccounts){
        System.debug('createNotifyParties '+ShipmentWithAccounts);
        List<Notify_Parties__c> notifyParties = new List<Notify_Parties__c>();
        for(Account Acc:[SELECT Id,
                         (SELECT Id FROM Contacts WHERE Include_in_SO_Communication__c = true) 
                         FROM Account 
                         WHERE Id IN :ShipmentWithAccounts.keySet()]){
                             System.debug('inside Query ');
                             if(Acc.Contacts != null && !(Acc.Contacts).isEmpty()){
                                 System.debug('contact has data '+Acc.contacts);
                                 for(Id shipId:ShipmentWithAccounts.get(Acc.Id)){    
                                     for(Contact con:Acc.Contacts){                       
                                         notifyParties.add(new Notify_Parties__c(
                                             Shipment_Order__c = shipId,
                                             Contact__c = con.Id
                                         ));
                                     }
                                 }         
                             }
                             
                         }
        System.debug('notifyParties '+notifyParties);
        if(notifyParties !=null && ! notifyParties.isEmpty()){
            insert notifyParties;
        }
        
    }
    
}