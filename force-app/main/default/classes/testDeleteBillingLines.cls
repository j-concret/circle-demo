@isTest(SeeAllData=true)
public class testDeleteBillingLines {

   private static testMethod void testDeletion()
   
   {
      
        
        Account account2 = new Account (
        name='Test Supplier', 
        Type ='Supplier', 
        ICE__c = '0050Y000001km5c');
        insert account2;    
          
        Account account = new Account (
        name='Acme1', 
        Type ='Client', 
        CSE_IOR__c = '0050Y000001LTZO',
        AcctSeed__GL_Account_Variable_1__c = 'a0h0Y000004Py7B',
        AcctSeed__GL_Account_Variable_2__c = 'a0h0Y000004Py7L',
        AcctSeed__GL_Account_Variable_3__c = 'a0h0Y000004Py7M' );
        insert account;    
          
        country_price_approval__c cpa = new country_price_approval__c( name ='Greenland', Billing_term__c = 'DAP/CIF - IOR pays',  Airwaybill_Instructions__c = 'User IOR Address',Supplier__c = account2.Id, Destination__c = 'Greenland' );
        insert cpa;
        
        IOR_Price_List__c iorpl = new IOR_Price_List__c ( Client_Name__c = account.Id, Name = 'Aruba', IOR_Fee__c = 0.15, On_Charge_Mark_up__c = 0.15,
        TecEx_Shipping_Fee_Markup__c =0.40, IOR_Min_Fee__c = 100,  Admin_Fee__c =100, Set_up_fee__c =100, Bank_Fees__c =200, Tax_Rate__c = 0.20,
        Estimated_Customs_Brokerage__c =250, Estimated_Customs_Clearance__c =250, Estimated_Customs_Handling__c =250, Estimated_Import_License__c =250, Destination__c = 'Greenland');
        insert iorpl; 
     
        Contact contact = new Contact ( lastname ='Testing Individual',  AccountId = account.Id);
        insert contact; 
        
        Opportunity opportunity = new Opportunity(AccountId = account.id, Name ='120-547', Ship_to_Country__c = cpa.Id, IOR_Price_List__c = iorpl.Id, Shipment_Value_USD__c =1000,
        Ship_From_Country__c = 'Algeria', CloseDate= system.today(), StageName ='POD Received', Client_Contact_for_this_Shipment__c = contact.Id, Who_arranges_International_courier__c ='Client', Tax_Treatment__c ='DAP/CIF - IOR pays' );
        insert opportunity;
     
        Shipment_Order__c shipmentOrder = new Shipment_Order__c(Account__c = account.id, Ship_to_Country__c = cpa.Id, IOR_Price_List__c = iorpl.Id, Shipment_Value_USD__c =1000, Opportunity__c = opportunity.Id,
        Client_Contact_for_this_Shipment__c = contact.Id, Who_arranges_International_courier__c ='Client', Tax_Treatment__c ='DAP/CIF - IOR pays', Shipping_Status__c ='Final Delivery in Progress' );
        insert shipmentOrder;
        
        Customer_Invoice__c customerInvoice = new Customer_Invoice__c(
        Client__c = account.id, 
        Shipment_Order__c = shipmentOrder.Id,
        Taxes_and_Duties__c = 2000,
        Invoice_Amount__c = 2000, 
        Invoice_Type__c = 'Top-up Invoice',
        Invoice_Status__c = 'Invoice Sent');
        insert customerInvoice; 
        
        
        AcctSeed__Billing__c billing = new AcctSeed__Billing__c( 
        Customer_Invoice__c = customerInvoice.Id,
        AcctSeed__Status__c = 'Approved',
        AcctSeed__Customer__c = account.Id,
        AcctSeed__Accounting_Period__c = 'a0f0Y000001q08y',
        AcctSeed__Billing_Format__c = 'a0n0Y0000010eIu',
        AcctSeed__Date__c     = system.today());
        insert billing;
               
        test.startTest();

        billing.Customer_Invoice_Changed__c = TRUE;
     
        
        update billing;

       
        test.stopTest();
      
      

      
   
     
       
    } 

   }