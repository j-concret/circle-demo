public class ShipmentLookup {
    @AuraEnabled
    public static Map<String, Object> getRecord(String objectName, Id recordId){
        Map<String, Object> result = new Map<String, Object> { 'status' => 'OK' };
        try{
            String query = 'SELECT Id, Name'+
                           ' FROM '+objectName+
                           ' WHERE Id = \''+recordId+'\''+
                           ' LIMIT 1';

            List<sObject> sobjList = Database.query(query);
            List<ResultWrapper> lstRet = new List<ResultWrapper>();

            for(SObject s : sobjList) {
                ResultWrapper obj = new ResultWrapper();
                obj.objName = objectName;
                obj.text = String.valueOf(s.get('Name'));
                obj.val = String.valueOf(s.get('Id'));
                lstRet.add(obj);
            }
            result.put('record', lstRet);
        }catch(Exception e) {
            result = new Map<String, Object> { 'status' => 'ERROR','msg'=>e.getMessage()};
        }

        return result;
    }
    @AuraEnabled(cacheable=true)
    public static String searchDB(String objectName, String fld_API_Text, String fld_API_Val,Integer lim,String fld_API_Search,String searchText ){

        searchText='\'%' + String.escapeSingleQuotes(searchText.trim()) + '%\'';


        String query = 'SELECT '+fld_API_Text+' ,'+fld_API_Val+
                       ' FROM '+objectName+
                       ' WHERE '+fld_API_Search+' LIKE '+searchText+
                       ' LIMIT '+lim;

        List<sObject> sobjList = Database.query(query);
        List<ResultWrapper> lstRet = new List<ResultWrapper>();

        for(SObject s : sobjList) {
            ResultWrapper obj = new ResultWrapper();
            obj.objName = objectName;
            obj.text = String.valueOf(s.get(fld_API_Text));
            obj.val = String.valueOf(s.get(fld_API_Val));
            lstRet.add(obj);
        }
        return JSON.serialize(lstRet);
    }

    public class ResultWrapper {
        @AuraEnabled public String objName {get; set;}
        @AuraEnabled public String text {get; set;}
        @AuraEnabled public String val {get; set;}
    }
}