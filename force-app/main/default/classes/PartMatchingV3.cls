public class PartMatchingV3 {
    
    public static Id zeeRecordType = Schema.SObjectType.Part__c.getRecordTypeInfosByDeveloperName().get('Zee').getRecordTypeId();
    
    public static Map<Id, Product2> searchNameProducts = new map<Id, Product2>();
    Public static Set<String> MapTest(List<Part__c> PRL,List<Product_matching_Rule__c> PMR){
        Map<String,String> categoryUS_HTS_Code = new Map<String,String> {
            'Audio devices'=>'851810',
            'Cameras'=>'852580',
            'Cellphones'=>'851712',
            'Gaming and virtual reality'=>'847160',
            'Power equipment'=>'850440',
            'Printers, scanners, copiers'=>'844332',
            'Rack (cabinet only), brackets and tools'=>'830249',
            'Servers, controllers and parts'=>'847150',
            'Storage devices'=>'852351',
            'Visual devices'=>'852859',
            'Routers, switches and transceivers'=>'851762',
            'Laptops & PCs'=>'847130',
            'Firewalls'=>'851762'
        };
                    
        Set<String> hscodes = new Set<String>();
        
        for(Part__c li: PRL) {
            List<Part__c> Parttobeupdate = new List<Part__c>();
            String Matchedrules ='-';
            String s = li.Name+' '+ li.Description_and_Functionality__c;
            String S1 = s.toUpperCase();
            Map<String, Object> m1 = new Map<String, Object>();
            
            for(Integer i=0; PMR.size()>i; i++) {
                Boolean Allcontainsmatched = FALSE;
                Boolean Doesnotcontainsmatched = FALSE;
                
                List<String> ContainsList = new List<String>();
                List<String> DoesnotContainsList = new List<String>();
                
                List<String> ConSplittedbyComma = new List<String>();
                List<String> DoesNotConSplittedbyComma = new List<String>();
                // All words must contain
                String Con = PMR[i].Contains__c;
                String Con1 = Con.toUpperCase();
                ConSplittedbyComma = Con1.split(',');
                
                if(PMR[i].Does_Not_Contains__c !=null  || !String.isBlank(PMR[i].Does_Not_Contains__c) ) {
                    String notCon = PMR[i].Does_Not_Contains__c;
                    String notCon1 = notCon.toUpperCase();
                    DoesNotConSplittedbyComma = notCon1.split(',');
                }
                
                for (String Constr : ConSplittedbyComma) {
                    if(S1.contains(Constr)) {
                        ContainsList.add(Constr);// Add to Contains List
                    }
                    
                    for (String NotConstr : DoesNotConSplittedbyComma) {
                        if(S1.contains(NotConstr)) {
                            DoesnotContainsList.add(NotConstr);// Add to Contains List
                        }
                    }
                }
                
                if((ConSplittedbyComma.size() == ContainsList.size() ) && ContainsList.size() > 0) {
                    Allcontainsmatched =TRUE;
                }
                if( DoesnotContainsList.size() > 0) {
                    Doesnotcontainsmatched = false;
                } else if(PMR[i].Does_Not_Contains__c == null) {
                    Doesnotcontainsmatched = true;
                }
                else{
                    Doesnotcontainsmatched = true;
                }
                
                if(Allcontainsmatched && Doesnotcontainsmatched)   {
                    Matchedrules = Matchedrules+PMR[i].Name;
                    Map<String, Object> m = (Map<String, Object>)JSON.deserializeUntyped(PMR[i].Fields_to_be_update__c);
                    m1.putAll(m);
                }
            }// PMR for loop ended
            // here actual update must be done on parts as per below condition
            for(string Key:  m1.keySet()) {
                if(Key == 'US_HTS_Code__c' ) { li.US_HTS_Code__c = string.valueof(m1.get(key));}
                if(Key == 'ECCN_NO__c' ) { li.ECCN_NO__c = string.valueof(m1.get(key));}
                if(Key == 'Li_ion_Batteries__c' ) { li.Li_ion_Batteries__c = string.valueof(m1.get(key));}
                if(Key == 'Encryption__c' ) { li.Encryption__c = string.valueof(m1.get(key));}
                if(Key == 'Wireless_capability__c' ) { li.Wireless_capability__c = string.valueof(m1.get(key));}
                if(Key == 'Sub_Category__c' ) { li.Sub_Category__c = string.valueof(m1.get(key));}
                if(Key == 'Manufacturer_Name__c' ) { li.Manufacturer_Name__c = string.valueof(m1.get(key));}
                if(Key == 'Category_New__c' ) { li.Category_New__c = string.valueof(m1.get(key));}
                if(Key == 'Country_of_Origin2__c' ) { li.Country_of_Origin2__c = string.valueof(m1.get(key));}
                if(Key == 'V3_COO__c' ) { li.V3_COO__c = string.valueof(m1.get(key));}
                if(Key == 'V3_ECCN__c' ) { li.V3_ECCN__c = string.valueof(m1.get(key));}
                if(Key == 'V3_HS_code__c' ) { li.V3_HS_code__c = string.valueof(m1.get(key));}
            }
            li.V3_Product_Match_Completed__c = true;
            li.matched_rule__c = Matchedrules;
            
            if(String.isBlank(li.US_HTS_Code__c) && String.isNotBlank(li.Client_Provided_Category__c) && categoryUS_HTS_Code.containsKey(li.Client_Provided_Category__c)) {
                li.US_HTS_Code__c = categoryUS_HTS_Code.get(li.Client_Provided_Category__c);
            }
            
            if(li.US_HTS_Code__c != null) hscodes.add(li.US_HTS_Code__c.left(4)+'%');
        }
        
        return hscodes;
    }
}