@RestResource(urlMapping='/UpdateCourierrates/*')
Global class NCPUpdateCourierservice {
      @Httppost
        global static void NCPGetCourierServices(){
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        Blob body = req.requestBody;
    	res.addHeader('Content-Type', 'application/json');
        String requestString = body.toString();
        NCPUpdateCourierServiceswra rw  = (NCPUpdateCourierServiceswra)JSON.deserialize(requestString,NCPUpdateCourierServiceswra.class);
          try{
              //check accesstoken validity here
              
           Courier_Rates__c CR = [SELECT Id, Name, CreatedDate, Freight_Request__c, Final_Rate__c, service_type__c, Status__c, Service_Type_Name__c, Origin__c, Carrier_Name__c FROM Courier_Rates__c  where id =: rw.CRID LIMIT 1];
           if(CR.id!=null){
               
               Cr.Status__C = rw.status;
               Update Cr;
               
               
            JSONGenerator gen = JSON.createGenerator(true);
            gen.writeStartObject();
            
             gen.writeFieldName('Success');
             gen.writeStartObject();
            
             	if(rw.CRID == null) {gen.writeNullField('Response');} else{gen.writeStringField('Response', 'Success- Courier rate updated');}
            
            gen.writeEndObject();
            gen.writeEndObject();
            String jsonData = gen.getAsString();
            res.responseBody = Blob.valueOf(jsonData);
            res.addHeader('Content-Type', 'application/json');
            res.statusCode = 200;        
            
               
                API_Log__c Al = New API_Log__c();
                Al.EndpointURLName__c='UpdateCourierrates';
                Al.StatusCode__c=string.valueof(res.statusCode);
                AL.Response__c = 'Success - Courierrates are updated';
                Insert Al;  
              }
              
          }
          catch(Exception e){
              
                    String ErrorString ='Authentication failed, Please check username and password';
               		res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
        			res.statusCode = 400;
                
                API_Log__c Al = New API_Log__c();
                //Al.Account__c=logincontact.AccountID;
                //Al.Login_Contact__c=logincontact.Id;
                Al.EndpointURLName__c='UpdateCourierrates';
                Al.Response__c=ErrorString;
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al;
                
              
          }
          
    
          
    }

}