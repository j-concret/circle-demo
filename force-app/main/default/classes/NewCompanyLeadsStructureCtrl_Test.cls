@isTest
public class NewCompanyLeadsStructureCtrl_Test {
    @testsetup
    public static void setupData(){
        User u1 = new User(
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'TecEx Branch Manager'].Id,
            LastName = 'Berkeley1',
            Email = 'puser1000@amamama.com',
            Username = 'puser0001@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST1',
            FirstName='Vat1',
            Title = 'title1',
            Alias = 'alias1',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US'
        );
        User u = new User(
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id,
            LastName = 'Berkeley',
            Email = 'puser000@amamama.com',
            Username = 'puser000@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            FirstName='Vat',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US'
        );
        insert u;
        insert u1;
        System.runAs(u) {
            Company__c compnayObj = new Company__c(Name = 'test', Country__c  = 'Australia',Region__c = 'India');
            Company__c compnayObj1 = new Company__c(Name = 'test1', Country__c  = 'Japan',Region__c = 'India');
            insert new List<Company__c>{compnayObj, compnayObj1};
                Company_User_Assignment__c companyUserAssignm = new Company_User_Assignment__c( Company__c = compnayObj.Id,User__c= u.id);
            Company_User_Assignment__c companyUserAssignm1 = new Company_User_Assignment__c( Company__c = compnayObj1.Id,User__c= u.id);
            insert new List<Company_User_Assignment__c>{companyUserAssignm,companyUserAssignm1 };
                }
    }
    @istest
    public static void getaData1(){
        Id userId = [SELECT Id FROM User WHERE LastName = 'Berkeley1' LIMIT 1].Id;
        test.startTest();
        NewCompanyLeadsStructureCtrl.masterwrapper obj = new NewCompanyLeadsStructureCtrl.masterwrapper();
        obj = NewCompanyLeadsStructureCtrl.getData('test', null);
        NewCompanyLeadsStructureCtrl.masterwrapper obj1 = new NewCompanyLeadsStructureCtrl.masterwrapper();
        obj1 = NewCompanyLeadsStructureCtrl.getData('test', 'Vat1');
        
        system.debug('obj : '+obj.wrapperClass);
		List<NewCompanyLeadsStructureCtrl.wrapper> wrapperLst = new List<NewCompanyLeadsStructureCtrl.wrapper>();

        Map<String, Object> result = NewCompanyLeadsStructureCtrl.updateRecords(JSON.serialize(obj.wrapperClass));
        Map<String, Object> result1 = NewCompanyLeadsStructureCtrl.insertCompanyUserAssignM(JSON.serialize(obj.wrapperClass), new List<String>{'userId'});
        Map<String, Object> result2 = NewCompanyLeadsStructureCtrl.getAllUsers();
        
        NewCompanyLeadsStructureCtrl.UserWrapper userWrap = new NewCompanyLeadsStructureCtrl.UserWrapper();
        userWrap.userId = userId;
        userWrap.companyAllocated = 5;
        userWrap.allocationLimit = 10;
        userWrap.companyCreated = 5;
        userWrap.creationLimit = 10;
        
        NewCompanyLeadsStructureCtrl.updateUsers(JSON.serialize(new List<NewCompanyLeadsStructureCtrl.UserWrapper>{userWrap}));
        test.stopTest();
    }
}