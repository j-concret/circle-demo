@isTest
private class TESU_UTIL_LeadWarn 
{
	private static final Integer STR_LEN = 5;
	
	private static final String VARIABLE_NAME = al.RandomStringUtils.randomAlphanumeric(STR_LEN);
	private static final String WHITESPACE_VARIABLE_NAME = '     ';
	private static final String VARIABLE_1 = al.RandomStringUtils.randomAlphanumeric(STR_LEN);
	private static final String VARIABLE_2 = al.RandomStringUtils.randomAlphanumeric(STR_LEN);
	
    private static testmethod void getBadInput()
    {
        //no asserts, just making sure this doesn't throw an exception
        // if an exception were thrown, then this test would fail
        System.assertEquals(null,UTIL_LeadWarn.get(null));
        System.assertEquals(null,UTIL_LeadWarn.get(WHITESPACE_VARIABLE_NAME));
    }

    private static testmethod void putBadInput()
    {
        //no asserts, just making sure this doesn't throw an exception
        // if an exception were thrown, then this test would fail
        System.assertEquals(null, UTIL_LeadWarn.put(null, null));
        System.assertEquals(null, UTIL_LeadWarn.put(WHITESPACE_VARIABLE_NAME, null));
    }
    
    private static testmethod void getWithDefault()
    {
    	//Varible shouldnt exist
    	System.assertEquals(UTIL_LeadWarn.get(VARIABLE_NAME), null);
    	
    	//Retrieve variable with default
    	UTIL_LeadWarn.get(VARIABLE_NAME, VARIABLE_1);
    	
    	//Now variable exists;
    	assertGlobalVariableExistsWith(VARIABLE_NAME, VARIABLE_1);	
    }

    private static testmethod void putInsert()
    {
        String previousValue = UTIL_LeadWarn.put(VARIABLE_NAME, null);
        assertGlobalVariableExistsWith(VARIABLE_NAME, null);
        System.assertEquals(null,previousValue);
        System.assertEquals(null,UTIL_LeadWarn.get(VARIABLE_NAME));

        previousValue = UTIL_LeadWarn.put(VARIABLE_NAME, VARIABLE_1);
        assertGlobalVariableExistsWith(VARIABLE_NAME, VARIABLE_1);
        System.assertEquals(null, previousValue);
        System.assertEquals(VARIABLE_1, UTIL_LeadWarn.get(VARIABLE_NAME));

        previousValue = UTIL_LeadWarn.put(VARIABLE_NAME, VARIABLE_1);
        assertGlobalVariableExistsWith(VARIABLE_NAME,VARIABLE_1);
        System.assertEquals(VARIABLE_1, previousValue);
        System.assertEquals(VARIABLE_1, UTIL_LeadWarn.get(VARIABLE_NAME));
    }
    
    private static testmethod void putUpdate()
    {
        Lead_Limit_Warn_Factor__c record = new Lead_Limit_Warn_Factor__c(Name = VARIABLE_NAME, Factor__c = VARIABLE_2);  
        insert record;
        
        assertGlobalVariableExistsWith(VARIABLE_NAME,VARIABLE_2);

        String previousValue = UTIL_LeadWarn.put(VARIABLE_NAME, VARIABLE_1);
        
        assertGlobalVariableExistsWith(VARIABLE_NAME, VARIABLE_1);
        System.assertEquals(VARIABLE_2,previousValue);
        System.assertEquals(VARIABLE_1,UTIL_LeadWarn.get(VARIABLE_NAME));
    }

    private static void assertGlobalVariableExistsWith(String name, String value)
    {
        Lead_Limit_Warn_Factor__c record = null;
        try
        {
            record = 
            [    
               SELECT Factor__c 
               FROM Lead_Limit_Warn_Factor__c 
               WHERE Name = :name 
               LIMIT 1 
            ];
        }
        catch(QueryException e)
        {
        	
        }
        
        System.assertNotEquals(null,record);
        System.assertEquals(value, record.Factor__c);
    }
	
}