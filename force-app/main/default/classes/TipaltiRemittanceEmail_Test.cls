@isTest
private class TipaltiRemittanceEmail_Test
{
    public static String CRON_EXP = '0 0 0 15 3 ? 2022';
    
    static testmethod void test1() 
    {
        Test.startTest();
        String jobId = System.schedule('ScheduleApexClassTest',
                                       CRON_EXP, 
                                       new TipaltiRemittanceEmail_Schdle());
        CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime
                          FROM CronTrigger WHERE id = :jobId];
        System.assertEquals(CRON_EXP, 
                            ct.CronExpression);
        System.assertEquals(0, ct.TimesTriggered);
        System.assertEquals('2022-03-15 00:00:00', 
                            String.valueOf(ct.NextFireTime));
        Test.stopTest();
    }
    
    static testMethod void  sendRemittanceEmail_Test(){
        Account account = new Account (name='Acme1', Type ='Client', 
                                       CSE_IOR__c = '0050Y000001LTZO', 
                                       Service_Manager__c = '0050Y000001LTZO', 
                                       Financial_Manager__c='0050Y000001LTZO', 
                                       Financial_Controller__c='0050Y000001LTZO', 
                                       Tax_recovery_client__c  = FALSE,
                                       Ship_From_Line_1__c = 'Ship_From_Line_1',
                                       Ship_From_Line_2__c = 'Ship_From_Line_2',
                                       Ship_From_Line_3__c = 'Ship_From_Line_3',
                                       Ship_From_City__c = 'Ship_From_City', 
                                       Ship_From_Zip__c = 'Ship_From_Zip',
                                       Ship_From_Country__c = 'Ship_From_Country' , 
                                       Ship_From_Other_notes__c = 'Ship_From_Other',
                                       Ship_From_Contact_Name__c = 'Ship_From_Contact',
                                       Ship_From_Contact_Email__c = 'Ship_From_Email',
                                       Ship_From_Contact_Tel__c = 'Ship_From_Tel',
                                       Client_Address_Line_1__c = 'Client_Address_Line_1',
                                       Client_Address_Line_2__c = 'Client_Address_Line_2',
                                       Client_Address_Line_3__c = 'Client_Address_Line_3',
                                       Client_City__c = 'Client_City',
                                       Client_Zip__c = 'Client_Zip',
                                       Client_Country__c = 'Client_Country', 
                                       Other_notes__c = 'Other_notes',
                                       Tax_Name__c = 'Tax_Name',
                                       Tax_ID__c = 'Tax_ID',
                                       Contact_Name__c = 'Contact_Name',
                                       Contact_Email__c = 'Contact_Email',
                                       Contact_Tel__c = 'Contact_Tel'
                                      );
        insert account;  
        
        
        Account account2 = new Account (name='TecEx Prospective Client', Type ='Supplier', ICE__c = '0050Y000001km5c', RecordTypeId = '0120Y0000006KjQQAU');
        insert account2;        
        
        Contact contact = new Contact ( lastname ='Testing Individual',  AccountId = account.Id,CCD_Contact__c = TRUE,email='test@gmail.com');
        insert contact;  
        
        Contact contact2 = new Contact ( lastname ='Testing supplier',  AccountId = account2.Id,CCD_Contact__c = TRUE,email='test@gmail.com');
        insert contact2; 
        
        IOR_Price_List__c iorpl = new IOR_Price_List__c ( Client_Name__c = account.Id, Name = 'Brazil', IOR_Fee__c = 0.15, On_Charge_Mark_up__c = 0.15,
                                                         TecEx_Shipping_Fee_Markup__c =0.40, IOR_Min_Fee__c = 100,  Admin_Fee__c =100, Set_up_fee__c =100, Bank_Fees__c =200, Tax_Rate__c = 0.20,
                                                         Estimated_Customs_Brokerage__c =250, Estimated_Customs_Clearance__c =250, Estimated_Customs_Handling__c =250, Estimated_Import_License__c =250, Destination__c = 'Brazil' );
        insert iorpl; 
        
        country_price_approval__c cpa = new country_price_approval__c( name ='Brazil', Billing_term__c = 'DAP/CIF - IOR pays',  Airwaybill_Instructions__c = 'User IOR Address', Supplier__c = account2.Id, 
                                                                      In_Country_Specialist__c = '0050Y000001km5c', Clearance_Destination__c = 'Brazil', Preferred_Supplier__c = TRUE, Destination__c = 'Brazil' );
        insert cpa;
        
        Currency_Management2__c conversion = new Currency_Management2__c (Name = 'US Dollar (USD)', Currency__c = 'US Dollar (USD)', ISO_Code__c = 'USD', Conversion_Rate__c = 1);
        insert conversion; 
        
        Country__c country = new Country__c(Brokerage_Costs__c = 618, CIF_Adjustment_Factor__c = 0.035, CIF_Absolute_value_adjustment__c = 200, Clearance_Costs__c = 488, 
                                            Country__c = 'Brazil', Name = 'Brazil', Handling_Costs__c = 1180, License_Permit_Costs__c = 0);
        insert country;
        
        Tax_Structure_Per_Country__c tax = new Tax_Structure_Per_Country__c(Applied_to_Value__c = 'CIF', Country__c = country.Id, Default_Duty_Rate__c = 0.5625 , 
                                                                            Order_Number__c = '1', Part_Specific__c = TRUE, Name = 'II', Tax_Type__c = 'Duties');
        insert tax;
        
        
        
        CPA_v2_0__c aCpav = new CPA_v2_0__c(Name = 'Brazil', Country_Applied_Costings__c = TRUE); insert aCpav;
        
        CPA_v2_0__c cpav2 = new CPA_v2_0__c(Name = 'Brazil Testing 2.0', Supplier__c = account2.Id, Service_Type__c = 'IOR/EOR', Related_Costing_CPA__c = aCpav.Id, 
                                            Preferred_Supplier__c = TRUE, VAT_Rate__c = 0.1, Destination__c = 'Brazil',  Final_Destination__c = 'Brazil', 
                                            VAT_Reclaim_Destination__c = FALSE, Country__c = country.id, Lead_ICE__c = '0050Y000001km5c'); insert cpav2;
        
        
        
        
        
        Shipment_Order__c shipmentOrder = new Shipment_Order__c(Account__c = account.id, Shipment_Value_USD__c = 2000, CPA_v2_0__c = cpav2.Id,
                                                                Client_Contact_for_this_Shipment__c = contact.Id, Who_arranges_International_courier__c ='Tecex', Tax_Treatment__c ='DAP/CIF - IOR pays', 
                                                                Ship_to_Country__c = cpa.Id, Destination__c = 'Brazil', Service_Type__c = 'IOR', IOR_Price_List__c = iorpl.Id, Chargeable_Weight__c = 200,  
                                                                Shipping_Status__c = 'Cost Estimate',RecordTypeId = '0120Y0000009cF0QAI' ,Included_in_OBS_billing__c = true, Miscellaneous_Fee__c = 400,  Miscellaneous_Fee_Name__c = 'Ad Hoc Charge', POD_Date__c =date.today() );  
        insert shipmentOrder; 
        
        List<CPA_Costing__c> costs = new List<CPA_Costing__c>();
        
        
        costs.add(new CPA_Costing__c(Shipment_Order_Old__c = shipmentOrder.Id , Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'Shipment value', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'IOR', Min__c = 375, Max__c = 14000, Conditional_value__c = 'Shipment value', Condition__c = 'Between', Floor__c = 0, Ceiling__c = 10000, Currency__c = 'US Dollar (USD)'));
        costs.add(new CPA_Costing__c(Shipment_Order_Old__c = shipmentOrder.Id , Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'Anil', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'IOR', Currency__c = 'US Dollar (USD)',Variable_threshold__c = null));
        
        
        insert costs;
        
        List<Invoice_New__c> invoiceList = new List<Invoice_New__c>();
        
        Invoice_New__c SI = new Invoice_New__c( 
            Invoice_Name__c = 'Test123', 
            Amount_Paid_via_Tipalti_Local_Currency__c = 1000,
            Account__c =account2.Id,
            Invoice_Date__c = date.today(), 
            Invoice_Currency__c = 'US Dollar (USD)',
            Conversion_Rate__c = conversion.Conversion_rate__C,
            Shipment_Order__c = shipmentOrder.Id, 
            Invoice_Type__c = 'Invoice', 
            Invoice_Status__c = 'Paid by Tipalti',
            Paid_Date_Tipalti__c = date.today(),
            POD_Date_New__c = shipmentOrder.POD_Date__c,
            RecordTypeId = Schema.Sobjecttype.Invoice_New__c.getRecordTypeInfosByDeveloperName().get('Supplier').getRecordTypeId());
        invoiceList.add(SI);
        
        Invoice_New__c SI2 = new Invoice_New__c( 
            Invoice_Name__c = 'Test125', 
            Amount_Paid_via_Tipalti_Local_Currency__c = 1000,
            Account__c =account2.Id,
            Invoice_Date__c = date.today(), 
            Invoice_Currency__c = 'US Dollar (USD)',
            Conversion_Rate__c = conversion.Conversion_rate__C,
            Shipment_Order__c = shipmentOrder.Id, 
            Invoice_Type__c = 'Invoice', 
            Invoice_Status__c = 'Paid by Tipalti',
            Paid_Date_Tipalti__c = date.today(),
            POD_Date_New__c = shipmentOrder.POD_Date__c,
            RecordTypeId = Schema.Sobjecttype.Invoice_New__c.getRecordTypeInfosByDeveloperName().get('Supplier').getRecordTypeId());
        invoiceList.add(SI2);
        
        Invoice_New__c SI3 = new Invoice_New__c( 
            Invoice_Name__c = 'Test157', 
            Amount_Paid_via_Tipalti_Local_Currency__c = 1000,
            Account__c =account2.Id,
            Invoice_Date__c = date.today(), 
            Invoice_Currency__c = 'US Dollar (USD)',
            Conversion_Rate__c = conversion.Conversion_rate__C,
            Shipment_Order__c = shipmentOrder.Id, 
            Invoice_Type__c = 'Invoice', 
            Invoice_Status__c = 'Paid by Tipalti',
            Paid_Date_Tipalti__c = date.today(),
            POD_Date_New__c = shipmentOrder.POD_Date__c,
            RecordTypeId = Schema.Sobjecttype.Invoice_New__c.getRecordTypeInfosByDeveloperName().get('Supplier').getRecordTypeId());
        invoiceList.add(SI3);
        
        INSERT invoiceList;
        
        Test.startTest();
        	TipaltiRemittanceEmail.sendRemittanceEmail();
        Test.stopTest();
    }
}