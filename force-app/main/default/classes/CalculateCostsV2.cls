public class CalculateCostsV2 {

    public static void createSOCosts(Map<Id,Shipment_Order__c> shipmentOrders,Set<Id> cpaIds){

        List<String> currencies = new List<String>();

        /* List<Shipment_Order__c> shipmentOrders = [SELECT Id, Name,  Insurance_Cost_CIF__c, Shipment_Value_USD__c, IOR_Fee_new__c, FC_Total_Customs_Brokerage_Cost__c,FC_Total_Handling_Costs__c, SupplierlU__c, FC_Total_License_Cost__c, FC_Total_Clearance_Costs__c, FC_Insurance_Fee_USD__c, FC_Miscellaneous_Fee__c, On_Charge_Mark_up__c, FC_International_Delivery_Fee__c, TecEx_Shipping_Fee_Markup__c, CPA_v2_0__c, CPA_v2_0__r.Related_Costing_CPA__c, CPA_v2_0__r.VAT_Rate__c,CPA_v2_0__r.Supplier__c,Chargeable_Weight__c, Account__c, Service_Type__c, Total_Taxes__c, of_Line_Items__c, FC_Total__c, of_Unique_Line_Items__c, CPA_Costings_Added__c, of_packages__c, Final_Deliveries_New__c, IOR_Refund_of_Tax_Duty__c FROM Shipment_Order__c WHERE Id IN :SOIds]; */

        Map<String,List<CPA_Costing__c> > cpaCostingMap = new Map<String,List<CPA_Costing__c> >();

        for(CPA_Costing__c cpaCosting : [SELECT Name,Shipment_Order_Old__c,Invoice__c,isManualCosting__c,Inactive__c, Amount__c, Applied_to__c,Cost_Category__c, Cost_Type__c, Max__c, Min__c, Rate__c, CostStatus__c, Updating__c, RecordTypeID, IOR_EOR__c, VAT_applicable__c, Threshold_Percentage__c, Within_Threshold__c, Variable_threshold__c, VAT_Rate__c, CPA_v2_0__r.VAT_Rate__c, Currency__c, Additional_Percent__c,Supplier__c, Cost_Note__c,Conditional_logic__c,Condition__c,Conditional_value__c,Value__c,Condition_2__c,Conditional_value_2__c,Value_2__c,Condition_3__c,Conditional_value_3__c,Value_3__c,Condition_4__c,Conditional_value_4__c,Value_4__c,Condition_5__c,Conditional_value_5__c,Value_5__c FROM CPA_Costing__c WHERE CPA_v2_0__c IN: cpaIds]) {

            currencies.add(cpaCosting.Currency__c);
            String key = cpaCosting.CPA_v2_0__c+'-'+cpaCosting.IOR_EOR__c;
            if(cpaCostingMap.containsKey(key)) {
                cpaCostingMap.get(key).add(cpaCosting);
            }else{
                cpaCostingMap.put(key,new List<CPA_Costing__c> {cpaCosting});
            }
        }

        Map<String,Currency_Management2__c> costingCurrency = new Map<String,Currency_Management2__c>();
        for(Currency_Management2__c curr : [SELECT Name, Conversion_Rate__c, ISO_Code__c, Currency__c FROM Currency_Management2__c WHERE Currency__c IN: currencies]) {
            costingCurrency.put(curr.Currency__c,curr);
        }

        List<CPA_Costing__c> deleteCostings = new List<CPA_Costing__c>();

        Map<Id,Map<String,CPA_Costing__c> > existingCostingMap = new Map<Id,Map<String,CPA_Costing__c> >();

        for(CPA_Costing__c relCost : [SELECT Id, Name, isManualCosting__c,Invoice__c, Supplier__c, DeleteAll__c, Shipment_Order_Old__c,Inactive__c, IOR_EOR__c,Amount__c, Applied_to__c, Conditional_value__c, Condition__c,Cost_Category__c, Cost_Type__c, Max__c, Min__c, Rate__c, CostStatus__c, Updating__c, RecordTypeID, VAT_Rate__c, VAT_applicable__c,Variable_threshold__c,Threshold_Percentage__c, Within_Threshold__c, Currency__c, Supplier_Invoice__c, Amended__c, Cost_Note__c,Value__c FROM CPA_Costing__c WHERE Shipment_Order_Old__c IN: shipmentOrders.keySet()]) {
            if(existingCostingMap.containsKey(relCost.Shipment_Order_Old__c)) {
                existingCostingMap.get(relCost.Shipment_Order_Old__c).put(relCost.Name,relCost);
            }else
                existingCostingMap.put(relCost.Shipment_Order_Old__c,new Map<String,CPA_Costing__c> {relCost.Name=>relCost});
        }

        List<CPA_Costing__c> addedCostings = new List<CPA_Costing__c>();
        Map<String,CPA_Costing__c> finalUpdateCostingMap = new MAP<String,CPA_Costing__c>();
        Id recordTypeId = Schema.SObjectType.CPA_Costing__c.getRecordTypeInfosByName().get('Transaction').getRecordTypeId();
        List<CPA_Costing__c> applicableCostings = new List<CPA_Costing__c>();

        for(Shipment_Order__c so : shipmentOrders.values()) {
            String key = so.CPA_v2_0__c+'-'+so.Service_Type__c;
            if(!cpaCostingMap.containsKey(key)) continue;
            List<CPA_Costing__c>  culledCostings = SOTriggerHelper.excludeAllCosts(so, cpaCostingMap.get(key));
            List<CPA_Costing__c>  SOCPAcostingsIOR = culledCostings.deepClone();

            for(CPA_Costing__c cost : SOCPAcostingsIOR) {
                if(existingCostingMap.containsKey(so.Id) && existingCostingMap.get(so.Id).containsKey(cost.Name)) {
                    if(existingCostingMap.get(so.Id).get(cost.Name).Amended__c) {
                        existingCostingMap.get(so.Id).remove(cost.Name);
                        continue;
                    }
                    cost.Inactive__c  = false;
                    cost.Id = existingCostingMap.get(so.Id).get(cost.Name).Id;
                    cost.Within_Threshold__c = existingCostingMap.get(so.Id).get(cost.Name).Within_Threshold__c;
                }
                cost.Shipment_Order_Old__c = SO.Id;
                cost.CPA_v2_0__c = so.CPA_v2_0__r.Related_Costing_CPA__c;
                cost.recordTypeID = recordTypeId;

                cost.VAT_Rate__c =  !cost.VAT_applicable__c ? 0 : so.CPA_v2_0__r.VAT_Rate__c == null ? 0 : so.CPA_v2_0__r.VAT_Rate__c;
                cost.Additional_Percent__c =  cost.Additional_Percent__c == null ? 0 : cost.Additional_Percent__c;
                cost.Variable_threshold__c =  cost.Variable_threshold__c == null ? 0 : cost.Variable_threshold__c;
                cost.Within_Threshold__c = null;
                cost.Updating__c = !cost.Updating__c;
                if(cost.Invoice__c == null) {
                    cost.Supplier__c = so.CPA_v2_0__r.Supplier__c;
                    cost.Amount__c = cost.Amount__c;
                }
                //UpdateTaxCosting class code
                if(cost.Name =='Taxes and duties recharge') {
                    cost.Amount__c = so.Total_Taxes__c;
                }
                SOTriggerHelper.calculateSOCostingAmount(so, cost, costingCurrency.get(cost.Currency__c));
                if(!cost.Inactive__c) {
                    addedCostings.add(cost);
                    if(cost.Id != null) existingCostingMap.get(so.Id).remove(cost.Name);
                }else if(cost.Id != null) {
                    existingCostingMap.get(so.Id).get(cost.Name).Inactive__c = true;
                }
            }

            if(existingCostingMap.containsKey(so.Id)) {
                for(CPA_Costing__c cost : existingCostingMap.get(so.Id).values()) {
                    if(cost.Inactive__c || (!cost.isManualCosting__c && cost.Invoice__c == null && !cost.Amended__c)) {
                        deleteCostings.add(cost);
                    }
                }
            }
        }
        if(!deleteCostings.isEmpty()) {
            delete deleteCostings;
        }
        upsert addedCostings;
    }
}