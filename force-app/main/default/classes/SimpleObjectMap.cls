public class SimpleObjectMap {
	private map<String, String> lFields;
	private map<String, String> lOldFields;
	public ID Id { get { return String.valueOf(Item.get('Id'));} }
	public ID OwnerId;
	public SObject Item;
	
	public SimpleObjectMap() {
		lFields = new map<String, String>();
		lOldFields = new map<String, String>();
	}
	public map<String, String> Fields {
		get {return lFields;}
		set { 
			lFields.clear();
			lFields.putAll(value);
		}
	}	
	public map<String, String> OldFields {
		get {return lOldFields;}
		set { 
			lOldFields.clear();
			lOldFields.putAll(value);
		}
	}	

	public static SimpleObjectMap Make(SObject item, ID OwnerId, map<String, String> oldFieldList, map<String, String> fieldList) {
		SimpleObjectMap result = Make(item, OwnerID, fieldList);
		result.OldFields = oldFieldList; 
		return result;   
	}  
	public static SimpleObjectMap Make(SObject item, ID OwnerId, map<String, String> fieldList) {
		SimpleObjectMap result = new SimpleObjectMap();
		result.Item = item; 
		result.OwnerId = OwnerId;
		result.Fields = fieldList; 
		return result;   
	}  
}