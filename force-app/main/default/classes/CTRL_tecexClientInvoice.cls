public class CTRL_tecexClientInvoice {

    public Invoice_New__c customerInvoice {get;set;}
    public Shipment_Order__c shipmentOrder {get;set;}
    public Account account {get;set;}

    public CTRL_tecexClientInvoice(ApexPages.StandardController controller) {
        this.customerInvoice = [SELECT Id, Name,Total_Credits_Applied_To_This_Invoice__c ,Stripe_Link__c , Upfront_No_Terms__c, Account__c, Total_Value_Applied__c, VAT_Number_Override__c, VAT_Number__c,Total_Invoice_Amount_Formula__c, Shipment_Order__c,Bill_To__c,Invoice_Note__c,Penalty_Status__c, Shipment_Order__r.Name, Freight_Request__r.Name, Conversion_Rate__c, PO_Number_Override__c, PO_Number__c, Invoice_Sent_Date__c, Amount_Outstanding_Local_Currency__c, Invoice_Name__c, Prepayment_date__c, Due_Date__c, IOR_Fees__c, EOR_Fees__c, Admin_Fees__c,   Actual_IOREOR_Costs__c, International_Freight_Fee__c, Liability_Cover_Fee__c, Total_Amount_Incl_Potential_Cash_Outlay__c, Cost_of_Sale_Shipping_Insurance__c, Taxes_and_Duties__c, Recharge_Tax_and_Duty_Other__c, Customs_Brokerage_Fees__c, Customs_Clearance_Fees__c, Customs_License_In__c, Customs_Handling_Fees__c, Bank_Fee__c, Miscellaneous_Fee__c, Miscellaneous_Fee_Name__c, Invoice_amount_USD__c, Collection_Administration_Fee__c, Amount_Outstanding__c, Invoice_Type__c, Invoice_Currency__c, Possible_Cash_Outlay__c, Cash_Outlay_Fee__c, Invoice_Amount_Local_Currency__c, IOR_EOR_Compliance__c, Government_and_In_country_Summary__c, Freight_and_Related_Costs__c, Billing_City__c, Billing_Country__c, Billing_Postal_Code__c, Billing_Street__c, Billing_State__c
                                FROM Invoice_New__c
                                WHERE Id = :ApexPages.currentPage().getParameters().get('id') LIMIT 1];
        if(customerInvoice.Shipment_Order__c !=null){
        this.shipmentOrder =  [ SELECT Id, Name,Service_Type__c,of_Line_Items__c ,Preferred_Freight_method__c , Client_Reference__c, of_packages__c, Chargeable_Weight__c, Final_Deliveries_New__c, Destination__c, Client_Reference_2__c, IOR_Price_List__r.Name, Ship_From_Country__c, Shipment_Value_USD__c, Ship_to_Country_new__c,
                                CreatedDate, New_Structure__c, Tax_recovery_Premium_Fee__c  
                                FROM Shipment_Order__c 
                                WHERE Id = :customerInvoice.Shipment_Order__c];
        }
        
        this.account = [SELECT Id, Name, Contract_Type_Signed__c, IOR_Payment_Terms__c, VAT_Number__c, BillingStreet, BillingCity, BillingState, BillingPostalCode, BillingCountry,
                        Cash_outlay_fee_base__c, Finance_Charge__c, New_Invoicing_Structure__c, Tax_Recovery_Switch_On__c, Tax_recovery_client__c, Prepayment_Term_Days__c 
                        FROM Account 
                        WHERE Id = :customerInvoice.Account__c];
    
    }

}