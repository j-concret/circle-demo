@RestResource(urlMapping='/NCPPushNotificationDeRegister/*')
Global class NCPPushNotificationDeRegister {
    
        
 @Httppost
    global static void NCPPushNotificationDeRegister(){
        
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        Blob body = req.requestBody;
    	res.addHeader('Content-Type', 'application/json');
        String requestString = body.toString();
        NCPPushNotificationDeRegisterWra rw = (NCPPushNotificationDeRegisterWra)JSON.deserialize(requestString,NCPPushNotificationDeRegisterWra.class);
        
         try {
         List<Access_token__c> at = [SELECT status__c,Access_Token__c FROM Access_token__c WHERE Access_Token__c =: rw.Accesstoken and status__c = 'Active' LIMIT 1];
          if(!AT.isempty()){
             If(rw.ContactID != null){
             contact Con = [SELECT ID,Push_Notification_Choice__c FROM Contact WHERE ID =: rw.ContactID LIMIT 1];
             Con.Push_Notification_Choice__c= FALSE;
                 Update Con;
             }
             
             If(!rw.Devices.isEmpty()){
                   list<APP_Tokens__c> PAL1 = new list<APP_Tokens__c>();  
                 List<string> regtokens =new list<string>();
                 For(Integer i =0;  rw.Devices.size()>i; i++)
                 {regtokens.add(rw.Devices[i].DeviceID);}
                 list<APP_Tokens__c> PAL =[Select ID,Contact__c,Device_Name__c,Registration_Token__c,Active__c from APP_Tokens__c where Registration_Token__c in:regtokens];
                 For(Integer j =0;  PAL.size()>j; j++)
                 {PAL[j].Active__C=FALSE;
                  PAL1.add(PAL[j]);
                 }
                 Update PAL1;
             }
             
            String SuccessString ='Device deactivated Succesfully';
            res.responseBody = Blob.valueOf(JSON.serializePretty(SuccessString));
            res.statusCode = 200;
              	API_Log__c Al = New API_Log__c();
                Al.Login_Contact__c=rw.ContactID;
                Al.EndpointURLName__c='Pushnotificationderegister';
                Al.Response__c='Success - Pushnotification de registered';
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al; 
              
              }
              Else{
                   String ErrorString ='INvalid access token or expired';
               	res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
            	res.statusCode = 400;
                API_Log__c Al = New API_Log__c();
                Al.Login_Contact__c=rw.ContactID;
                Al.EndpointURLName__c='Pushnotificationderegister';
                Al.Response__c=ErrorString;
                Al.StatusCode__c=string.valueof(res.statusCode);
              	Insert Al;
             }
            
            
        }
    Catch(Exception e){
        
        		String ErrorString ='Something went wrong while de-registering pushnotifications, please contact support_SF@tecex.com';
               	res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
            	res.statusCode = 500;
                API_Log__c Al = New API_Log__c();
                Al.Login_Contact__c=rw.ContactID;
                Al.EndpointURLName__c='Pushnotificationderegister';
                Al.Response__c=e.getMessage()+' at line number: '+e.getLineNumber();
                Al.StatusCode__c=string.valueof(res.statusCode);
              	Insert Al;
        
        
       }
        
        
        
        
    }
    

}