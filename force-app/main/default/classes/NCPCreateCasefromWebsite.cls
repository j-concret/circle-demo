@RestResource(urlMapping='/casecreation/*')
Global class NCPCreateCasefromWebsite {
     @Httppost
      global static void NCPCreateCasefromWebsite(){          
          RestRequest req = RestContext.request;
        	RestResponse res = RestContext.response;
          String Name = req.params.get('Name');
          String Surname = req.params.get('Surname');
          String Email = req.params.get('Email');
          String Title = req.params.get('Title');
          String Role = req.params.get('Role');
         
           Try{
              
                    user usr=[select ID from user where email='pietmana@tecex.com' limit 1];
               Case cs = new case();
               cs.Subject = 'Registration from website';
               cs.Status = 'new';
               cs.origin='web';
                cs.OwnerID=usr.ID;
               cs.Description='First name: '+Name+'; '+'Surname: '+Surname+'; '+'Email: '+Email+'; '+'Title: '+Title+'; '+'Role: '+Role;
               
               
                //  cs.Assigned_to__c=usr.ID;
                    cs.Subject = 'Registration from website';
                   // cs.Description ='New Registration from website';
                    cs.Status = 'new';
                    cs.origin='web';
               		cs.SuppliedName=Name+' '+Surname;
               		cs.SuppliedEmail=Email;
                    cs.Title__c=Title;
              		cs.RolefromWebsite__c=Role;  

                insert cs;
                    
                
                 	case cs1 =[Select Id,CaseNumber from case where id=:cs.Id];
                  	
                    JSONGenerator gen = JSON.createGenerator(true);
            		gen.writeStartObject();
            
             		gen.writeFieldName('Success');
             		gen.writeStartObject();
            
             			if(cs.id == null) {gen.writeNullField('Response');} else{gen.writeStringField('Response','Case created successfully. Casse Reference number is '+cs1.CaseNumber);}
           			    if(cs.id == null) {gen.writeNullField('CaseID');} else{gen.writeStringField('CaseID',+cs1.ID);}
            
            		gen.writeEndObject();
            		gen.writeEndObject();
               
            		String jsonData = gen.getAsString();
            		res.responseBody = Blob.valueOf(jsonData);
            		res.addHeader('Content-Type', 'application/json');
            		res.statusCode = 200;    
                API_Log__c Al = New API_Log__c();
                Al.EndpointURLName__c='NCPCreateCasefromWebsite';
                Al.StatusCode__c=string.valueof(res.statusCode);
                Al.Response__c = 'Created case' +cs.id;
                Insert Al;
                
                  
                  
                  
              }        
          
          Catch(Exception e){
              
              String ErrorString ='Something went wrong, please contact support_SF@tecex.com';
               		res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
            		res.addHeader('Content-Type', 'application/json');
        			res.statusCode = 500;
                
                API_Log__c Al = New API_Log__c();
                Al.EndpointURLName__c='NCPCreateCasefromWebsite';
                Al.Response__c=e.getMessage()+' at line number: '+e.getLineNumber();
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al;
                
              
              
          }




          
          
          
      }

}