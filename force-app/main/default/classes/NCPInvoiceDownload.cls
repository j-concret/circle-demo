@RestResource(urlMapping='/InvoiceDownload/*')
Global class NCPInvoiceDownload {
    
    
    @Httppost
    global static void NCPInvoiceDownload(){
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        Blob body = req.requestBody;
        res.addHeader('Content-Type', 'application/json');
        String requestString = body.toString();
        NCPInvoiceDownloadWra rw  = (NCPInvoiceDownloadWra)JSON.deserialize(requestString,NCPInvoiceDownloadWra.class);
        try{
            List<Access_token__c> AtList =[select status__c,Access_Token__c from Access_token__c where Access_Token__c=:rw.Accesstoken limit 1];
            if( AtList[0].status__C =='Active'){
                
                ID customerId=rw.InvoiceID;
                
                
                
                map<string,Object> Invoicemap =  CTRL_generateAndSendInvoice.NCPDownloadInvoice(customerId); 
                Boolean isemailSent =(Boolean)Invoicemap.get('isSent'); 
                If(isemailSent==TRUE) {
                    
                    attachment Att= (attachment)Invoicemap.get('NCPInvoiceAttachment'); 
                    
                    List<attmnt> oppatts = new list<attmnt>();
                    
                    if( att != null){
                        
                        attmnt a=new attmnt();               
                        a.AttachmentType=att.ContentType;
                        a.AttachmentName=att.Name;
                        // a.AttachmentId=att.Id;
                        
                        // attachment body to convert to blob format.
                        string MyFile=EncodingUtil.base64Encode(att.body);
                        Blob MyBlob=EncodingUtil.base64Decode(MyFile);
                        
                        a.AttachmentBody=MyBlob;
                        
                        oppatts.add(a);
                        
                        
                    }    
                    
                    string Jsonstring=Json.serialize(oppatts);
                    
                    res.responseBody=Blob.valueOf(Jsonstring);
                    res.addHeader('Content-Type', 'application/json');
                    res.statusCode = 200;
                    
                    
                    API_Log__c Al = New API_Log__c();
                    Al.EndpointURLName__c='InvoiceDownload';
                    // Al.Account__c=rw.AccountID;
                    Al.StatusCode__c=string.valueof(res.statusCode);
                    AL.Response__c = 'Success - InvoiceDownloaded';
                    Insert Al;  
                }
                Else{
                    String abc= ' Invoice not availble, Please contact  support_SF@tecex.com';
                    res.responseBody = Blob.valueOf(abc);
                    res.addHeader('Content-Type', 'application/json');
                    res.statusCode = 400;
                    
                    API_Log__c Al = New API_Log__c();
                    Al.EndpointURLName__c='InvoiceDownload';
                    //  Al.Account__c=rw.AccountID;
                    Al.StatusCode__c=string.valueof(res.statusCode);
                    AL.Response__c = ' Invoice not downloaded, due to no attachement on record';
                    Insert Al; 
                }
            }
            
            else{
                JSONGenerator gen = JSON.createGenerator(true);
                gen.writeStartObject();
                
                gen.writeFieldName('Error');
                gen.writeStartObject();
                
                gen.writeStringField('Response', 'Accesstoken Expired');
                
                gen.writeEndObject();
                gen.writeEndObject();
                String jsonData = gen.getAsString();
                res.responseBody = Blob.valueOf(jsonData);
                res.addHeader('Content-Type', 'application/json');
                res.statusCode = 400;        
                
                
            }
            
        }
        
        catch(Exception e){
            
            
            
            
            String ErrorString ='Something went wrong, please contact support_SF@tecex.com'+e.getMessage();
            res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
            res.addHeader('Content-Type', 'application/json');
            res.statusCode = 400;
            System.debug('Exception '+e.getMessage()+e.getLineNumber());
            API_Log__c Al = New API_Log__c();
            //   Al.Account__c=rw.AccountID;
            //Al.Login_Contact__c=logincontact.Id;
            Al.EndpointURLName__c='InvoiceDownload';
            Al.Response__c=ErrorString;
            Al.StatusCode__c=string.valueof(res.statusCode);
            Insert Al;
            
            
            
            
            
        }
        
    }
    global class attmnt{       
        public string AttachmentType{get;set;}
        public string AttachmentName{get;set;}
        public blob AttachmentBody{get;set;}
    } 
}