@isTest
global class NoChangeRecordUpdateBatchTest {
    
    static testmethod void test() {
        
        List<Account> accList = new List<Account>();
        for (Integer i=0; i<10 ; i++) {
            Account m = new Account(Name = 'Account ' + i);
            accList.add(m);
        }
        insert accList;
        
        Test.startTest();
        	NoChangeRecordUpdateBatch noChangeBatch = new NoChangeRecordUpdateBatch('Select Id FROM Account LIMIT 10');
        	Database.executeBatch(noChangeBatch);
        Test.stopTest();
        
    }
    
    static testmethod void test2() {
        
        List<Account> accList = new List<Account>();
        for (Integer i=0; i<10 ; i++) {
            Account m = new Account(Name = 'Account ' + i);
            accList.add(m);
        }
        insert accList;
        
        List<Id> recordId = new List<Id>();
        recordId.add(accList[0].Id);
        
        Test.startTest();
        	NoChangeRecordUpdateBatch noChangeBatch = new NoChangeRecordUpdateBatch(recordId);
        	Database.executeBatch(noChangeBatch);
        Test.stopTest();
        
    }
}