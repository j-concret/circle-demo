@RestResource(urlMapping='/SOAEmail/*')
Global class NCPSOAEmail {
   
   

     @Httppost
      global static void NCPSOAEmail(){
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        Blob body = req.requestBody;
        res.addHeader('Content-Type', 'application/json');
        String requestString = body.toString();
        NCPSOAEmailwra rw  = (NCPSOAEmailwra)JSON.deserialize(requestString,NCPSOAEmailwra.class);
          try{
              Access_token__c At =[select status__c,Access_Token__c from Access_token__c where Access_Token__c=:rw.Accesstoken ];
            	if( at.status__C =='Active'){
                     if(rw.ccEmailID ==null){ rw.ccEmailID=rw.toEmailID;}
                    ID customerId=rw.AccountID;
                    List<Account> Acc =[select id , name from account where id= :rw.AccountID ];
                    
                    list<Contact> contact = [SELECT Id, Name, Email, Title
                  FROM Contact
                 WHERE AccountId = :rw.AccountID AND Email!=null AND IsEmailBounced = false and email =:rw.toEmailID];
                    
					List<String> toAddress = new List<String>();
                    toaddress.add(rw.toEmailID);
                     List<String> ccAddress = new List<String>();
                    for(String cc : rw.ccEmailID.split(';')){ccAddress.add(cc); }
                    
                    
                    CTRL_EmailAndAttachStatement.NCPSOAsendStatement(Acc[0],toAddress,ccAddress,contact[0]); 
                    
               
          
             String abc= '"Success - SOAEmail sent"';
             res.responseBody = Blob.valueOf(abc);
             res.addHeader('Content-Type', 'application/json');
             res.statusCode = 200;
                
                API_Log__c Al = New API_Log__c();
                Al.EndpointURLName__c='SOAEmail';
                Al.Account__c=rw.AccountID;
                Al.StatusCode__c=string.valueof(res.statusCode);
                AL.Response__c = 'Success - SOAEmail sent';
                Insert Al;  
              }
              
              else{
                   JSONGenerator gen = JSON.createGenerator(true);
            gen.writeStartObject();
            
             gen.writeFieldName('Error');
             gen.writeStartObject();
            
                gen.writeStringField('Response', 'Accesstoken Expired');
            
            gen.writeEndObject();
            gen.writeEndObject();
            String jsonData = gen.getAsString();
            res.responseBody = Blob.valueOf(jsonData);
            res.addHeader('Content-Type', 'application/json');
            res.statusCode = 400;        
            
                  
              }
             
                }
              
            catch(Exception e){
                
                
                IF(e.getMessage()=='Insert failed. First exception on row 0; first error: INSUFFICIENT_ACCESS_ON_CROSS_REFERENCE_ENTITY, insufficient access rights on cross-reference id: []'){
              
             String abc= '"Success - SOAEmail sent"';
             res.responseBody = Blob.valueOf(abc);
             res.addHeader('Content-Type', 'application/json');
             res.statusCode = 200;
                
                API_Log__c Al = New API_Log__c();
                Al.EndpointURLName__c='SOAEmail';
                Al.Account__c=rw.AccountID;
                Al.StatusCode__c=string.valueof(res.statusCode);
                AL.Response__c = 'Success - SOAEmail sent';
                Insert Al; 
                }
                else{
                
                String ErrorString ='Something went wrong, please contact support_SF@tecex.com'+e.getMessage()+e.getLineNumber();
                       res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
                   res.addHeader('Content-Type', 'application/json');
                    res.statusCode = 400;
                
                API_Log__c Al = New API_Log__c();
                Al.Account__c=rw.AccountID;
                //Al.Login_Contact__c=logincontact.Id;
                Al.EndpointURLName__c='NCPSOAEmail';
                Al.Response__c=ErrorString;
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al;
                      
          }
          
          
      
      
      
            }
      
      }
}