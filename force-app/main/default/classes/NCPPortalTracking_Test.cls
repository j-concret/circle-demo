@isTest
public class NCPPortalTracking_Test {
    static testMethod void  testForNCPGetCourierServices(){
        
       Id shipmentOrderRecTypeId = Schema.SObjectType.shipment_order__c.getRecordTypeInfosByName().get('Cost Estimate').getRecordTypeId();
        
        
        CpaRules__c cpaRule = new CpaRules__c();
        cpaRule.Country__c = 'Finland';
        cpaRule.Criteria__c = 'Default';
        
        Insert cpaRule;
        
        Access_token__c at = new Access_token__c();
        at.Status__c = 'Active';
        at.Access_Token__c = '123';
        
        Insert at;
        
        Account acc = new Account(Name = 'Acme Test');
        
        Insert acc;
        
       Contact con = new Contact(LastName='Test Contact1',AccountId=acc.Id, Email = 'Test@test.io');
        
      Insert con;
        
        Account acc1 = new Account(Name = 'TecEx Prospective Client');
        
        Insert acc1;
        
        IOR_Price_List__c priceList = new IOR_Price_List__c();
        priceList.Destination__c = 'Finland';
        priceList.Client_Name__c = acc.Id;
        priceList.IOR_Fee__c = 10;
        
        Insert priceList;
        
        shipment_order__c shipment = new shipment_order__c();
        shipment.RecordTypeId = shipmentOrderRecTypeId;
        shipment.Destination__c = 'Finland';
        shipment.IOR_Price_List__c = priceList.Id;
        shipment.Shipment_Value_USD__c = 600;
        shipment.Account__c = acc.Id;
        shipment.Client_Contact_for_this_Shipment__c = con.Id;
        shipment.Portal_Tracking_Number__c ='2digNA';
        
        Insert shipment;
        
        SO_Travel_History__c SOTH =  new SO_Travel_History__c (
                                Source__c = 'Task (internal)',
                                Date_Time__c =  System.now(),
                                Location__c = '',
                                Description__c = 'NONE',
                                Expected_Date_to_Next_Status__c=System.now(),
                               Type_of_Update__c = 'NONE',
                                TecEx_SO__c = shipment.ID
                            );
        
        Insert SOTH;
        
        RestRequest req1 = new RestRequest();
        RestResponse res1 = new RestResponse();
        req1.requestURI = '/services/apexrest/PortalTrackingsdetails/*';
        req1.params.put('Portaltrackingnumber','2digNA');
        req1.httpMethod = 'POST';
        RestContext.request = req1;
        RestContext.response = res1;
        RestContext.request.addHeader('Content-Type', 'application/json');
        
        Test.startTest(); 
        	NCPPortalTracking.NCPGetCourierServices();
        Test.stopTest();
    }
    
     static testMethod void  testForNCPGetCourierServices_2(){
        
       Id shipmentOrderRecTypeId = Schema.SObjectType.shipment_order__c.getRecordTypeInfosByName().get('Cost Estimate').getRecordTypeId();
        
        
        CpaRules__c cpaRule = new CpaRules__c();
        cpaRule.Country__c = 'Finland';
        cpaRule.Criteria__c = 'Default';
        
        Insert cpaRule;
        
        Access_token__c at = new Access_token__c();
        at.Status__c = 'Active';
        at.Access_Token__c = '123';
        
        Insert at;
        
        Account acc = new Account(Name = 'Acme Test');
        
        Insert acc;
        
       Contact con = new Contact(LastName='Test Contact1',AccountId=acc.Id, Email = 'Test@test.io');
        
      Insert con;
        
        Account acc1 = new Account(Name = 'TecEx Prospective Client');
        
        Insert acc1;
        
        IOR_Price_List__c priceList = new IOR_Price_List__c();
        priceList.Destination__c = 'Finland';
        priceList.Client_Name__c = acc.Id;
        priceList.IOR_Fee__c = 10;
        
        Insert priceList;
        
        shipment_order__c shipment = new shipment_order__c();
        shipment.RecordTypeId = shipmentOrderRecTypeId;
        shipment.Destination__c = 'Finland';
        shipment.IOR_Price_List__c = priceList.Id;
        shipment.Shipment_Value_USD__c = 600;
        shipment.Account__c = acc.Id;
        shipment.Client_Contact_for_this_Shipment__c = con.Id;
        shipment.Portal_Tracking_Number__c ='2digNA';
        
        Insert shipment;
        
        
        RestRequest req1 = new RestRequest();
        RestResponse res1 = new RestResponse();
        req1.requestURI = '/services/apexrest/PortalTrackingsdetails/*';
        req1.params.put('Portaltrackingnumber','2digNA');
        req1.httpMethod = 'POST';
        RestContext.request = req1;
        RestContext.response = res1;
        RestContext.request.addHeader('Content-Type', 'application/json');
        
        Test.startTest(); 
        	NCPPortalTracking.NCPGetCourierServices();
        Test.stopTest();
    }
     static testMethod void  testForNCPGetCourierServices_3(){
        
       Id shipmentOrderRecTypeId = Schema.SObjectType.shipment_order__c.getRecordTypeInfosByName().get('Cost Estimate').getRecordTypeId();
        
        
        CpaRules__c cpaRule = new CpaRules__c();
        cpaRule.Country__c = 'Finland';
        cpaRule.Criteria__c = 'Default';
        
        Insert cpaRule;
        
        Access_token__c at = new Access_token__c();
        at.Status__c = 'Active';
        at.Access_Token__c = '123';
        
        Insert at;
        
        Account acc = new Account(Name = 'Acme Test');
        
        Insert acc;
        
       Contact con = new Contact(LastName='Test Contact1',AccountId=acc.Id, Email = 'Test@test.io');
        
      Insert con;
        
        Account acc1 = new Account(Name = 'TecEx Prospective Client');
        
        Insert acc1;
        
        IOR_Price_List__c priceList = new IOR_Price_List__c();
        priceList.Destination__c = 'Finland';
        priceList.Client_Name__c = acc.Id;
        priceList.IOR_Fee__c = 10;
        
        Insert priceList;
        
        shipment_order__c shipment = new shipment_order__c();
        shipment.RecordTypeId = shipmentOrderRecTypeId;
        shipment.Destination__c = 'Finland';
        shipment.IOR_Price_List__c = priceList.Id;
        shipment.Shipment_Value_USD__c = 600;
        shipment.Account__c = acc.Id;
        shipment.Client_Contact_for_this_Shipment__c = con.Id;
        shipment.Portal_Tracking_Number__c ='2digNA';
        
        Insert shipment;
        
        SO_Travel_History__c SOTH =  new SO_Travel_History__c (
                                Source__c = 'Task (internal)',
                                Date_Time__c =  System.now(),
                                Location__c = '',
                                Description__c = 'NONE',
                                Expected_Date_to_Next_Status__c=System.now(),
                               Type_of_Update__c = 'NONE',
                                TecEx_SO__c = shipment.ID
                            );
        
        Insert SOTH;
        
        RestRequest req1 = new RestRequest();
        RestResponse res1 = new RestResponse();
        req1.requestURI = '/services/apexrest/PortalTrackingsdetails/*';
//        req1.params.put('Portaltrackingnumber','2digNA');
        req1.httpMethod = 'POST';
        RestContext.request = req1;
        RestContext.response = res1;
        RestContext.request.addHeader('Content-Type', 'application/json');
        
        Test.startTest(); 
        	NCPPortalTracking.NCPGetCourierServices();
        Test.stopTest();
    }

}