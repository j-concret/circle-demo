@isTest(SeeAllData=false)
public class PReInspectionCarrierChoiceCtrl_Test {

    @Testsetup
    static void setupData(){
        CountryandRegionMap__c crm = new CountryandRegionMap__c();
            crm.Name = 'Brazil';
            crm.Country__c = 'Brazil';
            crm.European_Union__c = 'No';
            crm.Region__c = 'South America';
            
            insert crm;
            
            CountryandRegionMap__c crm1 = new CountryandRegionMap__c();
            crm1.Name = 'United States';
            crm1.Country__c = 'United States';
            crm1.European_Union__c = 'No';
            crm1.Region__c = 'Oceania';
            
            insert crm1;
            
            DefaultFreightValues__c df = new DefaultFreightValues__c();
            df.Name = 'Test';
            df.Courier_Base_Rate_KG__c = 12;
            df.Courier_Dangerous_Goods_premium__c = 12;
            df.Courier_Fixed_Charge__c = 12;
            df.Courier_Non_stackable_premium__c = 12;
            df.Courier_Oversized_premium__c = 12;
            df.FF_Base_Rate_KG__c = 12;
            df.FF_Dangerous_Goods_Fixed_Charge_Premium__c = 12;
            df.FF_Dangerous_Goods_premium__c = 12;
            df.FF_Dedicated_Pickup__c = 12;
            df.FF_Fixed_Charge__c = 12;
            df.FF_Non_stackable_Fixed_Charge_Premium__c = 12;
            df.FF_Non_stackable_premium__c = 12;
            df.FF_Oversized_Fixed_Charge_Premium__c = 12;
            df.FF_Oversized_premium__c = 12;
            insert df;
            
            RegionalFreightValues__c rfm = new RegionalFreightValues__c();
            rfm.Name = 'Oceania to South America';
            rfm.Courier_Discount_Premium__c = 12;
            rfm.Courier_Fixed_Premium__c = 10;
            rfm.Destination__c = 'Brazil';
            rfm.FF_Fixed_Premium__c = 123;
            rfm.FF_Regional_Discount_Premium__c = 12;
            rfm.Origin__c = 'United States';
            rfm.Preferred_Courier_Id__c = 'TECEX';
            rfm.Preferred_Courier_Name__c = 'TECEX';
            rfm.Preferred_Freight_Forwarder_Id__c = 'TECEX';
            rfm.Preferred_Freight_Forwarder_Name__c = 'TECEX';
            rfm.Risk_Adjustment_Factor__c = 1;
            insert rfm;

        Account account = new Account (name='TecEx Prospective Client', Type ='Client',
                                       CSE_IOR__c = '0050Y000001LTZO',
                                       Service_Manager__c = '0050Y000001LTZO',
                                       Financial_Manager__c='0050Y000001LTZO',
                                       Financial_Controller__c='0050Y000001LTZO',
                                       Tax_recovery_client__c  = FALSE
                                       );
        insert account;

        Account account2 = new Account (name='Test Supplier', Type ='Supplier', ICE__c = '0050Y000001km5c', RecordTypeId = '0120Y0000006KjQQAU');
        insert account2;

        Account account3 = new Account (name='Test Manufacturer', Type ='Manufacturer', RecordTypeId = '0120Y0000002wa0QAA', Manufacturer_Alias__c = 'Tester,Who' ); insert account3;

        Account account4 = new Account (name='ACME', Type ='Manufacturer', RecordTypeId = '0120Y0000002wa0QAA', Manufacturer_Alias__c = 'Buggs,Bunny' ); insert account4;


        Contact contact = new Contact ( lastname ='Testing Individual',  AccountId = account.Id);
        insert contact;

        Contact contact2 = new Contact ( lastname ='Testing supplier',  AccountId = account2.Id);
        insert contact2;

        IOR_Price_List__c iorpl = new IOR_Price_List__c ( Client_Name__c = account.Id, Name = 'Brazil', IOR_Fee__c = 0.15, On_Charge_Mark_up__c = 0.15,
                                                          TecEx_Shipping_Fee_Markup__c =0.40, IOR_Min_Fee__c = 100,  Admin_Fee__c =100, Set_up_fee__c =100, Bank_Fees__c =200, Tax_Rate__c = 0.20,
                                                          Estimated_Customs_Brokerage__c =250, Estimated_Customs_Clearance__c =250, Estimated_Customs_Handling__c =250, Estimated_Import_License__c =250, Destination__c = 'Brazil' );
        insert iorpl;

        country_price_approval__c cpa = new country_price_approval__c( name ='Brazil', Billing_term__c = 'DAP/CIF - IOR pays',  Airwaybill_Instructions__c = 'User IOR Address', Supplier__c = account2.Id,
                                                                       In_Country_Specialist__c = '0050Y000001km5c', Clearance_Destination__c = 'Brazil', Preferred_Supplier__c = TRUE, Destination__c = 'Brazil' );
        insert cpa;

        Currency_Management2__c CM = new Currency_Management2__c (Name = 'US Dollar (USD)', Currency__c = 'US Dollar (USD)', ISO_Code__c = 'USD', Conversion_Rate__c = 1);
        insert CM;

        Country__c country = new Country__c(Brokerage_Costs__c = 618, CIF_Adjustment_Factor__c = 0.035, CIF_Absolute_value_adjustment__c = 200, Clearance_Costs__c = 488,
                                            Country__c = 'Brazil', Name = 'Brazil', Handling_Costs__c = 1180, License_Permit_Costs__c = 0);
        insert country;

        Tax_Structure_Per_Country__c tax = new Tax_Structure_Per_Country__c(Applied_to_Value__c = 'CIF', Country__c = country.Id, Default_Duty_Rate__c = 0.5625,
                                                                            Order_Number__c = '1', Part_Specific__c = TRUE, Name = 'II', Tax_Type__c = 'Duties');
        insert tax;

        CPA_v2_0__c RelatedCPA = new CPA_v2_0__c(Name = 'Brazil', Country_Applied_Costings__c=true );
        insert RelatedCPA;
        CPA_v2_0__c cpav2 = new CPA_v2_0__c(Name = 'Brazil Testing 2.0', Supplier__c = account2.Id, Service_Type__c = 'IOR/EOR', Related_Costing_CPA__c = RelatedCPA.Id, Preferred_Supplier__c = TRUE,
                                            VAT_Rate__c = 0.1, Destination__c = 'Brazil',
                                            VAT_Reclaim_Destination__c = FALSE,
                                            Final_Destination__c ='Brazil',
                                            Country__c = country.id

                                            );
        insert cpav2;

        List<CPARules__c> CPARules = new List<CPARules__c>();
        CPARules.add(new CPARules__c(Country__c = 'Brazil',Criteria__c ='Default', Chargable_weight__c = 'NONE', Courier_Responsibility__c = 'NONE',   ECCN_No__c = 'NONE', NL_Product_User__c = 'NONE', Number_of_final_deliveries__c = 'NONE', ShipmentValue__c ='NONE', Sum_of_quantity_of_matched_products__c = 'NONE', Manufacturer__c = 'NONE', DefaultCPA2__c = cpav2.Id, CPA2__c =  cpav2.Id));
        insert CPARules;

        List<CPA_Costing__c> costs = new List<CPA_Costing__c>();
        costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'Shipment value', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'IOR', Min__c = 375, Max__c = 14000, Conditional_value__c = 'Shipment value', Condition__c = 'Between', Floor__c = 0, Ceiling__c = 10000, Currency__c = 'US Dollar (USD)'));
        costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'Anil', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'IOR', Currency__c = 'US Dollar (USD)',Variable_threshold__c = null));
        insert costs;


        Product2 product = new Product2(Name = 'CAB-ETH-S-RJ45', US_HTS_Code__c='85444210',description='Anil', ProductCode = 'CAB-ETH-S-RJ45', Manufacturer__c = account3.Id); insert product;
        Product2 product2 = new Product2(Name = 'CAB-ETH-S-RJ46',US_HTS_Code__c='85444210', ProductCode = 'CAB-ETH-S-RJ46', Manufacturer__c = account3.Id); insert product2;
        Product2 product3 = new Product2(Name = 'CAB-ETH-S', US_HTS_Code__c='85444210',ProductCode = 'CAB-ETH-S', Manufacturer__c = account3.Id); insert product3;




        Shipment_Order__c shipmentOrder = new Shipment_Order__c(Account__c = account.id, Shipment_Value_USD__c = 2000, CPA_v2_0__c = cpav2.Id,
                                                                Client_Contact_for_this_Shipment__c = contact.Id, Who_arranges_International_courier__c ='Client', Tax_Treatment__c ='DAP/CIF - IOR pays',
                                                                Ship_to_Country__c = cpa.Id, Destination__c = 'Brazil', Service_Type__c = 'IOR', IOR_Price_List__c = iorpl.Id, Chargeable_Weight__c = 200,
                                                                Shipping_Status__c = 'Cost Estimate Abandoned' );
        insert shipmentOrder;


        List<Part__c> partList = new List<Part__c>();

        partList.add(new Part__c(Name ='CAB-ETH-S-RJ45',  Quantity__c= 1, Product__c=product.Id, Commercial_Value__c = 15000, US_HTS_Code__c = '85444210',
                                 Country_of_Origin2__c = 'China', Shipment_Order__c = shipmentOrder.Id, Description_and_Functionality__c ='Anil'));

        partList.add(new Part__c(Name ='AsRdy',  Quantity__c= 1,  Product__c=product.Id, Commercial_Value__c = 15000, US_HTS_Code__c = '85444210',
                                 Country_of_Origin2__c = 'China', Shipment_Order__c = shipmentOrder.Id, Description_and_Functionality__c ='Anil AsRdy'));

        partList.add(new Part__c(Name ='CAB-ETH-S-RJ46',  Quantity__c= 1, Commercial_Value__c = 15000, US_HTS_Code__c = '85478123',
                                 Country_of_Origin2__c = 'China', Shipment_Order__c = shipmentOrder.Id, Description_and_Functionality__c ='AB-ETH-S-RJ46'));

        partList.add(new Part__c(Name ='CAB-ETH-S-RJ46',  Quantity__c= 1, Commercial_Value__c = 15000, US_HTS_Code__c = '834512879',
                                 Country_of_Origin2__c = 'China', Shipment_Order__c = shipmentOrder.Id, Description_and_Functionality__c ='AB-ETH-S-RJ46'));

        partList.add(new Part__c(Name ='Testing',  Quantity__c= 1, Commercial_Value__c = 15000, US_HTS_Code__c = '834512879',
                                 Country_of_Origin2__c = 'China', Shipment_Order__c = shipmentOrder.Id, Description_and_Functionality__c ='Testing'));


        partList.add(new Part__c(Name ='Who CAB-ETH-S',  Quantity__c= 1, Commercial_Value__c = 15000, US_HTS_Code__c = '8471254784',
                                 Country_of_Origin2__c = 'China', Shipment_Order__c = shipmentOrder.Id, Description_and_Functionality__c ='Testing'));

        insert partList;


    }

    static testMethod void getTaskRecordTest(){
        Master_task__c masterTask =  new Master_Task__c(Name = 'Demo Task', States__c = 'TecEx Pending;Resolved',
                                                        Operational__c = true, Condition__c = '{0} > 5000',
                                                        Condition_Fields_API_Name__c = 'Shipment_Order__c.Shipment_Value_USD__c,Country__c',
                                                        Assess_Condition_on_Creation_of__c = 'Shipment_Order__c',TecEx_Assigned_To__c = 'Compliance');
        insert masterTask;

        Shipment_Order__c shipmentOrder = [SELECT Account__c, Shipment_Value_USD__c, CPA_v2_0__c,
                                           Client_Contact_for_this_Shipment__c, Who_arranges_International_courier__c, Tax_Treatment__c,
                                           Ship_to_Country__c, Destination__c, Service_Type__c, IOR_Price_List__c, Chargeable_Weight__c,
                                           Shipping_Status__c FROM Shipment_Order__c LIMIT 1];

        Freight__c FR = new Freight__c(Shipment_Order__c =shipmentOrder.Id, Service_type__c ='Courier');
        insert FR;

        Task tsk = new Task(status = 'In Progress',RecordTypeId = Schema.SObjectType.Task.getRecordTypeInfosByName().get('MasterTaskRelated').getRecordTypeId(),priority = 'Normal',Subject='Call', WhatId = shipmentOrder.Id, Master_Task__c = masterTask.Id, State__c = 'Not Started',Auto_Resolved__c = true);
        insert tsk;

        Test.startTest();
        Task rtask = PReInspectionCarrierChoiceCtrl.getTaskRecord(tsk.Id);
        System.assert (rtask != null);
        Map<String,Object> taskMap = PReInspectionCarrierChoiceCtrl.getTaskWithConsignee(tsk.Id);
        System.assert (taskMap.get('task') != null);
        System.assert (taskMap.get('freight') != null);
        PReInspectionCarrierChoiceCtrl.updateTask(rtask.Id, 'TecEx Pending');

        Test.stopTest();

    }

    static testMethod void getTaskRecordTestCatchBlock(){
        Master_task__c masterTask =  new Master_Task__c(Name = 'Demo Task', States__c = 'TecEx Pending;Resolved',
                                                        Operational__c = true, Condition__c = '{0} > 5000',
                                                        Condition_Fields_API_Name__c = 'Shipment_Order__c.Shipment_Value_USD__c,Country__c',
                                                        Assess_Condition_on_Creation_of__c = 'Shipment_Order__c',TecEx_Assigned_To__c = 'Compliance');
        insert masterTask;

        Shipment_Order__c shipmentOrder = [SELECT Account__c, Shipment_Value_USD__c, CPA_v2_0__c,
                                           Client_Contact_for_this_Shipment__c, Who_arranges_International_courier__c, Tax_Treatment__c,
                                           Ship_to_Country__c, Destination__c, Service_Type__c, IOR_Price_List__c, Chargeable_Weight__c,
                                           Shipping_Status__c FROM Shipment_Order__c LIMIT 1];

        Freight__c FR = new Freight__c(Shipment_Order__c =shipmentOrder.Id, Service_type__c ='Courier');
        insert FR;

        Task tsk = new Task(status = 'In Progress',priority = 'Normal',Subject='Call', WhatId = shipmentOrder.Id, Master_Task__c = masterTask.Id, State__c = 'Not Started',Auto_Resolved__c = true);
        insert tsk;

        Test.startTest();
        try{
            Task rtask = PReInspectionCarrierChoiceCtrl.getTaskRecord('');
        }catch(Exception e) {
            System.debug(e.getMessage());
        }
        Test.stopTest();

    }

    static testMethod void getTaskWithConsigneeTestCatchBlock(){
        Master_task__c masterTask =  new Master_Task__c(Name = 'Demo Task', States__c = 'TecEx Pending;Resolved',
                                                        Operational__c = true, Condition__c = '{0} > 5000',
                                                        Condition_Fields_API_Name__c = 'Shipment_Order__c.Shipment_Value_USD__c,Country__c',
                                                        Assess_Condition_on_Creation_of__c = 'Shipment_Order__c',TecEx_Assigned_To__c = 'Compliance');
        insert masterTask;

        Shipment_Order__c shipmentOrder = [SELECT Account__c, Shipment_Value_USD__c, CPA_v2_0__c,
                                           Client_Contact_for_this_Shipment__c, Who_arranges_International_courier__c, Tax_Treatment__c,
                                           Ship_to_Country__c, Destination__c, Service_Type__c, IOR_Price_List__c, Chargeable_Weight__c,
                                           Shipping_Status__c FROM Shipment_Order__c LIMIT 1];

        Freight__c FR = new Freight__c(Shipment_Order__c =shipmentOrder.Id, Service_type__c ='Courier');
        insert FR;

        Task tsk = new Task(status = 'In Progress',priority = 'Normal',Subject='Call', WhatId = shipmentOrder.Id, Master_Task__c = masterTask.Id, State__c = 'Not Started',Auto_Resolved__c = true);
        insert tsk;

        Test.startTest();
        try{
            Map<String,Object> taskMap = PReInspectionCarrierChoiceCtrl.getTaskWithConsignee('');
        }catch(Exception e) {
            System.debug(e.getMessage());
        }
        Test.stopTest();

    }

    static testMethod void updateTaskTestCatchBlock(){
        Master_task__c masterTask =  new Master_Task__c(Name = 'Demo Task', States__c = 'TecEx Pending;Resolved',
                                                        Operational__c = true, Condition__c = '{0} > 5000',
                                                        Condition_Fields_API_Name__c = 'Shipment_Order__c.Shipment_Value_USD__c,Country__c',
                                                        Assess_Condition_on_Creation_of__c = 'Shipment_Order__c',TecEx_Assigned_To__c = 'Compliance');
        insert masterTask;

        Shipment_Order__c shipmentOrder = [SELECT Account__c, Shipment_Value_USD__c, CPA_v2_0__c,
                                           Client_Contact_for_this_Shipment__c, Who_arranges_International_courier__c, Tax_Treatment__c,
                                           Ship_to_Country__c, Destination__c, Service_Type__c, IOR_Price_List__c, Chargeable_Weight__c,
                                           Shipping_Status__c FROM Shipment_Order__c LIMIT 1];

        Freight__c FR = new Freight__c(Shipment_Order__c =shipmentOrder.Id, Service_type__c ='Courier');
        insert FR;

        Task tsk = new Task(status = 'In Progress',priority = 'Normal',Subject='Call', WhatId = shipmentOrder.Id, Master_Task__c = masterTask.Id, State__c = 'Not Started',Auto_Resolved__c = true);
        insert tsk;

        Test.startTest();
        try{
            PReInspectionCarrierChoiceCtrl.updateTask(tsk.Id,'');
        }catch(Exception e) {
            System.debug(e.getMessage());
        }
        Test.stopTest();

    }
}