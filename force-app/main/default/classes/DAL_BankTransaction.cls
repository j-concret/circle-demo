/**
 * Created by Caro on 2019-04-14.
 */

public with sharing class DAL_BankTransaction extends fflib_SObjectSelector {


    public List<Schema.SObjectField> getSObjectFieldList() {

        return new List<Schema.SObjectField>{
                Bank_transactions__c.Name,
                Bank_transactions__c.Id,
                Bank_transactions__c.Amount__c,
                Bank_transactions__c.Unapplied__c,
                Bank_transactions__c.Applied_amount__c,
                Bank_transactions__c.Date__c,
                Bank_transactions__c.RecordTypeId,
                Bank_transactions__c.Status__c,
                Bank_transactions__c.Supplier__c,
                Bank_transactions__c.Type__c
        };

    }

    public Schema.SObjectType getSObjectType() {

        return Bank_transactions__c.sObjectType;
    }

    public List<Bank_transactions__c> selectById(Set<Id> idSet) {

        return (List<Bank_transactions__c>) selectSObjectsById(idSet);
    }

    public List<Bank_transactions__c> selectUnappliedTransactionsByCustomerId(Id customerId){

        return selectUnappliedTransactionsByCustomerId(new Set<Id>{customerId});
    }

    public List<Bank_transactions__c> selectUnappliedTransactionsByCustomerId(Set<Id> customerIdSet) {
  

       fflib_QueryFactory unappliedBTs =  newQueryFactory()
                       .setCondition('Supplier__c IN :customerIdSet AND Type__c = \'Shipment\' AND (Unapplied__c >= 1 OR  Unapplied__c <= -1 )');

          return  Database.query(unappliedBTs.toSOQL());
    }
        
    

    public Decimal getTotalUnappliedShipmentAmountByCustomerId(Id customerId) {
        Decimal unappliedShipmentTransactionAmount = 0;
        for (DMN_Statement.AgingPeriod customerBankTransaction : getTotalUnappliedAmountByCustomerId(customerId)){
            if (customerBankTransaction.periodLabel == 'Shipment' ) unappliedShipmentTransactionAmount =  customerBankTransaction.periodAmount;
        }
        return unappliedShipmentTransactionAmount;
    }

    public List<DMN_Statement.AgingPeriod> getTotalUnappliedAmountByCustomerId(Id customerId) { 
      List<DMN_Statement.AgingPeriod> test = getTotalUnappliedAmountByCustomerId(new Set<Id>{customerId}).get(customerId);
      List<DMN_Statement.AgingPeriod> test2 = new List<DMN_Statement.AgingPeriod>();   
        
     System.debug('test'+ test);      
        
        If(test != null ){
            
      return test;        
        }        
        else{
         return test2;            
        } 
    }

            

    public Map<Id,List<DMN_Statement.AgingPeriod>> getTotalUnappliedAmountByCustomerId(Set<Id> customerIdSet) {

        Map<Id,List<DMN_Statement.AgingPeriod>> bankTransactionResult = new Map<Id,List<DMN_Statement.AgingPeriod>>();
        List<DMN_Statement.AgingPeriod> bankTransactionList = new List<DMN_Statement.AgingPeriod>();

        List<AggregateResult> bankTransactionAggregate = [SELECT
                SUM(Unapplied__c) UnappliedAmt,
                Supplier__c,
                Type__c
        FROM Bank_transactions__c
        WHERE Supplier__c IN :customerIdSet 
        AND (Unapplied__c >= 1 OR  Unapplied__c <= -1 )
        GROUP BY Supplier__c,Type__c];

        for (AggregateResult bankTransaction : bankTransactionAggregate) {
          //  System.debug('Supplier: '+(Id)bankTransaction.get('Supplier__c'));
            if (bankTransactionResult.containsKey((Id)bankTransaction.get('Supplier__c'))){
                bankTransactionList = bankTransactionResult.get((Id)bankTransaction.get('Supplier__c'));
                DMN_Statement.AgingPeriod bankTransactionType = new DMN_Statement.AgingPeriod((String)bankTransaction.get('Type__c'),(Decimal)bankTransaction.get('UnappliedAmt'));
                bankTransactionList.add(bankTransactionType);
                bankTransactionResult.put((Id)bankTransaction.get('Supplier__c'),bankTransactionList);
            } else {
                DMN_Statement.AgingPeriod bankTransactionType = new DMN_Statement.AgingPeriod((String)bankTransaction.get('Type__c'),(Decimal)bankTransaction.get('UnappliedAmt'));
                bankTransactionList.add(bankTransactionType);
                bankTransactionResult.put((Id)bankTransaction.get('Supplier__c'),bankTransactionList);
            }
        }

        return bankTransactionResult;
    }
}