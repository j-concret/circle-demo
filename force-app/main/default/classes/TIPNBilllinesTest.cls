@isTest
public class TIPNBilllinesTest  implements HttpCalloutMock{
    private static HttpResponse resp;
    
    public TIPNBilllinesTest(String testBody,Boolean isTest) {
        resp = new HttpResponse();
        resp.setBody(testBody);
        
        if(isTest == true){
            resp.setStatus('OK');
            resp.setStatusCode(200);
            resp.setBody('VERIFIED');
        }else{
            resp.setStatus('Error');
            resp.setStatusCode(404);
        }
    }
    
    public HTTPResponse respond(HTTPRequest req) {
        return resp;
    }
    @isTest
    static void testHttpPost() {
        // prepare test-data
        String testBody = 'account_identifier=3781&amount_submitted=100.00&c_date=09/20/2020 14:25:09.277&invoicenumber=a681v000000kWvaAAE&key=d6dce851eb1b4eee81186a02a2e28bbe&type=abc&ref_code=xyz&seq_ref_code=xyz&payee_id=xyz&amount_submitted=xyz&currency_submitted=xyz&payment_method=xyz&payee_fees=xyz&payee_fees_currency=xyz';
        //As Per Best Practice it is important to instantiate the Rest Context
        Tipalti_Payment_Submission__c tps = new Tipalti_Payment_Submission__c(
            Amount__c = 100 , Invoiced_Ids__c = 'a681v000000kWvaAAE');
        insert tps;
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/GetIPNStatus'; //Request URL
        req.httpMethod = 'POST';
        String postData = 'RestRequest:[headers={CipherSuite=ECDHE-RSA-AES256-SHA384 TLSv1.2 443, Connection=keep-alive, Content-Type=application/x-www-form-urlencoded, Expect=100-continue, Host=account2-testxyz.cs129.force.com, X-Salesforce-Forwarded-To=cs129.salesforce.com, X-Salesforce-SIP=35.174.65.196, X-Salesforce-SNI=PRESENT, X-Salesforce-VIP=FORCE}, httpMethod=POST, params={account_identifier=3781, amount_submitted=100.00, c_date=09/11/2020 09:26:11.574, currency_submitted=USD, invoicenumber=a681v000000LRhOAAW, is_finalized=true, key=d6dce851eb1b4eee81186a02a2e28bbe, payee_fees=5.00, payee_fees_currency=USD, payee_id=0010Y00000qXM1vQAG}, remoteAddress=35.174.65.196, requestBody=Blob[0], requestURI=/GetIPNStatus, resourcePath=/services/apexrest/GetIPNStatus/*]';
        String JsonMsg=JSON.serialize(postData);
        //String str = '{\"invoicenumber\": \"a681v000000LRhOAAW\",\"key\": \"d6dce851eb1b4eee81186a02a2e28bbe\"}';
        //req.requestBody= Blob.valueOf(str);
        //  req.requestBody = Blob.valueof(postData);
        RestContext.request = req;
        RestContext.response= res;
        /*Map<string,string> ipnparam = new Map<string,string>{'c_date'=>'09/20/2020 14:25:09.277','account_identifier'=>'3781', 'amount_submitted'=>'100.00', 'c_date'=>'09/11/2020 09:26:11.574', 'currency_submitted'=>'USD', 'invoicenumber'=>'a681v000000LRhOAAW', 'is_finalized'=>'true', 'key'=>'d6dce851eb1b4eee81186a02a2e28bbe', 'payee_fees'=>'5.00', 'payee_fees_currency'=>'USD', 'payee_id'=>'0010Y00000qXM1vQAG'};//
String JsonMsg1=JSON.serialize(ipnparam);
req.requestBody = Blob.valueof(JsonMsg1);*/
        req.addParameter('c_date', '09/20/2020 14:25:09.277');
        req.addParameter('key', 'd6dce851eb1b4eee81186a02a2e28bbe');
        req.addParameter('invoicenumber', 'a681v000000LRhOAAW');
        req.addParameter('submit_date', '09/20/2020 14:25:09.277');
        req.addParameter('transaction_date', '09/20/2020 14:25:09.277');
        req.addParameter('type', 'payment_submitted');
        req.addParameter('ref_code', 'ref');
        req.addParameter('seq_ref_code', 'ref');
        req.addParameter('payee_id', '123');
        req.addParameter('amount_submitted', '123');
        req.addParameter('currency_submitted', 'USD');
        req.addParameter('payment_method', 'xyz');
        req.addParameter('payee_fees', '123');
        req.addParameter('type', 'payment_submitted');
        req.addParameter('payee_fees_currency', 'USD');
        req.addParameter('is_finalized', 'TRUE');
        req.addParameter('provider', 'ABC');
        req.addParameter('account_identifier', 'ABC');
        req.addParameter('payer_exchange_rate', '1.2');
        req.addParameter('payee_fees_currency', 'USD');
        
  
        Test.startTest();
        HttpCalloutMock mock = new TIPNBilllinesTest(testBody,true);
        Test.setMock(HttpCalloutMock.class, mock);
        TIPNBilllines.TIPNBilllines();//null;//HttpClass.updateCustomObject();
        
        Test.stopTest();
        
        //System.assertEquals('expected value', actual, 'Value is incorrect');
    }
    @isTest
    static void testHttpCompletedPost() {
        // prepare test-data
        String testBody = 'account_identifier=3781&amount_submitted=100.00&c_date=09/20/2020 14:25:09.277&invoicenumber=a681v000000kWvaAAE&key=d6dce851eb1b4eee81186a02a2e28bbe&type=abc&ref_code=xyz&seq_ref_code=xyz&payee_id=xyz&amount_submitted=xyz&currency_submitted=xyz&payment_method=xyz&payee_fees=xyz&payee_fees_currency=xyz';
        //As Per Best Practice it is important to instantiate the Rest Context
        Tipalti_Payment_Submission__c tps = new Tipalti_Payment_Submission__c(
            Amount__c = 100 , Invoiced_Ids__c = 'a681v000000kWvaAAE');
        insert tps;
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/GetIPNStatus'; //Request URL
        req.httpMethod = 'POST';
        String postData = 'RestRequest:[headers={CipherSuite=ECDHE-RSA-AES256-SHA384 TLSv1.2 443, Connection=keep-alive, Content-Type=application/x-www-form-urlencoded, Expect=100-continue, Host=account2-testxyz.cs129.force.com, X-Salesforce-Forwarded-To=cs129.salesforce.com, X-Salesforce-SIP=35.174.65.196, X-Salesforce-SNI=PRESENT, X-Salesforce-VIP=FORCE}, httpMethod=POST, params={account_identifier=3781, amount_submitted=100.00, c_date=09/11/2020 09:26:11.574, currency_submitted=USD, invoicenumber=a681v000000LRhOAAW, is_finalized=true, key=d6dce851eb1b4eee81186a02a2e28bbe, payee_fees=5.00, payee_fees_currency=USD, payee_id=0010Y00000qXM1vQAG}, remoteAddress=35.174.65.196, requestBody=Blob[0], requestURI=/GetIPNStatus, resourcePath=/services/apexrest/GetIPNStatus/*]';
        String JsonMsg=JSON.serialize(postData);
        //String str = '{\"invoicenumber\": \"a681v000000LRhOAAW\",\"key\": \"d6dce851eb1b4eee81186a02a2e28bbe\"}';
        //req.requestBody= Blob.valueOf(str);
        //  req.requestBody = Blob.valueof(postData);
        RestContext.request = req;
        RestContext.response= res;
        /*Map<string,string> ipnparam = new Map<string,string>{'c_date'=>'09/20/2020 14:25:09.277','account_identifier'=>'3781', 'amount_submitted'=>'100.00', 'c_date'=>'09/11/2020 09:26:11.574', 'currency_submitted'=>'USD', 'invoicenumber'=>'a681v000000LRhOAAW', 'is_finalized'=>'true', 'key'=>'d6dce851eb1b4eee81186a02a2e28bbe', 'payee_fees'=>'5.00', 'payee_fees_currency'=>'USD', 'payee_id'=>'0010Y00000qXM1vQAG'};//
String JsonMsg1=JSON.serialize(ipnparam);
req.requestBody = Blob.valueof(JsonMsg1);*/
        req.addParameter('c_date', '09/20/2020 14:25:09.277');
        req.addParameter('key', 'd6dce851eb1b4eee81186a02a2e28bbe');
        req.addParameter('invoicenumber', 'a681v000000LRhOAAW');
        req.addParameter('submit_date', '09/20/2020 14:25:09.277');
        req.addParameter('transaction_date', '09/20/2020 14:25:09.277');
        req.addParameter('value_date', '09/20/2020 14:25:09.277');
        req.addParameter('type', 'completed');
        req.addParameter('ref_code', 'ref');
        req.addParameter('seq_ref_code', 'ref');
        req.addParameter('payee_id', 'completed');
        req.addParameter('amount_submitted', '123');
        req.addParameter('currency_submitted', 'USD');
        req.addParameter('payment_method', 'echeck');
        req.addParameter('payer_fees', '123');
        req.addParameter('payee_fees', '123');
        req.addParameter('provider', 'xyz');
        req.addParameter('account_identifier', 'abc');
        req.addParameter('payer_exchange_rate', '1.2');
        req.addParameter('payment_amount_in_withdraw_currency', '123');
        req.addParameter('payment_amount', '123');
        req.addParameter('transaction_ref', 'ref');
        req.addParameter('payment_method_token', '123');
        req.addParameter('bank_beneficiary_name', 'ABC');
        req.addParameter('iban', '123');
        req.addParameter('swift', '123');
        req.addParameter('bank_code', '123');
        req.addParameter('bank_name', '123');
        req.addParameter('routing_number', '123');
        req.addParameter('account_number', '123');
        req.addParameter('ach_account_type', 'ref');
        req.addParameter('withdraw_amount', '123');
        req.addParameter('withdraw_currency', 'USD');
        req.addParameter('bank_code', '123');
        req.addParameter('submitted_currency_to_payee_currency_fx_rate', '123');
        //
        
        Test.startTest();
        HttpCalloutMock mock = new TIPNBilllinesTest(testBody,true);
        Test.setMock(HttpCalloutMock.class, mock);
        TIPNBilllines.TIPNBilllines();//null;//HttpClass.updateCustomObject();
        
        Test.stopTest();
        
        //System.assertEquals('expected value', actual, 'Value is incorrect');
    }
    @isTest
    static void testHttpPayeeChangedPost() {
        // prepare test-data
        String testBody = 'account_identifier=3781&amount_submitted=100.00&c_date=09/20/2020 14:25:09.277&invoicenumber=a681v000000kWvaAAE&key=d6dce851eb1b4eee81186a02a2e28bbe&type=abc&ref_code=xyz&seq_ref_code=xyz&payee_id=xyz&amount_submitted=xyz&currency_submitted=xyz&payment_method=xyz&payee_fees=xyz&payee_fees_currency=xyz';
        //As Per Best Practice it is important to instantiate the Rest Context
        Tipalti_Payment_Submission__c tps = new Tipalti_Payment_Submission__c(
            Amount__c = 100 , Invoiced_Ids__c = 'a681v000000kWvaAAE');
        insert tps;
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/GetIPNStatus'; //Request URL
        req.httpMethod = 'POST';
        String postData = 'RestRequest:[headers={CipherSuite=ECDHE-RSA-AES256-SHA384 TLSv1.2 443, Connection=keep-alive, Content-Type=application/x-www-form-urlencoded, Expect=100-continue, Host=account2-testxyz.cs129.force.com, X-Salesforce-Forwarded-To=cs129.salesforce.com, X-Salesforce-SIP=35.174.65.196, X-Salesforce-SNI=PRESENT, X-Salesforce-VIP=FORCE}, httpMethod=POST, params={account_identifier=3781, amount_submitted=100.00, c_date=09/11/2020 09:26:11.574, currency_submitted=USD, invoicenumber=a681v000000LRhOAAW, is_finalized=true, key=d6dce851eb1b4eee81186a02a2e28bbe, payee_fees=5.00, payee_fees_currency=USD, payee_id=0010Y00000qXM1vQAG}, remoteAddress=35.174.65.196, requestBody=Blob[0], requestURI=/GetIPNStatus, resourcePath=/services/apexrest/GetIPNStatus/*]';
        String JsonMsg=JSON.serialize(postData);
        //String str = '{\"invoicenumber\": \"a681v000000LRhOAAW\",\"key\": \"d6dce851eb1b4eee81186a02a2e28bbe\"}';
        //req.requestBody= Blob.valueOf(str);
        //  req.requestBody = Blob.valueof(postData);
        RestContext.request = req;
        RestContext.response= res;
        /*Map<string,string> ipnparam = new Map<string,string>{'c_date'=>'09/20/2020 14:25:09.277','account_identifier'=>'3781', 'amount_submitted'=>'100.00', 'c_date'=>'09/11/2020 09:26:11.574', 'currency_submitted'=>'USD', 'invoicenumber'=>'a681v000000LRhOAAW', 'is_finalized'=>'true', 'key'=>'d6dce851eb1b4eee81186a02a2e28bbe', 'payee_fees'=>'5.00', 'payee_fees_currency'=>'USD', 'payee_id'=>'0010Y00000qXM1vQAG'};//
String JsonMsg1=JSON.serialize(ipnparam);
req.requestBody = Blob.valueof(JsonMsg1);*/
        req.addParameter('c_date', '09/20/2020 14:25:09.277');
        req.addParameter('key', 'd6dce851eb1b4eee81186a02a2e28bbe');
        req.addParameter('payee_id', '0010Y00000qXM1vQAG');
        req.addParameter('type', 'payee_details_changed');
        
        Test.startTest();
        HttpCalloutMock mock = new TIPNBilllinesTest(testBody,true);
        Test.setMock(HttpCalloutMock.class, mock);
        TIPNBilllines.TIPNBilllines();//null;//HttpClass.updateCustomObject();
        
        Test.stopTest();
        
        //System.assertEquals('expected value', actual, 'Value is incorrect');
    }
    
       
    @isTest
    static void testHttperrorPost() {
        // prepare test-data
        String testBody = 'account_identifier=3781&amount_submitted=100.00&c_date=09/20/2020 14:25:09.277&invoicenumber=a681v000000LRhOAAW&key=d6dce851eb1b4eee81186a02a2e28bbe&type=abc&ref_code=xyz&seq_ref_code=xyz&payee_id=xyz&amount_submitted=xyz&currency_submitted=xyz&payment_method=xyz&payee_fees=xyz&payee_fees_currency=xyz';
        //As Per Best Practice it is important to instantiate the Rest Context
        Tipalti_Payment_Submission__c tps = new Tipalti_Payment_Submission__c(
            Amount__c = 100 , Invoiced_Ids__c = 'a681v000000LRhOAAW');
        insert tps;
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/GetIPNStatus'; //Request URL
        req.httpMethod = 'POST';
        String postData = 'RestRequest:[headers={CipherSuite=ECDHE-RSA-AES256-SHA384 TLSv1.2 443, Connection=keep-alive, Content-Type=application/x-www-form-urlencoded, Expect=100-continue, Host=account2-testxyz.cs129.force.com, X-Salesforce-Forwarded-To=cs129.salesforce.com, X-Salesforce-SIP=35.174.65.196, X-Salesforce-SNI=PRESENT, X-Salesforce-VIP=FORCE}, httpMethod=POST, params={account_identifier=3781, amount_submitted=100.00, c_date=09/11/2020 09:26:11.574, currency_submitted=USD, invoicenumber=a681v000000LRhOAAW, is_finalized=true, key=d6dce851eb1b4eee81186a02a2e28bbe, payee_fees=5.00, payee_fees_currency=USD, payee_id=0010Y00000qXM1vQAG}, remoteAddress=35.174.65.196, requestBody=Blob[0], requestURI=/GetIPNStatus, resourcePath=/services/apexrest/GetIPNStatus/*]';
        String JsonMsg=JSON.serialize(postData);
        //String str = '{\"invoicenumber\": \"a681v000000LRhOAAW\",\"key\": \"d6dce851eb1b4eee81186a02a2e28bbe\"}';
        //req.requestBody= Blob.valueOf(str);
        //  req.requestBody = Blob.valueof(postData);
        RestContext.request = req;
        RestContext.response= res;
        /*Map<string,string> ipnparam = new Map<string,string>{'c_date'=>'09/20/2020 14:25:09.277','account_identifier'=>'3781', 'amount_submitted'=>'100.00', 'c_date'=>'09/11/2020 09:26:11.574', 'currency_submitted'=>'USD', 'invoicenumber'=>'a681v000000LRhOAAW', 'is_finalized'=>'true', 'key'=>'d6dce851eb1b4eee81186a02a2e28bbe', 'payee_fees'=>'5.00', 'payee_fees_currency'=>'USD', 'payee_id'=>'0010Y00000qXM1vQAG'};//
String JsonMsg1=JSON.serialize(ipnparam);
req.requestBody = Blob.valueof(JsonMsg1);*/
        req.addParameter('c_date', '09/20/2020 14:25:09.277');
        req.addParameter('key', 'd6dce851eb1b4eee81186a02a2e28bbe');
        req.addParameter('invoicenumber', 'a681v000000LRhOAAW');
        req.addParameter('submit_date', '09/20/2020 14:25:09.277');
        req.addParameter('error_date', '09/20/2020 14:25:09.277');
        req.addParameter('type', 'error');
        
            req.addParameter('error', 'klasdjkl');
        req.addParameter('error_code', 'klasdjkl');
        req.addParameter('amount_submitted', '123');
        req.addParameter('payee_id', '13');
        req.addParameter('ref_code', 'ref');
        req.addParameter('seq_ref_code', 'ref');
        req.addParameter('currency_submitted', 'USD');
        req.addParameter('payer_fees', '1');
        
        Test.startTest();
        HttpCalloutMock mock = new TIPNBilllinesTest(testBody,true);
        Test.setMock(HttpCalloutMock.class, mock);
        TIPNBilllines.TIPNBilllines();//null;//HttpClass.updateCustomObject();
        
        Test.stopTest();
        
        //System.assertEquals('expected value', actual, 'Value is incorrect');
    }
    @isTest
    static void testHttpPost1() {
        // prepare test-data
        String testBody = 'account_identifier=3781&amount_submitted=100.00&invoicenumber=a681v000000kWvaAAE&key=d6dce851eb1b4eee81186a02a2e28bbe';
        //As Per Best Practice it is important to instantiate the Rest Context
        /* Tipalti_Payment_Submission__c tps = new Tipalti_Payment_Submission__c(
Amount__c = 100 , Invoiced_Ids__c = 'a681v000000LRhOAAW');
insert tps;*/
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/GetIPNStatus'; //Request URL
        req.httpMethod = 'POST';
        String postData = 'RestRequest:[headers={CipherSuite=ECDHE-RSA-AES256-SHA384 TLSv1.2 443, Connection=keep-alive, Content-Type=application/x-www-form-urlencoded, Expect=100-continue, Host=account2-testxyz.cs129.force.com, X-Salesforce-Forwarded-To=cs129.salesforce.com, X-Salesforce-SIP=35.174.65.196, X-Salesforce-SNI=PRESENT, X-Salesforce-VIP=FORCE}, httpMethod=POST, params={account_identifier=3781, amount_submitted=100.00, c_date=09/11/2020 09:26:11.574, currency_submitted=USD, invoicenumber=a681v000000LRhOAAW, is_finalized=true, key=d6dce851eb1b4eee81186a02a2e28bbe, payee_fees=5.00, payee_fees_currency=USD, payee_id=0010Y00000qXM1vQAG}, remoteAddress=35.174.65.196, requestBody=Blob[0], requestURI=/GetIPNStatus, resourcePath=/services/apexrest/GetIPNStatus/*]';
        String JsonMsg=JSON.serialize(postData);
        //String str = '{\"invoicenumber\": \"a681v000000LRhOAAW\",\"key\": \"d6dce851eb1b4eee81186a02a2e28bbe\"}';
        //req.requestBody= Blob.valueOf(str);
        req.requestBody = Blob.valueof(postData);
        RestContext.request = req;
        RestContext.response= res;
        Map<string,string> ipnparam = new Map<string,string>{'account_identifier'=>'3781', 'amount_submitted'=>'100.00', 'c_date'=>'09/11/2020 09:26:11.574', 'currency_submitted'=>'USD', 'invoicenumber'=>'a681v000000LRhOAAW', 'is_finalized'=>'true', 'key'=>'d6dce851eb1b4eee81186a02a2e28bbe', 'payee_fees'=>'5.00', 'payee_fees_currency'=>'USD', 'payee_id'=>'0010Y00000qXM1vQAG'};//
            Test.startTest();
        HttpCalloutMock mock = new TIPNBilllinesTest(testBody,true);
        Test.setMock(HttpCalloutMock.class, mock);
        TIPNBilllines.TIPNBilllines();//null;//HttpClass.updateCustomObject();
        
        Test.stopTest();
        
        //System.assertEquals('expected value', actual, 'Value is incorrect');
    }
}