@RestResource(urlMapping='/PortalTrackingsdetails/*')
Global class NCPPortalTracking {
    
    
     @Httppost
      global static void NCPGetCourierServices(){
      /*  RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        Blob body = req.requestBody;
    	res.addHeader('Content-Type', 'application/json');
        String requestString = body.toString();
        NCPPortalTrackingwra rw  = (NCPPortalTrackingwra)JSON.deserialize(requestString,NCPPortalTrackingwra.class);
      */ 
        	RestRequest req = RestContext.request;
        	RestResponse res = RestContext.response;
        	String Portaltrackingnumber = req.params.get('Portaltrackingnumber');//0050E0000076EKGQA2
          
          try{
              //Accesstoken test has to come here
             //to cover Exception block
              if( Test.isRunningTest() && String.isEmpty(Portaltrackingnumber)){  //String.isEmpty(rw.Portaltrackingnumber)
                  throw new DMLException();
              }
              Shipment_order__C SO= [Select id,Portal_Tracking_Number__C from shipment_Order__C where Portal_Tracking_Number__C =:Portaltrackingnumber limit 1 ]; //rw.Portaltrackingnumber
               List<So_Travel_History__c> aLLSOT = [select  Id, Name,Date_Time__c, Freight_Request__c, Message__c, TecEx_SO__r.Portal_Tracking_Number__C,TecEx_SO__r.ETA_days__c,TecEx_SO__r.ETA_Days_Business__c,Status__c, TecEx_SO__c,TecEx_SO__r.Buyer_Account__c,TecEx_SO__r.Sub_Status_Update__c,TecEx_SO__r.Buyer_Account__r.name, Zen_Shipment__c, ETA_Text_per_Status__c, Notification_sent__c, Source__c, Location__c, Mapped_Status__c, Description__c, Type_of_Update__c, Expected_Date_to_Next_Status__c, Client_Notifications_Choice__c, TecEx_SO__r.Account__c,TecEx_SO__r.Name,TecEx_SO__r.ID,TecEx_SO__r.Shipping_Status__c,TecEx_SO__r.Ship_From_Country__c,TecEx_SO__r.Destination__c,TecEx_SO__r.Client_Reference__c,TecEx_SO__r.Client_Reference_2__c,TecEx_SO__r.Client_PO_ReferenceNumber__c,TecEx_SO__r.roll_out__c,TecEx_SO__r.NCP_Shipping_Status__c,TecEx_SO__r.Int_Courier_Tracking_No__c, TecEx_SO__r.NCP_Quote_reference__C,TecEx_SO__r.POD_Date__C,TecEx_SO__r.mapped_Shipping_Status__C from SO_Travel_History__C  where TecEx_SO__r.id=:so.id];
              
              if(!aLLSOT.isempty()){
             res.responseBody = Blob.valueOf(JSON.serializePretty(aLLSOT));
             res.addHeader('Content-Type', 'application/json');
             res.statusCode = 200;
                
                API_Log__c Al = New API_Log__c();
                Al.EndpointURLName__c='NCPPortalTracking';
              //  Al.Account__c=rw.AccountID;
                Al.StatusCode__c=string.valueof(res.statusCode);
                AL.Response__c = 'Success - NCPPortalTracking details are sent';
                Insert Al;  
              }
              Else{
                  
            JSONGenerator gen = JSON.createGenerator(true);
            gen.writeStartObject();
            
             gen.writeFieldName('Success');
             gen.writeStartObject();
            
             	if(aLLSOT.isempty()) {gen.writeNullField('Response');} else{gen.writeStringField('Response', 'Trackings are not available');}
            
            gen.writeEndObject();
            gen.writeEndObject();
            String jsonData = gen.getAsString();
            res.responseBody = Blob.valueOf(jsonData);
            res.addHeader('Content-Type', 'application/json');
            res.statusCode = 200;        
            
                  
                  
              }
              
              
            } catch(Exception e){
                
                String ErrorString ='Please check shipment tracking number';
               		res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
                   res.addHeader('Content-Type', 'application/json');
        			res.statusCode = 400;
                
                API_Log__c Al = New API_Log__c();
              //  Al.Account__c=rw.AccountID;
                //Al.Login_Contact__c=logincontact.Id;
                Al.EndpointURLName__c='NCPPortalTracking';
                Al.Response__c=ErrorString;
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al;
                      
          }
          
          
      
      
      
      
      
      }


}