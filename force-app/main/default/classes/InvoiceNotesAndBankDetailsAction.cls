public class InvoiceNotesAndBankDetailsAction {
    @InvocableMethod
    public static void doUpdate(List<ID> ids) {
         fw1__Payment_Center_Setting__c settings = getDefaultSetting();
         List< fw1__Invoice__c > invs = [
             SELECT Name,US_Bank_Details__c,EU_Bank_Details__c,
                 Notes__c,Euro_Conversion_Rate__c 
             FROM fw1__Invoice__c WHERE Id in :ids];
             
         for (fw1__Invoice__c inv : invs) {
            inv.US_Bank_Details__c = settings.US_Bank_Details__c;
            inv.EU_Bank_Details__c = settings.EU_Bank_Details__c;
            inv.Notes__c = settings.Notes__c;
            inv.Euro_Conversion_Rate__c = settings.Euro_Conversion_Rate__c;
         }
         
         update invs;
    }
    
    private static fw1__Payment_Center_Setting__c getDefaultSetting(){
        return [
            SELECT US_Bank_Details__c,EU_Bank_Details__c,Notes__c,Euro_Conversion_Rate__c
            FROM fw1__Payment_Center_Setting__c
            WHERE Name = 'Default Settings'
            LIMIT 1];
    }

}