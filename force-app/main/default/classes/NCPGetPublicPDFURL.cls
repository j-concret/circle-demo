@RestResource(urlMapping='/GetPublicPDFURL/*')
global class NCPGetPublicPDFURL {
    @Httppost
    global static void NCPGetPublicPDFURL(){
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        Blob body = req.requestBody;
        res.addHeader('Content-Type', 'application/json');
        String requestString = body.toString();
        NCPGetPublicPDFURLWra rw  = (NCPGetPublicPDFURLWra)JSON.deserialize(requestString,NCPGetPublicPDFURLWra.class);
        ContentDistribution cd = new ContentDistribution();
        ContentDistribution FinalConDis = new ContentDistribution();
        ContentVersion contentVersionRecord = new ContentVersion() ;
          list<attachment> atts= new list<attachment>();
        Try{
            
            Access_token__c At =[select status__c,Access_Token__c from Access_token__c where Access_Token__c=:rw.Accesstoken ];
            if( at.status__C =='Active' ){
                if(rw.recordId != null && (!(rw.recordId).startsWith('069') || (rw.recordId).startsWith('a68') )  ){
                 //  list<attachment> atts= new list<attachment>();
                   if((rw.recordId).startsWith('a68')){  
                      atts= [select ID,name,Body,description,parentID,createddate from attachment where parentID =:rw.recordId order by createddate desc limit 1];
                   
                   }
               		else{
                   	atts= [select ID,name,Body,description,parentID,createddate from attachment where Id =:rw.recordId order by createddate desc limit 1];
               		}
                   
                        
                                    ContentVersion[] contentVersions = new List<ContentVersion>();
                                    for (integer i = 0; i < atts.size(); i++) {
                                        ContentVersion contentVersion = createContentVersion(atts[i].name, atts[i].Body);
                                        contentVersions.add(contentVersion);
                                    }
                                    insert contentVersions;
                                    
                                    List<ContentDocumentLink> contentDocumentLinks = new List<ContentDocumentLink>();
                                    
                                    list<contentVersion>  cvs= [SELECT Id, ContentDocumentId FROM ContentVersion WHERE Id IN :contentVersions];
                                    for(ContentVersion CV : cvs){
                                        ContentDocumentLink contentDocumentLink = createContentDocumentLink(CV.ContentDocumentId, atts[0].parentID);
                                        contentDocumentLinks.add(contentDocumentLink);
                                    }
                                    insert contentDocumentLinks;
                                    
                                    ContentDocumentLink cdl = [select contentdocument.id, contentdocument.title, contentdocument.filetype from contentdocumentlink where linkedentityid =: atts[0].parentID limit 1];
                                    contentVersionRecord = [select id,Title from contentversion where contentdocumentid = :cdl.contentdocument.id and IsLatest =TRUE];
                    
                    
                   
                    
                    
                }
                else if(rw.recordId != null && (rw.recordId).startsWith('069')){
                    
                    contentVersionRecord = [select id,Title from contentversion where  ContentDocumentId = : rw.recordId AND IsLatest = true];                    
                }
                
                if(contentVersionRecord != null && contentVersionRecord.Id != null){
                cd.Name = contentVersionRecord.Title; //'Test'; // Please have document title
                cd.ContentVersionId = contentVersionRecord.id;
                cd.PreferencesAllowViewInBrowser= true;
                cd.PreferencesLinkLatestVersion=true;
                cd.PreferencesNotifyOnVisit=false;
                cd.PreferencesPasswordRequired=false;
                cd.PreferencesAllowOriginalDownload= true;
                insert cd;
                ContentDistribution contentDist = [SELECT Id, CreatedDate, LastModifiedDate, Name,ContentVersionId, ContentDocumentId, DistributionPublicUrl, ContentDownloadUrl, PdfDownloadUrl FROM ContentDistribution where Id=:cd.Id order by createddate desc limit 1 ];         
                
                
                JSONGenerator gen = JSON.createGenerator(true);
                gen.writeStartObject();
                gen.writeFieldName('Success');
                gen.writeStartObject();
                
                if(contentDist.ContentDocumentId == null) {gen.writeNullField('ContentDocumentId');} else{gen.writeIdField('ContentDocumentId',+contentDist.ContentDocumentId);}
                if(contentDist.PdfDownloadUrl == null) {gen.writeNullField('PdfDownloadUrl');} else{gen.writeStringField('PdfDownloadUrl',+contentDist.PdfDownloadUrl);}
                
                gen.writeEndObject();
                gen.writeEndObject();
                String jsonData = gen.getAsString();
                res.responseBody = Blob.valueOf(jsonData);
                res.statusCode=200;  
                
                API_Log__c Al = New API_Log__c();
                Al.EndpointURLName__c='GetPublicPDFURL';
                Al.StatusCode__c=string.valueof(res.statusCode);
                Al.Response__c = 'Attachments are downloaded to --' +rw.recordID;
                Insert Al;
                }
                
                
                
                
            }
        } Catch(Exception e){
           if(atts.isempty() && rw.Accesstoken !=null){
                              JSONGenerator gen = JSON.createGenerator(true);
                                    gen.writeStartObject();
                                    gen.writeFieldName('Success');
                                    gen.writeStartObject();
                                    
                                    gen.writeIdField('Message','Attachments are not availble');
                                    
                                    gen.writeEndObject();
                                    gen.writeEndObject();
                                    String jsonData = gen.getAsString();
                                    res.responseBody = Blob.valueOf(jsonData);
                                          res.statusCode=200;  
                
            }
            else{
            
            String ErrorString ='Something went wrong, please contact support_SF@tecex.com';
            
            res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
            res.addHeader('Content-Type', 'application/json');
            res.statusCode = 400;
            
            API_Log__c Al = New API_Log__c();
            Al.EndpointURLName__c='GetPublicPDFURL';
            Al.Response__c=e.getMessage()+' at line number: '+e.getLineNumber();
            Al.StatusCode__c=string.valueof(res.statusCode);
            Insert Al;
            } 
            
            
        }
    }
    public static contentVersion createContentVersion(String name, Blob body){
        ContentVersion contentVersion = new ContentVersion();
        contentVersion.ContentLocation = 'S'; // S = Stored in Salesforce
        contentVersion.PathOnClient = name;
        contentVersion.Title = name;
        contentVersion.VersionData = body;//EncodingUtil.base64Decode(body);
        return contentVersion;
    }
    
    private static ContentDocumentLink createContentDocumentLink(Id contentDocumentId, Id parentId){
        ContentDocumentLink contentDocumentLink = new ContentDocumentLink();
        contentDocumentLink.ContentDocumentId = contentDocumentId;
        contentDocumentLink.LinkedEntityId = parentId;
        contentDocumentLink.ShareType = 'V'; // Inferred permission
        contentDocumentLink.Visibility = 'AllUsers';
        return contentDocumentLink;
    }
}