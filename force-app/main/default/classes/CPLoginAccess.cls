@RestResource(urlMapping='/LoginAuthentication/*')
Global class CPLoginAccess {
 
    //Get Request
    @HttpGet
    global static void getrecords(){
        
    	RestRequest req = RestContext.request;
    	RestResponse res = RestContext.response;
        res.addHeader('Content-Type', 'application/json');
        String Username = req.params.get('Username');
        String Password = req.params.get('Password');
       	          
        try {
        Contact logincontact = [select Id,Account.id,Account.Name,lastname,Email,Password__c,Contact_Status__c from Contact where UserName__c =:username and Contact_status__C = 'Active'];
             
            Boolean result = logincontact.Password__c.equals(Password); 
           
            if(result == true){
                    res.responseBody = Blob.valueOf(JSON.serializePretty(logincontact));
        			res.statusCode = 200;
                API_Log__c Al = New API_Log__c();
                Al.Account__c=logincontact.AccountID;
                Al.Login_Contact__c=logincontact.Id;
                Al.EndpointURLName__c='LoginAuthentication';
                Al.Response__c='Login success';
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al;
                     
            
              }
            else{
                    String ErrorString ='Authentication failed, Please check username and password';
               		res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
        			res.statusCode = 400;
                API_Log__c Al = New API_Log__c();
                Al.Account__c=logincontact.AccountId;
                Al.Login_Contact__c=logincontact.Id;
                Al.EndpointURLName__c='LoginAuthentication';
                Al.Response__c='Authentication Failed';
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al;
                
            	}
        
            }
            catch (Exception e) {
                    String ErrorString ='Authentication failed, Please check username and password';
               		res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
            res.statusCode = 500;
                API_Log__c Al = New API_Log__c();
                Al.EndpointURLName__c='LoginAuthentication';
                Al.Response__c=e.getMessage()+ ' - incorrect email - '+Username;
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al;
               
            } 

	}

}