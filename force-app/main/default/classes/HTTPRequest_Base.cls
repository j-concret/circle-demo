public abstract class HTTPRequest_Base {
  public static final String HTTP_VERB_POST = 'POST';
  public static final String HTTP_CONTENT_TYPE = 'Content-Type';
     
  public static final Integer HTTP_OK = 200;  
  public static final String HTTP_ENCODING_JSON = 'application/json';
  public static final String HTTP_ENCODING_XML = 'text/xml';
  public static final Integer HTTP_INTERAL_SERVER_ERROR = 503;  
  public static final String HTTP_SOAP_ACTION = 'SOAPAction';
  public static final String ERR_INTERNAL_SERVER = 'Internal Server Error';
  
  public static final String DEV_ORG_ID = '00Dc0000000BGVg'; // VATit full sandbox
  public static final String UAT_ORG_ID = '00Dc0000000BGVg'; // VATit full sandbox
  public static final String PROD_ORG_ID = '00D20000000KWvN'; // VATit production
  
  public static final Set<String> VALID_ORG_IDS = new Set<String> {DEV_ORG_ID, UAT_ORG_ID, PROD_ORG_ID};
  
  private static final Integer DEFAULT_TIMEOUT = 20000;//20 seconds
  
  public abstract String GetWebServiceEndPoint();

  public abstract String getEncoding();
  
  public abstract Integer getTimeout();
  
  public class HTTPRequestException extends Exception {}
    
  public abstract HttpResponse createAndSendRequest(String body);

  public HttpRequest createRequest(String body)
  {
    HttpRequest request = new HttpRequest();
    
    request.setMethod(HTTP_VERB_POST);
    request.setHeader(HTTP_CONTENT_TYPE, getEncoding());
    request.setTimeout(getTimeout());
    request.setEndpoint(GetWebServiceEndPoint());
    request.setBody(body);
    
    System.debug('Request: ' +  body);
    
    return request;
  }
   
  public HttpResponse sendRequest(HttpRequest request)
  {
    Http h = new Http();
    //al.StopWatch stopWatch =  new al.StopWatch();
    
    //stopWatch.start(); 
    System.debug('Sending Request: ' + request.getBody());
    
    Httpresponse response = h.send(request);
    
    //stopWatch.stop();
    
    //System.debug('Request took: ' + stopWatch.getTime() + ' milliseconds.');
    
    if (response.getStatusCode() != HTTP_OK)
      throw new HTTPRequestException(string.valueof(response.getStatusCode()) + ' - ' + response.getStatus());

    System.debug('Response: ' +  response.getBody());
    
    return response;
  }

}