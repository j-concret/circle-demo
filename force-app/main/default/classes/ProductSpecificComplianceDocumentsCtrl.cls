public class ProductSpecificComplianceDocumentsCtrl {
    private static final String base64Chars = '' +
                                              'ABCDEFGHIJKLMNOPQRSTUVWXYZ' +
                                              'abcdefghijklmnopqrstuvwxyz' +
                                              '0123456789+/';

    @AuraEnabled
    public static Map<String, Object> getPartRecords(Id shipmentOrderId,Integer offset,Integer pageSize){
        try {
            Map<String, Object> result = new Map<String, Object> { 'status' => 'OK' };
            pageSize = Integer.valueOf(pageSize);
            offset = (Integer.valueOf(offset))*pageSize;
            Map<String,Compliance_Document_Field_Rules__mdt> rules = new Map<String,Compliance_Document_Field_Rules__mdt>();
            Map<Id,Country_Compliance__c> productDocnameMap = new Map<Id,Country_Compliance__c>();
            Map<String,ComplianceWrapper> ComplianceWrapperMap = new Map<String,ComplianceWrapper>();
            Map<Id,List<AttachmentWrapper> > partSpecificAttachments = new Map<Id,List<AttachmentWrapper> >();
            List<Map<String,Object> > partList = new List<Map<String,Object> >();

            Shipment_Order__c shipmentOrder = [SELECT Id, Name, IOR_Price_List__c, Ship_to_Country__c, Ship_to_Country__r.Ship_to_Country__c,CPA_v2_0__c, CPA_v2_0__r.Name, CPA_v2_0__r.Required_Documents_Shipment_Specific__c,
                                               CPA_v2_0__r.CPA_Required_Documents_Product_Specific__c, IOR_Price_List__r.Name, CPA_v2_0__r.Country__c,CPA_v2_0__r.CPA_Required_Documents_Part_Specific__c,
                                               I_have_checked_the_HS_Codes__c,SupplierlU__c FROM Shipment_Order__c
                                               WHERE Id = :shipmentOrderId];
            Country__c country = [SELECT Name, Id, Country__c FROM Country__c WHERE Country__c='Worldwide' LIMIT 1];
            String cntry = shipmentOrder.CPA_v2_0__r.Country__c;
            String worldwide = country.Id;

            List<String> requiredDocs = shipmentOrder.CPA_v2_0__r.CPA_Required_Documents_Product_Specific__c != null ? shipmentOrder.CPA_v2_0__r.CPA_Required_Documents_Product_Specific__c.split(';') : new List<String>();
            if(shipmentOrder.CPA_v2_0__r.CPA_Required_Documents_Part_Specific__c != null) {
                requiredDocs.addAll(shipmentOrder.CPA_v2_0__r.CPA_Required_Documents_Part_Specific__c.split(';'));
            }
            Map<Id,Part__c> partMap = new Map<Id,Part__c>([SELECT Id, Name, US_HTS_Code__c, Description_and_Functionality__c,
                                                           US_HS_Code_and_Associated_Details__c,Documents_Not_Required__c,Product__c, Product__r.Name, Product__r.CCATs__c,
                                                           Product__r.Description, Product__r.Manufacturer__r.Name, Product__r.Category__c,
                                                           Product__r.Sub_Category__c, US_HS_Code_and_Associated_Details__r.Status__c, Product__r.MSRP__c,
                                                           Product__r.Regulatory_Number__c, Product__r.Wireless_capability__c, Product__r.Encryption__c,
                                                           Product__r.Product_Documents_Not_Required__c, Matched_HS_Code2__c, Matched_HS_Code2__r.Status__c,
                                                           Product__r.Manufacturer__c, Matched_HS_Code2__r.Name, Product__r.ECCN__c,
                                                           Product__r.US_HTS_Code__c, ECCN_NO__c FROM Part__c WHERE Shipment_Order__c = : shipmentOrderId order by CreatedDate LIMIT: pageSize OFFSET: offset]);

            Set<Id> productIds = new Set<Id>();
            for(Part__c part : partMap.values()) {
                productIds.add(part.Product__c);
            }
            for(Compliance_Document_Field_Rules__mdt rule : [SELECT Id, DeveloperName, MasterLabel, Label, Compliance_Type__c, Compliance_status_Required__c,
                                                             Compliance_status_Visible__c, Date_of_issue_Required__c, Date_of_issue_Visible__c, Expiry_Date__c,Expiry_Date_Required__c,Is_Certification_Number_Required__c,Expiry_Date_Visible__c,
                                                             Factory_Specific_Required__c, Factory_Specific_Visible__c, General_Comments_Required__c,
                                                             General_Comments_Visibile__c, Is_Document_Visible__c, Modal_Number_Required__c,
                                                             Modal_Number_Visibile__c, Shipment_Order_Specific__c, Nature_Use__c, IsActive__c, Is_Link_Visible__c,
                                                             Is_Certification_Number_Visible__c,CE_Marking_Visible__c,Compliance_Sub_status_Visible__c,Standards_Visible__c,Supplier_Visible__c FROM Compliance_Document_Field_Rules__mdt where IsActive__c=true]) {

                rules.put(rule.MasterLabel,rule);
            }


            List<Country_Compliance__c> countryCompliances = [SELECT Id, Name, Product__c,Country_Compliance__c,
                                                              Country_Compliance__r.Compliance_Document_Name__c, Country_Compliance__r.Compliance_status__c,Country_Compliance__r.Compliance_Sub_status__c,
                                                              Country_Compliance__r.Compliance_type__c,Country_Compliance__r.Model_Number__c,Country_Compliance__r.Supplier__c,
                                                              Country_Compliance__r.Link__c,Country_Compliance__r.Expiry__c,Country_Compliance__r.Country__c,
                                                              Country_Compliance__r.Certification_number__c FROM Country_Compliance__c
                                                              WHERE Product__c IN :productIds];

            for(Country_Compliance__c cc : countryCompliances) {
                if(cc.Country_Compliance__r.Compliance_Sub_status__c == 'Supplier has Document' && cc.Country_Compliance__r.Supplier__c != shipmentOrder.SupplierlU__c) continue;
                productDocnameMap.put(cc.Country_Compliance__c,cc);
                String key = cc.Product__c+'-'+cc.Country_Compliance__r.Compliance_Document_Name__c+'-'+cc.Country_Compliance__r.Country__c;
                if(!ComplianceWrapperMap.containsKey(Key) || (ComplianceWrapperMap.containsKey(Key) && cc.Country_Compliance__r.Compliance_status__c == 'Not Required'))
                    ComplianceWrapperMap.put(key,new ComplianceWrapper(cc));
            }
            Set<Id> linkedEntities = new Set<Id>(partMap.keySet());
            linkedEntities.addAll(productDocnameMap.keySet());

            if(!linkedEntities.isEmpty()) {
                for(ContentDocumentLink linDoc : [SELECT ContentDocument.Title,ContentDocument.FileType,ContentDocument.ContentSize,
                                                  ContentDocument.FileExtension,ContentDocumentId,LinkedEntityId,SystemModstamp
                                                  FROM ContentDocumentLink WHERE LinkedEntityId IN :linkedEntities]) {
                    AttachmentWrapper att = new AttachmentWrapper(linDoc.ContentDocumentId,
                                                                  linDoc.LinkedEntityId,
                                                                  linDoc.ContentDocument.Title+'.'+linDoc.ContentDocument.FileType.toLowerCase(),
                                                                  null,
                                                                  linDoc.ContentDocument.ContentSize,
                                                                  linDoc.SystemModstamp.date()
                                                                  );

                    if(partMap.containsKey(linDoc.LinkedEntityId)) {
                        if(partSpecificAttachments.containsKey(linDoc.LinkedEntityId))
                            partSpecificAttachments.get(linDoc.LinkedEntityId).add(att);
                        else
                            partSpecificAttachments.put(linDoc.LinkedEntityId,new List<AttachmentWrapper> {att});
                    }else if( productDocnameMap.containsKey(linDoc.LinkedEntityId)) {
                        Country_Compliance__c cc = productDocnameMap.get(linDoc.LinkedEntityId);
                        String key = cc.Product__c+'-'+cc.Country_Compliance__r.Compliance_Document_Name__c+'-'+cc.Country_Compliance__r.Country__c;
                        ComplianceWrapperMap.get(key).addAttachment(att);
                    }
                }
            }

            for(Part__c part : partMap.values()) {
                List<ComplianceWrapper> docs = new List<ComplianceWrapper>();
                System.debug('requiredDocs:: '+requiredDocs);
                for(String requiredDoc : requiredDocs) {
                    ComplianceWrapper cmpWrap;
                    Compliance_Document_Field_Rules__mdt rule = rules.get(requiredDoc);
                    String nature = (rule !=null && rule.Nature_Use__c !=null ? rule.Nature_Use__c : '');
                    System.debug('nature:: '+nature);
                    if(part.Product__c != null) {
                        List<String> notReqDocs = part.Product__r.Product_Documents_Not_Required__c != null ? part.Product__r.Product_Documents_Not_Required__c.split(';') : new List<String>();
                        String key = part.Product__c+'-'+requiredDoc+'-';
                        switch on nature {
                            when  'WW_Specific','WW_WW' {
                                if(ComplianceWrapperMap.containsKey(key+cntry)) {
                                    cmpWrap = ComplianceWrapperMap.get(key+cntry);
                                    cmpWrap.nature = nature;
                                    cmpWrap.pro_isNotRequired = notReqDocs.contains(requiredDoc);
                                    cmpWrap.isNotRequired = notReqDocs.contains(requiredDoc) ? true : cmpWrap.status =='Not Required';
                                    cmpWrap.availableIcon = cmpWrap.isNotRequired ? 'action:reject' :  cmpWrap.status =='Document Required, but Unavailable' || String.isBlank(cmpWrap.cId) ? 'action:close' : 'action:approval';
                                    cmpWrap.hasAttachment = String.isNotBlank(cmpWrap.cId) && !cmpWrap.isNotRequired && nature == 'WW_WW';
                                }else if(ComplianceWrapperMap.containsKey(key+worldwide)) {
                                    cmpWrap = ComplianceWrapperMap.get(key+worldwide);
                                    cmpWrap.nature = nature;
                                    cmpWrap.pro_isNotRequired = notReqDocs.contains(requiredDoc);
                                    cmpWrap.isNotRequired = notReqDocs.contains(requiredDoc) ? true : cmpWrap.status =='Not Required';
                                    cmpWrap.availableIcon = cmpWrap.isNotRequired ? 'action:reject' : cmpWrap.status =='Document Required, but Unavailable' || String.isBlank(cmpWrap.cId) ? 'action:close' : 'action:approval';
                                    cmpWrap.hasAttachment = String.isNotBlank(cmpWrap.cId) && !cmpWrap.isNotRequired && nature == 'WW_WW';
                                }else{
                                    cmpWrap = new ComplianceWrapper(requiredDoc,notReqDocs.contains(requiredDoc),nature);
                                }
                            }
                            when 'Specific_Specific' {
                                if(ComplianceWrapperMap.containsKey(key+cntry)) {
                                    cmpWrap = ComplianceWrapperMap.get(key+cntry);
                                    cmpWrap.nature = nature;
                                    cmpWrap.pro_isNotRequired = notReqDocs.contains(requiredDoc);
                                    cmpWrap.isNotRequired = notReqDocs.contains(requiredDoc) ? true : cmpWrap.status =='Not Required';
                                    cmpWrap.availableIcon = cmpWrap.isNotRequired ? 'action:reject' : cmpWrap.status =='Document Required, but Unavailable' || String.isBlank(cmpWrap.cId) ? 'action:close' : 'action:approval';
                                    cmpWrap.hasAttachment = String.isNotBlank(cmpWrap.cId) && !cmpWrap.isNotRequired;
                                }else{
                                    cmpWrap = new ComplianceWrapper(requiredDoc,notReqDocs.contains(requiredDoc),nature);
                                }
                            }
                            when 'Part_Specific' {
                                String docNotReq = '';
                                if(part.Documents_Not_Required__c != null) docNotReq = part.Documents_Not_Required__c;     //because of part.Documents_Not_Required__c read only
                                List<String> partNotReqDocs = docNotReq.split(';');
                                Boolean isDocNotReq = notReqDocs.contains(requiredDoc) ? true : partNotReqDocs.contains(requiredDoc);
                                cmpWrap = new ComplianceWrapper(requiredDoc,isDocNotReq,nature);
                                cmpWrap.pro_isNotRequired = notReqDocs.contains(requiredDoc);
                                if(partSpecificAttachments.containsKey(part.Id))
                                    cmpWrap.attachment = partSpecificAttachments.get(part.Id);
                                cmpWrap.availableIcon = (cmpWrap.isNotRequired ? 'action:reject' : (cmpWrap.attachment.isEmpty() ? 'action:close' : 'action:approval'));
                            }
                            when else {
                                cmpWrap = new ComplianceWrapper(requiredDoc,notReqDocs.contains(requiredDoc),nature);
                            }
                        }
                    }else{
                        if(rule != null && rule.Nature_Use__c != null && rule.Nature_Use__c == 'Part_Specific') {
                            String docNotReq = '';
                            if(part.Documents_Not_Required__c != null) docNotReq = part.Documents_Not_Required__c; //because of part.Documents_Not_Required__c read only
                            List<String> notReqDocs = docNotReq.split(';');
                            cmpWrap = new ComplianceWrapper(requiredDoc,notReqDocs.contains(requiredDoc),rule.Nature_Use__c);
                            if(partSpecificAttachments.containsKey(part.Id))
                                cmpWrap.attachment = partSpecificAttachments.get(part.Id);
                        }else if(rule != null) {
                            cmpWrap = new ComplianceWrapper(requiredDoc,false,rule.Nature_Use__c);
                        }
                    }
                    docs.add(cmpWrap);
                }
                partList.add(new Map<String,object> {
                    'isExpanded'=>false,
                    'Id'=>part.Id,
                    'productId'=>part.Product__c,
                    'Product__r'=>part.Product__r,
                    'Matched_HS_Code2__r'=>part.Matched_HS_Code2__r,
                    'productName'=>part.Product__r.Name,
                    'matched_hs_code'=>part.Matched_HS_Code2__c,
                    'name'=>part.Name,
                    'US_HTS_Code'=>part.US_HTS_Code__c,
                    'ECCN_NO'=>part.ECCN_NO__c,
                    'totalColumns'=>10,
                    'requiredDocs'=>docs,
                    'DNR'=>part.Documents_Not_Required__c
                });
            }
            result.put('parts',partList);
            result.put('complianceFieldRules', rules);
            result.put('worldwide',worldwide);
            return result;
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    @AuraEnabled
    public static Map<String, Object> getAllPartsDetail(Id shipmentOrderId,Integer offset,Integer pageSize){
        Map<String, Object> result;
        try{
            result = getPartRecords(shipmentOrderId,offset,pageSize);
            if(String.valueOf(result.get('status')) != 'OK') return result;

            Map<String,List<String> > catWithSubCatValues = new Map<String,List<String> >();
            List<Schema.PicklistEntry> categoryValues = SObjectService.getPickListValues('Product2', 'Category__c');
            List<Schema.PicklistEntry> subCategoryValues = SObjectService.getPickListValues('Product2', 'Sub_Category__c');
            List<Schema.PicklistEntry> wirelessValues = SObjectService.getPickListValues('Product2', 'Wireless_capability__c');
            List<Schema.PicklistEntry> encryptionValues = SObjectService.getPickListValues('Product2', 'Encryption__c');
            List<Schema.PicklistEntry> matchedStatusValues = SObjectService.getPickListValues('HS_Codes_and_Associated_Details__c', 'Status__c');
            List<PicklistEntryWrapper> depEntries = (List<PicklistEntryWrapper>)JSON.deserialize(JSON.serialize(subCategoryValues), List<PicklistEntryWrapper>.class);
            List<String> controllingValues = new List<String>();
            List<String> wirelessPickValues = new List<String>();
            List<String> encryptionPickValues = new List<String>();
            List<String> matchedHSStatusPickValues = new List<String>();

            for (Schema.PicklistEntry ple : categoryValues) {
                String label = ple.getValue();
                catWithSubCatValues.put(label, new List<String>());
                controllingValues.add(label);
            }

            for (PicklistEntryWrapper plew : depEntries) {
                String label = plew.value;
                String validForBits = base64ToBits(plew.validFor);
                for (Integer i = 0; i < validForBits.length(); i++) {
                    String bit = validForBits.mid(i, 1);
                    if (bit == '1') {
                        catWithSubCatValues.get(controllingValues.get(i)).add(label);
                    }
                }
            }

            for (Schema.PicklistEntry ple : wirelessValues) {
                String label = ple.getValue();
                wirelessPickValues.add(label);
            }

            for (Schema.PicklistEntry ple : encryptionValues) {
                String label = ple.getValue();
                encryptionPickValues.add(label);
            }

            for (Schema.PicklistEntry ple : matchedStatusValues) {
                String label = ple.getValue();
                matchedHSStatusPickValues.add(label);
            }
            result.put('totalRecords',[SELECT count() FROM Part__c WHERE Shipment_Order__c =: shipmentOrderId]);
            result.put('catWithSubCatValues', catWithSubCatValues);
            result.put('wirelessPickValues', wirelessPickValues);
            result.put('encryptionPickValues', encryptionPickValues);
            result.put('matchedHSStatusPickValues', matchedHSStatusPickValues);
        }catch(Exception e) {
            result = new Map<String, Object> {'status'=>'ERROR','msg'=>e.getMessage()};
        }
        return result;
    }

    @AuraEnabled
    public static void createCountryCompliance(Id comDocId, Id productId){
        Map<String, Object> result = new Map<String, Object> { 'status' => 'OK' };
        try{
            insert new Country_Compliance__c(Name=comDocId+'&'+productId, Product__c=productId, Country_Compliance__c=comDocId);
        }
        catch(Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    @AuraEnabled
    public static Map<String, Object> updateShipmentParts(Id partId, Product2 product, HS_Codes_and_Associated_Details__c hsCode){
        Map<String, Object> result = new Map<String, Object> { 'status' => 'OK' };
        try{
            if(partId != NULL && hsCode != NULL && hsCode.Id != NULL)
                update new Part__c(Id=partId, Matched_HS_Code2__c=hsCode.Id);

            if(product != NULL && product.Id != NULL) update product;
            if(hsCode != NULL && hsCode.Id != NULL) update hsCode;

            List<Part__c> parts = [SELECT Id, Name, Product__r.ECCN__c, US_HTS_Code__c, Description_and_Functionality__c,
                                   US_HS_Code_and_Associated_Details__c, Product__c, Product__r.Name, Product__r.CCATs__c,
                                   Product__r.Description, Product__r.Manufacturer__r.Name, Product__r.Category__c,
                                   Product__r.US_HTS_Code__c,Product__r.Sub_Category__c, US_HS_Code_and_Associated_Details__r.Status__c,
                                   Product__r.MSRP__c,Product__r.Regulatory_Number__c, Product__r.Wireless_capability__c, Product__r.Encryption__c,
                                   Product__r.Product_Documents_Not_Required__c, Matched_HS_Code2__c, Matched_HS_Code2__r.Status__c,
                                   Product__r.Manufacturer__c, Matched_HS_Code2__r.Name
                                   FROM Part__c WHERE Id = :partId];

            result.put('parts', parts);
            result.put('msg', 'Successfully Updated');
        }
        catch(Exception e) {
            result = new Map<String, Object> {'status'=>'ERROR','msg'=>e.getMessage()};
        }

        return result;
    }

    @AuraEnabled
    public static void updateProductNotRequiredField(Map<Id,List<String> > productNotRequiredDocs){
        try{
            List<Product2> products = new List<Product2>();
            for(Id productId : productNotRequiredDocs.keySet()) {
                products.add(new Product2(Id=productId, Product_Documents_Not_Required__c=String.join(productNotRequiredDocs.get(productId), ';')));
            }
            update products;
        }
        catch(Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    @AuraEnabled
    public static Map<String, Object> removeAttachments(List<Id> attachmentIds){
        Map<String, Object> result = new Map<String, Object> { 'status' => 'OK' };
        try{
            Delete [SELECT Id, Title FROM ContentDocument where Id IN : attachmentIds];
        }
        catch(DmlException e) {
            result = new Map<String, Object> {'status'=>'ERROR','msg'=>e.getDmlMessage(0)};
        }
        catch(Exception e) {
            result = new Map<String, Object> {'status'=>'ERROR','msg'=>e.getMessage()};
        }
        return result;
    }

    @AuraEnabled
    public static Map<String, Object> updateShipmentDocNotRequired(Id shipmentId,String docsNotRequired){
        System.debug('docsNotRequired'+docsNotRequired);
        Map<String, Object> result = new Map<String, Object> { 'status' => 'OK' };
        try{
            update new Shipment_Order__c(Id=shipmentId,Documents_Not_Required__c=docsNotRequired);
            result.put('msg','Updated Successfully!');
        }catch(DmlException e) {
            result = new Map<String, Object> {'status'=>'ERROR','msg'=>e.getDmlMessage(0)};
        }
        catch(Exception e) {
            result = new Map<String, Object> {'status'=>'ERROR','msg'=>e.getMessage()};
        }
        return result;
    }

    @AuraEnabled
    public static Map<String, Object> deleteComplianceDoc(Compliance_Document__c doc,Id productId){
        Map<String, Object> result = new Map<String, Object> { 'status' => 'OK' };
        try{
            List<Country_Compliance__c> ccRecords = [SELECT Id, Name, Product__c,Country_Compliance__c,
                                                     Country_Compliance__r.Compliance_Document_Name__c, Country_Compliance__r.Compliance_status__c,
                                                     Country_Compliance__r.Compliance_type__c,Country_Compliance__r.Model_Number__c,
                                                     Country_Compliance__r.Link__c,Country_Compliance__r.Expiry__c,Country_Compliance__r.Country__c,
                                                     Country_Compliance__r.Certification_number__c FROM Country_Compliance__c
                                                     WHERE Product__c =:productId AND Country_Compliance__r.Compliance_Document_Name__c =: doc.Compliance_Document_Name__c AND Country_Compliance__r.Compliance_status__c = 'Available' LIMIT 1];

            if(!ccRecords.isEmpty()) {
                ComplianceWrapper comWrap = new ComplianceWrapper(ccRecords[0]);
                Map<String,Compliance_Document_Field_Rules__mdt> rules = Compliance_Document_Field_Rules__mdt.getAll();
                Compliance_Document_Field_Rules__mdt rule = rules.get(doc.Compliance_Document_Name__c);
                comWrap.nature = (rule !=null && rule.Nature_Use__c !=null ? rule.Nature_Use__c : '');
                comWrap.pro_isNotRequired = false;
                comWrap.isNotRequired = comWrap.status =='Not Required';
                comWrap.availableIcon = comWrap.isNotRequired ? 'action:reject' : String.isBlank(comWrap.cId) ? 'action:close' : 'action:approval';

                for(ContentDocumentLink linDoc : [SELECT ContentDocument.Title,ContentDocument.FileType,ContentDocument.ContentSize,
                                                  ContentDocument.FileExtension,ContentDocumentId,LinkedEntityId,SystemModstamp
                                                  FROM ContentDocumentLink WHERE LinkedEntityId =:ccRecords[0].Country_Compliance__c]) {
                    AttachmentWrapper att = new AttachmentWrapper(linDoc.ContentDocumentId,
                                                                  linDoc.LinkedEntityId,
                                                                  linDoc.ContentDocument.Title+'.'+linDoc.ContentDocument.FileType.toLowerCase(),
                                                                  null,
                                                                  linDoc.ContentDocument.ContentSize,
                                                                  linDoc.SystemModstamp.date()
                                                                  );
                    comWrap.addAttachment(att);

                }
                result.put('doc',comWrap);
            }
            Delete doc;
            result.put('msg','Updated Successfully!');
        }catch(DmlException e) {
            result = new Map<String, Object> {'status'=>'ERROR','msg'=>e.getDmlMessage(0)};
        }
        catch(Exception e) {
            result = new Map<String, Object> {'status'=>'ERROR','msg'=>e.getMessage()};
        }
        return result;
    }

    @AuraEnabled
    public static String upsertComplianceDoc(Compliance_Document__c doc,Id productId){
        Map<String, Object> result = new Map<String, Object> { 'status' => 'OK' };
        try {
            Boolean isCreate = (doc.Id == null || String.isBlank(doc.Id));
            upsert doc;
            if(isCreate) {
                createCountryCompliance(doc.Id,productId);
            }
            String cdInfo = '{"id":"'+doc.Id+'","fields":{"Compliance_type__c":{"value":"'+doc.Compliance_type__c+'"},"Certification_number__c":{"value":"'+doc.Certification_number__c+'"},"Compliance_status__c":{"value":"'+doc.Compliance_status__c+'"},"Link__c":{"value":"'+doc.Link__c+'"}}}';
            return cdInfo.replaceAll('null','');
        }catch(DmlException e) {
            throw new AuraHandledException(e.getDmlMessage(0));
        }catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    private static String base64ToBits(String validFor) {
        if (String.isEmpty(validFor)) return '';
        String validForBits = '';

        for (Integer i = 0; i < validFor.length(); i++) {
            String thisChar = validFor.mid(i, 1);
            Integer val = base64Chars.indexOf(thisChar);
            String bits = decimalToBinary(val).leftPad(6, '0');
            validForBits += bits;
        }
        return validForBits;
    }

    private static String decimalToBinary(Integer val) {
        String bits = '';
        while (val > 0) {
            Integer remainder = Math.mod(val, 2);
            val = Integer.valueOf(Math.floor(val / 2));
            bits = String.valueOf(remainder) + bits;
        }
        return bits;
    }

    public class PicklistEntryWrapper {
        public String active {get; set;}
        public String defaultValue {get; set;}
        public String label {get; set;}
        public String value {get; set;}
        public String validFor {get; set;}
        public PicklistEntryWrapper(){
        }
    }

    public class ComplianceWrapper {
        @AuraEnabled public String cId;
        @AuraEnabled public String country;
        @AuraEnabled public String status;
        @AuraEnabled public String name;
        @AuraEnabled public Boolean hasAttachment;
        @AuraEnabled public Boolean isNotRequired;
        @AuraEnabled public Boolean pro_isNotRequired;
        @AuraEnabled public String availableIcon;
        @AuraEnabled public String fileDocType;
        @AuraEnabled public String licenseNumber;
        @AuraEnabled public String fileLink;
        @AuraEnabled public String nature;
        @AuraEnabled public List<AttachmentWrapper> attachment;

        ComplianceWrapper(String name,Boolean isNotRequired,String nature){
            this.name = name;
            this.nature = nature;
            this.isNotRequired = isNotRequired;
            this.hasAttachment = false;
            this.pro_isNotRequired = isNotRequired;
            this.attachment = new List<AttachmentWrapper>();
            this.availableIcon = this.isNotRequired ? 'action:reject' : String.isBlank(this.cId) ? 'action:close' : 'action:approval';
        }

        public ComplianceWrapper(Country_Compliance__c cc){
            this.cId = cc.Country_Compliance__c;
            this.country = cc.Country_Compliance__r.Country__c;
            this.status = cc.Country_Compliance__r.Compliance_status__c;
            this.name = cc.Country_Compliance__r.Compliance_Document_Name__c;
            if(cc.Country_Compliance__r.Compliance_type__c == 'Certification Number') {
                if(cc.Country_Compliance__r.Certification_number__c != null) {
                    this.licenseNumber = cc.Country_Compliance__r.Certification_number__c;
                }
            }
            else if(cc.Country_Compliance__r.Compliance_type__c == 'Link') {
                this.fileDocType = 'pdf';
                this.fileLink = cc.Country_Compliance__r.Link__c;
            }else{
                this.fileDocType = 'pdf';
            }
            this.attachment = new List<AttachmentWrapper>();
        }

        public void addAttachment(AttachmentWrapper att){
            this.attachment.add(att);
        }
    }

}