@isTest
public class zeeVATLOEsController_Test {
    
    @testSetup
    public static void makeData() {
        DefaultFreightValues__c df = new DefaultFreightValues__c();
        df.Name = 'Test';
        df.Courier_Base_Rate_KG__c = 12;
        df.Courier_Dangerous_Goods_premium__c = 12;
        df.Courier_Fixed_Charge__c = 12;
        df.Courier_Non_stackable_premium__c = 12;
        df.Courier_Oversized_premium__c = 12;
        df.FF_Base_Rate_KG__c = 12;
        df.FF_Dangerous_Goods_Fixed_Charge_Premium__c = 12;
        df.FF_Dangerous_Goods_premium__c = 12;
        df.FF_Dedicated_Pickup__c = 12;
        df.FF_Fixed_Charge__c = 12;
        df.FF_Non_stackable_Fixed_Charge_Premium__c = 12;
        df.FF_Non_stackable_premium__c = 12;
        df.FF_Oversized_Fixed_Charge_Premium__c = 12;
        df.FF_Oversized_premium__c = 12;
        insert df;
        
        CountryandRegionMap__c cr = new CountryandRegionMap__c(Name ='Australia', Country__c='Australia', Region__c='South', European_Union__c='No');
        insert cr;
        CountryandRegionMap__c cr1 = new CountryandRegionMap__c(Name ='United States', Country__c='United States', Region__c='North America', European_Union__c='No');
        insert cr1;
        RegionalFreightValues__c rf = new RegionalFreightValues__c(
            name='North America to South', Origin__c='Australia', 
            Preferred_Freight_Forwarder_Name__c='Stream', 
            Destination__c='Australia', 
            Courier_Discount_Premium__c=10, 
            FF_Regional_Discount_Premium__c=10, 
            Courier_Fixed_Premium__c=10, 
            FF_Fixed_Premium__c=10, 
            Risk_Adjustment_Factor__c=1.41
        );
        insert rf;
 
         User user = [Select Id, Name, userName, Profile.Name, UserRole.name From user where Profile.Name = 'System Administrator' LIMIT 1];
        
        Id accManufRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Manufacturer').getRecordTypeId();
        Id accSupplierRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('IOR/EOR Supplier').getRecordTypeId();
        Account account = new Account (name='Acme1',  Type ='Client',  CSE_IOR__c = user.Id,  Service_Manager__c = user.Id,  Tax_recovery_client__c  = FALSE, Ship_From_Line_1__c = 'Ship_From_Line_1',
                                       Ship_From_Line_2__c = 'Ship_From_Line_2', Ship_From_Line_3__c = 'Ship_From_Line_3', Ship_From_City__c = 'Ship_From_City',  Ship_From_Zip__c = 'Ship_From_Zip',
                                       Ship_From_Country__c = 'Ship_From_Country' ,  Ship_From_Other_notes__c = 'Ship_From_Other', Ship_From_Contact_Name__c = 'Ship_From_Contact', Ship_From_Contact_Email__c = 'Ship_From_Email',
                                       Ship_From_Contact_Tel__c = 'Ship_From_Tel', Client_Address_Line_1__c = 'Client_Address_Line_1', Client_Address_Line_2__c = 'Client_Address_Line_2', Client_Address_Line_3__c = 'Client_Address_Line_3',
                                       Client_City__c = 'Client_City', Client_Zip__c = 'Client_Zip', Client_Country__c = 'Client_Country',  Other_notes__c = 'Other_notes',  Tax_Name__c = 'Tax_Name', Tax_ID__c = 'Tax_ID',
                                       Contact_Name__c = 'Contact_Name', Contact_Email__c = 'Contact_Email', Contact_Tel__c = 'Contact_Tel', Financial_Controller__c = user.Id );
          
        
        
        Account account2 = new Account (name='TecEx Prospective Client', Type ='Supplier', ICE__c = user.Id, RecordTypeId = accSupplierRecordTypeId);
        
         Account account3 = new Account (name='Test Manufacturer', Type ='Manufacturer', RecordTypeId = accManufRecordTypeId, Manufacturer_Alias__c = 'Tester,Who' ); 
		 List<Account> lstAcc = new List<Account>{account, account2,account3};
            insert lstAcc; 
        
        Contact contact = new Contact ( lastname ='Testing Individual',  AccountId = account.Id); insert contact;  
        
        IOR_Price_List__c iorpl = new IOR_Price_List__c ( Client_Name__c = account.Id, Name = 'Australia', IOR_Fee__c = 0.15, On_Charge_Mark_up__c = 0.15,
                                                         TecEx_Shipping_Fee_Markup__c =0.40, IOR_Min_Fee__c = 100,  Admin_Fee__c =100, Set_up_fee__c =100, Bank_Fees__c =200, Tax_Rate__c = 0.20,
                                                         Estimated_Customs_Brokerage__c =250, Estimated_Customs_Clearance__c =250, Estimated_Customs_Handling__c =250, Estimated_Import_License__c =250, 
                                                         Destination__c = 'Australia' ); insert iorpl; 
        
        country_price_approval__c cpa = new country_price_approval__c( name ='Australia', Billing_term__c = 'DAP/CIF - IOR pays',  Airwaybill_Instructions__c = 'User IOR Address', Supplier__c = account2.Id, 
                                                                      In_Country_Specialist__c = user.Id, Clearance_Destination__c = 'Australia', Preferred_Supplier__c = TRUE, 
                                                                      Destination__c = 'Australia' ); insert cpa;
        
        
        
        Country__c country = new Country__c(Brokerage_Costs__c = 618, CIF_Adjustment_Factor__c = 0.035, CIF_Absolute_value_adjustment__c = 200, Clearance_Costs__c = 488, 
                                            Country__c = 'Australia', Name = 'Australia', Handling_Costs__c = 1180, License_Permit_Costs__c = 0); insert country;
        
        CPA_v2_0__c aCpav = new CPA_v2_0__c(Name = 'Australia', Country_Applied_Costings__c = TRUE); insert aCpav;
        
        CPA_v2_0__c cpav2 = new CPA_v2_0__c(Name = 'Australia Testing 2.0', Supplier__c = account2.Id, Service_Type__c = 'IOR/EOR', Related_Costing_CPA__c = aCpav.Id, 
                                            Preferred_Supplier__c = TRUE, VAT_Rate__c = 0.1, Destination__c = 'Australia',  Final_Destination__c = 'Australia', 
                                            VAT_Reclaim_Destination__c = FALSE, Country__c = country.id, Lead_ICE__c = user.Id); insert cpav2; 
        
        List<CPARules__c> cpaRules = new List<CPARules__c>();
        cpaRules.add(new CPARules__c(Country__c = 'Australia',Criteria__c ='Default', Chargable_weight__c = 'NONE', Courier_Responsibility__c = 'NONE',   ECCN_No__c = 'NONE', NL_Product_User__c = 'NONE', Number_of_final_deliveries__c = 'NONE', ShipmentValue__c ='NONE', Sum_of_quantity_of_matched_products__c = 'NONE', Manufacturer__c = 'NONE', DefaultCPA2__c = cpav2.Id, CPA2__c =  cpav2.Id));
        insert cpaRules;
        
        Currency_Management2__c CM = new Currency_Management2__c (Name = 'Australian Dollar (AUD)', Currency__c = 'Australian Dollar (AUD)', ISO_Code__c = 'AUD', Conversion_Rate__c = 1);
        insert CM; 
        
        
        Shipment_Order__c shipmentOrder = new Shipment_Order__c(Account__c = account.id, Shipment_Value_USD__c = 2000, CPA_v2_0__c = cpav2.Id, IOR_CSE__c = user.Id, ICE__c = user.Id, Financial_Controller__c = user.Id,
                                                                Client_Contact_for_this_Shipment__c = contact.Id, Who_arranges_International_courier__c ='Client', Tax_Treatment__c ='DAP/CIF - IOR pays', 
                                                                Ship_to_Country__c = cpa.Id, Destination__c = 'Australia', Service_Type__c = 'IOR', IOR_Price_List__c = iorpl.Id, Chargeable_Weight__c = 200,  
                                                                Shipping_Status__c = 'Cost Estimate Abandoned', Ship_From_Country__c = 'Angola', SupplierlU__c = account2.Id );  
        Insert shipmentOrder; 
        
        Registrations__c regt = new Registrations__c (Name = 'Test Regt.', Registered_Address__c= 'Test Address 1', Registered_Address_2__c='Test Address 2', 
                                  Registered_Address_City__c= 'Test City', Registered_Address_Province__c='Test Province', 
                                  Registered_Address_Postal_Code__c='230000', Registered_Address_Country__c='Australia', 
                                  VAT_number__c='234566778', Company_name__c =account.Id, Country__c='Australia');
        Insert regt;
        
        Customs_Clearance_Documents__c CCD = New Customs_Clearance_Documents__c(Name='CCD001',Foreign_Exchange_Rate__c=CM.Conversion_Rate__c ,Tax_Currency__c='Australian Dollar (AUD)', Customs_Clearance_Documents__c=shipmentOrder.id, Duties_and_Taxes_per_CCD__c = 1000, Date_of_clearance__c = System.today(), Customs_Reference_Number__c = 'test123456');
        Insert CCD;
        
        Part__c part1 = new Part__c(Name ='CAB-ETH-S-RJ45',  Quantity__c= 1, Commercial_Value__c = 15000, US_HTS_Code__c = '85444210', 
                                    Country_of_Origin2__c = 'China', Shipment_Order__c = shipmentOrder.Id, Description_and_Functionality__c ='AB-ETH-S-RJ45'); 
        Insert part1;
        
    }
    public static testMethod void test1() {
        shipment_Order__c shipmentOrder = [SELECT Id FROM Shipment_Order__c LIMIT 1];

        PageReference pref = Page.ZeeVATLOEs;
        Test.setCurrentPage(pref);
        ApexPages.currentPage().getParameters().put('id',String.valueOf(shipmentOrder.id));
        
        Test.startTest();
        ApexPages.StandardController controller = new ApexPages.StandardController(shipmentOrder);
        
        zeeVATLOEsController valObj = new zeeVATLOEsController(controller);
        Test.stopTest();
    }

}