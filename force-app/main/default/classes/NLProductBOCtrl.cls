public with sharing class NLProductBOCtrl {

    @AuraEnabled
    public static Map<String,Object> getShipmentRecord(Id recordId){
        try {
            Map<String,Object> response = new Map<String,Object>();
            response.put('countries',Util.getPicklistValues('Shipment_Order__c','Beneficial_Owner_Country__c'));
            Task task = [SELECT Id,WhatId,State__c FROM Task WHERE Id=: recordId];

            List<Shipment_Order__c> shipments = [SELECT Id,Name,Account__r.Name,Intention_with_Goods__c,Intention_with_Goods_Other__c,Contact_name__c,Beneficial_Owner_Country__c,Buyer_or_BO_Part_of_EU__c,Beneficial_Owner_Company_Address__c,Buyer_or_BO_VAT_Number__c, Buyer_Retains_Ownership__c FROM Shipment_Order__c WHERE Id =: task.WhatId];

            if(shipments.isEmpty()) throw new AuraHandledException('No Shipment found related to this task.');
            response.put('record',shipments[0]);
            response.put('task',task);
            response.put('CountryandRegionMap',CountryandRegionMap__c.getAll());
            return response;
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    @AuraEnabled
    public static void updateShipmentTask(Task task,Shipment_order__c shipment){
        try {
            RecusrsionHandler.tasksCreated = true;
            task.state__c = 'Under Review';
            update task;

            RecusrsionHandler.tasksCreated = false;
            update shipment;
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }
}