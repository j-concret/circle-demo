public class importPartsFromCSV{

public Blob csvFileBody{get;set;}
public string csvAsString{get;set;}
public String[] csvFileLines{get;set;}
Public Shipment_Order__c shipmentOrder = new Shipment_Order__c();
public List<Part__c> parts {get;set;}




public importPartsFromCSV(ApexPages.StandardController controller){
this.shipmentOrder = (Shipment_Order__c)Controller.getRecord();
this.parts =         [ Select Id, Name, X6_Digit_Code__c, Brand_Name__c, Capacity__c, CCATS__c, CIF_1__c, CIF_Duties__c, Commercial_Value__c, COO_Matches_Product__c, Countries_Approved_in__c, Country_IOR_Approval__c, Country_of_Origin__c, Country_of_Origin2__c, 
                      Description_and_Functionality__c, Destination_HTS_Code__c, Dimension_cm_lxbxh__c, ECCN_Matches_Product__c, ECCN_NO__c, Extended_Value__c, FOB_Duties__c, HS_Code_Matches_Product__c, Includes__c, Licence_Exception_Available__c, Manufacturer_Name__c, 
                      Matched_HS_Code2__c, Matched_HS_Code__c, Matched_HS_Code_Description__c, New_Refurbished_Used__c, New_Product__c, No_HS_Code_Match__c, OEM__c, Part_Number__c, Part_Type__c, Product__c, Quantity__c, Rate__c, Restricted_Item__c, Shipment_Order__c, 
                      Ship_To_Country__c, Total_Value__c, Unit_Price_Validated__c, US_HTS_Code__c, Weight_kg__c 
                      FROM Part__c where Shipment_Order__c = :shipmentOrder.Id];

  
  }
 
 
  
  public Pagereference importPartCSVFile(){
       try{
           csvAsString = csvFileBody.toString();
           csvFileLines = csvAsString.split('\n'); 
            
           for(Integer i=1;i<csvFileLines.size();i++){
               Part__c accObj = new Part__c() ;
               string[] csvRecordData = csvFileLines[i].split(',');
               accObj.Shipment_Order__c = shipmentOrder.Id;
               accObj.Name = csvRecordData[0] ;             
               accObj.Part_Number__c = csvRecordData[1] ;
               accObj.Quantity__c = Decimal.valueOf(csvRecordData[2]);
               accObj.Commercial_Value__c = Decimal.valueOf(csvRecordData[3]);
               accObj.US_HTS_Code__c = csvRecordData[4];
               accObj.Country_of_Origin2__c = csvRecordData[5];
               accObj.Manufacturer_Name__c = csvRecordData[6];
               
               parts.add(accObj);                                                                             
                 
           }
        upsert parts ;
        PageReference pageRef = new ApexPages.StandardController(shipmentOrder).view();
        pageRef.setRedirect(true);
        return pageRef;
         
        }
        catch (Exception e)
        {
            ApexPages.Message errorMessage = new ApexPages.Message(ApexPages.severity.ERROR,'An error has occured while importing data Please make sure input csv file is correct');
            ApexPages.addMessage(errorMessage);
            
            return null;
        } 
       
  }
    

    
    


}