@RestResource(urlMapping='/RolloutExcelexport/*')
Global class NCPRolloutEXcelExport {
    
    
     @Httppost
      global static void NCPRolloutEXcelExport(){
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        Blob body = req.requestBody;
        res.addHeader('Content-Type', 'application/json');
        String requestString = body.toString();
        NCPRolloutEXcelExportwra rw  = (NCPRolloutEXcelExportwra)JSON.deserialize(requestString,NCPRolloutEXcelExportwra.class);
          try{
              Access_token__c At =[select status__c,Access_Token__c from Access_token__c where Access_Token__c=:rw.Accesstoken ];
            	if( at.status__C =='Active'){
            
                                     
              Attachment att=   RolloutExportforNCP.RolloutExportforNCP(rw.RolloutID); 
                    
              Blob MyBlob;     
              string MyFile;
              attmnt a=new attmnt();
            
         List<attmnt> oppatts = new list<attmnt>();
                    
            if( att != null){
           
                       
                a.AttachmentType=att.ContentType;
                a.AttachmentName=att.Name;
                
                // attachment body to convert to blob format.
                 MyFile=EncodingUtil.base64Encode(att.body);
                 MyBlob=EncodingUtil.base64Decode(MyFile);
                
                a.AttachmentBody=MyBlob;
                  oppatts.add(a);
              }    
               
               string Jsonstring=Json.serialize(oppatts);
             res.responseBody=Blob.valueOf(Jsonstring);
             res.addHeader('Content-Type', 'application/json');
             res.statusCode = 200;
                
                API_Log__c Al = New API_Log__c();
                Al.EndpointURLName__c='RolloutExcelexport';
               // Al.Account__c=rw.AccountID;
                Al.StatusCode__c=string.valueof(res.statusCode);
                AL.Response__c = 'Success - Rollout Excel export data sent';
                Insert Al;  
                    
              }
              
              else{
                   JSONGenerator gen = JSON.createGenerator(true);
            gen.writeStartObject();
            
             gen.writeFieldName('Error');
             gen.writeStartObject();
            
                gen.writeStringField('Response', 'Accesstoken Expired');
            
            gen.writeEndObject();
            gen.writeEndObject();
            String jsonData = gen.getAsString();
            res.responseBody = Blob.valueOf(jsonData);
            res.addHeader('Content-Type', 'application/json');
            res.statusCode = 400;        
            
                  
              }
             
                }
              
            catch(Exception e){
                
                
               
                
                String ErrorString ='Something went wrong, please contact support_SF@tecex.com';
                       res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
                   res.addHeader('Content-Type', 'application/json');
                    res.statusCode = 400;
                
                API_Log__c Al = New API_Log__c();
             //   Al.Account__c=rw.AccountID;
                //Al.Login_Contact__c=logincontact.Id;
                Al.EndpointURLName__c='RolloutExcelexport';
                Al.Response__c=ErrorString;
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al;
                      
          
          
          
      
            }
      
            }
     global class attmnt{       
        public string AttachmentType{get;set;}
        public string AttachmentName{get;set;}
        public string AttachmentId{get;set;}
        public blob AttachmentBody{get;set;}
    } 

}