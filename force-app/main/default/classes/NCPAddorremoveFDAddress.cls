@RestResource(urlMapping='/NCPAddorremoveFDAddress/*')
Global class NCPAddorremoveFDAddress {

     @Httppost
    global static void NCPupdateFreightAddress(){
    
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        Blob body = req.requestBody;
    	res.addHeader('Content-Type', 'application/json');
        String requestString = body.toString();
        NCPAddorremoveFDAddressWra rw = (NCPAddorremoveFDAddressWra)JSON.deserialize(requestString,NCPAddorremoveFDAddressWra.class);
           try {
               
               Access_token__c At =[select status__c,Access_Token__c from Access_token__c where Access_Token__c=:rw.Accesstoken ];
               if( at.status__C =='Active'){
                  //  List<ID> ExisitngshiptoIds = new List<Id>();
                  //  List<ID> NewShiptoIds = New list<ID>();
                  // For(integer i=0;rw.FinalDelivieries.size() > i; i++){ NewShiptoIds.add(rw.FinalDelivieries[i].FinalDestinationAddressID);}
                    
                  // For(Final_Delivery__c FDL1 : FDL){ ExisitngshiptoIds.add(FDL1.Ship_to_address__c);}
                   
                   List<Final_Delivery__c> FDL = [select id,Shipment_Order__c,Ship_to_address__c from Final_Delivery__c where Shipment_Order__c=:rw.SOID ];    
                   Delete FDL;
                   
              list<Final_Delivery__c> FDLI = new list<Final_Delivery__c>();
           		
              for(Integer i=0; rw.Final_Delivery.size()>i;i++) {
              
          		Final_Delivery__c FD = new Final_Delivery__c(
            	Shipment_Order__c = Rw.SOID,
              	name= rw.Final_Delivery[i].Name,
              	Ship_to_address__C  = rw.Final_Delivery[i].FinalDestinationAddressID
              
                );
            
           		FDLI.add(FD); 
                }
              Insert FDLI;
                   JSONGenerator gen = JSON.createGenerator(true);
            gen.writeStartObject();
            
             gen.writeFieldName('Success');
             gen.writeStartObject();
            
             	if(rw.Final_Delivery[0].Name == null) {gen.writeNullField('Response');} else{gen.writeStringField('Response', 'Success-add and remove Final Deliveries are done');}
            
            			gen.writeEndObject();
            			gen.writeEndObject();
            			String jsonData = gen.getAsString();
                        res.responseBody = Blob.valueOf(jsonData);
                        res.addHeader('Content-Type', 'application/json');
                        res.statusCode = 200;        
                            
                    
                API_Log__c Al = New API_Log__c();
               // Al.Account__c=FD.Shipment_Order__r.Account__c;
                Al.EndpointURLName__c='NCP-NCPAddorremoveFDAddress';
                Al.Response__c='Success- FinalDelivery removed and added';
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al; 
            
                   
                }
               else{
                     JSONGenerator gen = JSON.createGenerator(true);
            gen.writeStartObject();
            
             gen.writeFieldName('Error');
             gen.writeStartObject();
            
             	gen.writeStringField('Response', 'Accesstoken expired');
            
            gen.writeEndObject();
            gen.writeEndObject();
            String jsonData = gen.getAsString();
            res.responseBody = Blob.valueOf(jsonData);
            res.addHeader('Content-Type', 'application/json');
            res.statusCode = 400;  
               }
           }
        catch(Exception e){
            
            
            	String ErrorString ='Something went wrong while creating Final deliveries, please contact support_SF@tecex.com';
               	res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
                res.addHeader('Content-Type', 'application/json');
            	res.statusCode = 500;
                API_Log__c Al = New API_Log__c();
               // Al.Account__c=FD.Shipment_Order__r.Account__c;
                Al.EndpointURLName__c='NCP-NCPAddorremoveFDAddress';
                Al.Response__c=e.getMessage()+' at line number: '+e.getLineNumber();
                Al.StatusCode__c=string.valueof(res.statusCode);
              	Insert Al;
        }    
    }
 }