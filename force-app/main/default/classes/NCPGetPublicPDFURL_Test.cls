@IsTest
public class NCPGetPublicPDFURL_Test {
    static testMethod void testGetPublicPDFURLWithAttachment(){
        Case cs = new Case(Subject = 'Test', Origin = 'Phone', Status = 'New');
        insert cs;
        
        Attachment attach=new Attachment();       
        attach.Name='Unit Test Attachment';
        Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
        attach.body=bodyBlob;
        attach.parentId=cs.id;
        insert attach;
        
        Access_token__c accessTokenObj = new Access_token__c();
        accessTokenObj.Status__c = 'Active';
        accessTokenObj.Access_Token__c = 'asdfghj-sdfgfds-sfdfgf';
        
        Insert accessTokenObj;
        
        NCPGetPublicPDFURLWra wrapper = new NCPGetPublicPDFURLWra();
        wrapper.Accesstoken = accessTokenObj.Access_Token__c;
        wrapper.recordId = attach.id;
        
        
        RestRequest req1 = new RestRequest();
        RestResponse res1 = new RestResponse();
        req1.requestURI = '/services/apexrest/GetPublicPDFURL'; 
        req1.httpMethod = 'POST';
        req1.requestBody = Blob.valueOf(JSON.serialize(wrapper));     
        RestContext.request = req1;
        RestContext.response = res1;
        RestContext.request.addHeader('Content-Type', 'application/json');
        
        Test.startTest(); 
        NCPGetPublicPDFURL.NCPGetPublicPDFURL();
        Test.stopTest();
    }
    
    static testMethod void testGetPublicPDFURLWithContentDocument(){
       
		ContentVersion cv = new ContentVersion();
        cv.Title = 'Test Document';
        cv.PathOnClient = 'TestDocument.pdf';
        cv.VersionData = Blob.valueOf('Test Content');
        cv.IsMajorVersion = true;
        Insert cv;
        
        List<ContentVersion> cvList = [SELECT Id, Title, ContentDocumentId FROM ContentVersion WHERE Id = :cv.Id];
        
        Access_token__c accessTokenObj = new Access_token__c();
        accessTokenObj.Status__c = 'Active';
        accessTokenObj.Access_Token__c = 'asdfghj-sdfgfds-sfdfgf';
        
        Insert accessTokenObj;
        
        NCPGetPublicPDFURLWra wrapper = new NCPGetPublicPDFURLWra();
        wrapper.Accesstoken = accessTokenObj.Access_Token__c;
        wrapper.recordId = cvList[0].ContentDocumentId;
        
        
        RestRequest req1 = new RestRequest();
        RestResponse res1 = new RestResponse();
        req1.requestURI = '/services/apexrest/GetPublicPDFURL'; 
        req1.httpMethod = 'POST';
        req1.requestBody = Blob.valueOf(JSON.serialize(wrapper));     
        RestContext.request = req1;
        RestContext.response = res1;
        RestContext.request.addHeader('Content-Type', 'application/json');
        
        Test.startTest(); 
        NCPGetPublicPDFURL.NCPGetPublicPDFURL();
        Test.stopTest();
    }
     static testMethod void testGetPublicPDFURLWithAttachmentexception(){
        Case cs = new Case(Subject = 'Test', Origin = 'Phone', Status = 'New');
        insert cs;
        
        Attachment attach=new Attachment();       
        attach.Name='Unit Test Attachment';
        Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
        attach.body=bodyBlob;
        attach.parentId=cs.id;
        insert attach;
        
        Access_token__c accessTokenObj = new Access_token__c();
        accessTokenObj.Status__c = 'Active';
        accessTokenObj.Access_Token__c = 'asdfghj-sdfgfds-sfdfgf';
        
        Insert accessTokenObj;
        
        NCPGetPublicPDFURLWra wrapper = new NCPGetPublicPDFURLWra();
        wrapper.Accesstoken = null;
        wrapper.recordId = null;
        
        
        RestRequest req1 = new RestRequest();
        RestResponse res1 = new RestResponse();
        req1.requestURI = '/services/apexrest/GetPublicPDFURL'; 
        req1.httpMethod = 'POST';
        req1.requestBody = Blob.valueOf(JSON.serialize(wrapper));     
        RestContext.request = req1;
        RestContext.response = res1;
        RestContext.request.addHeader('Content-Type', 'application/json');
        
        Test.startTest(); 
        NCPGetPublicPDFURL.NCPGetPublicPDFURL();
        Test.stopTest();
    }
}