global class monthEndProcess implements Database.Batchable<sObject> {


    global Database.QueryLocator start(Database.BatchableContext BC) {
        // collect the batches of records or objects to be passed to execute
        
        
        String query = 'SELECT Id, Account__c,CI_Recharge_Tax_and_Duty__c, Final_Supplier_Invoice_Received__c, No_further_client_invoices_required__c, Who_arranges_International_courier__c,Actual_Total_Tax_and_Duty__c,Actual_Total_License_Cost__c,Actual_Total_Handling_Costs__c,Actual_Total_Customs_Brokerage_Cost__c,Actual_Total_Clearance_Costs__c,Actual_Miscellaneous_Fee__c,Actual_Liability_Cover__c,Actual_International_Delivery_Fee__c,Actual_Insurance_Fee_USD__c,Actual_Finance_Fee__c,Actual_Total_IOR_EOR__c,Actual_Cash_Outlay_Fee__c,Actual_Bank_Fees__c,Actual_Admin_Fee__c, CI_Total_Clearance_Costs__c,CI_International_Delivery_Fee__c,Service_Type__c,CI_IOR_and_Import_Compliance_Fee_USD__c,CI_Cash_Outlay_Fee__c,CI_Total_Customs_Brokerage_Cost__c,CI_Total_Licence_Cost__c,CI_Total_Handling_Cost__c,CI_Liability_Cover__c,CI_Bank_Fees__c,CI_Admin_Fee__c, CI_Miscellaneous_Fee__c,CI_EOR_and_Export_Compliance_Fee_USD__c, POD_Date__c, Account__r.Dimension__c, Account__r.Region__c, CI_Total__c, Actual_Total__c, Month_End_Journal_Entries__c, New_Month_Journal_Entries__c, SupplierlU__c, Total_Invoice_Amount__c, FC_Total__c, IOR_FEE_USD__c, EOR_and_Export_Compliance_Fee_USD__c, Handling_and_Admin_Fee__c, International_Delivery_Fee__c, Insurance_Fee_USD__c, Recharge_Tax_and_Duty__c, Recharge_Tax_and_Duty_Other__c, Collection_Administration_Fee__c, Total_Customs_Brokerage_Cost__c, Total_clearance_Costs__c, Total_License_Cost__c, Recharge_Handling_Costs__c, Finance_Fee__c, Bank_Fees__c, Miscellaneous_Fee__c, FC_Admin_Fee__c, FC_Bank_Fees__c, FC_Finance_Fee__c, FC_Insurance_Fee_USD__c, FC_International_Delivery_Fee__c, FC_IOR_and_Import_Compliance_Fee_USD__c, FC_Miscellaneous_Fee__c, FC_Recharge_Tax_and_Duty__c, FC_Total_Clearance_Costs__c, FC_Total_Customs_Brokerage_Cost__c, FC_Total_Handling_Costs__c, FC_Total_License_Cost__c, Tax_Cost__c, International_Delivery_Cost__c, Minimum_Brokerage_Costs__c, Minimum_Clearance_Costs__c, Minimum_Handling_Costs__c, Minimum_License_Permit_Costs__c, Recharge_License_Cost__c  FROM Shipment_Order__c where POD_Date__c!= null AND (Closed_Down_SO__c = FALSE)';
       

        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<Shipment_Order__c> shipmentOrderList){
       
        // process each batch of records
        
    Map<Id, Accounting_Period__c> accPeriodMap = new Map<Id, Accounting_Period__c>([Select Id, Name 
                                                                                 From Accounting_Period__c
                                                                                 Where Status__c = 'Open']); 
    
    
  Map<String, Id> accPeriod = new Map<String, Id>();   
  For (Id getAccPeriod : accPeriodMap.keySet()) { 
           accPeriod.put(accPeriodMap.get(getAccPeriod).Name, getAccPeriod );           
       } 
        
   Map<Id, Dimension__c> dimensionMap = new Map<Id, Dimension__c>([Select Id, Name
                                                                  From Dimension__c]);
    
   Map<String, Id> dimension = new Map<String, Id>(); 
    
    For (Id getDimension : dimensionMap.keySet()) { 
           dimension.put(dimensionMap.get(getDimension).Name, getDimension );           
       } 
   
   Map<Id, GL_Account__c> glAccountMap = new Map<Id, GL_Account__c>([Select Id, Name, GL_Account_Description__c,GL_Account_Short_Description__c,Currency__c  
                                                                  From GL_Account__c]); 
    
    
   Map<String, Id> glAccount = new Map<String, Id>(); 
    
   For (Id getGlAccount : glAccountMap.keySet()) { 
           glAccount.put(glAccountMap.get(getGlAccount).Name, getGlAccount );           
       } 
    
   Date currentDate = date.today();
        
        
  Map<Id, Account> accountMap = new Map<Id,Account>([Select Id, Name, Region__c, Dimension__c, Invoicing_Term_Parameters__c, IOR_Payment_Terms__c, Invoice_Timing__c
                                                    From Account]);
        
 
        
    List<Transaction_Line_New__c> transactionLines = new List<Transaction_Line_New__c>();
        
        for(Shipment_Order__c shipment : shipmentOrderList)
        { 	
           String dimension3 = accountMap.get(shipment.Account__c).Region__c; 
           String dimension1 = accountMap.get(shipment.Account__c).Dimension__c; 
           //String suppDimension1 = shipment.SupplierlU__r.Dimension__c; 
           
           Decimal rechargeOther = shipment.Recharge_Tax_and_Duty_Other__c == null? 0: shipment.Recharge_Tax_and_Duty_Other__c;
           Decimal recharge = shipment.Recharge_Tax_and_Duty__c == null? 0: shipment.Recharge_Tax_and_Duty__c;
           Decimal rechargeFC = shipment.FC_Recharge_Tax_and_Duty__c == null? 0: shipment.FC_Recharge_Tax_and_Duty__c;
           Decimal deliveryCost = shipment.International_Delivery_Cost__c == null? 0: shipment.International_Delivery_Cost__c;
           Decimal deliveryFC = shipment.FC_International_Delivery_Fee__c == null? 0: shipment.FC_International_Delivery_Fee__c;
           Decimal taxAmount = rechargeOther + recharge;
           Decimal ciTax = shipment.CI_Recharge_Tax_and_Duty__c == null ?0: shipment.CI_Recharge_Tax_and_Duty__c;
           Decimal taxescost = shipment.Tax_Cost__c == null? 0: shipment.Tax_Cost__c;
           Decimal taxCost = rechargeFC;
           Decimal adminCost = shipment.FC_Admin_Fee__c == null? 0: shipment.FC_Admin_Fee__c;
           Decimal clearanceFee = shipment.Total_clearance_Costs__c == null? 0: shipment.Total_clearance_Costs__c;
           Decimal brokerageFee = shipment.Total_Customs_Brokerage_Cost__c == null? 0: shipment.Total_Customs_Brokerage_Cost__c;
           Decimal handlingFee = shipment.Recharge_Handling_Costs__c == null? 0: shipment.Recharge_Handling_Costs__c;
           Decimal licenseFee = shipment.Recharge_License_Cost__c == null? 0: shipment.Recharge_License_Cost__c;
           Decimal iorFee = shipment.IOR_FEE_USD__c == null? 0: shipment.IOR_FEE_USD__c;
           Decimal cieorfee = shipment.CI_EOR_and_Export_Compliance_Fee_USD__c == null? 0: shipment.CI_EOR_and_Export_Compliance_Fee_USD__c; 
           Decimal iorCost= shipment.FC_IOR_and_Import_Compliance_Fee_USD__c == null? 0: shipment.FC_IOR_and_Import_Compliance_Fee_USD__c;
           Decimal adminFee = shipment.Handling_and_Admin_Fee__c == null? 0: shipment.Handling_and_Admin_Fee__c;
           Decimal ciadminFee = shipment.CI_Admin_Fee__c == null? 0: shipment.CI_Admin_Fee__c;
           Decimal cibankFee = shipment.CI_Bank_Fees__c == null? 0: shipment.CI_Bank_Fees__c;
           Decimal bankfee = shipment.Bank_Fees__c == null? 0: shipment.Bank_Fees__c;
           Decimal finance = shipment.Finance_Fee__c == null? 0: shipment.Finance_Fee__c;
           Decimal cicashout = shipment.CI_Cash_Outlay_Fee__c == null? 0: shipment.CI_Cash_Outlay_Fee__c;
           Decimal ciiorfee = shipment.CI_IOR_and_Import_Compliance_Fee_USD__c == null? 0: shipment.CI_IOR_and_Import_Compliance_Fee_USD__c;
           Decimal ciliab = shipment.CI_Liability_Cover__c == null? 0: shipment.CI_Liability_Cover__c;
           Decimal insurance = shipment.Insurance_Fee_USD__c == null? 0: shipment.Insurance_Fee_USD__c;
           Decimal fcadmin = shipment.FC_Admin_Fee__c == null?0 : shipment.FC_Admin_Fee__c;
           Decimal fcbank = shipment.FC_Bank_Fees__c == null?0 : shipment.FC_Bank_Fees__c;
            Decimal fcior = shipment.FC_IOR_and_Import_Compliance_Fee_USD__c == null?0 : shipment.FC_IOR_and_Import_Compliance_Fee_USD__c;
            Decimal fcbrok = shipment.FC_Total_Customs_Brokerage_Cost__c == null?0 : shipment.FC_Total_Customs_Brokerage_Cost__c;
            Decimal fcclear = shipment.FC_Total_Clearance_Costs__c == null?0 : shipment.FC_Total_Clearance_Costs__c;
            Decimal fchand = shipment.FC_Total_Handling_Costs__c == null?0 : shipment.FC_Total_Handling_Costs__c;
            Decimal fclic = shipment.FC_Total_License_Cost__c == null?0 : shipment.FC_Total_License_Cost__c;
            Decimal fcfin = shipment.FC_Finance_Fee__c == null?0 : shipment.FC_Finance_Fee__c;
            Decimal fcliab = shipment.FC_Insurance_Fee_USD__c == null?0 : shipment.FC_Insurance_Fee_USD__c; 
           Decimal fcmis = shipment.FC_Miscellaneous_Fee__c == null?0 : shipment.FC_Miscellaneous_Fee__c;
           Decimal Misfee = shipment.Miscellaneous_Fee__c == null? 0: shipment.Miscellaneous_Fee__c;
           Decimal CIMisfee = shipment.CI_Miscellaneous_Fee__c == null? 0: shipment.CI_Miscellaneous_Fee__c;
           Decimal oncharges = brokerageFee + licenseFee + clearanceFee + handlingFee;
           Decimal onChargeCosts = fcclear + fcbrok + fchand + fclic;
           Decimal ciclear = shipment.CI_Total_Clearance_Costs__c == null? 0: shipment.CI_Total_Clearance_Costs__c;
           Decimal cihand = shipment.CI_Total_Handling_Cost__c == null? 0 : shipment.CI_Total_Handling_Cost__c;
           Decimal cilic = shipment.CI_Total_Licence_Cost__c == null? 0: shipment.CI_Total_Licence_Cost__c;
            Decimal siNegTotal = taxCost + fcadmin + fcbank + fcior + fcbrok + fcclear + fchand + fclic + fcfin + fcliab + fcmis + (shipment.Who_arranges_International_courier__c =='TecEx'? deliveryCost + deliveryFC : 0);
           Decimal cibrok = shipment.CI_Total_Customs_Brokerage_Cost__c == null? 0: shipment.CI_Total_Customs_Brokerage_Cost__c;
           Decimal eorfee = shipment.EOR_and_Export_Compliance_Fee_USD__c == null ? 0: shipment.EOR_and_Export_Compliance_Fee_USD__c;
           Decimal eorfeecomb = adminFee + bankfee + Eorfee;
           Decimal cieorfeecomb = ciadminFee + ciBankFee + cieorfee;
           Decimal cidel = shipment.CI_International_Delivery_Fee__c == null ?0: shipment.CI_International_Delivery_Fee__c;
           Decimal intdel = shipment.International_Delivery_Fee__c == null ?0: shipment.International_Delivery_Fee__c;
           Decimal actualior = shipment.Actual_Total_IOR_EOR__c == null ?0: shipment.Actual_Total_IOR_EOR__c;
            Decimal actualadmin = shipment.Actual_Admin_Fee__c == null ?0: shipment.Actual_Admin_Fee__c;
            Decimal actualbank = shipment.Actual_Bank_Fees__c == null ?0: shipment.Actual_Bank_Fees__c;
            Decimal actualcashout = shipment.Actual_Cash_Outlay_Fee__c == null ?0: shipment.Actual_Cash_Outlay_Fee__c;
            Decimal actualfin = shipment.Actual_Finance_Fee__c == null ?0: shipment.Actual_Finance_Fee__c;
            Decimal actualins = shipment.Actual_Insurance_Fee_USD__c == null ?0: shipment.Actual_Insurance_Fee_USD__c;
            Decimal actualdel = shipment.Actual_International_Delivery_Fee__c == null ?0: shipment.Actual_International_Delivery_Fee__c;
            Decimal actualliab = shipment.Actual_Liability_Cover__c == null ?0: shipment.Actual_Liability_Cover__c;
            Decimal actualmis = shipment.Actual_Miscellaneous_Fee__c == null ?0: shipment.Actual_Miscellaneous_Fee__c;
            Decimal actualclear = shipment.Actual_Total_Clearance_Costs__c == null ?0: shipment.Actual_Total_Clearance_Costs__c;
            Decimal actualbrok = shipment.Actual_Total_Customs_Brokerage_Cost__c == null ?0: shipment.Actual_Total_Customs_Brokerage_Cost__c;
            Decimal actualhand = shipment.Actual_Total_Handling_Costs__c == null ?0: shipment.Actual_Total_Handling_Costs__c;
            Decimal actuallic = shipment.Actual_Total_License_Cost__c == null ?0: shipment.Actual_Total_License_Cost__c;
            Decimal actualtax = shipment.Actual_Total_Tax_and_Duty__c == null ?0: shipment.Actual_Total_Tax_and_Duty__c;
            Decimal actualTotal = shipment.Actual_Total__c == null ?0: shipment.Actual_Total__c;
            Decimal CiTotal = shipment.CI_Total__c == null ?0 : shipment.CI_Total__c;
            
          
           integer year =  date.today().year();
           integer month =  date.today().month();
           String month2 = String.valueOf(month);
           String month3 = month2.length() == 1? 0 + month2 : month2;
           
           String accountPeriod = String.valueOf(year)+'-'+ month3; 
            
            system.debug('accountPeriod---->'+accountPeriod   );
            If((((shipment.Total_Invoice_Amount__c -  CiTotal) > 50) || (shipment.Total_Invoice_Amount__c - CiTotal) < -50) && shipment.No_further_client_invoices_required__c == FALSE){

                Transaction_Line_New__c ciDT = NEW Transaction_Line_New__c(
                Amount__c = shipment.Total_Invoice_Amount__c - shipment.CI_Total__c,
                Date__c = date.today(),
                Business_Partner__c = shipment.Account__c,
                Description__c = 'Accrued Revenue',
                Dimension_1__c = dimension1,
                Dimension_3__c = dimension.get(dimension3),
                GL_Account__c = glAccount.get('613500'),
                RecordTypeId = Schema.Sobjecttype.Transaction_Line_New__c.getRecordTypeInfosByDeveloperName().get('Journal_Entry').getRecordTypeId(),
                Revenue_Type__c = 'Forecast',
                Shipment_Order__c = shipment.Id,
                Short_Description__c = 'Accrued Revenue',
                Status__c ='Posted',
                Accounting_Period_New__c = accPeriod.get(accountPeriod),
                Type_Code__c = 'Month End');
                transactionLines.add(ciDT);
                
                If((shipment.Miscellaneous_Fee__c != 0 && shipment.Miscellaneous_Fee__c != Null)||(shipment.CI_Miscellaneous_Fee__c != 0 && shipment.CI_Miscellaneous_Fee__c != Null)){
                Transaction_Line_New__c ciAD = new Transaction_Line_New__c(
                Amount__c = (MisFee - CIMisFee ) * -1, 
                Date__c = date.today(),
                Business_Partner__c = shipment.Account__c,
                Description__c = 'Revenue',
                Dimension_1__c = dimension1,
                Dimension_2__c = dimension.get('ADHOC'),
                Dimension_3__c = dimension.get(dimension3),
                GL_Account__c = glAccount.get('110000'),
                RecordTypeId = Schema.Sobjecttype.Transaction_Line_New__c.getRecordTypeInfosByDeveloperName().get('Journal_Entry').getRecordTypeId(),
                Revenue_Type__c = 'Forecast',
                Shipment_Order__c = shipment.Id,
                Short_Description__c = 'Revenue',
                Accounting_Period_New__c = accPeriod.get(accountPeriod),
                Status__c ='Posted',
                Type_Code__c = 'Month End');
                transactionLines.add(ciAD);}
        
                          
                
                If(((shipment.Bank_Fees__c != 0 && shipment.Bank_Fees__c != Null)||(shipment.Handling_and_Admin_Fee__c != 0 && shipment.Handling_and_Admin_Fee__c != Null)||(shipment.CI_Admin_Fee__c != 0 && shipment.CI_Admin_Fee__c != Null)||(shipment.CI_Bank_Fees__c != 0 && shipment.CI_Bank_Fees__c != Null)||(shipment.CI_EOR_and_Export_Compliance_Fee_USD__c != 0 && shipment.CI_EOR_and_Export_Compliance_Fee_USD__c != Null)||(shipment.EOR_and_Export_Compliance_Fee_USD__c != 0 && shipment.EOR_and_Export_Compliance_Fee_USD__c != Null)) && shipment.Service_Type__c== 'EOR') {
                Transaction_Line_New__c ciAD = new Transaction_Line_New__c(
                Amount__c = (eorfeecomb - cieorfeecomb) * -1 , 
                Date__c = date.today(),
                Business_Partner__c = shipment.Account__c,
                Description__c = 'Revenue',
                Dimension_1__c = dimension1,
                Dimension_2__c = dimension.get('EOR'),
                Dimension_3__c = dimension.get(dimension3),
                GL_Account__c = glAccount.get('110000'),
                RecordTypeId = Schema.Sobjecttype.Transaction_Line_New__c.getRecordTypeInfosByDeveloperName().get('Journal_Entry').getRecordTypeId(),
                Revenue_Type__c = 'Forecast',
                Shipment_Order__c = shipment.Id,
                Short_Description__c = 'Revenue',
                Accounting_Period_New__c = accPeriod.get(accountPeriod),
                Status__c ='Posted',
                Type_Code__c = 'Month End');
                transactionLines.add(ciAD);}
                
                
                If(finance != 0 || insurance != 0 || cicashout != 0 || ciliab != 0){
                Transaction_Line_New__c ciFN = new Transaction_Line_New__c(
                Amount__c = ((finance + insurance)-(cicashout + ciliab))* -1, 
                Date__c = date.today(),
                Business_Partner__c = shipment.Account__c,
                Description__c = 'Revenue',
                Dimension_1__c = dimension1,
                Dimension_2__c = dimension.get('FININSURANCE'),
                Dimension_3__c = dimension.get(dimension3),
                GL_Account__c = glAccount.get('110000'),
                RecordTypeId = Schema.Sobjecttype.Transaction_Line_New__c.getRecordTypeInfosByDeveloperName().get('Journal_Entry').getRecordTypeId(),
                Revenue_Type__c = 'Forecast',
                Shipment_Order__c = shipment.Id,
                Accounting_Period_New__c = accPeriod.get(accountPeriod),
                Short_Description__c = 'Revenue',
                Status__c ='Posted',
                Type_Code__c = 'Month End');
                transactionLines.add(ciFN);}
                
                
                If(((shipment.CI_Admin_Fee__c != 0 && shipment.CI_Admin_Fee__c != Null)||(shipment.CI_Bank_Fees__c != 0 && shipment.CI_Bank_Fees__c != Null)||(shipment.CI_IOR_and_Import_Compliance_Fee_USD__c != 0 && shipment.CI_IOR_and_Import_Compliance_Fee_USD__c != Null)||(shipment.Bank_Fees__c != 0 && shipment.Bank_Fees__c != Null)||(shipment.Handling_and_Admin_Fee__c != 0 && shipment.Handling_and_Admin_Fee__c != Null)||(shipment.IOR_FEE_USD__c != 0 && shipment.IOR_FEE_USD__c != Null)) && shipment.Service_Type__c== 'IOR'){
                Transaction_Line_New__c ciIOR = new Transaction_Line_New__c(
                Amount__c = ((iorFee + adminFee + bankfee)-( ciiorfee + cibankfee + ciadminfee))  * -1, 
                Date__c = date.today(),
                Business_Partner__c = shipment.Account__c,
                Description__c = 'Revenue',
                Dimension_1__c = dimension1,
                Dimension_2__c = dimension.get('IOR'),
                Dimension_3__c = dimension.get(dimension3),
                GL_Account__c = glAccount.get('110000'),
                RecordTypeId = Schema.Sobjecttype.Transaction_Line_New__c.getRecordTypeInfosByDeveloperName().get('Journal_Entry').getRecordTypeId(),
                Revenue_Type__c = 'Forecast',
                Shipment_Order__c = shipment.Id,
                Accounting_Period_New__c = accPeriod.get(accountPeriod),
                Short_Description__c = 'Revenue',
                Status__c ='Posted',
                Type_Code__c = 'Month End');
                transactionLines.add(ciIOR);}
                
                                           
                If(oncharges != 0 || cihand !=0 || cibrok != 0 || cilic != 0 || ciclear != 0 ){
                Transaction_Line_New__c ciOn = new Transaction_Line_New__c(
                Amount__c = (oncharges - (cihand + cibrok + cilic + ciclear)) * -1, 
                Date__c = date.today(),
                Business_Partner__c = shipment.Account__c,
                Description__c = 'Revenue',
                Dimension_1__c = dimension1,
                Dimension_2__c = dimension.get('ONCHARGES'),
                Dimension_3__c = dimension.get(dimension3),
                GL_Account__c = glAccount.get('110000'),
                RecordTypeId = Schema.Sobjecttype.Transaction_Line_New__c.getRecordTypeInfosByDeveloperName().get('Journal_Entry').getRecordTypeId(),
                Revenue_Type__c = 'Forecast',
                Shipment_Order__c = shipment.Id,
                Short_Description__c = 'Revenue',
                Accounting_Period_New__c = accPeriod.get(accountPeriod),
                Status__c ='Posted',
                Type_Code__c = 'Month End');
                transactionLines.add(ciOn);}
                
                If((shipment.International_Delivery_Fee__c != 0 && shipment.International_Delivery_Fee__c != Null) ||(shipment.CI_International_Delivery_Fee__c != 0 && shipment.CI_International_Delivery_Fee__c != Null)){
                Transaction_Line_New__c ciFR = new Transaction_Line_New__c(
                Amount__c = (intdel - cidel) * -1, 
                Date__c = date.today(),
                Business_Partner__c = shipment.Account__c,
                Description__c = 'Revenue',
                Dimension_1__c = dimension1,
                Dimension_2__c = dimension.get('SHIPPING'),
                Dimension_3__c = dimension.get(dimension3),
                GL_Account__c = glAccount.get('110000'),
                RecordTypeId = Schema.Sobjecttype.Transaction_Line_New__c.getRecordTypeInfosByDeveloperName().get('Journal_Entry').getRecordTypeId(),
                Revenue_Type__c = 'Forecast',
                Shipment_Order__c = shipment.Id,
                Short_Description__c = 'Revenue',
                 Accounting_Period_New__c = accPeriod.get(accountPeriod),
                Status__c ='Posted',
                Type_Code__c = 'Month End');
                transactionLines.add(ciFR);}
                
                If(taxAmount != 0 || ciTax != 0){
                Transaction_Line_New__c ciFR = new Transaction_Line_New__c(
                Amount__c = (taxAmount- ciTax) * -1, 
                Date__c = date.today(),
                Business_Partner__c = shipment.Account__c,
                Description__c = 'Revenue - Client taxes paid',
                Dimension_1__c = dimension1,
                Dimension_2__c = dimension.get('TAX'),
                Dimension_3__c = dimension.get(dimension3),
                GL_Account__c = glAccount.get('110151'),
                Accounting_Period_New__c = accPeriod.get(accountPeriod),
                RecordTypeId = Schema.Sobjecttype.Transaction_Line_New__c.getRecordTypeInfosByDeveloperName().get('Journal_Entry').getRecordTypeId(),
                Revenue_Type__c = 'Forecast',
                Shipment_Order__c = shipment.Id,
                Short_Description__c = 'Revenue-ClientTax',
                Status__c ='Posted',
                Type_Code__c = 'Month End');
                transactionLines.add(ciFR);}
                
                
            }
                         
            If(((siNegTotal - actualTotal) > 50 || (siNegTotal - actualTotal) < -50) && shipment.Final_Supplier_Invoice_Received__c == FALSE){
                
                
                Transaction_Line_New__c siCT = NEW Transaction_Line_New__c(
                Amount__c = (siNegTotal - actualTotal) * - 1,
                Date__c = date.today(),
                Business_Partner__c = shipment.SupplierlU__c,
                Description__c = 'Accruals',
                GL_Account__c = glAccount.get('921000'),
                RecordTypeId = Schema.Sobjecttype.Transaction_Line_New__c.getRecordTypeInfosByDeveloperName().get('Journal_Entry').getRecordTypeId(),
                Revenue_Type__c = 'Forecast',
                Shipment_Order__c = shipment.Id,
                Short_Description__c = 'Accruals',
                Status__c ='Posted',
                Accounting_Period_New__c = accPeriod.get(accountPeriod),
                Type_Code__c = 'Month End');
                transactionLines.add(siCT);
                
                If((shipment.FC_Miscellaneous_Fee__c != 0 && shipment.FC_Miscellaneous_Fee__c != Null)|| actualmis !=0 ){
                Transaction_Line_New__c siAD = new Transaction_Line_New__c(
                Amount__c = fcmis - actualmis,
                Date__c = date.today(),
                Business_Partner__c = shipment.SupplierlU__c,
                Description__c = 'Cost of Sales',
                Dimension_2__c = dimension.get('ADHOC'),
                GL_Account__c = glAccount.get('120000'),
                RecordTypeId = Schema.Sobjecttype.Transaction_Line_New__c.getRecordTypeInfosByDeveloperName().get('Journal_Entry').getRecordTypeId(),
                Accounting_Period_New__c = accPeriod.get(accountPeriod),
                Revenue_Type__c = 'Forecast',
                Shipment_Order__c = shipment.Id,
                Short_Description__c = 'Cost of Sales',
                Status__c ='Posted',
                Type_Code__c = 'Month End');
                transactionLines.add(siAD);}
                
                      
                
                If(actualcashout != 0 || actualliab!= 0 || actualfin != 0 || actualins != 0 || (shipment.FC_Finance_Fee__c != 0 && shipment.FC_Finance_Fee__c != Null)||(shipment.FC_Insurance_Fee_USD__c != 0 && shipment.FC_Insurance_Fee_USD__c != Null)) {
                Transaction_Line_New__c siAD = new Transaction_Line_New__c(
                Amount__c = (fcfin + fcliab)- (actualcashout + actualliab + actualfin + actualins), 
                Date__c = date.today(),
                Business_Partner__c = shipment.SupplierlU__c,
                Description__c = 'Cost of Sales',
                Dimension_2__c = dimension.get('FININSURANCE'),
                GL_Account__c = glAccount.get('120000'),
                RecordTypeId = Schema.Sobjecttype.Transaction_Line_New__c.getRecordTypeInfosByDeveloperName().get('Journal_Entry').getRecordTypeId(),
                Accounting_Period_New__c = accPeriod.get(accountPeriod),
                Revenue_Type__c = 'Forecast',
                Shipment_Order__c = shipment.Id,
                Short_Description__c = 'Cost of Sales',
                Status__c ='Posted',
                Type_Code__c = 'Month End');
                transactionLines.add(siAD);}
                
                If((shipment.FC_Bank_Fees__c != 0 && shipment.FC_Bank_Fees__c != Null)||(fcior != 0) || fcadmin != 0 || actualadmin != 0 || actualbank != 0 || actualior != 0 ){
                Transaction_Line_New__c siAD = new Transaction_Line_New__c(
                Amount__c = (fcior + fcadmin + fcbank) - (actualadmin + actualbank + actualior), 
                Date__c = date.today(),
                Business_Partner__c = shipment.SupplierlU__c,
                Description__c = 'Cost of Sales',
                Dimension_2__c = dimension.get('IOR'),
                GL_Account__c = glAccount.get('120000'),
                RecordTypeId = Schema.Sobjecttype.Transaction_Line_New__c.getRecordTypeInfosByDeveloperName().get('Journal_Entry').getRecordTypeId(),
                Accounting_Period_New__c = accPeriod.get(accountPeriod),
                Revenue_Type__c = 'Forecast',
                Shipment_Order__c = shipment.Id,
                Short_Description__c = 'Cost of Sales',
                Status__c ='Posted',
                Type_Code__c = 'Month End');
                transactionLines.add(siAD);}
                
                               
                If(onChargeCosts != 0 || actualclear != 0 || actualbrok != 0 || actuallic != 0 || actualhand != 0 ){
                Transaction_Line_New__c siAD = new Transaction_Line_New__c(
                Amount__c = onChargeCosts - (actualclear + actualbrok + actualhand + actuallic), 
                Date__c = date.today(),
                Business_Partner__c = shipment.SupplierlU__c,
                Description__c = 'Cost of Sales',
                Dimension_2__c = dimension.get('ONCHARGES'),
                GL_Account__c = glAccount.get('120000'),
                RecordTypeId = Schema.Sobjecttype.Transaction_Line_New__c.getRecordTypeInfosByDeveloperName().get('Journal_Entry').getRecordTypeId(),
                Accounting_Period_New__c = accPeriod.get(accountPeriod),
                Revenue_Type__c = 'Forecast',
                Shipment_Order__c = shipment.Id,
                Short_Description__c = 'Cost of Sales',
                Status__c ='Posted',
                Type_Code__c = 'Month End');
                transactionLines.add(siAD);}
                
                If((deliveryCost != 0 || actualdel != 0 || deliveryFC != 0)&& shipment.Who_arranges_International_courier__c == 'TecEx'){
                Transaction_Line_New__c siAD = new Transaction_Line_New__c(
                Amount__c = (deliveryCost + deliveryFC) - actualdel , 
                Date__c = date.today(),
                Business_Partner__c = shipment.SupplierlU__c,
                Description__c = 'Cost of Sales',
                Dimension_2__c = dimension.get('SHIPPING'),
                GL_Account__c = glAccount.get('120000'),
                RecordTypeId = Schema.Sobjecttype.Transaction_Line_New__c.getRecordTypeInfosByDeveloperName().get('Journal_Entry').getRecordTypeId(),
                Accounting_Period_New__c = accPeriod.get(accountPeriod),
                Revenue_Type__c = 'Forecast',
                Shipment_Order__c = shipment.Id,
                Short_Description__c = 'Cost of Sales',
                Status__c ='Posted',
                Type_Code__c = 'Month End');
                transactionLines.add(siAD);}
                
                If(actualtax != 0|| taxCost != 0){
                Transaction_Line_New__c siAD = new Transaction_Line_New__c(
                Amount__c = taxCost - actualtax, 
                Date__c = date.today(),
                Business_Partner__c = shipment.SupplierlU__c,
                Description__c = 'Revenue - Client taxes paid',
                Dimension_2__c = dimension.get('TAX'),
                GL_Account__c = glAccount.get('110151'),
                RecordTypeId = Schema.Sobjecttype.Transaction_Line_New__c.getRecordTypeInfosByDeveloperName().get('Journal_Entry').getRecordTypeId(),
                Accounting_Period_New__c = accPeriod.get(accountPeriod),
                Revenue_Type__c = 'Forecast',
                Shipment_Order__c = shipment.Id,
                Short_Description__c = 'Revenue-ClientTax',
                Status__c ='Posted',
                Type_Code__c = 'Month End');
                transactionLines.add(siAD);}
                
                
              
                
            }

        }
        try {
        	// Update the Account Record
            
           // update shipmentOrderList;
           system.debug('transactionLines.size()---->'+transactionLines.size());
            insert transactionLines;
        
        } catch(Exception e) {
            System.debug('error--> '+e);
        }
        
    }   
    
    global void finish(Database.BatchableContext BC) {
    	// execute any post-processing operations
  }
}