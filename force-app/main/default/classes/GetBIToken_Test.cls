@isTest
public class GetBIToken_Test implements HttpCalloutMock {
    
    private static HttpResponse resp;
    
    public GetBIToken_Test(String testBody,Boolean isTest) {
        
        resp = new HttpResponse();
        
        resp.setBody(testBody);
        
        if(isTest == true){
            
            resp.setStatus('OK');
            resp.setStatusCode(200);
            
        }else{
            
            resp.setStatus('Error');
            resp.setStatusCode(404);
            
        }
    }
    
    public HTTPResponse respond(HTTPRequest req) {
        
        return resp;
        
    }
    
    static testMethod void  GetBITokenTest(){
        
        
        
        Account account = new Account (name='Acme1',  Type ='Client',  CSE_IOR__c = '0050Y000001km5c',  Service_Manager__c = '0050Y000001km5c',  Tax_recovery_client__c  = FALSE, Ship_From_Line_1__c = 'Ship_From_Line_1',
                                       Ship_From_Line_2__c = 'Ship_From_Line_2', Ship_From_Line_3__c = 'Ship_From_Line_3', Ship_From_City__c = 'Ship_From_City',  Ship_From_Zip__c = 'Ship_From_Zip',
                                       Ship_From_Country__c = 'Ship_From_Country' ,  Ship_From_Other_notes__c = 'Ship_From_Other', Ship_From_Contact_Name__c = 'Ship_From_Contact', Ship_From_Contact_Email__c = 'Ship_From_Email',
                                       Ship_From_Contact_Tel__c = 'Ship_From_Tel', Client_Address_Line_1__c = 'Client_Address_Line_1', Client_Address_Line_2__c = 'Client_Address_Line_2', Client_Address_Line_3__c = 'Client_Address_Line_3',
                                       Client_City__c = 'Client_City', Client_Zip__c = 'Client_Zip', Client_Country__c = 'Client_Country',  Other_notes__c = 'Other_notes',  Tax_Name__c = 'Tax_Name', Tax_ID__c = 'Tax_ID',
                                       Contact_Name__c = 'Contact_Name', Contact_Email__c = 'Contact_Email', Contact_Tel__c = 'Contact_Tel', Financial_Controller__c = '0050Y000001km5c' ); 
        insert account;
        
        Contact contact1 = new Contact(AccountID =account.Id,LastName='Test');
        
        insert contact1;
        
        String testBody = 'Accesstoken=bc407192-e2bf-4ada-9ddd-dfa2e13aba9c&ContactID='+contact1.Id;
        
        JSONGenerator gen = JSON.createGenerator(true);
        
        gen.writeStartObject();
        gen.writeStringField('Accesstoken', 'bc407192-e2bf-4ada-9ddd-dfa2e13aba9c');
        gen.writeStringField('ContactID', contact1.Id);
        gen.writeEndObject();
        
        String jsonData = gen.getAsString();
        
        
        
        Access_token__c act = new Access_token__c(
            AccessToken_encoded__c='YmM0MDcxOTItZTJiZi00YWRhLTlkZGQtZGZhMmUxM2FiYTlj',
            Status__c = 'Active',
            Access_Token__c='bc407192-e2bf-4ada-9ddd-dfa2e13aba9c'
        );
        insert act;
        
        Power_Bi_Requests__c pbr = new Power_Bi_Requests__c(
            Name = 'PowerBi',
            client_id__c='03e8300e-2059-4580-b1a6-94bee02e8fb5',
            resource__c = 'https://analysis.windows.net/powerbi/api',
            client_secret__c='jKuyGTWB8G9-Q2v4vE1r4-.4e0__-wdo1C',
            grant_type__c='client_credentials',
            datasetId__c='c35a07b8-c419-4782-b2f2-aaf1baaeeeb9',
            reportId__c='107da9e7-57d9-49e6-9f5e-b7b578f2d589',
            WorkSpace_ID__c='10493185-e77b-4d4a-927a-2646fcf11328'
        );
        insert pbr;
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/GetBIToken'; //Request URL
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response= res;
        
        req.addParameter('Accesstoken', 'bc407192-e2bf-4ada-9ddd-dfa2e13aba9c');
        req.addParameter('ContactID', contact1.Id);
        req.requestBody = Blob.valueOf(jsonData);
        
        Test.startTest();
        
        HttpCalloutMock mock = new GetBIToken_Test(jsonData,true);
        
        Test.setMock(HttpCalloutMock.class, mock);
        
        GetBIToken.GetBIToken();
        
        Test.stopTest();
    }
}