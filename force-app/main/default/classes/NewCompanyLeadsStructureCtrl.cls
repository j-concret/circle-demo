public class NewCompanyLeadsStructureCtrl {
    
    public static Boolean changeCompanyOwner = false;
    
    
    @AuraEnabled
    public static masterwrapper getData(String searchCompanyString, String searchUserString){
        
        system.debug('searchCompanyString : '+searchCompanyString);
        
        String searchKey;
        String searchUserKey;
        List<Company__c> filteredCompany = new List<Company__c>();
        Set<Id> filtereduserIds = new Set<Id>();
        if(String.isNotBlank(searchCompanyString) && String.isNotBlank(searchUserString)){
            String searchCompanyKey = '%'+searchCompanyString+'%'; 
             searchUserKey = '%'+searchUserString+'%'; 
            
            Map<Id, User> filteredUserData = new Map<Id, User>([SELECT Id, name FROM user WHERE Name like :searchUserKey order by lastmodifieddate LIMIT 200]);
            filtereduserIds = filteredUserData.keyset();
            Map<Id, Company_User_Assignment__c> filterCompnayFromCUA = new Map<Id, Company_User_Assignment__c>([SELECT Id, Name,Company__c, User__c FROM Company_User_Assignment__c WHERE User__c=:filteredUserData.keySet()]);
            Set<Id> companyIds = new Set<Id>();
            if(filterCompnayFromCUA.size() > 0){
                for(Company_User_Assignment__c cua :filterCompnayFromCUA.values()){
                    companyIds.add(cua.Company__c );
                }
            }
            filteredCompany = [SELECT Id, Name,(SELECT Id, Status FROM Leads__r ORDER BY Status), Allocation_Date__c, Expiry_Date__c, Extra_Leads_Approved__c,
                               High_Ranking_Company__c , Highest_Lead_Status__c, KYC_Ranking_Score__c, 
                               Number_of_Leads__c,Country__c, Strategic_DNC__c,OwnerId,Owner.name, Reseller_Ranking__c FROM Company__c WHERE Name like : searchCompanyKey 
                               AND (Id =: companyIds OR  OwnerId =: filteredUserData.keySet()) order by lastmodifieddate desc limit 200];
            system.debug('First If');
        }else if(String.isNotBlank(searchCompanyString)){
            searchKey = '%'+searchCompanyString+'%'; 
            filteredCompany = [SELECT Id, Name,(SELECT Id, Status FROM Leads__r ORDER BY Status), Allocation_Date__c, Expiry_Date__c, Extra_Leads_Approved__c,
                               High_Ranking_Company__c , Highest_Lead_Status__c, KYC_Ranking_Score__c, 
                               Number_of_Leads__c,Country__c, Strategic_DNC__c,OwnerId,Owner.name, Reseller_Ranking__c FROM Company__c  WHERE Name Like :searchKey order by lastmodifieddate desc limit 200];
            
            system.debug('Second If');
        }else if(String.isNotBlank(searchUserString)){
            searchKey = '%'+searchUserString+'%'; 
            Map<Id, User> filteredUserData = new Map<Id, User>([SELECT Id, name FROM user WHERE Name like :searchKey order by lastmodifieddate LIMIT 200]);
            filtereduserIds = filteredUserData.keyset();
            Map<Id, Company_User_Assignment__c> filterCompnayFromCUA = new Map<Id, Company_User_Assignment__c>([SELECT Id, Name,Company__c, User__c FROM Company_User_Assignment__c WHERE User__c=:filteredUserData.keySet()]);
            Set<Id> companyIds = new Set<Id>();
            if(filterCompnayFromCUA.size() > 0){
                for(Company_User_Assignment__c cua :filterCompnayFromCUA.values()){
                    companyIds.add(cua.Company__c );
                }
            }
            
            filteredCompany = [SELECT Id, Name,(SELECT Id, Status FROM Leads__r ORDER BY Status), Allocation_Date__c, Expiry_Date__c, Extra_Leads_Approved__c,
                               High_Ranking_Company__c , Highest_Lead_Status__c, KYC_Ranking_Score__c, 
                               Number_of_Leads__c,Country__c, Strategic_DNC__c,OwnerId,Owner.name, Reseller_Ranking__c FROM Company__c WHERE Id =: companyIds OR OwnerId =: filteredUserData.keySet() order by lastmodifieddate desc limit 200];
            system.debug('Third If');
        }else{
            filteredCompany = [SELECT Id, Name,(SELECT Id, Status FROM Leads__r ORDER BY Status), Allocation_Date__c, Expiry_Date__c, Extra_Leads_Approved__c,
                               High_Ranking_Company__c , Highest_Lead_Status__c, KYC_Ranking_Score__c, 
                               Number_of_Leads__c,Country__c, Strategic_DNC__c,OwnerId,Owner.name, Reseller_Ranking__c FROM Company__c  order by lastmodifieddate desc limit 200];
            system.debug('Fourth If');
        }
        
        Map<Id, Company__c> companies = new Map<Id, Company__c>(filteredCompany);
        
        
        
        
        List<String> userhasAccessToUpdateLeadCheckbox = new List<String>{'Jacques Booysen', 'Michael Pepper', 'Patrick Buck','Keshav Chhipa'};
            Map<Id,User> MapUserHasAccessToUpdateLeadCheckbox = new Map<Id, User>([SELECT Id,Name FROM User WHERE Name =: userhasAccessToUpdateLeadCheckbox]);
        List<String> userhasAccessToUpdateDNCCheckbox = new List<String>{'Patrick Buck', 'Michael Pepper', 'Jacques Booysen', 'Oliver Nouwens', 'Juadit Rodrigues','Jean Oosthuysen', 'Noa Sussman', 'Cody Pooran', 'Nicholas Souris', 'Claude De Villiers', 'Keshav Domah', 'Jonathan Kaimowitz','Keshav Chhipa'};
            
            Map<Id,User> MapUserHasAccessToUpdateDNCCheckbox = new Map<Id, User>([SELECT Id,Name FROM User WHERE Name =: userhasAccessToUpdateDNCCheckbox]);
        Set<String> companiesName = new Set<String>();
        List<options> allCompanies = new List<options>();
        Set<String> companyNameString = new Set<String>();
        Set<Id> userIds = new Set<Id>();
        for(Company__c comObj : companies.values()){
            options opt = new options();
            opt.value = comObj.Id;
            opt.label = comObj.name;
            if(!companiesName.contains(comObj.name)){
                allCompanies.add(opt);
            }
            userIds.add(comObj.ownerId);
            companiesName.add(comObj.name);
            if(comObj.ownerId != null){
                companyNameString.add(comObj.name);
            }
        } 
        
        system.debug('companiesName-->'+companiesName.size());
        system.debug('searchUserKey :'+searchUserKey);
         List<Company_User_Assignment__c> cuaList = new List<Company_User_Assignment__c>();
        if(String.isNotBlank(searchUserString) && searchUserString != null){
            cuaList = [SELECT Id, User__r.Name, Company__c FROM Company_User_Assignment__c WHERE Company__c =: companies.KeySet() AND User__c =: filtereduserIds LIMIT 100];
        
        }else{
             cuaList = [SELECT Id, User__r.Name, Company__c FROM Company_User_Assignment__c WHERE Company__c =: companies.KeySet() LIMIT 100];
        }
        
        Map<Id, Company_User_Assignment__c> companyUserAssign = new Map<Id, Company_User_Assignment__c>(cuaList);
        List<wrapper> rowResults = new List<wrapper>();
        masterwrapper masterWrap = new masterwrapper();
        
        Map<Id, Company__c> companyToUpdate = new Map<Id,Company__c>();
        
        Set<Id> companiesHasAllocateduser = new Set<Id>();
        
        for(Company_User_Assignment__c comUserAssignm : companyUserAssign.Values()){
            wrapper wrap = new Wrapper();
            wrap.userName = comUserAssignm.User__r.Name;
            wrap.assignedUserID = comUserAssignm.User__c;
            wrap.companyData = companies.get(comUserAssignm.Company__c);
            wrap.noOfLeads = companies.get(comUserAssignm.Company__c).Leads__r.Size();
            if(companies.get(comUserAssignm.Company__c).Leads__r.Size() > 0){
                Integer size = companies.get(comUserAssignm.Company__c).Leads__r.size();
                Lead HighesStatuslead = companies.get(comUserAssignm.Company__c).Leads__r[size - 1];
                wrap.HighestLeadStatus = HighesStatuslead.Status;                
            }
            
            wrap.compnayUserAssignmId = comUserAssignm.Id;
            rowResults.add(wrap);
            // userIds.add(companies.get(comUserAssignm.Company__c).ownerId);
            userIds.add(comUserAssignm.User__c);
            companiesHasAllocateduser.add(companies.get(comUserAssignm.Company__c).Id);
            companyNameString.add(companies.get(comUserAssignm.Company__c).Name);
        }
        
        
        
        List<wrapper> companyWithNoUserAllocation = new List<wrapper>();
        for(Company__c comObj : companies.values()){
            
            if(!companiesHasAllocateduser.contains(comObj.Id)){
                wrapper wrap = new Wrapper();
                wrap.companyData = comObj;
                wrap.noOfLeads = comObj.Leads__r.Size();
                companyWithNoUserAllocation.add(wrap);
            }
        }        
        List<options> companyNamesWithAllocation = new List<options>();
        for(String name : companyNameString){
            options opt = new options();
            opt.value = name;
            opt.label = name;
            companyNamesWithAllocation.add(opt);            
        }
        
        List<options> allocatedTo = new List<options>();
        for(User usr : [SELECT Id, Name FROM USER WHERE Id =: userIds ORDER BY Name]){
            options opt = new options();
            opt.value = usr.Name;
            opt.label = usr.Name;
            allocatedTo.add(opt);            
        }
        system.debug('allocatedTo-->'+allocatedTo.size());
        
        List<options> newUserToAllocate = new List<options>();
        Map<Id,Profile> profileIds = new Map<Id, profile>([SELECT Id, Name FROM Profile WHERE Name = 'TecEx Branch Manager' OR Name = 'TecEx Junior Sales Developer' OR Name = 'TecEx Management']);
        for(User usr : [SELECT Id, Name FROM USER WHERE profileId =: profileIds.keySet() ORDER BY Name]){
            options opt = new options();
            opt.value = usr.Id;
            opt.label = usr.Name;
            newUserToAllocate.add(opt);            
        }
        
        masterWrap.userHasAccessToUpdateDNCCheckbox = false;
        masterWrap.userHasAccessToUpdateLeadCheckbox = false;
        masterWrap.userHasAccessToUpdateOwner = false;
        if(MapUserHasAccessToUpdateLeadCheckbox.containsKey(Userinfo.getUserId())){
            masterWrap.userHasAccessToUpdateLeadCheckbox = true;
        }
        if(MapUserHasAccessToUpdateDNCCheckbox.containsKey(Userinfo.getUserId())){
            masterWrap.userHasAccessToUpdateDNCCheckbox = true;
        }
        String profileName = [SELECT Id, Name FROM Profile WHERE ID =: userinfo.getProfileId() LIMIT 1].Name;
        if(profileName == 'System Administrator' || profileName == 'TecEx IP & Automation Team' || userinfo.getName() == 'Jacques Booysen' || userinfo.getName() == 'Patrick Buck'){
            masterWrap.userHasAccessToUpdateOwner = true;
        }
        masterWrap.wrapperClass = rowResults;
        masterWrap.stausPicklistValues = getPickListValuesIntoList('Lead', 'Status');
        masterWrap.companyNames = companyNamesWithAllocation;
        masterWrap.allocatedTo = allocatedTo;
        masterWrap.newUserToAllocate = newUserToAllocate; 
        masterWrap.allCompanies = allCompanies;
        masterWrap.companyWithNoUserAllocation = companyWithNoUserAllocation;
        // masterWrap.allCompaniesData = companies.values(); 
        // update companyToUpdate.Values();
        return masterWrap;
    }
    
    public static List<options> getPickListValuesIntoList(String objectType, String selectedField){
        List<options> options = new List<options>();
        
        Schema.SObjectType convertToObj = Schema.getGlobalDescribe().get(objectType);
        Schema.DescribeSObjectResult res = convertToObj.getDescribe();
        Schema.DescribeFieldResult fieldResult = res.fields.getMap().get(selectedField).getDescribe();
        
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry pickListVal : ple){
            
            options opt = new options();
            opt.label =pickListVal.getLabel();
            opt.value=pickListVal.getValue();
            options.add(opt);
        } 
        return options;
    }
    
    @AuraEnabled
    public static Map<String, Object> updateRecords(String dataToSave){
        NewCompanyLeadsStructureCtrl.changeCompanyOwner = true;
        Map<String, Object> result = new Map<String, Object>();
        List<Wrapper> listWrapper = (List<Wrapper>) JSON.deserialize(dataToSave, List<Wrapper>.Class);
        Set<Company__c> listToUpdate = new Set<Company__c>();
        List<Wrapper> recordToUpdateWithNewUser = new List<Wrapper>();
        List<Wrapper> recordToUpdateWithNewOwner = new List<Wrapper>();
        try{
            if(!listWrapper.isEmpty()){
                
                for(Wrapper record : listWrapper){
                    listToUpdate.add(record.companyData);
                    if(record.selectedNewUser != null){
                        recordToUpdateWithNewUser.add(record);
                    }
                    if(record.newCompanyOwner != null){
                        recordToUpdateWithNewOwner.add(record);
                    }
                }
                
                if(!listToUpdate.isEmpty()){
                    update new List<Company__c>(listToUpdate);
                }
                
                List<Company_User_Assignment__c> compnayUserAssignmToUpdate = new List<Company_User_Assignment__c>(); 
                for(Wrapper wrapperObj : recordToUpdateWithNewUser){
                    Company_User_Assignment__c compnayUserAssignm = new Company_User_Assignment__c(Id = wrapperObj.compnayUserAssignmId,
                                                                                                   User__c = wrapperObj.selectedNewUser.Id);
                    compnayUserAssignmToUpdate.add(compnayUserAssignm);                
                }
                system.debug('compnayUserAssignmToUpdate : '+compnayUserAssignmToUpdate);
                if(!compnayUserAssignmToUpdate.isEmpty()){
                    update compnayUserAssignmToUpdate;
                }
                
                List<Company__c> updateCompanyWithNewOwner = new List<Company__c>();
                for(Wrapper wrapperObj : recordToUpdateWithNewOwner){
                    Company__c companyObj = new Company__c(Id = wrapperObj.companyData.Id, OwnerId = wrapperObj.newCompanyOwner.Id, Allocation_Date__c = Date.today());
                    updateCompanyWithNewOwner.add(companyObj);
                }
                if(!updateCompanyWithNewOwner.isEmpty()){
                    update updateCompanyWithNewOwner;
                }
                
            }
            result.put('status', 'Ok');            
        }catch (exception e){
            result.put('error', e);
        }
        return result;
    }
    
    @AuraEnabled
    public static Map<String, Object> insertCompanyUserAssignM(String dataToInsert, List<String> userNames){
        Map<String, Object> result = new Map<String, Object>();
        
        List<Wrapper> listWrapper = (List<Wrapper>) JSON.deserialize(dataToInsert, List<Wrapper>.Class);
        
        Set<Company__c> companies = new Set<Company__c>();
        
        try{
            if(!listWrapper.isEmpty()){
                for(Wrapper record : listWrapper){
                    companies.add(record.companyData);
                }
                
                List<Company_User_Assignment__c> lstToInsert = new List<Company_User_Assignment__c>();
                for(Company__c companyObj : companies){
                    for(String user : userNames){
                        Company_User_Assignment__c companyUserAssignM = new Company_User_Assignment__c(
                            USer__c = user, Company__c = companyObj.Id);
                        lstToInsert.add(companyUserAssignM);
                    }
                }
                insert lstToInsert;
                result.put('status', 'Ok');
            }  
        }catch(exception e){
            result.put('error', e.getMessage());                
        }
        return result;
    }
    
    //User Limits
    //
    
    @AuraEnabled
    public static Map<String, Object> getAllUsers(){
        
        Map<String, Object> dataToReturn = new Map<String, Object>();
        
        List<options> usersName = new List<options>();
        
        List<UserWrapper> userWrpList = new List<UserWrapper>();
        
        List<Profile> profileMap = [SELECT Id FROM Profile where Name IN ('TecEx Branch Manager', 'TecEx Junior Sales Developer')];
        
        List<User> userList = [Select Id, Name, Number_of_Companies_to_Create__c, Number_of_Companies_to_be_Allocated__c from User where IsActive = true AND ProfileId IN :profileMap ORDER BY Name];
        List<String> statusToExclude = new List<String>{'Meeting Held', 'Meeting Held Contact at Future Date', 'Contact at Future Date', 'Meeting Result = Contact at Future Date', 'Contract Pending', 'Qualified', 'Contract Signed', 'Client Signed', 'Subsidiary Signed - Do Not Contact','Qualified Lead'};
            //  List<String> statusToExclude = new List<String>{'Contract Pending', 'Qualified', 'Contract Signed', 'Client Signed'};
            Map<Id, Company_User_Assignment__c> mapCUACompanyWithExcludesStatus = new map<Id, Company_User_Assignment__c>([SELECT Id, Company__r.Highest_Lead_Status__c FROM Company_User_Assignment__c WHERE Company__r.Highest_Lead_Status__c NOT IN : statusToExclude]);      
        
        //   Map<Id, AggregateResult> userToCompanyAllocatedMap = new Map<Id,AggregateResult>([Select User__c Id, Count(Id) total from Company_User_Assignment__c WHERE Id =: mapCUACompanyWithExcludesStatus.KeySet() Group By User__c]);
        Set<Id> companyIds = new Set<id>();
        Map<Id, Company__c> companyMap = new Map<Id, Company__c>([SELECT Id, OwnerId, CreatedById FROM Company__c LIMIT 49000]);
        for(Company__c com : companyMap.values()){
            if(com.ownerId != com.createdbyid){
                companyIds.add(com.Id);
            }
        }
        Map<Id, AggregateResult> userToCompanyCreatedMap = new Map<Id,AggregateResult>([Select CreatedById Id, count(Id) createdIds from Company__c WHERE Highest_Lead_Status__c NOT IN : statusToExclude AND Exhausted__c = false AND Id NOT IN : companyIds  Group By CreatedById]);
        Map<Id, AggregateResult> userToCompanyOwnerId = new Map<Id,AggregateResult>([Select OwnerId Id, count(Id) ownerIds from Company__c WHERE Highest_Lead_Status__c NOT IN : statusToExclude AND Exhausted__c = false Group By OwnerId]);
        
        
        for(User us : userList){
            UserWrapper obj = new UserWrapper();
            obj.userName = us.Name;
            obj.allocationLimit = us.Number_of_Companies_to_be_Allocated__c != null ? us.Number_of_Companies_to_be_Allocated__c : 0;
            obj.creationLimit = us.Number_of_Companies_to_Create__c != null ? us.Number_of_Companies_to_Create__c: 0;
            //   obj.companyAllocated = userToCompanyAllocatedMap.containsKey(us.Id) ? (Double)userToCompanyAllocatedMap.get(us.Id).get('total') : 0;
            /*   if(userToCompanyCreatedMap.containsKey(us.Id) && userToCompanyOwnerId.containsKey(us.Id)){
obj.companyAllocated = (Double)userToCompanyOwnerId.get(us.Id).get('ownerIds') - (Double)userToCompanyCreatedMap.get(us.Id).get('createdIds') ;
}else if(userToCompanyOwnerId.containsKey(us.Id)){
obj.companyAllocated = (Double)userToCompanyOwnerId.get(us.Id).get('ownerIds');
system.debug('obj.companyAllocated :'+obj.companyAllocated);
system.debug('us.Id :'+us.Id);
}else if(userToCompanyCreatedMap.containsKey(us.Id)){
obj.companyAllocated = 0 -(Double)userToCompanyCreatedMap.get(us.Id).get('createdIds');
}else{
obj.companyAllocated = 0;
}
*/
            obj.companyAllocated = userToCompanyOwnerId.containsKey(us.Id) ? (Double)userToCompanyOwnerId.get(us.Id).get('ownerIds') : 0;
            obj.companyCreated = userToCompanyCreatedMap.containsKey(us.Id) ? (Double)userToCompanyCreatedMap.get(us.Id).get('createdIds') : 0;
            obj.userId = us.Id;
            userWrpList.add(obj);
            
            options opt = new options();
            opt.label = us.Name;
            opt.value = us.Name;
            usersName.add(opt);
        }
        
        dataToReturn.put('userNameList', usersName);
        dataToReturn.put('usersData', userWrpList);
        
        
        return dataToReturn;
        
    }
    
    @AuraEnabled
    public static void updateUsers(String usersData){
        
        List<User> userToUpdate = new List<User>();    
        
        List<UserWrapper> userList = (List<UserWrapper>)JSON.deserialize(usersData,List<UserWrapper>.class);
        
        for(UserWrapper userWr : userList){
            
            User us = new user();
            us.Id = userWr.userId;
            us.Number_of_Companies_to_be_Allocated__c = userWr.allocationLimit;
            us.Number_of_Companies_to_Create__c = userWr.creationLimit;
            
            userToUpdate.add(us);
            
        } 
        if(!userToUpdate.isempty()){
            UPDATE userToUpdate;       
        }
        
        
    }
    
    
    
    public class masterwrapper{
        @AuraEnabled
        public List<Wrapper> wrapperClass;
        @AuraEnabled
        public List<options> stausPicklistValues;
        @AuraEnabled
        public List<options> companyNames;
        @AuraEnabled
        public List<options> allocatedTo;
        @AuraEnabled
        public List<options> newUserToAllocate;
        @AuraEnabled
        public List<options> allCompanies;
        @AuraEnabled
        public List<Company__c> allCompaniesData;
        @AuraEnabled
        public List<Wrapper> companyWithNoUserAllocation;
        @AuraEnabled
        public Boolean userHasAccessToUpdateDNCCheckbox;
        @AuraEnabled
        public Boolean userHasAccessToUpdateLeadCheckbox;
        @AuraEnabled
        public Boolean userHasAccessToUpdateOwner;
    }
    public class wrapper{
        @AuraEnabled
        public String userName;
        @AuraEnabled
        public String assignedUserID;
        @AuraEnabled
        public Company__c companyData;
        @AuraEnabled
        public String HighestLeadStatus;
        @AuraEnabled
        public Integer noOfLeads;
        @AuraEnabled
        public selectedNewUser selectedNewUser;
        @AuraEnabled
        public selectedNewUser newCompanyOwner;
        @AuraEnabled
        public Id compnayUserAssignmId;
    }
    
    public class options{
        @AuraEnabled
        Public String value;
        @AuraEnabled
        public String label;
    }
    
    public class selectedNewUser{
        @AuraEnabled
        Public String Id;
        @AuraEnabled
        public String Name;
    }
    
    
    public class UserWrapper {
        
        @AuraEnabled 
        public String userName;
        @AuraEnabled 
        public Double allocationLimit;
        @AuraEnabled 
        public Double creationLimit;
        @AuraEnabled 
        public Double companyAllocated;
        @AuraEnabled 
        public Double companyCreated;
        @AuraEnabled 
        public String userId;
    }
    
    
    
}