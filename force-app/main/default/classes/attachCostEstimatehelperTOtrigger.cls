public class attachCostEstimatehelperTOtrigger {
   
    public static  boolean firstRun = true;

    public attachCostEstimatehelperTOtrigger(ApexPages.StandardController controller) {

    }

public shipment_order__C sorecord {set;get;}
    

    @Future(callout=true)
    public static void sendEmailWithAttachment(List<id> listofQuoteHeader)
    {
       Shipment_Order__C  SO =  [ Select Id,Name,Cost_Estimate_attached__c,Shipping_Status__c,Cost_Estimate_Number__c,Expiry_Date__c,Shipment_Value_USD__c,IOR_FEE_USD__c,Total_Customs_Brokerage_Cost__c,Total_clearance_Costs__c,Recharge_Handling_Costs__c,Recharge_License_Cost__c,Handling_and_Admin_Fee__c,Bank_Fees__c,International_Delivery_Fee__c,Insurance_Fee_USD__c,Tax_Cost__c,Total_Invoice_Amount_Excl_Taxes_Duties__c,Chargeable_Weight__c,Shipping_Notes_Formula__c, 
       New_Structure__c, Shipping_Notes_New__c FROM Shipment_Order__c where Id =: listofQuoteHeader];
  system.debug('SHipmentOrderrecord'+SO);
    
                  
          
           
            //PageReference pref= page.attachCostEstimatePDF3;
            PageReference pref= page.attachCostEstimatePDFVF2;
              // PageReference pref = new PageReference('/apex/attachCostEstimatePDFVF');
               pref.getParameters().put('id',SO.Id);
               //pref.getParameters().put('id',(Id)QuoteHeaderid);
               pref.setRedirect(true);
               
               Attachment attachment = new Attachment();      
               Blob b;
               
                 try {
               
                   b = pref.getContentAsPDF();
                   
                   system.debug('Body--->'+ b);  
   
        } catch (VisualforceException e) {
            system.debug('in the catch block');
             b= Blob.valueOf('Some Text');
        }
               
             
               
             //  Blob b=pref.getContentAsPDF();
                           
               attachment.Body = b;
               
               If(SO.Shipping_Status__c == 'Cost Estimate'){               
               attachment.Name = 'CostEstimate' +so.name + '.pdf';
               }
               If(So.Shipping_Status__c == 'Shipment Pending'){               
               attachment.Name = 'Approved CostEstimate' +so.name + '.pdf';
               }
               attachment.IsPrivate = false;
               attachment.ParentId = SO.Id;
              
               try{
               insert attachment;
               so.Cost_Estimate_attached__c = TRUE;
               Update SO;    
               }
               
               catch(Exception e){}
               
              
           
           
    }
    
    //###########################################begining of code added by Perfect########################################
    @Future(callout=true)
    public static void sendEmailWithAttachment2(List<id> listofQuoteHeader)
    {
       Shipment_Order__C  SO =  [ Select Id,Name,Cost_Estimate_attached__c,Shipping_Status__c,Cost_Estimate_Number__c,Expiry_Date__c,Shipment_Value_USD__c,IOR_FEE_USD__c,Total_Customs_Brokerage_Cost__c,Total_clearance_Costs__c,Recharge_Handling_Costs__c,Recharge_License_Cost__c,Handling_and_Admin_Fee__c,Bank_Fees__c,International_Delivery_Fee__c,Insurance_Fee_USD__c,Tax_Cost__c,Total_Invoice_Amount_Excl_Taxes_Duties__c,Chargeable_Weight__c,Shipping_Notes_Formula__c, 
       New_Structure__c, Shipping_Notes_New__c FROM Shipment_Order__c where Id =: listofQuoteHeader];
  system.debug('SHipmentOrderrecord'+SO);
    
                  
          
           
            PageReference pref= page.attachCostEstimatePDFVF2;
              // PageReference pref = new PageReference('/apex/attachCostEstimatePDFVF');
               pref.getParameters().put('id',SO.Id);
               //pref.getParameters().put('id',(Id)QuoteHeaderid);
               pref.setRedirect(true);
               
               Attachment attachment = new Attachment();      
               Blob b;
               
                 try {
               
                   b = pref.getContentAsPDF();
                   
                   system.debug('Body--->'+ b);  
   
        } catch (VisualforceException e) {
            system.debug('in the catch block');
             b= Blob.valueOf('Some Text');
        }
               
             
               
             //  Blob b=pref.getContentAsPDF();
                           
               attachment.Body = b;
               
               If(SO.Shipping_Status__c == 'Cost Estimate'){               
               attachment.Name = 'CostEstimate' +so.name + '.pdf';
               }
               If(So.Shipping_Status__c == 'Shipment Pending'){               
               attachment.Name = 'Approved CostEstimate' +so.name + '.pdf';
               }
               attachment.IsPrivate = false;
               attachment.ParentId = SO.Id;
              
               try{
               insert attachment;
               so.Cost_Estimate_attached__c = TRUE;
               Update SO;    
               }
               
               catch(Exception e){}
               
           }
 //###########################################End of code added by Perfect########################################

}