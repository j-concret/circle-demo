@RestResource(urlMapping='/GetAllcases/*')
Global class NCPGetAllcases {
    
     @Httppost
    global static void NCPSORelatedObjects(){
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        Blob body = req.requestBody;
    	res.addHeader('Content-Type', 'application/json');
        String requestString = body.toString();
        NCPGetAllcasesWra rw  = (NCPGetAllcasesWra)JSON.deserialize(requestString,NCPGetAllcasesWra.class);
        try{ List<Access_token__c> at = [SELECT status__c,Access_Token__c FROM Access_token__c WHERE Access_Token__c =: rw.Accesstoken and status__c = 'Active' LIMIT 1];

            if(!at.isEmpty()) {
                  String jsonData='Test';
                Set<ID> CaseIds = New set<ID>();
                Set<ID> Ids = New set<ID>();
                System.debug('rw.ContactID : '+rw.ContactID);
                List<Case> Cs = [select Id,subject,Assigned_to__c,createddate,Assigned_to__r.name,CaseNumber,FullphotoURL__C,status,description,parentID,OwnerId,Owner.name,LastClientMsgTime__c,Last_Client_viewed_time__c,LastMessageTime__c,shipment_order__c,shipment_order__r.name,shipment_order__r.Shipping_Status__c,shipment_order__r.NCP_Quote_Reference__c,shipment_order__r.RecordType.name,shipment_order__r.Client_Reference__c,shipment_order__r.Closed_Down_SO__c FROM Case where Contactid =:rw.ContactID ]; 
                System.debug('Cs :'+Cs.Size());
                For(Integer i =0;Cs.size()>i;i++){CaseIds.add(Cs[i].ID);}  
                List<Attachment> Attmnts = [select Id,Name,ParentId,Body,BodyLength,ContentType, Parent.Type FROM Attachment where ParentId in :Cs ];
                 
                	List<EntitySubscription> Subscribers = new  List<EntitySubscription>([select SubscriberId,parentid  from EntitySubscription where  parentid IN: CaseIds ]);
                    For(Integer i =0;Subscribers.size()>i;i++){Ids.add(Subscribers[i].SubscriberId);}
                 //   list<User> Nofifyusers =new list<User>([Select id, Firstname,Lastname from user where id in:Ids ]);  
                   
                map<ID,User> Nofifyusers = new Map<ID,User> ([Select id, Firstname,Lastname from user where id in:Ids]);    
             
             JSONGenerator gen = JSON.createGenerator(true);
              gen.writeStartObject();
        //      gen.writeFieldName('CaseDetails');
        	System.debug('Cs : '+Cs);
            If(!Cs.isEmpty()){                               
              //Creating Object - Cases Id,subject,CaseNumber,parentID,OwnerId,Owner.name 
             gen.writeFieldName('Cases');
             	gen.writeStartArray();
     		         for(Case Cs1 :Cs){
                         //Start of Object - Final Delivery
        					gen.writeStartObject();
        				    gen.writeStringField('Id', Cs1.Id);System.debug('name-->'+CS1.Owner.name);
                       //Case Fields
                         if(cs1.Id== null) {gen.writeNullField('CaseNumber');} else{gen.writeStringField('CaseNumber', cs1.CaseNumber);}
                         if(cs1.subject== null) {gen.writeNullField('subject');} else{gen.writeStringField('subject', cs1.subject);}
                         if(cs1.OwnerId== null) {gen.writeNullField('OwnerId');} else{gen.writeStringField('OwnerId', cs1.OwnerId);}
                         if(cs1.Id== null) {gen.writeNullField('Ownername');} else{gen.writeStringField('Ownername', CS1.Owner.name);}
                         if(cs1.Id== null) {gen.writeNullField('CreatedDate');} else{gen.writeDatetimeField('CreatedDate', CS1.CreatedDate);}
               			 if( CS1.Status== null) {gen.writeNullField('Status');} else{gen.writeStringField('Status', CS1.Status);}
                         if(CS1.Description== null) {gen.writeNullField('Description');} else{gen.writeStringField('Description', CS1.Description);}
                         if(CS1.LastClientMsgTime__c== null) {gen.writeNullField('LatestClientCommentedtime');} else{gen.writeDateTimeField('LatestClientCommentedtime', CS1.LastClientMsgTime__c);}
                         if(CS1.LastMessageTime__c== null) {gen.writeNullField('LatestInternalCommentedtime');} else{gen.writeDateTimeField('LatestInternalCommentedtime', CS1.LastMessageTime__c);}
                         if(CS1.Last_Client_viewed_time__c== null) {gen.writeNullField('LatestClientCaseViewedTime');} else{gen.writeDateTimeField('LatestClientCaseViewedTime', CS1.Last_Client_viewed_time__c);}
                         if(CS1.Assigned_to__c== null) {gen.writeNullField('AssignedtoId');} else{gen.writeStringField('AssignedtoId', CS1.Assigned_to__c);}
                         if(CS1.Assigned_to__c== null) {gen.writeNullField('AssignedtoName');} else{gen.writeStringField('AssignedtoName', CS1.Assigned_to__r.name);}
                         if(CS1.FullphotoURL__C== null) {gen.writeNullField('FullphotoURL');} else{gen.writeStringField('FullphotoURL', CS1.FullphotoURL__C);}
                       
                         if(cs1.shipment_order__c !=null){  
                       //Shipment order Fields
                       		if(cs1.shipment_order__c== null) {gen.writeNullField('SOID');} else{gen.writeStringField('SOID', cs1.shipment_order__c);}
                       		if(cs1.shipment_order__r.name== null) {gen.writeNullField('SOName');} else{gen.writeStringField('SOName', cs1.shipment_order__r.name);}
                         	if(cs1.shipment_order__r.Shipping_Status__c== null) {gen.writeNullField('SOShippingStatus');} else{gen.writeStringField('SOShippingStatus', cs1.shipment_order__r.Shipping_Status__c);}
                            if(cs1.shipment_order__r.NCP_Quote_Reference__c== null) {gen.writeNullField('NCPQuoteReference');} else{gen.writeStringField('NCPQuoteReference', cs1.shipment_order__r.NCP_Quote_Reference__c);}
                            if(cs1.shipment_order__r.RecordType.name== null) {gen.writeNullField('RecordTypename');} else{gen.writeStringField('RecordTypename', cs1.shipment_order__r.RecordType.name);}
                         	if(cs1.shipment_order__r.Client_Reference__c== null) {gen.writeNullField('ClientReference');} else{gen.writeStringField('ClientReference', cs1.shipment_order__r.Client_Reference__c);}
                            if(cs1.shipment_order__r.Closed_Down_SO__c== null) {gen.writeNullField('ClosedDownSO');} else{gen.writeBooleanField('ClosedDownSO', cs1.shipment_order__r.Closed_Down_SO__c);}
                         }
                        
            If(!Attmnts.isEmpty()){
             //Creating Array & Object - Attachements
             gen.writeFieldName('Attachments');
             	gen.writeStartArray();
     		         for(Attachment Attmnts1 :Attmnts){
                         IF(Attmnts1.ParentId ==Cs1.Id){
        				//Start of Object - Attachements	
                         gen.writeStartObject();
                          
                          if(Attmnts1.Id == null) {gen.writeNullField('Id');} else{gen.writeStringField('Id', Attmnts1.Id);}
                          if(Attmnts1.Name == null) {gen.writeNullField('Name');} else{gen.writeStringField('Name', Attmnts1.Name);}
                          if(Attmnts1.ContentType == null) {gen.writeNullField('ContentType');} else{gen.writeStringField('ContentType', Attmnts1.ContentType);}
                         
                                    
        					gen.writeEndObject();
                         //End of Object - Attachements
                     }	
                     }
    		gen.writeEndArray();
                //End of Array - Attachementse 
         }   
              
             // Place holders for Participants           
                         
  				 If(!Subscribers.isEmpty()){
                     //Creating Object - Nofifyusers
                     gen.writeFieldName('Participants');
                     gen.writeStartArray();
                     for(EntitySubscription Subscribers1 :Subscribers) {
                         //Start of Object - Nofifyusers
                       
                         IF(Subscribers1.parentid ==Cs1.Id  && Subscribers1.id !=null ){
                               gen.writeStartObject();
                      
                         if(Nofifyusers.get(Subscribers1.SubscriberId) == null) {gen.writeNullField('ID');} else{gen.writeStringField('ID', Nofifyusers.get(Subscribers1.SubscriberId).ID  );}
                         if(Nofifyusers.get(Subscribers1.SubscriberId).Firstname== null) {gen.writeNullField('FirstName');} else{gen.writeStringField('Firstname', Nofifyusers.get(Subscribers1.SubscriberId).Firstname  );}
                         if(Nofifyusers.get(Subscribers1.SubscriberId).Lastname == null) {gen.writeNullField('Lastname');} else{gen.writeStringField('Lastname', Nofifyusers.get(Subscribers1.SubscriberId).Lastname  );}
						 gen.writeEndObject();
                         //End of Object - Nofifyusers
                         } 
                        
                         }
                        
                     

                     gen.writeEndArray();
                     //End of Array - Nofifyusers

                 }
                              
       					
                         
                         gen.writeEndObject();
                         //End of Object - 
    					}
                     
    		gen.writeEndArray();
                //End of Array - 
                                                                                             
            } 
                
                 
                 
                
                
              
                
                
                
    		gen.writeEndObject();
    		jsonData = gen.getAsString();
               
                  res.responseBody = Blob.valueOf(jsonData);
        	 	  res.statusCode = 200;
                
                API_Log__c Al = New API_Log__c();
            //    Al.Account__c=rw.AccountID;
                Al.EndpointURLName__c='GetAllcases';
                Al.Response__c='Success - GetAllcases';
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al;
               
           
            }
            
            
            
        }
        Catch(Exception e){System.debug('Error-->'+e.getMessage()+' at line number: '+e.getLineNumber());
            String ErrorString ='Something went wrong please contact SFsupport@tecex.com'+e.getMessage()+e.getLineNumber() ;
               		res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
            		res.addHeader('Content-Type', 'application/json');
        			res.statusCode = 400;
                
                API_Log__c Al = New API_Log__c();
              //  Al.Account__c=rw.AccountID;
                Al.EndpointURLName__c='GetAllcases';
                Al.Response__c=e.getMessage()+' at line number: '+e.getLineNumber();
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al; 
            
        }
    
    
    
    }


}