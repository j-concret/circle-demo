@isTest

private class TestCostEstimateAutoCreation

{

static testMethod void TestCostEstimateAutoCreation()
{
    Date todaysDate = System.today();
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
        
        //Create  & Insert Test User
  User u = new User(Alias = 'standt', Email='standarduser@sureswipe.com',
                  EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
                  LocaleSidKey='en_US', ProfileId = p.Id,
                  TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@sureswipe.com'); 
        System.runAs(u)
           
        {
          //Create and Insert Account
 Account account = new Account (name='Acme6', Type ='Client', CSE_IOR__c = '0050Y000001LTZO');
  insert account; 
   
   Contact contact = new Contact ( lastname ='Testing Person',  AccountId = account.Id);
   insert contact; 
   
   country_price_approval__c cpa = new country_price_approval__c( name ='Australia', Billing_term__c = 'DAP/CIF - IOR pays',  Airwaybill_Instructions__c = 'User IOR Address');
        insert cpa;
        
        IOR_Price_List__c iorpl = new IOR_Price_List__c ( Client_Name__c = account.Id, Name = 'Australia', IOR_Fee__c = 0.15, On_Charge_Mark_up__c = 0.15,
        TecEx_Shipping_Fee_Markup__c =0.40, IOR_Min_Fee__c = 100,  Admin_Fee__c =100, Set_up_fee__c =100, Bank_Fees__c =200, Tax_Rate__c = 0.20,
        Estimated_Customs_Brokerage__c =250, Estimated_Customs_Clearance__c =250, Estimated_Customs_Handling__c =250, Estimated_Import_License__c =250);
        insert iorpl; 
   
   Roll_Out__c rollout = new Roll_Out__c( Client_Name__c = account.Id,   Contact_For_The_Quote__c = contact.Id, Create_Roll_Out__c = False, Destinations__c = 'Australia',  Shipment_Value_in_USD__c = 1000,  Client_Reference__c = 'Testing 123');
   insert rollout;

//Update Asset status to swapped
           
rollout.Create_Roll_Out__c = True;
update rollout; 
           

        }
    }
}