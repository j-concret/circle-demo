@RestResource(urlMapping='/UpdateSOPackage/*')
Global class NCPUpdateSOPackage {
    
    @Httppost
    global static void NCPUpdateSOPackage(){
        
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        Blob body = req.requestBody;
        res.addHeader('Content-Type', 'application/json');
        String requestString = body.toString();
        NCPUpdateSOPackageWra rw = (NCPUpdateSOPackageWra)JSON.deserialize(requestString,NCPUpdateSOPackageWra.class);
        try {
            Access_token__c At =[select status__c,Access_Token__c from Access_token__c where Access_Token__c=:rw.Accesstoken ];
            if( at.status__C =='Active'){
                List<ID>SOPIDs = new list<ID>();
                list<Shipment_Order_Package__c> SOPs = new list<Shipment_Order_Package__c>();
                For(Integer i=0;rw.sop.size()>i;i++){ SOPIDs.add(rw.sop[i].SOPID);}
                list<Shipment_Order_Package__c> SOP = [select id,Weight_Unit__c,Lithium_Batteries__c,Dimension_Unit__c,packages_of_same_weight_dims__c,Length__c,Height__c,Breadth__c,Actual_Weight__c from Shipment_Order_Package__c where id in:SOPIDs];

                For(Integer i=0;sop.size()>i;i++){
                    for(Integer j=0;rw.sop.size()>j;j++){
                        if( SOP[i].id == rw.sop[j].SOPID ){
                            SOP[i].Weight_Unit__c= rw.sop[j].Weight_Unit;
                            SOP[i].Dimension_Unit__c = rw.sop[j].Dimension_Unit;
                            SOP[i].packages_of_same_weight_dims__c =rw.sop[j].Packages_of_Same_Weight;
                            SOP[i].Length__c=decimal.Valueof(rw.sop[j].Length);
                            SOP[i].Height__c=decimal.Valueof(rw.sop[j].Height);
                            SOP[i].Breadth__c=decimal.Valueof(rw.sop[j].Breadth);
                            SOP[i].Actual_Weight__c=decimal.Valueof(rw.sop[j].Actual_Weight);
                            SOP[i].Lithium_Batteries__c=rw.sop[j].LithiumBatteries !=null ? rw.sop[j].LithiumBatteries : false;
                            SOP[i].Contains_Batteries__c=rw.sop[j].Contains_Batteries !=null ? rw.sop[j].Contains_Batteries : false;
                            SOP[i].ION_PI966__c = rw.sop[j].ION_PI966 != null ? rw.sop[j].ION_PI966 : false;
                            SOP[i].ION_PI967__c = rw.sop[j].ION_PI967 != null ? rw.sop[j].ION_PI967 : false;
                            SOP[i].Metal_PI969__c = rw.sop[j].Metal_PI969 != null ? rw.sop[j].Metal_PI969 : false;
                            SOP[i].Metal_PI970__c = rw.sop[j].Metal_PI970 != null ? rw.sop[j].Metal_PI970 : false;
                            SOP[i].Dangerous_Goods__c = rw.sop[j].DangerousGoods != null ? rw.sop[j].DangerousGoods : false;
                            SOPs.add(SOP[i]);
                        }
                    }
                    
                }
                
                Update SOPs;
                
                JSONGenerator gen = JSON.createGenerator(true);
                gen.writeStartObject();
                
                gen.writeFieldName('Success');
                gen.writeStartObject();
                
                if(SOPs.size()<1) {gen.writeNullField('Response');} else{gen.writeStringField('Response', 'Success-Shipment order package updated');}
                
                gen.writeEndObject();
                gen.writeEndObject();
                String jsonData = gen.getAsString();
                res.responseBody = Blob.valueOf(jsonData);
                res.addHeader('Content-Type', 'application/json');
                res.statusCode = 200;        
                
                
                API_Log__c Al = New API_Log__c();
                Al.Account__c=rw.AccountID;
                Al.Login_Contact__c=rw.ContactID;
                Al.EndpointURLName__c='NCP-UpdateSOOrderPackage';
                Al.Response__c='Success- Shipment Order Package Updated';
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al; 
            }
        }
        catch (Exception e){
            String ErrorString ='Something went wrong while updating Shipment order packages details, please contact support_SF@tecex.com';
           // String ErrorString =e.getLineNumber()+e.getLineNumber()+e.getStackTraceString();
            
            res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
            res.addHeader('Content-Type', 'application/json');
            res.statusCode = 500;
            API_Log__c Al = New API_Log__c();
            Al.Account__c=rw.AccountID;
            Al.Login_Contact__c=rw.ContactID;
            Al.EndpointURLName__c='UpdateSOOrderPackage';
            Al.Response__c=e.getMessage()+' at line number: '+e.getLineNumber();
            Al.StatusCode__c=string.valueof(res.statusCode);
            Insert Al;
        }
    }
    
}