public class QueueableInsertSOP implements Queueable{
    
    @testVisible
    private static Boolean doChainJob = true;
    
    List<shipment_Order__c> SOList;
    List<Shipment_Order_Package__c> SOPList;
    String PAList;
    
    
    public QueueableInsertSOP(List<shipment_Order__c> SO,List<Shipment_Order_Package__c> SOP,String PA){
        this.SOList = SO;
        this.SOPList = SOP;
        this.PAList = PA;
        
    }
    
    
    public void execute(QueueableContext context) { 
        list<Shipment_Order_Package__c> SOPL = new list<Shipment_Order_Package__c>();
         list<Part__c> PAL = new list<Part__c>();  
        
        for(Integer l=0; l<SOList.size();l++) {
                        				insert SOList[l];
                        					}
      	List<shipment_Order__c> SOList1 = new List<shipment_Order__c>();
        SOList1=SOList;
        
        
        
        
        for(Integer i=0; i<=SOList1.size();i++) {
                 if(i<SOList1.size()){
                  
                    
                      for(Integer l=0; SOPList.size()>l;l++) {
                         //Create shipmentOrder Packages
                             Shipment_Order_Package__c SOP = new Shipment_Order_Package__c(
                         	 packages_of_same_weight_dims__c =SOPList[l].packages_of_same_weight_dims__c,
                             Weight_Unit__c=SOPList[l].Weight_Unit__c,
                             Actual_Weight__c=SOPList[l].Actual_Weight__c,
                             Dimension_Unit__c=SOPList[l].Dimension_Unit__c,
                             Length__c=SOPList[l].Length__c,
                             Breadth__c=SOPList[l].Breadth__c,
                             Height__c=SOPList[l].Height__c,
                             Shipment_Order__c=SOList1[i].id
                         
                         );
                           SOPL.add(SOP);
                     
                     }
						
                    
                     
                 }
             }
        
        Insert SOPL;
        
                System.debug('JSONString PPart_List-->'+PAList);
             	string code1 = PAList.replace('[','{ "Parts":[').replace(']','] }');
             	String  JsonString = code1;
             	System.debug('JSONString'+JSONString);
            	CPCreatePartsWrapperInternal rw;
             	If(!JSONString.contains('Chuck')){
           				rw = (CPCreatePartsWrapperInternal)JSON.deserialize(JSONString,CPCreatePartsWrapperInternal.class);
           				System.debug('rw.Parts-->'+rw.parts);
             		}
        
                      If(String.isNotBlank(PAList) && !JSONString.contains('Chuck') ){
                          
                    //   list<Part__c> PAL = new list<Part__c>();  
                          
                          
                          for(Integer i=0; i<=SOList.size();i++) {
                			 if(i<SOList.size()){
                     
                          for(Integer l=0; rw.Parts.size()>l;l++) {
                             Part__c PA = new Part__c(
                         	 
                                  Name =rw.Parts[l].PartNumber,
                                  Description_and_Functionality__c =rw.Parts[l].PartDescription,
                                  Quantity__c =rw.Parts[l].Quantity,
                                  Commercial_Value__c =rw.Parts[l].UnitPrice,
                                  US_HTS_Code__c =rw.Parts[l].HSCode,
                                  Country_of_Origin2__c =rw.Parts[l].CountryOfOrigin,
                                  ECCN_NO__c =rw.Parts[l].ECCNNo,
                                  Type_of_Goods__c=rw.Parts[l].Type_of_Goods,
                                  Li_ion_Batteries__c=rw.Parts[l].Li_ion_Batteries,
                                  Lithium_Battery_Types__c=rw.Parts[l].Li_ion_BatteryTypes,
                                  Shipment_Order__c=SOList[i].id
                         
                         		);
                           PAL.add(PA);
                          }
                           }
                          }
                          
                                      
                      }
        	System.debug('PAL size-->'+PAL.size());
        integer a =0;
        If(!JSONString.contains('Chuck') && doChainJob ==TRUE ){
             System.enqueueJob(new QueueableInsertPartList2(PAL,a)); 
        }
    }

}