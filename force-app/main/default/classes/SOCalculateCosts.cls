public class SOCalculateCosts
    {
       
        public SOCalculateCosts(Shipment_Order__c so)
        {
            this.SO = so;
            this.CAP = [SELECT Related_Costing_CPA__c, VAT_Rate__c FROM CPA_v2_0__c WHERE ID =: so.CPA_v2_0__c];
       //   this.Client = so.Account__r;

            if (so.CPA_v2_0__c != null)

            buildCostings();
        }

        public Shipment_Order__c SO { set; get; }
        public CPA_v2_0__c CAP { set; get; }
     // public Account Client { set; get; }
       
       // public Currency_Management2__c costingCurrency {get;set;}
        private List<CPA_Costing__c> relatedCostings;
        public List<CPA_Costing__c> relatedCostingsIOR;
        private List<CPA_Costing__c> relatedCostingsEOR;
        private List<CPA_Costing__c> addedCostings;
        private List<CPA_Costing__c> createdCostings;

        private Id recordTypeId = Schema.SObjectType.CPA_Costing__c.getRecordTypeInfosByName()
                .get('Display').getRecordTypeId();

        public void buildCostings()
        {
            relatedCostingsIOR = [SELECT Name, Shipment_Order_Old__c, Amount__c, Applied_to__c, Ceiling__c, Conditional_value__c, Condition__c, Floor__c, Cost_Category__c, Cost_Type__c, Max__c, Min__c, Rate__c, CostStatus__c, Updating__c, RecordTypeID, IOR_EOR__c, VAT_applicable__c, Variable_threshold__c, VAT_Rate__c, CPA_v2_0__r.VAT_Rate__c, Currency__c  FROM CPA_Costing__c WHERE CPA_v2_0__c =: CAP.Id AND IOR_EOR__c = 'IOR' ];
            relatedCostingsEOR = [SELECT Name, Shipment_Order_Old__c, Amount__c, Applied_to__c, Ceiling__c, Conditional_value__c, Condition__c, Floor__c, Cost_Category__c, Cost_Type__c, Max__c, Min__c, Rate__c, CostStatus__c, Updating__c, RecordTypeID, IOR_EOR__c, VAT_applicable__c, Variable_threshold__c, VAT_Rate__c, CPA_v2_0__r.VAT_Rate__c, Currency__c  FROM CPA_Costing__c WHERE CPA_v2_0__c =: CAP.Id AND IOR_EOR__c = 'EOR'];   
           // costingCurrency = [Select Name, Conversion_Rate__c, ISO_Code__c, Currency__c FROM Currency_Management2__c WHERE Currency__c =: relatedCostingsIOR[0].Currency__c OR Currency__c =: relatedCostingsEOR[0].Currency__c];
        
          
            system.debug('relatedCostingsIOR--->'+relatedCostingsIOR);
             
             
             
         If(SO.Service_Type__c == 'IOR') {
          
            List<CPA_Costing__c>  SOCPAcostingsIOR = relatedCostingsIOR.deepClone();
             
             system.debug('SOCPAcostingsIOR--->'+SOCPAcostingsIOR.size());
           
            for(CPA_Costing__c cost : SOCPAcostingsIOR)
            
            {
           
                cost.Shipment_Order_Old__c = SO.ID;
                cost.CPA_v2_0__c = CAP.Related_Costing_CPA__c;
                cost.recordTypeID = recordTypeId;
                cost.VAT_Rate__c =  CAP.VAT_Rate__c;

                if (SO.Shipment_Value_USD__c != null)
                {
                    cost.Updating__c = !cost.Updating__c;
                     SOTriggerHelper.calculateSOCostingAmount(so, cost, false, cost.Currency__c);
                }
            }
             
            addedCostings = SOCPAcostingsIOR;

        }
        
        Else If(SO.Service_Type__c == 'EOR') {
          
            List<CPA_Costing__c>  SOCPAcostingsEOR = relatedCostingsEOR.deepClone();
           
            for(CPA_Costing__c cost : SOCPAcostingsEOR)
            
            {
           
                cost.Shipment_Order_Old__c = SO.ID;
                cost.CPA_v2_0__c = CAP.Related_Costing_CPA__c;
                cost.recordTypeID = recordTypeId;
                cost.VAT_Rate__c =  CAP.VAT_Rate__c; 

                if (SO.Shipment_Value_USD__c != null && (SO.FC_Total__c == null || SO.FC_Total__c == 0))
                {
                    cost.Updating__c = !cost.Updating__c;

                    SOTriggerHelper.calculateSOCostingAmount(so, cost, false, cost.Currency__c);
                }
            }
            addedCostings = SOCPAcostingsEOR;
        }
        
        }


        public List<CPA_Costing__c> getApplicableCostings()
        {
            List<CPA_Costing__c> applicableCostings = new List<CPA_Costing__c>();
            if (addedCostings != null)
                for (CPA_Costing__c cost : addedCostings)
                    if (cost.Amount__c > -1 && cost.Shipment_Order_Old__c != null)
                        applicableCostings.Add(cost);

            return applicableCostings;
        }
    }