public with sharing class RollupOnCostingHandler {
    public static Map<Id,Shipment_Order__c> rollupCostingOnSO = new Map<Id,Shipment_Order__c>();

    public static void afterInsert(List<CPA_Costing__c> newList){
        Set<Id> invIds = new Set<Id>();
        Set<Id> soIds = new Set<Id>();
        Set<Id> oldInvIds = new Set<Id>();

        for(CPA_Costing__c CPAC : newList) {
            if(!invIds.contains(CPAC.Invoice__c) && CPAC.Invoice__c != null) {
                invIds.add(CPAC.Invoice__c);
            }
            if(!invIds.contains(CPAC.Shipment_Order_Old__c) && CPAC.Shipment_Order_Old__c != null) {
                soIds.add(CPAC.Shipment_Order_Old__c);
            }
            if(!invIds.contains(CPAC.Supplier_Invoice__c) && CPAC.Supplier_Invoice__c != null) {
                oldInvIds.add(CPAC.Supplier_Invoice__c);
            }
        }

        if(soIds.size() > 0 && invIds.size() == 0 && oldInvIds.size() > 0) {

            List<Supplier_Invoice__c> sisToUpdate = [SELECT Id, Roll_Up_Costings__c FROM supplier_Invoice__c WHERE id=: oldInvIds];

            for (Supplier_Invoice__c SO: sisToUpdate) {
                SO.Roll_Up_Costings__c = true;
            }
            update sisToUpdate;

            updateActualRollupAdmin(soIds);
        }
        else if(soIds.size() > 0 && invIds.size() >= 0 && oldInvIds.size() == 0) {

            List<Invoice_New__c> invoiceToUpdate = [SELECT Id, Actual_Admin_Costs__c, Actual_Bank_Costs__c, Actual_Customs_Brokerage_Costs__c, Actual_Customs_Clearance_Costs__c,
                                                    Actual_Customs_Handling_Costs__c,Actual_Customs_License_Costs__c, Actual_Duties_and_Taxes__c, Actual_Duties_and_Taxes_Other__c, Actual_Finance_Costs__c, Actual_Insurance_Costs__c, Actual_International_Delivery_Costs__c, Actual_IOREOR_Costs__c, Actual_Miscellaneous_Costs__c, FC_Customs_Brokerage_Costs__c, FC_Customs_Clearance_Costs__c,
                                                    FC_Customs_Handling_Costs__c, FC_Customs_License_Costs__c, FC_Duties_and_Taxes__c, FC_Duties_and_Taxes_Other__c, FC_Finance_Costs__c, FC_Insurance_Costs__c, FC_International_Delivery_Costs__c, FC_IOR_Costs__c, FC_Miscellaneous_Costs__c, Conversion_Rate__c FROM Invoice_New__c WHERE id =: invIds];

            for (Invoice_New__c SI: invoiceToUpdate) {
                SI.Roll_Up_Costings__c = true;
            }
            update invoiceToUpdate;

            updateRollupAdmin(soIds);
        }
    }

    public static void beforeUpdate(List<CPA_Costing__c> newList, Map<Id,CPA_Costing__c> oldMap) {
        Set<Id> shipmentIds = new Set<Id>();

        for(CPA_Costing__c CPAC : newList) {
            shipmentIds.add(CPAC.Shipment_Order_Old__c);
        }

        Map<Id, Shipment_Order__c> shipmentOrder = new Map<Id,Shipment_Order__c>([SELECT Id, Name, Shipment_Value_USD__c, IOR_Refund_of_Tax_Duty__c, Chargeable_Weight__c,of_Line_Items__c, of_Unique_Line_Items__c, Final_Deliveries_New__c, of_packages__c FROM Shipment_Order__c WHERE Id IN: shipmentIds]);

        if(shipmentOrder.isEmpty()) return;

        for(CPA_Costing__c CPAC : newList) {
            Decimal chargeableAmount = 0;

            if((CPAC.Rate__c != oldMap.get(CPAC.ID).Rate__c) ||
               (CPAC.VAT_Rate__c != oldMap.get(CPAC.ID).VAT_Rate__c) ||
               (CPAC.Variable_threshold__c != oldMap.get(CPAC.ID).Variable_threshold__c) ||
               (CPAC.Additional_Percent__c != oldMap.get(CPAC.ID).Additional_Percent__c) ||
               (CPAC.Applied_to__c != oldMap.get(CPAC.ID).Applied_to__c) &&
               CPAC.RecordTypeID == Schema.Sobjecttype.CPA_Costing__c.getRecordTypeInfosByDeveloperName().get('Display').getRecordTypeId() && CPAC.Rate__c != null) {
                if( CPAC.Applied_to__c == 'Shipment value') {
                    chargeableAmount = shipmentOrder.get(CPAC.Shipment_Order_Old__c).Shipment_Value_USD__c;
                }
                else if( CPAC.Applied_to__c == 'Total Taxes') {
                    chargeableAmount = shipmentOrder.get(CPAC.Shipment_Order_Old__c).IOR_Refund_of_Tax_Duty__c;
                }
                else if( CPAC.Applied_to__c == 'CIF value') {
                    chargeableAmount = (shipmentOrder.get(CPAC.Shipment_Order_Old__c).Shipment_Value_USD__c + 250) * 1.05;
                }
                else if( CPAC.Applied_to__c == 'Chargeable weight') {
                    system.debug('CPAC.Shipment_Order_Old__c--->'+CPAC.Shipment_Order_Old__c);
                    system.debug('CPAC.Shipment_Order_Old__c2--->'+shipmentOrder.get(CPAC.Shipment_Order_Old__c));

                    chargeableAmount = shipmentOrder.get(CPAC.Shipment_Order_Old__c).Chargeable_Weight__c;
                }
                else if( CPAC.Applied_to__c == '# Parts (line items)') {
                    chargeableAmount =  shipmentOrder.get(CPAC.Shipment_Order_Old__c).of_Line_Items__c;
                }
                else if( CPAC.Applied_to__c == '# Unique HS Codes') {
                    chargeableAmount =  shipmentOrder.get(CPAC.Shipment_Order_Old__c).of_Unique_Line_Items__c;
                }
                else if( CPAC.Applied_to__c == '# of Final Deliveries') {
                    chargeableAmount =  shipmentOrder.get(CPAC.Shipment_Order_Old__c).Final_Deliveries_New__c;
                }
                else {
                    chargeableAmount = shipmentOrder.get(CPAC.Shipment_Order_Old__c).of_packages__c;
                }

                CPAC.Inactive__c = CPAC.Variable_threshold__c > 0 && chargeableAmount < CPAC.Variable_threshold__c;


                if(CPAC.Applied_to__c == 'Shipment value' || CPAC.Applied_to__c == 'Total Taxes' || CPAC.Applied_to__c == 'CIF value' ) {
                    CPAC.Amount__c  =  (chargeableAmount - CPAC.Variable_threshold__c ) * (CPAC.Rate__c /100) * (1+(CPAC.VAT_Rate__c/100) ) * (1+(CPAC.Additional_Percent__c /100));

                }
                else if(CPAC.Applied_to__c == 'Chargeable weight') {
                    CPAC.Amount__c  =  (chargeableAmount - CPAC.Variable_threshold__c ) * CPAC.Rate__c  * (1+CPAC.VAT_Rate__c/100 ) * (1+CPAC.Additional_Percent__c );

                }
                else if(CPAC.Applied_to__c == '# Parts (line items)' || CPAC.Applied_to__c == '# Unique HS Codes' || CPAC.Applied_to__c == '# of Final Deliveries') {
                    CPAC.Amount__c  =  (chargeableAmount - CPAC.Variable_threshold__c ) * CPAC.Rate__c  * (1+CPAC.VAT_Rate__c ) * (1+CPAC.Additional_Percent__c );

                }else{
                    CPAC.Amount__c  = (chargeableAmount - CPAC.Variable_threshold__c ) * (CPAC.Rate__c == null ? 0 : CPAC.Rate__c) * (1+CPAC.VAT_Rate__c ) * (1+CPAC.Additional_Percent__c );
                }

            }
        }
    }

    public static void afterUpdate(List<CPA_Costing__c> newList, Map<Id,CPA_Costing__c> oldMap){
        Set<Id> invIds = new Set<Id>();
        Set<Id> soIds = new Set<Id>();
        Set<Id> oldInvIds = new Set<Id>();


        for(CPA_Costing__c CPAC : newList) {

            if((CPAC.Invoice_amount_local_currency__c != oldMap.get(CPAC.Id).Invoice_amount_local_currency__c && CPAC.Invoice_amount_local_currency__c != null) ||
               (CPAC.Amount__c != oldMap.get(CPAC.Id).Amount__c && CPAC.Amount__c != null) ||
               (CPAC.Cost_Category__c != oldMap.get(CPAC.id).Cost_Category__c && CPAC.Cost_Category__c != null) && CPAC.Shipment_Order_Old__c != null ) {

                if(!invIds.contains(CPAC.Invoice__c) && CPAC.Invoice__c != null) {
                    invIds.add(CPAC.Invoice__c);
                }
                if(!invIds.contains(CPAC.Shipment_Order_Old__c) && CPAC.Shipment_Order_Old__c != null) {
                    soIds.add(CPAC.Shipment_Order_Old__c);
                }
                if(!invIds.contains(CPAC.Supplier_Invoice__c) && CPAC.Supplier_Invoice__c != null) {
                    oldInvIds.add(CPAC.Supplier_Invoice__c);
                }
            }
        }

        if(soIds.size() > 0 && invIds.size() == 0 && oldInvIds.size() > 0) {

            List<Supplier_Invoice__c> sisToUpdate = [SELECT Id, Actual_Duties_and_Taxes_Other__c,Actual_Tax_Forex_Spread__c,FC_Admin_Fee__c,
                                                     FC_Bank_Fees__c,FC_Finance_Fee__c,FC_Insurance_Fee_USD__c,FC_Miscellaneous_Fee__c,FC_International_Delivery_Fee__c,Total_Duties_and_Taxes__c,Total_Brokerage_Costs__c,Total_Clearance_Costs__c,Total_Handling_Costs__c,Total_License_Permits_Cost__c,Total_IOR_EOR__c,Actual_Admin_Fee__c,Actual_Bank_Fees__c,Actual_Finance_Fee__c,Actual_Insurance_Fee_USD__c,Actual_Miscellaneous_Fee__c,Actual_International_Delivery_Fee__c,Actual_Duties_and_Taxes__c,Actual_Brokerage_Costs__c,Actual_Clearance_Costs__c,Actual_Handling_Costs__c,Actual_License_Permits_Cost__c,Actual_IOR_EOR_Cost__c FROM supplier_Invoice__c WHERE id=: oldInvIds];

            for (Supplier_Invoice__c SO: sisToUpdate) {
                SO.Roll_Up_Costings__c = true;
            }
            update sisToUpdate;
            updateActualRollupAdmin(soIds);
        }

        if(soIds.size() > 0 && invIds.size() >= 0 && oldInvIds.size() == 0) {
            List<Invoice_New__c> invoiceToUpdate = [SELECT Id, Actual_Admin_Costs__c, Actual_Bank_Costs__c, Actual_Customs_Brokerage_Costs__c, Actual_Customs_Clearance_Costs__c,
                                                    Actual_Customs_Handling_Costs__c,Actual_Customs_License_Costs__c, Actual_Duties_and_Taxes__c, Actual_Duties_and_Taxes_Other__c, Actual_Finance_Costs__c, Actual_Insurance_Costs__c, Actual_International_Delivery_Costs__c, Actual_IOREOR_Costs__c, Actual_Miscellaneous_Costs__c, FC_Customs_Brokerage_Costs__c, FC_Customs_Clearance_Costs__c,
                                                    FC_Customs_Handling_Costs__c, FC_Customs_License_Costs__c, FC_Duties_and_Taxes__c, FC_Duties_and_Taxes_Other__c, FC_Finance_Costs__c, FC_Insurance_Costs__c, FC_International_Delivery_Costs__c, FC_IOR_Costs__c, FC_Miscellaneous_Costs__c, Conversion_Rate__c FROM Invoice_New__c WHERE id =: invIds];

            for (Invoice_New__c SI: invoiceToUpdate) {
                SI.Roll_Up_Costings__c = true;
            }
            update invoiceToUpdate;

            updateRollupAdmin(soIds);
        }
    }

    public static void afterDelete(List<CPA_Costing__c> oldList){
        Set<Id> invIds = new Set<Id>();
        Set<Id> soIds = new Set<Id>();
        Set<Id> oldInvIds = new Set<Id>();

        for(CPA_Costing__c CPAC : oldList) {
            if(CPAC.Supplier_Invoice__c != null || CPAC.Invoice__c != null) {
                CPAC.addError('Selected record linked an supplier invoice. It cant be deleted ');
            }
            else if(CPAC.Supplier_Invoice__c == null && CPAC.Invoice__c == null && CPAC.DeleteAll__c == false) {
                if(!invIds.contains(CPAC.Invoice__c) && CPAC.Invoice__c != null) {
                    invIds.add(CPAC.Invoice__c);
                }
                if(!invIds.contains(CPAC.Shipment_Order_Old__c) && CPAC.Shipment_Order_Old__c != null) {
                    soIds.add(CPAC.Shipment_Order_Old__c);
                }
                if(!invIds.contains(CPAC.Supplier_Invoice__c) && CPAC.Supplier_Invoice__c != null) {
                    oldInvIds.add(CPAC.Supplier_Invoice__c);
                }
            }
        }

        if(soIds.size() > 0 && invIds.size() == 0 && oldInvIds.size() > 0) {

            List<Supplier_Invoice__c> sisToUpdate = [SELECT Id, Actual_Duties_and_Taxes_Other__c,Actual_Tax_Forex_Spread__c,FC_Admin_Fee__c,
                                                     FC_Bank_Fees__c,FC_Finance_Fee__c,FC_Insurance_Fee_USD__c,FC_Miscellaneous_Fee__c,FC_International_Delivery_Fee__c,Total_Duties_and_Taxes__c,Total_Brokerage_Costs__c,Total_Clearance_Costs__c,Total_Handling_Costs__c,Total_License_Permits_Cost__c,Total_IOR_EOR__c,Actual_Admin_Fee__c,Actual_Bank_Fees__c,Actual_Finance_Fee__c,Actual_Insurance_Fee_USD__c,Actual_Miscellaneous_Fee__c,Actual_International_Delivery_Fee__c,Actual_Duties_and_Taxes__c,Actual_Brokerage_Costs__c,Actual_Clearance_Costs__c,Actual_Handling_Costs__c,Actual_License_Permits_Cost__c,Actual_IOR_EOR_Cost__c FROM supplier_Invoice__c WHERE id=: oldInvIds];

            for (Supplier_Invoice__c SO: sisToUpdate) {
                SO.Roll_Up_Costings__c = true;
            }
            update sisToUpdate;

            updateActualRollupAdmin(soIds);

        }
        else if(soIds.size() > 0 && invIds.size() >= 0 && oldInvIds.size() == 0) {
            List<Invoice_New__c> invoiceToUpdate = [SELECT Id, Actual_Admin_Costs__c, Actual_Bank_Costs__c, Actual_Customs_Brokerage_Costs__c, Actual_Customs_Clearance_Costs__c,
                                                    Actual_Customs_Handling_Costs__c,Actual_Customs_License_Costs__c, Actual_Duties_and_Taxes__c, Actual_Duties_and_Taxes_Other__c, Actual_Finance_Costs__c, Actual_Insurance_Costs__c, Actual_International_Delivery_Costs__c, Actual_IOREOR_Costs__c, Actual_Miscellaneous_Costs__c, FC_Customs_Brokerage_Costs__c, FC_Customs_Clearance_Costs__c,
                                                    FC_Customs_Handling_Costs__c, FC_Customs_License_Costs__c, FC_Duties_and_Taxes__c, FC_Duties_and_Taxes_Other__c, FC_Finance_Costs__c, FC_Insurance_Costs__c, FC_International_Delivery_Costs__c, FC_IOR_Costs__c, FC_Miscellaneous_Costs__c, Conversion_Rate__c FROM Invoice_New__c WHERE id =: invIds];

            for (Invoice_New__c SI: invoiceToUpdate) {
                SI.Roll_Up_Costings__c = true;
            }
            update invoiceToUpdate;

            updateRollupAdmin(soIds);
        }
    }

    public static void updateRollupAdmin(Set<Id> soIds){
        //Map<Id,Shipment_Order__c> rollupCostingOnSO = new Map<Id,Shipment_Order__c>();

        for(AggregateResult a : [SELECT Shipment_Order_Old__c,Sum(Invoice_amount_USD_New__c) invAmount, Sum(Amount_in_USD__c) fcAmount,Cost_Category__c costCat, Tax_Sub_Category__c subcat FROM CPA_Costing__c WHERE Shipment_Order_Old__c IN:soIds And RecordTypeID =: Schema.Sobjecttype.CPA_Costing__c.getRecordTypeInfosByDeveloperName().get('Display').getRecordTypeId() Group by Cost_Category__c,Tax_Sub_Category__c,Shipment_Order_Old__c]) {

            Shipment_Order__c so;
            String shipmentId = String.valueOf(a.get('Shipment_Order_Old__c'));
            if(rollupCostingOnSO.containsKey(shipmentId)) {
                so = rollupCostingOnSO.get(shipmentId);
            }else{
                so = new Shipment_Order__c(
                    Id = shipmentId,
                    FC_Admin_Fee__c = 0,
                    FC_Bank_Fees__c = 0,
                    FC_Finance_Fee__c = 0,
                    FC_IOR_and_Import_Compliance_Fee_USD__c = 0,
                    FC_Insurance_Fee_USD__c = 0,
                    FC_International_Delivery_Fee__c = 0,
                    FC_Miscellaneous_Fee__c = 0,
                    FC_Recharge_Tax_and_Duty__c = 0,
                    FC_Total_Clearance_Costs__c = 0,
                    FC_Total_Customs_Brokerage_Cost__c = 0,
                    FC_Total_Handling_Costs__c = 0,
                    FC_Total_License_Cost__c = 0
                    );
                rollupCostingOnSO.put(shipmentId,so);
            }

            if(a.get('costCat') == 'Admin' && a.get('subcat') == null) {
                so.FC_Admin_Fee__c = (Decimal) a.get('fcAmount');
            }
            if(a.get('costCat') == 'Bank' && a.get('subcat') == null) {
                so.FC_Bank_Fees__c = (Decimal) a.get('fcAmount');
            }
            if(a.get('costCat') == 'Finance' && a.get('subcat') == null) {
                so.FC_Finance_Fee__c = (Decimal) a.get('fcAmount');
            }
            if(a.get('costCat') == 'IOR Cost' && a.get('subcat') == null) {
                so.FC_IOR_and_Import_Compliance_Fee_USD__c = (Decimal) a.get('fcAmount');
            }
            if(a.get('costCat') == 'Insurance' && a.get('subcat') == null) {
                so.FC_Insurance_Fee_USD__c = (Decimal) a.get('fcAmount');
            }
            if(a.get('costCat') == 'International freight' && a.get('subcat') == null) {
                so.FC_International_Delivery_Fee__c = (Decimal) a.get('fcAmount');
            }
            if(a.get('costCat') == 'Miscellaneous' && a.get('subcat') == null) {
                so.FC_Miscellaneous_Fee__c = (Decimal) a.get('fcAmount');
            }
            if(a.get('costCat') == 'Tax' && a.get('subcat') == null) {
                so.FC_Recharge_Tax_and_Duty__c = (Decimal) a.get('fcAmount');
            }
            if(a.get('costCat') == 'Clearance' && a.get('subcat') == null) {
                so.FC_Total_Clearance_Costs__c = (Decimal) a.get('fcAmount');
            }
            if(a.get('costCat') == 'Brokerage' && a.get('subcat') == null) {
                so.FC_Total_Customs_Brokerage_Cost__c = (Decimal) a.get('fcAmount');
            }
            if(a.get('costCat') == 'Handling' && a.get('subcat') == null) {
                so.FC_Total_Handling_Costs__c = (Decimal) a.get('fcAmount');
            }
            if(a.get('costCat') == 'Licenses' && a.get('subcat') == null) {
                so.FC_Total_License_Cost__c = (Decimal) a.get('fcAmount');
            }
            if(a.get('costCat') == 'Tax' && a.get('subcat') == 'Duties and Taxes Other') {
                // FCTAXOther = (Decimal) a.get('fcAmount');
            }
        }

        if(!RecusrsionHandler.soCreationProcessStarted)
            update rollupCostingOnSO.values();
    }

    public static void updateActualRollupAdmin(Set<Id> soIds){
        // Map<Id,Shipment_Order__c> rollupCostingOnSO = new Map<Id,Shipment_Order__c>();

        for(AggregateResult a : [SELECT Shipment_Order_Old__c,Sum(Invoice_amount_USD_New__c) invAmount, Sum(Amount_in_USD__c) fcAmount,Cost_Category__c costCat, Tax_Sub_Category__c subcat FROM CPA_Costing__c WHERE Shipment_Order_Old__c IN:soIds And RecordTypeID =: Schema.Sobjecttype.CPA_Costing__c.getRecordTypeInfosByDeveloperName().get('Display').getRecordTypeId() Group by Cost_Category__c,Tax_Sub_Category__c,Shipment_Order_Old__c]) {

            Shipment_Order__c so;
            String shipmentId = String.valueOf(a.get('Shipment_Order_Old__c'));
            if(rollupCostingOnSO.containsKey(shipmentId)) {
                so = rollupCostingOnSO.get(shipmentId);
            }else{
                so = new Shipment_Order__c(
                    Id=shipmentId,
                    Actual_Admin_Fee__c = 0,
                    Actual_Bank_Fees__c = 0,
                    Actual_Finance_Fee__c = 0,
                    Actual_Total_IOR_EOR__c = 0,
                    Actual_Insurance_Fee_USD__c = 0,
                    Actual_International_Delivery_Fee__c = 0,
                    Actual_Miscellaneous_Fee__c = 0,
                    Actual_Total_Tax_and_Duty__c = 0,
                    Actual_Total_Clearance_Costs__c = 0,
                    Actual_Total_Customs_Brokerage_Cost__c = 0,
                    Actual_Total_Handling_Costs__c = 0,
                    Actual_Total_License_Cost__c = 0,
                    Actual_Duties_and_Taxes_Other__c = 0,
                    Actual_Taxes_Forex_Spread__c = 0
                    );
                rollupCostingOnSO.put(shipmentId,so);
            }

            if(a.get('costCat') == 'Admin' && a.get('subcat') == null) {
                SO.Actual_Admin_Fee__c = (Decimal) a.get('invAmount');
                so.FC_Admin_Fee__c = (Decimal) a.get('fcAmount');
            }
            if(a.get('costCat') == 'Bank' && a.get('subcat') == null) {
                SO.Actual_Bank_Fees__c = (Decimal) a.get('invAmount');
                so.FC_Bank_Fees__c = (Decimal) a.get('fcAmount');
            }
            if(a.get('costCat') == 'Finance' && a.get('subcat') == null) {
                SO.Actual_Finance_Fee__c = (Decimal) a.get('invAmount');
                so.FC_Finance_Fee__c = (Decimal) a.get('fcAmount');
            }
            if(a.get('costCat') == 'IOR Cost' && a.get('subcat') == null) {
                SO.Actual_Total_IOR_EOR__c = (Decimal) a.get('invAmount');
                so.FC_IOR_and_Import_Compliance_Fee_USD__c = (Decimal) a.get('fcAmount');
            }
            if(a.get('costCat') == 'Insurance' && a.get('subcat') == null) {
                SO.Actual_Insurance_Fee_USD__c = (Decimal) a.get('invAmount');
                so.FC_Insurance_Fee_USD__c = (Decimal) a.get('fcAmount');
            }
            if(a.get('costCat') == 'International freight' && a.get('subcat') == null) {
                SO.Actual_International_Delivery_Fee__c = (Decimal)a.get('invAmount');
                so.FC_International_Delivery_Fee__c = (Decimal) a.get('fcAmount');
            }
            if(a.get('costCat') == 'Miscellaneous' && a.get('subcat') == null) {
                SO.Actual_Miscellaneous_Fee__c = (Decimal) a.get('invAmount');
                so.FC_Miscellaneous_Fee__c = (Decimal) a.get('fcAmount');
            }
            if(a.get('costCat') == 'Tax' && a.get('subcat') == null) {
                SO.Actual_Total_Tax_and_Duty__c = (Decimal) a.get('invAmount');
                so.FC_Recharge_Tax_and_Duty__c = (Decimal) a.get('fcAmount');
            }
            if(a.get('costCat') == 'Clearance' && a.get('subcat') == null) {
                SO.Actual_Total_Clearance_Costs__c = (Decimal) a.get('invAmount');
                so.FC_Total_Clearance_Costs__c = (Decimal) a.get('fcAmount');
            }
            if(a.get('costCat') == 'Brokerage' && a.get('subcat') == null) {
                SO.Actual_Total_Customs_Brokerage_Cost__c = (Decimal) a.get('invAmount');
                so.FC_Total_Customs_Brokerage_Cost__c = (Decimal) a.get('fcAmount');
            }
            if(a.get('costCat') == 'Handling' && a.get('subcat') == null) {
                SO.Actual_Total_Handling_Costs__c = (Decimal) a.get('invAmount');
                so.FC_Total_Handling_Costs__c = (Decimal) a.get('fcAmount');
            }
            if(a.get('costCat') == 'Licenses' && a.get('subcat') == null) {
                SO.Actual_Total_License_Cost__c = (Decimal) a.get('invAmount');
                so.FC_Total_License_Cost__c = (Decimal) a.get('fcAmount');
            }
            if(a.get('costCat') == 'Tax' && a.get('subcat') == 'Duties and Taxes Other') {
                SO.Actual_Duties_and_Taxes_Other__c = (Decimal) a.get('invAmount');
            }
            if(a.get('costCat') == 'Tax' && a.get('subcat') == 'Tax Forex Spread') {
                SO.Actual_Taxes_Forex_Spread__c = (Decimal) a.get('invAmount');
            }
        }
        if(!RecusrsionHandler.soCreationProcessStarted)
            update rollupCostingOnSO.values();
    }
}