public class ObjectsCustomLookUpController {
    @AuraEnabled
    public static List < sObject > fetchLookUpValues(String searchKeyWord, String ObjectName) {
        system.debug('ObjectName-->' + ObjectName);
        String searchKey = '%'+searchKeyWord + '%';
        
        List < sObject > returnList = new List < sObject > ();
        String sQuery;
        if(ObjectName.contains('Manufacturer__c')){
            List<string> splittedString = ObjectName.split(';');
            system.debug('splittedString :'+splittedString);
            String newObjectName = splittedString.remove(0);
            system.debug('newObjectName :'+newObjectName);
            Id ManufacturerRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Manufacturer').getRecordTypeId();
            sQuery =  'select id, Name from ' +newObjectName + ' where Name LIKE: searchKey AND Recordtypeid =:ManufacturerRecordTypeId order by createdDate DESC limit 5';
                        system.debug('sQuery :'+sQuery);
        }else if(ObjectName.contains(';')){
            List<string> splittedString = ObjectName.split(';');
            String newObjectName = splittedString.remove(0);
            
            Map<Id,Profile> profilemap = new map<Id, profile>([SELECT Id, Name FROM Profile WHERE Name =: splittedString]);
            
            List<String> systemAdminProf = new List<String>();
            for(Profile prof : profilemap.Values()){
                systemAdminProf.add(prof.Id);
            }
           
            if(newObjectName == 'User'){
                sQuery =  'select id, Name from ' +newObjectName + ' where Name LIKE: searchKey AND profileid =: systemAdminProf  order by createdDate DESC limit 5';
            }else{
                sQuery =  'select id, Name from ' +newObjectName + ' where Name LIKE: searchKey order by createdDate DESC limit 5';
            }
            
            
            
        }else{
            sQuery =  'select id, Name from ' +ObjectName + ' where Name LIKE: searchKey order by createdDate DESC limit 5';
        }
        // Create a Dynamic SOQL Query For Fetch Record List with LIMIT 5   
        
        List < sObject > lstOfRecords = Database.query(sQuery);
        
        for (sObject obj: lstOfRecords) {
            returnList.add(obj);
        }
        return returnList;
    }
    @AuraEnabled
    public static List < sObject > fetchLookUpValuesForRelatedObject(String searchKeyWord, String ObjectName, String relatedObjectId) {
        system.debug('ObjectName-->' + ObjectName);
        String searchKey = '%'+searchKeyWord + '%';
        
        List < sObject > returnList = new List < sObject > ();
        
        String sQuery =  'select id, Name from ' +ObjectName + ' where AccountId =: relatedObjectId AND Name LIKE: searchKey order by createdDate DESC limit 5';
        
        
        List < sObject > lstOfRecords = Database.query(sQuery);
        
        for (sObject obj: lstOfRecords) {
            returnList.add(obj);
        }
        
        
        // Create a Dynamic SOQL Query For Fetch Record List with LIMIT 5   
        
        return returnList;
    }
}