@RestResource(urlMapping='/SOOrderPackageCreation/*')
global class CPCreateSOOrderPackage {
     @Httppost
    global static void CreateSOOrderPackage(){
        
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        Blob body = req.requestBody;
    	res.addHeader('Content-Type', 'application/json');
        String requestString = body.toString();
        CPCreateSOOrderPackWrapper rw = (CPCreateSOOrderPackWrapper)JSON.deserialize(requestString,CPCreateSOOrderPackWrapper.class);
        
         try {
           
            
          If(!rw.ShipmentOrder_Packages.isEmpty()){
           
              
          list<Shipment_Order_Package__c> SOPL = new list<Shipment_Order_Package__c>();
           		
              for(Integer i=0; rw.ShipmentOrder_Packages.size()>i;i++) {
              
          Shipment_Order_Package__c SOP = new Shipment_Order_Package__c(
            
             
            Shipment_Order__c = Rw.ShipmentOrder_Packages[i].SOID,
            Weight_Unit__c= rw.ShipmentOrder_Packages[i].Weight_Unit,
            Dimension_Unit__c = rw.ShipmentOrder_Packages[i].Dimension_Unit,
            packages_of_same_weight_dims__c = rw.ShipmentOrder_Packages[i].Packages_of_Same_Weight,
            Length__c=decimal.Valueof(rw.ShipmentOrder_Packages[i].Length),
            Height__c=decimal.Valueof(rw.ShipmentOrder_Packages[i].Height),
            Breadth__c=decimal.Valueof(rw.ShipmentOrder_Packages[i].Breadth),
           	Actual_Weight__c=decimal.Valueof(rw.ShipmentOrder_Packages[i].Actual_Weight));
            
           
              		SOPL.add(SOP); 
                   
             }
              Insert SOPL;
              
              
            res.responseBody = Blob.valueOf(JSON.serializePretty(SOPL));
            res.statusCode = 200;
              	API_Log__c Al = New API_Log__c();
                Al.Account__c=rw.ClientID;
                Al.Login_Contact__c=rw.ContactID;
                Al.EndpointURLName__c='SOOrderPackageCreation';
                Al.Response__c='Success - Shipment Order Packages Created';
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al; 
            }
            }
        	catch (Exception e){
                
              	 String ErrorString ='Something went wrong while creating Shipment order packages, please contact support_SF@tecex.com';
               	res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
            	res.statusCode = 500;
                API_Log__c Al = New API_Log__c();
                Al.Account__c=rw.ClientID;
                Al.Login_Contact__c=rw.ContactID;
                Al.EndpointURLName__c='SOOrderPackageCreation';
                Al.Response__c=e.getMessage()+' at line number: '+e.getLineNumber();
                Al.StatusCode__c=string.valueof(res.statusCode);
              	Insert Al;

              	}
        
    }
    

}