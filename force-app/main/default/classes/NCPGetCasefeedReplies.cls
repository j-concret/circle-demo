@RestResource(urlMapping='/GetCasefeedreplies/*')
Global class NCPGetCasefeedReplies {
    
    @Httppost
    global static void NCPGetCasedetails(){
                RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        Blob body = req.requestBody;
        res.addHeader('Content-Type', 'application/json');
        String requestString = body.toString();
        NCPGetCasefeedReplieswra rw = (NCPGetCasefeedReplieswra)JSON.deserialize(requestString,NCPGetCasefeedReplieswra.class);
        try {
            
            
            Access_token__c At =[select status__c,Access_Token__c from Access_token__c where Access_Token__c=:rw.Accesstoken ];
            if( at.status__C =='Active'){
                List<FeedComment> feedCommentList = [select Id ,createdbyID, FeedItemId , ParentId , CreatedDate , LastEditDate , CommentBody,RelatedRecordId  from FeedComment where FeedItemId=:rw.CasefeedID];
                Map<Id,Id> feedCommentRelatedIds = new Map<Id,Id>();
                for(FeedComment feedComment:feedCommentList){
                    if(feedComment.RelatedRecordId != null){
                        feedCommentRelatedIds.put(feedComment.RelatedRecordId,feedComment.Id);
                    }
                }
                Map<Id,List<ContentVersion>> mapOfContentVersions= new Map<Id,List<ContentVersion>>();
                for(ContentVersion contentVersion:[SELECT Id,ContentDocumentId,Title from ContentVersion where Id in:feedCommentRelatedIds.keyset() ]){
                    if(feedCommentRelatedIds.containskey(contentVersion.ID)){
                        
                        If(mapOfContentVersions.containskey(feedCommentRelatedIds.get(contentVersion.ID))  ){
                            list<ContentVersion> existingContentVersions= mapOfContentVersions.get(feedCommentRelatedIds.get(contentVersion.ID));
                            existingContentVersions.add(contentVersion);
                            mapOfContentVersions.put(feedCommentRelatedIds.get(contentVersion.ID) ,existingContentVersions);//069
                            
                        }
                        else{
                            
                            mapOfContentVersions.put(feedCommentRelatedIds.get(contentVersion.ID) ,new list<ContentVersion>{contentVersion});//069
                            
                            
                        }
                    }
                }
                  List<Object> taskFeedObj = new List<Object>();
                If(!feedCommentList.isEmpty()){
                   
                  
                   List<ContentVersion> contentVersionList= new  List<ContentVersion>();
                    for(FeedComment feedComment :feedCommentList) {
                        Map<String,Object> genMap = new Map<String,Object>();
                        if(feedComment.Id != null) {genMap.put('Id', feedComment.Id);}
                        if(feedComment.ParentId != null) {genMap.put('ParentId', feedComment.ParentId);}
                        if(feedComment.createdbyID != null) {genMap.put('CreatedbyID', feedComment.createdbyID);}
                        if(feedComment.FeedItemId != null) {genMap.put('FeedItemId', feedComment.FeedItemId);}
                        if(feedComment.LastEditDate != null) {genMap.put('LastEditDate', feedComment.LastEditDate);}
                        if(feedComment.createddate != null) {genMap.put('createddate', feedComment.createddate);}
                        if(feedComment.CommentBody != null) {genMap.put('CommentBody', feedComment.CommentBody);}
                        if(feedComment.RelatedRecordId != null && feedCommentRelatedIds.containskey(feedComment.RelatedRecordId) ) {
                            if(mapOfContentVersions.get(feedComment.Id) != null){contentVersionList= mapOfContentVersions.get(feedComment.Id); }
                          //  List<ContentVersion>  contentVersionList= mapOfContentVersions.get(feedComment.Id);
                            List<Map<String,Object>> contentVersionListDetails = new List<Map<String,Object>>();
                            if(contentVersionList.size()>0){
                            for(ContentVersion contentVersion:contentVersionList){
                                 Map<String,Object> genMapCV = new Map<String,Object>();
                                        if(contentVersion.Title != null){genMapCV.put('Title',contentVersion.Title);}
                                  		if(contentVersion.Id != null){genMapCV.put('ContentverId',contentVersion.ID);}
                         				if(contentVersion.ContentDocumentID != null){genMapCV.put('ContentdocId',contentVersion.ContentDocumentID);}
                                 
                                  contentVersionListDetails.add(genMapCV);
                            }
                        }
                            genMap.put('ContentDocumentDetails', contentVersionListDetails);
                        }
                        taskFeedObj.add(genMap);
                    }
                }
                
                // List<Attachment> Attmnts = [select Id,Name,ParentId,Body,BodyLength,ContentType,LastModifiedDate, CreatedDate, SystemModstamp, Parent.Type FROM Attachment where ParentId =:Cs[0].ParentId ];
                //SELECT Id, FeedItemId, ParentId, CreatedById, CreatedDate, SystemModstamp, Revision, LastEditById, LastEditDate, CommentBody, IsDeleted, InsertedById, CommentType, RelatedRecordId, IsRichText, IsVerified, HasEntityLinks, Status, ThreadParentId, ThreadLevel, ThreadChildrenCount, ThreadLastUpdatedDate FROM FeedComment where FeedItemId= '0D51q00000TgXMxCAN'  
                //Select Id,contentdocumentid from contentversion where id='0681q000003KGuJAAW' 
                //   List<ContentDocumentLink> TS = [SELECT Id,ContentDocumentId,LinkedEntityId,Visibility,ShareType FROM ContentDocumentLink where LinkedEntityId  ='00T1q00000AI7lcEAD'];
                //   System.debug('TS-->'+TS);
                
                
                res.responseBody = Blob.valueOf(JSON.serializePretty(taskFeedObj));
                res.addHeader('Content-Type', 'application/json');
                res.statusCode = 200;
                
                API_Log__c Al = New API_Log__c();
                // Al.Account__c=cs.AccountID;
                //Al.Login_Contact__c=cs.ContactId;
                Al.EndpointURLName__c='GetCasefeedreplies';
                Al.Response__c='Success -GetCaseDetails Details are sent';
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al;
            }
        }
        catch (Exception e){
            
            String ErrorString ='Something went wrong while creating cost estimate, please contact support_SF@tecex.com';
            res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
            res.addHeader('Content-Type', 'application/json');
            res.statusCode = 500;
            
            API_Log__c Al = New API_Log__c();
            // Al.Account__c=rw.AccountID;
            //Al.Login_Contact__c=rw.contactId;
            Al.EndpointURLName__c='GetCasefeedreplies';
            Al.Response__c=e.getMessage()+' at line number: '+e.getLineNumber();
            Al.StatusCode__c=string.valueof(res.statusCode);
            Insert Al;
        }
        
    }
    
    
    
}