public class JiraIssue {

  public JiraIssue(String summary,String reason,String priority,String descriptionText){
    this.fields = new Fields(summary,reason,priority,descriptionText);
  }

	public class Project {
		public String id;
    public Project(String pId){
      this.id = pId;
    }
	}

	public class Description {
		public String type;
		public Integer version;
		public List<Content_Z> content;

    public Description(String descriptionText){
      this.type = 'doc';
      this.version = 1;
      this.content = new List<Content_Z>{new Content_Z(descriptionText)};
    }
	}

	public class Fields {
		public GenericNameType issuetype;
		public Project project;
		public String summary;
		public Customfield customfield_11405;
		public Customfield customfield_11408;
		public GenericNameType priority;
		public Description description;

    public Fields(String summary,String reason,String priority,String descriptionText){
      this.issuetype = new GenericNameType('IT Issue');// issue type static (only option for case type picklist)
      this.project = new Project('13000');//static project id, its for ticket system
      this.summary = summary;
      this.customfield_11405 = new Customfield('Tec EX');//Department : Tec EX
      this.customfield_11408 = new Customfield(reason);//case reason
      this.priority = new GenericNameType(priority);
      this.description = new Description(descriptionText);
    }
	}

	public Fields fields;

	public class Customfield {
		public String value;
    public Customfield(String value){
      this.value = value;
    }
	}

	public class Content {
		public String text;
		public String type;

    public Content(String text){
      this.text = text;
      this.type = 'text';
    }
	}

	public class GenericNameType {
		public String name;
    public GenericNameType(String name){
      this.name = name;
    }
	}

	public class Content_Z {
		public String type;
		public List<Content> content;

    public Content_Z(String descriptionText){
      this.type = 'paragraph';
      this.content = new List<Content>{new Content(descriptionText)};
    }
	}

}