public class SOQuickActionButtonCantainer_V2_Ctrl {
 @AuraEnabled
    public static Map<String,Object> getShipmentStatus(String recId){
        Map<String,Object> response = new Map<String,Object> {'status'=>'OK'};
            try{
                
              Shipment_order__C shipmentOrder = [Select id, Shipping_Status__c FROM Shipment_Order__c WHERE Id =: recId];
              response.put('shipmentStatus', shipmentOrder.Shipping_Status__c);  
            }catch(Exception e){
                setException(e.getMessage(), response);
            }
        return response;
    }
    
    public static void setException(String error, Map<String,Object> response){
         response.put('status','ERROR');
         response.put('msg',error);
    }
}