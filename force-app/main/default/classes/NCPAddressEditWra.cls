public class NCPAddressEditWra {
    
    
    public String Accesstoken;
    public String Id;
	public String Name;
	public String Contact_Full_Name;
	public String Contact_Email;
	public String Contact_Phone_Number;
   	public String AddressLine1;
	public String AddressLine2;
	public String City;
	public String Province;
	public String Postal_Code;
	public String All_Countries;
    public String AdditionalNumber;
    public String Comments;
    public String CompanyName;
    public String DefaultAddress;
    public String AddressStatus;
    public String PickupPreference;

}