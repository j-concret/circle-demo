@isTest
public class NCPDownloadattachment_Test {
    
     static testMethod void  testForNCPDownloadattachment(){
        
        Access_token__c at = new Access_token__c(Status__c = 'Active',
            												 Access_Token__c = 'asdfghj-sdfgfds-sfdfgf');
        Insert at;
        
        Account acc = new Account(Name = 'Test Account');
        Insert acc;
        
        Attachment attachment = new Attachment();
        attachment.Name = 'Test file';
        attachment.ParentId = acc.Id;
        attachment.body = Blob.valueOf('data of attachment');
        Insert attachment;         
        
        NCPattachdocumentWra obj = new NCPattachdocumentWra();
        obj.Accesstoken = at.Access_Token__c;
        obj.RecordID = attachment.Id;
        
        RestRequest req1 = new RestRequest();
        RestResponse res1 = new RestResponse();
        req1.requestURI = '/services/apexrest/NCPDownloddocument/'; 
        req1.httpMethod = 'POST';
        req1.requestBody = Blob.valueOf(JSON.serialize(obj));     
        RestContext.request = req1;
        RestContext.response = res1;
        RestContext.request.addHeader('Content-Type', 'application/json');
        
        Test.startTest(); 
        	NCPDownloadattachment.NCPDownloadattachment();
        Test.stopTest();
    }
    
    static testMethod void  testErrorForNCPDownloadattachment(){
        
        RestRequest req1 = new RestRequest();
        RestResponse res1 = new RestResponse();
        req1.requestURI = '/services/apexrest/NCPDownloddocument/'; 
        req1.httpMethod = 'POST';
        req1.requestBody = Blob.valueOf('{"value":"Test response"}');     
        RestContext.request = req1;
        RestContext.response = res1;
        RestContext.request.addHeader('Content-Type', 'application/json');
        
        Test.startTest(); 
        	NCPDownloadattachment.NCPDownloadattachment();
        Test.stopTest();
    }

}