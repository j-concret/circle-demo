@isTest
public class NCPDefaultUpdate_Test {
    
    public static testMethod void CPDefaultUpdation_Test1(){
        
        //Test Data
        Account acc = TestDataFactory.myClient();
        CPDefaults__c cpDef = new CPDefaults__c(Client_Account__c = acc.Id, Order_Type__c = 'Pro-Forma Quote');
        insert cpDef;
        
        //Json Body
        String json = '{"DefaultPref" : [{"CPDefaultRecordID":"'+cpDef.Id+'","ClientID":"'+acc.Id+'","Chargeable_Weight_Units":"KGs","ion_Batteries":"Yes","Order_Type":"Pro-Forma Quote",'+
            +'"Package_Dimensions":"Cm","Package_Weight_Units":"KGs","Second_hand_parts":"Yes","Service_Type":"IOR","TecEx_to_handle_freight":"Yes",'+
            +'"Country":"South Africa","DefaultCurrency":"CZK"}]}';
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
        req.requestURI = '/services/apexrest/NCPDefaultUpdate'; 
        req.httpMethod = 'Post';
        req.addHeader('Content-Type', 'application/json');
        req.requestBody = Blob.valueof(json);
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
        NCPDefaultUpdate.CPDefaultUpdation();
        Test.stopTest(); 
    }
    
    //Covering Catch exception
    public static testMethod void CPDefaultUpdation_Test2(){
        
        //Test Data
        Account acc = TestDataFactory.myClient();
        Contact con = TestDataFactory.myContact();
        
        //Json Body
        String json = '{"DefaultPref" : [{"CPDefaultRecordID":"'+acc.Id+'","ClientID":"'+con.Id+'","Chargeable_Weight_Units":"KGs","ion_Batteries":"Yes","Order_Type":"Pro-Forma Quote",'+
            +'"Package_Dimensions":"Cm","Package_Weight_Units":"KGs","Second_hand_parts":"Yes","Service_Type":"IOR","TecEx_to_handle_freight":"Yes",'+
            +'"Country":"South Africa","DefaultCurrency":"CZK"}]}';
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
        req.requestURI = '/services/apexrest/NCPDefaultUpdate'; 
        req.httpMethod = 'Post';
        req.addHeader('Content-Type', 'application/json');
        req.requestBody = Blob.valueof(json);
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
        NCPDefaultUpdate.CPDefaultUpdation();
        Test.stopTest(); 
    }

}