public class CPCreateSOOrderPackWrapper {
		public String ClientID;
        public String ContactID;
	
    public class ShipmentOrder_Packages {
		public String SOID;
        public String Weight_Unit;
		public String Dimension_Unit;
		public Integer Packages_of_Same_Weight;
		public String Length;
		public String Height;
		public String Breadth;
		public String Actual_Weight;
	}

	public List<ShipmentOrder_Packages> ShipmentOrder_Packages;

	
	public static CPCreateSOOrderPackWrapper parse(String json) {
		return (CPCreateSOOrderPackWrapper) System.JSON.deserialize(json, CPCreateSOOrderPackWrapper.class);
	}
}