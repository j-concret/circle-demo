@RestResource(urlMapping='/Addlineitems/*')
Global class CSIAddlineitems {
    @Httppost
    global static void CreateParts(){
        
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        Blob body = req.requestBody;
        res.addHeader('Content-Type', 'application/json');
        String requestString = body.toString();
        NCPAddPartsWrapper rw = (NCPAddPartsWrapper)JSON.deserialize(requestString,NCPAddPartsWrapper.class);
        
        try {
            List<Access_token__c> at = [SELECT status__c,Access_Token__c FROM Access_token__c WHERE Access_Token__c =: rw.Accesstoken and status__c = 'Active' LIMIT 1];
            
            //Parts creation
            
            If(!rw.Parts.isEmpty() && at[0].status__c=='Active' ){
                
                
                list<Part__c> PAL = new list<Part__c>();                
                for(Integer l=0; rw.Parts.size()>l;l++) {
                    
                    Part__c PA = new Part__c(
                        
                        Name =rw.Parts[l].PartNumber,
                        Description_and_Functionality__c =rw.Parts[l].PartDescription,
                        Quantity__c =rw.Parts[l].Quantity,
                        Commercial_Value__c =rw.Parts[l].UnitPrice,
                        Shipment_Order__c=rw.Parts[l].SOID
                        
                    );
                    PAL.add(PA);
                }
                system.debug('Parts list--->'+PAL); 
                system.debug('Parts list Parts.size()--->'+rw.Parts.size());
                Insert PAL;
                
                
                
                
                res.responseBody = Blob.valueOf(JSON.serializePretty(PAL));
                res.statusCode = 200;
                API_Log__c Al = New API_Log__c();
                Al.Account__c=rw.AccountID;
                //    Al.Login_Contact__c=rw.ContactID;
                Al.EndpointURLName__c='CSIAddlineitems';
                Al.Response__c='Success - Parts created from Record';
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al; 
            }
        }
        Catch(Exception e){
            
            String ErrorString ='Something went wrong while creating parts, please contact support_SF@tecex.com';
            res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
            res.statusCode = 500;
            API_Log__c Al = New API_Log__c();
            Al.Account__c=rw.AccountID;
            //   Al.Login_Contact__c=rw.ContactID;
            Al.EndpointURLName__c='CSIAddlineitems';
            Al.Response__c=e.getMessage()+' at line number: '+e.getLineNumber();
            Al.StatusCode__c=string.valueof(res.statusCode);
            Insert Al;
            
            
        }
        
        
        
        
    }
    
}