public class SO_Travel_History_Creation {
    
    @AuraEnabled
    
    public static Map<String,Object> createSOTravelObject(String source,String location,String description,Boolean StatusChanged,Boolean SubStatusChanged,Boolean textUpdate,String SOID){
  
			Map<String,Object> response = new Map<String,Object>{'status' => 'OK'};
            try{
                
                System.debug('SubStatusChanged '+SubStatusChanged);
                SOTravelCreation.forManualStatusChange(source,location,description,StatusChanged,SubStatusChanged,textUpdate,SOID);
               
                
            }catch(Exception e){
                System.debug('e msg');
                System.debug(e.getMessage());
                response.put('status','ERROR');
                response.put('message',e.getMessage());
                
            }
        
        return response;
        
    }
    
    @AuraEnabled
    public static Map<String,Object> getDescriptionFromCPA(String CPAId,String status){
        Map<String,Object> response = new Map<String,Object>{'status' => 'OK'};
            try{
                Id profileId=userinfo.getProfileId();
                String profileName=[Select Id,Name from Profile where Id=:profileId].Name;
                if(profileName != 'System Administrator'){
                    response.put('nonSystemAdminUser',true);
                }else{
                    response.put('nonSystemAdminUser',false);
                }
                Map<String,String> MappedStatusMap = new Map<String,String>{
                    'Cost Estimate'=>'Quote Created',
                        'Shipment Pending'=>'Compliance Pending',
                        'CI, PL and DS Received'=>'Compliance Pending',
                        'AM Approved CI,PL and DS\'s'=>'Compliance Pending',
                        'Complete Shipment Pack Submitted for Approval'=>'Compliance Pending',
                        'License Pending'=>'Compliance Pending',
                        'Awaiting AWB'=>'Compliance Pending',
                        'Customs Approved (Pending Payment)'=>'Compliance Pending',
                        'Client Approved to ship'=>'Approved to Ship',	
                        'In Transit to Hub'=>'In-Transit',
                        'Arrived at Hub'=>'In-Transit',
                        'In transit to country'=>'In-Transit',
                        'Arrived in Country, Awaiting Customs Clearance'=>'Arrived in Country',
                        'Cleared Customs'=>'Cleared Customs',
                        'Final Delivery in Progress'=>'Final Delivery in Transit',
                        'Awaiting POD'=>'Delivered',
                        'POD Received'=>'Delivered',
                        'Customs Clearance Docs Received'=>'Delivered',
                        'On Hold'=>'HALT'
                        };
                            SO_Status_Description__c SOSD = [SELECT Id , Name ,Description_ETA__c
                                                             FROM SO_Status_Description__c 
                                                             where CPA_v2_0__r.Id = :CPAId 
                                                             AND Type__c = 'Shipping status' 
                                                             AND Status__c = :MappedStatusMap.get(status) limit 1];
                response.put('SoDesc',SOSD);
                
            }catch(Exception e){
                
                response.put('status','ERROR');
                response.put('message',e.getMessage());
                
            }
        
        return response;
        
    }
    
    @AuraEnabled
    public static Boolean validateApprovedToShip(String SOID){
        
        try{
            Shipment_Order__c shipment = [Select Customs_Compliance__c, Shipping_Documents__c, Pick_up_Coordination__c, Invoice_Payment__c from Shipment_Order__c where Id = :SOID LIMIT 1];
		System.debug('query- value--> '+ shipment);
            if((shipment.Customs_Compliance__c == 'Green' && shipment.Shipping_Documents__c == 'Green') && (shipment.Pick_up_Coordination__c == 'Green' && shipment.Invoice_Payment__c == 'Green')){
                System.debug('TRUE');
                return true;
            }
            else{
                System.debug('FALSE');
                return false;
            }
            
        }
        catch(Exception ex){
            System.debug('error-->'+ ex.getStackTraceString());
            
            return false;
        }
        
                
    }
}