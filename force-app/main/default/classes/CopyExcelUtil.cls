/**
* ApexClass   	: CopyExcelUtil.cls
* Description	: Utility methods required for CopyExcelToSObjectsController
*/
public with sharing class CopyExcelUtil {
    
    /**
* @description : Get the custom setting required. Else throw the exception.
*/
    private static InvoiceExcelCopyPasteConfiguration__c getConfiguration(String sobjectName){
        InvoiceExcelCopyPasteConfiguration__c config = InvoiceExcelCopyPasteConfiguration__c.getInstance(sobjectName);
        if(config == null){
            throw new CopyExcelUtilException('Please configure the records for InvoiceExcelCopyPasteConfiguration__c.');
        }
        return config;
    }
    
    /**
* @description : Gives the default fields from the fieldset provided in custom setting
*/
    public static List<String> getDefaultFields(String sobjectName){
        List<String> fields = new List<String>();
        InvoiceExcelCopyPasteConfiguration__c config = getConfiguration(sobjectName);
        fields =getFieldSetFields(sobjectName,config.ParentFieldSet__c);	
        return fields;
    }
    
    /**
* @description : Gives the default fieldset name
*/
    public static String getDefaultFieldSet(String sobjectName){
        InvoiceExcelCopyPasteConfiguration__c config = getConfiguration(sobjectName);
        return config.ParentFieldSet__c;
    }
    
    /**
* @description : Gives the Maximum Rows that can be copied 
*/
    public static Decimal getMaxRows(String sobjectName) {
        InvoiceExcelCopyPasteConfiguration__c config = getConfiguration(sobjectName);
        return config.MaxRows__c;
    }
    
    /**
* @description : Custom exception to throw error when custom settings are not configured
*/
    public class CopyExcelUtilException extends Exception {}
    
    /**
* @description : Get the fieldset name from where the columns will come.
*/
    public static String getDisplayFieldSetName(String sobjectName){		
        InvoiceExcelCopyPasteConfiguration__c config = getConfiguration(sobjectName);
        return  config.DisplayFieldset__c;
    }
    
    
    
    //@description : Maintains the field vs type    
    public static Map<String,Schema.DisplayType> fieldVsType = new Map<String,Schema.DisplayType>();
    
    /**
* @description : Get the display type provided sobject name and field.
*/
    public static Schema.DisplayType getDisplayType(String sobjectName, String field){
        if(fieldVsType.get(field)==null){
            //do the global describe
            Map<String, Schema.SObjectType> GlobalDescribeMap = Schema.getGlobalDescribe(); 
            Schema.SObjectType SObjectTypeObj = GlobalDescribeMap.get(sobjectName);	                 
            Map<String, SObjectField> fieldMap = SObjectTypeObj.getDescribe().fields.getMap();
            Schema.DisplayType displayType = fieldMap.get(field).getDescribe().getType();
            fieldVsType.put(field,displayType);
        }
        return fieldVsType.get(field);
    }	
    
    /**
* @description : Prepares the sobject by adding appropriate data conversion
*/
    public static Map<String,Object> prepareSObject(Map<String,Object> rawSObject,String sobjectName){		  
        Map<String,Object> sobjectRecord = new Map<String,Object>();
        for(String field : rawSObject.keySet()){
            if(rawSObject.get(field)==null){
                continue;
            }
            if(rawSObject.get(field)!=null && (''+rawSObject.get(field)).trim() == ''){
                continue;
            }
            
            Schema.DisplayType displayType = getDisplayType(sobjectName,field);
            //apply the coversion depending upon the type of the field
            if(displayType == Schema.DisplayType.Date){
                sobjectRecord.put(field, Date.parse(''+rawSObject.get(field)));
            }else if(displayType == Schema.DisplayType.DateTime){
                sobjectRecord.put(field,DateTime.parse(''+rawSObject.get(field)));
            }else if(displayType == Schema.DisplayType.Double || displayType == Schema.DisplayType.Currency){
                // Remove any $ or , seperator.
                String value = String.valueOf(rawSObject.get(field));
                String newValue = value.replaceAll('[^.0-9]', '');
                sobjectRecord.put(field,Double.valueOf(''+newValue));
            }else if(displayType == Schema.DisplayType.Integer){
                sobjectRecord.put(field,Integer.valueOf(''+rawSObject.get(field)));
            }else if(displayType == Schema.DisplayType.Boolean){
                sobjectRecord.put(field,Boolean.valueOf(''+rawSObject.get(field)));
            }else{
                String value = String.valueOf(rawSObject.get(field));
                String newValue = value;
                if (field.containsIgnoreCase('hts')) {
                   newValue = value.replaceAll('[^0-9]', '');
                }
                sobjectRecord.put(field,newValue);
            }
        }
        return sobjectRecord;
    }
    
    /**
* @description : Get the list of fields from the fieldset
*/
    public static List<String> getFieldSetFields(String objectName,String fieldSetName){
        List<String> fieldList = new List<String>();
        //do the global describe
        Map<String, Schema.SObjectType> GlobalDescribeMap = Schema.getGlobalDescribe(); 
        Schema.SObjectType SObjectTypeObj = GlobalDescribeMap.get(objectName);
        Schema.DescribeSObjectResult DescribeSObjectResultObj = SObjectTypeObj.getDescribe();            
        Schema.FieldSet fieldSetObj = DescribeSObjectResultObj.FieldSets.getMap().get(fieldSetName);               	  
        if(fieldSetName!=null && objectName!=null){
            //To avoid duplicate entries using set to add elements	        
            for(Schema.FieldSetMember member : fieldSetObj.getFields()){
                fieldList.add(member.fieldPath.toLowerCase());
            }   
        }     
        return fieldList;  
    }
    
    /**
* @description : Get the list of api names of the fields from the fieldset
*/
    public static List<String> getFieldSetFieldNames(String objectName,String fieldSetName){
        List<String> fieldList = new List<String>();
        //do the global describe
        Map<String, Schema.SObjectType> GlobalDescribeMap = Schema.getGlobalDescribe(); 
        Schema.SObjectType SObjectTypeObj = GlobalDescribeMap.get(objectName);
        Schema.DescribeSObjectResult DescribeSObjectResultObj = SObjectTypeObj.getDescribe();            
        Schema.FieldSet fieldSetObj = DescribeSObjectResultObj.FieldSets.getMap().get(fieldSetName);               	  
        if(fieldSetName!=null && objectName!=null && String.isNotBlank(fieldSetName)){
            //To avoid duplicate entries using set to add elements	        
            for(Schema.FieldSetMember member : fieldSetObj.getFields()){
                fieldList.add(member.getLabel());
            }   
        }    
        return fieldList;  
    }

    /**
        * @description : Get the list of API fields for the column name
    */
    public static List<String> getAPIFieldsForColumnList(String objectName,String fieldSetName, List<String> columnNames){
        List<String> fieldList = new List<String>();
        Set<String> columnNameSet = new Set<String>(columnNames);
        Map<String, String> apiColumnsMap = new Map<String, String>();
        //do the global describe
        Map<String, Schema.SObjectType> GlobalDescribeMap = Schema.getGlobalDescribe(); 
        Schema.SObjectType SObjectTypeObj = GlobalDescribeMap.get(objectName);
        Schema.DescribeSObjectResult DescribeSObjectResultObj = SObjectTypeObj.getDescribe();            
        Schema.FieldSet fieldSetObj = DescribeSObjectResultObj.FieldSets.getMap().get(fieldSetName);               	  
        if(fieldSetName!=null && objectName!=null){
            //To avoid duplicate entries using set to add elements	        
            for(Schema.FieldSetMember member : fieldSetObj.getFields()){
                if (columnNameSet.contains(member.getLabel())) {
                    apiColumnsMap.put(member.getLabel(), member.fieldPath.toLowerCase());                    
                }                
            }
            for (String columnName : columnNameSet) {
                fieldList.add(apiColumnsMap.get(columnName));
            }   
        }     
        return fieldList;  
    }
}