@RestResource(urlMapping='/Stripecheckoutstatus/*')
global class Stripewebhook {
    @Httppost
    global static void Stripewebhook(){
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        try{
            //  String sigHeader = req.params.get('Stripe-Signature');
            // String endpointSecret='whsec_Utqwqf4ZFgpXlIZ2aoR65cAjggI5EdnH'; //Produ ction
            //   String endpointSecret='whsec_tRTI7asaUzQDyi0UjloPunoqPIrUIyAO';// test
            Blob body = req.requestBody;
            string abc=body.toString();
            String PIId;
            List<Invoice_new__C> updateclientinvoice= new List<Invoice_new__C>(); 
            Map<String,Object> Jasondata= (Map<String,Object>)Json.deserializeUntyped(abc);
            string EvtID= (String)Jasondata.get('id');
            
            //String abc = '{"id":"evt_1JDOIzCmiZaJxkqvEacH6bLV","object":"event","api_version":"2018-11-08","created":1626331460,"data":{"object":{"id":"pi_1JDOISCmiZaJxkqviTS4DdKy","object":"payment_intent","allowed_source_types":["card"],"amount":100,"amount_capturable":0,"amount_received":100,"application":null,"application_fee_amount":null,"canceled_at":null,"cancellation_reason":null,"capture_method":"automatic","charges":{"object":"list","data":[{"id":"ch_1JDOIyCmiZaJxkqvnbMozKuW","object":"charge","amount":100,"amount_captured":100,"amount_refunded":0,"amount_updates":[],"application":null,"application_fee":null,"application_fee_amount":null,"balance_transaction":"txn_1JDOIyCmiZaJxkqvSJdcWPT9","billing_details":{"address":{"city":null,"country":"ZA","line1":null,"line2":null,"postal_code":null,"state":null},"email":"byronf@tecex.com","name":"Byron","phone":null},"calculated_statement_descriptor":"VAT IT USA - TECEX","captured":true,"created":1626331460,"currency":"usd","customer":"cus_Jr6V7Q24IeMeJF","description":null,"destination":null,"dispute":null,"disputed":false,"failure_code":null,"failure_message":null,"fraud_details":{},"invoice":null,"livemode":false,"metadata":{},"on_behalf_of":null,"order":null,"outcome":{"network_status":"approved_by_network","reason":null,"risk_level":"normal","risk_score":31,"seller_message":"Payment complete.","type":"authorized"},"paid":true,"payment_intent":"pi_1JDOISCmiZaJxkqviTS4DdKy","payment_method":"pm_1JDOIwCmiZaJxkqvulcryMCv","payment_method_details":{"card":{"brand":"visa","checks":{"address_line1_check":null,"address_postal_code_check":null,"cvc_check":"pass"},"country":"US","exp_month":10,"exp_year":2025,"fingerprint":"5toHT22GOBUYQb96","funding":"credit","installments":null,"last4":"4242","network":"visa","three_d_secure":null,"wallet":null},"type":"card"},"receipt_email":"byronf@tecex.com","receipt_number":null,"receipt_url":"https://pay.stripe.com/receipts/acct_1DW4qECmiZaJxkqv/ch_1JDOIyCmiZaJxkqvnbMozKuW/rcpt_Jr6WyyeqbcbnKwk2bDBJ1ByGHjDiaQc","refunded":false,"refunds":{"object":"list","data":[],"has_more":false,"total_count":0,"url":"/v1/charges/ch_1JDOIyCmiZaJxkqvnbMozKuW/refunds"},"review":null,"shipping":null,"source":null,"source_transfer":null,"statement_descriptor":null,"statement_descriptor_suffix":null,"status":"succeeded","transfer_data":null,"transfer_group":null}],"has_more":false,"total_count":1,"url":"/v1/charges?payment_intent=pi_1JDOISCmiZaJxkqviTS4DdKy"},"client_secret":"pi_1JDOISCmiZaJxkqviTS4DdKy_secret_7DQ3pJK6GjAvfTHBHaCbN2v1i","confirmation_method":"automatic","created":1626331428,"currency":"usd","customer":"cus_Jr6V7Q24IeMeJF","description":null,"invoice":null,"last_payment_error":null,"livemode":false,"metadata":{},"next_action":null,"next_source_action":null,"on_behalf_of":null,"payment_method":"pm_1JDOIwCmiZaJxkqvulcryMCv","payment_method_options":{"card":{"installments":null,"network":null,"request_three_d_secure":"automatic"}},"payment_method_types":["card"],"receipt_email":"byronf@tecex.com","review":null,"setup_future_usage":null,"shipping":null,"source":null,"statement_descriptor":null,"statement_descriptor_suffix":null,"status":"succeeded","transfer_data":null,"transfer_group":null}},"livemode":false,"pending_webhooks":1,"request":{"id":"req_OZz4ScL4uEWsq0","idempotency_key":null},"type":"payment_intent.succeeded"}';
            Map<String, Object> data = (Map<String, Object>)Jasondata.get('data');
            Map<String, Object> object1 = (Map<String, Object>)data.get('object');
            
            
            
            
            //Charges
            //ChargesObject
            //ChrgesObjectData - Receipt URL
            //ChrgesObjectDatapayment_method_details
            //ChrgesObjectDatapayment_method_detailscard - last4
            
            PIID = (String)object1.get('id');
            
            system.debug('piid--->'+piid);
            
            List<Invoice_new__C> ClientInvoice = [Select Id,invoice_status__c,Stripe_last4__c,Stripe_receipt_URL__c,Stripe_Paymnet_Intent__c,Client_Stripe_Payment_Status__c,Stripe_status_date__c,Stripe_status_reason__c from Invoice_new__C where Stripe_Paymnet_Intent__c=: PIID limit 1];
            for(Invoice_new__C CI: ClientInvoice ){
                if((String)Jasondata.get('type') =='payment_intent.canceled'){
                    system.debug('in PI cancelled block');
                    CI.Client_Stripe_Payment_Status__c=(String)object1.get('status');
                    CI.Stripe_status_date__c = system.now();
                    CI.Stripe_status_reason__c=(String)object1.get('cancellation_reason');
                    
                }
                Else if((String)Jasondata.get('type') =='payment_intent.succeeded') {
                    system.debug('in PI Success block');
                    Map<String, Object> Charges = (Map<String, Object>)object1.get('charges');
                    List<Object> CObject = (list<Object>)Charges.get('data');
                    Map<String, Object> dataCharges = (Map<String, Object> )CObject[0];
                    // string receiptURL= (String)dataCharges.get('receipt_url');
                    Map<String, Object> paymentDetails = (Map<String, Object>)dataCharges.get('payment_method_details');//.get('card');
                    Map<String, Object> cardDetails = (Map<String, Object>)paymentDetails.get('card');
                    //string last4= (String)cardDetails.get('last4');
                    
                    CI.Client_Stripe_Payment_Status__c=(String)object1.get('status');
                    CI.Stripe_status_date__c = system.now();
                    CI.Stripe_status_reason__c='Payment completed sucesfully';
                    CI.Stripe_receipt_URL__c=(String)dataCharges.get('receipt_url');
                    CI.Stripe_last4__c=(String)cardDetails.get('last4');
                    CI.invoice_status__c='Paid via Stripe';
                    
                }
                updateclientinvoice.add(CI);
                
            }
            update updateclientinvoice;
            
        }
        catch(Exception e){
            String ErrorString =e.getMessage()+e.getLineNumber()+e.getStackTraceString();
            API_Log__c apiLog = new API_Log__c (StatusCode__c = '888',Response__c = ErrorString, Calling_Method_Name__c  = 'Stripe webhook');
            insert apiLog;
            System.debug('IPN Exception Msg--->'+ErrorString);
            
        }
        
    }
    
    
}