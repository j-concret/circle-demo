@RestResource(urlMapping='/GetUserPicture/*')
Global class NCPGetProfilepic {
    
    @Httppost
    global static void NCPGetProfilepic(){
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        Blob body = req.requestBody;
        res.addHeader('Content-Type', 'application/json');
        String requestString = body.toString();
        NCPGetProfilepicWra rw  = (NCPGetProfilepicWra)JSON.deserialize(requestString,NCPGetProfilepicWra.class);
        try{
            //Accesstoken test has to come here
            Access_token__c At =[select status__c,Access_Token__c from Access_token__c where Access_Token__c=:rw.Accesstoken ];
            if( at.status__C =='Active'){
                List<ID> splitted = new List<ID>();
                List<User> con;
                
                For(integer i=0;rw.UserIds.size()>i;i++){splitted.add(rw.UserIds[i].UserId); }
                System.debug('splitted-->'+splitted);
                
                con=[select Id, Username , LastName , FirstName , Name , CompanyName , Division , Department , Title , Street , City , State , PostalCode , Country , Address , Email , Phone , Fax , MobilePhone , IsActive , UserRoleId , ProfileId , UserType , LastLoginDate ,  ContactId , AccountId , FullPhotoUrl  from User where id in:splitted];
                
                
                if(!con.isempty()){
                    res.responseBody = Blob.valueOf(JSON.serializePretty(Con));
                    res.addHeader('Content-Type', 'application/json');
                    res.statusCode = 200;
                    
                    API_Log__c Al = New API_Log__c();
                    Al.EndpointURLName__c='GetUserProfile';
                    // Al.Login_Contact__c=rw.contactID;
                    Al.StatusCode__c=string.valueof(res.statusCode);
                    AL.Response__c = 'Success - User Profile details are sent';
                    Insert Al;  
                }
                Else{
                    
                    JSONGenerator gen = JSON.createGenerator(true);
                    gen.writeStartObject();
                    
                    gen.writeFieldName('Success');
                    gen.writeStartObject();
                    
                    if( con.isempty()) {gen.writeNullField('Response');} else{gen.writeStringField('Response', 'Information not available');}
                    
                    gen.writeEndObject();
                    gen.writeEndObject();
                    String jsonData = gen.getAsString();
                    res.responseBody = Blob.valueOf(jsonData);
                    res.addHeader('Content-Type', 'application/json');
                    res.statusCode = 400;        
                    
                }
                
            }  
        } catch(Exception e){
            
            String ErrorString ='Something went wrong, please contact support_SF@tecex.com'+e.getMessage();
            res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
            res.addHeader('Content-Type', 'application/json');
            res.statusCode = 400;
            
            API_Log__c Al = New API_Log__c();
            //Al.Account__c=rw.AccountID;
            // Al.Login_Contact__c=rw.contactID;
            Al.EndpointURLName__c='GetUserProfile';
            Al.Response__c=ErrorString;
            Al.StatusCode__c=string.valueof(res.statusCode);
            Insert Al;
            
        }
        
    }
    
    
}