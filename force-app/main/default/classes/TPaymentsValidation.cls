public class TPaymentsValidation {
    public static Set<id> accountIds = new  Set<id>();
    Public static void checkvalidation(){
        
        string result;
        string resbody;
        string resstatus;
        List<Invoice_New__c> opps= new List<Invoice_New__c>(); 
        String supplierRTypeId = Schema.Sobjecttype.Invoice_New__c.getRecordTypeInfosByDeveloperName().get('Supplier').getRecordTypeId();
        system.debug('Inside TPaymentsValidation');
        
        Map<String, String> xmlDataMap = new Map<String,String>();
       // String masterKey = 'j2O2HSydXz/YqW6fLD1ZDXupBI8Q6yTf5euM3dRZD7IYfwodhvC12N8M/Gc5lckO'; //Sandbox
        String masterKey = '6/AruxtA1M+gP+g5fZRtpYm0jXJdaQWZNv6k8oJu6DBYEH5papLWbnJGtc3o1SVg'; // Production
        String Payername = 'TecEx';
        
        Dom.Document doc = new Dom.Document();
        dom.XmlNode envelope = doc.createRootElement('soap:Envelope', null, null);
        envelope.setAttribute('xmlns:xsi','http://www.w3.org/2001/XMLSchema-instance');
        envelope.setAttribute('xmlns:xsd','http://www.w3.org/2001/XMLSchema');
        envelope.setAttribute('xmlns:soap','http://schemas.xmlsoap.org/soap/envelope/');
        dom.XmlNode body = envelope.addChildElement('soap:Body',null,null);
        
        //Dom.Xmlnode tipaltiPaymentsOrdersnode = PayeePayable.addChildElement('tipaltiPaymentsOrders', null, null);
        
        AggregateResult[] res = [SELECT Account__c, SUM(Amount_Outstanding_Local_Currency__c)sum 
                                 FROM Invoice_New__c 
                                 where  RecordTypeId = :supplierRTypeId
                                 AND Due_Date__c = TODAY
                                 AND Submitted_to_Tipalti__c = false 
                                 AND Account__r.Eligible_for_Tipalti_Autopay__c = true
                                 AND Tipalti_validation_completed__c = false
                                 AND Invoice_Type__c != 'Credit Note'
                                 AND ( Payment_Tracking__c = 'Ready For Payment'  or Payment_Tracking__c = 'Urgent') 
                                 AND (Invoice_Status__c = 'FM Approved' OR Invoice_Status__c = 'Partial Payment') 
                                 GROUP BY Account__c limit 45];
        
                System.debug('res Created');
        System.debug(JSON.serializePretty(res)); 
        Double sum1 = (res!=null && !res.isEmpty()) ? (double)res[0].get('sum') : 1.11;
        integer sum2 = sum1.intValue();
        
        // String Amount = (res!=null && !res.isEmpty()) ? String.valueOf((double)res[0].get('sum')) : '';
        String Amount = string.valueOf(sum2);
        
        Decimal Timestampheader1 = datetime.now().gettime()/1000;
        System.debug('Timestampheader1'+Timestampheader1);
        String untime=string.valueOf(Timestampheader1);
        
        String params= Payername+untime+Amount;
        
        String FinalKey = generateHmacSHA256Signature(params,masterKey);   
        List<API_Log__c> apiLogList = new  List<API_Log__c>();
        
        for(AggregateResult ag:res){
            //  dom.XmlNode TipaltiPaymentOrderItemnode=tipaltiPaymentsOrdersnode.addChildElement('TipaltiPaymentOrderItem',null, null);
            Datetime Timestampheader2 = datetime.now();
            String Refcodefinal = Timestampheader2.format('MM-dd-yyyy-mm-ss');
            id accid=(id)ag.get('Account__c');
            accountIds.add(accid);
            double amt= ((double)ag.get('sum') != null) ?(double)ag.get('sum') : 0;
            Dom.Xmlnode PayeePayable = body.addChildElement('PayeePayable', null, null);
            PayeePayable.setAttribute('xmlns','http://Tipalti.org/');
            Dom.Xmlnode PayerNamenode = PayeePayable.addChildElement('payerName', null, null);
            PayerNamenode.addTextNode(Payername) ; 
            PayeePayable.addChildElement('Idap', null, null).addTextNode(accid);
            PayeePayable.addChildElement('Amount', null, null).addTextNode(string.valueof(amt));
            Decimal Timestampheader = datetime.now().gettime()/1000;
            
            PayeePayable.addChildElement('timeStamp', null, null).addTextNode(string.valueof(Timestampheader));
            PayeePayable.addChildElement('key', null, null).addTextNode(FinalKey);
            
            
            
            
            String xmlString =doc.toXmlString();
            System.debug('xmlString =' + xmlString);
            result=xmlString;
            Integer strlength=xmlString.length();
            string length=string.valueOf(strlength);
            
            API_Log__c apiLog = new API_Log__c (Response__c = xmlString, Calling_Method_Name__c  = 'Before Validation Check, XML');
            apiLogList.add(apiLog);
            
            makeApiCallout(xmlString,length);
        }
        
        insert apiLogList;
        
        markTipaltiValidationCompleted();
    }
    
    public static void generateAPILogs(HttpResponse response,String url){
        API_Log__c apiLog = new API_Log__c (StatusCode__c = String.valueOf((Integer)response.getStatusCode()),Response__c = response.getBody(), Calling_Method_Name__c  = url);
        insert apiLog;
    }
    
    public  static String generateHmacSHA256Signature(String Keyvalue,string ClientsecreteValue) 
    {     
        String algorithmName = 'HmacSHA256';
        Blob hmacData = Crypto.generateMac(algorithmName,  Blob.valueOf(KeyValue), Blob.valueOf(ClientsecreteValue));
        String hextext = EncodingUtil.convertToHex(hmacData);
        
        return hextext;
    }
    
    @future(callout = True)
    public static void makeApiCallout(String xmlString,String length){
        string result;
        string resbody;
        string resstatus;
        Http P=new Http();
        HttpRequest req = new HttpRequest();
        
      // req.setEndpoint('https://api.sandbox.tipalti.com/v9/payerfunctions.asmx'); //Sandbox
        req.setEndpoint('https://api.tipalti.com/v9/PayerFunctions.asmx'); // Production
        req.setMethod('POST');
        req.setHeader('Content-Type','text/xml;charset=utf-8');
        req.setHeader('Content-Length', length);
        req.setHeader('SOAPAction', 'http://Tipalti.org/ProcessPayments');
        req.setBody(xmlString);
        
        HttpResponse response = P.send(req);
        
        system.debug('status code is >>>>'+ response.getStatusCode() + ' ' + response.getStatus());
        system.debug('status code is >>>>'+ response.getStatusCode() + ' ' + response.getStatus());
        System.debug('Body is >>>>'+response.getBody());
        
        if (response.getStatus()=='OK') {
            System.debug('inside construction');
            generateAPILogs(response,'checkvalidation success Response');
        } else {
            System.debug('Body is >>>>'+response.getBody());
            system.debug('status code is >>>>'+ response.getStatusCode() + ' ' + response.getStatus());
            resbody=response.getBody();
            resstatus=response.getStatus();
            generateAPILogs(response,'checkvalidation failure Response');
        }   
    }
    
    public static void markTipaltiValidationCompleted(){
        String supplierRTypeId = Schema.Sobjecttype.Invoice_New__c.getRecordTypeInfosByDeveloperName().get('Supplier').getRecordTypeId();
        System.debug('accountIds accountIds');
		System.debug(accountIds);
        List<Invoice_New__c> invoiceList = new List<Invoice_New__c>();
        For(Invoice_New__c invoice : [SELECT Account__c,Submitted_to_Tipalti__c, Tipalti_validation_completed__c 
                                      FROM Invoice_New__c 
                                      where  RecordTypeId = :supplierRTypeId
                                      AND Due_Date__c = TODAY
                                      AND Submitted_to_Tipalti__c = false 
                                      AND Account__r.Eligible_for_Tipalti_Autopay__c = true 
                                      AND Tipalti_validation_completed__c = false
                                      AND Invoice_Type__c != 'Credit Note'
                                      AND ( Payment_Tracking__c = 'Ready For Payment'  or Payment_Tracking__c = 'Urgent') 
                                       AND (Invoice_Status__c = 'FM Approved' OR Invoice_Status__c = 'Partial Payment') 
                                      AND Account__c IN :accountIds
                                      ]){
                                          invoice.Tipalti_validation_completed__c = true;
                                          invoiceList.add(invoice);       
                                      }
        update invoiceList;
        System.debug('Updating the TipaltiValidationCompleted to TRUE');
        System.debug(JSON.serializePretty(invoiceList));
    }
}