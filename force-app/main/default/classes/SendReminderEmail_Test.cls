@IsTest
public class SendReminderEmail_Test {

    @testSetup
    static void setup(){
        UserRole r = new UserRole(DeveloperName = 'MyCustomRole', Name = 'My Role');
        insert r;
        
        User u = new User(
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id,
            LastName = 'Berkeley',
            Email = 'puser000@amamama.com',
            Username = 'puser000@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            FirstName='Vat',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            UserRoleId = r.Id
        );
        insert u;
        System.runAs(u) {
            List<GL_Account__c> glAccounts = new List<GL_Account__c>();
            glAccounts.add(new GL_Account__c(Name = '70001', GL_Account_Description__c = 'Prepaid Expenses', GL_Account_Short_Description__c = 'PE'));
            glAccounts.add(new GL_Account__c(Name = '70000', GL_Account_Description__c = 'Revenue Received In Advance', GL_Account_Short_Description__c = 'RRIA'));
            glAccounts.add(new GL_Account__c(Name = '123456', GL_Account_Description__c = 'Forex', GL_Account_Short_Description__c = 'Forex'));
            glAccounts.add(new GL_Account__c(Name = 'Net Taxes', GL_Account_Description__c = 'Net Taxes', GL_Account_Short_Description__c = 'Net Taxes'));
            glAccounts.add(new GL_Account__c(Name = '999999', GL_Account_Description__c = 'Revenue Received In Advance', GL_Account_Short_Description__c = 'RevRecInAdv'));
            glAccounts.add(new GL_Account__c(Name = '610000', GL_Account_Description__c = 'Revenue Received In Advance', GL_Account_Short_Description__c = 'RevRecInAdv'));
            glAccounts.add(new GL_Account__c(Name = '110000', GL_Account_Description__c = 'Revenue', GL_Account_Short_Description__c = 'Revenue'));
            glAccounts.add(new GL_Account__c(Name = '120000', GL_Account_Description__c = 'Cost of Sales', GL_Account_Short_Description__c = 'Cost of Sale'));
            glAccounts.add(new GL_Account__c(Name = '314500', GL_Account_Description__c = 'Bank Charges', GL_Account_Short_Description__c = 'Bank Charges'));
            glAccounts.add(new GL_Account__c(Name = '610000', GL_Account_Description__c = 'Customer Control Account', GL_Account_Short_Description__c = 'AR Control'));
            glAccounts.add(new GL_Account__c(Name = '910500', GL_Account_Description__c = 'Unallocated Receipts', GL_Account_Short_Description__c = 'Unallocated'));
            glAccounts.add(new GL_Account__c(Name = '910501', GL_Account_Description__c = 'Unallocated Receipts Customers', GL_Account_Short_Description__c = 'UnallocatedC'));
            glAccounts.add(new GL_Account__c(Name = '920000', GL_Account_Description__c = 'Supplier Control Account', GL_Account_Short_Description__c = 'AP Control'));
            glAccounts.add(new GL_Account__c(Name = '921000', GL_Account_Description__c = 'Accruals', GL_Account_Short_Description__c = 'Accruals'));
            glAccounts.add(new GL_Account__c(Name = '110151', GL_Account_Description__c = 'Accruals', GL_Account_Short_Description__c = 'Accruals'));
            glAccounts.add(new GL_Account__c(Name = '240000', GL_Account_Description__c = 'Accruals', GL_Account_Short_Description__c = 'Accruals'));
            
            insert glAccounts;
            
            
            List<Dimension__c> dimensionList = new List<Dimension__c>();
            dimensionList.add(new Dimension__c(Name = 'MASELIM',  Dimension_Description__c = 'Test 610000', Short_Dimension_Description__c = 'Test 610000', Dimension_Type_Code__c = 'PRO'));
            insert dimensionList;
            
            CountryandRegionMap__c crm = new CountryandRegionMap__c();
            crm.Name = 'Brazil';
            crm.Country__c = 'Brazil';
            crm.European_Union__c = 'No';
            crm.Region__c = 'South America';
            
            insert crm;
            
            CountryandRegionMap__c crm1 = new CountryandRegionMap__c();
            crm1.Name = 'Australia';
            crm1.Country__c = 'Australia';
            crm1.European_Union__c = 'No';
            crm1.Region__c = 'Oceania';
            
            insert crm1;
            
            DefaultFreightValues__c df = new DefaultFreightValues__c();
            df.Name = 'Test';
            df.Courier_Base_Rate_KG__c = 12;
            df.Courier_Dangerous_Goods_premium__c = 12;
            df.Courier_Fixed_Charge__c = 12;
            df.Courier_Non_stackable_premium__c = 12;
            df.Courier_Oversized_premium__c = 12;
            df.FF_Base_Rate_KG__c = 12;
            df.FF_Dangerous_Goods_Fixed_Charge_Premium__c = 12;
            df.FF_Dangerous_Goods_premium__c = 12;
            df.FF_Dedicated_Pickup__c = 12;
            df.FF_Fixed_Charge__c = 12;
            df.FF_Non_stackable_Fixed_Charge_Premium__c = 12;
            df.FF_Non_stackable_premium__c = 12;
            df.FF_Oversized_Fixed_Charge_Premium__c = 12;
            df.FF_Oversized_premium__c = 12;
            insert df;
            
            RegionalFreightValues__c rfm = new RegionalFreightValues__c();
            rfm.Name = 'Oceania to South America';
            rfm.Courier_Discount_Premium__c = 12;
            rfm.Courier_Fixed_Premium__c = 10;
            rfm.Destination__c = 'Brazil';
            rfm.FF_Fixed_Premium__c = 123;
            rfm.FF_Regional_Discount_Premium__c = 12;
            rfm.Origin__c = 'Australia';
            rfm.Preferred_Courier_Id__c = 'test';
            rfm.Preferred_Courier_Name__c = 'test';
            rfm.Preferred_Freight_Forwarder_Id__c = 'test';
            rfm.Preferred_Freight_Forwarder_Name__c = 'test';
            rfm.Risk_Adjustment_Factor__c = 1;
            insert rfm;
            
            List<Account> accs = new List<Account>();
            Id EORRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('IOR/EOR Supplier').getRecordTypeId();
            Id ManufacturerRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Manufacturer').getRecordTypeId();
            Id AccClientRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Client').getRecordTypeId();
            
            Account account = new Account (name='Acme1', Type ='Client', 
                                           CSE_IOR__c = u.id, 
                                           Service_Manager__c = u.id, RecordTypeId = AccClientRecordTypeId,
                                           Financial_Manager__c=u.id,New_Invoicing_Structure__c = true, 
                                           Financial_Controller__c=u.id,Liability_Product_Agreement__c = 'Ad Hoc Agreement', 
                                           Tax_recovery_client__c  = FALSE,Dimension__c = dimensionList[0].Id,Debt_Reminder_Days_Before_Due_Date__c = 3 );
            Account account2 = new Account (name='Test Supplier', Type ='Supplier', ICE__c = u.id, RecordTypeId = EORRecordTypeId,New_Invoicing_Structure__c=true,Liability_Product_Agreement__c = 'Annual Rate Agreement');
            Account account3 = new Account (name='Test Manufacturer', Type ='Manufacturer', RecordTypeId = ManufacturerRecordTypeId, Manufacturer_Alias__c = 'Tester,Who' );
            Account account4 = new Account (name='TecEx Prospective Client', Type ='Client',New_Invoicing_Structure__c = true);
            accs.add(account);
            accs.add(account2);
            accs.add(account3);
            accs.add(account4);
            insert accs;
            
            List<Contact> cons = new List<Contact>();
            
            Contact contact = new Contact (Client_Notifications_Choice__c = 'opt-in', email = 's.swapnil@g.tecex.com', lastname ='Testing Individual',  AccountId = accs[0].Id,Include_in_invoicing_emails__c=true);
            Contact contact2 = new Contact (Client_Notifications_Choice__c = 'opt-in', email = 's.swapnil@g.tecex.com', lastname ='Testing supplier',  AccountId = accs[1].Id,Include_in_invoicing_emails__c=true);
            cons.add(contact);
            cons.add(contact2);
            insert cons;
            
            IOR_Price_List__c iorpl = new IOR_Price_List__c ( Client_Name__c = accs[0].Id, Name = 'Bulgaria', IOR_Fee__c = 0.15, On_Charge_Mark_up__c = 0.15,
                                                             TecEx_Shipping_Fee_Markup__c =0.40, IOR_Min_Fee__c = 100,  Admin_Fee__c =100, Set_up_fee__c =100, Bank_Fees__c =200, Tax_Rate__c = 0.20,
                                                             Estimated_Customs_Brokerage__c =250, Estimated_Customs_Clearance__c =250, Estimated_Customs_Handling__c =250, Estimated_Import_License__c =250, Destination__c = 'Brazil' );
            
            insert iorpl;
            IOR_Price_List__c iorpl1 = new IOR_Price_List__c ( Client_Name__c = accs[3].Id, Name = 'Belgium', IOR_Fee__c = 0.15, On_Charge_Mark_up__c = 0.15,
                                                              TecEx_Shipping_Fee_Markup__c =0.40, IOR_Min_Fee__c = 100,  Admin_Fee__c =100, Set_up_fee__c =100, Bank_Fees__c =200, Tax_Rate__c = 0.20,
                                                              Estimated_Customs_Brokerage__c =250, Estimated_Customs_Clearance__c =250, Estimated_Customs_Handling__c =250, Estimated_Import_License__c =250, Destination__c = 'Belgium' );
            
            insert iorpl1;
            
            country_price_approval__c cpa = new country_price_approval__c( name ='Brazil', Billing_term__c = 'DAP/CIF - IOR pays',  Airwaybill_Instructions__c = 'User IOR Address', Supplier__c = accs[1].Id, 
                                                                          In_Country_Specialist__c = u.Id, Clearance_Destination__c = 'Brazil', Preferred_Supplier__c = TRUE, Destination__c = 'Brazil' );
            insert cpa;
            
            Currency_Management2__c CM = new Currency_Management2__c (Name = 'US Dollar (USD)', Currency__c = 'US Dollar (USD)', ISO_Code__c = 'USD', Conversion_Rate__c = 1);
            insert CM; 
            
            Country__c country = new Country__c(Brokerage_Costs__c = 618, CIF_Adjustment_Factor__c = 0.035, CIF_Absolute_value_adjustment__c = 200, Clearance_Costs__c = 488, 
                                                Country__c = 'Brazil', Name = 'Brazil', Handling_Costs__c = 1180, License_Permit_Costs__c = 0);
            insert country;
            
            Tax_Structure_Per_Country__c tax = new Tax_Structure_Per_Country__c(Applied_to_Value__c = 'CIF', Country__c = country.Id, Default_Duty_Rate__c = 0.5625 , 
                                                                                Order_Number__c = '1', Part_Specific__c = TRUE, Name = 'II', Tax_Type__c = 'Duties');
            insert tax;
            
            List<CPA_v2_0__c> cpaList = new List<CPA_v2_0__c>();
            CPA_v2_0__c RelatedCPA = new CPA_v2_0__c(Name = 'Brazil', Country_Applied_Costings__c=true );
            CPA_v2_0__c RelatedCPA2 = new CPA_v2_0__c(Name = 'Australia', Country_Applied_Costings__c=true );
            insert RelatedCPA;
            insert RelatedCPA2;
            
            CPA_v2_0__c cpav2 = new CPA_v2_0__c(Name = 'Brazil Testing 2.0', Supplier__c = accs[1].Id, Service_Type__c = 'IOR/EOR', 
                                                Related_Costing_CPA__c = RelatedCPA.Id,Preferred_Supplier__c = TRUE, VAT_Rate__c = 0.1, 
                                                Destination__c = 'Brazil',  Final_Destination__c = 'Brazil',VAT_Reclaim_Destination__c = FALSE, 
                                                Country__c = country.id, Lead_ICE__c = u.Id,CIF_Freight_and_Insurance__c = 'CIF Amount');
            cpaList.add(cpav2) ;
            
            CPA_v2_0__c cpav3 = new CPA_v2_0__c(Name = 'Brazil Testing 2.0', Supplier__c = accs[1].Id, Service_Type__c = 'IOR/EOR', 
                                                Related_Costing_CPA__c = RelatedCPA2.Id,Preferred_Supplier__c = TRUE, VAT_Rate__c = 0.1, 
                                                Destination__c = 'Australia',  Final_Destination__c = 'Australia',VAT_Reclaim_Destination__c = FALSE, 
                                                Country__c = country.id, Lead_ICE__c = u.Id,CIF_Freight_and_Insurance__c = 'Insurance Amount'); 
            cpaList.add(cpav3);
            
            CPA_v2_0__c cpav4 = new CPA_v2_0__c(Name = 'Brazil Testing 2.0', Supplier__c = accs[1].Id, Service_Type__c = 'IOR/EOR', 
                                                Related_Costing_CPA__c = RelatedCPA2.Id,Preferred_Supplier__c = TRUE, VAT_Rate__c = 0.1, 
                                                Destination__c = 'Australia',  Final_Destination__c = 'Australia',VAT_Reclaim_Destination__c = FALSE, 
                                                Country__c = country.id, Lead_ICE__c = u.Id); 
            cpaList.add(cpav4);
            
            CPA_v2_0__c cpav5 = new CPA_v2_0__c(Name = 'Brazil Testing 2.0', Supplier__c = accs[1].Id, Service_Type__c = 'IOR/EOR', 
                                                Related_Costing_CPA__c = RelatedCPA2.Id,Preferred_Supplier__c = TRUE, VAT_Rate__c = 0.1, 
                                                Destination__c = 'Australia',  Final_Destination__c = 'Australia',VAT_Reclaim_Destination__c = FALSE, 
                                                Country__c = country.id, Lead_ICE__c = u.Id, CIF_Freight_and_Insurance__c = 'Freight Amount'); 
            cpaList.add(cpav5);
            
            insert cpaList;
            
            List<CPARules__c> CPARules = new List<CPARules__c>();
            CPARules.add(new CPARules__c(Country__c = 'Brazil',Criteria__c ='Default', Chargable_weight__c = 'NONE', 
                                         Courier_Responsibility__c = 'NONE',   ECCN_No__c = 'NONE', NL_Product_User__c = 'NONE', 
                                         Number_of_final_deliveries__c = 'NONE', ShipmentValue__c ='NONE', 
                                         Sum_of_quantity_of_matched_products__c = 'NONE', Manufacturer__c = 'NONE', 
                                         DefaultCPA2__c = cpaList[0].Id, CPA2__c =  cpaList[0].Id));
            insert CPARules;
            
            List<CPA_Costing__c> costs = new List<CPA_Costing__c>();
            costs.add(new CPA_Costing__c(Cost_Category__c = 'Insurance',CPA_v2_0__c = cpaList[0].Id, Applied_to__c = 'Shipment value', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'IOR', Min__c = 375, Max__c = 14000, Conditional_value__c = 'Shipment value', Condition__c = 'Between', Floor__c = 0, Ceiling__c = 10000, Currency__c = 'US Dollar (USD)'));
            costs.add(new CPA_Costing__c(Cost_Category__c = 'Insurance',CPA_v2_0__c = cpaList[0].Id, Applied_to__c = 'Anil', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'IOR', Currency__c = 'US Dollar (USD)',Variable_threshold__c = null));
            insert costs;
            
            List<Product2> products = new List<Product2>();
            Product2 product = new Product2(Name = 'CAB-ETH-S-RJ45', description='Anil', ProductCode = 'CAB-ETH-S-RJ45',US_HTS_Code__c = '85478123', Manufacturer__c = accs[2].Id);
            Product2 product2 = new Product2(Name = 'CAB-ETH-S-RJ46', ProductCode = 'CAB-ETH-S-RJ46',US_HTS_Code__c = '85478123', Manufacturer__c = accs[2].Id);
            Product2 product3 = new Product2(Name = 'CAB-ETH-S', ProductCode = 'CAB-ETH-S', US_HTS_Code__c = '85478123',Manufacturer__c = accs[2].Id);
            products.add(product);
            products.add(product2);
            products.add(product3);
            insert products;
            
            List<Product_matching_Rule__c> PMR = new  List<Product_matching_Rule__c>();
            PMR.add(new Product_matching_Rule__c(Contains__c='Anil',Does_Not_Contains__c='Test',Product_to_be_match__c=products[0].id));
            PMR.add(new Product_matching_Rule__c(Contains__c='Anil',Does_Not_Contains__c='Test',Product_to_be_match__c=products[0].id));
            Insert PMR;
            
            List<HS_Codes_and_Associated_Details__c> HSCodeList = new  List<HS_Codes_and_Associated_Details__c>();
            HS_Codes_and_Associated_Details__c hs = new HS_Codes_and_Associated_Details__c(Name = '85444210', Destination_HS_Code__c = '85444210', Description__c = 'Something that does something',
                                                                                           Rate__c = 0.2, Country__c = country.id, US_HTS_Code__c = '');
            
            HS_Codes_and_Associated_Details__c hs2 = new HS_Codes_and_Associated_Details__c(Name = '10562231145', Destination_HS_Code__c = '10562231145', Description__c = 'Something that does something',
                                                                                            Rate__c = 0.2, Country__c = country.id, US_HTS_Code__c = '85478123');
            
            HS_Codes_and_Associated_Details__c hs3 = new HS_Codes_and_Associated_Details__c(Name = '8345124587', Destination_HS_Code__c = '8345124587', Description__c = 'Something that does something',
                                                                                            Rate__c = 0.2, Country__c = country.id, US_HTS_Code__c = '');
            
            HS_Codes_and_Associated_Details__c hs4 = new HS_Codes_and_Associated_Details__c(Name = '8471512459', Destination_HS_Code__c = '8471512459', Description__c = 'Something that does something',
                                                                                            Rate__c = 0.2, Country__c = country.id, US_HTS_Code__c = '');
            
            HS_Codes_and_Associated_Details__c hs5 = new HS_Codes_and_Associated_Details__c(Name = '10562231156', Destination_HS_Code__c = '10562231156', Description__c = 'Something that does something',
                                                                                            Rate__c = 0.5, Country__c = country.id, US_HTS_Code__c = '85478123');
            
            HSCodeList.add(hs);
            HSCodeList.add(hs2);
            HSCodeList.add(hs3);
            HSCodeList.add(hs4);
            HSCodeList.add(hs5);
            insert HSCodeList;
           Roll_Out__c new_roll_out = new Roll_Out__c( Client_Name__c=accs[0].id,
                                                          Contact_For_The_Quote__c=cons[0].id,
                                                          Shipment_Value_in_USD__c=200,
                                                          Destinations__c='Brazil',
                                                          Number_of_Cost_Estimates__c =0
                                                         );
     
      	Insert new_roll_out;            
        }
        
    }
    
    public static testMethod void TestSchedular(){
        Product2 product = [SELECT Id,Manufacturer__c FROM Product2 LIMIT 1];
        Account clientAcc = [SELECT Id FROM Account where Type ='Client' LIMIT 1];
        Account supplAcc = [SELECT Id FROM Account where Type ='Supplier' LIMIT 1];
        Account manufAcc = [SELECT Id FROM Account where Type ='Manufacturer' LIMIT 1];
        Contact clientcon = [SELECT Id FROM Contact where AccountId=: clientAcc.Id];
        Contact supplcon = [SELECT Id FROM Contact where AccountId=: supplAcc.Id];
        User usr = [SELECT Id FROM User where Email ='puser000@amamama.com' LIMIT 1];
        Country_price_approval__c cpa = [SELECT Id FROM Country_price_approval__c];
        CPA_v2_0__c cpav2 = [SELECT Id,Lead_ICE__c FROM CPA_v2_0__c where Destination__c='Brazil' LIMIT 1];
        CPA_v2_0__c cpav3 = [SELECT Id,Lead_ICE__c FROM CPA_v2_0__c where Destination__c='Australia' LIMIT 1];
        
        GL_Account__c bankAccount = [SELECT Id,Name FROM GL_Account__c limit 1];
        
        Shipment_Order__c shipmentOrder = new Shipment_Order__c(Account__c = clientAcc.Id, Shipment_Value_USD__c = 2000, CPA_v2_0__c = cpav2.Id,Total_Taxes__c=20,of_Line_Items__c = 3,
                                                                Client_Contact_for_this_Shipment__c = clientcon.Id, Who_arranges_International_courier__c ='TecEx', Tax_Treatment__c ='DAP/CIF - IOR pays', 
                                                                Ship_to_Country__c = cpa.Id,Internal_Shipment_Type_Categorisation__c = 'Other',Pressure_Point_Comment__c = 'abc',VAT_Claiming_Country__c=false, Destination__c = 'Brazil', Service_Type__c = 'IOR', Chargeable_Weight__c = 200,Ship_From_Country__c = 'Australia',  
                                                                Shipping_Status__c = 'Cost Estimate Abandoned',Roll_Up_Costings_A__c = false,Populate_Invoice__c = false,Li_ion_Batteries__c = 'No',
                                                                Loss_Making_Shipments__c = false,Pressure_Point__c = false,Insurance_Fee__c = 0,IOR_CSE__c = usr.id, ICE__c = usr.Id );
         
        Shipment_Order__c shipmentOrder2 = new Shipment_Order__c(Account__c = supplAcc.Id, Shipment_Value_USD__c = 2000, CPA_v2_0__c = cpav2.Id,Total_Taxes__c=20,of_Line_Items__c = 3,
                                                                 Client_Contact_for_this_Shipment__c = supplcon.Id, Who_arranges_International_courier__c ='Client', Tax_Treatment__c ='DAP/CIF - IOR pays', 
                                                                 Ship_to_Country__c = cpa.Id,Internal_Shipment_Type_Categorisation__c = 'Other',Pressure_Point_Comment__c = 'abc',Destination__c = 'Belgium', Service_Type__c = 'IOR', Chargeable_Weight__c = 200,  
                                                                 Shipping_Status__c = 'Shipment Pending',Roll_Up_Costings_A__c = false,Populate_Invoice__c = false,IOR_CSE__c = usr.id, ICE__c = usr.Id );
        
        List<Shipment_Order__c>shipments = new List<Shipment_Order__c>{shipmentOrder, shipmentOrder2};
            
            test.startTest();
        insert shipments;
        	List<Part__c> partList = new List<Part__c>();

        partList.add(new Part__c(Name ='CAB-ETH-S-RJ45',  Quantity__c= 1, Product__c=product.Id, Commercial_Value__c = 15000, US_HTS_Code__c = '85444210',
                                 Country_of_Origin2__c = 'China', Shipment_Order__c = shipmentOrder.Id, Description_and_Functionality__c ='Anil'));

        partList.add(new Part__c(Name ='AsRdy',  Quantity__c= 1,  Product__c=product.Id, Commercial_Value__c = 15000, US_HTS_Code__c = '85444210',
                                 Country_of_Origin2__c = 'China', Shipment_Order__c = shipmentOrder2.Id, Description_and_Functionality__c ='Anil AsRdy'));

        partList.add(new Part__c(Name ='CAB-ETH-S-RJ46',  Quantity__c= 1, Commercial_Value__c = 15000, US_HTS_Code__c = '85478123',
                                 Country_of_Origin2__c = 'China', Shipment_Order__c = shipmentOrder.Id, Description_and_Functionality__c ='AB-ETH-S-RJ46'));

        partList.add(new Part__c(Name ='CAB-ETH-S-RJ46',  Quantity__c= 1, Commercial_Value__c = 15000, US_HTS_Code__c = '834512879',
                                 Country_of_Origin2__c = 'China', Shipment_Order__c = shipmentOrder2.Id, Description_and_Functionality__c ='AB-ETH-S-RJ46'));

        partList.add(new Part__c(Name ='Testing',  Quantity__c= 1, Commercial_Value__c = 15000, US_HTS_Code__c = '834512879',
                                 Country_of_Origin2__c = 'China', Shipment_Order__c = shipmentOrder.Id, Description_and_Functionality__c ='Testing'));


        partList.add(new Part__c(Name ='Who CAB-ETH-S',  Quantity__c= 1, Commercial_Value__c = 15000, US_HTS_Code__c = '8471254784',
                                 Country_of_Origin2__c = 'China', Shipment_Order__c = shipmentOrder2.Id, Description_and_Functionality__c ='Testing'));

        insert partList;
        
        Map<String,Object> getRecordTypesForBankTransaction = BankTransactionProcessController.getRecordTypes('Bank_transactions__c');
        Map<Id,RecordType> recordTypeList = (Map<Id,RecordType>)getRecordTypesForBankTransaction.get('recordTypeList');
        List<RecordType> recordTypes = recordTypeList.values();
        Bank_transactions__c bankTransaction = new Bank_transactions__c(
            Bank_Account_New__c = bankAccount.Id,
            Date__c = date.today(),
            Supplier__c = clientAcc.Id,
            Amount__c = 500,
            Bank_charges__c = 50,
            RecordTypeId = recordTypes[0].Id,
            Conversion_Rate__c = 1
        );
        
        insert bankTransaction;
        
        
        String clientRTypeId = Schema.Sobjecttype.Invoice_New__c.getRecordTypeInfosByDeveloperName().get('Client').getRecordTypeId();
        Invoice_New__c inv = new Invoice_New__c(
            IOR_Fees__c = 400,
            EOR_Fees__c = 400,
            Bank_Fee__c = 400,
            Admin_Fees__c = 400,
            Customs_Handling_Fees__c = 400,
            Customs_Brokerage_Fees__c = 400,
            Customs_Clearance_Fees__c = 400,
            Customs_License_In__c = 400,
            International_Freight_Fee__c = 400,
            Cash_Outlay_Fee__c = 400,
            Liability_Cover_Fee__c = 400,
            Taxes_and_Duties__c = 400,
            Recharge_Tax_and_Duty_Other__c = 400,
            Miscellaneous_Fee__c = 400,
            Miscellaneous_Fee_Name__c = 'Miscellaneous',
            RecordTypeId = clientRTypeId,
            Account__c = clientAcc.Id,
            Conversion_Rate__c = 1,
            Shipment_Order__c = shipments[0].Id,
            Invoice_Currency__c = 'US Dollar (USD)',
            Invoice_amount_USD__c = 10,
            Invoice_Type__c = 'Cash Outlay Invoice',
            Send_Reminder_Mail__c = true,
            Send_Reminder_Mail_Override__c = true,
            Due_Date__c = System.now().date().addDays(3)
        );
       
        insert inv;
        
        
        String CRON_EXP = '0 0 14 ? * WED 2021';
        String jobId = System.schedule('SendReminderEmail',CRON_EXP,new SendReminderEmail());
        Test.stopTest();
    }
}