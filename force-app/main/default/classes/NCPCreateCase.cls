@RestResource(urlMapping='/NCPCreateCase/*')
Global class NCPCreateCase {

      @Httppost
      global static void NCPCreateCase(){
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        Blob body = req.requestBody;
    	res.addHeader('Content-Type', 'application/json');
        String requestString = body.toString();
        NCPCreateCaseWra rw  = (NCPCreateCaseWra)JSON.deserialize(requestString,NCPCreateCaseWra.class);
          Try{
              
              Access_token__c At =[select status__c,Access_Token__c from Access_token__c where Access_Token__c=:rw.Accesstoken ];
            	if( at.status__C =='Active' ){
                     Id CPrecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('ClientPlatform').getRecordTypeId();
                 //   if(rw.SOID!=null){ SO = [Select Id,CSE_IOR__c from shipment_Order__c where id =:rw.SOID limit 1];} else{Ac = [Select Id,CSE_IOR__c from Account where id =:rw.AccountID limit 1]; }   
                // Shipment_Order__C  SO = [Select Id,CSE_IOR__c from shipment_Order__c where id =:rw.SOID limit 1];
                 Account   Ac = [Select Id,Service_Manager__c,Compliance__c from Account where id =:rw.AccountID limit 1];
               Case cs = new case();
                    CS.RecordTypeId=CPrecordTypeId;
                    cs.Subject = rw.Subject;
                    cs.Reason = 'Client Portal';
                    cs.Description = rw.Description;
                    cs.Status = 'new';
                    cs.AccountId=rw.AccountID;
                    cs.Contactid=rw.ContactId;
                    cs.CreatedById=rw.ClientuserID;
                    cs.OwnerId=rw.ClientuserID;
                    //Linked record
                    if((rw.PageOrigin =='TecEx - Message sent from Dashboard' || rw.PageOrigin=='TecEx - Message sent from My Team page'|| rw.PageOrigin=='TecEx - Message sent from TecEx - Message sent from Product Catalog page') && rw.AccountID !=null){
                      //  cs.Linked_Record_Id__c=rw.AccountID;
                          cs.Link_to_Record__c = 'https://tecex.lightning.force.com/'+rw.AccountID;
                    		}
                    		else{
                        		//cs.Linked_Record_Id__c=rw.recordID;
                        		cs.Link_to_Record__c = 'https://tecex.lightning.force.com/'+rw.recordID;
                    			}
                    //Assigned
                    if((rw.PageOrigin =='TecEx - Message sent from Dashboard') ){
                        	cs.Assigned_to__c=Ac.Service_Manager__c;
                    		}
                    		else if(rw.PageOrigin =='TecEx - Message sent from TecEx - Message sent from Product Catalog page'){
                        		cs.Assigned_to__c=Ac.Compliance__c;
                    		}
                    else if(rw.PageOrigin=='TecEx - Message sent from My Team page'){
                        
                        cs.Assigned_to__c=ac.Service_Manager__c;
                    }
                    		
                    //recordtype 
                /*    if( rw.PageOrigin != 'Requesting new team member' ){
                        	CS.RecordTypeId=CPrecordTypeId;	
                        }
                    
                  */  
                    if(rw.SOID!=null){cs.Shipment_Order__c=rw.SOID;}  
                   
                    if(rw.Source!=null){ cs.origin=rw.Source;}
                insert cs;
                    
                    system.debug('rw.Attachment--->'+rw.Attachment);
                                        	if(rw.Attachment !=null){
                                        list<Attachment> atts=new List<Attachment>();
                                        For(Integer i=0;rw.Attachment.size()>i;i++){
                                            Attachment a = new Attachment (ParentId = cs.id, Body =rw.Attachment[i].FileBody,                                                     
                                            Name =rw.Attachment[i].FileName); //,contenttype=rw.Attachment[i].FileType
                                            atts.add(a);
                                        }
                                        insert atts; 
                                   } 
                
                 	case cs1 =[Select Id,CaseNumber from case where id=:cs.Id];
                  	JSONGenerator gen = JSON.createGenerator(true);
            		gen.writeStartObject();
            
             		gen.writeFieldName('Success');
             		gen.writeStartObject();
            
             			if(cs.id == null) {gen.writeNullField('Response');} else{gen.writeStringField('Response','Case created successfully. Casse Reference number is '+cs1.CaseNumber);}
           				 if(cs.id == null) {gen.writeNullField('CaseID');} else{gen.writeStringField('CaseID',+cs1.ID);}
            
            		gen.writeEndObject();
            		gen.writeEndObject();
            		String jsonData = gen.getAsString();
            		res.responseBody = Blob.valueOf(jsonData);
            		res.addHeader('Content-Type', 'application/json');
            		res.statusCode = 200;    
                API_Log__c Al = New API_Log__c();
                Al.EndpointURLName__c='NCPCreateCase';
                Al.StatusCode__c=string.valueof(res.statusCode);
                Al.Response__c = 'Created case' +cs.id;
                Insert Al;
                
                  
                  
                  
              }        
          }
          Catch(Exception e){
              
              String ErrorString ='Something went wrong, please contact support_SF@tecex.com';
               		res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
            		res.addHeader('Content-Type', 'application/json');
        			res.statusCode = 400;
                
                API_Log__c Al = New API_Log__c();
                Al.Account__c=rw.AccountID;
                Al.Login_Contact__c=rw.contactId;
                Al.EndpointURLName__c='NCPCreateCase';
                Al.Response__c=e.getMessage()+' at line number: '+e.getLineNumber();
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al;
                
              
              
          }



      }


}