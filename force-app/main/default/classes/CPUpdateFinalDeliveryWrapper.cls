public class CPUpdateFinalDeliveryWrapper {
    public String ID;
	public String Name;
	public String ContactName;
	public String ContactEmail;
	public String ContactNumber;
	public String ContactAddreaaLine1;
	public String ContactAddreaaLine2;
	public String City;
	public String ZIP;
	public String Country;

	
	public static CPUpdateFinalDeliveryWrapper parse(String json) {
		return (CPUpdateFinalDeliveryWrapper) System.JSON.deserialize(json, CPUpdateFinalDeliveryWrapper.class);
	}

}