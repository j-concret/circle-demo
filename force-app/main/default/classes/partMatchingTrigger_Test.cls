@isTest
public class partMatchingTrigger_Test {
    
    private static testMethod void  setUpData(){
        Account account = new Account (name='Acme1',  Type ='Client',  CSE_IOR__c = '0050Y000001LTZO',  Service_Manager__c = '0050Y000001LTZO',  Tax_recovery_client__c  = FALSE, Ship_From_Line_1__c = 'Ship_From_Line_1',
                                       Ship_From_Line_2__c = 'Ship_From_Line_2', Ship_From_Line_3__c = 'Ship_From_Line_3', Ship_From_City__c = 'Ship_From_City',  Ship_From_Zip__c = 'Ship_From_Zip',
                                       Ship_From_Country__c = 'Ship_From_Country' ,  Ship_From_Other_notes__c = 'Ship_From_Other', Ship_From_Contact_Name__c = 'Ship_From_Contact', Ship_From_Contact_Email__c = 'Ship_From_Email',
                                       Ship_From_Contact_Tel__c = 'Ship_From_Tel', Client_Address_Line_1__c = 'Client_Address_Line_1', Client_Address_Line_2__c = 'Client_Address_Line_2', Client_Address_Line_3__c = 'Client_Address_Line_3',
                                       Client_City__c = 'Client_City', Client_Zip__c = 'Client_Zip', Client_Country__c = 'Client_Country',  Other_notes__c = 'Other_notes',  Tax_Name__c = 'Tax_Name', Tax_ID__c = 'Tax_ID',
                                       Contact_Name__c = 'Contact_Name', Contact_Email__c = 'Contact_Email', Contact_Tel__c = 'Contact_Tel' ); insert account;  
		
		
        Account account2 = new Account (name='Test Supplier', Type ='Supplier', ICE__c = '0050Y000001km5c', RecordTypeId = '0120Y0000006KjQQAU' ); insert account2;
        
        Account account3 = new Account (name='Test Manufacturer', Type ='Manufacturer', RecordTypeId = '0120Y0000002wa0QAA', Manufacturer_Alias__c = 'Tester,Who' ); insert account3;
        
        Account account4 = new Account (name='ACME', Type ='Manufacturer', RecordTypeId = '0120Y0000002wa0QAA', Manufacturer_Alias__c = 'Buggs,Bunny' ); insert account4;

        Contact contact = new Contact ( lastname ='Testing Individual',  AccountId = account.Id); insert contact;  
        
        IOR_Price_List__c iorpl = new IOR_Price_List__c ( Client_Name__c = account.Id, Name = 'Brazil', IOR_Fee__c = 0.15, On_Charge_Mark_up__c = 0.15,
                                                         TecEx_Shipping_Fee_Markup__c =0.40, IOR_Min_Fee__c = 100,  Admin_Fee__c =100, Set_up_fee__c =100, Bank_Fees__c =200, Tax_Rate__c = 0.20,
                                                         Estimated_Customs_Brokerage__c =250, Estimated_Customs_Clearance__c =250, Estimated_Customs_Handling__c =250, Estimated_Import_License__c =250, 
                                                         Destination__c = 'Brazil' ); insert iorpl; 
        
        country_price_approval__c cpa = new country_price_approval__c( name ='Brazil', Billing_term__c = 'DAP/CIF - IOR pays',  Airwaybill_Instructions__c = 'User IOR Address', Supplier__c = account2.Id, 
                                                                      In_Country_Specialist__c = '0050Y000001km5c', Clearance_Destination__c = 'Brazil', Preferred_Supplier__c = TRUE, 
                                                                      Destination__c = 'Brazil' ); insert cpa;
        
        Country__c country = new Country__c(Brokerage_Costs__c = 618, CIF_Adjustment_Factor__c = 0.035, CIF_Absolute_value_adjustment__c = 200, Clearance_Costs__c = 488, 
                                            Country__c = 'Brazil', Name = 'Brazil', Handling_Costs__c = 1180, License_Permit_Costs__c = 0); insert country;
	   
        CPA_v2_0__c aCpav = new CPA_v2_0__c(Name = 'Brazil', Country_Applied_Costings__c = TRUE); insert aCpav;
        
        CPA_v2_0__c cpav2 = new CPA_v2_0__c(Name = 'Brazil Testing 2.0', Supplier__c = account2.Id, Service_Type__c = 'IOR/EOR', Related_Costing_CPA__c = aCpav.Id, 
                                            Preferred_Supplier__c = TRUE, VAT_Rate__c = 0.1, Destination__c = 'Brazil',  Final_Destination__c = 'Brazil', 
                                            VAT_Reclaim_Destination__c = FALSE, Country__c = country.id, Lead_ICE__c = '0050Y000001km5c'); insert cpav2;
        
        
        List<CPARules__c> CPARules = new List<CPARules__c>();
        CPARules.add(new CPARules__c(Country__c = 'Brazil',Criteria__c ='Default', Chargable_weight__c = 'NONE', Courier_Responsibility__c = 'NONE',   ECCN_No__c = 'NONE', NL_Product_User__c = 'NONE', Number_of_final_deliveries__c = 'NONE', ShipmentValue__c ='NONE', Sum_of_quantity_of_matched_products__c = 'NONE', Manufacturer__c = 'NONE', DefaultCPA2__c = cpav2.Id, CPA2__c =  cpav2.Id));
        insert CPARules;
        
        Product2 product = new Product2(Name = 'CAB-ETH-S-RJ45', ProductCode = 'CAB-ETH-S-RJ45', Manufacturer__c = account3.Id);insert product;
        Product2 product2 = new Product2(Name = 'CAB-ETH-S-RJ46', ProductCode = 'CAB-ETH-S-RJ46', Manufacturer__c = account3.Id);insert product2;
        Product2 product3 = new Product2(Name = 'CAB-ETH-S', ProductCode = 'CAB-ETH-S', Manufacturer__c = account3.Id);insert product3;
        
        HS_Codes_and_Associated_Details__c hs = new HS_Codes_and_Associated_Details__c(Name = '85444210', Destination_HS_Code__c = '85444210', Description__c = 'Something that does something',
                                                                                     Rate__c = 0.2, Country__c = country.id, US_HTS_Code__c = '');insert hs;
        
        HS_Codes_and_Associated_Details__c hs2 = new HS_Codes_and_Associated_Details__c(Name = '10562231145', Destination_HS_Code__c = '10562231145', Description__c = 'Something that does something',
                                                                                     Rate__c = 0.2, Country__c = country.id, US_HTS_Code__c = '85478123');insert hs2;
        
        HS_Codes_and_Associated_Details__c hs3 = new HS_Codes_and_Associated_Details__c(Name = '8345124587', Destination_HS_Code__c = '8345124587', Description__c = 'Something that does something',
                                                                                     Rate__c = 0.2, Country__c = country.id, US_HTS_Code__c = '');insert hs3;
        
        HS_Codes_and_Associated_Details__c hs4 = new HS_Codes_and_Associated_Details__c(Name = '8471512459', Destination_HS_Code__c = '8471512459', Description__c = 'Something that does something',
                                                                                     Rate__c = 0.2, Country__c = country.id, US_HTS_Code__c = '');insert hs4;
        
        HS_Codes_and_Associated_Details__c hs5 = new HS_Codes_and_Associated_Details__c(Name = '10562231156', Destination_HS_Code__c = '10562231156', Description__c = 'Something that does something',
                                                                                     Rate__c = 0.5, Country__c = country.id, US_HTS_Code__c = '85478123');insert hs5;
        
        List<Tax_Structure_Per_Country__c> taxStructure = new List<Tax_Structure_Per_Country__c>();
		taxStructure.add(new Tax_Structure_Per_Country__c(Country__c = country.Id, Applied_to_Value__c = 'CIF', Name = 'I', Order_Number__c = '1', Part_Specific__c = TRUE, Tax_Type__c = 'Duties', Default_Duty_Rate__c = 0.5625)); 
        taxStructure.add(new Tax_Structure_Per_Country__c(Country__c = country.Id, Applied_to_Value__c = 'CIF', Name = 'II', Order_Number__c = '2', Part_Specific__c = FALSE, Tax_Type__c = 'Other', Rate__c = 0.40, Additional_Percent__c = 0.01 ));
        taxStructure.add(new Tax_Structure_Per_Country__c(Country__c = country.Id, Applied_to_Value__c = 'CIF', Name = 'III', Order_Number__c = '3', Part_Specific__c = FALSE, Tax_Type__c = 'VAT', Rate__c = 0.40, Applies_to_Order__c = '1'));
        taxStructure.add(new Tax_Structure_Per_Country__c(Country__c = country.Id, Applied_to_Value__c = 'CIF', Name = 'IV', Order_Number__c = '4', Part_Specific__c = FALSE, Tax_Type__c = 'Other', Amount__c = 400));     
        taxStructure.add(new Tax_Structure_Per_Country__c(Country__c = country.Id, Applied_to_Value__c = 'CIF', Name = 'V', Order_Number__c = '5', Part_Specific__c = FALSE, Tax_Type__c = 'Other', Rate__c = 0.40, Applies_to_Order__c = '1,2,3'));
        taxStructure.add(new Tax_Structure_Per_Country__c(Country__c = country.Id, Applied_to_Value__c = 'CIF', Name = 'VI', Order_Number__c = '6', Part_Specific__c = FALSE, Tax_Type__c = 'Other', Rate__c = 0.40, Applies_to_Order__c = '1,2,3,4', Additional_Percent__c = 0.01));
        taxStructure.add(new Tax_Structure_Per_Country__c(Country__c = country.Id, Applied_to_Value__c = 'CIF', Name = 'VII', Order_Number__c = '7', Part_Specific__c = FALSE, Tax_Type__c = 'Other', Rate__c = 0.40, Max__c = 1000, Min__c = 50));       
        taxStructure.add(new Tax_Structure_Per_Country__c(Country__c = country.Id, Applied_to_Value__c = 'CIF', Name = 'VIII', Order_Number__c = '8', Part_Specific__c = FALSE, Tax_Type__c = 'Other', Rate__c = 0.40, Applies_to_Order__c = '1,2,3,4,5,7'));
        taxStructure.add(new Tax_Structure_Per_Country__c(Country__c = country.Id, Applied_to_Value__c = 'CIF', Name = 'IX', Order_Number__c = '9', Part_Specific__c = FALSE, Tax_Type__c = 'Other', Rate__c = 0.40, Applies_to_Order__c = '1,2,3,4,6'));
        taxStructure.add(new Tax_Structure_Per_Country__c(Country__c = country.Id, Applied_to_Value__c = 'CIF', Name = 'X', Order_Number__c = '10', Part_Specific__c = FALSE, Tax_Type__c = 'Other', Rate__c = 0.40)); 
        insert taxStructure;
        
        
        Shipment_Order__c shipmentOrder = new Shipment_Order__c(Account__c = account.id, Shipment_Value_USD__c = 2000, CPA_v2_0__c = cpav2.Id,
                                                                Client_Contact_for_this_Shipment__c = contact.Id, Who_arranges_International_courier__c ='Client', Tax_Treatment__c ='DAP/CIF - IOR pays', 
                                                                Ship_to_Country__c = cpa.Id, Destination__c = 'Brazil', Service_Type__c = 'IOR', IOR_Price_List__c = iorpl.Id, Chargeable_Weight__c = 200,  
                                                                Shipping_Status__c = 'Cost Estimate Abandoned' );  insert shipmentOrder; 
       

        List<Part__c> partList = new List<Part__c>();
        
        partList.add(new Part__c(Name ='CAB-ETH-S-RJ45',  Quantity__c= 1, Commercial_Value__c = 15000, US_HTS_Code__c = '85444210', 
        Country_of_Origin2__c = 'China', Shipment_Order__c = shipmentOrder.Id, Description_and_Functionality__c ='AB-ETH-S-RJ45'));
        
        partList.add(new Part__c(Name ='CAB-ETH-S-RJ46',  Quantity__c= 1, Commercial_Value__c = 15000, US_HTS_Code__c = '85478123', 
        Country_of_Origin2__c = 'China', Shipment_Order__c = shipmentOrder.Id, Description_and_Functionality__c ='AB-ETH-S-RJ46'));
        
        partList.add(new Part__c(Name ='CAB-ETH-S-RJ46',  Quantity__c= 1, Commercial_Value__c = 15000, US_HTS_Code__c = '834512879', 
        Country_of_Origin2__c = 'China', Shipment_Order__c = shipmentOrder.Id, Description_and_Functionality__c ='AB-ETH-S-RJ46'));
        
        partList.add(new Part__c(Name ='Testing',  Quantity__c= 1, Commercial_Value__c = 15000, US_HTS_Code__c = '834512879', 
        Country_of_Origin2__c = 'China', Shipment_Order__c = shipmentOrder.Id, Description_and_Functionality__c ='Testing'));
        
        
        partList.add(new Part__c(Name ='Who CAB-ETH-S',  Quantity__c= 1, Commercial_Value__c = 15000, US_HTS_Code__c = '8471254784', 
        Country_of_Origin2__c = 'China', Shipment_Order__c = shipmentOrder.Id, Description_and_Functionality__c ='Testing'));
        
        insert partList;
        
        
        
     
        test.startTest();
              
       product.Manufacturer__c = account4.Id;
       update product;        
        
        
        shipmentOrder.Shipping_Status__c = 'Cost Estimate Abandoned';
        update shipmentOrder;
	
       List<Part__c> partToUpdate = [Select Name, Id from Part__c where Shipment_Order__c =: shipmentOrder.Id]; 
        
         for (Integer i = 0; i < partToUpdate.size(); i++ ){     
        partToUpdate[i].Name = 'CAB-ETH-S'; 
        partToUpdate[i].US_HTS_Code__c = '85440321546'; 
         }
        
        update partToUpdate;
        delete partToUpdate;

        test.stopTest();
 
         
        
    } 

}