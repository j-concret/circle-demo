public with sharing class CTRL_EmailAndAttachStatement {
    
    @AuraEnabled
    public static List<Contact> getRelatedContacts(String clientId){
        return [SELECT Id, Name, Email, Title 
                FROM Contact 
                WHERE AccountId = :clientId AND Email!=null AND IsEmailBounced = false];
    }
    
    @AuraEnabled
    public static String getUserEmail() {
        return UserInfo.getUserEmail();
    }
    
    @AuraEnabled
    public static boolean updateInvoices(Id cliendId){
        
        RecusrsionHandler.runTaskInQueueable = true;
        boolean isUpdated =  false;
        
        List<Invoice_New__c> customerInvoiceList2 = new List<Invoice_New__c> ([SELECT Id, Name, Account__c, Shipment_Order__c, Invoice_Age_Text__c, Invoice_Age__c , Today__c, Shipment_Order__r.Name, Freight_Request__r.Name, Conversion_Rate__c, PO_Number_Override__c, PO_Number__c, Invoice_Sent_Date__c, Amount_Outstanding_Local_Currency__c, Invoice_Name__c, Prepayment_date__c, Due_Date__c, IOR_Fees__c, EOR_Fees__c, Admin_Fees__c,   Actual_IOREOR_Costs__c, International_Freight_Fee__c, Liability_Cover_Fee__c, Total_Amount_Incl_Potential_Cash_Outlay__c, Cost_of_Sale_Shipping_Insurance__c, Taxes_and_Duties__c, Recharge_Tax_and_Duty_Other__c, Customs_Brokerage_Fees__c, Customs_Clearance_Fees__c, Customs_License_In__c, Customs_Handling_Fees__c, Bank_Fee__c, Miscellaneous_Fee__c, Miscellaneous_Fee_Name__c, Invoice_amount_USD__c, Collection_Administration_Fee__c, Amount_Outstanding__c, Invoice_Type__c, Invoice_Currency__c, Possible_Cash_Outlay__c, Cash_Outlay_Fee__c, Invoice_Amount_Local_Currency__c, IOR_EOR_Compliance__c, Government_and_In_country_Summary__c, Freight_and_Related_Costs__c, Billing_City__c, Billing_Country__c, Billing_Postal_Code__c, Billing_Street__c, Billing_State__c, Invoice_Status__c, Ship_To_Country__c, Penalty_Outstanding_Amount__c
                                                                               FROM Invoice_New__c
                                                                               WHERE Account__c = :cliendId
                                                                              ]);
        
        List<Invoice_New__c> InvoicesToUpade = new List<Invoice_New__c>();
        for(Invoice_New__c invRec: customerInvoiceList2){
            
            System.debug(' Invoice Name ### ' + invRec.Name);
            System.debug(' Date Field Value Before ### ' + invRec.Today__c);
            System.debug(' Age Field Value Before ### ' + invRec.Invoice_Age_Text__c);
            
            invRec.Today__c = System.today();
            invRec.Invoice_Age_Text__c = invRec.Invoice_Age__c ;
            System.debug(' Date Field Value ###  After' + invRec.Today__c);
            System.debug(' Age Field Value After ### ' + invRec.Invoice_Age_Text__c);
            
            InvoicesToUpade.add(invRec);
        }
        
        System.debug('List Size ########### ' + InvoicesToUpade.size());
        if(!InvoicesToUpade.isEmpty()){
            try{
                update InvoicesToUpade;
                isUpdated = true;
                
            }catch(Exception e) {
                System.debug('The following exception has occurred while updating invoices Today and Age fields: ' + e.getMessage() + 'dd ' + e.getLineNumber() + '' + e.getStackTraceString());
            }
        }
        
        return isUpdated;
    }
    
    @AuraEnabled
    public static Boolean isPenaltyApproved(id accId){
        Boolean isApproved;
        /*List<Invoice_New__c> rec = [SELECT Id, Account__c, Name, Freight_Request__r.Name, Shipment_Order__r.Name, Penalty_Status__c, Penalty_Outstanding_Amount__c 
FROM Invoice_New__c 
WHERE Account__c = :accId AND Penalty_Status__c = 'Approved' AND Penalty_Outstanding_Amount__c > 0 ];*/
        
        Account account  = [SELECT Id, Penalty_Status__c, Penalty_Amount_Due__c FROM Account WHERE Id =:accId];
        
        if((account.Penalty_Status__c == 'Approved'|| account.Penalty_Status__c == 'New') && account.Penalty_Amount_Due__c > 0 ){ 
            isApproved = true;
        } else{ isApproved = false;}
        
        return isApproved;
    }
    
    
    @AuraEnabled
    public static Boolean sendStatement(Id customerId, List<String> toAddress, List<String> ccAddress) {
        
        Messaging.SingleEmailMessage email;
        
        Boolean isSent = false;
        
        if(toAddress.size()>0){
            Contact myCon = getRelatedContacts(customerId)[0];
            EmailTemplate emailTemplates = [
                SELECT Subject, Body, HtmlValue
                FROM EmailTemplate
                WHERE DeveloperName = 'Tecex_Statement_Of_Account' Limit 1];
            
            if(emailTemplates != null){
                email = new Messaging.SingleEmailMessage();
                
                //EmailTemplate emailTemplate = emailTemplates[0];
                
                //add template
                email.setTemplateId(emailTemplates.Id);
                
                //set target object for merge fields
                email.setTargetObjectId(myCon.Id);
                
                email.setwhatId(customerId);
                
                Account client = new DAL_Account().selectById(customerId);
                email.setToAddresses(toAddress);
                System.debug('cc Address###  ' + ccAddress);
                System.debug('cc Address###  ' + ccAddress[0]);
                System.debug('cc Address empty?###  ' + ccAddress.isEmpty());
                System.debug('cc Address Size###  ' + ccAddress.size());
                
                if(ccAddress[0] !=''){
                    email.setCcAddresses(ccAddress);
                }
                
                //#####Email attachment
                PageReference statementPdf = new PageReference(Site.getPathPrefix() + '/apex/CustomStatementNew');
                statementPdf.getParameters().put('id', customerId);
                Blob pdfPageBlob = (Test.isRunningTest()) ? Blob.valueOf(String.valueOf('This is a test page')) : statementPdf.getContentAsPDF();
                String customerName = client.Name;   
                Attachment statementAttachment  = new Attachment();
                statementAttachment.Body        = pdfPageBlob;
                statementAttachment.Name        = 'General Statement-' + customerName + '-' + (System.now()).date().format() + '.pdf';
                statementAttachment.ParentId    = customerId;
                statementAttachment.IsPrivate   = false;
                statementAttachment.Description = 'Customer Statement';
                
                List<Messaging.EmailFileAttachment> statementAttachmentsList = new List<Messaging.EmailFileAttachment>();
                Messaging.EmailFileAttachment statementAttachmentFile = new Messaging.EmailFileAttachment();
                
                statementAttachmentFile.setFileName(statementAttachment.Name);
                statementAttachmentFile.setBody(statementAttachment.Body);
                statementAttachmentFile.setContentType(statementAttachment.ContentType);
                
                statementAttachmentsList.add(statementAttachmentFile);
                email.setFileAttachments(statementAttachmentsList);
                
                //Send Email
                List<Messaging.SendEmailResult> r = Messaging.sendEmail(new List<Messaging.SingleEmailMessage> {email});
                
                if (r[0].isSuccess()){
                    isSent = true;
                    
                    insert statementAttachment;
                    
                }
            }
        }
        return isSent;
    }
    
    @AuraEnabled
    public static Boolean sendPenalty(Id customerId, List<String> toAddress, List<String> ccAddress) {
        
        Messaging.SingleEmailMessage email;
        
        Boolean isSent = false;
        
        if(toAddress.size()>0){
            Contact myCon = getRelatedContacts(customerId)[0];
            EmailTemplate emailTemplates = [
                SELECT Subject, Body, HtmlValue
                FROM EmailTemplate
                WHERE DeveloperName = 'Tecex_Penalty_Invoice' LIMIT 1];
            
            if(emailTemplates != null){
                email = new Messaging.SingleEmailMessage();
                
                //EmailTemplate emailTemplate = emailTemplates[0];
                
                //add template
                email.setTemplateId(emailTemplates.Id);
                
                //set target object for merge fields
                email.setTargetObjectId(myCon.Id);
                
                email.setwhatId(customerId);
                
                Account client = new DAL_Account().selectById(customerId);
                
                email.setToAddresses(toAddress);
                
                System.debug('cc Address###  ' + ccAddress);
                System.debug('cc Address###  ' + ccAddress[0]);
                System.debug('cc Address empty?###  ' + ccAddress.isEmpty());
                System.debug('cc Address Size###  ' + ccAddress.size());
                
                if(ccAddress[0] !=''){
                    email.setCcAddresses(ccAddress);
                }
                
                //#####Email attachment
                PageReference penaltyPdf = new PageReference(Site.getPathPrefix() + '/apex/tecexPenaltyInvoice');
                penaltyPdf.getParameters().put('id', customerId);
                Blob pdfPageBlob = (Test.isRunningTest()) ?
                    Blob.valueOf(String.valueOf('This is a test page')) : penaltyPdf.getContentAsPDF();
                String customerName = client.Name;   
                Attachment penaltyAttachment  = new Attachment();
                penaltyAttachment.Body        = pdfPageBlob;
                penaltyAttachment.Name        = 'Penalty Invoice-' + customerName + '-' + (System.now()).date().format() + '.pdf';
                penaltyAttachment.ParentId    = customerId;
                penaltyAttachment.IsPrivate   = false;
                penaltyAttachment.Description = 'Penalty Invoice';
                
                List<Messaging.EmailFileAttachment> penaltyAttachmentsList = new List<Messaging.EmailFileAttachment>();
                Messaging.EmailFileAttachment penaltyAttachmentFile = new Messaging.EmailFileAttachment();
                
                penaltyAttachmentFile.setFileName(penaltyAttachment.Name);
                penaltyAttachmentFile.setBody(penaltyAttachment.Body);
                penaltyAttachmentFile.setContentType(penaltyAttachment.ContentType);
                
                penaltyAttachmentsList.add(penaltyAttachmentFile);
                email.setFileAttachments(penaltyAttachmentsList);
                
                //Send Email
                List<Messaging.SendEmailResult> r = Messaging.sendEmail(new List<Messaging.SingleEmailMessage> {email});
                
                if (r[0].isSuccess()){
                    isSent = true;
                    insert penaltyAttachment;
                }
            }
        }
        return isSent;
    }
    
    @AuraEnabled
    public static Boolean saveStatement(Id accId){
        
        Boolean isSaved = false;   
        Account client = new DAL_Account().selectById(accId);
        
        try{  
            //#####Email attachment
            PageReference statementPdf = new PageReference(Site.getPathPrefix() + '/apex/CustomStatementNew');
            statementPdf.getParameters().put('id', accId);
            Blob pdfPageBlob = (Test.isRunningTest()) ?
                Blob.valueOf(String.valueOf('This is a test page')) : statementPdf.getContentAsPDF();
            String customerName = client.Name;   
            Attachment statementAttachment  = new Attachment();
            statementAttachment.Body        = pdfPageBlob;
            statementAttachment.Name        = 'General Statement-' + customerName + '-' + (System.now()).date().format() + '.pdf';
            statementAttachment.ParentId    = accId;
            statementAttachment.IsPrivate   = false;
            statementAttachment.Description = 'Customer Statement';
            insert statementAttachment;
            isSaved = true;
        }catch (Exception e){
            System.debug(' Error while trying to insert Statement ###: ' + e.getMessage());
        }
        
        return isSaved;
    }
    
    @AuraEnabled
    public static Boolean savePenalty(Id accId){
        
        Boolean isSaved = false;   
        Account client = new DAL_Account().selectById(accId);
        
        try{  
            //#####Email attachment
            PageReference penaltyPdf = new PageReference(Site.getPathPrefix() + '/apex/tecexPenaltyInvoice');
            penaltyPdf.getParameters().put('id', accId);
            Blob pdfPageBlob = (Test.isRunningTest()) ?
                Blob.valueOf(String.valueOf('This is a test page')) : penaltyPdf.getContentAsPDF();
            String customerName = client.Name;   
            Attachment penaltyAttachment  = new Attachment();
            penaltyAttachment.Body        = pdfPageBlob;
            penaltyAttachment.Name        = 'Penalty Invoice-' + customerName + '-' + (System.now()).date().format() + '.pdf';
            penaltyAttachment.ParentId    = accId;
            penaltyAttachment.IsPrivate   = false;
            penaltyAttachment.Description = 'Penalty Invoice';
            insert penaltyAttachment;
            isSaved = true;
        }catch (Exception e){
            System.debug(' Error while trying to insert Penalty Invoice ###: ' + e.getMessage());
        }
        
        return isSaved;
    }
    
    
    @AuraEnabled
    public static Attachment NCPDownloadStatement(Id accId,string Accountname){
        
        Boolean isSaved = false;   
        
        //  list<Account> client = [Select Id,Name from Account where Id =:accId];
        //Account Client=  new DAL_Account().selectById(accId);
        Attachment statementAttachment  = new Attachment();
        Attachment statementAttachment1  = new Attachment();
        Blob pdfPageBlob;  
        
        //#####Email attachment
        PageReference statementPdf = new PageReference(Site.getPathPrefix() + '/apex/CustomStatementNew');
        statementPdf.getParameters().put('id', accId);
        pdfPageBlob = (Test.isRunningTest()) ?
            Blob.valueOf(String.valueOf('This is a test page')) : statementPdf.getContentAsPDF();
        String customerName = Accountname;   
        
        statementAttachment.Body        = pdfPageBlob;
        statementAttachment.Name        = 'General Statement-' + customerName + '-' + (System.now()).date().format() + '.pdf';
        statementAttachment.ParentId    = accId;
        statementAttachment.IsPrivate   = false;
        statementAttachment.Description = 'Customer Statement';
        
        return statementAttachment;
    }
    
    @AuraEnabled
    public static Boolean NCPSOAsendStatement(Account client, List<String> toAddress, List<String> ccAddress,Contact myCon) {
        
        List<OrgWideEmailAddress> owea = [select Id from OrgWideEmailAddress where Address = 'app@tecex.com'];
        
        Messaging.SingleEmailMessage email;
        
        Boolean isSent = false;
        
        if(toAddress.size()>0){
            //Contact myCon = getRelatedContacts(customerId)[0];
            EmailTemplate emailTemplates = [
                SELECT Subject, Body, HtmlValue
                FROM EmailTemplate
                WHERE DeveloperName = 'Tecex_Statement_Of_Account' Limit 1];
            
            if(emailTemplates != null){
                email = new Messaging.SingleEmailMessage();
                
                //EmailTemplate emailTemplate = emailTemplates[0];
                
                //add template
                email.setTemplateId(emailTemplates.Id);
                
                //set target object for merge fields
                email.setTargetObjectId(mycon.id);
                
                email.setwhatId(client.id);
                
                if ( owea.size() > 0 ) {
                    email.setOrgWideEmailAddressId(owea[0].Id);
                }
                
                //  Account client = new DAL_Account().selectById(customerId);
                email.setToAddresses(toAddress);
                System.debug('cc Address###  ' + ccAddress);
                System.debug('cc Address###  ' + ccAddress[0]);
                System.debug('cc Address empty?###  ' + ccAddress.isEmpty());
                System.debug('cc Address Size###  ' + ccAddress.size());
                
                if(ccAddress[0] !=''){
                    email.setCcAddresses(ccAddress);
                }
                System.debug('site-->'+Site.getPathPrefix());
                //#####Email attachment
                PageReference statementPdf = new PageReference(Site.getPathPrefix() + '/apex/CustomStatementNew');
                statementPdf.getParameters().put('id', client.id);
                Blob pdfPageBlob = (Test.isRunningTest()) ? Blob.valueOf(String.valueOf('This is a test page')) : statementPdf.getContentAsPDF();
                String customerName = client.Name;   
                Attachment statementAttachment  = new Attachment();
                statementAttachment.Body        = pdfPageBlob;
                statementAttachment.Name        = 'General Statement-' + customerName + '-' + (System.now()).date().format() + '.pdf';
                statementAttachment.ParentId    = client.id;
                statementAttachment.IsPrivate   = false;
                statementAttachment.Description = 'Customer Statement';
                
                List<Messaging.EmailFileAttachment> statementAttachmentsList = new List<Messaging.EmailFileAttachment>();
                Messaging.EmailFileAttachment statementAttachmentFile = new Messaging.EmailFileAttachment();
                
                statementAttachmentFile.setFileName(statementAttachment.Name);
                statementAttachmentFile.setBody(statementAttachment.Body);
                statementAttachmentFile.setContentType(statementAttachment.ContentType);
                
                statementAttachmentsList.add(statementAttachmentFile);
                email.setFileAttachments(statementAttachmentsList);
                
                //Send Email
                List<Messaging.SendEmailResult> r = Messaging.sendEmail(new List<Messaging.SingleEmailMessage> {email});
                
                if (r[0].isSuccess()){
                    isSent = true;
                    
                    insert statementAttachment;
                    
                }
            }
        }
        return isSent;
    }
    
    @future(callout=true)
    public static void NCPRolloutSendMail(string ROID, List<String> toAddress, List<String> ccAddress){
        
        List<OrgWideEmailAddress> owea = [select Id from OrgWideEmailAddress where Address = 'app@tecex.com'];
        
        List<EmailTemplate> lstEmailTemplates = [SELECT Id,Body, Name, Subject FROM EmailTemplate WHERE Name ='Tecex RollOut'];
        
        Roll_Out__c rollOut = [Select Id, Contact_For_The_Quote__c from Roll_Out__c Where Id=:ROID LIMIT 1];
        
        Boolean isSent = false;
        system.debug('ROID-->'+ROID);
        //Roll_Out__c  ROs =  [Select Id, Name, Client_Name__c, RecordTypeId, Create_Roll_Out__c, Destinations__c, Shipment_Value_in_USD__c, Contact_For_The_Quote__c, File_Reference_Name__c  FROM Roll_Out__c WHERE Id = :ROID Limit 1];
        //system.debug('ROs-->'+ROs);
        PageReference ExportSoRollout = new PageReference(Site.getPathPrefix() + '/apex/NCPRolloutExport');
        //  PageReference ExportSoRollout = page.NCPRolloutExport;
        
        ExportSoRollout.getParameters().put('id', ROId);
        
        Blob pdfPageBlob = (Test.isRunningTest()) ?
            Blob.valueOf(String.valueOf('This is a test page')) : ExportSoRollout.getContent();
        
        Attachment InvoiceAttachment  = new Attachment();
        InvoiceAttachment.Body        = pdfPageBlob;
        InvoiceAttachment.Name        ='Rollout'+ (System.now()).date().format() + '.xls';
        InvoiceAttachment.ParentId    = ROID;
        InvoiceAttachment.IsPrivate   = false;
        InvoiceAttachment.Description = 'Rollouts';
        //  insert InvoiceAttachment;
        
        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
        if(!ccAddress.Isempty() && ccAddress[0] !=''){
            email.setCcAddresses(ccAddress);
        }
        if ( owea.size() > 0 ) {
            email.setOrgWideEmailAddressId(owea[0].Id);
        }
        email.setToAddresses(toAddress);
        email.setTemplateId(lstEmailTemplates[0].Id);
        email.setWhatId(ROID);
        email.setTargetObjectId(rollOut.Contact_For_The_Quote__c);
        //        email.setSaveAsActivity(true);
        
        if(InvoiceAttachment !=null){
            
            List<Messaging.EmailFileAttachment> InvoiceAttachmentsList = new List<Messaging.EmailFileAttachment>();
            Messaging.EmailFileAttachment InvoiceAttachmentFile = new Messaging.EmailFileAttachment();
            
            InvoiceAttachmentFile.setFileName(InvoiceAttachment.Name);
            InvoiceAttachmentFile.setBody(InvoiceAttachment.Body);
            InvoiceAttachmentFile.setContentType(InvoiceAttachment.ContentType);
            
            InvoiceAttachmentsList.add(InvoiceAttachmentFile);
            email.setFileAttachments(InvoiceAttachmentsList);
            
            List<Messaging.SendEmailResult> r = Messaging.sendEmail(new List<Messaging.SingleEmailMessage> {email});
            if (r[0].isSuccess()){
                isSent = true;
                //   insert InvoiceAttachment;
                
            }
            
        }
        
        // return isSent;
    }
    
    
}