global class btMonthEndProcessSchedule implements Schedulable 
{
   global void execute(SchedulableContext SC) 
   {
      btMonthEndProcess UC = new btMonthEndProcess();
      database.executebatch(UC);

    }
}