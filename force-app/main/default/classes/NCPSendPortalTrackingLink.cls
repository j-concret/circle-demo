@RestResource(urlMapping='/PortalTrackingLink/*')
Global class NCPSendPortalTrackingLink {
      @Httppost
      global static void NCPSendPortalTrackingLink(){
          
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        Blob body = req.requestBody;
    	res.addHeader('Content-Type', 'application/json');
        String requestString = body.toString();
        NCPPortalTrackingwra rw  = (NCPPortalTrackingwra)JSON.deserialize(requestString,NCPPortalTrackingwra.class);
           try {
                 List<Access_token__c> At =[select status__c,Access_Token__c from Access_token__c where Access_Token__c=:rw.Accesstoken and status__c ='Active' limit 1];
                if(At.size() > 0) {
                     Messaging.SingleEmailMessage[] messages = new List<Messaging.SingleEmailMessage>();
   					 EmailTemplate et = [Select Id,Name from EmailTemplate where Name = 'sendTrackingnumber'];
                    
                    OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where Address = 'app@tecex.com'];
                    
                    shipment_Order__C SO = [SELECT Id, Client_Contact_for_this_Shipment__c FROM Shipment_Order__c where Id=:rw.SOID];
                      Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
                    	
                        email.saveAsActivity = false;
                     
                if ( owea.size() > 0 ) {
                email.setOrgWideEmailAddressId(owea.get(0).Id);
                }
                email.setReplyTo('support@tecex.com');
                email.setTargetObjectId(SO.Client_Contact_for_this_Shipment__c);
                email.setUseSignature(false);
                email.setTreatTargetObjectAsRecipient(false);
                email.setTemplateID(et.Id);
                email.setWhatId(SO.Id);
               
                    
                      List<String> toAddress = new List<String>();
                    toAddress.add(rw.toemailaddress);
                    email.setToAddresses(toAddress);
                messages.add(email);
                    
                 Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
                    
                    
                JSONGenerator gen = JSON.createGenerator(true);
                gen.writeStartObject();
                gen.writeFieldName('Success');
                gen.writeStartObject();
                
                gen.writeStringField('Message','Email sent succesfully');
                gen.writeEndObject();
                gen.writeEndObject();
                String jsonData = gen.getAsString();
                res.responseBody = Blob.valueOf(jsonData);
                res.statusCode=200;  
                
                API_Log__c Al = New API_Log__c();
                Al.EndpointURLName__c='NCPSendPortalTrackingLink';
                Al.StatusCode__c=string.valueof(res.statusCode);
                Al.Response__c = 'POrtal tracking link sent';
                Insert Al;
                    
                    
                    
           } 
               else{
                 res.responseBody = Blob.valueOf(Util.getResponseString('Error','Access Token is invalid or expired.'));
                 res.addHeader('Content-Type', 'application/json');
                 res.statusCode = 400;
                 insert new API_Log__c(
                    
                    EndpointURLName__c = 'NCPSendPortalTrackingLink',
                    Response__c = 'Error - Invalid AccessToken',
                    StatusCode__c = String.valueof(res.statusCode)
                    );
               }
      
           }
          catch(exception e){
              System.debug('error -->'+ e.getLineNumber() +' -- >  '+ e.getMessage());
               res.responseBody = Blob.valueOf(Util.getResponseString('Error','Something went wrong'));
                 res.addHeader('Content-Type', 'application/json');
                 res.statusCode = 500;
                 insert new API_Log__c(
                    
                    EndpointURLName__c = 'NCPSendPortalTrackingLink',
                    Response__c = 'Error - Invalid AccessToken',
                    StatusCode__c = String.valueof(res.statusCode)
                    );
               }
      
              
          
      }
    

}