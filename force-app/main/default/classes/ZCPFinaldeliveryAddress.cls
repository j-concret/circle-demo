@RestResource(urlMapping='/GetZeeFinaldeliveryAddress/*')
Global class ZCPFinaldeliveryAddress {
    
     @Httppost
      global static void ZCPFinaldeliveryAddress(){
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        Blob body = req.requestBody;
    	res.addHeader('Content-Type', 'application/json');
        String requestString = body.toString();
        ZCPFinaldeliveryAddresswra rw  = (ZCPFinaldeliveryAddresswra)JSON.deserialize(requestString,ZCPFinaldeliveryAddresswra.class);
          Try{
              Access_token__c At =[select status__c,Access_Token__c from Access_token__c where Access_Token__c=:rw.Accesstoken ];
              if( at.status__C =='Active' ){
                       Id devRecordTypeId = Schema.SObjectType.Client_Address__c.getRecordTypeInfosByName().get('Final Destinations').getRecordTypeId();
                       String eComProspectiveAccId = '';
                       Set<Id> accountIds = new Set<Id>();
                       accountIds.add(rw.AccountID);
                 
                  List<Account> accountList = [Select Id, Name,RecordType.Name, Allow_Customised_Final_Deliveries__c FROM Account
                                                         WHERE Id = :rw.AccountID OR Name = 'TecEx E-Commerce Prospective Client'];
            
             for(Account acc : accountList){
                                
                                //Collecting E-Commerce Prospective Client id
                                if(acc.Name == 'TecEx E-Commerce Prospective Client'){
                                    eComProspectiveAccId = acc.Id;
                                }
                                
                                if(((acc.RecordType.Name == 'Client (E-commerce)' || acc.RecordType.Name == 'Zee') && acc.Name != 'TecEx E-Commerce Prospective Client')){
                              accountIds.add(eComProspectiveAccId);
                              
                                    if(!acc.Allow_Customised_Final_Deliveries__c){
                                        accountIds.remove(acc.Id);
                                                              
                                    }
                                }
                                
                            }
                
            
              List<Client_Address__c> clientAddresses = [SELECT Id,Name,client__r.name,client__r.Allow_Customised_Final_Deliveries__c,CaseID__c,CompanyName__c,AdditionalContactNumber__c,Contact_Email__c,Contact_Full_Name__c,Contact_Phone_Number__c,Address__c,Address2__c,Province__c,City__c,Postal_Code__c,All_Countries__c FROM Client_Address__c WHERE Client__c IN :accountIds AND All_Countries__c=: rw.Shiptocountry AND recordtypeID =: devRecordTypeId ];
                
	                res.responseBody = Blob.valueOf(JSON.serializePretty(clientAddresses));
                    res.addHeader('Content-Type', 'application/json');
                    res.statusCode = 200;
                    
                    API_Log__c Al = New API_Log__c();
                    Al.EndpointURLName__c='ZCPFinaldeliveryAddress';
                    Al.StatusCode__c=string.valueof(res.statusCode);
                    Insert Al;  

              }
              }
                  catch(Exception e){
          	String ErrorString ='Something went wrong while creating cost estimate, please contact support_SF@tecex.com';
            res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
            res.addHeader('Content-Type', 'application/json');
            res.statusCode = 500;
			
            System.debug(e.getMessage()+' at line number: '+e.getLineNumber());          
                      
            API_Log__c Al = New API_Log__c();
            Al.EndpointURLName__c='ZCPFinaldeliveryAddress';
            Al.Response__c=e.getMessage()+' at line number: '+e.getLineNumber();
            Al.StatusCode__c=string.valueof(res.statusCode);
            Insert Al;

                      
                  }
              }
}