public class ShipmentOrderToInvoiceController {

    private ApexPages.StandardController standardController;
    

    public ShipmentOrderToInvoiceController(ApexPages.StandardController standardController) 
    { 
        this.standardController = standardController;
    }

    public PageReference createInvoice()
    {
        ShipmentOrderToInvoice soToInv = new ShipmentOrderToInvoice();
        id shipOrderId = standardController.getId();
        soToInv.savePdf(shipOrderId);
        PageReference sendInvoice = new PageReference(
            '/email/author/emailauthor.jsp?retURL=/' + 
            shipOrderId + '&rtype=003&template_id=' + soToInv.getShipmentTemplate() + 
            '&email_type=visualforce&new_template=1&p3_lkid=' + shipOrderId);
         
        return sendInvoice;
    }

}