@isTest

public class testCreateAttachment {
   private static testMethod void  createAttachment(){
        Account account = new Account (name='Acme1', Type ='Client', CSE_IOR__c = '0050Y000001LTZO');
        insert account; 
        
        Account account2 = new Account (name='Test Supplier', Type ='Supplier', ICE__c = '0050Y000001km5c');
        insert account2;      
             
          
        country_price_approval__c cpa = new country_price_approval__c( name ='Aruba', Billing_term__c = 'DAP/CIF - IOR pays',  Airwaybill_Instructions__c = 'User IOR Address', Supplier__c = account2.Id);
        insert cpa;
        
        IOR_Price_List__c iorpl = new IOR_Price_List__c ( Client_Name__c = account.Id, Name = 'Aruba', IOR_Fee__c = 0.15, On_Charge_Mark_up__c = 0.15,
        TecEx_Shipping_Fee_Markup__c =0.40, IOR_Min_Fee__c = 100,  Admin_Fee__c =100, Set_up_fee__c =100, Bank_Fees__c =200, Tax_Rate__c = 0.20,
        Estimated_Customs_Brokerage__c =250, Estimated_Customs_Clearance__c =250, Estimated_Customs_Handling__c =250, Estimated_Import_License__c =250);
        insert iorpl; 
     
        Contact contact = new Contact ( lastname ='Testing Individual',  AccountId = account.Id);
        insert contact; 
        
        Opportunity opportunity = new Opportunity(AccountId = account.id, Name ='120-547', Ship_to_Country__c = cpa.Id, IOR_Price_List__c = iorpl.Id, Shipment_Value_USD__c =1000,
        Ship_From_Country__c = 'Algeria', CloseDate= system.today(), StageName ='Shipment Pending', Client_Contact_for_this_Shipment__c = contact.Id, Who_arranges_International_courier__c ='Client', Tax_Treatment__c ='DAP/CIF - IOR pays' );
        insert opportunity;
     
        Shipment_Order__c shipmentOrder = new Shipment_Order__c(Account__c = account.id, Ship_to_Country__c = cpa.Id, IOR_Price_List__c = iorpl.Id, Shipment_Value_USD__c =1000, Opportunity__c = opportunity.Id,
        Client_Contact_for_this_Shipment__c = contact.Id, Who_arranges_International_courier__c ='Client', Tax_Treatment__c ='DAP/CIF - IOR pays', Shipping_Status__c ='Shipment Pending' );
        insert shipmentOrder;
        

        test.startTest();

        PageReference pageRef = Page.Attachment;
        
        Test.setCurrentPage(pageRef);
        
        
        pageRef.getParameters().put('id',shipmentOrder.id);
        ApexPages.StandardController stdCon= new ApexPages.StandardController(shipmentOrder);
        AttachmentUploadController con = new AttachmentUploadController(stdCon);
        
        con.file_Name = 'Test Document';
        con.file_Body = Blob.valueOf('unit test Attachment Body');
        con.processUpload();
        
           
        Attachment__c currentAttachment= [select id, Initial_HTTP_Response__c, Validation_HTTP_Response__c, Attachment_Type__c  from attachment__c where Shipment_Order__c =: shipmentOrder.Id order by createdDate desc];  
        
        
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        uploadFileToTransact.uploadFile(con.file_Body , 'Commercial Invoice', Blob.valueOf('xml'), 'BatchMetadata.xml', 'http://34.244.26.115:8080/tecexsfws/uploadBatch', String.valueof(currentAttachment.Id));

        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        updateEphesoftResponse.updateAtt(currentAttachment.Id, 'testdata');
        
             
        test.stopTest();

     
       
    } 
    
    private static testMethod void  testEphesoftResponse(){
        Account account = new Account (name='Acme1', Type ='Client', CSE_IOR__c = '0050Y000001LTZO');
        insert account; 
        
        Account account2 = new Account (name='Test Supplier', Type ='Supplier', ICE__c = '0050Y000001km5c');
        insert account2;      
             
          
        country_price_approval__c cpa = new country_price_approval__c( name ='Aruba', Billing_term__c = 'DAP/CIF - IOR pays',  Airwaybill_Instructions__c = 'User IOR Address', Supplier__c = account2.Id);
        insert cpa;
        
        IOR_Price_List__c iorpl = new IOR_Price_List__c ( Client_Name__c = account.Id, Name = 'Aruba', IOR_Fee__c = 0.15, On_Charge_Mark_up__c = 0.15,
        TecEx_Shipping_Fee_Markup__c =0.40, IOR_Min_Fee__c = 100,  Admin_Fee__c =100, Set_up_fee__c =100, Bank_Fees__c =200, Tax_Rate__c = 0.20,
        Estimated_Customs_Brokerage__c =250, Estimated_Customs_Clearance__c =250, Estimated_Customs_Handling__c =250, Estimated_Import_License__c =250);
        insert iorpl; 
     
        Contact contact = new Contact ( lastname ='Testing Individual',  AccountId = account.Id);
        insert contact; 
        
        Opportunity opportunity = new Opportunity(AccountId = account.id, Name ='120-547', Ship_to_Country__c = cpa.Id, IOR_Price_List__c = iorpl.Id, Shipment_Value_USD__c =1000,
        Ship_From_Country__c = 'Algeria', CloseDate= system.today(), StageName ='Shipment Pending', Client_Contact_for_this_Shipment__c = contact.Id, Who_arranges_International_courier__c ='Client', Tax_Treatment__c ='DAP/CIF - IOR pays' );
        insert opportunity;
     
        Shipment_Order__c shipmentOrder = new Shipment_Order__c(Account__c = account.id, Ship_to_Country__c = cpa.Id, IOR_Price_List__c = iorpl.Id, Shipment_Value_USD__c =1000, Opportunity__c = opportunity.Id,
        Client_Contact_for_this_Shipment__c = contact.Id, Who_arranges_International_courier__c ='Client', Tax_Treatment__c ='DAP/CIF - IOR pays', Shipping_Status__c ='Shipment Pending' );
        insert shipmentOrder;
        

        

        PageReference pageRef = Page.Attachment;
        
        Test.setCurrentPage(pageRef);
        
        
        pageRef.getParameters().put('id',shipmentOrder.id);
        ApexPages.StandardController stdCon= new ApexPages.StandardController(shipmentOrder);
        AttachmentUploadController con = new AttachmentUploadController(stdCon);
        
        con.file_Name = 'Test Document';
        con.file_Body = Blob.valueOf('unit test Attachment Body');
        con.processUpload();
        
        Attachment__c currentAttachment= [select id, Initial_HTTP_Response__c, Validation_HTTP_Response__c, Attachment_Type__c  from attachment__c where Shipment_Order__c =: shipmentOrder.Id order by createdDate desc]; 
        
        currentAttachment.Initial_HTTP_Response__c = 'UploadResponse xml';
        update currentAttachment;
        
        test.startTest();
        
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        con.Ephesoft();
        
        
       
        test.stopTest();

     
       
    } 

   }