@isTest
public class NCPCreateCase_Test {

    static testMethod void  testForNCPSORelatedObjects(){
        
        Id shipmentOrderRecTypeId = Schema.SObjectType.shipment_order__c.getRecordTypeInfosByName().get('Cost Estimate').getRecordTypeId();
        Id accountRecTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Freight Supplier').getRecordTypeId();
        
CpaRules__c cpaRule = new CpaRules__c(Country__c = 'Finland',
                                    	 	  Criteria__c = 'Default');
        Insert cpaRule;
        
        Access_token__c accessTokenObj = new Access_token__c(Status__c = 'Active',
            												 Access_Token__c = 'asdfghj-sdfgfds-sfdfgf');
        Insert accessTokenObj;
        
        Account acc = new Account(Name = 'TecEx Prospective Client',
                                  RecordTypeId = accountRecTypeId);
        Insert acc;
        Account acc1 = new Account(Name = 'DHL',
                                  Supplier_type__c = 'Logistics Provider');
        Insert acc1;
        Contact con = new Contact(LastName = 'Testcon',
                                  AccountId = acc.Id);
        Insert con;
        
        IOR_Price_List__c priceList = new IOR_Price_List__c(Destination__c = 'Finland',
                                                            Client_Name__c = acc.Id,
                                                            IOR_Fee__c = 10);
        Insert priceList;
        
        Country__c country = new Country__c(Name = 'United States',Country__c = 'United States');
        Insert country;
        
        CPA_v2_0__c cpaV2 = new CPA_v2_0__c(Name = 'Test',
                                            Country_Applied_Costings__c = true);
        Insert cpaV2;
        
        CPA_v2_0__c cpa = new CPA_v2_0__c(Name = 'Test Cpa',
                                          Freight_Only_Lane__c = true,
                                          Inactive_Lane__c = false,
                                          Country__c = country.Id,
                                          Supplier__c = acc.Id,
                                          Related_Costing_CPA__c = cpaV2.Id,
                                          Preferred_Freight_Forwarder__c = acc1.Id);
        Insert cpa;
        
        shipment_order__c shipment = new shipment_order__c(RecordTypeId = shipmentOrderRecTypeId,
                                                           CPA_v2_0__c = cpa.Id,
                                                           Destination__c = 'Finland',
                                                           IOR_Price_List__c = priceList.Id,
                                                           Account__c = acc.Id,
                                                           CPA_Costings_Added__c = true);
        Insert shipment;
        
        NCPCreateCaseWra.Attachment attach = new NCPCreateCaseWra.Attachment();
        attach.filename = 'attachment Name';
        attach.filebody = Blob.valueOf('data for the attachment');
        
        List<NCPCreateCaseWra.Attachment> attachmentList = new List<NCPCreateCaseWra.Attachment>();
        attachmentList.add(attach);
        
        Account aObj = [Select Id, OwnerId from Account Limit 1];
        
        NCPCreateCaseWra obj = new NCPCreateCaseWra();
        obj.AccessToken = accessTokenObj.Access_Token__c;
        obj.Subject = 'Testing';
        obj.Description = 'Testing the Class.';
        obj.ContactID = con.Id;
        obj.AccountID = acc.Id;
        obj.SOID = shipment.Id;
        obj.ClientuserID = aObj.OwnerId;
        obj.Attachment = attachmentList;
        
        RestRequest req1 = new RestRequest();
        RestResponse res1 = new RestResponse();
        req1.requestURI = '/services/apexrest/NCPCreateCase/'; 
        req1.httpMethod = 'POST';
        req1.requestBody = Blob.valueOf(JSON.serialize(obj));     
        RestContext.request = req1;
        RestContext.response = res1;
        RestContext.request.addHeader('Content-Type', 'application/json');
        
        Test.startTest(); 
        	NCPCreateCase.NCPCreateCase();
        Test.stopTest();
    }
    
    static testMethod void  testErrorForNCPSORelatedObjects(){
        
        RestRequest req1 = new RestRequest();
        RestResponse res1 = new RestResponse();
        req1.requestURI = '/services/apexrest/NCPCreateCase/'; 
        req1.httpMethod = 'POST';
        req1.requestBody = Blob.valueOf('{"value":"Test Response"}');     
        RestContext.request = req1;
        RestContext.response = res1;
        RestContext.request.addHeader('Content-Type', 'application/json');
        
        Test.startTest(); 
        	NCPCreateCase.NCPCreateCase();
        Test.stopTest();
    }
}