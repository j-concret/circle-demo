@isTest
public class NCPUpdateCase_Test {
    
    static testMethod void  testForNCPUpdateCase(){
        
        Access_token__c at = new Access_token__c(Status__c = 'Active',
            												 Access_Token__c = 'asdfghj-sdfgfds-sfdfgf');
        Insert at;
        
        Account acc = new Account(Name = 'Test Account');
        Insert acc;
        
        Contact con = new Contact(LastName = 'Testcon',
                                  AccountId = acc.Id);
        Insert con;
        
        Case cs = new Case(Subject = 'Test Subject',
                          Status = 'Waiting',
                          Description = 'Description');
        Insert cs;
        
        List<NCPUpdateCaseWra.CaseContactRole> notifyConList = new List<NCPUpdateCaseWra.CaseContactRole>();
        
        NCPUpdateCaseWra obj = new NCPUpdateCaseWra();
        obj.Accesstoken = at.Access_Token__c;
        obj.AccountID = acc.Id;
        obj.ContactID = con.Id;
        obj.CaseID = cs.Id;
        obj.NotifyContacts = notifyConList; 
        
        RestRequest req1 = new RestRequest();
        RestResponse res1 = new RestResponse();
        req1.requestURI = '/services/apexrest/NCPUpdateCase/'; 
        req1.httpMethod = 'POST';
        req1.requestBody = Blob.valueOf(JSON.serialize(obj));     
        RestContext.request = req1;
        RestContext.response = res1;
        RestContext.request.addHeader('Content-Type', 'application/json');
        
        Test.startTest(); 
        	NCPUpdateCase.NCPUpdateCase();
        Test.stopTest();
    }
    
    static testMethod void  testErrorForNCPUpdateCase(){
                
        RestRequest req1 = new RestRequest();
        RestResponse res1 = new RestResponse();
        req1.requestURI = '/services/apexrest/NCPUpdateCase/'; 
        req1.httpMethod = 'POST';
        req1.requestBody = Blob.valueOf('{"value":"Test Response"}');     
        RestContext.request = req1;
        RestContext.response = res1;
        RestContext.request.addHeader('Content-Type', 'application/json');
        
        Test.startTest(); 
        	NCPUpdateCase.NCPUpdateCase();
        Test.stopTest();
    }

}