@isTest
private class DMN_StatementTest {
	
	@isTest static void TestDMN_Statement() {

        //Prapare testing data
        String period                = 'testingPeriod';
        Decimal amount               = 12.2;
        Date lineItemDate            = system.today();
        String shipmentNumber        = '1234';
        String receipt_InvoiceNumber = '2468';
        String purchaseOrderNumber   = '369';
        String shipToCountry         = 'South Africa'; 
        String lineItemType          = 'Testing';
        Date dueDate                 = lineItemDate.addDays(10);
        Decimal invoiceTotal         = 10;
        Decimal statementBalance     = 600;
        Object compareTo             = 5;

        DMN_Statement statement                  = new DMN_Statement();
        DMN_Statement.StatementLineItem lineItem = new DMN_Statement.StatementLineItem(lineItemDate, shipmentNumber, receipt_InvoiceNumber, purchaseOrderNumber, shipToCountry, lineItemType, dueDate, invoiceTotal, statementBalance);
        DMN_Statement.AgingPeriod agingPeriod    = new DMN_Statement.AgingPeriod(period, amount);
        agingPeriod.compareTo(agingPeriod);
        lineItem.compareTo(lineItem);
	}
}