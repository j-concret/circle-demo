@IsTest
public class RolloutExportforNCP_Test {

    static testMethod void testRolloutExportforNCP(){
        Access_token__c accessTokenObj = new Access_token__c();
        accessTokenObj.Status__c = 'Active';
        accessTokenObj.Access_Token__c = 'asdfghj-sdfgfds-sfdfgf';
        
        Insert accessTokenObj;
        
        Account acc = new Account(Name = 'Acme Test');
        
        Insert acc;
        
        Test.startTest();
        RolloutExportforNCP.RolloutExportforNCP(acc.id);
        Test.stopTest();
    }
}