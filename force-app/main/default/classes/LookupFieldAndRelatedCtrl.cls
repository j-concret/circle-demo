public with sharing class LookupFieldAndRelatedCtrl {

    @AuraEnabled
    public static List<Map<String,Object> > fetchMatchingRecords( String search_keyword, String object_name,String id_field_name,String label_field_name,String additionalFilters,String filterField,String filterValue ){

        List<Map<String,Object> > data = new List<Map<String,Object> >();
        String search_value =  '%'+ search_keyword + '%';

        String query = 'SELECT '+id_field_name+','+label_field_name+' FROM ' + object_name + ' WHERE Name LIKE:search_value';
        if(String.isNotBlank(additionalFilters)) {
            Map<String,Object> filters = (Map<String,Object>)JSON.deserializeUntyped(additionalFilters);
            for(String key : filters.keySet()) {
                if(key == 'RecordTypeId'){
                    query += ' AND ( ';
                    List<Object> RecordIds =(List<Object>)filters.get(key);
                    for(Integer i = 0; i< RecordIds.Size();i++){
                        query += key+' = \'' + RecordIds[i]+ '\' '+((i+1) < RecordIds.Size() ? ' OR ' : ' ') ;
                    }
                     query += ' ) ';
                }else{
                query += ' AND '+key+' = \''+filters.get(key)+'\' ';
                }
            }
        }
        if(String.isNotBlank(filterField) && String.isNotBlank(filterValue)) {
            query += ' AND '+ filterField+' = \''+filterValue+'\'';
        }

        query +=' ORDER BY createdDate DESC LIMIT 10';

        for(SObject sobj:  Database.query(query)) {
            data.add(new Map<String,Object> {'Id'=>String.valueOf(sobj.get(id_field_name)),'Name'=>String.valueOf(sobj.get(label_field_name))});
        }
        return data;
    }

    @AuraEnabled
    public static List<Map<String,Object> > getSelectedRecord( String record_id, String object_name,String id_field_name,String label_field_name,String additionalFilters,String filterField,String filterValue ){

        List<Map<String,Object> > data = new List<Map<String,Object> >();

        String query = 'SELECT '+id_field_name+','+label_field_name+' FROM ' + object_name + ' WHERE '+id_field_name+'=:record_id';
        if(String.isNotBlank(additionalFilters)) {
            Map<String,Object> filters = (Map<String,Object>)JSON.deserializeUntyped(additionalFilters);
            for(String key : filters.keySet()) {
                if(key == 'RecordTypeId'){
                    query += ' AND ( ';
                    List<Object> RecordIds =(List<Object>)filters.get(key);
                    for(Integer i = 0; i< RecordIds.Size();i++){
                        query += key+' = \'' + RecordIds[i]+ '\' '+((i+1) < RecordIds.Size() ? ' OR ' : ' ') ;
                    }
                     query += ' ) ';
                }else{
                query += ' AND '+key+' = \''+filters.get(key)+'\' ';
                }
            }
        }
        if(String.isNotBlank(filterField) && String.isNotBlank(filterValue)) {
            query += ' AND '+ filterField+' = \''+filterValue+'\'';
        }

        query +=' ORDER BY createdDate DESC LIMIT 10';

        for(SObject sobj:  Database.query(query)) {
            data.add(new Map<String,Object> {'Id'=>String.valueOf(sobj.get(id_field_name)),'Name'=>String.valueOf(sobj.get(label_field_name))});
        }
        return data;
    }
}