public class TH_Address extends TH_Base {

	protected override map<String, String> dragonFieldMap() {
		return XF_Dragon_Request.AddressFieldMap;
	}	

    /*public void execute () {
   		if (Trigger.isAfter && Trigger.isUpdate) { afterUpdate(); }
   		else if (Trigger.isAfter && (Trigger.isInsert || Trigger.isUndelete)) { afterInsert(); }
   		else if (Trigger.isAfter && Trigger.isDelete) { afterDelete(); }
    }
    
	public void afterUpdate() {
		map<Client_Address__c, Client_Address__c> items = new map<Client_Address__c, Client_Address__c>(); 
		for (SObject so: Trigger.new) 
			items.put((Client_Address__c)so, (Client_Address__c)Trigger.oldMap.get(so.Id));
       	doAfterUpdate(items);
	}
	
	public void afterInsert() {
		list<Client_Address__c> items = new list<Client_Address__c>(); 
		for (SObject so: Trigger.new) 
			items.add((Client_Address__c)so);
		doAfterInsert(items);
	}
	
	public void afterDelete() {
		list<Client_Address__c> items = new list<Client_Address__c>(); 
		for (SObject so: Trigger.old) 
			items.add((Client_Address__c)so);
		doAfterDelete(items);
	}
	
	public void doAfterUpdate(map<Client_Address__c, Client_Address__c> items) {
		list<SimpleObjectMap> mappedItems = new list<SimpleObjectMap>();
		for (Client_Address__c item: items.keySet())  
			try {
	  			if (!AppUtils.isAPICall(item, items.get(item))) 
	  				if (hasChanges(items.get(item), item)) 
	  				  	mappedItems.add(SimpleObjectMap.Make(item, item.Client__c, getFields(item)));
			} catch (Exception e) {
				SysUtils.processException(e, item);
				throw e;
			}
		dragonService.upsertAddresses(mappedItems, false);
	}
	
	public void doAfterInsert(list<Client_Address__c> items) { 
		list<SimpleObjectMap> mappedItems = new list<SimpleObjectMap>();
		for (Client_Address__c item: items) 
			try {
	  			if (!AppUtils.isAPICall(item))
	  				mappedItems.add(SimpleObjectMap.Make(item, item.Client__c, getFields(item)));
			} catch (Exception e) {
				SysUtils.processException(e, item);
				throw e;
			}
  		dragonService.upsertAddresses(mappedItems, true);
	}
	
	public void doAfterDelete(list<Client_Address__c> items) { 
		list<ID> deletedItems = new list<ID>();
		for (Client_Address__c item: items) 
  			// how do we determine this if this is an API call?
  			deletedItems.add(item.Id);
  		dragonService.deleteAddresses(deletedItems);
	} */
}