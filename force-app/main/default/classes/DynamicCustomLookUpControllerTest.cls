@isTest
public class DynamicCustomLookUpControllerTest {
@testSetup static void setup() {
       
        Account account2 = new Account (name='Test Client', Type ='Client'); 
        insert account2;
     	Enterprise_Billing__c ebill = new Enterprise_Billing__c (Date__c = Date.today(), Account__c =account2.id, Short_Name__c='Test Ent', Customer_PO__c = 'customerPO', Billing_Start_Date__c = Date.valueOf('2020-04-06'), Billing_End_Date__c = Date.valueOf('2020-04-06'));
        insert ebill;
    
        
    }
    
    @isTest static void testFetchLookUpValues() {
        Test.startTest();
        List<Account> accList = DynamicCustomLookUpController.fetchLookUpValues('Test','Account');
        Test.stopTest();
        System.assert((accList[0].Name).containsIgnoreCase('Test'));
    }
    
    @isTest static void testfetchLookUpValuesWithType(){
        Test.startTest();
        List<Account> accList = DynamicCustomLookUpController.fetchLookUpValuesWithType('Test','Account');
        Test.stopTest();
        System.assert((accList[0].Name).containsIgnoreCase('Test'));
    }
    
    @isTest static void testfetchLookUpValuesWithAccount(){
        Test.startTest();
        Account account = [select Id, Name from Account where Name = 'Test Client']; 
        List<Enterprise_Billing__c> entList = DynamicCustomLookUpController.fetchLookUpValuesWithAccount('Test','Enterprise_Billing__c',account.Id);
        Test.stopTest();
        System.assert((entList[0].Short_Name__c).containsIgnoreCase('Test'));
    }
}