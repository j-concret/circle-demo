@RestResource(urlMapping='/CPCEQuickQuoteCreation/*')
Global class CPCEProformaCreation {
    @Httppost
    global static void CECreation(){

        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        Blob body = req.requestBody;
        res.addHeader('Content-Type', 'application/json');
        String requestString = body.toString();
        CPCEProformaCreationWrapper rw = (CPCEProformaCreationWrapper)JSON.deserialize(requestString,CPCEProformaCreationWrapper.class);
        try {
            Shipment_Order__c SO = new Shipment_Order__c();
            Account Ac = [SELECT Id,(SELECT Quote_Expiry_choice_In_days__c FROM Contacts WHERE Id=:rw.ContactID),Lead_AM__c,Service_Manager__c,Shipping_Premium_Discount__c,Vat_Team__c,Compliance__c,Operations_Manager__c,Financial_Manager__c,Financial_Controller__c,Logistics_Coordinator__c,cse_IOR__c,finance_Team__c,CDC__c,invoice_Timing__c FROM Account WHERE Id =: rw.AccountID];


            SO.Account__c = rw.AccountID;
            SO.Client_Contact_for_this_Shipment__c = rw.ContactID;
            if(rw.ServiceType=='IOR') {
                SO.Destination__c= rw.ShipTo;
                SO.Ship_From_Country__c = rw.ShipFrom;
            }
            else{
                SO.Destination__c= rw.ShipFrom;
                SO.Ship_From_Country__c = rw.ShipTo;
            }
            SO.Service_Type__c= rw.ServiceType;
            SO.Source__c= 'Client Portal';
            SO.Client_Reference__c= rw.Reference1;
            SO.Client_Reference_2__c= rw.Reference2;
            SO.Who_arranges_International_courier__c= rw.Courier_responsibility;
            SO.Final_Deliveries_New__c = 1;
            SO.Number_of_Final_Deliveries_Client__c=1;
            SO.Shipment_Value_USD__c= rw.ShipmentvalueinUSD;
            SO.Chargeable_Weight__c  =decimal.valueof(rw.estimatedChargableweight);
            SO.RecordTypeId= '0120Y0000009cEz';
            SO.Shipping_Status__c   ='Cost Estimate Abandoned';
            SO.Compliance_Team__c = ac.Compliance__c;
            SO.Vat_Team__c = ac.Vat_Team__c;
            SO.Finance_Team__c =ac.finance_Team__c;
            SO.IOR_CSE__c =ac.cse_IOR__c;
            SO.CDC__c =ac.CDC__c;
            SO.invoice_Timing__C=ac.invoice_Timing__c;
            SO.Lead_AM__c=ac.Lead_AM__c;
            SO.Financial_Controller__c=ac.Financial_Controller__c;
            SO.Financial_Manager__c =ac.Financial_Manager__c;
            SO.Freight_co_ordinator__c = ac.Logistics_Coordinator__c;
            SO.Service_Manager__c =ac.Service_Manager__c;
            SO.Operations_Manager__c=ac.Operations_Manager__c;
            SO.Quote_expiry_notification_Date__c =  System.now().addDays(Integer.valueOf(60 - ac.contacts[0].Quote_Expiry_choice_In_days__c));

            if(rw.Courier_responsibility == 'TecEx' || rw.Courier_responsibility == 'Client') {
                SO.NON_Pickup_freight_request_created__c = true;
            } else {
                SO.NON_Pickup_freight_request_created__c = false;
            }
            Insert SO;

            // Create Freight
            Freight__c FR = new Freight__c();
            if(rw.Courier_responsibility == 'TecEx' || rw.Courier_responsibility == 'Client') {
                Id ConsigneerecordTypeId = Schema.SObjectType.Client_Address__c.getRecordTypeInfosByDeveloperName().get('Consignee_Address').getRecordTypeId();
                List<Carrier_Account__c> CarrierAccounts = [SELECT ID__c,Carrier_Name__c,Carrier_Account_Description__c,name,Account__c,Account__r.name FROM Carrier_Account__c WHERE Account__c =:rw.AccountID OR Account__c ='0011l00000bMRDiAAO'];

                Shipment_Order__c SO1 = [SELECT Id,SupplierlU__c,Destination__c,CPA_v2_0__c FROM Shipment_Order__c WHERE id= :SO.Id];

                Client_Address__c Pickupadd = Test.isRunningTest() ?[SELECT Id,name,CaseId__c,Contact_Full_Name__c, Contact_Email__c,Contact_Phone_Number__c,AdditionalContactNumber__c,CompanyName__c,Address__c,Address2__c,City__c,Province__c,Postal_Code__c,All_Countries__c,Comments__c FROM Client_Address__c WHERE All_Countries__c = : rw.ShipFrom AND RecordType.Name = 'PickUp']
                                              :[SELECT Id,name,CaseId__c,Contact_Full_Name__c, Contact_Email__c,Contact_Phone_Number__c,AdditionalContactNumber__c,CompanyName__c,Address__c,Address2__c,City__c,Province__c,Postal_Code__c,All_Countries__c,Comments__c FROM Client_Address__c WHERE All_Countries__c = : rw.ShipFrom AND Document_Type__c  = 'Default' AND RecordType.Name = 'PickUp'];

                List<Client_Address__c> Consigneeadd = [SELECT Id,Default_All_Countries__c,Default_Postal_Code__c,
                                                        Default_Province__c,Default__c,Default_Address__c,Default_Address2__c,
                                                        Default_City__c,Default_CompanyName__c,Default_Contact_Email__c,
                                                        Default_Contact_Full_Name__c,Default_Contact_Phone_Number__c,
                                                        CPA_v2_0__c,name,Naming_Conventions__c,CaseId__c,Contact_Full_Name__c,
                                                        Contact_Email__c,Contact_Phone_Number__c,AdditionalContactNumber__c,
                                                        CompanyName__c,Address__c,Address2__c,City__c,Province__c,
                                                        Postal_Code__c,All_Countries__c,Comments__c
                                                        FROM Client_Address__c WHERE Client__c =: So1.SupplierlU__c and recordtypeid = :ConsigneerecordTypeId and address_status__c = 'Active' and CPA_v2_0__c=:So1.CPA_v2_0__c and Carrier_Type__c = 'Courier' and Document_Type__C = 'AWB'];

                List<Client_Address__c> FF_Consigneeadd = [SELECT Id,Default_All_Countries__c,Default_Postal_Code__c,
                                                           Default_Province__c,Default__c,Default_Address__c,Default_Address2__c,
                                                           Default_City__c,Default_CompanyName__c,Default_Contact_Email__c,
                                                           Default_Contact_Full_Name__c,Default_Contact_Phone_Number__c,
                                                           CPA_v2_0__c,name,Naming_Conventions__c,CaseId__c,Contact_Full_Name__c,
                                                           Contact_Email__c,Contact_Phone_Number__c,AdditionalContactNumber__c,
                                                           CompanyName__c,Address__c,Address2__c,City__c,Province__c,
                                                           Postal_Code__c,All_Countries__c,Comments__c FROM Client_Address__c WHERE Client__c =: So1.SupplierlU__c and recordtypeid = :ConsigneerecordTypeId and address_status__c = 'Active' and CPA_v2_0__c=:So1.CPA_v2_0__c and Carrier_Type__c = 'Freight Forwarder' and Document_Type__C = 'AWB'];

                FR.Pick_up_Contact_No__c=Pickupadd.Contact_Phone_Number__c;
                FR.Ship_From_Country__c = rw.ShipFrom;
                FR.Ship_To__c = rw.ShipTo;
                FR.Client__c=rw.AccountID;
                Fr.Shipment_Order__c = SO.Id;
                Fr.International_Shipping_Discount_Override__c = Ac.Shipping_Premium_Discount__c;

                Fr.Chargeable_weight_in_KGs_packages__c = So.Chargeable_Weight__c;
                FR.Pick_up_Contact_No__c=Pickupadd.Contact_Phone_Number__c;
                FR.Pick_Up_Contact_Email__c=Pickupadd.Contact_Email__c;
                FR.Pick_Up_Contact_Address1__c=Pickupadd.Address__c;
                FR.Pick_Up_Contact_Address2__c=Pickupadd.Address2__c;
                FR.Pick_Up_Contact_City__c=Pickupadd.City__c;
                FR.Pick_Up_Contact_Province__c=Pickupadd.Province__c;
                FR.Pick_Up_Contact_Country__c=Pickupadd.All_Countries__c;
                FR.Pick_Up_Contact_ZIP__c=Pickupadd.Postal_Code__c;
                Fr.Ship_From_Address__c = Pickupadd.Id; //pickup
                Fr.Notify_Address__C=    Consigneeadd[0].id;// Notify(Broker) address
                FR.AWB_Consignee_Naming_Conventions__c=Consigneeadd[0].Naming_Conventions__c;
                FR.Con_Company__c=Consigneeadd[0].Default_CompanyName__c;
                FR.Con_Name__c=Consigneeadd[0].Default_Contact_Full_Name__c;
                FR.Con_Email__c=Consigneeadd[0].Default_Contact_Email__c;
                FR.Con_Phone__c=Consigneeadd[0].Default_Contact_Phone_Number__c;

                if(Consigneeadd[0].Default_Address__c != null) {
                    if(Consigneeadd[0].Default_Address__c.length() >= 34) {
                        FR.Con_AddressLine1__c =Consigneeadd[0].Default_Address__c.substring(0,34);
                    }
                    else {
                        FR.Con_AddressLine1__c =Consigneeadd[0].Default_Address__c;
                    }
                }
                if (Consigneeadd[0].Default_Address2__c != null) {
                    if(Consigneeadd[0].Default_Address2__c.length() >= 34) {
                        FR.Con_AddressLine2__c = Consigneeadd[0].Default_Address2__c.substring(0,34);
                    }else {
                        FR.Con_AddressLine2__c = Consigneeadd[0].Default_Address2__c;
                    }
                }

                String street = Consigneeadd[0].Default_Address__c+Consigneeadd[0].Default_Address2__c;
                if(street.length() >= 34) {
                    FR.Con_Street__c = street.substring(0,34);
                }else {
                    FR.Con_Street__c =street;
                }

                FR.Con_City__c=Consigneeadd[0].Default_City__c;
                FR.Con_State__c=Consigneeadd[0].Default_Province__c;
                FR.Con_Country__c=Consigneeadd[0].Default_All_Countries__c;
                FR.Con_Postal_Code__c=Consigneeadd[0].Default_Postal_Code__c;
                 //FR.Pick_Up_Contact_ZIP__c=Pickupadd.Postal_Code__c;
                Fr.FF_Consignee_Address__c = FF_Consigneeadd[0].id;// Notify(Broker) address
                //	If(Consigneeadd.size() <= 0){
                
                FR.FF_AWB_Consignee_Naming_Conventions__c = FF_Consigneeadd[0].Naming_Conventions__c;
                FR.FF_Con_Company__c = FF_Consigneeadd[0].Default_CompanyName__c;
                FR.FF_Con_Name__c = FF_Consigneeadd[0].Default_Contact_Full_Name__c;
                FR.FF_Con_Email__c = FF_Consigneeadd[0].Default_Contact_Email__c;
                FR.FF_Con_Phone__c = FF_Consigneeadd[0].Default_Contact_Phone_Number__c;
                
                if(FF_Consigneeadd[0].Default_Address__c != null){
                    if(FF_Consigneeadd[0].Default_Address__c.length() >= 34){ 
                        FR.FF_Con_AddressLine1__c =FF_Consigneeadd[0].Default_Address__c.substring(0,34);
                    }else {  
                        FR.FF_Con_AddressLine1__c =FF_Consigneeadd[0].Default_Address__c;
                    }
                }
                IF (FF_Consigneeadd[0].Default_Address2__c != null) {  
                    if(FF_Consigneeadd[0].Default_Address2__c.length() >= 34){ 
                        FR.FF_Con_AddressLine2__c =FF_Consigneeadd[0].Default_Address2__c.substring(0,34);
                    }else {  
                        FR.FF_Con_AddressLine2__c =FF_Consigneeadd[0].Default_Address2__c;
                    }
                }
                
                String FF_street = FF_Consigneeadd[0].Default_Address__c+FF_Consigneeadd[0].Default_Address2__c;
                if(FF_street.length() >= 34){ FR.FF_Con_Street__c =FF_street.substring(0,34);}else {  FR.FF_Con_Street__c =FF_street;}
                
                FR.FF_Con_City__c = FF_Consigneeadd[0].Default_City__c;
                FR.FF_Con_State__c = FF_Consigneeadd[0].Default_Province__c;
                FR.FF_Con_Country__c = FF_Consigneeadd[0].Default_All_Countries__c;
                FR.FF_Con_Postal_Code__c = FF_Consigneeadd[0].Default_Postal_Code__c;
                Insert FR;
            }

            Update SO;

            System.enqueueJob(new QueueableUpdateCE(SO));

            Shipment_Order__c SO1 = [SELECT Id,Name,Ship_From_Country__c,IOR_Price_List__r.name,Service_Type__c,Final_Deliveries_New__c,Shipment_Value_USD__c,Type_of_Goods__c,Li_ion_Batteries__c,Lithium_Battery_Types__c,of_packages__c,Chargeable_Weight__c,TecEx_Invoice_Number__c,IOR_FEE_USD__c,IOR_and_Import_Compliance_Fee_USD_numb__c,Tax_recovery_Premium_Fee__c,EOR_and_Export_Compliance_Fee_USD__c,EOR_and_Export_Compliance_Fee_USD_numb__c,Set_up_Fee__c,Handling_and_Admin_Fee__c,Bank_Fees__c,Quoted_Admin_Fee__c,Quoted_Bank_Fee__c,First_Invoice_Created__c,TopUp_Invoice_Created__c,Pull_CPA_and_IOR_PL__c,Taxes_Calculated__c, Recharge_License_Cost__c,xxxTotal_Invoice_Amount__c,Total_Customs_Brokerage_Cost__c,Recharge_Handling_Costs__c,Total_clearance_Costs__c,Recharge_Tax_and_Duty__c,Recharge_Tax_and_Duty_Other__c, International_Delivery_Fee__c,Insurance_Fee_USD__c,Total_License_Cost__c,Finance_Fee__c,Miscellaneous_Fee__c,Miscellaneous_Fee_Name__c,Collection_Administration_Fee__c,Total_Invoice_Amount_Excl_Taxes_Duties__c,Total_including_estimated_duties_and_tax__c,Total_Invoice_Amount__c,Rebate_Due__c,Forex_Rate_Used_Euro_Dollar__c FROM Shipment_order__c WHERE Id=: SO.Id];


            res.responseBody = Blob.valueOf(JSON.serializePretty(SO1));
            res.statusCode = 200;

            API_Log__c Al = New API_Log__c();
            Al.Account__c=rw.AccountID;
            Al.Login_Contact__c=rw.ContactID;
            Al.EndpointURLName__c='CEQuickQuoteCreation';
            Al.Response__c='Success - QuickQuote Created - '+SO.Id;
            Al.StatusCode__c=string.valueof(res.statusCode);
            Insert Al;
        }
        Catch(DMLexception e){
            String ErrorString ='Something went wrong while creating QuickQuote, please contact support_SF@tecex.com';
            res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
            res.statusCode = 500;
            API_Log__c Al = New API_Log__c();
            Al.Account__c=rw.AccountID;
            Al.Login_Contact__c=rw.ContactID;
            Al.EndpointURLName__c='CEQuickQuoteCreation';
            Al.Response__c=e.getMessage()+' at line number: '+e.getLineNumber();
            Al.StatusCode__c=string.valueof(res.statusCode);
            Insert Al;
        }
    }
}