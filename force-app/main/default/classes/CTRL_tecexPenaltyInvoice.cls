public  class CTRL_tecexPenaltyInvoice {

    public Account account {get;set;}
	public List<Invoice_New__c> customerInvoiceList{get;set;}
	public List<Bank_transactions__c> bankTransactionsList{get;set;}
	public Decimal amountDue {get;set;}
	public String invNumber{get;set;}

    public CTRL_tecexPenaltyInvoice(ApexPages.StandardController controller) {
		
		
		
		this.account = [SELECT Id, Name,  IOR_Payment_Terms__c, VAT_Number__c, BillingStreet, BillingCity, BillingState, BillingPostalCode, BillingCountry,
                        Cash_outlay_fee_base__c, Finance_Charge__c, New_Invoicing_Structure__c, Tax_Recovery_Switch_On__c, Tax_recovery_client__c, Penalty_Amount_Due__c, Amount_Outstanding_0_Current__c , Amount_Outstanding_1_30_Days__c, Amount_Outstanding_31_60_Days__c, Amount_Outstanding_61_90_Days__c, Amount_Outstanding_Over_90_Days__c, Unapplied_credits_payments__c, Penalty_PO_Reference__c, Total_Amount_Outstanding__c
                        FROM Account 
                        WHERE Id = :ApexPages.currentPage().getParameters().get('id') LIMIT 1];
		
		System.debug('Account Id ==> ' +ApexPages.currentPage().getParameters().get('id') );
        this.customerInvoiceList = [SELECT Id, Name, Account__c, Shipment_Order__c, Invoice_Age_Text__c, Invoice_Age__c , Today__c, Shipment_Order__r.Name, Freight_Request__r.Name, Conversion_Rate__c, PO_Number_Override__c, PO_Number__c, Invoice_Sent_Date__c, Amount_Outstanding_Local_Currency__c, Invoice_Name__c, Prepayment_date__c, Due_Date__c, IOR_Fees__c, EOR_Fees__c, Admin_Fees__c,   Actual_IOREOR_Costs__c, International_Freight_Fee__c, Liability_Cover_Fee__c, Total_Amount_Incl_Potential_Cash_Outlay__c, Cost_of_Sale_Shipping_Insurance__c, Taxes_and_Duties__c, Recharge_Tax_and_Duty_Other__c, Customs_Brokerage_Fees__c, Customs_Clearance_Fees__c, Customs_License_In__c, Customs_Handling_Fees__c, Bank_Fee__c, Miscellaneous_Fee__c, Miscellaneous_Fee_Name__c, Invoice_amount_USD__c, Collection_Administration_Fee__c, Amount_Outstanding__c, Invoice_Type__c, Invoice_Currency__c, Possible_Cash_Outlay__c, Cash_Outlay_Fee__c, Invoice_Amount_Local_Currency__c, IOR_EOR_Compliance__c, Government_and_In_country_Summary__c, Freight_and_Related_Costs__c, Billing_City__c, Billing_Country__c, Billing_Postal_Code__c, Billing_Street__c, Billing_State__c, Invoice_Status__c, Ship_To_Country__c, Penalty_Outstanding_Amount__c
                                FROM Invoice_New__c
                                WHERE Account__c = :ApexPages.currentPage().getParameters().get('id') 
								AND (Penalty_Status__c = 'Approved' OR Penalty_Status__c = 'New') AND Penalty_Outstanding_Amount__c>0
                                ORDER BY Due_Date__c DESC];
        //System.debug('Invoice List Size ==> ' +customerInvoiceList.size());
       // System.debug('Invoice Name ==> ' +customerInvoiceList[0]);
		
		this.bankTransactionsList = [SELECT Id, Name, Unapplied__c, Date__c
									FROM Bank_transactions__c
									WHERE Supplier__c =:ApexPages.currentPage().getParameters().get('id')
									AND Type__c = 'Shipment' AND (Unapplied__c >= 1 OR  Unapplied__c <= -1)];

		Decimal penaltyAmountDue;
		Decimal invoiceBalance;
		if(account.Penalty_Amount_Due__c == null){
			penaltyAmountDue = 0;
		}else { penaltyAmountDue = account.Penalty_Amount_Due__c;}
        
        if(account.Total_Amount_Outstanding__c == null){
            invoiceBalance = 0;
        }else { invoiceBalance = account.Total_Amount_Outstanding__c;}

		this.amountDue = penaltyAmountDue + invoiceBalance;

		this.invNumber = (System.now()).format('ddMMyy-HHmmss','Africa/Harare');  //String.valueOf(System.Now());



	}

}