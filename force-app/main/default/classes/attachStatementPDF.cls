public class attachStatementPDF {


   public Account account {get;set;}
    public Invoice_New__c invoice {get;set;} 
   public List<AcctSeed__Billing__c> billing {get;set;}
   list<AggregateResult> BillingBalance = null;
   
  
   
    
    public attachStatementPDF(ApexPages.StandardController controller) {
    
        this.account = [Select Id, Name, BillingAddress, IOR_Payment_Terms__c, VAT_Number__c, BillingStreet, BillingCity, BillingState, BillingPostalCode, BillingCountry, Total_Amount_Outstanding__c,
                       Total_Amount_Outstanding_0_Current__c, Total_Amount_Outstanding_1_30_Days__c, Total_Amount_Outstanding_31_60_Days__c, Total_Amount_Outstanding_61_90_Days__c, 
                       Total_Amount_Outstanding_Over_90_Days__c, Total_Amount_Credits__c  from Account where Id = :ApexPages.currentPage().getParameters().get('id')];
     
     
        this.billing = [ Select  AcctSeed__Accounting_Period__c, AcctSeed__Accounting_Year__c, Account_Period_Formula__c, AcctSeed__Age__c, 
                         AcctSeed__Balance__c, AcctSeed__Billing_Cash_Receipt_Count__c, AcctSeed__Billing_City__c, AcctSeed__Billing_Comment__c, AcctSeed__Billing_Contact__c, 
                         AcctSeed__Billing_Country__c, AcctSeed__Billing_Cycle_End_Date__c, AcctSeed__Billing_Cycle_Start_Date__c, AcctSeed__Date__c, AcctSeed__Billing_Format__c, 
                         AcctSeed__Billing_PostalCode__c, AcctSeed__Billing_State__c, AcctSeed__Billing_Street__c, AcctSeed__Billing_Terms_Name__c, AcctSeed__Cash_Application_Adjustment_Amount__c, 
                         Client_Reference_1__c, Client_Reference_2__c, AcctSeed__Closed_Accounting_Period__c, AcctSeed__Credit_Memo_Applied_Amount__c, AcctSeed__Currency_Conversion_Rate__c, 
                         AcctSeed__Customer__c, Customer_Invoice__c, Customer_Invoice__r.Name, AcctSeed__Discount_Percent__c, AcctSeed__Discount_Amount__c, AcctSeed__Discount_Due_Date__c, AcctSeed__Due_Date2__c, Invoice_Type__c, 
                         AcctSeed__Ledger_Amount__c, AcctSeed__Line_Count__c, AcctSeed__Opportunity__c, Override_Accounting_Period__c, AcctSeed__PDF_Email_Status__c, AcctSeed__PO_Number__c, AcctSeed__Status__c, 
                         AcctSeed__Proprietary_Billing_Number__c, AcctSeed__Received_Amount__c, AcctSeed__Recurring_Billing__c, Shipment_Order_Number__c, AcctSeed__Shipping_City__c, AcctSeed__Shipping_Contact__c, 
                         AcctSeed__Shipping_Country__c, AcctSeed__Shipping_PostalCode__c, AcctSeed__Shipping_State__c, AcctSeed__Shipping_Street__c, Ship_to_Country__c, Sum_of_First_Invoices__c, AcctSeed__Total__c, 
                         AcctSeed__Type__c FROM  AcctSeed__Billing__c where AcctSeed__Customer__c = :account.Id AND AcctSeed__Balance__c != 0 AND AcctSeed__Status__c = 'Posted'];
        
        List<Invoice_New__c> invoiceObj = [Select PO_Number__c, PO_Number_Override__c from Invoice_New__c Where Account__c = :account.Id And (PO_Number_Override__c != null OR PO_Number__c != null) LIMIT 1];
        
        if(invoiceObj != null && invoiceObj.size() != 0){
            this.invoice = invoiceObj[0];
        }
        else{
            this.invoice = [Select PO_Number__c, PO_Number_Override__c from Invoice_New__c Where Account__c = :account.Id LIMIT 1];
        }
        
        
      BillingBalance = [Select SUM(AcctSeed__Balance__c) TotalBalance FROM AcctSeed__Billing__c  where AcctSeed__Customer__c = :account.Id Group by AcctSeed__Due_Date2__c  ];
                                                                            
    }
    
    public List<AggregateResult> getBillingBalance () {return BillingBalance ;}

    public PageReference createStatement() {

    PageReference pdf = new PageReference('/apex/CustomStatementNew?id='+ApexPages.currentPage().getParameters().get('id'));

    // create the new attachment
    Attachment attach = new Attachment();

    // the contents of the attachment from the pdf
    Blob body;

    try {

        // returns the output of the page as a PDF
        body = pdf.getContentAsPDF();

        // need to pass unit test -- current bug    
        } catch (VisualforceException e) {
            system.debug('in the catch block');
             body = Blob.valueOf('Some Text');
        }

    attach.Body = body;
    
    attach.Name = account.Name+system.today().format()+'Statement.pdf';
    attach.IsPrivate = false;
    // attach the pdf to the account
    attach.ParentId = ApexPages.currentPage().getParameters().get('id');
    insert attach;

    
   PageReference returnToPage= new PageReference('/'+ApexPages.currentPage().getParameters().get('id'));
    returnToPage.setRedirect(true);
     return returnToPage;
    
    }
}