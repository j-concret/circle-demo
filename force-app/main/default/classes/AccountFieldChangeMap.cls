public class AccountFieldChangeMap extends FieldChangeMap { 
	protected override String getValue(Object value) {
		if (value == null) return null;
		  
		if (Field.References('User'))   
			return [SELECT UserName FROM User WHERE ID = :(ID)value].UserName;
		else if (Field.Name == 'Entity_Type__c') { 
			if (String.valueOf(value) == 'Parent Client') return 'Client';
			else if (String.valueOf(value) == 'Registered Entity') return 'RegisteredEntity';
			else if (String.valueOf(value) == 'Division') return 'Division';  	
			else return null;
		} 
		else
			return super.getValue(value);  
	}	
}