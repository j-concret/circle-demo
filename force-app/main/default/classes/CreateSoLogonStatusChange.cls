public class CreateSoLogonStatusChange {
    public static Boolean isSOLogCreated = false;
    public static Boolean isSOStatusChanged = false;
    public static void createSOLOG(Map<Id,Shipment_Order__c> soMap,String Source){

        Map<String,Shipment_Order__c> SOList = new Map<String,Shipment_Order__c>();
        Map<ID,SO_Travel_History__c> SoTravelMap = new Map<ID,SO_Travel_History__c>();
        Map<String,String> CPAValuesMap = new Map<String,String>();
        String mappedStatus,typeOfUpdate ='';

        //list<Shipment_Order__C> SO2 = [SELECT Id,Shipping_status__c,banner_feed__c,Clearance_Destination__c,Ship_to_Country_new__c,Cost_Estimate_Acceptance_Date__c,Mapped_Shipping_status__c,CPA_v2_0__c,CPA_v2_0__r.Final_Destination__c,CPA_v2_0__r.Tracking_Term__c,Client_Contact_for_this_Shipment__r.Client_Notifications_Choice__c,Client_Contact_for_this_Shipment__r.Major_Minor_notification__c,Client_Contact_for_this_Shipment__r.Client_Tasks_notification_choice__c,Expected_Date_to_Next_Status__c FROM Shipment_Order__c WHERE Id IN:SO];

        for(Shipment_Order__c SO1: soMap.values()) {
            CPAValuesMap.put(SO1.CPA_v2_0__c,SO1.Id);
            //SOList.put(SO1.Id,SO1);

            if(Source == 'SO Status Changed') {
                mappedStatus = 'Approved to Ship';
            }else if(Source == 'FDA Status changed' ) {
                mappedStatus = SO1.Mapped_Shipping_status__c;
            }else{
                mappedStatus = 'Compliance Pending';
            }

            if(Source == 'SO Robots Changed' ) {
                typeOfUpdate = 'MINOR';
            }else if(Source == 'FDA Status changed' ) {
                typeOfUpdate = 'NONE';
            }else{
                typeOfUpdate = 'MAJOR';
            }

            SoTravelMap.put(SO1.ID,
                            new SO_Travel_History__c (
                                Source__c = Source,
                                Date_Time__c =  System.now(),
                                Location__c = '',
                                Mapped_Status__C= mappedStatus,
                                Description__c = 'NONE',
                                Expected_Date_to_Next_Status__c=SO1.Expected_Date_to_Next_Status__c,
                                Client_Notifications_Choice__c =SO1.Client_Contact_for_this_Shipment__r.Client_Notifications_Choice__c != null ? SO1.Client_Contact_for_this_Shipment__r.Client_Notifications_Choice__c : 'Not applicable',
                                Major_Minor_notification__c = SO1.Client_Contact_for_this_Shipment__r.Major_Minor_notification__c != null ? SO1.Client_Contact_for_this_Shipment__r.Major_Minor_notification__c : '',
                                Client_Tasks_notification_choice__c = SO1.Client_Contact_for_this_Shipment__r.Client_Tasks_notification_choice__c,
                                Type_of_Update__c = typeOfUpdate,
                                TecEx_SO__c = SO1.ID,
                                SO_Shipping_Status__c  = SO1.Shipping_Status__c
                                )
                            );

        }

        for(SO_Status_Description__c SOSD : [SELECT Id, Name,Description_ETA__c,Expected_Days__C,Status__c,CPA_v2_0__c FROM SO_Status_Description__c WHERE CPA_v2_0__c IN :CPAValuesMap.keySet() AND Type__c = 'Shipping status']) {
			System.debug('SOSD '+JSON.serializePretty(SOSD));
            Shipment_Order__c shipmentOrder = soMap.get(CPAValuesMap.get(SOSD.CPA_v2_0__c));
            if(SOSD.Status__c == mappedStatus) {
                
                String des = (SOSD.Description_ETA__c).replace('[Ship to Country]',shipmentOrder.Ship_to_Country_new__c).replace('[Clearance Destination]',shipmentOrder.Clearance_Destination__c).replace('[Ship from Country]',shipmentOrder.Ship_From_Country__c );
                SO_Travel_History__c soth = SoTravelMap.get(shipmentOrder.Id);
                soth.Description__c = des;
                if(soth.Source__c == 'SO Creation')  {
                  soth.Expected_Date_to_Next_Status__c = datetime.now().adddays(integer.valueOf(SOSD.Expected_Days__C));
                }
                SoTravelMap.put(shipmentOrder.Id,soth);
                shipmentOrder.Banner_Feed__c = des;
                //SOList.put(CPAValuesMap.get(SOSD.CPA_v2_0__c),shipmentOrder);
            }

        }
        if(SoTravelMap != null && !SoTravelMap.isEmpty()) {
            insert SoTravelMap.values();
        }
        /* if(SOList != null && !SOList.isEmpty()) {
            update SOList.values();
        } */
    }
}