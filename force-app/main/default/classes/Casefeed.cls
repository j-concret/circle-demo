@RestResource(urlMapping='/NCPcasefeedAttachments/*')
Global class Casefeed {
    
     @Httppost
        global static void NCPCasefeed(){
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        Blob body = req.requestBody;
        res.addHeader('Content-Type', 'application/json');
        String requestString = body.toString();
        Casefeedwra rw  = (Casefeedwra)JSON.deserialize(requestString,Casefeedwra.class);
          Try{
              
             Access_token__c At =[select status__c,Access_Token__c from Access_token__c where Access_Token__c=:rw.Accesstoken];
                if( at.status__C =='Active' ){
                   // list<ContentDocument> CDatchs1=new List<ContentDocument>();
                   list<ContentVersion> atchs1=new List<ContentVersion>(); 
                     list<ContentDocumentLink > CDL=new List<ContentDocumentLink >();
                    
                        if(!rw.Atts.isempty()){
                                        list<ContentVersion> atchs=new List<ContentVersion>();
                                        List<Id> CVIDs = new List<ID>();
                                        For(Integer i=0;rw.Atts.size()>i;i++){
                                            
                                            
                                           ContentVersion cv = new ContentVersion();
                                                   cv.ContentLocation = 'S';
                                                   cv.VersionData =rw.Atts[i].filebody;
                                                   cv.NetworkId='0DB1v000000kB98GAE';// acc2-0DB1q00000000ozGAA,Prod - 0DB1v000000kB98GAE
                                                     // cv.VersionData = EncodingUtil.base64Decode(base64Data);
                                                   cv.Title = rw.Atts[i].fileName;
                                                   cv.PathOnClient = rw.Atts[i].fileName;
                                                    atchs.add(CV);
                                                    
                                        }
                                        insert atchs; 
                             atchs1 = [Select ID,ContentDocumentId  from ContentVersion where Id in:atchs ];
                            System.debug('atchs1--->'+atchs1);
                        
           
                
                                   } 
                
                 
               
                    
                    
                    
             res.responseBody = Blob.valueOf(JSON.serializePretty(atchs1));
             res.addHeader('Content-Type', 'application/json');
             res.statusCode = 200;
                

               
              
                API_Log__c Al = New API_Log__c();
                Al.EndpointURLName__c='NCPcasefeedAttachments';
              //  Al.Response__c='case created'+cs.Id;
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al;
              
                }
              
             }
            catch(Exception e){
                
                    String ErrorString =e.getMessage();
                res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
                res.statusCode = 500;
                API_Log__c Al = New API_Log__c();
                Al.EndpointURLName__c='NCPcasefeedAttachments';
                Al.Response__c=e.getMessage()+' at line number: '+e.getLineNumber();
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al;
              
              }
        }
}