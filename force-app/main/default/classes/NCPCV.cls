@RestResource(urlMapping='/NCPAPICountryValidations/*')
Global class NCPCV {
 @Httpget
    global static void NCPCV(){
        
        String jsonData='Test';
        RestRequest req = RestContext.request;
    	RestResponse res = RestContext.response;
        res.addHeader('Content-Type', 'application/json');
       // String ClientID = req.params.get('ClientAccId');
      
         List<Country_Validations__c> CVL = [Select ID,Ship_from_Country__c,EOR_Condition__c,EOR__c,EOR_Fields_to_Consider__c,EOR_Refurbished_Goods__c,EOR_Second_Hand_Goods__c ,IOR_Note__c,EOR_Note__c,Refurbished_Goods__c,Second_Hand_Goods__c,Name,Action_Type__c,Condition__c,Condition_Value__c ,Display_Toaster__c,Field_Validation_has_to_run_on__c,Shipto_Country__c,Toaster_Message__c,Zee_IOR_Note__c from Country_Validations__c];
       try {
         If(!CVL.Isempty()){
             
             JSONGenerator gen = JSON.createGenerator(true);
             gen.writeStartObject();
             
             
          
             //Creating Array & Object - Client Defaults
             gen.writeFieldName('CountyValidations');
             	gen.writeStartArray();
     		         for(Country_Validations__c CPD1 :CVL){
        				//Start of Object - SClient Defaults	
                         gen.writeStartObject();
                          
                          if(CPD1.Id == null) {gen.writeNullField('Id');} else{gen.writeStringField('Id', CPD1.Id);}
                          if(CPD1.Name == null) {gen.writeNullField('Name');} else{gen.writeStringField('Name', CPD1.Name);}
                          if(CPD1.Action_Type__c == null) {gen.writeNullField('Action_Type');} else{gen.writeStringField('Action_Type', CPD1.Action_Type__c);}
                          if(CPD1.Condition__c == null) {gen.writeNullField('Condition');} else{gen.writeStringField('Condition', CPD1.Condition__c);}
                          if(CPD1.Condition_Value__c == null) {gen.writeNullField('Condition_Value');} else{gen.writeStringField('Condition_Value', CPD1.Condition_Value__c);}
                          if(CPD1.Display_Toaster__c == null) {gen.writeNullField('Display_Toaster');} else{gen.writeStringField('Display_Toaster', CPD1.Display_Toaster__c);}
                          if(CPD1.Field_Validation_has_to_run_on__c == null) {gen.writeNullField('Field_Validation_has_to_run_on');} else{gen.writeStringField('Field_Validation_has_to_run_on', CPD1.Field_Validation_has_to_run_on__c);}
                          if(CPD1.Shipto_Country__c == null) {gen.writeNullField('Shipto_Country');} else{gen.writeStringField('Shipto_Country', CPD1.Shipto_Country__c);}
                          if(CPD1.Toaster_Message__c == null) {gen.writeNullField('Toaster_Message');} else{gen.writeStringField('Toaster_Message', CPD1.Toaster_Message__c);}
                          if(CPD1.IOR_Note__c == null) {gen.writeNullField('IORNote');} else{gen.writeStringField('IORNote', CPD1.IOR_Note__c);}
                          if(CPD1.EOR_Note__c == null) {gen.writeNullField('EORNote');} else{gen.writeStringField('EORNote', CPD1.EOR_Note__c);}
                          if(CPD1.Refurbished_Goods__c == null) {gen.writeNullField('RefurbishedGoods');} else{gen.writeStringField('RefurbishedGoods', CPD1.Refurbished_Goods__c);}
                          if(CPD1.Second_Hand_Goods__c == null) {gen.writeNullField('SecondHandGoods');} else{gen.writeStringField('SecondHandGoods', CPD1.Second_Hand_Goods__c);}
                          if(CPD1.Ship_from_Country__c == null) {gen.writeNullField('ShipfromCountry');} else{gen.writeStringField('ShipfromCountry', CPD1.Ship_from_Country__c);}
                          if(CPD1.EOR_Condition__c == null) {gen.writeNullField('EORCondition');} else{gen.writeStringField('EORCondition', CPD1.EOR_Condition__c);}
                          if(CPD1.EOR__c == null) {gen.writeNullField('EOR');} else{gen.writeStringField('EOR', CPD1.EOR__c);}
                          if(CPD1.EOR_Fields_to_Consider__c == null) {gen.writeNullField('EORFieldstoConsider');} else{gen.writeStringField('EORFieldstoConsider', CPD1.EOR_Fields_to_Consider__c);}
                          if(CPD1.EOR_Refurbished_Goods__c == null) {gen.writeNullField('EORRefurbishedGoods');} else{gen.writeStringField('EORRefurbishedGoods', CPD1.EOR_Refurbished_Goods__c);}
                          if(CPD1.EOR_Second_Hand_Goods__c == null) {gen.writeNullField('EORSecondHandGoods');} else{gen.writeStringField('EORSecondHandsGoods', CPD1.EOR_Second_Hand_Goods__c);}
                          if(CPD1.Zee_IOR_Note__c == null) {gen.writeNullField('ZeeIORNote');} else{gen.writeStringField('ZeeIORNote', CPD1.Zee_IOR_Note__c);}
                         
                         gen.writeEndObject();
                         //End of Object - Client Defaults
    					}
    		gen.writeEndArray();
                //End of Array - Client Defaults 
           
             
              
             
             gen.writeEndObject();
    		jsonData = gen.getAsString();
             
             
              res.responseBody = Blob.valueOf(jsonData);
              res.addHeader('Content-Type', 'application/json');
              res.statusCode = 200;
                
                API_Log__c Al = New API_Log__c();
             //   Al.Account__c=clientID;
                Al.EndpointURLName__c='NCPAPICountryvalidations';
                Al.Response__c='Success - NCPAPICountryvalidations are sent';
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al;
             
             
                  }
         
         
            }
        
        catch(exception e){
            System.debug('Line number ====> '+e.getLineNumber() + 'Exception Message =====> '+e.getMessage());
            
           		  String ErrorString ='NCPAPICountryvalidations are not found';
               	  res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
             res.addHeader('Content-Type', 'application/json');
            	res.statusCode = 500;
                API_Log__c Al = New API_Log__c();
               // Al.Account__c=ClientID;
                Al.EndpointURLName__c='NCPAPICountryvalidations';
                Al.Response__c=e.getMessage()+' at line number: '+e.getLineNumber();
                Al.StatusCode__c=string.valueof(res.statusCode);
              	Insert Al;
            
            
        }
        
        
		}

}