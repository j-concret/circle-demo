public class NCPRolloutWrapper {
    public String AccountID;
	public String ContactID;
	public Double Shipment_Value_USD;
	public Double Chargable_Weight;
	public String Courier_responsibility;
    public String ServiceType;
	public String Reference1;
	public String Reference2;
	public String ShipFrom;
	public String ShipTo;
    public Integer NumberOfFinaldeliveries;
    public String Type_of_Goods;
    public String Li_ion_Batteries;
    public String Li_ion_BatteryTypes;
    public String Accesstoken;
    public String BuyerID;
    
    
    
	public List<ShipmentOrder_Packages> ShipmentOrder_Packages;

	public class ShipmentOrder_Packages {
		public String Weight_Unit;
       	public String Dimension_Unit;
		public Integer Packages_of_Same_Weight;
		public Double Length;
		public Double Height;
		public Double Breadth;
		public Double Actual_Weight;
		
	}

}