@IsTest
public class NCPQuoteFlowModels_Test {

    @Testsetup
    static void setupData(){

          DefaultFreightValues__c df = new DefaultFreightValues__c();
        df.Name = 'Test';
        df.Courier_Base_Rate_KG__c = 12;
        df.Courier_Dangerous_Goods_premium__c = 12;
        df.Courier_Fixed_Charge__c = 12;
        df.Courier_Non_stackable_premium__c = 12;
        df.Courier_Oversized_premium__c = 12;
        df.FF_Base_Rate_KG__c = 12;
        df.FF_Dangerous_Goods_Fixed_Charge_Premium__c = 12;
        df.FF_Dangerous_Goods_premium__c = 12;
        df.FF_Dedicated_Pickup__c = 12;
        df.FF_Fixed_Charge__c = 12;
        df.FF_Non_stackable_Fixed_Charge_Premium__c = 12;
        df.FF_Non_stackable_premium__c = 12;
        df.FF_Oversized_Fixed_Charge_Premium__c = 12;
        df.FF_Oversized_premium__c = 12;
        insert df;
        
        CountryandRegionMap__c cr = new CountryandRegionMap__c(Name ='Brazil', Country__c='Brazil', Region__c='South', European_Union__c='No');
        insert cr;
        CountryandRegionMap__c cr1 = new CountryandRegionMap__c(Name ='United States', Country__c='United States', Region__c='North America', European_Union__c='No');
        insert cr1;
        RegionalFreightValues__c rf = new RegionalFreightValues__c(
            name='North America to South', Origin__c='Brazil', 
            Preferred_Freight_Forwarder_Name__c='Stream', 
            Destination__c='Brazil', 
            Courier_Discount_Premium__c=10, 
            FF_Regional_Discount_Premium__c=10, 
            Courier_Fixed_Premium__c=10, 
            FF_Fixed_Premium__c=10, 
            Risk_Adjustment_Factor__c=1.41
        );
        insert rf;
        
          
Account account = new Account (name='TecEx Prospective Client', Type ='Client',
                                       CSE_IOR__c = '0050Y000001LTZO',
                                       Service_Manager__c = '0050Y000001LTZO',
                                       Financial_Manager__c='0050Y000001LTZO',
                                       Financial_Controller__c='0050Y000001LTZO',
                                       Tax_recovery_client__c  = FALSE,
                                       Ship_From_Line_1__c = 'Ship_From_Line_1',
                                       Ship_From_Line_2__c = 'Ship_From_Line_2',
                                       Ship_From_Line_3__c = 'Ship_From_Line_3',
                                       Ship_From_City__c = 'Ship_From_City',
                                       Ship_From_Zip__c = 'Ship_From_Zip',
                                       Ship_From_Country__c = 'Ship_From_Country',
                                       Ship_From_Other_notes__c = 'Ship_From_Other',
                                       Ship_From_Contact_Name__c = 'Ship_From_Contact',
                                       Ship_From_Contact_Email__c = 'Ship_From_Email',
                                       Ship_From_Contact_Tel__c = 'Ship_From_Tel',
                                       Client_Address_Line_1__c = 'Client_Address_Line_1',
                                       Client_Address_Line_2__c = 'Client_Address_Line_2',
                                       Client_Address_Line_3__c = 'Client_Address_Line_3',
                                       Client_City__c = 'Client_City',
                                       Client_Zip__c = 'Client_Zip',
                                       Client_Country__c = 'Client_Country',
                                       Other_notes__c = 'Other_notes',
                                       Tax_Name__c = 'Tax_Name',
                                       Tax_ID__c = 'Tax_ID',
                                       Contact_Name__c = 'Contact_Name',
                                       Contact_Email__c = 'Contact_Email',
                                       Contact_Tel__c = 'Contact_Tel'
                                       );
        insert account;

        Account account2 = new Account (name='Test Supplier', Type ='Supplier', ICE__c = '0050Y000001km5c', RecordTypeId = '0120Y0000006KjQQAU');
        insert account2;

        Account account3 = new Account (name='Test Manufacturer', Type ='Manufacturer', RecordTypeId = '0120Y0000002wa0QAA', Manufacturer_Alias__c = 'Tester,Who' ); insert account3;

        Account account4 = new Account (name='ACME', Type ='Manufacturer', RecordTypeId = '0120Y0000002wa0QAA', Manufacturer_Alias__c = 'Buggs,Bunny' ); insert account4;


        Contact contact = new Contact (Client_Notifications_Choice__c = 'opt-in', lastname ='Testing Individual',  AccountId = account.Id);
        insert contact;

        Contact contact2 = new Contact (Client_Notifications_Choice__c = 'opt-in', lastname ='Testing supplier',  AccountId = account2.Id);
        insert contact2;

        IOR_Price_List__c iorpl = new IOR_Price_List__c ( Client_Name__c = account.Id, Name = 'Brazil', IOR_Fee__c = 0.15, On_Charge_Mark_up__c = 0.15,
                                                          TecEx_Shipping_Fee_Markup__c =0.40, IOR_Min_Fee__c = 100,  Admin_Fee__c =100, Set_up_fee__c =100, Bank_Fees__c =200, Tax_Rate__c = 0.20,
                                                          Estimated_Customs_Brokerage__c =250, Estimated_Customs_Clearance__c =250, Estimated_Customs_Handling__c =250, Estimated_Import_License__c =250, Destination__c = 'Brazil' );
        insert iorpl;

        country_price_approval__c cpa = new country_price_approval__c( name ='Brazil', Billing_term__c = 'DAP/CIF - IOR pays',  Airwaybill_Instructions__c = 'User IOR Address', Supplier__c = account2.Id,
                                                                       In_Country_Specialist__c = '0050Y000001km5c', Clearance_Destination__c = 'Brazil', Preferred_Supplier__c = TRUE, Destination__c = 'Brazil' );
        insert cpa;

        Currency_Management2__c CM = new Currency_Management2__c (Name = 'US Dollar (USD)', Currency__c = 'US Dollar (USD)', ISO_Code__c = 'USD', Conversion_Rate__c = 1);
        insert CM;

        Country__c country = new Country__c(Brokerage_Costs__c = 618, CIF_Adjustment_Factor__c = 0.035, CIF_Absolute_value_adjustment__c = 200, Clearance_Costs__c = 488,
                                            Country__c = 'Brazil', Name = 'Brazil', Handling_Costs__c = 1180, License_Permit_Costs__c = 0);
        insert country;

        Tax_Structure_Per_Country__c tax = new Tax_Structure_Per_Country__c(Applied_to_Value__c = 'CIF', Country__c = country.Id, Default_Duty_Rate__c = 0.5625,
                                                                            Order_Number__c = '1', Part_Specific__c = TRUE, Name = 'II', Tax_Type__c = 'Duties');
        insert tax;

        CPA_v2_0__c RelatedCPA = new CPA_v2_0__c(Name = 'Brazil', Country_Applied_Costings__c=true );
        insert RelatedCPA;
        CPA_v2_0__c cpav2 = new CPA_v2_0__c(Name = 'Brazil Testing 2.0', Supplier__c = account2.Id, Service_Type__c = 'IOR/EOR', Related_Costing_CPA__c = RelatedCPA.Id, Preferred_Supplier__c = TRUE,
                                            VAT_Rate__c = 0.1, Destination__c = 'Brazil',

                                            Default_Section_1_Address_Line_1__c= 'Address_Line_1',
                                            Default_Section_1_Address_Line_2__c= 'Address_Line_2',
                                            Default_Section_1_Address_Line_3__c= 'Address_Line_3',
                                            Default_Section_1_City__c= 'City',
                                            Default_Section_1_Zip__c= 'Zip',
                                            Default_Section_1_Country__c = 'Country',
                                            Default_Section_1_Other__c= 'Other',
                                            Default_Section_1_Tax_Name__c= 'Tax_Name',
                                            Default_Section_1_Tax_Id__c= 'Tax_Id',
                                            Default_Section_1_Contact_Name__c= 'Contact_Name',
                                            Default_Section_1_Contact_Email__c= 'Contact_Email',
                                            Default_Section_1_Contact_Tel__c= 'Contact_Tel',
                                            Alternate_Section_1_Address_Line_1__c= 'Address_Line_1',
                                            Alternate_Section_1_Address_Line_2__c= 'Address_Line_2',
                                            Alternate_Section_1_Address_Line_3__c= 'Address_Line_3',
                                            Alternate_Section_1_City__c= 'City',
                                            Alternate_Section_1_Zip__c= 'Zip',
                                            Alternate_Section_1_Country__c = 'Country',
                                            Alternate_Section_1_Other__c = 'Other',
                                            Alternate_Section_1_Tax_Name__c = 'Tax_Name',
                                            Alternate_Section_1_Tax_Id__c = 'Tax_Id',
                                            Alternate_Section_1_Contact_Name__c= 'Contact_Name',
                                            Alternate_Section_1_Contact_Email__c= 'Contact_Email',
                                            Alternate_Section_1_Contact_Tel__c= 'Contact_Tel',
                                            Default_Section_2_Address_Line_1__c= 'Address_Line_1',
                                            Default_Section_2_Address_Line_2__c= 'Address_Line_2',
                                            Default_Section_2_Address_Line_3__c= 'Address_Line_3',
                                            Default_Section_2_City__c= 'City',
                                            Default_Section_2_Zip__c= 'Zip',
                                            Default_Section_2_Country__c = 'Country',
                                            Default_Section_2_Other__c= 'Other',
                                            Default_Section_2_Tax_Name__c= 'Tax_Name',
                                            Default_Section_2_Tax_Id__c= 'Tax_Id',
                                            Default_Section_2_Contact_Name__c= 'Contact_Name',
                                            Default_Section_2_Contact_Email__c= 'Contact_Email',
                                            Default_Section_2_Contact_Tel__c= 'Contact_Tel',
                                            Alternate_Section_2_Address_Line_1__c= 'Address_Line_1',
                                            Alternate_Section_2_Address_Line_2__c= 'Address_Line_2',
                                            Alternate_Section_2_Address_Line_3__c= 'Address_Line_3',
                                            Alternate_Section_2_City__c= 'City',
                                            Alternate_Section_2_Zip__c= 'Zip',
                                            Alternate_Section_2_Country__c = 'Country',
                                            Alternate_Section_2_Other__c = 'Other',
                                            Alternate_Section_2_Tax_Name__c = 'Tax_Name',
                                            Alternate_Section_2_Tax_Id__c = 'Tax_Id',
                                            Alternate_Section_2_Contact_Name__c= 'Contact_Name',
                                            Alternate_Section_2_Contact_Email__c= 'Contact_Email',
                                            Alternate_Section_2_Contact_Tel__c= 'Contact_Tel',
                                            Default_Section_3_Address_Line_1__c= 'Address_Line_1',
                                            Default_Section_3_Address_Line_2__c= 'Address_Line_2',
                                            Default_Section_3_Address_Line_3__c= 'Address_Line_3',
                                            Default_Section_3_City__c= 'City',
                                            Default_Section_3_Zip__c= 'Zip',
                                            Default_Section_3_Country__c = 'Country',
                                            Default_Section_3_Other__c= 'Other',
                                            Default_Section_3_Tax_Name__c= 'Tax_Name',
                                            Default_Section_3_Tax_Id__c= 'Tax_Id',
                                            Default_Section_3_Contact_Name__c= 'Contact_Name',
                                            Default_Section_3_Contact_Email__c= 'Contact_Email',
                                            Default_Section_3_Contact_Tel__c= 'Contact_Tel',
                                            Alternate_Section_3_Address_Line_1__c= 'Address_Line_1',
                                            Alternate_Section_3_Address_Line_2__c= 'Address_Line_2',
                                            Alternate_Section_3_Address_Line_3__c= 'Address_Line_3',
                                            Alternate_Section_3_City__c= 'City',
                                            Alternate_Section_3_Zip__c= 'Zip',
                                            Alternate_Section_3_Country__c = 'Country',
                                            Alternate_Section_3_Other__c = 'Other',
                                            Alternate_Section_3_Tax_Name__c = 'Tax_Name',
                                            Alternate_Section_3_Tax_Id__c = 'Tax_Id',
                                            Alternate_Section_3_Contact_Name__c= 'Contact_Name',
                                            Alternate_Section_3_Contact_Email__c= 'Contact_Email',
                                            Alternate_Section_3_Contact_Tel__c= 'Contact_Tel',
                                            Default_Section_4_Address_Line_1__c= 'Address_Line_1',
                                            Default_Section_4_Address_Line_2__c= 'Address_Line_2',
                                            Default_Section_4_Address_Line_3__c= 'Address_Line_3',
                                            Default_Section_4_City__c= 'City',
                                            Default_Section_4_Zip__c= 'Zip',
                                            Default_Section_4_Country__c = 'Country',
                                            Default_Section_4_Other__c= 'Other',
                                            Default_Section_4_Tax_Name__c= 'Tax_Name',
                                            Default_Section_4_Tax_Id__c= 'Tax_Id',
                                            Default_Section_4_Contact_Name__c= 'Contact_Name',
                                            Default_Section_4_Contact_Email__c= 'Contact_Email',
                                            Default_Section_4_Contact_Tel__c= 'Contact_Tel',
                                            Alternate_Section_4_Address_Line_1__c= 'Address_Line_1',
                                            Alternate_Section_4_Address_Line_2__c= 'Address_Line_2',
                                            Alternate_Section_4_Address_Line_3__c= 'Address_Line_3',
                                            Alternate_Section_4_City__c= 'City',
                                            Alternate_Section_4_Zip__c= 'Zip',
                                            Alternate_Section_4_Country__c = 'Country',
                                            Alternate_Section_4_Other__c = 'Other',
                                            Alternate_Section_4_Tax_Name__c = 'Tax_Name',
                                            Alternate_Section_4_Tax_Id__c = 'Tax_Id',
                                            Alternate_Section_4_Contact_Name__c= 'Contact_Name',
                                            Alternate_Section_4_Contact_Email__c= 'Contact_Email',
                                            Alternate_Section_4_Contact_Tel__c= 'Contact_Tel',
                                            VAT_Reclaim_Destination__c = FALSE,
                                            Final_Destination__c ='Brazil',
                                            Country__c = country.id

                                            );
        insert cpav2;

        List<CPARules__c> CPARules = new List<CPARules__c>();
        CPARules.add(new CPARules__c(Country__c = 'Brazil',Criteria__c ='Default', Chargable_weight__c = 'NONE', Courier_Responsibility__c = 'NONE',   ECCN_No__c = 'NONE', NL_Product_User__c = 'NONE', Number_of_final_deliveries__c = 'NONE', ShipmentValue__c ='NONE', Sum_of_quantity_of_matched_products__c = 'NONE', Manufacturer__c = 'NONE', DefaultCPA2__c = cpav2.Id, CPA2__c =  cpav2.Id));
        insert CPARules;

        List<CPA_Costing__c> costs = new List<CPA_Costing__c>();
        costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'Shipment value', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'IOR', Min__c = 375, Max__c = 14000, Conditional_value__c = 'Shipment value', Condition__c = 'Between', Floor__c = 0, Ceiling__c = 10000, Currency__c = 'US Dollar (USD)'));
        costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'Anil', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'IOR', Currency__c = 'US Dollar (USD)',Variable_threshold__c = null));
        insert costs;


        Product2 product = new Product2(Name = 'CAB-ETH-S-RJ45', US_HTS_Code__c='85444210',description='Anil', ProductCode = 'CAB-ETH-S-RJ45', Manufacturer__c = account3.Id); insert product;
        Product2 product2 = new Product2(Name = 'CAB-ETH-S-RJ46',US_HTS_Code__c='85444210', ProductCode = 'CAB-ETH-S-RJ46', Manufacturer__c = account3.Id); insert product2;
        Product2 product3 = new Product2(Name = 'CAB-ETH-S', US_HTS_Code__c='85444210',ProductCode = 'CAB-ETH-S', Manufacturer__c = account3.Id); insert product3;




        Shipment_Order__c shipmentOrder = new Shipment_Order__c(Account__c = account.id, Shipment_Value_USD__c = 2000, CPA_v2_0__c = cpav2.Id,
                                                                Client_Contact_for_this_Shipment__c = contact.Id, Who_arranges_International_courier__c ='Client', Tax_Treatment__c ='DAP/CIF - IOR pays',
                                                                Ship_to_Country__c = cpa.Id, Destination__c = 'Brazil', Service_Type__c = 'IOR', IOR_Price_List__c = iorpl.Id, Chargeable_Weight__c = 200,
                                                                Shipping_Status__c = 'Cost Estimate Abandoned' );
        insert shipmentOrder;


        List<Part__c> partList = new List<Part__c>();

        partList.add(new Part__c(Name ='CAB-ETH-S-RJ45',  Quantity__c= 1, Product__c=product.Id, Commercial_Value__c = 15000, US_HTS_Code__c = '85444210',
                                 Country_of_Origin2__c = 'China', Shipment_Order__c = shipmentOrder.Id, Description_and_Functionality__c ='Anil'));

        partList.add(new Part__c(Name ='AsRdy',  Quantity__c= 1,  Product__c=product.Id, Commercial_Value__c = 15000, US_HTS_Code__c = '85444210',
                                 Country_of_Origin2__c = 'China', Shipment_Order__c = shipmentOrder.Id, Description_and_Functionality__c ='Anil AsRdy'));

        partList.add(new Part__c(Name ='CAB-ETH-S-RJ46',  Quantity__c= 1, Commercial_Value__c = 15000, US_HTS_Code__c = '85478123',
                                 Country_of_Origin2__c = 'China', Shipment_Order__c = shipmentOrder.Id, Description_and_Functionality__c ='AB-ETH-S-RJ46'));

        partList.add(new Part__c(Name ='CAB-ETH-S-RJ46',  Quantity__c= 1, Commercial_Value__c = 15000, US_HTS_Code__c = '834512879',
                                 Country_of_Origin2__c = 'China', Shipment_Order__c = shipmentOrder.Id, Description_and_Functionality__c ='AB-ETH-S-RJ46'));

        partList.add(new Part__c(Name ='Testing',  Quantity__c= 1, Commercial_Value__c = 15000, US_HTS_Code__c = '834512879',
                                 Country_of_Origin2__c = 'China', Shipment_Order__c = shipmentOrder.Id, Description_and_Functionality__c ='Testing'));


        partList.add(new Part__c(Name ='Who CAB-ETH-S',  Quantity__c= 1, Commercial_Value__c = 15000, US_HTS_Code__c = '8471254784',
                                 Country_of_Origin2__c = 'China', Shipment_Order__c = shipmentOrder.Id, Description_and_Functionality__c ='Testing'));

        insert partList;


    }
    
    static testMethod void testNCPQuoteFlowModels(){
        Access_Token__c att = new Access_Token__c(
            AccessToken_encoded__c='zmM0MDcxOTItZTJiZi00YWRhLTlkZGQtZGZhMmUxM2FiYTlj',
            Access_Token__c='cc407192-e2bf-4ada-9ddd-dfa2e13aba9c',
            Status__c='Active'
            );
        insert att;
        
        Master_task__c masterTask =  new Master_Task__c(Name = 'Demo Task', States__c = 'TecEx Pending;Resolved',
                                                        Operational__c = true, Condition__c = '{0} > 5000',Client_Visible__c=TRUE,Displayed_at__c='Quote Phase',
                                                        Condition_Fields_API_Name__c = 'Shipment_Order__c.Shipment_Value_USD__c,Country__c',
                                                        Assess_Condition_on_Creation_of__c = 'Shipment_Order__c',TecEx_Assigned_To__c = 'Compliance');
        insert masterTask;

        Shipment_Order__c shipmentOrder = [SELECT Account__c, Shipment_Value_USD__c, CPA_v2_0__c,
                                           Client_Contact_for_this_Shipment__c, Who_arranges_International_courier__c, Tax_Treatment__c,
                                           Ship_to_Country__c, Destination__c, Service_Type__c, IOR_Price_List__c, Chargeable_Weight__c,
                                           Shipping_Status__c FROM Shipment_Order__c LIMIT 1];

        Master_Task_Template__c mstTaskTemp = new Master_Task_Template__c(Displayed_to__c = 'TecEx',
                                                                          Master_Task__c = masterTask.Id,
                                                                          State__c = 'Resolved',Task_Title__c = 'Demo',
                                                                          Task_Description__c = 'Testing {0} {1}',
                                                                          Task_Description_Fields__c='Shipment_Order__c.Name,Task.Subject',
                                                                          Complete_Button_Text__c = 'Submit',Complete_Button_Action__c = 'Yes',
                                                                          Complete_Button_Target_Object_API_Name__c = 'Task',
                                                                          Complete_Button_Target_Field_API_Name__c = 'State__c',
                                                                          Complete_Button_Target_Field_Set_Value__c = 'Resolved',
                                                                          Description__c = 'Testing {0} {1}',
                                                                          Description_Fields__c = 'Shipment_Order__c.Name,Task.Subject',
                                                                          Building_Block_Type__c = 'Instruction');
        insert mstTaskTemp;

         Master_Task_Prerequisite__c prequisite = new Master_Task_Prerequisite__c(
            Master_Task_Dependent__c = masterTask.Id,
            Master_Task_Prerequisite__c = masterTask.Id
        );
        insert prequisite;
        
        
        List<Task> childList = new List<Task>();
        Task tsk = new Task(status = 'In Progress',priority = 'Normal', WhatId = shipmentOrder.Id, Master_Task__c = masterTask.Id,Auto_Resolved__c = true,Subject = 'Demo Task');
        Task tsk1 = new Task(status = 'In Progress',priority = 'Normal', WhatId = shipmentOrder.Id, Master_Task__c = masterTask.Id, IsNotApplicable__c = false,Subject = 'Demo Task');
        childList.add(tsk);
        childList.add(tsk1);

        insert childList;
        
        FeedItem post = new FeedItem();
        post.Body = 'HelloThere';
        post.ParentId = tsk.Id;
        post.Title = 'FileName';
        post.visibility = 'AllUsers';
        insert post;
        
         FeedItem post1 = new FeedItem();
        post1.Body = 'HelloThere';
        post1.ParentId = tsk.Id;
        post1.Title = 'FileName';
        post1.visibility = 'AllUsers';
        insert post1;
        
        ContentVersion cv = new ContentVersion();
        cv.Title = 'Test Document';
        cv.PathOnClient = 'TestDocument.pdf';
        cv.VersionData = Blob.valueOf('Test Content');
        cv.IsMajorVersion = true;
        
        Insert cv;
        
        FeedAttachment fda = new FeedAttachment(type='Content',FeedEntityId = post.id, recordId=cv.Id);
        insert fda;
        
       
        NCPQuoteFlowModelsWra wrapper = new NCPQuoteFlowModelsWra();
        wrapper.AccessToken = att.Access_Token__c;
        wrapper.AccountID = shipmentOrder.Account__c;
        wrapper.SOID = shipmentOrder.Id;
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/NCPQuoteFlowModels'; 
        req.requestBody = Blob.valueOf(JSON.serialize(wrapper));
        req.httpMethod = 'POST';
        req.addHeader('Content-Type', 'application/json');
        
        RestContext.request = req;
        RestContext.response = res;
        
         Test.startTest();
         NCPQuoteFlowModels.NCPQuoteFlowModels();
         Test.stopTest();

    }
    
      static testMethod void testNCPQuoteFlowModels1(){
        Access_Token__c att = new Access_Token__c(
            AccessToken_encoded__c='zmM0MDcxOTItZTJiZi00YWRhLTlkZGQtZGZhMmUxM2FiYTlj',
            Access_Token__c='cc407192-e2bf-4ada-9ddd-dfa2e13aba9c',
            Status__c='Active'
            );
        insert att;
        
        Master_task__c masterTask =  new Master_Task__c(Name = 'Demo Task', States__c = 'TecEx Pending;Resolved',
                                                        Operational__c = true, Condition__c = '{0} > 5000',Client_Visible__c=TRUE,Displayed_at__c='Quote Phase',
                                                        Condition_Fields_API_Name__c = 'Shipment_Order__c.Shipment_Value_USD__c,Country__c',
                                                        Assess_Condition_on_Creation_of__c = 'Shipment_Order__c',TecEx_Assigned_To__c = 'Compliance');
        insert masterTask;

        Shipment_Order__c shipmentOrder = [SELECT Account__c, Shipment_Value_USD__c, CPA_v2_0__c,
                                           Client_Contact_for_this_Shipment__c, Who_arranges_International_courier__c, Tax_Treatment__c,
                                           Ship_to_Country__c, Destination__c, Service_Type__c, IOR_Price_List__c, Chargeable_Weight__c,
                                           Shipping_Status__c FROM Shipment_Order__c LIMIT 1];

        Master_Task_Template__c mstTaskTemp = new Master_Task_Template__c(Displayed_to__c = 'TecEx',
                                                                          Master_Task__c = masterTask.Id,
                                                                          State__c = 'Resolved',Task_Title__c = 'Demo',
                                                                          Task_Description__c = 'Testing {0} {1}',
                                                                          Task_Description_Fields__c='Shipment_Order__c.Name,Task.Subject',
                                                                          Complete_Button_Text__c = 'Submit',Complete_Button_Action__c = 'Yes',
                                                                          Complete_Button_Target_Object_API_Name__c = 'Task',
                                                                          Complete_Button_Target_Field_API_Name__c = 'State__c',
                                                                          Complete_Button_Target_Field_Set_Value__c = 'Resolved',
                                                                          Description__c = 'Testing {0} {1}',
                                                                          Description_Fields__c = 'Shipment_Order__c.Name,Task.Subject',
                                                                          Building_Block_Type__c = 'Instruction');
        insert mstTaskTemp;

         Master_Task_Prerequisite__c prequisite = new Master_Task_Prerequisite__c(
            Master_Task_Dependent__c = masterTask.Id,
            Master_Task_Prerequisite__c = masterTask.Id
        );
        insert prequisite;
        
        
        List<Task> childList = new List<Task>();
        Task tsk = new Task(status = 'In Progress',priority = 'Normal', WhatId = shipmentOrder.Id, Master_Task__c = masterTask.Id,Auto_Resolved__c = true,Subject = 'Demo Task');
        Task tsk1 = new Task(status = 'In Progress',priority = 'Normal', WhatId = shipmentOrder.Id, Master_Task__c = masterTask.Id, IsNotApplicable__c = false,Subject = 'Demo Task');
        childList.add(tsk);
        childList.add(tsk1);

        insert childList;
        
        FeedItem post = new FeedItem();
        post.Body = 'HelloThere';
        post.ParentId = tsk.Id;
        post.Title = 'FileName';
        post.visibility = 'AllUsers';
        insert post;
        
         FeedItem post1 = new FeedItem();
        post1.Body = 'HelloThere';
        post1.ParentId = tsk.Id;
        post1.Title = 'FileName';
        post1.visibility = 'AllUsers';
        insert post1;
        
        ContentVersion cv = new ContentVersion();
        cv.Title = 'Test Document';
        cv.PathOnClient = 'TestDocument.pdf';
        cv.VersionData = Blob.valueOf('Test Content');
        cv.IsMajorVersion = true;
        
        Insert cv;
        
        FeedAttachment fda = new FeedAttachment(type='Content',FeedEntityId = post.id, recordId=cv.Id);
        insert fda;
        
       
        NCPQuoteFlowModelsWra wrapper = new NCPQuoteFlowModelsWra();
        wrapper.AccessToken = att.Access_Token__c;
        wrapper.AccountID = shipmentOrder.Account__c;
        wrapper.SOID = 'demo';
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/NCPQuoteFlowModels'; 
        req.requestBody = Blob.valueOf(JSON.serialize(wrapper));
        req.httpMethod = 'POST';
        req.addHeader('Content-Type', 'application/json');
        
        RestContext.request = req;
        RestContext.response = res;
        
         Test.startTest();
         NCPQuoteFlowModels.NCPQuoteFlowModels();
         Test.stopTest();

    }
}