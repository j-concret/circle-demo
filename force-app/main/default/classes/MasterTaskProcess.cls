public class MasterTaskProcess {

    private static String masterTaskFilter = 'TecEx';

    public static void taskCreation(List<Id> ids){

        if(ids == null || ids.isEmpty()) return;

        Schema.sObjectType objType = ids[0].getSObjectType();
        String objName = objType.getDescribe().getName();
        List<Task> tasks = new List<Task>();
        List<Id> cntIds = new List<Id>();
        List<Master_Task__c> masterTasks = new List<Master_Task__c>();
        List<SObject> SObjectRecords = new List<SObject>();
        Map<String,Task> existingTasks = new Map<String,Task>();
        Map<Id,List<Sobject> > shipmentParts = new Map<Id,List<Sobject> >();
        Map<Id,List<Sobject> > shipmentPackages = new Map<Id,List<Sobject> >();
        Map<Id,List<Sobject> > finalDeliveries = new Map<Id,List<Sobject> >();

        Integer count= Database.countQuery('SELECT count() FROM  Master_Task__c WHERE Operational__c = true AND Assess_Condition_on_Creation_of__c=:objName');
        if(count == 0) return;

        for(Task extTask : [SELECT Id,Auto_Resolved__c,Description,Assigned_to__c,Binding_Quote_Exempted_V2__c,LastModifiedById,CreatedById,Client_Assigned_To__c,Client_Assigned_To__r.Name,TecEx_Assigned_To__c,TecEx_Pending_ETA_days__c,Under_Review_ETA_days__c,Task_Category__c,Shipment_Order__c,Shipment_Order__r.Name,Shipment_Order__r.Account__c,Shipment_Order__r.Account__r.Name,State__c,WhatId,Master_Task__c,Master_Task__r.Name,Master_Task__r.Set_Default_to_Inactive__c, Subject,Inactive__c,Blocks_Approval_to_Ship__c,CreatedDate,CreatedBy.Name, LastModifiedDate,LastModifiedBy.Name,Reason_Not_Applicable__c,IsNotApplicable__c FROM Task WHERE Master_Task__c != null AND WhatId IN:ids ]) {
            existingTasks.put(extTask.Master_Task__c+''+extTask.whatId,extTask);
        }

        Map<Id,Freight__c> freightReqs = new Map<Id,Freight__c>();
        Map<String,String> relatedObjects = new Map<String,String> {'Part__c'=>'Parts__r','Shipment_Order_Package__c'=>'Shipment_Order_Packages__r'};

        String query = 'SELECT '+String.join(new List<String>(objType.getDescribe().fields.getMap().keySet()),',');

        if(objName.equalsIgnoreCase('Shipment_Order__c')) {

            query += ',Account__r.'+String.join(new List<String>(Account.sObjectType.getDescribe().fields.getMap().keySet()), ',Account__r.');
            query += ',CPA_v2_0__r.'+String.join(new List<String>(CPA_v2_0__c.sObjectType.getDescribe().fields.getMap().keySet()), ',CPA_v2_0__r.');

            query += ',CPA_v2_0__r.Country__r.'+String.join(new List<String>(Country__c.sObjectType.getDescribe().fields.getMap().keySet()), ',CPA_v2_0__r.Country__r.');

            for(Part__c prt : Database.query('SELECT '+String.join(new List<String>(Part__c.sObjectType.getDescribe().fields.getMap().keySet()), ',')+' FROM Part__c WHERE Shipment_Order__c IN:ids')) {
                if(shipmentParts.containsKey(prt.Shipment_Order__c))
                    shipmentParts.get(prt.Shipment_Order__c).add(prt);
                else
                    shipmentParts.put(prt.Shipment_Order__c,new List<SObject> {prt});
            }
            for(Shipment_Order_Package__c pck : Database.query('SELECT '+String.join(new List<String>(Shipment_Order_Package__c.sObjectType.getDescribe().fields.getMap().keySet()), ',')+' FROM Shipment_Order_Package__c WHERE Shipment_Order__c IN:ids')) {
                if(shipmentPackages.containsKey(pck.Shipment_Order__c))
                    shipmentPackages.get(pck.Shipment_Order__c).add(pck);
                else
                    shipmentPackages.put(pck.Shipment_Order__c,new List<SObject> {pck});
            }
            for(Final_Delivery__c finalDel : Database.query('SELECT '+String.join(new List<String>(Final_Delivery__c.sObjectType.getDescribe().fields.getMap().keySet()), ',')+' FROM Final_Delivery__c WHERE Shipment_Order__c IN:ids')){
                if(finalDeliveries.containsKey(finalDel.Shipment_Order__c))
                    finalDeliveries.get(finalDel.Shipment_Order__c).add(finalDel);
                else
                    finalDeliveries.put(finalDel.Shipment_Order__c, new List<SObject> {finalDel});
            }

            for(Freight__c freightReq : Database.query('SELECT '+ String.join(new List<String>(Freight__c.sObjectType.getDescribe().fields.getMap().keySet()),',')+' FROM Freight__c WHERE Shipment_Order__c IN:ids')) {
                freightReqs.put(freightReq.Shipment_Order__c,freightReq);
            }
        }
        else if(objName.equalsIgnoreCase('Freight__c')) {
            query += ',Shipment_Order__r.Client_Contact_for_this_Shipment__c';
        }
        query += ' FROM '+objName+' WHERE Id IN :ids';

        String masterQuery = 'SELECT '+ String.join(new List<String>(Master_Task__c.sObjectType.getDescribe().fields.getMap().keySet()),',');
        masterQuery += ' FROM Master_Task__c WHERE Operational__c = true AND Assess_Condition_on_Creation_of__c=:objName';

        if(objName.equalsIgnoreCase('Shipment_Order__c')) {
            List<String> destinations = new List<String> {'All'};
            SObjectRecords = Database.query(query);
            for(SObject ship : SObjectRecords) {
                destinations.add(String.valueOf(ship.get('Destination__c')));
            }
            if(SObjectRecords[0].getSObject('Account__r') != null && SObjectRecords[0].getSObject('Account__r').get('Client_type__c') == 'E-commerce') {
                masterTaskFilter = 'Zee';
            }

            masterQuery += ' AND Shipment_Order_Record_Type__c includes (\''+ masterTaskFilter +'\')';
            masterQuery += ' AND Ship_To_Country__c includes (\''+String.join(destinations,'\',\'')+'\')';
            masterTasks = Database.query(masterQuery);
        }else{
            SObjectRecords = Database.query(query);
            masterTasks = Database.query(masterQuery);
        }

        for(SObject sobj : SObjectRecords) {
            List<String> masterTaskNames;
            Boolean createTasks = false;
            if(!objName.equalsIgnoreCase('Shipment_Order__c') || (objName.equalsIgnoreCase('Shipment_Order__c') && (String.valueOf(sobj.get('Mapped_Shipping_status__c')) == 'Quote Created' || String.valueOf(sobj.get('Mapped_Shipping_status__c')) == 'Compliance Pending'))) {
                createTasks = true;
            }
            else if(objName.equalsIgnoreCase('Shipment_Order__c')) {
                masterTaskNames = new List<String> {'Liability Cover: Temporary Storage > 25 days','Generate Zee Clearance Letter'};
            }

            for(Master_Task__c master : masterTasks) {
                if(!master.Applies_to_Domestic_Movements__c && objName.equalsIgnoreCase('Shipment_Order__c') && (Boolean)sobj.get('Domestic_Movement__c')){
                    continue;
                }
                Boolean onetaskAllow = false;
                if(masterTaskNames != null && masterTaskNames.contains(master.name)) {
                    onetaskAllow = true;
                }
                System.debug('Master Task Id: '+master.Id);
                Boolean isTrue = false;
                if(String.isNotBlank(master.Condition__c) && String.isNotBlank(master.Condition_Fields_API_Name__c)) {
                    String part_package_names = '';
                    List<String> states = master.States__c.split(';');
                    String relationshipName;
                    Map<String,Object> variables = new Map<String,Object>();
                    List<String> fields = master.Condition_Fields_API_Name__c.split(',');
                    String ex = master.Condition__c;
                    if(ex.contains('\'')) {
                        ex = ex.replaceAll('\'','\'\'');
                    }
                    String expression = String.format(ex,fields);

                    for(String field : fields) {
                        field = field.trim();
                        Object val = null;
                        if(objName.equalsIgnoreCase('Shipment_Order__c') && field.startsWithIgnoreCase('Freight__c')) {
                            val = freightReqs.containsKey(sobj.Id) ? getFieldValue(freightReqs.get(sobj.Id), field) : null;
                        }
                        else if(objName.equalsIgnoreCase('Shipment_Order__c') && field.startsWithIgnoreCase('Country__c')) {
                            SObject cpaObj = sobj.getSObject('CPA_v2_0__r');
                            SObject countryObj = cpaObj == null ? null : cpaObj.getSObject('Country__r');
                            val = countryObj == null ? null :  getFieldValue(countryObj, field);
                        }
                        else
                            val = getFieldValue(sobj,field);
                        variables.put(field,val);
                    }

                    List<SObject> relatedList;
                    if(objName.equalsIgnoreCase('Shipment_Order__c') && master.Condition_Fields_API_Name__c.containsIgnoreCase('Part__c') && shipmentParts.containsKey(sobj.Id))
                        relatedList = shipmentParts.get(sobj.Id);
                    else if(objName.equalsIgnoreCase('Shipment_Order__c') && master.Condition_Fields_API_Name__c.containsIgnoreCase('Shipment_Order_Package__c') && shipmentPackages.containsKey(sobj.Id))
                        relatedList = shipmentPackages.get(sobj.Id);
                    else if(objName.equalsIgnoreCase('Shipment_Order__c') && master.Condition_Fields_API_Name__c.containsIgnoreCase('Final_Delivery__c') && finalDeliveries.containsKey(sobj.Id))
                        relatedList = finalDeliveries.get(sobj.Id);

                    if(relatedList != null) {
                        for(SObject relatedObj : relatedList) {
                            Map<String,Object> vars = new Map<String,Object>(variables);
                            for(String field : fields) {
                                if(!vars.containsKey(field) || (vars.containsKey(field) && vars.get(field) == null)) {
                                    Object val = getFieldValue(relatedObj, field);
                                    vars.put(field,val);
                                }
                            }
                            Boolean result = Boolean.valueOf(ScriptEngine.getInstance().eval(expression, vars));
                            if(result) {
                                isTrue = true;
                                part_package_names += String.isBlank(part_package_names) ? String.valueOf(relatedObj.get('Name')) : ','+String.valueOf(relatedObj.get('Name'));
                            }
                        }

                    }else{
                        Object result = ScriptEngine.getInstance().eval(expression, variables);
                        isTrue = Boolean.valueOf(result);
                    }
                    if(isTrue && (!existingTasks.containsKey(master.Id+''+sobj.Id) || (existingTasks.containsKey(master.Id+''+sobj.Id) && existingTasks.get(master.Id+''+sobj.Id).Auto_Resolved__c && (existingTasks.get(master.Id+''+sobj.Id).State__c == 'Resolved' || existingTasks.get(master.Id+''+sobj.Id).IsNotApplicable__c)))) {
                        Id MasterTaskRecordId = Schema.SObjectType.Task.getRecordTypeInfosByName().get('MasterTaskRelated').getRecordTypeId();
                        String sub = master.Task_Subject__c;
                        if(objName.equalsIgnoreCase('Shipment_Order__c')) {
                            sub = master.Task_Subject__c+' - '+ sobj.get('NCP_Quote_reference__c') + (sobj.get('Client_Reference__c') != null ? ' '+sobj.get('Client_Reference__c') : '') + (sobj.get('Client_Reference_2__c') != null ? ' '+sobj.get('Client_Reference_2__c') : '');
                        }

                        Task newTask = new Task(
                            //Subject = master.Name,
                            Subject = sub,
                            whatID = String.valueOf(sobj.get('Id')),
                            Shipment_Order__c = objName.equalsIgnoreCase('Shipment_Order__c') ? sobj.Id : null,
                            WhoId = objName.equalsIgnoreCase('Contact') ? sobj.Id : null,
                            Status = 'Not Started',
                            Auto_Resolved__c = false,
                            Binding_Quote_Exempted_V2__c = master.Binding_Quote_Exempted_V2__c,
                            Blocks_Approval_to_Ship__c = master.Blocks_Approval_to_Ship__c,
                            Inactive__c = master.Set_Default_to_Inactive__c,
                            Master_Task__c = master.Id,
                            Description = String.isNotBlank(part_package_names) ? part_package_names : null,
                            State__c = states[0],
                            Reason_Not_Applicable__c=null,
                            IsNotApplicable__c = false,
                            Task_Category__c = master.Task_Category__c,
                            TecEx_Assigned_To__c = master.TecEx_Assigned_To__c,
                            TecEx_Pending_ETA_days__c = master.Estimated_TecEx_Pending_Time_days__c,
                            Under_Review_ETA_days__c = master.Estimated_Under_Review_Time_days__c,
                            IsVisibleInSelfService  = true,
                            RecordTypeId =MasterTaskRecordId
                            );

                        if(existingTasks.containsKey(master.Id+''+sobj.Id)) newTask.Id = existingTasks.get(master.Id+''+sobj.Id).Id;

                        if(objName.equalsIgnoreCase('Shipment_Order__c')) {
                            newTask.Client_Assigned_To__c = String.valueOf(sobj.get('Client_Contact_for_this_Shipment__c'));

                            switch on master.TecEx_Assigned_To__c {
                              when 'AM'{
                                newTask.Assigned_to__c =  sobj.get('IOR_CSE__c') == null ? (sobj.get('Lead_AM__c') == null ? null : String.valueOf(sobj.get('Lead_AM__c'))) : String.valueOf(sobj.get('IOR_CSE__c'));
                              }
                              when 'ICE'{
                                newTask.Assigned_to__c = sobj.get('ICE__c') == null ? (sobj.get('Lead_ICE__c') == null ? null : String.valueOf(sobj.get('Lead_ICE__c'))) : String.valueOf(sobj.get('ICE__c'));
                              }
                              when 'FC'{
                                newTask.Assigned_to__c = sobj.get('Financial_Controller__c') == null ? null : String.valueOf(sobj.get('Financial_Controller__c'));
                              }
                              when 'Compliance'{
                                newTask.Assigned_to__c =  sobj.get('Compliance_Team__c') == null ? null : String.valueOf(sobj.get('Compliance_Team__c'));
                              }
                              when 'ISM'{
                                newTask.Assigned_to__c =  sobj.get('Service_Manager__c') == null ? null : String.valueOf(sobj.get('Service_Manager__c'));
                              }
                              when 'VAT'{
                                newTask.Assigned_to__c =  sobj.get('Vat_Team__c') == null ? null : String.valueOf(sobj.get('Vat_Team__c'));
                              }
                              when 'LC'{
                                newTask.Assigned_to__c =  sobj.get('Freight_co_ordinator__c') == null ? null : String.valueOf(sobj.get('Freight_co_ordinator__c'));
                              }
                              when else {
                                newTask.Assigned_to__c = null;
                              }
                            }
                        }
                        else if(objName.equalsIgnoreCase('Freight__c')) {
                            sobject shipObj = sobj.getSObject('Shipment_Order__c');
                            newTask.Client_Assigned_To__c = String.valueOf(shipObj.get('Client_Contact_for_this_Shipment__c'));
                        }
                        else if(objName.equalsIgnoreCase('Account')) {
                            newTask.Assigned_to__c = sobj.get('Signed_By__c') == null ? null :  String.valueOf(sobj.get('Signed_By__c'));
                        }
                        if(newTask.Client_Assigned_To__c != null) {
                            cntIds.add(newTask.Client_Assigned_To__c);
                        }
                        if(createTasks || newTask.Id != null || onetaskAllow)
                          tasks.add(newTask);
                    }
                    if(isTrue && existingTasks.containsKey(master.Id+''+sobj.Id) && objName.equalsIgnoreCase('Shipment_Order__c') && existingTasks.get(master.Id+''+sobj.Id).State__c != 'Resolved' && !existingTasks.get(master.Id+''+sobj.Id).IsNotApplicable__c) {
                        Task tsk = existingTasks.get(master.Id+''+sobj.Id);
                        Boolean isChanged = false;
                        String assigned_to;

                        String tecex_assigned_to = tsk.State__c == 'Under Review' && master.Multiple_TecEx_People__c ? master.Tecex_Assigned_to_Under_Review__c : master.TecEx_Assigned_To__c;

                        switch on tecex_assigned_to {
                          when 'AM'{
                            assigned_to =  sobj.get('IOR_CSE__c') == null ? (sobj.get('Lead_AM__c') == null ? null : String.valueOf(sobj.get('Lead_AM__c'))) : String.valueOf(sobj.get('IOR_CSE__c'));
                          }
                          when 'ICE'{
                            assigned_to = sobj.get('ICE__c') == null ? (sobj.get('Lead_ICE__c') == null ? null : String.valueOf(sobj.get('Lead_ICE__c'))) : String.valueOf(sobj.get('ICE__c'));
                          }
                          when 'FC'{
                            assigned_to = sobj.get('Financial_Controller__c') == null ? null : String.valueOf(sobj.get('Financial_Controller__c'));
                          }
                          when 'Compliance'{
                            assigned_to = sobj.get('Compliance_Team__c') == null ? null : String.valueOf(sobj.get('Compliance_Team__c'));
                          }
                          when 'ISM'{
                            assigned_to = sobj.get('Service_Manager__c') == null ? null : String.valueOf(sobj.get('Service_Manager__c'));
                          }
                          when 'VAT'{
                            assigned_to =  sobj.get('Vat_Team__c') == null ? null : String.valueOf(sobj.get('Vat_Team__c'));
                          }
                          when 'LC'{
                          	assigned_to =  sobj.get('Freight_co_ordinator__c') == null ? null : String.valueOf(sobj.get('Freight_co_ordinator__c'));
                          }
                          when else {
                            assigned_to = null;
                          }
                        }

                        if(tsk.Assigned_to__c != assigned_to) {
                            tsk.Assigned_to__c = assigned_to;
                            isChanged = true;
                        }

                        String shipCntId = String.valueOf(sobj.get('Client_Contact_for_this_Shipment__c'));
                        if(tsk.Client_Assigned_To__c != shipCntId) {
                            tsk.Client_Assigned_To__c = shipCntId;
                            isChanged = true;
                        }

                        if(tsk.Description != part_package_names) {
                            tsk.Description = String.isNotBlank(part_package_names) ? part_package_names : null;
                            isChanged = true;
                        }
                        if(isChanged) tasks.add(tsk);
                    }
                    else if(!isTrue && existingTasks.containsKey(master.Id+''+sobj.Id) && existingTasks.get(master.Id+''+sobj.Id).State__c != 'Resolved' && !existingTasks.get(master.Id+''+sobj.Id).IsNotApplicable__c && String.isNotBlank(master.Refresh_Completed_State__c)) {
                        Task tsk = existingTasks.get(master.Id+''+sobj.Id);
                        if(master.Refresh_Completed_State__c == 'Set to Resolved')
                            tsk.state__c = 'Resolved';
                        else
                            tsk.IsNotApplicable__c = true;
                        tsk.Auto_Resolved__c = true;
                        tasks.add(tsk);
                    }
                }
            }
        }
        if(!cntIds.isEmpty()) {
            Map<Id,Id> cntUser = new Map<Id,Id>();
            for(User usr: [SELECT Id, ContactId, Contact.Firstname, Contact.Lastname, ProfileId, Profile.Name FROM User WHERE ContactId IN:cntIds]) {
                cntUser.put(usr.ContactId,usr.Id);
            }

            for(Task tsk : tasks) {
                if(tsk.Client_Assigned_To__c != null && cntUser.containsKey(tsk.Client_Assigned_To__c))
                    tsk.ownerId = cntUser.get(tsk.Client_Assigned_To__c);
            }
        }
        if(!tasks.isEmpty()) upsert tasks;
    }

    private static Object getFieldValue(Sobject sobj,String fieldName){

        List<String> field = fieldName.split('\\.');
        String objName = sobj.getSObjectType().getDescribe().getName();
        //List<String> objects = new List<String> {'shipment_order__c','freight__c','country__c','part__c','shipment_order_package__c'};//values in lowercase
        Map<String,String> relatedObjects = new Map<String,String> {'account'=>'Account__r','cpa_v2_0__c'=>'CPA_v2_0__r'};//keys in lowercase

        try{
            if(sobj.isSet(field[1].trim().toLowerCase()) && objName.equalsIgnoreCase(field[0].trim())) {
                return sobj.get(field[1].trim());
            }
        }catch(Exception e) {}
        if(objName == 'Shipment_Order__c' && relatedObjects.containsKey(field[0].trim().toLowerCase())) {
            SObject obj = sobj.getSObject(relatedObjects.get(field[0].trim().toLowerCase()));
            return obj== null ? null : obj.get(field[1].trim());
        }
        return null;
    }
}