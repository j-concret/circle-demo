@RestResource(urlMapping='/ShipmentOrderPackageDetails/*')
Global class CPShipmentOrderPackageDetails {
    
    @Httpget
    global static void ShipmentOrderPackageDetails(){
         RestRequest req = RestContext.request;
    	RestResponse res = RestContext.response;
        res.addHeader('Content-Type', 'application/json');
        String SOPID = req.params.get('SOPID');
        String LoginAccount = req.params.get('LoginAccount');
        String LoginContact = req.params.get('LoginContact');
        
         try {  
          
          Shipment_Order_Package__c SOP = [select id,Chargeable_Weight_KGs__c,Shipment_Order__c,Weight_Unit__c,Dimension_Unit__c,packages_of_same_weight_dims__c,Length__c,Height__c,Breadth__c,Actual_Weight__c,Actual_Weight_KGs__c,Oversized__c,Volumetric_Weight_KGs__c from Shipment_Order_Package__c where id=:SOPID];
            
         
          		  res.responseBody = Blob.valueOf(JSON.serializePretty(SOP));
        		  res.statusCode = 200;
                
                API_Log__c Al = New API_Log__c();
                Al.Account__c=LoginAccount;
                Al.Login_Contact__c=LoginContact;
                Al.EndpointURLName__c='ShipmentOrderPackageDetails';
                Al.Response__c='Success - ShipmentOrderPackage Details are sent';
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al;
             
      
      }
        catch(Exception e){
            
                String ErrorString ='Something went wrong while getting Shipment order packages details, please contact support_SF@tecex.com';
               	res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
            res.statusCode = 500;
                 API_Log__c Al = New API_Log__c();
                Al.Account__c=LoginAccount;
                Al.Login_Contact__c=LoginContact;
                Al.EndpointURLName__c='ShipmentOrderPackageDetails';
                Al.Response__c='Exception - ShipmentOrderPackage Details are not sent'+e.getMessage();
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al;
            
            
        }
        
        
    }

}