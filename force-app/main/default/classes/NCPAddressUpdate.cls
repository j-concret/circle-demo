@RestResource(urlMapping='/UpdateAddress/*')
Global class NCPAddressUpdate {
    @Httppost
    global static void CPPickupAddress(){
     	RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        Blob body = req.requestBody;
    	res.addHeader('Content-Type', 'application/json');
        String requestString = body.toString();
        NCPAddressEditWra rw = (NCPAddressEditWra)JSON.deserialize(requestString,NCPAddressEditWra.class);
     	Client_Address__c AB =[select id,Name,Comments__c,CompanyName__c,Pickup_Preference__c,AdditionalContactNumber__c,Contact_Full_Name__c,Contact_Email__c,Contact_Phone_Number__c,Address__c,Address2__c,City__c,Province__c,Postal_Code__c,All_Countries__c,Client__c  from Client_Address__c where Id =:rw.Id];
        try{
        Access_token__c At =[select status__c,Access_Token__c from Access_token__c where Access_Token__c=:rw.Accesstoken];
        if( at.status__C =='Active'){
             		If(rw.DefaultAddress == 'TRUE'){
                		Id PickuprecordTypeId = Schema.SObjectType.Client_Address__c.getRecordTypeInfosByDeveloperName().get('PickUp').getRecordTypeId();
                		List<Client_Address__c> AB1=[select id,Name,default__C,Address_status__c,Comments__c,All_Countries__c,Client__c  from Client_Address__c where All_Countries__c= :rw.All_Countries and default__C = true and Client__c =:Ab.Client__c and recordtypeID=:PickuprecordTypeId];
                
                  		for(Integer i=0; AB1.size()>i;i++) { AB1[i].Default__c =FALSE;} Update AB1;
                    }
            AB.Name = Rw.Name;
            AB.Contact_Full_Name__c= rw.Contact_Full_Name;
            AB.Contact_Email__c = rw.Contact_Email;
            AB.Contact_Phone_Number__c = rw.Contact_Phone_Number;
            AB.AdditionalContactNumber__c = rw.AdditionalNumber;
            AB.Address__c=rw.AddressLine1;
            AB.Address2__c=rw.AddressLine2;
            AB.City__c=rw.City;
            AB.Province__c=rw.Province;
            AB.Postal_Code__c=rw.Postal_Code;
            AB.All_Countries__c=rw.All_Countries;
            AB.Comments__c=rw.Comments;
            AB.CompanyName__c=rw.CompanyName;
            AB.default__c=boolean.valueof(rw.DefaultAddress);
            AB.address_status__c=rw.AddressStatus;
            AB.Pickup_Preference__c=rw.PickupPreference;
            
            Update AB;
            
            JSONGenerator gen = JSON.createGenerator(true);
            gen.writeStartObject();
            
             gen.writeFieldName('Success');
             gen.writeStartObject();
            
             	if(Rw.ID == null) {gen.writeNullField('Response');} else{gen.writeStringField('Response', 'Success-Addrerss updated');}
            
             gen.writeEndObject();
            gen.writeEndObject();
            String jsonData = gen.getAsString();
           
           // String ASD = 'Success-Addrerss updated';	
            //res.responseBody = Blob.valueOf(JSON.serializePretty(ASD));
              res.responseBody = Blob.valueOf(jsonData);
              res.addHeader('Content-Type', 'application/json');
              res.statusCode = 200;
              	API_Log__c Al = New API_Log__c();
                Al.Account__c=AB.Client__c;
                Al.EndpointURLName__c='NCP-UpdateAddress';
                Al.Response__c='Success - AddressUpdated';
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al; 
            
            
        }
        }
        Catch(Exception e){
            
           		String ErrorString ='Something went wrong while updating address, please contact support_SF@tecex.com';
               	//res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
               //System.debug('Error '+e.getMessage()+e.getStackTraceString()+e.getLineNumber());
              res.responseBody = Blob.valueOf(ErrorString);
             	res.addHeader('Content-Type', 'application/json');
            	res.statusCode = 500;
                API_Log__c Al = New API_Log__c();
                Al.Account__c=AB.Client__c;
                Al.EndpointURLName__c='NCP-AddressUpdate';
                Al.Response__c=e.getMessage()+' at line number: '+e.getLineNumber();
                Al.StatusCode__c=string.valueof(res.statusCode);
              	Insert Al;

            
            
        }
    }
}