public class NCPUpdateSOPackageWra {
    
    public List<SOP> SOP;
    
    public String Accesstoken;
    public String AccountID;
    public String ContactID;
    
    public class SOP {
        public String SOPID;
        public String Weight_Unit;
        public String Dimension_Unit;
        public Integer Packages_of_Same_Weight;
        public String Length;
        public String Height;
        public String Breadth;
        public String Actual_Weight;
        public Boolean LithiumBatteries;
        public Boolean Contains_Batteries;
        public Boolean ION_PI966;
        public Boolean ION_PI967;
        public Boolean Metal_PI969;
        public Boolean Metal_PI970;
        public Boolean DangerousGoods;
    }

}