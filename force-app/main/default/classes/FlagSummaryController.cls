public with sharing class FlagSummaryController {

    @AuraEnabled
    public static Map<String,Object> getFlagCounts(Id recordId){
        Map<String,Object> response = new Map<String,Object>{'status'=>'OK'};
        Map<String,FlagCount> flagWithCount = new Map<String,FlagCount>();
        try{
            for(String flagVal : picklist_values('Task','Flag_Type__c')) {
              flagWithCount.put(flagVal,new FlagCount(flagVal,0,4));
            }
            Shipment_Order__c shipmentOrder = [SELECT Id,Name FROM Shipment_Order__c WHERE Id=:recordId];

            for(Task task : [SELECT Priority_Level__c,Flag_Type__c FROM Task where WhatId=:recordId And Status='In Progress']) {
              Integer colorcode = 4;
              if(task.Priority_Level__c == 'Shipment Stop') {
                colorcode = 1;
              }
              if(task.Priority_Level__c == 'Shipment Block') {
                colorcode = 2;
              }
              if(task.Priority_Level__c == 'Shipment Alert') {
                colorcode = 3;
              }

              flagWithCount.get(task.Flag_Type__c).setCountAndColor(colorcode);
            }
            response.put('data',flagWithCount.values());
            response.put('shipmentOrder',shipmentOrder);
        }catch(Exception e){
          response.put('status','ERROR');
          response.put('msg',e.getMessage());
        }
        return response;
    }

    @AuraEnabled
    public static Map<String,Object> refreshFlags(Id recordId){
        Map<String,Object> response = new Map<String,Object>{'status'=>'OK'};
        try{
            update new Shipment_Order__c(Id=recordId,Update_Rag_Status_Time__c =true);
        }
        catch(Exception e){
          response.put('status','ERROR');
          response.put('msg',e.getMessage());
        }
        return response;
    }
    public class FlagCount {
      @AuraEnabled
      public String name;
      @AuraEnabled
      public Integer count;
      @AuraEnabled
      public String colorName;
      @AuraEnabled
      public Integer colorCode;//1-red ,2-orange,3-yellow,4-green

      public FlagCount(String name,Integer count,Integer colorCode){
        this.name = name;
        this.count = count;
        this.colorCode = colorCode;
        this.colorName = 'green';//default color
      }
      public void setCountAndColor(Integer colorCode){
        if(this.colorCode > colorCode)
          this.colorCode = colorCode;

        this.colorName = this.colorCode == 1 ? 'red' : this.colorCode == 2 ? 'orange' : this.colorCode == 3 ? 'yellow' : 'green';
        this.count++;
      }
    }

    public static Set<String> picklist_values(String object_name, String field_name) {
        Set<String> values = new Set<String>();
        String[] types = new String[] {object_name};
        Schema.DescribeSobjectResult[] results = Schema.describeSObjects(types);
        for(Schema.DescribeSobjectResult res : results) {
            for (Schema.PicklistEntry entry : res.fields.getMap().get(field_name).getDescribe().getPicklistValues()) {
                if (entry.isActive()) {
                    values.add(entry.getValue());
                }
            }
        }
        return values;
    }

}