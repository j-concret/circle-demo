@isTest(SeeAllData=true)
public with sharing class BT_AppliedBankTransactionAlert_Test3 {

    static testmethod void testsendEmail(){

       /* //Load CSV file saved in static resource  
        List<Account> lstAcc = Test.loadData(Account.sObjectType,'testAccount');
        List<AcctSeed__GL_Account__c> lstBankAcc = Test.loadData(AcctSeed__GL_Account__c.sObjectType,'testBankAccount');
        List<AcctSeed__Accounting_Period__c> lstAccountingPeriod = Test.loadData(AcctSeed__Accounting_Period__c.sObjectType,'testAccountingPeriod');
        List<AcctSeed__Cash_Disbursement_Batch__c> lstCashDisbursementBatch = Test.loadData(AcctSeed__Cash_Disbursement_Batch__c.sObjectType,'testCashDisbursementBatch');
        //List<Bank_transactions__c> lstbankTransaction = Test.loadData(Bank_transactions__c.sObjectType,'testBankTransaction');
       List<AcctSeed__Cash_Disbursement__c> lstCashDisbursement = Test.loadData(AcctSeed__Cash_Disbursement__c.sObjectType,'testCashDisbursement');

        //Confirm that total number of accounts created is 1
        System.assertEquals(lstAcc.size(), 1);
        System.assertEquals(lstBankAcc.size(), 1);
        System.assertEquals(lstAccountingPeriod.size(), 1);
        System.assertEquals(lstCashDisbursementBatch.size(), 1);
        System.assertEquals(lstCashDisbursement.size(), 1);
        //System.assertEquals(lstbankTransaction.size(), 1);  */
        
        Bank_transactions__c testBankTrans = [SELECT Id, NAme
                                                FROM Bank_transactions__c
                                               WHERE Id = 'a3q1v000001AquUAAS'
                                               LIMIT 1
                                             ];
        List<Id> btIdList = new List<Id>{testBankTrans.Id};
        test.startTest();
        BT_AppliedBankTransactionAlert.sendEmail(btIdList);
        test.stopTest();
    }

}