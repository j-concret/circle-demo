public class CPA_Costing_TriggerHandler extends Base_Handler
{
    Private List<CPA_Costing__c> newCostings = trigger.new;
    Private List<CPA_Costing__c> oldCostings = trigger.old;
    Private List<ID> CPACostings_Delete = new List<ID>();

    public override void bulkBefore()
    {
        super.bulkBefore();

        if (trigger.isDelete)
            return;

        List<ID> relatedSOIds = new List<ID>();

        for(CPA_Costing__c cost : newCostings)
            relatedSOIds.Add(cost.Shipment_Order_Old__c);

        List<Shipment_Order__c> SOs = [SELECT Chargeable_Weight__c, of_Line_Items__c, of_Unique_Line_Items__c, Total_Taxes__c,  Shipment_Value_USD__c FROM Shipment_Order__c WHERE ID in: relatedSOIds];

        for(CPA_Costing__c cost : newCostings)
        {
            for (Shipment_Order__c so : SOs)
                if(so.ID == cost.Shipment_Order_Old__c)
                {
                    costIdSO.put(cost.Id, SO);
                }
        }

        if (trigger.isUpdate)
        {
        //    for(CPA_Costing__c cost : newCostings)
       //     {
         //       if (isUserChange(cost) && ((CPA_Costing__c) Trigger.oldMap.get(cost.ID)).Updating__c == cost.Updating__c && ((CPA_Costing__c) Trigger.oldMap.get(cost.ID)).Shipment_Order_Old__c <> null)
          //      {
          //          if (cost.Cost_Type__c != 'Fixed' && cost.Rate__c != null && ((CPA_Costing__c) Trigger.oldMap.get(cost.ID)).Amount__c != cost.Amount__c )
                       

          //         cost.Cost_Type__c = 'Captured';
           //        cost.Amended__c = true;

            //       SOTriggerHelper.calculateSOCostingAmount(costIdSO.get(cost.ID), cost, true, cost.Currency__c );
            //       cost.Cost_Type__c = 'Captured';
              //  }
        //    }
      //  }
 //   }

    //Used to check if the update was made by a user on the Cost or whether it was a changed because of a shipment order change.
 //   private boolean isUserChange(CPA_Costing__c cost)
  //  {
    //    CPA_Costing__c oldCost = (CPA_Costing__c) Trigger.oldMap.get(cost.ID);

    //    if (cost.Cost_Type__c == 'Fixed' && cost.Amount__c != oldCost.Amount__c)
      //      return true;
   //     else if ((cost.Cost_Type__c == 'Variable' || cost.Cost_Type__c == 'Captured') && (cost.Amount__c != oldCost.Amount__c && cost.Rate__c == oldCost.Rate__c))
     //       return true;
    //    else if ((cost.Cost_Type__c == 'Variable' || cost.Cost_Type__c == 'Captured') && (cost.Amount__c == oldCost.Amount__c && cost.Rate__c != oldCost.Rate__c))
     //       return true;
     //   else
   //        return false;
    }
        
    }

    Map<ID, Shipment_Order__c> costIdSO = new Map<ID, Shipment_Order__c>();

    public override void bulkAfter()
    {
        super.bulkAfter();
    }

    public override void beforeInsert(List<SObject> newlstObj)
    {
        super.beforeInsert(newlstObj);
    }

    public override void beforeUpdate(List<SObject> newlstObj, List<SObject> oldlstObj, Map<Id, SObject> newMapObj, Map<Id, SObject> oldMapObj)
    {
        super.beforeUpdate(newlstObj, oldlstObj, newMapObj, oldMapObj);

    }

    public override void beforeDelete(List<SObject> oldlstObj, Map<Id, SObject> oldMapObj)
    {
        super.beforeDelete(oldlstObj, oldMapObj);
    }

    public override void afterInsert(List<SObject> newlstObj, Map<Id, SObject> newMapObj)
    {
        super.afterInsert(newlstObj, newMapObj);
    }

    public override void afterUpdate(List<SObject> newlstObj, List<SObject> oldlstObj, Map<Id, SObject> newMapObj, Map<Id, SObject> oldMapObj)
    {
        super.afterUpdate(newlstObj, oldlstObj, newMapObj, oldMapObj);
    }

    public override void afterDelete(List<SObject> oldlstObj, Map<Id, SObject> oldMapObj)
    {
        super.afterDelete(oldlstObj, oldMapObj);
    }

    public override void andFinally()
    {
        super.andFinally();

    }
}