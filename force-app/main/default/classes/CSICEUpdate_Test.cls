@IsTest
public class CSICEUpdate_Test {
 @Testsetup
    static void setupdata(){
        Id shipmentOrderRecTypeId = Schema.SObjectType.shipment_order__c.getRecordTypeInfosByName().get('Cost Estimate').getRecordTypeId();
        Id accountRecTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('IOR/EOR Supplier').getRecordTypeId();

        
        Access_token__c accessTokenObj = new Access_token__c();
        accessTokenObj.Status__c = 'Active';
        accessTokenObj.Access_Token__c = 'asdfghj-sdfgfds-sfdfgf';
        
        Insert accessTokenObj;
        
        Account acc = new Account(Name = 'Acme Test',RecordTypeId = accountRecTypeId);
        
        Insert acc;
        
         Account account4 = new Account (name='TecEx Prospective Client', Type ='Client',New_Invoicing_Structure__c = true);
        insert account4;
        
        Contact contact = new Contact ( lastname ='Testing Individual',  AccountId = acc.Id,Client_Notifications_Choice__c='Opt-In');
        insert contact; 
        
        
        Country__c country = new Country__c(Brokerage_Costs__c = 618, CIF_Adjustment_Factor__c = 0.035, CIF_Absolute_value_adjustment__c = 200, Clearance_Costs__c = 488, 
                                            Country__c = 'Brazil', Name = 'Brazil', Handling_Costs__c = 1180, License_Permit_Costs__c = 0);
        insert country;
        
        CPA_v2_0__c RelatedCPA = new CPA_v2_0__c(Name = 'Brazil', Country_Applied_Costings__c=true );
        insert RelatedCPA;
        CPA_v2_0__c cpav2 = new CPA_v2_0__c(Name = 'Brazil Testing 2.0', Supplier__c = acc.Id, Service_Type__c = 'IOR/EOR', Related_Costing_CPA__c = RelatedCPA.Id, Preferred_Supplier__c = TRUE,
                                            VAT_Rate__c = 0.1, Destination__c = 'Brazil',  
                                            
                                            Default_Section_1_Address_Line_1__c= 'Address_Line_1' ,
                                            Default_Section_1_Address_Line_2__c= 'Address_Line_2' ,
                                            Default_Section_1_Address_Line_3__c= 'Address_Line_3' ,
                                            Default_Section_1_City__c= 'City' ,
                                            Default_Section_1_Zip__c= 'Zip' ,
                                            Default_Section_1_Country__c = 'Country' , 
                                            Default_Section_1_Other__c= 'Other' ,
                                            Default_Section_1_Tax_Name__c= 'Tax_Name' ,
                                            Default_Section_1_Tax_Id__c= 'Tax_Id' ,
                                            Default_Section_1_Contact_Name__c= 'Contact_Name' ,
                                            Default_Section_1_Contact_Email__c= 'Contact_Email' ,
                                            Default_Section_1_Contact_Tel__c= 'Contact_Tel' ,
                                            Alternate_Section_1_Address_Line_1__c= 'Address_Line_1' ,
                                            Alternate_Section_1_Address_Line_2__c= 'Address_Line_2' ,
                                            Alternate_Section_1_Address_Line_3__c= 'Address_Line_3' ,
                                            Alternate_Section_1_City__c= 'City' ,
                                            Alternate_Section_1_Zip__c= 'Zip' ,
                                            Alternate_Section_1_Country__c = 'Country' ,
                                            Alternate_Section_1_Other__c = 'Other' ,
                                            Alternate_Section_1_Tax_Name__c = 'Tax_Name' ,
                                            Alternate_Section_1_Tax_Id__c = 'Tax_Id' ,
                                            Alternate_Section_1_Contact_Name__c= 'Contact_Name' ,
                                            Alternate_Section_1_Contact_Email__c= 'Contact_Email' ,
                                            Alternate_Section_1_Contact_Tel__c= 'Contact_Tel', 
                                            Default_Section_2_Address_Line_1__c= 'Address_Line_1' ,
                                            Default_Section_2_Address_Line_2__c= 'Address_Line_2' ,
                                            Default_Section_2_Address_Line_3__c= 'Address_Line_3' ,
                                            Default_Section_2_City__c= 'City' ,
                                            Default_Section_2_Zip__c= 'Zip' ,
                                            Default_Section_2_Country__c = 'Country' , 
                                            Default_Section_2_Other__c= 'Other' ,
                                            Default_Section_2_Tax_Name__c= 'Tax_Name' ,
                                            Default_Section_2_Tax_Id__c= 'Tax_Id' ,
                                            Default_Section_2_Contact_Name__c= 'Contact_Name' ,
                                            Default_Section_2_Contact_Email__c= 'Contact_Email' ,
                                            Default_Section_2_Contact_Tel__c= 'Contact_Tel' ,
                                            Alternate_Section_2_Address_Line_1__c= 'Address_Line_1' ,
                                            Alternate_Section_2_Address_Line_2__c= 'Address_Line_2' ,
                                            Alternate_Section_2_Address_Line_3__c= 'Address_Line_3' ,
                                            Alternate_Section_2_City__c= 'City' ,
                                            Alternate_Section_2_Zip__c= 'Zip' ,
                                            Alternate_Section_2_Country__c = 'Country' ,
                                            Alternate_Section_2_Other__c = 'Other' ,
                                            Alternate_Section_2_Tax_Name__c = 'Tax_Name' ,
                                            Alternate_Section_2_Tax_Id__c = 'Tax_Id' ,
                                            Alternate_Section_2_Contact_Name__c= 'Contact_Name' ,
                                            Alternate_Section_2_Contact_Email__c= 'Contact_Email',
                                            Alternate_Section_2_Contact_Tel__c= 'Contact_Tel',
                                            Default_Section_3_Address_Line_1__c= 'Address_Line_1' ,
                                            Default_Section_3_Address_Line_2__c= 'Address_Line_2' ,
                                            Default_Section_3_Address_Line_3__c= 'Address_Line_3' ,
                                            Default_Section_3_City__c= 'City' ,
                                            Default_Section_3_Zip__c= 'Zip' ,
                                            Default_Section_3_Country__c = 'Country' , 
                                            Default_Section_3_Other__c= 'Other' ,
                                            Default_Section_3_Tax_Name__c= 'Tax_Name' ,
                                            Default_Section_3_Tax_Id__c= 'Tax_Id' ,
                                            Default_Section_3_Contact_Name__c= 'Contact_Name' ,
                                            Default_Section_3_Contact_Email__c= 'Contact_Email' ,
                                            Default_Section_3_Contact_Tel__c= 'Contact_Tel' ,
                                            Alternate_Section_3_Address_Line_1__c= 'Address_Line_1' ,
                                            Alternate_Section_3_Address_Line_2__c= 'Address_Line_2' ,
                                            Alternate_Section_3_Address_Line_3__c= 'Address_Line_3' ,
                                            Alternate_Section_3_City__c= 'City' ,
                                            Alternate_Section_3_Zip__c= 'Zip' ,
                                            Alternate_Section_3_Country__c = 'Country' ,
                                            Alternate_Section_3_Other__c = 'Other' ,
                                            Alternate_Section_3_Tax_Name__c = 'Tax_Name' ,
                                            Alternate_Section_3_Tax_Id__c = 'Tax_Id' ,
                                            Alternate_Section_3_Contact_Name__c= 'Contact_Name' ,
                                            Alternate_Section_3_Contact_Email__c= 'Contact_Email' ,
                                            Alternate_Section_3_Contact_Tel__c= 'Contact_Tel',
                                            Default_Section_4_Address_Line_1__c= 'Address_Line_1' ,
                                            Default_Section_4_Address_Line_2__c= 'Address_Line_2' ,
                                            Default_Section_4_Address_Line_3__c= 'Address_Line_3' ,
                                            Default_Section_4_City__c= 'City' ,
                                            Default_Section_4_Zip__c= 'Zip' ,
                                            Default_Section_4_Country__c = 'Country' , 
                                            Default_Section_4_Other__c= 'Other' ,
                                            Default_Section_4_Tax_Name__c= 'Tax_Name' ,
                                            Default_Section_4_Tax_Id__c= 'Tax_Id' ,
                                            Default_Section_4_Contact_Name__c= 'Contact_Name' ,
                                            Default_Section_4_Contact_Email__c= 'Contact_Email' ,
                                            Default_Section_4_Contact_Tel__c= 'Contact_Tel' ,
                                            Alternate_Section_4_Address_Line_1__c= 'Address_Line_1' ,
                                            Alternate_Section_4_Address_Line_2__c= 'Address_Line_2' ,
                                            Alternate_Section_4_Address_Line_3__c= 'Address_Line_3' ,
                                            Alternate_Section_4_City__c= 'City' ,
                                            Alternate_Section_4_Zip__c= 'Zip' ,
                                            Alternate_Section_4_Country__c = 'Country' ,
                                            Alternate_Section_4_Other__c = 'Other' ,
                                            Alternate_Section_4_Tax_Name__c = 'Tax_Name' ,
                                            Alternate_Section_4_Tax_Id__c = 'Tax_Id' ,
                                            Alternate_Section_4_Contact_Name__c= 'Contact_Name' ,
                                            Alternate_Section_4_Contact_Email__c= 'Contact_Email' ,
                                            Alternate_Section_4_Contact_Tel__c= 'Contact_Tel' ,
                                            VAT_Reclaim_Destination__c = FALSE,
                                            Final_Destination__c ='Brazil',
                                            Country__c = country.id,
                                            CIF_Freight_and_Insurance__c  = 'CIF Amount'
                                           );
        insert cpav2;
        
        
        CpaRules__c cpaRule = new CpaRules__c();
        cpaRule.Country__c = 'Finland';
        cpaRule.Criteria__c = 'Default';
        cpaRule.DefaultCPA2__c = cpav2.Id;
        cpaRule.CPA2__c =  cpav2.Id;
        Insert cpaRule;
        
        
        
       IOR_Price_List__c iorpl = new IOR_Price_List__c ( Client_Name__c = acc.Id, Name = 'Brazil', IOR_Fee__c = 0.15, On_Charge_Mark_up__c = 0.15,
                                                         TecEx_Shipping_Fee_Markup__c =0.40, IOR_Min_Fee__c = 100,  Admin_Fee__c =100, Set_up_fee__c =100, Bank_Fees__c =200, Tax_Rate__c = 0.20,
                                                         Estimated_Customs_Brokerage__c =250, Estimated_Customs_Clearance__c =250, Estimated_Customs_Handling__c =250, Estimated_Import_License__c =250, Destination__c = 'Brazil' );
        insert iorpl; 
        
        IOR_Price_List__c iorpl1 = new IOR_Price_List__c ( Client_Name__c = account4.Id, Name = 'Brazil', IOR_Fee__c = 0.15, On_Charge_Mark_up__c = 0.15,
                                                              TecEx_Shipping_Fee_Markup__c =0.40, IOR_Min_Fee__c = 100,  Admin_Fee__c =100, Set_up_fee__c =100, Bank_Fees__c =200, Tax_Rate__c = 0.20,
                                                              Estimated_Customs_Brokerage__c =250, Estimated_Customs_Clearance__c =250, Estimated_Customs_Handling__c =250, Estimated_Import_License__c =250, Destination__c = 'Belgium' );
            
        insert iorpl1;
        
        shipment_order__c shipment = new shipment_order__c();
        shipment.RecordTypeId = shipmentOrderRecTypeId;
        shipment.Destination__c = 'Brazil';
        shipment.Account__c = acc.Id;
        shipment.CPA_v2_0__c = cpav2.Id;
        shipment.Special_Client_Invoicing__c = true;
        shipment.Clear_out_all_costings__c = false;
        shipment.shipping_status__c='Cost Estimate';
        shipment.Shipment_Value_USD__c = 2000;
        shipment.Insurance_Fee__c = 0;
        shipment.Client_Actual_Freight_Value__c = 10;
        shipment.IOR_Cost_If_in_USD_2__c = 10;
        shipment.IOR_Cost_Custom_b__c = 10;
        shipment.IOR_Refund_of_Tax_and_Duty__c = 10;
        shipment.Customs_Brokerage_Cost_fixed__c = 10;
        shipment.Bank_Cost_b__c = 10;
        shipment.Supplier_Admin_Cost__c = 10 ;
        shipment.Customs_Handling_Cost__c = 10;
        shipment.Courier_Handover_Cost__c = 10;
        shipment.Delvier_to_IOR_Warehouse_Cost__c = 10;
        shipment.Local_Delivery_Cost__c = 10;
        shipment.Examination_Cost__c = 10 ;
        shipment.Open_and_Repack_Cost__c = 10;
        shipment.Loading_Cost__c = 10;
        shipment.Storage_Cost__c = 10;
        shipment.Terminal_Charges_Cost__c = 10;
        shipment.Handling_other_costs__c = 10;
        shipment.Customs_Clearance_Cost_b__c = 10;
        shipment.Import_Declaration_Cost__c = 10;
        shipment.Quarantine_cost__c = 10;
        shipment.Customs_Inspection_Cost__c = 10;
        shipment.Delivery_Order_Cost__c = 10;
        shipment.License_Permit_Cost__c = 10;
        shipment.Who_arranges_International_courier__c = 'Client';
        shipment.Fixed_IOR_Cost__c = 10;
        shipment.Miscellaneous_Costs__c = 10;
        shipment.Liability_Cover_Fee_USD__c= 0.0;
        shipment.Liability_Cover_Cost_USD__c = 0.0;
        shipment.Forecast_Override__c = true;
        shipment.Handling_and_Admin_Fee__c = 0.0;
        shipment.Bank_Fees__c = 0.0;
               
        Insert shipment;
        
    }
    
    static testMethod void CSICEUpdate(){
        Access_token__c accessTokenObj = [SELECT Id,Access_Token__c,Status__c FROM Access_token__c];
        shipment_order__c shipment = [SELECT Id,RecordTypeId,Destination__c,IOR_Price_List__c,Account__c,CPA_v2_0__c,shipping_status__c FROM shipment_order__c];
        Test.startTest();
        List<CSICEUpdateWra.Parts> partList = new List<CSICEUpdateWra.Parts>();
        
		CSICEUpdateWra.Parts partsWrap = new CSICEUpdateWra.Parts();
        partsWrap.ClientpartReferencenumber = '123465';
        partsWrap.PartDescription = 'Anil';
        partsWrap.PartNumber = '123';
        partsWrap.Quantity = 1;
        partsWrap.UnitPrice = 1500;
        partList.add(partsWrap);
        
        
        CSICEUpdateWra wrapper = new CSICEUpdateWra();
        wrapper.AccessToken = accessTokenObj.Access_Token__c;
        wrapper.SOID = shipment.Id;
        wrapper.estimatedChargableweight = 10.23;
        wrapper.ServiceType = 'IOR';
        wrapper.Courier_responsibility = 'TecEx';
        wrapper.Reference1 = 'Ref1';
        wrapper.Reference2 = 'Ref2';
        wrapper.ShipmentvalueinUSD = 100;
        wrapper.PONumber = '123';
        wrapper.Type_of_Goods = 'Refurbished';
        wrapper.Li_ion_Batteries = 'Yes';
        wrapper.Li_ion_BatteryTypes = 'Lithium ION batteries';
        wrapper.NumberOfFinaldeliveries = 123;
        wrapper.Parts = partList;
        
        RestRequest req1 = new RestRequest();
        RestResponse res1 = new RestResponse();
        req1.requestURI = '/services/apexrest/CEupdate'; 
        req1.httpMethod = 'POST';
        req1.requestBody = Blob.valueOf(JSON.serialize(wrapper));     
        RestContext.request = req1;
        RestContext.response = res1;
        RestContext.request.addHeader('Content-Type', 'application/json');

        CSICEUpdate.CSICEUpdate();
        Test.stopTest(); 
    }
    
    static testMethod void CSICEUpdateExpiredTokenTest(){
        Access_token__c accessTokenObj = [SELECT Id,Access_Token__c,Status__c FROM Access_token__c];
        shipment_order__c shipment = [SELECT Id,RecordTypeId,Destination__c,IOR_Price_List__c,Account__c,CPA_v2_0__c,shipping_status__c FROM shipment_order__c];
           
        
        CSICEUpdateWra wrapper = new CSICEUpdateWra();
        wrapper.AccessToken = '';
        wrapper.SOID = shipment.Id;
        
        RestRequest req1 = new RestRequest();
        RestResponse res1 = new RestResponse();
        req1.requestURI = '/services/apexrest/CEDetails'; 
        req1.httpMethod = 'POST';
        req1.requestBody = Blob.valueOf(JSON.serialize(wrapper));     
        RestContext.request = req1;
        RestContext.response = res1;
        RestContext.request.addHeader('Content-Type', 'application/json');
        Test.startTest();
        CSICEUpdate.CSICEUpdate();
        Test.stopTest(); 
    }
     static testMethod void CSICEUpdateNoParts(){
        Access_token__c accessTokenObj = [SELECT Id,Access_Token__c,Status__c FROM Access_token__c];
        shipment_order__c shipment = [SELECT Id,RecordTypeId,Destination__c,IOR_Price_List__c,Account__c,CPA_v2_0__c,shipping_status__c FROM shipment_order__c];
        Test.startTest();
        List<CSICEUpdateWra.Parts> partList = new List<CSICEUpdateWra.Parts>();
        
		CSICEUpdateWra.Parts partsWrap = new CSICEUpdateWra.Parts();
        partsWrap.ClientpartReferencenumber = '123465';
        partsWrap.PartDescription = 'Anil';
        partsWrap.PartNumber = '123';
        partsWrap.Quantity = 1;
        partsWrap.UnitPrice = 1500;
      //  partList.add(partsWrap);
        
        
        CSICEUpdateWra wrapper = new CSICEUpdateWra();
        wrapper.AccessToken = accessTokenObj.Access_Token__c;
        wrapper.SOID = shipment.Id;
        wrapper.estimatedChargableweight = 10.23;
        wrapper.ServiceType = 'IOR';
        wrapper.Courier_responsibility = 'Client';
        wrapper.Reference1 = 'Ref1';
        wrapper.Reference2 = 'Ref2';
        wrapper.ShipmentvalueinUSD = 100;
        wrapper.PONumber = '123';
        wrapper.Type_of_Goods = 'Refurbished';
        wrapper.Li_ion_Batteries = 'Yes';
        wrapper.Li_ion_BatteryTypes = 'Lithium ION batteries';
        wrapper.NumberOfFinaldeliveries = 123;
        wrapper.Parts = partList;
        
        RestRequest req1 = new RestRequest();
        RestResponse res1 = new RestResponse();
        req1.requestURI = '/services/apexrest/CEupdate'; 
        req1.httpMethod = 'POST';
        req1.requestBody = Blob.valueOf(JSON.serialize(wrapper));     
        RestContext.request = req1;
        RestContext.response = res1;
        RestContext.request.addHeader('Content-Type', 'application/json');

        CSICEUpdate.CSICEUpdate();
        Test.stopTest(); 
    }
}