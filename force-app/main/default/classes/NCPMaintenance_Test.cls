@isTest
public class NCPMaintenance_Test {
    
    @isTest
    public static void NCPMaintenanceTest() {
        Maintenance__c maintObj = new Maintenance__c(Display_Message__c = 'Test',   
                                            Schedule_end_time_UTC__c = System.now(), 
                                            Schedule_start_time__c = System.now(), 
                                            Type_Of_Maintenance__c= 'Client Portal',
                                            Status__c = 'Active');

        insert maintObj;

        RestRequest req1 = new RestRequest();
        RestResponse res1 = new RestResponse();
        req1.requestURI = '/services/apexrest/NCPMaintenance/'; 
        req1.httpMethod = 'GET';    
        RestContext.request = req1;
        RestContext.response = res1;
        RestContext.request.addHeader('Content-Type', 'application/json');
        
        Test.startTest(); 
        	NCPMaintenance.NCPMaintenance();
        Test.stopTest();
    }

    @isTest
    public static void NCPMaintenanceTestElse() {
        
        RestRequest req1 = new RestRequest();
        RestResponse res1 = new RestResponse();
        req1.requestURI = '/services/apexrest/NCPMaintenance/'; 
        req1.httpMethod = 'GET';    
        RestContext.request = req1;
        RestContext.response = res1;
        RestContext.request.addHeader('Content-Type', 'application/json');
        
        Test.startTest(); 
        	NCPMaintenance.NCPMaintenance();
        Test.stopTest();
    }
}