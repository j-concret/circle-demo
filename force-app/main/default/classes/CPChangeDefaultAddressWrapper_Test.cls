@Istest
public class CPChangeDefaultAddressWrapper_Test {

     static testMethod void testParse() { 
		String json = '{'+
		''+
		'  \"ChangeDefaultAddress\": ['+
		'    {'+
		'      '+
		'    \"All_Countries\": \"Test\",'+
		'    \"ClientID\": \"Test Full Name\",'+
		'    \"NewAddressID\": \"0010Y00000PEqvHQAT\"'+
		'     '+
		'    },'+
		'    {'+
		'      '+
		'    \"All_Countries\": \"Test\",'+
		'    \"ClientID\": \"Test Full Name\",'+
		'    \"NewAddressID\": \"0010Y00000PEqvHQAT\"'+
		'   '+
		'    },'+
		'    {'+
		'    '+
		'    \"All_Countries\": \"Test\",'+
		'    \"ClientID\": \"Test Full Name\",'+
		'    \"NewAddressID\": \"0010Y00000PEqvHQAT\"'+
		'    },'+
		'    {'+
		'     '+
		'    \"All_Countries\": \"Test\",'+
		'    \"ClientID\": \"Test Full Name\",'+
		'    \"NewAddressID\": \"0010Y00000PEqvHQAT\"'+
		'     '+
		'    }'+
		''+
		'  ]'+
		''+
		'}';
		CPChangeDefaultAddressWrapper obj = CPChangeDefaultAddressWrapper.parse(json);
		System.assert(obj != null);
	}
}