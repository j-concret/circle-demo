@RestResource(urlMapping='/SendAllClientSOs/*')
Global class NCPSendAllClientSO {

    @Httppost
    global static void NCPSendAllClientSO(){

        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        Blob body = req.requestBody;
        res.addHeader('Content-Type', 'application/json');
        String requestString = body.toString();
        NCPSendAllClientSOWra rw = (NCPSendAllClientSOWra)JSON.deserialize(requestString,NCPSendAllClientSOWra.class);
        try {
            list<Access_token__c> At =[select status__c,Access_Token__c from Access_token__c where Access_Token__c=:rw.Accesstoken ];
            //if( at.status__C =='Active'){
            if( at.size() >0) {
                //'Quote - Expired'
                List<Shipment_Order__c> FinalSos = new list<Shipment_Order__c>();
                List<String> include = New list<String> {'Quote - Complete or Incomplete','Shipment - Compliance Pending','Shipment - Tracking','Shipment - Completed'};

                List<Shipment_Order__c> SOs = [SELECT Id,Client_Invoice_Status_Roll_up__c,Shipment_Value_USD__c,ETA_Days_Business__c,ETA_Formula__c,tax_cost__C,Potential_Liability_cover_fee_USD__c,Sub_Status_Update__c,Mapped_Shipping_status__c,banner_feed__c,Final_Delivery_Date__c,ETA_Text_per_Status__c,Cost_Estimate_Acceptance_Date__c,Minimum_tax_range__c,ETA_days__c,Maximum_tax_range__c,Total_Cost_Range_Max__c,Total_Cost_Range_Min__c,POD_Date__c,CreatedById,CreatedBy.Name,ETA__C,Roll_out__c,Roll_out__r.name,Buyer_Account__r.id,Buyer_Account__r.Name,Destination__c,Ship_From_Country__c,Service_Type__c,NCP_Shipping_Status__c,Expiry_Date__c,Total_Invoice_Amount__c,of_Line_Items__c,Final_Deliveries_New__c,Invoice_Payment__c,Shipping_Documents__c,Customs_Compliance__c,Pick_up_Coordination__c,Client_Reference__c,Client_Reference_2__c,name,CreatedDate,Shipping_Status__c,NCP_Quote_Reference__c,Client_Contact_for_this_Shipment__r.name,Shipment_Status_Notes__c,Client_Task__c FROM Shipment_Order__c WHERE Account__c =:rw.AccountID and NCP_Shipping_Status__c in:include order by createddate desc limit 2000 ];

                List<Shipment_Order__c> SOs1 = [SELECT Id,Client_Invoice_Status_Roll_up__c,Shipment_Value_USD__c,ETA_Days_Business__c,ETA_Formula__c,tax_cost__C,Potential_Liability_cover_fee_USD__c,Sub_Status_Update__c,Mapped_Shipping_status__c,banner_feed__c,Final_Delivery_Date__c,ETA_Text_per_Status__c,Cost_Estimate_Acceptance_Date__c,Minimum_tax_range__c,ETA_days__c,Maximum_tax_range__c,Total_Cost_Range_Max__c,Total_Cost_Range_Min__c,POD_Date__c,CreatedById,CreatedBy.Name,ETA__C,Roll_out__c,Roll_out__r.name,Buyer_Account__r.id,Buyer_Account__r.Name,Destination__c,Ship_From_Country__c,Service_Type__c,NCP_Shipping_Status__c,Expiry_Date__c,Total_Invoice_Amount__c,of_Line_Items__c,Final_Deliveries_New__c,Invoice_Payment__c,Shipping_Documents__c,Customs_Compliance__c,Pick_up_Coordination__c,Client_Reference__c,Client_Reference_2__c,name,CreatedDate,Shipping_Status__c,NCP_Quote_Reference__c,Client_Contact_for_this_Shipment__r.name,Shipment_Status_Notes__c,Client_Task__c FROM Shipment_Order__c WHERE Account__c =:rw.AccountID and NCP_Shipping_Status__c='Quote - Expired'  order by createddate desc limit 500 ];
                FinalSos.addall(SOs);
                FinalSos.addall(SOs1);

                res.responseBody = Blob.valueOf(JSON.serializePretty(FinalSos));
                res.addHeader('Content-Type', 'application/json');
                res.statusCode = 200;

                API_Log__c Al = New API_Log__c();
                Al.Account__c=rw.AccountID;
                Al.Login_Contact__c=rw.ContactID;
                Al.EndpointURLName__c='NCP-SendAllClientShipmentOrders';
                Al.Response__c='Success - List of shipment orders are sent';
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al;
            }
            else{
                JSONGenerator gen = JSON.createGenerator(true);
                gen.writeStartObject();

                gen.writeFieldName('Success');
                gen.writeStartObject();

                gen.writeStringField('Response', 'Access token Expired');

                gen.writeEndObject();
                gen.writeEndObject();
                String jsonData = gen.getAsString();
                res.responseBody = Blob.valueOf(jsonData);
                res.addHeader('Content-Type', 'application/json');
                res.statusCode = 400;
            }
        }
        catch(exception e) {
            res.responseBody = Blob.valueOf(e.getMessage());
            res.addHeader('Content-Type', 'application/json');
            res.statusCode = 500;
            API_Log__c Al = New API_Log__c();
            Al.Account__c=rw.AccountID;
            Al.Login_Contact__c=rw.ContactId;
            Al.EndpointURLName__c='NCP-SendAllClientShipmentOrders';
            Al.Response__c=e.getMessage();
            Al.StatusCode__c=string.valueof(res.statusCode);
            Insert Al;
        }
    }
}