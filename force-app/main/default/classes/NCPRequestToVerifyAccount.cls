@RestResource(urlMapping='/VerifyAccount/*')
Global class NCPRequestToVerifyAccount {
    @Httppost
    global static void NCPRequestToVerifyAccount(){
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        Blob body = req.requestBody;
        res.addHeader('Content-Type', 'application/json');
        String requestString = body.toString();
        
        
        NCPRequestToVerifyAccountwra rw = (NCPRequestToVerifyAccountwra)JSON.deserialize(requestString,NCPRequestToVerifyAccountwra.class);
        try {
            List<Access_token__c> at = [SELECT status__c,Access_Token__c FROM Access_token__c WHERE Access_Token__c =: rw.Accesstoken and status__c = 'Active' LIMIT 1];
            If(!at.isEmpty()){
                
                Account rec = [SELECT id,Name,vetted_Account__C,Owner.Name,Owner.Email FROM Account where id =:rw.AccountID  LIMIT 1];
                Contact con = [select Id,name,email,phone from contact where accountid=:rec.Id AND Id=:rw.ContactID limit 1];

                if(rec !=null && con!=null){
                   // rec.vetted_Account__C = TRUE;
                   // Update Rec;
                    
                    NCPRequestToVerifyAccount.sendemail(rec,con);
                    res.responseBody = Blob.valueOf(JSON.serializePretty(rec));
                    res.addHeader('Content-Type', 'application/json');
                    res.statusCode = 200;
                    API_Log__c Al = New API_Log__c();
                    Al.EndpointURLName__c='NCPRequestToVerifyAccount';
                    Al.Response__c='Success - Account verification Email Sent';
                    Al.StatusCode__c=string.valueof(res.statusCode);
                    Insert Al;
                }
                else {
                    
                    JSONGenerator gen = JSON.createGenerator(true);
                    gen.writeStartObject();
                    gen.writeFieldName('Success');
                    gen.writeStartObject();
                    gen.writeStringField('Message','Invalid AccountID');
                    gen.writeEndObject();
                    gen.writeEndObject();
                    String jsonData = gen.getAsString();
                    res.responseBody = Blob.valueOf(jsonData);
                    res.addHeader('Content-Type', 'application/json');
                    res.statusCode = 400;
                    
                    
                    
                }
            } 
            
        }
        catch(Exception e){
            String ErrorString ='Something went wrong, please contact support_SF@tecex.com';
            res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
            res.addHeader('Content-Type', 'application/json');
            res.statusCode = 500;
            
            API_Log__c Al = New API_Log__c();
            
            Al.EndpointURLName__c='NCPRequestToVerifyAccount';
            Al.Response__c=e.getMessage()+' at line number: '+e.getLineNumber();
            Al.StatusCode__c=string.valueof(res.statusCode);
            Insert Al;
            
            
        }
        
        
        
    }
    
    public static void sendemail(Account Acc,Contact con){
        
        String baseUrl = URL.getSalesforceBaseUrl().toExternalForm();
        
        Messaging.SingleEmailMessage semail = new Messaging.SingleEmailMessage();
        //String[] sendingTo = new String[]{'luhar.aadil@concret.io'};
        String[] sendingTo = new String[]{Acc.owner.Email};
        String[] sendingcc = new String[]{'pietmana@tecex.com','jacquesb@tecex.com'};
        semail.setToAddresses(sendingTo);
        semail.setCcAddresses(sendingcc);  
        semail.setSubject('URGENT! Client requested account vetting');
        semail.setHtmlBody('Hi '+Acc.Owner.Name+',<br/><br/> '+con.Name+' from  <a href='+baseUrl+'/'+acc.Id+'>'+Acc.Name+'</a> , has registered on the TecEx mobile App.<br/><br/>'+
                           'They have requested that you verify their account so that they can get full/unrestircted access on the App.<br/><br/>'+
                           'Please contact '+con.name+' to sign our contract and complete the KYC & vetting process.<br/>'+
                           'Email : '+con.Email+'<br/>'+
                           'Tel : '+con.Phone+'<br/>');
        
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] {semail});
        
    }
    
    
    
    
}