@isTest
public class ObjectsCustomLookUpControllerTest {
	@testSetup static void setup() {
       
        Account account2 = new Account (name='Test Client', Type ='Client'); 
        insert account2;
        
        Contact contact = new Contact (LastName='Test Contact' , AccountId = account2.Id); 
        insert contact;
        
        
        User u1 = new User(
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'TecEx Branch Manager'].Id,
            LastName = 'Berkeley1',
            Email = 'puser1000@amamama.com',
            Username = 'puser0001@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST1',
            FirstName='Test',
            Title = 'title1',
            Alias = 'alias1',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US'
        );
        insert u1;
     	
    
        
    }
    
    @isTest static void testFetchLookUpValues() {
        Test.startTest();
        List<Account> accList = ObjectsCustomLookUpController.fetchLookUpValues('Test','Account');
        Test.stopTest();
        System.assert((accList[0].Name).containsIgnoreCase('Test'));
    }
    @isTest
    static void testFetchLookUpValues1() {
        Test.startTest();
        List<User> accList = ObjectsCustomLookUpController.fetchLookUpValues('Test','User;TecEx Branch Manager');
        Test.stopTest();
        System.assert((accList[0].Name).containsIgnoreCase('Test'));
    }
    @isTest static void fetchLookUpValuesForRelatedObject() {
        Test.startTest();
        Account account =[select Id from Account limit 1];
        List<Contact> conList = ObjectsCustomLookUpController.fetchLookUpValuesForRelatedObject('Test','Contact', account.Id);
        Test.stopTest();
        System.assert((conList[0].Name).containsIgnoreCase('Test'));
    }
    
}