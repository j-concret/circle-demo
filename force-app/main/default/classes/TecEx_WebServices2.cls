global class TecEx_WebServices2 {

  webservice static String sendEmailSummary(List<Shipment_Order__c> SOList, String SupplierId, String newEmailAddress){
      Shipping_Order_Email_Summary_Supplier.sendEmailSummary(null,SupplierId,newEmailAddress);
      return 'Email Sent';
    }
}