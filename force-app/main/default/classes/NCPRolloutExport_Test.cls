@IsTest
public class NCPRolloutExport_Test {

       static testMethod void  TestForNCPRollOutEmails(){
       
        Account acc = new Account(Name = 'Acme Test');
        
        Insert acc;
        
        Contact contact = new Contact ( lastname ='Testing Individual',Client_Notifications_Choice__c='Opt-In',  AccountId = acc.Id,email='abc@def.com');
           insert contact; 
           Roll_Out__c new_roll_out = new Roll_Out__c( Client_Name__c=acc.id,
                                                          Contact_For_The_Quote__c=contact.id,
                                                          Shipment_Value_in_USD__c=200,
                                                          Destinations__c='Brazil',
                                                          Number_of_Cost_Estimates__c =0
                                                         );
     
      	Insert new_roll_out;
           
           
       
       PageReference testPage = Page.NCPRolloutExport; 
       Test.setCurrentPage(testPage);
       testPage.getParameters().put('Id', String.valueOf(new_roll_out.Id));
        
       ApexPages.StandardController sc = new  ApexPages.StandardController(new_roll_out);     
       NCPRolloutExport ext = new NCPRolloutExport(sc);         
         
      
          /* ApexPages.currentPage().getParameters().put('Id',String.valueOf(new_roll_out.Id));
			
			ApexPages.StandardController sc = new ApexPages.StandardController(new_roll_out);
			NCPRolloutExport sssController = new NCPRolloutExport(sc);
			sssController.save();*/
         //  NCPRolloutExport (ApexPages.StandardController controller);
       }
    
}