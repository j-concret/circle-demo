@isTest
public class NCPGetAllcases_Test {
    
    public static testMethod void NCPSORelatedObjects_Test(){
        
        //Test Data 
        
        Access_token__c at = new Access_token__c();
        at.Status__c = 'Active';
        at.Access_Token__c = '1234';
        
        Insert at;
        
        Case c = new Case();
            c.Subject = 'Test Subject';
            c.Status = 'Waiting';
            c.Description = 'Description';
            insert c;
        
        //Json Body
        String json = '{"AccessToken":"'+at.Access_Token__c+'"}';
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
        req.requestURI = '/services/apexrest/GetAllcases/'; 
        req.httpMethod = 'POST';
        req.addHeader('Content-Type', 'application/json');
        req.requestBody = Blob.valueof(json);
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
        NCPGetAllcases.NCPSORelatedObjects();
        Test.stopTest(); 
    }
    
    public static testMethod void NCPSORelatedObjects_Test1(){
        
        //Test Data 
        Id shipmentOrderRecTypeId = Schema.SObjectType.shipment_order__c.getRecordTypeInfosByName().get('Cost Estimate').getRecordTypeId();
        
        Access_token__c at = new Access_token__c();
        at.Status__c = 'Active';
        at.Access_Token__c = '1234';
        
        Insert at;
        Id accountRecTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Freight Supplier').getRecordTypeId();
 
        Account acc = new Account(Name = 'Test Supplier',
                                  RecordTypeId = accountRecTypeId);
        Insert acc;
        Account acc1 = new Account(Name = 'DHL',
                                  Supplier_type__c = 'Logistics Provider');
        Insert acc1;
        Contact con = new Contact(LastName = 'Test contact',AccountId = acc.Id);
        Insert con;        
        
        CpaRules__c cpaRule = new CpaRules__c(Country__c = 'Finland',
                                              Criteria__c = 'Default');
        Insert cpaRule;
        
        IOR_Price_List__c priceList = new IOR_Price_List__c(Destination__c = 'Finland',
                                                            Client_Name__c = acc.Id,
                                                            IOR_Fee__c = 10);
        Insert priceList;
        
        Country__c country = new Country__c(Name = 'United States',Country__c = 'United States');
        Insert country;
        
        CPA_v2_0__c cpaV2 = new CPA_v2_0__c(Name = 'Test',
                                            Country_Applied_Costings__c = true);
        Insert cpaV2;
        
        CPA_v2_0__c cpa = new CPA_v2_0__c(Name = 'Test Cpa',
                                          Freight_Only_Lane__c = true,
                                          Inactive_Lane__c = false,
                                          Country__c = country.Id,
                                          Supplier__c = acc.Id,
                                          Related_Costing_CPA__c = cpaV2.Id,
                                          Preferred_Freight_Forwarder__c = acc1.Id);
        Insert cpa;
        
        shipment_order__c shipment = new shipment_order__c(RecordTypeId = shipmentOrderRecTypeId,
                                                           CPA_v2_0__c = cpa.Id,
                                                           Destination__c = 'Finland',
                                                           IOR_Price_List__c = priceList.Id,
                                                           Account__c = acc.Id,
                                                           CPA_Costings_Added__c = true);
        Insert shipment;
        
        Profile prof = [select id from profile where name LIKE '%Standard%']; 
        
        User user = new User();
        user.firstName = 'test1_case';
        user.lastName = 'test2_case';
        user.profileId = prof.id;
        user.username = 'test121_case@test.com';
        user.email = 'test@test.com';
        user.Alias = 'test572';
        user.TimeZoneSidKey = 'America/Los_Angeles';
        user.LocaleSidKey = 'en_US';
        user.EmailEncodingKey = 'ISO-8859-1';
        user.LanguageLocaleKey = 'en_US';
        insert user;
        
        system.runAs(user){
            Case c = new Case();
            c.Contactid = con.Id;
            c.Subject = 'Test Subject';
            c.Status = 'Waiting';
            c.Description = 'Description';
            c.AccountId = acc.Id;
            c.Shipment_Order__c = shipment.Id;
            insert c;
        }
        
        //Json Body
        String json = '{"ContactID":"'+con.Id+'","AccessToken":"'+at.Access_Token__c+'"}';
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
        req.requestURI = '/services/apexrest/GetAllcases/'; 
        req.httpMethod = 'POST';
        req.addHeader('Content-Type', 'application/json');
        req.requestBody = Blob.valueof(json);
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
        NCPGetAllcases.NCPSORelatedObjects();
        Test.stopTest(); 
    }
}