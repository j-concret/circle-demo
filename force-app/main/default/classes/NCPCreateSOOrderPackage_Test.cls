@isTest
public class NCPCreateSOOrderPackage_Test {
	
    public static testMethod void NCPCreateSOOrderPackage_Test(){
        
        //Test Data
        Id shipmentOrderRecTypeId = Schema.SObjectType.shipment_order__c.getRecordTypeInfosByName().get('Cost Estimate').getRecordTypeId();
        
        CpaRules__c cpaRule = new CpaRules__c();
        cpaRule.Country__c = 'Finland';
        cpaRule.Criteria__c = 'Default';
        
        Insert cpaRule;
        
        Access_token__c at = new Access_token__c();
        at.Status__c = 'Active';
        at.Access_Token__c = '123';
        
        Insert at;
        
        Account acc = new Account(Name = 'TecEx Prospective Client');
        
        Insert acc;
        
        IOR_Price_List__c priceList = new IOR_Price_List__c();
        priceList.Destination__c = 'Finland';
        priceList.Client_Name__c = acc.Id;
        priceList.IOR_Fee__c = 10;
        
        Insert priceList;
        
        shipment_order__c shipment = new shipment_order__c();
        shipment.RecordTypeId = shipmentOrderRecTypeId;
        shipment.Destination__c = 'Finland';
        shipment.IOR_Price_List__c = priceList.Id;
        shipment.Shipment_Value_USD__c = 600;
        shipment.Account__c = acc.Id;
        
        Insert shipment;
        
        //Json Body
        String json = '{"Accesstoken":"'+at.Access_Token__c+'","AccountID":"'+acc.Id+'","ShipmentOrder_Packages" : [{"SOID":"'+shipment.Id+'",'+
            +'"Weight_Unit":"KGs","Dimension_Unit":"CMs","Packages_of_Same_Weight":"10","Length":"10.2","Height":"10.4","Breadth":"10.6","Actual_Weight":"100","LithiumBatteries":true}]}';
    
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
        req.requestURI = '/services/apexrest/CreateSOOrderPackage'; 
        req.httpMethod = 'Post';
        req.addHeader('Content-Type', 'application/json');
        req.requestBody = Blob.valueof(json);
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
        NCPCreateSOOrderPackage.NCPCreateSOOrderPackage();
        Test.stopTest(); 
    }
    
    //Covering catch exception
    public static testMethod void NCPCreateSOOrderPackage_Test1(){
        
        //Test Data
        Id shipmentOrderRecTypeId = Schema.SObjectType.shipment_order__c.getRecordTypeInfosByName().get('Cost Estimate').getRecordTypeId();
        
        CpaRules__c cpaRule = new CpaRules__c();
        cpaRule.Country__c = 'Finland';
        cpaRule.Criteria__c = 'Default';
        
        Insert cpaRule;
        
        Access_token__c at = new Access_token__c();
        at.Status__c = 'Active';
        at.Access_Token__c = '123';
        
        Insert at;
        
        Account acc = new Account(Name = 'TecEx Prospective Client');
        
        Insert acc;
        
        //Json Body
        String json = '{"Accesstoken":"'+at.Access_Token__c+'","AccountID":"'+acc.Id+'","ShipmentOrder_Packages" : [{"SOID":"Null",'+
            +'"Weight_Unit":"KG","Dimension_Unit":"CMs","Packages_of_Same_Weight":"10","Length":"10.2","Height":"10.4","Breadth":"10.6","Actual_Weight":"100","LithiumBatteries":true}]}';
    
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
        req.requestURI = '/services/apexrest/CreateSOOrderPackage'; 
        req.httpMethod = 'Post';
        req.addHeader('Content-Type', 'application/json');
        req.requestBody = Blob.valueof(json);
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
        NCPCreateSOOrderPackage.NCPCreateSOOrderPackage();
        Test.stopTest(); 
    }
}