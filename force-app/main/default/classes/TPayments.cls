public class TPayments {
    
  //  public string result{set;get;}
  //  public string resbody{set;get;}
  //  public string resstatus{set;get;}
  //  public List<Invoice_New__c> opps{set;get;}
  //  public String supplierRTypeId = Schema.Sobjecttype.Invoice_New__c.getRecordTypeInfosByDeveloperName().get('Supplier').getRecordTypeId();
    
    @future(callout = True)
    Public static void checkvalidation(){
         submit();
  /*  
     string result;
     string resbody;
     string resstatus;
     List<Invoice_New__c> opps= new List<Invoice_New__c>(); 
     String supplierRTypeId = Schema.Sobjecttype.Invoice_New__c.getRecordTypeInfosByDeveloperName().get('Supplier').getRecordTypeId();
        system.debug('Inside con');
        
        Map<String, String> xmlDataMap = new Map<String,String>();
      //  String masterKey = 'j2O2HSydXz/YqW6fLD1ZDXupBI8Q6yTf5euM3dRZD7IYfwodhvC12N8M/Gc5lckO'; //Sandbox
         String masterKey = '6/AruxtA1M+gP+g5fZRtpYm0jXJdaQWZNv6k8oJu6DBYEH5papLWbnJGtc3o1SVg'; // Production
        String Payername = 'TecEx';
        
        Dom.Document doc = new Dom.Document();
        dom.XmlNode envelope = doc.createRootElement('soap:Envelope', null, null);
        envelope.setAttribute('xmlns:xsi','http://www.w3.org/2001/XMLSchema-instance');
        envelope.setAttribute('xmlns:xsd','http://www.w3.org/2001/XMLSchema');
        envelope.setAttribute('xmlns:soap','http://schemas.xmlsoap.org/soap/envelope/');
        dom.XmlNode body = envelope.addChildElement('soap:Body',null,null);
        
        //Dom.Xmlnode tipaltiPaymentsOrdersnode = PayeePayable.addChildElement('tipaltiPaymentsOrders', null, null);
        
        AggregateResult[] res = [SELECT Account__c, SUM(Invoice_amount_USD__c)sum 
                                 FROM Invoice_New__c 
                                 where Invoice_Status__c = 'FM Approved' 
                                 AND RecordTypeId = :supplierRTypeId
                                 AND Due_Date__c = TODAY
                                 AND Submitted_to_Tipalti__c = false 
                                 AND Account__r.Eligible_for_Tipalti_Autopay__c = true 
                                 GROUP BY Account__c];
       Double sum1 = (res!=null && !res.isEmpty()) ? (double)res[0].get('sum') : 1.11;
       integer sum2 = sum1.intValue();
        
       // String Amount = (res!=null && !res.isEmpty()) ? String.valueOf((double)res[0].get('sum')) : '';
        String Amount = string.valueOf(sum2);
        
        Decimal Timestampheader1 = datetime.now().gettime()/1000;
        System.debug('Timestampheader1'+Timestampheader1);
        String untime=string.valueOf(Timestampheader1);
        
        String params= Payername+untime+Amount;
        
        String FinalKey = generateHmacSHA256Signature(params,masterKey);   
        
        for(AggregateResult ag:res){
          //  dom.XmlNode TipaltiPaymentOrderItemnode=tipaltiPaymentsOrdersnode.addChildElement('TipaltiPaymentOrderItem',null, null);
            Datetime Timestampheader2 = datetime.now();
            String Refcodefinal = Timestampheader2.format('MM-dd-yyyy-mm-ss');
            id accid=(id)ag.get('Account__c');
            double amt=(double)ag.get('sum');
        			Dom.Xmlnode PayeePayable = body.addChildElement('PayeePayable', null, null);
       				PayeePayable.setAttribute('xmlns','http://Tipalti.org/');
        			Dom.Xmlnode PayerNamenode = PayeePayable.addChildElement('payerName', null, null);
        			PayerNamenode.addTextNode(Payername) ; 
            		PayeePayable.addChildElement('Idap', null, null).addTextNode(accid);
            		PayeePayable.addChildElement('Amount', null, null).addTextNode(string.valueof(amt));
             		Decimal Timestampheader = datetime.now().gettime()/1000;
        
        			PayeePayable.addChildElement('timeStamp', null, null).addTextNode(string.valueof(Timestampheader));
        			PayeePayable.addChildElement('key', null, null).addTextNode(FinalKey);
           
        }
        
            
        String xmlString =doc.toXmlString();
        System.debug('xmlString =' + xmlString);
        result=xmlString;
        Integer strlength=xmlString.length();
        string length=string.valueOf(strlength);
        
          API_Log__c apiLog = new API_Log__c (Response__c = xmlString, Calling_Method_Name__c  = 'Before Validation Check, XML');
        insert apiLog;
   /*     
        Http P=new Http();
        HttpRequest req = new HttpRequest();
        
      //  req.setEndpoint('https://api.sandbox.tipalti.com/v9/payerfunctions.asmx'); //Sandbox
        req.setEndpoint('https://api.tipalti.com/v9/PayerFunctions.asmx'); // Production
        req.setMethod('POST');
        req.setHeader('Content-Type','text/xml;charset=utf-8');
        req.setHeader('Content-Length', length);
        req.setHeader('SOAPAction', 'http://Tipalti.org/ProcessPayments');
        req.setBody(xmlString);
        
        HttpResponse response = P.send(req);
        
        system.debug('status code is >>>>'+ response.getStatusCode() + ' ' + response.getStatus());
        system.debug('status code is >>>>'+ response.getStatusCode() + ' ' + response.getStatus());
        System.debug('Body is >>>>'+response.getBody());
        
        if (response.getStatus()=='OK') {
            System.debug('inside construction');
            submit();
            generateAPILogs(response,'checkvalidation success Response');
            
        } else {
            System.debug('Body is >>>>'+response.getBody());
            system.debug('status code is >>>>'+ response.getStatusCode() + ' ' + response.getStatus());
            resbody=response.getBody();
            resstatus=response.getStatus();
            generateAPILogs(response,'checkvalidation failure Response');
        }   
        API_Log__c apiLog = new API_Log__c (Response__c = xmlString, Calling_Method_Name__c  = 'Before Validation Check, XML');
        insert apiLog;
   */     
    }
    
   // @future
    Public static  void submit(){
     
     string result;
     string resbody;
     string resstatus;
     //List<Invoice_New__c> opps= new List<Invoice_New__c>(); 
     String supplierRTypeId = Schema.Sobjecttype.Invoice_New__c.getRecordTypeInfosByDeveloperName().get('Supplier').getRecordTypeId();
     
        
        Map<String, String> xmlDataMap = new Map<String,String>();
      //  String masterKey = 'j2O2HSydXz/YqW6fLD1ZDXupBI8Q6yTf5euM3dRZD7IYfwodhvC12N8M/Gc5lckO'; //sandbox
         String masterKey = '6/AruxtA1M+gP+g5fZRtpYm0jXJdaQWZNv6k8oJu6DBYEH5papLWbnJGtc3o1SVg'; // Production
        String Payername = 'TecEx';
        //DateTime dt = Datetime.now();
        //String GroupTitle = dt.format('yyyMMdd');
        String paymentGroupTitle = 'TecExPayments';
        System.debug('paymentGroupTitle'+paymentGroupTitle);
        
        Decimal Timestampheader1 = datetime.now().gettime()/1000;
        System.debug('Timestampheader1-->'+Timestampheader1);
        String untime=string.valueOf(Timestampheader1);
       
        	        
        String params= Payername+untime+paymentGroupTitle;
        String FinalKey = generateHmacSHA256Signature(params,masterKey);
        System.debug('params-->'+params);
        System.debug('FinalKey-->'+FinalKey);
        
        //opps=new list<Invoice_New__c>();
        
        map<id,string> accidoppids=new map<id,string>();              
        list<Invoice_New__c> opps=[select id,Account__c from Invoice_New__c];
        if(opps.size()>0){
            for(Invoice_New__c op:opps){
                if(accidoppids.containskey(op.Account__c)){
                    string opstr=accidoppids.get(op.Account__c)+';'+op.id;
                    accidoppids.remove(op.Account__c);
                    accidoppids.put(op.Account__c,opstr);
                }else{
                    accidoppids.put(op.Account__c,op.id);  
                }
            }
        }
        
        
        Dom.Document doc = new Dom.Document();
        dom.XmlNode envelope = doc.createRootElement('soap:Envelope', null, null);
        envelope.setAttribute('xmlns:xsi','http://www.w3.org/2001/XMLSchema-instance');
        envelope.setAttribute('xmlns:xsd','http://www.w3.org/2001/XMLSchema');
        envelope.setAttribute('xmlns:soap','http://schemas.xmlsoap.org/soap/envelope/');
        
        dom.XmlNode body = envelope.addChildElement('soap:Body',null,null);
        
        Dom.Xmlnode ProcessPaymentsNode = body.addChildElement('ProcessPayments', null, null);
        ProcessPaymentsNode.setAttribute('xmlns','http://Tipalti.org/');
        Dom.Xmlnode PayerNamenode = ProcessPaymentsNode.addChildElement('payerName', null, null);
        PayerNamenode.addTextNode(Payername) ; 
        
        Dom.Xmlnode paymentGroupTitlenode = ProcessPaymentsNode.addChildElement('paymentGroupTitle', null, null);
        paymentGroupTitlenode.addTextNode(paymentGroupTitle); 
        
        Dom.Xmlnode tipaltiPaymentsOrdersnode = ProcessPaymentsNode.addChildElement('tipaltiPaymentsOrders', null, null);
        AggregateResult[] res = [SELECT Account__c, SUM(Invoice_amount_USD__c)sum 
                                 FROM Invoice_New__c 
                                 where Invoice_Status__c = 'FM Approved' 
                                 AND RecordTypeId = :supplierRTypeId
                                 AND Due_Date__c = TODAY
                                 AND Submitted_to_Tipalti__c = false 
                                 AND Account__r.Eligible_for_Tipalti_Autopay__c = true 
                                 GROUP BY Account__c];
        for(AggregateResult ag:res){
            dom.XmlNode TipaltiPaymentOrderItemnode=tipaltiPaymentsOrdersnode.addChildElement('TipaltiPaymentOrderItem',null, null);
            Datetime Timestampheader2 = datetime.now();
            String Refcodefinal = Timestampheader2.format('MM-dd-mm-ss');
            system.debug('Refcodefinal-->'+Refcodefinal);
            id accid=(id)ag.get('Account__c');
            double amt=(double)ag.get('sum');
            TipaltiPaymentOrderItemnode.addChildElement('Idap', null, null).addTextNode(accid);
            TipaltiPaymentOrderItemnode.addChildElement('Amount', null, null).addTextNode(string.valueof(amt));
            TipaltiPaymentOrderItemnode.addChildElement('RefCode', null, null).addTextNode(Refcodefinal);
            //string str=accidoppids.get(accid);
            TipaltiPaymentOrderItemnode.addChildElement('IgnoreThresholds', null, null).addTextNode('true');
            TipaltiPaymentOrderItemnode.addChildElement('Currency', null, null).addTextNode('USD');
          //  TipaltiPaymentOrderItemnode.addChildElement('IncomeType', null, null).addTextNode('null');
            
        }
       // Decimal Timestampheader = datetime.now().gettime()/1000;
        
        ProcessPaymentsNode.addChildElement('timeStamp', null, null).addTextNode(string.valueof(Timestampheader1));
        ProcessPaymentsNode.addChildElement('key', null, null).addTextNode(FinalKey);     
        String xmlString =doc.toXmlString();
        
        
        System.debug('xmlString =' + xmlString);
        result=xmlString;
        Integer strlength=xmlString.length();
        string length=string.valueOf(strlength);
        Http P=new Http();
        HttpRequest req = new HttpRequest();
        
        //req.setEndpoint('https://api.sandbox.tipalti.com/v9/payerfunctions.asmx'); //Sandbox
        req.setEndpoint('https://api.tipalti.com/v9/PayerFunctions.asmx'); //Production
        req.setMethod('POST');
        req.setHeader('Content-Type','text/xml;charset=utf-8');
        req.setHeader('Content-Length', length);
        req.setHeader('SOAPAction', 'http://Tipalti.org/ProcessPayments');
        req.setBody(xmlString);
        
        
        
        HttpResponse response = P.send(req);
        system.debug('status code is >>>>'+ response.getStatusCode() + ' ' + response.getStatus());
        system.debug('status code is >>>>'+ response.getStatusCode() + ' ' + response.getStatus());
        System.debug('Body is >>>>'+response.getBody());
        
        if (response.getStatus()=='OK') {
             API_Log__c apiLog1 = new API_Log__c (Response__c = xmlString, Calling_Method_Name__c  = 'Submited Payment XML - in Success block');
            insert apiLog1;
       
            System.debug('The status code returned was expected: ');
            system.debug('status code is >>>>'+ response.getStatusCode() + ' ' + response.getStatus());
            System.debug('Body is >>>>'+response.getBody());
            resbody=response.getBody();
            resstatus=response.getStatus();
            generateAPILogs(response,'Payment submitted - success Response');
            markInvoiceTipulti();
        } else {
             API_Log__c apiLog1 = new API_Log__c (Response__c = xmlString, Calling_Method_Name__c  = 'Submited Payment XML - in Fail block');
            insert apiLog1;
            System.debug('Body is >>>>'+response.getBody());
            system.debug('status code is >>>>'+ response.getStatusCode() + ' ' + response.getStatus());
            resbody=response.getBody();
            resstatus=response.getStatus();
            generateAPILogs(response,'Payment submitted -  failure Response');
        }   
        
        //API_Log__c apiLog = new API_Log__c (Response__c = xmlString, Calling_Method_Name__c  = 'Before Submit, XML');
        //insert apiLog;
        
    }
    
    //Generate Signature
    public  static String generateHmacSHA256Signature(String Keyvalue,string ClientsecreteValue) 
    {     
        String algorithmName = 'HmacSHA256';
        Blob hmacData = Crypto.generateMac(algorithmName,  Blob.valueOf(KeyValue), Blob.valueOf(ClientsecreteValue));
        String hextext = EncodingUtil.convertToHex(hmacData);
        
        return hextext;
    }
    
    public static void generateAPILogs(HttpResponse response,String url){
        API_Log__c apiLog = new API_Log__c (StatusCode__c = String.valueOf((Integer)response.getStatusCode()),Response__c = response.getBody(), Calling_Method_Name__c  = url);
        insert apiLog;
    }
    
    public static void markInvoiceTipulti(){
     String supplierRTypeId = Schema.Sobjecttype.Invoice_New__c.getRecordTypeInfosByDeveloperName().get('Supplier').getRecordTypeId();
     
        List<Invoice_New__c> invoiceList = new List<Invoice_New__c>();
        For(Invoice_New__c invoice : [SELECT Account__c,Submitted_to_Tipalti__c
                                      FROM Invoice_New__c 
                                      where Invoice_Status__c = 'FM Approved' 
                                      AND RecordTypeId = :supplierRTypeId
                                      AND Due_Date__c = TODAY
                                      AND Submitted_to_Tipalti__c = false 
                                      AND Account__r.Eligible_for_Tipalti_Autopay__c = true ]){
                                          invoice.Submitted_to_Tipalti__c = true;
                                          invoiceList.add(invoice);       
                                      }
        update invoiceList;
    }
}