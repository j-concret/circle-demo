public with sharing class InvoiceLineItemDeleteAllController {
    
    public InvoiceLineItemDeleteAllController() {
    }
    
    public void deleteAll() {
        Id invoiceParentId = ApexPages.currentPage().getParameters().get('id');
        Part__c[] delLineItems = [SELECT Id FROM Part__c WHERE Shipment_Order__c=:invoiceParentId];
        try {
            delete delLineItems;
        } catch (Exception e) {
            System.debug('Exception = ' + e.getMessage());
        }
    }
}