Global class SupplierCCDEmail {
    
    @InvocableMethod(label='Send a CCD email Alert' description='sends an email to Suppliers to upload CCD docs')
    public static void sendEmail(List<ID> SOIdList) {

        Shipment_Order__c SO = [SELECT Id, Name, SupplierlU__c, CDC__c, CDC__r.Email
                                     FROM Shipment_Order__c
                                    WHERE Id =: SOIdList
                                  ];
       
        List<Contact> conList   = [SELECT Id, LastName, Email, AccountId
                                     FROM Contact
                                    WHERE AccountId = : SO.SupplierlU__c AND Include_in_invoicing_emails__c = true
                                ];
           
                //Retrieve Email template
        EmailTemplate et=[Select id from EmailTemplate where name=:'CCD email to Suppliers'];

        //Create email list
        List<Messaging.SingleEmailMessage> emails = new List<Messaging.SingleEmailMessage>();
        
        Messaging.SingleEmailMessage singleMail   = new Messaging.SingleEmailMessage();
        
        //add template
       singleMail.setTemplateId(et.Id);

       //set target object for merge fields
        singleMail.setTargetObjectId(conList[0].Id);

        singleMail.setwhatId(SO.Id);

        //set to save as activity or not
        singleMail.setSaveAsActivity(true);

        //add address's that you are sending the email to
        //String [] ToAddress= new string[] {conList.Email} ;
        String [] CCAddress= new string[] {SO.CDC__r.Email};
        String [] ToAddress= new string[] {} ;
        for(Contact em : conList){
            ToAddress.add(em.Email);
        }
        
        //set addresses
        singleMail.setCCAddresses(CCAddress);
        singleMail.setToAddresses(ToAddress);

       
       
        //add mail
        emails.add(singleMail);

        //send the message
        Messaging.sendEmail(emails);
    }

}