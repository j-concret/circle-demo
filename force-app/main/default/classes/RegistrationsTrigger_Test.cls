@isTest
public class RegistrationsTrigger_Test {
    
    @isTest
    public static void InsertTest(){
        Id accManufRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Manufacturer').getRecordTypeId();
        
        Account account = new Account (name='Test Manufacturer', Type ='Manufacturer', RecordTypeId = accManufRecordTypeId, Manufacturer_Alias__c = 'Tester,Who' ); 
        INSERT account;
        
        List<Registrations__c> regtList = new List<Registrations__c>();
        regtList.add(new Registrations__c(Name='Aus Regt1', Company_name__c = account.Id, Country__c = 'Australia'));
       // regtList.add(new Registrations__c(Name='Aus Regt4', Company_name__c = account.Id, Country__c = 'Estonia'));
        regtList.add(new Registrations__c(Name='Aus Regt5', Company_name__c = account.Id, Country__c = 'Germany'));
        regtList.add(new Registrations__c(Name='Aus Regt6', Company_name__c = account.Id, Country__c = 'Poland'));
        regtList.add(new Registrations__c(Name='Aus Regt7', Company_name__c = account.Id, Country__c = 'Cyprus'));
        regtList.add(new Registrations__c(Name='Aus Regt8', Company_name__c = account.Id, Country__c = 'United States'));
        regtList.add(new Registrations__c(Name='Aus Regt9', Company_name__c = account.Id, Country__c = 'Czech Republic'));
        regtList.add(new Registrations__c(Name='Aus Regt10', Company_name__c = account.Id, Country__c = 'Spain'));
       // regtList.add(new Registrations__c(Name='Aus Regt11', Company_name__c = account.Id, Country__c = 'European Union (EU Countries)'));
        regtList.add(new Registrations__c(Name='Aus Regt12', Company_name__c = account.Id, Country__c = 'United Kingdom'));
        regtList.add(new Registrations__c(Name='Aus Regt13', Company_name__c = account.Id, Country__c = 'France'));
        regtList.add(new Registrations__c(Name='Aus Regt14', Company_name__c = account.Id, Country__c = 'Latvia'));
        regtList.add(new Registrations__c(Name='Aus Regt15', Company_name__c = account.Id, Country__c = 'Canada'));
        INSERT regtList;
        
        Test.startTest();
        
        List<Registrations__c> regtList2 = new List<Registrations__c>();
        regtList2.add(new Registrations__c(Name='Aus Regt1', Company_name__c = account.Id, Country__c = 'Netherlands'));
        //regtList2.add(new Registrations__c(Name='Aus Regt2', Company_name__c = account.Id, Country__c = 'Poland'));
        //regtList2.add(new Registrations__c(Name='Aus Regt3', Company_name__c = account.Id, Country__c = 'Portugal'));
        //regtList2.add(new Registrations__c(Name='Aus Regt4', Company_name__c = account.Id, Country__c = 'Romania'));
        regtList2.add(new Registrations__c(Name='Aus Regt5', Company_name__c = account.Id, Country__c = 'Singapore'));
        regtList2.add(new Registrations__c(Name='Aus Regt6', Company_name__c = account.Id, Country__c = 'Italy'));
        regtList2.add(new Registrations__c(Name='Aus Regt7', Company_name__c = account.Id, Country__c = 'Japan'));
        
        INSERT regtList2;
        
        	
        Test.stopTest();
    }

}