@IsTest
public class CPPartEditWrapper_Test {
    
    static testMethod void testParse() {
    String json = '{'+
  
    '\"Parts\": ['+
    '    {'+
    '      '+
    '      \"PartNumber\": \"Test\",'+
    '      \"PartDescription\": \"Test\",'+
    '      \"Quantity\": 1,'+
    '      \"UnitPrice\": 1.00,'+
    '      \"HSCode\": \"8456879658\",'+
    '      \"CountryOfOrigin\": \"USA\",'+
        '\"Type_of_Goods\" : \"Refurbished\",'+
    '\"Li_ion_Batteries\" : \"Yes\",'+
    '\"Li_ion_BatteryTypes\" : \"Lithium ION batteries\", '+
    '      \"ECCNNo\": \"1234\"'+
    '     '+
    '    },'+
    '    {'+
    '      '+
    '      \"PartNumber\": \"Test\",'+
    '      \"PartDescription\": \"Test\",'+
    '      \"Quantity\": 1,'+
    '      \"UnitPrice\": 1.00,'+
    '      \"HSCode\": \"8456879658\",'+
    '      \"CountryOfOrigin\": \"USA\",'+
            '\"Type_of_Goods\" : \"Refurbished\",'+
    '\"Li_ion_Batteries\" : \"Yes\",'+
    '\"Li_ion_BatteryTypes\" : \"Lithium ION batteries\", '+
    '      \"ECCNNo\": \"1234\"'+
    '   '+
    '    },'+
    '    {'+
    '    '+
    '      \"PartNumber\": \"Test\",'+
    '      \"PartDescription\": \"Test\",'+
    '      \"Quantity\": 1,'+
    '      \"UnitPrice\": 1.00,'+
    '      \"HSCode\": \"8456879658\",'+
    '      \"CountryOfOrigin\": \"USA\",'+
            '\"Type_of_Goods\" : \"Refurbished\",'+
    '\"Li_ion_Batteries\" : \"Yes\",'+
    '\"Li_ion_BatteryTypes\" : \"Lithium ION batteries\", '+
    '      \"ECCNNo\": \"1234\"'+
    '     '+
    '    },'+
    '    {'+
    '     '+
    '      \"PartNumber\": \"Test\",'+
    '      \"PartDescription\": \"Test\",'+
    '      \"Quantity\": 1,'+
    '      \"UnitPrice\": 1.00,'+
    '      \"HSCode\": \"8456879658\",'+
    '      \"CountryOfOrigin\": \"USA\",'+
            '\"Type_of_Goods\" : \"Refurbished\",'+
    '\"Li_ion_Batteries\" : \"Yes\",'+
    '\"Li_ion_BatteryTypes\" : \"Lithium ION batteries\", '+
    '      \"ECCNNo\": \"1234\"'+
    '     '+
    '    }'+
    '  ]'+
    '}';
    CPPartEditWrapper obj = CPPartEditWrapper.parse(json);
    System.assert(obj != null);
  }
    

}