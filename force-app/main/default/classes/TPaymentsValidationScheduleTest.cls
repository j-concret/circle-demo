@isTest
public class TPaymentsValidationScheduleTest implements HttpCalloutMock{
  private static HttpResponse resp;
    
    public TPaymentsValidationScheduleTest(String testBody,Boolean isTest) {
        resp = new HttpResponse();
        resp.setBody(testBody);        
        if(isTest == true){
            resp.setStatus('OK');
            resp.setStatusCode(200);
        }else{
            resp.setStatus('Error');
            resp.setStatusCode(404);
        }
    }
    
    public HTTPResponse respond(HTTPRequest req) {
        return resp;
    }
    
    public static testMethod void testschedule() {
        
        String testBody = 'This is a test :-)';
        
        HttpCalloutMock mock = new TPaymentsValidationScheduleTest(testBody,false);
        Test.setMock(HttpCalloutMock.class, mock);
        
        Test.startTest();
       // TPaymentSchedule  b = new TPaymentSchedule ();
        TPaymentsValidationSchedule.execute(null); 
        Test.stopTest();
        System.assertEquals(404, resp.getStatusCode());        
        System.assertEquals(resp.getBody(), testBody );
        
    }
}