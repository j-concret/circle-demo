public class RobotSummaryCtrl {
    
    //public static Map<String,Integer> categoryOrder = new Map<String,Order>{''};
    @AuraEnabled
    public static List<RobotSummaryWrapper> getSummary(Id soId){
        try {
            
            Map<String,RobotSummaryWrapper> categories = getPicklistValues('Task','Task_Category__c');
            for(Task tsk : [SELECT Id,Task_Category__c,State__c,Blocks_Approval_to_Ship__c FROM Task WHERE WhatId =:soId AND Master_Task__c != null AND IsNotApplicable__c = false]) {
                if(categories.containsKey(tsk.Task_Category__c) && categories.get(tsk.Task_Category__c).priority > 1) {
                    if(tsk.State__c == 'Client Pending' && tsk.Blocks_Approval_to_Ship__c) {
                        categories.get(tsk.Task_Category__c).priority = 1;//red
                    }else if( categories.get(tsk.Task_Category__c).priority > 2 && (tsk.State__c == 'Not Started' || tsk.State__c == 'TecEx Pending' || tsk.State__c == 'Under Review') &&  tsk.Blocks_Approval_to_Ship__c) {
                        categories.get(tsk.Task_Category__c).priority = 2;//orange
                    }else if(categories.get(tsk.Task_Category__c).priority > 2 && (tsk.State__c == 'Resolved' || (tsk.State__c != 'Resolved' && !tsk.Blocks_Approval_to_Ship__c))) {
                        categories.get(tsk.Task_Category__c).priority = 3;//green
                    }
                }
            }
            return categories.values();
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }
    
    public static Map<String,RobotSummaryWrapper> getPicklistValues(String object_name, String field_name) {
        Map<String,RobotSummaryWrapper> values = new Map<String,RobotSummaryWrapper>();
        String[] types = new String[] {object_name};
            Schema.DescribeSobjectResult[] results = Schema.describeSObjects(types);
        for(Schema.DescribeSobjectResult res : results) {
            for (Schema.PicklistEntry entry : res.fields.getMap().get(field_name).getDescribe().getPicklistValues()) {
                if (entry.isActive() && entry.getValue()!='Onboarding') {
                    values.put(entry.getValue(),new RobotSummaryWrapper(entry.getValue()));
                }
            }
        }
        return values;
    }
    
    public class RobotSummaryWrapper {
        @AuraEnabled public String categoryName;
        @AuraEnabled public Integer priority;
        
        public RobotSummaryWrapper(String name){
            this.categoryName = name;
            this.priority = 3;
        }
    }
    
    @AuraEnabled
    public static Map<String,Object> getShipmentStatus(String soId){
        Map<String,Object> soData = new Map<String,Object>{'status' => 'OK'};
            try{
                List<Shipment_Order__c> soList = [SELECT Shipping_Status__c, Id FROM Shipment_Order__c
                                                  WHERE Id =:soId ];
                soData.put('data',soList);                
            }catch(Exception e){
                soData.put('status','Error');
                soData.put('data',e.getMessage());
            }
        return soData;
    }
}