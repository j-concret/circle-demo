public with sharing class InvoiceLineItemQuickEntryController {
    public String sobjectName 					{get;set;} //Name of the sobject
    public Id parentId                          {get;set;} // Parent record where the records needs to be stored.
    public SObject record						{get;set;} //record to hold the default values
    public List<String> defaultFields			{get;set;} //holds the default fields from the fieldset
    public String fieldSetName					{get;set;} //name of the fieldset which contains order and fields
    public List<String> columns					{get;set;} //holds the columns for the table coming from the fieldset
    public List<String> columnAPINames			{get;set;} //holds the column API names for the table coming from the fieldset
    public Schema.SObjectType  SObjectType      {get;set;} //holds the sobject type, will be used to create instances dynamically
    public String maxRowSize					{get;set;} // Maximum row size to be allowed to copy paste.
    public String columnsSerialized				{get{      //returns the columns in the serialized format
        return JSON.serialize(this.columns);
    }set;}		
    public String columnsApiNameSerialized		{get;set;} //Api name of the columns in serialized form
    public Integer noOfUnsuccessfulRecords		{get;set;} //holds no of unscussessful records
    public Integer noOfSuccessfulRecords  		{get;set;} //holds successful records
    public String  defaultFieldsFieldset        {get;set;} //holds the fieldset of default fields
    
    public InvoiceLineItemQuickEntryController() {
        
    }
    
    public InvoiceLineItemQuickEntryController(ApexPages.StandardSetController controller) {
        try{
            //initialize default values
            noOfSuccessfulRecords = 0;
            noOfUnsuccessfulRecords = 0;
            //Get the sobject name
            sobjectName = ApexPages.currentPage().getParameters().get('sobjectName'); 
            parentId = 	ApexPages.currentPage().getParameters().get('id');
            Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe();
            record = gd.get(sobjectName).newSObject();
            sobjectType = gd.get(sobjectName);
            //get the default getParentFieldSetNamefieldset
            defaultFieldsFieldset = CopyExcelUtil.getDefaultFieldSet(sobjectName);
            
            //get the default fields 
            defaultFields = CopyExcelUtil.getDefaultFields(sobjectName);
            
            //get the fieldset whose columns needs to be shown
            fieldSetName = CopyExcelUtil.getDisplayFieldSetName(sobjectName);
            
            //get the columns needs to be shown on UI
            columns = CopyExcelUtil.getFieldSetFieldNames(sobjectName,fieldSetName);
            
            //serialized columns
            columnAPINames = CopyExcelUtil.getFieldSetFields(sobjectName,fieldSetName);
            
            columnsApiNameSerialized = JSON.serialize(columnAPINames);
            
            maxRowSize = String.valueOf(CopyExcelUtil.getMaxRows(sobjectName));
            
        }catch(Exception e){			
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage()));
        }
    }
    
    
    /**
* @description : This method clears the the no of successful and unsuccessful records. 
* 				 Table is cleared on the UI side.
*/
    public PageReference clearRecords(){
        noOfUnsuccessfulRecords = 0;
        noOfSuccessfulRecords = 0;
        return null;
    }
    
    /**
* @description : This method saves the records. Records will come in json format and then records will be parsed 
*			and sobjects will be created and inserted. 
*/
    public PageReference saveRecords(){
        List<String> errors = new List<String>();
        try{			
            noOfSuccessfulRecords = 0;
            noOfUnsuccessfulRecords = 0;
            List<SObject> sobjectsToBeInserted = new List<SObject>();
            //get the json data
            String jsonData = ApexPages.currentPage().getParameters().get('jsonData');
            Boolean isColumnHeaderCustomized = Boolean.valueOf(ApexPages.currentPage().getParameters().get('isColumnHeaderCustomized'));			
            String newCustomizedColumn = ApexPages.currentPage().getParameters().get('newCustomizedColumn');	            
            
            //serialized columns
            
            if (isColumnHeaderCustomized) {
                // If the columns are hand picked in the UI then get the columns and their api names for the data insert.
                List<Object> newCustomizedColumnObject = (List<Object>)JSON.deserializeUntyped(newCustomizedColumn);
                columns = new List<String>();
                for (Object columnObj : newCustomizedColumnObject) {
                    if (columnObj!= null){
                        columns.add(String.valueOf(columnObj));
                    }
                }
                columnAPINames = CopyExcelUtil.getAPIFieldsForColumnList(sobjectName,fieldSetName,columns);
            } else {
                columns = CopyExcelUtil.getFieldSetFieldNames(sobjectName,fieldSetName);
                columnAPINames = CopyExcelUtil.getFieldSetFields(sobjectName,fieldSetName);
            }
            
            if(String.isNotBlank(jsonData) && jsonData.length() != 0){
                List<Object> sobjects = (List<Object>)JSON.deserializeUntyped(jsonData);
                System.debug('sobjects = ' + sobjects);
                for(Object sobj : sobjects){
                    //if there is any problem with one record then just show the error to user and
                    //proceed for the  next record
                    try{
                        //create sobject record
                        SObject tempSObject = sobjectType.newSObject();
                        String uniqueRef = '';
                        //copy default fields
                        for(String defaultField : defaultFields){
                            tempSObject.put(defaultField,parentId);
                            uniqueRef += tempSObject.get(defaultField) + '_';
                        } 
                        // Map<String,Object> sobjectRecord = (Map<String,Object>) sobj;
                        Map<String, Object> sobjectRecord = new Map<String, Object>();
                        List<Object> tempObjList = (List<Object>)sobj;
                        for (Integer i=0; i< columnAPINames.size();i++) {
                            sobjectRecord.put(columnAPINames.get(i), tempObjList.get(i));
                        }
                        sobjectRecord = CopyExcelUtil.prepareSObject(sobjectRecord,sobjectName);
                        
                        for(String field : sobjectRecord.keySet()){
                            tempSObject.put(field,sobjectRecord.get(field));
                            uniqueRef += sobjectRecord.get(field) + '_';
                        }
                        tempSObject.put('UniqueRef__c',uniqueRef );
                        sobjectsToBeInserted.add(tempSObject);
                    }catch(Exception e){
                        //log the error and maintain count of unsuccessful records
                        noOfUnsuccessfulRecords++;
                        errors.add(e.getMessage());
                    }
                }	
                Database.SaveResult[] srList = Database.insert(sobjectsToBeInserted, false);
                // Iterate through each returned result
                for (Database.SaveResult sr : srList) {
                    if (sr.isSuccess()) {
                        noOfSuccessfulRecords++;
                    }
                }
            } else {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,System.Label.No_Record_Error)); 
            }
            
            if(errors.size()>0){			
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,System.Label.Below_are_the_errors_for_unsuccessful_records));    	
                for(String err : errors){
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,err));    		
                }
            }
            
        }catch(Exception e){			
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage()));
        }
        return null;
    }
}