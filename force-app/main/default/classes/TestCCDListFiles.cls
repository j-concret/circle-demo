@IsTest
public class TestCCDListFiles {
    
    
    @isTest
    public static void testCCDFiles(){
        Test.startTest();
        Customs_Clearance_Documents__c customs_Clearance_Documents = TestCCDUtil.createSetUpData();
        Map<String,Object> recordMap = CCDListFiles.CCDListRecords(customs_Clearance_Documents.Id);
        Test.stopTest();
        System.assertEquals('OK', recordMap.get('status'));
    }   
}