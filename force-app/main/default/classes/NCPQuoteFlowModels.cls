@RestResource(urlMapping='/NCPQuoteFlowModels/*')
Global class NCPQuoteFlowModels {
    
      @Httppost
    global static void NCPQuoteFlowModels(){
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        Blob body = req.requestBody;
        res.addHeader('Content-Type', 'application/json');
        String requestString = body.toString();
        NCPQuoteFlowModelsWra rw  = (NCPQuoteFlowModelsWra)JSON.deserialize(requestString,NCPQuoteFlowModelsWra.class);
        try{
            List<Access_token__c> at = [SELECT status__c,Access_Token__c FROM Access_token__c WHERE Access_Token__c =: rw.Accesstoken and status__c = 'Active' LIMIT 1];

             if(!at.isEmpty()) {
                 String jsonData='Test';
                 list<String> include= new list<String>{'Quote Phase','Quote Phase & After SO'};
                 Set<ID> Ids = New set<ID>();
                 shipment_order__c SOs = [Select id from shipment_Order__C where ID =:rw.SOID ];
                 Map<String,Task> taskMapwithMasters = new Map<String,Task>();
                List<Task> Ts = [SELECT Id,Manually_Override_Inactive_Field__c,Master_Task__r.task_Subject__c,Master_Task__r.Set_Default_to_Inactive__c,LastClientMsgTime__c, LastMessageTime__c, Last_Client_viewed_time__c,createddate,  master_Task__r.client_visible__C,state__c,Subject,inactive__C,IsNotApplicable__c,Description,Task_Category__c,Blocks_Approval_to_Ship__c FROM Task WHERE WhatId =: rw.SOID AND Master_Task__c != null and State__c !='Resolved' and  Master_Task__r.Client_Visible__c=TRUE and Master_Task__r.Displayed_at__c IN:include  ];
                 //LastClientMsgTime__c, LastMessageTime__c, Last_Client_viewed_time__c,createddate
                 for(Task task :Ts) {
                     taskMapwithMasters.put(task.Master_Task__c,task);

                 }

                 /* START : Task inactive logic */

                 Set<Id> prerequisiteIds = new Set<Id>();
                 Map<Id,List<Id> > masterTaskPrerequisiteIds = new Map<Id,List<Id> >();
                 Set<Id> masterTaskInactive = new Set<Id>();
                 for(Master_Task_Prerequisite__c mstPre : [SELECT id,Name, Master_Task_Dependent__c, Master_Task_Prerequisite__c FROM Master_Task_Prerequisite__c WHERE Master_Task_Dependent__c IN: taskMapwithMasters.keySet()]) {
                     prerequisiteIds.add(mstPre.Master_Task_Prerequisite__c);
                     if(masterTaskPrerequisiteIds.containsKey(mstPre.Master_Task_Dependent__c))
                         masterTaskPrerequisiteIds.get(mstPre.Master_Task_Dependent__c).add(mstPre.Master_Task_Prerequisite__c);
                     else
                         masterTaskPrerequisiteIds.put(mstPre.Master_Task_Dependent__c,new List<Id> {mstPre.Master_Task_Prerequisite__c});
                 }

                 for(Task preTask : [SELECT Id,Master_Task__c FROM Task WHERE Master_Task__c IN:prerequisiteIds AND WhatID =:rw.SOID AND State__c !='Resolved' AND State__c !='Not Applicable' AND IsNotApplicable__c = false]) {
                     for(Id mstId : masterTaskPrerequisiteIds.keySet()) {
                         if(!masterTaskInactive.contains(mstId) && masterTaskPrerequisiteIds.get(mstId).contains(preTask.Master_Task__c)) {
                             masterTaskInactive.add(mstId);
                         }
                     }
                 }


                 /* END : Task inactive logic */



                 list<Master_Task_Template__c> mtTemplate = [SELECT Id,State__c, Name, Master_Task__c, Task_Title__c, Displayed_to__c FROM Master_Task_Template__c where Displayed_to__c = 'Client'and Master_Task__c in:taskMapwithMasters.keySet()];

                 List<FeedItem> FeedsonTask= new list<FeedItem>();
                 Map<Id,List<FeedItem> > FeedItemTasks = new Map<Id,List<FeedItem> >();
                 Map<String,String> contenetverIds = new Map<String,String>();
                 Map<String,List<ContentVersion> > contenetDocIds = new map<String,List<ContentVersion> >();
                 Set<ID> FeedIds = New set<ID>();

                 for(FeedItem feedItem: [select Id,ParentId,InsertedById,Title,Body,LinkUrl,createddate,visibility,CommentCount,(select Id,Title,FeedEntityId,RecordId from FeedAttachments) from FeedItem where parentid=:Ts and visibility !='InternalUsers']) {

                     FeedsonTask.add(feedItem);
                     FeedIds.add(feedItem.Id);
                     
                    
                     if(FeedItemTasks.containsKey(feedItem.ParentId)) {

                         List<FeedItem> FeedsList = FeedItemTasks.get(feedItem.ParentId);
                         FeedsList.add(feedItem);
                         FeedItemTasks.put(feedItem.ParentId,FeedsList);

                     }else{

                         List<FeedItem> FeedsList = new  List<FeedItem>();
                         FeedsList.add(feedItem);
                         FeedItemTasks.put(feedItem.ParentId,FeedsList);

                     }
                 }

                 If(!FeedsonTask.isEmpty()){
                     // Feedatt =[SELECT Id, RecordId, FeedEntityId FROM FeedAttachment WHERE FeedEntityId  in: FeedIds];
                     for(FeedAttachment feed:[SELECT Id, RecordId, FeedEntityId FROM FeedAttachment WHERE FeedEntityId in: FeedIds]) {
                         contenetverIds.put(feed.RecordId,Feed.FeedEntityId);       //068
                     }

                     If(!contenetverIds.isEmpty()){
                         for(ContentVersion CD:[SELECT Id,ContentDocumentId,Title from ContentVersion where Id in:contenetverIds.keyset() ]) {
                             If(contenetverIds.containskey(CD.ID)  ){
                                 System.debug('contenetDocIds--> 1 IF  -->'+contenetDocIds);
                                 If(contenetDocIds.containskey(contenetverIds.get(CD.ID))  ){
                                     list<ContentVersion> abc= contenetDocIds.get(contenetverIds.get(cd.ID));
                                     abc.add(cd);
                                     contenetDocIds.put(contenetverIds.get(CD.ID),abc);   //069

                                 }
                                 else{

                                     contenetDocIds.put(contenetverIds.get(CD.ID),new list<ContentVersion> {CD});  //069


                                 }
                             }
                         }


                     }

                 }
                  //List<EntitySubscription> Subscribers = new  List<EntitySubscription>([select SubscriberId  from EntitySubscription where  parentid In: task ]);
                //For(Integer i =0;Subscribers.size()>i;i++){Ids.add(Subscribers[i].SubscriberId);}
                //list<User> Nofifyusers =new list<User>([Select id, Firstname,Lastname from user where id in:Ids ]);
                Map<Id,List<Id>> mapsOfSubs = new  Map<Id,List<Id>>(); 
                Map<Id,List<User>> SubsOnTask = new  Map<Id,List<User>>();
                List<Id> SubsIdList = new List<Id>();
                for(EntitySubscription subs:[select SubscriberId,parentid  from EntitySubscription where  parentid In: Ts ]){
                    //mapsOfSubs.put(subs.SubscriberId,subs.parentid);
                    if(mapsOfSubs.containsKey(subs.parentid)){
                        List<Id> SubsIdList1 = mapsOfSubs.get(subs.parentid);
                        SubsIdList1.add(subs.SubscriberId);
                        mapsOfSubs.put(subs.parentid,SubsIdList1);
                        
                    }else{
                        List<Id> SubsIdList1 = new List<Id>();//mapsOfSubs.get(subs.parentid);
                        SubsIdList1.add(subs.SubscriberId);
                        mapsOfSubs.put(subs.parentid,SubsIdList1);
                    }
                    
                    Ids.add(subs.SubscriberId);
                }
                
                System.debug('Subids'+Ids);
                //For(Integer i =0;Subscribers.size()>i;i++){Ids.add(Subscribers[i].SubscriberId);}
                //list<User> Nofifyusers =new list<User>([Select id, Firstname,Lastname from user where id in:Ids ]);
                 Map<Id,User> NotifyUsers =  new Map<Id,User>(); //[Select Id, Firstname,Lastname,fullphotoUrl from user where Id in:Ids ];
               for(User usr:[Select id, Firstname,Lastname,fullphotoUrl from user where id in:Ids ]){
                  NotifyUsers.put(usr.Id,usr);
                   /* if(mapsOfSubs.containsKey(usr.Id)){
                        if(SubsOnTask.containsKey(mapsOfSubs.get(usr.Id))){
                            
                            List<User> NotifyUsers = SubsOnTask.get(mapsOfSubs.get(usr.Id));
                            NotifyUsers.add(usr);
                            SubsOnTask.put(mapsOfSubs.get(usr.Id),NotifyUsers);  
                            
                        }else{
                            
                            List<User> NotifyUsers = new  List<User>();
                            NotifyUsers.add(usr);
                            SubsOnTask.put(mapsOfSubs.get(usr.Id),NotifyUsers);  
                            
                        }
                    }*/
                    
                }
                

                 List<SO_Travel_History__c>  SOTHList = [SELECT Id, Date_Time__c, Freight_Request__c, Message__c, Status__c, TecEx_SO__c, Zen_Shipment__c, ETA_Text_per_Status__c, Notification_sent__c, Source__c, Location__c, Mapped_Status__c, Description__c, Expected_Date_to_Next_Status__c, Type_of_Update__c, Client_Notifications_Choice__c, Client_Tasks_notification_choice__c FROM SO_Travel_History__c where TecEx_SO__c =: rw.SOID];
                 List<Invoice_New__c> Invlist  =[select Id, Name,Invoice_Type__c,Amount_Outstanding__c,Due_Date__c,Invoice_Status__c,Total_Invoice_Amount_Formula__c, CreatedDate,recordtype.name from Invoice_New__c where shipment_Order__C =: rw.SOID and recordtype.name='Client' ];

                 // select Id , Name , CreatedDate from Invoice_New__c
                 JSONGenerator gen = JSON.createGenerator(true);
                 gen.writeStartObject();
                 //Creating Array & Object - Shipment Order
                 gen.writeFieldName('ShipmentOrder');
                 gen.writeStartObject();
                 if(SOs.Id == null) {gen.writeNullField('Id');} else{gen.writeStringField('Id', SOs.Id);}
                 gen.writeEndObject();
                 //End of Array & Object - Shipment Order

             


              

             


                 If(!Ts.isEmpty()){
                     //Creating Object - Tasks  id,status, description
                     gen.writeFieldName('Task');
                     gen.writeStartArray();
                     for(Task TS1 :Ts) {

                         String title;

                         For(Master_Task_Template__c Mtt: mtTemplate ){
                             if(TS1.State__c == Mtt.State__c  && TS1.Master_Task__c == MTT.Master_Task__c) {
                                 title = Mtt.Task_Title__c;
                             }
                         }

                         if(!TS1.Manually_Override_Inactive_Field__c) {
                             TS1.Inactive__c = TS1.Master_Task__r.Set_Default_to_Inactive__c ? TS1.Master_Task__r.Set_Default_to_Inactive__c : masterTaskInactive.contains(TS1.Master_Task__c);
                         }


                         //Start of Object - Final Delivery
                         gen.writeStartObject();
                         gen.writeStringField('Id', TS1.Id);
                         if(TS1.state__c == null) {gen.writeNullField('State');} else{gen.writeStringField('State', TS1.state__c);}
                         if(TS1.Subject == null) {gen.writeNullField('Subject');} else{gen.writeStringField('Subject', TS1.Subject);}
                         if(ts1.Description == null) {gen.writeNullField('Description');} else{gen.writeStringField('Description', ts1.Description);}
                         if(ts1.Task_Category__c == null) {gen.writeNullField('Category');} else{gen.writeStringField('Category', ts1.Task_Category__c);}
                         if(ts1.master_Task__r.client_visible__C == null) {gen.writeNullField('ClientVisibility');} else{gen.writeBooleanField('ClientVisibility', ts1.master_Task__r.client_visible__C);}
                         if(ts1.Inactive__C == null) {gen.writeNullField('Inactive');} else{gen.writeBooleanField('Inactive', ts1.Inactive__C);}
                         if(ts1.Blocks_Approval_to_Ship__c == null) {gen.writeNullField('BlocksApprovaltoShip');} else{gen.writeBooleanField('BlocksApprovaltoShip', ts1.Blocks_Approval_to_Ship__c);}
                         if(ts1.IsNotApplicable__c == null) {gen.writeNullField('IsNotApplicable');} else{gen.writeBooleanField('IsNotApplicable', ts1.IsNotApplicable__c);}
                         if(title == null) {gen.writeNullField('TemplateTitle');} else{gen.writeStringField('TemplateTitle', title);}
                        //  if(title == null) {gen.writeNullField('TemplateTitle');} else{gen.writeStringField('TemplateTitle', ts1.master_task__r.task_Subject__c);}
                         if(ts1.LastClientMsgTime__c == null) {gen.writeNullField('LastClientMsgTime');} else{gen.writeDateTimeField('LastClientMsgTime', ts1.LastClientMsgTime__c);}
                         if(ts1.LastMessageTime__c == null) {gen.writeNullField('LastMessageTime');} else{gen.writeDateTimeField('LastMessageTime', ts1.LastMessageTime__c);}
                         if(ts1.Last_Client_viewed_time__c == null) {gen.writeNullField('Last_Client_viewed_time');} else{gen.writeDateTimeField('Last_Client_viewed_time', ts1.Last_Client_viewed_time__c);}
                         if(ts1.createddate == null) {gen.writeNullField('createddate');} else{gen.writeDateTimeField('createddate', ts1.createddate);}
                         If(!FeedsonTask.isEmpty()){
                             //Creating Array & Object - FeedsonTask
                             gen.writeFieldName('FeedsonTask');
                             gen.writeStartArray();
                             if(FeedItemTasks != null && FeedItemTasks.containsKey(ts1.Id)) {
                                 List<FeedItem> taskFeeds = new List<FeedItem>();
                                 taskFeeds = FeedItemTasks.get(ts1.Id);
                                 for(FeedItem taskFeed :taskFeeds) {
                                     //Start of Object - taskFeeds
                                     gen.writeStartObject();

                                     if(taskFeed.Id == null) {gen.writeNullField('Id');} else{gen.writeStringField('Id', taskFeed.Id);}
                                     if(taskFeed.ParentId == null) {gen.writeNullField('ParentId');} else{gen.writeStringField('ParentId', taskFeed.ParentId);}
                                     if(taskFeed.InsertedById == null) {gen.writeNullField('InsertedById');} else{gen.writeStringField('InsertedById', taskFeed.InsertedById);}
                                     if(taskFeed.Title == null) {gen.writeNullField('Title');} else{gen.writeStringField('Title', taskFeed.Title);}
                                     if(taskFeed.Body == null) {gen.writeNullField('Body');} else{gen.writeStringField('Body', taskFeed.Body);}
                                     if(taskFeed.LinkUrl == null) {gen.writeNullField('LinkUrl');} else{gen.writeStringField('LinkUrl', taskFeed.LinkUrl);}
                                     if(taskFeed.createddate == null) {gen.writeNullField('createddate');} else{ gen.writeDateTimeField('createddate', taskFeed.createddate);}
                                     if(taskFeed.CommentCount== null) {gen.writeNullField('CommentCount');} else{gen.writeNumberField('CommentCount', taskFeed.CommentCount);}

                                     //  gen.writeEndObject();
                                     //End of Object - taskFeed

                                     System.debug('contenetDocIds--> 2  -->'+contenetDocIds.containskey(taskFeed.id));

                                     if(contenetDocIds.containskey(taskFeed.id)) {
                                         //Creating Array & Object - ContentDocId
                                         gen.writeFieldName('ContentDocId');
                                         gen.writeStartArray();

                                         for(ContentVersion CD1 :contenetDocIds.get(taskFeed.id)) {
                                             //Start of Object - ContentDocId
                                             gen.writeStartObject();

                                             if(CD1.Id == null) {gen.writeNullField('Title');} else{gen.writeStringField('Title',CD1.Title);}
                                             if(CD1.Id == null) {gen.writeNullField('ContentverId');} else{gen.writeStringField('ContentverId',CD1.ID);}
                                             if(CD1.ContentDocumentID == null) {gen.writeNullField('ContentdocId');} else{gen.writeStringField('ContentdocId',CD1.ContentDocumentID);}

                                             gen.writeEndObject();
                                             //End of Object - ContentDocId

                                         }

                                         gen.writeEndArray();
                                         //End of Array - ContentDocId
                                     }


                                     gen.writeEndObject();
                                     //End of Object - taskFeed

                                 }
                             }
                             gen.writeEndArray();
                             //End of Array - taskFeeds
                         }
                        If(!mapsOfSubs.isEmpty()){
                        //Creating Array & Object - FeedsonTask
                        gen.writeFieldName('Participants');
                        gen.writeStartArray();
                        if(mapsOfSubs != null && mapsOfSubs.containsKey(ts1.Id)){
                            List<Id> UsersIds = new List<Id>();
                            UsersIds = mapsOfSubs.get(ts1.Id);      
                            for(Id usrId :UsersIds) {
                                User usr = NotifyUsers.get(usrId);
                                //Start of Object - User
                                gen.writeStartObject();
                              //  id, Firstname,Lastname
                                if(usr.Id == null) {gen.writeNullField('Id');} else{gen.writeStringField('Id', usr.Id);}
                                if(usr.Firstname == null) {gen.writeNullField('Firstname');} else{gen.writeStringField('Firstname', usr.Firstname);}
                                if(usr.Lastname == null) {gen.writeNullField('Lastname');} else{gen.writeStringField('Lastname', usr.Lastname);}
                                if(usr.fullphotoUrl == null) {gen.writeNullField('fullphotoUrl');} else{gen.writeStringField('fullphotoUrl', usr.fullphotoUrl);}
                               
                                //  gen.writeEndObject();
                                //End of Object - User
                                
                             
                                
                                
                                gen.writeEndObject();
                                //End of Object - notifyUsers
                                
                            }
                        }
                        gen.writeEndArray();
                        //End of Array - SubsOnTask
                    }


                         gen.writeEndObject();
                         //End of Object - Final Delivery
                     }
                     gen.writeEndArray();
                     //End of Array - Freight

                 }
                 gen.writeEndObject();
                 jsonData = gen.getAsString();

                 res.responseBody = Blob.valueOf(jsonData);
                 res.statusCode = 200;

                 API_Log__c Al = New API_Log__c();
                 Al.Account__c=rw.AccountID;
                 Al.EndpointURLName__c='NCPSORelatedObjects';
                 Al.Response__c='Success - NSO RelatedObject Details are sent';
                 Al.StatusCode__c=string.valueof(res.statusCode);
                 Insert Al;
             }}
        Catch(Exception e){
            String ErrorString ='Something went wrong please contact SFsupport@tecex.com';
            res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
            res.addHeader('Content-Type', 'application/json');
            res.statusCode = 400;

            API_Log__c Al = New API_Log__c();
            Al.Account__c=rw.AccountID;
            Al.EndpointURLName__c='NCPSORelatedObjects';
            Al.Response__c=e.getMessage()+' at line number: '+e.getLineNumber();
            Al.StatusCode__c=string.valueof(res.statusCode);
            Insert Al;

        }



    }
    

}