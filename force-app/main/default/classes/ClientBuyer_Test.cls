@isTest
public class ClientBuyer_Test {
	
    public static testMethod void ClientBuyer_Test(){
        
        //Test Data
        Access_token__c aToken = new Access_token__c();
        aToken.Status__c = 'Active';
        aToken.Access_Token__c = '123';
        
        Insert aToken;
        
        Account acc = new Account(Name = 'Test Account');
        
        insert  acc;
        
        //Json Body
        String json = '{"Accesstoken":"'+aToken.Access_Token__c+'","ClientBuyeraccountName":"ClientBuyeraccount","AccountID":"'+acc.Id+'"}';
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
        req.requestURI = '/services/apexrest/NCPCreateClientBuyer'; 
        req.httpMethod = 'Post';
        req.addHeader('Content-Type', 'application/json');
        req.requestBody = Blob.valueof(json);
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
        ClientBuyer.ClientBuyer();
        Test.stopTest(); 
    }
    
    
    public static testMethod void ClientBuyer_Test1(){
        
        //Test Data
        Access_token__c aToken = new Access_token__c();
        aToken.Status__c = 'Active';
        aToken.Access_Token__c = '123';
        
        Insert aToken;
        
        Account acc = new Account(Name = 'Test Account');
       
        insert  acc;
        
        //Json Body
        String json = '{"Accesstoken":"","ClientBuyeraccountName":"ClientBuyeraccount","AccountID":"'+acc.Id+'"}';
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
        req.requestURI = '/services/apexrest/NCPCreateClientBuyer'; 
        req.httpMethod = 'Post';
        req.addHeader('Content-Type', 'application/json');
        req.requestBody = Blob.valueof(json);
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
        ClientBuyer.ClientBuyer();
        Test.stopTest(); 
    }
}