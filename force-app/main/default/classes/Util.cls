public class Util {
    
    public static Boolean skipCodeExecution = true;
    
    public static void logShipmentAndPlatformECount(List<Sobject> events){
        API_Log__c log;
        Integer soCount = 0,ccCount = 0,feedCount = 0;
        Map<String,Object> response;
        List<API_Log__c> logs = [SELECT Id,Name,Response__c,StatusCode__c FROM API_Log__c WHERE StatusCode__c='007' AND createdDate = TODAY LIMIT 1];
        if(!logs.isEmpty()){
            log = logs[0];
            response = (Map<String,Object>)JSON.deserializeUntyped(log.Response__c);
            soCount = Integer.valueOf(response.get('sotrigger'));
            ccCount = Integer.valueOf(response.get('Courier'));
            feedCount = Integer.valueOf(response.get('FeedTrigger'));
        }
        else {
            response = new Map<String,Object>();
            log = new API_Log__c(StatusCode__c='007');
        }
        
        for( Sobject tevent : events){
            if(String.valueOf(tevent.get('Source__c')) == 'shipmentTrigger'){
                soCount++;
            }else if(String.valueOf(tevent.get('Source__c')) == 'FeedItem_Trigger'){
                feedCount++;
            }else if(String.valueOf(tevent.get('Source__c')) == 'CreateCourierrates'){
                ccCount++;
            }
        }
        response.put('sotrigger',soCount);
        response.put('Courier',ccCount);
        response.put('FeedTrigger',feedCount);
        log.Response__c = JSON.serialize(response);
        upsert log;
        System.debug('log Id::'+log.Id);
    }

    public static List<String> getPicklistValues(String object_name, String field_name) {
        List<String> values = new List<String>();
        String[] types = new String[] {object_name};
        Schema.DescribeSobjectResult[] results = Schema.describeSObjects(types);
        for(Schema.DescribeSobjectResult res : results) {
            for (Schema.PicklistEntry entry : res.fields.getMap().get(field_name).getDescribe().getPicklistValues()) {
                if (entry.isActive()) {
                    values.add(entry.getValue());
                }
            }
        }
        return values;
    }

    public static Map<String, Object> prepareErrorMap(String message){
        Map<String, Object> result = new Map<String, Object>();
        result.put('status', 'ERROR');
        result.put('msg', message);
        return result;
    }
    
    public static String getAppName() {
        if(Test.isRunningTest() && skipCodeExecution){
            return 'Zee';
        }
        Set<Id> appIds = new Set<Id>();
        List<UserAppInfo> userAppInfo = [SELECT Id, AppDefinitionId FROM UserAppInfo WHERE UserId = :UserInfo.getUserId()];
        for(UserAppInfo uai : userAppInfo){
            appIds.add(uai.AppDefinitionId);
        }
        
        List<AppDefinition> appDefinition = [SELECT DurableId, Label FROM AppDefinition Where DurableId IN :appIds];
        for(AppDefinition ad : appDefinition){
            if(ad.Label == 'Zee'){
                return ad.Label;
            }
        }
        return appDefinition[0].Label;
    }
    
    public static String getResponseString(String type,String message){
        JSONGenerator gen = JSON.createGenerator(true);
        gen.writeStartObject();
        gen.writeFieldName(type); // type can be Sucess or Error
        gen.writeStartObject();
        gen.writeStringField('Response', message);
        gen.writeEndObject();
        gen.writeEndObject();
        return gen.getAsString();
    }
    
    public static List<List<String>> parseCSV(Blob csvFileBody,Boolean skipHeaders) {
        if(csvFileBody == null) {
            ApexPages.Message errorMessage = new ApexPages.Message(ApexPages.severity.ERROR, 'FILE_MISSING');
            ApexPages.addMessage(errorMessage);
            return null;
        }
        
        String contents = csvFileBody.toString();
        
        List<List<String>> allFields = new List<List<String>>();
        
        // replace instances where a double quote begins a field containing a comma
        // in this case you get a double quote followed by a doubled double quote
        // do this for beginning and end of a field
        contents = contents.replaceAll(',"""',',"DBLQT').replaceall('""",','DBLQT",');
        // now replace all remaining double quotes - we do this so that we can reconstruct
        // fields with commas inside assuming they begin and end with a double quote
        contents = contents.replaceAll('""','DBLQT');
        //windows case - replace all carriage + new line character to just new line character
        contents = contents.replaceAll('\r\n','\n');
        //now replace all return char to new line character
        contents = contents.replaceAll('\r','\n');
        // we are not attempting to handle fields with a newline inside of them
        // so, split on newline to get the spreadsheet rows
        List<String> lines = new List<String>();
        try {
            lines = contents.split('\n');
        } catch (System.ListException e) {
            System.debug('Limits exceeded?' + e.getMessage());
        }
        Integer num = 0;
        for(String line : lines) {
            // check for blank CSV lines (only commas)
            if (line.replaceAll(',','').trim().length() == 0) break;
            
            List<String> fields = line.split(',');  
            List<String> cleanFields = new List<String>();
            String compositeField;
            Boolean makeCompositeField = false;
            for(String field : fields) {
                if (field.startsWith('"') && field.endsWith('"')) {
                    cleanFields.add(field.replaceAll('DBLQT','"').trim());
                } else if (field.startsWith('"')) {
                    makeCompositeField = true;
                    compositeField = field;
                } else if (field.endsWith('"')) {
                    compositeField += ',' + field;
                    cleanFields.add(compositeField.replaceAll('DBLQT','"').trim());
                    makeCompositeField = false;
                } else if (makeCompositeField) {
                    compositeField +=  ',' + field;
                } else {
                    cleanFields.add(field.replaceAll('DBLQT','"').trim());
                }
            }
            
            allFields.add(cleanFields);
        }
        if (skipHeaders) allFields.remove(0);
        return allFields;       
    }
}