@IsTest
public class CPRolloutWrapper_Test {
   static testMethod void testParse() {
		String json = '{'+
		'\"Account_Name\": \"0010Y00000PEqvHQAT\",'+
		'\"Contact_Name\": \"0031v00001hY02HAAS\",'+
		'\"Shipment_Value_USD\":1000,'+
		'\"Chargable_Weight\":40.2,'+
		'\"Courier_responsibility\": \"Client\",'+
		'\"Reference1\": \"Ref1\",'+
		'\"Reference2\": \"Ref2\",'+
		'\"ShipFrom\": \"United States\",'+
		'\"ShipTo\": \"Belgium;United Kingdom;France\", '+
		' \"ShipmentOrder_Packages\": ['+
		'    {'+
		
		'      \"Weight_Unit\": \"KGs\",'+
        '      \"Dimension_Unit\": \"CMs\",'+
		'      \"Packages_of_Same_Weight\": 1,'+
		'      \"Length\": 1.00,'+
		'      \"Height\": 1.00,'+
		'      \"Breadth\": 1.00,'+
		'      \"Actual_Weight\": 1.00'+
		
		'    },'+
		'    {'+
		
		'      \"Weight_Unit\": \"KGs\",'+
        '      \"Dimension_Unit\": \"CMs\",'+
		'      \"Packages_of_Same_Weight\": 1,'+
		'      \"Length\": 1.00,'+
		'      \"Height\": 1.00,'+
		'      \"Breadth\": 1.00,'+
		'      \"Actual_Weight\": 1.00'+
		
		'    },'+
		'	 {'+
		
		'      \"Weight_Unit\": \"KGs\",'+
        '      \"Dimension_Unit\": \"CMs\",'+
		'      \"Packages_of_Same_Weight\": 1,'+
		'      \"Length\": 1.00,'+
		'      \"Height\": 1.00,'+
		'      \"Breadth\": 1.00,'+
		'      \"Actual_Weight\": 1.00'+
		
		'    }'+
		''+
		'  ]'+
		''+
		'}';
		CPRolloutWrapper obj = CPRolloutWrapper.parse(json);
		System.assert(obj != null);
	}

}