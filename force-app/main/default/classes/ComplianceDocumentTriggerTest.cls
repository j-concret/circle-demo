@isTest
public class ComplianceDocumentTriggerTest {
    
    @testSetup
    public static void testSetup(){
        Country__c country = new Country__c(Brokerage_Costs__c = 618, CIF_Adjustment_Factor__c = 0.035, CIF_Absolute_value_adjustment__c = 200, Clearance_Costs__c = 488,
                                            Country__c = 'Brazil', Name = 'Brazil', Handling_Costs__c = 1180, License_Permit_Costs__c = 0);
        insert country;
        Country__c worldwide = new Country__c(Name='Worldwide',Country__c='Worldwide');
        insert worldwide;
        
        List<Compliance_Document__c> CCDs = new List<Compliance_Document__c> {
            new Compliance_Document__c(Compliance_Document_Name__c ='ANRT License',Country__c=country.Id,Compliance_type__c='Document',Compliance_status__c = 'Available',Certification_number__c='12345'),
                new Compliance_Document__c(Compliance_Document_Name__c ='Test Certificate (Not EMC/CB)',Country__c=country.Id,Compliance_type__c='Document',Compliance_status__c = 'Available',Certification_number__c='123456'),
                new Compliance_Document__c(Compliance_Document_Name__c ='CE Certificate',Country__c=worldwide.Id,Compliance_type__c='Document',Compliance_status__c = 'Available',Certification_number__c='1234567')
                };
                    Insert CCDs;
    }
    
    @isTest
    public static void testUpdateDoc(){
        try{
            Compliance_Document__c doc = [select Id,Certification_number__c from Compliance_Document__c where Compliance_Document_Name__c ='ANRT License' LIMIT 1];
            doc.Certification_number__c = '123456';
            update doc;
        }catch(Exception e){}
    }
}