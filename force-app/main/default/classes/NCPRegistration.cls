@RestResource(urlMapping='/NCPRegistration/*')
Global class NCPRegistration {
    @Httppost
    global static void NCPRegistration(){
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        Blob body = req.requestBody;
        res.addHeader('Content-Type', 'application/json');
        String requestString = body.toString();
        
        NCPRegistrationwra rw = (NCPRegistrationwra)JSON.deserialize(requestString,NCPRegistrationwra.class);
        
         List<User> createdUsers = new list<user>();
        try {
            Boolean Emailexistingcheck=true;
            List<contact> Existingcontact = new list<contact>();
            List<Contact> Existingdomain = new list<contact>();
            ID OwnID;
            ID SignedbyID;
                
                 
        List<Access_token__c> at = [SELECT status__c,Access_Token__c FROM Access_token__c WHERE Access_Token__c =: rw.Accesstoken and status__c = 'Active' LIMIT 1];
         
            if(rw.emailaddress !=null && !at.isEmpty() ) {
                // Email validation
                
                String str1 = rw.emailaddress;
				String mailDomain = str1.substringBetween('@', '.');
                String Conquery = 'Select Id,email,accountid,account.Lead_AM__c,account.emaildomain__c from contact where email like '+'\'%'+ mailDomain+'%\''+'limit 1';
                List<Contact> Con = Database.query(Conquery); 
                
                
                List<User> Usrs= [select id,name,email from user where email='byronf@tecex.com' or email='jacquesb@tecex.com'];
                for(user u: Usrs){
                    if(u.email=='byronf@tecex.com'){OwnID=u.id;}
                    else{SignedbyID=u.id;}
                }
                
                
                for(Contact co:con){ 
                    if(co.email == rw.emailaddress){ Existingcontact.add(co);}
                    else if(co.account.emaildomain__C==mailDomain) { Existingdomain.add(co); }
                     }
                
                if(!Existingcontact.isempty()){
                     res.responseBody = Blob.valueOf(Util.getResponseString('Error','contact is already exist, pls click on forgot password'));
                     res.statusCode = 400;
                     Emailexistingcheck= false;
                	}  
            
            }
                
        if(!at.isEmpty() && Emailexistingcheck == TRUE){
            String AccRecID ;
            String ClientType;
            String Pricelist;
            
            
            Map<String,String> emailValidityRes = checkEmailValidation(rw.typeOfGoods, rw.emailAddress);
            
            if(String.isNotBlank(rw.typeOfGoods) && rw.typeOfGoods == 'Amazon FBA'){
                AccRecID = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Client (E-commerce)').getRecordTypeId();
                ClientType='E-commerce';
                Pricelist='E-commerce Price List';
            }else{
                AccRecID = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Client').getRecordTypeId();
                ClientType ='Reseller';
                Pricelist='Standard Price List';
            
            }
            
        			Account acc = new Account(
                            RecordTypeId=AccRecID,
                            OwnerID=ownId,// newly added
                            Name = rw.companyName,
                            Phone= rw.countryCode+rw.contactNumber,
                            Contact_Email__c = rw.emailAddress,
                           // Client_type__c = rw.typeOfCompany,
                            Lead_Source__c='App self-registration',
                            Types_of_Goods_Other__c=rw.TypeofGoods,
                        	Vetted_account__c= false,
                        	From_CTA__C = 'Register',
                        	campaign__C=rw.campaign,
                            signed_by__C=SignedbyID, //Newly added
                            Country__c = rw.BasedAt,
                            Price_List_Type__c =Pricelist,
                        	Branch_PL__c='TecEx - Self registration',
                            Client_type__c=ClientType
                            );
            if(rw.typeOfGoods == 'Amazon FBA') {
                								acc.Liability_Product_Agreement__c ='Liability Cover Declined';
                                                acc.Finance_Charge__c = 0;
                                                acc.Cash_outlay_fee_base__c ='No Cash Outlay Fee';
                                                acc.IOR_Payment_Terms__c = 0;
                                                acc.Prepayment_Term_Days__c = 0;
                                                
                                               }
            if(!Test.isRunningTest()){
             if(Existingdomain.isempty()){ 
                                            insert acc; 
                                         }
            }
               
            
                Contact con = new contact(
               
                    //AccountId = acc.Id,
                    //Phone=acc.phone,
                    FirstName = rw.firstName,
                    LastName = rw.lastName,
                    Email = rw.emailAddress,
                    
                    Contact_Role__c = rw.role
                    );
                
            if(!Existingdomain.isempty()){ con.Accountid=Existingdomain[0].accountid;                              
                                         } 
            else{ con.AccountId = acc.Id; }
            insert con;
            
            
            if(con!=null) { 
           
                
            contact usercreation = [select Id,firstname,lastname,email,name from contact where id =:con.id ];
            List<User> userList = new List<User>(); 
            Profile profileId = [SELECT Id FROM Profile WHERE Name = 'Tecex Customer' LIMIT 1];
            
                List<String> nickNames = new List<String>();
              
                for(User usr : [SELECT Id, CommunityNickname,Username FROM User LIMIT 10000]){
                    nickNames.add(usr.CommunityNickname);
                 
                }       

                String nickname;
                if(nickNames.contains(usercreation.Name)){
                    nickname = usercreation.Name +' '+usercreation.Email.split('@', 2)[1];
                    system.debug('nicknameTest :'+nickname);
                    if(nickNames.contains(usercreation.Name +' '+usercreation.Email.split('@', 2)[1])){
                        nickname = usercreation.Name +' '+usercreation.Email.split('@', 2)[1]+'1';
                    }
                }else{
                    nickname = usercreation.Name;
                }
                
               
                
                
                

                    
                User usr = new User(LastName = usercreation.LastName,
                                FirstName=usercreation.FirstName,
                                Alias = usercreation.Name.left(8),
                                Email = usercreation.Email,
                                CommunityNickname  = nickname,
                                Username = usercreation.Email,
                                ContactId = usercreation.Id,
                                ProfileId = profileId.Id,
                                TimeZoneSidKey = 'GMT',
                                LanguageLocaleKey = 'en_US',
                                EmailEncodingKey = 'UTF-8',
                                LocaleSidKey = 'en_US'
                               );
             if(!Test.isRunningTest()){
                Insert Usr;
             EnableNCPUser.NCPRegistrationaddPM(Usr.Id);
                  }

            }
            
            //email selection
            If(acc !=null && Existingdomain.isempty() ){
                sendemail(Acc,'NewAccount');
            } 
            else{
                Account Acc1 = [select Id,Lead_AM__c,Lead_AM__r.email from account where id =:Existingdomain[0].accountid limit 1 ];
                
                system.debug('Account--->'+Acc1);
                 sendemail(Acc1,Existingdomain[0].id);
            }
            
            
                   JSONGenerator gen = JSON.createGenerator(true);
                    gen.writeStartObject();
                    
                    gen.writeFieldName('Success');
                    gen.writeStartObject();
                    
                    gen.writeStringField('Message','Created contact ID:' +Con.id);
                    
                    gen.writeEndObject();
                    gen.writeEndObject();
                    String jsonData = gen.getAsString();
                    res.responseBody = Blob.valueOf(jsonData);
                    res.addHeader('Content-Type', 'application/json');
                    res.statusCode = 200;    
                    API_Log__c Al = New API_Log__c();
                    Al.EndpointURLName__c='NCPRegistration';
                    Al.StatusCode__c=string.valueof(res.statusCode);
                    Insert Al;

        }
            else{
                
            String ErrorString ='Authentication failed';
            res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
            res.addHeader('Content-Type', 'application/json');
            res.statusCode = 400;

            }
            
            }
        catch(Exception e) {
             String ErrorString;
            if(!rw.emailAddress.contains('@')){ErrorString ='Invalid email address';}
            else{
           // ErrorString ='Something went wrong while creating cost estimate, please contact support_SF@tecex.com';
            ErrorString = createdUsers+'-'+e.getLineNumber()+e.getStackTraceString()+e.getMessage();
          
            }
              res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
            res.addHeader('Content-Type', 'application/json');
            res.statusCode = 500;

            API_Log__c Al = New API_Log__c();
            Al.EndpointURLName__c='NCPRegistration';
            Al.Response__c=e.getMessage()+' at line number: '+e.getLineNumber();
            Al.StatusCode__c=string.valueof(res.statusCode);
            Insert Al;

        }
    }
    
    public static Map<String,String> checkEmailValidation(String typeOfGoods, String email){
        Map<String,String> response = new Map<String,String>();
        response.put('Status','OK');
        String emailRegex = '^[a-zA-Z0-9._|\\\\%#~`=?&/$^*!}{+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$'; // source: http://www.regular-expressions.info/email.html
        Pattern MyPattern = Pattern.compile(emailRegex);
        Matcher MyMatcher = MyPattern.matcher(email);
        
        if (!MyMatcher.matches()) {
            response.put('Status','ERROR');
            response.put('Message','Please enter a valid email address');
            return response;
        }
        
        String fullDomain = email.split('@').get(1);
        String Domain = (fullDomain.split('\\.').get(0)).toLowerCase();
        Map<String,EmailBlocker__c> emailBlockers = (Map<String,EmailBlocker__c>)EmailBlocker__c.getAll();
        
        if(emailBlockers !=null && emailBlockers.containsKey(Domain)){
            EmailBlocker__c blockedEmail = emailBlockers.get(Domain);
            List<String> business = (blockedEmail.Type_of_business__c).split(';');
            if(business.contains(typeOfGoods)){
                response.put('Status','ERROR');
                response.put('Message','The email address you provided is not a valid business email address');
			}
        }
       
        
        return response;
    }
    
     public static void sendemail(Account Acc,string accountstatus){
         
         String baseUrl = URL.getSalesforceBaseUrl().toExternalForm();
         
        Messaging.SingleEmailMessage semail = new Messaging.SingleEmailMessage();
         if(accountstatus=='NewAccount'){
             system.debug('IN new account email block');
        String[] sendingTo = new String[]{'pietmana@tecex.com','jacquesb@tecex.com','michaelp@tecex.com'};
		//String[] sendingTo = new String[]{'anilk@tecex.com'};
		semail.setToAddresses(sendingTo);
		semail.setSubject('New account signed via self registration');
		//semail.setPlainTextBody('Hi, New account registered via self registration.'+' '+'https://tecex.lightning.force.com/'+Acc.Id);
		semail.setHtmlBody('Hi,<br/> New account is created via portal self registration. <br/>'+
                           'To view this account <a href='+baseUrl+'/'+acc.Id+'>click here.</a>');
         }
         
         else {
             system.debug('IN else new account email block');
         String[] sendingTo = new String[]{};
             sendingTo.add(acc.Lead_AM__r.email);
		//String[] sendingTo = new String[]{'anilk@tecex.com'};
		semail.setToAddresses(sendingTo);
		semail.setSubject('New contact signed via self registration to an existing account');
		//semail.setPlainTextBody('Hi, New account registered via self registration.'+' '+'https://tecex.lightning.force.com/'+Acc.Id);
		semail.setHtmlBody('Hi,<br/> New contact is created via portal self registration to an existing account <br/>'+
                           'To view this account <a href='+baseUrl+'/'+accountstatus+'>click here.</a>');
             
         }
         Messaging.sendEmail(new Messaging.SingleEmailMessage[] {semail});
         
            }
    
    
}