public class generateCCDDetailsWrapper {
    
    public static List<Object> getDetails(Customs_Clearance_Documents__c customs_Clearance_Document){
        List<Object> sectionsList =  new List<Object>();
        Map<String,Object> generalSection =  new Map<String,Object>{
            'section_Name'=>'General',
                'fields'=> new List<Map<String,Object>>{
                    new Map<String, Object>{
                        'label'=>'Shipment Order',
                            'apiName'=>'Customs_Clearance_Documents__r.Name',
                            'isEditable'=>false,
                            'value'=>customs_Clearance_Document.Customs_Clearance_Documents__r.Name,
                            'isRelation'=>true,
                            'relationName'=>'Customs_Clearance_Documents__r'   
                            },new Map<String, Object>{
                                'label'=>'Tax Treatment',
                                    'apiName'=>'Tax_Treatment__c',
                                    'isEditable'=>false,
                                    'value'=>customs_Clearance_Document.Tax_Treatment__c,
                                    'isRelation'=>false,
                                    'relationName'=>''   
                                    },new Map<String, Object>{
                                        'label'=>'Shipment Order Status',
                                            'apiName'=>'Shipment_Order_Status__c',
                                            'isEditable'=>false,
                                            'value'=>customs_Clearance_Document.Shipment_Order_Status__c,
                                            'isRelation'=>false,
                                            'relationName'=>''   
                                            },new Map<String, Object>{
                                                'label'=>'VAT Reclaim Shipment',
                                                    'apiName'=>'VAT_Reclaim_Shipment__c',
                                                    'value'=>customs_Clearance_Document.VAT_Reclaim_Shipment__c,
                                                    'isEditable'=>false,
                                                    'isRelation'=>false,
                                                    'relationName'=>''   
                                                    },new Map<String, Object>{
                                                        'label'=>'Ship to Country',
                                                            'apiName'=>'Ship_to_Country__c',
                                                            'value'=>customs_Clearance_Document.Ship_to_Country__c,
                                                            'isEditable'=>false,
                                                            'isRelation'=>false,
                                                            'relationName'=>''   
                                                            },new Map<String, Object>{
                                                                'label'=>'Region',
                                                                    'apiName'=>'Region__c',
                                                                    'value'=>customs_Clearance_Document.Region__c,
                                                                    'isEditable'=>false,
                                                                    'isRelation'=>false,
                                                                    'relationName'=>''   
                                                                    },new Map<String, Object>{
                                                                        'label'=>'Account',
                                                                            'apiName'=>'Account__c',
                                                                            'value'=>customs_Clearance_Document.Account__c,
                                                                            'isEditable'=>false,
                                                                            'isRelation'=>false,
                                                                            'relationName'=>''   
                                                                            },new Map<String, Object>{
                                                                                'label'=>'Invoice Timing',
                                                                                    'apiName'=>'Invoice_Timing__c',
                                                                                    'value'=>customs_Clearance_Document.Invoice_Timing__c,
                                                                                    'isEditable'=>false,
                                                                                    'isRelation'=>false,
                                                                                    'relationName'=>''   
                                                                                    },new Map<String, Object>{
                                                                                        'label'=>'Shipment Order Value',
                                                                                            'apiName'=>'Shipment_Order_Value__c',
                                                                                            'value'=>customs_Clearance_Document.Shipment_Order_Value__c,
                                                                                            'isEditable'=>false,
                                                                                            'isRelation'=>false,
                                                                                            'relationName'=>''   
                                                                                            },new Map<String, Object>{
                                                                                                'label'=>'Supplier',
                                                                                                    'apiName'=>'Supplier__c',
                                                                                                    'value'=>customs_Clearance_Document.Supplier__c,
                                                                                                    'isEditable'=>false,
                                                                                                    'isRelation'=>false,
                                                                                                    'relationName'=>''   
                                                                                                    }
                    
                }    
        };
            
            Map<String,Object> taxSection =  new Map<String,Object>{
                'section_Name'=>'Taxes & Duties per CCD',
                    'fields'=> new List<Map<String,Object>>{
                        new Map<String, Object>{
                            'label'=>'Tax Currency',
                                'apiName'=>'Tax_Currency__c',
                                'value'=>customs_Clearance_Document.Tax_Currency__c,
                                'isEditable'=>false,
                                'isRelation'=>false,
                                'relationName'=>''
                                },new Map<String, Object>{
                                    'label'=>'Foreign Exchange Rate to USD',
                                        'apiName'=>'Foreign_Exchange_Rate__c',
                                        'value'=>customs_Clearance_Document.Foreign_Exchange_Rate__c,
                                        'isEditable'=>false,
                                        'isRelation'=>false,
                                        'relationName'=>''   
                                        },new Map<String, Object>{
                                            'label'=>'Duties and Taxes per CCD(Local Currency)',
                                                'apiName'=>'Duties_and_Taxes_per_CCD__c',
                                                'value'=>customs_Clearance_Document.Duties_and_Taxes_per_CCD__c,
                                                'isEditable'=>false,
                                                'isRelation'=>false,
                                                'relationName'=>''   
                                                },new Map<String, Object>{
                                                    'label'=>'Duties and Taxes per CCD(USD)',
                                                        'apiName'=>'Taxes_per_CCDs_USD__c',
                                                        'value'=>customs_Clearance_Document.Taxes_per_CCDs_USD__c,
                                                        'isEditable'=>false,
                                                        'isRelation'=>false,
                                                        'relationName'=>''   
                                                        }
                        
                    }    
            };
                Map<String,Object> supportSection =  new Map<String,Object>{
                    'section_Name'=>'CCD Support Section',
                        'fields'=> new List<Map<String,Object>>{
                            new Map<String, Object>{
                                'label'=>'Taxes per Tax Calculation',
                                    'apiName'=>'Taxes_per_Tax_Calculation__c',
                                    'value'=>customs_Clearance_Document.Taxes_per_Tax_Calculation__c,
                                    'isEditable'=>false,
                                    'isRelation'=>false,
                                    'relationName'=>''
                                    },new Map<String, Object>{
                                        'label'=>'Sum of Supplier Duties and Taxes',
                                            'apiName'=>'Sum_of_Supplier_Duties_and_Taxes__c',
                                            'value'=>customs_Clearance_Document.Sum_of_Supplier_Duties_and_Taxes__c,
                                            'isEditable'=>false,
                                            'isRelation'=>false,
                                            'relationName'=>''   
                                            },new Map<String, Object>{
                                                'label'=>'Tax Calculation Difference',
                                                    'apiName'=>'Tax_Calculation_Difference__c',
                                                    'value'=>customs_Clearance_Document.Tax_Calculation_Difference__c,
                                                    'isEditable'=>false,
                                                    'isRelation'=>false,
                                                    'relationName'=>''   
                                                    },new Map<String, Object>{
                                                        'label'=>'Supplier (Overcharge) / Undercharge',
                                                            'apiName'=>'Supplier_Overcharge_Undercharge__c',
                                                            'value'=>customs_Clearance_Document.Supplier_Overcharge_Undercharge__c,
                                                            'isEditable'=>false,
                                                            'isRelation'=>false,
                                                            'relationName'=>''   
                                                            },new Map<String, Object>{
                                                                'label'=>'Percentage Difference',
                                                                    'apiName'=>'Percentage_Difference_CDC__c',
                                                                    'value'=>customs_Clearance_Document.Percentage_Difference_CDC__c,
                                                                    'isEditable'=>false,
                                                                    'isRelation'=>false,
                                                                    'relationName'=>''   
                                                                    },new Map<String, Object>{
                                                                        'label'=>'Percentage Difference',
                                                                            'apiName'=>'Percentage_Difference_Supplier__c',
                                                                            'value'=>customs_Clearance_Document.Percentage_Difference_Supplier__c,
                                                                            'isEditable'=>false,
                                                                            'isRelation'=>false,
                                                                            'relationName'=>''   
                                                                            },new Map<String, Object>{
                                                                                'label'=>'Reason For Difference',
                                                                                    'apiName'=>'Reason_For_Difference_CDC__c',
                                                                                    'value'=>customs_Clearance_Document.Reason_For_Difference_CDC__c,
                                                                                    'isEditable'=>true,
                                                                                    'type'=>'picklist',
                                                                                    'picklistValues'=>picklist_values('Customs_Clearance_Documents__c','Reason_For_Difference_CDC__c')
                                                                                    },new Map<String, Object>{
                                                                                        'label'=>'Reason For Difference',
                                                                                            'apiName'=>'Reason_For_Difference_Supplier__c',
                                                                                            'value'=>customs_Clearance_Document.Reason_For_Difference_Supplier__c,
                                                                                            'isEditable'=>true,
                                                                                            'type'=>'picklist',
                                                                                            'picklistValues'=>picklist_values('Customs_Clearance_Documents__c','Reason_For_Difference_Supplier__c')
                                                                                            },new Map<String, Object>{
                                                                                                'label'=>'Elaboration On Reason',
                                                                                                    'apiName'=>'Elaboration_On_Reason_CDC__c',
                                                                                                    'value'=>customs_Clearance_Document.Elaboration_On_Reason_CDC__c,
                                                                                                    'isEditable'=>true,
                                                                                                    'type'=>'textarea',
                                                                                                    'relationName'=>''   
                                                                                                    },new Map<String, Object>{
                                                                                                        'label'=>'Elaboration On Reason',
                                                                                                            'apiName'=>'Reason_For_Supplier_Over_Undercharge__c',
                                                                                                            'value'=>customs_Clearance_Document.Reason_For_Supplier_Over_Undercharge__c,
                                                                                                            'isEditable'=>true,
                                                                                                            'type'=>'textarea',
                                                                                                            'relationName'=>''   
                                                                                                            }		
                            
                        }    
                };
                    Map<String,Object> infoSection =  new Map<String,Object>{
                        'section_Name'=>'CCD Info',
                            'fields'=> new List<Map<String,Object>>{
                                new Map<String, Object>{
                                    'label'=>'Status',
                                        'apiName'=>'Status__c',
                                        'isEditable'=>true,
                                        'value'=>customs_Clearance_Document.Status__c,
                                        'type'=>'picklist',
                                        'picklistValues'=>picklist_values('Customs_Clearance_Documents__c','Status__c')
                                        },new Map<String, Object>{
                                            'label'=>'Reason For Query',
                                                'apiName'=>'Reason_For_Query__c',
                                                'value'=>customs_Clearance_Document.Reason_For_Query__c,
                                                'isEditable'=>true,
                                                'type'=>'picklist',
                                                'picklistValues'=>picklist_values('Customs_Clearance_Documents__c','Reason_For_Query__c')
                                                },new Map<String, Object>{
                                                    'label'=>'Elaboration on Queried Status',
                                                        'apiName'=>'Elaboration_on_Queried_Status__c',
                                                        'value'=>customs_Clearance_Document.Elaboration_on_Queried_Status__c,
                                                        'isEditable'=>true,
                                                        'type'=>'textarea',
                                                        'relationName'=>''   
                                                        },new Map<String, Object>{
                                                            'label'=>'I have reviewed the attachment',
                                                                'value'=>customs_Clearance_Document.Review_Attachment__c,
                                                                'apiName'=>'Review_Attachment__c ',
                                                                'isEditable'=>true,
                                                                'type'=>'checkbox',
                                                                'relationName'=>''   
                                                                }
                                
                            }    
                    };
                        sectionsList.add(generalSection);
        sectionsList.add(taxSection);
        sectionsList.add(supportSection);
        sectionsList.add(infoSection);
        return sectionsList;
    }
    
    public static Set<String> picklist_values(String object_name, String field_name) {
        Set<String> values = new Set<String>{'None'};
            String[] types = new String[] {object_name};
                Schema.DescribeSobjectResult[] results = Schema.describeSObjects(types);
        for(Schema.DescribeSobjectResult res : results) {
            for (Schema.PicklistEntry entry : res.fields.getMap().get(field_name).getDescribe().getPicklistValues()) {
                if (entry.isActive()) {
                    values.add(entry.getValue());
                }
            }
        }
        return values;
    }
}