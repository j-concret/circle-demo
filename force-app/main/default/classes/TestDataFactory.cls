@isTest
public class TestDataFactory {
  
    //Test Supplier record
    public static Account mySupplier(){
        Account mySupplier      = new Account();
        mySupplier.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('IOR/EOR Supplier').getRecordTypeId(); //'0120Y0000006KjQQAU';
        mySupplier.Name         = 'Test Supplier';
        mySupplier.Type         = 'Supplier';
        mySupplier.ICE__c       = '0050Y000001km5c';
        insert mySupplier;
        return mySupplier;
    }

    // Account/Client record
    public static Account myClient(){
        Account myClient      = new Account();
        myClient.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Client').getRecordTypeId();
        myClient.Name         = 'Test Client';
        myClient.Type         = 'Client'; 
        myClient.CSE_IOR__c   = '0050Y000001km5c';
        myClient.Service_Manager__c = '0050Y000001km5c';
        insert myClient;
        return myClient;
    }

    // Contact Record
    public static Contact myContact(){
        String myAccountName          = myClient().Name;
        Contact myContact             = new Contact();
        myContact.LastName            = 'Testing';
        myContact.Email               = 'dayeniwear@gmail.com';
        myContact.AccountId         = [SELECT Id 
                                          FROM Account 
                                         WHERE Name = :myAccountName LIMIT 1].Id;
        myContact.Contact_Role__c     = 'Executive';
        insert myContact;
        return myContact;
    }
     
    public static Contact myContact1(){
        String myAccountName          = myClient().Name;
        Contact myContact             = new Contact();
        myContact.LastName            = 'Testing1';
        myContact.Email               = 'dayeniofficial@gmail.com';
        myContact.AccountId         = [SELECT Id 
                                          FROM Account 
                                         WHERE Name = :myAccountName LIMIT 1].Id;
        myContact.Contact_Role__c     = 'Executive';
        insert myContact;
        return myContact;
    }


    //Country Price/Approval(Country - IOR Supplier) record
    public static country_price_approval__c myIorSupplier(){
        String mySupplierName                     = mySupplier().Name;
        country_price_approval__c myIorSupplier  = new country_price_approval__c();
        myIorSupplier.Name                       = 'South Africa - Test Supplier';
        myIorSupplier.Supplier__c                =[SELECT Id 
                                                     FROM Account 
                                                    WHERE Name = :mySupplierName LIMIT 1].Id;
        myIorSupplier.Billing_term__c            = 'DAP/CIF - IOR pays';
        myIorSupplier.Destination__c             = 'South Africa';
        myIorSupplier.Clearance_Destination__c   = 'South Africa';
        myIorSupplier.Airwaybill_Instructions__c = 'Just testing';
        insert myIorSupplier;
        return myIorSupplier;
    }

    //Price list record
    public static IOR_Price_List__c myPriceList(){
        String myAccountName                     = myClient().Name;
        IOR_Price_List__c myPriceList            = new IOR_Price_List__c();
        myPriceList.Client_Name__c               = [SELECT Id 
                                                      FROM Account 
                                                     WHERE Name = :myAccountName LIMIT 1].Id; 
        myPriceList.Name                         = 'South Africa';
        myPriceList.Admin_Fee__c                 = 12345;
        myPriceList.Set_up_fee__c                = 246810;
        myPriceList.Bank_Fees__c                 = 3691215;
        myPriceList.Tax_Rate__c                  = 0.15;
        myPriceList.TecEx_Shipping_Fee_Markup__c = 0.03;
        myPriceList.On_Charge_Mark_up__c         = 0.03;
        myPriceList.IOR_Fee__c                   = 0.02;
        myPriceList.IOR_Min_Fee__c                 = 51015;
        myPriceList.Estimated_Customs_Brokerage__c = 123;
        myPriceList.Estimated_Customs_Clearance__c = 243;
        myPriceList.Estimated_Import_License__c    = 369;
        myPriceList.Estimated_Customs_Handling__c  = 111;
        insert myPriceList;
        return myPriceList;
    }

    //Shipment order record 
    public static Shipment_Order__c myShipmentOrder(){
        String myAccountName                                  = myClient().Name;
        String myIorSupplierName                              = myIorSupplier().Name;
        String myPriceListName                                = myPriceList().Name;
        String myContactLastName                              = myContact().LastName;
        
        Shipment_Order__c myShipmentOrder                     = new Shipment_Order__c();
        myShipmentOrder.RecordTypeId                          = Schema.SObjectType.Shipment_Order__c.getRecordTypeInfosByName().get('Shipment Order').getRecordTypeId(); //'0120Y0000009cF0QAI'
        myShipmentOrder.Account__c                            = [SELECT Id 
                                                                  FROM Account 
                                                                 WHERE Name = :myAccountName LIMIT 1].Id; 
        myShipmentOrder.Ship_to_Country__c                    = [SELECT Id 
                                                                   FROM country_price_approval__c
                                                                  WHERE Name = :myIorSupplierName LIMIT 1].Id;  
        myShipmentOrder.IOR_Price_List__c                     = [SELECT Id 
                                                                   FROM IOR_Price_List__c
                                                                  WHERE Name = :myPriceListName LIMIT 1].Id;
        myShipmentOrder.Ship_From_Country__c                  = 'United States';
        myShipmentOrder.Shipment_Value_USD__c                 = 600;
        myShipmentOrder.Client_Contact_for_this_Shipment__c   = [SELECT Id 
                                                                   FROM Contact 
                                                                  WHERE LastName = :myContactLastName  LIMIT 1].Id;
        myShipmentOrder.Service_Type__c                       = 'EOR';
        myShipmentOrder.Who_arranges_International_courier__c = 'Client';
        myShipmentOrder.Chargeable_Weight__c                  = 20;
        myShipmentOrder.Total_Chargeable_Weight_as_per_Final_AWB__c  = 20;

        insert myShipmentOrder;
        return myShipmentOrder;
    }

    public static Customer_Invoice__c myFirstInvoice(){
        String myAccountName                    = myClient().Name;
        Id myShipmentOrderId                    = myShipmentOrder().Id;
      
        Customer_Invoice__c myCustomerInvoice   = new Customer_Invoice__c();
        myCustomerInvoice.RecordTypeId          = Schema.SObjectType.Customer_Invoice__c.getRecordTypeInfosByName().get('New First Invoice').getRecordTypeId();
        myCustomerInvoice.Client__c             = [SELECT Id 
                                                     FROM Account 
                                                    WHERE Name = :myAccountName LIMIT 1].Id; 
        myCustomerInvoice.Shipment_Order__c     = [SELECT Id 
                                                     FROM Shipment_Order__c 
                                                    WHERE Id = :myShipmentOrderId LIMIT 1].Id;
        myCustomerInvoice.Invoice_Type__c       = 'Cash Outlay Invoice';
        myCustomerInvoice.Invoice_Status__c     = 'Invoice Sent';
        myCustomerInvoice.Aging_At_Last_Statement__c = 'Testing testing';
        myCustomerInvoice.Invoice_Amount__c     = 500;
        myCustomerInvoice.Total_Applied_to_other_invoice__c = 25;
        myCustomerInvoice.Total_Recieved__c     = 222;
        myCustomerInvoice.Total_Credits_Applied__c = 30;
        myCustomerInvoice.Total_Overpayments_Applied__c = 26;
        myCustomerInvoice.Due_Date__c           = date.newInstance(2019, 7, 15);
        insert myCustomerInvoice;
        return myCustomerInvoice;
    }

    public static Customer_Invoice__c myFirstInvoice2(){
        String myAccountName                    = mySupplier().Name;
        Id myShipmentOrderId                    = myShipmentOrder().Id;
      
        Customer_Invoice__c myCustomerInvoice2   = new Customer_Invoice__c();
        myCustomerInvoice2.RecordTypeId          = Schema.SObjectType.Customer_Invoice__c.getRecordTypeInfosByName().get('New First Invoice').getRecordTypeId();
        myCustomerInvoice2.Client__c             = [SELECT Id 
                                                     FROM Account 
                                                    WHERE Name = :myAccountName LIMIT 1].Id; 
        myCustomerInvoice2.Shipment_Order__c     = [SELECT Id 
                                                     FROM Shipment_Order__c 
                                                    WHERE Id = :myShipmentOrderId LIMIT 1].Id;
        myCustomerInvoice2.Invoice_Type__c       = 'Cash Outlay Invoice';
        myCustomerInvoice2.Invoice_Status__c     = 'Invoice Sent';
        myCustomerInvoice2.Invoice_Amount__c     = 502;
        myCustomerInvoice2.Total_Applied_to_other_invoice__c = 22;
        myCustomerInvoice2.Total_Recieved__c     = 222;
        myCustomerInvoice2.Total_Credits_Applied__c = 32;
        myCustomerInvoice2.Total_Overpayments_Applied__c = 22;
        myCustomerInvoice2.Due_Date__c           = date.newInstance(2019, 7, 16);
        insert myCustomerInvoice2;
        return myCustomerInvoice2;
    }

 public static Customer_Invoice__c myCreditNoteInvoice(){
        String myAccountName                    = myClient().Name;
        Id myShipmentOrderId                    = myShipmentOrder().Id;
      
        Customer_Invoice__c myCreditNoteInvoice   = new Customer_Invoice__c();
        myCreditNoteInvoice.RecordTypeId          = Schema.SObjectType.Customer_Invoice__c.getRecordTypeInfosByName().get('Credit note').getRecordTypeId(); //0121v000000yvs3AAA'; //'
        myCreditNoteInvoice.Client__c             = [SELECT Id 
                                                     FROM Account 
                                                    WHERE Name = :myAccountName LIMIT 1].Id; 
        myCreditNoteInvoice.Shipment_Order__c     = [SELECT Id 
                                                     FROM Shipment_Order__c 
                                                    WHERE Id = :myShipmentOrderId LIMIT 1].Id;
        myCreditNoteInvoice.Invoice_Type__c       = 'Credit Note';

        insert myCreditNoteInvoice;
        return myCreditNoteInvoice;
    }

    public static AcctSeed__GL_Account__c myBankAccount(){
              //Bank Account record
        AcctSeed__GL_Account__c myBankAccount = new AcctSeed__GL_Account__c();
        myBankAccount.Name = 'Bank';
        myBankAccount.AcctSeed__Type__c = 'Expense';
        insert myBankAccount;
        return myBankAccount;
    }

    public static Bank_transactions__c bankTrans(){
        //Creating bank transaction record   
        String bankAccName = myBankAccount().Name;
        String myClientName = myClient().Name;
        Bank_transactions__c bankTrans = new Bank_transactions__c();
        bankTrans.RecordTypeId = Schema.SObjectType.Bank_transactions__c.getRecordTypeInfosByName().get('Payment').getRecordTypeId();
        bankTrans.Supplier__c = [SELECT Id 
                                 FROM Account 
                                WHERE Name = :myClientName LIMIT 1].Id;
        bankTrans.Date__c = System.today()-1;
        bankTrans.Amount__c = 123456;
        bankTrans.Forex_Rate__c = 1;
        bankTrans.Rate_adjustment__c = 1.03;
        bankTrans.Bank_charges__c = 15;
        bankTrans.Bank_Account__c = [SELECT Id 
                                       FROM AcctSeed__GL_Account__c 
                                      WHERE Name = :bankAccName LIMIT 1].Id;
        bankTrans.Currency__c = 'US Dollar (USD)';
        bankTrans.Type__c = 'Shipment';
        insert bankTrans;
        return bankTrans;
    }


    public static Attachment myAttachment(){
    
        Id parent = myClient().Id;
      
        Attachment att = new Attachment();
        att.Name = 'Test Attachment';
        Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');

        att.Body = bodyBlob;
        att.ParentId = parent;
                              
        insert att;
        return att;
    }

    public static Attachment myAttachment2(){
    
        Id parent = myClient().Id;
      
        Attachment att2 = new Attachment();
        att2.Name = 'Test Attachment';
        Blob bodyBlob=Blob.valueOf('Unit Test2 Attachment Body');

        att2.Body = bodyBlob;
        att2.ParentId = parent;
                              
        insert att2;
        return att2;
    }
}