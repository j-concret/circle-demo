public class TAuthentication {
    
    @AuraEnabled
    public static String TAuth() {
       
        //string baseUrl = "https://ui2.tipalti.com"; //production
        //String baseUrl = 'https://ui2.sandbox.tipalti.com'; //Sandbox
        //String masterKey = 'j2O2HSydXz/YqW6fLD1ZDXupBI8Q6yTf5euM3dRZD7IYfwodhvC12N8M/Gc5lckO';//sandbox
        //String masterKey = '6/AruxtA1M+gP+g5fZRtpYm0jXJdaQWZNv6k8oJu6DBYEH5papLWbnJGtc3o1SVg';//Production
        
        String Finalstring;
        try{
        ID contactId = Test.isRunningTest() ? [SELECT Id FROM Contact WHERE LastName='Test'].Id : [Select contactid from User where id =: Userinfo.getUserid()].contactId;
        if( contactId != null){
            Contact Con = [select Id,AccountID,Eligible_to_access_Tipalti__c from contact where id =:contactid];
            //String PayeeID  = [Select AccountID,Eligible_to_access_Tipalti__c from Contact where id =: contactid].AccountId;
            // String PayeeID='0010Y00000qXM1vQAG'; // uncomment above two lines and comment this line in portal.
            String payer = 'TecEx';
            
            String masterKey = '6/AruxtA1M+gP+g5fZRtpYm0jXJdaQWZNv6k8oJu6DBYEH5papLWbnJGtc3o1SVg';
            Decimal Timestampheader = datetime.now().gettime()/1000;
            String baseUrl = 'https://ui2.tipalti.com'; //integration
            String params='idap='+Con.AccountID+'&payer='+payer+'&ts='+Math.round(Timestampheader);
          // String params='idap=0011v00002keToNAAU&payer='+payer+'&ts='+Math.round(Timestampheader);
            String hashInBase64 = generateHmacSHA256Signature(params,masterKey);
            String IframeUrl = 'https://ui2.tipalti.com/payeedashboard/home?'+params+'&hashkey='+hashInBase64; // For verify bank details
            
            
            if(Con.Eligible_to_access_Tipalti__c == TRUE) {Finalstring = 'PASS'+IframeUrl;}      Else {Finalstring = 'FAIL'+IframeUrl;}
            System.debug('IframeUrl--->'+IframeUrl);
            System.debug('Finalstring--->'+Finalstring);
        }else{
            Finalstring = 'FAIL';
        } 
        }catch(Exception e){
            API_Log__c apiLog1 = new API_Log__c (Response__c = e.getMessage(),
                                                 Calling_Method_Name__c  = 'TAuth Detail Iframe');
            insert apiLog1;
            return e.getMessage();
        }
        
        return Finalstring;
    }
    @AuraEnabled
    public static String PaymentDetails() {
        
        
         //string baseUrl = "https://ui2.tipalti.com"; //production
        //String baseUrl = 'https://ui2.sandbox.tipalti.com'; //Sandbox
        //String masterKey = 'j2O2HSydXz/YqW6fLD1ZDXupBI8Q6yTf5euM3dRZD7IYfwodhvC12N8M/Gc5lckO';//sandbox
        //String masterKey = '6/AruxtA1M+gP+g5fZRtpYm0jXJdaQWZNv6k8oJu6DBYEH5papLWbnJGtc3o1SVg';//Production
        
        
        String Finalstring;
        try{
        ID contactId = Test.isRunningTest() ? [SELECT Id FROM Contact WHERE LastName='Test'].Id : [Select contactid from User where id =: Userinfo.getUserid()].contactId;
        if( contactId != null){
            Contact Con = [select Id,AccountID,Eligible_to_access_Tipalti__c from contact where id =:contactid];
            
            String payer = 'TecEx';
            String masterKey = '6/AruxtA1M+gP+g5fZRtpYm0jXJdaQWZNv6k8oJu6DBYEH5papLWbnJGtc3o1SVg';
            Decimal Timestampheader = datetime.now().gettime()/1000;
            String baseUrl = 'https://ui2.tipalti.com'; //integration
            String params='idap='+Con.AccountID+'&payer='+payer+'&preferredPayerEntity='+'&ts='+Math.round(Timestampheader);
            String hashInBase64 = generateHmacSHA256Signature(params,masterKey);
            String IframeUrl = 'https://ui2.tipalti.com/PayeeDashboard/PaymentsHistory?'+params+'&hashkey='+hashInBase64; // For payment details
            
            
            if(Con.Eligible_to_access_Tipalti__c == TRUE) {Finalstring = 'PASS'+IframeUrl;}      Else {Finalstring = 'FAIL'+IframeUrl;}
            System.debug('IframeUrl--->'+IframeUrl);
            System.debug('Finalstring--->'+Finalstring);
        }
        else{
            Finalstring = 'FAIL';
        } 
        }catch(Exception e){
            API_Log__c apiLog1 = new API_Log__c (Response__c = e.getMessage(),
                                                 Calling_Method_Name__c  = 'TAuth Detail Iframe');
            insert apiLog1;
            return e.getMessage();
        }
        
        return Finalstring;
    }
    
    //Generate Signature
    public  static String generateHmacSHA256Signature(String Keyvalue,string ClientsecreteValue) 
    {     
        String algorithmName = 'HmacSHA256';
        Blob hmacData = Crypto.generateMac(algorithmName,  Blob.valueOf(KeyValue), Blob.valueOf(ClientsecreteValue));
        String hextext = EncodingUtil.convertToHex(hmacData);
        
        return hextext;
    }
    
  
    
}