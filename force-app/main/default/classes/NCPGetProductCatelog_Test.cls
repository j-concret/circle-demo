@isTest
public class NCPGetProductCatelog_Test {

    static testMethod void  testForNCPGetProductCatelog(){
        
        Access_token__c at = new Access_token__c();
        at.Access_Token__c = 'cc407192-e2bf-4ada-9ddd-dfa2e13aba9c';
        at.Status__c='Active';
        Insert at;
        
        Account acc = new Account(Name = 'TecEx Prospective Client');
        Insert acc;
        
        NCPGetProductCatelogWrap obj = new NCPGetProductCatelogWrap();
        obj.Accesstoken = at.Access_Token__c;
        obj.AccountID = acc.Id;
        
        RestRequest req1 = new RestRequest();
        RestResponse res1 = new RestResponse();
        req1.requestURI = '/services/apexrest/NCPGetProductCatelog/'; 
        req1.httpMethod = 'POST';
        req1.requestBody = Blob.valueOf(JSON.serialize(obj));     
        RestContext.request = req1;
        RestContext.response = res1;
        RestContext.request.addHeader('Content-Type', 'application/json');
        
        Test.startTest(); 
        	NCPGetProductCatelog.NCPGetProductCatelog();
        Test.stopTest();
    }
    
    static testMethod void  testErrorForNCPGetProductCatelog(){
                
        RestRequest req1 = new RestRequest();
        RestResponse res1 = new RestResponse();
        req1.requestURI = '/services/apexrest/NCPGetProductCatelog/'; 
        req1.httpMethod = 'POST';
        req1.requestBody = Blob.valueOf('{"value":"Test Response"}');     
        RestContext.request = req1;
        RestContext.response = res1;
        RestContext.request.addHeader('Content-Type', 'application/json');
        
        Test.startTest(); 
        	NCPGetProductCatelog.NCPGetProductCatelog();
        Test.stopTest();
    }
}