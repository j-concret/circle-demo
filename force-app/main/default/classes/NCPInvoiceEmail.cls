@RestResource(urlMapping='/InvoiceEmail/*')
Global class NCPInvoiceEmail {
    
    

     @Httppost
      global static void NCPInvoiceEmail(){
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        Blob body = req.requestBody;
        res.addHeader('Content-Type', 'application/json');
        String requestString = body.toString();
        NCPInvoiceEmailWra rw  = (NCPInvoiceEmailWra)JSON.deserialize(requestString,NCPInvoiceEmailWra.class);
          try{
              Access_token__c At =[select status__c,Access_Token__c from Access_token__c where Access_Token__c=:rw.Accesstoken ];
            	if( at.status__C =='Active'){
                    if(rw.ccEmailID ==null){ rw.ccEmailID=rw.toEmailID;}
                    ID customerId=rw.InvoiceID;
					List<String> toAddress = new List<String>();
                    toaddress.add(rw.toEmailID);
                     List<String> ccAddress = new List<String>();
                  
                    for(String cc : rw.ccEmailID.split(';')){ccAddress.add(cc); }
                    
                    
              Boolean isemailSent =      CTRL_generateAndSendInvoice.NCPsendInvoice(customerId,toAddress,ccAddress); 
                    
                    If(isemailSent==TRUE) {
          
             String abc= '"Success - InvoiceEmail sent"';
             res.responseBody = Blob.valueOf(abc);
             res.addHeader('Content-Type', 'application/json');
             res.statusCode = 200;
                
                API_Log__c Al = New API_Log__c();
                Al.EndpointURLName__c='InvoiceEmail';
               // Al.Account__c=rw.AccountID;
                Al.StatusCode__c=string.valueof(res.statusCode);
                AL.Response__c = 'Success - InvoiceEmail sent';
                Insert Al;  
                    }
                    Else{
                        String abc= ' InvoiceEmail not sent, Please contact  support_SF@tecex.com';
             res.responseBody = Blob.valueOf(abc);
             res.addHeader('Content-Type', 'application/json');
             res.statusCode = 400;
                        
                         API_Log__c Al = New API_Log__c();
                Al.EndpointURLName__c='InvoiceEmail';
              //  Al.Account__c=rw.AccountID;
                Al.StatusCode__c=string.valueof(res.statusCode);
                AL.Response__c = ' InvoiceEmail not sent, due to no attachement on record'+rw.InvoiceID;
                Insert Al; 
                    }
              }
              
              else{
                   JSONGenerator gen = JSON.createGenerator(true);
            gen.writeStartObject();
            
             gen.writeFieldName('Error');
             gen.writeStartObject();
            
                gen.writeStringField('Response', 'Accesstoken Expired');
            
            gen.writeEndObject();
            gen.writeEndObject();
            String jsonData = gen.getAsString();
            res.responseBody = Blob.valueOf(jsonData);
            res.addHeader('Content-Type', 'application/json');
            res.statusCode = 400;        
            
                  
              }
             
                }
              
            catch(Exception e){
                
                
               
                
                String ErrorString ='Something went wrong, please contact support_SF@tecex.com'+e.getMessage()+e.getLineNumber();
                       res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
                   res.addHeader('Content-Type', 'application/json');
                    res.statusCode = 500;
                
                API_Log__c Al = New API_Log__c();
             //   Al.Account__c=rw.AccountID;
                //Al.Login_Contact__c=logincontact.Id;
                Al.EndpointURLName__c='InvoiceEmail';
                Al.Response__c=ErrorString+'InvoiceID-->'+rw.InvoiceID;
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al;
                      
          
          
          
      
            }
      
            }

}