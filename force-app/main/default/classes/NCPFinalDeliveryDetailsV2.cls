@RestResource(urlMapping='/GetFinalDeliveryDetailsV2/*')
Global class NCPFinalDeliveryDetailsV2 {
    
    @Httppost
    global static void FinalDeliveryDetails(){
        
       RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        Blob body = req.requestBody;
    	res.addHeader('Content-Type', 'application/json');
        String requestString = body.toString();
        NCPFinalDeliveryDetailsWraV2 rw = (NCPFinalDeliveryDetailsWraV2)JSON.deserialize(requestString,NCPFinalDeliveryDetailsWraV2.class);
        
        try {  
          
          
           Access_token__c At =[select status__c,Access_Token__c from Access_token__c where Access_Token__c=:rw.Accesstoken ];
           if( at.status__C =='Active'){
               
               Final_Delivery__c FD = [select id,Shipment_Order__r.account__c,Name,Ship_to_address__c , Ship_to_address__r.Name , Ship_to_address__r.Address__c , Ship_to_address__r.City__c , Ship_to_address__r.Country__c , Ship_to_address__r.Province__c , Ship_to_address__r.Contact_Email__c , Ship_to_address__r.Contact_Full_Name__c , Ship_to_address__r.Contact_Phone_Number__c , Ship_to_address__r.All_Countries__c , Ship_to_address__r.CompanyName__c , Ship_to_address__r.Address2__c,Ship_to_address__r.Postal_Code__c from Final_Delivery__c where id=:rw.FDID];
            
          		  res.responseBody = Blob.valueOf(JSON.serializePretty(FD));
               	  res.addHeader('Content-Type', 'application/json');
        		  res.statusCode = 200;
                
                API_Log__c Al = New API_Log__c();
              //  Al.Account__c=LoginAccount;
             //   Al.Login_Contact__c=LoginContact;
                Al.EndpointURLName__c='FinalDeliveryDetails';
                Al.Response__c='Success - FinalDelivery Details are sent';
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al;
              
          }
          Else{
                
              string ErrorString = 'Failed - Invalid accesstoken';
              res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
               res.addHeader('Content-Type', 'application/json');
        	  res.statusCode = 400;
                
                API_Log__c Al = New API_Log__c();
             //   Al.Account__c=LoginAccount;
              //  Al.Login_Contact__c=LoginContact;
                Al.EndpointURLName__c='FinalDeliveryDetails';
                Al.Response__c='Failed - Failed - Invalid accesstoken';
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al;
              
              
          }
          
          
      
      
      }
        catch(Exception e){
            
           	String ErrorString ='Something went wrong while getting final delivery details, please contact support_SF@tecex.com';
             res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
             res.addHeader('Content-Type', 'application/json');
            res.statusCode = 500;
                 API_Log__c Al = New API_Log__c();
              //  Al.Account__c=LoginAccount;
              //  Al.Login_Contact__c=LoginContact;
                Al.EndpointURLName__c='FinalDeliveryDetails';
                Al.Response__c='Exception - FinalDelivery Details are not sent';
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al;
            
            
        }
        
    }
    

}