@RestResource(urlMapping='/NCPGetClientBuyer/*')
Global class GetClientBuyer {
     @Httppost
    global static void ClientBuyer(){
        
       RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        Blob body = req.requestBody;
        res.addHeader('Content-Type', 'application/json');
        String requestString = body.toString();
        GetClientBuyerwra rw = (GetClientBuyerwra)JSON.deserialize(requestString,GetClientBuyerwra.class);
        
         try {
             
            System.debug('Accesstoken-->'+rw.Accesstoken);
             
            Access_token__c At =[select status__c,Access_Token__c from Access_token__c where Access_Token__c=:rw.Accesstoken ];
            if( at.status__C =='Active'){
                
                 List<Client_Buyer__c> records = new List<Client_Buyer__c>();
                 
                 String accID=rw.AccountID;
                 String query = 'Select Buyer_Account_Name__c,Buyer_Account__c from Client_Buyer__c where Client_Account__c =:accID and Buyer_Account_Name__c like\'%'+rw.searchaccountName+'%\'';
                	
                 records = Database.query(query);
           		 System.debug('Final Query' +query);
                
             res.responseBody = Blob.valueOf(JSON.serializePretty(records));
             res.addHeader('Content-Type', 'application/json');
             res.statusCode = 200;
                
                API_Log__c Al = New API_Log__c();
              //    Al.Account__c=reqBody.AccountID;
              //  Al.Login_Contact__c=rw.ContactID;
                Al.EndpointURLName__c='NCPGetClientBuyer';
                Al.Response__c='Success - NCPGetClientBuyer';
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al;  

                
                
            }
         }
        catch(Exception e){
            
                String ErrorString ='Something went wrong while creating cost estimate, please contact support_SF@tecex.com';
                res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
             	res.addHeader('Content-Type', 'application/json');
                res.statusCode = 500;
                
            	API_Log__c Al = New API_Log__c();
                Al.EndpointURLName__c='NCPGetClientBuyer';
                Al.Response__c=e.getMessage()+' at line number: '+e.getLineNumber();
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al;
            
        }
    }
    
    
    
}