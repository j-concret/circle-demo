@RestResource(urlMapping='/NCPCreateClientBuyer/*')
Global class ClientBuyer {
    
     @Httppost
    global static void ClientBuyer(){
        
         RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        Blob body = req.requestBody;
        res.addHeader('Content-Type', 'application/json');
        String requestString = body.toString();
        ClientBuyerWra rw = (ClientBuyerWra)JSON.deserialize(requestString,ClientBuyerWra.class);
        
         try {
            Access_token__c At =[select status__c,Access_Token__c from Access_token__c where Access_Token__c=:rw.Accesstoken ];
            if( at.status__C =='Active'){
                Buyer_Accounts__c BA = New Buyer_Accounts__c(name=rw.ClientBuyeraccountName);
                Insert BA;
                
                Buyer_Accounts__c BA1 =[Select Id, name from Buyer_Accounts__c where ID = :BA.ID];
                
                Client_Buyer__c CB =new Client_Buyer__c(Buyer_Account__c= BA1.Id,Client_Account__c =rw.AccountID);
                Insert CB;
                
             res.responseBody = Blob.valueOf(JSON.serializePretty(BA1));
             res.addHeader('Content-Type', 'application/json');
             res.statusCode = 200;
                
                API_Log__c Al = New API_Log__c();
                Al.Account__c=rw.AccountID;
              //  Al.Login_Contact__c=rw.ContactID;
                Al.EndpointURLName__c='NCPCreateClientBuyer';
                Al.Response__c='Success - NCPCreateClientBuyer Created - '+BA1.ID;
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al;  
                
                
                
            }
         }
             catch(Exception e){
                 
                String ErrorString ='Something went wrong while creating cost estimate, please contact support_SF@tecex.com';
                res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
             	res.addHeader('Content-Type', 'application/json');
                res.statusCode = 500;
                
            	API_Log__c Al = New API_Log__c();
                Al.Account__c= rw.AccountID;
              //  Al.Login_Contact__c=rw.ContactID;
                Al.EndpointURLName__c='NCPCreateClientBuyer';
                Al.Response__c=e.getMessage()+' at line number: '+e.getLineNumber();
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al;
                 
             }
        
    }
    
    
}