public class QueueableUpdateSO implements Queueable {
    
    shipment_Order__c SO;
    
    public QueueableUpdateSO(shipment_Order__c SO){
        this.SO = SO;
        
    }
    
    
    public void execute(QueueableContext context) { 
         Decimal DATPCCD1=0; 
         Integer CountID1=0; 
        AggregateResult[] groupedResults = [SELECT COUNT(ID) CountID,SUM(Total_Value__c) DATPCCD from part__c  where  Shipment_Order__c =: SO.ID];
            system.debug('groupedResults (ShipmentOrder) -->'+groupedResults);
            
            for(AggregateResult a : groupedResults){
                DATPCCD1 = (Decimal) a.get('DATPCCD');
                 CountID1 = (Integer) a.get('CountID');
                
            }
        
        if(CountID1 >= 1){
        SO.Total_Line_Item_Extended_Value__c = DATPCCD1;
        SO.Shipment_Value_USD__c = DATPCCD1;   
        }
        
        SO.Shipping_Status__c= 'Shipment Pending';
        Update SO;
            
    }

}