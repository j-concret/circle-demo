public class RolloutExportforNCP {
    
   public static Attachment RolloutExportforNCP(Id RolloutID){
    Attachment statementAttachment  = new Attachment();
       Blob pdfPageBlob;  
    
    //#####Email attachment
    PageReference statementPdf = new PageReference(Site.getPathPrefix() + '/apex/ExportSORollOut');
    statementPdf.getParameters().put('id', RolloutID);
     pdfPageBlob = (Test.isRunningTest()) ?
    Blob.valueOf(String.valueOf('This is a test page')) : statementPdf.getContent();
       
     statementAttachment.Body        = pdfPageBlob;
    statementAttachment.Name        = 'Rollout'+'.xls';
    statementAttachment.ParentId    = RolloutID;
    statementAttachment.IsPrivate   = false;
    statementAttachment.Description = 'Rollout';
    
   
    return statementAttachment;
    }
   
    

}