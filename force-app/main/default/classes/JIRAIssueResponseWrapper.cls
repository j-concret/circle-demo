public with sharing class JIRAIssueResponseWrapper {
    public class Errors {
    }

    //success response 201
    public List<Issues> issues;
    public List<Errors> errors;

    public class Issues {
        public String id;
        public String key;
        public String self;
    }

    //error response 400
    public Integer status;
    public ElementErrors elementErrors;
    public Integer failedElementNumber;

    public class ElementErrors {
        public List<String> errorMessages;
        public Errors errors;
    }
}