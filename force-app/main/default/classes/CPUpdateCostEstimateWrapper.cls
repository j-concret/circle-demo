public class CPUpdateCostEstimateWrapper {
    public String ID;
    public String Reference1;
	public String Reference2;
	public Double Shipment_Value_USD;
	public String request_EmailEstimate;
	public String Email_Estimate_to;
	public String Courier_responsibility;
	public String LionBatteries;
	public String LionBatteriestype;
	public String TypeOfGoods;
    public String PONumber;

	
	public static CPUpdateCostEstimateWrapper parse(String json) {
		return (CPUpdateCostEstimateWrapper) System.JSON.deserialize(json, CPUpdateCostEstimateWrapper.class);
	}

}