@isTest
public class Lex_AutoFill_Address_Test implements HttpCalloutMock {
    
    private static HttpResponse resp;
    
    public Lex_AutoFill_Address_Test(String testBody,Boolean isTest) {
        
        resp = new HttpResponse();
        
        resp.setBody(testBody);
        
        if(isTest == true){
            
            resp.setStatus('OK');
            resp.setStatusCode(200);
            
        }else{
            
            resp.setStatus('Error');
            resp.setStatusCode(404);
            
        }
    }
    
    public HTTPResponse respond(HTTPRequest req) {
        
        return resp;
        
    }
    
    public testmethod static void testLex_AutoFill_Address() {
        
        no_zip_code_countries__c nzp = new no_zip_code_countries__c(
            Name='Angola',
            zip_code__c='12345');
        insert nzp;
        
        Test.startTest();
        
        HttpCalloutMock mock = new Lex_AutoFill_Address_Test('testBody',true);
        Test.setMock(HttpCalloutMock.class, mock);
        
        Lex_AutoFill_Address.getAddressDetailsByPlaceId('12345');
        Lex_AutoFill_Address.getAddressFrom('AbC');
        Lex_AutoFill_Address.getZipCodeByCountry('Angola');
        
        Test.stopTest();
        
    }
}