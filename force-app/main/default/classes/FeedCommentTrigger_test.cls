@isTest
public class FeedCommentTrigger_test {
  static testMethod void  ProfileWithoutTecexCustomer(){
        
        Case caseObj = new Case(Subject='Test', Status = 'New');
        insert caseObj;
        FeedItem f = new FeedItem();
        f.ParentId = caseObj.Id;
        f.body = 'test';
        insert f;
        FeedComment fc = new FeedComment();
        fc.CommentBody = 'legal test';
      fc.FeedItemId=f.Id;
        insert fc;
        Task t = new Task(Subject='Donni',Status='Not Started',Priority='Normal');
        insert t;
      FeedItem f1 = new FeedItem();
        f1.ParentId = t.Id;
        f1.body = 'test';
        insert f1;
          FeedComment fc1 = new FeedComment();
      fc1.FeedItemId=f1.Id;
        fc1.CommentBody = 'legal test';
        insert fc1;
        
        
    }
    
    static testMethod void  ProfileWithTecexCustomer(){
        
        Profile profileId = [SELECT Id FROM Profile WHERE Name = 'Tecex Customer' LIMIT 1];
        
        Account account = new Account(name='Acme1');
        insert account;
        
        Contact contact = new Contact ( lastname ='Testing Individual', Client_Notifications_Choice__c='Opt-In', AccountId = account.Id); 
        insert contact;  
        
        User usr = new User(LastName = 'LIVESTON',
                            FirstName='JASON',
                            Alias = 'jliv',
                            Email = 'jason.liveston@asdf.com',
                            Username = 'jason.liveston@asdf.com',
                            ProfileId = profileId.id,
                            ContactId = contact.Id,
                            TimeZoneSidKey = 'GMT',
                            LanguageLocaleKey = 'en_US',
                            EmailEncodingKey = 'UTF-8',
                            LocaleSidKey = 'en_US'
                           );
        insert usr;
        
        system.runAs(usr){
          Case caseObj = new Case(Subject='Test', Status = 'New');
        insert caseObj;
        FeedItem f = new FeedItem();
        f.ParentId = caseObj.Id;
        f.body = 'test';
        insert f;
        FeedComment fc = new FeedComment();
        fc.CommentBody = 'legal test';
      fc.FeedItemId=f.Id;
        insert fc;
        Task t = new Task(Subject='Donni',Status='Not Started',Priority='Normal');
        insert t;
      FeedItem f1 = new FeedItem();
        f1.ParentId = t.Id;
        f1.body = 'test';
        insert f1;
          FeedComment fc1 = new FeedComment();
      fc1.FeedItemId=f1.Id;
        fc1.CommentBody = 'legal test';
        insert fc1;
        }
        
    }
}