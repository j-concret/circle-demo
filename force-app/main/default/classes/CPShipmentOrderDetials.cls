@RestResource(urlMapping='/ShipmentOrderDetails/*')
Global class CPShipmentOrderDetials {

    @Httpget
    global static void ShipmentOrderDetials(){

        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        res.addHeader('Content-Type', 'application/json');
        String Username = req.params.get('Username');
        String Password = req.params.get('Password');
        String SOID = req.params.get('SOID');
        Contact logincontact = [select Id,Account.id, lastname,Email,username__c,Password__c,Contact_Status__c from Contact where username__c =:username and Contact_status__c = 'Active'];
        try {

            if(logincontact.Password__c == Password && logincontact.username__c == Username && logincontact.Contact_status__C == 'Active' ) {

                String query = 'SELECT '+String.join(new List<String>(Shipment_order__c.sObjectType.getDescribe().fields.getMap().keySet()),',') + ' FROM shipment_Order__c where Id =:SOID';

                Shipment_order__c SOs = Database.query(query);
                res.responseBody = Blob.valueOf(JSON.serializePretty(SOs));
                res.statusCode = 200;

                API_Log__c Al = New API_Log__c();
                Al.Account__c=logincontact.AccountID;
                Al.Login_Contact__c=logincontact.Id;
                Al.EndpointURLName__c='ShipmentOrderDetails';
                Al.Response__c='Success - shipment orders Details are sent';
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al;
            }
            else{
                String ErrorString ='Authentication failed, Please check username and password';
                res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
                res.statusCode = 400;

                API_Log__c Al = New API_Log__c();
                Al.Account__c=logincontact.AccountID;
                Al.Login_Contact__c=logincontact.Id;
                Al.EndpointURLName__c='ShipmentOrderDetails';
                Al.Response__c=ErrorString;
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al;
            }
        }
        Catch(exception e){
            String ErrorString ='Failed, Please check username and password';
            res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
            res.statusCode = 500;
            API_Log__c Al = New API_Log__c();
            Al.Account__c=logincontact.AccountID;
            Al.Login_Contact__c=logincontact.Id;
            Al.EndpointURLName__c='ShipmentOrderDetails';
            Al.Response__c=e.getMessage();
            Al.StatusCode__c=string.valueof(res.statusCode);
            Insert Al;
        }
    }
}