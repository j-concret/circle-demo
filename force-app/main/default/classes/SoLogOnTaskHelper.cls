public class SoLogOnTaskHelper {
    public static Boolean isRobotChanged = false;
     public static List<String> ExcludedShippingStatus = new List<String>{'POD Received','Customs Clearance Docs Received','Cancelled - With fees/costs','Cancelled - No fees/costs','Shipment Abandoned'};
        public static List<String> PastExcludedShippingStatus = new List<String>{'In transit to country','Arrived in Country, Awaiting Customs Clearance','Cleared Customs','Final Delivery in Progress'};
            public static void getShipmentAndMasterTaskDetails(Map<String,String> shipmentIdsMap,Set<Id> masterTaskIds, Map<String,Task> tasksMap){
                Map<Id,String> shipmentIds = new Map<Id,String>();
                Map<Id,Task> descriptionMap = new Map<Id,Task>();
                Set<Id> shipmentInternalIds = new Set<Id>();
                Map<String,Shipment_Order__c> SOList = new Map<String,Shipment_Order__c>();
                Map<String,String> CPAValuesMap = new Map<String,String>();
                Map<String,SO_Travel_History__c> SoTravelMap = new Map<String,SO_Travel_History__c>();

                Map<Id,Master_Task__c> masterTaskById = new Map<Id,Master_Task__c>([SELECT Name, Id,Tecex_Assigned_to_Under_Review__c, Notifications__c, Internal_Notifications__c FROM Master_Task__c WHERE Id IN :masterTaskIds]);

                for(Master_Task__c masterTask : masterTaskById.values()) {
                    Task task = tasksMap.get(masterTask.Id);

                    if(masterTask.Internal_Notifications__c != null) {
                        if( (task.State__c == 'TecEx Pending' && masterTask.Internal_Notifications__c.contains('State change to Tecex Pending')) || (task.State__c == 'Under Review' && masterTask.Internal_Notifications__c.contains('State change to Under Review')) || (task.State__c == 'Resolved' && masterTask.Internal_Notifications__c.contains('State change to Resolved') ) ) {
                            shipmentIds.put(shipmentIdsMap.get(masterTask.Id),'Tecex');
                            descriptionMap.put(shipmentIdsMap.get(masterTask.Id),task);
                        }
                    }
                    if(masterTask.Notifications__c != null) {
                        if( (task.State__c == 'Client Pending' && masterTask.Notifications__c.contains('State change to Client Pending')) || (task.State__c == 'Resolved' && masterTask.Notifications__c.contains('State change to Resolved')) ) {
                            shipmentIds.put(shipmentIdsMap.get(masterTask.Id),'Client');
                            descriptionMap.put(shipmentIdsMap.get(masterTask.Id),task);
                        }
                    }

                }

                for(Shipment_Order__c shipmentOrder : [SELECT Id, Shipping_Status__c,Clearance_Destination__c,Ship_to_Country_new__c, CPA_v2_0__c,Client_Contact_for_this_Shipment__r.Client_Notifications_Choice__c,Client_Contact_for_this_Shipment__r.Major_Minor_notification__c,Client_Contact_for_this_Shipment__r.Client_Tasks_notification_choice__c,Mapped_Shipping_status__c, Expected_Date_to_Next_Status__c FROM Shipment_Order__c WHERE Id IN :shipmentIds.keySet()]) {

                    SOList.put(shipmentOrder.Id,shipmentOrder);
                    CPAValuesMap.put(shipmentOrder.CPA_v2_0__c,shipmentOrder.Id);
                    Task task = descriptionMap.get(shipmentOrder.Id);
                    SoTravelMap.put(shipmentOrder.Id,
                                    new SO_Travel_History__c (
                                        Source__c = (shipmentIds.get(shipmentOrder.Id) == 'Client') ? 'Tasks (client)' : 'Task (internal)',
                                        Date_Time__c = System.now(),
                                        Location__c = '',
                                        SO_Shipping_Status__c  = shipmentOrder.Shipping_Status__c,
                                        Mapped_Status__c = shipmentOrder.Mapped_Shipping_status__c,
                                        Description__c = 'The Task '+task.Subject+' was updated to a '+task.State__c+ ' State',//'No Description available on the CPA',//SOSD.Description_ETA__c,
                                        // Task__c = task.Id,
                                        Task_State__c = task.State__c,
                                        Tecex_Assigned_to_Under_Review__c = masterTaskById.get(task.Master_Task__c).Tecex_Assigned_to_Under_Review__c,
                                        Task_Subject__c = task.Subject,
                                        Expected_Date_to_Next_Status__c = shipmentOrder.Expected_Date_to_Next_Status__c,
                                        Type_of_Update__c = 'NONE',
                                        TecEx_SO__c = shipmentOrder.Id,
                                        Client_Notifications_Choice__c =shipmentOrder.Client_Contact_for_this_Shipment__r.Client_Notifications_Choice__c != null ? shipmentOrder.Client_Contact_for_this_Shipment__r.Client_Notifications_Choice__c : 'Not applicable',
                                        Major_Minor_notification__c = shipmentOrder.Client_Contact_for_this_Shipment__r.Major_Minor_notification__c != null ? shipmentOrder.Client_Contact_for_this_Shipment__r.Major_Minor_notification__c : '',
                                        Client_Tasks_notification_choice__c = shipmentOrder.Client_Contact_for_this_Shipment__r.Client_Tasks_notification_choice__c
                                    )
                                   );
                }

                List<SO_Travel_History__c> SoLogs = [select Id FROM SO_Travel_History__c where TecEx_SO__c IN :shipmentIds.keySet() AND SO_Shipping_Status__c IN: ExcludedShippingStatus];
                if(SoTravelMap != null && !SoTravelMap.isEmpty() && SoLogs.isEmpty()) {
                    System.debug('inser soTravelMap'+JSON.serialize(SoTravelMap.values()));
                    insert SoTravelMap.values();
                }
            }

    public static void getSummary(Map<Id, Shipment_Order__c> soMap){
        Map<Id,Shipment_Order__c> oldShipMap= new Map<Id,Shipment_Order__c>(soMap.values().deepClone(true,true,true));
        System.debug('oldShipMap '+JSON.serialize(oldShipMap));
        List<Shipment_Order__c> SOlist21= new List<Shipment_Order__c>();
        Map<Id,Shipment_Order__c> SOlist11= new Map<Id,Shipment_Order__c>();
        Map<Id,String> DescriptionMap= new Map<Id,String>();

        Map<String,RobotSummaryCtrl.RobotSummaryWrapper> categories = RobotSummaryCtrl.getPicklistValues('Task','Task_Category__c');

        for(Task tsk : [SELECT Id,Task_Category__c,State__c,Blocks_Approval_to_Ship__c FROM Task WHERE WhatId =:soMap.keySet() AND Master_Task__c != null AND IsNotApplicable__c = false]) {
            if(categories.containsKey(tsk.Task_Category__c) && categories.get(tsk.Task_Category__c).priority > 1) {
                if(tsk.State__c == 'Client Pending' && tsk.Blocks_Approval_to_Ship__c) {
                    categories.get(tsk.Task_Category__c).priority = 1;//red
                }else if( categories.get(tsk.Task_Category__c).priority > 2 && (tsk.State__c == 'Not Started' || tsk.State__c == 'TecEx Pending' || tsk.State__c == 'Under Review') &&  tsk.Blocks_Approval_to_Ship__c) {
                    categories.get(tsk.Task_Category__c).priority = 2;//orange
                }else if(categories.get(tsk.Task_Category__c).priority > 2 && (tsk.State__c == 'Resolved' || (tsk.State__c != 'Resolved' && !tsk.Blocks_Approval_to_Ship__c))) {
                    categories.get(tsk.Task_Category__c).priority = 3;//green
                }
            }
        }

        for(Shipment_Order__c shipment : soMap.values()){
            shipment.Invoice_Payment__c = categories.get('Invoice Payment').priority == 1 ? 'Red' : categories.get('Invoice Payment').priority == 2 ? 'Orange' :'Green';
            shipment.Pick_up_Coordination__c = categories.get('Pick-up Coordination').priority == 1 ? 'Red' : categories.get('Pick-up Coordination').priority == 2 ? 'Orange' :'Green';
            shipment.Shipping_Documents__c = categories.get('Shipping Documents').priority == 1 ? 'Red' : categories.get('Shipping Documents').priority == 2 ? 'Orange' :'Green';
            shipment.Customs_Compliance__c = categories.get('Customs Compliance').priority == 1 ? 'Red' : categories.get('Customs Compliance').priority == 2 ? 'Orange' :'Green';
        }
        System.debug('soMap update '+JSON.serialize(soMap));

        for(Id id:soMap.keySet()) {
            Shipment_Order__c so = soMap.get(id);

            if(((oldShipMap.get(so.id).Invoice_Payment__c != so.Invoice_Payment__c && so.Invoice_Payment__c=='Green') ||
                (oldShipMap.get(so.id).Pick_up_Coordination__c != so.Pick_up_Coordination__c && so.Pick_up_Coordination__c=='Green') ||
                (oldShipMap.get(so.id).Shipping_Documents__c != so.Shipping_Documents__c && so.Shipping_Documents__c=='Green' )||
                (oldShipMap.get(so.id).Customs_Compliance__c != so.Customs_Compliance__c && so.Customs_Compliance__c=='Green'))
              ) {
                  if(oldShipMap.get(so.id).Invoice_Payment__c != so.Invoice_Payment__c) {
                      DescriptionMap.put(So.Id, 'Invoice Payment');
                  }else if(oldShipMap.get(so.id).Pick_up_Coordination__c != so.Pick_up_Coordination__c) {
                      DescriptionMap.put(So.Id, 'Pick-up Coordination');
                  }else if(oldShipMap.get(so.id).Shipping_Documents__c != so.Shipping_Documents__c) {
                      DescriptionMap.put(So.Id, 'Shipping Documents');
                  }else if(oldShipMap.get(so.id).Customs_Compliance__c != so.Customs_Compliance__c) {
                      DescriptionMap.put(So.Id, 'Customs Compliance');
                  }
                  SOlist21.add(so);
              }

            if((oldShipMap.get(SO.id).Invoice_Payment__c != SO.Invoice_Payment__c ||
                oldShipMap.get(SO.id).Pick_up_Coordination__c != SO.Pick_up_Coordination__c ||
                oldShipMap.get(SO.id).Shipping_Documents__c != SO.Shipping_Documents__c ||
                oldShipMap.get(SO.id).Customs_Compliance__c != SO.Customs_Compliance__c) &&
               (SO.Invoice_Payment__c == 'Green' &&
                SO.Pick_up_Coordination__c == 'Green' &&
                SO.Shipping_Documents__c == 'Green' &&
                SO.Customs_Compliance__c == 'Green' ) &&
               So.mapped_shipping_status__c == 'Compliance Pending') {
                   So.Shipping_Status__c = 'Client Approved to ship';
                   SOlist11.put(So.Id,so);
               }
        }

        if(!SOlist21.isEmpty() && !CreateSoLogonStatusChange.isSOLogCreated) {

            /*** New code implementation for Description based on ROBOTS ***/
            List<SO_Travel_History__c> SoLogs = [select Id FROM SO_Travel_History__c where TecEx_SO__c IN:SOlist21 AND (Mapped_Status__c = 'Approved to Ship' OR SO_Shipping_Status__c IN: ExcludedShippingStatus OR SO_Shipping_Status__c IN:PastExcludedShippingStatus)];
           System.debug('SoLogs '+JSON.serialize(SoLogs));
            if(SoLogs.isEmpty()){
                System.debug('SO logs are not empty');
                SOTravelCreation.forRobotsChange(SOlist21,DescriptionMap);
            }

        }

        if(!SOlist11.isEmpty() ) {

            List<SO_Travel_History__c> SoLogs = [select Id FROM SO_Travel_History__c where TecEx_SO__c IN:SOlist11.keySet() AND Mapped_Status__c = 'Approved to Ship'];
            if(SoLogs.isEmpty()){
                SOTravelCreation.forSOStatusChange(SOlist11,'SO Status Changed');
            }
        }
    }

}