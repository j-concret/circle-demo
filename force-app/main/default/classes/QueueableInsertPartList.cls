public class QueueableInsertPartList implements Queueable{
    
     @testVisible
    private static Boolean doChainJob = true;
    List<shipment_Order__c> SOList;
   
    String PAList;
    
    public QueueableInsertPartList(List<shipment_Order__c> SO,String PA){
        this.SOList = SO;
        this.PAList = PA;
        
    }
    
    
    public void execute(QueueableContext context) { 
         list<Part__c> PAL = new list<Part__c>();  
        
        					for(Integer l=0; l<SOList.size();l++) {
                        				insert SOList[l];
                        					}
      	List<shipment_Order__c> SOList1 = new List<shipment_Order__c>();
        SOList1=SOList;
        
        System.debug('JSONString PPart_List-->'+PAList);
             	string code1 = PAList.replace('[','{ "Parts":[').replace(']','] }');
             	String  JsonString = code1;
             	System.debug('JSONString'+JSONString);
            	CPCreatePartsWrapperInternal rw;
             	If(!JSONString.contains('Chuck')){
           				rw = (CPCreatePartsWrapperInternal)JSON.deserialize(JSONString,CPCreatePartsWrapperInternal.class);
           				System.debug('rw.Parts-->'+rw.parts);
             		}
        
                      If(String.isNotBlank(PAList) && !JSONString.contains('Chuck') ){
                          
                    //   list<Part__c> PAL = new list<Part__c>();  
                          
                          
                          for(Integer i=0; i<=SOList.size();i++) {
                			 if(i<SOList.size()){
                     
                          for(Integer l=0; rw.Parts.size()>l;l++) {
                             Part__c PA = new Part__c(
                         	 
                                  Name =rw.Parts[l].PartNumber,
                                  Description_and_Functionality__c =rw.Parts[l].PartDescription,
                                  Quantity__c =rw.Parts[l].Quantity,
                                  Commercial_Value__c =rw.Parts[l].UnitPrice,
                                  US_HTS_Code__c =rw.Parts[l].HSCode,
                                  Country_of_Origin2__c =rw.Parts[l].CountryOfOrigin,
                                  ECCN_NO__c =rw.Parts[l].ECCNNo,
                                  Type_of_Goods__c=rw.Parts[l].Type_of_Goods,
                                  Li_ion_Batteries__c=rw.Parts[l].Li_ion_Batteries,
                                  Lithium_Battery_Types__c=rw.Parts[l].Li_ion_BatteryTypes,
                                  Shipment_Order__c=SOList[i].id
                         
                         		);
                           PAL.add(PA);
                          }
                           }
                          }
                          
                                      
                      }
        	System.debug('PAL size-->'+PAL.size());
        integer a =0;
          If(String.isNotBlank(PAList) && !JSONString.contains('Chuck') && doChainJob ==TRUE){ // Newly added line
             System.enqueueJob(new QueueableInsertPartList2(PAL,a)); 
          }
        
   
            						}
    
     
    
}