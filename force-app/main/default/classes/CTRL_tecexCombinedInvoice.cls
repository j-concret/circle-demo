public class CTRL_tecexCombinedInvoice {

    public Invoice_New__c customerInvoice {get;set;}
    public Shipment_Order__c shipmentOrder {get;set;}
    public Account account {get;set;}
	public Enterprise_Billing__c ebillObject {get;set;}
    public List<Invoice_New__c> invoiceList {get;set;}
    public Decimal combinedIORANDCompFee {get;set;}
    public Decimal combinedEORANDCompFee {get;set;}
    public Decimal combinedAdminFee {get;set;}
    public Decimal combinedSubTotalIOREOR {get;set;}
    public Decimal combinedintFreightFee {get;set;}
    public Decimal combinedLiabilityFee {get;set;}
    public Decimal combinedSubTotalFreight {get;set;}
    public Decimal combinedRechargeTaxAndDuty {get;set;}
    public Decimal combinedRechargeTaxAndDutyOther {get;set;}
    public Decimal combinedCustomsBrokerageCost {get;set;}
    public Decimal combinedClearanceCosts {get;set;}
    public Decimal combinedLicenseCosts {get;set;}
    public Decimal combinedHandlingCosts {get;set;}
    public Decimal combinedBankFee {get;set;}
    public Decimal combinedMiscellaneousFee {get;set;}
    public Decimal combinedCashDisbursementFeePossibleCashOutlay {get;set;}
    public Decimal combinedCashDisbursementFee {get;set;}
    public Decimal combinedSubTotalGovernmentandIncountrySummary {get;set;}
    public Decimal combinedTotalAmountInclPotentialCashOutlay {get;set;}
    
    public CTRL_tecexCombinedInvoice(ApexPages.StandardController controller) {
        
        this.ebillObject =	[SELECT Id , Name , Customer_PO__c , Account__r.Id , Account__r.Name , Account__r.IOR_Payment_Terms__c , 
                             Account__r.VAT_Number__c, Account__r.BillingStreet, Account__r.BillingCity, 
                             Account__r.BillingState, Account__r.BillingPostalCode, Account__r.BillingCountry,Account__r.Cash_outlay_fee_base__c,
                             Account__r.Finance_Charge__c, Account__r.New_Invoicing_Structure__c, Account__r.Tax_Recovery_Switch_On__c,
                             Account__r.Tax_recovery_client__c,Account__r.Prepayment_Term_Days__c,
                             (SELECT Id, Name, Account__c, Shipment_Order__c, Shipment_Order__r.Name, 
                             Freight_Request__r.Name, Conversion_Rate__c, PO_Number_Override__c, PO_Number__c, 
                              Invoice_Sent_Date__c, Amount_Outstanding_Local_Currency__c, Invoice_Name__c, 
                              Prepayment_date__c, Due_Date__c, IOR_Fees__c, EOR_Fees__c, Admin_Fees__c,   Actual_IOREOR_Costs__c, 
                              International_Freight_Fee__c, Liability_Cover_Fee__c, Total_Amount_Incl_Potential_Cash_Outlay__c, 
                              Cost_of_Sale_Shipping_Insurance__c, Taxes_and_Duties__c, Recharge_Tax_and_Duty_Other__c, 
                              Customs_Brokerage_Fees__c, Customs_Clearance_Fees__c, Customs_License_In__c, Customs_Handling_Fees__c, 
                              Bank_Fee__c, Miscellaneous_Fee__c, Miscellaneous_Fee_Name__c, Invoice_amount_USD__c, 
                              Collection_Administration_Fee__c, Amount_Outstanding__c, Invoice_Type__c, Invoice_Currency__c, 
                              Possible_Cash_Outlay__c, Cash_Outlay_Fee__c, Invoice_Amount_Local_Currency__c, IOR_EOR_Compliance__c, 
                              Government_and_In_country_Summary__c, Freight_and_Related_Costs__c, Billing_City__c, Billing_Country__c, 
                              Billing_Postal_Code__c, Billing_Street__c, Billing_State__c FROM Invoices__r)  
                             FROM Enterprise_Billing__c where Id = :ApexPages.currentPage().getParameters().get('id') LIMIT 1];
        this.invoiceList = new List<Invoice_New__c>();
        this.invoiceList = this.ebillObject.Invoices__r;
        this.combinedIORANDCompFee = 0;
    this.combinedEORANDCompFee = 0;
    this.combinedAdminFee = 0;
    this.combinedSubTotalIOREOR = 0;
    this.combinedintFreightFee = 0;
    this.combinedLiabilityFee = 0;
    this.combinedSubTotalFreight = 0;
    this.combinedRechargeTaxAndDuty = 0 ;
    this.combinedRechargeTaxAndDutyOther = 0 ;
    this.combinedCustomsBrokerageCost = 0 ;
    this.combinedClearanceCosts = 0;
    this.combinedLicenseCosts = 0 ;
    this.combinedHandlingCosts = 0;
    this.combinedBankFee= 0 ;
  	this.combinedMiscellaneousFee = 0 ;
    this.combinedCashDisbursementFeePossibleCashOutlay = 0 ;
    this.combinedCashDisbursementFee = 0 ;
    this.combinedSubTotalGovernmentandIncountrySummary = 0 ;
    this.combinedTotalAmountInclPotentialCashOutlay = 0 ;
        for(Invoice_New__c invoice:this.ebillObject.Invoices__r){
    this.combinedIORANDCompFee += invoice.IOR_Fees__c;
    this.combinedEORANDCompFee += invoice.EOR_Fees__c;
    this.combinedAdminFee += invoice.Admin_Fees__c;
    this.combinedSubTotalIOREOR += invoice.IOR_EOR_Compliance__c;
    this.combinedintFreightFee += invoice.International_Freight_Fee__c;
    this.combinedLiabilityFee += invoice.Liability_Cover_Fee__c;
    this.combinedSubTotalFreight += invoice.Freight_and_Related_Costs__c ;
    this.combinedRechargeTaxAndDuty += invoice.Taxes_and_Duties__c ;
    this.combinedRechargeTaxAndDutyOther += invoice.Recharge_Tax_and_Duty_Other__c ;
    this.combinedCustomsBrokerageCost += invoice.Customs_Brokerage_Fees__c ;
    this.combinedClearanceCosts += invoice.Customs_Clearance_Fees__c ;
    this.combinedLicenseCosts += invoice.Customs_License_In__c ;
    this.combinedHandlingCosts += invoice.Customs_Handling_Fees__c ;
    this.combinedBankFee += invoice.Bank_Fee__c ;
  	this.combinedMiscellaneousFee += invoice.Miscellaneous_Fee__c ;
    this.combinedCashDisbursementFeePossibleCashOutlay += invoice.Possible_Cash_Outlay__c ;
    this.combinedCashDisbursementFee += invoice.Cash_Outlay_Fee__c ;
    this.combinedSubTotalGovernmentandIncountrySummary += invoice.Government_and_In_country_Summary__c ;
    this.combinedTotalAmountInclPotentialCashOutlay += invoice.Total_Amount_Incl_Potential_Cash_Outlay__c ;
        }
        /*this.customerInvoice = [SELECT Id, Name, Account__c, Shipment_Order__c, Shipment_Order__r.Name, Freight_Request__r.Name, Conversion_Rate__c, PO_Number_Override__c, PO_Number__c, Invoice_Sent_Date__c, Amount_Outstanding_Local_Currency__c, Invoice_Name__c, Prepayment_date__c, Due_Date__c, IOR_Fees__c, EOR_Fees__c, Admin_Fees__c,   Actual_IOREOR_Costs__c, International_Freight_Fee__c, Liability_Cover_Fee__c, Total_Amount_Incl_Potential_Cash_Outlay__c, Cost_of_Sale_Shipping_Insurance__c, Taxes_and_Duties__c, Recharge_Tax_and_Duty_Other__c, Customs_Brokerage_Fees__c, Customs_Clearance_Fees__c, Customs_License_In__c, Customs_Handling_Fees__c, Bank_Fee__c, Miscellaneous_Fee__c, Miscellaneous_Fee_Name__c, Invoice_amount_USD__c, Collection_Administration_Fee__c, Amount_Outstanding__c, Invoice_Type__c, Invoice_Currency__c, Possible_Cash_Outlay__c, Cash_Outlay_Fee__c, Invoice_Amount_Local_Currency__c, IOR_EOR_Compliance__c, Government_and_In_country_Summary__c, Freight_and_Related_Costs__c, Billing_City__c, Billing_Country__c, Billing_Postal_Code__c, Billing_Street__c, Billing_State__c
                                FROM Invoice_New__c
                                WHERE Id = :ApexPages.currentPage().getParameters().get('id') LIMIT 1];
        if(customerInvoice.Shipment_Order__c !=null){
        this.shipmentOrder =  [ SELECT Id, Name, Client_Reference__c, Client_Reference_2__c, IOR_Price_List__r.Name, Ship_From_Country__c, Shipment_Value_USD__c, Ship_to_Country_new__c,
                                CreatedDate, New_Structure__c, Tax_recovery_Premium_Fee__c  
                                FROM Shipment_Order__c 
                                WHERE Id = :customerInvoice.Shipment_Order__c];
        }
        
        this.account = [SELECT Id, Name,  IOR_Payment_Terms__c, VAT_Number__c, BillingStreet, BillingCity, BillingState, BillingPostalCode, BillingCountry,
                        Cash_outlay_fee_base__c, Finance_Charge__c, New_Invoicing_Structure__c, Tax_Recovery_Switch_On__c, Tax_recovery_client__c 
                        FROM Account 
                        WHERE Id = :customerInvoice.Account__c];*/
    
    }

}