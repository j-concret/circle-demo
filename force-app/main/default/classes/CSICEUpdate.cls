@RestResource(urlMapping='/CEupdate/*')
Global class CSICEUpdate {


    @HttpPOST
    global static void CSICEUpdate(){

        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        res.addHeader('Content-Type', 'application/json');
        Blob body = req.requestBody;
        String requestString = body.toString();
        CSICEUpdateWra rw = (CSICEUpdateWra)JSON.deserialize(requestString,CSICEUpdateWra.class);

        try {

            list<Access_token__c> At =[select status__c,Access_Token__c,AccessToken_encoded__c from Access_token__c where Access_Token__c=:rw.Accesstoken and status__C ='Active' ];
            if( !at.isempty()) {
                Decimal FinalDutyAmount = 0.00;

                If(rw.Parts.isEmpty()){

                    Shipment_Order__c SO = [Select Id,Final_Deliveries_New__c,Number_of_Final_Deliveries_Auto__c,Type_of_Goods__c,Lithium_Battery_Types__c,Li_ion_Batteries__c,Client_PO_ReferenceNumber__c,Chargeable_Weight__c,Service_Type__c,Who_arranges_International_courier__c,Client_Reference__c,Client_Reference_2__c,Shipment_Value_USD__c from Shipment_order__c where Id=: rw.SOID and shipping_status__c='Cost Estimate' ];

                    So.Number_of_Final_Deliveries_Client__c = rw.NumberOfFinaldeliveries;

                    So.Final_Deliveries_New__c = So.Number_of_Final_Deliveries_Client__c > So.Number_of_Final_Deliveries_Auto__c ? So.Number_of_Final_Deliveries_Client__c : So.Number_of_Final_Deliveries_Client__c;

                    if(So.Type_of_Goods__c != rw.Type_of_Goods){
                        So.Type_of_Goods__c=rw.Type_of_Goods;
                    }
                    So.Lithium_Battery_Types__c=rw.Li_ion_BatteryTypes;
                    So.Li_ion_Batteries__c=rw.Li_ion_Batteries;
                    So.Client_PO_ReferenceNumber__c=rw.PONumber;
                    So.Chargeable_Weight__c=rw.estimatedChargableweight;
                    So.Service_Type__c=rw.ServiceType;
                    So.Who_arranges_International_courier__c=rw.Courier_responsibility;
                    So.Client_Reference__c=rw.Reference1;
                    So.Client_Reference_2__c=rw.Reference2;
                    So.Shipment_Value_USD__c=rw.ShipmentvalueinUSD;



                    Update So;
                }



                If(!rw.Parts.isEmpty()){

                    List<Part__c> Prtlist = New List<Part__C>();
                    List<Part__c> PAL = New List<Part__C>();
                     List<Id> calculateTaxesList = new List<Id>();

                    shipment_Order__C SO = [Select Id,recalculate_taxes__c,Final_Deliveries_New__c,Type_of_Goods__c,Lithium_Battery_Types__c,Li_ion_Batteries__c,Client_PO_ReferenceNumber__c,Chargeable_Weight__c,Service_Type__c,Who_arranges_International_courier__c,Client_Reference__c,Client_Reference_2__c,Shipment_Value_USD__c from Shipment_order__C where Id=: rw.SOID  ];//and shipping_status__c='Cost Estimate'
                    calculateTaxesList.add(So.Id);
                    Prtlist =[Select Id from Part__C where Shipment_order__C =: rw.SOID];
                    if(!Prtlist.isEmpty()) {
                        System.debug('Inside part delete');
                        RecusrsionHandler.skipTriggerExecution = true;
                        Delete Prtlist;
                    }

                    for(Integer l=0; rw.parts.size()>l; l++) {
                        Part__c PA = new Part__c(
                            Name =rw.parts[l].PartNumber,
                            Description_and_Functionality__c =rw.parts[l].PartDescription,
                            Quantity__c =rw.parts[l].Quantity,
                            Commercial_Value__c =rw.parts[l].UnitPrice,
                            ClientReferencenumber__c=rw.parts[l].ClientpartReferencenumber,
                            Shipment_Order__c=rw.SOID
                            );
                        PAL.add(PA);
                    }

                   
                     RecusrsionHandler.runTaskIsQueued = true;
                     RecusrsionHandler.skipTriggerExecution = false;
                     insert PAL;
                     
                }
                 shipment_Order__C SOs = [Select Id,Name,Shipping_status__c,Tax_Cost__c, Ship_From_Country__c,Service_Type__c,Final_Deliveries_New__c,Shipment_Value_USD__c,Type_of_Goods__c,Li_ion_Batteries__c,Lithium_Battery_Types__c,of_packages__c,Chargeable_Weight__c,TecEx_Invoice_Number__c,
                                         IOR_and_Import_Compliance_Fee_USD_numb__c,EOR_and_Export_Compliance_Fee_USD_numb__c,Account__C,Client_Contact_for_this_Shipment__c,of_Line_Items__c,
                                         Set_up_Fee__c,Handling_and_Admin_Fee__c,Bank_Fees__c,Reason_For_Pro_Forma_Quote__C,
                                         Recharge_License_Cost__c,Total_Customs_Brokerage_Cost__c,Recharge_Handling_Costs__c,Total_clearance_Costs__c,
                                         Recharge_Tax_and_Duty__c,Recharge_Tax_and_Duty_Other__c, International_Delivery_Fee__c,Insurance_Fee_USD__c,Finance_Fee__c,
                                         Miscellaneous_Fee__c,Miscellaneous_Fee_Name__c,Collection_Administration_Fee__c,Total_Invoice_Amount_Excl_Taxes_Duties__c,Total_including_estimated_duties_and_tax__c,
                                         Total_Invoice_Amount__c,Forex_Rate_Used_Euro_Dollar__c,Quote_type__c,Total_Cost_Range_Min__c,Total_Cost_Range_Max__c,Estimate_Pre_Approval_Time_Formula__c,createddate,Expiry_Date__c,Shipping_Notes_New__c from Shipment_order__C where Id=:rw.SOID  ];


                List<AggregateResult> allSoDuties = [SELECT Sum(Value__c) DutyAmount
                                                     FROM Tax_Calculation__c
                                                     Where Shipment_Order__c =: SOs.id
                                                                               And Tax_Type__c = 'Duties' ];
                for(AggregateResult a : allSoDuties) {  FinalDutyAmount = (Decimal) a.get('DutyAmount'); }
                List<Part__c> Parts = [Select Id,Name,Quantity__c,US_HTS_Code__c,Line_Item_Duty__c,Part_Specific_Tax_1__c,Part_Specific_Tax_2__c,Part_Specific_Tax_3__c,ClientReferencenumber__c from Part__C where Shipment_order__c =: Sos.id  ];



                if(SOs.id !=null && SOS.Shipping_status__c != 'Cost Estimate Abandoned') {

                    JSONGenerator gen = JSON.createGenerator(true);
                    gen.writeStartObject();
                    //Creating Array & Object - Shipment Order
                    gen.writeFieldName('ShipmentOrder');
                    gen.writeStartObject();
                    if(SOs.Id == null) {gen.writeNullField('Id');} else{gen.writeStringField('Id', SOs.Id);}
                    if(FinalDutyAmount==0.00 ||FinalDutyAmount== null ||FinalDutyAmount <= 0) {gen.writeNullField('FinalDutyAmount');} else{gen.writeNumberField('FinalDutyAmount',FinalDutyAmount);}
                    if(SOs.Name== null) {gen.writeNullField('Name');} else{gen.writeStringField('Name',Sos.Name);}
                    if(SOs.Shipping_status__c== null) {gen.writeNullField('Shippingstatus');} else{gen.writeStringField('Shippingstatus',Sos.Shipping_status__c);}
                    if(SOs.Ship_From_Country__c== null) {gen.writeNullField('ShipFromCountry');} else{gen.writeStringField('ShipFromCountry',Sos.Ship_From_Country__c);}
                    if(SOs.Service_Type__c== null) {gen.writeNullField('ServiceType');} else{gen.writeStringField('ServiceType',Sos.Service_Type__c);}
                    if(SOs.Final_Deliveries_New__c== null) {gen.writeNullField('FinalDeliveriesNew');} else{gen.writeNumberField('FinalDeliveriesNew',Sos.Final_Deliveries_New__c);}
                    if(SOs.Shipment_Value_USD__c== null) {gen.writeNullField('ShipmentValueUSD');} else{gen.writeNumberField('ShipmentValueUSD',Sos.Shipment_Value_USD__c);}
                    if(SOs.Type_of_Goods__c== null) {gen.writeNullField('TypeofGoods');} else{gen.writeStringField('TypeofGoods',Sos.Type_of_Goods__c);}
                    if(SOs.Li_ion_Batteries__c== null) {gen.writeNullField('LiionBatteries');} else{gen.writeStringField('LiionBatteries',Sos.Li_ion_Batteries__c);}
                    if(SOs.Lithium_Battery_Types__c== null) {gen.writeNullField('LithiumBatteryTypes');} else{gen.writeStringField('LithiumBatteryTypes',Sos.Lithium_Battery_Types__c);}
                    if(SOs.of_packages__c== null) {gen.writeNullField('ofpackages');} else{gen.writeNumberField('ofpackages',Sos.of_packages__c);}
                    if(SOs.Chargeable_Weight__c== null) {gen.writeNullField('ChargeableWeight');} else{gen.writeNumberField('ChargeableWeight',Sos.Chargeable_Weight__c);}
                    if(SOs.TecEx_Invoice_Number__c== null) {gen.writeNullField('TecExInvoiceNumber');} else{gen.writeStringField('TecExInvoiceNumber',Sos.TecEx_Invoice_Number__c);}
                    if(SOs.IOR_and_Import_Compliance_Fee_USD_numb__c== null) {gen.writeNullField('IORandImportComplianceFeeUSDnumb');} else{gen.writeNumberField('IORandImportComplianceFeeUSDnumb',Sos.IOR_and_Import_Compliance_Fee_USD_numb__c);}
                    if(SOs.EOR_and_Export_Compliance_Fee_USD_numb__c== null) {gen.writeNullField('EORandExportComplianceFeeUSDnumb');} else{gen.writeNumberField('EORandExportComplianceFeeUSDnumb',Sos.EOR_and_Export_Compliance_Fee_USD_numb__c);}
                    if(SOs.Account__C== null) {gen.writeNullField('Account');} else{gen.writeStringField('Account',Sos.Account__C);}
                    if(SOs.Client_Contact_for_this_Shipment__c== null) {gen.writeNullField('ClientContactforthisShipment');} else{gen.writeStringField('ClientContactforthisShipment',Sos.Client_Contact_for_this_Shipment__c);}
                    if(SOs.Set_up_Fee__c== null) {gen.writeNullField('SetupFee');} else{gen.writeNumberField('SetupFee',Sos.Set_up_Fee__c);}
                    if(SOs.Handling_and_Admin_Fee__c== null) {gen.writeNullField('HandlingandAdminFee');} else{gen.writeNumberField('HandlingandAdminFee',Sos.Handling_and_Admin_Fee__c);}
                    if(SOs.Bank_Fees__c== null) {gen.writeNullField('BankFees');} else{gen.writeNumberField('BankFees',Sos.Bank_Fees__c);}
                    if(SOs.Recharge_License_Cost__c == null) {gen.writeNullField('RechargeLicenseCost ');} else{gen.writeNumberField('RechargeLicenseCost ',Sos.Recharge_License_Cost__c );}
                    if(SOs.Total_Customs_Brokerage_Cost__c== null) {gen.writeNullField('TotalCustomsBrokerageCost');} else{gen.writeNumberField('TotalCustomsBrokerageCost',Sos.Total_Customs_Brokerage_Cost__c);}
                    if(SOs.Recharge_Handling_Costs__c== null) {gen.writeNullField('RechargeHandlingCosts');} else{gen.writeNumberField('RechargeHandlingCosts',Sos.Recharge_Handling_Costs__c);}
                    if(SOs.Total_clearance_Costs__c== null) {gen.writeNullField('TotalclearanceCosts');} else{gen.writeNumberField('TotalclearanceCosts',Sos.Total_clearance_Costs__c);}
                    if(SOs.Recharge_Tax_and_Duty__c== null) {gen.writeNullField('RechargeTaxandDuty');} else{gen.writeNumberField('RechargeTaxandDuty',Sos.Recharge_Tax_and_Duty__c);}
                    if(SOs.Recharge_Tax_and_Duty_Other__c== null) {gen.writeNullField('RechargeTaxandDutyOther');} else{gen.writeNumberField('RechargeTaxandDutyOther',Sos.Recharge_Tax_and_Duty_Other__c);}
                    if(SOs.International_Delivery_Fee__c== null) {gen.writeNullField('InternationalDeliveryFee');} else{gen.writeNumberField('InternationalDeliveryFee',Sos.International_Delivery_Fee__c);}
                    if(SOs.Insurance_Fee_USD__c== null) {gen.writeNullField('InsuranceFeeUSD');} else{gen.writeNumberField('InsuranceFeeUSD',Sos.Insurance_Fee_USD__c);}
                    if(SOs.Finance_Fee__c== null) {gen.writeNullField('FinanceFee');} else{gen.writeNumberField('FinanceFee',Sos.Finance_Fee__c);}
                    if(SOs.Miscellaneous_Fee__c== null) {gen.writeNullField('MiscellaneousFee');} else{gen.writeNumberField('MiscellaneousFee',Sos.Miscellaneous_Fee__c);}
                    if(SOs.Miscellaneous_Fee_Name__c== null) {gen.writeNullField('MiscellaneousFeeName');} else{gen.writeStringField('MiscellaneousFeeName',Sos.Miscellaneous_Fee_Name__c);}
                    if(SOs.Collection_Administration_Fee__c== null) {gen.writeNullField('CollectionAdministrationFee');} else{gen.writeNumberField('CollectionAdministrationFee',Sos.Collection_Administration_Fee__c);}
                    if(SOs.Total_Invoice_Amount_Excl_Taxes_Duties__c== null) {gen.writeNullField('TotalInvoiceAmountExclTaxesDuties');} else{gen.writeNumberField('TotalInvoiceAmountExclTaxesDuties',Sos.Total_Invoice_Amount_Excl_Taxes_Duties__c);}
                    if(SOs.Total_including_estimated_duties_and_tax__c== null) {gen.writeNullField('Totalincludingestimateddutiesandtax');} else{gen.writeNumberField('Totalincludingestimateddutiesandtax',Sos.Total_including_estimated_duties_and_tax__c);}
                    if(SOs.Total_Invoice_Amount__c== null) {gen.writeNullField('TotalInvoiceAmount');} else{gen.writeNumberField('TotalInvoiceAmount',Sos.Total_Invoice_Amount__c);}
                    if(SOs.Forex_Rate_Used_Euro_Dollar__c== null) {gen.writeNullField('ForexRateUsedEuroDollar');} else{gen.writeNumberField('ForexRateUsedEuroDollar',Sos.Forex_Rate_Used_Euro_Dollar__c);}
                    if(SOs.Quote_type__c== null) {gen.writeNullField('Quotetype');} else{gen.writeStringField('Quotetype',Sos.Quote_type__c);}
                    if(SOs.Total_Cost_Range_Min__c== null) {gen.writeNullField('TotalCostRangeMin');} else{gen.writeNumberField('TotalCostRangeMin',Sos.Total_Cost_Range_Min__c);}
                    if(SOs.Total_Cost_Range_Max__c== null) {gen.writeNullField('TotalCostRangeMax');} else{gen.writeNumberField('TotalCostRangeMax',Sos.Total_Cost_Range_Max__c);}
                    if(SOs.Estimate_Pre_Approval_Time_Formula__c== null) {gen.writeNullField('EstimatePreApprovalTimeFormula');} else{gen.writeStringField('EstimatePreApprovalTimeFormula',Sos.Estimate_Pre_Approval_Time_Formula__c);}
                    if(SOs.createddate== null) {gen.writeNullField('createddate');} else{gen.writedatetimeField('createddate',Sos.createddate);}
                    if(SOs.Expiry_Date__c== null) {gen.writeNullField('ExpiryDate');} else{gen.writedateField('ExpiryDate',Sos.Expiry_Date__c);}
                    if(SOs.Shipping_Notes_New__c== null) {gen.writeNullField('ShippingNotesNew');} else{gen.writeStringField('ShippingNotesNew',Sos.Shipping_Notes_New__c);}
                    if(SOs.Reason_For_Pro_Forma_Quote__C== null) {gen.writeNullField('ReasonForProFormaQuote');} else{gen.writeStringField('ReasonForProFormaQuote',Sos.Reason_For_Pro_Forma_Quote__C);}
					if(SOs.Tax_Cost__c == null) {gen.writeNullField('TaxCost');} else{gen.writeNumberField('TaxCost',Sos.Tax_Cost__c );}

                    gen.writeEndObject();
                    //End of Array & Object - Shipment Order

                    If(!Parts.isEmpty()){
                        //Creating Array & Object - Parts
                        gen.writeFieldName('Parts');
                        gen.writeStartArray();
                        for(Part__c Parts1 :Parts) {
                            //Start of Object - Parts
                            gen.writeStartObject();

                            if(Parts1.Id == null) {gen.writeNullField('Id');} else{gen.writeStringField('Id', Parts1.Id);}
                            if(Parts1.Name == null) {gen.writeNullField('PartNumber');} else{gen.writeStringField('PartNumber', Parts1.Name);}
                            if(Parts1.Line_Item_Duty__c == null) {gen.writeNullField('LineItemDuty');} else{gen.writenumberField('LineItemDuty', Parts1.Line_Item_Duty__c);}
                            if(Parts1.Part_Specific_Tax_1__c == null) {gen.writeNullField('PartSpecificTax1');} else{gen.writenumberField('PartSpecificTax1', Parts1.Part_Specific_Tax_1__c);}
                            if(Parts1.Part_Specific_Tax_2__c == null) {gen.writeNullField('PartSpecificTax2');} else{gen.writenumberField('PartSpecificTax2', Parts1.Part_Specific_Tax_2__c);}
                            if(Parts1.Part_Specific_Tax_3__c == null) {gen.writeNullField('PartSpecificTax3');} else{gen.writenumberField('PartSpecificTax3', Parts1.Part_Specific_Tax_3__c);}
                            if(Parts1.Quantity__c == null) {gen.writeNullField('Quantity');} else{gen.writeNumberField('Quantity', Parts1.Quantity__c);}
                            if(Parts1.US_HTS_Code__c == null) {gen.writeNullField('USHTSCode');} else{gen.writeStringField('USHTSCode', Parts1.US_HTS_Code__c);}
                            if(Parts1.US_HTS_Code__c == null) {gen.writeBooleanField('Mapped',false);} else{gen.writeBooleanField('Mapped', true);}
                            if(Parts1.ClientReferencenumber__c == null) {gen.writeNullField('ClientReferencenumber');} else{gen.writeStringField('ClientReferencenumber',  Parts1.ClientReferencenumber__c);}

                            gen.writeEndObject();
                            //End of Object - lineitem
                        }
                        gen.writeEndArray();
                        //End of Array - lineitem
                    }


                    gen.writeEndObject();
                    string jsonData = gen.getAsString();

                    res.responseBody = Blob.valueOf(jsonData);
                    res.statusCode = 200;

                    API_Log__c Al = New API_Log__c();
                    Al.EndpointURLName__c='CSICEUpdate';
                    Al.Response__c=sos.Account__C+','+rw.SOID+'Success - Quote updated and details are sent';
                    Al.StatusCode__c=string.valueof(res.statusCode);
                    Insert Al;
                  }

                 else if( SOS.Shipping_status__c == 'Cost Estimate Abandoned'){

                JSONGenerator gen = JSON.createGenerator(true);
                gen.writeStartObject();

                gen.writeFieldName('Error');
                gen.writeStartObject();

                gen.writeStringField('Response', 'Quote details are not available on costestimate abandoned stage');

                gen.writeEndObject();
                gen.writeEndObject();
                String jsonData = gen.getAsString();
                res.responseBody = Blob.valueOf(jsonData);
                res.addHeader('Content-Type', 'application/json');
                res.statusCode = 200;

                   API_Log__c Al = New API_Log__c();
                    Al.EndpointURLName__c='CSICEUpdate';
                    Al.Response__c=sos.Account__C+','+sos.Client_Contact_for_this_Shipment__c+','+rw.SOID+','+'Quote details are not available on costestimate abandoned stage';
                    Al.StatusCode__c=string.valueof(res.statusCode);
                    Insert Al;
				
                }
                else{

                    String ErrorString ='calculations are still in progress, please try after 5-10 sec';
                    res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
                    res.statusCode = 400;

                    API_Log__c Al = New API_Log__c();
                 	//   Al.Account__c=sos.Account__C;
                  	//  Al.Login_Contact__c=sos.Client_Contact_for_this_Shipment__c;
                    Al.EndpointURLName__c='CSICEUpdate';
                    Al.Response__c=rw.SOID+ErrorString;
                    Al.StatusCode__c=string.valueof(res.statusCode);
                    Insert Al;

                }



            }
            else{

                JSONGenerator gen = JSON.createGenerator(true);
                gen.writeStartObject();

                gen.writeFieldName('Error');
                gen.writeStartObject();

                gen.writeStringField('Response', 'Access Token Expired or Invlid');

                gen.writeEndObject();
                gen.writeEndObject();
                String jsonData = gen.getAsString();
                res.responseBody = Blob.valueOf(jsonData);
                res.addHeader('Content-Type', 'application/json');
                res.statusCode = 400;

                API_Log__c Al = New API_Log__c();
                // Al.Account__c=sos[0].Account__C;
                // Al.Login_Contact__c=sos[0].Client_Contact_for_this_Shipment__c ;
                Al.EndpointURLName__c='CSICEUpdate';
                Al.Response__c=rw.SOID+'Access Token Expired or Invlid';
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al;
            }
        }
        Catch(Exception e){

            //  String ErrorString =e.getLineNumber() +'---' +e.getMessage() ;
            String ErrorBody='Please create case with Tecex with full details - support_SF@tecex.com  or Call CEDetails API '+e.getMessage();
            res.responseBody =  Blob.valueOf(JSON.serializePretty(ErrorBody));
            //res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
            res.statusCode = 500;
            API_Log__c Al = New API_Log__c();
            Al.EndpointURLName__c='CSICEUpdate';
            Al.Response__c=rw.SOID+e.getMessage()+e.getLineNumber();
            Al.StatusCode__c=string.valueof(res.statusCode);
            Insert Al;

        }

    }



}