@Istest
public class NCPRolloutEXcelExport_Test {

    static testMethod void testNCPRolloutEXcelExport(){
        Access_token__c accessTokenObj = new Access_token__c();
        accessTokenObj.Status__c = 'Active';
        accessTokenObj.Access_Token__c = 'asdfghj-sdfgfds-sfdfgf';
        
        Insert accessTokenObj;
        
        Account acc = new Account(Name = 'Acme Test');
        
        Insert acc;
        
        NCPRolloutEXcelExportwra wrapper = new NCPRolloutEXcelExportwra();
        wrapper.Accesstoken = accessTokenObj.Access_Token__c;
        wrapper.RolloutID = acc.Id;
        
        RestRequest req1 = new RestRequest();
        RestResponse res1 = new RestResponse();
        req1.requestURI = '/services/apexrest/RolloutExcelexport'; 
        req1.httpMethod = 'POST';
        req1.requestBody = Blob.valueOf(JSON.serialize(wrapper));     
        RestContext.request = req1;
        RestContext.response = res1;
        RestContext.request.addHeader('Content-Type', 'application/json');
        
        Test.startTest(); 
        NCPRolloutEXcelExport.NCPRolloutEXcelExport();
        Test.stopTest();
    }
    
    static testMethod void testNCPRolloutEXcelExportElse(){
        Access_token__c accessTokenObj = new Access_token__c();
        accessTokenObj.Status__c = 'Expired';
        accessTokenObj.Access_Token__c = 'asdfghj-sdfgfds-sfdfgf';
        
        Insert accessTokenObj;
        
        Account acc = new Account(Name = 'Acme Test');
        
        Insert acc;
        
        NCPRolloutEXcelExportwra wrapper = new NCPRolloutEXcelExportwra();
        wrapper.Accesstoken = accessTokenObj.Access_Token__c;
        wrapper.RolloutID = acc.Id;
        
        RestRequest req1 = new RestRequest();
        RestResponse res1 = new RestResponse();
        req1.requestURI = '/services/apexrest/RolloutExcelexport'; 
        req1.httpMethod = 'POST';
        req1.requestBody = Blob.valueOf(JSON.serialize(wrapper));     
        RestContext.request = req1;
        RestContext.response = res1;
        RestContext.request.addHeader('Content-Type', 'application/json');
        
        Test.startTest(); 
        NCPRolloutEXcelExport.NCPRolloutEXcelExport();
        Test.stopTest();
    }
}