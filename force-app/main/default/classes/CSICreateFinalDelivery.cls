@RestResource(urlMapping='/CreateFinalDeliveryAdd/*')
Global class CSICreateFinalDelivery {
    
    @Httppost
    global static void CreateFinalDelivery(){
    
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        Blob body = req.requestBody;
    	res.addHeader('Content-Type', 'application/json');
        String requestString = body.toString();
        NCPCreateFinalDeliveryWraV2 rw = (NCPCreateFinalDeliveryWraV2)JSON.deserialize(requestString,NCPCreateFinalDeliveryWraV2.class);
        try{
       
           Access_token__c At =[select status__c,Access_Token__c from Access_token__c where Access_Token__c=:rw.Final_Delivery[0].Accesstoken ];
            	if( at.status__C =='Active'){
              
              list<Final_Delivery__c> FDL = new list<Final_Delivery__c>();
           		
              for(Integer i=0; rw.Final_Delivery.size()>i;i++) {
              
          Final_Delivery__c FD = new Final_Delivery__c(
            
             
              Shipment_Order__c = Rw.Final_Delivery[i].SOID,
              name= rw.Final_Delivery[i].Name,
              Ship_to_address__C  = rw.Final_Delivery[i].FinalDestinationAddressID
              
                );
            
           
              		FDL.add(FD); 
                   
             }
              Insert FDL;
                 
            JSONGenerator gen = JSON.createGenerator(true);
            gen.writeStartObject();
            
             gen.writeFieldName('Success');
             gen.writeStartObject();
            
             	if(rw.Final_Delivery[0].Name == null) {gen.writeNullField('Response');} else{gen.writeStringField('Response', 'Success-Final Delivery Created');}
            
            gen.writeEndObject();
            gen.writeEndObject();
            String jsonData = gen.getAsString();
            res.responseBody = Blob.valueOf(jsonData);
            res.addHeader('Content-Type', 'application/json');
            res.statusCode = 200;        
              	
                    
                API_Log__c Al = New API_Log__c();
               // Al.Account__c=FD.Shipment_Order__r.Account__c;
                Al.EndpointURLName__c='CSI-CreateFinalDelivery';
                Al.Response__c='Success- FinalDelivery Created';
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al; 
            
        }
         
        }
        	catch (Exception e){
                
              	String ErrorString ='Something went wrong while creating Final deliveries, please contact support_SF@tecex.com';
               	res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
                res.addHeader('Content-Type', 'application/json');
            	res.statusCode = 500;
                API_Log__c Al = New API_Log__c();
               // Al.Account__c=FD.Shipment_Order__r.Account__c;
                Al.EndpointURLName__c='CSI-CreateFinalDelivery';
                Al.Response__c=e.getMessage()+' at line number: '+e.getLineNumber();
                Al.StatusCode__c=string.valueof(res.statusCode);
              	Insert Al;

            
        	}

        
        
    }
    

}