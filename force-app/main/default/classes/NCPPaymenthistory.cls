@RestResource(urlMapping='/NCPPaymentHistory/*')
global class NCPPaymenthistory {
    
    @Httppost
    global static void NCPPaymenthistory(){
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        res.addHeader('Content-Type', 'application/json');
        Blob body = req.requestBody;
        String requestString = body.toString();
        RequestWrapper reqBody;
        {
             
            try{
                reqBody = (RequestWrapper)JSON.deserialize(requestString,RequestWrapper.class);
            	Access_token__c at = [SELECT status__c,Access_Token__c FROM Access_token__c WHERE Access_Token__c =: reqBody.Accesstoken];
            	if( at.status__c == 'Active') {
                    List<Bank_transactions__c> BT = [SELECT RecordType.Name,BTname__c,Accounting_Period_Formula__c,Accounting_Period_New__c,Amount_USD__c,Amount__c,Applied_amount__c,Bank_Account_New__c,Bank_charges__c,BM_email__c,BT_Comments__c,BT_Status__c,Cheque_Payment__c,Conversion_Rate__c,CreatedById,CreatedDate,Currency__c,Date__c,Finance_Team__c,Forex_Rate__c,Forex_total__c,Id,IsDeleted,LastActivityDate,LastModifiedById,LastModifiedDate,LastReferencedDate,LastViewedDate,Month_End_Journal_Entries__c,Name,New_Month_Reversal_Journal_Entries__c,OwnerId,Penalty_Applied_Amount__c,Posted_Forex__c,Post_Forex__c,Rate_adjustment__c,RecordTypeId,R_Type__c,Status__c,Supplier__c,SystemModstamp,Transaction_Lines_Created__c,Transaction_Line_Balance__c,Transaction_Total_Neg__c,Transaction_Total__c,Type__c,Unapplied__c,USD_payment__c,WT_Number__c  FROM Bank_transactions__c where Supplier__c =: reqBody.AccountID ];
                    List<Bank_Transaction_Applied__c> BTA = [SELECT  Invoice__r.Shipment_Order__c,Invoice__r.Shipment_Order__r.name,Invoice__r.Name,RecordType.Name,Accounting_Period_Formula__c,Accounting_Period_New__c,Account_Name__c,Account__c,Adjustment_Amount__c,Amount__c,APD_created__c,Auto_Reversal_Date__c,Auto_Reversed_User__c,Auto_Reverse_BTA__c,Bank_Transaction__c,BT_Date__c,CreatedById,CreatedDate,Customer_Invoice_Value__c,Customer_Invoice__c,Customer_Shipment_Order__c,Date__c,Forex_Formula__c,Id,Invoice__c,IsDeleted,LastActivityDate,LastModifiedById,LastModifiedDate,LastReferencedDate,LastViewedDate,Name,Payable_Posted__c,Payable_Status_New__c,Payable_Status__c,POD_Date__c,Posted_Forex__c,Post_Forex__c,RecordTypeId,Shipment_Order__c,Ship_to_Country__c,Supplier_Invoice_Value__c,Supplier_Invoice__c,Supplier_Shipment_Order_del__c,SystemModstamp,Total_Applied_amount_with_Forex__c,Total_Applied_amount__c,Total_Value_Applied_To_Penalty__c,Transaction_Lines_Created__c,Transaction_Line_Balance__c,Transaction_Total_Neg__c,Transaction_Total__c FROM Bank_Transaction_Applied__c where Bank_Transaction__c in :BT and Invoice__r.Paid_Date__c > 2020-01-01 ];
				    JSONGenerator gen = JSON.createGenerator(true);
             		gen.writeStartObject();
                     					 //Creating Array & Object - ClientId
                                          gen.writeFieldName('ClinetID');
                                          gen.writeStartObject();
                                          if(reqBody.AccountID == null) {gen.writeNullField('Id');} else{gen.writeStringField('Id', reqBody.AccountID);}
                                          gen.writeEndObject();
                                          //End of Array & Object - ClientId
                                          
                    			If(!BT.isEmpty()){
                                    gen.writeFieldName('BankTransactions');
                                    gen.writeStartArray();
                                    
                                    for(Bank_transactions__c BT1 :BT){
                                        
                                         System.debug(BT1.BTname__c);
                                         gen.writeStartObject();
                                        
                                     					if(BT1.Id == null) {gen.writeNullField('Id');} else{gen.writeStringField('Id', BT1.Id);}
                                                    	if(BT1.name == null) {gen.writeNullField('Name');} else{gen.writeStringField('Name', BT1.Name);}
                                        				if(BT1.BTname__c == null) {gen.writeNullField('BTname');} else{gen.writeStringField('BTname', BT1.BTname__c);}
                                        				if(BT1.Date__c == null) {gen.writeNullField('Date');} else{gen.writeDateField('Date', BT1.Date__c);}
                                        				if(BT1.Amount_USD__c == null) {gen.writeNullField('Amount_USD');} else{gen.writeNumberField('Amount_USD', BT1.Amount_USD__c);}
                                    					if(BT1.Unapplied__c == null) {gen.writeNullField('Unapplied');} else{gen.writeNumberField('Unapplied', BT1.Unapplied__c);}	
                                        			//	if(SOID =='soid') {gen.writeNullField('SOId');} else{gen.writeStringField('SOId', SOID);}
                                        			//	if(SOname =='soname') {gen.writeNullField('SOName');} else{gen.writeStringField('SOName', SOname);}
                                    			 gen.writeFieldName('BankTransactionsApplied');
                                    			 gen.writeStartArray();
                                        			If(!BTA.isEmpty()){
                                        		 			for(Bank_Transaction_Applied__c BTA1 :BTA){
                                                               
                                                     					If(BTA1.Bank_Transaction__c == BT1.ID && BTA1.Id != null){
                                                                       gen.writeStartObject();
                                                                            if(BTA1.Id == null) {gen.writeNullField('Id');} else{gen.writeStringField('Id',BTA1.Id);}
                                        									if(BTA1.name == null) {gen.writeNullField('Name');} else{gen.writeStringField('Name', BTA1.name);}
                                        									if(BTA1.Date__c == null) {gen.writeNullField('Date');} else{gen.writeDateField('Date', BTA1.Date__c);}
                                        									if(BTA1.Invoice__c == null) {gen.writeNullField('InvoiceName');} else{gen.writeStringField('InvoiceName', BTA1.Invoice__r.Name);}
                                                                            if(BTA1.Invoice__c == null) {gen.writeNullField('Invoiceid');} else{gen.writeStringField('Invoiceid', BTA1.Invoice__c);}
                                    										if(BTA1.Amount__c == null) {gen.writeNullField('Amount');} else{gen.writeNumberField('Amount', BTA1.Amount__c);}					
																		    if(BTA1.Invoice__r.Shipment_Order__c ==null) {gen.writeNullField('SOId');} else{gen.writeStringField('SOId', BTA1.Invoice__r.Shipment_Order__c);}
                                        									if(BTA1.Invoice__r.Shipment_Order__r.name ==null) {gen.writeNullField('SOName');} else{gen.writeStringField('SOName', BTA1.Invoice__r.Shipment_Order__r.name);}
                                    			                  
                                                                       gen.writeEndObject();     
                                                                        }
                                                     
                                                     
                                                 						}
                                        							}
                                        		gen.writeEndArray();
                                    gen.writeEndObject();
                                    		}
                                        
                                    
                                    gen.writeEndArray();
                                    
                                    
                        
                    			}
                    
                    gen.writeEndObject();
    				String jsonData = gen.getAsString();
               		System.debug('jsonData-->'+jsonData);
                  res.responseBody = Blob.valueOf(jsonData);
        	 	  res.statusCode = 200;
                 insert new API_Log__c(
                    Account__c = reqBody.AccountID,
                    EndpointURLName__c = 'NCPPaymentHistory',
                    Response__c = 'Success - Related BT and BTA sent for this account '+reqBody.AccountID,
                    StatusCode__c = String.valueof(res.statusCode)
                    );
                    
            
                
                }
                 else{
                res.responseBody = Blob.valueOf(JSON.serializePretty('Access Token is invalid or expired.'));
                res.addHeader('Content-Type', 'application/json');
                res.statusCode = 400;
                insert new API_Log__c(
                    Account__c = reqBody.AccountID,
                    EndpointURLName__c = 'NCPPaymentHistory',
                    Response__c = 'Error - Invalid AccessToken',
                    StatusCode__c = String.valueof(res.statusCode)
                    );

                 }
            }catch(Exception e){
                
            String ASD = 'Something went wrong while sending related invoices of Account, please contact support_SF@tecex.com';
            res.responseBody = Blob.valueOf(JSON.serializePretty(ASD));
            res.addHeader('Content-Type', 'application/json');
            res.statusCode = 500;
            insert new API_Log__c(
                Account__c = (reqBody != null && String.isNotBlank(reqBody.AccountID)) ? reqBody.AccountID : null,
                EndpointURLName__c = 'NCPPaymentHistory',
                Response__c = 'Error - NCPPaymentHistory :'+e.getMessage(),
                StatusCode__c = String.valueof(res.statusCode)
                );
                
                
            }
            
            
            
        }
        
        
        
    }
 public class RequestWrapper {
        public String Accesstoken;
        public String AccountID;
       
    }
 // List<Bank_transactions__c> BT = [SELECT RecordType.Name,BTname__C,Accounting_Period_Formula__c,Accounting_Period_New__c,Amount_USD__c,Amount__c,Applied_amount__c,Bank_Account_New__c,Bank_Account__c,Bank_charges__c,BM_email__c,BT_Comments__c,BT_Status__c,Cash_Disbursement__c,Cheque_Payment__c,Conversion_Rate__c,CreatedById,CreatedDate,Currency__c,Date__c,Finance_Team__c,Forex_Rate__c,Forex_total__c,Id,IsDeleted,LastActivityDate,LastModifiedById,LastModifiedDate,LastReferencedDate,LastViewedDate,Month_End_Journal_Entries__c,Name,New_Month_Reversal_Journal_Entries__c,OwnerId,Penalty_Applied_Amount__c,Posted_Forex__c,Post_Forex__c,Rate_adjustment__c,RecordTypeId,R_Type__c,Status__c,Supplier__c,SystemModstamp,Transaction_Lines_Created__c,Transaction_Line_Balance__c,Transaction_Total_Neg__c,Transaction_Total__c,Type__c,Unapplied__c,USD_payment__c,WT_Number__c  FROM Bank_transactions__c where Supplier__c =: reqBody.AccountID ];
                
}