@isTest
public class NCPInvoiceDetails_Test {
	
    static testMethod void  testForNCPInvoiceDetails(){
        
        Access_token__c accessTokenObj = new Access_token__c(
        	AccessToken_encoded__c='zmM0MDcxOTItZTJiZi00YWRhLTlkZGQtZGZhMmUxM2FiYTlj',
            Access_Token__c='cc407192-e2bf-4ada-9ddd-dfa2e13aba9c',
            Status__c='Active');
        
        Insert accessTokenObj;
        
        Account acc = new Account(Name = 'TecEx Prospective Client');
        
        Insert acc; 
        
        CpaRules__c cpaRules = new CpaRules__c(
            Country__c = 'Finland',
        	Criteria__c = 'Based on Condition');
        Insert cpaRules;
        
        IOR_Price_List__c iorPriceList = new IOR_Price_List__c(
        	ior_fee__c = 5,
        	client_name__c = acc.Id);
        Insert iorPriceList;
        
        IOR_Price_List__c iorpl = new IOR_Price_List__c ( Client_Name__c = acc.Id, Name = 'Finland', IOR_Fee__c = 0.15, On_Charge_Mark_up__c = 0.15,
                                                             TecEx_Shipping_Fee_Markup__c =0.40, IOR_Min_Fee__c = 100,  Admin_Fee__c =100, Set_up_fee__c =100, Bank_Fees__c =200, Tax_Rate__c = 0.20,Admin_Above_10000__c = 1000,Bank_Above_10000__c=100,
                                                             Estimated_Customs_Brokerage__c =250, Estimated_Customs_Clearance__c =250, Estimated_Customs_Handling__c =250, Estimated_Import_License__c =250, Destination__c = 'Finland',Agreed_Pricing__c = false );
            insert iorpl;
                
        Shipment_Order__c shipmentOrder = new Shipment_Order__c(
        	POD_Date__c = Date.newInstance(2020, 03, 21),
        	Destination__c = 'Finland',
        	Account__c = acc.Id,
            Shipment_Value_USD__c = 100,
            of_Line_Items__c = 1,
        	IOR_Price_List__c = iorpl.Id);
        Insert shipmentOrder;
        
        Invoice_New__c invoice = new Invoice_New__c(
            account__c = acc.Id,
        	Shipment_Order__c = shipmentOrder.Id);
        
        Insert invoice; 
        
        Map<String,String> body = new Map<String,String> {'Accesstoken'=>accessTokenObj.Access_Token__c,'AccountID'=>acc.Id,'InvoiceID'=>invoice.Id};
        RestRequest req1 = new RestRequest();
        RestResponse res1 = new RestResponse();
        req1.requestURI = '/services/apexrest/NCPInvoiceDetails/'; 
        req1.httpMethod = 'POST';
        req1.requestBody = Blob.valueOf(JSON.serialize(body));     
        RestContext.request = req1;
        RestContext.response = res1;
        RestContext.request.addHeader('Content-Type', 'application/json');
        
        Test.startTest(); 
        	NCPInvoiceDetails.logEmailStatementRequest();
        Test.stopTest();
        
    }
    
    static testMethod void  testErrorForNCPInvoiceDetails(){
        RestRequest req1 = new RestRequest();
        RestResponse res1 = new RestResponse();
        req1.requestURI = '/services/apexrest/NCPInvoiceDetails/'; 
        req1.httpMethod = 'POST';
        req1.requestBody = Blob.valueOf('');     
        RestContext.request = req1;
        RestContext.response = res1;
        RestContext.request.addHeader('Content-Type', 'application/json');
        
        Test.startTest(); 
        	NCPInvoiceDetails.logEmailStatementRequest();
        Test.stopTest();
    }
}