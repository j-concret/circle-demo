@RestResource(urlMapping='/MyAllAddresses/*')
Global class CPAddressManagement {
     @Httpget
    global static void CPPickupAddress(){
        
        RestRequest req = RestContext.request;
    	RestResponse res = RestContext.response;
     	res.addHeader('Content-Type', 'application/json');
        String ClientAccId = req.params.get('ClientAccId');
        
       
        
        try {
            Id PickuprecordTypeId = Schema.SObjectType.Client_Address__c.getRecordTypeInfosByDeveloperName().get('PickUp').getRecordTypeId();
            Id FDrecordTypeId = Schema.SObjectType.Client_Address__c.getRecordTypeInfosByDeveloperName().get('Final_Destinations').getRecordTypeId();
        
            List<Client_Address__c> ABs = [select id,recordtype.name,Address_Status__c,Name,default__c,Comments__c,CompanyName__c,AdditionalContactNumber__c,Contact_Full_Name__c,Contact_Email__c,Contact_Phone_Number__c,Address__c,Address2__c,City__c,Province__c,Postal_Code__c,All_Countries__c,Client__c    from Client_Address__c where Client__c =:ClientAccId   and( RecordType.ID =:PickuprecordTypeId or RecordType.ID =:FDrecordTypeId ) ORDER BY default__C desc];
            IF(!ABs.Isempty()){
                    res.responseBody = Blob.valueOf(JSON.serializePretty(ABs));
        			res.statusCode = 200;
                
                API_Log__c Al = New API_Log__c();
                Al.Account__c=ClientAccId;
                Al.EndpointURLName__c='PickupAddress';
                Al.Response__c='Success - List of Pickup Address are sent';
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al;
            }
            
            Else{
                
                String ErrorString = 'No Addresses found';
                res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
        		res.statusCode = 400;
                
                API_Log__c Al = New API_Log__c();
                Al.Account__c=ClientAccId;
                Al.EndpointURLName__c='PickupAddress';
                Al.Response__c='Success - 0 Pickup Address are found';
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al;
                   
                
            }
        }
        
        Catch(Exception e){
           		String ErrorString ='There are currently no addresses for this country. Please create a new address.';
            System.debug(ErrorString);
              
              }
        
           }
    
    

}