@IsTest
public class NCPSOAEmail_Test {
	static testMethod void testNCPSOAEmail(){
        Access_token__c accessTokenObj = new Access_token__c();
        accessTokenObj.Status__c = 'Active';
        accessTokenObj.Access_Token__c = 'asdfghj-sdfgfds-sfdfgf';
        
        Insert accessTokenObj;
        
        Account acc = new Account(Name = 'TecEx Prospective Client');
        
        Insert acc;
        
        Contact contact = new Contact ( lastname ='Testing Individual',  AccountId = acc.Id, Email='math@gmail.com'); 
        insert contact;
        
        Contact contact2 = new Contact ( lastname ='Testing Individual2',  AccountId = acc.Id, Email='math2@gmail.com',Include_in_invoicing_emails__c = true); 
        insert contact2;  
        
        NCPSOAEmailwra wrapper = new NCPSOAEmailwra();
        wrapper.Accesstoken = accessTokenObj.Access_Token__c;
        wrapper.AccountID = acc.Id;
        wrapper.toEmailID = 'math@gmail.com';
        wrapper.ccEmailID = 'test@test.com';
        
        RestRequest req1 = new RestRequest();
        RestResponse res1 = new RestResponse();
        req1.requestURI = '/services/apexrest/SOAEmail'; 
        req1.httpMethod = 'POST';
        req1.requestBody = Blob.valueOf(JSON.serialize(wrapper));     
        RestContext.request = req1;
        RestContext.response = res1;
        RestContext.request.addHeader('Content-Type', 'application/json');
        
        Test.startTest(); 
        NCPSOAEmail.NCPSOAEmail();
        Test.stopTest();
    }
    
    static testMethod void testNCPSOAEmailElse(){
        Access_token__c accessTokenObj = new Access_token__c();
        accessTokenObj.Status__c = 'Expired';
        accessTokenObj.Access_Token__c = 'asdfghj-sdfgfds-sfdfgf';
        
        Insert accessTokenObj;
        
        Account acc = new Account(Name = 'Acme Test');
        
        Insert acc;
        
        NCPSOAEmailwra wrapper = new NCPSOAEmailwra();
        wrapper.Accesstoken = accessTokenObj.Access_Token__c;
        wrapper.AccountID = acc.Id;
        wrapper.toEmailID = 'test@test.com';
        wrapper.ccEmailID = 'test@test.com';
        
        RestRequest req1 = new RestRequest();
        RestResponse res1 = new RestResponse();
        req1.requestURI = '/services/apexrest/SOAEmail'; 
        req1.httpMethod = 'POST';
        req1.requestBody = Blob.valueOf(JSON.serialize(wrapper));     
        RestContext.request = req1;
        RestContext.response = res1;
        RestContext.request.addHeader('Content-Type', 'application/json');
        
        Test.startTest(); 
        NCPSOAEmail.NCPSOAEmail();
        Test.stopTest();
    }
    
    static testMethod void testNCPSOAEmailCatchBlock(){
        Access_token__c accessTokenObj = new Access_token__c();
        accessTokenObj.Status__c = 'Active';
        accessTokenObj.Access_Token__c = 'asdfghj-sdfgfds-sfdfgf';
        
        Insert accessTokenObj;
        
        Account acc = new Account(Name = 'TecEx Prospective Client');
        
        Insert acc;
        
        NCPSOAEmailwra wrapper = new NCPSOAEmailwra();
        wrapper.Accesstoken = accessTokenObj.Access_Token__c;
        wrapper.AccountID = acc.Id;
        wrapper.toEmailID = 'test@test.com';
        wrapper.ccEmailID = 'test@test.com';
        
        RestRequest req1 = new RestRequest();
        RestResponse res1 = new RestResponse();
        req1.requestURI = '/services/apexrest/SOAEmail'; 
        req1.httpMethod = 'POST';
        req1.requestBody = Blob.valueOf(JSON.serialize(wrapper));     
        RestContext.request = req1;
        RestContext.response = res1;
        RestContext.request.addHeader('Content-Type', 'application/json');
        
        Test.startTest(); 
        NCPSOAEmail.NCPSOAEmail();
        Test.stopTest();
    }
}