public class ValuationCalAppendix2Controller {
    
    public Shipment_Order__c shipmentOrder {get;set;}
    public Map<Id, String> partToAttachmentMap {get;set;}
    public Map<String,Boolean> partImageMapRendered {get;set;}
    public boolean hasData {get;set;}
    
    public ValuationCalAppendix2Controller(ApexPages.StandardController controller){
        
        List<Id> partId = new List<Id>();
        Map<Id, String> idToImage = new Map<Id, String>();
        Map<Id,Id> cntDocumentids = new Map<Id,Id>();
        partImageMapRendered = new Map<String,Boolean>();
        partToAttachmentMap = new Map<Id, String>();
        hasData = false;
        
        Id currentSOId = ApexPages.currentPage().getParameters().get('id');
        
        this.shipmentOrder = [Select Id, NCP_Quote_Reference__c, (Select Id, Name from Parts__r) from Shipment_Order__c Where Id =:currentSOId];
        
        for(Part__c prt : shipmentOrder.Parts__r){
            partId.add(prt.Id);
            
        }
        
        for(ContentDocumentLink cnt : [SELECT Id, LinkedEntityId, ContentDocumentId FROM ContentDocumentLink Where LinkedEntityId =: partId]){
            cntDocumentids.put(cnt.ContentDocumentId,cnt.LinkedEntityId);
        }
        if(!cntDocumentids.isEmpty()){
            for(ContentVersion cntVersion : [SELECT Id, FirstPublishLocationId,ContentDocumentId, Title FROM ContentVersion where ContentDocumentId =:cntDocumentids.keySet() AND isLatest = true AND Title LIKE '%Attachment B%' order by CreatedDate]){
                idToImage.put(cntDocumentids.get(cntVersion.ContentDocumentId), cntVersion.Id);
                
            }
            if(!idToImage.isEmpty()){
                hasData = true;
                this.partToAttachmentMap = idToImage;
            }
            
        }
        
        for(Id key : partId){
            if(partToAttachmentMap.containsKey(key)){
                partImageMapRendered.put(key,true); 
            }else{
                partImageMapRendered.put(key, false); 
            }
        }
        
        
    }
    
}