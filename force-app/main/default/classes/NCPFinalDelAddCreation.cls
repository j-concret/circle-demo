@RestResource(urlMapping='/FinalDelAddresscreate/*')
global class NCPFinalDelAddCreation {
    
    @Httppost
    global static void CPFinalDelAddressCreation(){
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        Blob body = req.requestBody;
        res.addHeader('Content-Type', 'application/json');
        String requestString = body.toString();
        NCPFinalDestaddCreationwra rw = (NCPFinalDestaddCreationwra)JSON.deserialize(requestString,NCPFinalDestaddCreationwra.class);
        try {
              
 		Access_token__c At =[select status__c,Access_Token__c from Access_token__c where Access_Token__c=:rw.FianlDelAddress[0].Accesstoken ];
        if( at.status__C =='Active'){
        If(!rw.FianlDelAddress.isEmpty()){
            Id PickuprecordTypeId = Schema.SObjectType.Client_Address__c.getRecordTypeInfosByDeveloperName().get('Final_Destinations').getRecordTypeId();
              
          list<Client_Address__c> PickaddL = new list<Client_Address__c>();
                
              for(Integer i=0; rw.FianlDelAddress.size()>i;i++) {
              
                        Client_Address__c Pickadd = new Client_Address__c(
            
                        recordtypeID=PickuprecordTypeId,   
                        Name = Rw.FianlDelAddress[i].Name,
                        Contact_Full_Name__c= rw.FianlDelAddress[i].Contact_Full_Name,
                        Contact_Email__c = rw.FianlDelAddress[i].Contact_Email,
                        Contact_Phone_Number__c = rw.FianlDelAddress[i].Contact_Phone_Number,
                        Address__c=rw.FianlDelAddress[i].Address1,
                        Address2__c=rw.FianlDelAddress[i].Address2,
                        City__c=rw.FianlDelAddress[i].City,
                        Province__c=rw.FianlDelAddress[i].Province,
                        Postal_Code__c=rw.FianlDelAddress[i].Postal_Code,
                        Client__c=rw.FianlDelAddress[i].AccountID,
                        Comments__c= rw.FianlDelAddress[i].Comments,
                        CompanyName__c= rw.FianlDelAddress[i].CompanyName,
                        AdditionalContactNumber__c= rw.FianlDelAddress[i].AdditionalNumber,
                         address_status__c = 'Active',
                         All_Countries__c=rw.FianlDelAddress[i].All_Countries);
           
                    PickaddL.add(Pickadd); 
                   
             }
              Insert PickaddL;
            res.responseBody = Blob.valueOf(JSON.serializePretty(PickaddL));
            res.addHeader('Content-Type', 'application/json');
            res.statusCode = 200;
                API_Log__c Al = New API_Log__c();
                Al.Account__c=PickaddL[0].Client__c;
                Al.EndpointURLName__c='FinalAddresscreation';
                Al.Response__c='Success-Final Addrerss Created';
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al; 
              
        
        }
        }
       
        
        }
            catch(Exception e){
                 String ErrorString ='Something went wrong while creating final delivery address, please contact support_SF@tecex.com';
                res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
                res.addHeader('Content-Type', 'application/json');
                res.statusCode = 500;
                API_Log__c Al = New API_Log__c();
               // Al.Account__c=PickaddL[0].Client__c;
                Al.EndpointURLName__c='FinalAddresscreation';
                Al.Response__c=e.getMessage()+' at line number: '+e.getLineNumber();
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al;
                
            }
        
    }

}