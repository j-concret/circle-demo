public class RolloutComponentAddMoreSO {
    
      //1. Create rollout and costEstimates and return created cost estimates 
    @AuraEnabled
   
         public static List<Shipment_Order__c> CreateROSO(String ExistingrolloutID ,String AccID,String ConId,String Destinations,String ShipFrom,Decimal SOValue,String Courierresp,String Reff1,String Reff2){
         
          /*    Roll_Out__c new_roll_out = new Roll_Out__c( Client_Name__c=AccID,
                                                          Contact_For_The_Quote__c=ConId,
                                                          Shipment_Value_in_USD__c=SOValue,
                                                          Destinations__c=Destinations
                                                         );  
             insert new_roll_out;
            */ 
        Account ac = [Select ID,Lead_AM__c,Service_Manager__C,Financial_Manager__c,Operations_Manager__c,Financial_Controller__c,cse_IOR__c,finance_Team__C,CDC__c,invoice_Timing__C from Account where id =:AccID ];     
       List<Shipment_Order__c> SOList = new List<Shipment_Order__c>();
             
           
             
             for(String destinationlist: Destinations.split(';')) {
                
                 Shipment_Order__c Cos = new Shipment_Order__c();
                 Cos.Account__c = AccID;
                 Cos.Client_Contact_for_this_Shipment__c = ConId;  
                 Cos.Ship_From_Country__c = ShipFrom; 
                 Cos.Destination__c= destinationlist;
                 Cos.Shipment_Value_USD__c= SOValue;
                 Cos.Who_arranges_International_courier__c= Courierresp;
                 
                 Cos.RecordTypeId= '0120Y0000009cEz';
                 Cos.Roll_Out__c= ExistingrolloutID; 
                 Cos.Service_Type__c= 'IOR';  
                 Cos.Client_Reference__c= Reff1;
                 Cos.Client_Reference_2__c= Reff2;
                 Cos.service_Manager__C =ac.Service_Manager__c;
                 Cos.Shipping_Status__c   ='Roll-Out';
                 											Cos.Finance_Team__c =ac.finance_Team__C; //Added new 02182020
                                						    Cos.IOR_CSE__c =ac.cse_IOR__c; //Added new 02182020
                    										Cos.CDC__c =ac.CDC__c; //Added new 02182020
                    										Cos.invoice_Timing__C=ac.invoice_Timing__C;//Added new 02182020
                    										Cos.Lead_AM__c=ac.Lead_AM__c;//Added new 02182020
                   											Cos.Financial_Controller__c=ac.Financial_Controller__c;//Added new 02182020
                   											Cos.Financial_Manager__c =ac.Financial_Manager__c; 	//Added new 02182020 
                 										    Cos.Operations_Manager__c = ac.Operations_Manager__c; //Added new 
                 
                 SOList.add(Cos);
             }
          //  insert SOList;
            
               Roll_Out__c Rl = [Select Id, destinations__C,Number_of_Cost_Estimates__c from roll_Out__c where Id =: ExistingrolloutID ];
            	string ExistingDestinations = Rl.Destinations__c;
                Decimal NoCos = 0.00;
                NoCos= Rl.Number_of_Cost_Estimates__c;
             
            	Rl.Destinations__c =  ExistingDestinations+';'+Destinations;
             	Rl.Number_of_Cost_Estimates__c =NoCos+SOList.size();
                Update Rl;
          
            // 
           
             
             
             for(Integer i=0; i<=SOList.size();i++) {
                 if(i<SOList.size()){
                                                         
                   insert SOList [i];
                                       
                 System.debug('Inner Loop'+i);
                 }
             }
            
             
             
             
             System.debug('Created SOLIST in method--->'+SOList);
             
                          
          return [select ID,name,Roll_Out__c,Account__c,Service_Type__c,Client_Reference__c,Client_Reference_2__c,Client_Contact_for_this_Shipment__c,Ship_From_Country__c,Destination__c,Shipment_Value_USD__c,Who_arranges_International_courier__c,Shipping_Status__c from Shipment_Order__c where Roll_Out__c =: ExistingrolloutID];
 
         }
    
     //2. Create costestimates to created rolllout and return created all costestimates 
    @AuraEnabled
   
         public static List<Shipment_Order__c> CreateMoreROSO(String RolloutId,String Destinations,String AccID,String ConId,String ShipFrom,Decimal SOValue,String Courierresp,String Reff1,String Reff2){
                    
            
              Account ac = [Select ID,Lead_AM__c,Service_Manager__C,Financial_Manager__c,Operations_Manager__c, Financial_Controller__c,cse_IOR__c,finance_Team__C,CDC__c,invoice_Timing__C from Account where id =:AccID ];            
             List<Shipment_Order__c> SOList = new List<Shipment_Order__c>();
           
             
             for(String destinationlist: Destinations.split(';')) {
                
                 Shipment_Order__c Cos = new Shipment_Order__c();
                 Cos.Account__c = AccID;
                 Cos.Client_Contact_for_this_Shipment__c = ConId;  
                 Cos.Ship_From_Country__c = ShipFrom; 
                 Cos.Destination__c= destinationlist;
                 Cos.Shipment_Value_USD__c= SOValue;
                 Cos.Who_arranges_International_courier__c= Courierresp;
                
                 Cos.RecordTypeId= '0120Y0000009cEz';
                 Cos.Roll_Out__c= RolloutId; 
                 Cos.Service_Type__c= 'IOR';  
                 Cos.Client_Reference__c= Reff1;
                 Cos.Client_Reference_2__c= Reff2;
                 Cos.service_Manager__C =ac.Service_Manager__c;
                 Cos.Shipping_Status__c   ='Roll-Out';
                 
                 											Cos.Finance_Team__c =ac.finance_Team__C; //Added new 02182020
                                						    Cos.IOR_CSE__c =ac.cse_IOR__c; //Added new 02182020
                    										Cos.CDC__c =ac.CDC__c; //Added new 02182020
                    										Cos.invoice_Timing__C=ac.invoice_Timing__C;//Added new 02182020
                    										Cos.Lead_AM__c=ac.Lead_AM__c;//Added new 02182020
                   											Cos.Financial_Controller__c=ac.Financial_Controller__c;//Added new 02182020
                   											Cos.Financial_Manager__c =ac.Financial_Manager__c; 	//Added new 02182020 
                 										    Cos.Operations_Manager__c = ac.Operations_Manager__c; //Added new
                 
                 SOList.add(Cos);
             }
             insert SOList;
             
                          
          return [select ID,name,Roll_Out__c,Account__c,Service_Type__c,Client_Reference__c,Client_Reference_2__c,Client_Contact_for_this_Shipment__c,Ship_From_Country__c,Destination__c,Shipment_Value_USD__c,Who_arranges_International_courier__c,Shipping_Status__c from Shipment_Order__c where Roll_Out__c =: RolloutId];
 
         }
     //3.Update Chargableweight to all costestimates on rollout 
    @AuraEnabled
   
         public static List<Shipment_Order__c> UpdateChargableWeight(String RolloutId,Decimal Chweight){
                    
             List<Shipment_Order__c> RelatedCE = [select ID,Chargeable_Weight__c from Shipment_Order__C where Roll_Out__c = :RolloutId and Chargeable_Weight__c = null];
             System.debug('fetched CE'+RelatedCE);
                         
             List<Shipment_Order__c> SOList = new List<Shipment_Order__c>();
           
             System.debug('selected Chargable weight'+Chweight);
             System.debug('Created rollout'+RolloutId);
             System.debug('CE size'+RelatedCE.size());
             
             
             for(Integer i=0; i<=RelatedCE.size();i++) {
                 if(i<RelatedCE.size()){
                    RelatedCE[i].Chargeable_Weight__c = Chweight;
                                     
                     update RelatedCE[i];
                                       
                 System.debug('Inner Loop'+i);
                 }
             }
             
           
             
                    
          return [select ID,name,Chargeable_Weight__c,Roll_Out__c from Shipment_Order__c where Roll_Out__c =: RolloutId Limit 1];
 
         }
    
    //4. Insert Packages from rolloutcomponent
     
    @AuraEnabled
    public static void savePackages(List<Shipment_Order_Package__c> accList, ID RolloutId){
        
        
        
        List<Shipment_Order__c> RelatedCE1 = [select ID,Chargeable_Weight__c from Shipment_Order__C where Roll_Out__c = :RolloutId];
       
        
        For(integer i=0; i<accList.size(); i++){
            list<Shipment_Order_Package__c> CCO = new list<Shipment_Order_Package__c> ();
            For(Integer l=0;l<RelatedCE1.size(); l++){
            Shipment_Order_Package__c COO1 = new Shipment_Order_Package__c();
                COO1.Shipment_Order__c = RelatedCE1[L].Id;
                COO1.packages_of_same_weight_dims__c = accList[i].packages_of_same_weight_dims__c;
                COO1.Weight_Unit__c = accList[i].Weight_Unit__c;
                COO1.Actual_Weight__c = accList[i].Actual_Weight__c;
                COO1.Dimension_Unit__c = accList[i].Dimension_Unit__c;
                COO1.Length__c = accList[i].Length__c;
                COO1.Breadth__c = accList[i].Breadth__c;
                COO1.Height__c = accList[i].Height__c;
                CCO.add(COO1);
              //  System.debug('COO1--->'+COO1);
              //  System.debug('RelatedCE1'+RelatedCE1[L].Id);
             }
            Insert CCO;
        //    System.debug('CCO--->'+CCO);
        }
        
    
}

    //5. Get Account and contact name for the selected roolout
     
    @AuraEnabled
    public static List<Roll_Out__c> GetAccIDandConID(ID RolloutIds){
        
        return [select ID,Client_Name__c,Contact_For_The_Quote__c from Roll_Out__c where ID =: RolloutIds Limit 1];
        
       
    
}
    

}