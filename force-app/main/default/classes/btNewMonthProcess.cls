global class btNewMonthProcess implements Database.Batchable<sObject> {


    global Database.QueryLocator start(Database.BatchableContext BC) {
        // collect the batches of records or objects to be passed to execute
        
        String query = 'SELECT Id, Unapplied__c, Supplier__c , RecordTypeId, Date__c, Bank_Account_New__c FROM Bank_transactions__c where Unapplied__c != 0';
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<Bank_transactions__c> btList) {
       
        // process each batch of records
        
        Map<Id, Account> accountMap = new Map<Id, Account>([Select Id, Name, RecordTypeId
                                                             FROM Account]);
        
        Map<Id, Accounting_Period__c> accPeriodMap = new Map<Id, Accounting_Period__c>([Select Id, Name 
                                                                                 From Accounting_Period__c
                                                                                 Where Status__c = 'Open']); 
    
    
  Map<String, Id> accPeriod = new Map<String, Id>();   
  For (Id getAccPeriod : accPeriodMap.keySet()) { 
           accPeriod.put(accPeriodMap.get(getAccPeriod).Name, getAccPeriod );           
       } 
        
   Map<Id, Dimension__c> dimensionMap = new Map<Id, Dimension__c>([Select Id, Name
                                                                  From Dimension__c]);
    
   Map<String, Id> dimension = new Map<String, Id>(); 
    
    For (Id getDimension : dimensionMap.keySet()) { 
           dimension.put(dimensionMap.get(getDimension).Name, getDimension );           
       } 
   
   Map<Id, GL_Account__c> glAccountMap = new Map<Id, GL_Account__c>([Select Id, Name, GL_Account_Description__c,GL_Account_Short_Description__c,Currency__c  
                                                                  From GL_Account__c]); 
    
    
   Map<String, Id> glAccount = new Map<String, Id>(); 
    
   For (Id getGlAccount : glAccountMap.keySet()) { 
           glAccount.put(glAccountMap.get(getGlAccount).Name, getGlAccount );           
       } 
    
           integer year =  date.today().year();
           integer month = date.today().month();
           String month2 = String.valueOf(month);
           String month3 = month2.length() == 1? 0 + month2 : month2;
           
           String accountPeriod = String.valueOf(year)+'-'+ month3;
        
      List<Transaction_Line_New__c> transactionLines = new List<Transaction_Line_New__c>();
        
        for(Bank_transactions__c bts : btList){ 	
            
           
            bts.Accounting_Period_New__c = accPeriod.get(accountPeriod);
            
             Id accountRecordType = Schema.Sobjecttype.Account.getRecordTypeInfosByDeveloperName().get('Client').getRecordTypeId();
             Id btAccountRT = accountMap.get(bts.Supplier__c).RecordTypeId;
            
            If(bts.RecordTypeId == Schema.Sobjecttype.Bank_transactions__c.getRecordTypeInfosByDeveloperName().get('Payment').getRecordTypeId()){

                   Transaction_Line_New__c CTTL = new Transaction_Line_New__c(
                   Accounting_Period_New__c  = accPeriod.get(accountPeriod),
                   Amount__c = bts.Unapplied__c * -1 ,
                   Bank_Transaction__c = bts.Id,
                   Business_Partner__c = bts.Supplier__c,
                   Date__c = date.today(),
                   Description__c = btAccountRT == accountRecordType ? 'Revenue received in advance': 'Prepayments',
                   GL_Account__c = btAccountRT == accountRecordType ?  glAccount.get('920600'): glAccount.get('611500'),
                   RecordTypeId = Schema.Sobjecttype.Transaction_Line_New__c.getRecordTypeInfosByDeveloperName().get('Journal_Entry').getRecordTypeId(),
                   Revenue_Type__c = 'Forecast',
                   Short_Description__c  = btAccountRT == accountRecordType ? 'RRIA': 'Prepayments',
                   Status__c ='Posted',
                   Type_Code__c = 'CP');
                   transactionLines.add(CTTL);
                  
                  
                  Transaction_Line_New__c DTTL = new Transaction_Line_New__c(
                   Accounting_Period_New__c  = accPeriod.get(accountPeriod),
                   Amount__c = bts.Unapplied__c,
                   Bank_Transaction__c = bts.Id,
                   Business_Partner__c = bts.Supplier__c,
                   Date__c = date.today(),
                   Description__c = glAccountMap.get(bts.Bank_Account_New__c).GL_Account_Description__c,
                   GL_Account__c = bts.Bank_Account_New__c,
                   RecordTypeId = Schema.Sobjecttype.Transaction_Line_New__c.getRecordTypeInfosByDeveloperName().get('Journal_Entry').getRecordTypeId(),
                   Revenue_Type__c = 'Forecast',
                   Short_Description__c  =  glAccountMap.get(bts.Bank_Account_New__c).GL_Account_Short_Description__c,
                   Status__c ='Posted',
                   Type_Code__c = 'CP');
                   transactionLines.add(DTTL);     
            }
            
            
            
            
            If(bts.RecordTypeId == Schema.Sobjecttype.Bank_transactions__c.getRecordTypeInfosByDeveloperName().get('Receipt').getRecordTypeId()){

                   Transaction_Line_New__c CTTL = new Transaction_Line_New__c(
                   Accounting_Period_New__c  = accPeriod.get(accountPeriod),
                   Amount__c = bts.Unapplied__c * -1,
                   Bank_Transaction__c = bts.Id,
                   Business_Partner__c = bts.Supplier__c,
                   Date__c = date.today(),
                   Description__c = glAccountMap.get(bts.Bank_Account_New__c).GL_Account_Description__c,
                   GL_Account__c = bts.Bank_Account_New__c,
                   RecordTypeId = Schema.Sobjecttype.Transaction_Line_New__c.getRecordTypeInfosByDeveloperName().get('Journal_Entry').getRecordTypeId(),
                   Revenue_Type__c = 'Forecast',
                   Short_Description__c  =  glAccountMap.get(bts.Bank_Account_New__c).GL_Account_Short_Description__c,
                   Status__c ='Posted',
                   Type_Code__c = 'Cr');
                   transactionLines.add(CTTL);
                
                
                
                
                
                Transaction_Line_New__c DTTL = new Transaction_Line_New__c(
                   Accounting_Period_New__c  = accPeriod.get(accountPeriod),
                   Amount__c = bts.Unapplied__c,
                   Bank_Transaction__c = bts.Id,
                   Business_Partner__c = bts.Supplier__c,
                   Date__c = date.today(),
                   Description__c = btAccountRT == accountRecordType ? 'Revenue received in advance': 'Prepayments',
                   GL_Account__c = btAccountRT == accountRecordType ?  glAccount.get('920600'): glAccount.get('611500'),
                   RecordTypeId = Schema.Sobjecttype.Transaction_Line_New__c.getRecordTypeInfosByDeveloperName().get('Journal_Entry').getRecordTypeId(),
                   Revenue_Type__c = 'Forecast',
                   Short_Description__c  = btAccountRT == accountRecordType ? 'RRIA': 'Prepayments',
                   Status__c ='Posted',
                   Type_Code__c = 'CR');
                   transactionLines.add(DTTL);
                  
                  
     
            }
            
           
                
              
                
            }

        
        try {
        	
            
          insert transactionLines;
            
        
        } catch(Exception e) {
            System.debug(e);
        }
        
    }   
    
    global void finish(Database.BatchableContext BC) {
    	// execute any post-processing operations
  }
}