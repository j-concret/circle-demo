@RestResource(urlMapping='/DefaultUpdate/*')
global class CPDefaultUpdate {
    
     @Httppost
        global static void CPDefaultUpdation(){
     	RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        Blob body = req.requestBody;
    	res.addHeader('Content-Type', 'application/json');
        String requestString = body.toString();
        CPDefaultupdatewrapper rw = (CPDefaultupdatewrapper)JSON.deserialize(requestString,CPDefaultupdatewrapper.class);
     try {
           
           If(!rw.DefaultPref.isEmpty()){
                List<CPDefaults__c> ExistingDefaults = [select id,ship_From__c,Client_Account__c,Chargeable_Weight_Units__c,Li_ion_Batteries__c,Order_Type__c,Package_Dimensions__c,Package_Weight_Units__c,Second_hand_parts__c,Service_Type__c,TecEx_to_handle_freight__c from CPDefaults__c where Client_Account__c =:rw.DefaultPref[0].ClientAccId];
       
             
               list<CPDefaults__c> tobeupdate = new list<CPDefaults__c>();
                for(Integer J=0; ExistingDefaults.size()>J; J++) {
                    
                    for(Integer i=0; rw.DefaultPref.size()>i;i++) {
                        If(ExistingDefaults[j].id == rw.DefaultPref[i].CPDefaultRecordID){
                            
            						ExistingDefaults[j].Client_Account__c= rw.DefaultPref[i].ClientAccId;
            						ExistingDefaults[j].Chargeable_Weight_Units__c = rw.DefaultPref[i].Chargeable_Weight_Units;
            						ExistingDefaults[j].Li_ion_Batteries__c = rw.DefaultPref[i].ion_Batteries;
            						//ExistingDefaults[j].Order_Type__c=rw.DefaultPref[i].Order_Type;
            						ExistingDefaults[j].Package_Dimensions__c=rw.DefaultPref[i].Package_Dimensions;
            						ExistingDefaults[j].Package_Weight_Units__c=rw.DefaultPref[i].Package_Weight_Units;
           							ExistingDefaults[j].Second_hand_parts__c=rw.DefaultPref[i].Second_hand_parts;
            						ExistingDefaults[j].Service_Type__c=rw.DefaultPref[i].Service_Type;
            						ExistingDefaults[j].TecEx_to_handle_freight__c=rw.DefaultPref[i].TecEx_to_handle_freight;
                            		ExistingDefaults[j].ship_From__c=rw.DefaultPref[i].country;
                            tobeupdate.add(ExistingDefaults[j]);
                            
                        }
                    
                    }
                
                }
             		
               
               Update tobeupdate;
             
                   res.responseBody = Blob.valueOf(JSON.serializePretty(tobeupdate));
            	   res.statusCode = 200;
              	API_Log__c Al = New API_Log__c();
                Al.Account__c=ExistingDefaults[0].Client_Account__c;
                Al.EndpointURLName__c='CPDefaultUpdation';
                Al.Response__c='Success-Default preferecenses Updated';
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al; 
              
              
              }
              
             
            }
            
        	catch (Exception e){
                
              	String ErrorString ='Something went wrong while creating default preferences, please contact support_SF@tecex.com';
               	res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
            	res.statusCode = 500;
                API_Log__c Al = New API_Log__c();
                Al.EndpointURLName__c='CPDefaultupdation';
                Al.Response__c=e.getMessage()+' at line number: '+e.getLineNumber();
                Al.StatusCode__c=string.valueof(res.statusCode);
              	Insert Al;

              	}
        
        
   }

}