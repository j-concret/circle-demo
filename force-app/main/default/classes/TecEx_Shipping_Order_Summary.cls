global class TecEx_Shipping_Order_Summary implements Database.Batchable<sObject> {
    
    global String query;
    
    global TecEx_Shipping_Order_Summary() {
        query = 'Select Total_Invoiced_Amount__c,Total_Receipted__c,Id,Name,Client_Reference__c,Account__r.Name,Client_Contact_for_this_Shipment__c,Shipment_Status_Notes__c,Ship_From_Country__c,IOR_Price_List__r.Name ,Shipment_Value_USD__c,No_of_Parts__c, Shipping_Status__c,Final_Delivery_Date__c,Int_Courier_Tracking_No__c  from  Shipment_Order__c where Shipping_Status__c != \'POD Received\' AND Shipping_Status__c != \'Cancelled - No fees/costs\' AND Shipping_Status__c != \'Cancelled - With fees/costs\' AND Shipping_Status__c != \'Customs Clearance Docs Received\' AND Shipping_Status__c != \'Cost Estimate\' AND Account__r.CSE_IOR__c != null';
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<Shipment_Order__c> scope) {

        Shipping_Order_Email_Summary.sendEmailSummary(scope,null,null);

    }
    
    global void finish(Database.BatchableContext BC) {
        
    }
    
}