@isTest(SeeAllData=true)
public class FeedItem_CaseMail_Test {

    static testMethod void  sendFeedCommentMail_Test(){
                
         Profile profileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
        
        Account account = new Account(name='Acme1');
        insert account;
        
        Contact contact = new Contact ( lastname ='Testing Individual',  AccountId = account.Id); 
        insert contact;  
        
        User usr = new User(LastName = 'LIVESTON',
                            FirstName='JASON',
                            Alias = 'jliv',
                            Email = 'jason.liveston@asdf.com',
                            Username = 'jason.liveonce001@asdf.com',
                            ProfileId = profileId.id,
                            TimeZoneSidKey = 'GMT',
                            LanguageLocaleKey = 'en_US',
                            EmailEncodingKey = 'UTF-8',
                            LocaleSidKey = 'en_US'
                           );
        insert usr;
        System.runAs(usr) {
        
        Case caseObj = new Case(Subject='Test', Status = 'New');
        insert caseObj;
        FeedItem f = new FeedItem();
        f.ParentId = caseObj.Id;
        f.body = '@[JASON LIVESTON], test';
        insert f;
        FeedComment fc = new FeedComment();
        fc.CommentBody = '@[JASON LIVESTON], legal test';
      fc.FeedItemId=f.Id;
        insert fc;
            Case caseObj2 = new Case(Subject='Test', Status = 'New');
        insert caseObj2;
      FeedItem f1 = new FeedItem();
        f1.ParentId = caseObj2.Id;
        f1.body = '@[JASON LIVESTON], test';
        insert f1;
          FeedComment fc1 = new FeedComment();
      fc1.FeedItemId=f1.Id;
        fc1.CommentBody = '@[JASON LIVESTON], legal test';
        insert fc1;
            
        
        Test.startTest();
            List<FeedComment> feedCommentList = [Select FeedItemId,
                ParentId,
                CreatedDate,
                CreatedById,
            	CreatedBy.Name,
                CommentBody,
                RelatedRecordId from FeedComment];
        FeedItem_CaseMail.sendFeedCommentMail(feedCommentList);
        FeedItem_CaseMail.sendFeedItemMail(new List<FeedItem>{f1});
        Test.stopTest();
        
        }        
    }

}