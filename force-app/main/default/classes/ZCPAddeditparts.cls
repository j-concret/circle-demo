@RestResource(urlMapping='/ZCPAddeditlineitems/*')
Global class ZCPAddeditparts {
    
      @Httppost
      global static void ZCPAddeditparts(){
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        Blob body = req.requestBody;
    	res.addHeader('Content-Type', 'application/json');
        String requestString = body.toString();
        ZCPAddpartswra rw  = (ZCPAddpartswra)JSON.deserialize(requestString,ZCPAddpartswra.class);
          Try{
              Access_token__c At =[select status__c,Access_Token__c from Access_token__c where Access_Token__c=:rw.Accesstoken ];
              if( at.status__C =='Active' ){
                  
                list<Part__c > ExistingParts = new list<Part__c >();
                List<Part__c> insertLineItems = new List<Part__c>();
                  
                  If(rw.SOID !=null){
                       ExistingParts = [Select Name,shipment_order__c,Description_and_Functionality__c,Quantity__c,Commercial_Value__c,US_HTS_Code__c,Country_of_Origin2__c,ECCN_NO__c,Type_of_Goods__c,Li_ion_Batteries__c,Lithium_Battery_Types__c from Part__c  where shipment_order__c =:rw.SOID ];
               				 if(ExistingParts.size()>0) {
                    			RecusrsionHandler.skipTriggerExecution=true;
                   				 delete ExistingParts;
                				}
                    }
                  if(!rw.LineItems.isempty()){
                      
                    
                            for(Integer l=0; rw.LineItems.size()>l; l++) {
                                insertLineItems.add(
                                    new Part__c(
                                        Name = rw.LineItems[l].partNumber,
                                        Description_and_Functionality__c = rw.LineItems[l].description,
                                        Quantity__c = rw.LineItems[l].quantity,
                                        Commercial_Value__c = rw.LineItems[l].unitPrice,
                                        Shipment_Order__c = rw.SOId,
                                        US_HTS_Code__c = rw.LineItems[l].hsCode != null ? rw.LineItems[l].hsCode : '',
                                        Country_of_Origin__c = rw.LineItems[l].countryOfOrigin != null ? rw.LineItems[l].countryOfOrigin : ''
                                    )
                                );
                            }

							RecusrsionHandler.runTaskInQueueable = true;
                            
                            insert insertLineItems;
                      
                  }
               List<Part__C> PAL1=[select Id,Name,Client_Provided_Category__c,Description_and_Functionality__c,Quantity__c,Commercial_Value__c,Shipment_Order__c,US_HTS_Code__c,Country_of_Origin__c from Part__C where Id in :insertLineItems];
                res.responseBody = Blob.valueOf(JSON.serializePretty(PAL1));
                res.statusCode = 200;
                
                API_Log__c Al = New API_Log__c();
              //  Al.Account__c=rw.accountID;
                // Al.Login_Contact__c=So.Client_Contact_for_this_Shipment__c;
                Al.EndpointURLName__c='ZCPAddeditparts';
                Al.Response__c='Success - parts updated';
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al;
        
              
              
              }
              }
            
           catch(Exception e){
          	//String ErrorString ='Something went wrong while creating cost estimate, please contact support_SF@tecex.com';
            String ErrorString =e.getStackTraceString()+''+e.getLineNumber()+''+e.getMessage();
            res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
            res.addHeader('Content-Type', 'application/json');
            res.statusCode = 500;

            API_Log__c Al = New API_Log__c();
            Al.EndpointURLName__c='ZCPAddeditparts';
            Al.Response__c=e.getMessage()+' at line number: '+e.getLineNumber();
            Al.StatusCode__c=string.valueof(res.statusCode);
            Insert Al;

                      
                  }
              }

}