// Trigger handler for the Account object
public class TH_Account extends TH_Base {
    
    protected override map<String, String> dragonFieldMap() {
        return XF_Dragon_Request.ClientFieldMap;
    }   
    
    protected override Type fieldMapItemType() { 
        return AccountFieldChangeMap.class;
    }

    public void execute () { 
        if (Trigger.isAfter && Trigger.isUpdate) { afterUpdate(); }
        else if (Trigger.isAfter && (Trigger.isInsert || Trigger.isUndelete)) { afterInsert(); }
    }
    
    public void afterUpdate() {  
        map<Account, Account> items = new map<Account, Account>();
        list<Account> newitems = new list<Account>(); 
        for (SObject so: Trigger.new) {
        String status = (String)so.get(Account.Client_Status__c);
        Account oldAccount = (Account) Trigger.oldMap.get(so.Id);
        if(status == 'Active' && oldAccount.Client_Status__c == 'Prospect'){
                newitems.add((Account)so);
            } else if(status != 'Prospect'){
                items.put((Account)so, (Account)Trigger.oldMap.get(so.Id));
            }
        }
        if(items.size()>0){
        doAfterUpdate(items);
        }
        if(newitems.size()>0){
        doAfterInsert(newitems);
        }
    }
    
    public void afterInsert() {  
        list<Account> items = new list<Account>(); 
        for (SObject so: Trigger.new){
        String status = (String)so.get(Account.Client_Status__c);
        if(status == 'Active'){
            items.add((Account)so);
            }
            }
        doAfterInsert(items);
    }
    
    public void doAfterUpdate(map<Account, Account> items) {
        list<SimpleObjectMap> mappedItems = new list<SimpleObjectMap>();
        for (Account item: items.keySet()){
        String status = item.Client_Status__c;
        if(status == 'Active'){   
            try {
                if (!AppUtils.isAPICall(item, items.get(item))) 
                    if (hasChanges(items.get(item), item)) 
                        mappedItems.add(SimpleObjectMap.Make(item, null, fieldMap.XMLoldValues(), fieldMap.XMLnewValues()));
            } catch (Exception e) {
                SysUtils.processException(e, item);
                throw e;
            }
            }
        }
        dragonService.updateClientEntity(mappedItems);
    }
    
    public void doAfterInsert(list<Account> items) { 
        // Send the new account information to Dragon
        list<SimpleObjectMap> mappedItems = new list<SimpleObjectMap>();
        for (Account item: items){ 
        String status = item.Client_Status__c;
        if(status == 'Active'){
            try {
                if (!AppUtils.isAPICall(item))
                    mappedItems.add(SimpleObjectMap.Make(item, null, getFields(item)));
            } catch (Exception e) {
                SysUtils.processException(e, item);
                throw e;
            }
            }
        }
        dragonService.insertClients(mappedItems, true);
        // Extract the billing address and add it to the address object
        for (Account a: items){ 
        String status = a.Client_Status__c;
        if(status == 'Active'){
            try {
                if (!SysUtils.empty(SysUtils.ListOp.ALL, new list<string> {a.BillingStreet, a.BillingState, a.BillingPostalCode, a.BillingCountry, a.BillingCity})) {
                    System.Debug('Address found, now adding it to the address object.');
                    Client_Address__c address = new Client_Address__c();
                    address.Address__c = a.BillingStreet;
                    address.Province__c = a.BillingState;
                    address.Postal_Code__c = a.BillingPostalCode;
                    address.Country__c = a.BillingCountry;
                    address.City__c = a.BillingCity;
                    address.Client__c = a.ID;
                    address.Type__c = 'Physical';
                    insert address;
                    
                    Account forUpdate = new Account();
                    forUpdate.ID = a.ID;
                    forUpdate.BillingStreet = null;                     
                    forUpdate.BillingState = null;                      
                    forUpdate.BillingPostalCode = null;                     
                    forUpdate.BillingCountry = null;                    
                    forUpdate.BillingCity = null;  
                    forUpdate.Update_Dragon__c = 'No';  
                    update forUpdate;               
                }
            } catch (Exception e) {
                SysUtils.processException(e, a);
                throw e;
            }
            }
          }
    }
    
    public XF_ErrorWebServerResult pushClientToDragon(ID AccountId) {
        Account item = Database.query('SELECT ' + SysUtils.join(dragonFieldMap().keySet(), ', ') + ' FROM Account WHERE Id = :AccountId'); 
        list<SimpleObjectMap> mappedItems = new list<SimpleObjectMap>();
        mappedItems.add(SimpleObjectMap.Make(item, null, getFields(item)));
        return dragonService.insertClients(mappedItems, false);
    }
     
}