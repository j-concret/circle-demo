global class CommunityLoginController {
    
    @AuraEnabled
    global static Map<String,String> getUserDetails(){
        Map<String,String> response = new Map<String,String>();
        try {
            
             String Accesstoken = GuidUtilCP.NewGuid();
            System.debug('Accesstoken'+Accesstoken);
            string Base64Accesstoken = EncodingUtil.base64Encode(Blob.valueof(Accesstoken));
            System.debug('Base64Accesstoken'+Base64Accesstoken);
                
                 Access_token__c At = New Access_token__c();
                    At.Access_Token__c=Accesstoken;
                    At.AccessToken_encoded__c=Base64Accesstoken;
                 Insert At;  

            
            response.put('AccessToken',At.AccessToken_encoded__c);
            response.put('userSid',(UserInfo.getSessionId()));
            system.debug('userSid'+UserInfo.getSessionId());
            response.put('usid',UserInfo.getUserId());
            Id userId = UserInfo.getUserId();
                   User u = [select Id, contactId from User where Id = : userId];
                  // response.put('User',u);
                    if(u.contactId != null){
                    Id getContactId = u.contactId;
                    Contact contact = [SELECT Id , Name , Account.Id , Account.Name FROM Contact where Id =:getContactId];
                    response.put('AccountId',contact.Account.Id);
        }
        }
             catch (Exception ex) {
            response.put('error',ex.getMessage());
        }
        return response;
    }
    
    @AuraEnabled
    global static String checkPortal(String username, String password) {
        
        try {
            ApexPages.PageReference lgn = Site.login(username,password, '');
            aura.redirect(lgn);
          
           return null;
        }
        catch (Exception ex) {
            return ex.getMessage();
        }
    }
    @AuraEnabled
    global static String forgotPassowrd(String username) {
            String procesMsg = '';
            Boolean  isForgetDone;
            if (Site.isValidUsername(username)) {
                    
                    isForgetDone = Site.forgotPassword(username);
                    procesMsg = 'Email has been sent!';
                }
            else {
                
                procesMsg = 'Username Does not exists';
            }
            
            return procesMsg+'';
            
        }
}