@RestResource(urlMapping='/SendAllDevices/*')
Global class NCPSendallDevices {
     @Httppost
    global static void NCPSendallDevices(){
        
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        Blob body = req.requestBody;
    	res.addHeader('Content-Type', 'application/json');
        String requestString = body.toString();
        NCPSendallDevicesWra rw = (NCPSendallDevicesWra)JSON.deserialize(requestString,NCPSendallDevicesWra.class);
        
         try {
         List<Access_token__c> at = [SELECT status__c,Access_Token__c FROM Access_token__c WHERE Access_Token__c =: rw.Accesstoken and status__c = 'Active' LIMIT 1];
             if(!AT.isempty()){
           List<APP_Tokens__c> PAL1=[select Id,Contact__c,Device_Name__c,Registration_Token__c,Active__C from APP_Tokens__c where Contact__c  =: rw.ContactID and Active__C = TRUE];
      			if(!PAL1.isempty()){
            res.responseBody = Blob.valueOf(JSON.serializePretty(PAL1));
            res.statusCode = 200;
              	API_Log__c Al = New API_Log__c();
                Al.Login_Contact__c=rw.ContactID;
                Al.EndpointURLName__c='SendAllDevices';
                Al.Response__c='Success - SendAllDevices Info sent';
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al; 
          
     				 }
                 Else{
                      String ErrorString ='Contact doesnot have active devices';
               		res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
            		res.statusCode = 400;
                	API_Log__c Al = New API_Log__c();
                	Al.Login_Contact__c=rw.ContactID;
                	Al.EndpointURLName__c='SendAllDevices';
                	Al.Response__c=ErrorString;
                	Al.StatusCode__c=string.valueof(res.statusCode);
              		Insert Al;
                     
                 }
                 }
             Else{
                   String ErrorString ='INvalid access token or expired';
               	res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
            	res.statusCode = 400;
                API_Log__c Al = New API_Log__c();
                Al.Login_Contact__c=rw.ContactID;
                Al.EndpointURLName__c='SendAllDevices';
                Al.Response__c=ErrorString;
                Al.StatusCode__c=string.valueof(res.statusCode);
              	Insert Al;
             }
            
        }
    Catch(Exception e){
        
        		String ErrorString ='Something went wrong while sending device information, please contact support_SF@tecex.com';
               	res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
            	res.statusCode = 500;
                API_Log__c Al = New API_Log__c();
                Al.Login_Contact__c=rw.ContactID;
                Al.EndpointURLName__c='SendAllDevices';
                Al.Response__c=e.getMessage()+' at line number: '+e.getLineNumber();
                Al.StatusCode__c=string.valueof(res.statusCode);
              	Insert Al;
        
        
       }
        
        
        
        
    }
    

}