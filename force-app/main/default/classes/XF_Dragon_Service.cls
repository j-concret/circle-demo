public class XF_Dragon_Service {
 
	protected XF_Dragon_Request request;
	
	public XF_Dragon_Service() {
      request = new XF_Dragon_Request(); 	
	}

	private static Boolean checkSize(String name, list<SimpleObjectMap> items) {
		System.debug(String.format('Sending {0} {1} to Dragon.', new String[]{String.valueOf(items.size()), name}));
		return items.size() > 0;		
	}
		
	private static Boolean checkSize(String name, list<ID> items) {
		System.debug(String.format('Deleting {0} {1} from Dragon.', new String[]{String.valueOf(items.size()), name}));
		return items.size() > 0;		
	}
		
    @Future(callout=true)
    private static void sendDragonRequest(String request) {
    	new XF_Dragon().createAndSendRequest(request); 
    }
    
    private static XF_ErrorWebServerResult sendSyncDragonRequest(String request) {
    	HttpResponse response = new XF_Dragon().createAndSendRequest(request);
    	return new XF_ErrorWebServerResult(response); 
    } 
      
    public void updateClientEntity(list<SimpleObjectMap> items)
    {
   		if (checkSize('accounts', items))
   		    sendDragonRequest(request.updateClientEntity(items));    
    }
    
    public XF_ErrorWebServerResult insertClients(list<SimpleObjectMap> items, Boolean sendAsync)
    {
   		if (checkSize('accounts', items)) {
   		    if (sendAsync) {
   		    	sendDragonRequest(request.insertClients(items));
   		    	return new XF_ErrorWebServerResult();
   		    } 
   		    else return sendSyncDragonRequest(request.insertClients(items));
   		} else return new XF_ErrorWebServerResult();
    }
    
    public void upsertContacts(list<SimpleObjectMap> items, Boolean isNew)
    { 
   		if (checkSize('contacts', items))
   		    sendDragonRequest(request.upsertContacts(items, isNew));    
    }
    
    public void upsertAddresses(list<SimpleObjectMap> items, Boolean isNew)
    { 
   		if (checkSize('addresses', items))
   		    sendDragonRequest(request.upsertAddresses(items, isNew));    
    }
    
    public void deleteContacts(list<ID> items)
    { 
   		if (checkSize('contacts', items))
   		    sendDragonRequest(request.deleteContacts(items));    
    }
    
    public void deleteAddresses(list<ID> items)
    { 
   		if (checkSize('addresses', items))
   		    sendDragonRequest(request.deleteAddresses(items));    
    }
}