@RestResource(urlMapping='/NCPQuickQuoteCreation/*')
global class NCPQuickQuoteCreation {
    @Httppost
    global static void NCPQuickQuoteCreation(){
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        Blob body = req.requestBody;
        res.addHeader('Content-Type', 'application/json');
        String requestString = body.toString();
        
        NCPQuickQuoteCreationWrap rw = (NCPQuickQuoteCreationWrap)JSON.deserialize(requestString,NCPQuickQuoteCreationWrap.class);
        try {
            List<Access_token__c> at = [SELECT status__c,Access_Token__c FROM Access_token__c WHERE Access_Token__c =: rw.Accesstoken and status__c = 'Active' LIMIT 1];
            If(!at.isEmpty()){
                
                
                
                String costEstimateRID ;
                String leadRID ;
                Decimal estimatedCW = 0;
                Account ac;
                Contact contact;
                String ClientType;
                String Pricelist;
                
                Map<String,String> emailValidityRes = checkEmailValidation(rw.typeOfGoods, rw.emailAddress);
                
                if(String.isNotBlank(rw.typeOfGoods) && rw.typeOfGoods == 'Amazon FBA'){
                    leadRID = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Client (E-commerce)').getRecordTypeId();
                    costEstimateRID = Schema.SObjectType.Shipment_Order__c.getRecordTypeInfosByName().get('Zee Cost Estimate').getRecordTypeId();
                    ac = [SELECT Id,Service_Manager__c,Financial_Manager__c,Financial_Controller__c FROM Account WHERE Name = 'E-commerce Quick Quote Demo'];
                    contact = [select Id from Contact where AccountId =: ac.Id and LastName='E-Commerce - Don\'t Delete' limit 1];
                    ClientType='E-commerce';
                    Pricelist='E-commerce Price List';
                }else{
                    leadRID = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('TecEx').getRecordTypeId();
                    costEstimateRID = Schema.SObjectType.Shipment_Order__c.getRecordTypeInfosByName().get('Cost Estimate').getRecordTypeId();
                    ac = [SELECT Id,Service_Manager__c,Financial_Manager__c,Financial_Controller__c FROM Account WHERE Name = 'TecEx Quick Quote Demo'];
                    contact = [select Id from Contact where AccountId =: ac.Id and LastName='TecEx - Don\'t Delete' limit 1];
                    ClientType ='Reseller';
                    Pricelist='Standard Price List';
                    
                }
                
                
                
                
                //Lead Creation
                
                Lead lead = new Lead(Firstname=rw.FirstName,LastName = rw.LastName,Company = rw.companyName,Email = rw.emailAddress,
                                     Country = rw.companyLocation,
                                     phone=rw.contactNumber,
                                     Leadsource='Web',
                                     Type_of_Goods_being_shipped_online_form__c = rw.typeOfGoods,
                                     Contacts_Background__c = rw.aboutShipment,
                                     Phone_Extension__c = rw.countryCode,
                                     Client_Type__C=ClientType,
                                     Price_List_Type__c =Pricelist,
                                     RecordTypeId = leadRID);
                if(String.isNotBlank(rw.typeOfGoods) && rw.typeOfGoods == 'Amazon FBA'){
                    lead.Liability_Product_Agreement__c = 'Liability Cover Declined';
                    lead.Cash_outlay_fee_base__c = 'No Cash Outlay Fee';
                    /*lead.Finance_Charge__c = 0;
                    lead.IOR_Payment_Terms__c = 0;
                    lead.Prepayment_Term_Days__c = 0;*/
                }
                 
                
                
                Insert lead;
                
                //Shipment Creation Logic
                if(String.isNotBlank(rw.weightUnit) && rw.weightUnit.toUpperCase() == 'KG'){
                    estimatedCW = rw.chargableWeight;
                }else{
                    estimatedCW = rw.chargableWeight / 2.20462;
                }
                
                Shipment_Order__c shipment = new Shipment_Order__c();
                shipment.Account__c = ac.Id;
                shipment.Client_Contact_for_this_Shipment__c = contact.Id;
                shipment.Ship_From_Country__c= rw.shipFrom;
                shipment.Destination__c = rw.shipTo;
                shipment.Shipment_Value_USD__c= rw.shipmentValueUSD;
                shipment.Source__c= 'Client Portal';
                shipment.Chargeable_Weight__c = estimatedCW;
                shipment.Client_Reference__c = lead.Id;
                shipment.Client_Reference_2__c = rw.FirstName+' '+ rw.LastName;
                shipment.RecordTypeId= costEstimateRID;
                shipment.Who_arranges_International_courier__c= 'Client';
                shipment.Service_Type__c= 'IOR';
                shipment.Shipping_Status__c ='Cost Estimate Abandoned';
                shipment.Client_Taking_Liability_Cover__c = 'No';
                shipment.Final_Deliveries_New__c = 1;
                shipment.service_Manager__c = ac.Service_Manager__c;
                shipment.Financial_Manager__c = ac.Financial_Manager__c;
                shipment.Financial_Controller__c = ac.Financial_Controller__c;
                shipment.Lead_Name__c = lead.Id;
                
                RecusrsionHandler.skipFDTriggerExecutionOnSOCreation = true;
                insert shipment;
                
                 System.enqueueJob(new QueueableUpdateCE(shipment));
                
                shipment_Order__C SO1 = [Select Id,Account__c,Client_Contact_for_this_Shipment__c,Name,NCP_Quote_Reference__c,Ship_From_Country__c,IOR_Price_List__r.name,Service_Type__c,Final_Deliveries_New__c,Shipment_Value_USD__c,Type_of_Goods__c,Li_ion_Batteries__c,Lithium_Battery_Types__c,of_packages__c,Chargeable_Weight__c,TecEx_Invoice_Number__c, 
                                         IOR_FEE_USD__c,IOR_and_Import_Compliance_Fee_USD_numb__c,Tax_recovery_Premium_Fee__c,EOR_and_Export_Compliance_Fee_USD__c,EOR_and_Export_Compliance_Fee_USD_numb__c,
                                         Set_up_Fee__c,Handling_and_Admin_Fee__c,Bank_Fees__c,Quoted_Admin_Fee__c,Quoted_Bank_Fee__c,First_Invoice_Created__c,TopUp_Invoice_Created__c,Pull_CPA_and_IOR_PL__c,
                                         Taxes_Calculated__c, Recharge_License_Cost__c ,xxxTotal_Invoice_Amount__c,Total_Customs_Brokerage_Cost__c,Recharge_Handling_Costs__c,Total_clearance_Costs__c,
                                         Recharge_Tax_and_Duty__c,Recharge_Tax_and_Duty_Other__c, International_Delivery_Fee__c,Insurance_Fee_USD__c,Total_License_Cost__c,Finance_Fee__c,
                                         Miscellaneous_Fee__c,Miscellaneous_Fee_Name__c,Collection_Administration_Fee__c,Total_Invoice_Amount_Excl_Taxes_Duties__c,Total_including_estimated_duties_and_tax__c,
                                         Total_Invoice_Amount__c,Rebate_Due__c,Forex_Rate_Used_Euro_Dollar__c,ETA_Formula__c,Estimate_Transit_Time__c,
                                         shipping_Notes_formula__C,Estimate_Transit_Time_Formula__c,Estimate_Customs_Clearance_Time_Formula__c,Compliance_Process__c from Shipment_order__C where Id=: shipment.ID]; 
                
                if(emailValidityRes != null && emailValidityRes.get('Status') == 'OK'){
                    res.responseBody = Blob.valueOf(JSON.serializePretty(SO1));
                    res.addHeader('Content-Type', 'application/json');
                    res.statusCode = 200;
                    
                    API_Log__c Al = New API_Log__c();
                    Al.EndpointURLName__c='NCPQuickQuoteCreation';
                    Al.StatusCode__c=string.valueof(res.statusCode);
                    Insert Al;  
                }
                else if(emailValidityRes.get('Status') == 'ERROR'){
                    JSONGenerator gen = JSON.createGenerator(true);
                    gen.writeStartObject();
                    
                    gen.writeFieldName('Error');
                    gen.writeStartObject();
                    
                    gen.writeStringField('Message',emailValidityRes.get('Message'));
                    
                    gen.writeEndObject();
                    gen.writeEndObject();
                    String jsonData = gen.getAsString();
                    res.responseBody = Blob.valueOf(jsonData);
                    res.addHeader('Content-Type', 'application/json');
                    res.statusCode = 400;    
                    API_Log__c Al = New API_Log__c();
                    Al.EndpointURLName__c='NCPQuickQuoteCreation';
                    Al.StatusCode__c=string.valueof(res.statusCode);
                    Insert Al;
                }
            }
            else{
                
                String ErrorString ='Authentication failed';
                res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
                res.addHeader('Content-Type', 'application/json');
                res.statusCode = 400;
                
            }
            
        }
        catch(Exception e) {
             String ErrorString;
            if(!rw.emailAddress.contains('@')){ErrorString ='Invalid email address';}
            else{
          //  ErrorString ='Something went wrong while creating cost estimate, please contact support_SF@tecex.com';
          ErrorString = e.getLineNumber()+e.getStackTraceString()+e.getMessage();
            
            }
           // String ErrorString = e.getLineNumber()+e.getStackTraceString()+e.getMessage();
            res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
            res.addHeader('Content-Type', 'application/json');
            res.statusCode = 500;
            
            API_Log__c Al = New API_Log__c();
            Al.EndpointURLName__c='NCPQuickQuoteCreation';
            Al.Response__c=e.getMessage()+' at line number: '+e.getLineNumber();
            Al.StatusCode__c=string.valueof(res.statusCode);
            Insert Al;
            
        }
    }
    
    public static Map<String,String> checkEmailValidation(String typeOfGoods, String email){
        Map<String,String> response = new Map<String,String>();
        response.put('Status','OK');
        String emailRegex = '^[a-zA-Z0-9._|\\\\%#~`=?&/$^*!}{+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$'; // source: http://www.regular-expressions.info/email.html
        Pattern MyPattern = Pattern.compile(emailRegex);
        Matcher MyMatcher = MyPattern.matcher(email);
        
        if (!MyMatcher.matches()) {
            response.put('Status','ERROR');
            response.put('Message','Please enter a valid email address');
            return response;
        }
        
        String fullDomain = email.split('@').get(1);
        String Domain = (fullDomain.split('\\.').get(0)).toLowerCase();
        Map<String,EmailBlocker__c> emailBlockers = (Map<String,EmailBlocker__c>)EmailBlocker__c.getAll();
        
        if(emailBlockers !=null && emailBlockers.containsKey(Domain)){
            EmailBlocker__c blockedEmail = emailBlockers.get(Domain);
            List<String> business = (blockedEmail.Type_of_business__c).split(';');
            if(business.contains(typeOfGoods)){
                response.put('Status','ERROR');
                response.put('Message','The email address you provided is not a valid business email address');
            }
        }
        
        
        return response;
    }
}