@RestResource(urlMapping='/NCPupdateFreightPickupAddress/*')
Global class NCPupdateFreightAddress {

     @Httppost
    global static void NCPupdateFreightAddress(){
    
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        Blob body = req.requestBody;
    	res.addHeader('Content-Type', 'application/json');
        String requestString = body.toString();
        NCPupdateFreightAddressWra rw = (NCPupdateFreightAddressWra)JSON.deserialize(requestString,NCPupdateFreightAddressWra.class);
           try {
               
               
                Access_token__c At =[select status__c,Access_Token__c from Access_token__c where Access_Token__c=:rw.Accesstoken ];
            	if( at.status__C =='Active'){
                    
           		Freight__c FR = [Select Id,Ship_From_Address__c from Freight__c where id = :rw.FRID];      
           		Fr.Ship_from_address__c = rw.PickupaddressID;
                   
            	Update FR;
                    
            JSONGenerator gen = JSON.createGenerator(true);
            gen.writeStartObject();
            
             gen.writeFieldName('Success');
             gen.writeStartObject();
            
             	if(FR.ID == null) {gen.writeNullField('Response');} else{gen.writeStringField('Response', 'Success- FreightPickupAddress Updated');}
            
            gen.writeEndObject();
            gen.writeEndObject();
            String jsonData = gen.getAsString();
            res.responseBody = Blob.valueOf(jsonData);
            res.addHeader('Content-Type', 'application/json');
            res.statusCode = 200;        
           
            
                API_Log__c Al = New API_Log__c();
              //  Al.Account__c=SO.Account__c;
                Al.EndpointURLName__c='NCPupdateFreightPickupAddress';
                Al.Response__c='Success- updateFreightPickupAddress Updated';
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al; 
                }
               
               Else{
             JSONGenerator gen = JSON.createGenerator(true);
            gen.writeStartObject();
            
             gen.writeFieldName('Error');
             gen.writeStartObject();
            
             	gen.writeStringField('Response', 'Accesstoken expired');
            
            gen.writeEndObject();
            gen.writeEndObject();
            String jsonData = gen.getAsString();
            res.responseBody = Blob.valueOf(jsonData);
            res.addHeader('Content-Type', 'application/json');
            res.statusCode = 400;        
                  
                   
               }
            }
        	catch (Exception e){
                
              	 String ErrorString ='Something went wrong while updating ShippingFees details, please contact support_SF@tecex.com';
               	res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
                res.addHeader('Content-Type', 'application/json');
                res.statusCode = 500;
                API_Log__c Al = New API_Log__c();
                //Al.Account__c=FD.Shipment_Order__r.Account__c;
                Al.EndpointURLName__c='NCPupdateFreightPickupAddress';
                Al.Response__c=e.getMessage()+' at line number: '+e.getLineNumber();
                Al.StatusCode__c=string.valueof(res.statusCode);
              	Insert Al;
		     	}

        
        
    }
 

    
}