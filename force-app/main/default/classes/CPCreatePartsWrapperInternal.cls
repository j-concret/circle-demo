public class CPCreatePartsWrapperInternal {
    
     public class Parts {
		
        public String PartNumber;
		public String PartDescription;
		public Integer Quantity;
		public Double UnitPrice;
		public String HSCode;
		public String CountryOfOrigin;
        public String Type_of_Goods;
		public String Li_ion_Batteries;
		public String Li_ion_BatteryTypes;        
		public String ECCNNo;
	}

public List<Parts> Parts;

	
	 public static CPCreatePartsWrapperInternal parse(String json) {
         
         System.debug('json string in Wrapper'+json);
		return (CPCreatePartsWrapperInternal) System.JSON.deserialize(json, CPCreatePartsWrapperInternal.class);
	}

}