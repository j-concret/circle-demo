public class AppUtils 
{
	private static final String API_DATE = 'Last_API_Update__c';
	public static Boolean IS_API_CALL_OVERRIDE = false;
	
	public static Boolean IsAPICall(SObject item) 
	{
		return IsAPICall(item, null); 
	}

    // method assumes there is a Last_API_Update__c field on the passed object  	
	public static Boolean IsAPICall(SObject item, SObject oldItem) 
	{
		//This API Override switch can be set to ensure this function returns true
		//this has been implemented to allow Leads to be converted in a Test without the system trying to make API Calls
		if (IS_API_CALL_OVERRIDE)
			return true;
			
		if (oldItem != null) 
		{
  			System.debug('Old API Date: ' + oldItem.get(API_DATE) + ', New API date: ' + item.get(API_DATE));
			
			if (oldItem.get(API_DATE) != item.get(API_DATE)) 
			{
				System.debug('API call detected.');
				return true;
			} else 
				return false;
		} else
		{
			System.debug('API Date: ' + item.get(API_DATE));
			if (item.get(API_DATE) != null) 
			{
				System.debug('API call detected.');
				return true;
			} else 
				return false;
		}
	}		
}