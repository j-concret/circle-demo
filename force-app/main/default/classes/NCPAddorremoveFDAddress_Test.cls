@isTest
public class NCPAddorremoveFDAddress_Test {
	
    public static testMethod void FinalDeliveryDetails_Test(){
        
        //Test Data 
        
        Id shipmentOrderRecTypeId = Schema.SObjectType.shipment_order__c.getRecordTypeInfosByName().get('Cost Estimate').getRecordTypeId();
        
        CpaRules__c cpaRule = new CpaRules__c();
        cpaRule.Country__c = 'Finland';
        cpaRule.Criteria__c = 'Default';
        
        Insert cpaRule;
        
        Access_token__c at = new Access_token__c();
        at.Status__c = 'Active';
        at.Access_Token__c = '1234';
        
        Insert at;
        
        Account acc = new Account(Name = 'Acme Test');
        
        Insert acc;
        
        IOR_Price_List__c priceList = new IOR_Price_List__c();
        priceList.Destination__c = 'Finland';
        priceList.Client_Name__c = acc.Id;
        priceList.IOR_Fee__c = 10;
        
        Insert priceList;
        
        shipment_order__c shipment = new shipment_order__c();
        shipment.RecordTypeId = shipmentOrderRecTypeId;
        shipment.Destination__c = 'Finland';
        shipment.IOR_Price_List__c = priceList.Id;
        shipment.Shipment_Value_USD__c = 600;
        shipment.Account__c = acc.Id;
        
        Insert shipment;
        
        Client_Address__c clientAddress = new Client_Address__c();
        clientAddress.Name = 'My Address';
        clientAddress.client__c = acc.Id;
        
        insert clientAddress;
        
        Final_Delivery__c finalDelv = new Final_Delivery__c();
        finalDelv.Shipment_Order__c = shipment.Id;
        finalDelv.name= 'Test Final Delivery';
        finalDelv.Ship_to_address__C  = clientAddress.Id;
        
        insert finalDelv;

        //Json Body
        String json = '{"SOID":"'+shipment.Id+'","AccessToken":"'+at.Access_Token__c+'", "Final_Delivery" : [{"Name":"Test Final Delivery", "FinalDestinationAddressID":"'+clientAddress.Id+'"}]}';
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
        req.requestURI = '/services/apexrest/NCPAddorremoveFDAddress'; 
        req.httpMethod = 'Post';
        req.addHeader('Content-Type', 'application/json');
        req.requestBody = Blob.valueof(json);
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
        NCPAddorremoveFDAddress.NCPupdateFreightAddress();
        Test.stopTest(); 
    }
    
    public static testMethod void FinalDeliveryDetails_Test1(){
        
        //Test Data 
        
        Id shipmentOrderRecTypeId = Schema.SObjectType.shipment_order__c.getRecordTypeInfosByName().get('Cost Estimate').getRecordTypeId();
        
        CpaRules__c cpaRule = new CpaRules__c();
        cpaRule.Country__c = 'Finland';
        cpaRule.Criteria__c = 'Default';
        
        Insert cpaRule;
        
        Access_token__c at = new Access_token__c();
        at.Status__c = 'Expired';
        at.Access_Token__c = '1234';
        
        Insert at;
        
        Account acc = new Account(Name = 'Acme Test');
        
        Insert acc;
        
        IOR_Price_List__c priceList = new IOR_Price_List__c();
        priceList.Destination__c = 'Finland';
        priceList.Client_Name__c = acc.Id;
        priceList.IOR_Fee__c = 10;
        
        Insert priceList;
        
        shipment_order__c shipment = new shipment_order__c();
        shipment.RecordTypeId = shipmentOrderRecTypeId;
        shipment.Destination__c = 'Finland';
        shipment.IOR_Price_List__c = priceList.Id;
        shipment.Shipment_Value_USD__c = 600;
        shipment.Account__c = acc.Id;
        
        Insert shipment;
        
        Client_Address__c clientAddress = new Client_Address__c();
        clientAddress.Name = 'My Address';
        clientAddress.client__c = acc.Id;
        
        insert clientAddress;
        
        Final_Delivery__c finalDelv = new Final_Delivery__c();
        finalDelv.Shipment_Order__c = shipment.Id;
        finalDelv.name= 'Test Final Delivery';
        finalDelv.Ship_to_address__C  = clientAddress.Id;
        
        insert finalDelv;

        //Json Body
        String json = '{"SOID":"'+shipment.Id+'","AccessToken":"'+at.Access_Token__c+'", "Final_Delivery" : [{"Name":"Test Final Delivery", "FinalDestinationAddressID":"'+clientAddress.Id+'"}]}';
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
        req.requestURI = '/services/apexrest/NCPAddorremoveFDAddress'; 
        req.httpMethod = 'Post';
        req.addHeader('Content-Type', 'application/json');
        req.requestBody = Blob.valueof(json);
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
        NCPAddorremoveFDAddress.NCPupdateFreightAddress();
        Test.stopTest(); 
    }
    
    //Covering Catch Exception
    public static testMethod void FinalDeliveryDetails_Test2(){
        
        //Test Data 
        
        Id shipmentOrderRecTypeId = Schema.SObjectType.shipment_order__c.getRecordTypeInfosByName().get('Cost Estimate').getRecordTypeId();
        
        CpaRules__c cpaRule = new CpaRules__c();
        cpaRule.Country__c = 'Finland';
        cpaRule.Criteria__c = 'Default';
        
        Insert cpaRule;
        
        Access_token__c at = new Access_token__c();
        at.Status__c = 'Active';
        at.Access_Token__c = '1234';
        
        Insert at;
        
        Account acc = new Account(Name = 'Acme Test');
        
        Insert acc;
        
        IOR_Price_List__c priceList = new IOR_Price_List__c();
        priceList.Destination__c = 'Finland';
        priceList.Client_Name__c = acc.Id;
        priceList.IOR_Fee__c = 10;
        
        Insert priceList;
        
        shipment_order__c shipment = new shipment_order__c();
        shipment.RecordTypeId = shipmentOrderRecTypeId;
        shipment.Destination__c = 'Finland';
        shipment.IOR_Price_List__c = priceList.Id;
        shipment.Shipment_Value_USD__c = 600;
        shipment.Account__c = acc.Id;
        
        Insert shipment;
        
        Client_Address__c clientAddress = new Client_Address__c();
        clientAddress.Name = 'My Address';
        clientAddress.client__c = acc.Id;
        
        insert clientAddress;
        
        Final_Delivery__c finalDelv = new Final_Delivery__c();
        finalDelv.Shipment_Order__c = shipment.Id;
        finalDelv.name= 'Test Final Delivery';
        finalDelv.Ship_to_address__C  = clientAddress.Id;
        
        insert finalDelv;

        //Json Body
        String json = '{"SOID":"'+shipment.Id+'","AccessToken":"'+at.Access_Token__c+'", "Final_Delivery" : [{"Name":"Test Final Delivery", "FinalDestinationAddressID":"Null"}]}';
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
        req.requestURI = '/services/apexrest/NCPAddorremoveFDAddress'; 
        req.httpMethod = 'Post';
        req.addHeader('Content-Type', 'application/json');
        req.requestBody = Blob.valueof(json);
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
        NCPAddorremoveFDAddress.NCPupdateFreightAddress();
        Test.stopTest(); 
    }
}