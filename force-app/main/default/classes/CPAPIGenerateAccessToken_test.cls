@IsTest
public class CPAPIGenerateAccessToken_test {
  static testMethod void  setUpData1(){
     
           Account account = new Account (name='Acme1', Type ='Client', 
                                       CSE_IOR__c = '0050Y000001LTZO', 
                                       Service_Manager__c = '0050Y000001LTZO', 
                                         Financial_Manager__c='0050Y000001LTZO', 
                                         Financial_Controller__c='0050Y000001LTZO', 
                                       Tax_recovery_client__c  = FALSE,
                                       Ship_From_Line_1__c = 'Ship_From_Line_1',
                                       Ship_From_Line_2__c = 'Ship_From_Line_2',
                                       Ship_From_Line_3__c = 'Ship_From_Line_3',
                                       Ship_From_City__c = 'Ship_From_City', 
                                       Ship_From_Zip__c = 'Ship_From_Zip',
                                       Ship_From_Country__c = 'Ship_From_Country' , 
                                       Ship_From_Other_notes__c = 'Ship_From_Other',
                                       Ship_From_Contact_Name__c = 'Ship_From_Contact',
                                       Ship_From_Contact_Email__c = 'Ship_From_Email',
                                       Ship_From_Contact_Tel__c = 'Ship_From_Tel',
                                       Client_Address_Line_1__c = 'Client_Address_Line_1',
                                       Client_Address_Line_2__c = 'Client_Address_Line_2',
                                       Client_Address_Line_3__c = 'Client_Address_Line_3',
                                       Client_City__c = 'Client_City',
                                       Client_Zip__c = 'Client_Zip',
                                       Client_Country__c = 'Client_Country', 
                                       Other_notes__c = 'Other_notes',
                                       Tax_Name__c = 'Tax_Name',
                                       Tax_ID__c = 'Tax_ID',
                                       Contact_Name__c = 'Contact_Name',
                                       Contact_Email__c = 'Contact_Email',
                                       Contact_Tel__c = 'Contact_Tel'
                                      );
        insert account;  
         
         
         
        Contact contact = new Contact ( lastname ='Testing Individual',  AccountId = account.Id,Email='anilk@tecex.com',UserName__c='anilk@tecex.com',Password__c='Test@1234',Contact_status__C = 'Active');
        insert contact;   
        
        
    
     
        CPAPIGenerateAccessTokenWra outerObj = new CPAPIGenerateAccessTokenWra();
		outerObj.Username= contact.UserName__c;
		outerObj.Password= contact.Password__c;
		outerObj.UserID=contact.ID;
		   
 		string jsonReq = json.serialize(outerObj);
     
    RestRequest req1 = new RestRequest();
    RestResponse res1 = new RestResponse();
    req1.requestURI = 'https://alpha-testxyz.cs105.force.com/services/apexrest/GenerateAccessToken'; 
    req1.httpMethod = 'POST';
    req1.requestBody = Blob.valueOf(jsonReq);
    RestContext.request = req1;
    RestContext.response = res1;
     RestContext.request.addHeader('Content-Type', 'application/json');

 		Test.startTest();
        	CPAPIGenerateAccessToken.GenerateAccessToken();
       test.stopTest();
       
    }  
    static testMethod void  setUpData2(){
     
           Account account = new Account (name='Acme1', Type ='Client', 
                                       CSE_IOR__c = '0050Y000001LTZO', 
                                       Service_Manager__c = '0050Y000001LTZO', 
                                         Financial_Manager__c='0050Y000001LTZO', 
                                         Financial_Controller__c='0050Y000001LTZO', 
                                       Tax_recovery_client__c  = FALSE,
                                       Ship_From_Line_1__c = 'Ship_From_Line_1',
                                       Ship_From_Line_2__c = 'Ship_From_Line_2',
                                       Ship_From_Line_3__c = 'Ship_From_Line_3',
                                       Ship_From_City__c = 'Ship_From_City', 
                                       Ship_From_Zip__c = 'Ship_From_Zip',
                                       Ship_From_Country__c = 'Ship_From_Country' , 
                                       Ship_From_Other_notes__c = 'Ship_From_Other',
                                       Ship_From_Contact_Name__c = 'Ship_From_Contact',
                                       Ship_From_Contact_Email__c = 'Ship_From_Email',
                                       Ship_From_Contact_Tel__c = 'Ship_From_Tel',
                                       Client_Address_Line_1__c = 'Client_Address_Line_1',
                                       Client_Address_Line_2__c = 'Client_Address_Line_2',
                                       Client_Address_Line_3__c = 'Client_Address_Line_3',
                                       Client_City__c = 'Client_City',
                                       Client_Zip__c = 'Client_Zip',
                                       Client_Country__c = 'Client_Country', 
                                       Other_notes__c = 'Other_notes',
                                       Tax_Name__c = 'Tax_Name',
                                       Tax_ID__c = 'Tax_ID',
                                       Contact_Name__c = 'Contact_Name',
                                       Contact_Email__c = 'Contact_Email',
                                       Contact_Tel__c = 'Contact_Tel'
                                      );
        insert account;  
         
         
         
        Contact contact = new Contact ( lastname ='Testing Individual',  AccountId = account.Id,Email='anilk@tecex.com',UserName__c='anilk@tecex.com',Password__c='Test@1234',Contact_status__C = 'Active');
        insert contact;   
        
        
    
     
        CPAPIGenerateAccessTokenWra outerObj = new CPAPIGenerateAccessTokenWra();
		outerObj.Username= 'A@a.com';
		outerObj.Password= 'test';
		outerObj.UserID=contact.ID;
		   
 		string jsonReq = json.serialize(outerObj);
     
    RestRequest req1 = new RestRequest();
    RestResponse res1 = new RestResponse();
    req1.requestURI = 'https://alpha-testxyz.cs105.force.com/services/apexrest/GenerateAccessToken'; 
    req1.httpMethod = 'POST';
    req1.requestBody = Blob.valueOf(jsonReq);
    RestContext.request = req1;
    RestContext.response = res1;
     RestContext.request.addHeader('Content-Type', 'application/json');

 		Test.startTest();
        	CPAPIGenerateAccessToken.GenerateAccessToken();
       test.stopTest();
       
    } 
    
}