@isTest
public class cpa2ComponentController_Test{
    
    private static testMethod void  setUpData(){
        Account account = new Account (name='Acme1', Type ='Client', CSE_IOR__c = '0050Y000001LTZO', Service_Manager__c = '0050Y000001LTZO', Tax_recovery_client__c  = FALSE,
                                       Ship_From_Line_1__c = 'Ship_From_Line_1',
                                       Ship_From_Line_2__c = 'Ship_From_Line_2',
                                       Ship_From_Line_3__c = 'Ship_From_Line_3',
                                       Ship_From_City__c = 'Ship_From_City', 
                                       Ship_From_Zip__c = 'Ship_From_Zip',
                                       Ship_From_Country__c = 'Ship_From_Country' , 
                                       Ship_From_Other_notes__c = 'Ship_From_Other',
                                       Ship_From_Contact_Name__c = 'Ship_From_Contact',
                                       Ship_From_Contact_Email__c = 'Ship_From_Email',
                                       Ship_From_Contact_Tel__c = 'Ship_From_Tel',
                                       Client_Address_Line_1__c = 'Client_Address_Line_1',
                                       Client_Address_Line_2__c = 'Client_Address_Line_2',
                                       Client_Address_Line_3__c = 'Client_Address_Line_3',
                                       Client_City__c = 'Client_City',
                                       Client_Zip__c = 'Client_Zip',
                                       Client_Country__c = 'Client_Country', 
                                       Other_notes__c = 'Other_notes',
                                       Tax_Name__c = 'Tax_Name',
                                       Tax_ID__c = 'Tax_ID',
                                       Contact_Name__c = 'Contact_Name',
                                       Contact_Email__c = 'Contact_Email',
                                       Contact_Tel__c = 'Contact_Tel'
                                      );
        insert account;  
        
        
        
        Account account2 = new Account (name='Test Supplier', Type ='Supplier', ICE__c = '0050Y000001km5c', RecordTypeId = '0120Y0000006KjQQAU');
        insert account2;      
        
        Contact contact = new Contact ( lastname ='Testing Individual',  AccountId = account.Id);
        insert contact;  
        
        IOR_Price_List__c iorpl = new IOR_Price_List__c ( Client_Name__c = account.Id, Name = 'Brazil', IOR_Fee__c = 0.15, On_Charge_Mark_up__c = 0.15,
                                                         TecEx_Shipping_Fee_Markup__c =0.40, IOR_Min_Fee__c = 100,  Admin_Fee__c =100, Set_up_fee__c =100, Bank_Fees__c =200, Tax_Rate__c = 0.20,
                                                         Estimated_Customs_Brokerage__c =250, Estimated_Customs_Clearance__c =250, Estimated_Customs_Handling__c =250, Estimated_Import_License__c =250, Destination__c = 'Brazil' );
        insert iorpl; 
        
        country_price_approval__c cpa = new country_price_approval__c( name ='Brazil', Billing_term__c = 'DAP/CIF - IOR pays',  Airwaybill_Instructions__c = 'User IOR Address', Supplier__c = account2.Id, 
                                                                      In_Country_Specialist__c = '0050Y000001km5c', Shipping_Notes_New__c = 'Testing Notes');
        insert cpa;
        
        Currency_Management2__c CM = new Currency_Management2__c (Name = 'US Dollar (USD)', Currency__c = 'US Dollar (USD)', ISO_Code__c = 'USD', Conversion_Rate__c = 1);
        insert CM; 

        
        CPA_v2_0__c cpav22 = new CPA_v2_0__c(Name = 'Brazil Testing 2.0', Supplier__c = account2.Id, Service_Type__c = 'IOR/EOR', Related_Costing_CPA__c = 'a260Y00000EnDjr', Preferred_Supplier__c = TRUE,
                                            VAT_Rate__c = 0.1, Destination__c = 'Brazil',  
                                            Default_Section_1_Naming_Convention__c = 'VAT Claiming Entity (EORI) Details',
                                            Default_Section_2_Naming_Convention__c = 'VAT Claiming Entity (EORI) Details with CPA Contact Details',
                                            Default_Section_3_Naming_Convention__c = 'CPA c/o with VAT Claiming Entity (EORI) Details',
                                            Default_Section_4_Naming_Convention__c = 'VAT Claiming Entity (EORI) c/o CPA c/o with End User Details',
                                            Alternate_Section_1_Naming_Convention__c = 'VAT Claiming Entity (EORI) c/o with End User Details',
                                            Alternate_Section_2_Naming_Convention__c = 'VAT Claiming Entity (EORI) Details with CPA Contact Details',
                                            Alternate_Section_3_Naming_Convention__c = 'VAT Claiming Entity (EORI) Details',
                                            Alternate_Section_4_Naming_Convention__c = 'CPA c/o with VAT Claiming Entity (EORI) Details',
                                            Default_Section_1_Address_Line_1__c= 'Address_Line_1' ,
                                            Default_Section_1_Address_Line_2__c= 'Address_Line_2' ,
                                            Default_Section_1_Address_Line_3__c= 'Address_Line_3' ,
                                            Default_Section_1_City__c= 'City' ,
                                            Default_Section_1_Zip__c= 'Zip' ,
                                            Default_Section_1_Country__c = 'Country' , 
                                            Default_Section_1_Other__c= 'Other' ,
                                            Default_Section_1_Tax_Name__c= 'Tax_Name' ,
                                            Default_Section_1_Tax_Id__c= 'Tax_Id' ,
                                            Default_Section_1_Contact_Name__c= 'Contact_Name' ,
                                            Default_Section_1_Contact_Email__c= 'Contact_Email' ,
                                            Default_Section_1_Contact_Tel__c= 'Contact_Tel' ,
                                            Alternate_Section_1_Address_Line_1__c= 'Address_Line_1' ,
                                            Alternate_Section_1_Address_Line_2__c= 'Address_Line_2' ,
                                            Alternate_Section_1_Address_Line_3__c= 'Address_Line_3' ,
                                            Alternate_Section_1_City__c= 'City' ,
                                            Alternate_Section_1_Zip__c= 'Zip' ,
                                            Alternate_Section_1_Country__c = 'Country' ,
                                            Alternate_Section_1_Other__c = 'Other' ,
                                            Alternate_Section_1_Tax_Name__c = 'Tax_Name' ,
                                            Alternate_Section_1_Tax_Id__c = 'Tax_Id' ,
                                            Alternate_Section_1_Contact_Name__c= 'Contact_Name' ,
                                            Alternate_Section_1_Contact_Email__c= 'Contact_Email' ,
                                            Alternate_Section_1_Contact_Tel__c= 'Contact_Tel', 
                                            Default_Section_2_Address_Line_1__c= 'Address_Line_1' ,
                                            Default_Section_2_Address_Line_2__c= 'Address_Line_2' ,
                                            Default_Section_2_Address_Line_3__c= 'Address_Line_3' ,
                                            Default_Section_2_City__c= 'City' ,
                                            Default_Section_2_Zip__c= 'Zip' ,
                                            Default_Section_2_Country__c = 'Country' , 
                                            Default_Section_2_Other__c= 'Other' ,
                                            Default_Section_2_Tax_Name__c= 'Tax_Name' ,
                                            Default_Section_2_Tax_Id__c= 'Tax_Id' ,
                                            Default_Section_2_Contact_Name__c= 'Contact_Name' ,
                                            Default_Section_2_Contact_Email__c= 'Contact_Email' ,
                                            Default_Section_2_Contact_Tel__c= 'Contact_Tel' ,
                                            Alternate_Section_2_Address_Line_1__c= 'Address_Line_1' ,
                                            Alternate_Section_2_Address_Line_2__c= 'Address_Line_2' ,
                                            Alternate_Section_2_Address_Line_3__c= 'Address_Line_3' ,
                                            Alternate_Section_2_City__c= 'City' ,
                                            Alternate_Section_2_Zip__c= 'Zip' ,
                                            Alternate_Section_2_Country__c = 'Country' ,
                                            Alternate_Section_2_Other__c = 'Other' ,
                                            Alternate_Section_2_Tax_Name__c = 'Tax_Name' ,
                                            Alternate_Section_2_Tax_Id__c = 'Tax_Id' ,
                                            Alternate_Section_2_Contact_Name__c= 'Contact_Name' ,
                                            Alternate_Section_2_Contact_Email__c= 'Contact_Email',
                                            Alternate_Section_2_Contact_Tel__c= 'Contact_Tel',
                                            Default_Section_3_Address_Line_1__c= 'Address_Line_1' ,
                                            Default_Section_3_Address_Line_2__c= 'Address_Line_2' ,
                                            Default_Section_3_Address_Line_3__c= 'Address_Line_3' ,
                                            Default_Section_3_City__c= 'City' ,
                                            Default_Section_3_Zip__c= 'Zip' ,
                                            Default_Section_3_Country__c = 'Country' , 
                                            Default_Section_3_Other__c= 'Other' ,
                                            Default_Section_3_Tax_Name__c= 'Tax_Name' ,
                                            Default_Section_3_Tax_Id__c= 'Tax_Id' ,
                                            Default_Section_3_Contact_Name__c= 'Contact_Name' ,
                                            Default_Section_3_Contact_Email__c= 'Contact_Email' ,
                                            Default_Section_3_Contact_Tel__c= 'Contact_Tel' ,
                                            Alternate_Section_3_Address_Line_1__c= 'Address_Line_1' ,
                                            Alternate_Section_3_Address_Line_2__c= 'Address_Line_2' ,
                                            Alternate_Section_3_Address_Line_3__c= 'Address_Line_3' ,
                                            Alternate_Section_3_City__c= 'City' ,
                                            Alternate_Section_3_Zip__c= 'Zip' ,
                                            Alternate_Section_3_Country__c = 'Country' ,
                                            Alternate_Section_3_Other__c = 'Other' ,
                                            Alternate_Section_3_Tax_Name__c = 'Tax_Name' ,
                                            Alternate_Section_3_Tax_Id__c = 'Tax_Id' ,
                                            Alternate_Section_3_Contact_Name__c= 'Contact_Name' ,
                                            Alternate_Section_3_Contact_Email__c= 'Contact_Email' ,
                                            Alternate_Section_3_Contact_Tel__c= 'Contact_Tel',
                                            Default_Section_4_Address_Line_1__c= 'Address_Line_1' ,
                                            Default_Section_4_Address_Line_2__c= 'Address_Line_2' ,
                                            Default_Section_4_Address_Line_3__c= 'Address_Line_3' ,
                                            Default_Section_4_City__c= 'City' ,
                                            Default_Section_4_Zip__c= 'Zip' ,
                                            Default_Section_4_Country__c = 'Country' , 
                                            Default_Section_4_Other__c= 'Other' ,
                                            Default_Section_4_Tax_Name__c= 'Tax_Name' ,
                                            Default_Section_4_Tax_Id__c= 'Tax_Id' ,
                                            Default_Section_4_Contact_Name__c= 'Contact_Name' ,
                                            Default_Section_4_Contact_Email__c= 'Contact_Email' ,
                                            Default_Section_4_Contact_Tel__c= 'Contact_Tel' ,
                                            Alternate_Section_4_Address_Line_1__c= 'Address_Line_1' ,
                                            Alternate_Section_4_Address_Line_2__c= 'Address_Line_2' ,
                                            Alternate_Section_4_Address_Line_3__c= 'Address_Line_3' ,
                                            Alternate_Section_4_City__c= 'City' ,
                                            Alternate_Section_4_Zip__c= 'Zip' ,
                                            Alternate_Section_4_Country__c = 'Country' ,
                                            Alternate_Section_4_Other__c = 'Other' ,
                                            Alternate_Section_4_Tax_Name__c = 'Tax_Name' ,
                                            Alternate_Section_4_Tax_Id__c = 'Tax_Id' ,
                                            Alternate_Section_4_Contact_Name__c= 'Contact_Name' ,
                                            Alternate_Section_4_Contact_Email__c= 'Contact_Email' ,
                                            Alternate_Section_4_Contact_Tel__c= 'Contact_Tel' ,
                                            VAT_Reclaim_Destination__c = FALSE                                           
                                            
                                           );
        insert cpav22;
        
        List<CPA_Costing__c> costs2 = new List<CPA_Costing__c>();
        
        
        costs2.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav22.Id, Applied_to__c = 'Shipment value', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'IOR', Min__c = 375, Max__c = 14000, Conditional_value__c = 'Shipment value', Condition__c = 'Between', Floor__c = 0, Ceiling__c = 10000, Currency__c = 'US Dollar (USD)'));
        costs2.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav22.Id, Applied_to__c = 'CIF value', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'IOR', Max__c = 50000, Conditional_value__c = 'Shipment value', Condition__c = 'Between', Floor__c = 0, Ceiling__c = 10000, Currency__c = 'US Dollar (USD)'));
        costs2.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav22.Id, Applied_to__c = 'Total Taxes', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'IOR', Min__c = 14000, Conditional_value__c = 'Shipment value', Condition__c = '<', Floor__c = 20000,  Currency__c = 'US Dollar (USD)'));
        costs2.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav22.Id, Applied_to__c = 'Shipment value', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'IOR', Conditional_value__c = 'CIF value', Condition__c = '>=', Floor__c = 5000, Currency__c = 'US Dollar (USD)'));
        
        costs2.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav22.Id, Applied_to__c = 'Shipment value', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'IOR', Min__c = 375, Max__c = 14000, Currency__c = 'US Dollar (USD)', Variable_threshold__c = 1000));
        costs2.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav22.Id, Applied_to__c = 'CIF value', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'IOR', Max__c = 50000, Currency__c = 'US Dollar (USD)', Variable_threshold__c = 5000));
        costs2.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav22.Id, Applied_to__c = 'Total Taxes', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'IOR', Min__c = 14000, Currency__c = 'US Dollar (USD)', Variable_threshold__c = 500));
        costs2.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav22.Id, Applied_to__c = 'Shipment value', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'IOR', Currency__c = 'US Dollar (USD)', Variable_threshold__c = 9000));
        
        
        costs2.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav22.Id, Applied_to__c = 'Chargeable weight', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'IOR', Min__c = 375, Max__c = 500, Conditional_value__c = 'Chargeable weight', Condition__c = 'Between', Floor__c = 0, Ceiling__c = 500, Currency__c = 'US Dollar (USD)'));
        costs2.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav22.Id, Applied_to__c = '# packages', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'IOR', Max__c = 200, Conditional_value__c = '# of packages', Condition__c = '>', Floor__c = 5,  Currency__c = 'US Dollar (USD)'));
        costs2.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav22.Id, Applied_to__c = '# Parts (line items)', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'IOR', Min__c = 100, Currency__c = 'US Dollar (USD)'));
        costs2.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav22.Id, Applied_to__c = '# Unique HS Codes', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'IOR', Currency__c = 'US Dollar (USD)'));
        
        costs2.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav22.Id, Applied_to__c = 'Chargeable weight', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'IOR', Min__c = 375, Max__c = 500, Currency__c = 'US Dollar (USD)', Variable_threshold__c = 100));
        costs2.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav22.Id, Applied_to__c = '# packages', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'IOR', Max__c = 200, Currency__c = 'US Dollar (USD)', Variable_threshold__c = 4));
        costs2.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav22.Id, Applied_to__c = '# Parts (line items)', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'IOR', Min__c = 100, Currency__c = 'US Dollar (USD)', Variable_threshold__c = 2));
        costs2.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav22.Id, Applied_to__c = '# Final Deliveries', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'IOR', Currency__c = 'US Dollar (USD)',  Variable_threshold__c = 2));
        
        costs2.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav22.Id, Cost_Type__c = 'Fixed',  Amount__c = 800, IOR_EOR__c = 'IOR', Min__c = 375, Max__c = 14000, Currency__c = 'US Dollar (USD)'));
        costs2.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav22.Id, Cost_Type__c = 'Fixed',  Amount__c = 900, IOR_EOR__c = 'IOR', Max__c = 1000, Currency__c = 'US Dollar (USD)'));
        costs2.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav22.Id, Cost_Type__c = 'Fixed',  Amount__c = 100, IOR_EOR__c = 'IOR', Min__c = 200, Currency__c = 'US Dollar (USD)'));
        costs2.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav22.Id, Cost_Type__c = 'Fixed',  Amount__c = 350, IOR_EOR__c = 'IOR', Currency__c = 'US Dollar (USD)'));
        
        
        costs2.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav22.Id, Applied_to__c = 'Shipment value', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'EOR', Min__c = 375, Max__c = 14000, Conditional_value__c = 'Shipment value', Condition__c = 'Between', Floor__c = 0, Ceiling__c = 10000, Currency__c = 'US Dollar (USD)'));
        costs2.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav22.Id, Applied_to__c = 'CIF value', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'EOR', Max__c = 50000, Conditional_value__c = 'Shipment value', Condition__c = 'Between', Floor__c = 0, Ceiling__c = 10000, Currency__c = 'US Dollar (USD)'));
        costs2.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav22.Id, Applied_to__c = 'Total Taxes', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'EOR', Min__c = 14000, Conditional_value__c = 'Shipment value', Condition__c = '<', Floor__c = 20000,  Currency__c = 'US Dollar (USD)'));
        costs2.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav22.Id, Applied_to__c = 'Shipment value', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'EOR', Conditional_value__c = 'Shipment value', Condition__c = '>', Floor__c = 20000, Currency__c = 'US Dollar (USD)'));
        
        costs2.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav22.Id, Applied_to__c = 'Shipment value', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'EOR', Min__c = 375, Max__c = 14000, Currency__c = 'US Dollar (USD)', Variable_threshold__c = 1000));
        costs2.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav22.Id, Applied_to__c = 'CIF value', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'EOR', Max__c = 50000, Currency__c = 'US Dollar (USD)', Variable_threshold__c = 5000));
        costs2.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav22.Id, Applied_to__c = 'Total Taxes', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'EOR', Min__c = 14000, Currency__c = 'US Dollar (USD)', Variable_threshold__c = 500));
        costs2.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav22.Id, Applied_to__c = 'Shipment value', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'EOR', Currency__c = 'US Dollar (USD)', Variable_threshold__c = 9000));
        
        
        costs2.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav22.Id, Applied_to__c = 'Chargeable weight', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'EOR', Min__c = 375, Max__c = 500, Currency__c = 'US Dollar (USD)'));
        costs2.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav22.Id, Applied_to__c = '# packages', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'EOR', Max__c = 200, Currency__c = 'US Dollar (USD)'));
        costs2.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav22.Id, Applied_to__c = '# Parts (line items)', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'EOR', Min__c = 100, Currency__c = 'US Dollar (USD)'));
        costs2.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav22.Id, Applied_to__c = '# Unique HS Codes', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'EOR', Currency__c = 'US Dollar (USD)'));
        
        costs2.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav22.Id, Applied_to__c = 'Chargeable weight', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'EOR', Min__c = 375, Max__c = 500, Currency__c = 'US Dollar (USD)', Variable_threshold__c = 100));
        costs2.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav22.Id, Applied_to__c = '# packages', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'EOR', Max__c = 200, Currency__c = 'US Dollar (USD)', Variable_threshold__c = 4));
        costs2.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav22.Id, Applied_to__c = '# Parts (line items)', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'EOR', Min__c = 100, Currency__c = 'US Dollar (USD)', Variable_threshold__c = 2));
        costs2.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav22.Id, Applied_to__c = '# Final Deliveries', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'EOR', Currency__c = 'US Dollar (USD)',  Variable_threshold__c = 2));
        
        costs2.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav22.Id, Cost_Type__c = 'Fixed',  Amount__c = 800, IOR_EOR__c = 'EOR', Min__c = 375, Max__c = 14000, Currency__c = 'US Dollar (USD)'));
        costs2.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav22.Id, Cost_Type__c = 'Fixed',  Amount__c = 900, IOR_EOR__c = 'EOR', Max__c = 1000, Currency__c = 'US Dollar (USD)'));
        costs2.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav22.Id, Cost_Type__c = 'Fixed',  Amount__c = 100, IOR_EOR__c = 'EOR', Min__c = 200, Currency__c = 'US Dollar (USD)'));
        costs2.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav22.Id, Cost_Type__c = 'Fixed',  Amount__c = 350, IOR_EOR__c = 'EOR', Currency__c = 'US Dollar (USD)'));
        
        costs2.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav22.Id, Applied_to__c = 'Anil', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'IOR', Min__c = 375, Max__c = 500, Currency__c = 'US Dollar (USD)', Variable_threshold__c = null));
        costs2.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav22.Id, Applied_to__c = 'Anil', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'IOR', Max__c = 200, Currency__c = 'US Dollar (USD)', Variable_threshold__c = null));
        costs2.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav22.Id, Applied_to__c = 'Anil', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'IOR', Min__c = 100, Currency__c = 'US Dollar (USD)', Variable_threshold__c = null));
        costs2.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav22.Id, Applied_to__c = 'Anil', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'IOR', Currency__c = 'US Dollar (USD)',  Variable_threshold__c = null));
        
        
        insert costs2;

        
       
        
        
        
        
        test.startTest();
        
       cpa2ComponentController.getCpa (cpav22.id);
       cpa2ComponentController.getModifiedBy(cpav22.id);
       cpa2ComponentController.getCreatedBy(cpav22.id);
       cpa2ComponentController.getLabels(cpav22.id);
       cpa2ComponentController.UpdateCpa(cpav22);
       
        
        
        
        
        test.stopTest();
        
    } 
    
    
    
   
    
    
}