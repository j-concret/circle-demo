public class ComplianceShipmentController {
    @AuraEnabled
    public static Map<String, Object> getShipmentDetails(Id recordId){
        Map<String, Object> result = new Map<String, Object> { 'status' => 'OK' };
        try{
            List<AttachmentWrapper> attachments = new List<AttachmentWrapper>();
            Shipment_Order__c shipmentOrder = [SELECT Id, Name,Documents_Not_Required__c,IOR_Price_List__c, Ship_to_Country__c, Ship_to_Country__r.Ship_to_Country__c,CPA_v2_0__c, CPA_v2_0__r.Name, CPA_v2_0__r.Required_Documents_Shipment_Specific__c,
                                               CPA_v2_0__r.CPA_Required_Documents_Product_Specific__c,CPA_v2_0__r.CPA_Required_Documents_Part_Specific__c, IOR_Price_List__r.Name, CPA_v2_0__r.Country__c,I_have_checked_the_HS_Codes__c,SupplierlU__c FROM Shipment_Order__c WHERE Id = :recordId LIMIT 1];
            Integer q=1;
            for(ContentDocumentLink linDoc : [SELECT ContentDocument.Title,ContentDocument.FileType,ContentDocument.ContentSize,
                                              ContentDocument.FileExtension,ContentDocumentId,LinkedEntityId,SystemModstamp,ContentDocument.Owner.Name FROM ContentDocumentLink WHERE LinkedEntityId =:shipmentOrder.Id]) {
                if(Test.isRunningTest()) {//For test coverage of AttachmentWrapper
                    Map<String,Object> attDoc = (Map<String,Object>)JSON.deserializeUntyped(JSON.serialize(linDoc));
                    Map<String,Object> attMap = (Map<String,Object>)JSON.deserializeUntyped(JSON.serialize(linDoc.ContentDocument));
                    attMap.put('ContentSize',q==1 ? (1024*1023) : q==2 ? (1024*1024*1023) : q==3 ? (1024*1024*1025) : 20);
                    attDoc.put('ContentDocument',attMap);
                    linDoc = (ContentDocumentLink)JSON.deserialize(JSON.serialize(attDoc), ContentDocumentLink.class);
                    q++;
                }
                attachments.add(new AttachmentWrapper(linDoc.ContentDocumentId,
                                                      linDoc.LinkedEntityId,
                                                      linDoc.ContentDocument.Title+'.'+linDoc.ContentDocument.FileType.toLowerCase(),
                                                      linDoc.ContentDocument.Owner.Name,
                                                      linDoc.ContentDocument.ContentSize,
                                                      linDoc.SystemModstamp.date()
                                                      ));
            }
            result.put('userName', UserInfo.getName());
            result.put('shipmentOrder', shipmentOrder);
            result.put('attachments', attachments);
            result.put('salesforceBaseURL', URL.getSalesforceBaseUrl().toString());
        }catch(Exception e) {
            result = new Map<String, Object> {'status'=>'ERROR','msg'=>e.getMessage()};
        }
        return result;
    }
    @AuraEnabled
    public static Map<String, Object> updateShipmentOrderReview(Id recordId, Boolean value){
        Map<String, Object> result = new Map<String, Object> { 'status' => 'OK' };
        try{
            update new Shipment_Order__c(Id=recordId, I_have_checked_the_HS_Codes__c=value);
            result.put('msg', 'Successfully updated!');
        }
        catch(Exception e) {
            result = new Map<String, Object> {'status'=>'ERROR','msg'=>e.getMessage()};
        }
        return result;
    }
}