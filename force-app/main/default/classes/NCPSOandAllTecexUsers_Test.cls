@isTest
public class NCPSOandAllTecexUsers_Test {
    public static User createUser(String roleId, String profId, String firstName, String lastName){
        User tempUser = prepareUser(roleId, profId, firstName, lastName);
        return tempUser;
    }

    private static User prepareUser(String roleId, String profId, String firstName, String lastName) {  
		
        String orgId = UserInfo.getOrganizationId();  
        String dateString =   
        String.valueof(Datetime.now()).replace(' ','').replace(':','').replace('-','');  
        Integer randomInt = Integer.valueOf(math.rint(math.random()*1000000));  
        String uniqueName = orgId + dateString + randomInt;  
        User tempUser = new User(  
            FirstName = firstName,  
            LastName = lastName,  
            email = uniqueName + '@sfdc' + orgId + '.org',  
            Username = uniqueName + '@sfdc' + orgId + '.org',  
            EmailEncodingKey = 'ISO-8859-1',  
            Alias = uniqueName.substring(18, 23),  
            TimeZoneSidKey = 'America/Los_Angeles',  
            LocaleSidKey = 'en_US',  
            LanguageLocaleKey = 'en_US',  
            ProfileId = profId,
            isactive=true
        );    
        if( String.isBlank(roleId) == false ){
            tempUser.UserRoleId = roleId;
        }
        return tempUser;  
    }

    public static User createCommunityUser(String contactId, String profId, String firstName, String lastName) {
        User tempUser = prepareUser('', profId, firstName, lastName);
        tempUser.ContactId = contactId;
        return tempUser;
    }

    public static Account createAccount(String Name){
        Account portalAccount = new Account(name = Name );
        return portalAccount;
    }
    public static Contact createContact(String firstName, String lastName, String email, String accountId){
        Contact portalContact = new contact(
            FirstName = firstName, 
            LastName = lastName, 
            Email = email, 
            AccountId = accountId
        );
        return portalContact;
    }
    
@isTest
  public static void testGetRelatedContacts(){
   
      
       Profile prof = [select id from profile where name ='System Administrator' limit 1];
      UserRole r = new UserRole(DeveloperName = 'MyCustomRole', Name = 'My Role');
        insert r;
       User user = new User();
        user.firstName = 'Firstname1@';
        user.lastName = 'lastname1@';
        user.profileId = prof.id;
        user.email = '123_1@test.com';
        user.username = '123_1@tecex.com';
        user.EmailEncodingKey = 'ISO-8859-1';
        user.Alias = 'Alias';
        user.TimeZoneSidKey = 'America/Los_Angeles';
        user.LanguageLocaleKey = 'en_US';
        user.LocaleSidKey = 'en_US';
        user.UserRoleId = r.Id;
        insert user;
      
       User user1 = new User();
        user1.firstName = 'Fname2';
        user1.lastName = 'lame2';
        user1.profileId = prof.id;
        user1.email = '123_1@test.com';
        user1.username = '123_12@tecex.com';
        user1.EmailEncodingKey = 'ISO-8859-1';
        user1.Alias = 'Ali34';
        user1.TimeZoneSidKey = 'America/Los_Angeles';
        user1.LanguageLocaleKey = 'en_US';
        user1.LocaleSidKey = 'en_US';
        user1.stand_by_user__C=user.id;
      user1.UserRoleId = r.Id;
        insert user1;
      
      System.runAs(user1){
       Access_Token__c at = new Access_Token__c(
      AccessToken_encoded__c='zmM0MDcxOTItZTJiZi00YWRhLTlkZGQtZGZhMmUxM2FiYTlj',
      Access_Token__c='cc407192-e2bf-4ada-9ddd-dfa2e13aba9c',
      Status__c='Active'
    );
    insert at;
      OutOfOffice OutOfOffice1 = new OutOfOffice();
      
      OutOfOffice1.UserId =user1.Id;
      OutOfOffice1.IsEnabled = true;
      OutOfOffice1.StartDate=system.now();
      OutOfOffice1.EndDate=system.now()+1;
      OutOfOffice1.message='Iam out of office now';
      
      insert OutOfOffice1;
       Id accountRecTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Freight Supplier').getRecordTypeId();
    
	 Account account = new Account (name='TecEx Prospective Client', Type ='Supplier', 
                                       CSE_IOR__c = '0050Y000001km5cQAA', 
                                       Service_Manager__c = '0050Y000001km5cQAA', 
                                         Financial_Manager__c='0050Y000001km5cQAA', 
                                         Financial_Controller__c='0050Y000001km5cQAA', 
                                       Tax_recovery_client__c  = FALSE,
                                       Contact_Name__c = 'Contact_Name',
                                       Contact_Email__c = 'Contact_Email',
                                       Contact_Tel__c = 'Contact_Tel',RecordTypeId = accountRecTypeId
                                      );
        insert account;  
         
         
        Account account2 = new Account (name='Test Supplier', Type ='Supplier', ICE__c = '0050Y000001km5c', RecordTypeId = '0120Y0000006KjQQAU');
        insert account2;        
         
        Contact contact = new Contact ( lastname ='Testing Individual',Client_Notifications_Choice__c='Opt-In',  AccountId = account.Id);
        insert contact;  
        
        Contact contact2 = new Contact ( lastname ='Testing supplier',Client_Notifications_Choice__c='Opt-In',  AccountId = account2.Id);
        insert contact2; 
        
         IOR_Price_List__c iorpl = new IOR_Price_List__c ( Client_Name__c = account.Id, Name = 'Brazil', IOR_Fee__c = 0.15, On_Charge_Mark_up__c = 0.15,
                                                         TecEx_Shipping_Fee_Markup__c =0.40, IOR_Min_Fee__c = 100,  Admin_Fee__c =100, Set_up_fee__c =100, Bank_Fees__c =200, Tax_Rate__c = 0.20,
                                                         Estimated_Customs_Brokerage__c =250, Estimated_Customs_Clearance__c =250, Estimated_Customs_Handling__c =250, Estimated_Import_License__c =250, Destination__c = 'Brazil' );
        insert iorpl; 
       
        country_price_approval__c cpa = new country_price_approval__c( name ='Brazil', Billing_term__c = 'DAP/CIF - IOR pays',  Airwaybill_Instructions__c = 'User IOR Address', Supplier__c = account2.Id, 
                                                                      In_Country_Specialist__c = '0050Y000001km5c', Clearance_Destination__c = 'Brazil', Preferred_Supplier__c = TRUE, Destination__c = 'Brazil' );
        insert cpa;
       
         Currency_Management2__c CM = new Currency_Management2__c (Name = 'US Dollar (USD)', Currency__c = 'US Dollar (USD)', ISO_Code__c = 'USD', Conversion_Rate__c = 1);
        insert CM; 
        
         Country__c country = new Country__c(Brokerage_Costs__c = 618, CIF_Adjustment_Factor__c = 0.035, CIF_Absolute_value_adjustment__c = 200, Clearance_Costs__c = 488, 
                                            Country__c = 'Brazil', Name = 'Brazil', Handling_Costs__c = 1180, License_Permit_Costs__c = 0);
        insert country;
        
        Tax_Structure_Per_Country__c tax = new Tax_Structure_Per_Country__c(Applied_to_Value__c = 'CIF', Country__c = country.Id, Default_Duty_Rate__c = 0.5625 , 
                                                                            Order_Number__c = '1', Part_Specific__c = TRUE, Name = 'II', Tax_Type__c = 'Duties');
        insert tax;
        
        
        
        CPA_v2_0__c RelatedCPA = new CPA_v2_0__c(Name = 'Brazil', Country_Applied_Costings__c=true );
        insert RelatedCPA;
       
        system.debug('cpav21-->'+RelatedCPA);
       
       
       CPA_v2_0__c cpav2 = new CPA_v2_0__c(Name = 'Brazil Testing 2.0', Supplier__c = account2.Id, Service_Type__c = 'IOR/EOR', Related_Costing_CPA__c = 'a260Y00000EnDjr', Preferred_Supplier__c = TRUE,
                                            VAT_Rate__c = 0.1, Destination__c = 'Brazil',  
                                            VAT_Reclaim_Destination__c = FALSE,
                                            Final_Destination__c ='Brazil',
                                            Country__c = country.id
                                            
                                           );
        insert cpav2;
       
        List<CPARules__c> CPARules = new List<CPARules__c>();
          CPARules.add(new CPARules__c(Country__c = 'Brazil',Criteria__c ='Default', Chargable_weight__c = 'NONE', Courier_Responsibility__c = 'NONE',   ECCN_No__c = 'NONE', NL_Product_User__c = 'NONE', Number_of_final_deliveries__c = 'NONE', ShipmentValue__c ='NONE', Sum_of_quantity_of_matched_products__c = 'NONE', Manufacturer__c = 'NONE', DefaultCPA2__c = cpav2.Id, CPA2__c =  cpav2.Id));
        insert CPARules;

 		List<CPA_Costing__c> costs = new List<CPA_Costing__c>();
           costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'Shipment value', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'IOR', Min__c = 375, Max__c = 14000, Conditional_value__c = 'Shipment value', Condition__c = 'Between', Floor__c = 0, Ceiling__c = 10000, Currency__c = 'US Dollar (USD)'));
          costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'Anil', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'IOR', Currency__c = 'US Dollar (USD)',Variable_threshold__c = null));
        
        insert costs;
       
        Account acc1 = createAccount('Test Account');
        insert acc1;
        
       Shipment_Order__c shipmentOrder = new Shipment_Order__c(Account__c = account.id, Shipment_Value_USD__c =10000, CPA_v2_0__c = cpav2.Id,Shipping_Status__c   ='Shipment Abandoned',
                                                                Service_Manager__c = '0050Y000001km5cQAA',Client_Contact_for_this_Shipment__c = contact.Id, Who_arranges_International_courier__c ='Client', Tax_Treatment__c ='DAP/CIF - IOR pays', Ship_to_Country__c = cpa.Id,
                                                                Destination__c = 'Brazil', Service_Type__c = 'IOR', IOR_Price_List__c = iorpl.Id, of_Unique_Line_Items__c = 10, Total_Taxes__c = 1500, of_Line_Items__c= 10, Chargeable_Weight__c = 200, of_packages__c = 20, Final_Deliveries_New__c = 10,
                                                                CI_Admin_Fee__c  =100, CI_Bank_Fees__c  = 100, CI_Finance_Fee__c  = 100, CI_Insurance_Fee_USD__c  = 100, CI_International_Delivery_Fee__c  = 100, CI_IOR_and_Import_Compliance_Fee_USD__c  = 100, 
                                                                CI_Miscellaneous_Fee__c  = 100, CI_Recharge_Tax_and_Duty__c  = 100, CI_Total_Clearance_Costs__c  = 100, CI_Total_Customs_Brokerage_Cost__c  = 100, CI_Total_Handling_Cost__c  = 100, 
                                                                CI_Total_Licence_Cost__c  = 100, FC_Admin_Fee__c  = 100, FC_Bank_Fees__c  = 100, FC_Finance_Fee__c  = 100, FC_Insurance_Fee_USD__c   = 100, FC_International_Delivery_Fee__c  = 100, 
                                                                FC_IOR_and_Import_Compliance_Fee_USD__c  = 100, FC_Miscellaneous_Fee__c  = 100, FC_Recharge_Tax_and_Duty__c  = 100, FC_Total_Clearance_Costs__c  = 100, 
                                                                FC_Total_Customs_Brokerage_Cost__c  = 100, FC_Total_Handling_Costs__c  = 100, FC_Total_License_Cost__c  = 100, Total_CIF_Duties__c = 0, Total_FOB_Duties__c = 0, Actual_Admin_Fee__c = 15, 
                                                                Actual_Bank_Fees__c = 15, Actual_Exchange_gain_loss_on_payment__c = 15, Actual_Finance_Fee__c = 15, Actual_Insurance_Fee_USD__c = 15, Actual_International_Delivery_Fee__c = 15, 
                                                                Actual_Miscellaneous_Fee__c = 15, Actual_Total_Clearance_Costs__c = 15, Actual_Total_Customs_Brokerage_Cost__c = 15, Actual_Total_Handling_Costs__c = 15, Actual_Total_IOR_EOR__c = 15, 
                                                                Actual_Total_License_Cost__c = 15, Actual_Total_Tax_and_Duty__c = 15 );
         
      insert  shipmentOrder;
          
     Id profileId = [Select Id From Profile Where Name ='TecEx Customer'].Id;
        
        Contact con1 = createContact('test','Contact','test@gmail.com',account.Id);
        insert con1;
        User usr = createCommunityUser(con1.Id,profileId,'Amit','Singh');
        insert usr; 

    Map<String,String> body = new Map<String,String>{'Accesstoken'=>at.Access_Token__c,'AccountID'=>account.Id,'SOID'=>shipmentOrder.Id};
    RestRequest req = new RestRequest();
    RestResponse res = new RestResponse();
    req.requestURI = '/services/apexrest/SOandAllTecexUsers/'; //Request URL
    req.httpMethod = 'POST';
    req.requestBody = Blob.valueOf(JSON.serialize(body));

    RestContext.request = req;
    RestContext.response= res;

    Test.startTest();
      NCPSOandAllTecexUsers.getRelatedContacts();      
    Test.stopTest();
      }
  }

  @isTest
  public static void testNegativeGetRelatedContacts(){
    Map<String,String> body = new Map<String,String>();//{'Accesstoken'=>at.Access_Token__c,'AccountID'=>acc.Id};
    RestRequest req = new RestRequest();
    RestResponse res = new RestResponse();
    req.requestURI = '/services/apexrest/SOandAllTecexUsers/'; //Request URL
    req.httpMethod = 'POST';
    req.requestBody = Blob.valueOf(JSON.serialize(body));

    RestContext.request = req;
    RestContext.response= res;

    Test.startTest();
      NCPSOandAllTecexUsers.getRelatedContacts(); 
    Test.stopTest();

  }

  @isTest
  public static void testNegative2GetRelatedContacts(){
    Access_Token__c at = new Access_Token__c(
      AccessToken_encoded__c='zmM1MDcxOTItZTJiZi00YWRhLTlkZGQtZGZhMmUxM2FiYTlj',
      Access_Token__c='cc497192-e2bf-4ada-9ddd-dfa2e13aba9c',
      Status__c='Expired'
    );
    insert at;

    Account acc = new Account (name='Test Account', Type ='Supplier');
    insert acc;

    Map<String,String> body = new Map<String,String>{'Accesstoken'=>at.Access_Token__c,'AccountID'=>acc.Id};
    RestRequest req = new RestRequest();
    RestResponse res = new RestResponse();
    req.requestURI = '/services/apexrest/SOandAllTecexUsers/'; //Request URL
    req.httpMethod = 'POST';
    req.requestBody = Blob.valueOf(JSON.serialize(body));

    RestContext.request = req;
    RestContext.response= res;

    Test.startTest();
      NCPSOandAllTecexUsers.getRelatedContacts(); 
    Test.stopTest();

  }
}