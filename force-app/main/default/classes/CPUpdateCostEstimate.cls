@RestResource(urlMapping='/UpdateCostEstimate/*')
global class CPUpdateCostEstimate {
    @Httpput
    global static void CPUpdateCostEstimates(){
        
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        Blob body = req.requestBody;
    	res.addHeader('Content-Type', 'application/json');
        String requestString = body.toString();
        CPUpdateCostEstimateWrapper rw = (CPUpdateCostEstimateWrapper)JSON.deserialize(requestString,CPUpdateCostEstimateWrapper.class);
     	Shipment_Order__c AB =[Select id,Account__C,Li_ion_Batteries__c,Type_of_Goods__c,Lithium_Battery_Types__c,Client_Reference__c,Client_Reference_2__c,Shipment_Value_USD__c,Request_Email_Estimate__c,Email_Estimate_To__c,Who_arranges_International_courier__c  from Shipment_Order__c where Id =:rw.Id];
       system.debug('rw.Shipment_Value_USD -->'+rw.Shipment_Value_USD);
        system.debug('AB.Shipment_Value_USD__c -->'+AB.Shipment_Value_USD__c);
        
        try {
             If((rw.Shipment_Value_USD ==AB.Shipment_Value_USD__c) && (Boolean.valueof(rw.request_EmailEstimate) == TRUE)  ){
                 system.debug('In If block');
           
            AB.Request_Email_Estimate__c=Boolean.valueof(rw.request_EmailEstimate);
            AB.Email_Estimate_To__c=rw.Email_Estimate_to;
         
            Update AB;
                

            
            String ASD = 'Success-CostEstimate Email Sent';	
            res.responseBody = Blob.valueOf(JSON.serializePretty(ASD));
            res.statusCode = 200;
              	API_Log__c Al = New API_Log__c();
                Al.Account__c=AB.Account__c;
                Al.EndpointURLName__c='UpdateCostEstimate';
                Al.Response__c='Success-CostEstimate Email Sent';
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al; 
            }
            else{
                 system.debug('In else block');
            AB.Li_ion_Batteries__c = Rw.LionBatteries;
            AB.Type_of_Goods__c= rw.TypeOfGoods;
            AB.Lithium_Battery_Types__c = rw.LionBatteriestype;
            AB.Client_Reference__c = rw.Reference1;
            AB.Client_Reference_2__c=rw.Reference2;
            AB.Shipment_Value_USD__c=rw.Shipment_Value_USD;
            AB.Request_Email_Estimate__c=Boolean.valueof(rw.request_EmailEstimate);
            AB.Email_Estimate_To__c=rw.Email_Estimate_to;
            AB.Who_arranges_International_courier__c=rw.Courier_responsibility;
            AB.Client_PO_ReferenceNumber__c=rw.PONumber;
           
           
            
            Update AB;
                

            
            String ASD = 'Record Succesfully updated';	
            res.responseBody = Blob.valueOf(JSON.serializePretty(ASD));
            res.statusCode = 200;
              	API_Log__c Al = New API_Log__c();
                Al.Account__c=AB.Account__c;
                Al.EndpointURLName__c='UpdateCostEstimate';
                Al.Response__c='Record Successfully updated';
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al; 
            }
            }
        	catch (Exception e){
                
              	String ErrorString ='Something went wrong while updating cost estimate, please contact support_SF@tecex.com';
               	res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
            	res.statusCode = 500;
                API_Log__c Al = New API_Log__c();
                Al.Account__c=AB.Account__c;
                Al.EndpointURLName__c='UpdateCostEstimate';
                Al.Response__c=e.getMessage()+' at line number: '+e.getLineNumber();
                Al.StatusCode__c=string.valueof(res.statusCode);
              	Insert Al;

            
        	}
        
        
        
        }

}