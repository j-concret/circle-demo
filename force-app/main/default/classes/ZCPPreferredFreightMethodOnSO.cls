@RestResource(urlMapping='/ZCPPreferredFreightMethod/*')
Global class ZCPPreferredFreightMethodOnSO {
    
     @Httppost
      global static void ZCPPreferredFreightMethodOnSO(){
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        Blob body = req.requestBody;
    	res.addHeader('Content-Type', 'application/json');
        String requestString = body.toString();
        ZCPPreferredFreightMethodOnSOwra rw  = (ZCPPreferredFreightMethodOnSOwra)JSON.deserialize(requestString,ZCPPreferredFreightMethodOnSOwra.class);
          Try{
              Access_token__c At =[select status__c,Access_Token__c from Access_token__c where Access_Token__c=:rw.Accesstoken ];
              if( at.status__C =='Active' ){
                  List<String>PreferedMethods = new list<String>();
                  PreferedMethods.add('Air'); 
                  string PreferedMethods1= shipmentordernewctrl.getPreferredFreight(rw.ShipFromcountry,rw.ShipTocountry);
                  if(PreferedMethods1 !=null && PreferedMethods1 !='' ){PreferedMethods.add(PreferedMethods1);}
                  
                  
                res.responseBody = Blob.valueOf(JSON.serializePretty(PreferedMethods));
                res.statusCode = 200;
                
                API_Log__c Al = New API_Log__c();
                Al.EndpointURLName__c='ZCPPreferredFreightMethodOnSO';
                Al.Response__c='Success- PreferredFreightMethods sent';
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al;
        
              
              
              }
              }
            
           catch(Exception e){
          	//String ErrorString ='Something went wrong while creating cost estimate, please contact support_SF@tecex.com';
            String ErrorString =e.getStackTraceString()+''+e.getLineNumber()+''+e.getMessage();
            res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
            res.addHeader('Content-Type', 'application/json');
            res.statusCode = 500;

            API_Log__c Al = New API_Log__c();
            Al.EndpointURLName__c='ZCPPreferredFreightMethodOnSO';
            Al.Response__c=e.getMessage()+' at line number: '+e.getLineNumber();
            Al.StatusCode__c=string.valueof(res.statusCode);
            Insert Al;

                      
                  }
              }

}