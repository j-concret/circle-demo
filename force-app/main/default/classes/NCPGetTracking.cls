@RestResource(urlMapping='/GetTrackings/*')
Global class NCPGetTracking {

 

     @Httppost
      global static void NCPGetCourierServices(){
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        Blob body = req.requestBody;
        res.addHeader('Content-Type', 'application/json');
        String requestString = body.toString();
        GetTrackingWra rw  = (GetTrackingWra)JSON.deserialize(requestString,GetTrackingWra.class);
          try{
              
              //Accesstoken test has to come here
              Access_token__c At =[select status__c,Access_Token__c from Access_token__c where Access_Token__c=:rw.Accesstoken ];
            if( at.status__C =='Active'){
                
               List<So_Travel_History__c> aLLSOT = [select Id,Type_of_Update__c,Name,LastModifiedDate,CreatedDate,Status__c,Date_Time__c,Freight_Request__c,TecEx_SO__c,TecEx_SO__r.Sub_Status_Update__c,Mapped_Status__c,Description__c,TecEx_SO__r.Account__c,TecEx_SO__r.Buyer_Account__c,TecEx_SO__r.Buyer_Account__r.name,TecEx_SO__r.ETA_days__c,TecEx_SO__r.Name,TecEx_SO__r.ID,TecEx_SO__r.Shipping_Status__c,TecEx_SO__r.Ship_From_Country__c,TecEx_SO__r.Destination__c,TecEx_SO__r.Client_Reference__c,TecEx_SO__r.Client_Reference_2__c,TecEx_SO__r.Client_PO_ReferenceNumber__c,TecEx_SO__r.roll_out__r.name,TecEx_SO__r.Service_type__c,TecEx_SO__r.NCP_Shipping_Status__c,TecEx_SO__r.NCP_Quote_Reference__c,TecEx_SO__r.Client_Contact_for_this_Shipment__c from SO_Travel_History__C  where TecEx_SO__r.Account__c = :rw.AccountID and TecEx_SO__r.NCP_Shipping_Status__c = 'Shipment - Tracking'];
              
              if(!aLLSOT.isempty()){
             res.responseBody = Blob.valueOf(JSON.serializePretty(aLLSOT));
             res.addHeader('Content-Type', 'application/json');
             res.statusCode = 200;
                
                API_Log__c Al = New API_Log__c();
                Al.EndpointURLName__c='GetTrackings';
                Al.Account__c=rw.AccountID;
                Al.StatusCode__c=string.valueof(res.statusCode);
                AL.Response__c = 'Success - Trackings are sent';
                Insert Al;  
              }
              Else{
                  
            JSONGenerator gen = JSON.createGenerator(true);
            gen.writeStartArray();
            
          /*   gen.writeFieldName('Success');
             gen.writeStartObject();
            
                 if(aLLSOT.isempty()) {gen.writeNullField('Response');} else{gen.writeStringField('Response', 'Trackings are not available');}
            
            gen.writeEndObject();
            */
             gen.writeEndArray();
            String jsonData = gen.getAsString();
            res.responseBody = Blob.valueOf(jsonData);
            res.addHeader('Content-Type', 'application/json');
            res.statusCode = 200;        
                  
               }   
                  
              }
              else{
                    throw new DMLException();
              
              }
              
              
            } catch(Exception e){
                
                String ErrorString ='Something went wrong, please contact support_SF@tecex.com';
                       res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
                   res.addHeader('Content-Type', 'application/json');
                    res.statusCode = 400;
                
                API_Log__c Al = New API_Log__c();
                Al.Account__c=rw.AccountID;
                //Al.Login_Contact__c=logincontact.Id;
                Al.EndpointURLName__c='GetCourierrates';
                Al.Response__c=ErrorString;
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al;
                      
          }
          
          
      
      
      
      
      
      }
}