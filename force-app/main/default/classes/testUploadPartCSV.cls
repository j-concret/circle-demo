@isTest(SeeAllData=true)



public class testUploadPartCSV {
    
   private static testMethod void  setUpData(){
        Account account = new Account (name='Acme1', Type ='Client', CSE_IOR__c = '0050Y000001LTZO');
        insert account;  
        
        Account account2 = new Account (name='Test Supplier', Type ='Supplier', ICE__c = '0050Y000001km5c');
        insert account2;      
         
        Contact contact = new Contact ( lastname ='Testing Individual',  AccountId = account.Id);
        insert contact;     
          
        country_price_approval__c cpa = new country_price_approval__c( name ='Sri Lanka', Billing_term__c = 'DAP/CIF - IOR pays',  Airwaybill_Instructions__c = 'User IOR Address', Supplier__c = account2.Id,
        Clearance_Destination__c ='Sri Lanka', Preferred_Supplier__c = TRUE);
        insert cpa;  
        
        IOR_Price_List__c iorpl = new IOR_Price_List__c ( Client_Name__c = account.Id, Name = 'Sri Lanka', Destination__c = 'Sri Lanka', IOR_Fee__c = 0.15, On_Charge_Mark_up__c = 0.15,
        TecEx_Shipping_Fee_Markup__c =0.40, IOR_Min_Fee__c = 100,  Admin_Fee__c =100, Set_up_fee__c =100, Bank_Fees__c =200, Tax_Rate__c = 0.20,
        Estimated_Customs_Brokerage__c =250, Estimated_Customs_Clearance__c =250, Estimated_Customs_Handling__c =250, Estimated_Import_License__c =250);
        insert iorpl; 
     
        Shipment_Order__c shipmentOrder = new Shipment_Order__c(Account__c = account.id, IOR_Price_List__c = iorpl.Id, Shipment_Value_USD__c =1000, 
        Client_Contact_for_this_Shipment__c = contact.Id, Who_arranges_International_courier__c ='Client', Tax_Treatment__c ='DAP/CIF - IOR pays', Shipping_Status__c ='Shipment Pending', Ship_to_Country__c = cpa.Id  );
        insert shipmentOrder;
        
        Opportunity opportunity = new Opportunity(AccountId = account.id, Name ='120-547', Ship_to_Country__c = cpa.Id, IOR_Price_List__c = iorpl.Id, Shipment_Value_USD__c =1000,
        Ship_From_Country__c = 'Algeria', CloseDate= system.today(), StageName ='Shipment Pending', Client_Contact_for_this_Shipment__c = contact.Id, Who_arranges_International_courier__c ='Client', Tax_Treatment__c ='DAP/CIF - IOR pays',
        shipment_Order__c = shipmentOrder.Id );
        insert opportunity;
        
        Part__c part1 = new Part__c(Name ='CAB-ETH-S-RJ45', Part_Type__c ='Server', Part_Number__c ='AB-ETH-S-RJ45', Quantity__c= 1, Commercial_Value__c = 2000, US_HTS_Code__c = '85444210',
        Shipment_Order__c = shipmentOrder.Id);
        insert part1;
        
        
        Part__c part2 = new Part__c(Name ='CAB-ETH-S-RJ45', Part_Type__c ='Server', Part_Number__c ='AB-ETH-S-RJ45', Quantity__c= 4, Commercial_Value__c = 5000, US_HTS_Code__c = '85444210',
        Shipment_Order__c = shipmentOrder.Id);
        insert part2;
        
        country_price_approval__c cpa2 = new country_price_approval__c( name ='South Africa', Billing_term__c = 'DAP/CIF - IOR pays',  Airwaybill_Instructions__c = 'User IOR Address', Supplier__c = account2.Id,
        Clearance_Destination__c ='South Africa', Preferred_Supplier__c = TRUE);
        insert cpa2;  
        
        IOR_Price_List__c iorpl2 = new IOR_Price_List__c ( Client_Name__c = account.Id, Name = 'South Africa', Destination__c = 'South Africa', IOR_Fee__c = 0.15, On_Charge_Mark_up__c = 0.15,
        TecEx_Shipping_Fee_Markup__c =0.40, IOR_Min_Fee__c = 100,  Admin_Fee__c =100, Set_up_fee__c =100, Bank_Fees__c =200, Tax_Rate__c = 0.20,
        Estimated_Customs_Brokerage__c =250, Estimated_Customs_Clearance__c =250, Estimated_Customs_Handling__c =250, Estimated_Import_License__c =250);
        insert iorpl2; 
     
        Shipment_Order__c shipmentOrder2 = new Shipment_Order__c(Account__c = account.id, IOR_Price_List__c = iorpl2.Id, Shipment_Value_USD__c =1000, 
        Client_Contact_for_this_Shipment__c = contact.Id, Who_arranges_International_courier__c ='Client', Tax_Treatment__c ='DAP/CIF - IOR pays', Shipping_Status__c ='Shipment Pending', Ship_to_Country__c = cpa2.Id  );
        insert shipmentOrder2;
        
        Opportunity opportunity2 = new Opportunity(AccountId = account.id, Name ='120-547', Ship_to_Country__c = cpa2.Id, IOR_Price_List__c = iorpl2.Id, Shipment_Value_USD__c =1000,
        Ship_From_Country__c = 'Algeria', CloseDate= system.today(), StageName ='Shipment Pending', Client_Contact_for_this_Shipment__c = contact.Id, Who_arranges_International_courier__c ='Client', Tax_Treatment__c ='DAP/CIF - IOR pays',
        Shipment_Order__c = shipmentOrder2.Id);
        insert opportunity2;
        
        Part__c part3 = new Part__c(Name ='CAB-ETH-S-RJ45', Part_Type__c ='Server', Part_Number__c ='AB-ETH-S-RJ45', Quantity__c= 1, Commercial_Value__c = 2000, US_HTS_Code__c = '85444210',
        Shipment_Order__c = shipmentOrder2.Id);
        insert part3;
        
        
        Part__c part4 = new Part__c(Name ='CAB-ETH-S-RJ45', Part_Type__c ='Server', Part_Number__c ='AB-ETH-S-RJ45', Quantity__c= 4, Commercial_Value__c = 5000, US_HTS_Code__c = '85444210',
        Shipment_Order__c = shipmentOrder2.Id);
        insert part4;
        
        country_price_approval__c cpa3 = new country_price_approval__c( name ='Jordan', Billing_term__c = 'DAP/CIF - IOR pays',  Airwaybill_Instructions__c = 'User IOR Address', Supplier__c = account2.Id,
        Clearance_Destination__c ='Jordan', Preferred_Supplier__c = TRUE);
        insert cpa3;  
        
        IOR_Price_List__c iorpl3 = new IOR_Price_List__c ( Client_Name__c = account.Id, Name = 'Jordan', Destination__c = 'Jordan', IOR_Fee__c = 0.15, On_Charge_Mark_up__c = 0.15,
        TecEx_Shipping_Fee_Markup__c =0.40, IOR_Min_Fee__c = 100,  Admin_Fee__c =100, Set_up_fee__c =100, Bank_Fees__c =200, Tax_Rate__c = 0.20,
        Estimated_Customs_Brokerage__c =250, Estimated_Customs_Clearance__c =250, Estimated_Customs_Handling__c =250, Estimated_Import_License__c =250);
        insert iorpl3; 
     
        Shipment_Order__c shipmentOrder3 = new Shipment_Order__c(Account__c = account.id, IOR_Price_List__c = iorpl3.Id, Shipment_Value_USD__c =1000, 
        Client_Contact_for_this_Shipment__c = contact.Id, Who_arranges_International_courier__c ='Client', Tax_Treatment__c ='DAP/CIF - IOR pays', Shipping_Status__c ='Shipment Pending', Ship_to_Country__c = cpa3.Id  );
        insert shipmentOrder3;
        
        Opportunity opportunity3 = new Opportunity(AccountId = account.id, Name ='120-547', Ship_to_Country__c = cpa3.Id, IOR_Price_List__c = iorpl3.Id, Shipment_Value_USD__c =1000,
        Ship_From_Country__c = 'Algeria', CloseDate= system.today(), StageName ='Shipment Pending', Client_Contact_for_this_Shipment__c = contact.Id, Who_arranges_International_courier__c ='Client', Tax_Treatment__c ='DAP/CIF - IOR pays',
        Shipment_Order__c = shipmentOrder3.Id);
        insert opportunity3;
        
        Part__c part5 = new Part__c(Name ='CAB-ETH-S-RJ45', Part_Type__c ='Server', Part_Number__c ='AB-ETH-S-RJ45', Quantity__c= 1, Commercial_Value__c = 2000, US_HTS_Code__c = '85444210',
        Shipment_Order__c = shipmentOrder3.Id);
        insert part5;
        
        
        Part__c part6 = new Part__c(Name ='CAB-ETH-S-RJ45', Part_Type__c ='Server', Part_Number__c ='AB-ETH-S-RJ45', Quantity__c= 4, Commercial_Value__c = 5000, US_HTS_Code__c = '85444210',
        Shipment_Order__c = shipmentOrder3.Id);
        insert part6;
        
                country_price_approval__c cpa4 = new country_price_approval__c( name ='France', Billing_term__c = 'DAP/CIF - IOR pays',  Airwaybill_Instructions__c = 'User IOR Address', Supplier__c = account2.Id,
        Clearance_Destination__c ='France', Preferred_Supplier__c = TRUE);
        insert cpa4;  
        
        IOR_Price_List__c iorpl4 = new IOR_Price_List__c ( Client_Name__c = account.Id, Name = 'France', Destination__c = 'France', IOR_Fee__c = 0.15, On_Charge_Mark_up__c = 0.15,
        TecEx_Shipping_Fee_Markup__c =0.40, IOR_Min_Fee__c = 100,  Admin_Fee__c =100, Set_up_fee__c =100, Bank_Fees__c =200, Tax_Rate__c = 0.20,
        Estimated_Customs_Brokerage__c =250, Estimated_Customs_Clearance__c =250, Estimated_Customs_Handling__c =250, Estimated_Import_License__c =250);
        insert iorpl4; 
     
        Shipment_Order__c shipmentOrder4 = new Shipment_Order__c(Account__c = account.id, IOR_Price_List__c = iorpl4.Id, Shipment_Value_USD__c =1000, 
        Client_Contact_for_this_Shipment__c = contact.Id, Who_arranges_International_courier__c ='Client', Tax_Treatment__c ='DAP/CIF - IOR pays', Shipping_Status__c ='Shipment Pending', Ship_to_Country__c = cpa4.Id  );
        insert shipmentOrder4;
        
        Opportunity opportunity4 = new Opportunity(AccountId = account.id, Name ='120-547', Ship_to_Country__c = cpa4.Id, IOR_Price_List__c = iorpl4.Id, Shipment_Value_USD__c =1000,
        Ship_From_Country__c = 'Algeria', CloseDate= system.today(), StageName ='Shipment Pending', Client_Contact_for_this_Shipment__c = contact.Id, Who_arranges_International_courier__c ='Client', Tax_Treatment__c ='DAP/CIF - IOR pays',
        Shipment_Order__c = shipmentOrder4.Id);
        insert opportunity4;
        
        Part__c part7 = new Part__c(Name ='CAB-ETH-S-RJ45', Part_Type__c ='Server', Part_Number__c ='AB-ETH-S-RJ45', Quantity__c= 1, Commercial_Value__c = 2000, US_HTS_Code__c = '85444210',
        Shipment_Order__c = shipmentOrder4.Id);
        insert part7;
        
        
        Part__c part8 = new Part__c(Name ='CAB-ETH-S-RJ45', Part_Type__c ='Server', Part_Number__c ='AB-ETH-S-RJ45', Quantity__c= 4, Commercial_Value__c = 5000, US_HTS_Code__c = '85444210',
        Shipment_Order__c = shipmentOrder4.Id);
        insert part8;
        
        country_price_approval__c cpa5 = new country_price_approval__c( name ='Bangladesh', Billing_term__c = 'DAP/CIF - IOR pays',  Airwaybill_Instructions__c = 'User IOR Address', Supplier__c = account2.Id,
        Clearance_Destination__c ='Bangladesh', Preferred_Supplier__c = TRUE);
        insert cpa5;  
        
        IOR_Price_List__c iorpl5 = new IOR_Price_List__c ( Client_Name__c = account.Id, Name = 'Bangladesh', Destination__c = 'Bangladesh', IOR_Fee__c = 0.15, On_Charge_Mark_up__c = 0.15,
        TecEx_Shipping_Fee_Markup__c =0.40, IOR_Min_Fee__c = 100,  Admin_Fee__c =100, Set_up_fee__c =100, Bank_Fees__c =200, Tax_Rate__c = 0.20,
        Estimated_Customs_Brokerage__c =250, Estimated_Customs_Clearance__c =250, Estimated_Customs_Handling__c =250, Estimated_Import_License__c =250);
        insert iorpl5; 
     
        Shipment_Order__c shipmentOrder5 = new Shipment_Order__c(Account__c = account.id, IOR_Price_List__c = iorpl5.Id, Shipment_Value_USD__c =1000, 
        Client_Contact_for_this_Shipment__c = contact.Id, Who_arranges_International_courier__c ='Client', Tax_Treatment__c ='DAP/CIF - IOR pays', Shipping_Status__c ='Shipment Pending', Ship_to_Country__c = cpa5.Id  );
        insert shipmentOrder5;
        
        Opportunity opportunity5 = new Opportunity(AccountId = account.id, Name ='120-547', Ship_to_Country__c = cpa5.Id, IOR_Price_List__c = iorpl5.Id, Shipment_Value_USD__c =1000,
        Ship_From_Country__c = 'Algeria', CloseDate= system.today(), StageName ='Shipment Pending', Client_Contact_for_this_Shipment__c = contact.Id, Who_arranges_International_courier__c ='Client', Tax_Treatment__c ='DAP/CIF - IOR pays',
        Shipment_Order__c = shipmentOrder5.Id);
        insert opportunity5;
        
        Part__c part9 = new Part__c(Name ='CAB-ETH-S-RJ45', Part_Type__c ='Server', Part_Number__c ='AB-ETH-S-RJ45', Quantity__c= 1, Commercial_Value__c = 2000, US_HTS_Code__c = '85444210',
        Shipment_Order__c = shipmentOrder5.Id);
        insert part9;
        
        
        Part__c part10 = new Part__c(Name ='CAB-ETH-S-RJ45', Part_Type__c ='Server', Part_Number__c ='AB-ETH-S-RJ45', Quantity__c= 4, Commercial_Value__c = 5000, US_HTS_Code__c = '85444210',
        Shipment_Order__c = shipmentOrder5.Id);
        insert part10;
        
        country_price_approval__c cpa6 = new country_price_approval__c( name ='Japan', Billing_term__c = 'DAP/CIF - IOR pays',  Airwaybill_Instructions__c = 'User IOR Address', Supplier__c = account2.Id,
        Clearance_Destination__c ='Japan', Preferred_Supplier__c = TRUE);
        insert cpa6;  
        
        IOR_Price_List__c iorpl6 = new IOR_Price_List__c ( Client_Name__c = account.Id, Name = 'Japan', Destination__c = 'Japan', IOR_Fee__c = 0.15, On_Charge_Mark_up__c = 0.15,
        TecEx_Shipping_Fee_Markup__c =0.40, IOR_Min_Fee__c = 100,  Admin_Fee__c =100, Set_up_fee__c =100, Bank_Fees__c =200, Tax_Rate__c = 0.20,
        Estimated_Customs_Brokerage__c =250, Estimated_Customs_Clearance__c =250, Estimated_Customs_Handling__c =250, Estimated_Import_License__c =250);
        insert iorpl6; 
     
        Shipment_Order__c shipmentOrder6 = new Shipment_Order__c(Account__c = account.id, IOR_Price_List__c = iorpl6.Id, Shipment_Value_USD__c =1000, 
        Client_Contact_for_this_Shipment__c = contact.Id, Who_arranges_International_courier__c ='Client', Tax_Treatment__c ='DAP/CIF - IOR pays', Shipping_Status__c ='Shipment Pending', Ship_to_Country__c = cpa6.Id  );
        insert shipmentOrder6;
        
        Opportunity opportunity6 = new Opportunity(AccountId = account.id, Name ='120-547', Ship_to_Country__c = cpa6.Id, IOR_Price_List__c = iorpl6.Id, Shipment_Value_USD__c =1000,
        Ship_From_Country__c = 'Algeria', CloseDate= system.today(), StageName ='Shipment Pending', Client_Contact_for_this_Shipment__c = contact.Id, Who_arranges_International_courier__c ='Client', Tax_Treatment__c ='DAP/CIF - IOR pays',
        Shipment_Order__c = shipmentOrder6.Id);
        insert opportunity6;
        
        Part__c part11 = new Part__c(Name ='CAB-ETH-S-RJ45', Part_Type__c ='Server', Part_Number__c ='AB-ETH-S-RJ45', Quantity__c= 1, Commercial_Value__c = 2000, US_HTS_Code__c = '85444210',
        Shipment_Order__c = shipmentOrder6.Id);
        insert part11;
        
        
        Part__c part12 = new Part__c(Name ='CAB-ETH-S-RJ45', Part_Type__c ='Server', Part_Number__c ='AB-ETH-S-RJ45', Quantity__c= 4, Commercial_Value__c = 5000, US_HTS_Code__c = '85444210',
        Shipment_Order__c = shipmentOrder6.Id);
        insert part12;
        
        country_price_approval__c cpa7 = new country_price_approval__c( name ='Netherlands', Billing_term__c = 'DAP/CIF - IOR pays',  Airwaybill_Instructions__c = 'User IOR Address', Supplier__c = account2.Id,
        Clearance_Destination__c ='Netherlands', Preferred_Supplier__c = TRUE);
        insert cpa7;  
        
        IOR_Price_List__c iorpl7 = new IOR_Price_List__c ( Client_Name__c = account.Id, Name = 'Netherlands', Destination__c = 'Netherlands', IOR_Fee__c = 0.15, On_Charge_Mark_up__c = 0.15,
        TecEx_Shipping_Fee_Markup__c =0.40, IOR_Min_Fee__c = 100,  Admin_Fee__c =100, Set_up_fee__c =100, Bank_Fees__c =200, Tax_Rate__c = 0.20,
        Estimated_Customs_Brokerage__c =250, Estimated_Customs_Clearance__c =250, Estimated_Customs_Handling__c =250, Estimated_Import_License__c =250);
        insert iorpl7; 
     
        Shipment_Order__c shipmentOrder7 = new Shipment_Order__c(Account__c = account.id, IOR_Price_List__c = iorpl7.Id, Shipment_Value_USD__c =1000, 
        Client_Contact_for_this_Shipment__c = contact.Id, Who_arranges_International_courier__c ='Client', Tax_Treatment__c ='DAP/CIF - IOR pays', Shipping_Status__c ='Shipment Pending', Ship_to_Country__c = cpa7.Id  );
        insert shipmentOrder7;
        
        Opportunity opportunity7 = new Opportunity(AccountId = account.id, Name ='120-547', Ship_to_Country__c = cpa7.Id, IOR_Price_List__c = iorpl7.Id, Shipment_Value_USD__c =1000,
        Ship_From_Country__c = 'Algeria', CloseDate= system.today(), StageName ='Shipment Pending', Client_Contact_for_this_Shipment__c = contact.Id, Who_arranges_International_courier__c ='Client', Tax_Treatment__c ='DAP/CIF - IOR pays',
        Shipment_Order__c = shipmentOrder7.Id);
        insert opportunity7;
        
        Part__c part13 = new Part__c(Name ='CAB-ETH-S-RJ45', Part_Type__c ='Server', Part_Number__c ='AB-ETH-S-RJ45', Quantity__c= 1, Commercial_Value__c = 2000, US_HTS_Code__c = '85444210',
        Shipment_Order__c = shipmentOrder7.Id);
        insert part13;
        
        
        Part__c part14 = new Part__c(Name ='CAB-ETH-S-RJ45', Part_Type__c ='Server', Part_Number__c ='AB-ETH-S-RJ45', Quantity__c= 4, Commercial_Value__c = 5000, US_HTS_Code__c = '85444210',
        Shipment_Order__c = shipmentOrder7.Id);
        insert part14;
        
        country_price_approval__c cpa8 = new country_price_approval__c( name ='Cayman Islands', Billing_term__c = 'DAP/CIF - IOR pays',  Airwaybill_Instructions__c = 'User IOR Address', Supplier__c = account2.Id,
        Clearance_Destination__c ='Cayman Islands', Preferred_Supplier__c = TRUE);
        insert cpa8;  
        
        IOR_Price_List__c iorpl8 = new IOR_Price_List__c ( Client_Name__c = account.Id, Name = 'Cayman Islands', Destination__c = 'Cayman Islands', IOR_Fee__c = 0.15, On_Charge_Mark_up__c = 0.15,
        TecEx_Shipping_Fee_Markup__c =0.40, IOR_Min_Fee__c = 100,  Admin_Fee__c =100, Set_up_fee__c =100, Bank_Fees__c =200, Tax_Rate__c = 0.20,
        Estimated_Customs_Brokerage__c =250, Estimated_Customs_Clearance__c =250, Estimated_Customs_Handling__c =250, Estimated_Import_License__c =250);
        insert iorpl8; 
     
        Shipment_Order__c shipmentOrder8 = new Shipment_Order__c(Account__c = account.id, IOR_Price_List__c = iorpl8.Id, Shipment_Value_USD__c =1000, 
        Client_Contact_for_this_Shipment__c = contact.Id, Who_arranges_International_courier__c ='Client', Tax_Treatment__c ='DAP/CIF - IOR pays', Shipping_Status__c ='Shipment Pending', Ship_to_Country__c = cpa8.Id  );
        insert shipmentOrder8;
        
        Opportunity opportunity8 = new Opportunity(AccountId = account.id, Name ='120-547', Ship_to_Country__c = cpa8.Id, IOR_Price_List__c = iorpl8.Id, Shipment_Value_USD__c =1000,
        Ship_From_Country__c = 'Algeria', CloseDate= system.today(), StageName ='Shipment Pending', Client_Contact_for_this_Shipment__c = contact.Id, Who_arranges_International_courier__c ='Client', Tax_Treatment__c ='DAP/CIF - IOR pays',
        Shipment_Order__c = shipmentOrder8.Id);
        insert opportunity8;
        
        Part__c part15 = new Part__c(Name ='CAB-ETH-S-RJ45', Part_Type__c ='Server', Part_Number__c ='AB-ETH-S-RJ45', Quantity__c= 1, Commercial_Value__c = 2000, US_HTS_Code__c = '8471500150',
        Shipment_Order__c = shipmentOrder8.Id);
        insert part15;
        
        
        Part__c part16 = new Part__c(Name ='CAB-ETH-S-RJ45', Part_Type__c ='Server', Part_Number__c ='AB-ETH-S-RJ45', Quantity__c= 4, Commercial_Value__c = 5000, US_HTS_Code__c = '85444210',
        Shipment_Order__c = shipmentOrder8.Id);
        insert part16;
        
        StaticResource testdoc = [Select Id,Body from StaticResource where name ='testUploadPartCSV'];

      
        test.startTest();
        
       importPartsFromCSV testUpload = new importPartsFromCSV(new ApexPages.StandardController(shipmentOrder));  
       testUpload.csvFileBody = testdoc.Body;
       testUpload.importPartCSVFile();

            
      
        test.stopTest();

     
       
    } 

   }