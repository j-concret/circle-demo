@isTest
private class DAL_AttachmentTest {
	
	@isTest static void TestDAL_Attachment() {
        
        Attachment att  = TestDataFactory.myAttachment();
        Id attachmentId = att.Id;
        String name     = att.Name;
        Id parentId     = att.ParentId;
        
        Set<Id> idSet   = new Set<Id>{attachmentId};
            
        Test.startTest();
        
        //Class instantiation
        DAL_Attachment attachment = new DAL_Attachment();
        
        //getSObjectFieldList
        List<Schema.SObjectField> sobjectFieldList = attachment.getSObjectFieldList();
       
        //getSObjectType
        Schema.SObjectType sobjectType = attachment.getSObjectType();
        
        //selectById
        List<Attachment> attByIdSet = attachment.selectById(idSet);
        Attachment attById          = attachment.selectById(attachmentId );
        
        //selectByNameAndParent
        List<Attachment> attByNameAndParent = attachment.selectByNameAndParent( name,parentId);
        
        Test.stopTest();

        //getSObjectFieldList assert
        System.assertEquals(5, sobjectFieldList.size());

        //getSObjectType assert
        System.assert(att.getSObjectType() == sobjectType );

        //selectById
        System.assertEquals(1, attByIdSet.size());
        System.assert(attById.Id == att.Id );

        //selectByNameAndParent
        System.assertEquals(1,attByNameAndParent.size());
        System.assert(attByNameAndParent[0].Id == att.Id);

        system.assertNotEquals(null, attById);
        system.assertNotEquals(null, attByIdSet);
        system.assertNotEquals(null, attByNameAndParent);
   
	}
	

	
}