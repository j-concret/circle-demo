@RestResource(urlMapping='/NCPEmailvalidation/*')
global class NCPEmailValidation {
     @Httppost
    global static void NCPEmailValidation(){
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        Blob body = req.requestBody;
        res.addHeader('Content-Type', 'application/json');
        String requestString = body.toString();
        
        NCPEmailValidationwra rw = (NCPEmailValidationwra)JSON.deserialize(requestString,NCPEmailValidationwra.class);
        try {
        List<Access_token__c> at = [SELECT status__c,Access_Token__c FROM Access_token__c WHERE Access_Token__c =: rw.Accesstoken and status__c = 'Active' LIMIT 1];
        If(!at.isEmpty()){
            
                     Map<String,String> emailValidityRes = checkEmailValidation(rw.typeOfGoods, rw.emailAddress);
            
            if(emailValidityRes != null && emailValidityRes.get('Status') == 'OK'){
           
                    JSONGenerator gen = JSON.createGenerator(true);
            		gen.writeStartObject();
            
             		gen.writeFieldName('Success');
             		gen.writeStartObject();
            
             			gen.writeStringField('Message','Email validated Succesfully');
            
            		gen.writeEndObject();
            		gen.writeEndObject();
            		String jsonData = gen.getAsString();
            		res.responseBody = Blob.valueOf(jsonData);
            		res.addHeader('Content-Type', 'application/json');
            		res.statusCode = 200;    
                API_Log__c Al = New API_Log__c();
                Al.EndpointURLName__c='NCPQuickQuoteCreation';
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al;
              
            }
            else if(emailValidityRes.get('Status') == 'ERROR'){
                JSONGenerator gen = JSON.createGenerator(true);
            		gen.writeStartObject();
            
             		gen.writeFieldName('Error');
             		gen.writeStartObject();
            
             			gen.writeStringField('Message',emailValidityRes.get('Message'));
            
            		gen.writeEndObject();
            		gen.writeEndObject();
            		String jsonData = gen.getAsString();
            		res.responseBody = Blob.valueOf(jsonData);
            		res.addHeader('Content-Type', 'application/json');
            		res.statusCode = 200;    
                API_Log__c Al = New API_Log__c();
                Al.EndpointURLName__c='NCPEmailValidation';
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al;
            }
        
          
        }
            else{
                
            String ErrorString ='Authentication failed';
            res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
            res.addHeader('Content-Type', 'application/json');
            res.statusCode = 400;

            }
            
            }
        catch(Exception e) {
             String ErrorString;
            if(!rw.emailAddress.contains('@')){ErrorString ='Invalid emailID';}
            else{
            ErrorString ='Something went wrong while creating cost estimate, please contact support_SF@tecex.com';
            }
            //String ErrorString = e.getLineNumber()+e.getStackTraceString()+e.getMessage();
            res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
            res.addHeader('Content-Type', 'application/json');
            res.statusCode = 500;

            API_Log__c Al = New API_Log__c();
            Al.EndpointURLName__c='NCPEmailValidation';
            Al.Response__c=e.getMessage()+' at line number: '+e.getLineNumber();
            Al.StatusCode__c=string.valueof(res.statusCode);
            Insert Al;

        }
    }
    
    public static Map<String,String> checkEmailValidation(String typeOfGoods, String email){
        Map<String,String> response = new Map<String,String>();
        response.put('Status','OK');
        String emailRegex = '^[a-zA-Z0-9._|\\\\%#~`=?&/$^*!}{+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$'; // source: http://www.regular-expressions.info/email.html
        Pattern MyPattern = Pattern.compile(emailRegex);
        Matcher MyMatcher = MyPattern.matcher(email);
        
        if (!MyMatcher.matches()) {
            response.put('Status','ERROR');
            response.put('Message','Please enter a valid email address');
            return response;
        }
        
        String fullDomain = email.split('@').get(1);
        String Domain = (fullDomain.split('\\.').get(0)).toLowerCase();
        Map<String,EmailBlocker__c> emailBlockers = (Map<String,EmailBlocker__c>)EmailBlocker__c.getAll();
        
        if(emailBlockers !=null && emailBlockers.containsKey(Domain)){
            EmailBlocker__c blockedEmail = emailBlockers.get(Domain);
            List<String> business = (blockedEmail.Type_of_business__c).split(';');
            if(business.contains(typeOfGoods)){
                response.put('Status','ERROR');
                response.put('Message','The email address you provided is not a valid business email address');
			}
        }
       
        
        return response;
    }

}