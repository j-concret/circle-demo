@istest
public class CompanyTriggerTest {
    /*  @istest
public static void insertCompany(){
UserRole r = new UserRole(DeveloperName = 'MyCustomRole', Name = 'My Role');
insert r;

User u = new User(
ProfileId = [SELECT Id FROM Profile WHERE Name = 'TecEx Branch Manager'].Id,
LastName = 'Berkeley',
Email = 'puser000@amamama.com',
Username = 'puser000@amamama.com' + System.currentTimeMillis(),
CompanyName = 'TEST',
FirstName='Vat',
Title = 'title',
Alias = 'alias',
TimeZoneSidKey = 'America/Los_Angeles',
EmailEncodingKey = 'UTF-8',
LanguageLocaleKey = 'en_US',
LocaleSidKey = 'en_US',
UserRoleId = r.Id,
Number_of_Companies_to_be_Allocated__c = 5
);
insert u;
System.runAs(u) {
Test.startTest();
company__c com = new company__c(Name = 'Amazon');
insert com;
Test.stopTest();
}
}
/*  public static void insertCompany1(){
UserRole r = new UserRole(DeveloperName = 'MyCustomRole', Name = 'My Role');
insert r;

User u = new User(
ProfileId = [SELECT Id FROM Profile WHERE Name = 'TecEx Branch Manager'].Id,
LastName = 'Berkeley',
Email = 'puser000@amamama.com',
Username = 'puser000@amamama.com' + System.currentTimeMillis(),
CompanyName = 'TEST',
FirstName='Vat',
Title = 'title',
Alias = 'alias',
TimeZoneSidKey = 'America/Los_Angeles',
EmailEncodingKey = 'UTF-8',
LanguageLocaleKey = 'en_US',
LocaleSidKey = 'en_US',
UserRoleId = r.Id

);
insert u;
System.runAs(u) {
Test.startTest();
company__c com = new company__c(Name = 'Amazon');
insert com;
Test.stopTest();
}
}*/
    @istest
    public static void insertCompany2(){
        UserRole r = new UserRole(DeveloperName = 'MyCustomRole', Name = 'My Role');
        insert r;
        
        User u = new User(
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'TecEx Branch Manager'].Id,
            LastName = 'Berkeley',
            Email = 'puser000@amamama.com',
            Username = 'puser000@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            FirstName='Vat',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            UserRoleId = r.Id,
            Number_of_Companies_to_be_Allocated__c = 5
            
        );
        insert u;
        System.runAs(u) {
            Test.startTest();
            company__c com = new company__c(Name = 'Amazon');
            insert com;
            Company__c company = [SELECT Id, OwnerID,Request_to_change_owner__c,Highest_Lead_Status__c FROM Company__c LIMIT 1];
            company.Highest_Lead_Status__c = 'New Lead';
            company.Request_to_change_owner__c = true;
            
            company.Change_Owner_Approval_Status__c = null;
            update company;
            
            Test.stopTest();
        }
    }
	@istest
    public static void insertCompany3(){
        UserRole r = new UserRole(DeveloperName = 'MyCustomRole', Name = 'My Role');
        insert r;
        
        User u1 = new User(
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'TecEx Branch Manager'].Id,
            LastName = 'Berkeley',
            Email = 'puser000@amamama.com',
            Username = 'puser000@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            FirstName='Vat',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            UserRoleId = r.Id,
            Number_of_Companies_to_be_Allocated__c = 5
            
        );
        insert u1;
        
        
        User u = new User(
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id,
            LastName = 'Berkeley',
            Email = 'puser000@amamama.com',
            Username = 'puser000@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            FirstName='Vat',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            UserRoleId = r.Id,
            Number_of_Companies_to_be_Allocated__c = 5
            
        );
        insert u;
        System.runAs(u) {
            Test.startTest();
            company__c com = new company__c(Name = 'Amazon');
            insert com;
            Company__c company = [SELECT Id, OwnerID,Request_to_change_owner__c,Highest_Lead_Status__c FROM Company__c LIMIT 1];
            company.OwnerId = u1.Id;
            update company;
            
            Test.stopTest();
        }
    }
}