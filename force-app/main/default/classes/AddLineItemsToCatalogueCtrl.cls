public with sharing class AddLineItemsToCatalogueCtrl {
    @AuraEnabled
    public static Map<String,Object> createLineItems(String account_id,String lineItems){
        try{
            Map<String,Object> response = new Map<String,Object>();
            Set<String> existing = new Set<String>();
            List<ProductWrapper> products = (List<ProductWrapper>)JSON.deserialize(lineItems,List<ProductWrapper>.class);
            Map<String,ProductWrapper> lineItemsRefMap = new Map<String,ProductWrapper>();
            Map<String,ProductWrapper> lineItemsCodeMap = new Map<String,ProductWrapper>();

            for(ProductWrapper proWrapper: products) {
                if(lineItemsRefMap.containsKey(proWrapper.internal_reference) || lineItemsCodeMap.containsKey(proWrapper.product_code)) continue;

                lineItemsRefMap.put(proWrapper.internal_reference,proWrapper);
                lineItemsCodeMap.put(proWrapper.product_code,proWrapper);
            }

            List<String> internalRefs = new List<String>(lineItemsRefMap.keySet());
            List<String> codes = new List<String>(lineItemsCodeMap.keySet());

            for(Product_Catalogue__c pro_cat : [SELECT Id,Client_Reference__c,Part__r.Name FROM Product_Catalogue__c WHERE Account__c =: account_id AND (Client_Reference__c IN:internalRefs OR Part__r.Name IN:codes)]) {
                //existing.add(pro_cat.Client_Reference__c);

                if(lineItemsRefMap.containsKey(pro_cat.Client_Reference__c)) {
                    ProductWrapper p = lineItemsRefMap.get(pro_cat.Client_Reference__c);
                    lineItemsCodeMap.remove(p.product_code);
                    lineItemsRefMap.remove(p.internal_reference);

                }
                if(lineItemsCodeMap.containsKey(pro_cat.Part__r.Name)) {
                    ProductWrapper p = lineItemsCodeMap.get(pro_cat.Part__r.Name);
                    lineItemsRefMap.remove(p.internal_reference);
                    lineItemsCodeMap.remove(p.product_code);
                }
            }

            Map<String,ProductWrapper> lineItemsCodeMapCopy = new Map<String,ProductWrapper>(lineItemsCodeMap);
            List<String> pro_codes = new List<String>(lineItemsCodeMapCopy.keySet());
            List<Product_Catalogue__c> prodCatalogues = new List<Product_Catalogue__c>();
            List<Part__c> newParts = new List<Part__c>();

            String query = 'SELECT '+String.join(new List<String>(Part__c.sObjectType.getDescribe().fields.getMap().keySet()),',') +' FROM Part__c WHERE Name IN:pro_codes';
            for(Part__c part : Database.query(query)) {
                if(lineItemsCodeMapCopy.isEmpty()) break;
                if(lineItemsCodeMapCopy.containsKey(part.Name)) {
                    ProductWrapper pro_wrap = lineItemsCodeMapCopy.get(part.Name);
                    Part__c clonedPart = part.clone(false, false, false, false);
                    clonedPart.Shipment_Order__c = null;
                    clonedPart.Commercial_Value__c = pro_wrap.unit_price;
                    clonedPart.Description_and_Functionality__c = pro_wrap.description;
                    clonedPart.Quantity__c = null;
                    clonedPart.RecordTypeId = Schema.Sobjecttype.Part__c.getRecordTypeInfosByDeveloperName().get('Catalogue_Record_Type').getRecordTypeId();
                    newParts.add(clonedPart);

                    lineItemsCodeMapCopy.remove(part.Name);
                }
            }

            if(!lineItemsCodeMapCopy.isEmpty()) {
                for(ProductWrapper pro_wrap: lineItemsCodeMapCopy.values()) {
                    newParts.add(new Part__c(
                                     Name = pro_wrap.product_code,
                                     Description_and_Functionality__c = pro_wrap.description,
                                     Commercial_Value__c = pro_wrap.unit_price,
                                     RecordTypeId = Schema.Sobjecttype.Part__c.getRecordTypeInfosByDeveloperName().get('Catalogue_Record_Type').getRecordTypeId()
                                     ));
                }
            }

            if(!newParts.isEmpty()) {
                insert newParts;

                for(Part__c part : newParts) {
                    ProductWrapper pro_wrap = lineItemsCodeMap.get(part.Name);
                    prodCatalogues.add(new Product_Catalogue__c(
                                           Client_Reference__c = pro_wrap.internal_reference,
                                           Account__c = account_id,
                                           Part__c = part.Id,
                                           Unit_Price__c = pro_wrap.unit_price
                                           ));
                }
            }

            insert prodCatalogues;
            response.put('existing',existing);
            return response;//'Successfully added Line items to product catalogue ';
        }catch(Exception e) {
            throw new AuraHandledException('Error Occurred: '+ e.getMessage());
        }
    }

    public class ProductWrapper {
        public String internal_reference;
        public String product_code;
        public String description;
        public Decimal unit_price;
    }

}