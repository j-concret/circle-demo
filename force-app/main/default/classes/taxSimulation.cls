public class taxSimulation {
    
  @InvocableMethod(label='taxSimulation' description='Reruns Tax Calculations with Variable Changes')
  public static void populateTaxSim(List<Id> taxSimIds){
   
      
  Set<String> soIds = new Set<String>();
      
      
   Map<Id, Tax_Calculator__c> taxSim = new Map<Id, Tax_Calculator__c>([Select Id, Name, Shipment_Order__c ,Country_Name__c 
                                                                     FROM Tax_Calculator__c
                                                                     Where Id in: taxSimIds]);	 
      
      
   for(Id sim : taxSim.keyset()){soIds.add(taxSim.get(sim).Shipment_Order__c);}
      
      System.debug('String soIds'+soIds);
    

  Map<Id, Shipment_Order__c> shipmentOrder = new Map<Id,Shipment_Order__c>([Select Id, Name, Shipment_Value_USD__c, Total_Line_Item_Extended_Value__c, Total_Taxes__c, 
                                                                            Tax_Cost__c, CPA_v2_0__r.Final_Destination__c, IOR_Price_List__r.Tax_Buffer__c, Invoice_Timing__c,
                                                                            Account__r.Invoice_Timing__c, Ship_From_Country__c,of_Line_Items__c
                                                                             From Shipment_Order__c
                                                                             Where Name in :soIds]); 
      
      
   Map<String, Shipment_Order__c> soNameMap = new Map<String,Shipment_Order__c>();
      
   for(Id soName : shipmentOrder.keyset()){soNameMap.put(shipmentOrder.get(soName).Name, shipmentOrder.get(soName));}  
      
      
  Map<Id, Part__c> allLinkedParts = new Map<Id, Part__c>([Select Id, Name, US_HTS_Code__c, Clearance_Country__r.Name, Quantity__c, Commercial_Value__c, Country_of_Origin2__c, Default_Duty_Rate__c,
                                Applied_To_Value__c, VAT_Applied_To_Value__c, Matched_HS_Code_Description__c, Rate__c, Part_Specific_Rate_1__c, Part_Specific_Rate_2__c, Clearance_Country__r.Country__c,
                                Part_Specific_Rate_3__c, Total_VAT__c, Vat_Exempted__c, VAT_Rate__c, Matched_HS_Code2__c, Matched_HS_Code2__r.Name, Shipment_Order__r.Name, Specific_VAT_Rate__c 
                                FROM Part__c
                                WHERE Shipment_Order__r.Name in :soIds]);    
      
  System.debug('String allLinkedParts'+allLinkedParts);   
      
  Map<Id, Tax_Calculation__c> calculatedTaxes = new Map<Id, Tax_Calculation__c>([SELECT Id, Shipment_Order__c, Value__c, Order_Number__c, Applied_To_Value__c, Tax_Name__c, Applied_to_Value_Type__c,
                                                    Rate__c, Tax_Type__c, Part_Specific__c, Applied_to_Order_Number__c, Country__c, Shipment_Order__r.CPA_v2_0__r.Final_Destination__c, Shipment_Order__r.Name
                                                    FROM Tax_Calculation__c 
                                                    WHERE Shipment_Order__r.Name in :soIds]); 
      
    System.debug('calculatedTaxes'+calculatedTaxes);
      
      Map<Id, Country__c> coutryList = new Map<Id, Country__c>([select Country__c, Name, CIF_Adjustment_Factor__c, CIF_Absolute_value_adjustment__c, Foreign_Currency_Adjustment_Factor__c
            from Country__c]);
      
      Map<String, Decimal> rateMap = new Map<String,Decimal>();
      Map<Id,Tax_Structure_Per_Country__c > taxMap = new Map<Id,Tax_Structure_Per_Country__c >([Select Amount__c,Tax_Type__c,Rate__c,Name ,Part_Specific__c, Max__c, Min__c, Order_Number__c,   
            Applied_to_Value__c,  Use_Total_From_Order_No__c, APPLIES_TO_ORDER__C, Additional_Percent__c, Default_Duty_Rate__c, Additional_Part_Specific_Tax__c, Country__c, Country__r.Country__c
            from Tax_Structure_Per_Country__c
            Where Tax_Type__c = 'Duties']);
      
      for(Id taxes : taxMap.keyset()){rateMap.put(taxMap.get(taxes).Country__r.Country__c, taxMap.get(taxes).Default_Duty_Rate__c); }
      
      Map<String, Country__c> countryNameMap = new Map<String,Country__c>();
      
      
      List<Tax_Calculator_Line_Item__c> newTCLI = new List<Tax_Calculator_Line_Item__c>();
      List<Tax_Simulation_Tax__c> newTSTL = new List<Tax_Simulation_Tax__c>();
      List<Tax_Calculator__c> simUpdate = new List<Tax_Calculator__c>();
      
      for(Id countryName : coutryList.keyset()){
          
          countryNameMap.put(coutryList.get(countryName).Country__c, coutryList.get(countryName));
                                              
                                               
                                               
                                               }
      
      for(Id sim : taxSim.keyset()){
          
      
          
      taxSim.get(sim).Shipment_Value__c = soNameMap.size() == 0 ? null : soNameMap.get(taxSim.get(sim).Shipment_Order__c).Shipment_Value_USD__c; 
      taxSim.get(sim).Ship_From_Country__c = soNameMap.size() == 0 ? null : soNameMap.get(taxSim.get(sim).Shipment_Order__c).Ship_From_Country__c; 
      taxSim.get(sim).Current_Tax_Amount__c = soNameMap.size() == 0 ? null : soNameMap.get(taxSim.get(sim).Shipment_Order__c).Total_Taxes__c;
      taxSim.get(sim).Total_Line_Item_Value__c = soNameMap.size() == 0 ? null : soNameMap.get(taxSim.get(sim).Shipment_Order__c).Total_Line_Item_Extended_Value__c;
      taxSim.get(sim).Tax_Buffer__c = soNameMap.size() == 0 ? null : soNameMap.get(taxSim.get(sim).Shipment_Order__c).IOR_Price_List__r.Tax_Buffer__c;
      taxSim.get(sim).Foreign_Currency_Adjustment_Factor__c = soNameMap.size() == 0 ? countryNameMap.get(taxSim.get(sim).Country_Name__c).Foreign_Currency_Adjustment_Factor__c: countryNameMap.get(soNameMap.get(taxSim.get(sim).Shipment_Order__c).CPA_v2_0__r.Final_Destination__c).Foreign_Currency_Adjustment_Factor__c ;
      taxSim.get(sim).Country_Name__c = taxSim.get(sim).Country_Name__c == null ? countryNameMap.get(soNameMap.get(taxSim.get(sim).Shipment_Order__c).CPA_v2_0__r.Final_Destination__c).Country__c:taxSim.get(sim).Country_Name__c;
      taxSim.get(sim).CIF_Adjustment_Factor__c = soNameMap.size() == 0 ? countryNameMap.get(taxSim.get(sim).Country_Name__c).CIF_Adjustment_Factor__c :countryNameMap.get(soNameMap.get(taxSim.get(sim).Shipment_Order__c).CPA_v2_0__r.Final_Destination__c).CIF_Adjustment_Factor__c;
      taxSim.get(sim).CIF_Absolute_Value_Adjustment__c = soNameMap.size() == 0 ? countryNameMap.get(taxSim.get(sim).Country_Name__c).CIF_Absolute_Value_Adjustment__c : countryNameMap.get(soNameMap.get(taxSim.get(sim).Shipment_Order__c).CPA_v2_0__r.Final_Destination__c).CIF_Absolute_Value_Adjustment__c;
      taxSim.get(sim).Number_of_Line_Items__c = soNameMap.size() == 0 ? null : soNameMap.get(taxSim.get(sim).Shipment_Order__c).of_Line_Items__c;
      taxSim.get(sim).Default_Duty_Rate__c = soNameMap.size() == 0 ? rateMap.get(taxSim.get(sim).Country_Name__c) : rateMap.get(soNameMap.get(taxSim.get(sim).Shipment_Order__c).CPA_v2_0__r.Final_Destination__c) ; 
          
          for  (Id li : allLinkedParts.keyset()){
           
              iF(allLinkedParts.get(li).Shipment_Order__c == soNameMap.get(taxSim.get(sim).Shipment_Order__c).Id){
                  
               Tax_Calculator_Line_Item__c newLI = new Tax_Calculator_Line_Item__c (
               Name = allLinkedParts.get(li).Name,
               Clearance_Destination__c = allLinkedParts.get(li).Clearance_Country__r.Country__c,
               Country_of_Origin__c = allLinkedParts.get(li).Country_of_Origin2__c, 
               Default_Duty_Rate__c = allLinkedParts.get(li).Default_Duty_Rate__c,
               Duty_Applied_To_Value__c =  allLinkedParts.get(li).Applied_To_Value__c,
               Duty_Rate__c =  allLinkedParts.get(li).Rate__c,
              // Estimated_Line_Duty__c = allLinkedParts.get(li).Country_of_Origin2__c,
               HS_Code__c = allLinkedParts.get(li).US_HTS_Code__c,
               Matched_HS_Code__c = allLinkedParts.get(li).Matched_HS_Code2__r.Name,
               Matched_HS_Code_Description__c = allLinkedParts.get(li).Matched_HS_Code_Description__c,
              // No_HS_Code_Match__c = allLinkedParts.get(li).Country_of_Origin2__c,
               Part_Specific_Rate_1__c = allLinkedParts.get(li).Part_Specific_Rate_1__c,
               Part_Specific_Rate_2__c = allLinkedParts.get(li).Part_Specific_Rate_2__c,
               Part_Specific_Rate_3__c = allLinkedParts.get(li).Part_Specific_Rate_3__c,
               Re_matched_Part_Specific_Rate_1__c = allLinkedParts.get(li).Part_Specific_Rate_1__c,
               Re_matched_Part_Specific_Rate_2__c = allLinkedParts.get(li).Part_Specific_Rate_2__c,
               Re_matched_Part_Specific_Rate_3__c = allLinkedParts.get(li).Part_Specific_Rate_3__c,
               Re_matched_Duty_Rate__c = allLinkedParts.get(li).Rate__c,
               Re_matched_HS_Code__c = allLinkedParts.get(li).Matched_HS_Code2__r.Name,
               Re_matched_HS_Code_Description__c = allLinkedParts.get(li).Matched_HS_Code_Description__c,
               Re_matched_VAT_Exempted__c = allLinkedParts.get(li).Vat_Exempted__c,
               Re_matched_Specific_VAT_Rate__c = allLinkedParts.get(li).Specific_VAT_Rate__c,
               Specific_VAT_Rate__c = allLinkedParts.get(li).Specific_VAT_Rate__c,
               Quantity__c = allLinkedParts.get(li).Quantity__c,
               Tax_Calculator__c = sim,
               Unit_Price__c = allLinkedParts.get(li).Commercial_Value__c,
               VAT_Applied_To_Value__c = allLinkedParts.get(li).VAT_Applied_To_Value__c,
               Vat_Exempted__c = allLinkedParts.get(li).Vat_Exempted__c,
               VAT_Rate__c = allLinkedParts.get(li).VAT_Rate__c);  
               newTCLI.add(newLI);}} 
          
          
          for  (Id taxes : calculatedTaxes.keyset()){
           
              iF(calculatedTaxes.get(taxes).Shipment_Order__c == soNameMap.get(taxSim.get(sim).Shipment_Order__c).Id){
                  
               Tax_Simulation_Tax__c newTaxSims = new Tax_Simulation_Tax__c (    
               Applied_to_Order_Number__c = calculatedTaxes.get(taxes).Applied_to_Order_Number__c, 
               Applied_To_Value__c = calculatedTaxes.get(taxes).Applied_To_Value__c,
               Applied_to_Value_Type__c = calculatedTaxes.get(taxes).Applied_to_Value_Type__c,
               Order_Number__c = calculatedTaxes.get(taxes).Order_Number__c,
               Part_Specific__c = calculatedTaxes.get(taxes).Part_Specific__c,
               Rate__c = calculatedTaxes.get(taxes).Rate__c,
               Tax_Calculator__c = sim,
               Tax_Name__c = calculatedTaxes.get(taxes).Tax_Name__c,
               Tax_Type__c = calculatedTaxes.get(taxes).Tax_Type__c,
               Value__c = calculatedTaxes.get(taxes).Value__c);
               newTSTL.add(newTaxSims); 
                  
                  
                  
              }}
          
          simUpdate.add(taxSim.get(sim));
          
          
      }
      
      insert newTSTL;
      insert newTCLI;
      update simUpdate;
    
        
    }
        
   

}