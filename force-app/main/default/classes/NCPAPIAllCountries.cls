@RestResource(urlMapping='/GetAllCountrieslist/*')
global class NCPAPIAllCountries {
     @Httpget
    global static void NCPAPIAllCountries(){
         RestRequest req = RestContext.request;
         RestResponse res = RestContext.response;

        Schema.DescribeFieldResult F = Shipment_Order__c.Ship_From_Country__c.getDescribe();
        Schema.sObjectField T = F.getSObjectField();
        List<PicklistEntry> entries = T.getDescribe().getPicklistValues();

               
        		res.addHeader('Content-Type', 'application/json');
        		res.responseBody = Blob.valueOf(JSON.serializePretty(entries));
        	 	 res.statusCode = 200;

}

}