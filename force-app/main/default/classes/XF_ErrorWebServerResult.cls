public class XF_ErrorWebServerResult {
	public Boolean hasError;
	public String errorMessage;
	public String errorDetails;
	public String errorCode;
	
	public XF_ErrorWebServerResult(HttpResponse response) {
		this(response.getBody());	
	}   
	
	public XF_ErrorWebServerResult(String response) {
		fromXML(response);	
	}   
	
	public XF_ErrorWebServerResult() {
		hasError = false;	  
	}   
	
	private void clear() {
		hasError = false;
		errorMessage = null;
		errorDetails = null;
		errorCode = null;
	}

	/*	
	<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
		<soapenv:Body>
			<soapenv:Fault>
				<faultcode>soapenv:Server.Application</faultcode>
				<faultstring>Application Error</faultstring>
				<detail>
					<error code="10060">Socket error: Could not bind the socket to the appropriate port and interface [trace 25] (Route: Dragon Server (Gilbert) - Address: rosetta:1687)</error>
				</detail>
			</soapenv:Fault>
		</soapenv:Body>
	</soapenv:Envelope>
	*/
	private XF_ErrorWebServerResult fromXML(String XML) {
		clear();
		list<String> nodes = new list<String> {'Body', 'Fault', 'detail', 'error'};
		DOM.Document doc = new DOM.Document();
		doc.load(XML);
		DOM.XMLNode current = doc.getRootElement();
		DOM.XMLNode found;
		for (String tag: nodes) {
			found = null;
       		for (DOM.XMLNode node: current.getChildElements()) 
       			if (node.getName() == tag) {
       				found = node;
       				break;  
       			}
       		if (found != null) current = found;
       		else break;
		}	
		
		if (found != null) {
   			hasError = true;
		    errorCode = found.getAttributeValue('code', null);
		    errorMessage = found.getText();
		}
		return this;	
	}
	
	public String toJSON() {
		return JSON.serialize(this);
	} 
}