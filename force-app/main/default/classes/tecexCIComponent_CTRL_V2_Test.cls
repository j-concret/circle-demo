@IsTest(SeeAllData=false)
public class tecexCIComponent_CTRL_V2_Test {
    
    @testSetup
    private static void setUpData(){
        
        Id ZeeCEId = Schema.Sobjecttype.Shipment_Order__c.getRecordTypeInfosByDeveloperName().get('Zee_Cost_Estimate').getRecordTypeId();
        
        Account account = new Account (name='Acme1',  Type ='Client',  CSE_IOR__c = '0050Y000001km5c',  Service_Manager__c = '0050Y000001km5c',  Tax_recovery_client__c  = FALSE, Ship_From_Line_1__c = 'Ship_From_Line_1',
                                       Ship_From_Line_2__c = 'Ship_From_Line_2', Ship_From_Line_3__c = 'Ship_From_Line_3', Ship_From_City__c = 'Ship_From_City',  Ship_From_Zip__c = 'Ship_From_Zip',
                                       Ship_From_Country__c = 'Ship_From_Country' ,  Ship_From_Other_notes__c = 'Ship_From_Other', Ship_From_Contact_Name__c = 'Ship_From_Contact', Ship_From_Contact_Email__c = 'Ship_From_Email',
                                       Ship_From_Contact_Tel__c = 'Ship_From_Tel', Client_Address_Line_1__c = 'Client_Address_Line_1', Client_Address_Line_2__c = 'Client_Address_Line_2', Client_Address_Line_3__c = 'Client_Address_Line_3',
                                       Client_City__c = 'Client_City', Client_Zip__c = 'Client_Zip', Client_Country__c = 'Client_Country',  Other_notes__c = 'Other_notes',  Tax_Name__c = 'Tax_Name', Tax_ID__c = 'Tax_ID',
                                       Contact_Name__c = 'Contact_Name', Contact_Email__c = 'Contact_Email', Contact_Tel__c = 'Contact_Tel', Financial_Controller__c = '0050Y000001km5c' ); insert account;  
        
        
        Account account2 = new Account (name='TecEx Prospective Client', Type ='Supplier', ICE__c = '0050Y000001km5c', RecordTypeId = '0120Y0000006KjQQAU'); insert account2;  
        Account account3 = new Account (name='Test Manufacturer', Type ='Manufacturer', RecordTypeId = '0120Y0000002wa0QAA', Manufacturer_Alias__c = 'Tester,Who' ); insert account3;
        
        Contact contact = new Contact (Client_Notifications_Choice__c = 'opt-in', lastname ='Testing Individual',  AccountId = account.Id); insert contact;
        
        DefaultFreightValues__c df = new DefaultFreightValues__c();
        df.Name = 'Test';
        df.Courier_Base_Rate_KG__c = 12;
        df.Courier_Dangerous_Goods_premium__c = 12;
        df.Courier_Fixed_Charge__c = 12;
        df.Courier_Non_stackable_premium__c = 12;
        df.Courier_Oversized_premium__c = 12;
        df.FF_Base_Rate_KG__c = 12;
        df.FF_Dangerous_Goods_Fixed_Charge_Premium__c = 12;
        df.FF_Dangerous_Goods_premium__c = 12;
        df.FF_Dedicated_Pickup__c = 12;
        df.FF_Fixed_Charge__c = 12;
        df.FF_Non_stackable_Fixed_Charge_Premium__c = 12;
        df.FF_Non_stackable_premium__c = 12;
        df.FF_Oversized_Fixed_Charge_Premium__c = 12;
        df.FF_Oversized_premium__c = 12;
        insert df;
        
        IOR_Price_List__c iorpl = new IOR_Price_List__c ( Client_Name__c = account.Id, Name = 'Australia', IOR_Fee__c = 0.15, On_Charge_Mark_up__c = 0.15,
                                                         TecEx_Shipping_Fee_Markup__c =0.40, IOR_Min_Fee__c = 100,  Admin_Fee__c =100, Set_up_fee__c =100, Bank_Fees__c =200, Tax_Rate__c = 0.20,
                                                         Estimated_Customs_Brokerage__c =250, Estimated_Customs_Clearance__c =250, Estimated_Customs_Handling__c =250, Estimated_Import_License__c =250, 
                                                         Destination__c = 'Australia' ); insert iorpl; 
        
        country_price_approval__c cpa = new country_price_approval__c( name ='Australia', Billing_term__c = 'DAP/CIF - IOR pays',  Airwaybill_Instructions__c = 'User IOR Address', Supplier__c = account2.Id, 
                                                                      In_Country_Specialist__c = '0050Y000001km5c', Clearance_Destination__c = 'Australia', Preferred_Supplier__c = TRUE, 
                                                                      Destination__c = 'Australia' ); insert cpa;
        
        
        
        Country__c country = new Country__c(Brokerage_Costs__c = 618, CIF_Adjustment_Factor__c = 0.035, CIF_Absolute_value_adjustment__c = 200, Clearance_Costs__c = 488, 
                                            Country__c = 'Australia', Name = 'Australia', Handling_Costs__c = 1180, License_Permit_Costs__c = 0); insert country;
        
        CPA_v2_0__c aCpav = new CPA_v2_0__c(Name = 'Australia', Country_Applied_Costings__c = TRUE); insert aCpav;
        
        CPA_v2_0__c cpav2 = new CPA_v2_0__c(Name = 'Australia Testing 2.0', Supplier__c = account2.Id, Service_Type__c = 'IOR/EOR', Related_Costing_CPA__c = aCpav.Id, 
                                            Preferred_Supplier__c = TRUE, VAT_Rate__c = 0.1, Destination__c = 'Australia',  Final_Destination__c = 'Australia', 
                                            VAT_Reclaim_Destination__c = FALSE, Country__c = country.id, Lead_ICE__c = '0050Y000001km5c', CI_Currency__c = 'US Dollar (USD)'); insert cpav2; 
        
        List<CPARules__c> CPARules = new List<CPARules__c>();
        CPARules.add(new CPARules__c(Country__c = 'Australia',Criteria__c ='Default', Chargable_weight__c = 'NONE', Courier_Responsibility__c = 'NONE',   ECCN_No__c = 'NONE', NL_Product_User__c = 'NONE', Number_of_final_deliveries__c = 'NONE', ShipmentValue__c ='NONE', Sum_of_quantity_of_matched_products__c = 'NONE', Manufacturer__c = 'NONE', DefaultCPA2__c = cpav2.Id, CPA2__c =  cpav2.Id));
        insert CPARules;
        
        Currency_Management2__c CM = new Currency_Management2__c (Name = 'US Dollar (USD)', Currency__c = 'US Dollar (USD)', ISO_Code__c = 'USD', Conversion_Rate__c = 1);
        insert CM; 
        
        
        
        List<CPA_Costing__c> costs = new List<CPA_Costing__c>();
        
        
        costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'Shipment value', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'IOR', Min__c = 375, Max__c = 14000, Conditional_value__c = 'Shipment value', Condition__c = 'Between', Floor__c = 0, Ceiling__c = 10000, Currency__c = 'US Dollar (USD)', Variable_threshold__c = null));
        costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'CIF value', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'IOR', Max__c = 50000, Conditional_value__c = 'Shipment value', Condition__c = 'Between', Floor__c = 0, Ceiling__c = 10000, Currency__c = 'US Dollar (USD)' , Variable_threshold__c = null));
        costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'Total Taxes', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'IOR', Min__c = 14000, Conditional_value__c = 'Shipment value', Condition__c = '<', Floor__c = 20000,  Currency__c = 'US Dollar (USD)' , Variable_threshold__c = null));
        costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'Shipment value', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'IOR', Conditional_value__c = 'CIF value', Condition__c = '>=', Floor__c = 5000, Currency__c = 'US Dollar (USD)' , Variable_threshold__c = null));
        
        costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'Shipment value', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'IOR', Min__c = 375, Max__c = 14000, Currency__c = 'US Dollar (USD)', Variable_threshold__c = 1000));
        costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'CIF value', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'IOR', Max__c = 50000, Currency__c = 'US Dollar (USD)', Variable_threshold__c = 5000));
        costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'Total Taxes', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'IOR', Min__c = 14000, Currency__c = 'US Dollar (USD)', Variable_threshold__c = 500));
        costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'Shipment value', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'IOR', Currency__c = 'US Dollar (USD)', Variable_threshold__c = 9000));
        
        
        costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'Chargeable weight', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'IOR', Min__c = 375, Max__c = 500, Conditional_value__c = 'Chargeable weight', Condition__c = 'Between', Floor__c = 0, Ceiling__c = 500, Currency__c = 'US Dollar (USD)' , Variable_threshold__c = null));
        costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = '# packages', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'IOR', Max__c = 200, Conditional_value__c = '# of packages', Condition__c = '>', Floor__c = 5,  Currency__c = 'US Dollar (USD)' , Variable_threshold__c = null));
        costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = '# Parts (line items)', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'IOR', Min__c = 100, Currency__c = 'US Dollar (USD)' , Variable_threshold__c = null));
        costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = '# Unique HS Codes', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'IOR', Currency__c = 'US Dollar (USD)' , Variable_threshold__c = null));
        
        costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'Chargeable weight', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'IOR', Min__c = 375, Max__c = 500, Currency__c = 'US Dollar (USD)', Variable_threshold__c = 100));
        costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = '# packages', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'IOR', Max__c = 200, Currency__c = 'US Dollar (USD)', Variable_threshold__c = 4));
        costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = '# Parts (line items)', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'IOR', Min__c = 100, Currency__c = 'US Dollar (USD)', Variable_threshold__c = 2));
        costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = '# of Final Deliveries', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'IOR', Currency__c = 'US Dollar (USD)',  Variable_threshold__c = 2));
        
        costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = null, Cost_Type__c = 'Fixed',  Amount__c = 800, IOR_EOR__c = 'IOR', Currency__c = 'US Dollar (USD)', Variable_threshold__c = null));
        costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = null, Cost_Type__c = 'Fixed',  Amount__c = 900, IOR_EOR__c = 'IOR', Currency__c = 'US Dollar (USD)', Variable_threshold__c = null));
        costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = null, Cost_Type__c = 'Fixed',  Amount__c = 100, IOR_EOR__c = 'IOR', Currency__c = 'US Dollar (USD)', Variable_threshold__c = null));
        costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = null, Cost_Type__c = 'Fixed',  Amount__c = 350, IOR_EOR__c = 'IOR', Currency__c = 'US Dollar (USD)', Variable_threshold__c = null));
        
        insert costs;
        
        
        
        //Product2 product = new Product2(Name = 'CAB-ETH-S-RJ45', Manufacturer__c = '0011v00001p4OOc'); 
        Product2 product = new Product2(Name = 'CAB-ETH-S-RJ45', Manufacturer__c = account3.id); 
        
        Insert product;
        
        HS_Codes_and_Associated_Details__c hsCode = new HS_Codes_and_Associated_Details__c(Name = '85444210', Destination_HS_Code__c = '85444210', Description__c = 'Decriprion', Country__c = country.id,
                                                                                           Additional_Part_Specific_Tax_Rate_1__c = 0.1, Additional_Part_Specific_Tax_Rate_2__c = 0.1, Additional_Part_Specific_Tax_Rate_3__c = 0.1,
                                                                                           Rate__c = 0.25);
        
        
        List<Tax_Structure_Per_Country__c> taxStructure = new List<Tax_Structure_Per_Country__c>();
        taxStructure.add(new Tax_Structure_Per_Country__c(Country__c = country.Id, Applied_to_Value__c = 'CIF', Name = 'I', Order_Number__c = '1', Part_Specific__c = TRUE, Tax_Type__c = 'Duties', Default_Duty_Rate__c = 0.5625)); 
        taxStructure.add(new Tax_Structure_Per_Country__c(Country__c = country.Id, Applied_to_Value__c = 'CIF', Name = 'II', Order_Number__c = '2', Part_Specific__c = FALSE, Tax_Type__c = 'VAT', Rate__c = 0.40));
        taxStructure.add(new Tax_Structure_Per_Country__c(Country__c = country.Id, Applied_to_Value__c = 'CIF', Name = 'III', Order_Number__c = '3', Part_Specific__c = FALSE, Tax_Type__c = 'Other', Rate__c = 0.40));
        taxStructure.add(new Tax_Structure_Per_Country__c(Country__c = country.Id, Applied_to_Value__c = 'CIF', Name = 'IV', Order_Number__c = '4', Part_Specific__c = FALSE, Tax_Type__c = 'Other',Rate__c = 0.40, Applies_to_Order__c = '1'));     
        taxStructure.add(new Tax_Structure_Per_Country__c(Country__c = country.Id, Applied_to_Value__c = 'CIF', Name = 'V', Order_Number__c = '5', Part_Specific__c = FALSE, Tax_Type__c = 'Other', Rate__c = 0.40, Min__c = 20, Max__c = 100));
        taxStructure.add(new Tax_Structure_Per_Country__c(Country__c = country.Id, Applied_to_Value__c = 'CIF', Name = 'VI', Order_Number__c = '6', Part_Specific__c = FALSE, Tax_Type__c = 'Other', Rate__c = 0.40, Applies_to_Order__c = '2,3,4,5', Additional_Percent__c = 0.01));
        taxStructure.add(new Tax_Structure_Per_Country__c(Country__c = country.Id, Applied_to_Value__c = 'CIF', Name = 'VII', Order_Number__c = '7', Part_Specific__c = FALSE, Tax_Type__c = 'Other', Rate__c = 0.40, Min__c = 20));       
        taxStructure.add(new Tax_Structure_Per_Country__c(Country__c = country.Id, Applied_to_Value__c = 'CIF', Name = 'VIII', Order_Number__c = '8', Part_Specific__c = FALSE, Tax_Type__c = 'Other', Rate__c = 0.40, Applies_to_Order__c = '2,3,4,5,6'));
        taxStructure.add(new Tax_Structure_Per_Country__c(Country__c = country.Id, Applied_to_Value__c = 'CIF', Name = 'IX', Order_Number__c = '9', Part_Specific__c = FALSE, Tax_Type__c = 'Other', Rate__c = 0.40, Max__c = 100));
        taxStructure.add(new Tax_Structure_Per_Country__c(Country__c = country.Id, Applied_to_Value__c = 'CIF', Name = 'X', Order_Number__c = '10', Part_Specific__c = FALSE, Tax_Type__c = 'Other', Rate__c = 0.40, Applies_to_Order__c = '2,3,4,5,6,7')); 
        insert taxStructure;
        
        List< Shipment_Order_Approval__c> approvals = new List<Shipment_Order_Approval__c>();
        approvals.add(new Shipment_Order_Approval__c(NAME='Test 1 Condition',ACTION__C='Please arrange for MSDS to be sent with the products',ACTIVE__C=TRUE,ASSIGNED_TO__C='AM',BINDING_QUOTE_EXEMPTED__C=FALSE,CONDITION_LOGIC__C=NULL,CONTROLLER_2__C='',CONTROLLER_3__C='',CONTROLLER__C='Does Not Equal',Description_Source__c = 'Generic Statement',DESCRIPTION__C='This product [Product] contains Li-Ion batteries. An MSDS will be required for freight purposes',FIELD_2__C='',FIELD_3__C='',FIELD_4__C='',FIELD_5__C='',FIELD_6__C='',FIELD__C='Lithium Batteries',FLAG_TYPE__C='Freight',OBJECT_1__C='Product',OBJECT_2__C='',OBJECT_3__C='',OBJECT_4__C='',OBJECT_5__C='',OBJECT_6__C='',PRIORITY__C='Shipment Alert',SUBJECT__C='Product contains Li-ion batteries: [Product]',VALUE_2__C='',VALUE_3__C='',VALUE__C='None'));
        approvals.add(new Shipment_Order_Approval__c(NAME='Test 2 Condition',ACTION__C='Review CPA Documentation Section (Required Document Notes), follow instructions for obtaining required documents or contact supplier for further information',ACTIVE__C=TRUE,ASSIGNED_TO__C='ICE',BINDING_QUOTE_EXEMPTED__C=FALSE,CONDITION_LOGIC__C='1',CONTROLLER_2__C='',CONTROLLER_3__C='',CONTROLLER__C='Does Not Equal',Description_Source__c = 'Generic Statement',DESCRIPTION__C='Product Requires Compliance Document/s [X]',FIELD_2__C='',FIELD_3__C='',FIELD_4__C='',FIELD_5__C='',FIELD_6__C='',FIELD__C='Product Specific Documents',FLAG_TYPE__C='Compliance',OBJECT_1__C='CPA',OBJECT_2__C='',OBJECT_3__C='',OBJECT_4__C='',OBJECT_5__C='',OBJECT_6__C='',PRIORITY__C='Shipment Block',SUBJECT__C='Product Compliance Document/s Required',VALUE_2__C='',VALUE_3__C='',VALUE__C='NULL'));
        insert approvals;
        
        List<Shipment_Order_Approval__c> approvalToCondition = [Select Name, Id from Shipment_Order_Approval__c where Name = 'Test 2 Condition' limit 1];
        
        String approvalId = approvalToCondition[0].Id;
        
        Shipment_Approval_Condition__c approvalCondition = new 
            Shipment_Approval_Condition__c(Condition_Number__c = '1', Condition_Object_1__c = 'Line Item',Condition_Field_1__c='Part Description', Condition_Controller__c = 'ISNULL', Shipment_Order_Approval__c = approvalId);
        insert approvalCondition;
        
        
        Shipment_Order__c shipmentOrder = new Shipment_Order__c(Account__c = account.id, Shipment_Value_USD__c = 20000, CPA_v2_0__c = cpav2.Id, IOR_CSE__c = '0050Y000001km5c', ICE__c = '0050Y000001km5c', Financial_Controller__c = '0050Y000001km5c',
                                                                Client_Contact_for_this_Shipment__c = contact.Id, Who_arranges_International_courier__c ='Client', Tax_Treatment__c ='DAP/CIF - IOR pays', 
                                                                Ship_to_Country__c = cpa.Id, Destination__c = 'Australia', Service_Type__c = 'IOR', IOR_Price_List__c = iorpl.Id, Chargeable_Weight__c = 200,  
                                                                Shipping_Status__c = 'Cost Estimate Abandoned', Ship_From_Country__c = 'Angola', SupplierlU__c = account2.Id );  insert shipmentOrder;
        
        Shipment_Order__c shipmentOrder2 = new Shipment_Order__c(Account__c = account.id, Shipment_Value_USD__c = 20000, CPA_v2_0__c = cpav2.Id, IOR_CSE__c = '0050Y000001km5c', ICE__c = '0050Y000001km5c', Financial_Controller__c = '0050Y000001km5c',
                                                                Client_Contact_for_this_Shipment__c = contact.Id, Who_arranges_International_courier__c ='Client', Tax_Treatment__c ='DAP/CIF - IOR pays', 
                                                                Ship_to_Country__c = cpa.Id, Destination__c = 'Australia', Service_Type__c = 'IOR', IOR_Price_List__c = iorpl.Id, Chargeable_Weight__c = 200,  
                                                                Shipping_Status__c = 'Cost Estimate Abandoned', Ship_From_Country__c = 'Angola', SupplierlU__c = account2.Id, RecordTypeId = ZeeCEId );  insert shipmentOrder2;
        
        Registrations__c regt = new Registrations__c (Name = 'Test Regt.', Registered_Address__c= 'Test Address 1', Registered_Address_2__c='Test Address 2', 
                                  Registered_Address_City__c= 'Test City', Registered_Address_Province__c='Test Province', 
                                  Registered_Address_Postal_Code__c='230000', Registered_Address_Country__c='Australia', 
                                  VAT_number__c='234566778', Company_name__c =account.Id, Country__c='Australia');
        Insert regt;
        
        CountryandRegionMap__c crm = new CountryandRegionMap__c();
        crm.Name = 'Australia';
        crm.Country__c = 'Australia';
        crm.European_Union__c = 'No';
        crm.Region__c = 'Australia';
        
        insert crm;
        CountryandRegionMap__c crm1 = new CountryandRegionMap__c();
        crm1.Name = 'Angola';
        crm1.Country__c = 'Angola';
        crm1.European_Union__c = 'No';
        crm1.Region__c = 'Angola';
        
        insert crm1;
        RegionalFreightValues__c rfm = new RegionalFreightValues__c();
        rfm.Name = 'Angola to Australia';
        rfm.Courier_Discount_Premium__c = 12;
        rfm.Courier_Fixed_Premium__c = 10;
        rfm.Destination__c = 'Australia';
        rfm.FF_Fixed_Premium__c = 123;
        rfm.FF_Regional_Discount_Premium__c = 12;
        rfm.Origin__c = 'Angola';
        rfm.Preferred_Courier_Id__c = 'test';
        rfm.Preferred_Courier_Name__c = 'test';
        rfm.Preferred_Freight_Forwarder_Id__c = 'test';
        rfm.Preferred_Freight_Forwarder_Name__c = 'test';
        rfm.Risk_Adjustment_Factor__c = 1;
        insert rfm;
        Freight__c Fr = New Freight__c(Logistics_provider__c=account2.Id,Li_Ion_Batteries__c='No',Shipment_Order__c=shipmentOrder.Id,Ship_Froma__c='Angola',Ship_From__c='Angola',Ship_To__c='Australia',Chargeable_weight_in_KGs_packages__c=1,Estimated_chargeable_weight__c=1, Pick_Up_Request__c = true, Ship_From_Country__c = 'Angola', Suggested_Supplier_Name__c = 'Fedex');
        Insert Fr;
        
        
        Part__c part1 = new Part__c(Name ='CAB-ETH-S-RJ45',  Quantity__c= 1, Commercial_Value__c = 15000, US_HTS_Code__c = '85444210', 
                                    Country_of_Origin2__c = 'China', Shipment_Order__c = shipmentOrder.Id, Description_and_Functionality__c ='AB-ETH-S-RJ45'); insert part1;
        
        Id FinalDestrecTypeId = Schema.SObjectType.Client_Address__c.getRecordTypeInfosByName().get('Consignee Address').getRecordTypeId();
        
        List<Final_Delivery__c> FD = new List<Final_Delivery__c>();
        FD.add(new Final_Delivery__c(Shipment_Order__c=shipmentorder.id,Name='Test',Contact_name__c='Anil',Contact_email__c='anilk@tecex.com',Contact_number__c='123456',Address_Line_1__c = 'add1',Address_Line_2__c='sdfasdfas'));
        Insert FD;
        
        List<Client_Address__c> cAddress = new List<Client_Address__c>();
        cAddress.add( new Client_Address__c(RecordTypeID=FinalDestrecTypeId,
                                                           Name='Name',
                                                           Contact_Full_Name__c='FullName',
                                                           Contact_Email__c='anilk@teccex.com',
                                                           Contact_Phone_Number__c='2563563',
                                                           Address__c='Add1',
                                                           Address2__c='Add2',
                                                           City__c='City',
                                                           Province__c='Proince',
                                                           Document_Type__c='CI',   
                                                           Postal_Code__c='5236',
                                                           All_Countries__c= 'South Africa',
                                                           VAT_NON_VAT_address__c = 'VAT',
                                                           Client__c = shipmentOrder.SupplierlU__c,
                                                           Port_of_Entry__c = 'Australia',
                                            			   type__c = 'Physical',
                                            			   Naming_Conventions__c = 'Use Final Delivery Details',
                                                           CPA_v2_0__c =  shipmentOrder.CPA_v2_0__c )) ; 
        
        cAddress.add( new Client_Address__c(RecordTypeID=FinalDestrecTypeId,
                                                           Name='Name',
                                            				Naming_Conventions__c = 'Use Final Delivery Details',
                                                           Contact_Full_Name__c='FullName',
                                                           Contact_Email__c='anilk@teccex.com',
                                                           Contact_Phone_Number__c='2563563',
                                                           Address__c='Add1',
                                                           Address2__c='Add2',
                                                           City__c='City',
                                                           Province__c='Proince',
                                                           Document_Type__c='CI',   
                                                           Postal_Code__c='5236',
                                                           All_Countries__c= 'South Africa',
                                                           VAT_NON_VAT_address__c = 'VAT',
                                            			   Address_Type__c = 'Section 2',
                                                           Client__c = shipmentOrder.SupplierlU__c,
                                                           Port_of_Entry__c = 'Perth',
                                                           CPA_v2_0__c =  shipmentOrder.CPA_v2_0__c )) ; 
        
        insert cAddress;
        
    }
    
    private static testMethod void  testMethodForTecEx_Zee(){
        List<Registrations__c> regtList = [SELECT Id, Name, Company_name__c, Country__c, Finance_contact_Email__c, 
                            Finance_contact_Name__c, Finance_contact_Phone__c, Registered_Address_2__c, 
                            Registered_Address_City__c, Registered_Address_Country__c, 
                            Registered_Address_Postal_Code__c, Registered_Address_Province__c, 
                            Registered_Address__c, Type_of_registration__c, VAT_number__c, Verified__c 
                            FROM Registrations__c];
        Shipment_Order__c shipmentOrder = [Select Id, Name,Contact_name__c, Account__r.Name from Shipment_Order__c LIMIT 1];
        List<Client_Address__c> cAddress = [Select Name, Naming_Conventions__c, Contact_Full_Name__c, Contact_Email__c, 
                                      Contact_Phone_Number__c, Address__c, Address2__c, City__c, Province__c, 
                                      Document_Type__c, Postal_Code__c, All_Countries__c, VAT_NON_VAT_address__c, Address_Type__c,
                                      Client__c, Port_of_Entry__c, CPA_v2_0__c, CompanyName__c from Client_Address__c];
        
        List<Final_Delivery__c> FD = new List<Final_Delivery__c>();
        FD.add(new Final_Delivery__c(Shipment_Order__c=shipmentOrder.id,Name='Test',Contact_name__c='Anil',Contact_email__c='anilk@tecex.com',Contact_number__c='123456',Address_Line_1__c = 'add1',Address_Line_2__c='sdfasdfas'));
        Insert FD;

        Map<String,String> additionColumns = new Map<String, String>();
        additionColumns.put('Test2', 'test');
        
        List<Part__c> newPartList = new List<Part__c>();
        //newPartList.add(new part__C(id = part1.id, Additional_CI_Columns__c = JSON.serialize(additionColumns)));
       
        Shipment_Order_Package__c SOP = new Shipment_Order_Package__c(packages_of_same_weight_dims__c=2,Weight_Unit__c='KGs',Actual_Weight__c=2,
                                                                      Dimension_Unit__c = 'INs',
                                                                      Height__c=2,
                                                                      Length__c=3,
                                                                      Breadth__c=2,
                                                                      Shipment_Order__c=shipmentOrder.id,
                                                                      Created_From__c='FromRecord'
                                                                        );
         Insert SOP;
        
        Id ZeeCEId = Schema.Sobjecttype.Shipment_Order__c.getRecordTypeInfosByDeveloperName().get('Zee_Cost_Estimate').getRecordTypeId();
        
        Shipment_Order__c zeeShipmentOrder = [Select Id, Name,Contact_name__c, Account__r.Name from Shipment_Order__c where RecordTypeId = :ZeeCEId LIMIT 1];
     
        Test.startTest();
        tecexCIComponent_CTRL_V2.getShipmentRelatedData(shipmentOrder.id); 
        
        tecexCIComponent_CTRL_V2.checkMissingData(shipmentOrder.id,'VAT','Perth');
        
        tecexCIComponent_CTRL_V2.getShipmentPckgLineItem(shipmentOrder.id,'VAT','Perth');
        
        tecexCIComponent_CTRL_V2.getShipmentPckgLineItem(zeeShipmentOrder.id,'VAT','Perth');
        
        // Below statement to test the  xls generation.
        //tecexCIComponent_CTRL_V2.saveCI('testest', shipmentOrder.id, shipmentOrder.name, 'xls', JSON.serialize(newPartList), 'VAT', 'Perth');
      
        
        // Below statement to text the save pdf.
        tecexCIComponent_CTRL_V2.saveCI('testest', shipmentOrder.id, shipmentOrder.name, 'pdf', JSON.serialize(newPartList), 'VAT', 'Perth');
        
        
        // Below conditions to check all the values of naming conventions.
        cAddress[0].Naming_Conventions__c = 'CPA c/o Final Delivery Details';
        tecexCIComponent_CTRL_V2.namingConventionTransformation(cAddress, FD, shipmentOrder, new List<Registrations__c>());
        
         cAddress[0].Naming_Conventions__c = 'Client c/o CPA Details as per Below';
        tecexCIComponent_CTRL_V2.namingConventionTransformation(cAddress, FD, shipmentOrder, new List<Registrations__c>());
        
         cAddress[0].Naming_Conventions__c = 'Client c/o CPA c/o Final Delivery Details';
        tecexCIComponent_CTRL_V2.namingConventionTransformation(cAddress, FD, shipmentOrder, new List<Registrations__c>());
        
         cAddress[0].Naming_Conventions__c = 'Beneficial Owner c/o Final Delivery Details';
        tecexCIComponent_CTRL_V2.namingConventionTransformation(cAddress, FD, shipmentOrder, new List<Registrations__c>());
        
           cAddress[0].Naming_Conventions__c = 'Beneficial Owner c/o Use CPA Details as per Below';
        tecexCIComponent_CTRL_V2.namingConventionTransformation(cAddress, FD, shipmentOrder, new List<Registrations__c>());
        
           cAddress[0].Naming_Conventions__c = 'VAT Claiming Entity c/o with CPA details below';
        tecexCIComponent_CTRL_V2.namingConventionTransformation(cAddress, FD, shipmentOrder, new List<Registrations__c>());
        
           cAddress[0].Naming_Conventions__c = 'VAT Claiming Entity (Tax#) c/o with Final Delivery Details';
        tecexCIComponent_CTRL_V2.namingConventionTransformation(cAddress, FD, shipmentOrder, new List<Registrations__c>());
        
           cAddress[0].Naming_Conventions__c = 'VAT Claiming Entity (Tax#) c/o CPA c/o with Final Delivery Details';
        tecexCIComponent_CTRL_V2.namingConventionTransformation(cAddress, FD, shipmentOrder, new List<Registrations__c>());
        
           cAddress[0].Naming_Conventions__c = 'TecEx c/o Final Delivery Company';
        tecexCIComponent_CTRL_V2.namingConventionTransformation(cAddress, FD, shipmentOrder, new List<Registrations__c>());
        cAddress[0].Naming_Conventions__c = 'Client Registration Address';
        tecexCIComponent_CTRL_V2.namingConventionTransformation(cAddress, FD, shipmentOrder, regtList);
        
        cAddress[0].Naming_Conventions__c = 'Client Registration, Tax # c/o Final Delivery Details';
        tecexCIComponent_CTRL_V2.namingConventionTransformation(cAddress, FD, shipmentOrder, regtList);
        
Test.stopTest();        
        
    } 
    
    /*private static testMethod void  testMethodForZee(){
        
        List<Registrations__c> regtList = [SELECT Id, Name, Company_name__c, Country__c, Finance_contact_Email__c, 
                            Finance_contact_Name__c, Finance_contact_Phone__c, Registered_Address_2__c, 
                            Registered_Address_City__c, Registered_Address_Country__c, 
                            Registered_Address_Postal_Code__c, Registered_Address_Province__c, 
                            Registered_Address__c, Type_of_registration__c, VAT_number__c, Verified__c 
                            FROM Registrations__c];
        
        Id ZeeCEId = Schema.Sobjecttype.Shipment_Order__c.getRecordTypeInfosByDeveloperName().get('Zee_Cost_Estimate').getRecordTypeId();
        
        Shipment_Order__c shipmentOrder = [Select Id, Name,Contact_name__c, Account__r.Name from Shipment_Order__c where RecordTypeId = :ZeeCEId LIMIT 1];
        List<Client_Address__c> cAddress = [Select Name, Naming_Conventions__c, Contact_Full_Name__c, Contact_Email__c, 
                                      Contact_Phone_Number__c, Address__c, Address2__c, City__c, Province__c, 
                                      Document_Type__c, Postal_Code__c, All_Countries__c, CompanyName__c, VAT_NON_VAT_address__c, Address_Type__c,
                                      Client__c, Port_of_Entry__c, CPA_v2_0__c from Client_Address__c];
        
        List<Final_Delivery__c> FD = new List<Final_Delivery__c>();
        FD.add(new Final_Delivery__c(Shipment_Order__c=shipmentOrder.id,Name='Test',Contact_name__c='Anil',Contact_email__c='anilk@tecex.com',Contact_number__c='123456',Address_Line_1__c = 'add1',Address_Line_2__c='sdfasdfas'));
        Insert FD;

        Map<String,String> additionColumns = new Map<String, String>();
        additionColumns.put('Test2', 'test');
            
        Test.startTest();        
        
        // Below conditions to check all the values of naming conventions.
        cAddress[0].Naming_Conventions__c = 'Client Registration Address';
        tecexCIComponent_CTRL_V2.namingConventionTransformation(cAddress, FD, shipmentOrder, regtList);
        
        cAddress[0].Naming_Conventions__c = 'Client Registration, Tax # c/o Final Delivery Details';
        tecexCIComponent_CTRL_V2.namingConventionTransformation(cAddress, FD, shipmentOrder, regtList);
        
        Test.stopTest();
        
    } */
    
    
    
}