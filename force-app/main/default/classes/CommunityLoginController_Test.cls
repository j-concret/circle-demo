@IsTest
public class CommunityLoginController_Test {

    static testMethod void getUserDetails_Test(){
        
        String Accesstoken = GuidUtilCP.NewGuid();
        string Base64Accesstoken = EncodingUtil.base64Encode(Blob.valueof(Accesstoken));
        
        Access_token__c At = New Access_token__c();
        At.Access_Token__c=Accesstoken;
        At.AccessToken_encoded__c=Base64Accesstoken;
        Insert At;
        
        Account acc = new Account(Name = 'Testing');
        insert acc;
        
        Contact con = new Contact(LastName = 'contact',Firstname = 'Test',Client_Notifications_Choice__c='Opt-In', AccountId = acc.Id);
        insert con;
        
        Profile profileId = [SELECT Id FROM Profile WHERE Name = 'Tecex Customer' LIMIT 1];
        
        User usr = new User(LastName = 'LIVESTON',
                            FirstName='JASON',
                            Alias = 'jliv',
                            Email = 'jason.liveston@asdf.com',
                            Username = 'jason.liveston@asdf.com',
                            ProfileId = profileId.id,
                            ContactId = con.Id,
                            TimeZoneSidKey = 'GMT',
                            LanguageLocaleKey = 'en_US',
                            EmailEncodingKey = 'UTF-8',
                            LocaleSidKey = 'en_US'
                           );
        insert usr;
        
        Test.startTest();
        System.runAs(usr){CommunityLoginController.getUserDetails();}
        
        Test.stopTest();
    }
    
    static testMethod void checkPortal_Test(){
        Test.startTest();
        CommunityLoginController.checkPortal('testUser','123456');
        
        Test.stopTest();
        
    }
    
    static testMethod void forgotPassowrd_Test(){
        Account acc = new Account(Name = 'Testing');
        insert acc;
        
        Contact con = new Contact(LastName = 'contact',Firstname = 'Test',Client_Notifications_Choice__c='Opt-In', AccountId = acc.Id);
        insert con;
        
        Profile profileId = [SELECT Id FROM Profile WHERE Name = 'Tecex Customer' LIMIT 1];
        User usr = new User(LastName = 'LIVESTON',
                            FirstName='JASON',
                            Alias = 'jliv',
                            Email = 'jason.liveston@asdf.com',
                            Username = 'jason.liveston@asdf.com',
                            ProfileId = profileId.id,
                            ContactId = con.Id,
                            TimeZoneSidKey = 'GMT',
                            LanguageLocaleKey = 'en_US',
                            EmailEncodingKey = 'UTF-8',
                            LocaleSidKey = 'en_US'
                           );
        insert usr;
        
        Test.startTest();
        System.runAs(usr){CommunityLoginController.forgotPassowrd('jason.liveston@asdf.com');}
        
        Test.stopTest();
    }
}