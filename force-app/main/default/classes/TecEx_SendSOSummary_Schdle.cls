global class TecEx_SendSOSummary_Schdle implements Schedulable {
	global void execute(SchedulableContext sc) {
		TecEx_Shipping_Order_Summary b = new TecEx_Shipping_Order_Summary();
		database.executebatch(b);
	}
}