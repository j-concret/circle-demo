@isTest
public class CustomMetadataLoaderCtrlTest {
    @isTest
    public static void testCreateMetadata(){
        String csv = 'Compliance_status_Required__c,Compliance_status_Visible__c,Compliance_Type__c,Date_of_issue_Required__c,Date_of_issue_Visible__c,DeveloperName\nFALSE,TRUE,Document,FALSE,FALSE,ITAC';
        ContentVersion contentVersion_1 = new ContentVersion(
            Title='SampleTitleTest', 
            PathOnClient ='SampleTitleTest.csv',
            VersionData = Blob.valueOf(csv), 
            origin = 'H'
        );
        insert contentVersion_1;
        Test.startTest();
        ContentDocument doc = [SELECT Id, Title FROM ContentDocument where Title='SampleTitleTest' limit 1];
        CustomMetadataLoaderCtrl.saveRecord(doc.Id);
        Test.stopTest();
    }
}