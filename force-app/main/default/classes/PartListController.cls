public class PartListController {
	@AuraEnabled   
    public static void CreateParts(String PPart_List,Id shipId){
        System.debug('JSONString PPart_List-->'+PPart_List);
        string code1 = PPart_List.replace('[','{ "Parts":[').replace(']','] }');
        String  JsonString = code1;
        System.debug('JSONString'+JSONString);
        CPCreatePartsWrapperInternal rw;
        If(!JSONString.contains('Chuck')){
            rw = (CPCreatePartsWrapperInternal)JSON.deserialize(JSONString,CPCreatePartsWrapperInternal.class);
            System.debug('rw.Parts-->'+rw.parts);
        }
        list<Part__c> PAL = new list<Part__c>();    
        If(String.isNotBlank(PPart_List) && !JSONString.contains('Chuck') ){
            
            for(Integer l=0; rw.Parts.size()>l;l++) {
                Part__c PA = new Part__c(
                    
                    Name =rw.Parts[l].PartNumber,
                    Description_and_Functionality__c =rw.Parts[l].PartDescription,
                    Quantity__c =rw.Parts[l].Quantity,
                    Commercial_Value__c =rw.Parts[l].UnitPrice,
                    US_HTS_Code__c =rw.Parts[l].HSCode,
                    Country_of_Origin2__c =rw.Parts[l].CountryOfOrigin,
                    ECCN_NO__c =rw.Parts[l].ECCNNo,
                    Type_of_Goods__c=rw.Parts[l].Type_of_Goods,
                    Li_ion_Batteries__c=rw.Parts[l].Li_ion_Batteries,
                    Lithium_Battery_Types__c=rw.Parts[l].Li_ion_BatteryTypes,
                    Shipment_Order__c=shipId
                    
                );
                PAL.add(PA);
            }
            
            Insert PAL;
            
        }
        else{
            Part__c PA = new Part__c(
                
                Name ='Chuck',
                Description_and_Functionality__c ='Chuck',
                Quantity__c =1,
                Commercial_Value__c =1,
                US_HTS_Code__c ='1',
                Country_of_Origin2__c ='USA',
                ECCN_NO__c ='123'
                
            );
            PAL.add(PA);
        }
        
    }
    
    
}