@RestResource(urlMapping='/ShipmentOrderListviews/*')
global class CPSOListViews {

    //Get Request
    @HttpPost
    global static void SOListViews(){
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        Blob body = req.requestBody;
        
        res.addHeader('Content-Type', 'application/json');
        String requestString = body.toString();
        CPSOListViewsWrapper rw = (CPSOListViewsWrapper)JSON.deserialize(requestString,CPSOListViewsWrapper.class);
        List<Shipment_Order__c> records = new List<Shipment_Order__c>();
        String SOID=rw.AccountId;
        String DonotShow = 'DonotShow';
       
       String query = 'Select Id,Name,Shipping_Status__c,Shipping_Status_on_ClientPortal__c,service_type__c,Client_Reference__c,Client_Reference_2__c,Ship_From_Country__c,Destination__c,Shipment_Value_USD__c,Shipping_Notes_New__c from Shipment_Order__c where Account__C=: SOID and Shipping_Status_on_ClientPortal__c != :DonotShow';
       if (!string.isEmpty(rw.ShippingStatus))        
        {
                    query+=' and Shipping_Status_on_ClientPortal__c IN' +rw.ShippingStatus;
         }
       
        if (!string.isEmpty(rw.shipfrom))        
        {
            query+=' and Ship_From_Country__c IN' +rw.shipfrom;
         
        }
      
        if (!string.isEmpty(rw.shipto))        
        {
            query+=' and Destination__c IN' +rw.shipto;
         
        }
        if (!string.isEmpty(rw.servicetype))        
        {
            query+=' and service_type__c IN' +rw.servicetype;
         
        }
        if (!string.isEmpty(rw.CourierResponsibility))        
        {
            query+=' and Who_arranges_International_courier__c IN' +rw.CourierResponsibility;
         
        }
         
    
        try{
     records = Database.query(query);
            System.debug('Final Query' +query);
            if(!records.isempty()){   
                  res.responseBody = Blob.valueOf(JSON.serializePretty(records));
                  res.statusCode = 200;
                
                API_Log__c Al = New API_Log__c();
                Al.Account__c=rw.AccountID;
                Al.Login_Contact__c=rw.ContactID;
                Al.EndpointURLName__c='ShipmentOrderListviews';
                Al.Response__c='Success - ShipmentOrderListviews Details are sent';
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al;
            }
            else{
                
                String abc = 'No records found';
                 res.responseBody = Blob.valueOf(JSON.serializePretty(abc));
                  res.statusCode = 400;
                
                API_Log__c Al = New API_Log__c();
                Al.Account__c=rw.AccountID;
                Al.Login_Contact__c=rw.ContactID;
                Al.EndpointURLName__c='ShipmentOrderListviews';
                Al.Response__c='Success - No records found for selected criteria in shipmentorder list views';
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al;
                
                
            }
        }
        
        catch (Exception e){
            String ErrorString ='Something went wrong while fetching shipmentorder list views, please contact support_SF@tecex.com';
                res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
            res.statusCode = 500;
                API_Log__c Al = New API_Log__c();
                Al.Account__c=rw.AccountID;
                Al.Login_Contact__c=rw.ContactID;
                Al.EndpointURLName__c='ShipmentOrderListviews';
                Al.Response__c='Exception - ShipmentOrderListviews Details are not sent'+e.getMessage();
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al;
            
            
            
            
        }

        
        
    }
    
}