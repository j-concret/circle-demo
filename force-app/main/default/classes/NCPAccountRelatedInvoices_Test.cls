@isTest
public with sharing class NCPAccountRelatedInvoices_Test {

    @isTest
    public static void testGetRelatedInvoices(){
        Access_Token__c at = new Access_Token__c(
            AccessToken_encoded__c='zmM0MDcxOTItZTJiZi00YWRhLTlkZGQtZGZhMmUxM2FiYTlj',
            Access_Token__c='cc407192-e2bf-4ada-9ddd-dfa2e13aba9c',
            Status__c='Active'
            );
        insert at;

        Account acc = new Account (name='TecEx Prospective Client', Type ='Supplier');
        insert acc;

        Map<String,String> body = new Map<String,String> {'Accesstoken'=>at.Access_Token__c,'AccountID'=>acc.Id};
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/NCPAccountRelatedInvoices/'; //Request URL
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueOf(JSON.serialize(body));

        RestContext.request = req;
        RestContext.response= res;

        Test.startTest();
        NCPAccountRelatedInvoices.getAccountRelatedInvoices();
        List<Invoice_New__c> cnts = (List<Invoice_New__c>)JSON.deserialize(res.responseBody.toString(),List<Invoice_New__c>.class);
        System.assertEquals(0, cnts.size());
        Test.stopTest();
    }

    @isTest
    public static void testNegativeGetRelatedInvoices(){
        Map<String,String> body = new Map<String,String>();//{'Accesstoken'=>at.Access_Token__c,'AccountID'=>acc.Id};
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/NCPAccountRelatedInvoices/'; //Request URL
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueOf(JSON.serialize(body));

        RestContext.request = req;
        RestContext.response= res;

        Test.startTest();
        	NCPAccountRelatedInvoices.getAccountRelatedInvoices();
        Test.stopTest();
    }

    @isTest
    public static void testNegative2GetRelatedInvoices(){
        Access_Token__c at = new Access_Token__c(
            AccessToken_encoded__c='zmM1MDcxOTItZTJiZi00YWRhLTlkZGQtZGZhMmUxM2FiYTlj',
            Access_Token__c='cc497192-e2bf-4ada-9ddd-dfa2e13aba9c',
            Status__c='Active'
            );
        insert at;

        Map<String,String> body = new Map<String,String> {'Accesstoken'=>at.Access_Token__c,'AccountID'=>''};
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/NCPAccountRelatedInvoices/'; //Request URL
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueOf(JSON.serialize(body));

        RestContext.request = req;
        RestContext.response= res;

        Test.startTest();
        	NCPAccountRelatedInvoices.getAccountRelatedInvoices();
        Test.stopTest();
    }
    
    @isTest
    public static void testNegative2GetException(){
        Access_Token__c at = new Access_Token__c(
            AccessToken_encoded__c='zmM1MDcxOTItZTJiZi00YWRhLTlkZGQtZGZhMmUxM2FiYTlj',
            Access_Token__c='cc497192-e2bf-4ada-9ddd-dfa2e13aba9c',
            Status__c='Active'
            );
        //insert at;

        Map<String,String> body = new Map<String,String> {'Accesstoken'=>at.Access_Token__c,'AccountID'=>''};
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/NCPAccountRelatedInvoices/'; //Request URL
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueOf(JSON.serialize(body));

        RestContext.request = req;
        RestContext.response= res;

        Test.startTest();
        	NCPAccountRelatedInvoices.getAccountRelatedInvoices();
        Test.stopTest();
    }
}