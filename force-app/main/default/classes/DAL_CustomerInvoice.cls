/**
 * Created by Caro on 2019-04-08.
 */

public with sharing class DAL_CustomerInvoice extends fflib_SObjectSelector {

    public List<Schema.SObjectField> getSObjectFieldList() {

        return new List<Schema.SObjectField>{
                Customer_Invoice__c.Name,
                Customer_Invoice__c.Id,
                Customer_Invoice__c.Age__c,
                Customer_Invoice__c.Aging_At_Last_Statement__c,
                Customer_Invoice__c.Amount_Outstanding__c,
                Customer_Invoice__c.Client__c,
                Customer_Invoice__c.Client_PO_ReferenceNumber__c,
                Customer_Invoice__c.Due_Date__c,
                Customer_Invoice__c.Invoice_Amount__c,
                Customer_Invoice__c.Invoice_Sent_Date__c,
                Customer_Invoice__c.Invoice_Status__c,
                Customer_Invoice__c.Invoice_Type__c,
                Customer_Invoice__c.RecordTypeId,
                Customer_Invoice__c.Ship_To_Country__c,
                Customer_Invoice__c.Shipment_Order__c,
                Customer_Invoice__c.BT__c
        };
    }

    public Schema.SObjectType getSObjectType() {

        return Customer_Invoice__c.sObjectType;
    }

    public List<Customer_Invoice__c> selectById(Set<Id> idSet) {

        return (List<Customer_Invoice__c>) selectSObjectsById(idSet);
    }

    public Decimal selectUnappliedCreditNotesTotalByCustomerId(Id customerId) {

        Decimal unappliedCreditNoteAmount = 0;
        for (DMN_Statement.AgingPeriod creditNote: getTotalUnappliedAmountByCustomerId(customerId)){
            if (creditNote.periodLabel == 'Credit Note' ) unappliedCreditNoteAmount =  creditNote.periodAmount;
        }
        return unappliedCreditNoteAmount;

    }

    public List<DMN_Statement.AgingPeriod> getTotalUnappliedAmountByCustomerId(Id customerId) {

        return getTotalUnappliedAmountByCustomerId(new Set<Id>{customerId}).get(customerId);
    }

    public Map<Id,List<DMN_Statement.AgingPeriod>> getTotalUnappliedAmountByCustomerId(Set<Id> customerIdSet) {

        Map<Id,List<DMN_Statement.AgingPeriod>> invoiceResult = new Map<Id,List<DMN_Statement.AgingPeriod>>();
        List<DMN_Statement.AgingPeriod> invoiceList = new List<DMN_Statement.AgingPeriod>();

        List<AggregateResult> invoiceAggregate = [SELECT
                SUM(BT__c) UnappliedAmt,
                Client__c,
                Invoice_Type__c
        FROM Customer_Invoice__c
        WHERE Client__c IN :customerIdSet
        AND Invoice_Status__c <> NULL
        GROUP BY Client__c,Invoice_Type__c];

        for (AggregateResult invoice : invoiceAggregate) {
            System.debug((Id)invoice.get('Client__c'));
            if (invoiceResult.containsKey((Id)invoice.get('Client__c'))){
                invoiceList = invoiceResult.get((Id)invoice.get('Client__c'));
                DMN_Statement.AgingPeriod invoiceType = new DMN_Statement.AgingPeriod((String)invoice.get('Invoice_Type__c'),(Decimal)invoice.get('UnappliedAmt'));
                invoiceList.add(invoiceType);
                invoiceResult.put((Id)invoice.get('Client__c'),invoiceList);
            } else {
                DMN_Statement.AgingPeriod bankTransactionType = new DMN_Statement.AgingPeriod((String)invoice.get('Invoice_Type__c'),(Decimal)invoice.get('UnappliedAmt'));
                invoiceList.add(bankTransactionType);
                invoiceResult.put((Id)invoice.get('Client__c'),invoiceList);
            }
        }

        return invoiceResult;
    }

    public List<Customer_Invoice__c> selectUnappliedCreditNotesTotalByCustomerId(Set<Id> customerIdSet) {

        return (List<Customer_Invoice__c>) Database.query(
                newQueryFactory().
                        setCondition('Client__c IN :customerIdSet AND Invoice_Status__c <> NULL AND (BT__c >= 1 OR BT__c <= -1)').
                        toSOQL());
    }

    public List<Customer_Invoice__c> selectOpenInvoicesByCustomerId(Id customerId) {

        return selectOpenInvoicesByCustomerId(new Set<Id>{customerId});

    }

    public List<Customer_Invoice__c> selectOpenInvoicesByCustomerId(Set<Id> customerIdSet) {

        return (List<Customer_Invoice__c>) Database.query(
                newQueryFactory().
                        setCondition('Client__c IN :customerIdSet AND Invoice_Status__c <> NULL AND  (BT__c >= 1 OR BT__c <= -1)').
                        toSOQL());
    }

    public List<Customer_Invoice__c> selectByCustomerId(Set<Id> customerIdSet) {

        return (List<Customer_Invoice__c>) Database.query(
                newQueryFactory().
                        setCondition('Client__c in :customerIdSet AND Invoice_Status__c <> NULL AND  (BT__c >= 1 OR BT__c <= -1)').
                        toSOQL());
    }

    public List<Customer_Invoice__c> selectByCustomerId(Id customerId) {

        return selectByCustomerId(new Set<Id>{customerId});
    }

    public List<DMN_Statement.AgingPeriod> getInvoiceAgingByCustomerId(Id customerId) {
      
      List<DMN_Statement.AgingPeriod> test = getInvoiceAgingByCustomerId(new Set<Id>{customerId}).get(customerId);
      List<DMN_Statement.AgingPeriod> test2 = new List<DMN_Statement.AgingPeriod>(); 
        
        
      System.debug('test'+ test);      
        
        If(test != null ){
            
      return test;        
        }        
        else{
         return test2;            
        } 
    }
        

      

    public Map<Id,List<DMN_Statement.AgingPeriod>> getInvoiceAgingByCustomerId(Set<Id> customerIdSet) {

        Map<Id,List<DMN_Statement.AgingPeriod>> invoiceAgingResult = new Map<Id,List<DMN_Statement.AgingPeriod>>();
        List<DMN_Statement.AgingPeriod> customerInvoiceAgingList = new List<DMN_Statement.AgingPeriod>();

        List<AggregateResult> invoiceAgingAggregate = [SELECT SUM(BT__c) Amt,
                Aging_At_Last_Statement__c,
                Client__c
        FROM Customer_Invoice__c
        WHERE Client__c IN :customerIdSet
        AND Invoice_Status__c <> NULL
        AND (BT__c >= 1  Or BT__c <= - 1)
        AND Invoice_Type__c <> 'Credit Note'
        GROUP BY Client__c, Aging_At_Last_Statement__c];

        for (AggregateResult clientInvoiceAgingPeriod : invoiceAgingAggregate) {
            if (invoiceAgingResult.containsKey((Id)clientInvoiceAgingPeriod.get('Client__c'))){
                customerInvoiceAgingList = invoiceAgingResult.get((Id)clientInvoiceAgingPeriod.get('Client__c'));
                DMN_Statement.AgingPeriod statementAgingPeriod = new DMN_Statement.AgingPeriod((String)clientInvoiceAgingPeriod.get('Aging_At_Last_Statement__c'),(Decimal)clientInvoiceAgingPeriod.get('Amt'));
                customerInvoiceAgingList.add(statementAgingPeriod);
                invoiceAgingResult.put((Id)clientInvoiceAgingPeriod.get('Client__c'),customerInvoiceAgingList);
            } else {
                DMN_Statement.AgingPeriod statementAgingPeriod = new DMN_Statement.AgingPeriod((String)clientInvoiceAgingPeriod.get('Aging_At_Last_Statement__c'),(Decimal)clientInvoiceAgingPeriod.get('Amt'));
                customerInvoiceAgingList.add(statementAgingPeriod);
                invoiceAgingResult.put((Id)clientInvoiceAgingPeriod.get('Client__c'),customerInvoiceAgingList);
            }
        }

        return invoiceAgingResult;
    }
}