@RestResource(urlMapping='/NCPMaintenance/*')
Global class NCPMaintenance {
    
    @HttpGet
    global static void NCPMaintenance(){
        
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
     
        List<Maintenance__c> At = [SELECT Id, Name, Display_Message__c,Type_Of_Maintenance__c,Schedule_end_time_UTC__c, Schedule_start_time__c, Status__c FROM Maintenance__c where Type_Of_Maintenance__c = 'Client Portal' and (status__c = 'Active' or status__c= 'On-Going') LIMIT 1];
    		 If(!At.isEmpty()){
                 
        			JSONGenerator gen = JSON.createGenerator(true);
            		gen.writeStartObject();
            
             		gen.writeFieldName('Success');
             		gen.writeStartObject();
            
                 if(At[0].Id == NULL) {gen.writeNullField('Id');} else{gen.writeStringField('Id', At[0].Name);}  
                 if(At[0].Name == NULL) {gen.writeNullField('Name');} else{gen.writeStringField('Name', At[0].Name);} 
                 if(At[0].Display_Message__c == NULL) {gen.writeNullField('DisplayMessage');} else{gen.writeStringField('DisplayMessage', At[0].Display_Message__c);} 
                 if(At[0].Schedule_start_time__c == NULL) {gen.writeNullField('Schedulestarttime');} else{gen.writeDatetimeField('Schedulestarttime', At[0].Schedule_start_time__c);} 
                 if(At[0].Schedule_end_time_UTC__c == NULL) {gen.writeNullField('ScheduleendtimeUTC');} else{gen.writeDatetimeField('ScheduleendtimeUTC', At[0].Schedule_end_time_UTC__c);} 
                 if(At[0].Status__c == NULL) {gen.writeNullField('Status');} else{gen.writeStringField('Status', At[0].Status__c);} 
                 
            		
                 
                    gen.writeEndObject();
            		gen.writeEndObject();
            		String jsonData = gen.getAsString();
            		res.responseBody = Blob.valueOf(jsonData);
            		res.addHeader('Content-Type', 'application/json');
            		res.statusCode = 200;  
                 
             }
      else {
          JSONGenerator gen = JSON.createGenerator(true);
            		gen.writeStartObject();
            
             		gen.writeFieldName('Success');
             		gen.writeStartObject();
            
                 			gen.writeStringField('Response', 'Currently we dont have any maintanance'); 
                 
          			gen.writeEndObject();
            		gen.writeEndObject();
            		String jsonData = gen.getAsString();
            		res.responseBody = Blob.valueOf(jsonData);
            		res.addHeader('Content-Type', 'application/json');
            		res.statusCode = 200;  
            
        }
    }

}