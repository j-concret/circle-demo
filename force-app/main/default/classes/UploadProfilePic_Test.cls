@isTest
public class UploadProfilePic_Test {
    static testMethod void  UploadProfilePicTest(){
        
        Profile profileId = [SELECT Id FROM Profile WHERE Name = 'Tecex Customer' LIMIT 1];
        
        Account account = new Account(name='Acme1');
        insert account;
        
        Contact contact = new Contact ( lastname ='Testing Individual',  AccountId = account.Id); 
        insert contact;  
        
        User usr = new User(LastName = 'LIVESTON',
                            FirstName='JASON',
                            Alias = 'jliv',
                            Email = 'jason.liveston@asdf.com',
                            Username = 'jason.liveston@asdf.com',
                            ProfileId = profileId.id,
                            ContactId = contact.Id,
                            TimeZoneSidKey = 'GMT',
                            LanguageLocaleKey = 'en_US',
                            EmailEncodingKey = 'UTF-8',
                            LocaleSidKey = 'en_US'
                           );
        insert usr;
        System.runAs(usr) {
            User usr1 = [select Id,UpdateProfilepic__c,ProfilePicId__c FROM USER WHERE LastName = 'LIVESTON'];
            usr1.UpdateProfilepic__c = true;
            usr1.ProfilePicId__c='abc';
            update usr1;
        }
    } 
}