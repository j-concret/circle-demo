@Istest
public class NCPFinalDestinations_Test {
    static testMethod void testPostMethodElseCondition(){
        Access_token__c accessTokenObj = new Access_token__c();
        accessTokenObj.Status__c = 'Active';
        accessTokenObj.Access_Token__c = 'asdfghj-sdfgfds-sfdfgf';
        
        Insert accessTokenObj;
        
        Account acc = new Account(Name = 'Testing');
        insert acc;
        
        Id FinrecordTypeId = Schema.SObjectType.Client_Address__c.getRecordTypeInfosByDeveloperName().get('Final_Destinations').getRecordTypeId();

        Client_Address__c clientAddr1 = new Client_Address__c(Client__c=acc.Id,RecordTypeId =FinrecordTypeId,All_Countries__c = 'Brazil');
        insert clientAddr1;
        
        NCPFinalDestinationsWrap wrapperObj = new NCPFinalDestinationsWrap();
        wrapperObj.Accesstoken = accessTokenObj.Access_Token__c;
        wrapperObj.ClientAccId = acc.Id;
        wrapperObj.Country = 'Brazil';
        
        RestRequest request = new RestRequest();
        RestResponse res = new RestResponse();
        request.requestUri ='/services/apexrest/api/FinalDestinationssList/';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueOf(JSON.serialize(wrapperObj));
        RestContext.request = request;
        RestContext.response = res;
        
        Test.startTest();
        NCPFinalDestinations.NCPFinalDestinations();
        Test.stopTest(); 
        
    }
    
    static testMethod void testPostMethodIfCondition(){
        Access_token__c accessTokenObj = new Access_token__c();
        accessTokenObj.Status__c = 'Active';
        accessTokenObj.Access_Token__c = 'asdfghj-sdfgfds-sfdfgf';
        
        Insert accessTokenObj;
        
        Account acc = new Account(Name = 'Testing');
        insert acc;
        
        Id FinrecordTypeId = Schema.SObjectType.Client_Address__c.getRecordTypeInfosByDeveloperName().get('Final_Destinations').getRecordTypeId();

        Client_Address__c clientAddr1 = new Client_Address__c(Client__c=acc.Id,RecordTypeId =FinrecordTypeId,All_Countries__c = 'Brazil',Address_Status__c = 'Active');
        insert clientAddr1;
        
        NCPFinalDestinationsWrap wrapperObj = new NCPFinalDestinationsWrap();
        wrapperObj.Accesstoken = accessTokenObj.Access_Token__c;
        wrapperObj.ClientAccId = acc.Id;
        wrapperObj.Country = 'Brazil';
        
        RestRequest request = new RestRequest();
        RestResponse res = new RestResponse();
        request.requestUri ='/services/apexrest/api/FinalDestinationssList/';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueOf(JSON.serialize(wrapperObj));
        RestContext.request = request;
        RestContext.response = res;
        
        Test.startTest();
        NCPFinalDestinations.NCPFinalDestinations();
        Test.stopTest(); 
        
    }
        
}