/**
 * Created by Caro on 2019-04-22.
 */

public with sharing class DAL_Attachment extends fflib_SObjectSelector {

    public List<Schema.SObjectField> getSObjectFieldList() {

        return new List<Schema.SObjectField>{
                Attachment.Name,
                Attachment.ParentId,
                Attachment.Id,
                Attachment.ContentType,
                Attachment.Body
        };
    }

    public Schema.SObjectType getSObjectType() {

        return Attachment.sObjectType;
    }

    public Attachment selectById(Id attachmentId) {

        return selectById(new Set<Id>{attachmentId})[0];
    }

    public List<Attachment> selectById(Set<Id> idSet) {

        return (List<Attachment>) selectSObjectsById(idSet);
    }

    public List<Attachment> selectByNameAndParent(String Name, Id parentId){
        return (List<Attachment>) Database.query(
                newQueryFactory().
                        setCondition('ParentId = :parentId AND Name = :Name').
                        toSOQL());
    }

}