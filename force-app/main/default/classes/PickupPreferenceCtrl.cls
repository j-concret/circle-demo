public with sharing class PickupPreferenceCtrl {
@AuraEnabled
public static Freight__c getFreight(String soId){
    try{
        Freight__c objFreight=  [SELECT Id,Client_Address_Pickup_Preference__c,SF_Name__c,SF_AddressLine1__c,Ship_From_Address__c,Ship_From_Address__r.Name,Ship_From_Address__r.Pickup_Preference__c FROM Freight__c WHERE Shipment_Order__c =: soid LIMIT 1];
        return objFreight;            
    } catch(Exception e) {throw new AuraHandledException(e.getMessage());           
    }
    
}

@AuraEnabled
public static void updateFreight(Freight__c freight, String tskId){
    Client_Address__c objClinetAddressToUpdate = New Client_Address__c(id =freight.Ship_From_Address__c , Pickup_Preference__c = freight.Client_Address_Pickup_Preference__c );   
    try {
        update freight;
    } catch (Exception e) {throw new AuraHandledException(e.getMessage());
        }
    try {
      update objClinetAddressToUpdate;
    } catch (Exception e) {throw new AuraHandledException(e.getMessage());
    }
    //update related task to Resolved if pickup prefenrence has been selected.
    if(freight.Client_Address_Pickup_Preference__c != null) {
        try {
            Task objTask = New Task(id=tskId,State__c= 'Resolved');
            update objTask;
            } catch (Exception e) {throw new AuraHandledException(e.getMessage());
        }
    }  
    
}
    
@AuraEnabled 
public static Map<String, String> getPickupPreferencePicklist(){
    Map<String, String> options = new Map<String, String>();
    
    Schema.DescribeFieldResult fieldResult = Client_Address__c.Pickup_Preference__c.getDescribe();
    
    List<Schema.PicklistEntry> picklistValues = fieldResult.getPicklistValues();
    for (Schema.PicklistEntry p: picklistValues) {           
        options.put(p.getValue(), p.getLabel());
    }
    return options;
}

}