public class FreightPBTrigger {



    public static Map<Id, Freight__C> frWithClones = new Map<Id, Freight__C>();
    public static Map<Id,Shipment_Order__c> soTOUpdateWithCalculatedFrFee = new Map<Id,Shipment_Order__c>();

    public class ShipToFromCountryWrapper {
        public string shipTo;
        public string shipFrom;

        public ShipToFromCountryWrapper(String shipTo, String shipFrom){
            this.shipTo = shipTo;
            this.shipFrom = shipFrom;
        }

    }

    public class ShipToFromRegionWrapper {
        public string shipToRegion;
        public string shipFromRegion;

        public ShipToFromRegionWrapper(String shipToRegion, String shipFromRegion){
            this.shipToRegion = shipToRegion;
            this.shipFromRegion = shipFromRegion;
        }

    }

    public class FreightVariablesWrapper {
        Decimal chargeableWeightSum;
        Boolean isOverSized;

        public FreightVariablesWrapper(Decimal chargeableWeightSum, Boolean isOverSized){
            this.chargeableWeightSum = chargeableWeightSum;
            this.isOverSized = isOverSized;
        }
    }


    public static void getFreightValueFlow(List<Freight__c> frList, Boolean isInsert){
        Set<Id> ids = new Set<Id>(Trigger.newMap.keySet());
        String parentFields = 'Shipment_Order__r.Hub_Country__c, Shipment_Order__r.Destination__c, Shipment_Order__r.Ship_From_Country__c,'
            +'Shipment_Order__r.Account__c, Shipment_Order__r.Li_ion_Batteries__c, Shipment_Order__r.CPA_v2_0__r.Freight_Only_Lane__c,'
            +'Shipment_Order__r.IOR_CSE__c, Shipment_Order__r.Account__r.Shipping_Premium_Discount__c,'
            +'Shipment_Order__r.CPA_v2_0__r.Preferred_Freight_Forwarder__r.Name, Shipment_Order__r.CPA_v2_0__r.Preferred_Freight_Forwarder__c,'
            +'Shipment_Order__r.CPA_v2_0__r.Preferred_Courier__c, Shipment_Order__r.CPA_v2_0__r.Preferred_Courier__r.Name';

        Map<Id,Freight__c> freightWithParents = new Map<Id,Freight__c>((List<Freight__c>)Database.query('SELECT Id,'+parentFields+' FROM Freight__c WHERE Id IN:ids'));

        Map<Id, ShipToFromCountryWrapper> freightWithShipToFromCountry = new Map<Id, ShipToFromCountryWrapper>();

        Map<Id,Id> shipmentIdWithFreightForSOP = new Map<Id,Id>();
        Map<Id,Freight__c> freightWithRecentSOP = new Map<Id, Freight__c>();
        Map<Id, RegionalFreightValues__c> freightWithRegionalFRValues = new Map<Id, RegionalFreightValues__c>();

        Map<Id, FFVolumeDiscountPremium__c> freightWithFFVolumeDiscount = new Map<Id, FFVolumeDiscountPremium__c>();


        Map<Id, FreightVariablesWrapper> freightWithVariables = new Map<Id, FreightVariablesWrapper>();
        Map<Id,Id> allSOWithFreight = new Map<Id,Id>();

        List<Shipment_Order_Package__c> sopList = new List<Shipment_Order_Package__c>();
        Map<String,CountryandRegionMap__c> countryRegionMap = CountryandRegionMap__c.getall();
        Map<String,RegionalFreightValues__c> regionalFreightMap = RegionalFreightValues__c.getall();

        for(Freight__c fr : frList) {

            allSOWithFreight.put(fr.Shipment_Order__c, fr.Id);

            if(!fr.Pick_Up_Request__c) {
                shipmentIdWithFreightForSOP.put(fr.Shipment_Order__c, fr.Id);
            }

            Freight__c freightParent  = freightWithParents.get(fr.Id);
            if(!isInsert) {
                fr.putSObject('Shipment_Order__r',freightParent.getSobject('Shipment_Order__r'));
            }
        }

        sopList = [SELECT Id, Freight__c,Shipment_Order__c,Chargeable_Weight_KGs__c,Oversized__c FROM Shipment_Order_Package__c WHERE Shipment_Order__c IN :allSOWithFreight.keySet() ORDER BY Shipment_Order__c
                  ];

        if(!shipmentIdWithFreightForSOP.isEmpty()){

            List<Shipment_Order_Package__c> SOPToUpdate = new List<Shipment_Order_Package__c>();

            for(Shipment_Order_Package__c sop : sopList){
                if(sop.Freight__c == null){
                    sop.Freight__c = shipmentIdWithFreightForSOP.get(sop.Shipment_Order__c);
                    SOPToUpdate.add(sop);
                }
            }

            // Make sure to not refire the FR Trigger/Shipment Order From SOP Trigger
            // If we are updating SOP from here
            // Getting the Rollups VIA Query only.
            if(!SOPToUpdate.isEmpty()){
                RecusrsionHandler.skipSOPTriggerExecutionOnSOCreation = true;
                update sopList;
                sopList = [SELECT Id, Freight__c,Shipment_Order__c,Chargeable_Weight_KGs__c,Oversized__c FROM Shipment_Order_Package__c WHERE Shipment_Order__c IN :allSOWithFreight.keySet() ORDER BY Shipment_Order__c
                          ];
            }
        }


        //prepare oversized Freight Set and Chargeable_Weight_KGs__c sum
        for(Shipment_Order_Package__c sop : sopList) {
            if(sop.Freight__c != null) {

                if(freightWithVariables.containsKey(sop.Freight__c)) {
                    FreightVariablesWrapper freightVariable = freightWithVariables.get(sop.Freight__c);
                    if(!freightVariable.isOverSized) {
                        freightVariable.isOverSized = sop.Oversized__c;
                    }
                    freightVariable.chargeableWeightSum += sop.Chargeable_Weight_KGs__c;

                    freightWithVariables.put(sop.Freight__c, freightVariable);
                }else{
                    freightWithVariables.put(sop.Freight__c, new FreightVariablesWrapper(sop.Chargeable_Weight_KGs__c, sop.Oversized__c));
                }
            }
        }







        //get Default Values
        //Here we need to pick first record
        List<DefaultFreightValues__c> defaultFreightValuesList = DefaultFreightValues__c.getall().values();

        // get FFVolumeDiscountPremium__c
        // here we need to filter record as per two conditions.
        List<FFVolumeDiscountPremium__c> ffVolueDiscountPremium = FFVolumeDiscountPremium__c.getall().values();
        for(Freight__c fr : frList) {

            Decimal miniumChageableWeight = 0;

            System.debug('@@@@@ '+fr.Shipment_Order__r.CPA_v2_0__r.Freight_Only_Lane__c);



            if(freightWithVariables.containsKey(fr.Id)) {
                if(freightWithVariables.get(fr.Id).chargeableWeightSum < 45 && fr.Shipment_Order__r.CPA_v2_0__r.Freight_Only_Lane__c) {
                    miniumChageableWeight = 45;
                }else{
                    miniumChageableWeight = freightWithVariables.get(fr.Id).chargeableWeightSum;
                }
            }else{
                if(fr.Chargeable_weight_in_KGs_packages__c < 45 && fr.Shipment_Order__r.CPA_v2_0__r.Freight_Only_Lane__c) {
                    miniumChageableWeight = 45;
                }else{
                    miniumChageableWeight = fr.Chargeable_weight_in_KGs_packages__c;
                }
            }



            for(FFVolumeDiscountPremium__c ffVDP : ffVolueDiscountPremium) {
                if(ffVDP.Ceiling_Kgs__c >= miniumChageableWeight && ffVDP.Floor_Kgs__c <= miniumChageableWeight) {
                    freightWithFFVolumeDiscount.put(fr.Id, ffVDP);
                    break;
                }
            }
        }

        List<Freight__c> frToUpdate = new List<Freight__c>();

        //Prepare freigt fields update here.
        for(Freight__c fr : frList) {

            Freight__c freightParent  = freightWithParents.get(fr.Id);

            if(isInsert) {
                fr = fr.clone(true, true, true, false);
                frToUpdate.add(fr);
            }

            Boolean isOversized = false;

            if(freightWithVariables.containsKey(fr.Id)) {
                fr.Chargeable_weight_in_KGs_packages__c = freightWithVariables.get(fr.Id).chargeableWeightSum;
                isOversized = freightWithVariables.get(fr.Id).isOverSized;
            }

            fr.Client__c = isInsert ? freightParent.Shipment_Order__r.Account__c : fr.Shipment_Order__r.Account__c;

            Boolean isLiionBatteries = isInsert ? (freightParent.Shipment_Order__r.Li_ion_Batteries__c == 'Yes') : (fr.Shipment_Order__r.Li_ion_Batteries__c == 'Yes');

                if(isLiionBatteries) {
                    fr.Courier_Dangerous_Goods_premium__c = defaultFreightValuesList[0].Courier_Dangerous_Goods_premium__c;
                }else{
                    fr.Courier_Dangerous_Goods_premium__c = 0;
                }

            if(isOversized) {
                fr.Courier_Oversized_premium__c = defaultFreightValuesList[0].Courier_Oversized_premium__c;
                fr.FF_Oversized_Fixed_Charge_Premium__c = defaultFreightValuesList[0].FF_Oversized_Fixed_Charge_Premium__c;
                fr.FF_Oversized_Premium__c = defaultFreightValuesList[0].FF_Oversized_premium__c;
            }else{
                fr.Courier_Oversized_premium__c = 0;
                fr.FF_Oversized_Fixed_Charge_Premium__c = 0;
                fr.FF_Oversized_Premium__c = 0;
            }

            fr.Courier_Rate_kg__c = defaultFreightValuesList[0].Courier_Base_Rate_KG__c;

            String shipTo = '';
            String shipFrom = '';

            String hubCountry = isInsert ? freightParent.Shipment_Order__r.Hub_Country__c : fr.Shipment_Order__r.Hub_Country__c;
            String shipFromCountry = isInsert ? freightParent.Shipment_Order__r.Ship_From_Country__c : fr.Shipment_Order__r.Ship_From_Country__c;
            String destination = isInsert ? freightParent.Shipment_Order__r.Destination__c : fr.Shipment_Order__r.Destination__c;
            // To fill ship to Country. Required for Country and Region Map Records.
            if(fr.Pick_Up_Request__c) {
                shipTo = fr.Ship_To__c;
                shipFrom = fr.Ship_From_Country__c;
            }else if(hubCountry != null ) {
                shipTo = hubCountry;
                shipFrom = shipFromCountry;
            }else{
                shipTo = destination;
                shipFrom = shipFromCountry;
            }

            System.debug('shipTo -->'+shipTo);
            System.debug('shipFrom -->'+shipFrom);
            System.debug('countryRegionMap -->'+countryRegionMap);
            String shipToRegion = countryRegionMap.get(shipTo).Region__c;
            String shipFromRegion = countryRegionMap.get(shipFrom).Region__c;

            //region freight values fill here.;
            RegionalFreightValues__c regionFreightValue = regionalFreightMap.get(shipFromRegion+' to '+shipToRegion);

            fr.FF_Regional_Discount_Premium__c = regionFreightValue.FF_Regional_Discount_Premium__c;
            fr.Courier_Regional_Rate_D_P__c = regionFreightValue.Courier_Discount_Premium__c;
            fr.FF_Fixed_Charge_Regional_Premium__c = regionFreightValue.FF_Fixed_Premium__c;
            fr.Courier_Fixed_Charge_Regional_Premium__c = regionFreightValue.Courier_Fixed_Premium__c;

            if(freightWithFFVolumeDiscount.containsKey(fr.Id)) {
                FFVolumeDiscountPremium__c ffVDP =  freightWithFFVolumeDiscount.get(fr.Id);
                fr.FF_Volume_Discount__c = ffVDP.Discount_Premium__c;
                fr.Courier_Fixed_Charge__c = ffVDP.Courier_Fixed_Charge__c;
                fr.Courier_Volume_Discount__c = ffVDP.Courier_Discount_Premium__c;
            }

            if(fr.Freight_Coordinator__c == null) {
                fr.Freight_Coordinator__c = isInsert ? freightParent.Shipment_Order__r.IOR_CSE__c : fr.Shipment_Order__r.IOR_CSE__c;
            }

            fr.Freight_forwarder_Fixed_charge__c = fr.Chargeable_weight_in_KGs_packages__c > 80 ? 175 : 375;
            fr.Freight_forwarder_Rate_KG_charged__c = defaultFreightValuesList[0].FF_Base_Rate_KG__c;

            if((!isInsert && fr.Shipment_Order__r.Account__r.Shipping_Premium_Discount__c != null) ||
               (isInsert && freightParent.Shipment_Order__r.Account__r.Shipping_Premium_Discount__c != null)) {
                   fr.International_Shipping_Discount_Override__c = isInsert ? freightParent.Shipment_Order__r.Account__r.Shipping_Premium_Discount__c : fr.Shipment_Order__r.Account__r.Shipping_Premium_Discount__c;
               }else{
                   fr.International_Shipping_Discount_Override__c = null;
               }

            if(isLiionBatteries) {
                fr.Li_Ion_Batteries__c = 'Yes';
            }else{
                fr.Li_Ion_Batteries__c = 'No';
            }

            //Dangerous_Goods__c - check the hasDangerousGood (fr.Batteries there but So.Batteries not)
            //We can merge the below assignment in above after confirmation
             //Dangerous_Goods__c only update manually by Freight team.
            /*  if(fr.Li_Ion_Batteries__c == 'Yes') {
                fr.Dangerous_Goods__c = True;
            }*/

            //NOTE Li_Ion_Battery_Type__c NOT FOUND

            fr.Oversized__c = isOversized;

            String preferredSupplierName = '';

            Id cpaPreferredFFId = isInsert ?  freightParent.Shipment_Order__r.CPA_v2_0__r.Preferred_Freight_Forwarder__c : fr.Shipment_Order__r.CPA_v2_0__r.Preferred_Freight_Forwarder__c;
            Boolean cpaFFOnlyLane = isInsert ?  freightParent.Shipment_Order__r.CPA_v2_0__r.Freight_Only_Lane__c : fr.Shipment_Order__r.CPA_v2_0__r.Freight_Only_Lane__c;

            if(fr.Suggested_Supplier_Name__c != null) {
                system.debug('286');
                preferredSupplierName = fr.Suggested_Supplier_Name__c;
            }else if(fr.Chargeable_weight_in_KGs_packages__c >= fr.Weight_threshold__c || cpaFFOnlyLane) {
                if(cpaPreferredFFId == null) {
                    system.debug('290');
                    preferredSupplierName = regionFreightValue != null ? regionFreightValue.Preferred_Freight_Forwarder_Name__c : '';
                }else{
                    system.debug('293');
                    preferredSupplierName = isInsert ? freightParent.Shipment_Order__r.CPA_v2_0__r.Preferred_Freight_Forwarder__r.Name : fr.Shipment_Order__r.CPA_v2_0__r.Preferred_Freight_Forwarder__r.Name;
                }
            }else{

                Id cpaPreferredCourierId = isInsert ? freightParent.Shipment_Order__r.CPA_v2_0__r.Preferred_Courier__c : fr.Shipment_Order__r.CPA_v2_0__r.Preferred_Courier__c;

                if(cpaPreferredCourierId == null) {
                    system.debug('301');
                    preferredSupplierName = regionFreightValue != null ? regionFreightValue.Preferred_Courier_Name__c : '';
                }else{
                    system.debug('304');
                    preferredSupplierName = isInsert ? freightParent.Shipment_Order__r.CPA_v2_0__r.Preferred_Courier__r.Name : fr.Shipment_Order__r.CPA_v2_0__r.Preferred_Courier__r.Name;
                }
            }

            if(!String.isBlank(preferredSupplierName) &&
               (preferredSupplierName == 'Kintetsu World Express South Africa Proprietary Limited' ||
                preferredSupplierName == 'Kintetsu SA')) {
                    fr.Dedicated_PickUp__c = True;
                    fr.FF_Dedicated_Pickup__c = defaultFreightValuesList[0].FF_Dedicated_Pickup__c;
                }else{
                    fr.Dedicated_PickUp__c = False;
                    fr.FF_Dedicated_Pickup__c = 0;
                }


            if(isLiionBatteries) {
                fr.FF_Dangerous_Goods_Fixed_Charge_Premium__c = defaultFreightValuesList[0].FF_Dangerous_Goods_Fixed_Charge_Premium__c;
                fr.FF_Dangerous_Goods_premium__c = defaultFreightValuesList[0].FF_Dangerous_Goods_premium__c;
            }else{
                fr.FF_Dangerous_Goods_Fixed_Charge_Premium__c = 0;
                fr.FF_Dangerous_Goods_premium__c = 0;
            }

            if(fr.Non_Stackable__c) {
                fr.Courier_Non_stackable_premium__c = defaultFreightValuesList[0].Courier_Non_stackable_premium__c;
                fr.FF_Non_Stackable_Premium__c = defaultFreightValuesList[0].FF_Non_stackable_premium__c;
                fr.FF_Non_stackable_Fixed_Charge_Premium__c = defaultFreightValuesList[0].FF_Non_stackable_Fixed_Charge_Premium__c;
            }else{
                fr.FF_Non_Stackable_Premium__c = 0;
                fr.FF_Non_stackable_Fixed_Charge_Premium__c = 0;
                fr.Courier_Non_stackable_premium__c = 0;
            }

            if(cpaFFOnlyLane) {
                fr.FF_Only_Lane__c = true;
            }else{
                fr.FF_Only_Lane__c = false;
            }


            fr.Service_type__c = (fr.Chargeable_weight_in_KGs_packages__c >= fr.Weight_threshold__c || fr.FF_Only_Lane__c ) ? 'Freight Forwarder' : 'Courier';

            if(freightWithShipToFromCountry.containsKey(fr.Id)) {
                fr.Ship_To__c = freightWithShipToFromCountry.get(fr.Id).shipTo;
                fr.Ship_From__c = freightWithShipToFromCountry.get(fr.Id).shipFrom;
            }

            if(!String.isBlank(preferredSupplierName)) {
                fr.Suggested_Supplier_Name__c = preferredSupplierName;
            }

        }

        if(!frToUpdate.isEmpty()) {
            ZenkraftOnFreightHandler.skipTriggerExecution = true;
            Formula.recalculateFormulas(frToUpdate);
            ZenkraftOnFreightHandler.updateFreightFields(frToUpdate);
            update frToUpdate;
        }

        for ( Freight__C fr : isInsert ? frToUpdate : frList ) {
            frWithClones.put(fr.Id, fr.clone(true, true, true, false));
        }
        Formula.recalculateFormulas(frWithClones.values());
        //Formula.recalculateFormulas(frList);
        ZenkraftOnFreightHandler.updateFreightFields(frWithClones.values());
        Formula.recalculateFormulas(frWithClones.values());

        for(Freight__c freight : frList) {
            Freight__c fr = frWithClones.get(freight.Id);
            System.debug('International_freight_fee_invoiced__c '+fr.International_freight_fee_invoiced__c);
            if(soTOUpdateWithCalculatedFrFee.containsKey(fr.Shipment_Order__c)) {
                soTOUpdateWithCalculatedFrFee.get(fr.Shipment_Order__c).Freight_Fee_Calculated_Freight_Request__c += fr.International_freight_fee_invoiced__c;
                soTOUpdateWithCalculatedFrFee.get(fr.Shipment_Order__c).International_Delivery_Cost__c = fr.Who_arranges_International__c == 'Client' ? 0 : soTOUpdateWithCalculatedFrFee.get(fr.Shipment_Order__c).Freight_Fee_Calculated_Freight_Request__c * 0.6;
            }else{
                soTOUpdateWithCalculatedFrFee.put(fr.Shipment_Order__c,new Shipment_Order__c(Id = fr.Shipment_Order__c, Freight_Fee_Calculated_Freight_Request__c =  fr.International_freight_fee_invoiced__c, International_Delivery_Cost__c = fr.Who_arranges_International__c == 'Client' ? 0 : (fr.International_freight_fee_invoiced__c * 0.6)));
            }
        }

        system.debug('soTOUpdateWithCalculatedFrFee ' + JSON.serialize(soTOUpdateWithCalculatedFrFee));

        if(!soTOUpdateWithCalculatedFrFee.isEmpty() && !RecusrsionHandler.freightUpdateFromSO) {
            RecusrsionHandler.skipTriggerExecution = false;
            RecusrsionHandler.tasksCreated = false;
            update soTOUpdateWithCalculatedFrFee.values();
        }

    }

    public static Boolean isChanged(Id recordId,String fieldName){
        if(Trigger.isExecuting && Trigger.isUpdate) {
            return Trigger.newMap.get(recordId).get(fieldName) != Trigger.oldMap.get(recordId).get(fieldName);
        }
        return false;
    }

    public static void updateSo(List<Freight__c> frList){

        Set<Id> soIdsTOProcess = new Set<Id>();
        List<Shipment_Order__c> soList = New List<Shipment_Order__c>();

        //Update so : Rollup freight flow
        //need to confirm Manual_Cost_Override__c logic
        for(Freight__c fr : frList) {
            if(isChanged(fr.Id, 'Manual_Cost_Override__c') &&
               fr.Shipment_Order__c != null &&
               (fr.SO_Status__c != 'Cost Estimate Abandoned' || fr.SO_Status__c != 'Shipment Abandoned')) {
                   //soIdsTOProcess.add(fr.Shipment_Order__c);
                   soList.add(new Shipment_Order__c(Id = fr.Shipment_Order__c,Freight_Fee_Calculated_Freight_Request__c = fr.International_freight_fee_invoiced__c));
               }
        }
        if(!soList.isEmpty()) update soList;
    }


    public static void updateSupplier(List<Freight__c> frList){
        Map<String, Id> supplierWithId = new Map<String, Id>();
        List<String> accountNames = new List<String>();
        accountNames.add('Comexas Airfreight');
        accountNames.add('Dachser Inteligent logistics');
        accountNames.add('DB Schenker');
        accountNames.add('DHL');
        accountNames.add('DHL - USA');
        accountNames.add('Embassy Freight Systems USA Inc');
        accountNames.add('Embassy Freight USA');
        accountNames.add('Embassy Freight Services UK');
        accountNames.add('Expeditors');
        accountNames.add('Expeditors Switzerland');
        accountNames.add('Expeditors USA');
        accountNames.add('Fedex');
        accountNames.add('FedEx LA');
        accountNames.add('Hecksher Linjeagenturer A/S');
        accountNames.add('Kintetsu SA');
        accountNames.add('Kintetsu World Express South Africa Proprietary Limited');
        accountNames.add('Navegate');
        accountNames.add('Stream');
        accountNames.add('Test');
        accountNames.add('Test Supplier');
        accountNames.add('TNT');
        accountNames.add('UPS Japan');

        for(Account acc : [SELECT Id, Name FROM Account WHERE Name IN :accountNames]){
            supplierWithId.put(acc.Name, acc.Id);
        }

        /*supplierWithId.put('Comexas Airfreight', '0011v00001u3buDAAQ');
        supplierWithId.put('Dachser Inteligent logistics', '0010Y00000TS8bZQAT');
        supplierWithId.put('DB Schenker', '0010Y00000TS8bTQAT');
        supplierWithId.put('DHL', '0010Y00000TS8bEQAT');
        supplierWithId.put('DHL - USA', '0010Y00000TS8aoQAD');
        supplierWithId.put('Embassy Freight Systems USA Inc', '0010Y00000TS8bOQAT');
        supplierWithId.put('Embassy Freight USA', '0010Y00000TS8aZQAT');
        supplierWithId.put('Embassy Freight Services UK', '0010Y00000TS8afQAD');
        supplierWithId.put('Expeditors', '0010Y00000TS8b8QAD');
        supplierWithId.put('Expeditors Switzerland', '0010Y00000ZLJF0QAP');
        supplierWithId.put('Expeditors USA', '0010Y00000TS8aYQAT');
        supplierWithId.put('Fedex', '0010Y00000TS8auQAD');
        supplierWithId.put('FedEx LA', '0011v00002ACZGhAAP');
        supplierWithId.put('Hecksher Linjeagenturer A/S', '0011v00002BarJMAAZ');
        supplierWithId.put('Kintetsu SA', '0010Y0000155QHHQA2');
        supplierWithId.put('Kintetsu World Express South Africa Proprietary Limited', '0010Y00000TS8bHQAT');
        supplierWithId.put('Navegate', '0010Y00000qyxJFQAY');
        supplierWithId.put('Stream', '0010Y00001fcNjiQAE');
        supplierWithId.put('Test', '0011v000025ByMgAAK');
        supplierWithId.put('Test Supplier', '0010Y00000qXM1vQAG');
        supplierWithId.put('TNT', '0011v00002EQpmVAAT');
        supplierWithId.put('UPS Japan', '0011v00002G9TlOAAV');*/



        for(Freight__c fr : frList) {
            If(
                (isChanged(fr.Id, 'Status__c') &&
                 (fr.status__c == 'Submitted' || fr.status__c == 'Live')
                 && fr.Logistics_provider__c ==  null)
                || (isChanged(fr.Id, 'Suggested_Supplier_Name__c') &&
                    (fr.status__c == 'Submitted' || fr.status__c == 'Live')
                   )
            ){

                if(fr.Suggested_Supplier_Name__c != null && supplierWithId.containsKey(fr.Suggested_Supplier_Name__c)) {
                    // COMMENTED DUE TO DATA
                    fr.Logistics_provider__c = supplierWithId.get(fr.Suggested_Supplier_Name__c);
                }
            }
        }
    }

    public static void freightUpdateFlowCode(List<Freight__c> frList, Boolean isInsert){
        soTOUpdateWithCalculatedFrFee.clear();
        List<Freight__c> frToProcess = new List<Freight__c>();
        for(Freight__c fr :frList) {
            if(isGetFreightValuesMatched(isInsert, fr)) {
                frToProcess.add(fr);
            }
        }

        if(!frToProcess.isEmpty()) {
            getFreightValueFlow(frToProcess, isInsert);
        }
    }


    public static Boolean isGetFreightValuesMatched(Boolean isInsert, Freight__c fr){

        if(isInsert) {
            return true;
        }
        else if(fr.shipment_order__c == null || fr.SO_Status__c == 'Cost Estimate Abandoned' || fr.SO_Status__c == 'Shipment Abandoned') {
            return false;
        }
        else if(
            isChanged(fr.Id,'FF_Only_Lane__c')
            ||
            isChanged(fr.Id,'Selected_Rate_From_Existing__c')
            ||
            isChanged(fr.Id,'Freight_forwarder_calculated_charge__c')
            ||
            isChanged(fr.Id,'Service_type__c')
            ||
            isChanged(fr.Id,'Courier_Type__c')
            ||
            isChanged(fr.Id,'Calculated_Courier_Cost__c')
            ||
            isChanged(fr.Id,'International_freight_fee_invoiced__c')
            ||
            isChanged(fr.Id,'International_Freight_Fee_Invoiced_Num__c')
            ||
            isChanged(fr.Id,'International_Freight_Fee_Invoiced_ZK__c')
            ||
            isChanged(fr.Id,'Chargeable_weight_in_KGs_packages__c')
            ||
            isChanged(fr.Id,'Ship_From_Country__c')
            ||
            isChanged(fr.Id,'Ship_To__c')
            ||
            isChanged(fr.Id,'Oversized__c')
            ||
            isChanged(fr.Id,'Non_Stackable__c')
            ||
            isChanged(fr.Id,'Dangerous_Goods__c')
            ||
            isChanged(fr.Id,'Dedicated_PickUp__c')
            ||
            isChanged(fr.Id,'Li_Ion_Batteries__c')
            ||
            isChanged(fr.Id,'Weight_threshold__c')
            ||
            isChanged(fr.Id,'Suggested_Supplier_Name__c')
            ||
            isChanged(fr.Id,'Shipment_Order__c')) {
                return true;
            }
        return false;
    }


}