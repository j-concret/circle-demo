@istest
public class Lex_ServiceTeamDBTest {
    
    @istest
    public static void setup(){
        UserRole r = new UserRole(DeveloperName = 'MyCustomRole', Name = 'My Role');
        insert r;
        
        User u = new User(
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id,
            LastName = 'Berkeley',
            Email = 'puser000@amamama.com',
            Username = 'puser000@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            FirstName='Vat',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            UserRoleId = r.Id
        );
        insert u;
        System.runAs(u) {
            
        
        
            
            List<Dimension__c> dimensionList = new List<Dimension__c>();
            dimensionList.add(new Dimension__c(Name = 'MASELIM',  Dimension_Description__c = 'Test 610000', Short_Dimension_Description__c = 'Test 610000', Dimension_Type_Code__c = 'PRO'));
            insert dimensionList;
            
            CountryandRegionMap__c crm = new CountryandRegionMap__c();
            crm.Name = 'Brazil';
            crm.Country__c = 'Brazil';
            crm.European_Union__c = 'No';
            crm.Region__c = 'South America';
            
            insert crm;
            
            CountryandRegionMap__c crm1 = new CountryandRegionMap__c();
            crm1.Name = 'Australia';
            crm1.Country__c = 'Australia';
            crm1.European_Union__c = 'No';
            crm1.Region__c = 'Oceania';
            
            insert crm1;
            
             CountryandRegionMap__c crm3 = new CountryandRegionMap__c();
        crm3.Name = 'United States';
        crm3.Country__c = 'United States';
        crm3.European_Union__c = 'No';
        crm3.Region__c = 'Oceania';
        
        insert crm3;
            
            DefaultFreightValues__c df = new DefaultFreightValues__c();
            df.Name = 'Test';
            df.Courier_Base_Rate_KG__c = 12;
            df.Courier_Dangerous_Goods_premium__c = 12;
            df.Courier_Fixed_Charge__c = 12;
            df.Courier_Non_stackable_premium__c = 12;
            df.Courier_Oversized_premium__c = 12;
            df.FF_Base_Rate_KG__c = 12;
            df.FF_Dangerous_Goods_Fixed_Charge_Premium__c = 12;
            df.FF_Dangerous_Goods_premium__c = 12;
            df.FF_Dedicated_Pickup__c = 12;
            df.FF_Fixed_Charge__c = 12;
            df.FF_Non_stackable_Fixed_Charge_Premium__c = 12;
            df.FF_Non_stackable_premium__c = 12;
            df.FF_Oversized_Fixed_Charge_Premium__c = 12;
            df.FF_Oversized_premium__c = 12;
            insert df;
            
            RegionalFreightValues__c rfm = new RegionalFreightValues__c();
            rfm.Name = 'Oceania to South America';
            rfm.Courier_Discount_Premium__c = 12;
            rfm.Courier_Fixed_Premium__c = 10;
            rfm.Destination__c = 'Brazil';
            rfm.FF_Fixed_Premium__c = 123;
            rfm.FF_Regional_Discount_Premium__c = 12;
            rfm.Origin__c = 'Australia';
            rfm.Preferred_Courier_Id__c = 'test';
            rfm.Preferred_Courier_Name__c = 'test';
            rfm.Preferred_Freight_Forwarder_Id__c = 'test';
            rfm.Preferred_Freight_Forwarder_Name__c = 'test';
            rfm.Risk_Adjustment_Factor__c = 1;
            insert rfm;
            
            List<Account> accs = new List<Account>();
            Id EORRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('IOR/EOR Supplier').getRecordTypeId();
            Id ManufacturerRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Manufacturer').getRecordTypeId();
            
            Account account = new Account (name='Acme1', Type ='Client', 
                                           CSE_IOR__c = u.id, 
                                           Service_Manager__c = u.id, 
                                           Financial_Manager__c=u.id,New_Invoicing_Structure__c = true, 
                                           Financial_Controller__c=u.id,Liability_Product_Agreement__c = 'Ad Hoc Agreement', 
                                           Tax_recovery_client__c  = FALSE,Dimension__c = dimensionList[0].Id );
            Account account2 = new Account (name='Test Supplier', Type ='Supplier', ICE__c = u.id, RecordTypeId = EORRecordTypeId,New_Invoicing_Structure__c=true,Liability_Product_Agreement__c = 'Annual Rate Agreement');
            Account account3 = new Account (name='Test Manufacturer', Type ='Manufacturer', RecordTypeId = ManufacturerRecordTypeId, Manufacturer_Alias__c = 'Tester,Who' );
            Account account4 = new Account (name='TecEx Prospective Client', Type ='Client',New_Invoicing_Structure__c = true);
            accs.add(account);
            accs.add(account2);
            accs.add(account3);
            accs.add(account4);
            insert accs;
            
            List<Contact> cons = new List<Contact>();
            
            Contact contact = new Contact (Client_Notifications_Choice__c = 'opt-in', email = 'test@test.com', lastname ='Testing Individual',  AccountId = accs[0].Id);
            Contact contact2 = new Contact (Client_Notifications_Choice__c = 'opt-in', email = 'test@test.com', lastname ='Testing supplier',  AccountId = accs[1].Id);
            cons.add(contact);
            cons.add(contact2);
            insert cons;
            
            IOR_Price_List__c iorpl = new IOR_Price_List__c ( Client_Name__c = account4.Id, Name = 'Bulgaria', IOR_Fee__c = 0.15, On_Charge_Mark_up__c = 0.15,
                                                             TecEx_Shipping_Fee_Markup__c =0.40, IOR_Min_Fee__c = 100,  Admin_Fee__c =100, Set_up_fee__c =100, Bank_Fees__c =200, Tax_Rate__c = 0.20,
                                                             Estimated_Customs_Brokerage__c =250, Estimated_Customs_Clearance__c =250, Estimated_Customs_Handling__c =250, Estimated_Import_License__c =250, Destination__c = 'Brazil',
                                                            Active__c = true);
            
            insert iorpl;
            IOR_Price_List__c iorpl1 = new IOR_Price_List__c ( Client_Name__c = accs[3].Id, Name = 'Belgium', IOR_Fee__c = 0.15, On_Charge_Mark_up__c = 0.15,
                                                              TecEx_Shipping_Fee_Markup__c =0.40, IOR_Min_Fee__c = 100,  Admin_Fee__c =100, Set_up_fee__c =100, Bank_Fees__c =200, Tax_Rate__c = 0.20,
                                                              Estimated_Customs_Brokerage__c =250, Estimated_Customs_Clearance__c =250, Estimated_Customs_Handling__c =250, Estimated_Import_License__c =250, Destination__c = 'Belgium' );
            
            insert iorpl1;
            
            Country__c country = new Country__c(Brokerage_Costs__c = 618, CIF_Adjustment_Factor__c = 0.035, CIF_Absolute_value_adjustment__c = 200, Clearance_Costs__c = 488, 
                                                Country__c = 'Brazil', Name = 'Brazil', Handling_Costs__c = 1180, License_Permit_Costs__c = 0);
            insert country;
            
            List<CPA_v2_0__c> cpaList = new List<CPA_v2_0__c>();
            CPA_v2_0__c RelatedCPA = new CPA_v2_0__c(Name = 'Brazil', Country_Applied_Costings__c=true );
            CPA_v2_0__c RelatedCPA2 = new CPA_v2_0__c(Name = 'Australia', Country_Applied_Costings__c=true );
            insert RelatedCPA;
            insert RelatedCPA2;
            
            CPA_v2_0__c cpav2 = new CPA_v2_0__c(Name = 'Brazil Testing 2.0', Supplier__c = accs[1].Id, Service_Type__c = 'IOR/EOR', 
                                                Related_Costing_CPA__c = RelatedCPA.Id,Preferred_Supplier__c = TRUE, VAT_Rate__c = 0.1, 
                                                Destination__c = 'Brazil',  Final_Destination__c = 'Brazil',VAT_Reclaim_Destination__c = FALSE, 
                                                Country__c = country.id, Lead_ICE__c = u.Id,CIF_Freight_and_Insurance__c = 'CIF Amount', Green_Lane__c = true);
            cpaList.add(cpav2) ;
            
            CPA_v2_0__c cpav3 = new CPA_v2_0__c(Name = 'Brazil Testing 2.0', Supplier__c = accs[1].Id, Service_Type__c = 'IOR/EOR', 
                                                Related_Costing_CPA__c = RelatedCPA2.Id,Preferred_Supplier__c = TRUE, VAT_Rate__c = 0.1, 
                                                Destination__c = 'Australia',  Final_Destination__c = 'Australia',VAT_Reclaim_Destination__c = FALSE, 
                                                Country__c = country.id, Lead_ICE__c = u.Id,CIF_Freight_and_Insurance__c = 'Insurance Amount'); 
            cpaList.add(cpav3);
            
            CPA_v2_0__c cpav4 = new CPA_v2_0__c(Name = 'Brazil Testing 2.0', Supplier__c = accs[1].Id, Service_Type__c = 'IOR/EOR', 
                                                Related_Costing_CPA__c = RelatedCPA2.Id,Preferred_Supplier__c = TRUE, VAT_Rate__c = 0.1, 
                                                Destination__c = 'Australia',  Final_Destination__c = 'Australia',VAT_Reclaim_Destination__c = FALSE, 
                                                Country__c = country.id, Lead_ICE__c = u.Id); 
            cpaList.add(cpav4);
            
            CPA_v2_0__c cpav5 = new CPA_v2_0__c(Name = 'Brazil Testing 2.0', Supplier__c = accs[1].Id, Service_Type__c = 'IOR/EOR', 
                                                Related_Costing_CPA__c = RelatedCPA2.Id,Preferred_Supplier__c = TRUE, VAT_Rate__c = 0.1, 
                                                Destination__c = 'Australia',  Final_Destination__c = 'Australia',VAT_Reclaim_Destination__c = FALSE, 
                                                Country__c = country.id, Lead_ICE__c = u.Id, CIF_Freight_and_Insurance__c = 'Freight Amount'); 
            cpaList.add(cpav5);
            
            CPA_v2_0__c cpav6 = new CPA_v2_0__c(Name = 'Brazil Testing 2.0', Supplier__c = accs[1].Id, Service_Type__c = 'IOR/EOR', 
                                                Related_Costing_CPA__c = RelatedCPA.Id,Preferred_Supplier__c = TRUE, VAT_Rate__c = 0.1, 
                                                Destination__c = 'Brazil',  Final_Destination__c = 'Brazil',VAT_Reclaim_Destination__c = FALSE, 
                                                Country__c = country.id, Lead_ICE__c = u.Id,CIF_Freight_and_Insurance__c = 'CIF Amount', Tracking_Term__c = 'DAP - Supplier');
            cpaList.add(cpav6) ;
            

            
            CPA_v2_0__c cpav7 = new CPA_v2_0__c(Name = 'Brazil Testing 2.0', Supplier__c = accs[1].Id, Service_Type__c = 'IOR/EOR', 
                                                Related_Costing_CPA__c = RelatedCPA.Id,Preferred_Supplier__c = TRUE, VAT_Rate__c = 0.1, 
                                                Destination__c = 'Brazil',  Final_Destination__c = 'Brazil',VAT_Reclaim_Destination__c = FALSE, 
                                                Country__c = country.id, Lead_ICE__c = u.Id,CIF_Freight_and_Insurance__c = 'CIF Amount', Tracking_Term__c = 'DDP');
            cpaList.add(cpav7) ;
            
            insert cpaList;
            
            country_price_approval__c cpa = new country_price_approval__c( name ='Brazil', Billing_term__c = 'DAP/CIF - IOR pays',  Airwaybill_Instructions__c = 'User IOR Address', Supplier__c = account2.Id,
                                                                          In_Country_Specialist__c = '0050Y000001km5c', Clearance_Destination__c = 'Brazil', Preferred_Supplier__c = TRUE,
                                                                          Destination__c = 'Brazil' );
            insert cpa;
            
            Test.startTest();
            List<Shipment_Order__c> soList = new List<Shipment_Order__c>();
            Shipment_Order__c shipmentOrder = new Shipment_Order__c(Account__c = account4.Id, Shipment_Value_USD__c = 2000, CPA_v2_0__c = cpav2.Id,Total_Taxes__c=20,of_Line_Items__c = 3,
                                                                    Client_Contact_for_this_Shipment__c = contact.Id, Who_arranges_International_courier__c ='TecEx', Tax_Treatment__c ='DAP/CIF - IOR pays', 
                                                                    Ship_to_Country__c = cpa.Id,Internal_Shipment_Type_Categorisation__c = 'Other',Pressure_Point_Comment__c = 'abc',VAT_Claiming_Country__c=false, Destination__c = 'Brazil',
                                                                    Service_Type__c = 'IOR', Chargeable_Weight__c = 200,Ship_From_Country__c = 'Australia',  
                                                                    Shipping_Status__c = 'Shipment Pending',Roll_Up_Costings_A__c = false,Populate_Invoice__c = false,Li_ion_Batteries__c = 'No',
                                                                    Loss_Making_Shipments__c = false,Pressure_Point__c = false,Insurance_Fee__c = 0,IOR_CSE__c = userinfo.getUserId(), ICE__c = u.Id, IOR_Price_List__c=iorpl.Id,
                                                                    Int_Courier_Tracking_No__c  = '12345', Int_Courier_Picklist__c = 'DHL',Internal_Shipment_Type__c = 'Sensitive');
            
            insert shipmentOrder;
            
            Shipment_Order__c shipmentOrder1 = new Shipment_Order__c(Account__c = account4.Id, Shipment_Value_USD__c = 200000, CPA_v2_0__c = cpav2.Id,Total_Taxes__c=20,of_Line_Items__c = 3,
                                                                    Client_Contact_for_this_Shipment__c = contact.Id, Who_arranges_International_courier__c ='TecEx', Tax_Treatment__c ='DAP/CIF - IOR pays', 
                                                                    Ship_to_Country__c = cpa.Id,Internal_Shipment_Type_Categorisation__c = 'Other',Pressure_Point_Comment__c = 'abc',VAT_Claiming_Country__c=false, Destination__c = 'Brazil',
                                                                    Service_Type__c = 'IOR', Chargeable_Weight__c = 200,Ship_From_Country__c = 'Australia',  
                                                                    Shipping_Status__c = 'Shipment Pending',Roll_Up_Costings_A__c = false,Populate_Invoice__c = false,Li_ion_Batteries__c = 'No',
                                                                    Loss_Making_Shipments__c = false,Pressure_Point__c = false,Insurance_Fee__c = 0,IOR_CSE__c = userinfo.getUserId(), ICE__c = u.Id, IOR_Price_List__c=iorpl.Id,
																	 Domestic_Tracking_Number__c  = '1234567', Domestic_Courier__c  = 'DHL');
            
            insert shipmentOrder1;
            
            Freight__c FR = new Freight__c(Shipment_Order__c =shipmentOrder1.Id, Service_type__c ='Courier',Pickup_availability_Ready__c = DateTime.newInstanceGMT(2021, 8, 27, 10, 2, 3),Suggested_Supplier_Name__c = 'DHL');
        insert FR;
            
            
            
            Shipment_Order__c shipmentOrder2 = new Shipment_Order__c(Account__c = account4.Id, Shipment_Value_USD__c = 200000, CPA_v2_0__c = cpav2.Id,Total_Taxes__c=20,of_Line_Items__c = 3,
                                                                    Client_Contact_for_this_Shipment__c = contact.Id, Who_arranges_International_courier__c ='TecEx', Tax_Treatment__c ='DAP/CIF - IOR pays', 
                                                                    Ship_to_Country__c = cpa.Id,Internal_Shipment_Type_Categorisation__c = 'Other',Pressure_Point_Comment__c = 'abc',VAT_Claiming_Country__c=false, Destination__c = 'Brazil',
                                                                    Service_Type__c = 'IOR', Chargeable_Weight__c = 200,Ship_From_Country__c = 'Australia',  
                                                                    Shipping_Status__c = 'Arrived at Hub',Roll_Up_Costings_A__c = false,Populate_Invoice__c = false,Li_ion_Batteries__c = 'No',
                                                                    Loss_Making_Shipments__c = false,Pressure_Point__c = false,Insurance_Fee__c = 0,IOR_CSE__c = userinfo.getUserId(), ICE__c = u.Id, IOR_Price_List__c=iorpl.Id,
																	 Domestic_Tracking_Number__c  = '1234567', Domestic_Courier__c  = 'DHL');
            
            insert shipmentOrder2;
            
            Freight__c FR2 = new Freight__c(Shipment_Order__c =shipmentOrder1.Id, Service_type__c ='Courier',Pickup_availability_Ready__c = DateTime.newInstanceGMT(2021, 8, 27, 10, 2, 3),Suggested_Supplier_Name__c = 'DHL');
        insert FR2;
            
            
            
            Shipment_Order__c shipmentOrder3 = new Shipment_Order__c(Account__c = account4.Id, Shipment_Value_USD__c = 200000, CPA_v2_0__c = cpav6.Id,Total_Taxes__c=20,of_Line_Items__c = 3,
                                                                    Client_Contact_for_this_Shipment__c = contact.Id, Who_arranges_International_courier__c ='TecEx', Tax_Treatment__c ='DAP/CIF - IOR pays', 
                                                                    Ship_to_Country__c = cpa.Id,Internal_Shipment_Type_Categorisation__c = 'Other',Pressure_Point_Comment__c = 'abc',VAT_Claiming_Country__c=false, Destination__c = 'Brazil',
                                                                    Service_Type__c = 'IOR', Chargeable_Weight__c = 200,Ship_From_Country__c = 'Australia',  
                                                                    Shipping_Status__c = 'Arrived at Hub',Roll_Up_Costings_A__c = false,Populate_Invoice__c = false,Li_ion_Batteries__c = 'No',
                                                                    Loss_Making_Shipments__c = false,Pressure_Point__c = false,Insurance_Fee__c = 0,IOR_CSE__c = userinfo.getUserId(), ICE__c = u.Id, IOR_Price_List__c=iorpl.Id,
																	 Domestic_Tracking_Number__c  = '1234567', Domestic_Courier__c  = 'DHL');
            
            insert shipmentOrder3;
            
            
            Shipment_Order__c shipmentOrder4 = new Shipment_Order__c(Account__c = account4.Id, Shipment_Value_USD__c = 200000, CPA_v2_0__c = cpav7.Id,Total_Taxes__c=20,of_Line_Items__c = 3,
                                                                    Client_Contact_for_this_Shipment__c = contact.Id, Who_arranges_International_courier__c ='TecEx', Tax_Treatment__c ='DAP/CIF - IOR pays', 
                                                                    Ship_to_Country__c = cpa.Id,Internal_Shipment_Type_Categorisation__c = 'Other',Pressure_Point_Comment__c = 'abc',VAT_Claiming_Country__c=false, Destination__c = 'Brazil',
                                                                    Service_Type__c = 'IOR', Chargeable_Weight__c = 200,Ship_From_Country__c = 'Australia',  
                                                                    Shipping_Status__c = 'Arrived at Hub',Roll_Up_Costings_A__c = false,Populate_Invoice__c = false,Li_ion_Batteries__c = 'No',
                                                                    Loss_Making_Shipments__c = false,Pressure_Point__c = false,Insurance_Fee__c = 0,IOR_CSE__c = userinfo.getUserId(), ICE__c = u.Id, IOR_Price_List__c=iorpl.Id,
																	 Domestic_Tracking_Number__c  = '1234567', Domestic_Courier__c  = 'DHL');
            
            insert shipmentOrder4;
            
            
            Shipment_Order__c shipmentOrder5 = new Shipment_Order__c(Account__c = account4.Id, Shipment_Value_USD__c = 200000, CPA_v2_0__c = cpav7.Id,Total_Taxes__c=20,of_Line_Items__c = 3,
                                                                    Client_Contact_for_this_Shipment__c = contact.Id, Who_arranges_International_courier__c ='TecEx', Tax_Treatment__c ='DAP/CIF - IOR pays', 
                                                                    Ship_to_Country__c = cpa.Id,Internal_Shipment_Type_Categorisation__c = 'Other',Pressure_Point_Comment__c = 'abc',VAT_Claiming_Country__c=false, Destination__c = 'Brazil',
                                                                    Service_Type__c = 'IOR', Chargeable_Weight__c = 200,Ship_From_Country__c = 'Australia',  
                                                                    Shipping_Status__c = 'Awaiting POD',Roll_Up_Costings_A__c = false,Populate_Invoice__c = false,Li_ion_Batteries__c = 'No',
                                                                    Loss_Making_Shipments__c = false,Pressure_Point__c = false,Insurance_Fee__c = 0,IOR_CSE__c = userinfo.getUserId(), ICE__c = u.Id, IOR_Price_List__c=iorpl.Id,
																	 Domestic_Tracking_Number__c  = '1234567', Domestic_Courier__c  = 'DHL');
            
            insert shipmentOrder5;
        
        //    shipmentOrder1.Sub_Status_Update__c ='In Storage';
          //  update shipmentOrder1;
            
            Lex_ServiceTeamDB.getSORecords(new List<String>(), new List<String>(), new List<String>(), new List<String>());
                    Test.stoptest();
        }
    }
    
     
    
  
}