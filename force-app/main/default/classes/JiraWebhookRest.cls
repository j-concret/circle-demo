@RestResource(urlMapping='/jira_sfdc/*')
global class JiraWebhookRest {
    @HttpPost
    global static void doPost() {
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        try{
            Map<String,String> priority_mapping = new Map<String,String> {
                'Show Stopper'=>'Show Stopper',
                'Minor'=>'Low',
                'Major'=>'Urgent',
                'Normal'=>'Medium'
            };

            Map<String,String> status_mapping = new Map<String,String> {
                'Waiting for Support'=>'Escalated',
                'In Progress'=>'Working',
                'Waiting for customer'=>'Waiting',
                'Closed'=>'Closed'
            };
            String response = req.requestBody.toString();
            JIRAWebhookResponse jiraRes = (JIRAWebhookResponse)JSON.deserialize(response,JIRAWebhookResponse.class);
            String issueKey = jiraRes.issue.key;
            List<Case> cases = [SELECT Id,Solution__c,Jira_Case_Number__c,description,Subject, Reason, Priority, Status FROM Case WHERE RecordTypeId =:Schema.SObjectType.Case.getRecordTypeInfosByName().get('Tech Support').getRecordTypeId() AND Jira_Case_Number__c =:issueKey LIMIT 1];
            if(!cases.isEmpty()) {
                Case cs = cases[0];
                if(priority_mapping.containsKey(jiraRes.issue ?.fields ?.priority ?.name))
                    cs.Priority = priority_mapping.get(jiraRes.issue ?.fields ?.priority ?.name);

                cs.Reason = jiraRes.issue ?.fields ?.customfield_11408 ?.value;
                cs.Subject = jiraRes.issue ?.fields ?.summary;
                cs.Solution__c = jiraRes.issue ?.fields ?.resolution ?.name;

                if(status_mapping.containsKey(jiraRes.issue ?.fields ?.status ?.name))
                    cs.Status = status_mapping.get(jiraRes.issue ?.fields ?.status ?.name);

                cs.description = jiraRes.issue ?.fields ?.description;
                updateCase(cs);
            }
        }catch(Exception e) {
            //Api_log
        }
    }

    private static void updateCase(Case cas){

        Jira_Webhook_Setting__c setting = Jira_Webhook_Setting__c.getInstance('JIRA_WEBHOOK_SETTING');
        if(setting == null) return;
        String access_token;
        Boolean isRefreshed = false;

        if(setting.Access_Token__c !=null && lessThan24Hours(setting.LastModifiedDate)) {
            access_token = setting.Access_Token__c;
        }else{
            isRefreshed = true;
            access_token = getAccessToken();
        }


        HttpResponse response = sendRequest(cas,access_token);
        //The session ID or OAuth token used has expired or is invalid
        if(response !=null && response.getStatusCode() == 401) {
            isRefreshed = true;
            access_token = getAccessToken();
            if(String.isNotBlank(access_token)) sendRequest(cas,access_token);
        }

        if(isRefreshed && String.isNotBlank(access_token)) {
            setting.Access_Token__c = access_token;
            update setting;
        }
    }

    private static HttpResponse sendRequest(Case cas,String access_token){
        if(String.isBlank(access_token)) return null;

        String endpoint = URL.getSalesforceBaseUrl().toExternalForm();
        endpoint +='/services/data/v53.0/sobjects/Case/'+cas.Id;
        cas.put('Id',null);

        HttpRequest request = new HttpRequest();
        request.setEndpoint(endpoint);
        request.setTimeout(60000);
        request.setMethod('PATCH');
        request.setHeader('Authorization', 'Bearer ' + access_token);
        request.setHeader('Content-Type', 'application/json');
        request.setBody(JSON.serialize(cas));
        return new Http().send(request);
    }

    public static String getAccessToken(){
        String USERNAME = 'jitender@g.tecex.com.staging';
        String PASSWORD = 'C0ncret109yq40p25S2HDmqr8sPwXlvx2v';
        String ENP_POINT_URL = URL.getSalesforceBaseUrl().toExternalForm();

        String LoginXML='<?xml version="1.0" encoding="UTF-8"?>';
        loginXML += '<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns1="urn:partner.soap.sforce.com">';
        loginXML += '<SOAP-ENV:Header>';
        loginXML +='<ns1:LoginScopeHeader>';
        loginXML += '<ns1:organizationId>'+UserInfo.getOrganizationId()+'</ns1:organizationId>';
        loginXML += '</ns1:LoginScopeHeader>';
        loginXML += '</SOAP-ENV:Header>';
        loginXML += '<SOAP-ENV:Body>';
        loginXML += '<ns1:login>';
        loginXML +='<ns1:username>'+ USERNAME+'</ns1:username>';
        loginXML += '<ns1:password>'+ password+'</ns1:password>';
        loginXML += '</ns1:login>';
        loginXML += '</SOAP-ENV:Body>';
        loginXML +='</SOAP-ENV:Envelope>';

        ENP_POINT_URL +='/services/Soap/u/22.0';

        HttpRequest request = new HttpRequest();
        request.setEndpoint(ENP_POINT_URL);
        request.setTimeout(60000);
        request.setMethod('POST');
        request.setHeader('SOAPAction','""');
        request.setHeader('Content-Type', 'text/xml;charset=UTF-8');
        request.setBody(loginXML);
        HttpResponse res = new Http().send(request);

        String responseBody = res.getBody();
        return getValueFromXMLString(responseBody, 'sessionId');
    }

    private static string getValueFromXMLString(string xmlString, string keyField){
        String xmlKeyValue = '';
        if(xmlString.contains('<' + keyField + '>')) {
            try{
                xmlKeyValue = xmlString.substring(xmlString.indexOf('<' + keyField + '>')+keyField.length() + 2, xmlString.indexOf('</' + keyField + '>'));
            }catch (Exception e) {}
        }
        return xmlKeyValue;
    }

    private static Boolean lessThan24Hours(Datetime dt){
        Long dt1Long = dt.getTime();
        Long dt2Long = DateTime.now().getTime();
        Long milliseconds = dt2Long - dt1Long;
        Long seconds = milliseconds / 1000;
        Long minutes = seconds / 60;
        Long hours = minutes / 60;
        return hours < 24;
    }
}