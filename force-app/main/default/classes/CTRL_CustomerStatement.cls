/**
 * Created by Caro on 2019-04-14.
 */

public with sharing class CTRL_CustomerStatement {

    public List<DMN_Statement.StatementLineItem> billing {get;set;}
    public Decimal statementTotal {get;set;}
    public DMN_Statement.StatementBody statementBody {get;set;}
    public List<DMN_Statement.AgingPeriod> statementAging {get
    {
        if (statementAging == null) {
            statementAging = StatementService.generateAging(customerId);
           
        }
        return statementAging;
    }set;}
    public Map<String,String> statementHeader {get;set;}
    public Map<String,String> statementFooter {get;set;}
    public Decimal unappliedTotal {get;set;}
    Id customerId {get;set;}
    Account customer {get;set;}

    public  CTRL_CustomerStatement(ApexPages.StandardController controller) {
        this.customerId = controller.getId();
        this.customer = (Account) controller.getRecord();
        this.statementBody = StatementService.generateBody(customerId);
        this.billing = statementBody.lineItems;
        this.statementTotal = statementBody.statementTotal;
        this.statementHeader = StatementService.generateHeader();
        this.statementFooter = StatementService.generateFooter();
        this.statementBody.statementType = 'General';
        this.unappliedTotal = StatementService.generateUnappliedTotal(customerId);
        
    }
   
}