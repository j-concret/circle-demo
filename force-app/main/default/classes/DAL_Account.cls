/**
 * Created by Caro on 2019-05-02.
 */

public with sharing class DAL_Account extends fflib_SObjectSelector {

        public List<Schema.SObjectField> getSObjectFieldList() {

            return new List<Schema.SObjectField>{
                    Account.Name,
                    Account.Total_Amount_Outstanding__c,
                    Account.Id
            };
        }

        public Schema.SObjectType getSObjectType() {

            return Account.sObjectType;
        }

        public Account selectById(Id accountId) {

            return selectById(new Set<Id>{accountId})[0];
        }

        public List<Account> selectById(Set<Id> idSet) {

            return (List<Account>) selectSObjectsById(idSet);
        }
}