public class CPDefaultupdatewrapper {
 
    public List<DefaultPref> DefaultPref;

	public class DefaultPref {
        public String CPDefaultRecordID;
		public String ClientAccId;
		public String Chargeable_Weight_Units;
		public String ion_Batteries;
		public String Order_Type;
		public String Package_Dimensions;
		public String Package_Weight_Units;
		public String Second_hand_parts;
		public String Service_Type;
		public String TecEx_to_handle_freight;
        public String Country;
	}

	public static CPDefaultupdatewrapper parse(String json) {
		return (CPDefaultupdatewrapper) System.JSON.deserialize(json, CPDefaultupdatewrapper.class);
	}

}