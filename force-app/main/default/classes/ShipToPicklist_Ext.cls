public with sharing class ShipToPicklist_Ext {

    private final Opportunity mysObject;
    public String selectedEntry{get; set;}
    private List<PriceBookEntry> productlist;

    // The extension constructor initializes the private member
    // variable mysObject by using the getRecord method from the standard
    // controller.
    public ShipToPicklist_Ext(ApexPages.StandardController stdController) {
        if (!Test.isRunningTest()) stdController.addFields(new List<String> {'Pricebook2Id','product_ID__c'});
        this.mysObject = (Opportunity)stdController.getRecord();
        selectedEntry = this.mysObject.product_ID__c; 
    }

    public List<SelectOption> getPicklistOptions() {
         List<SelectOption> options = new List<SelectOption>();

          productlist = [Select Id, Product2.Name FROM PriceBookEntry where PriceBook2Id = :mysObject.Pricebook2Id];
          options.add(new SelectOption('--None--','--None--'));
          for (Integer j=0;j<productlist.size();j++)
          {
              options.add(new SelectOption(productlist[j].Id,productlist[j].Product2.Name));
          }

          options.sort();

          return options;
    }

    public PageReference PbEntryCreate () {
         if(selectedEntry != null){
        mysObject.product_ID__c = selectedEntry;

        update mysObject;

        List<OpportunityLineItem> listToDelete = [select Id from OpportunityLineItem where OpportunityId=:mysObject.Id];

        if(listToDelete.size()>0){
            delete listToDelete;
        }

        List<OpportunityLineItem> oliList = new List<OpportunityLineItem>();

                OpportunityLineItem oli = new OpportunityLineItem(OpportunityId=mysObject.Id, PricebookEntryId=selectedEntry, Quantity = 1, UnitPrice = 0, IOR_Fee__c=0);
                oliList.add(oli);


        insert oliList;

        }

        PageReference pageRef = ApexPages.currentPage();
        pageRef.setRedirect(true);
        return pageRef;
    }

}