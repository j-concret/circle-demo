@RestResource(urlMapping='/NCPAccountRelatedContacts/*')
global class NCPAccountRelatedContacts {

    @Httppost
    global static void getRelatedContacts(){
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        res.addHeader('Content-Type', 'application/json');
        Blob body = req.requestBody;
        String requestString = body.toString();
        RequestWrapper reqBody;
        try {
            reqBody = (RequestWrapper)JSON.deserialize(requestString,RequestWrapper.class);
            List<Access_token__c> at = [SELECT status__c,Access_Token__c FROM Access_token__c WHERE Access_Token__c =: reqBody.Accesstoken and status__c = 'Active' LIMIT 1];

            if(!at.isEmpty()) {
                if(String.isBlank(reqBody.AccountID)) {
                  res.responseBody = Blob.valueOf(Util.getResponseString('Error','Invalid Account id.'));
                  res.statusCode = 400;
                }else{
                    Set<ID> CONIDS = new Set<ID>();
                    Set<ID> UIDS = new Set<ID>();
                    //(select Id, LastLoginDate ,  ContactId , AccountId , FullPhotoUrl  from Users)
                    
                    List<Contact> contacts = [SELECT Id,Account.Service_Manager__c,Account.Lead_AM__c,Account.CSE_IOR__c,Account.Financial_Manager__c,Account.Financial_Controller__c,Account.Operations_Manager__c,Account.Sales_Executive__c,
                                              Email,Name,Include_in_invoicing_emails__c ,Contact_Role__c  FROM Contact Where AccountId =:reqBody.AccountID];
                    
                    For(Integer i=0;contacts.size()>i;i++){
                        CONIDS.add(contacts[i].id);
                        UIDS.add(contacts[i].Account.Service_Manager__c);
                        UIDS.add(contacts[i].Account.Lead_AM__c);
                        UIDS.add(contacts[i].Account.CSE_IOR__c);
                        UIDS.add(contacts[i].Account.Financial_Manager__c);
                        UIDS.add(contacts[i].Account.Financial_Controller__c);
                        UIDS.add(contacts[i]. Account.Operations_Manager__c);
                        UIDS.add(contacts[i].Account.Sales_Executive__c);
                                              
                    }
                    
                    List<user> AllUsers = [select Id,name,email,contact.Include_in_invoicing_emails__c,contact.Contact_Role__c, LastLoginDate ,isactive,  ContactId , AccountId , FullPhotoUrl  from User where isactive=true and (id in :UIDS or contactid in:CONIDS)  ];
                    
                    
                    res.responseBody = Blob.valueOf(JSON.serializePretty(AllUsers));
                    res.statusCode = 200;

                    insert new API_Log__c(
                        Account__c = reqBody.AccountID,
                        EndpointURLName__c = 'NCPAccountRelatedContacts',
                        Response__c='Success - Related Contacts sent for this account '+reqBody.AccountID,
                        StatusCode__c=string.valueof(res.statusCode)
                        );
                }
            }
            else{
                res.responseBody = Blob.valueOf(Util.getResponseString('Error','Access Token is invalid or expired.'));
                res.statusCode = 400;
                insert new API_Log__c(
                    Account__c = String.isNotBlank(reqBody.AccountID) ? reqBody.AccountID : null,
                    EndpointURLName__c = 'NCPAccountRelatedContacts',
                    Response__c='Error - Invalid AccessToken',
                    StatusCode__c=string.valueof(res.statusCode)
                    );
            }
        }catch(Exception e) {
            String jsonData = Util.getResponseString('Error','Something went wrong while sending related contacts of Account, please contact support_SF@tecex.com');
            res.responseBody = Blob.valueOf(jsonData);
            res.statusCode = 500;
            insert new API_Log__c(
                Account__c = reqBody != null && String.isNotBlank(reqBody.AccountID) ? reqBody.AccountID : null,
                EndpointURLName__c = 'NCPAccountRelatedContacts',
                Response__c = 'Error - NCPAccountRelatedContacts : '+e.getMessage(),
                StatusCode__c = string.valueof(res.statusCode)
                );
        }
    }

    public class RequestWrapper {
        public String Accesstoken;
        public String AccountID;
    }
}