Global class  BT_AppliedBankTransactionAlert {

    @InvocableMethod(label='Send an email Alert' description='sends an email')
    public static void sendEmail(List<ID> btIdList) {

        Bank_transactions__c BT = [SELECT Id, Name, Supplier__c
                                     FROM Bank_transactions__c
                                    WHERE Id =: btIdList
                                  ];
       
        List<Contact> conList   = [SELECT Id, LastName, Email, AccountId
                                     FROM Contact
                                    WHERE AccountId = : BT.Supplier__c AND Include_in_invoicing_emails__c = true
                                ];
        
        //Retrieve Email template
        EmailTemplate et=[Select id from EmailTemplate where name=:'Applied Bank Transactions'];

        //Create email list
        List<Messaging.SingleEmailMessage> emails = new List<Messaging.SingleEmailMessage>();
        
        Messaging.SingleEmailMessage singleMail   = new Messaging.SingleEmailMessage();
        
        //add template
       singleMail.setTemplateId(et.Id);

       //set target object for merge fields
        singleMail.setTargetObjectId(conList[0].Id);

        singleMail.setwhatId(BT.Id);

        //set to save as activity or not
        singleMail.setSaveAsActivity(true);

        //add address's that you are sending the email to
        //String [] ToAddress= new string[] {conList.Email} ;
        String [] CCAddress= new string[] {'finance@tecex.com'};
        String [] ToAddress= new string[] {} ;
        for(Contact em : conList){
            ToAddress.add(em.Email);
        }
        
        //set addresses
        singleMail.setCCAddresses(CCAddress);
        singleMail.setToAddresses(ToAddress);

       
       
        //add mail
        emails.add(singleMail);

        //send the message
        Messaging.sendEmail(emails);
    }

}