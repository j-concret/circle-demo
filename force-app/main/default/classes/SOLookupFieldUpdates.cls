public class SOLookupFieldUpdates {
    
    @Future
    public static void Updates(List<Id> SOIds)  {
        
       
        List<ID> AccID = new list<Id>();
        List<ID> CPA1 = new list<Id>();
        List<ID> IORPLID = new list<Id>();
        List<ID> CPA2 = new list<Id>();
        
        
        shipment_Order__C[] SOs= [Select Id,Handling_and_Admin_Fee__c,Bank_Fees__c,CPA_v2_0__c,Who_arranges_International_courier__c,Lead_AM__c,Financial_Controller__c,Financial_Manager__c,FF_Est_Cost__c,Calculated_Hub_Shipping_Cost__c,International_Delivery_Cost__c,Hub_Shipment_Formula__c,Manual_hub_Shipping_Cost__c,Value_Hub_Shipping_Cost__c,Ship_to_Country__r.Freight_only__c,Manual_International_Delivery_Cost__c,Chargeable_Weight__c,Value_International_Delivery_Cost__c,Calculated_International_Delivery_Cost__c,IOR_Fee_new__c,IOR_Min_USD__c,Tax_recovery_Premium_Rate__c,EOR_Fee_new__c,EOR_Min_USD_new__c,On_Charge_Mark_up__c,Tax_Rate__c,Bank_Cost_b__c,Courier_Handover_Cost__c,Customs_Brokerage_Cost_CIF__c,Customs_Brokerage_Cost_fixed__c,Customs_Clearance_Cost_b__c,Customs_Handling_Cost__c,Customs_Inspection_Cost__c,Delivery_Order_Cost__c,Delvier_to_IOR_Warehouse_Cost__c,Examination_Cost__c,Fixed_IOR_Cost__c,Handling_other_costs__c,Hub_Country__c,Hub_Shipping_Rate_KG__c,ICE__c,IOR_Cost_b__c,Import_Declaration_Cost__c,Lead_ICE__c,License_Permit_Cost__c,Loading_Cost__c,Local_Delivery_Cost__c,Quarantine_cost__c,Shipping_Notes_New__c,Storage_Cost__c,Supplier_Admin_Cost__c,Terminal_Charges_Cost__c,Account__c,ship_to_country__C,finance_team__C,IOR_Price_list__C,service_Manager__C,IOR_CSE__c,Insurance_Fee__c from shipment_Order__C where id IN :SOIds]; 
         for(Integer i=0;SOs.size()>i;i++ ){
              AccID.add(SOs[i].Account__c);
              CPA1.add(SOs[i].ship_to_country__C);
              IORPLID.add(SOs[i].IOR_Price_list__C);
              CPA2.add(SOs[i].CPA_v2_0__c);
            
         }
            
       
          // Update from account  
         	for (Account cs: [select ID,finance_team__C,Lead_AM__c,Financial_Controller__c,Financial_Manager__c,service_Manager__C,CSE_IOR__c,Insurance_Fee__c from Account where Id IN :AccId]){
        	 for(Integer i=0;SOs.size()>i;i++ ){
           SOs[i].finance_team__C = cs.finance_team__C;
           SOs[i].service_Manager__C = cs.service_Manager__C;
           SOs[i].IOR_CSE__c = cs.CSE_IOR__c;
           SOs[i].Insurance_Fee__c = cs.Insurance_Fee__c;
                 SOs[i].Lead_AM__c = cs.Lead_AM__c;
                 SOs[i].Financial_Controller__c = cs.Financial_Controller__c;
                 SOs[i].Financial_Manager__c = cs.Financial_Manager__c;
                 
                 
           //SOs[i].CPA_Costings_Added__c = TRUE;
          // Value:International Delivery Cost field update
                 
                 IF(SOs[i].Manual_International_Delivery_Cost__c >= 0){
                     SOs[i].Value_International_Delivery_Cost__c = SOs[i].Manual_International_Delivery_Cost__c;
                   }
                 Else IF(SOs[i].Chargeable_Weight__c >120 || SOs[i].Ship_to_Country__r.Freight_only__c){ SOs[i].Value_International_Delivery_Cost__c = SOs[i].FF_Est_Cost__c;}
                 Else {SOs[i].Value_International_Delivery_Cost__c = SOs[i].Calculated_International_Delivery_Cost__c;
                     
                 }
             // Value:Hub Shipping Cost field update    
                 IF(SOs[i].Hub_Shipment_Formula__c == TRUE && SOs[i].Manual_hub_Shipping_Cost__c > 0){
                     SOs[i].Value_Hub_Shipping_Cost__c = SOs[i].Manual_hub_Shipping_Cost__c;
                   }
                 Else IF(SOs[i].Hub_Shipment_Formula__c == TRUE && SOs[i].Manual_hub_Shipping_Cost__c == 0)
                 { SOs[i].Value_Hub_Shipping_Cost__c = SOs[i].calculated_hub_Shipping_Cost__c;}
              
                 // International Delivery Cost field update  
                 SOs[i].International_Delivery_Cost__c = SOs[i].Value_International_Delivery_Cost__c + SOs[i].Value_Hub_Shipping_Cost__c;
                   
                
        	}
   			 }
         
         
         // Update from  IORPL  
         
         
         	for (Ior_Price_list__C IOR: [select ID,Admin_Fee__c,Bank_Fees__c,TecEx_Shipping_Fee_Markup__c,Tax_Rate__c,On_Charge_Mark_up__c,EOR_Fee__c,EOR_Min_USD__c,Tax_recovery_Premium_Rate__c,IOR_Min_Fee__c,IOR_Fee__c,Client_Name__r.name from IOR_price_List__C where Id IN :IORPLID]){
        	for(Integer i=0;SOs.size()>i;i++ ){
               SOs[i].IOR_Fee_new__c = IOR.IOR_Fee__c;
               SOs[i].IOR_Min_USD__c = IOR.IOR_Min_Fee__c;
               SOs[i].Tax_recovery_Premium_Rate__c = IOR.Tax_recovery_Premium_Rate__c;
               SOs[i].EOR_Fee_new__c = IOR.EOR_Fee__c;
               SOs[i].EOR_Min_USD_new__c = IOR.EOR_Min_USD__c;
               SOs[i].On_Charge_Mark_up__c = IOR.On_Charge_Mark_up__c;
               SOs[i].Tax_Rate__c = IOR.Tax_Rate__c;
               SOs[i].TecEx_Shipping_Fee_Markup__c = IOR.TecEx_Shipping_Fee_Markup__c;
                 //SOs[i].Handling_and_Admin_Fee__c = IOR.Admin_Fee__c;
                 //SOs[i].Bank_Fees__c = IOR.Bank_Fees__c;
               
                
        	}
   			 }
         
 			// Update from CPA2  
         
         
         	for (CPA_v2_0__c CPAQ2: [select ID, Minimum_Brokerage_Costs__c,Shipping_Notes__c,Minimum_Clearance_Costs__c,Minimum_Handling_Costs__c,Minimum_License_Permit_Costs__c,
                                     CPA_Minimum_Brokerage_Costs__c, CPA_Minimum_Clearance_Costs__c, CPA_Minimum_Handling_Costs__c, CPA_Minimum_License_Permit_Costs__c
                                     from CPA_v2_0__c where Id IN :CPA2]){
        	for(Integer i=0;SOs.size()>i;i++ ){
                
               /* edited by Masechaba 080520
               SOs[i].Minimum_Brokerage_Costs__c = CPAQ2.Minimum_Brokerage_Costs__c;
               SOs[i].Minimum_Clearance_Costs__c = CPAQ2.Minimum_Clearance_Costs__c;
               SOs[i].Minimum_Handling_Costs__c = CPAQ2.Minimum_Handling_Costs__c;
               SOs[i].Minimum_License_Permit_Costs__c = CPAQ2.Minimum_License_Permit_Costs__c;*/
               
                // new edits
               SOs[i].Minimum_Brokerage_Costs__c = CPAQ2.CPA_Minimum_Brokerage_Costs__c;
               SOs[i].Minimum_Clearance_Costs__c = CPAQ2.CPA_Minimum_Clearance_Costs__c;
               SOs[i].Minimum_Handling_Costs__c = CPAQ2.CPA_Minimum_Handling_Costs__c;
               SOs[i].Minimum_License_Permit_Costs__c = CPAQ2.CPA_Minimum_License_Permit_Costs__c;
               SOs[i].Shipping_Notes_New__c = CPAQ2.Shipping_Notes__c;
               
                
        	}
   			 }         
       
         // Update from CPA1
              
        for (Country_Price_Approval__C cs1: [select ID,Billing_term__c,Name,IOR_cost_fixed_in_USD__c,Handling_other_costs__c,Hub_Country__c,Local_Delivery_Cost__c,Hub_Shipping_Rate_KG__c,Terminal_Charges_Cost__c,Quarantine_cost__c,Supplier_Admin_Cost__c,Storage_Cost__c,Shipping_Notes_New__c,In_Country_Specialist__c,IOR_Cost__c,Import_Declaration_Cost__c,Lead_ICE__c,License_Permit_Cost__c,Loading_Cost__c,Examination_Cost__c,Deliver_to_IOR_Warehouse_Cost__c,Delivery_Order_Cost__c,Customs_Inspection_Cost__c,Customs_Handling_Cost__c,Customs_Clearance_Cost__c,Customs_Brokerage_Cost_fixed__c,Customs_Brokerage_Cost_CIF__c,Destination__C,bank_cost__C,Courier_Handover_Cost__c from Country_Price_Approval__C where Id IN :CPA1]){
       for(Integer i=0;SOs.size()>i;i++ ){
        SOs[i].Bank_Cost_b__c =cs1.bank_cost__C;
        SOs[i].Courier_Handover_Cost__c =cs1.Courier_Handover_Cost__c;
        SOs[i].Customs_Brokerage_Cost_CIF__c =cs1.Customs_Brokerage_Cost_CIF__c;
        SOs[i].Customs_Brokerage_Cost_fixed__c =cs1.Customs_Brokerage_Cost_fixed__c;
        SOs[i].Customs_Clearance_Cost_b__c =cs1.Customs_Clearance_Cost__c;
        SOs[i].Customs_Handling_Cost__c =cs1.Customs_Handling_Cost__c;
        SOs[i].Customs_Inspection_Cost__c =cs1.Customs_Inspection_Cost__c;
        SOs[i].Delivery_Order_Cost__c =cs1.Delivery_Order_Cost__c;
        SOs[i].Delvier_to_IOR_Warehouse_Cost__c =cs1.Deliver_to_IOR_Warehouse_Cost__c;
        SOs[i].Examination_Cost__c =cs1.Examination_Cost__c;
        SOs[i].Fixed_IOR_Cost__c =cs1.IOR_cost_fixed_in_USD__c;
        SOs[i].Handling_other_costs__c =cs1.Handling_other_costs__c;
        SOs[i].Hub_Country__c =cs1.Hub_Country__c;
        SOs[i].Hub_Shipping_Rate_KG__c =cs1.Hub_Shipping_Rate_KG__c;
        SOs[i].ICE__c =cs1.In_Country_Specialist__c;
        SOs[i].IOR_Cost_b__c =cs1.IOR_Cost__c;
        SOs[i].Import_Declaration_Cost__c =cs1.Import_Declaration_Cost__c;
        SOs[i].Lead_ICE__c =cs1.Lead_ICE__c;
        SOs[i].License_Permit_Cost__c =cs1.License_Permit_Cost__c;
        SOs[i].Loading_Cost__c =cs1.Loading_Cost__c;
        SOs[i].Local_Delivery_Cost__c =cs1.Local_Delivery_Cost__c;
        SOs[i].Quarantine_cost__c =cs1.Quarantine_cost__c;
        //SOs[i].Shipping_Notes_New__c =cs1.Shipping_Notes_New__c;
        SOs[i].Storage_Cost__c =cs1.Storage_Cost__c;
        SOs[i].Supplier_Admin_Cost__c =cs1.Supplier_Admin_Cost__c;
        SOs[i].Terminal_Charges_Cost__c =cs1.Terminal_Charges_Cost__c;
           
           IF(cs1.Billing_term__c  == 'DAP/CIF - IOR pays') { SOs[i].Tax_Treatment__c = 'DAP/CIF - IOR pays'; }
               Else IF(SOs[i].Who_arranges_International_courier__c  == 'Client') { SOs[i].Tax_Treatment__c = 'DDP - Client Account'; }
             Else  { SOs[i].Tax_Treatment__c = 'DDP - Tecex Account'; }
              
           
           
           
    //    SOs[i].Tax_Treatment__c =cs1.Billing_term__c;
           
        	}
   			 }
        
        
         
         for(Integer i=0;SOs.size()>i;i++ ){
            update SOs[i];
         }
      
        
        
    }

}