public class FirebaseNotifications {
    
    public static void notifyFirebaseInBulkmap(Map<String,String> SoMap){
        if(!System.isBatch()){
            
            List<SO_Travel_History__c> SOTHList = new  List<SO_Travel_History__c>();
            if(Test.isRunningTest()){
                SOTHList = [SELECT Id , Name ,push_Notification_choice__c, Client_NCPAccess__c,TecEx_SO__r.NCP_Quote_Reference__c,TecEx_SO__r.recordtype.name,Mapped_Status__c,Description__c,TecEx_SO__r.Id ,is_Notification_Sent__c , Source__c,TecEx_SO__r.Name , TecEx_SO__r.Client_Contact_for_this_Shipment__c , TecEx_SO__r.Client_Contact_for_this_Shipment__r.AccountId , TecEx_SO__r.Client_Contact_for_this_Shipment__r.Email   , TecEx_SO__r.IOR_CSE__c , TecEx_SO__r.ICE__c FROM SO_Travel_History__c  where  Id IN :SoMap.keySet() limit 1];
            }else{
                SOTHList = [SELECT Id , Name ,push_Notification_choice__c, Client_NCPAccess__c,TecEx_SO__r.NCP_Quote_Reference__c,TecEx_SO__r.recordtype.name,Mapped_Status__c,Description__c,TecEx_SO__r.Id ,is_Notification_Sent__c , Source__c,TecEx_SO__r.Name , TecEx_SO__r.Client_Contact_for_this_Shipment__c , TecEx_SO__r.Client_Contact_for_this_Shipment__r.AccountId , TecEx_SO__r.Client_Contact_for_this_Shipment__r.Email   , TecEx_SO__r.IOR_CSE__c , TecEx_SO__r.ICE__c FROM SO_Travel_History__c where  Id IN :SoMap.keySet() and push_Notification_choice__c=TRUE AND TecEx_SO__r.Shipping_Status__c != 'Cost Estimate'];// and Source__c != 'SO Creation'
            }
            
            System.debug('SOTHList Firebase '+JSON.serializePretty(SoMap.keySet()));
            System.debug('SOTHList Firebase '+JSON.serializePretty(SOTHList));
            
            /*String RecordId = (SOTHList != null && !SOTHList.isEmpty())?string.valueof(SOTHList[0].TecEx_SO__c):'';
            String quoteReference = (SOTHList != null && !SOTHList.isEmpty())?string.valueof(SOTHList[0].TecEx_SO__r.NCP_Quote_Reference__c):'';
            String shipmentOrderType = (SOTHList != null && !SOTHList.isEmpty())?string.valueof(SOTHList[0].TecEx_SO__r.recordtype.name):'';
            */
            
            if(!SOTHList.isempty()){
                
                Map<String,SO_Travel_History__c> sourceMap = new Map<String,SO_Travel_History__c>();  
                Map<Id,List<Id>> contactMap = new Map<Id,List<Id>>(); 
                Map<Id,Id> shipmentsOnSOLogs = new Map<Id,Id>(); 
                
                
                for(SO_Travel_History__c soLog: SOTHList){
                    sourceMap.put(soLog.Source__c,soLog);
                    List<Id> contactId = new List<Id>();
                    contactId.add(soLog.TecEx_SO__r.Client_Contact_for_this_Shipment__c);
                    contactMap.put(soLog.Id,contactId);
                    shipmentsOnSOLogs.put(soLog.TecEx_SO__c,soLog.Id);
                }
                
                for(Notify_Parties__c notifyParty:[SELECT Id,Shipment_Order__c,Contact__r.Client_Notifications_Choice__c,
                                                   Contact__c,Contact__r.Email,Contact__r.Client_Tasks_notification_choice__c  
                                                   FROM Notify_Parties__c 
                                                   Where Shipment_Order__c IN :shipmentsOnSOLogs.keySet() 
                                                   AND Contact__r.Client_Push_Notifications_Choice__c = 'Opt-In' //AND Contact__r.Include_in_SO_Communication__c = true
                                                   ]){
                                                       
                                                       List<Id> contactId = contactMap.get(shipmentsOnSOLogs.get(notifyParty.Shipment_Order__c));                                  
                                                       contactId.add(notifyParty.Contact__c);
                                                       contactMap.put(shipmentsOnSOLogs.get(notifyParty.Shipment_Order__c),contactId); 
                                                       
                                                   }
                
                List<Id> contactIdsForFirebase = new List<Id>();
                for(Id sologId:contactMap.keySet()){
                    List<Id> contactId = contactMap.get(sologId);
                    contactIdsForFirebase.addAll(contactId);
                }
                
                Firebase_push_notifications__c[] FPNLIst = [Select Id, Name,  Subject__c, Body__c, Mapping_Fields__c from Firebase_push_notifications__c where name IN : sourceMap.keySet()];
                System.debug('FPNLIst'+JSON.serialize(FPNLIst));
                
                List<APP_Tokens__c> ATList   =[SELECT Id, Name, Contact__c, Contact__r.push_Notification_choice__c,Active__c, Device_Name__c, Registration_Token__c FROM APP_Tokens__c where Contact__c IN :contactIdsForFirebase and Active__c = TRUE];
               	Map<Id,List<APP_Tokens__c>> appTokenMap = new Map<Id,List<APP_Tokens__c>>();
                for(APP_Tokens__c apt: ATList){
                    
                    List<APP_Tokens__c> apptknList = new List<APP_Tokens__c>();
                    if(appTokenMap.containsKey(apt.Contact__c)){
                        apptknList = appTokenMap.get(apt.Contact__c);
                    }
                    apptknList.add(apt);
                    appTokenMap.put(apt.Contact__c,apptknList);
                }
                
                for(Firebase_push_notifications__c FPN:FPNLIst){
                    String FinalBody;
                    String FinalSubject;
                    String[] QFields = new String[0];
                    map<String,String> QFields1= new map<String,string>();
                    Map<String,String> valueMap = new Map<String,String>();
                    List<String> ConTokens = new List<String>();
                    String FinalConTokens;
                    Map<String, Object> m = (Map<String, Object>)JSON.deserializeUntyped(FPN.Mapping_Fields__c);
                    String FinalBody1=FPN.body__C;
                    String FinalSubject1=FPN.subject__c;
                    for(string Key:  m.keySet()){ 
                        QFields.add(string.valueof(m.get(key)));
                        QFields1.put(string.valueof(m.get(key)),Key);
                        FinalBody1 = FinalBody1.replace(key,'//'+string.valueof(m.get(key))+'//');
                        FinalSubject1=FinalSubject1.replace(key,'//'+string.valueof(m.get(key))+'//');
                        FinalBody=FinalBody1;
                        FinalSubject=FinalSubject1;
                    }
                    SO_Travel_History__c SOLOG = sourceMap.get(FPN.Name);
                    for(string Key:  QFields1.keySet()){ 
                        if(Key.contains('TecEx_SO__r')){ 
                            String profileName = (String)solog.getSobject('TecEx_SO__r').get(key.substringAfter('TecEx_SO__r.'));
                            valueMap.put('//'+key+'//',profileName);   } else{
                                valueMap.put('//'+key+'//',string.valueof(solog.get(Key)));}
                    }
                    if(!Test.isRunningTest()){
                    for(string Key:  valueMap.keySet()){ 
                        FinalBody1 = FinalBody1.replace(key,string.valueof(valueMap.get(key)));
                        FinalSubject1=FinalSubject1.replace(key,string.valueof(valueMap.get(key)));
                        
                        FinalBody=FinalBody1;
                        FinalSubject=FinalSubject1;
                        
                    }
                    }
                    String RecordId = (SOLOG != null)?string.valueof(SOLOG.TecEx_SO__c):'';
                    String quoteReference = (SOLOG != null)?string.valueof(SOLOG.TecEx_SO__r.NCP_Quote_Reference__c):'';
                    String shipmentOrderType = (SOLOG != null)?string.valueof(SOLOG.TecEx_SO__r.recordtype.name):'';
                    
                    
                    List<Id> contactIds = contactMap.get(SOLOG.Id);
                    System.debug('contactIds appTokenMap = > ');
                    System.debug(JSON.serializePretty(appTokenMap));
					System.debug(contactIds);
                    for(Id conId:contactIds){
                        if(appTokenMap.ContainsKey(conId)){
                        for(APP_Tokens__c AppToken  : appTokenMap.get(conId)){
                            Firebasecallout(FinalSubject,FinalBody,AppToken.Registration_Token__c,RecordId,quoteReference,shipmentOrderType);   
                        }
                        }
                    }
                }
                
            }
        }
    }
    
    public static void NotifyFirebaseforcasefeeditem(List<ID> FeedIds ){
        
        String FinalBody;
        String FinalSubject;
        String SORecordId;
        String RecordId;
        String quoteReference;
        String shipmentOrderType;
        
        set<Id> ContactIds = new set<Id>();
        set<Id> CaseIds = new set<Id>();
        set<Id> FeedItemIds = new set<Id>();
        set<Id> FeedcommentIds = new set<Id>();
        List<Case> caselist = new List<Case>();
        
        For(Id Ids : FeedIds){ 
            If(Ids.getSObjectType() == FeedItem.SObjectType){FeedItemIds.add(Ids);}
            else{FeedcommentIds.add(Ids);}
        }        
        if(!FeedItemIds.isempty()){ 
            List<FeedItem> Feeditemslist = [select Id,parentId,body from Feeditem where id in:FeedItemIds ];
            For(FeedItem FI : Feeditemslist){ 
                CaseIds.add(Fi.parentID);
                FinalBody=fi.body;
                Finalbody=finalbody.stripHtmlTags();
                SORecordId=fi.parentId;
                RecordId=fi.Id;
            }
            
        }
        
        else{
            List<Feedcomment> Feedcommentlist = [select id,commentbody,parentId from Feedcomment where id in:FeedcommentIds ];
            For(Feedcomment FI : Feedcommentlist){ 
                CaseIds.add(Fi.parentID);
                FinalBody=fi.commentbody;
                Finalbody=finalbody.stripHtmlTags();
                SORecordId=fi.parentId;
                RecordId=fi.Id;
            }
            
        }
        if(CaseIds != null && !CaseIds.isEmpty()){
            caselist = [select Id,subject,shipment_Order__r.Client_Contact_for_this_Shipment__c,shipment_Order__c,shipment_Order__r.NCP_Quote_Reference__c,shipment_Order__r.recordtype.name from case where id in:CaseIds and shipment_Order__c !=null ];
        }
        
        For(case cs : caselist){ 
            ContactIds.add(cs.shipment_Order__r.Client_Contact_for_this_Shipment__c);
            FinalSubject = cs.subject;
            SORecordId =  cs.shipment_Order__c;
            quoteReference = cs.shipment_Order__r.NCP_Quote_Reference__c;
            shipmentOrderType = cs.shipment_Order__r.recordtype.name;
            RecordId = cs.Id;
        }
        List<APP_Tokens__c> AT   =[SELECT Id, Name, Contact__c, Contact__r.push_Notification_choice__c,Active__c, Device_Name__c, Registration_Token__c FROM APP_Tokens__c where Contact__c IN:ContactIds and Active__c = TRUE];
        if(FinalSubject !=null && FinalBody!=null && !AT.isempty()){ 
            if(!Test.isRunningTest())
                FirebasecalloutForchatter(FinalSubject,FinalBody,AT[0].Registration_Token__c,SORecordId,quoteReference,shipmentOrderType,RecordId);
        }
    }
    public static void NotifyFirebasefortaskfeeditem(List<ID> FeedIds ){
        
        String FinalBody;
        String FinalSubject;
        String SORecordId;
        String RecordId;
        String quoteReference;
        String shipmentOrderType;
        
        set<Id> ContactIds = new set<Id>();
        set<Id> TaskIds = new set<Id>();
        set<Id> FeedItemIds = new set<Id>();
        set<Id> FeedcommentIds = new set<Id>();
        List<Task> Tasklist = new List<Task>();
        
        For(Id Ids : FeedIds){ 
            If(Ids.getSObjectType() == FeedItem.SObjectType){FeedItemIds.add(Ids);}
            else{FeedcommentIds.add(Ids);}
        }        
        if(!FeedItemIds.isempty()){ 
            List<FeedItem> Feeditemslist = [select Id,parentId,body from Feeditem where id in:FeedItemIds ];
            For(FeedItem FI : Feeditemslist){ 
                TaskIds.add(Fi.parentID);
                FinalBody=fi.body;
                Finalbody=finalbody.stripHtmlTags();
                SORecordId=fi.parentId;
                RecordId=fi.Id;
            }
            
        }
        
        else{
            List<Feedcomment> Feedcommentlist = [select id,commentbody,parentId from Feedcomment where id in:FeedcommentIds ];
            For(Feedcomment FI : Feedcommentlist){ 
                TaskIds.add(Fi.parentID);
                FinalBody=fi.commentbody;
                Finalbody=finalbody.stripHtmlTags();
                SORecordId=fi.parentId;
                RecordId=fi.Id;
            }
            
            
        }
        if(TaskIds != null && !TaskIds.isEmpty()){
            Tasklist = [select Id,subject,shipment_Order__r.Client_Contact_for_this_Shipment__c,shipment_Order__c,shipment_Order__r.NCP_Quote_Reference__c,shipment_Order__r.recordtype.name from task where id in:TaskIds and shipment_Order__c !=null ];
        }        
        For(task cs : Tasklist){ 
            ContactIds.add(cs.shipment_Order__r.Client_Contact_for_this_Shipment__c);
            FinalSubject = cs.subject;
            SORecordId =  cs.shipment_Order__c;
            quoteReference = cs.shipment_Order__r.NCP_Quote_Reference__c;
            shipmentOrderType = cs.shipment_Order__r.recordtype.name;
            RecordId = cs.Id;
        }
        List<APP_Tokens__c> AT   =[SELECT Id, Name, Contact__c, Contact__r.push_Notification_choice__c,Active__c, Device_Name__c, Registration_Token__c FROM APP_Tokens__c where Contact__c IN:ContactIds and Active__c = TRUE];
        if(FinalSubject !=null && FinalBody!=null && !AT.isempty()){
            if(!Test.isRunningTest())
                FirebasecalloutForchatter(FinalSubject,FinalBody,AT[0].Registration_Token__c,SORecordId,quoteReference,shipmentOrderType, RecordId);
        }
    }
    
    @Future(callout=true)
    public static void Firebasecallout(String Subject,string Body,String Tokens,String RecordID,String quoteReference,String shipmentOrderType){
        System.debug('Firebasecallout');
        JSONGenerator gen = JSON.createGenerator(true);
        gen.writeStartObject();
        if(Tokens==null) {gen.writeNullField('to');} else{gen.writestringField('to', Tokens);}
        if(Tokens==null) {gen.writeNullField('time_to_live');} else{gen.writenumberField('time_to_live', 604800);}
        //   if(Tokens==null) {gen.writeNullField('click_action');} else{gen.writeStringField('click_action','https://www.tecex.com');}//"click_action": "Your URL here"
        gen.writeFieldName('notification');
        gen.writeStartObject();
        if(Body==null) {gen.writeNullField('body');} else{gen.writeStringField('body', Body);}
        if(Subject==null) {gen.writeNullField('title');} else{gen.writeStringField('title', Subject);}
        // if(Subject==null) {gen.writeNullField('link');} else{gen.writeStringField('link', 'https://www.tecex.com');}
        gen.writeEndObject();
        gen.writeFieldName('data');
        gen.writeStartObject();
        if(RecordId==null) {gen.writeNullField('shipmentOrderId');} else{gen.writeStringField('shipmentOrderId', RecordId);}
        if(quoteReference==null) {gen.writeNullField('quoteReference');} else{gen.writeStringField('quoteReference', quoteReference);}
        if(shipmentOrderType!=null && shipmentOrderType=='Cost Estimate') {gen.writeStringField('type', 'progress_update_quote');} else{gen.writeStringField('type', 'progress_update_shipment');}
        gen.writeEndObject();
        
        gen.writeEndObject();
        String jsonData = gen.getAsString();
        
        System.debug('Final Json--->'+jsonData);
        
        Http P=new Http();
        HttpRequest req = new HttpRequest();
        
        req.setEndpoint('https://fcm.googleapis.com/fcm/send'); 
        req.setMethod('POST');
        req.setTimeout(120000);
        req.setHeader('Content-Type','application/json');
        req.setHeader('Authorization','key=AAAAhW6cZnc:APA91bFVx-TeE9BFym7SvTf2g_RhokoqsgvUgKJH4A67MOripdhrMFsHvVGvlefF0oUw5tJkysBQj2rW0ThddSdEMn9dJWkdVFEeEqSRleFSbRu64bQGG1cEFWd8TGYeFhO0zfbq51vp');
        req.setBody(jsonData);
        
        HttpResponse response = P.send(req);
        system.debug('status code is >>>>'+ response.getStatusCode() + ' ' + response.getStatus());
        System.debug('Body is >>>>'+response.getBody());
        
        
        
    }
    
    @Future(callout=true)
    public static void FirebasecalloutForchatter(String Subject,string Body,String Tokens,String SORecordID,String quoteReference,String shipmentOrderType,string RecordID){
        
        JSONGenerator gen = JSON.createGenerator(true);
        gen.writeStartObject();
        if(Tokens==null) {gen.writeNullField('to');} else{gen.writestringField('to', Tokens);}
        if(Tokens==null) {gen.writeNullField('time_to_live');} else{gen.writenumberField('time_to_live', 604800);}
        //   if(Tokens==null) {gen.writeNullField('click_action');} else{gen.writeStringField('click_action','https://www.tecex.com');}//"click_action": "Your URL here"
        gen.writeFieldName('notification');
        gen.writeStartObject();
        if(Body==null) {gen.writeNullField('body');} else{gen.writeStringField('body', Body);}
        if(Subject==null) {gen.writeNullField('title');} else{gen.writeStringField('title', Subject);}
        // if(Subject==null) {gen.writeNullField('link');} else{gen.writeStringField('link', 'https://www.tecex.com');}
        gen.writeEndObject();
        gen.writeFieldName('data');
        gen.writeStartObject();
        if(SORecordId==null) {gen.writeNullField('shipmentOrderId');} else{gen.writeStringField('shipmentOrderId', SORecordId);}
        if(recordId !=null && recordId.startsWithIgnoreCase('00T')) {gen.writeNullField('caseId');gen.writeStringField('taskId', RecordId); } else{gen.writeNullField('taskId');gen.writeStringField('caseId', RecordId);}
        if(quoteReference==null) {gen.writeNullField('quoteReference');} else{gen.writeStringField('quoteReference', quoteReference);}
        if(quoteReference==null) {gen.writeNullField('subject');} else{gen.writeStringField('subject', quoteReference);}
        if(shipmentOrderType!=null && shipmentOrderType=='Cost Estimate') {gen.writeStringField('type', 'message_quote');} else{gen.writeStringField('type', 'message_shipment');}
        gen.writeEndObject();
        
        gen.writeEndObject();
        String jsonData = gen.getAsString();
        
        System.debug('Final Json--->'+jsonData);
        
        Http P=new Http();
        HttpRequest req = new HttpRequest();
        
        req.setEndpoint('https://fcm.googleapis.com/fcm/send'); 
        req.setMethod('POST');
        req.setTimeout(120000);
        req.setHeader('Content-Type','application/json');
        req.setHeader('Authorization','key=AAAAhW6cZnc:APA91bFVx-TeE9BFym7SvTf2g_RhokoqsgvUgKJH4A67MOripdhrMFsHvVGvlefF0oUw5tJkysBQj2rW0ThddSdEMn9dJWkdVFEeEqSRleFSbRu64bQGG1cEFWd8TGYeFhO0zfbq51vp');
        req.setBody(jsonData);
        
        HttpResponse response = P.send(req);
        system.debug('status code is >>>>'+ response.getStatusCode() + ' ' + response.getStatus());
        System.debug('Body is >>>>'+response.getBody());
        
        
        
    }
    
}