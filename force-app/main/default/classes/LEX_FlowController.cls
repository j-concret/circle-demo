/**
 * Created by Chibuye Kunda on 2019/01/25.
 */

public with sharing class LEX_FlowController {


    /** This will automatically populate the client contact object
     *  @return ServerResponse object returned
     */
 /*   @AuraEnabled
    public static LEX_NewRollOutController.ServerResponse getAccountAndContact(){

        return LEX_NewRollOutController.getAccountAndContact();             //return the list of objectes

    }//end of function definition
    */
    @AuraEnabled
    public static Shipment_Order__c getShipmentOrder(Id recordId) {

        Shipment_Order__c shipmentOrder = [select id, Name, Shipment_Value_USD__c, of_Line_Items__c,Final_Deliveries_New__c,Destination__c, Li_ion_Batteries__c, Who_arranges_International_courier__c, Chargeable_Weight__c, 
                                           of_packages__c, Number_Delivered__c, Type_of_Goods__c,Lithium_Battery_Types__c

                                  from Shipment_Order__c where Id =: recordId];    //return the shipmentorder 
        return ShipmentOrder;
    }  //end of function definition

}//end of class definition