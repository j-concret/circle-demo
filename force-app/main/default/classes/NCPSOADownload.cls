@RestResource(urlMapping='/SOADownload/*')
Global class NCPSOADownload {

     @Httppost
      global static void NCPSOADownload(){
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        Blob body = req.requestBody;
        res.addHeader('Content-Type', 'application/json');
        String requestString = body.toString();
        NCPSOADownloadwra rw  = (NCPSOADownloadwra)JSON.deserialize(requestString,NCPSOADownloadwra.class);
           list<Account> client = new  list<Account>();
          try{
              Access_token__c At =[select status__c,Access_Token__c from Access_token__c where Access_Token__c=:rw.Accesstoken ];
            	if( at.status__C =='Active'){
                      client = [Select Id,Name from Account where Id =:rw.AccountID];
                    
                    ID customerId=rw.AccountID;
					                  
                /*    Id profileId= userinfo.getProfileId();
String profileName=[Select Id,Name from Profile where Id=:profileId].Name;
system.debug('ProfileName'+profileName); */
              attachment Att=  CTRL_EmailAndAttachStatement.NCPDownloadStatement(customerId,client[0].name); 
                 
                     List<attmnt> oppatts = new list<attmnt>();
                    
            if( att != null){
           
                attmnt a=new attmnt();               
                a.AttachmentType=att.ContentType;
                a.AttachmentName=att.Name;
               // a.AttachmentId=att.Id;
                
                // attachment body to convert to blob format.
                string MyFile=EncodingUtil.base64Encode(att.body);
                Blob MyBlob=EncodingUtil.base64Decode(MyFile);
                
                a.AttachmentBody=MyBlob;
                
                oppatts.add(a);
                
             
            }    
               
           string Jsonstring=Json.serialize(oppatts);
                      
            res.responseBody=Blob.valueOf(Jsonstring);
             res.addHeader('Content-Type', 'application/json');
             res.statusCode = 200;
                
                API_Log__c Al = New API_Log__c();
                Al.EndpointURLName__c='SOAEmail';
                Al.Account__c=rw.AccountID;
                Al.StatusCode__c=string.valueof(res.statusCode);
                AL.Response__c = 'Success - SOAEmail sent';
                Insert Al;  
              }
              
              else{
                   JSONGenerator gen = JSON.createGenerator(true);
            gen.writeStartObject();
            
             gen.writeFieldName('Error');
             gen.writeStartObject();
            
                gen.writeStringField('Response', 'Accesstoken Expired');
            
            gen.writeEndObject();
            gen.writeEndObject();
            String jsonData = gen.getAsString();
            res.responseBody = Blob.valueOf(jsonData);
            res.addHeader('Content-Type', 'application/json');
            res.statusCode = 400;        
            throw new DMLException();
                  
              }
             
                }
              
            catch(Exception e){
                
                
                IF(e.getMessage()=='Insert failed. First exception on row 0; first error: INSUFFICIENT_ACCESS_ON_CROSS_REFERENCE_ENTITY, insufficient access rights on cross-reference id: []'){
                                    
             String abc= 'Success - SOAEmail sent';
             res.responseBody = Blob.valueOf(abc);
             res.addHeader('Content-Type', 'application/json');
             res.statusCode = 200;
                
                API_Log__c Al = New API_Log__c();
                Al.EndpointURLName__c='SOAEmail';
                Al.Account__c=rw.AccountID;
                Al.StatusCode__c=string.valueof(res.statusCode);
                AL.Response__c = 'Success - SOAEmail sent';
                Insert Al; 
                }
                else{
                
                String ErrorString ='Something went wrong, please contact support_SF@tecex.com';
                //  String ErrorString1 =+e.getMessage()+' - '+ e.getLineNumber()+' - '+e.getStackTraceString()+' - '+client[0].id;
                  
                    res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
                   res.addHeader('Content-Type', 'application/json');
                    res.statusCode = 400;
                
                API_Log__c Al = New API_Log__c();
                Al.Account__c=rw.AccountID;
                //Al.Login_Contact__c=logincontact.Id;
                Al.EndpointURLName__c='NCPSOAEmail';
                Al.Response__c=ErrorString;
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al;
                      
          }
          
          
      
      
      
            }
      
      }
    global class attmnt{       
        public string AttachmentType{get;set;}
        public string AttachmentName{get;set;}
        public string AttachmentId{get;set;}
        public blob AttachmentBody{get;set;}
    } 

}