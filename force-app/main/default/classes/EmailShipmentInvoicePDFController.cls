public with sharing class EmailShipmentInvoicePDFController {

    EmailTemplate emailtemp;
    public String body {get;set;}
    public List<Messaging.SingleEmailMessage> emailList {get;set;}
    
    
   
    public EmailShipmentInvoicePDFController(){
        
        String [] ids = ApexPages.currentPage().getParameters().get('ids').split(',');
       
        body = '';
        
        emailtemp = [SELECT Id FROM EmailTemplate WHERE Name = 'Shipment Template Batch'];
             
       
        
        emailList = new List<Messaging.SingleEmailMessage>();
        
        List< Shipment_Order__c > invs = [select Id from Shipment_Order__c where Id IN: ids ORDER BY Account__r.Name, Name];
        for (Shipment_Order__c inv : invs) {
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            
            mail.setSenderDisplayName('Kulturra.com Batch Invoice PDF');
            mail.setTemplateId(emailtemp.Id);    
            mail.setTargetObjectId(UserInfo.getUserId());
            mail.setSaveAsActivity(false);
            mail.setWhatId(inv.Id);
            
            emailList.add(mail);
            mail = null;
            
        }
        
        
        Messaging.sendEmail(emailList);
        
       
       
        
        for (Messaging.SingleEmailMessage emsg : emailList){
            
            body += (body!='') ? '<div style="page-break-before: always;">' : '';
            body += emsg.getHtmlBody();
            
            emsg = null;
        }
        emailList = null;
        
    }
    
}