/**
 * Created by Chibuye Kunda on 2019/01/17.
 */
@IsTest
public with sharing class LEX_NewRollOutControllerTest {

    /** this is the name of our test record
     */
    private static final String TEST_LASTNAME = 'Test User';

    /** this is the name of our community test record
     */
    private static final String TEST_COMMUNITY_LASTNAME = 'Test Comm User';

    /** this is the last name of the contact
     */
    private static final String CONTACT_LASTNAME = 'CONTACT LASTNAME';

    /** this is the account name
     */
    private static final String ACCOUNT_NAME = 'ACCOUNT NAME';

    /** this is the role name
     */
    private static final String ROLE_NAME = 'Role Name';



    /** this function will initialise our test data
     */
    @TestSetup
    static void setupData(){

        createRole();                   //create the user role
        createUser( 'TecEx IOR Team' );                 //create the user

        User test_user = getUser( TEST_LASTNAME );      //get the test user

        System.runAs( test_user ) {

            createAccountAndContact( test_user.Id );

            Account test_account = getAccount( ACCOUNT_NAME );
            country_price_approval__c cpa = new country_price_approval__c( Name = 'Aruba', Billing_term__c = 'DAP/CIF - IOR pays',
                                                                           Airwaybill_Instructions__c = 'User IOR Address' );
            insert cpa;                            //insert the CPA

            IOR_Price_List__c iorpl = new IOR_Price_List__c ( Client_Name__c = test_account.Id, Name = 'Aruba', IOR_Fee__c = 0.15, On_Charge_Mark_up__c = 0.15,
                                                              TecEx_Shipping_Fee_Markup__c =0.40, IOR_Min_Fee__c = 100,  Admin_Fee__c =100, Set_up_fee__c =100,
                                                              Bank_Fees__c =200, Tax_Rate__c = 0.20, Estimated_Customs_Brokerage__c =250,
                                                              Estimated_Customs_Clearance__c =250, Estimated_Customs_Handling__c =250, Estimated_Import_License__c =250 );
            insert iorpl;           //insert the iorpl

        }//end of run as block

        Contact test_contact = getContact( CONTACT_LASTNAME );

        createCommunityUser( test_contact.Id );

    }//end of function definition






    /** this function will test the function that get destinations from
     */
    @IsTest
    static void testGetDestinationValues(){

        User usr = getUser( TEST_LASTNAME );

        System.runAs( usr ){

            LEX_NewRollOutController.getDestinationValues();

        }//end of runAs block

    }//end of function definition





    /** this function will get list view ID
     */
    @IsTest
    static void testGetRecordListViewID(){

        User usr = getUser( TEST_LASTNAME );

        //run in test user context
        System.runAs( usr ){

            LEX_NewRollOutController.getRecordListViewID();

        }//end of runAs block

    }//end of function definition





    @IsTest
    static void testCreateRolloutRecord(){

        User usr = getUser( TEST_LASTNAME );

        //run in test internal user context
        System.runAs( usr ){

            Id account_id = [ SELECT Id
                              FROM Account
                              WHERE Name =: ACCOUNT_NAME  ].Id;

            Id contact_id = [ SELECT Id
                              FROM Contact
                              WHERE LastName =: CONTACT_LASTNAME ].Id;

            List< String > destination_list = new List< String >{ 'Brazil' };
            LEX_NewRollOutController.createRolloutRecord( account_id, contact_id, '12345', 128, destination_list, 'false' );

        }//end of runAs-block

        usr = getUser( TEST_COMMUNITY_LASTNAME );

        //run in test community user context
        System.runAs( usr ){

            Id account_id = [ SELECT Id
                              FROM Account
                              WHERE Name =: ACCOUNT_NAME  ].Id;

            Id contact_id = [ SELECT Id
                              FROM Contact
                              WHERE LastName =: CONTACT_LASTNAME ].Id;

            List< String > destination_list = new List< String >{ 'Brazil', 'Zambia' };
            LEX_NewRollOutController.createRolloutRecord( account_id, contact_id, '12345', 128, destination_list, 'false' );

        }//end of runAs-block


    }//end of function definition




    @IsTest
    static void testGetAccountAndContact(){

        User usr = getUser( TEST_COMMUNITY_LASTNAME );

        //run in test user context
        System.runAs( usr ){

            LEX_NewRollOutController.getAccountAndContact();

        }//end of runAs-block

    }




    @IsTest
    static void testIsCommunityUser(){

        User usr = getUser( TEST_LASTNAME );

        //run in test user context
        System.runAs( usr ){

            LEX_NewRollOutController.isCommunityUser();

        }//end of runAs-block

    }





    @IsTest
    static void testGetErrorResponse(){

        LEX_NewRollOutController.getErrorResponse( 'Testing' );


    }





    /**this function will create a test user
     * @param profile_nameP is the profile we are setting on the user
     * @return will return a User record
     */
    private static void createUser( String profile_nameP ){

        User test_user = new User();                    //this will hold our user
        UserRole user_role = getUserRole( ROLE_NAME );          //get the user role

        //setup the user attributes
        test_user.LastName = TEST_LASTNAME;
        test_user.email = 'test@test.com';
        test_user.Username = 'testwwww@test.com.test1';
        test_user.CommunityNickname = 'testing';
        test_user.Alias = 'testing';
        test_user.EmailEncodingKey = 'ISO-8859-1';
        test_user.TimeZoneSidKey = 'America/Los_Angeles';
        test_user.LocaleSidKey = 'en_US';
        test_user.LanguageLocaleKey = 'en_US';
        test_user.UserRoleId = user_role.Id;

        test_user.ProfileId = [ SELECT Id
                                FROM Profile
                                WHERE Name =: profile_nameP ].Id;

        insert test_user;

    }//end of function definition





    private static void createCommunityUser( String contact_idP  ){

        User test_user = new User();                    //this will hold our user

        //setup the user attributes
        test_user.LastName = TEST_COMMUNITY_LASTNAME;
        test_user.email = 'test2@test.com';
        test_user.Username = 'testwwww@test2.com.test1';
        test_user.CommunityNickname = 'testing2';
        test_user.Alias = 'testing2';
        test_user.EmailEncodingKey = 'ISO-8859-1';
        test_user.TimeZoneSidKey = 'America/Los_Angeles';
        test_user.LocaleSidKey = 'en_US';
        test_user.LanguageLocaleKey = 'en_US';
        test_user.ContactId = contact_idP;
        test_user.isActive = true;

        test_user.ProfileId = [ SELECT Id
                                FROM Profile
                                WHERE Name =: 'Tecex Customer' ].Id;

        insert test_user;

    }//end of function definition





    private static void createAccountAndContact( String user_idP ){

        Account test_account = new Account( name = ACCOUNT_NAME, Type = 'Client', CSE_IOR__c = user_idP );              //this is our test account
        insert test_account;

        Contact test_contact = new Contact( LastName = CONTACT_LASTNAME, AccountId = test_account.Id );                        //this is our test contact
        insert test_contact;

    }




    private static void createRole(){

        UserRole test_role = new UserRole( DeveloperName='DeveloperName', Name=ROLE_NAME );
        insert test_role;

    }//end of function definition




    private static Account getAccount( String account_nameP ){

        Account acc = [ SELECT Id, Name, Type, CSE_IOR__c
                        FROM Account
                        WHERE Name =: account_nameP ];

        return acc;

    }





    private static Contact getContact( String contact_last_nameP ){

        Contact con = [ SELECT Id, LastName, AccountId
                        FROM Contact
                        WHERE LastName =: contact_last_nameP ];

        return con;

    }





    /** this function will return User record
     *  @param last_nameP is the last name of the user
     */
    private static User getUser( String last_nameP ){

        //select the user
        User usr = [ SELECT Id
                     FROM User
                     WHERE LastName =: last_nameP ];

        return usr;             //return the user object

    }//end of function definition





    /** this function will return a user role
     *  @ role_nameP is the name of the role
     */
    private static UserRole getUserRole( String role_nameP ){

        UserRole user_role = [ SELECT Id
                               FROM UserRole
                               WHERE Name =: role_nameP ];

        return user_role;

    }




}