@RestResource(urlMapping='/Defaultcreation/*')
global class CPDefaultCreation {
    
    @Httppost
        global static void CPDefaultCreations(){
     	RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        Blob body = req.requestBody;
    	res.addHeader('Content-Type', 'application/json');
        String requestString = body.toString();
        CPDefaultCreationwrapper rw = (CPDefaultCreationwrapper)JSON.deserialize(requestString,CPDefaultCreationwrapper.class);
     try {
           
           
          If(!rw.DefaultPref.isEmpty()){
              
          list<CPDefaults__c> DefaultPrefList = new list<CPDefaults__c>();
              String ClientId='asdf';
              String OrderType='asdf';
             		
              for(Integer i=0; rw.DefaultPref.size()>i;i++) {
              
           CPDefaults__c CPD1 = new CPDefaults__c(
            Client_Account__c= rw.DefaultPref[i].ClientAccID,
            Defaults_Created_by__c= rw.DefaultPref[i].ContactID,
            Chargeable_Weight_Units__c = rw.DefaultPref[i].Chargeable_Weight_Units,
            Li_ion_Batteries__c = rw.DefaultPref[i].ion_Batteries,
            Order_Type__c=rw.DefaultPref[i].Order_Type,
            Package_Dimensions__c=rw.DefaultPref[i].Package_Dimensions,
            Package_Weight_Units__c=rw.DefaultPref[i].Package_Weight_Units,
           	Second_hand_parts__c=rw.DefaultPref[i].Second_hand_parts,
            Service_Type__c=rw.DefaultPref[i].Service_Type,
            TecEx_to_handle_freight__c=rw.DefaultPref[i].TecEx_to_handle_freight,
            Ship_From__c=rw.DefaultPref[i].Country
          );
           
               DefaultPrefList.add(CPD1); 
               ClientId = rw.DefaultPref[i].ClientAccID;
               OrderType = rw.DefaultPref[i].Order_Type;
                   
             }
              
                   List<CPDefaults__c> CPD = [Select ID from CPDefaults__c where Client_Account__c=:ClientID and Order_Type__c=:OrderType];
              
              if(CPD.size()==NULL || CPD.size()==0 ){
              Insert DefaultPrefList;
                   res.responseBody = Blob.valueOf(JSON.serializePretty(DefaultPrefList));
            	   res.statusCode = 200;
              	API_Log__c Al = New API_Log__c();
                Al.Account__c=DefaultPrefList[0].Client_Account__c;
                
                Al.EndpointURLName__c='CPDefaultCreation';
                Al.Response__c='Success-Default preferecenses Created';
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al; 
              
              
              }
              
              Else{
             String ErrorString ='Defaults already existing, Please update '+CPD[0].ID;
            res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
            res.statusCode = 400;
              	API_Log__c Al = New API_Log__c();
                Al.Account__c=DefaultPrefList[0].Client_Account__c;
                Al.EndpointURLName__c='CPDefaultCreation';
                Al.Response__c= 'Default preferences already created for selected order type' +CPD[0].ID;
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al; 
              }
            }
            }
        	catch (Exception e){
                
              	String ErrorString ='Something went wrong while creating default preferences, please contact support_SF@tecex.com';
               	res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
            	res.statusCode = 500;
                API_Log__c Al = New API_Log__c();
                Al.EndpointURLName__c='CPDefaultCreation';
                Al.Response__c=e.getMessage()+' at line number: '+e.getLineNumber();
                Al.StatusCode__c=string.valueof(res.statusCode);
              	Insert Al;

              	}
        
        
   }
    

}