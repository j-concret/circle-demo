@isTest
public class ValidateEmail_Test {
     static testMethod void  setUpData(){
          Account account = new Account (name='Acme1', Type ='Client', 
                                       CSE_IOR__c = '0050Y000001LTZO', 
                                       Service_Manager__c = '0050Y000001LTZO', 
                                       Financial_Manager__c='0050Y000001LTZO', 
                                       Financial_Controller__c='0050Y000001LTZO', 
                                       Tax_recovery_client__c  = FALSE,
                                       Ship_From_Line_1__c = 'Ship_From_Line_1',
                                       Ship_From_Line_2__c = 'Ship_From_Line_2',
                                       Ship_From_Line_3__c = 'Ship_From_Line_3',
                                       Ship_From_City__c = 'Ship_From_City', 
                                       Ship_From_Zip__c = 'Ship_From_Zip',
                                       Ship_From_Country__c = 'Ship_From_Country' , 
                                       Ship_From_Other_notes__c = 'Ship_From_Other',
                                       Ship_From_Contact_Name__c = 'Ship_From_Contact',
                                       Ship_From_Contact_Email__c = 'Ship_From_Email',
                                       Ship_From_Contact_Tel__c = 'Ship_From_Tel',
                                       Client_Address_Line_1__c = 'Client_Address_Line_1',
                                       Client_Address_Line_2__c = 'Client_Address_Line_2',
                                       Client_Address_Line_3__c = 'Client_Address_Line_3',
                                       Client_City__c = 'Client_City',
                                       Client_Zip__c = 'Client_Zip',
                                       Client_Country__c = 'Client_Country', 
                                       Other_notes__c = 'Other_notes',
                                       Tax_Name__c = 'Tax_Name',
                                       Tax_ID__c = 'Tax_ID',
                                       Contact_Name__c = 'Contact_Name',
                                       Contact_Email__c = 'Contact_Email',
                                       Contact_Tel__c = 'Contact_Tel'
                                      );
        insert account;  
         
         
        Account account2 = new Account (name='Test Supplier', Type ='Supplier', ICE__c = '0050Y000001km5c', RecordTypeId = '0120Y0000006KjQQAU');
        insert account2;        
         
        Contact contact = new Contact ( lastname ='Testing Individual',  AccountId = account.Id,email='anilk@tecex.com',userName__c='anilk@tecex.com',contact_Status__C='Active');
        insert contact;  
        
        Contact contact2 = new Contact ( lastname ='Testing supplier',  AccountId = account2.Id);
        insert contact2; 
        
         
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
       
       
        req.requestURI = '/services/apexrest/ValidateEmailId'; 
        req.addParameter('Username', Contact.userName__c);
        
        req.httpMethod = 'GET';
        req.addHeader('Content-Type', 'application/json');
        
        RestContext.request = req;
        RestContext.response = res;
        
         Test.startTest();
         ValidateEmail.Validate();
         Test.stopTest();
         
     }
      static testMethod void  setUpData2(){
          Account account = new Account (name='Acme1', Type ='Client', 
                                       CSE_IOR__c = '0050Y000001LTZO', 
                                       Service_Manager__c = '0050Y000001LTZO', 
                                       Financial_Manager__c='0050Y000001LTZO', 
                                       Financial_Controller__c='0050Y000001LTZO', 
                                       Tax_recovery_client__c  = FALSE,
                                       Ship_From_Line_1__c = 'Ship_From_Line_1',
                                       Ship_From_Line_2__c = 'Ship_From_Line_2',
                                       Ship_From_Line_3__c = 'Ship_From_Line_3',
                                       Ship_From_City__c = 'Ship_From_City', 
                                       Ship_From_Zip__c = 'Ship_From_Zip',
                                       Ship_From_Country__c = 'Ship_From_Country' , 
                                       Ship_From_Other_notes__c = 'Ship_From_Other',
                                       Ship_From_Contact_Name__c = 'Ship_From_Contact',
                                       Ship_From_Contact_Email__c = 'Ship_From_Email',
                                       Ship_From_Contact_Tel__c = 'Ship_From_Tel',
                                       Client_Address_Line_1__c = 'Client_Address_Line_1',
                                       Client_Address_Line_2__c = 'Client_Address_Line_2',
                                       Client_Address_Line_3__c = 'Client_Address_Line_3',
                                       Client_City__c = 'Client_City',
                                       Client_Zip__c = 'Client_Zip',
                                       Client_Country__c = 'Client_Country', 
                                       Other_notes__c = 'Other_notes',
                                       Tax_Name__c = 'Tax_Name',
                                       Tax_ID__c = 'Tax_ID',
                                       Contact_Name__c = 'Contact_Name',
                                       Contact_Email__c = 'Contact_Email',
                                       Contact_Tel__c = 'Contact_Tel'
                                      );
        insert account;  
         
         
        Account account2 = new Account (name='Test Supplier', Type ='Supplier', ICE__c = '0050Y000001km5c', RecordTypeId = '0120Y0000006KjQQAU');
        insert account2;        
         
        Contact contact = new Contact ( lastname ='Testing Individual',  AccountId = account.Id,email='anilk@tecex.com',userName__c='anilk@tecex.com',contact_Status__C='Inactive');
        insert contact;  
        
        Contact contact2 = new Contact ( lastname ='Testing supplier',  AccountId = account2.Id);
        insert contact2; 
        
         
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
       
       
        req.requestURI = '/services/apexrest/ValidateEmailId'; 
        req.addParameter('Username', Contact.userName__c);
        
        req.httpMethod = 'GET';
        req.addHeader('Content-Type', 'application/json');
        
        RestContext.request = req;
        RestContext.response = res;
        
         Test.startTest();
         ValidateEmail.Validate();
         Test.stopTest();
         
     }
     static testMethod void  setUpData3(){
          Account account = new Account (name='Acme1', Type ='Client', 
                                       CSE_IOR__c = '0050Y000001LTZO', 
                                       Service_Manager__c = '0050Y000001LTZO', 
                                       Financial_Manager__c='0050Y000001LTZO', 
                                       Financial_Controller__c='0050Y000001LTZO', 
                                       Tax_recovery_client__c  = FALSE,
                                       Ship_From_Line_1__c = 'Ship_From_Line_1',
                                       Ship_From_Line_2__c = 'Ship_From_Line_2',
                                       Ship_From_Line_3__c = 'Ship_From_Line_3',
                                       Ship_From_City__c = 'Ship_From_City', 
                                       Ship_From_Zip__c = 'Ship_From_Zip',
                                       Ship_From_Country__c = 'Ship_From_Country' , 
                                       Ship_From_Other_notes__c = 'Ship_From_Other',
                                       Ship_From_Contact_Name__c = 'Ship_From_Contact',
                                       Ship_From_Contact_Email__c = 'Ship_From_Email',
                                       Ship_From_Contact_Tel__c = 'Ship_From_Tel',
                                       Client_Address_Line_1__c = 'Client_Address_Line_1',
                                       Client_Address_Line_2__c = 'Client_Address_Line_2',
                                       Client_Address_Line_3__c = 'Client_Address_Line_3',
                                       Client_City__c = 'Client_City',
                                       Client_Zip__c = 'Client_Zip',
                                       Client_Country__c = 'Client_Country', 
                                       Other_notes__c = 'Other_notes',
                                       Tax_Name__c = 'Tax_Name',
                                       Tax_ID__c = 'Tax_ID',
                                       Contact_Name__c = 'Contact_Name',
                                       Contact_Email__c = 'Contact_Email',
                                       Contact_Tel__c = 'Contact_Tel'
                                      );
        insert account;  
         
         
        Account account2 = new Account (name='Test Supplier', Type ='Supplier', ICE__c = '0050Y000001km5c', RecordTypeId = '0120Y0000006KjQQAU');
        insert account2;        
         
        Contact contact = new Contact ( lastname ='Testing Individual',  AccountId = account.Id,email='anilk@tecex.com',userName__c='anilk@tecex.com',contact_Status__C='Inactive');
        insert contact;  
        
        Contact contact2 = new Contact ( lastname ='Testing supplier',  AccountId = account2.Id);
        insert contact2; 
        
         
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
       
       
        req.requestURI = '/services/apexrest/ValidateEmailId'; 
    //    req.addParameter('Username', Contact.userName__c);
        
        req.httpMethod = 'GET';
        req.addHeader('Content-Type', 'application/json');
        
        RestContext.request = req;
        RestContext.response = res;
        
         Test.startTest();
         ValidateEmail.Validate();
         Test.stopTest();
         
     }
    
    

}