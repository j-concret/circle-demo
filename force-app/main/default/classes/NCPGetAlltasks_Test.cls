@isTest
    public class NCPGetAlltasks_Test {

    static testMethod void  testForNCPGetAlltasks(){
        Id shipmentOrderRecTypeId = Schema.SObjectType.shipment_order__c.getRecordTypeInfosByName().get('Cost Estimate').getRecordTypeId();
		Id accountRecTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('IOR/EOR Supplier').getRecordTypeId();

        Access_token__c at = new Access_token__c();
        at.Access_Token__c = 'cc407192-e2bf-4ada-9ddd-dfa2e13aba9c';
        at.Status__c='Active';
        Insert at;
        
        Account acc = new Account(Name = 'Acme',RecordTypeId = accountRecTypeId);
        Insert acc;
        
        Contact contact = new Contact ( lastname ='Testing Individual',  AccountId = acc.Id,Client_Notifications_Choice__c='Opt-In');
        insert contact; 
        
        
        Country__c country = new Country__c(Brokerage_Costs__c = 618, CIF_Adjustment_Factor__c = 0.035, CIF_Absolute_value_adjustment__c = 200, Clearance_Costs__c = 488, 
                                            Country__c = 'Brazil', Name = 'Brazil', Handling_Costs__c = 1180, License_Permit_Costs__c = 0);
        insert country;
        
        CPA_v2_0__c RelatedCPA = new CPA_v2_0__c(Name = 'Brazil', Country_Applied_Costings__c=true );
        insert RelatedCPA;
        CPA_v2_0__c cpav2 = new CPA_v2_0__c(Name = 'Brazil Testing 2.0', Supplier__c = acc.Id, Service_Type__c = 'IOR/EOR', Related_Costing_CPA__c = 'a260Y00000EnDjr', Preferred_Supplier__c = TRUE,
                                            VAT_Rate__c = 0.1, Destination__c = 'Brazil',
                                            VAT_Reclaim_Destination__c = FALSE,
                                            Final_Destination__c ='Brazil',
                                            Country__c = country.id,
                                            CIF_Freight_and_Insurance__c  = 'CIF Amount'
                                           );
        insert cpav2;
        
        
        CpaRules__c cpaRule = new CpaRules__c();
        cpaRule.Country__c = 'Finland';
        cpaRule.Criteria__c = 'Default';
        cpaRule.DefaultCPA2__c = cpav2.Id;
        cpaRule.CPA2__c =  cpav2.Id;
        Insert cpaRule;
        
        
        
        IOR_Price_List__c priceList = new IOR_Price_List__c();
        priceList.Destination__c = 'Finland';
        priceList.Client_Name__c = acc.Id;
        priceList.IOR_Fee__c = 10;
        
        Insert priceList;
        
        shipment_order__c shipment = new shipment_order__c();
        shipment.RecordTypeId = shipmentOrderRecTypeId;
        shipment.Destination__c = 'Finland';
        shipment.IOR_Price_List__c = priceList.Id;
        shipment.Account__c = acc.Id;
        shipment.CPA_v2_0__c = cpav2.Id;
        shipment.shipping_status__c='In Transit to Hub';
        Insert shipment;
        
        Master_Task__c masterTask = new Master_Task__c(Name = 'Demo Task',Client_Visible__c=TRUE);
        Insert masterTask;
        
        //insert new Master_Task_Prerequisite__c(Master_Task_Dependent__c = masterTask.Id, Master_Task_Prerequisite__c = masterTask.Id);

         Master_Task_Template__c mstTaskTemp = new Master_Task_Template__c(Displayed_to__c = 'Client',
                                                                          Master_Task__c = masterTask.Id,
                                                                          State__c = 'TecEx Pending',Task_Title__c = 'Demo',
                                                                          Task_Description__c = 'Testing {0} {1}',
                                                                          Task_Description_Fields__c='Shipment_Order__c.Name,Task.Subject',
                                                                          Complete_Button_Text__c = 'Submit',Complete_Button_Action__c = 'Yes',
                                                                          Complete_Button_Target_Object_API_Name__c = 'Task',
                                                                          Complete_Button_Target_Field_API_Name__c = 'State__c',
                                                                          Complete_Button_Target_Field_Set_Value__c = 'Resolved',
                                                                          Description__c = 'Testing {0} {1}',
                                                                          Description_Fields__c = 'Shipment_Order__c.Name,Task.Subject',
                                                                          Building_Block_Type__c = 'Instruction');
        insert mstTaskTemp;
         ContentVersion cv = new ContentVersion();
        cv.Title = 'Test Document';
        cv.PathOnClient = 'TestDocument.pdf';
        cv.VersionData = Blob.valueOf('Test Content');
        cv.IsMajorVersion = true;
        
        Insert cv;
        Id TaskRecordTypeId = Schema.SObjectType.Task.getRecordTypeInfosByName().get('MasterTaskRelated').getRecordTypeId();

        Task tk = new Task(status = 'In Progress',RecordTypeId = TaskRecordTypeId,
                           priority = 'Normal',State__c = 'TecEx Pending',
                           WhatId = shipment.Id,
                           Master_Task__c = masterTask.Id,
                           IsNotApplicable__c = false);
        Insert tk;

        FeedItem fd = new FeedItem(Title = 'Hello',ParentId = tk.Id, body='body',visibility='AllUsers');
        
        insert fd;
        
        FeedAttachment fda = new FeedAttachment(type='Content',FeedEntityId = fd.id, recordId=cv.Id);
        
        insert fda;
        NCPGetAlltasksWra obj = new NCPGetAlltasksWra();
        obj.Accesstoken = at.Access_Token__c;
        obj.AccountID = acc.id;
        
        RestRequest req1 = new RestRequest();
        RestResponse res1 = new RestResponse();
        req1.requestURI = '/services/apexrest/NCPGetAllTasks/'; 
        req1.httpMethod = 'POST';
        req1.requestBody = Blob.valueOf(JSON.serialize(obj));     
        RestContext.request = req1;
        RestContext.response = res1;
        RestContext.request.addHeader('Content-Type', 'application/json');
        
        Test.startTest(); 
        	NCPGetAlltasks.NCPGetAlltasks();
        Test.stopTest();
    }
    
    static testMethod void  testErrorForNCPGetAlltasks(){
        
        RestRequest req1 = new RestRequest();
        RestResponse res1 = new RestResponse();
        req1.requestURI = '/services/apexrest/NCPGetAllTasks/'; 
        req1.httpMethod = 'POST';
        req1.requestBody = Blob.valueOf('{"value":"Test Response"}');     
        RestContext.request = req1;
        RestContext.response = res1;
        RestContext.request.addHeader('Content-Type', 'application/json');
        
        Test.startTest(); 
        	NCPGetAlltasks.NCPGetAlltasks();
        Test.stopTest();
    }
}