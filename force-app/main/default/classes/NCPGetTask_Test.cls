@isTest
public class NCPGetTask_Test {

    static testMethod void  testForNCPGetTask(){
        
        Access_token__c at = new Access_token__c();
        at.Access_Token__c = 'cc407192-e2bf-4ada-9ddd-dfa2e13aba9c';
        at.Status__c='Active';
        Insert at;
        
        Account acc = new Account(Name = 'TecEx Prospective Client');
        Insert acc;
        
        Master_Task__c masterTask = new Master_Task__c(Name = 'Demo Task', States__c = 'TecEx Pending');
        Insert masterTask;
        
        Task tk = new Task(status = 'In Progress',
                           priority = 'Normal',
                           WhatId = acc.Id,
                           Master_Task__c = masterTask.Id);
        Insert tk;
         ContentVersion cv = new ContentVersion();
        cv.Title = 'Test Document';
        cv.PathOnClient = 'TestDocument.pdf';
        cv.VersionData = Blob.valueOf('Test Content');
        cv.IsMajorVersion = true;
        
        Insert cv;
        FeedItem fd = new FeedItem(Title = 'Hello',ParentId = tk.Id, body='body',visibility='AllUsers');
        
        insert fd;
        
        FeedAttachment fda = new FeedAttachment(type='Content',FeedEntityId = fd.id, recordId=cv.Id);
        
        insert fda;
        NCPGetTaskWrapper obj = new NCPGetTaskWrapper();
        obj.Accesstoken = at.Access_Token__c;
        obj.TaskID = tk.Id;
        
        RestRequest req1 = new RestRequest();
        RestResponse res1 = new RestResponse();
        req1.requestURI = '/services/apexrest/NCPGetTaskDetails/'; 
        req1.httpMethod = 'POST';
        req1.requestBody = Blob.valueOf(JSON.serialize(obj));     
        RestContext.request = req1;
        RestContext.response = res1;
        RestContext.request.addHeader('Content-Type', 'application/json');
        
        Test.startTest(); 
        	NCPGetTask.NCPGetAlltasks();
        Test.stopTest();
    }
    
    static testMethod void  testErrorForNCPGetTask(){
        
        RestRequest req1 = new RestRequest();
        RestResponse res1 = new RestResponse();
        req1.requestURI = '/services/apexrest/NCPGetTaskDetails/'; 
        req1.httpMethod = 'POST';
        req1.requestBody = Blob.valueOf('{"value":"Test Response"}');     
        RestContext.request = req1;
        RestContext.response = res1;
        RestContext.request.addHeader('Content-Type', 'application/json');
        
        Test.startTest(); 
        	NCPGetTask.NCPGetAlltasks();
        Test.stopTest();
    }
}