global class TX_WebServices {

	webservice static String pushClientToDragon(ID AccountId) {
		String result = new TH_Account().pushClientToDragon(AccountId).toJSON();
		System.debug('JSON response will be: ' + result);
		return result;	
	}

	webservice static String sendEmailSummary(List<Shipment_Order__c> SOList, String ClientId, String newEmailAddress){
			Shipping_Order_Email_Summary.sendEmailSummary(null,ClientId,newEmailAddress);
			return 'Email Sent';
    }
}