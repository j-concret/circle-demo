@isTest

public class SOA_Test {
    
         private static testMethod void  setUpData(){
        Account account = new Account (name='Acme1',  Type ='Client',  CSE_IOR__c = '0050Y000001km5c',  Service_Manager__c = '0050Y000001km5c',  Tax_recovery_client__c  = FALSE, Ship_From_Line_1__c = 'Ship_From_Line_1',
                                       Ship_From_Line_2__c = 'Ship_From_Line_2', Ship_From_Line_3__c = 'Ship_From_Line_3', Ship_From_City__c = 'Ship_From_City',  Ship_From_Zip__c = 'Ship_From_Zip',
                                       Ship_From_Country__c = 'Ship_From_Country' ,  Ship_From_Other_notes__c = 'Ship_From_Other', Ship_From_Contact_Name__c = 'Ship_From_Contact', Ship_From_Contact_Email__c = 'Ship_From_Email',
                                       Ship_From_Contact_Tel__c = 'Ship_From_Tel', Client_Address_Line_1__c = 'Client_Address_Line_1', Client_Address_Line_2__c = 'Client_Address_Line_2', Client_Address_Line_3__c = 'Client_Address_Line_3',
                                       Client_City__c = 'Client_City', Client_Zip__c = 'Client_Zip', Client_Country__c = 'Client_Country',  Other_notes__c = 'Other_notes',  Tax_Name__c = 'Tax_Name', Tax_ID__c = 'Tax_ID',
                                       Contact_Name__c = 'Contact_Name', Contact_Email__c = 'Contact_Email', Contact_Tel__c = 'Contact_Tel', Financial_Controller__c = '0050Y000001km5c' ); insert account;  
		
		
        Account account2 = new Account (name='Test Supplier', Type ='Supplier', ICE__c = '0050Y000001km5c', RecordTypeId = '0120Y0000006KjQQAU'); insert account2;      
        
        Contact contact = new Contact ( lastname ='Testing Individual',  AccountId = account.Id); insert contact;  
        
        IOR_Price_List__c iorpl = new IOR_Price_List__c ( Client_Name__c = account.Id, Name = 'Brazil', IOR_Fee__c = 0.15, On_Charge_Mark_up__c = 0.15,
                                                         TecEx_Shipping_Fee_Markup__c =0.40, IOR_Min_Fee__c = 100,  Admin_Fee__c =100, Set_up_fee__c =100, Bank_Fees__c =200, Tax_Rate__c = 0.20,
                                                         Estimated_Customs_Brokerage__c =250, Estimated_Customs_Clearance__c =250, Estimated_Customs_Handling__c =250, Estimated_Import_License__c =250, 
                                                         Destination__c = 'Brazil' ); insert iorpl; 
        
        country_price_approval__c cpa = new country_price_approval__c( name ='Brazil', Billing_term__c = 'DAP/CIF - IOR pays',  Airwaybill_Instructions__c = 'User IOR Address', Supplier__c = account2.Id, 
                                                                      In_Country_Specialist__c = '0050Y000001km5c', Clearance_Destination__c = 'Brazil', Preferred_Supplier__c = TRUE, 
                                                                      Destination__c = 'Brazil' ); insert cpa;
        
        
        
        Country__c country = new Country__c(Brokerage_Costs__c = 618, CIF_Adjustment_Factor__c = 0.035, CIF_Absolute_value_adjustment__c = 200, Clearance_Costs__c = 488, 
                                            Country__c = 'Brazil', Name = 'Brazil', Handling_Costs__c = 1180, License_Permit_Costs__c = 0); insert country;
	   
        CPA_v2_0__c aCpav = new CPA_v2_0__c(Name = 'Brazil', Country_Applied_Costings__c = TRUE); insert aCpav;
        
        CPA_v2_0__c cpav2 = new CPA_v2_0__c(Name = 'Brazil Testing 2.0', Supplier__c = account2.Id, Service_Type__c = 'IOR/EOR', Related_Costing_CPA__c = aCpav.Id, 
                                            Preferred_Supplier__c = TRUE, VAT_Rate__c = 0.1, Destination__c = 'Brazil',  Final_Destination__c = 'Brazil', 
                                            VAT_Reclaim_Destination__c = FALSE, Country__c = country.id, Lead_ICE__c = '0050Y000001km5c'); insert cpav2; 
             
         List<CPARules__c> CPARules = new List<CPARules__c>();
         CPARules.add(new CPARules__c(Country__c = 'Brazil',Criteria__c ='Default', Chargable_weight__c = 'NONE', Courier_Responsibility__c = 'NONE',   ECCN_No__c = 'NONE', NL_Product_User__c = 'NONE', Number_of_final_deliveries__c = 'NONE', ShipmentValue__c ='NONE', Sum_of_quantity_of_matched_products__c = 'NONE', Manufacturer__c = 'NONE', DefaultCPA2__c = cpav2.Id, CPA2__c =  cpav2.Id));
         insert CPARules;
             
        Currency_Management2__c CM = new Currency_Management2__c (Name = 'US Dollar (USD)', Currency__c = 'US Dollar (USD)', ISO_Code__c = 'EUR', Conversion_Rate__c = 1);
        insert CM; 
             
             
             
             List<CPA_Costing__c> costs = new List<CPA_Costing__c>();
       
       
      costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'Shipment value', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'IOR', Min__c = 375, Max__c = 14000, Conditional_value__c = 'Shipment value', Condition__c = 'Between', Floor__c = 0, Ceiling__c = 10000, Currency__c = 'US Dollar (USD)', Variable_threshold__c = null));
      costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'CIF value', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'IOR', Max__c = 50000, Conditional_value__c = 'Shipment value', Condition__c = 'Between', Floor__c = 0, Ceiling__c = 10000, Currency__c = 'US Dollar (USD)' , Variable_threshold__c = null));
      costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'Total Taxes', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'IOR', Min__c = 14000, Conditional_value__c = 'Shipment value', Condition__c = '<', Floor__c = 20000,  Currency__c = 'US Dollar (USD)' , Variable_threshold__c = null));
      costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'Shipment value', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'IOR', Conditional_value__c = 'CIF value', Condition__c = '>=', Floor__c = 5000, Currency__c = 'US Dollar (USD)' , Variable_threshold__c = null));

      costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'Shipment value', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'IOR', Min__c = 375, Max__c = 14000, Currency__c = 'US Dollar (USD)', Variable_threshold__c = 1000));
      costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'CIF value', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'IOR', Max__c = 50000, Currency__c = 'US Dollar (USD)', Variable_threshold__c = 5000));
      costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'Total Taxes', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'IOR', Min__c = 14000, Currency__c = 'US Dollar (USD)', Variable_threshold__c = 500));
      costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'Shipment value', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'IOR', Currency__c = 'US Dollar (USD)', Variable_threshold__c = 9000));

      
      costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'Chargeable weight', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'IOR', Min__c = 375, Max__c = 500, Conditional_value__c = 'Chargeable weight', Condition__c = 'Between', Floor__c = 0, Ceiling__c = 500, Currency__c = 'US Dollar (USD)' , Variable_threshold__c = null));
      costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = '# packages', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'IOR', Max__c = 200, Conditional_value__c = '# of packages', Condition__c = '>', Floor__c = 5,  Currency__c = 'US Dollar (USD)' , Variable_threshold__c = null));
      costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = '# Parts (line items)', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'IOR', Min__c = 100, Currency__c = 'US Dollar (USD)' , Variable_threshold__c = null));
      costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = '# Unique HS Codes', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'IOR', Currency__c = 'US Dollar (USD)' , Variable_threshold__c = null));

      costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'Chargeable weight', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'IOR', Min__c = 375, Max__c = 500, Currency__c = 'US Dollar (USD)', Variable_threshold__c = 100));
      costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = '# packages', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'IOR', Max__c = 200, Currency__c = 'US Dollar (USD)', Variable_threshold__c = 4));
      costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = '# Parts (line items)', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'IOR', Min__c = 100, Currency__c = 'US Dollar (USD)', Variable_threshold__c = 2));
      costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = '# of Final Deliveries', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'IOR', Currency__c = 'US Dollar (USD)',  Variable_threshold__c = 2));
      
      costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = null, Cost_Type__c = 'Fixed',  Amount__c = 800, IOR_EOR__c = 'IOR', Currency__c = 'US Dollar (USD)', Variable_threshold__c = null));
      costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = null, Cost_Type__c = 'Fixed',  Amount__c = 900, IOR_EOR__c = 'IOR', Currency__c = 'US Dollar (USD)', Variable_threshold__c = null));
      costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = null, Cost_Type__c = 'Fixed',  Amount__c = 100, IOR_EOR__c = 'IOR', Currency__c = 'US Dollar (USD)', Variable_threshold__c = null));
      costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = null, Cost_Type__c = 'Fixed',  Amount__c = 350, IOR_EOR__c = 'IOR', Currency__c = 'US Dollar (USD)', Variable_threshold__c = null));
        
       insert costs;
        
             
        
            Product2 product = new Product2(Name = 'CAB-ETH-S-RJ45', Manufacturer__c = '0011v00001p4OOc'); 
             
             Insert product;
             
           HS_Codes_and_Associated_Details__c hsCode = new HS_Codes_and_Associated_Details__c(Name = '85444210', Destination_HS_Code__c = '85444210', Description__c = 'Decriprion', Country__c = country.id,
                                                                                             Additional_Part_Specific_Tax_Rate_1__c = 0.1, Additional_Part_Specific_Tax_Rate_2__c = 0.1, Additional_Part_Specific_Tax_Rate_3__c = 0.1,
                                                                                             Rate__c = 0.25);
         
        
        List<Tax_Structure_Per_Country__c> taxStructure = new List<Tax_Structure_Per_Country__c>();
		taxStructure.add(new Tax_Structure_Per_Country__c(Country__c = country.Id, Applied_to_Value__c = 'CIF', Name = 'I', Order_Number__c = '1', Part_Specific__c = TRUE, Tax_Type__c = 'Duties', Default_Duty_Rate__c = 0.5625)); 
        taxStructure.add(new Tax_Structure_Per_Country__c(Country__c = country.Id, Applied_to_Value__c = 'CIF', Name = 'II', Order_Number__c = '2', Part_Specific__c = FALSE, Tax_Type__c = 'VAT', Rate__c = 0.40));
        taxStructure.add(new Tax_Structure_Per_Country__c(Country__c = country.Id, Applied_to_Value__c = 'CIF', Name = 'III', Order_Number__c = '3', Part_Specific__c = FALSE, Tax_Type__c = 'Other', Rate__c = 0.40));
        taxStructure.add(new Tax_Structure_Per_Country__c(Country__c = country.Id, Applied_to_Value__c = 'CIF', Name = 'IV', Order_Number__c = '4', Part_Specific__c = FALSE, Tax_Type__c = 'Other',Rate__c = 0.40, Applies_to_Order__c = '1'));     
        taxStructure.add(new Tax_Structure_Per_Country__c(Country__c = country.Id, Applied_to_Value__c = 'CIF', Name = 'V', Order_Number__c = '5', Part_Specific__c = FALSE, Tax_Type__c = 'Other', Rate__c = 0.40, Min__c = 20, Max__c = 100));
        taxStructure.add(new Tax_Structure_Per_Country__c(Country__c = country.Id, Applied_to_Value__c = 'CIF', Name = 'VI', Order_Number__c = '6', Part_Specific__c = FALSE, Tax_Type__c = 'Other', Rate__c = 0.40, Applies_to_Order__c = '2,3,4,5', Additional_Percent__c = 0.01));
        taxStructure.add(new Tax_Structure_Per_Country__c(Country__c = country.Id, Applied_to_Value__c = 'CIF', Name = 'VII', Order_Number__c = '7', Part_Specific__c = FALSE, Tax_Type__c = 'Other', Rate__c = 0.40, Min__c = 20));       
        taxStructure.add(new Tax_Structure_Per_Country__c(Country__c = country.Id, Applied_to_Value__c = 'CIF', Name = 'VIII', Order_Number__c = '8', Part_Specific__c = FALSE, Tax_Type__c = 'Other', Rate__c = 0.40, Applies_to_Order__c = '2,3,4,5,6'));
        taxStructure.add(new Tax_Structure_Per_Country__c(Country__c = country.Id, Applied_to_Value__c = 'CIF', Name = 'IX', Order_Number__c = '9', Part_Specific__c = FALSE, Tax_Type__c = 'Other', Rate__c = 0.40, Max__c = 100));
        taxStructure.add(new Tax_Structure_Per_Country__c(Country__c = country.Id, Applied_to_Value__c = 'CIF', Name = 'X', Order_Number__c = '10', Part_Specific__c = FALSE, Tax_Type__c = 'Other', Rate__c = 0.40, Applies_to_Order__c = '2,3,4,5,6,7')); 
        insert taxStructure;
        
        List< Shipment_Order_Approval__c> approvals = new List<Shipment_Order_Approval__c>();
        approvals.add(new Shipment_Order_Approval__c(NAME='Test 1 Condition',ACTION__C='Please arrange for MSDS to be sent with the products',ACTIVE__C=TRUE,ASSIGNED_TO__C='AM',BINDING_QUOTE_EXEMPTED__C=FALSE,CONDITION_LOGIC__C=NULL,CONTROLLER_2__C='',CONTROLLER_3__C='',CONTROLLER__C='Does Not Equal',Description_Source__c = 'Generic Statement',DESCRIPTION__C='This product [Product] contains Li-Ion batteries. An MSDS will be required for freight purposes',FIELD_2__C='',FIELD_3__C='',FIELD_4__C='',FIELD_5__C='',FIELD_6__C='',FIELD__C='Lithium Batteries',FLAG_TYPE__C='Freight',OBJECT_1__C='Product',OBJECT_2__C='',OBJECT_3__C='',OBJECT_4__C='',OBJECT_5__C='',OBJECT_6__C='',PRIORITY__C='Shipment Alert',SUBJECT__C='Product contains Li-ion batteries: [Product]',VALUE_2__C='',VALUE_3__C='',VALUE__C='None'));
        approvals.add(new Shipment_Order_Approval__c(NAME='Test 2 Condition',ACTION__C='Review CPA Documentation Section (Required Document Notes), follow instructions for obtaining required documents or contact supplier for further information',ACTIVE__C=TRUE,ASSIGNED_TO__C='ICE',BINDING_QUOTE_EXEMPTED__C=FALSE,CONDITION_LOGIC__C='1',CONTROLLER_2__C='',CONTROLLER_3__C='',CONTROLLER__C='Does Not Equal',Description_Source__c = 'Generic Statement',DESCRIPTION__C='Product Requires Compliance Document/s [X]',FIELD_2__C='',FIELD_3__C='',FIELD_4__C='',FIELD_5__C='',FIELD_6__C='',FIELD__C='Product Specific Documents',FLAG_TYPE__C='Compliance',OBJECT_1__C='CPA',OBJECT_2__C='',OBJECT_3__C='',OBJECT_4__C='',OBJECT_5__C='',OBJECT_6__C='',PRIORITY__C='Shipment Block',SUBJECT__C='Product Compliance Document/s Required',VALUE_2__C='',VALUE_3__C='',VALUE__C='NULL'));
        insert approvals;
        
        List<Shipment_Order_Approval__c> approvalToCondition = [Select Name, Id from Shipment_Order_Approval__c where Name = 'Test 2 Condition' limit 1];
        
        String approvalId = approvalToCondition[0].Id;
        
        Shipment_Approval_Condition__c approvalCondition = new 
         Shipment_Approval_Condition__c(Condition_Number__c = '1', Condition_Object_1__c = 'Line Item',Condition_Field_1__c='Part Description', Condition_Controller__c = 'ISNULL', Shipment_Order_Approval__c = approvalId);
        insert approvalCondition;
             
        Shipment_Order__c shipmentOrder = new Shipment_Order__c(Account__c = account.id, Shipment_Value_USD__c = 2000, CPA_v2_0__c = cpav2.Id, IOR_CSE__c = '0050Y000001km5c', ICE__c = '0050Y000001km5c', Financial_Controller__c = '0050Y000001km5c',
                                                                Client_Contact_for_this_Shipment__c = contact.Id, Who_arranges_International_courier__c ='Client', Tax_Treatment__c ='DAP/CIF - IOR pays', 
                                                                Ship_to_Country__c = cpa.Id, Destination__c = 'Brazil', Service_Type__c = 'IOR', IOR_Price_List__c = iorpl.Id, Chargeable_Weight__c = 200,  
                                                                Shipping_Status__c = 'Cost Estimate Abandoned', Ship_From_Country__c = 'Angola' );  insert shipmentOrder; 
       

        Part__c part1 = new Part__c(Name ='CAB-ETH-S-RJ45',  Quantity__c= 1, Commercial_Value__c = 15000, US_HTS_Code__c = '85444210', 
        Country_of_Origin2__c = 'China', Shipment_Order__c = shipmentOrder.Id, Description_and_Functionality__c ='AB-ETH-S-RJ45'); insert part1;
        
      List<Id> SOIDS = new List<Id>();
      SOIDS.add(shipmentOrder.Id);
                
        test.startTest();
                
        SOA.approvalRules(SOIDS) ;   
        shipmentOrder.Shipping_Status__c = 'Cost Estimate'; update shipmentOrder;
		
        test.stopTest();
 
        
    } 

    
            private static testMethod void  setUpData1(){
        Account account = new Account (name='Acme1',  Type ='Client',  CSE_IOR__c = '0050Y000001km5c',  Service_Manager__c = '0050Y000001km5c',  Tax_recovery_client__c  = FALSE, Ship_From_Line_1__c = 'Ship_From_Line_1',
                                       Ship_From_Line_2__c = 'Ship_From_Line_2', Ship_From_Line_3__c = 'Ship_From_Line_3', Ship_From_City__c = 'Ship_From_City',  Ship_From_Zip__c = 'Ship_From_Zip',
                                       Ship_From_Country__c = 'Ship_From_Country' ,  Ship_From_Other_notes__c = 'Ship_From_Other', Ship_From_Contact_Name__c = 'Ship_From_Contact', Ship_From_Contact_Email__c = 'Ship_From_Email',
                                       Ship_From_Contact_Tel__c = 'Ship_From_Tel', Client_Address_Line_1__c = 'Client_Address_Line_1', Client_Address_Line_2__c = 'Client_Address_Line_2', Client_Address_Line_3__c = 'Client_Address_Line_3',
                                       Client_City__c = 'Client_City', Client_Zip__c = 'Client_Zip', Client_Country__c = 'Client_Country',  Other_notes__c = 'Other_notes',  Tax_Name__c = 'Tax_Name', Tax_ID__c = 'Tax_ID',
                                       Contact_Name__c = 'Contact_Name', Contact_Email__c = 'Contact_Email', Contact_Tel__c = 'Contact_Tel', Financial_Controller__c = '0050Y000001km5c' ); insert account;  
		
		
        Account account2 = new Account (name='Test Supplier', Type ='Supplier', ICE__c = '0050Y000001km5c', RecordTypeId = '0120Y0000006KjQQAU'); insert account2;      
        
        Contact contact = new Contact ( lastname ='Testing Individual',  AccountId = account.Id); insert contact;  
        
        IOR_Price_List__c iorpl = new IOR_Price_List__c ( Client_Name__c = account.Id, Name = 'Brazil', IOR_Fee__c = 0.15, On_Charge_Mark_up__c = 0.15,
                                                         TecEx_Shipping_Fee_Markup__c =0.40, IOR_Min_Fee__c = 100,  Admin_Fee__c =100, Set_up_fee__c =100, Bank_Fees__c =200, Tax_Rate__c = 0.20,
                                                         Estimated_Customs_Brokerage__c =250, Estimated_Customs_Clearance__c =250, Estimated_Customs_Handling__c =250, Estimated_Import_License__c =250, 
                                                         Destination__c = 'Brazil' ); insert iorpl; 
        
        country_price_approval__c cpa = new country_price_approval__c( name ='Brazil', Billing_term__c = 'DAP/CIF - IOR pays',  Airwaybill_Instructions__c = 'User IOR Address', Supplier__c = account2.Id, 
                                                                      In_Country_Specialist__c = '0050Y000001km5c', Clearance_Destination__c = 'Brazil', Preferred_Supplier__c = TRUE, 
                                                                      Destination__c = 'Brazil' ); insert cpa;
        
        Country__c country = new Country__c(Brokerage_Costs__c = 618, CIF_Adjustment_Factor__c = 0.035, CIF_Absolute_value_adjustment__c = 200, Clearance_Costs__c = 488, 
                                            Country__c = 'Brazil', Name = 'Brazil', Handling_Costs__c = 1180, License_Permit_Costs__c = 0); insert country;
	   
        CPA_v2_0__c aCpav = new CPA_v2_0__c(Name = 'Brazil', Country_Applied_Costings__c = TRUE); insert aCpav;
        
        CPA_v2_0__c cpav2 = new CPA_v2_0__c(Name = 'Brazil Testing 2.0', Supplier__c = account2.Id, Service_Type__c = 'IOR/EOR', Related_Costing_CPA__c = aCpav.Id, 
                                            Preferred_Supplier__c = TRUE, VAT_Rate__c = 0.1, Destination__c = 'Brazil',  Final_Destination__c = 'Brazil', 
                                            VAT_Reclaim_Destination__c = FALSE, Country__c = country.id, Lead_ICE__c = '0050Y000001km5c'); insert cpav2; 
             
          Currency_Management2__c CM = new Currency_Management2__c (Name = 'US Dollar (USD)', Currency__c = 'US Dollar (USD)', ISO_Code__c = 'USD', Conversion_Rate__c = 1);
        insert CM; 
           
         List<CPARules__c> CPARules = new List<CPARules__c>();
         CPARules.add(new CPARules__c(Country__c = 'Brazil',Criteria__c ='Default', Chargable_weight__c = 'NONE', Courier_Responsibility__c = 'NONE',   ECCN_No__c = 'NONE', NL_Product_User__c = 'NONE', Number_of_final_deliveries__c = 'NONE', ShipmentValue__c ='NONE', Sum_of_quantity_of_matched_products__c = 'NONE', Manufacturer__c = 'NONE', DefaultCPA2__c = cpav2.Id, CPA2__c =  cpav2.Id));
         insert CPARules;
             
             
             List<CPA_Costing__c> costs = new List<CPA_Costing__c>();
       
       
      costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'Shipment value', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'IOR', Min__c = 375, Max__c = 14000, Conditional_value__c = 'Shipment value', Condition__c = 'Between', Floor__c = 0, Ceiling__c = 10000, Currency__c = 'US Dollar (USD)', Variable_threshold__c = null));
      costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'CIF value', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'IOR', Max__c = 50000, Conditional_value__c = 'Shipment value', Condition__c = 'Between', Floor__c = 0, Ceiling__c = 10000, Currency__c = 'US Dollar (USD)' , Variable_threshold__c = null));
      costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'Total Taxes', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'IOR', Min__c = 14000, Conditional_value__c = 'Shipment value', Condition__c = '<', Floor__c = 20000,  Currency__c = 'US Dollar (USD)' , Variable_threshold__c = null));
      costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'Shipment value', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'IOR', Conditional_value__c = 'CIF value', Condition__c = '>=', Floor__c = 5000, Currency__c = 'US Dollar (USD)' , Variable_threshold__c = null));

      costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'Shipment value', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'IOR', Min__c = 375, Max__c = 14000, Currency__c = 'US Dollar (USD)', Variable_threshold__c = 1000));
      costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'CIF value', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'IOR', Max__c = 50000, Currency__c = 'US Dollar (USD)', Variable_threshold__c = 5000));
      costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'Total Taxes', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'IOR', Min__c = 14000, Currency__c = 'US Dollar (USD)', Variable_threshold__c = 500));
      costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'Shipment value', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'IOR', Currency__c = 'US Dollar (USD)', Variable_threshold__c = 9000));

      
      costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'Chargeable weight', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'IOR', Min__c = 375, Max__c = 500, Conditional_value__c = 'Chargeable weight', Condition__c = 'Between', Floor__c = 0, Ceiling__c = 500, Currency__c = 'US Dollar (USD)' , Variable_threshold__c = null));
      costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = '# packages', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'IOR', Max__c = 200, Conditional_value__c = '# of packages', Condition__c = '>', Floor__c = 5,  Currency__c = 'US Dollar (USD)' , Variable_threshold__c = null));
      costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = '# Parts (line items)', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'IOR', Min__c = 100, Currency__c = 'US Dollar (USD)' , Variable_threshold__c = null));
      costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = '# Unique HS Codes', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'IOR', Currency__c = 'US Dollar (USD)' , Variable_threshold__c = null));

      costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'Chargeable weight', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'IOR', Min__c = 375, Max__c = 500, Currency__c = 'US Dollar (USD)', Variable_threshold__c = 100));
      costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = '# packages', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'IOR', Max__c = 200, Currency__c = 'US Dollar (USD)', Variable_threshold__c = 4));
      costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = '# Parts (line items)', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'IOR', Min__c = 100, Currency__c = 'US Dollar (USD)', Variable_threshold__c = 2));
      costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = '# of Final Deliveries', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'IOR', Currency__c = 'US Dollar (USD)',  Variable_threshold__c = 2));
      
      costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = null, Cost_Type__c = 'Fixed',  Amount__c = 800, IOR_EOR__c = 'IOR', Currency__c = 'US Dollar (USD)', Variable_threshold__c = null));
      costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = null, Cost_Type__c = 'Fixed',  Amount__c = 900, IOR_EOR__c = 'IOR', Currency__c = 'US Dollar (USD)', Variable_threshold__c = null));
      costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = null, Cost_Type__c = 'Fixed',  Amount__c = 100, IOR_EOR__c = 'IOR', Currency__c = 'US Dollar (USD)', Variable_threshold__c = null));
      costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = null, Cost_Type__c = 'Fixed',  Amount__c = 350, IOR_EOR__c = 'IOR', Currency__c = 'US Dollar (USD)', Variable_threshold__c = null));
        
       insert costs;
        
        
        List<Tax_Structure_Per_Country__c> taxStructure = new List<Tax_Structure_Per_Country__c>();
		taxStructure.add(new Tax_Structure_Per_Country__c(Country__c = country.Id, Applied_to_Value__c = 'CIF', Name = 'I', Order_Number__c = '1', Part_Specific__c = TRUE, Tax_Type__c = 'Duties', Default_Duty_Rate__c = 0.5625)); 
        taxStructure.add(new Tax_Structure_Per_Country__c(Country__c = country.Id, Applied_to_Value__c = 'CIF', Name = 'II', Order_Number__c = '2', Part_Specific__c = FALSE, Tax_Type__c = 'VAT', Rate__c = 0.40));
        taxStructure.add(new Tax_Structure_Per_Country__c(Country__c = country.Id, Applied_to_Value__c = '1', Name = 'III', Order_Number__c = '3', Part_Specific__c = FALSE, Tax_Type__c = 'Other', Rate__c = 0.40));
        taxStructure.add(new Tax_Structure_Per_Country__c(Country__c = country.Id, Applied_to_Value__c = '1', Name = 'IV', Order_Number__c = '4', Part_Specific__c = FALSE, Tax_Type__c = 'Other',Rate__c = 0.40, Applies_to_Order__c = '1'));     
        taxStructure.add(new Tax_Structure_Per_Country__c(Country__c = country.Id, Applied_to_Value__c = '2', Name = 'V', Order_Number__c = '5', Part_Specific__c = FALSE, Tax_Type__c = 'Other', Rate__c = 0.40, Min__c = 20, Max__c = 100));
        taxStructure.add(new Tax_Structure_Per_Country__c(Country__c = country.Id, Applied_to_Value__c = '2', Name = 'VI', Order_Number__c = '6', Part_Specific__c = FALSE, Tax_Type__c = 'Other', Rate__c = 0.40, Applies_to_Order__c = '2,3,4,5', Additional_Percent__c = 0.01));
        taxStructure.add(new Tax_Structure_Per_Country__c(Country__c = country.Id, Applied_to_Value__c = '3', Name = 'VII', Order_Number__c = '7', Part_Specific__c = FALSE, Tax_Type__c = 'Other', Rate__c = 0.40, Min__c = 20));       
        taxStructure.add(new Tax_Structure_Per_Country__c(Country__c = country.Id, Applied_to_Value__c = '3', Name = 'VIII', Order_Number__c = '8', Part_Specific__c = FALSE, Tax_Type__c = 'Other', Rate__c = 0.40, Applies_to_Order__c = '2,3,4,5,6'));
        taxStructure.add(new Tax_Structure_Per_Country__c(Country__c = country.Id, Applied_to_Value__c = '4', Name = 'IX', Order_Number__c = '9', Part_Specific__c = FALSE, Tax_Type__c = 'Other', Rate__c = 0.40, Max__c = 100));
        taxStructure.add(new Tax_Structure_Per_Country__c(Country__c = country.Id, Applied_to_Value__c = '4', Name = 'X', Order_Number__c = '10', Part_Specific__c = FALSE, Tax_Type__c = 'Other', Rate__c = 0.40, Applies_to_Order__c = '2,3,4,5,6,7')); 
        insert taxStructure;
        
        List< Shipment_Order_Approval__c> approvals = new List<Shipment_Order_Approval__c>();
        approvals.add(new Shipment_Order_Approval__c(NAME='Test 1 Condition',ACTION__C='Please arrange for MSDS to be sent with the products',ACTIVE__C=TRUE,ASSIGNED_TO__C='AM',BINDING_QUOTE_EXEMPTED__C=FALSE,CONDITION_LOGIC__C=NULL,CONTROLLER_2__C='',CONTROLLER_3__C='',CONTROLLER__C='Does Not Equal',Description_Source__c = 'Generic Statement',DESCRIPTION__C='This product [Product] contains Li-Ion batteries. An MSDS will be required for freight purposes',FIELD_2__C='',FIELD_3__C='',FIELD_4__C='',FIELD_5__C='',FIELD_6__C='',FIELD__C='Lithium Batteries',FLAG_TYPE__C='Freight',OBJECT_1__C='Product',OBJECT_2__C='',OBJECT_3__C='',OBJECT_4__C='',OBJECT_5__C='',OBJECT_6__C='',PRIORITY__C='Shipment Alert',SUBJECT__C='Product contains Li-ion batteries: [Product]',VALUE_2__C='',VALUE_3__C='',VALUE__C='None'));
        approvals.add(new Shipment_Order_Approval__c(NAME='Test 2 Condition',ACTION__C='Review CPA Documentation Section (Required Document Notes), follow instructions for obtaining required documents or contact supplier for further information',ACTIVE__C=TRUE,ASSIGNED_TO__C='ICE',BINDING_QUOTE_EXEMPTED__C=FALSE,CONDITION_LOGIC__C='1',CONTROLLER_2__C='',CONTROLLER_3__C='',CONTROLLER__C='Does Not Equal',Description_Source__c = 'Generic Statement',DESCRIPTION__C='Product Requires Compliance Document/s [X]',FIELD_2__C='',FIELD_3__C='',FIELD_4__C='',FIELD_5__C='',FIELD_6__C='',FIELD__C='Product Specific Documents',FLAG_TYPE__C='Compliance',OBJECT_1__C='CPA',OBJECT_2__C='',OBJECT_3__C='',OBJECT_4__C='',OBJECT_5__C='',OBJECT_6__C='',PRIORITY__C='Shipment Block',SUBJECT__C='Product Compliance Document/s Required',VALUE_2__C='',VALUE_3__C='',VALUE__C='NULL'));
        insert approvals;
        
        List<Shipment_Order_Approval__c> approvalToCondition = [Select Name, Id from Shipment_Order_Approval__c where Name = 'Test 2 Condition' limit 1];
        
        String approvalId = approvalToCondition[0].Id;
        
        Shipment_Approval_Condition__c approvalCondition = new 
         Shipment_Approval_Condition__c(Condition_Number__c = '1', Condition_Object_1__c = 'Line Item',Condition_Field_1__c='Part Description', Condition_Controller__c = 'ISNULL', Shipment_Order_Approval__c = approvalId);
        insert approvalCondition;
         
        Shipment_Order__c shipmentOrder = new Shipment_Order__c(Account__c = account.id, Shipment_Value_USD__c = 2000, CPA_v2_0__c = cpav2.Id, IOR_CSE__c = '0050Y000001km5c', ICE__c = '0050Y000001km5c', Financial_Controller__c = '0050Y000001km5c',
                                                                Client_Contact_for_this_Shipment__c = contact.Id, Who_arranges_International_courier__c ='Client', Tax_Treatment__c ='DAP/CIF - IOR pays', 
                                                                Ship_to_Country__c = cpa.Id, Destination__c = 'Brazil', Service_Type__c = 'IOR', IOR_Price_List__c = iorpl.Id, Chargeable_Weight__c = 200,  
                                                                Shipping_Status__c = 'Cost Estimate', Ship_From_Country__c = 'Angola', CPA_Costings_Added__c = FALSE );  insert shipmentOrder; 
       

        List<Id> SOIDS = new List<Id>();
      SOIDS.add(shipmentOrder.Id);
                
        test.startTest();
                
       SOA.approvalRules(SOIDS) ;  
       
        Part__c part1 = new Part__c(Name ='CAB-ETH-S-RJ45',  Quantity__c= 1, Commercial_Value__c = 15000, US_HTS_Code__c = '85444210', 
        Country_of_Origin2__c = 'China', Shipment_Order__c = shipmentOrder.Id, Description_and_Functionality__c =''); insert part1;

        test.stopTest();
 
         
        
    } 

}