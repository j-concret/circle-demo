/**
 * Created by Caro on 2019-04-14.
 */

public with sharing class DAL_ShipmentOrder extends fflib_SObjectSelector {

    public List<Schema.SObjectField> getSObjectFieldList() {

        return new List<Schema.SObjectField>{
                Shipment_Order__c.Name,
                Shipment_Order__c.Id
        };
    }

    public Schema.SObjectType getSObjectType() {

        return Shipment_Order__c.sObjectType;
    }

    public Shipment_Order__c selectById(Id shipmentOrderId) {

        return selectById(new Set<Id>{shipmentOrderId})[0];
    }

    public List<Shipment_Order__c> selectById(Set<Id> idSet) {

        return (List<Shipment_Order__c>) selectSObjectsById(idSet);
    }

}