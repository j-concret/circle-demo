@isTest
public class monthEndProcess_Test {
    
    @isTest
    public static void monthEndProcessBatchTest(){
        
        List<shipment_order__c> shipmentOrderList = new List<shipment_order__c>();
        List<GL_Account__c> glAccountList = new List<GL_Account__c>();
        
        Accounting_Period__c accountingPeriod = new Accounting_Period__c(Name = '2020-10', 
                                                                         Start_Date__c = Date.newInstance(2020, 09, 1), 
                                                                         End_Date__c = Date.newInstance(2020, 10, 25), 
                                                                         Status__c = 'Open');
        Insert accountingPeriod;
        
        Dimension__c dimension = new Dimension__c(Name = 'IOR', Dimension_Type_Code__c = 'BRA', 
                                                  Dimension_Description__c = 'Testing', 
                                                  Short_Dimension_Description__c = 'IOR');
        Insert dimension;
        
        Account acc = new Account(Name = 'Acme Test', Region__c = 'USA', Dimension__c = dimension.Id, 
                                  Invoicing_Term_Parameters__c = 'No Terms', IOR_Payment_Terms__c = 32, 
                                  Invoice_Timing__c = 'Upfront invoicing');
        
        Insert acc;
        
        IOR_Price_List__c priceList = new IOR_Price_List__c(Destination__c = 'Finland', 
                                                            Client_Name__c = acc.Id, 
                                                            IOR_Fee__c = 10);
        Insert priceList;
        
        CPARules__c cpaRules = new CPARules__c(Country__c = 'Finland');
        Insert cpaRules;
        
        shipment_order__c shipment1 = new shipment_order__c(IOR_Price_List__c = priceList.Id, Populate_Invoice__c = false,
                                                            Account__c = acc.Id, POD_Date__c = Date.newInstance(2020,09,11), 
                                                            Closed_Down_SO__c = false, Miscellaneous_Fee__c = 2220.0, 
                                                            CI_Miscellaneous_Fee__c = 20.0, Bank_Fees__c = 9.00, 
                                                            Handling_and_Admin_Fee__c = 499.00, CI_Admin_Fee__c = 31.00, 
                                                            CI_Bank_Fees__c = 11.00, CI_EOR_and_Export_Compliance_Fee_USD__c = 10.00, 
                                                            CI_IOR_and_Import_Compliance_Fee_USD__c = 11.00, 
                                                            CI_Total_Licence_Cost__c = 1.00, Service_Type__c = 'EOR', 
                                                            Finance_Fee__c = 23.00, No_further_client_invoices_required__c = FALSE, 
                                                            FC_Miscellaneous_Fee__c = 100.00, FC_Bank_Fees__c = 100.00, Destination__c = 'Finland');
        shipmentOrderList.add(shipment1);
        
        shipment_order__c shipment2 = new shipment_order__c(IOR_Price_List__c = priceList.Id, Account__c = acc.Id, Populate_Invoice__c = false, 
                                                            POD_Date__c = Date.newInstance(2020, 09, 11), 
                                                            Closed_Down_SO__c = false, Miscellaneous_Fee__c = 2220.0, 
                                                            CI_Miscellaneous_Fee__c = 20.0, Bank_Fees__c = 9.00, 
                                                            Handling_and_Admin_Fee__c = 499.00, CI_Admin_Fee__c = 31.00, 
                                                            CI_Bank_Fees__c = 11.00, CI_EOR_and_Export_Compliance_Fee_USD__c = 10.00, 
                                                            CI_IOR_and_Import_Compliance_Fee_USD__c = 11.00, Service_Type__c = 'IOR', 
                                                            Finance_Fee__c = 23.00, No_further_client_invoices_required__c = FALSE, 
                                                            Actual_Total_Clearance_Costs__c = 50.00, CI_Recharge_Tax_and_Duty__c = 1.00, 
                                                            Final_Supplier_Invoice_Received__c = FALSE, FC_Insurance_Fee_USD__c = 1000.00, 
                                                            Actual_Total__c = 10.00, Destination__c = 'Finland');        
        shipmentOrderList.add(shipment2);
        
        Insert shipmentOrderList;
                        
        GL_Account__c glAccount1 = new GL_Account__c(Name = '613500', GL_Account_Description__c = 'Testing the Batch.', 
                                                    GL_Account_Short_Description__c = 'Revenue', 
                                                    Currency__c = 'US Dollar (USD)');
        glAccountList.add(glAccount1);
        
        GL_Account__c glAccount2 = new GL_Account__c(Name = '110000', GL_Account_Description__c = 'Testing the Batch.', 
                                                    GL_Account_Short_Description__c = 'Revenue', 
                                                    Currency__c = 'US Dollar (USD)');
        glAccountList.add(glAccount2);
        
        GL_Account__c glAccount3 = new GL_Account__c(Name = '110151', GL_Account_Description__c = 'Testing the Batch.', 
                                                    GL_Account_Short_Description__c = 'Revenue', 
                                                    Currency__c = 'US Dollar (USD)');
        glAccountList.add(glAccount3);
        
        GL_Account__c glAccount4 = new GL_Account__c(Name = '921000', GL_Account_Description__c = 'Testing the Batch.', 
                                                    GL_Account_Short_Description__c = 'Revenue', 
                                                    Currency__c = 'US Dollar (USD)');
        glAccountList.add(glAccount4);
        
        GL_Account__c glAccount5 = new GL_Account__c(Name = '120000', GL_Account_Description__c = 'Testing the Batch.', 
                                                    GL_Account_Short_Description__c = 'Revenue', 
                                                    Currency__c = 'US Dollar (USD)');
        glAccountList.add(glAccount5);
        
        Insert glAccountList;
        
        Test.startTest();
        	monthEndProcess obj = new monthEndProcess();
        	Database.executeBatch(obj);
        Test.stopTest();
        
        List<Transaction_Line_New__c> transactionLineList = [Select Id, Description__c from Transaction_Line_New__c];
        
        System.assert(transactionLineList.size() > 0, 'Checking the size of the Transaction Line List.');
        System.assert(transactionLineList != null);
    }

}