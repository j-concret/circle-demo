@isTest(SeeAllData=false)
public class TaskDetailPageCtrl_Test {

    @Testsetup
    static void setupData(){

        List<Account> accounts = new List<Account>();
        Account account = new Account (name='TecEx Prospective Client', Type ='Client',
                                       CSE_IOR__c = '0050Y000001LTZO',
                                       Service_Manager__c = '0050Y000001LTZO',
                                       Financial_Manager__c='0050Y000001LTZO',
                                       Financial_Controller__c='0050Y000001LTZO',
                                       Tax_recovery_client__c  = FALSE,
                                       Ship_From_Line_1__c = 'Ship_From_Line_1',
                                       Ship_From_Line_2__c = 'Ship_From_Line_2',
                                       Ship_From_Line_3__c = 'Ship_From_Line_3',
                                       Ship_From_City__c = 'Ship_From_City',
                                       Ship_From_Zip__c = 'Ship_From_Zip',
                                       Ship_From_Country__c = 'Ship_From_Country',
                                       Ship_From_Other_notes__c = 'Ship_From_Other',
                                       Ship_From_Contact_Name__c = 'Ship_From_Contact',
                                       Ship_From_Contact_Email__c = 'Ship_From_Email',
                                       Ship_From_Contact_Tel__c = 'Ship_From_Tel',
                                       Client_Address_Line_1__c = 'Client_Address_Line_1',
                                       Client_Address_Line_2__c = 'Client_Address_Line_2',
                                       Client_Address_Line_3__c = 'Client_Address_Line_3',
                                       Client_City__c = 'Client_City',
                                       Client_Zip__c = 'Client_Zip',
                                       Client_Country__c = 'Client_Country',
                                       Other_notes__c = 'Other_notes',
                                       Tax_Name__c = 'Tax_Name',
                                       Tax_ID__c = 'Tax_ID',
                                       Contact_Name__c = 'Contact_Name',
                                       Contact_Email__c = 'Contact_Email',
                                       Contact_Tel__c = 'Contact_Tel'
                                       );
        accounts.add(account);

        accounts.add(new Account (name='Test Supplier', Type ='Supplier', ICE__c = '0050Y000001km5c', RecordTypeId = '0120Y0000006KjQQAU'));

        accounts.add(new Account (name='Test Manufacturer', Type ='Manufacturer', RecordTypeId = '0120Y0000002wa0QAA', Manufacturer_Alias__c = 'Tester,Who'));
        insert accounts;

        Contact contact = new Contact (Client_Notifications_Choice__c = 'opt-in', lastname ='Testing Individual',  AccountId = accounts[0].Id, email = 'test@test.com');
        insert contact;


        IOR_Price_List__c iorpl = new IOR_Price_List__c ( Client_Name__c = accounts[0].Id, Name = 'Brazil', IOR_Fee__c = 0.15, On_Charge_Mark_up__c = 0.15,
                                                          TecEx_Shipping_Fee_Markup__c =0.40, IOR_Min_Fee__c = 100,  Admin_Fee__c =100, Set_up_fee__c =100, Bank_Fees__c =200, Tax_Rate__c = 0.20,
                                                          Estimated_Customs_Brokerage__c =250, Estimated_Customs_Clearance__c =250, Estimated_Customs_Handling__c =250, Estimated_Import_License__c =250, Destination__c = 'Brazil' );
        insert iorpl;

        country_price_approval__c cpa = new country_price_approval__c( name ='Brazil', Billing_term__c = 'DAP/CIF - IOR pays',  Airwaybill_Instructions__c = 'User IOR Address', Supplier__c = accounts[1].Id,In_Country_Specialist__c = '0050Y000001km5c', Clearance_Destination__c = 'Brazil', Preferred_Supplier__c = TRUE, Destination__c = 'Brazil' );
        insert cpa;

        Currency_Management2__c CM = new Currency_Management2__c (Name = 'US Dollar (USD)', Currency__c = 'US Dollar (USD)', ISO_Code__c = 'USD', Conversion_Rate__c = 1);
        insert CM;

        Country__c country = new Country__c(Brokerage_Costs__c = 618, CIF_Adjustment_Factor__c = 0.035, CIF_Absolute_value_adjustment__c = 200, Clearance_Costs__c = 488,
                                            Country__c = 'Brazil', Name = 'Brazil', Handling_Costs__c = 1180, License_Permit_Costs__c = 0);
        insert country;

        Tax_Structure_Per_Country__c tax = new Tax_Structure_Per_Country__c(Applied_to_Value__c = 'CIF', Country__c = country.Id, Default_Duty_Rate__c = 0.5625,
                                                                            Order_Number__c = '1', Part_Specific__c = TRUE, Name = 'II', Tax_Type__c = 'Duties');
        insert tax;

        CPA_v2_0__c RelatedCPA = new CPA_v2_0__c(Name = 'Brazil', Country_Applied_Costings__c=true );
        insert RelatedCPA;
        CPA_v2_0__c cpav2 = new CPA_v2_0__c(Name = 'Brazil Testing 2.0', Supplier__c = accounts[1].Id, Service_Type__c = 'IOR/EOR', Related_Costing_CPA__c = RelatedCPA.Id, Preferred_Supplier__c = TRUE,
                                            VAT_Rate__c = 0.1, Destination__c = 'Brazil',

                                            Default_Section_1_Address_Line_1__c= 'Address_Line_1',
                                            Default_Section_1_Address_Line_2__c= 'Address_Line_2',
                                            Default_Section_1_Address_Line_3__c= 'Address_Line_3',
                                            Default_Section_1_City__c= 'City',
                                            Default_Section_1_Zip__c= 'Zip',
                                            Default_Section_1_Country__c = 'Country',
                                            Default_Section_1_Other__c= 'Other',
                                            Default_Section_1_Tax_Name__c= 'Tax_Name',
                                            Default_Section_1_Tax_Id__c= 'Tax_Id',
                                            Default_Section_1_Contact_Name__c= 'Contact_Name',
                                            Default_Section_1_Contact_Email__c= 'Contact_Email',
                                            Default_Section_1_Contact_Tel__c= 'Contact_Tel',
                                            Alternate_Section_1_Address_Line_1__c= 'Address_Line_1',
                                            Alternate_Section_1_Address_Line_2__c= 'Address_Line_2',
                                            Alternate_Section_1_Address_Line_3__c= 'Address_Line_3',
                                            Alternate_Section_1_City__c= 'City',
                                            Alternate_Section_1_Zip__c= 'Zip',
                                            Alternate_Section_1_Country__c = 'Country',
                                            Alternate_Section_1_Other__c = 'Other',
                                            Alternate_Section_1_Tax_Name__c = 'Tax_Name',
                                            Alternate_Section_1_Tax_Id__c = 'Tax_Id',
                                            Alternate_Section_1_Contact_Name__c= 'Contact_Name',
                                            Alternate_Section_1_Contact_Email__c= 'Contact_Email',
                                            Alternate_Section_1_Contact_Tel__c= 'Contact_Tel',
                                            Default_Section_2_Address_Line_1__c= 'Address_Line_1',
                                            Default_Section_2_Address_Line_2__c= 'Address_Line_2',
                                            Default_Section_2_Address_Line_3__c= 'Address_Line_3',
                                            Default_Section_2_City__c= 'City',
                                            Default_Section_2_Zip__c= 'Zip',
                                            Default_Section_2_Country__c = 'Country',
                                            Default_Section_2_Other__c= 'Other',
                                            Default_Section_2_Tax_Name__c= 'Tax_Name',
                                            Default_Section_2_Tax_Id__c= 'Tax_Id',
                                            Default_Section_2_Contact_Name__c= 'Contact_Name',
                                            Default_Section_2_Contact_Email__c= 'Contact_Email',
                                            Default_Section_2_Contact_Tel__c= 'Contact_Tel',
                                            Alternate_Section_2_Address_Line_1__c= 'Address_Line_1',
                                            Alternate_Section_2_Address_Line_2__c= 'Address_Line_2',
                                            Alternate_Section_2_Address_Line_3__c= 'Address_Line_3',
                                            Alternate_Section_2_City__c= 'City',
                                            Alternate_Section_2_Zip__c= 'Zip',
                                            Alternate_Section_2_Country__c = 'Country',
                                            Alternate_Section_2_Other__c = 'Other',
                                            Alternate_Section_2_Tax_Name__c = 'Tax_Name',
                                            Alternate_Section_2_Tax_Id__c = 'Tax_Id',
                                            Alternate_Section_2_Contact_Name__c= 'Contact_Name',
                                            Alternate_Section_2_Contact_Email__c= 'Contact_Email',
                                            Alternate_Section_2_Contact_Tel__c= 'Contact_Tel',
                                            Default_Section_3_Address_Line_1__c= 'Address_Line_1',
                                            Default_Section_3_Address_Line_2__c= 'Address_Line_2',
                                            Default_Section_3_Address_Line_3__c= 'Address_Line_3',
                                            Default_Section_3_City__c= 'City',
                                            Default_Section_3_Zip__c= 'Zip',
                                            Default_Section_3_Country__c = 'Country',
                                            Default_Section_3_Other__c= 'Other',
                                            Default_Section_3_Tax_Name__c= 'Tax_Name',
                                            Default_Section_3_Tax_Id__c= 'Tax_Id',
                                            Default_Section_3_Contact_Name__c= 'Contact_Name',
                                            Default_Section_3_Contact_Email__c= 'Contact_Email',
                                            Default_Section_3_Contact_Tel__c= 'Contact_Tel',
                                            Alternate_Section_3_Address_Line_1__c= 'Address_Line_1',
                                            Alternate_Section_3_Address_Line_2__c= 'Address_Line_2',
                                            Alternate_Section_3_Address_Line_3__c= 'Address_Line_3',
                                            Alternate_Section_3_City__c= 'City',
                                            Alternate_Section_3_Zip__c= 'Zip',
                                            Alternate_Section_3_Country__c = 'Country',
                                            Alternate_Section_3_Other__c = 'Other',
                                            Alternate_Section_3_Tax_Name__c = 'Tax_Name',
                                            Alternate_Section_3_Tax_Id__c = 'Tax_Id',
                                            Alternate_Section_3_Contact_Name__c= 'Contact_Name',
                                            Alternate_Section_3_Contact_Email__c= 'Contact_Email',
                                            Alternate_Section_3_Contact_Tel__c= 'Contact_Tel',
                                            Default_Section_4_Address_Line_1__c= 'Address_Line_1',
                                            Default_Section_4_Address_Line_2__c= 'Address_Line_2',
                                            Default_Section_4_Address_Line_3__c= 'Address_Line_3',
                                            Default_Section_4_City__c= 'City',
                                            Default_Section_4_Zip__c= 'Zip',
                                            Default_Section_4_Country__c = 'Country',
                                            Default_Section_4_Other__c= 'Other',
                                            Default_Section_4_Tax_Name__c= 'Tax_Name',
                                            Default_Section_4_Tax_Id__c= 'Tax_Id',
                                            Default_Section_4_Contact_Name__c= 'Contact_Name',
                                            Default_Section_4_Contact_Email__c= 'Contact_Email',
                                            Default_Section_4_Contact_Tel__c= 'Contact_Tel',
                                            Alternate_Section_4_Address_Line_1__c= 'Address_Line_1',
                                            Alternate_Section_4_Address_Line_2__c= 'Address_Line_2',
                                            Alternate_Section_4_Address_Line_3__c= 'Address_Line_3',
                                            Alternate_Section_4_City__c= 'City',
                                            Alternate_Section_4_Zip__c= 'Zip',
                                            Alternate_Section_4_Country__c = 'Country',
                                            Alternate_Section_4_Other__c = 'Other',
                                            Alternate_Section_4_Tax_Name__c = 'Tax_Name',
                                            Alternate_Section_4_Tax_Id__c = 'Tax_Id',
                                            Alternate_Section_4_Contact_Name__c= 'Contact_Name',
                                            Alternate_Section_4_Contact_Email__c= 'Contact_Email',
                                            Alternate_Section_4_Contact_Tel__c= 'Contact_Tel',
                                            VAT_Reclaim_Destination__c = FALSE,
                                            Final_Destination__c ='Brazil',
                                            Country__c = country.id);
        insert cpav2;

        List<CPARules__c> CPARules = new List<CPARules__c>();
        CPARules.add(new CPARules__c(Country__c = 'Brazil',Criteria__c ='Default', Chargable_weight__c = 'NONE', Courier_Responsibility__c = 'NONE',   ECCN_No__c = 'NONE', NL_Product_User__c = 'NONE', Number_of_final_deliveries__c = 'NONE', ShipmentValue__c ='NONE', Sum_of_quantity_of_matched_products__c = 'NONE', Manufacturer__c = 'NONE', DefaultCPA2__c = cpav2.Id, CPA2__c =  cpav2.Id));
        insert CPARules;

        List<CPA_Costing__c> costs = new List<CPA_Costing__c>();
        costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'Shipment value', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'IOR', Min__c = 375, Max__c = 14000, Conditional_value__c = 'Shipment value', Condition__c = '>=', Floor__c = 0, Ceiling__c = 10000, Currency__c = 'US Dollar (USD)'));
        costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'Anil', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'IOR', Currency__c = 'US Dollar (USD)',Variable_threshold__c = null));
        insert costs;
        


        List<Product2> products = new List<Product2>();
        products.add(new Product2(Name = 'CAB-ETH-S-RJ45', US_HTS_Code__c='85444210',description='Anil', ProductCode = 'CAB-ETH-S-RJ45', Manufacturer__c = accounts[2].Id));
        products.add(new Product2(Name = 'CAB-ETH-S-RJ46',US_HTS_Code__c='85444210', ProductCode = 'CAB-ETH-S-RJ46', Manufacturer__c = accounts[2].Id)); 
        products.add(new Product2(Name = 'CAB-ETH-S', US_HTS_Code__c='85444210',ProductCode = 'CAB-ETH-S', Manufacturer__c = accounts[2].Id));

        Master_Task__c masterTask = new Master_Task__c(Name = 'Demo Task', States__c = 'TecEx Pending;Resolved',
                                                       Operational__c = true, Condition__c = '{0} > 5000',
                                                       Condition_Fields_API_Name__c = 'Shipment_Order__c.Shipment_Value_USD__c,Country__c,Part__c,Freight__c',
                                                       Assess_Condition_on_Creation_of__c = 'Shipment_Order__c',Ship_To_Country__c = 'Brazil');
        Insert masterTask;

        Master_Task_Template__c mstTaskTemp = new Master_Task_Template__c(Displayed_to__c = 'TecEx',
                                                                          Master_Task__c = masterTask.Id,
                                                                          State__c = 'TecEx Pending',Task_Title__c = 'Demo',
                                                                          Task_Description__c = 'Testing {0} {1}',
                                                                          Task_Description_Fields__c='Shipment_Order__c.Name,Task.Subject',
                                                                          Complete_Button_Text__c = 'Submit',Complete_Button_Action__c = 'Yes',
                                                                          Complete_Button_Target_Object_API_Name__c = 'Task',
                                                                          Complete_Button_Target_Field_API_Name__c = 'State__c',
                                                                          Complete_Button_Target_Field_Set_Value__c = 'Resolved',
                                                                          Description__c = 'Testing {0} {1}',
                                                                          Description_Fields__c = 'Shipment_Order__c.Name,Task.Subject',
                                                                          Building_Block_Type__c = 'Instruction');
        insert mstTaskTemp;


        Shipment_Order__c shipmentOrder = new Shipment_Order__c(Account__c = accounts[0].id, Shipment_Value_USD__c = 6000, CPA_v2_0__c = cpav2.Id,IOR_CSE__c ='0050Y000001LTZO',
                                                                Client_Contact_for_this_Shipment__c = contact.Id, Who_arranges_International_courier__c ='Client', Tax_Treatment__c ='DAP/CIF - IOR pays',
                                                                Ship_to_Country__c = cpa.Id, Destination__c = 'Brazil', Service_Type__c = 'IOR', IOR_Price_List__c = iorpl.Id, Chargeable_Weight__c = 200,
                                                                Shipping_Status__c = 'Cost Estimate Abandoned' );
        insert shipmentOrder;


        List<Part__c> partList = new List<Part__c>();

        partList.add(new Part__c(Name ='CAB-ETH-S-RJ45',  Quantity__c= 1, Product__c=products[0].Id, Commercial_Value__c = 15000, US_HTS_Code__c = '85444210',
                                 Country_of_Origin2__c = 'China', Shipment_Order__c = shipmentOrder.Id, Description_and_Functionality__c ='Anil'));

        partList.add(new Part__c(Name ='AsRdy',  Quantity__c= 1,  Product__c=products[0].Id, Commercial_Value__c = 15000, US_HTS_Code__c = '85444210',
                                 Country_of_Origin2__c = 'China', Shipment_Order__c = shipmentOrder.Id, Description_and_Functionality__c ='Anil AsRdy'));

        partList.add(new Part__c(Name ='CAB-ETH-S-RJ46',  Quantity__c= 1, Commercial_Value__c = 15000, US_HTS_Code__c = '85478123',
                                 Country_of_Origin2__c = 'China', Shipment_Order__c = shipmentOrder.Id, Description_and_Functionality__c ='AB-ETH-S-RJ46'));

        partList.add(new Part__c(Name ='CAB-ETH-S-RJ46',  Quantity__c= 1, Commercial_Value__c = 15000, US_HTS_Code__c = '834512879',
                                 Country_of_Origin2__c = 'China', Shipment_Order__c = shipmentOrder.Id, Description_and_Functionality__c ='AB-ETH-S-RJ46'));

        partList.add(new Part__c(Name ='Testing',  Quantity__c= 1, Commercial_Value__c = 15000, US_HTS_Code__c = '834512879',
                                 Country_of_Origin2__c = 'China', Shipment_Order__c = shipmentOrder.Id, Description_and_Functionality__c ='Testing'));

        partList.add(new Part__c(Name ='Who CAB-ETH-S',  Quantity__c= 1, Commercial_Value__c = 15000, US_HTS_Code__c = '8471254784',
                                 Country_of_Origin2__c = 'China', Shipment_Order__c = shipmentOrder.Id, Description_and_Functionality__c ='Testing'));
        insert partList;
    }

    static testmethod void getTaskRecordTestWithInstructionBlockType(){

        Shipment_Order__c so = [SELECT Id, Shipping_Status__c FROM SHipment_order__c LIMIT 1];
        so.Shipping_Status__c = 'Cost Estimate';
        update so;

        List<Task> task = [SELECT Id FROM Task];

        Test.startTest();
        TaskDetailPageCtrl.getTaskRecord(task[0].Id,false);
        Test.stopTest();
    }

    static testmethod void getTaskRecordTestWithInputBlockType(){

        Master_task__c masterTask =  [SELECT id FROM Master_Task__c LIMIT 1];

        Shipment_Order__c shipmentOrder = [SELECT Id, Shipping_Status__c FROM SHipment_order__c LIMIT 1];
        shipmentOrder.Shipping_Status__c = 'Cost Estimate';

        update shipmentOrder;


        Master_Task_Template__c mstTaskTemp = new Master_Task_Template__c(Displayed_to__c = 'TecEx',
                                                                          Master_Task__c = masterTask.Id,
                                                                          State__c = 'TecEx Pending',Task_Title__c = 'Demo',
                                                                          Task_Description__c = 'Testing {0} {1}',
                                                                          Task_Description_Fields__c='Shipment_Order__c.Name,Task.Subject',
                                                                          Complete_Button_Text__c = 'Submit',Complete_Button_Action__c = 'Yes',
                                                                          Complete_Button_Target_Object_API_Name__c = 'Task',
                                                                          Complete_Button_Target_Field_API_Name__c = 'State__c',
                                                                          Complete_Button_Target_Field_Set_Value__c = 'Resolved',
                                                                          Description__c = 'Testing {0} {1}',
                                                                          Description_Fields__c = 'Shipment_Order__c.Name,Task.Subject',
                                                                          Building_Block_Type__c = 'Input',
                                                                          Target_Field_API_Name__c = 'Data_Centre_Ticket_Number__c',Target_Object_API_Name__c = 'Shipment_Order__c');
        insert mstTaskTemp;

        Master_Task_Template__c mstTaskTemp2 = new Master_Task_Template__c(Displayed_to__c = 'TecEx',
                                                                           Master_Task__c = masterTask.Id,
                                                                           State__c = 'TecEx Pending',Task_Title__c = 'Demo',
                                                                           Task_Description__c = 'Testing {0} {1}',
                                                                           Task_Description_Fields__c='Shipment_Order__c.Name,Task.Subject',
                                                                           Complete_Button_Text__c = 'Submit',Complete_Button_Action__c = 'Yes',
                                                                           Complete_Button_Target_Object_API_Name__c = 'Task',
                                                                           Complete_Button_Target_Field_API_Name__c = 'State__c',
                                                                           Complete_Button_Target_Field_Set_Value__c = 'Resolved',
                                                                           Description__c = 'Testing {0} {1}',
                                                                           Description_Fields__c = 'Shipment_Order__c.Name,Task.Subject',
                                                                           Building_Block_Type__c = 'Input',
                                                                           Target_Field_API_Name__c = 'AWB_Type__c',Target_Object_API_Name__c = 'Shipment_Order__c');
        insert mstTaskTemp2;

        Master_Task_Template__c mstTaskTemp3 = new Master_Task_Template__c(Displayed_to__c = 'TecEx',
                                                                           Master_Task__c = masterTask.Id,
                                                                           State__c = 'TecEx Pending',Task_Title__c = 'Demo',
                                                                           Task_Description__c = 'Testing {0} {1} {2}',
                                                                           Task_Description_Fields__c='Shipment_Order__c.Name,Task.Subject, Shipment_Order__c.Quote_expiry_notification_Date__c',
                                                                           Complete_Button_Text__c = 'Submit',Complete_Button_Action__c = 'Yes',
                                                                           Complete_Button_Target_Object_API_Name__c = 'Task',
                                                                           Complete_Button_Target_Field_API_Name__c = 'State__c',
                                                                           Complete_Button_Target_Field_Set_Value__c = 'Resolved',
                                                                           Description__c = 'Testing {0} {1} {2}',
                                                                           Description_Fields__c = 'Shipment_Order__c.Name,Task.Subject,Shipment_Order__c.Quote_expiry_notification_Date__c',
                                                                           Building_Block_Type__c = 'Input',
                                                                           Target_Field_API_Name__c = 'AM_Flags__c',Target_Object_API_Name__c = 'Shipment_Order__c');
        insert mstTaskTemp3;

        List<Task> taskList = [Select ID From Task];

        Test.startTest();
        Map<String,Object> taskRec = TaskDetailPageCtrl.getTaskRecord(taskList[0].id,false);
        System.assert (taskRec.get('task')!= null);
        TaskDetailPageCtrl.TaskWrapper wrapper = (TaskDetailPageCtrl.TaskWrapper)taskRec.get('template');
        for(TaskDetailPageCtrl.Block block : wrapper.blocks) {
            if(block.sfFieldType == 'picklist') {
                block.value = 'House';
            }
            else if(block.sfFieldType == 'boolean') {
                block.value = 'true';
            }else if(block.sfFieldType == 'datetime') {
                block.value = '05/26/2021T07:59:00.000Z';
            }
            else {
                block.value = '23432432';
            }
        }
        TaskDetailPageCtrl.updateSfRecord(taskList[0].Id, JSON.serialize(wrapper));
        Test.stopTest();
    }

    static testmethod void getTaskRecordTestWithComponentBlockType(){


        Master_task__c masterTask =  [Select id FROM Master_Task__c LIMIT 1];


        Shipment_Order__c shipmentOrder = [Select Id, Shipping_Status__c FROM SHipment_order__c LIMIT 1];
        shipmentOrder.Shipping_Status__c = 'Cost Estimate';

        update shipmentOrder;


        //Create Document
        ContentVersion cv = new ContentVersion();
        cv.Title = 'Test Document';
        cv.PathOnClient = 'TestDocument.pdf';
        cv.VersionData = Blob.valueOf('Test Content');
        cv.IsMajorVersion = true;
        cv.FirstPublishLocationId = masterTask.Id;

        Insert cv;


        Master_Task_Template__c mstTaskTemp = new Master_Task_Template__c(Displayed_to__c = 'TecEx',
                                                                          Master_Task__c = masterTask.Id,
                                                                          State__c = 'TecEx Pending',Task_Title__c = 'Demo',
                                                                          Task_Description__c = 'Testing {0} {1} {2}',
                                                                          Task_Description_Fields__c='Shipment_Order__c.Name,Task.Subject , Shipment_Order__c.Quote_expiry_notification_Date__c',
                                                                          Complete_Button_Text__c = 'Submit',Complete_Button_Action__c = 'Yes',
                                                                          Complete_Button_Target_Object_API_Name__c = 'Task',
                                                                          Complete_Button_Target_Field_API_Name__c = 'State__c',
                                                                          Complete_Button_Target_Field_Set_Value__c = 'Resolved',
                                                                          Description__c = 'Testing {0} {1} {2}',
                                                                          Description_Fields__c = 'Shipment_Order__c.Name,Task.Subject, Shipment_Order__c.Quote_expiry_notification_Date__c ',
                                                                          Building_Block_Type__c = 'Component',Referring_Component__c = 'Download Document',Document_Name__c = 'Test Document',
                                                                          Target_Field_API_Name__c = 'Id',Target_Object_API_Name__c = 'Master_Task__c');
        insert mstTaskTemp;



        Test.startTest();

        List<Task> taskList = [Select ID From Task];
        TaskDetailPageCtrl.getTaskRecord(taskList[0].id,false);
        Test.stopTest();
    }

    static testMethod void getPrerequisiteTasksTest(){

        Master_Task__c masterTask = [Select id FROM Master_Task__c LIMIT 1 ];

        Master_Task_Prerequisite__c masterTaskPrereq = new Master_Task_Prerequisite__c(Master_Task_Dependent__c = masterTask.Id);
        insert masterTaskPrereq;

        Shipment_Order__c shipmentOrder = [Select Id, Shipping_Status__c FROM SHipment_order__c LIMIT 1];
        shipmentOrder.Shipping_Status__c = 'Cost Estimate';

        update shipmentOrder;

        Test.startTest();

        Task tsk = [Select id from Task];
        TaskDetailPageCtrl.getPrerequisiteTasks(tsk.Id);
        Test.stopTest();
    }

    static testMethod void updateTaskTest(){
        Shipment_Order__c shipmentOrder = [Select Id, Shipping_Status__c FROM SHipment_order__c LIMIT 1];
        shipmentOrder.Shipping_Status__c = 'Cost Estimate';

        update shipmentOrder;

        Test.startTest();
        Task tsk = [Select Id FROM Task LIMIT 1];
        tsk.state__c = 'Resolved';

        Task rTask = TaskDetailPageCtrl.updateTask(tsk);
        System.assert (rTask.Id != null);
        Test.stopTest();
    }
    
    static testMethod void updateTaskTestNegativeCase(){
        Test.startTest();
        Task tsk = new Task(subject='test');
        try{
        	Task rTask = TaskDetailPageCtrl.updateTask(tsk);
        }catch(Exception e){}
        try{
            Task rTask2 = TaskDetailPageCtrl.updateTask(null);
        }catch(Exception e){}
        Test.stopTest();
    }
}