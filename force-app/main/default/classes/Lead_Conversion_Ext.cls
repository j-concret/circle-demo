/*****************************************************************************************************\
    @ Func Area     : Leads
    @ Date          : 12/2015
    @ Test Class    : Lead_Conversion_Ext_Test  
    @ Additional    : Extension that is used to control the custom lead conversion process
     -----------------------------------------------------------------------------
    @ Last Modified By  : 
    @ Last Modified On  :   
    @ Last Modified Reason  : 
  -----------------------------------------------------------------------------   
******************************************************************************************************/
public with sharing class Lead_Conversion_Ext {
    //Class Variables  
    private final Lead mysObject;
    private final id leadId;
    public boolean showAttachmentRL{get;set;}
    

    //Extension Constructor
    /*public Lead_Conversion_Ext(ApexPages.StandardController stdController) {
        this.mysObject = (Lead)stdController.getRecord();
        this.leadId = this.mysObject.Id;
        this.showAttachmentRL = false;
        checkAttachments();
    }
    
    //Method called by page actions to trigger validation messages and checks for attachments
    public PageReference leadConversionCheck() {
        Id UserId = UserInfo.getUserId();
        Set <String> recordTypeIds = new Set <String>();
        String PageRefString = '';
        recordTypeIds.add('012D0000000VDVIIA4');
        recordTypeIds.add('012D0000000VDVJIA4');
        recordTypeIds.add('012D0000000VDVNIA4');
        recordTypeIds.add('012D0000000VDVKIA4');

        
        List <User> currentUser = [Select Id, Override_Validation__c from User where Id=: UserId];

        mysObject.Ready_To_Convert__c = true;
        if(currentUser[0].Override_Validation__c == true && recordTypeIds.contains(mysObject.recordTypeId)){
            mysObject.Client_Status__c = 'Prospect';
            PageRefString = '/lead/leadconvert.jsp?retURL=' + leadId + '&id=' + leadId + '&nooverride=1';
        }else{
            mysObject.Client_Status__c = 'Active';
            PageRefString = '/lead/leadconvert.jsp?retURL=' + leadId + '&id=' + leadId + '&nooppti=1&nooverride=1';
        }

        try{
            update mysObject;
        }catch(exception e){
            
            ApexPages.addMessages(e);
            return null;
            
        }
        
        if (showAttachmentRL == true){
            return null;
        }
        
        PageReference leadConversionPage = new PageReference(PageRefString);
        leadConversionPage.setRedirect(true);
        return leadConversionPage;
    }
    
    //Methoid used to check for attachments. Will need enhancement to check the actual file name in future.
    public void checkAttachments(){
        List<Attachment> currentAttchments = [Select Id from Attachment where ParentId = :mysObject.Id];
         Id UserId = UserInfo.getUserId();
        
        List <User> currentUser = [Select Id, Override_Validation__c from User where Id=: UserId];
        
        if(currentAttchments.size() < 1 && currentUser[0].Override_Validation__c == false){
            showAttachmentRL = true;
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Info,'Please ensure the contract is attached before converting this lead');
            ApexPages.addMessage(myMsg);
        }
    }*/
}