public class GetFreightOptions {


    // 1.returns FreightRecordID using this method in Getrelatedfreightcomponent with Shipment edit flow
    @AuraEnabled
     public static ID GetFreightOptionsRecordID(ID SOId){
           
       Freight__c Freight = [select id,Li_Ion_Batteries__c,Li_Ion_Battery_Type__c,Pick_up_Address__c,Pick_up_Contact_Details__c,Pick_up_Contact_No__c from Freight__c where Shipment_Order__c =: SOId];       
        id recId = Freight.Id;
        return recId;
      }
    
    //2. Returns related list of shipping packages records
    @AuraEnabled
   
         public static List<Shipment_Order_Package__c> getSOPackages(ID SOId1){
        return [SELECT
               ID, packages_of_same_weight_dims__c, Actual_Weight__c, Breadth__c, Dimension_Unit__c,Freight__c,Height__c,Length__c,Weight_Unit__c
               FROM Shipment_Order_Package__c where Shipment_Order__c =: SOId1 LIMIT 200];
    }
    
    //2.1 updating related list of shipping packages records
    @AuraEnabled
    public static void updateSOPackages(List<Shipment_Order_Package__c> editedAccountList){
        try{
            update editedAccountList;
            system.debug('editedAccountList'+editedAccountList);
            
        } catch(Exception e){
           
        }
    }
    
    //3. Returns related list of Parts
    @AuraEnabled
   
         public static List<Part__c> getSOParts(ID SOId1){
        return [SELECT
               ID, Name, Description_and_Functionality__c, Commercial_Value__c, Quantity__c,Country_of_Origin2__c,US_HTS_Code__c,ECCN_NO__c
               FROM Part__c where Shipment_Order__c =: SOId1 LIMIT 200];
    }
    
    //3.1 updating related list of shipping packages records
    @AuraEnabled
    public static void updateSOParts(List<Part__c> editedAccountList){
        try{
            update editedAccountList;
            system.debug('editedAccountList'+editedAccountList);
            
        } catch(Exception e){
           
        }
    }
    //4. Returns related list of FinalDeliveries
    @AuraEnabled
   
         public static List<Final_Delivery__c> getSOFinalDeliveries(ID SOId1){
        return [SELECT
               ID, Contact_email__c, Contact_name__c, Contact_number__c,Delivery_address__c
               FROM Final_Delivery__c where Shipment_Order__c =: SOId1 LIMIT 200];
    }
    
    //4.1 updating related list of FinalDeliveries
    @AuraEnabled
    public static void updateSOFinalDeliveries(List<Final_Delivery__c> editedAccountList){
        try{
            update editedAccountList;
            system.debug('editedAccountList'+editedAccountList);
            
        } catch(Exception e){
           
        }
    }
    
    
   
}