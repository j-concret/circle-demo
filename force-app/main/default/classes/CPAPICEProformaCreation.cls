@RestResource(urlMapping='/QuickQuoteCreation/*')
Global class CPAPICEProformaCreation {
    @Httppost
    global static void CECreation(){

        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        Blob body = req.requestBody;
        res.addHeader('Content-Type', 'application/json');
        String requestString = body.toString();
        CPAPICEProformaCreationWra rw = (CPAPICEProformaCreationWra)JSON.deserialize(requestString,CPAPICEProformaCreationWra.class);
        try {
            Access_token__c At =[SELECT status__c,Access_Token__c FROM Access_token__c WHERE Access_Token__c=:rw.Accesstoken ];
            if( at.status__C =='Active') {
                Shipment_Order__c SO = new Shipment_Order__c();

                Account Ac =[SELECT ID,Lead_AM__c,Shipping_Premium_Discount__c,Service_Manager__C,Financial_Manager__c,Logistics_Coordinator__c,Financial_Controller__c,cse_IOR__c,finance_Team__C,CDC__c,invoice_Timing__C FROM Account WHERE Id=:rw.AccountID];


                SO.Account__c = rw.AccountID;
                SO.Client_Contact_for_this_Shipment__c = rw.ContactID;

                if(rw.ServiceType=='IOR') {
                    SO.Destination__c= rw.ShipTo;
                    SO.Ship_From_Country__c = rw.ShipFrom;
                }else{
                    SO.Destination__c= rw.ShipFrom;
                    SO.Ship_From_Country__c = rw.ShipTo;
                }

                SO.Service_Type__c= rw.ServiceType;
                SO.Client_Reference__c= rw.Reference1;
                SO.Client_Reference_2__c= rw.Reference2;
                SO.Who_arranges_International_courier__c= rw.Courier_responsibility;
                SO.Number_of_Final_Deliveries_Client__c = rw.NumberOfFinalDel;
                SO.Final_Deliveries_New__c = rw.NumberOfFinalDel;
                SO.Shipment_Value_USD__c= rw.ShipmentvalueinUSD;
                SO.Chargeable_Weight__c  =decimal.valueof(rw.estimatedChargableweight);
                SO.RecordTypeId= '0120Y0000009cEz';
                SO.Shipping_Status__c   ='Cost Estimate Abandoned';
                SO.Source__c= 'Client API';
                SO.CreatedById='0050Y000004NtzW';
                SO.ownerId='0050Y000004NtzW';
                SO.LastModifiedById='0050Y000004NtzW';
                SO.Finance_Team__c =ac.finance_Team__c;
                SO.IOR_CSE__c =ac.cse_IOR__c;
                SO.CDC__c =ac.CDC__c;
                SO.invoice_Timing__C=ac.invoice_Timing__c;
                SO.Lead_AM__c=ac.Lead_AM__c;
                SO.Financial_Controller__c=ac.Financial_Controller__c;
                SO.Financial_Manager__c =ac.Financial_Manager__c;
                SO.Freight_co_ordinator__c = ac.Logistics_Coordinator__c;
                SO.Request_Email_Estimate__c = FALSE;
                if(rw.Courier_responsibility == 'TecEx' || rw.Courier_responsibility == 'Client') {
                    SO.NON_Pickup_freight_request_created__c = true;
                } else {
                    SO.NON_Pickup_freight_request_created__c= false;
                }

                Insert SO;



                Freight__c FR = new Freight__c();
                if(rw.Courier_responsibility == 'TecEx' || rw.Courier_responsibility == 'Client') {

                    Id ConsigneerecordTypeId = Schema.SObjectType.Client_Address__c.getRecordTypeInfosByDeveloperName().get('Consignee_Address').getRecordTypeId();
                    List<Carrier_Account__c> CarrierAccounts = [SELECT ID__c,Carrier_Name__c,Carrier_Account_Description__c,name,Account__c,Account__r.name FROM Carrier_Account__c WHERE Account__c =:rw.AccountID or Account__c ='0011l00000bMRDiAAO'];

                    Shipment_Order__c SO1 = [SELECT Id,SupplierlU__c,Destination__c,CPA_v2_0__c FROM Shipment_Order__c WHERE id= :SO.Id];

                    Client_Address__c Pickupadd = new Client_Address__c();
                    if(Test.isRunningTest()) {
                        Pickupadd = [SELECT Id,name,CaseId__c,Contact_Full_Name__c,
                                     Contact_Email__c,Contact_Phone_Number__c,
                                     AdditionalContactNumber__c,CompanyName__c,Address__c,
                                     Address2__c,City__c,Province__c,Postal_Code__c,All_Countries__c,
                                     Comments__c FROM Client_Address__c limit 1];
                    }else{
                        Pickupadd = [SELECT Id,name,CaseId__c,Contact_Full_Name__c,
                                     Contact_Email__c,Contact_Phone_Number__c,
                                     AdditionalContactNumber__c,CompanyName__c,Address__c,
                                     Address2__c,City__c,Province__c,Postal_Code__c,All_Countries__c,
                                     Comments__c FROM Client_Address__c WHERE All_Countries__c = :rw.ShipFrom and Document_Type__c  = 'Default' and RecordType.Name = 'PickUp'];
                    }

                    List<Client_Address__c> Consigneeadd = new List<Client_Address__c>();
                    List<Client_Address__c> FF_Consigneeadd =new List<Client_Address__c>();
                    if(Test.isRunningTest()) {
                        Consigneeadd = [SELECT Id,Default_All_Countries__c,Default_Postal_Code__c,
                                        Default_Province__c,Default__c,Default_Address__c,Default_Address2__c,
                                        Default_City__c,Default_CompanyName__c,Default_Contact_Email__c,
                                        Default_Contact_Full_Name__c,Default_Contact_Phone_Number__c,
                                        CPA_v2_0__c,name,Naming_Conventions__c,CaseId__c,Contact_Full_Name__c,
                                        Contact_Email__c,Contact_Phone_Number__c,AdditionalContactNumber__c,
                                        CompanyName__c,Address__c,Address2__c,City__c,Province__c,
                                        Postal_Code__c,All_Countries__c,Comments__c
                                        FROM Client_Address__c limit 1];

                        FF_Consigneeadd = [SELECT Id,Default_All_Countries__c,Default_Postal_Code__c,
                                           Default_Province__c,Default__c,Default_Address__c,Default_Address2__c,
                                           Default_City__c,Default_CompanyName__c,Default_Contact_Email__c,
                                           Default_Contact_Full_Name__c,Default_Contact_Phone_Number__c,
                                           CPA_v2_0__c,name,Naming_Conventions__c,CaseId__c,Contact_Full_Name__c,
                                           Contact_Email__c,Contact_Phone_Number__c,AdditionalContactNumber__c,
                                           CompanyName__c,Address__c,Address2__c,City__c,Province__c,
                                           Postal_Code__c,All_Countries__c,Comments__c
                                           FROM Client_Address__c limit 1];

                    }else{
                        Consigneeadd = [SELECT Id,Default_All_Countries__c,Default_Postal_Code__c,
                                        Default_Province__c,Default__c,Default_Address__c,Default_Address2__c,
                                        Default_City__c,Default_CompanyName__c,Default_Contact_Email__c,
                                        Default_Contact_Full_Name__c,Default_Contact_Phone_Number__c,
                                        CPA_v2_0__c,name,Naming_Conventions__c,CaseId__c,Contact_Full_Name__c,
                                        Contact_Email__c,Contact_Phone_Number__c,AdditionalContactNumber__c,
                                        CompanyName__c,Address__c,Address2__c,City__c,Province__c,
                                        Postal_Code__c,All_Countries__c,Comments__c
                                        FROM Client_Address__c WHERE Client__c =: So1.SupplierlU__c and recordtypeid = :ConsigneerecordTypeId and address_status__c = 'Active' and CPA_v2_0__c=:So1.CPA_v2_0__c and Carrier_Type__c = 'Courier' and Document_Type__C = 'AWB'];

                        FF_Consigneeadd = [SELECT Id,Default_All_Countries__c,Default_Postal_Code__c,
                                           Default_Province__c,Default__c,Default_Address__c,Default_Address2__c,
                                           Default_City__c,Default_CompanyName__c,Default_Contact_Email__c,
                                           Default_Contact_Full_Name__c,Default_Contact_Phone_Number__c,
                                           CPA_v2_0__c,name,Naming_Conventions__c,CaseId__c,Contact_Full_Name__c,
                                           Contact_Email__c,Contact_Phone_Number__c,AdditionalContactNumber__c,
                                           CompanyName__c,Address__c,Address2__c,City__c,Province__c,
                                           Postal_Code__c,All_Countries__c,Comments__c
                                           FROM Client_Address__c WHERE Client__c =: So1.SupplierlU__c and recordtypeid = :ConsigneerecordTypeId and address_status__c = 'Active' and CPA_v2_0__c=:So1.CPA_v2_0__c and Carrier_Type__c = 'Freight Forwarder' and Document_Type__C = 'AWB'];
                    }
                    FR.Pick_up_Contact_No__c = Pickupadd.Contact_Phone_Number__c;
                    FR.Ship_From_Country__c = SO.Ship_From_Country__c;
                    FR.Ship_To__c = SO.Destination__c;
                    FR.Client__c = rw.AccountID;
                    Fr.Shipment_Order__c = SO.Id;
                    System.debug('Ac.Shipping_Premium_Discount__c '+Ac.Shipping_Premium_Discount__c);
                    Fr.International_Shipping_Discount_Override__c = Ac.Shipping_Premium_Discount__c;

                    Fr.Chargeable_weight_in_KGs_packages__c = So.Chargeable_Weight__c;
                    FR.Pick_up_Contact_No__c = Pickupadd.Contact_Phone_Number__c;
                    FR.Pick_Up_Contact_Email__c = Pickupadd.Contact_Email__c;
                    FR.Pick_Up_Contact_Address1__c = Pickupadd.Address__c;
                    FR.Pick_Up_Contact_Address2__c = Pickupadd.Address2__c;
                    FR.Pick_Up_Contact_City__c = Pickupadd.City__c;
                    FR.Pick_Up_Contact_Province__c = Pickupadd.Province__c;
                    FR.Pick_Up_Contact_Country__c = Pickupadd.All_Countries__c;
                    FR.Pick_Up_Contact_ZIP__c = Pickupadd.Postal_Code__c;
                    Fr.Ship_From_Address__c = Pickupadd.Id;             //pickup
                    Fr.Notify_Address__C = Consigneeadd[0].id;            // Notify(Broker) address

                    FR.AWB_Consignee_Naming_Conventions__c = Consigneeadd[0].Naming_Conventions__c;
                    FR.Con_Company__c = Consigneeadd[0].Default_CompanyName__c;
                    FR.Con_Name__c = Consigneeadd[0].Default_Contact_Full_Name__c;
                    FR.Con_Email__c = Consigneeadd[0].Default_Contact_Email__c;
                    FR.Con_Phone__c = Consigneeadd[0].Default_Contact_Phone_Number__c;

                    if(Consigneeadd[0].Default_Address__c != null) {

                        if(Consigneeadd[0].Default_Address__c.length() >= 34) {
                            FR.Con_AddressLine1__c =Consigneeadd[0].Default_Address__c.substring(0,34);
                        }else {
                            FR.Con_AddressLine1__c =Consigneeadd[0].Default_Address__c;
                        }
                    }
                    if (Consigneeadd[0].Default_Address2__c != null) {
                        if(Consigneeadd[0].Default_Address2__c.length() >= 34) {
                            FR.Con_AddressLine2__c =Consigneeadd[0].Default_Address2__c.substring(0,34);
                        }else {
                            FR.Con_AddressLine2__c =Consigneeadd[0].Default_Address2__c;
                        }
                    }

                    String street = Consigneeadd[0].Default_Address__c+Consigneeadd[0].Default_Address2__c;
                    if(street.length() >= 34) {
                        FR.Con_Street__c =street.substring(0,34);
                    }else {
                        FR.Con_Street__c =street;
                    }

                    FR.Con_City__c = Consigneeadd[0].Default_City__c;
                    FR.Con_State__c = Consigneeadd[0].Default_Province__c;
                    FR.Con_Country__c = Consigneeadd[0].Default_All_Countries__c;
                    FR.Con_Postal_Code__c = Consigneeadd[0].Default_Postal_Code__c;
                    //FR.Pick_Up_Contact_ZIP__c=Pickupadd.Postal_Code__c;
                    Fr.FF_Consignee_Address__c = FF_Consigneeadd[0].id;            // Notify(Broker) address
                    //	if(Consigneeadd.size() <= 0){

                    FR.FF_AWB_Consignee_Naming_Conventions__c = FF_Consigneeadd[0].Naming_Conventions__c;
                    FR.FF_Con_Company__c = FF_Consigneeadd[0].Default_CompanyName__c;
                    FR.FF_Con_Name__c = FF_Consigneeadd[0].Default_Contact_Full_Name__c;
                    FR.FF_Con_Email__c = FF_Consigneeadd[0].Default_Contact_Email__c;
                    FR.FF_Con_Phone__c = FF_Consigneeadd[0].Default_Contact_Phone_Number__c;

                    if(FF_Consigneeadd[0].Default_Address__c != null) {
                        if(FF_Consigneeadd[0].Default_Address__c.length() >= 34) {
                            FR.FF_Con_AddressLine1__c =FF_Consigneeadd[0].Default_Address__c.substring(0,34);
                        }else {
                            FR.FF_Con_AddressLine1__c =FF_Consigneeadd[0].Default_Address__c;
                        }
                    }
                    if (FF_Consigneeadd[0].Default_Address2__c != null) {
                        if(FF_Consigneeadd[0].Default_Address2__c.length() >= 34) {
                            FR.FF_Con_AddressLine2__c =FF_Consigneeadd[0].Default_Address2__c.substring(0,34);
                        }else {
                            FR.FF_Con_AddressLine2__c =FF_Consigneeadd[0].Default_Address2__c;
                        }
                    }

                    String FF_street = FF_Consigneeadd[0].Default_Address__c+FF_Consigneeadd[0].Default_Address2__c;
                    if(FF_street.length() >= 34) {
                        FR.FF_Con_Street__c =FF_street.substring(0,34);
                    }else {
                        FR.FF_Con_Street__c =FF_street;
                    }

                    FR.FF_Con_City__c = FF_Consigneeadd[0].Default_City__c;
                    FR.FF_Con_State__c = FF_Consigneeadd[0].Default_Province__c;
                    FR.FF_Con_Country__c = FF_Consigneeadd[0].Default_All_Countries__c;
                    FR.FF_Con_Postal_Code__c = FF_Consigneeadd[0].Default_Postal_Code__c;

                    Insert FR;
                }
                if(!Test.isRunningTest()) {
                    System.enqueueJob(new QueueableUpdateCE(SO));
                }

                Shipment_order__c SO1 = [SELECT Id,Name FROM Shipment_order__c WHERE Id=: SO.Id ];
                res.responseBody = Blob.valueOf(JSON.serializePretty(SO1));
                res.statusCode = 200;

                API_Log__c Al = New API_Log__c();
                Al.Account__c=rw.AccountID;
                Al.Login_Contact__c=rw.ContactID;
                Al.EndpointURLName__c='CEQuickQuoteCreation';
                Al.Response__c='Success - QuickQuote Created - '+SO.Id;
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al;

            }
        }
        Catch(exception e){
            String ErrorString ='Accesstoken Expired or Something went wrong while creating QuickQuote, please contact support_SF@tecex.com';
            res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
            res.statusCode = 500;
            API_Log__c Al = New API_Log__c();
            Al.Account__c=rw.AccountID;
            Al.Login_Contact__c=rw.ContactID;
            Al.EndpointURLName__c='CEQuickQuoteCreation';
            Al.Response__c=e.getMessage()+' at line number: '+e.getLineNumber();
            Al.StatusCode__c=string.valueof(res.statusCode);
            Insert Al;
        }
    }
}