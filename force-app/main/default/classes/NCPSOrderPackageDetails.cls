@RestResource(urlMapping='/SOrderPackageDetails/*')
Global class NCPSOrderPackageDetails {

    @Httppost
    global static void NCPSOrderPackageDetails(){
        
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        Blob body = req.requestBody;
    	res.addHeader('Content-Type', 'application/json');
        String requestString = body.toString();
        NCPSOrderPackageDetailsWra rw = (NCPSOrderPackageDetailsWra)JSON.deserialize(requestString,NCPSOrderPackageDetailsWra.class);
           try {
                Access_token__c At =[select status__c,Access_Token__c from Access_token__c where Access_Token__c=:rw.Accesstoken ];
            	if( at.status__C =='Active'){
                 
                    
                     Shipment_Order_Package__c SOP = [select id,Dangerous_Goods__c,Lithium_Batteries__c,Chargeable_Weight_KGs__c,Shipment_Order__c,Weight_Unit__c,Dimension_Unit__c,packages_of_same_weight_dims__c,Length__c,Height__c,Breadth__c,Actual_Weight__c,Actual_Weight_KGs__c,Oversized__c,Volumetric_Weight_KGs__c from Shipment_Order_Package__c where id=:rw.SOPID];
            
         
          		  res.responseBody = Blob.valueOf(JSON.serializePretty(SOP));
                     res.addHeader('Content-Type', 'application/json');
        		  res.statusCode = 200;
                
                API_Log__c Al = New API_Log__c();
                Al.Account__c=rw.AccountID;
                Al.Login_Contact__c=rw.ContactID;
                Al.EndpointURLName__c='NCP-ShipmentOrderPackageDetails';
                Al.Response__c='Success - ShipmentOrderPackage Details are sent';
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al;
             
                }
               
               
           		}
        Catch(Exception e){
         		String ErrorString ='Something went wrong while getting Shipment order packages details, please contact support_SF@tecex.com';
               	res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
                res.addHeader('Content-Type', 'application/json');
            	res.statusCode = 500;
                API_Log__c Al = New API_Log__c();
                Al.Account__c=rw.AccountID;
                Al.Login_Contact__c=rw.ContactID;
                Al.EndpointURLName__c='ShipmentOrderPackageDetails';
                Al.Response__c='Exception - ShipmentOrderPackage Details are not sent'+e.getMessage();
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al;
        }
        
        
    }
    
}