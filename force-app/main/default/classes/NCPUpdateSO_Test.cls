@isTest
public class NCPUpdateSO_Test {
	
    public static testMethod void CPUpdateCostEstimates_Test1(){
        
        //Test Data
        Id shipmentOrderRecTypeId = Schema.SObjectType.shipment_order__c.getRecordTypeInfosByName().get('Cost Estimate').getRecordTypeId();
        
        Access_token__c accessTokenObj = new Access_token__c();
        accessTokenObj.Status__c = 'Active';
        accessTokenObj.Access_Token__c = '123';
        
        Insert accessTokenObj;
        
        CpaRules__c cpaRule = new CpaRules__c();
        cpaRule.Country__c = 'Finland';
        cpaRule.Criteria__c = 'Default';
        
        Insert cpaRule;
        
        Account acc = new Account(Name = 'TecEx Prospective Client');
        
        Insert acc;
        
        Buyer_Accounts__c byAcc = new Buyer_Accounts__c(Name = 'Buyer test');
        
        insert  byAcc;
        
        IOR_Price_List__c priceList = new IOR_Price_List__c ( Client_Name__c = acc.Id, Name = 'Finland', IOR_Fee__c = 0.15, On_Charge_Mark_up__c = 0.15,
                                                             TecEx_Shipping_Fee_Markup__c =0.40, IOR_Min_Fee__c = 100,  Admin_Fee__c =100, Set_up_fee__c =100, Bank_Fees__c =200, Tax_Rate__c = 0.20,
                                                             Estimated_Customs_Brokerage__c =250, Estimated_Customs_Clearance__c =250, Estimated_Customs_Handling__c =250, Estimated_Import_License__c =250, Destination__c = 'Finland' );

        Insert priceList;
        
        shipment_order__c shipment = new shipment_order__c();
        shipment.RecordTypeId = shipmentOrderRecTypeId;
        shipment.Destination__c = 'Finland';
        shipment.IOR_Price_List__c = priceList.Id;
        shipment.Shipment_Value_USD__c = 600;
        shipment.Account__c = acc.Id;
        
        Insert shipment;
        //Json Body
        String json = '{"Accesstoken":"'+accessTokenObj.Access_Token__c+'","ID":"'+shipment.Id+'","Reference1":"Null","Reference2":"Null", "Shipment_Value_USD":"600.00",'+
        				+'"LionBatteries":"Yes","LionBatteriestype":"","TypeOfGoods":"Refurbished","PONumber":"600", "ServiceType":"IOR",'+
        				+'"ShipFromCountry":"Aruba","ChargeableWeight":"123.00","FinalDeliveries":"1234","BuyerAccount":"'+byAcc.Id+'", "NCPClientNotes":"My Notes",'+
            +'"ShippingStatus":"Cost Estimate","ReasonforCancel":"Reason For cancellation","NCPCancelQuoteReason":"Other"}';
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
        req.requestURI = '/services/apexrest/UpdateOrder'; 
        req.httpMethod = 'Post';
        req.addHeader('Content-Type', 'application/json');
        req.requestBody = Blob.valueof(json);
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
        NCPUpdateSO.CPUpdateCostEstimates();
        Test.stopTest(); 
    }
    
    //Covering Catch Exception
    public static testMethod void CPUpdateCostEstimates_Test2(){
        
        //Test Data
        Id shipmentOrderRecTypeId = Schema.SObjectType.shipment_order__c.getRecordTypeInfosByName().get('Cost Estimate').getRecordTypeId();
        
        CpaRules__c cpaRule = new CpaRules__c();
        cpaRule.Country__c = 'Finland';
        cpaRule.Criteria__c = 'Default';
        
        Insert cpaRule;
        
        Access_token__c accessTokenObj = new Access_token__c();
        accessTokenObj.Status__c = 'Active';
        accessTokenObj.Access_Token__c = '123';
        
        Insert accessTokenObj;
        
        Account acc = new Account(Name = 'TecEx Prospective Client');
        
        Insert acc;
        
        IOR_Price_List__c priceList = new IOR_Price_List__c ( Client_Name__c = acc.Id, Name = 'Finland', IOR_Fee__c = 0.15, On_Charge_Mark_up__c = 0.15,
                                                             TecEx_Shipping_Fee_Markup__c =0.40, IOR_Min_Fee__c = 100,  Admin_Fee__c =100, Set_up_fee__c =100, Bank_Fees__c =200, Tax_Rate__c = 0.20,
                                                             Estimated_Customs_Brokerage__c =250, Estimated_Customs_Clearance__c =250, Estimated_Customs_Handling__c =250, Estimated_Import_License__c =250, Destination__c = 'Finland' );

        Insert priceList;
        
        shipment_order__c shipment = new shipment_order__c();
        shipment.RecordTypeId = shipmentOrderRecTypeId;
        shipment.Destination__c = 'Finland';
        shipment.IOR_Price_List__c = priceList.Id;
        shipment.Shipment_Value_USD__c = 600;
        shipment.Account__c = acc.Id;
        
        Insert shipment;
        //Json Body
        String json = '{"Accesstoken":"'+accessTokenObj.Access_Token__c+'","ID":"'+shipment.Id+'","Reference1":"Null","Reference2":"Null", "Shipment_Value_USD":"600",'+
        				+'"LionBatteries":"Yes","LionBatteriestype":"Lithium ION batteries","TypeOfGoods":"Refurbished","PONumber":"600", "ServiceType":"IOR",'+
        				+'"ShipFromCountry":"Aruba","ChargeableWeight":"123","FinalDeliveries":1234,"BuyerAccount":"'+acc.Id+'", "NCPClientNotes":"My Notes",'+
            +'"ShippingStatus":"Cost Estimate Rejected","ReasonforCancel":"Reason For cancellation","NCPCancelQuoteReason":"Other"}';
         
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
        req.requestURI = '/services/apexrest/UpdateOrder'; 
        req.httpMethod = 'Post';
        req.addHeader('Content-Type', 'application/json');
        req.requestBody = Blob.valueof(json);
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
        NCPUpdateSO.CPUpdateCostEstimates();
        Test.stopTest(); 
    }
}