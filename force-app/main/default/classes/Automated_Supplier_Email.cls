public class Automated_Supplier_Email {
	public static void sendEmailSummary(List<Shipment_Order__c> SOList, String SupplierId, String newEmailAddress){
    	List <Shipment_Order__c> soListToSend = SOList;
        //String financeContactAlias;
        if(SupplierId != null){
        	soListToSend = Automated_Supplier_Email.getSOList(SupplierId);
        }
        Map <Id,List<Shipment_Order__c>> soMap = new Map <Id,List<Shipment_Order__c>>();
        Map <Id,List<String>> soTableMap = new Map <Id,List<String>>();
        List <Account> allAccounts;
        if(soListToSend.size()>0){
        	for(Shipment_Order__c currentSO:soListToSend){
            	String soName = currentSO.Name;
                String shipStatus = '';
                if(currentSO.Shipping_Status__c != null){
                	shipStatus = currentSO.Shipping_Status__c;
                }
                String datePODReceived = '';
                if(currentSO.POD_Date__c != null){
                    datePODReceived = String.valueOf(currentSO.POD_Date__c);
                }
                String clientAccount = '';
                if(currentSO.POD_Date__c != null){
                    clientAccount = currentSO.Account__r.Name;
                }
                String soDestination = '';
                if(currentSO.Destination__c != null){
                    soDestination = currentSO.Destination__c;
                }
                if(soMap.containsKey(currentSO.SupplierlU__c)){
                	List<Shipment_Order__c> contactSO = soMap.get(currentSO.SupplierlU__c);
                    List<String> contactSOHtml = soTableMap.get(currentSO.SupplierlU__c);
                    contactSO.add(currentSO);
                    String htmlRows = '<tr><td style="padding:5px; border:1px solid black; "">'+soName+'</td><td style="padding:5px; border:1px solid black; "">'+shipStatus+'</td><td style="padding:5px; border:1px solid black; "">'+datePODReceived+'</td><td style="padding:5px; border:1px solid black; "">'+clientAccount+'</td><td style="padding:5px; border:1px solid black; "">'+soDestination+'</td></tr>';
                    contactSOHtml.add(htmlRows);
                    soTableMap.put(currentSO.SupplierlU__c,contactSOHtml);
                    soMap.put(currentSO.SupplierlU__c,contactSO);
                }else{
                    List<Shipment_Order__c> contactSO = new List <Shipment_Order__c>();
                    List<String> contactSOHtml = new List <String>();
                    contactSO.add(currentSO);
                    String htmlRows = '<tr><td style="padding:5px; border:1px solid black; "">'+soName+'</td><td style="padding:5px; border:1px solid black; "">'+shipStatus+'</td><td style="padding:5px; border:1px solid black; "">'+datePODReceived+'</td><td style="padding:5px; border:1px solid black; "">'+clientAccount+'</td><td style="padding:5px; border:1px solid black; "">'+soDestination+'</td></tr>';
                    contactSOHtml.add(htmlRows);
                    soTableMap.put(currentSO.SupplierlU__c,contactSOHtml);
                    soMap.put(currentSO.SupplierlU__c,contactSO);
                }
           	}
            Set <Id> AccountSet = new Set <Id>();
            allAccounts = [Select Id,Name from Account where Id in : soMap.KeySet()];
            if(allAccounts.size()>0){
            	for(Account currentAccount : allAccounts){
                	AccountSet.add(currentAccount.Id);
                }
            }
            List<String> emailAddressesToSend = new List<String>();
            List <Account> FinalallAccounts1 = new List<Account>();
            List <Contact> contactRecs = [Select Id,AccountId,Name,Email,Include_in_invoicing_emails__c from Contact where AccountId in : accountSet AND Include_in_invoicing_emails__c = TRUE];
            Map<id,Account> FinalAccounts = new Map<id,Account>([Select Id,Name from Account where id in : accountSet]);
            if(contactRecs.size()>0){
            	for(Contact financeContact : contactRecs){
                	emailAddressesToSend.add(financeContact.Email);
                    FinalallAccounts1.add(FinalAccounts.get(financeContact.AccountId));
                }
            }
            String allToEmails = string.join(emailAddressesToSend, ';');
            Set<Account> Accset = new set<Account>(FinalallAccounts1);
            List <Account> FinalallAccounts = new List<Account>(Accset);
            List <Messaging.SingleEmailMessage> mails = new List <Messaging.SingleEmailMessage>();
            if(FinalallAccounts.size()>0){
                OrgWideEmailAddress[] owea = [Select Id from OrgWideEmailAddress where Address = 'finance@tecex.com'];
                for(Integer i=0;FinalallAccounts.size()>i;i++){
                	for(Integer J=0;contactRecs.size()>j;J++){
                        if(FinalallAccounts[i].id == contactRecs[j].accountid){
                            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                            String Email=contactRecs[j].Email;
                            //String ContactName=contactRecs[j].Name;
                            //financeContactAlias = ContactName;
                            if(owea.size()>0){
                            	mail.setOrgWideEmailAddressId(owea.get(0).Id);
                            }
                            string body = '<font face="Calibri">Dear Finance Team,<br/><br/>Please note that we are still waiting for invoices for the following:'; //Dear '+financeContactAlias+',<br/><br/>
                            body += '<br/><br/><table cellspacing=0 style="font-family:Calibri; border:1px solid black; border-collapse:collapse;"><tr><th style="padding:5px; border:1px solid black;">Shipment Number</th><th style="padding:5px; border:1px solid black;">Shipment Status</th><th style="padding:5px; border:1px solid black;">Date of POD</th><th style="padding:5px; border:1px solid black;">Account</th><th style="padding:5px; border:1px solid black;">Destination</th></tr>';
                            List<String> mailTable=soTableMap.get(FinalallAccounts[i].ID);
                            if(mailTable.size()>0){
                                for(String currentItem: mailTable){
                                    body += currentItem;
                                }
                            }
                            body += '</table><br/>Kindly upload onto the TecEx Supplier Portal (https://tecex.force.com/SupplierPortal) as soon as possible to avoid delay in payment.<br/><br/>Should you require any further information, please contact finance@tecex.com.<br/><br/>Regards,<br/>The TecEx Finance Team<br/><br/></font>';
                            mail.setSubject('Outstanding Invoices for '+FinalallAccounts[i].Name);
                            String[] toAddresses = new String[] {Email};
                            //String[] toAddresses = new String[] {'pieterr@tecex.com'};
                            mail.setToAddresses(toAddresses);
                            String[] ccAddresses = new String[] {'finance@tecex.com'};
                            mail.setCcAddresses(ccAddresses);
                            mail.setSaveAsActivity(false);
                            mail.setWhatId(FinalallAccounts[i].Id);
                            mail.setHtmlBody(body);
                            mails.add(mail);
                        }
                    }
                }
                Messaging.sendEmail(mails);
            }
        }
    }
    public static List<Shipment_Order__c> getSOList(String SupplierId){
    	List <Shipment_Order__c> soRecords = [Select Id,Name,SupplierlU__c,SupplierlU__r.Name,Account__r.Name,Destination__c,Shipping_Status__c,POD_Date__c,Days_Since_POD_Date__c,CreatedDate,Test_Account__c,Final_Supplier_Invoice_Received__c from Shipment_Order__c where Account__c = :SupplierId AND (Shipping_Status__c = 'POD Received' OR Shipping_Status__c = 'Customs Clearance Docs Received') AND (Actual_Total__c = 0.00) AND (CreatedDate >= 2020-01-01T00:00:00Z) AND (Days_Since_POD_Date__c > 2) AND (Test_Account__c = FALSE) AND (Final_Supplier_Invoice_Received__c = FALSE) AND (SupplierlU__r.Name != 'Stream (supplier)') AND (SupplierlU__r.Name != 'Kintetsu World Express South Africa Proprietary Limited')];
        if(soRecords.size()>0){
            return soRecords;
        }else{
            return null;
        }
    }
}