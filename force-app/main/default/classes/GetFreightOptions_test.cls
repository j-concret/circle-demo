@IsTest(SeeAllData = True)

public class GetFreightOptions_test {
    
     private static testMethod void  setUpData(){
     
         Account account = new Account (name='Acme1', Type ='Client', CSE_IOR__c = '0050Y000001LTZO');
        insert account;  
        
        Account account2 = new Account (name='Test Supplier', Type ='Supplier', ICE__c = '0050Y000001km5c', RecordTypeId = '0120Y0000006KjQQAU');
        insert account2;      
         
        Contact contact = new Contact ( lastname ='Testing Individual',  AccountId = account.Id);
        insert contact;  
        
        IOR_Price_List__c iorpl = new IOR_Price_List__c ( Client_Name__c = account.Id, Name = 'Brazil', IOR_Fee__c = 0.15, On_Charge_Mark_up__c = 0.15,
        TecEx_Shipping_Fee_Markup__c =0.40, IOR_Min_Fee__c = 100,  Admin_Fee__c =100, Set_up_fee__c =100, Bank_Fees__c =200, Tax_Rate__c = 0.20,
        Estimated_Customs_Brokerage__c =250, Estimated_Customs_Clearance__c =250, Estimated_Customs_Handling__c =250, Estimated_Import_License__c =250, Destination__c = 'Brazil' );
        insert iorpl; 
       
        country_price_approval__c cpa = new country_price_approval__c( name ='Brazil', Billing_term__c = 'DAP/CIF - IOR pays',  Airwaybill_Instructions__c = 'User IOR Address', Supplier__c = account2.Id, 
        In_Country_Specialist__c = '0050Y000001km5c');
        insert cpa;
        
        CPA_v2_0__c cpav2 = new CPA_v2_0__c(Name = 'Brazil Testing 2.0', Supplier__c = account2.Id, Service_Type__c = 'IOR/EOR', Related_Costing_CPA__c = 'a260Y00000EnDjr', Preferred_Supplier__c = TRUE,
                                           VAT_Rate__c = 0.1, Destination__c = 'Brazil');
        insert cpav2;
       
        List<CPA_Costing__c> costs = new List<CPA_Costing__c>();

       
       costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'Shipment value', Cost_Type__c = 'Variable',  Amount__c = null, Rate__c = 10, IOR_EOR__c = 'IOR', Min__c = 375, Max__c = 14000, Conditional_value__c = 'Shipment value', Condition__c = 'Between', Floor__c = 0, Ceiling__c = 10000));
       costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'Shipment value', Cost_Type__c = 'Variable',  Amount__c = null, Rate__c = 10, IOR_EOR__c = 'IOR', Min__c = 563, Max__c = 14000, Conditional_value__c = 'Shipment value', Condition__c = 'Between', Floor__c = 10000, Ceiling__c = 20000));
       costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'Shipment value', Cost_Type__c = 'Variable',  Amount__c = null, Rate__c = 10, IOR_EOR__c = 'IOR', Min__c = 750, Max__c = 14000, Conditional_value__c = 'Shipment value', Condition__c = '>', Floor__c = 20000));
       costs.add(new CPA_Costing__c(Cost_Category__c = 'International freight',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'Chargeable weight', Cost_Type__c = 'Variable',  Amount__c = null, Rate__c = 10, IOR_EOR__c = 'IOR', Min__c = 100, Max__c = 500, Conditional_value__c = 'Chargeable weight', Condition__c = '>', Floor__c = 100));
       costs.add(new CPA_Costing__c(Cost_Category__c = 'International freight',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'Chargeable weight', Cost_Type__c = 'Variable',  Amount__c = null, Rate__c = 10, IOR_EOR__c = 'IOR', Min__c = 55));
       costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'Shipment value', Cost_Type__c = 'Variable',  Amount__c = null, Rate__c = 10, IOR_EOR__c = 'IOR', Conditional_value__c = 'CIF value', Condition__c = '>', Floor__c = 20000));
       costs.add(new CPA_Costing__c(Cost_Category__c = 'Miscellaneous',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'CIF value', Cost_Type__c = 'Variable',  Amount__c = null, Rate__c = 10, IOR_EOR__c = 'IOR'));
       costs.add(new CPA_Costing__c(Cost_Category__c = 'Miscellaneous',CPA_v2_0__c = cpav2.Id, Applied_to__c = '# packages', Cost_Type__c = 'Variable', Amount__c = null, Rate__c = 10, IOR_EOR__c = 'IOR'));
       costs.add(new CPA_Costing__c(Cost_Category__c = 'Miscellaneous',CPA_v2_0__c = cpav2.Id, Applied_to__c = '# Parts (line items)', Cost_Type__c = 'Variable', Amount__c = null, Rate__c = 10, IOR_EOR__c = 'IOR'));
       costs.add(new CPA_Costing__c(Cost_Category__c = 'Miscellaneous',CPA_v2_0__c = cpav2.Id, Applied_to__c = '# Unique HS Codes', Cost_Type__c = 'Variable', Amount__c = null, Rate__c = 10, IOR_EOR__c = 'IOR'));
       costs.add(new CPA_Costing__c(Cost_Category__c = 'Miscellaneous',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'Total Taxes', Cost_Type__c = 'Variable', Amount__c = null, Rate__c = 10, IOR_EOR__c = 'IOR'));
       costs.add(new CPA_Costing__c(Cost_Category__c = 'Miscellaneous',CPA_v2_0__c = cpav2.Id, Variable_threshold__c = 10,  Applied_to__c = 'Shipment value', Cost_Type__c = 'Variable',  Amount__c = null, Rate__c = 10, IOR_EOR__c = 'IOR'));
       costs.add(new CPA_Costing__c(Cost_Category__c = 'Miscellaneous',CPA_v2_0__c = cpav2.Id, Variable_threshold__c = 100,  Applied_to__c = 'Chargeable weight', Cost_Type__c = 'Variable',  Amount__c = null, Rate__c = 10, IOR_EOR__c = 'IOR', VAT_applicable__c = TRUE));
       costs.add(new CPA_Costing__c(Cost_Category__c = 'Miscellaneous',CPA_v2_0__c = cpav2.Id, Cost_Type__c = 'Fixed',  Amount__c = 300, Rate__c = null, IOR_EOR__c = 'IOR'));
       costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'Shipment value', Cost_Type__c = 'Variable',  Amount__c = null, Rate__c = 10, IOR_EOR__c = 'EOR', Min__c = 375, Max__c = 14000, Conditional_value__c = 'Shipment value', Condition__c = 'Between', Floor__c = 0, Ceiling__c = 10000));
       costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'Shipment value', Cost_Type__c = 'Variable',  Amount__c = null, Rate__c = 10, IOR_EOR__c = 'EOR', Min__c = 563, Max__c = 14000, Conditional_value__c = 'Shipment value', Condition__c = 'Between', Floor__c = 10000, Ceiling__c = 20000));
       costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'Shipment value', Cost_Type__c = 'Variable',  Amount__c = null, Rate__c = 10, IOR_EOR__c = 'EOR', Min__c = 750, Max__c = 14000, Conditional_value__c = 'Shipment value', Condition__c = '>', Floor__c = 20000));
       costs.add(new CPA_Costing__c(Cost_Category__c = 'International freight',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'Chargeable weight', Cost_Type__c = 'Variable',  Amount__c = null, Rate__c = 10, IOR_EOR__c = 'EOR', Max__c = 100));
       costs.add(new CPA_Costing__c(Cost_Category__c = 'International freight',CPA_v2_0__c = cpav2.Id, Applied_to__c = '# Packages', Cost_Type__c = 'Variable',  Amount__c = null, Rate__c = 10, IOR_EOR__c = 'EOR', Conditional_value__c = '# of packages', Condition__c = '>', Floor__c = 2));
       costs.add(new CPA_Costing__c(Cost_Category__c = 'Miscellaneous',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'CIF value', Cost_Type__c = 'Variable',  Amount__c = null, Rate__c = 10, IOR_EOR__c = 'EOR'));
       costs.add(new CPA_Costing__c(Cost_Category__c = 'Miscellaneous',CPA_v2_0__c = cpav2.Id, Applied_to__c = '# packages', Cost_Type__c = 'Variable', Amount__c = null, Rate__c = 10, IOR_EOR__c = 'EOR'));
       costs.add(new CPA_Costing__c(Cost_Category__c = 'Miscellaneous',CPA_v2_0__c = cpav2.Id, Applied_to__c = '# Parts (line items)', Cost_Type__c = 'Variable', Amount__c = null, Rate__c = 10, IOR_EOR__c = 'EOR'));
       costs.add(new CPA_Costing__c(Cost_Category__c = 'Miscellaneous',CPA_v2_0__c = cpav2.Id, Applied_to__c = '# Unique HS Codes', Cost_Type__c = 'Variable', Amount__c = null, Rate__c = 10, IOR_EOR__c = 'EOR'));
       costs.add(new CPA_Costing__c(Cost_Category__c = 'Miscellaneous',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'Total Taxes', Cost_Type__c = 'Variable', Amount__c = null, Rate__c = 10, IOR_EOR__c = 'EOR'));
       costs.add(new CPA_Costing__c(Cost_Category__c = 'Miscellaneous',CPA_v2_0__c = cpav2.Id, Variable_threshold__c = 10,  Applied_to__c = 'Shipment value', Cost_Type__c = 'Variable',  Amount__c = null, Rate__c = 10, IOR_EOR__c = 'EOR'));
       costs.add(new CPA_Costing__c(Cost_Category__c = 'Miscellaneous',CPA_v2_0__c = cpav2.Id, Variable_threshold__c = 100,  Applied_to__c = 'Chargeable weight', Cost_Type__c = 'Variable',  Amount__c = null, Rate__c = 10, IOR_EOR__c = 'EOR'));
       costs.add(new CPA_Costing__c(Cost_Category__c = 'Miscellaneous',CPA_v2_0__c = cpav2.Id,  Cost_Type__c = 'Fixed',  Amount__c = 100, Rate__c = null, IOR_EOR__c = 'EOR'));
       insert costs;
        
        
        Shipment_Order__c shipmentOrder = new Shipment_Order__c(Account__c = account.id, Shipment_Value_USD__c =1000, CPA_v2_0__c = cpav2.Id,
        Client_Contact_for_this_Shipment__c = contact.Id, Who_arranges_International_courier__c ='Client', Tax_Treatment__c ='DAP/CIF - IOR pays', Ship_to_Country__c = cpa.Id,
        Destination__c = 'Brazil', Service_Type__c = 'IOR', IOR_Price_List__c = iorpl.Id, 
        CI_Admin_Fee__c  =100, CI_Bank_Fees__c  = 100, CI_Finance_Fee__c  = 100, CI_Insurance_Fee_USD__c  = 100, CI_International_Delivery_Fee__c  = 100, CI_IOR_and_Import_Compliance_Fee_USD__c  = 100, 
        CI_Miscellaneous_Fee__c  = 100, CI_Recharge_Tax_and_Duty__c  = 100, CI_Total_Clearance_Costs__c  = 100, CI_Total_Customs_Brokerage_Cost__c  = 100, CI_Total_Handling_Cost__c  = 100, 
        CI_Total_Licence_Cost__c  = 100, FC_Admin_Fee__c  = 100, FC_Bank_Fees__c  = 100, FC_Finance_Fee__c  = 100, FC_Insurance_Fee_USD__c   = 100, FC_International_Delivery_Fee__c  = 100, 
        FC_IOR_and_Import_Compliance_Fee_USD__c  = 100, FC_Miscellaneous_Fee__c  = 100, FC_Recharge_Tax_and_Duty__c  = 100, FC_Total_Clearance_Costs__c  = 100, 
        FC_Total_Customs_Brokerage_Cost__c  = 100, FC_Total_Handling_Costs__c  = 100, FC_Total_License_Cost__c  = 100, Total_CIF_Duties__c = 0, Total_FOB_Duties__c = 0);
        insert shipmentOrder; 
       
        Shipment_Order__c shipmentOrder2 = new Shipment_Order__c(Account__c = account.id, Shipment_Value_USD__c =10000, CPA_v2_0__c = cpav2.Id,
        Client_Contact_for_this_Shipment__c = contact.Id, Who_arranges_International_courier__c ='Client', Tax_Treatment__c ='DAP/CIF - IOR pays', Ship_to_Country__c = cpa.Id,
        Destination__c = 'Brazil', Service_Type__c = 'EOR', IOR_Price_List__c = iorpl.Id, of_Unique_Line_Items__c = 10, Total_Taxes__c= 1500, of_Line_Items__c= 10, Chargeable_Weight__c = 200,
        CI_Admin_Fee__c  =100, CI_Bank_Fees__c  = 100, CI_Finance_Fee__c  = 100, CI_Insurance_Fee_USD__c  = 100, CI_International_Delivery_Fee__c  = 100, CI_IOR_and_Import_Compliance_Fee_USD__c  = 100, 
        CI_Miscellaneous_Fee__c  = 100, CI_Recharge_Tax_and_Duty__c  = 100, CI_Total_Clearance_Costs__c  = 100, CI_Total_Customs_Brokerage_Cost__c  = 100, CI_Total_Handling_Cost__c  = 100, 
        CI_Total_Licence_Cost__c  = 100, FC_Admin_Fee__c  = 100, FC_Bank_Fees__c  = 100, FC_Finance_Fee__c  = 100, FC_Insurance_Fee_USD__c   = 100, FC_International_Delivery_Fee__c  = 100, 
        FC_IOR_and_Import_Compliance_Fee_USD__c  = 100, FC_Miscellaneous_Fee__c  = 100, FC_Recharge_Tax_and_Duty__c  = 100, FC_Total_Clearance_Costs__c  = 100, 
        FC_Total_Customs_Brokerage_Cost__c  = 100, FC_Total_Handling_Costs__c  = 100, FC_Total_License_Cost__c  = 100, Total_CIF_Duties__c = 0, Total_FOB_Duties__c = 0);
        insert shipmentOrder2; 
        
         Freight__c FR = new Freight__c(Shipment_Order__c = shipmentOrder.Id,Li_Ion_Batteries__c = 'No');
         insert FR;         
         
         Shipment_Order_Package__c SOP = new Shipment_Order_Package__c(packages_of_same_weight_dims__c = 1,Shipment_Order__c = shipmentOrder.Id,
         Freight__c = FR.ID,
         Weight_Unit__c = 'KGs',
         Actual_Weight__c = 1,
         Dimension_Unit__c = 'CMs',
         Height__c = 1,
         Breadth__c = 1, 
         Length__c =1                                                         
                                                                       
         );                                                              
         
         Insert SOP;
         
         Part__c PA = new Part__c (Quantity__c=1,
                                  Commercial_Value__c=1,
                                  Part_Number__c='854785',
                                  Name='8547857',
                                  Shipment_Order__c=shipmentOrder.Id,
                                 US_HTS_Code__c= '85176290006');
         INsert PA;
         
         Final_Delivery__c FD = new Final_Delivery__c(
                                                     Shipment_Order__c =shipmentOrder.Id,
                                                     Contact_name__c = 'Con',
                                                     Contact_number__c = '235689657',
                                                     Contact_email__c ='anil@anil.com' );
         
         Insert FD;
         
        List<Id> soIds = new List<Id>();
        soIds.add(shipmentOrder.id); 
       
        List<Id> soIds2 = new List<Id>();
        soIds2.add(shipmentOrder2.id); 
         
        

         
        Shipment_Order__C SO1 = [select id from shipment_order__C where ID =:shipmentOrder.Id];
        Freight__c FR1 = [select id from Freight__c where Shipment_Order__c =: SO1.ID limit 1 ]; 
        Shipment_Order_Package__c SOP1 = [Select Id from Shipment_Order_Package__c where Shipment_Order__c =: SO1.Id limit 1]; 
        Part__c PA1  =[Select Id from Part__c where Shipment_Order__c =: SO1.Id limit 1];
        Final_Delivery__c FD1=[Select Id from Final_Delivery__c where Shipment_Order__c =: SO1.Id limit 1];
         
           
        
        list<Shipment_Order_Package__c > SOPList = New list<Shipment_Order_Package__c >(); 
        list<Part__c > PartList = New list<Part__c  >();
        list<Final_Delivery__c  > FDList = New list<Final_Delivery__c  >();
       
           
       
               
                 
      test.startTest();
     
     // Shipment_Order__C  SO = GetFreightOptions.GetFreightOptionsRecordID(ids);
     // LEX_LookUpControllerforRelatedcontacts.fetchMatchingContacts( 'ac.id','test', 'Contact' );
    
         GetFreightOptions.GetFreightOptionsRecordID(SO1.id); // call freightID
         
         GetFreightOptions.getSOPackages(SOP1.id); // So Packages
        GetFreightOptions.updateSOPackages(SOPList); // List Sopackages
         
         GetFreightOptions.getSOParts(PA1.id); //Parts
         GetFreightOptions.updateSOParts(PartList ); //List parts
        
         GetFreightOptions.getSOFinalDeliveries(FD1.id); // final deliveries
         GetFreightOptions.updateSOFinalDeliveries(FDList); // List Final deliveries
      

       test.stopTest();
       
    } 

}