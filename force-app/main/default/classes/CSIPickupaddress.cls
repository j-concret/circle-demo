@RestResource(urlMapping='/MyPickupAddresses/*')
Global class CSIPickupaddress {
 public class My1Exception extends Exception {} 
    @Httppost
    global static void NCPPickupAddressList(){
        
        RestRequest req = RestContext.request;
    	RestResponse res = RestContext.response;
     	res.addHeader('Content-Type', 'application/json');
        Blob body = req.requestBody;
        String requestString = body.toString();
        NCPFinalDestinationsWrap rw = (NCPFinalDestinationsWrap)JSON.deserialize(requestString,NCPFinalDestinationsWrap.class);
        
       
        
        try {
            
             Access_token__c At =[select status__c,Access_Token__c from Access_token__c where Access_Token__c=:rw.Accesstoken ];
               if( at.status__C =='Active'){
            Id recordTypeId = Schema.SObjectType.Client_Address__c.getRecordTypeInfosByDeveloperName().get('PickUp').getRecordTypeId();
        
            List<Client_Address__c> ABs = [select id,Address_Status__c,Name,default__c,Comments__c,CompanyName__c,AdditionalContactNumber__c,Contact_Full_Name__c,Contact_Email__c,Contact_Phone_Number__c,Address__c,Address2__c,City__c,Province__c,Postal_Code__c,All_Countries__c,Client__c    from Client_Address__c where Client__c =:rw.ClientAccId  and All_Countries__c = : rw.Country and Address_Status__c = 'Active' and RecordType.ID =:recordTypeId ORDER BY default__C desc];
            IF(!ABs.Isempty()){
                    res.responseBody = Blob.valueOf(JSON.serializePretty(ABs));
        			res.statusCode = 200;
                 res.addHeader('Content-Type', 'application/json');
                API_Log__c Al = New API_Log__c();
                Al.Account__c=rw.ClientAccId;
                Al.EndpointURLName__c='PickupAddress';
                Al.Response__c='Success - List of Pickup Address are sent';
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al;
            }
            
            Else{
                
                String ErrorString = 'No Addresses found';
                res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
        		res.statusCode = 200;
                 res.addHeader('Content-Type', 'application/json');
                API_Log__c Al = New API_Log__c();
                Al.Account__c=rw.ClientAccId;
                Al.EndpointURLName__c='PickupAddress';
                Al.Response__c='Success - 0 Pickup Address are found';
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al;
                throw new My1Exception('First exception');

               
                
            }
         } // If End
        }
        
        Catch(My1Exception e){
           		String ErrorString ='There are currently no addresses for this country. Please create a new address.';
               	res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
               res.addHeader('Content-Type', 'application/json');
                res.statusCode = 500;
                API_Log__c Al = New API_Log__c();
                Al.Account__c=rw.ClientAccId;
                Al.EndpointURLName__c='PickupAddress';
                Al.Response__c=e.getMessage()+' at line number: '+e.getLineNumber();
                Al.StatusCode__c=string.valueof(res.statusCode);
              	Insert Al;
            
            
        }
        
        
        
        
        
    }
    
}