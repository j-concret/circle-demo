@IsTest
public class NCPEmailValidation_Test {
    @testSetup
    static void setup(){
        Access_token__c accessTokenObj = new Access_token__c();
        accessTokenObj.Status__c = 'Active';
        accessTokenObj.Access_Token__c = 'asdfghj-sdfgfds-sfdfgf';
        
        Insert accessTokenObj;
        
        EmailBlocker__c emailB = new EmailBlocker__c(Name='gmail',Type_of_business__c='Medical Equipment;Tech;Marketing material;Other;Amazon FBA');
        insert emailB;
    }
    
    static testMethod void testNCPValidEmailAddres(){
        Access_token__c accessTokenObj = [SELECT Access_Token__c FROM Access_token__c LIMIT 1];
        
        NCPEmailValidationWra wrapperObj = new NCPEmailValidationWra();
        wrapperObj.Accesstoken = accessTokenObj.Access_Token__c; 
        wrapperObj.EmailAddress = 'abc@def.com';
        wrapperObj.typeOfGoods = 'Medical Equipment';
        RestRequest request = new RestRequest();
        RestResponse res = new RestResponse();
        request.requestUri ='/services/apexrest/api/NCPEmailvalidation/';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueOf(JSON.serialize(wrapperObj));
        RestContext.request = request;
        RestContext.response = res;
        
        Test.startTest();
        NCPEmailValidation.NCPEmailValidation();
        Test.stopTest();
    }
    static testMethod void testNCPBlockedEmailAddres(){
        Access_token__c accessTokenObj = [SELECT Access_Token__c FROM Access_token__c LIMIT 1];
        
        NCPEmailValidationWra wrapperObj = new NCPEmailValidationWra();
        wrapperObj.Accesstoken = accessTokenObj.Access_Token__c; 
        wrapperObj.EmailAddress = 'abc@gmail.com';
        wrapperObj.typeOfGoods = 'Medical Equipment';
        RestRequest request = new RestRequest();
        RestResponse res = new RestResponse();
        request.requestUri ='/services/apexrest/api/NCPEmailvalidation/';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueOf(JSON.serialize(wrapperObj));
        RestContext.request = request;
        RestContext.response = res;
        
        Test.startTest();
        NCPEmailValidation.NCPEmailValidation();
        Test.stopTest();
    }
    
     static testMethod void testNCPAuthFailed(){
        NCPEmailValidationWra wrapperObj = new NCPEmailValidationWra();
        wrapperObj.Accesstoken = 'abc'; 
        wrapperObj.EmailAddress = 'abc@gmail.com';
        wrapperObj.typeOfGoods = 'Medical Equipment';
        RestRequest request = new RestRequest();
        RestResponse res = new RestResponse();
        request.requestUri ='/services/apexrest/api/NCPEmailvalidation/';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueOf(JSON.serialize(wrapperObj));
        RestContext.request = request;
        RestContext.response = res;
        
        Test.startTest();
        NCPEmailValidation.NCPEmailValidation();
        Test.stopTest();
    }
    static testMethod void testNCPInvalidEmailAddres(){
        Access_token__c accessTokenObj = [SELECT Access_Token__c FROM Access_token__c LIMIT 1];
        
        NCPEmailValidationWra wrapperObj = new NCPEmailValidationWra();
        wrapperObj.Accesstoken = accessTokenObj.Access_Token__c; 
        wrapperObj.EmailAddress = 'abcgmailcom';
        wrapperObj.typeOfGoods = 'Medical Equipment';
        RestRequest request = new RestRequest();
        RestResponse res = new RestResponse();
        request.requestUri ='/services/apexrest/api/NCPEmailvalidation/';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueOf(JSON.serialize(wrapperObj));
        RestContext.request = request;
        RestContext.response = res;
        
        Test.startTest();
        NCPEmailValidation.NCPEmailValidation();
        Test.stopTest();
    }
}