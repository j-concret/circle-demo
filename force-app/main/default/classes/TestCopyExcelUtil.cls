/**
* ApexClass   	: TestCopyExcelUtil.cls
* Description	: Controller for the CopyExcelUtil
*/
@isTest
public with sharing class TestCopyExcelUtil {
    
    /**
* @description : This verifies if the sobject is prepared properly from the raw structure of sobject
*/
    @isTest public static void testPrepareSObject(){
        
        //create raw account object
        Map<String,Object> rawSObject = new Map<String,Object>();
        rawSObject.put('AnnualRevenue','21');
        rawSObject.put('SLAExpirationDate__c','01/01/2014');
        rawSObject.put('Industry',null);
        rawSObject.put('Type','');
        rawSObject.put('NumberofLocations__c','2');        
        Test.startTest();
        Map<String,Object> sobjectRecord = CopyExcelUtil.prepareSObject(rawSObject,'Account');
        Test.stopTest();
        System.assert(sobjectRecord.get('AnnualRevenue')!=null);//Checks if the record has fields properly populated
        
    }
    
}