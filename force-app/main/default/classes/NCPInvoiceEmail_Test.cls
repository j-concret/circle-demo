@Istest
public class NCPInvoiceEmail_Test {

    static testMethod void testNCPInvoiceEmail(){
        Id shipmentOrderRecTypeId = Schema.SObjectType.shipment_order__c.getRecordTypeInfosByName().get('Cost Estimate').getRecordTypeId();
        
        CpaRules__c cpaRule = new CpaRules__c();
        cpaRule.Country__c = 'Finland';
        cpaRule.Criteria__c = 'Default';
        
        Insert cpaRule;
        
        Access_token__c accessTokenObj = new Access_token__c();
        accessTokenObj.Status__c = 'Active';
        accessTokenObj.Access_Token__c = 'asdfghj-sdfgfds-sfdfgf';
        
        Insert accessTokenObj;
        
        Account acc = new Account(Name = 'TecEx Prospective Client');
        
        Insert acc;
       Contact contact = new Contact (Client_Notifications_Choice__c = 'opt-in', email = 'test@test.com', lastname ='Testing Individual',  AccountId = acc.Id);
        
        Insert contact;
        
        IOR_Price_List__c priceList = new IOR_Price_List__c();
        priceList.Destination__c = 'Finland';
        priceList.Client_Name__c = acc.Id;
        priceList.IOR_Fee__c = 10;
        
        Insert priceList;
        
        shipment_order__c shipment = new shipment_order__c();
        shipment.RecordTypeId = shipmentOrderRecTypeId;
        shipment.Destination__c = 'Finland';
        shipment.IOR_Price_List__c = priceList.Id;
        shipment.Shipment_Value_USD__c = 600;
        shipment.Account__c = acc.Id;
        shipment.Client_Contact_for_this_Shipment__c =contact.Id ;
        shipment.Email_Estimate_To__c='abc@xyz.com';
        Insert shipment;
        
        Invoice_New__c clientInv = new Invoice_New__c(   Invoice_Name__c = 'Test',  Account__c = acc.id, Due_date__c = date.today(),  Invoice_Currency__c = 'US Dollar (USD)',
                                                      Shipment_Order__c = shipment.Id,  Invoice_Type__c = 'Invoice',  Invoice_Status__c = 'FC Approved', FC_Admin_Costs__c= 100, FC_Bank_Costs__c=100,
                                                      FC_Finance_Costs__c=100, FC_IOR_Costs__c=100,  FC_International_Delivery_Costs__c=100, FC_Miscellaneous_Costs__c=100, FC_Duties_and_Taxes__c=100,
                                                      FC_Customs_Clearance_Costs__c=100, FC_Customs_Brokerage_Costs__c=100, FC_Customs_Handling_Costs__c=100, FC_Customs_License_Costs__c=100, FC_Duties_and_Taxes_Other__c=100,
                                                      Actual_Admin_Costs__c=100, Actual_Bank_Costs__c=100, Actual_Customs_Brokerage_Costs__c=100, Actual_Customs_Clearance_Costs__c=100, Actual_Duties_and_Taxes__c=100,
                                                      Actual_Finance_Costs__c=100, Actual_Customs_Handling_Costs__c=100, Actual_IOREOR_Costs__c=100, Actual_International_Delivery_Costs__c=100,
                                                      Actual_Miscellaneous_Costs__c=100, Actual_Customs_License_Costs__c=100, Actual_Duties_and_Taxes_Other__c=100, Invoice_amount_USD__c= 2000, Invoice_Amount_Local_Currency__c= 2000,            
                                                      RecordTypeId = Schema.Sobjecttype.Invoice_New__c.getRecordTypeInfosByDeveloperName().get('Supplier').getRecordTypeId());
        Insert clientInv;
        
        NCPInvoiceEmailWra wrapper = new NCPInvoiceEmailWra();
        wrapper.Accesstoken = accessTokenObj.Access_Token__c;
        wrapper.InvoiceID = clientInv.Id;
        wrapper.toEmailID = 'test@test.com';
        wrapper.ccEmailID = 'test@test.com';
        
        RestRequest req1 = new RestRequest();
        RestResponse res1 = new RestResponse();
        req1.requestURI = '/services/apexrest/InvoiceEmail'; 
        req1.httpMethod = 'POST';
        req1.requestBody = Blob.valueOf(JSON.serialize(wrapper));     
        RestContext.request = req1;
        RestContext.response = res1;
        RestContext.request.addHeader('Content-Type', 'application/json');
        
        Test.startTest(); 
        NCPInvoiceEmail.NCPInvoiceEmail();
        Test.stopTest();
    }
    
    static testMethod void testNCPInvoiceEmailSent(){
        Id shipmentOrderRecTypeId = Schema.SObjectType.shipment_order__c.getRecordTypeInfosByName().get('Cost Estimate').getRecordTypeId();
        
        CpaRules__c cpaRule = new CpaRules__c();
        cpaRule.Country__c = 'Finland';
        cpaRule.Criteria__c = 'Default';
        
        Insert cpaRule;
        
        Access_token__c accessTokenObj = new Access_token__c();
        accessTokenObj.Status__c = 'Active';
        accessTokenObj.Access_Token__c = 'asdfghj-sdfgfds-sfdfgf';
        
        Insert accessTokenObj;
        
        Account acc = new Account(Name = 'TecEx Prospective Client');
        
        Insert acc;
       Contact contact = new Contact (Client_Notifications_Choice__c = 'opt-in', email = 'test@test.com', lastname ='Testing Individual',  AccountId = acc.Id);
        
        Insert contact;
        
        IOR_Price_List__c priceList = new IOR_Price_List__c();
        priceList.Destination__c = 'Finland';
        priceList.Client_Name__c = acc.Id;
        priceList.IOR_Fee__c = 10;
        
        Insert priceList;
        
        shipment_order__c shipment = new shipment_order__c();
        shipment.RecordTypeId = shipmentOrderRecTypeId;
        shipment.Destination__c = 'Finland';
        shipment.IOR_Price_List__c = priceList.Id;
        shipment.Shipment_Value_USD__c = 600;
        shipment.Account__c = acc.Id;
        shipment.Client_Contact_for_this_Shipment__c =contact.Id ;
        shipment.Email_Estimate_To__c='abc@xyz.com';
        Insert shipment;
        Invoice_New__c clientInv = new Invoice_New__c(   Invoice_Name__c = 'Test',  Account__c = acc.id, Due_date__c = date.today(),  Invoice_Currency__c = 'US Dollar (USD)',
                                                      Shipment_Order__c = shipment.Id,  Invoice_Type__c = 'Invoice',  Invoice_Status__c = 'FC Approved', FC_Admin_Costs__c= 100, FC_Bank_Costs__c=100,
                                                      FC_Finance_Costs__c=100, FC_IOR_Costs__c=100,  FC_International_Delivery_Costs__c=100, FC_Miscellaneous_Costs__c=100, FC_Duties_and_Taxes__c=100,
                                                      FC_Customs_Clearance_Costs__c=100, FC_Customs_Brokerage_Costs__c=100, FC_Customs_Handling_Costs__c=100, FC_Customs_License_Costs__c=100, FC_Duties_and_Taxes_Other__c=100,
                                                      Actual_Admin_Costs__c=100, Actual_Bank_Costs__c=100, Actual_Customs_Brokerage_Costs__c=100, Actual_Customs_Clearance_Costs__c=100, Actual_Duties_and_Taxes__c=100,
                                                      Actual_Finance_Costs__c=100, Actual_Customs_Handling_Costs__c=100, Actual_IOREOR_Costs__c=100, Actual_International_Delivery_Costs__c=100,
                                                      Actual_Miscellaneous_Costs__c=100, Actual_Customs_License_Costs__c=100, Actual_Duties_and_Taxes_Other__c=100, Invoice_amount_USD__c= 2000, Invoice_Amount_Local_Currency__c= 2000,            
                                                      RecordTypeId = Schema.Sobjecttype.Invoice_New__c.getRecordTypeInfosByDeveloperName().get('Supplier').getRecordTypeId());
        Insert clientInv;
        
        Attachment attach = new Attachment();
        attach.parentId = clientInv.Id;
        attach.Name = 'Testing';
        attach.Body = Blob.valueOf(JSON.serialize('wrapper'));
        insert attach;
        
        NCPInvoiceEmailWra wrapper = new NCPInvoiceEmailWra();
        wrapper.Accesstoken = accessTokenObj.Access_Token__c;
        wrapper.InvoiceID = clientInv.Id;
        wrapper.toEmailID = 'test@test.com';
        wrapper.ccEmailID = 'test@test.com';
        
        RestRequest req1 = new RestRequest();
        RestResponse res1 = new RestResponse();
        req1.requestURI = '/services/apexrest/InvoiceEmail'; 
        req1.httpMethod = 'POST';
        req1.requestBody = Blob.valueOf(JSON.serialize(wrapper));     
        RestContext.request = req1;
        RestContext.response = res1;
        RestContext.request.addHeader('Content-Type', 'application/json');
        
        Test.startTest(); 
        NCPInvoiceEmail.NCPInvoiceEmail();
        Test.stopTest();
    }
    
    static testMethod void testNCPInvoiceEmailTokenExpired(){
        Id shipmentOrderRecTypeId = Schema.SObjectType.shipment_order__c.getRecordTypeInfosByName().get('Cost Estimate').getRecordTypeId();
        
        CpaRules__c cpaRule = new CpaRules__c();
        cpaRule.Country__c = 'Finland';
        cpaRule.Criteria__c = 'Default';
        
        Insert cpaRule;
        
        Access_token__c accessTokenObj = new Access_token__c();
        accessTokenObj.Status__c = 'Expired';
        accessTokenObj.Access_Token__c = 'asdfghj-sdfgfds-sfdfgf';
        
        Insert accessTokenObj;
        
        Account acc = new Account(Name = 'TecEx Prospective Client');
        
        Insert acc;
       Contact contact = new Contact (Client_Notifications_Choice__c = 'opt-in', email = 'test@test.com', lastname ='Testing Individual',  AccountId = acc.Id);
        
        Insert contact;
        
        IOR_Price_List__c priceList = new IOR_Price_List__c();
        priceList.Destination__c = 'Finland';
        priceList.Client_Name__c = acc.Id;
        priceList.IOR_Fee__c = 10;
        
        Insert priceList;
        
        shipment_order__c shipment = new shipment_order__c();
        shipment.RecordTypeId = shipmentOrderRecTypeId;
        shipment.Destination__c = 'Finland';
        shipment.IOR_Price_List__c = priceList.Id;
        shipment.Shipment_Value_USD__c = 600;
        shipment.Account__c = acc.Id;
        shipment.Client_Contact_for_this_Shipment__c =contact.Id ;
        shipment.Email_Estimate_To__c='abc@xyz.com';
        Insert shipment;
        Invoice_New__c clientInv = new Invoice_New__c(   Invoice_Name__c = 'Test',  Account__c = acc.id, Due_date__c = date.today(),  Invoice_Currency__c = 'US Dollar (USD)',
                                                      Shipment_Order__c = shipment.Id,  Invoice_Type__c = 'Invoice',  Invoice_Status__c = 'FC Approved', FC_Admin_Costs__c= 100, FC_Bank_Costs__c=100,
                                                      FC_Finance_Costs__c=100, FC_IOR_Costs__c=100,  FC_International_Delivery_Costs__c=100, FC_Miscellaneous_Costs__c=100, FC_Duties_and_Taxes__c=100,
                                                      FC_Customs_Clearance_Costs__c=100, FC_Customs_Brokerage_Costs__c=100, FC_Customs_Handling_Costs__c=100, FC_Customs_License_Costs__c=100, FC_Duties_and_Taxes_Other__c=100,
                                                      Actual_Admin_Costs__c=100, Actual_Bank_Costs__c=100, Actual_Customs_Brokerage_Costs__c=100, Actual_Customs_Clearance_Costs__c=100, Actual_Duties_and_Taxes__c=100,
                                                      Actual_Finance_Costs__c=100, Actual_Customs_Handling_Costs__c=100, Actual_IOREOR_Costs__c=100, Actual_International_Delivery_Costs__c=100,
                                                      Actual_Miscellaneous_Costs__c=100, Actual_Customs_License_Costs__c=100, Actual_Duties_and_Taxes_Other__c=100, Invoice_amount_USD__c= 2000, Invoice_Amount_Local_Currency__c= 2000,            
                                                      RecordTypeId = Schema.Sobjecttype.Invoice_New__c.getRecordTypeInfosByDeveloperName().get('Supplier').getRecordTypeId());
        Insert clientInv;
        
        NCPInvoiceEmailWra wrapper = new NCPInvoiceEmailWra();
        wrapper.Accesstoken = accessTokenObj.Access_Token__c;
        wrapper.InvoiceID = clientInv.Id;
        wrapper.toEmailID = 'test@test.com';
        wrapper.ccEmailID = 'test@test.com';
        
        RestRequest req1 = new RestRequest();
        RestResponse res1 = new RestResponse();
        req1.requestURI = '/services/apexrest/InvoiceEmail'; 
        req1.httpMethod = 'POST';
        req1.requestBody = Blob.valueOf(JSON.serialize(wrapper));     
        RestContext.request = req1;
        RestContext.response = res1;
        RestContext.request.addHeader('Content-Type', 'application/json');
        
        Test.startTest(); 
        NCPInvoiceEmail.NCPInvoiceEmail();
        Test.stopTest();
    }
}