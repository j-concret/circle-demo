@RestResource(urlMapping='/FinalDeliveryDetails/*')
global class CPFinalDeliveryDetails {
    
      @Httpget
    global static void FinalDeliveryDetails(){
        
        RestRequest req = RestContext.request;
    	RestResponse res = RestContext.response;
        res.addHeader('Content-Type', 'application/json');
        String FDID = req.params.get('FDID');
        String LoginAccount = req.params.get('LoginAccount');
        String LoginContact = req.params.get('LoginContact');
        
        
      try {  
          
          Final_Delivery__c FD = [select id,Ship_to_address__C,Shipment_Order__r.account__c,Name,Contact_name__c,Contact_email__c,Contact_number__c,Address_Line_1__c,Address_Line_2__c,City__c,Zip__c,Country__c from Final_Delivery__c where id=:FDID];
            
          If(LoginAccount==FD.Shipment_Order__r.account__c){
          		  res.responseBody = Blob.valueOf(JSON.serializePretty(FD));
        		  res.statusCode = 200;
                
                API_Log__c Al = New API_Log__c();
                Al.Account__c=LoginAccount;
                Al.Login_Contact__c=LoginContact;
                Al.EndpointURLName__c='FinalDeliveryDetails';
                Al.Response__c='Success - FinalDelivery Details are sent';
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al;
              
          }
          Else{
                
              string ErrorString = 'Failed - AccountID and loginaccountid not same';
              res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
        	  res.statusCode = 400;
                
                API_Log__c Al = New API_Log__c();
                Al.Account__c=LoginAccount;
                Al.Login_Contact__c=LoginContact;
                Al.EndpointURLName__c='FinalDeliveryDetails';
                Al.Response__c='Failed - AccountID and loginaccountid not same';
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al;
              
              
          }
          
          
      
      
      }
        catch(Exception e){
            
           		 String ErrorString ='Something went wrong while getting final delivery details, please contact support_SF@tecex.com';
               	res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
            res.statusCode = 500;
                 API_Log__c Al = New API_Log__c();
                Al.Account__c=LoginAccount;
                Al.Login_Contact__c=LoginContact;
                Al.EndpointURLName__c='FinalDeliveryDetails';
                Al.Response__c='Exception - FinalDelivery Details are not sent';
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al;
            
            
        }
        
    }

}