public class SOTravelNotificationHelper {
    public static void sendEmailNotification(Map<String,String> SoMap){
        //Set<String> shipmentIds

        Messaging.SingleEmailMessage[] messages = new List<Messaging.SingleEmailMessage>();
        List<String> sourceList = new List<String>();
        Set<String> shipmentIds = new Set<String>();
        Id CPrecordTypeId = Schema.SObjectType.Shipment_Order__c.getRecordTypeInfosByDeveloperName().get('Cost_Estimate').getRecordTypeId();

        List<SO_Travel_History__c> SOTHList = [SELECT Id, Name,Task_State__c,Type_of_Update__c, Client_NCPAccess__c,TecEx_SO__c,TecEx_SO__r.Id,Tecex_Assigned_to_Under_Review__c,is_Notification_Sent__c,Source__c,TecEx_SO__r.Name, TecEx_SO__r.Client_Contact_for_this_Shipment__c,TecEx_SO__r.Client_Contact_for_this_Shipment__r.AccountId, TecEx_SO__r.Client_Contact_for_this_Shipment__r.Email,TecEx_SO__r.IOR_CSE__c, TecEx_SO__r.ICE__c,TecEx_SO__r.Financial_Controller__c,TecEx_SO__r.Compliance_Team__c,TecEx_SO__r.Service_Manager__c,TecEx_SO__r.Vat_Team__c FROM SO_Travel_History__c WHERE Id IN :SoMap.keySet() AND TecEx_SO__r.recordtypeid !=: CPrecordTypeId AND (TecEx_SO__r.shipping_status__c NOT IN ('Cost Estimate Rejected', 'Cancelled - with Fees/costs','Cancelled - no Fees/costs') OR (TecEx_SO__r.shipping_status__c IN ('Cost Estimate Rejected','Cancelled - with Fees/costs','Cancelled - no Fees/costs') AND Source__c = 'Manual Status Change'))]; //'POD Received','Customs Clearance Docs Received'

        List<SO_Travel_History__c> SOTHList1 = new List<SO_Travel_History__c>();


        Map<String,EmailTemplate> emailTemMap = new Map<String,EmailTemplate>();
        Map<String,String> SourceShips = new Map<String,String>();
        for(SO_Travel_History__c soLog: SOTHList) {
            if(soLog.Source__c !='SO Creation' && soLog.Source__c != 'Checkpoints') {
                sourceList.add(soLog.Source__c);
                shipmentIds.add(soLog.TecEx_SO__c);
                SourceShips.put(soLog.TecEx_SO__c,soLog.Source__c);
            }
        }

        Map<String,List<String> > ContactsWithShipment = new Map<String,List<String> >();
        Map<String,List<String> > ContactsWithOptedOut= new Map<String,List<String> >();
        System.debug('shipmentIds --> '+shipmentIds);
        for(Notify_Parties__c notifyParty:[SELECT Id,Shipment_Order__c,Contact__r.Client_Notifications_Choice__c,Contact__c,Contact__r.Email,Contact__r.Client_Tasks_notification_choice__c FROM Notify_Parties__c Where Shipment_Order__c IN :shipmentIds]) {// Contact__r.Include_in_SO_Communication__c = true AND

            System.debug('notifyParty --> '+notifyParty);
            List<String> contactEmails = new List<String>();
            List<String> optedContactEmails = new List<String>();
            String Source = SourceShips.get(notifyParty.Shipment_Order__c);
            if(Source != null) {
                if(Source == 'Task (internal)' || (Source == 'Tasks (client)' && !notifyParty.Contact__r.Client_Tasks_notification_choice__c)) {

                }else{
                    if(notifyParty.Contact__r.Client_Notifications_Choice__c != 'Opt-Out') {
                        if(ContactsWithShipment.containsKey(notifyParty.Shipment_Order__c)) {
                            contactEmails = ContactsWithShipment.get(notifyParty.Shipment_Order__c);
                        }

                        if(notifyParty.Contact__r.Email != null)
                            contactEmails.add(notifyParty.Contact__r.Email);



                        ContactsWithShipment.put(notifyParty.Shipment_Order__c,contactEmails);
                    }else{
                        if(ContactsWithOptedOut.containsKey(notifyParty.Shipment_Order__c)) {
                            optedContactEmails = ContactsWithOptedOut.get(notifyParty.Shipment_Order__c);
                        }

                        if(notifyParty.Contact__r.Email != null)
                            optedContactEmails.add(notifyParty.Contact__r.Email);



                        ContactsWithOptedOut.put(notifyParty.Shipment_Order__c,optedContactEmails);
                    }
                }
            }
        }

        for(EmailTemplate emailTemplate : [Select Id,name,Subject,Description,HtmlValue,DeveloperName,Body from EmailTemplate where name in :sourceList]) {
            emailTemMap.put(emailTemplate.name,emailTemplate);
        }


        OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where Address = 'app@tecex.com'];
        for(SO_Travel_History__c soTravel : SOTHList) {
            if(soTravel.Source__c !='SO Creation' && soTravel.Source__c != 'Checkpoints' && soTravel.Source__c !='Delivered Status Update') {
                EmailTemplate emailTemplate = emailTemMap.get(soTravel.Source__c);
                Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
                //message.setTargetObjectId(shipmentOrder.Id);
                if(soTravel.TecEx_SO__r.Client_Contact_for_this_Shipment__c != null) {
                    message.setTargetObjectId(soTravel.TecEx_SO__r.Client_Contact_for_this_Shipment__c);
                }
                //contactId'0031q00000YGZD8AAP'
                soTravel.is_Notification_Sent__c = true;
                SOTHList1.add(soTravel);
                //message.setSenderDisplayName('TecEx App');
                if ( owea.size() > 0 ) {
                    message.setOrgWideEmailAddressId(owea.get(0).Id);
                }
                message.setReplyTo('noreplyapp@tecex.com');
                message.setUseSignature(false);
                message.setBccSender(false);
                message.setSaveAsActivity(false);
                message.setTreatTargetObjectAsRecipient(false);
                message.setTemplateID(emailTemplate.Id);
                message.setWhatId(soTravel.Id);
                List<String> toAddress = new List<String>();
                List<String> ccAddress = new List<String>();
                if( (soTravel.Source__c == 'Task (internal)' || soTravel.Source__c == 'Tasks (client)')
                    && soTravel.Tecex_Assigned_to_Under_Review__c != null && soTravel.Task_State__c  == 'Under Review' ) {
                    switch on soTravel.Tecex_Assigned_to_Under_Review__c {
                        when 'AM' {
                            if(soTravel.TecEx_SO__r.IOR_CSE__c !=null ) {
                                toAddress.add(soTravel.TecEx_SO__r.IOR_CSE__c);
                            }
                        }
                        when 'ICE' {
                            if(soTravel.TecEx_SO__r.ICE__c != null) {
                                toAddress.add(soTravel.TecEx_SO__r.ICE__c);
                            }
                        }
                        when 'FC' {
                            if(soTravel.TecEx_SO__r.Financial_Controller__c != null) {
                                toAddress.add(soTravel.TecEx_SO__r.Financial_Controller__c);
                            }
                        }
                        when 'Compliance' {
                            if(soTravel.TecEx_SO__r.Compliance_Team__c != null) {
                                toAddress.add(soTravel.TecEx_SO__r.Compliance_Team__c);
                            }
                        }
                        when 'ISM' {
                            if(soTravel.TecEx_SO__r.Service_Manager__c != null) {
                                toAddress.add(soTravel.TecEx_SO__r.Service_Manager__c);
                            }
                        }
                        when 'VAT' {
                            if(soTravel.TecEx_SO__r.Vat_Team__c != null) {
                                toAddress.add(soTravel.TecEx_SO__r.Vat_Team__c);
                            }
                        }
                    }
                    System.debug('Inside '+toAddress);
                }
                else if(SoMap.get(soTravel.Id) == 'all') {
                    if( soTravel.TecEx_SO__r.Client_Contact_for_this_Shipment__r.Email != null && soTravel.Client_NCPAccess__c ) {
                        toAddress.add(soTravel.TecEx_SO__r.Client_Contact_for_this_Shipment__r.Email);
                    }
                    if(!soTravel.Client_NCPAccess__c) {
                        if(soTravel.TecEx_SO__r.IOR_CSE__c !=null ) {
                            toAddress.add(soTravel.TecEx_SO__r.IOR_CSE__c);
                        }
                        if(soTravel.TecEx_SO__r.ICE__c !=null) {
                            ccAddress.add(soTravel.TecEx_SO__r.ICE__c);
                        }
                    }else{
                        if(soTravel.TecEx_SO__r.IOR_CSE__c !=null ) {
                            ccAddress.add(soTravel.TecEx_SO__r.IOR_CSE__c);
                        }
                        if(soTravel.TecEx_SO__r.ICE__c !=null) {
                            ccAddress.add(soTravel.TecEx_SO__r.ICE__c);
                        }
                    }
                }else{
                    if(soTravel.TecEx_SO__r.IOR_CSE__c !=null ) {
                        toAddress.add(soTravel.TecEx_SO__r.IOR_CSE__c);
                    }
                    if(soTravel.TecEx_SO__r.ICE__c !=null) {
                        ccAddress.add(soTravel.TecEx_SO__r.ICE__c);
                    }
                }
                if(ContactsWithShipment.get(soTravel.TecEx_SO__c) != null) {


                    List<String> ccAddressFromNotifyParties = ContactsWithShipment.get(soTravel.TecEx_SO__c);
                    ccAddress.addAll(ccAddressFromNotifyParties);
                }

                if(ContactsWithOptedOut.get(soTravel.TecEx_SO__c) != null && soTravel.Type_of_Update__c == 'MAJOR') {
                    //optedContactEmails
                    List<String> ccAddressFromNotifyParties = ContactsWithOptedOut.get(soTravel.TecEx_SO__c);
                    ccAddress.addAll(ccAddressFromNotifyParties);
                }
                // ccAddress.add('luhar.aadil@concret.io');
                //ccAddress.add('anilk@tecex.com');
                message.toAddresses = toAddress;
                message.ccaddresses = ccAddress;

                if(soTravel.TecEx_SO__r.Client_Contact_for_this_Shipment__c != null) {
                    messages.add(message);
                }
            }
        }

        if(messages != null && !messages.isEmpty()) {


            Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);

            if (results[0].success)
            {
                System.debug('The email was sent successfully.');

                if(SOTHList1 != null && !SOTHList1.isEmpty()) {
                    update SOTHList1;
                }

            } else {
                System.debug('The email failed to send: ' +  results[0].errors[0].message);
            }
        }
    }
}