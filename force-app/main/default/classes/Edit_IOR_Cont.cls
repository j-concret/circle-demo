public class Edit_IOR_Cont {
    public List <IOR_Price_List__c> currentPriceList {get;set;}
    private String accountID = ApexPages.currentPage().getParameters().get('id');
    
    public Edit_IOR_Cont(){
        System.debug(accountID);
        if(accountID != '' && accountID != null){
            currentPriceList = [SELECT Id, Name, Estimate_Transit_time__c, Estimate_Customs_Clearance_Time__c, IOR_Fee__c, Shipping_Notes__c, On_Charge_Mark_up__c, TecEx_Shipping_Fee_Markup__c, Client_Name__c, IOR_Min_Fee__c, Tax_Rate__c, IOR_Fee_as_of__c, Estimated_Customs_Brokerage__c, Estimated_Customs_Clearance__c, Estimated_Import_License__c, Estimated_Customs_Handling__c, Admin_Fee__c, Set_up_fee__c, Bank_Fees__c FROM IOR_Price_List__c where Client_Name__c =:accountId];
        }    
    }
    
    public PageReference save(){
        update currentPriceList;
        
        return new pagereference('/'+accountID);
    }
    
    public PageReference plsCancel(){
        return new pagereference('/'+accountID);
    }
}