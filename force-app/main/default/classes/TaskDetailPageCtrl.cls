public class TaskDetailPageCtrl {
    
    @AuraEnabled
    public static Map<String,Object> getTaskRecord(String taskId,Boolean clientView){
        try {
            String displayTo = clientView ? 'Client' : 'TecEx';
            Map<String,String> accountMap;
            Freight__c freightReq;
            
            Task tsk = [SELECT Id,Manually_Override_Inactive_Field__c,ETA_Countdown__c ,Critical_Path_Task__c,whoId,Description,Assigned_to__c,Assigned_to__r.Name,LastModifiedById,CreatedById,Client_Assigned_To__c,Client_Assigned_To__r.Name,TecEx_Assigned_To__c,TecEx_Pending_ETA_days__c,Under_Review_ETA_days__c,Task_Category__c,Shipment_Order__c,Shipment_Order__r.Name,Shipment_Order__r.Account__c,Shipment_Order__r.Account__r.Name,State__c,WhatId,Master_Task__c,Master_Task__r.Name,Master_Task__r.Set_Default_to_Inactive__c,Master_Task__r.States__c, Subject,Inactive__c,Blocks_Approval_to_Ship__c,Binding_Quote_Exempted_V2__c,CreatedDate,CreatedBy.Name, LastModifiedDate,LastModifiedBy.Name,Reason_Not_Applicable__c,Auto_CI_NotApplicableReasons__c,IsNotApplicable__c FROM Task WHERE Id=:taskId];

            String state = tsk.State__c;
            String mstId = tsk.Master_Task__c;
            Id WhatId = tsk.WhatId;
            
            Schema.sObjectType objType = tsk.WhatId.getSObjectType();
            String objName = objType.getDescribe().getName();
            
            //String soId = tsk.Shipment_Order__c;
            
            List<String> states = tsk.Master_Task__r.States__c.split(';');
            List<String> bindingOptions = getPicklistValues('Task', 'Binding_Quote_Exempted_V2__c');
            List<String> notApplicableReasonOptions = getPicklistValues('Task', 'Auto_CI_NotApplicableReasons__c');
            
            String query = 'SELECT '+String.join(new List<String>(objType.getDescribe().fields.getMap().keySet()),',');
            
            if(objName.equalsIgnoreCase('Shipment_Order__c')) {
                query += ',Account__r.'+String.join(new List<String>(Account.sObjectType.getDescribe().fields.getMap().keySet()), ',Account__r.');
                query += ',CPA_v2_0__r.'+String.join(new List<String>(CPA_v2_0__c.sObjectType.getDescribe().fields.getMap().keySet()), ',CPA_v2_0__r.');
                
                String freightQuery = 'SELECT '+ String.join(new List<String>(Freight__c.sObjectType.getDescribe().fields.getMap().keySet()),',');
                freightQuery += ' FROM Freight__c WHERE shipment_order__c =: WhatId';
                
                List<Freight__c> freightReqs = Database.query(freightQuery);
                freightReq = !freightReqs.isEmpty() ? freightReqs[0] : null;
            }
            query += ' FROM '+objName+' WHERE Id =:WhatId';
            
            SObject whatObj = Database.query(query);
            
            if(objName.equalsIgnoreCase('Account')) {
                accountMap = new Map<String,String>();
                accountMap.put('Id',String.valueOf(whatObj.get('Id')));
                accountMap.put('Name',String.valueOf(whatObj.get('Name')));
            }
            
            
            Set<Id> masterTaskPrerequisiteIds = new Set<Id>();
            for(Master_Task_Prerequisite__c mstPre : [SELECT id,Name, Master_Task_Dependent__c, Master_Task_Prerequisite__c FROM Master_Task_Prerequisite__c WHERE Master_Task_Dependent__c =: mstId]) {
                masterTaskPrerequisiteIds.add(mstPre.Master_Task_Prerequisite__c);
            }
            
            Integer unresolvedTasks = Database.countQuery('SELECT count() FROM Task WHERE  Master_Task__c IN:masterTaskPrerequisiteIds AND WhatID =:WhatId AND State__c !=\'Resolved\' AND State__c !=\'Not Applicable\' AND IsNotApplicable__c = false');
            
            if(!tsk.Manually_Override_Inactive_Field__c) {
                tsk.Inactive__c = tsk.Master_Task__r.Set_Default_to_Inactive__c ? tsk.Master_Task__r.Set_Default_to_Inactive__c : unresolvedTasks > 0;
            }
            
            TaskWrapper tskWrapper = new TaskWrapper();
            
            Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
            for(Master_Task_Template__c temp :Database.query('SELECT '+String.join(new List<String>(Master_Task_Template__c.sObjectType.getDescribe().fields.getMap().keySet()),',')+' FROM Master_Task_Template__c WHERE Displayed_to__c=:displayTo AND Master_Task__c =: mstId AND State__c=:state ORDER BY Building_Block_Number__c')) {
                
                tskWrapper.taskTitle = temp.Task_Title__c;
                if(String.isBlank(tskWrapper.taskDescription) && String.isNotBlank(temp.Task_Description__c)) {
                    List<String> fieldValues = new List<String>();
                    if(String.isNotBlank(temp.Task_Description_Fields__c)) {
                        for(String field : temp.Task_Description_Fields__c.split(',')) {
                            Object fieldValue;
                            if(field.startsWithIgnoreCase('task')) {
                                fieldValue = getFieldValue(tsk,field);
                            }
                            else if(freightReq !=null && field.startsWithIgnoreCase('Freight__c')) {
                                fieldValue = getFieldValue(freightReq,field);
                            }
                            else{
                                fieldValue = getFieldValue(whatObj,field);
                            }
                            fieldValues.add(fieldValue == null ? '' : String.valueOf(fieldValue));
                        }
                    }
                    temp.Task_Description__c = temp.Task_Description__c.replaceAll('\'','\'\'');
                    tskWrapper.taskDescription = String.format(temp.Task_Description__c,fieldValues);
                }
                
                tskWrapper.btnLabel = temp.Complete_Button_Text__c;
                if(temp.Complete_Button_Action__c == 'Yes' && String.isNotBlank(temp.Complete_Button_Target_Object_API_Name__c) && String.isNotBlank(temp.Complete_Button_Target_Field_API_Name__c) && String.isNotBlank(temp.Complete_Button_Target_Field_Set_Value__c) && schemaMap.containsKey(temp.Complete_Button_Target_Object_API_Name__c.toLowerCase())) {
                    tskWrapper.btnAction = temp.Complete_Button_Action__c;
                    tskWrapper.btnObject = schemaMap.get(temp.Complete_Button_Target_Object_API_Name__c).getDescribe().getName();
                    Schema.SObjectType ObjSchema = schemaMap.get(tskWrapper.btnObject);
                    Map<String, Schema.SObjectField> fieldsNameMap = ObjSchema.getDescribe().fields.getMap();
                    
                    tskWrapper.btnField = fieldsNameMap.get(temp.Complete_Button_Target_Field_API_Name__c).getDescribe().getName();
                    Schema.DisplayType FldType = fieldsNameMap.get(temp.Complete_Button_Target_Field_API_Name__c).getDescribe().getType();
                    tskWrapper.btnFieldType = FldType.name();
                    tskWrapper.btnValue = temp.Complete_Button_Target_Field_Set_Value__c;
                }
                
                Block b = new Block();
                b.blockType = temp.Building_Block_Type__c;
                b.documentAppendedText = temp.Document_Appended_Text__c;
                b.displayNumber = Integer.valueOf(temp.Display_Number__c);
                
                if(String.isNotBlank(temp.Description__c)) {
                    List<String> fieldValues = new List<String>();
                    if(String.isNotBlank(temp.Description_Fields__c)) {
                        for(String field : temp.Description_Fields__c.split(',')) {
                            Object fieldValue;
                            if(field.startsWithIgnoreCase('task')) {
                                fieldValue = getFieldValue(tsk,field);
                            }
                            else if(freightReq !=null && field.startsWithIgnoreCase('Freight__c')) {
                                fieldValue = getFieldValue(freightReq,field);
                            }
                            else{
                                fieldValue = getFieldValue(whatObj,field);
                            }
                            fieldValues.add(fieldValue == null ? '' : String.valueOf(fieldValue));
                        }
                    }
                    temp.Description__c = temp.Description__c.replaceAll('\'','\'\'');
                    b.blockDescription = String.format(temp.Description__c,fieldValues);
                }
                
                if(temp.Building_Block_Type__c == 'Instruction') {
                    tskWrapper.blocks.add(b);
                }else if(temp.Building_Block_Type__c == 'Input' && String.isNotBlank(temp.Target_Field_API_Name__c) && String.isNotBlank(temp.Target_Object_API_Name__c)) {
                    
                    Map<String,String> customOpts = getCustomOptions(temp);
                    if(temp.Target_Object_API_Name__c.equalsIgnoreCase('Task'))
                        b.sourceId = String.valueOf(getFieldValue(tsk,temp.Target_Object_API_Name__c+'.Id'));
                    else if(temp.Target_Object_API_Name__c.equalsIgnoreCase('Freight__c'))
                        b.sourceId = String.valueOf(getFieldValue(freightReq,temp.Target_Object_API_Name__c+'.Id'));
                    else
                        b.sourceId = String.valueOf(getFieldValue(whatObj,temp.Target_Object_API_Name__c+'.Id'));
                    
                    b.inputHeading = temp.Input_Heading__c;
                    b.inputHint = temp.Input_Hint__c;
                    b.isRequired = temp.Input_Required__c;
                    
                    Schema.SObjectType ObjSchema = schemaMap.get(temp.Target_Object_API_Name__c);
                    Map<String, Schema.SObjectField> fieldMap = ObjSchema.getDescribe().fields.getMap();
                    
                    if(fieldMap.containsKey(temp.Target_Field_API_Name__c)) {
                        b.sfObject = ObjSchema.getDescribe().getName();
                        Schema.SObjectField field = fieldMap.get(temp.Target_Field_API_Name__c);
                        b.sfFieldName = field.getDescribe().getName();
                        Schema.DisplayType FldType = field.getDescribe().getType();
                        b.sfFieldType = FldType.name();
                        b.scale = field.getDescribe().getScale();
                        if(Schema.DisplayType.BOOLEAN == FldType) {
                            b.inputType = 'radio';
                            if(customOpts.containsKey('true') && customOpts.containsKey('false')) {
                                b.options = new List<Map<String,String> > {
                                    new Map<String,String> {'label'=>customOpts.get('true'),'value'=>'TRUE'},
                                        new Map<String,String> {'label'=>customOpts.get('false'),'value'=>'FALSE'}
                                };
                                    }else{
                                        b.options = new List<Map<String,String> > {
                                            new Map<String,String> {'label'=>'TRUE','value'=>'TRUE'},
                                                new Map<String,String> {'label'=>'FALSE','value'=>'FALSE'}
                                        };
                                            }
                        }
                        else if(Schema.DisplayType.PICKLIST == FldType || Schema.DisplayType.MULTIPICKLIST == FldType) {
                            b.multiSelect = Schema.DisplayType.MULTIPICKLIST == FldType;
                            b.inputType = 'select';
                            b.options = new List<Map<String,String> >();
                            for (Schema.PicklistEntry entry : field.getDescribe().getPicklistValues()) {
                                if (entry.isActive()) {
                                    b.options.add(new Map<String,String> {'label'=>entry.getLabel(),'value'=>entry.getValue()});
                                }
                            }
                            if(!b.multiSelect && b.options.size()<=3) {
                                b.inputType = 'radio';
                                for(Map<String,String> opt : b.options) {
                                    if(customOpts.containsKey(opt.get('value').toLowerCase())) {
                                        opt.put(
                                            'label',customOpts.get(opt.get('value').toLowerCase())
                                        );
                                    }
                                }
                            }
                        }
                        else if(Schema.DisplayType.DATE == FldType || Schema.DisplayType.TIME == FldType|| Schema.DisplayType.DATETIME == FldType || Schema.DisplayType.STRING == FldType || Schema.DisplayType.EMAIL == FldType || Schema.DisplayType.TEXTAREA == FldType) {
                            b.inputType = FldType.name().toLowerCase();
                        }
                        else if(Schema.DisplayType.DOUBLE == FldType || Schema.DisplayType.INTEGER == FldType || Schema.DisplayType.LONG == FldType) {
                            b.inputType = 'number';
                        }
                    }
                    tskWrapper.blocks.add(b);
                }else if(temp.Building_Block_Type__c == 'Component') {
                    b.cmpName = temp.Referring_Component__c;
                    if(String.isNotBlank(temp.Target_Object_API_Name__c) && String.isNotBlank(temp.Target_Field_API_Name__c)) {
                        if(temp.Target_Object_API_Name__c.equalsIgnoreCase('Master_Task__c'))
                            b.sourceId = tsk.Master_Task__c;
                        else if(temp.Target_Object_API_Name__c.equalsIgnoreCase('Task'))
                            b.sourceId = String.valueOf(getFieldValue(tsk,temp.Target_Object_API_Name__c+'.'+temp.Target_Field_API_Name__c));
                        else if(temp.Target_Object_API_Name__c.equalsIgnoreCase('Freight__c'))
                            b.sourceId = String.valueOf(getFieldValue(freightReq,temp.Target_Object_API_Name__c+'.'+temp.Target_Field_API_Name__c));
                        else
                            b.sourceId = String.valueOf(getFieldValue(whatObj,temp.Target_Object_API_Name__c+'.'+temp.Target_Field_API_Name__c));
                        
                        if(String.isNotBlank(temp.Document_Name__c) ) {
                            String str = temp.Document_Name__c;
                            String regex = '[^{\\}]+(?=})';
                            Matcher m = Pattern.compile(regex).matcher(str);
                            Integer i = 0;
                            List<String> vals = new List<String>();
                            while(m.find()) {
                                String field = String.valueOf(m.group(0));
                                String val;
                                if(field.equalsIgnoreCase('Task'))
                                    val = String.valueOf(getFieldValue(tsk,field));
                                else
                                    val = String.valueOf(getFieldValue(whatObj,field));
                                
                                vals.add(val);
                                str = str.replace(field,String.valueOf(i));
                                i++;
                            }
                            b.docName = String.format(str,vals);
                        }
                        
                        if(b.cmpName == 'Download Document') {
                            String doc = '%'+b.docName+'%';
                            List<Id> cntDocs = new List<Id>();
                            
                            for(ContentDocumentLink linkDoc : [SELECT Id, LinkedEntityId, ContentDocumentId, IsDeleted, SystemModstamp, ShareType, Visibility FROM ContentDocumentLink where LinkedEntityId =:b.sourceId]) {
                                cntDocs.add(linkDoc.ContentDocumentId);
                            }
                            // Maxmium we can have 5 Master task template record so we can query inside loop
                            for(ContentVersion cnVersion : [SELECT IsLatest, Id, Title, ContentDocumentId FROM ContentVersion where Title like: doc AND IsLatest=true AND ContentDocumentId IN:cntDocs]) {
                                
                                if(b.docName != null) {
                                    b.docName = 'AllDocs';
                                }else{
                                    b.docName = cnVersion.Title;
                                }
                                if(b.url == null)
                                    b.url='/sfc/servlet.shepherd/document/download/'+cnVersion.ContentDocumentId;
                                else
                                    b.url += '/'+cnVersion.ContentDocumentId;
                            }
                        }
                    }
                    tskWrapper.blocks.add(b);
                }
            }
            return new Map<String,Object> {'freight'=>freightReq,'account'=>accountMap,'task'=>tsk,'states'=>states,'template'=>tskWrapper,'bindingOptions'=>bindingOptions,'notApplicableReasonOptions'=>notApplicableReasonOptions};
                } catch (Exception e) {
                    
                    throw new AuraHandledException(e.getMessage());
                }
    }
    
    private static List<String> getPicklistValues(String object_name, String field_name) {
        List<String> values = new List<String>();
        String[] types = new String[] {object_name};
            Schema.DescribeSobjectResult[] results = Schema.describeSObjects(types);
        for(Schema.DescribeSobjectResult res : results) {
            for (Schema.PicklistEntry entry : res.fields.getMap().get(field_name).getDescribe().getPicklistValues()) {
                if (entry.isActive()) {
                    values.add(entry.getValue());
                }
            }
        }
        return values;
    }
    
    @AuraEnabled
    public static void updateSfRecord(String taskId,String data){
        try {
            
            Task tsk = [SELECT Id,State__c,WhatId,Master_Task__c, Subject FROM Task WHERE Id=:taskId];
            Map<String,String> fieldsValue;
            
            Schema.sObjectType objType = tsk.WhatId.getSObjectType();
            String objName = objType.getDescribe().getName();
            
            //soject to fieldMapping, to fetch id of record by using specified key name from "fieldsValue" Map
            Map<String,String> objectIdFields = new Map<String,String> {'Shipment_Order__c'=>'Id','Account'=>'Id','Account__c'=>'Account__c','CPA_v2_0__c'=>'CPA_v2_0__c','Task'=>'taskId','Freight__c'=>'freightId'};
                
                if(objName.equalsIgnoreCase('Shipment_Order__c')) {
                    Shipment_Order__c shipmentOrder = [SELECT Id,Account__c,CPA_v2_0__c FROM Shipment_Order__c WHERE Id=:tsk.WhatId];
                    String freightId = [SELECT Id FROM Freight__c WHERE Shipment_Order__c=:shipmentOrder.Id limit 1] ?.Id;
                    
                    // field mapping with actual record Id
                    fieldsValue = new Map<String,String> {'Id'=>shipmentOrder.Id,'Account__c'=>shipmentOrder.Account__c,'CPA_v2_0__c'=>shipmentOrder.CPA_v2_0__c,'taskId'=>tsk.Id,'freightId'=>freightId};
                        }else{
                            fieldsValue = new Map<String,String> {'taskId'=>tsk.Id};
                                if(objectIdFields.containsKey(objName))
                                fieldsValue.put(objectIdFields.get(objName),tsk.WhatId);
                        }
            
            TaskWrapper tskWrapper = (TaskWrapper)JSON.deserialize(data, TaskWrapper.class);
            Map<Id,SObject> updateObjs = new Map<Id,SObject>();
            List<Sobject> tasksUpdate = new List<SObject>();
            Boolean isSOUpdate = false;
            for(Block blk : tskWrapper.blocks) {
                if(blk.blockType == 'Input') {
                    SObject obj;
                    if(String.isNotBlank(blk.sourceId) && updateObjs.containsKey(blk.sourceId))
                        obj = updateObjs.get(blk.sourceId);
                    else{
                        Type t = Type.forName(blk.sfObject);
                        obj = (SObject)t.newInstance();
                        obj.put('Id',blk.sourceId);
                    }
                    if(!isSOUpdate && blk.sfObject.equalsIgnoreCase('Shipment_Order__c'))
                        isSOUpdate = true;
                    
                    convertValue(blk.sfFieldType, obj, blk.sfFieldName, blk.value);
                    updateObjs.put(blk.sourceId,obj);
                }
            }
            
            if(tskWrapper.btnAction == 'Yes' && fieldsValue != null && objectIdFields.containsKey(tskWrapper.btnObject) && fieldsValue.get(objectIdFields.get(tskWrapper.btnObject)) != null) {
                Type t = Type.forName(tskWrapper.btnObject);
                SObject obj = (SObject)t.newInstance();
                String tskId = fieldsValue.get(objectIdFields.get(tskWrapper.btnObject));
                obj.put('Id',tskId);
                convertValue(tskWrapper.btnFieldType, obj, tskWrapper.btnField, tskWrapper.btnValue);
                //obj.put(tskWrapper.btnField,tskWrapper.btnValue);
                
                if(tskWrapper.btnObject.equalsIgnoreCase('task')) tasksUpdate.add(obj);
                else
                    updateObjs.put(tskId,obj);
            }
            
            
            
            if(!updateObjs.isEmpty()) {
                if(isSOUpdate && !tasksUpdate.isEmpty()) RecusrsionHandler.tasksCreated = true;
               
                update updateObjs.values();
            }
            
            if(!tasksUpdate.isEmpty()) {
                RecusrsionHandler.tasksCreated = false;
                update tasksUpdate;
            }
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }
    
    private static void convertValue(String fieldType,SObject updateObj,String key,String val){
        if(fieldType == 'currency' || fieldType == 'double' || fieldType == 'percent' || fieldType == 'decimal') {
            updateObj.put(key, decimal.valueOf(val));
        }
        else if(fieldType == 'boolean') {
            updateObj.put(key, Boolean.valueOf(val));
        }
        else if(fieldType == 'date') {
            
            updateObj.put(key, date.valueOf(val));
        }
        else if(fieldType == 'datetime') {
            //updateObj.put(key, Datetime.valueOf(val.replace('T',' ')));
            //Adding these lines to get the local time from GMT
            Datetime z = Datetime.valueOf(val.replace('T',' '));
            if(!UserInfo.getUserId().contains('0051v000005cUgI')){
                Integer offset = UserInfo.getTimezone().getOffset(z);
                Datetime local = z.addSeconds(offset/1000);
                updateObj.put(key,local);
            }else{
                updateObj.put(key,z);
            }
        }
        else{
            updateObj.put(key,val);
        }
    }
    
    @AuraEnabled
    public static List<Task> getPrerequisiteTasks(Id taskId){
        try{
            Task tsk = [SELECT Id,WhatID,Master_Task__c FROM Task WHERE Id=:taskId];
            
            Set<Id> masterTaskPrerequisiteIds = new Set<Id>();
            for(Master_Task_Prerequisite__c mstPre : [SELECT id,Name, Master_Task_Dependent__c, Master_Task_Prerequisite__c FROM Master_Task_Prerequisite__c WHERE Master_Task_Dependent__c =: tsk.Master_Task__c]) {
                masterTaskPrerequisiteIds.add(mstPre.Master_Task_Prerequisite__c);
            }
            
            return [SELECT Id,Subject,State__c FROM Task WHERE Master_Task__c IN : masterTaskPrerequisiteIds AND WhatID =: tsk.WhatId AND State__c != 'Resolved' AND State__c !='Not Applicable' AND IsNotApplicable__c = false];
        }catch(Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }
    
    @AuraEnabled
    public static Task updateTask(Task task){
        try {
            update task;
            return [SELECT Id,Description,Assigned_to__c,LastModifiedById,CreatedById,Client_Assigned_To__c,Client_Assigned_To__r.Name,TecEx_Assigned_To__c,TecEx_Pending_ETA_days__c,Under_Review_ETA_days__c,Task_Category__c,Shipment_Order__c,Shipment_Order__r.Name,Shipment_Order__r.Account__c,Shipment_Order__r.Account__r.Name,State__c,WhatID,Master_Task__c,Master_Task__r.Name,Master_Task__r.Set_Default_to_Inactive__c, Subject,Inactive__c,Blocks_Approval_to_Ship__c,Binding_Quote_Exempted_V2__c,CreatedDate,CreatedBy.Name, LastModifiedDate,LastModifiedBy.Name,Reason_Not_Applicable__c,IsNotApplicable__c,Auto_CI_NotApplicableReasons__c FROM Task WHERE Id=: task.Id];
        }catch (DmlException ex) {
            throw new AurahandledException(ex.getDmlMessage(0));
        }
        catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }
    private static Map<String,String> getCustomOptions(Master_Task_Template__c temp){
        Map<String,String> alternateVals = new Map<String,String>();
        if(String.isNotBlank(temp.Option_1_Label__c) && String.isNotBlank(temp.Option_1_SF_value__c))
            alternateVals.put(temp.Option_1_SF_value__c.toLowerCase(),temp.Option_1_Label__c);
        
        if(String.isNotBlank(temp.Option_2_Label__c) && String.isNotBlank(temp.Option_2_SF_value__c))
            alternateVals.put(temp.Option_2_SF_value__c.toLowerCase(),temp.Option_2_Label__c);
        
        if(String.isNotBlank(temp.Option_3_Label__c) && String.isNotBlank(temp.Option_3_SF_value__c))
            alternateVals.put(temp.Option_3_SF_value__c.toLowerCase(),temp.Option_3_Label__c);
        
        return alternateVals;
    }
    
    private static Object getFieldValue(Sobject sobj,String fieldName){
        
       
        
        Map<String, Schema.SObjectField> fieldMap = sobj.getSObjectType().getDescribe().fields.getMap();
        
       
        
        
        List<String> field = fieldName.split('\\.');
        if(sobj == null || field.size() <2) return '';
        String objName = sobj.getSObjectType().getDescribe().getName();
        Schema.SObjectField field1 = fieldMap.get(field[1].trim().toLowerCase());
        
        Schema.DisplayType FldType ;
        if(field1 != null){
		FldType = field1.getDescribe().getType();
        }
        
        Map<String,String> relatedObjects = new Map<String,String> {'account'=>'Account__r','cpa_v2_0__c'=>'CPA_v2_0__r'};//keys in lowercase
            try{
                if(sobj.isSet(field[1].trim().toLowerCase()) && objName.equalsIgnoreCase(field[0].trim())) {
                    if(FldType != null && Schema.DisplayType.DATETIME == FldType) {
                          
                        String objType = Datetime.valueOf(sobj.get(field[1].trim())).format(); 
                        
                        return objType;
                    }
                    return sobj.get(field[1].trim());
                }
            }catch(Exception e) {}
        
        if(objName == 'Shipment_Order__c' && relatedObjects.containsKey(field[0].trim().toLowerCase())) {
            SObject obj = sobj.getSObject(relatedObjects.get(field[0].trim().toLowerCase()));
            if(FldType != null && Schema.DisplayType.DATETIME == FldType && obj != null) {
                
                String objType = Datetime.valueOf(obj.get(field[1].trim())).format(); 
                
                return objType;
               
            }
            return obj== null ? null : obj.get(field[1].trim());
        }
        return null;
    }
    
    public class TaskWrapper {
        @AuraEnabled public String taskTitle;
        @AuraEnabled public String taskDescription;
        @AuraEnabled public String btnAction;
        @AuraEnabled public String btnLabel;
        @AuraEnabled public String btnObject;
        @AuraEnabled public String btnField;
        @AuraEnabled public String btnFieldType;
        @AuraEnabled public String btnValue;
        @AuraEnabled public List<Block> blocks = new List<Block>();
    }
    
    public class Block {
        @AuraEnabled public Integer blockNumber;
        @AuraEnabled public String blockType;
        @AuraEnabled public String cmpName;
        @AuraEnabled public String docName;
        @AuraEnabled public String sourceId;
        @AuraEnabled public String url;
        @AuraEnabled public String blockDescription;
        @AuraEnabled public String inputHeading;
        @AuraEnabled public String inputHint;
        @AuraEnabled public String inputType;
        @AuraEnabled public String sfObject;
        @AuraEnabled public String sfFieldName;
        @AuraEnabled public String sfFieldType;
        @AuraEnabled public String value;
        @AuraEnabled public Integer scale;
        @AuraEnabled public String documentAppendedText;
        @AuraEnabled public Boolean multiSelect = false;
        @AuraEnabled public List<Map<String,String> > options;
        @AuraEnabled public Integer displayNumber;
        @AuraEnabled public Boolean isRequired = false;
    }
}