@IsTest
public class CSIGetuserDetails_Test {

     @testSetup
    static void setup() {
        Account acc = new Account(name='Test Account');
        insert acc;
        
        Contact contact = new Contact ( lastname ='Testing Individual',  AccountId = acc.Id,Client_Notifications_Choice__c = 'Opt-In'); 
        insert contact;  
        
        Profile profileId = [SELECT Id FROM Profile WHERE Name = 'Tecex Customer' LIMIT 1];
        
        User usr = new User(LastName = 'User',
                            FirstName='Testing',
                            Alias = 'tuser',
                            Email = 'testingUser@asdf.com',
                            Username = 'testingUser@asdf.com',
                            ContactId = contact.Id,
                            ProfileId = profileId.Id,
                            TimeZoneSidKey = 'GMT',
                            LanguageLocaleKey = 'en_US',
                            EmailEncodingKey = 'UTF-8',
                            LocaleSidKey = 'en_US'
                           );
        insert usr;
    }
    
    
    static testmethod void testGetMethod(){
        User usr = [SELECT Id from User where Email = 'testingUser@asdf.com'];
        
        CSIGetAccessTokenWra wrapper = new CSIGetAccessTokenWra();
        wrapper.ClientuserID = usr.Id;
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();             
        req.requestURI = '/services/apexrest/MyDetails/';
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueOf(JSON.serialize(wrapper));
        RestContext.request = req;
        RestContext.response= res;
        Test.startTest();
        CSIGetuserDetails.NCPGetuserDetails6();
        Test.stopTest();
    }

 static testmethod void testGetMethod1(){
        User usr = [SELECT Id from User where Email = 'testingUser@asdf.com'];
        
     	CSIGetAccessTokenWra wrapper = new CSIGetAccessTokenWra();
        wrapper.ClientuserID = usr.Id;
     
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();             
        req.requestURI = '/services/apexrest/MyDetails/';
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueOf(JSON.serialize(wrapper));
        RestContext.request = req;
        RestContext.response= res;
        Test.startTest();
        CSIGetuserDetails.NCPGetuserDetails6();
        Test.stopTest();
    }
}