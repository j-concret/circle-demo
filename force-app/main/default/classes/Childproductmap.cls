public class Childproductmap {

    @AuraEnabled
    public static list<part__c> getpartdata(){
        List<Part__c> Partslist = new list<part__c>();
        part__c[] prtl = [select ID,name,Search_Name_Text__c,Product__c from part__c where Product__c ='' limit 20];
        return prtl;
    }

    @AuraEnabled
    public static string updatepartdata(list<part__c> PPartl){
        System.debug(PPartl);
        list<part__c> Partstoupdate = new list<part__c>();
        for(integer i=0; PPartl.size()>i; i++) {
            if(Ppartl[i].product__c !=null) {
                Partstoupdate.add(Ppartl[i]);
            }
        }
        update Partstoupdate;
        String returnstring =  Partstoupdate.size()+' records updated';
        return returnstring;
    }

}