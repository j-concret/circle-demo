@RestResource(urlMapping='/SendFinalQuoteEmail/*')
Global class NCPSendFinalQuoteEmail {
    
      @Httppost
    global static void CPUpdateCostEstimates(){
        
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        Blob body = req.requestBody;
    	res.addHeader('Content-Type', 'application/json');
        String requestString = body.toString();
        NCPSendFinalQuoteEmailwra rw = (NCPSendFinalQuoteEmailwra)JSON.deserialize(requestString,NCPSendFinalQuoteEmailwra.class);
     	      
        try {
             Access_token__c At =[select status__c,Access_Token__c from Access_token__c where Access_Token__c=:rw.Accesstoken ];
           if( at.status__C =='Active' ){
              Shipment_Order__c AB =[Select id,Account__C,Li_ion_Batteries__c,Type_of_Goods__c,Lithium_Battery_Types__c,Client_Reference__c,Client_Reference_2__c,Shipment_Value_USD__c,Request_Email_Estimate__c,Email_Estimate_To__c,Who_arranges_International_courier__c  from Shipment_Order__c where Id =:rw.Id limit 1];
            
           		If((rw.Shipment_Value_USD ==AB.Shipment_Value_USD__c) && (rw.request_EmailEstimate == TRUE)  ){
              //   AB.Request_Email_Estimate__c=Boolean.valueof(rw.request_EmailEstimate);
                 AB.Email_Estimate_To__c=rw.Email_Estimate_to;
                      Update AB;
               ShipmentOrderTriggerHandler.NCPSendCEMail(rw.ID);
                     /*   AB.Request_Email_Estimate__c=Boolean.valueof(rw.request_EmailEstimate);
                        AB.Email_Estimate_To__c=rw.Email_Estimate_to;
                     
                        Update AB;
                   */
            JSONGenerator gen = JSON.createGenerator(true);
            gen.writeStartObject();
            
             gen.writeFieldName('Success');
             gen.writeStartObject();
            
             	if(AB.ID == null) {gen.writeNullField('Response');} else{gen.writeStringField('Response', 'Quote sent to '+ rw.Email_Estimate_to );}
            
            gen.writeEndObject();
            gen.writeEndObject();
            String jsonData = gen.getAsString();
            res.responseBody = Blob.valueOf(jsonData);
            res.addHeader('Content-Type', 'application/json');
            res.statusCode = 200;        
            
            
                API_Log__c Al = New API_Log__c();
                Al.Account__c=AB.Account__c;
                Al.EndpointURLName__c='NCPUpdateSO';
                Al.Response__c='Record Successfully updated' +AB.ID;
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al; 
                 } 
                    
               }
            }
        	catch (Exception e){
                
              	String ErrorString ='Something went wrong while updating cost estimate, please contact support_SF@tecex.com';
               	res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
            	res.addHeader('Content-Type', 'application/json');
                res.statusCode = 500;
                API_Log__c Al = New API_Log__c();
                Al.EndpointURLName__c='NCPUpdateSO';
                Al.Response__c=e.getMessage()+' at line number: '+e.getLineNumber();
                Al.StatusCode__c=string.valueof(res.statusCode);
              	Insert Al;

            
        	}
        
        
        
        }

}