@isTest
private class DAL_BankTransactionTest {
	
    @isTest static void TestDAL_BankTransaction() {

        //Prepare testing data
        
        //Account records:
        Account myClient   = TestDataFactory.myClient();
        Account mySupplier = TestDataFactory.mySupplier();
        
        //Bank transaction records:
        Bank_transactions__c bankTransactionRecord  = TestDataFactory.bankTrans();

        //bank transaction record Id set
        Set<id> idSet = new Set<id>{bankTransactionRecord.Id};
        
        //Customer Id's
        Id customerId         = myClient.Id;
        Set<id> customerIdSet = new Set<id>{myClient.Id, mySupplier.Id};
        
        
        Test.startTest();
        
        //Class Instantiation
        DAL_BankTransaction dalBankTransaction = new DAL_BankTransaction();

        //getSObjectFieldList
        List<Schema.SObjectField> sobjectFieldList = dalBankTransaction.getSObjectFieldList();

        //getSObjectType
        Schema.SObjectType sobjectType = dalBankTransaction.getSObjectType();

        //selectById
        List<Bank_transactions__c> bankTransById = dalBankTransaction.selectById(idSet);

        //selectUnappliedTransactionsByCustomerId
        List<Bank_transactions__c> unappliedTransactionsByCustomerId    = dalBankTransaction.selectUnappliedTransactionsByCustomerId(customerId);
        List<Bank_transactions__c> unappliedTransactionsByCustomerIdSet = dalBankTransaction.selectUnappliedTransactionsByCustomerId(customerIdSet);

        //getTotalUnappliedShipmentAmountByCustomerId
        Decimal totalUnappliedShipmentAmountByCustomerId = dalBankTransaction.getTotalUnappliedShipmentAmountByCustomerId(customerId);

        //getTotalUnappliedAmountByCustomerId
        List<DMN_Statement.AgingPeriod> totalUnappliedAmountByCustomerId = dalBankTransaction.getTotalUnappliedAmountByCustomerId(customerId);       

        //bankTransactionResult
        Map<Id,List<DMN_Statement.AgingPeriod>> totalUnappliedAmountByCustomerIdMap = dalBankTransaction.getTotalUnappliedAmountByCustomerId(customerIdSet);

        Test.stopTest();
        
        //getSObjectFieldList assert
        System.assertEquals(10, sobjectFieldList.size());	

        //getSObjectType Assert
        System.assert(bankTransactionRecord.getSObjectType() == sobjectType);

        //selectById assert
        System.assertEquals(1, bankTransById.size());

        //selectUnappliedTransactionsByCustomerId assert
        System.assertEquals(1, unappliedTransactionsByCustomerId.size());
        System.assertEquals(1, unappliedTransactionsByCustomerIdSet.size());
	}
}