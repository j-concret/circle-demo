@isTest
public with sharing class NCPAccountRelatedContacts_Test {

  @isTest
  public static void testGetRelatedContacts(){
    Access_Token__c at = new Access_Token__c(
      AccessToken_encoded__c='zmM0MDcxOTItZTJiZi00YWRhLTlkZGQtZGZhMmUxM2FiYTlj',
      Access_Token__c='cc407192-e2bf-4ada-9ddd-dfa2e13aba9c',
      Status__c='Active'
    );
    insert at;

    Account acc = new Account (name='Test Account', Type ='Supplier');
    insert acc;

    insert new List<Contact>{
      new Contact(LastName='Test Contact1',AccountId=acc.Id),
      new Contact(LastName='Test Contact2',AccountId=acc.Id)
    };

    Map<String,String> body = new Map<String,String>{'Accesstoken'=>at.Access_Token__c,'AccountID'=>acc.Id};
    RestRequest req = new RestRequest();
    RestResponse res = new RestResponse();
    req.requestURI = '/services/apexrest/NCPAccountRelatedContacts/'; //Request URL
    req.httpMethod = 'POST';
    req.requestBody = Blob.valueOf(JSON.serialize(body));

    RestContext.request = req;
    RestContext.response= res;

    Test.startTest();
      NCPAccountRelatedContacts.getRelatedContacts();      
    Test.stopTest();

  }

  @isTest
  public static void testNegativeGetRelatedContacts(){
    Map<String,String> body = new Map<String,String>();//{'Accesstoken'=>at.Access_Token__c,'AccountID'=>acc.Id};
    RestRequest req = new RestRequest();
    RestResponse res = new RestResponse();
    req.requestURI = '/services/apexrest/NCPAccountRelatedContacts/'; //Request URL
    req.httpMethod = 'POST';
    req.requestBody = Blob.valueOf(JSON.serialize(body));

    RestContext.request = req;
    RestContext.response= res;

    Test.startTest();
      NCPAccountRelatedContacts.getRelatedContacts();
    Test.stopTest();

  }

  @isTest
  public static void testNegative2GetRelatedContacts(){
    Access_Token__c at = new Access_Token__c(
      AccessToken_encoded__c='zmM1MDcxOTItZTJiZi00YWRhLTlkZGQtZGZhMmUxM2FiYTlj',
      Access_Token__c='cc497192-e2bf-4ada-9ddd-dfa2e13aba9c',
      Status__c='Expired'
    );
    insert at;

    Account acc = new Account (name='Test Account', Type ='Supplier');
    insert acc;

    Map<String,String> body = new Map<String,String>{'Accesstoken'=>at.Access_Token__c,'AccountID'=>acc.Id};
    RestRequest req = new RestRequest();
    RestResponse res = new RestResponse();
    req.requestURI = '/services/apexrest/NCPAccountRelatedContacts/'; //Request URL
    req.httpMethod = 'POST';
    req.requestBody = Blob.valueOf(JSON.serialize(body));

    RestContext.request = req;
    RestContext.response= res;

    Test.startTest();
      NCPAccountRelatedContacts.getRelatedContacts();
    Test.stopTest();

  }
}