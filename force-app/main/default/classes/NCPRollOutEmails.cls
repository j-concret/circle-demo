@RestResource(urlMapping='/RollOutEmails/*')
Global class NCPRollOutEmails {
    @Httppost
    global static void NCPRollOutEmails(){
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        Blob body = req.requestBody;
        res.addHeader('Content-Type', 'application/json');
        String requestString = body.toString();
        NCPRollOutEmailsWra rw  = (NCPRollOutEmailsWra)JSON.deserialize(requestString,NCPRollOutEmailsWra.class);
        try{
            Access_token__c At =[select status__c,Access_Token__c from Access_token__c where Access_Token__c=:rw.Accesstoken ];
            if( at.status__C =='Active'){
                
                Id userId = UserInfo.getUserId();
                List<User> userList = [Select ID,contactId,contact.AccountId from USer where id =:userId];
              
                List<String> toAddress = new List<String>();
                toaddress.add(rw.toEmailID);
                List<String> ccAddress = new List<String>();
                If(rw.ccEmailID !=null){
                    for(String cc : rw.ccEmailID.split(';')){ccAddress.add(cc); }
                }
                
                
                Roll_Out__C RO = [Select Id, Name, Client_Name__c, RecordTypeId, Create_Roll_Out__c, Destinations__c, Shipment_Value_in_USD__c, Contact_For_The_Quote__c, File_Reference_Name__c from roll_out__c where id =:rw.rolloutid];
               	System.debug('ro '+RO.id);
                if(userList[0].contact.AccountId == RO.Client_Name__c){
                    CTRL_EmailAndAttachStatement.NCPRolloutSendMail(rw.RolloutId, toaddress,ccAddress); 
                   /* Roll_Out__C ro1= new Roll_Out__C(Id = RO.Id,isNCPEmail__c = true,
                                                     toAddress__c = rw.toEmailID,ccAddress__c = rw.ccEmailID);
                    update ro1;*/
                    
                    JSONGenerator gen = JSON.createGenerator(true);
                    gen.writeStartObject();
                    
                    gen.writeFieldName('Success');
                    gen.writeStartObject();
                    
                    gen.writeStringField('Response', 'Rollout Email Sent ');
                    
                    gen.writeEndObject();
                    gen.writeEndObject();
                    String jsonData = gen.getAsString();
                    res.responseBody = Blob.valueOf(jsonData);
                    res.addHeader('Content-Type', 'application/json');
                    res.statusCode = 200; 
                    
                    API_Log__c Al = New API_Log__c();
                    Al.EndpointURLName__c='RollOutEmails';
                    //Al.Account__c=rw.AccountID;
                    Al.StatusCode__c=string.valueof(res.statusCode);
                    AL.Response__c = 'Success - RollOutEmails sent';
                    Insert Al;  
                }else{
                    JSONGenerator gen = JSON.createGenerator(true);
                    gen.writeStartObject();
                    
                    gen.writeFieldName('Success');
                    gen.writeStartObject();
                    
                    gen.writeStringField('Response', 'You are authorize to access this rollout');
                    
                    gen.writeEndObject();
                    gen.writeEndObject();
                    String jsonData = gen.getAsString();
                    res.responseBody = Blob.valueOf(jsonData);
                    res.addHeader('Content-Type', 'application/json');
                    res.statusCode = 200; 
                    
                    API_Log__c Al = New API_Log__c();
                    Al.EndpointURLName__c='RollOutEmails';
                    //Al.Account__c=rw.AccountID;
                    Al.StatusCode__c=string.valueof(res.statusCode);
                    AL.Response__c = 'Success - You are authorize to access this rollout';
                    Insert Al;  
                    
                }
                
            }
            else{
                JSONGenerator gen = JSON.createGenerator(true);
                gen.writeStartObject();
                
                gen.writeFieldName('Error');
                gen.writeStartObject();
                
                gen.writeStringField('Response', 'Accesstoken Expired');
                
                gen.writeEndObject();
                gen.writeEndObject();
                String jsonData = gen.getAsString();
                res.responseBody = Blob.valueOf(jsonData);
                res.addHeader('Content-Type', 'application/json');
                res.statusCode = 400;        
                
                
            }
            
        }
        
        catch(Exception e){
            
            /*   
IF(e.getMessage()=='Insert failed. First exception on row 0; first error: INSUFFICIENT_ACCESS_ON_CROSS_REFERENCE_ENTITY, insufficient access rights on cross-reference id: []'){

String abc= 'RollOutEmails sent';
res.responseBody = Blob.valueOf(abc);
res.addHeader('Content-Type', 'application/json');
res.statusCode = 200;

API_Log__c Al = New API_Log__c();
Al.EndpointURLName__c='RollOutEmails';
//Al.Account__c=rw.AccountID;
Al.StatusCode__c=string.valueof(res.statusCode);
AL.Response__c = 'Success - RollOutEmails sent';
Insert Al; 
}
else{
*/     
            String ErrorString ='Something went wrong, please contact support_SF@tecex.com';
            res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
            res.addHeader('Content-Type', 'application/json');
            res.statusCode = 500;
            
            API_Log__c Al = New API_Log__c();
            //Al.Account__c=rw.AccountID;
            //Al.Login_Contact__c=logincontact.Id;
            Al.EndpointURLName__c='RollOutEmails';
            Al.Response__c=ErrorString + e.getMessage()+e.getLineNumber();
            Al.StatusCode__c=string.valueof(res.statusCode);
            Insert Al;
            
            // }
            
            
            
            
            
        }
        
    }
}