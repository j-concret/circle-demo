@RestResource(urlMapping='/NCPOnboarding/*')
Global class NCPOnboarding {
    
    @Httppost
    global static void NCPOnboarding(){
        
         RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        Blob body = req.requestBody;
    	res.addHeader('Content-Type', 'application/json');
        String requestString = body.toString();
        NCPOnboardingWrap rw = (NCPOnboardingWrap)JSON.deserialize(requestString,NCPOnboardingWrap.class);
        
        Try{
            String LeadID;
            List<Access_token__c> at = [SELECT status__c,Access_Token__c FROM Access_token__c WHERE Access_Token__c =: rw.Accesstoken and status__c = 'Active' LIMIT 1];
             lead le = new lead();
            If(!at.isEmpty() && rw.createlead==TRUE){
                
               // lead le = new lead();
                    le.FirstName= rw.firstName;
                    le.LastName = rw.lastName;
                    le.Company = rw.companyName;
                    le.Email = rw.email;
                    le.Country = rw.country;
                    le.phone=rw.phone;
                    le.Leadsource='Client Portal';
                    le.Client_Type__c = rw.typeOfCompany;
                	le.Price_List_Type__c ='Standard Price List';
                Insert le;
                
                LeadID=Le.Id;
                    
                JSONGenerator gen = JSON.createGenerator(true);
            		gen.writeStartObject();
            
             		gen.writeFieldName('Success');
             		gen.writeStartObject();
            
             			if(le.id == null) {gen.writeNullField('Response');} else{gen.writeStringField('LeadID',Le.Id);}
            
            		gen.writeEndObject();
            		gen.writeEndObject();
            		String jsonData = gen.getAsString();
            		res.responseBody = Blob.valueOf(jsonData);
            		res.addHeader('Content-Type', 'application/json');
            		res.statusCode = 200;    
                API_Log__c Al = New API_Log__c();
                Al.EndpointURLName__c='NCPOnboarding - Leadcreation';
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al;
                    
            }
            
            Else if(!at.isEmpty() && rw.createlead==FALSE && rw.createQuote==TRUE && rw.createAccount==FALSE){
              //  Lead leu =[Select id,Status,Related_Client__c from lead where id =: LeadID];
  				Account ac = [SELECT Id,Service_Manager__c,Financial_Manager__c,Financial_Controller__c FROM Account WHERE Id=:'0010Y00000PEqvHQAT'];
                String CERecordTypeID = Schema.SObjectType.Shipment_Order__c.getRecordTypeInfosByName().get('Cost Estimate').getRecordTypeId();
                Decimal chWeight = 0;
                if(String.isNotBlank(rw.weightUnit) && rw.weightUnit.toUpperCase() == 'KG')
                    chWeight = rw.chargeableWeight;
                else {
                    chWeight = rw.chargeableWeight * 0.453592;
                }
              

                Shipment_Order__c CE = new Shipment_Order__c();
                CE.Account__c = '0010Y00000PEqvHQAT';
                CE.Client_Contact_for_this_Shipment__c = '0031v00002LwQxXAAV';
                CE.Destination__c= rw.shippingTo;
                CE.Ship_From_Country__c = rw.shippingFrom;
                CE.Shipment_Value_USD__c= rw.shipmentValue;
				CE.Source__c= 'Client Portal';
                CE.Chargeable_Weight__c = chWeight;
                CE.RecordTypeId= CERecordTypeID;
                CE.Type_of_Goods__c = rw.typeOfGoods;
                CE.Shipping_Notes_New__c = rw.shippingNotes;
				CE.Who_arranges_International_courier__c= 'TecEx';
                CE.Service_Type__c= 'IOR';
                CE.service_Manager__c = ac.Service_Manager__c;
                CE.Financial_Manager__c =ac.Financial_Manager__c;
                CE.Financial_Controller__c =ac.Financial_Controller__c;
                CE.Shipping_Status__c ='Cost Estimate';
                CE.Lead_Name__c = rw.LeadID;
                insert CE;
                
                shipment_Order__C SO1 = [Select Id,Name,NCP_Quote_Reference__c,Ship_From_Country__c,IOR_Price_List__r.name,Service_Type__c,Final_Deliveries_New__c,Shipment_Value_USD__c,Type_of_Goods__c,Li_ion_Batteries__c,Lithium_Battery_Types__c,of_packages__c,Chargeable_Weight__c,TecEx_Invoice_Number__c, 
                IOR_FEE_USD__c,IOR_and_Import_Compliance_Fee_USD_numb__c,Tax_recovery_Premium_Fee__c,EOR_and_Export_Compliance_Fee_USD__c,EOR_and_Export_Compliance_Fee_USD_numb__c,
                Set_up_Fee__c,Handling_and_Admin_Fee__c,Bank_Fees__c,Quoted_Admin_Fee__c,Quoted_Bank_Fee__c,First_Invoice_Created__c,TopUp_Invoice_Created__c,Pull_CPA_and_IOR_PL__c,
                Taxes_Calculated__c, Recharge_License_Cost__c ,xxxTotal_Invoice_Amount__c,Total_Customs_Brokerage_Cost__c,Recharge_Handling_Costs__c,Total_clearance_Costs__c,
                Recharge_Tax_and_Duty__c,Recharge_Tax_and_Duty_Other__c, International_Delivery_Fee__c,Insurance_Fee_USD__c,Total_License_Cost__c,Finance_Fee__c,
                Miscellaneous_Fee__c,Miscellaneous_Fee_Name__c,Collection_Administration_Fee__c,Total_Invoice_Amount_Excl_Taxes_Duties__c,Total_including_estimated_duties_and_tax__c,
                Total_Invoice_Amount__c,Rebate_Due__c,Forex_Rate_Used_Euro_Dollar__c,
                                         shipping_Notes_formula__C,Estimate_Transit_Time_Formula__c,Estimate_Customs_Clearance_Time_Formula__c,Compliance_Process__c from Shipment_order__C where Id=: CE.ID]; 
                
                    
             res.responseBody = Blob.valueOf(JSON.serializePretty(SO1));
             res.addHeader('Content-Type', 'application/json');
             res.statusCode = 200;
                
                API_Log__c Al = New API_Log__c();
                Al.EndpointURLName__c='NCPOnboarding - Quotecreation';
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al;  
                
                
            }
            else if(!at.isEmpty() && rw.createlead==FALSE && rw.createQuote==false && rw.createAccount==TRUE &&rw.leadId != null){
               
                
                Account acc = new Account(
                    Name = rw.companyName,
                    Phone= rw.Phone,
                    Contact_Email__c = rw.email,
                    Client_type__c = rw.typeOfCompany,
                    Lead_Source__c='Web',
                    Country__c = rw.country
                    );
                insert acc;
                Contact con = new contact(
               
                    AccountId = acc.Id,
                    //Phone=acc.phone,
                    FirstName = rw.firstName,
                    LastName = rw.lastName,
                    Email = rw.email,
                    Contact_Role__c = rw.role
                    );
                insert con;
                Lead leu =[Select id,Status,Related_Client__c from lead where id =:rw.leadid];
                Leu.status='Qualified Lead';
                leu.Related_Client__c = acc.id;
                Update Leu;
                
                 JSONGenerator gen = JSON.createGenerator(true);
            		gen.writeStartObject();
            
             		gen.writeFieldName('Success');
             		gen.writeStartObject();
            
             			if(le.id == null) {gen.writeNullField('LeadID');} else{gen.writeStringField('LeadID',Le.Id);}
                		if(Acc.id == null) {gen.writeNullField('AccountID');} else{gen.writeStringField('AccountId',Acc.Id);}
                		if(con.id == null) {gen.writeNullField('ContactID');} else{gen.writeStringField('ContactID',con.Id);}
            
            		gen.writeEndObject();
            		gen.writeEndObject();
            		String jsonData = gen.getAsString();
            		res.responseBody = Blob.valueOf(jsonData);
            		res.addHeader('Content-Type', 'application/json');
            		res.statusCode = 200;    
                API_Log__c Al = New API_Log__c();
                Al.EndpointURLName__c='NCPOnboarding - Accountcreation';
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al;
                
            }
            
            else if(!at.isEmpty() && rw.createCase == true){
                
              //  Account Ac = [Select id from account where id =:rw.AccountID];
               // Contact Con= [Select id from contact where id = :rw.ContactId];
                Case cs = new case();
               
                    cs.Subject = rw.Subject;
                    cs.Reason = 'Client Portal';
                    cs.Description = rw.Description;
                    cs.Status = 'new';
                    cs.AccountId=rw.AccountID;
                    cs.Contactid=rw.ContactId;
                    
                   
                insert cs;
                
                case cs1 =[Select Id,CaseNumber from case where id=:cs.Id];
                  JSONGenerator gen = JSON.createGenerator(true);
            		gen.writeStartObject();
            
             		gen.writeFieldName('Success');
             		gen.writeStartObject();
            
             			if(cs.id == null) {gen.writeNullField('Response');} else{gen.writeStringField('Response','Case created successfully. Casse Reference number is '+cs1.CaseNumber);}
            
            		gen.writeEndObject();
            		gen.writeEndObject();
            		String jsonData = gen.getAsString();
            		res.responseBody = Blob.valueOf(jsonData);
            		res.addHeader('Content-Type', 'application/json');
            		res.statusCode = 200;    
                API_Log__c Al = New API_Log__c();
                Al.EndpointURLName__c='NCPOnboarding - Casecreation';
                Al.StatusCode__c=string.valueof(res.statusCode);
                Al.Response__c = 'Created case' +cs.id;
                Insert Al;
                
            }
            
            
        }
        Catch(Exception e){
            
             String jsonData = Util.getResponseString('Error','Something went wrong on onboarding, please contact support_SF@tecex.com');
            res.responseBody = Blob.valueOf(jsonData);
            res.statusCode = 500;
            insert new API_Log__c(
           //     Account__c = (reqBody != null && String.isNotBlank(reqBody.AccountID)) ? reqBody.AccountID : null,
                EndpointURLName__c = 'NCPOnboarding',
                Response__c = 'Error - NCPOnboarding :'+e.getMessage()+' at line number: '+e.getLineNumber(),
                StatusCode__c = String.valueof(res.statusCode)
                );
        }
    
        
    }

}