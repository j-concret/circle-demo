public class BDD_Utils 
{

    public static String itemsQry(String tableName, Map<String, String> fieldList) 
    {
    	return itemsQry(tableName, fieldList, 3);  
    }  
    
    public static String itemsQry(String tableName, Map<String, String> fieldList, Integer maxRows) 
    {
    	return SysUtils.selectFrom(tableName, fieldList.keySet(), new List<string> {'Last_API_Update__c'}) + ' LIMIT ' + maxRows;	
    }  
    
    public static ID getRandomID(String tableName) 
    {
    	Integer maxCount = 20;
    	
    	Integer numRows = Database.countQuery('SELECT COUNT() FROM ' + tableName + ' LIMIT ' + maxCount);
		Integer rowNum = Math.floor(Math.random() * Math.min(numRows, maxCount)).intValue(); 
		SObject o = Database.query('SELECT Id FROM ' + tableName + ' LIMIT 1 OFFSET :rowNum');
		
		return (ID)o.get('Id');
    }
    
    public static ID getRandomUserId()
    {
    	return getRandomID('User');	
    }
    
    public static ID getRandomAccountId() 
    {
    	return getRandomID('Account');	
    }
}