public class CaptureLineItems {
    /** used to get all the related Part__c List for a specific shipmement Order **/
    public static List<Part__c> getParts(Id ShipmentOrderId){
        return [SELECT Name,Specific_VAT_Rate__c,Part_Specific_Rate_1__c,Part_Specific_Rate_2__c,Part_Specific_Rate_3__c,Commercial_Value__c,Total_Value__c,US_HTS_Code__c,Matched_HS_Code2__c,Matched_HS_Code2__r.Name,Rate__c,Default_Duty_Rate__c,Captured_HS_code__c,Captured_HS_code_rate__c,Captured_Specific_VAT_Rate__c,Captured_Part_Specific_Rate_1__c,Captured_Part_Specific_Rate_2__c,Captured_Part_Specific_Rate_3__c FROM Part__c WHERE Shipment_Order__c =:ShipmentOrderId];
    }
    
    /** returns the Map of LineItems linked to specific customs_Clearance_Documents Id **/
    @AuraEnabled
    public static Map<String,Object> getLineItems(Id recordId){
        Map<String,Object> responseMap = new Map<String,Object>();
        responseMap.put('status','OK');
        try{
            Customs_Clearance_Documents__c customs_Clearance_Documents = CCDListFiles.getCustomsClearanceDocument(recordId);
            List<Part__c> parts = getParts(customs_Clearance_Documents.Customs_Clearance_Documents__r.Id);
            responseMap.put('data',parts);
        }catch(Exception e) {
            responseMap.put('status','Error');
            responseMap.put('message',e.getMessage());
        }
        
        return responseMap;
    }
    
    /** This Method takes List of Part__c for the Lightning Component and upsert the record accordingly **/
    @AuraEnabled
    public static Map<String,Object> updateParts(List<Part__c> parts){
        Map<String,Object> responseMap = new Map<String,Object> {'status'=>'OK'};
        try{
            upsert parts;
            responseMap.put('data', parts);
        }catch(Exception e) {
            responseMap.put('status', 'ERROR');
            responseMap.put('message', e.getMessage());
        }
        return responseMap;
    }
}