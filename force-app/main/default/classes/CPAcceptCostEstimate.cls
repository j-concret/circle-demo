@RestResource(urlMapping='/AcceptCostestimate/*')
Global class CPAcceptCostEstimate {
     @Httpput
    global static void CPAcceptCEstimate(){
        
        RestRequest req = RestContext.request;
    	RestResponse res = RestContext.response;
        res.addHeader('Content-Type', 'application/json');  
        String CEID = req.params.get('CEID');
        Id SOrecordTypeId = Schema.SObjectType.Shipment_Order__c.getRecordTypeInfosByDeveloperName().get('Shipment_Order').getRecordTypeId();
        Shipment_Order__c SO = [select Id,Name,Account__C,Client_Contact_for_this_Shipment__c,Shipping_Status__c,RecordTypeId from Shipment_Order__c where ID =:CEID limit 1];
         Decimal DATPCCD1=0;   
         try {
             
             List<Final_Delivery__c> FR = [select Id from Final_Delivery__c where Shipment_Order__c=:SO.id];
             List<Part__c> Pts = [select Id from Part__c where Shipment_Order__c=:SO.id];
             If(Fr.size()<=0 || Pts.size()<=0 ){
                 		//String ResponseString ='Number of Final deliveries availble : '+Fr.size()+ ' and Number of parts availble : '+Pts.size() +' Please add atleast one final delivery and part to accept cost estimate' ;
                        String ResponseString ='Please add Final Deliveries and Line Items in the below sections to accept the Cost Estimate';
                 		res.responseBody = Blob.valueOf(JSON.serializePretty(ResponseString));
                        res.statusCode = 400;
                   
                            API_Log__c Al = New API_Log__c();
                            Al.Account__c=So.Account__c;
                            Al.Login_Contact__c=So.Client_Contact_for_this_Shipment__c;
                            Al.EndpointURLName__c='AcceptCostestimate';
                            Al.Response__c=ResponseString;
                            Al.StatusCode__c=string.valueof(res.statusCode);
                            Insert Al;
                            
                 
             		}
             
             
               Else if(SO.Shipping_Status__c == 'Cost Estimate' && Fr.size()>0 && Pts.size()>0 ){
                   
            AggregateResult[] groupedResults = [SELECT COUNT(ID) CountID,SUM(Total_Value__c) DATPCCD from part__c  where  Shipment_Order__c =: SO.ID];
            system.debug('groupedResults (ShipmentOrder) -->'+groupedResults);
            
            for(AggregateResult a : groupedResults){
                DATPCCD1 = (Decimal) a.get('DATPCCD');                             
            }
                   
                SO.Total_Line_Item_Extended_Value__c = DATPCCD1;
                SO.Shipment_Value_USD__c = DATPCCD1;   
                SO.Shipping_Status__c = 'Shipment Pending';
                SO.RecordTypeId= SOrecordTypeId;  
                Update SO;
                
            		String ResponseString ='Shipment is Live. '+So.Name;
                    res.responseBody = Blob.valueOf(JSON.serializePretty(ResponseString));
        			res.statusCode = 200;
               
                API_Log__c Al = New API_Log__c();
                Al.Account__c=So.Account__c;
                Al.Login_Contact__c=So.Client_Contact_for_this_Shipment__c;
                Al.EndpointURLName__c='AcceptCostestimate';
                Al.Response__c=ResponseString;
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al;
                
                
            	  }
            else {
                    String ErrorString ='CostEstimate not updated.';
               		res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
        			res.statusCode = 400;
                
                API_Log__c Al = New API_Log__c();
                Al.Account__c=So.Account__c;
                Al.Login_Contact__c=So.Client_Contact_for_this_Shipment__c;
                Al.EndpointURLName__c='AcceptCostestimate';
                Al.Response__c=ErrorString;
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al;
                
            	}
        
            }
            catch (Exception e) {
                String ErrorString ='Something went wrong while accepting cost estimate, please contact support_SF@tecex.com';
               	res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
                res.statusCode = 500;
                API_Log__c Al = New API_Log__c();
                Al.Account__c=So.Account__c;
              //  Al.Login_Contact__c=So.Client_Contact_for_this_Shipment__c;
                Al.EndpointURLName__c='AcceptCostestimate';
                Al.Response__c=e.getMessage();
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al;
            } 
        
        
        
    }

}