@RestResource(urlMapping='/AcceptCE/*')
Global class AcceptCE {
     @Httppost
      global static void NCPAcceptCE(){
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        Blob body = req.requestBody;
    	res.addHeader('Content-Type', 'application/json');
        String requestString = body.toString();
        AcceptCEWra rw  = (AcceptCEWra)JSON.deserialize(requestString,AcceptCEWra.class);
          Try{
               Id SOrecordTypeId = Schema.SObjectType.Shipment_Order__c.getRecordTypeInfosByDeveloperName().get('Shipment_Order').getRecordTypeId();
               Id ZeerecordTypeId = Schema.SObjectType.Shipment_Order__c.getRecordTypeInfosByDeveloperName().get('Zee_Shipment_Order').getRecordTypeId();
               Access_token__c At =[select status__c,Access_Token__c from Access_token__c where Access_Token__c=:rw.Accesstoken ];
            	if( at.status__C =='Active' ){
              
                list<id> soids = new list<id>();
                    for(integer i=0;rw.SO.size()>i;i++){ SOIDS.add(rw.SO[i].soid);}
                
                list<Shipment_Order__c> SOtoupdate = new list<Shipment_Order__c>();    
                list<Shipment_Order__c> SO = [select Id,Populate_Invoice__C,of_Line_Items__c,Final_Deliveries_New__c,Name,Account__C,Client_Contact_for_this_Shipment__c,Shipping_Status__c,RecordTypeId from Shipment_Order__c where ID in:soids and Shipping_Status__c = 'Cost Estimate' ];
                    For(Shipment_Order__c So1 :SO){
                        system.debug('Inside For');
                        if(So1.of_Line_Items__c > 0 && So1.of_Line_Items__c != null && SO1.Shipping_Status__c == 'Cost Estimate'){
                             Decimal DATPCCD1=0;
                                                 AggregateResult[] groupedResults = [SELECT COUNT(ID) CountID,SUM(Total_Value__c) DATPCCD from part__c  where  Shipment_Order__c =: SO1.ID];
                                                system.debug('groupedResults (ShipmentOrder) -->'+groupedResults);
                                                 for(AggregateResult a : groupedResults){
                                                                    DATPCCD1 = (Decimal) a.get('DATPCCD');                             
                                                                }
                                    SO1.Total_Line_Item_Extended_Value__c = DATPCCD1;
                                    SO1.Shipment_Value_USD__c = DATPCCD1;   
                                   // SO1.Shipping_Status__c = 'Shipment Pending';
                                    if(rw.AppName=='TecEx') {SO1.RecordTypeId= SOrecordTypeId;SO1.Shipping_Status__c = 'Shipment Pending';} else{SO1.Shipping_Status__c = 'Shipment Pending'; }  //SO1.RecordTypeId= ZeerecordTypeId; SO1.Populate_Invoice__C=true;
                                         SOtoupdate.add(SO1);
                                      }
                        else{
                                    
                                     if(rw.AppName=='TecEx') {SO1.RecordTypeId= SOrecordTypeId;SO1.Shipping_Status__c = 'Shipment Pending';} else{SO1.Shipping_Status__c = 'Shipment Pending';} //SO1.RecordTypeId= ZeerecordTypeId; SO1.Populate_Invoice__C=true; 
                                    SOtoupdate.add(SO1);
                        	}
                        
                    }
                    //System.debug('SOtoupdate -->'+SOtoupdate);
                    update SOtoupdate;
                    
            JSONGenerator gen = JSON.createGenerator(true);
            gen.writeStartObject();
            
             gen.writeFieldName('Success');
             gen.writeStartObject();
            
             	if(soids.isempty()) {gen.writeNullField('Response');} else{gen.writeStringField('Response', 'Cost estimates are live');}
            
            gen.writeEndObject();
            gen.writeEndObject();
            String jsonData = gen.getAsString();
            res.responseBody = Blob.valueOf(jsonData);
            res.addHeader('Content-Type', 'application/json');
            res.statusCode = 200; 
                    
                    
                }
          }
          catch(Exception e){
                 String ErrorString ='Something went wrong, please contact support_SF@tecex.com';
               		res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
            		res.addHeader('Content-Type', 'application/json');
        			res.statusCode = 400;
                
                API_Log__c Al = New API_Log__c();
                Al.Account__c=rw.AccountID;
                Al.EndpointURLName__c='NCPAcceptCE';
                Al.Response__c=e.getMessage()+' at line number: '+e.getLineNumber();
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al;
                
              
          }



      }

}