@isTest
public class TestUtil {
  //Account factories
  public static List<Account> createAccounts( Integer numOfAcc, Boolean doInsert ){
    List<Account> testAcc = new List<Account>();
    for( Integer i = 0; i < numOfAcc; i++ ){
          Account acc = new Account ( 
          Name = 'test123' + i,
          BillingCountry = 'US',
          BillingStreet = '123 Main St',
          BillingCity = 'Chicago',
          BillingState = 'IL',
          BillingPostalCode = '60603',
          Converted__c = true);
          
          testAcc.add( acc );
    }
    if( doInsert )
      insert testAcc;
      
    return testAcc;
  }
   
  
  //Contact factories
  public static List<Contact> createContacts( Integer size, Id accId, Boolean doInsert ){
    List<Contact> contacts = new List<Contact>();
    for (Integer i = 0; i < size; i++) {
        Contact newContact = new Contact();
        newContact.AccountId = accId;
        newContact.FirstName = 'testxx' + i;
        newContact.LastName = 'testxx' + i;
        newContact.Email = 'testxx@test.com';
        contacts.add(newContact);
    }
    if ( doInsert )
      insert contacts;
      
    return contacts;
  }
  
  public static fw1__Payment_Center_Setting__c addDefaultSettings() {
        
        fw1__Payment_Center_Setting__c defaultSetting = new fw1__Payment_Center_Setting__c();
            defaultSetting.Name = 'Default Settings';
            defaultSetting.fw1__Schedule__c = 'Daily';
            defaultSetting.fw1__Recurring_Start_Time__c = '6';
            defaultSetting.fw1__Default_Email_Subject__c = 'Your {COMPANY_NAME} invoice is ready';
            defaultSetting.fw1__Default_Email_Message__c = 'Thank you for giving {COMPANY_NAME} the opportunity to serve you. Please see attached for details of your invoice.';
            //defaultSetting.Default_Terms_And_Conditions__c = ' ';
            defaultSetting.fw1__Payment_Receipt_Email_Subject__c = '{COMPANY_NAME} Payment Processed';
            defaultSetting.fw1__Payment_Receipt_Email_Message__c = 'Thank you, your payment has been successfully processed. Below you will find the details of your payment.';
            defaultSetting.fw1__Email_Footer__c = 'Please do not reply to this message. This email is not regularly monitored.';
            defaultSetting.fw1__Email_Currency_Symbol__c = '$';
            //defaultSetting.Email_Currency_Symbol__c = UserInfo.getDefaultCurrency();
            //if (defaultSetting.Email_Currency_Symbol__c == 'USD' || defaultSetting.Email_Currency_Symbol__c == null) {
            //    defaultSetting.Email_Currency_Symbol__c = '$';
            //}
            defaultSetting.fw1__Online_Payment_Type__c = 'Charge';
            defaultSetting.fw1__Store_Credit_Card__c = true;
            defaultSetting.fw1__Invoice_Template__c = 'Invoice Template';
            defaultSetting.fw1__Receipt_Template__c = 'Standard Payment Receipt v3.14';
            defaultSetting.fw1__Failed_Attempt1__c = '3';
            defaultSetting.fw1__Failed_Attempt2__c = '5';
            defaultSetting.fw1__Failed_Attempt3__c = '7'; 
            defaultSetting.fw1__Failed_Attempt_Final__c = 'O';
            defaultSetting.fw1__Failed_Auto_Payment_Template__c = 'Failed Automatic Payment';
            defaultSetting.fw1__Outstanding_Invoice_Template__c = 'Invoice Template - Outstanding';
            defaultSetting.fw1__Dunning_Age__c = 30;
            defaultSetting.fw1__Outstanding_Invoice_Job_Time__c = '5';
            defaultSetting.fw1__Electronic_Signature_Prompt__c = 'BY typing my name below, I acknowledge that I have read and agree to the terms and conditions pertaining to this transaction.';
            defaultSetting.fw1__Attach_Terms_And_Conditions__c = true;
            defaultSetting.fw1__Batch_Invoice_Default_Template__c = 'Invoice Template - Batch';
            defaultSetting.fw1__Email_me_a_copy_of_invoice__c = true;
            defaultSetting.fw1__Statement_Default_Template__c = 'Standard Account Statement v4.2';
            //defaultSetting.fw1__Sender_Display_Name__c = Common.cutString(UserInfo.getOrganizationName(), 50);
            defaultSetting.fw1__Custom_Css__c = 'OnlinePaymentCss';
            defaultSetting.fw1__Use_Invoice_Logo_In_Online_Payment_Page__c = true;
            defaultSetting.fw1__Display_Profile_By_Contact__c = true;
            defaultSetting.fw1__CreditCard_Expiry_Notify_Days__c = 30;
            defaultSetting.fw1__CreditCard_Expiry_Notify_Days2__c = 15;
            defaultSetting.fw1__CreditCard_Expiry_Template__c = 'Credit Card Expiration Notice v3.12';
            defaultSetting.fw1__Require_Credit_Memo__c = true;
            defaultSetting.fw1__Default_Credit_Memo_Email_Subject__c = '{COMPANY_NAME} has issued you a credit';
            defaultSetting.fw1__Default_Credit_Memo_Email_Message__c = 'A credit has been issued to your account. Please see attached for more details.';
            defaultSetting.fw1__Is_Auto_Submit_On_Swipe__c = true;
            defaultSetting.fw1__Enable_Asset_Tracking__c = false;
            defaultSetting.fw1__Credit_Card_Surcharge_Label__c = 'Convenience fee';
            
            insert defaultSetting;
           return defaultSetting;
             
        }
}