@istest
public class LeadTriggerTest {
	@istest
    public static void leadTriggerTest(){
        Company__c company = new Company__c(Name = 'Test Company',Region__C='US');
        insert company;
        Lead ld = new Lead();
        ld.Company  = 'Test';
        ld.LastName = 'LastName';
        ld.Country__c = 'Australia';
        ld.Client_Type__c = 'Distributor';
        ld.Price_List_Type__c = 'Standard Price List';
        ld.Status = 'Cool Down';
        ld.Related_Company__c = company.Id;
        ld.Branch_GlobalPL__c = 'TecEx - Andre US';
        insert ld;
    }
    
    @istest
    public static void leadTriggerTest1(){
        
        String zeeRecordType = Schema.Sobjecttype.lead.getRecordTypeInfosByDeveloperName().get('Client_E_commerce').getRecordTypeId();
		
        Group queue = [SELECT Id  FROM Group Where Type = 'Queue'AND DeveloperName = 'Zee_Lead_Queue'];
        
        Lead_Round_Robin_Assignment__c lrr = new Lead_Round_Robin_Assignment__c();
        lrr.Name='Default';
        lrr.User_Index__c=-1;
        Insert lrr;
        
        Lead ld = new Lead();
        ld.recordtypeid=zeeRecordType;
    	ld.client_Type__c='E-commerce';
    	ld.Price_List_Type__c='E-commerce Price List';
        ld.OwnerId  = queue.Id;
        ld.Company  = 'Test1';
        ld.LastName = 'LastName1';
        ld.Country__c = 'Australia';
        ld.Branch_GlobalPL__c = 'TecEx - Andre US';
        
        insert ld;
    }
    
}