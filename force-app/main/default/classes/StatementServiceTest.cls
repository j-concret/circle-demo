@isTest
private class StatementServiceTest {
    
    @isTest static void TestStatementServiceTest() {
        

        Test.startTest(); 

        Customer_Invoice__c customerInvoiceRecord2 = TestDataFactory.myFirstInvoice();
        
        Account myClient      = TestDataFactory.myClient();
        Id nullId;
        Attachment att        = TestDataFactory.myAttachment();
        Attachment att2       = TestDataFactory.myAttachment2();
        String statementType  = 'General';
        String statementType2 = 'Test General';

        Shipment_Order__c myShipmentOrder = TestDataFactory.myShipmentOrder();

        DAL_Attachment dalAtt                  = new DAL_Attachment();
        List<Attachment> selectByNameAndParent = dalAtt.selectByNameAndParent(att.Name, att.ParentId);
        
        //generateBody
        DMN_Statement.StatementBody statementBody  =  StatementService.generateBody(myClient.Id);
        DMN_Statement.StatementBody statementBody2 =  StatementService.generateBody(nullId);

        //generateBody
        DAL_CustomerInvoice dalCustomerInvoice           = new DAL_CustomerInvoice();
        List<Customer_Invoice__c> dalCustomerInvoiceList = dalCustomerInvoice.selectOpenInvoicesByCustomerId(myClient.Id);
        DMN_Statement.StatementBody generateBody         =  StatementService.generateBody(myClient.Id);

        //getStatementt
        String getStatement  = StatementService.getStatement (myClient.Id, myClient.Name, statementType);
        String getStatement2 = StatementService.getStatement (myClient.Id, myClient.Name, statementType2);
        
         //attachStatement
        String attachStatement  = StatementService.attachStatement (myClient.Id, myClient.Name, statementType);
        String attachStatement2 = StatementService.attachStatement (myClient.Id, myClient.Name, statementType2);
        
        //calculateInvoiceAge
        String invoiceAge = StatementService.calculateInvoiceAge(customerInvoiceRecord2.Due_Date__c);  
        
        Test.stopTest();
    }
}