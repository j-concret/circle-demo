public with sharing class LEX_LookUpController {

    /** this function will fetch values for our lookup field
     *  @param search_keywordP this is the search keyword
     *  @param object_nameP this is the name of the object we are searching
     *  @return List of sObjects will be returned
     */
    @AuraEnabled
    public static List< sObject > fetchMatchingRecords( String search_keywordP, String object_nameP ){

        
        String search_value = '%'+search_keywordP + '%';                //create our search phrase
        List< sObject > return_list = new List< sObject >();        //list of values
      String query = 'SELECT Id, Name FROM ' + object_nameP + ' WHERE Name LIKE:search_value ORDER BY createdDate DESC LIMIT 10';   //setup our query

    //    String query = 'SELECT Id, Name FROM ' + object_nameP + ' WHERE Name LIKE \'%'+ searchtest +'%\'  ORDER BY createdDate DESC LIMIT 5';   //setup our query
        List< sObject > list_of_record = Database.query( query );           //execute query

        //loop through the returned list
        for( sObject current_object : list_of_record )
            return_list.add( current_object );                  //add to the return list

        return return_list;

    }//end of function definition
    
    
    @AuraEnabled
    public static List< sObject > fetchMatchingclientAccounts( String search_keywordP, String object_nameP ){

        String ClientRecordTypeID = '0120Y0000009cEvQAI';
        String ClientEcommerceRecordTypeID = '0125E000000bANcQAM';
        String search_value =  '%'+ search_keywordP + '%';                //create our search phrase
        List< sObject > return_list = new List< sObject >();        //list of values 0120Y0000009cEvQAI
        String query = 'SELECT Id, Name FROM ' + object_nameP + ' WHERE Name LIKE:search_value and (RecordTypeId =: ClientRecordTypeID or RecordTypeId =:ClientEcommerceRecordTypeID) ORDER BY createdDate DESC LIMIT 10';   //setup our query
        List< sObject > list_of_record = Database.query( query );           //execute query

        //loop through the returned list
        for( sObject current_object : list_of_record )
            return_list.add( current_object );                  //add to the return list

        return return_list;

    }//end of function definition

}//end of class definition