@isTest
public class costingLookUpControllerTest {

@testSetup static void setup() {
       
        Account account2 = new Account (name='Test Client', Type ='Client'); 
        insert account2;
     	
    }
    
    @isTest static void testFetchLookUpValues() {
        Test.startTest();
        List<Account> accList = costingLookUpController.fetchLookUpValues('Test','Account');
        Test.stopTest();
        System.assert((accList[0].Name).containsIgnoreCase('Test'));
    }
    
    
}