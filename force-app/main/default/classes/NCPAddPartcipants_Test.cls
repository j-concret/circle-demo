@IsTest
public class NCPAddPartcipants_Test {

    static testMethod void  NegativeTestForNCPUpdateCase(){
        
        
          DefaultFreightValues__c df = new DefaultFreightValues__c();
        df.Name = 'Test';
        df.Courier_Base_Rate_KG__c = 12;
        df.Courier_Dangerous_Goods_premium__c = 12;
        df.Courier_Fixed_Charge__c = 12;
        df.Courier_Non_stackable_premium__c = 12;
        df.Courier_Oversized_premium__c = 12;
        df.FF_Base_Rate_KG__c = 12;
        df.FF_Dangerous_Goods_Fixed_Charge_Premium__c = 12;
        df.FF_Dangerous_Goods_premium__c = 12;
        df.FF_Dedicated_Pickup__c = 12;
        df.FF_Fixed_Charge__c = 12;
        df.FF_Non_stackable_Fixed_Charge_Premium__c = 12;
        df.FF_Non_stackable_premium__c = 12;
        df.FF_Oversized_Fixed_Charge_Premium__c = 12;
        df.FF_Oversized_premium__c = 12;
        insert df;
        
        CountryandRegionMap__c cr = new CountryandRegionMap__c(Name ='Brazil', Country__c='Brazil', Region__c='South', European_Union__c='No');
        insert cr;
        CountryandRegionMap__c cr1 = new CountryandRegionMap__c(Name ='United States', Country__c='United States', Region__c='North America', European_Union__c='No');
        insert cr1;
        RegionalFreightValues__c rf = new RegionalFreightValues__c(
            name='North America to South', Origin__c='Brazil', 
            Preferred_Freight_Forwarder_Name__c='Stream', 
            Destination__c='Brazil', 
            Courier_Discount_Premium__c=10, 
            FF_Regional_Discount_Premium__c=10, 
            Courier_Fixed_Premium__c=10, 
            FF_Fixed_Premium__c=10, 
            Risk_Adjustment_Factor__c=1.41
        );
        insert rf;
        
          

                               
        Access_token__c accessTokenObj = new Access_token__c(
        	AccessToken_encoded__c='zmM0MDcxOTItZTJiZi00YWRhLTlkZGQtZGZhMmUxM2FiYTlj',
            Access_Token__c='cc407192-e2bf-4ada-9ddd-dfa2e13aba9c',
            Status__c='Active');
        
        Insert accessTokenObj;
        
        Account acc = new Account(Name = 'TecEx Prospective Client');
        
        Insert acc; 
        
        Contact con = new Contact(
            LastName = 'Test Contact',Client_Notifications_Choice__c='Opt-In',
            AccountId = acc.Id);
        Insert con;
        
        
        Profile prof = [select id from profile where name LIKE '%Standard%'];
        
        Id shipmentOrderRecTypeId = Schema.SObjectType.shipment_order__c.getRecordTypeInfosByName().get('Cost Estimate').getRecordTypeId();
        
        CpaRules__c cpaRule = new CpaRules__c();
        cpaRule.Country__c = 'Finland';
        cpaRule.Criteria__c = 'Default';
        
        Insert cpaRule;
        
        IOR_Price_List__c priceList = new IOR_Price_List__c();
        priceList.Destination__c = 'Finland';
        priceList.Client_Name__c = acc.Id;
        priceList.IOR_Fee__c = 10;
        
        Insert priceList;
        
        shipment_order__c shipment = new shipment_order__c();
        shipment.RecordTypeId = shipmentOrderRecTypeId;
        shipment.Destination__c = 'Finland';
        shipment.IOR_Price_List__c = priceList.Id;
        shipment.Shipment_Value_USD__c = 600;
        shipment.Account__c = acc.Id;
        
        Insert shipment;
        
        User user = new User();
        user.firstName = 'Firstname1@';
        user.lastName = 'lastname1@';
        user.profileId = prof.id;
        user.email = '123_1@test.com';
        user.username = '123_1@test.com';
        user.EmailEncodingKey = 'ISO-8859-1';
        user.Alias = 'Alias';
        user.TimeZoneSidKey = 'America/Los_Angeles';
        user.LanguageLocaleKey = 'en_US';
        user.LocaleSidKey = 'en_US';
        
        insert user;
        
         Case c = new Case(Subject='Test', Status = 'New');
        insert c;
        
        NCPAddPartcipantsWra wrapper = new NCPAddPartcipantsWra();
        wrapper.Accesstoken = accessTokenObj.Access_Token__c;
        wrapper.RecordID = c.Id;
        wrapper.AccountID = acc.Id;
        wrapper.contactId = con.Id;
        NCPAddPartcipantsWra.CaseContactRole childWrap = new NCPAddPartcipantsWra.CaseContactRole();
        childWrap.NotifyContactID = user.Id;
        wrapper.NotifyContacts = new List<NCPAddPartcipantsWra.CaseContactRole>{childWrap};
        
        RestRequest req1 = new RestRequest();
        RestResponse res1 = new RestResponse();
        req1.requestURI = '/services/apexrest/NCPAddParticipants/'; 
        req1.httpMethod = 'POST';
        req1.requestBody = Blob.valueOf(JSON.serialize(wrapper));     
        RestContext.request = req1;
        RestContext.response = res1;
        RestContext.request.addHeader('Content-Type', 'application/json');
        
        Test.startTest(); 
        	NCPAddPartcipants.NCPAddPartcipants();
        Test.stopTest();
    }
    
    static testMethod void  EmptyAccessTokenTestForNCPUpdateCase(){

          DefaultFreightValues__c df = new DefaultFreightValues__c();
        df.Name = 'Test';
        df.Courier_Base_Rate_KG__c = 12;
        df.Courier_Dangerous_Goods_premium__c = 12;
        df.Courier_Fixed_Charge__c = 12;
        df.Courier_Non_stackable_premium__c = 12;
        df.Courier_Oversized_premium__c = 12;
        df.FF_Base_Rate_KG__c = 12;
        df.FF_Dangerous_Goods_Fixed_Charge_Premium__c = 12;
        df.FF_Dangerous_Goods_premium__c = 12;
        df.FF_Dedicated_Pickup__c = 12;
        df.FF_Fixed_Charge__c = 12;
        df.FF_Non_stackable_Fixed_Charge_Premium__c = 12;
        df.FF_Non_stackable_premium__c = 12;
        df.FF_Oversized_Fixed_Charge_Premium__c = 12;
        df.FF_Oversized_premium__c = 12;
        insert df;
        
        CountryandRegionMap__c cr = new CountryandRegionMap__c(Name ='Brazil', Country__c='Brazil', Region__c='South', European_Union__c='No');
        insert cr;
        CountryandRegionMap__c cr1 = new CountryandRegionMap__c(Name ='United States', Country__c='United States', Region__c='North America', European_Union__c='No');
        insert cr1;
        RegionalFreightValues__c rf = new RegionalFreightValues__c(
            name='North America to South', Origin__c='Brazil', 
            Preferred_Freight_Forwarder_Name__c='Stream', 
            Destination__c='Brazil', 
            Courier_Discount_Premium__c=10, 
            FF_Regional_Discount_Premium__c=10, 
            Courier_Fixed_Premium__c=10, 
            FF_Fixed_Premium__c=10, 
            Risk_Adjustment_Factor__c=1.41
        );
        insert rf;
        
          

                               
      
        
        Account acc = new Account(Name = 'TecEx Prospective Client');
        
        Insert acc; 
        
        Contact con = new Contact(
            LastName = 'Test Contact',Client_Notifications_Choice__c='Opt-In',
            AccountId = acc.Id);
        Insert con;
        
        
        Profile prof = [select id from profile where name LIKE '%Standard%'];
        
        Id shipmentOrderRecTypeId = Schema.SObjectType.shipment_order__c.getRecordTypeInfosByName().get('Cost Estimate').getRecordTypeId();
        
        CpaRules__c cpaRule = new CpaRules__c();
        cpaRule.Country__c = 'Finland';
        cpaRule.Criteria__c = 'Default';
        
        Insert cpaRule;
        
        IOR_Price_List__c priceList = new IOR_Price_List__c();
        priceList.Destination__c = 'Finland';
        priceList.Client_Name__c = acc.Id;
        priceList.IOR_Fee__c = 10;
        
        Insert priceList;
        
        shipment_order__c shipment = new shipment_order__c();
        shipment.RecordTypeId = shipmentOrderRecTypeId;
        shipment.Destination__c = 'Finland';
        shipment.IOR_Price_List__c = priceList.Id;
        shipment.Shipment_Value_USD__c = 600;
        shipment.Account__c = acc.Id;
        
        Insert shipment;
        
        User user = new User();
        user.firstName = 'Firstname1@';
        user.lastName = 'lastname1@';
        user.profileId = prof.id;
        user.email = '123_1@test.com';
        user.username = '123_1@test.com';
        user.EmailEncodingKey = 'ISO-8859-1';
        user.Alias = 'Alias';
        user.TimeZoneSidKey = 'America/Los_Angeles';
        user.LanguageLocaleKey = 'en_US';
        user.LocaleSidKey = 'en_US';
        
        insert user;
        
        Case c = new Case(Subject='Test', Status = 'New');
        insert c;
        
        NCPAddPartcipantsWra wrapper = new NCPAddPartcipantsWra();
        wrapper.Accesstoken = '';
        wrapper.RecordID = c.Id;
        wrapper.AccountID = acc.Id;
        wrapper.contactId = con.Id;
        NCPAddPartcipantsWra.CaseContactRole childWrap = new NCPAddPartcipantsWra.CaseContactRole();
        childWrap.NotifyContactID = user.Id;
        wrapper.NotifyContacts = new List<NCPAddPartcipantsWra.CaseContactRole>{childWrap};
        
        RestRequest req1 = new RestRequest();
        RestResponse res1 = new RestResponse();
        req1.requestURI = '/services/apexrest/NCPAddParticipants/'; 
        req1.httpMethod = 'POST';
        req1.requestBody = Blob.valueOf(JSON.serialize(wrapper));     
        RestContext.request = req1;
        RestContext.response = res1;
        RestContext.request.addHeader('Content-Type', 'application/json');
        
        Test.startTest(); 
        	NCPAddPartcipants.NCPAddPartcipants();
        Test.stopTest();
    }
    
    static testMethod void  EmptyContactTestForNCPUpdateCase(){

          DefaultFreightValues__c df = new DefaultFreightValues__c();
        df.Name = 'Test';
        df.Courier_Base_Rate_KG__c = 12;
        df.Courier_Dangerous_Goods_premium__c = 12;
        df.Courier_Fixed_Charge__c = 12;
        df.Courier_Non_stackable_premium__c = 12;
        df.Courier_Oversized_premium__c = 12;
        df.FF_Base_Rate_KG__c = 12;
        df.FF_Dangerous_Goods_Fixed_Charge_Premium__c = 12;
        df.FF_Dangerous_Goods_premium__c = 12;
        df.FF_Dedicated_Pickup__c = 12;
        df.FF_Fixed_Charge__c = 12;
        df.FF_Non_stackable_Fixed_Charge_Premium__c = 12;
        df.FF_Non_stackable_premium__c = 12;
        df.FF_Oversized_Fixed_Charge_Premium__c = 12;
        df.FF_Oversized_premium__c = 12;
        insert df;
        
        CountryandRegionMap__c cr = new CountryandRegionMap__c(Name ='Brazil', Country__c='Brazil', Region__c='South', European_Union__c='No');
        insert cr;
        CountryandRegionMap__c cr1 = new CountryandRegionMap__c(Name ='United States', Country__c='United States', Region__c='North America', European_Union__c='No');
        insert cr1;
        RegionalFreightValues__c rf = new RegionalFreightValues__c(
            name='North America to South', Origin__c='Brazil', 
            Preferred_Freight_Forwarder_Name__c='Stream', 
            Destination__c='Brazil', 
            Courier_Discount_Premium__c=10, 
            FF_Regional_Discount_Premium__c=10, 
            Courier_Fixed_Premium__c=10, 
            FF_Fixed_Premium__c=10, 
            Risk_Adjustment_Factor__c=1.41
        );
        insert rf;
        
          

                   Access_token__c accessTokenObj = new Access_token__c(
        	AccessToken_encoded__c='zmM0MDcxOTItZTJiZi00YWRhLTlkZGQtZGZhMmUxM2FiYTlj',
            Access_Token__c='cc407192-e2bf-4ada-9ddd-dfa2e13aba9c',
            Status__c='Active');
        
        Insert accessTokenObj;            
      
        
        Account acc = new Account(Name = 'TecEx Prospective Client');
        
        Insert acc; 
        
        Contact con = new Contact(
            LastName = 'Test Contact',Client_Notifications_Choice__c='Opt-In',
            AccountId = acc.Id);
        Insert con;
        
        
        Profile prof = [select id from profile where name LIKE '%Standard%'];
        
        Id shipmentOrderRecTypeId = Schema.SObjectType.shipment_order__c.getRecordTypeInfosByName().get('Cost Estimate').getRecordTypeId();
        
        CpaRules__c cpaRule = new CpaRules__c();
        cpaRule.Country__c = 'Finland';
        cpaRule.Criteria__c = 'Default';
        
        Insert cpaRule;
        
        IOR_Price_List__c priceList = new IOR_Price_List__c();
        priceList.Destination__c = 'Finland';
        priceList.Client_Name__c = acc.Id;
        priceList.IOR_Fee__c = 10;
        
        Insert priceList;
        
        shipment_order__c shipment = new shipment_order__c();
        shipment.RecordTypeId = shipmentOrderRecTypeId;
        shipment.Destination__c = 'Finland';
        shipment.IOR_Price_List__c = priceList.Id;
        shipment.Shipment_Value_USD__c = 600;
        shipment.Account__c = acc.Id;
        
        Insert shipment;
        
        User user = new User();
        user.firstName = 'Firstname1@';
        user.lastName = 'lastname1@';
        user.profileId = prof.id;
        user.email = '123_1@test.com';
        user.username = '123_1@test.com';
        user.EmailEncodingKey = 'ISO-8859-1';
        user.Alias = 'Alias';
        user.TimeZoneSidKey = 'America/Los_Angeles';
        user.LanguageLocaleKey = 'en_US';
        user.LocaleSidKey = 'en_US';
        
        insert user;
        
        Case c = new Case(Subject='Test', Status = 'New');
        insert c;
        
        NCPAddPartcipantsWra wrapper = new NCPAddPartcipantsWra();
        wrapper.Accesstoken = accessTokenObj.Access_Token__c ;
        wrapper.RecordID = c.Id;
        wrapper.AccountID = acc.Id;
        wrapper.contactId = con.Id;
        NCPAddPartcipantsWra.CaseContactRole childWrap = new NCPAddPartcipantsWra.CaseContactRole();
        childWrap.NotifyContactID = user.Id;
        wrapper.NotifyContacts = new List<NCPAddPartcipantsWra.CaseContactRole>();//{childWrap};
        
        RestRequest req1 = new RestRequest();
        RestResponse res1 = new RestResponse();
        req1.requestURI = '/services/apexrest/NCPAddParticipants/'; 
        req1.httpMethod = 'POST';
        req1.requestBody = Blob.valueOf(JSON.serialize(wrapper));     
        RestContext.request = req1;
        RestContext.response = res1;
        RestContext.request.addHeader('Content-Type', 'application/json');
        
        Test.startTest(); 
        	NCPAddPartcipants.NCPAddPartcipants();
        Test.stopTest();
    }
}