@IsTest
public class CPCreateSOOrderPackWrapper_Test {
    static testMethod void testParse() {
		String json = '{'+
		'\"ShipmentOrder_Packages\": ['+
		'    {'+
		'      \"SOID\": \"a0R1l0000022JeM\",'+
        '      \"ClientID\": \"001D000001kdDDLIA2\",'+
        '      \"ContactID\": \"0031v00001hY02H\",'+
		'      \"Weight_Unit\": \"KGs\",'+
		'      \"Dimension_Unit\": \"CMs\",'+
		'      \"Packages_of_Same_Weight\": 1,'+
		'      \"Length\": 1.00,'+
		'      \"Height\": 1.00,'+
		'      \"Breadth\": 1.00,'+
		'      \"Actual_Weight\": 1.00'+
		'     '+
		'    },'+
		'    {'+
		'      \"SOID\": \"a0R1l0000022JeM\",'+
        '      \"ClientID\": \"001D000001kdDDLIA2\",'+
        '      \"ContactID\": \"0031v00001hY02H\",'+
		'      \"Weight_Unit\": \"KGs\",'+
		'      \"Dimension_Unit\": \"CMs\",'+
		'      \"Packages_of_Same_Weight\": 1,'+
		'      \"Length\": 1.00,'+
		'      \"Height\": 1.00,'+
		'      \"Breadth\": 1.00,'+
		'      \"Actual_Weight\": 1.00'+
		'   '+
		'    },'+
		'    {'+
		'      \"SOID\": \"a0R1l0000022JeM\",'+
        '      \"ClientID\": \"001D000001kdDDLIA2\",'+
        '      \"ContactID\": \"0031v00001hY02H\",'+
		'      \"Weight_Unit\": \"KGs\",'+
		'      \"Dimension_Unit\": \"CMs\",'+
		'      \"Packages_of_Same_Weight\": 1,'+
		'      \"Length\": 1.00,'+
		'      \"Height\": 1.00,'+
		'      \"Breadth\": 1.00,'+
		'      \"Actual_Weight\": 1.00'+
		'     '+
		'    },'+
		'    {'+
		'      \"SOID\": \"a0R1l0000022JeM\",'+
        '      \"ClientID\": \"001D000001kdDDLIA2\",'+
        '      \"ContactID\": \"0031v00001hY02H\",'+
		'      \"Weight_Unit\": \"KGs\",'+
		'      \"Dimension_Unit\": \"CMs\",'+
		'      \"Packages_of_Same_Weight\": 1,'+
		'      \"Length\": 1.00,'+
		'      \"Height\": 1.00,'+
		'      \"Breadth\": 1.00,'+
		'      \"Actual_Weight\": 1.00'+
		'     '+
		'    }'+
		''+
		'  ]'+
		'}';
		CPCreateSOOrderPackWrapper obj = CPCreateSOOrderPackWrapper.parse(json);
		System.assert(obj != null);
	}

}