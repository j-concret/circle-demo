@RestResource(urlMapping='/NCPUpdateProfilepic/*')
Global class NCPupdateProfilePic {
    
      @Httppost
        global static void NCPCreateCase(){
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        Blob body = req.requestBody;
    	res.addHeader('Content-Type', 'application/json');
        String requestString = body.toString();
        NCPupdateProfilePicWra rw  = (NCPupdateProfilePicWra)JSON.deserialize(requestString,NCPupdateProfilePicWra.class);
          Try{
              
              
              document d = new document();
              d.AuthorId = rw.UserID;
              d.Name='Profilepic';
              d.FolderId = rw.UserID;
              d.type = 'jpg'; 
      		  d.IsPublic = true;
              d.body=rw.pic;
      		  insert d;
              System.debug('Document -->'+d.Id);
              
              user u = [Select Id,ProfilePicId__c,UpdateProfilepic__c,LastModifiedById,UserPreferencesShowProfilePicToGuestUsers  from user where id =:rw.UserID];
              u.ProfilePicId__c = d.Id;
              u.UpdateProfilepic__c = true;
              u.UserPreferencesShowProfilePicToGuestUsers = true;
              Update U;
              
             
            JSONGenerator gen = JSON.createGenerator(true);
            gen.writeStartObject();
            
             gen.writeFieldName('Success');
             gen.writeStartObject();
            
             	gen.writeStringField('Success',  'Profile picture updated succesfully');
            
            gen.writeEndObject();
            gen.writeEndObject();
            String jsonData = gen.getAsString();
            res.responseBody = Blob.valueOf(jsonData);
            res.addHeader('Content-Type', 'application/json');
            res.statusCode = 200; 
              
       
              
                }
          catch(Exception e){
                String ErrorString ='Something went wrong, please contact support_SF@tecex.com';
               		res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
            		res.addHeader('Content-Type', 'application/json');
        			res.statusCode = 400;
                
                API_Log__c Al = New API_Log__c();
               // Al.Account__c=rw.AccountID;
                Al.EndpointURLName__c='NCPUpdateProfilepic';
                Al.Response__c=e.getMessage()+' at line number: '+e.getLineNumber();
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al;
              
            }
      }
  /*  
    public static Boolean updateUserProfilePic(blob userProfilePicString, String userId){
				Boolean updateSuccessful = true;

			
				try{
                    
    				Blob blobImage = userProfilePicString;

    				ConnectApi.BinaryInput fileUpload = new ConnectApi.BinaryInput(blobImage, 'image/jpg', 'userImage.jpg');
    				ConnectApi.Photo photoProfile = ConnectApi.UserProfiles.setPhoto(null, userId,  fileUpload);
					}
                        catch(Exception exc){
                            updateSuccessful = false;
                        }
                        
                        return updateSuccessful;
                        
    }
    public static ConnectApi.Photo setPhoto(String communityId, String userId, String fileId, Integer versionNumber){ 
        ConnectApi.Photo a = new ConnectApi.Photo();


return a;}
*/
    
}