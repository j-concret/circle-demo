@isTest(SeeAllData=true)
public with sharing class SO_GenerateCostEstimate_Test {

    static testmethod void testGenerateCostEstimate(){

        Account account = new Account (name='TecEx Prospective Client', Type ='Client', IOR_Payment_Terms__c = 20.00, VAT_Number__c = '1', BillingStreet= 'Billing Street', BillingCity = 'Test City', BillingState = 'Test State', BillingPostalCode = '987654', BillingCountry = 'Test Country', Total_Amount_Outstanding__c = 1000.00,
                       Total_Amount_Outstanding_0_Current__c = 2000.00, Total_Amount_Outstanding_1_30_Days__c = 2000.00, Total_Amount_Outstanding_31_60_Days__c = 10000.00, Total_Amount_Outstanding_61_90_Days__c = 80000.00, 
                       Total_Amount_Outstanding_Over_90_Days__c = 20000.00, Total_Amount_Credits__c = 100.00 ,CSE_IOR__c = '0050Y000001LTZO');
        insert account;    
    
        country_price_approval__c cpa = new country_price_approval__c( name ='Aruba', Billing_term__c = 'DAP/CIF - IOR pays',  Airwaybill_Instructions__c = 'User IOR Address');
        insert cpa;
        
        IOR_Price_List__c iorpl = new IOR_Price_List__c ( Client_Name__c = account.Id, Name = 'Aruba', IOR_Fee__c = 0.15, On_Charge_Mark_up__c = 0.15,
        TecEx_Shipping_Fee_Markup__c =0.40, IOR_Min_Fee__c = 100,  Admin_Fee__c =100, Set_up_fee__c =100, Bank_Fees__c =200, Tax_Rate__c = 0.20,
        Estimated_Customs_Brokerage__c =250, Estimated_Customs_Clearance__c =250, Estimated_Customs_Handling__c =250, Estimated_Import_License__c =250);
        insert iorpl; 
     
        Contact contact = new Contact ( lastname ='Testing Individual',  AccountId = account.Id);
        insert contact; 
        
        Opportunity opportunity = new Opportunity(AccountId = account.id, Name ='120-547', Ship_to_Country__c = cpa.Id, IOR_Price_List__c = iorpl.Id, Shipment_Value_USD__c =1000,
        Ship_From_Country__c = 'Algeria', CloseDate= system.today(), StageName ='POD Received', Client_Contact_for_this_Shipment__c = contact.Id, Who_arranges_International_courier__c ='Client', Tax_Treatment__c ='DAP/CIF - IOR pays' );
        insert opportunity;
     
        Shipment_Order__c shipmentOrder = new Shipment_Order__c(Account__c = account.id, Ship_to_Country__c = cpa.Id, IOR_Price_List__c = iorpl.Id, Shipment_Value_USD__c =1000, Opportunity__c = opportunity.Id,
        Client_Contact_for_this_Shipment__c = contact.Id, Who_arranges_International_courier__c ='Client', Tax_Treatment__c ='DAP/CIF - IOR pays', Shipping_Status__c ='Cost Estimate');
        insert shipmentOrder;
 
        test.startTest();
        Boolean quickTest = SO_GenerateCostEstimate.generateCostEstimate(shipmentOrder.Id);
        SO_GenerateCostEstimate.generateCostEstimateDocAsync(new Map<String, String> {shipmentOrder.Id => 'Test Document'});
        SO_GenerateCostEstimate.generateCostEstimateFromBatch(shipmentOrder);
        SO_GenerateCostEstimate.generateCostEstimateForAPI(shipmentOrder.Id);
        test.stopTest();
    }

}