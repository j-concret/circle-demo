@IsTest
public class profileRedirect_Test {
    static testMethod void  testgetUserInfo(){
		Test.startTest();
        profileRedirect.getUserInfo();
        Test.stopTest();
    }
    
   static testMethod void  testgetShipmentId(){
        Access_token__c accessTokenObj = new Access_token__c(
        	AccessToken_encoded__c='zmM0MDcxOTItZTJiZi00YWRhLTlkZGQtZGZhMmUxM2FiYTlj',
            Access_Token__c='cc407192-e2bf-4ada-9ddd-dfa2e13aba9c',
            Status__c='Active');
        
        Insert accessTokenObj;
        
        Account acc = new Account(Name = 'TecEx Prospective Client');
        
        Insert acc; 
        
        Contact con = new Contact(
            LastName = 'Test Contact',Client_Notifications_Choice__c='Opt-In',
            AccountId = acc.Id);
        Insert con;
        
        
        Profile prof = [select id from profile where name LIKE '%Standard%'];
        
        Id shipmentOrderRecTypeId = Schema.SObjectType.shipment_order__c.getRecordTypeInfosByName().get('Cost Estimate').getRecordTypeId();
        
        CpaRules__c cpaRule = new CpaRules__c();
        cpaRule.Country__c = 'Finland';
        cpaRule.Criteria__c = 'Default';
        
        Insert cpaRule;
        
        IOR_Price_List__c priceList = new IOR_Price_List__c();
        priceList.Destination__c = 'Finland';
        priceList.Client_Name__c = acc.Id;
        priceList.IOR_Fee__c = 10;
        
        Insert priceList;
        
        shipment_order__c shipment = new shipment_order__c();
        shipment.RecordTypeId = shipmentOrderRecTypeId;
        shipment.Destination__c = 'Finland';
        shipment.IOR_Price_List__c = priceList.Id;
        shipment.Shipment_Value_USD__c = 600;
        shipment.Account__c = acc.Id;
        
        Insert shipment;
        
        User user = new User();
        user.firstName = 'Firstname1@';
        user.lastName = 'lastname1@';
        user.profileId = prof.id;
        user.email = '123_1@test.com';
        user.username = '123_1@test.com';
        user.EmailEncodingKey = 'ISO-8859-1';
        user.Alias = 'Alias';
        user.TimeZoneSidKey = 'America/Los_Angeles';
        user.LanguageLocaleKey = 'en_US';
        user.LocaleSidKey = 'en_US';
        
        insert user;
        
         Case c = new Case(Subject='Test', Status = 'New',Shipment_Order__c=shipment.Id);
        insert c;
       Task t = new Task(Subject='Donni',Status='Not Started',Priority='Normal',Shipment_Order__c=shipment.Id);
        insert t;
        
      FeedItem f1 = new FeedItem();
        f1.ParentId = t.Id;
        f1.body = 'test';
        insert f1;
          FeedComment fc1 = new FeedComment();
      fc1.FeedItemId=f1.Id;
        fc1.CommentBody = 'legal test';
        insert fc1;
       Test.startTest();
        profileRedirect.getShipmentId(c.Id,false);
        profileRedirect.getShipmentId(t.Id,true);
        profileRedirect.getShipmentId(fc1.Id,false);
        Test.stopTest();
    }
}