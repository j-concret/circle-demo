public class Constants {
    public final static String SHIPMENT_VALUE = 'Shipment value';
    public final static String CIF_VALUE= 'CIF value';
    public final static String CHARGEABLE_WEIGHT = 'Chargeable weight';
    public final static String NO_OF_PACKAGES = '# packages';
    public final static String NO_OF_LINE_ITEMS = '# Parts (line items)';
    public final static String NO_OF_UNIQUE_LINES = '# Unique HS Codes';
    public final static String TOTAL_TAXES = 'Total Taxes';

    //Cost types
    public final static String Cost_Type_Variable = 'Variable';
    public final static String Cost_Type_Fixed = 'Fixed';
    public final static String Cost_Type_Captured = 'Captured';

    //Mathematical operators
    public final static String greater_Than = '>';
    public final static String greater_Equals_Than = '>=';
    public final static String Less_Equals_Than = '<=';
    public final static String Less_Than = '<';
    public final static String Between = 'between';
    public final static String EQUALS = 'equals';
    public final static String DOES_NOT_EQUAL = 'does not equal';
    public final static String CONTAINS = 'contains';
    public final static String DOES_NOT_CONTAINS = 'does not contain';

    public enum status {
        Good,
        Caution,
        Warning
    }

    public static boolean isEquals(String one, string two){
        return one.deleteWhitespace().equalsIgnoreCase(two.deleteWhitespace());
    }

    public static boolean isNotEquals(String one, string two){
        return !one.deleteWhitespace().equalsIgnoreCase(two.deleteWhitespace());
    }
}