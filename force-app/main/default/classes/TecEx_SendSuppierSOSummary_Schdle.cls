global class TecEx_SendSuppierSOSummary_Schdle implements Schedulable {
  global void execute(SchedulableContext sc) {
    TecEx_Summary_SO_Supplier b = new TecEx_Summary_SO_Supplier();
    database.executebatch(b);
  }
}