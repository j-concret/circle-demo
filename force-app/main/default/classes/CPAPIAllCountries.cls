@RestResource(urlMapping='/GetAllCountries/*')
Global with sharing class CPAPIAllCountries {
 @Httpget
    global static void AllCountries(){
         RestRequest req = RestContext.request;
         RestResponse res = RestContext.response;

        Schema.DescribeFieldResult F = Shipment_Order__c.Ship_From_Country__c.getDescribe();
        Schema.sObjectField T = F.getSObjectField();
        List<PicklistEntry> entries = T.getDescribe().getPicklistValues();

                 res.responseBody = Blob.valueOf(JSON.serializePretty(entries));
        	 	 res.statusCode = 200;

}
}