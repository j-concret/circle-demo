public class TH_Contact extends TH_Base {
	
	protected override map<String, String> dragonFieldMap() {
		return XF_Dragon_Request.ContactFieldMap;
	}	

    public void execute () {
   		if (Trigger.isAfter && Trigger.isUpdate) { afterUpdate(); }
   		else if (Trigger.isAfter && (Trigger.isInsert || Trigger.isUndelete)) { afterInsert(); }
   		else if (Trigger.isAfter && Trigger.isDelete) { afterDelete(); }
    }
    
	public void afterUpdate() {
		map<Contact, Contact> items = new map<Contact, Contact>(); 
		for (SObject so: Trigger.new) 
			items.put((Contact)so, (Contact)Trigger.oldMap.get(so.Id));
       	doAfterUpdate(items);
	}
	
	public void afterInsert() {
		list<Contact> items = new list<Contact>(); 
		for (SObject so: Trigger.new) 
			items.add((Contact)so);
		doAfterInsert(items);
	}
	
	public void afterDelete() {
		list<Contact> items = new list<Contact>(); 
		for (SObject so: Trigger.old) 
			items.add((Contact)so);
		doAfterDelete(items);
	}
	
	public void doAfterUpdate(map<Contact, Contact> items) {
		list<SimpleObjectMap> mappedItems = new list<SimpleObjectMap>();
		for (Contact item: items.keySet())  
			try {
	  			if (!AppUtils.isAPICall(item, items.get(item))) 
	  				if (hasChanges(items.get(item), item)) 
	  				  	mappedItems.add(SimpleObjectMap.Make(item, item.AccountId, getFields(item)));
			} catch (Exception e) {
				SysUtils.processException(e, item);
				throw e;
			}
		dragonService.upsertContacts(mappedItems, false);
	}
	
	public void doAfterInsert(list<Contact> items) { 
		list<SimpleObjectMap> mappedItems = new list<SimpleObjectMap>();
		for (Contact item: items) 
			try {
	  			if (!AppUtils.isAPICall(item))
	  				mappedItems.add(SimpleObjectMap.Make(item, item.AccountId, getFields(item)));
			} catch (Exception e) {
				SysUtils.processException(e, item);
				throw e;
			}
  		dragonService.upsertContacts(mappedItems, true);
	}
	 
	public void doAfterDelete(list<Contact> items) { 
		list<ID> deletedItems = new list<ID>();
		for (Contact item: items) 
  			// how do we determine this if this is an API call?
  			deletedItems.add(item.Id);
  		dragonService.deleteContacts(deletedItems);
	} 
}