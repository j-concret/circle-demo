public class uploadFileToTransact {
    @Future(callout=true)
    public static void uploadFile(Blob file_body, String file_name, Blob xmlBody, String xmlFilename, String reqEndPoint, String pId){
          String boundary = '----------------------------741e90d31eff';
          String header = '--'+boundary+'\nContent-Disposition: form-data; name="ImageFile"; filename="'+file_name+'";\nContent-Type: application/pdf';
          String xmlHeader = '--'+boundary+'\nContent-Disposition: form-data; name="MetadataFile"; filename="'+xmlFilename+'";\nContent-Type: application/xml';
          String footer = '--'+boundary+'--';             
          String headerEncoded = EncodingUtil.base64Encode(Blob.valueOf(header+'\r\n\r\n'));
          String xmlHeaderEncoded = EncodingUtil.base64Encode(Blob.valueOf(xmlHeader+'\r\n\r\n'));

          while(xmlHeaderEncoded.endsWith('='))
          {
           xmlHeader+=' ';
           xmlHeaderEncoded = EncodingUtil.base64Encode(Blob.valueOf(xmlHeader+'\r\n\r\n'));
          }
          String bodyEncoded = EncodingUtil.base64Encode(file_body);
          String xmlBodyEncoded = EncodingUtil.base64Encode(xmlBody);
     
          Blob bodyBlob = null;
          String last4Bytes = bodyEncoded.substring(bodyEncoded.length()-4,bodyEncoded.length());
          String last4XMLBytes = xmlBodyEncoded.substring(xmlBodyEncoded.length()-4,xmlBodyEncoded.length());

         if(last4XMLBytes.endsWith('==')) {
            last4XMLBytes = last4XMLBytes.substring(0,2) + '0K';
            xmlBodyEncoded = xmlBodyEncoded.substring(0,xmlBodyEncoded.length()-4) + last4XMLBytes;
            // We have appended the \r\n to the Blob, so leave header as it is.
          } else if(last4XMLBytes.endsWith('=')) {
            last4XMLBytes = last4XMLBytes.substring(0,3) + 'N';
            xmlBodyEncoded = xmlBodyEncoded.substring(0,xmlBodyEncoded.length()-4) + last4XMLBytes;
            // We have appended the CR e.g. \r, still need to prepend the line feed to the header
            header = '\n' + header;
            headerEncoded = EncodingUtil.base64Encode(Blob.valueOf(header+'\r\n\r\n'));
          } else {
            // Prepend the CR LF to the header
            header = '\r\n' + header;
            headerEncoded = EncodingUtil.base64Encode(Blob.valueOf(header+'\r\n\r\n'));
          }

          while(headerEncoded.endsWith('='))
          {
           header+=' ';
           headerEncoded = EncodingUtil.base64Encode(Blob.valueOf(header+'\r\n\r\n'));
          }
        
        String footerEncoded = null; 
        if(last4Bytes.endsWith('==')) {
            last4Bytes = last4Bytes.substring(0,2) + '0K';
            bodyEncoded = bodyEncoded.substring(0,bodyEncoded.length()-4) + last4Bytes;
            // We have appended the \r\n to the Blob, so leave footer as it is.
            footerEncoded = EncodingUtil.base64Encode(Blob.valueOf(footer));
            bodyBlob = EncodingUtil.base64Decode(xmlHeaderEncoded+xmlBodyEncoded+headerEncoded+bodyEncoded+footerEncoded);
          } else if(last4Bytes.endsWith('=')) {
            last4Bytes = last4Bytes.substring(0,3) + 'N';
            bodyEncoded = bodyEncoded.substring(0,bodyEncoded.length()-4) + last4Bytes;
            // We have appended the CR e.g. \r, still need to prepend the line feed to the footer
            footer = '\n' + footer;
            footerEncoded = EncodingUtil.base64Encode(Blob.valueOf(footer));
            bodyBlob = EncodingUtil.base64Decode(xmlHeaderEncoded+xmlBodyEncoded+headerEncoded+bodyEncoded+footerEncoded);              
          } else {
            // Prepend the CR LF to the footer
            footer = '\r\n' + footer;
            footerEncoded = EncodingUtil.base64Encode(Blob.valueOf(footer));
            bodyBlob = EncodingUtil.base64Decode(xmlHeaderEncoded+xmlBodyEncoded+headerEncoded+bodyEncoded+footerEncoded);  
          }
          
        System.debug('Sending Request: ' + xmlHeaderEncoded+xmlBodyEncoded+headerEncoded+bodyEncoded+footerEncoded);
        System.debug('XML Header: ' + xmlHeaderEncoded);
        System.debug('XML Body: ' + xmlBodyEncoded);
        System.debug('Header: ' + headerEncoded);
        System.debug('Body: ' + bodyEncoded);
        System.debug('Footer: ' + footerEncoded);
        
          HttpRequest req = new HttpRequest();
          req.setHeader('Content-Type','multipart/form-data; boundary='+boundary);
          req.setHeader('BatchClassID','BC8');
          req.setMethod('POST');
          req.setEndpoint(reqEndPoint);
          req.setBodyAsBlob(bodyBlob);
          req.setTimeout(120000);
     
          Http http = new Http();
          HTTPResponse res = http.send(req);
          system.debug('Response====>>>>'+res);
          system.debug('ResponseBody=====>>>>>'+res.getBody());
          
          updateEphesoftResponse.updateAtt(pId, res.getBody());
    }   
}