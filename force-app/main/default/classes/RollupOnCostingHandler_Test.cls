@isTest
public class RollupOnCostingHandler_Test {

    @testSetup
    public static void  setUpData2(){

        User userObj = [Select Id, Name, userName, Profile.Name, UserRole.name From user where Profile.Name = 'System Administrator' LIMIT 1];

        Account account2 = new Account (name='TecEx Prospective Client', Type ='Supplier', ICE__c = userObj.Id, RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('IOR/EOR Supplier').getRecordTypeId());
        insert account2;

        Contact contact = new Contact ( lastname ='Testing Individual',  AccountId = account2.Id);
        insert contact;

        Country__c country = new Country__c(Brokerage_Costs__c = 618, CIF_Adjustment_Factor__c = 0.035, CIF_Absolute_value_adjustment__c = 200, Clearance_Costs__c = 488,
                                            Country__c = 'Brazil', Name = 'Brazil', Handling_Costs__c = 1180, License_Permit_Costs__c = 0);
        insert country;

        CPA_v2_0__c relatedCPA = new CPA_v2_0__c(Name = 'Brazil', Country_Applied_Costings__c=true );
        insert relatedCPA;

        CPA_v2_0__c cpav2 = new CPA_v2_0__c(Name = 'Brazil Testing 2.0', Supplier__c = account2.Id, Service_Type__c = 'IOR/EOR', Related_Costing_CPA__c = relatedCPA.Id, Preferred_Supplier__c = TRUE,
                                            VAT_Rate__c = 0.1, Destination__c = 'Brazil',

                                            Default_Section_1_Address_Line_1__c= 'Address_Line_1',
                                            Default_Section_1_Address_Line_2__c= 'Address_Line_2',
                                            Default_Section_1_Address_Line_3__c= 'Address_Line_3',
                                            Default_Section_1_City__c= 'City',
                                            Default_Section_1_Zip__c= 'Zip',
                                            Default_Section_1_Country__c = 'Country',
                                            Default_Section_1_Other__c= 'Other',
                                            Default_Section_1_Tax_Name__c= 'Tax_Name',
                                            Default_Section_1_Tax_Id__c= 'Tax_Id',
                                            Default_Section_1_Contact_Name__c= 'Contact_Name',
                                            Default_Section_1_Contact_Email__c= 'Contact_Email',
                                            Default_Section_1_Contact_Tel__c= 'Contact_Tel',
                                            Alternate_Section_1_Address_Line_1__c= 'Address_Line_1',
                                            Alternate_Section_1_Address_Line_2__c= 'Address_Line_2',
                                            Alternate_Section_1_Address_Line_3__c= 'Address_Line_3',
                                            Alternate_Section_1_City__c= 'City',
                                            Alternate_Section_1_Zip__c= 'Zip',
                                            Alternate_Section_1_Country__c = 'Country',
                                            Alternate_Section_1_Other__c = 'Other',
                                            Alternate_Section_1_Tax_Name__c = 'Tax_Name',
                                            Alternate_Section_1_Tax_Id__c = 'Tax_Id',
                                            Alternate_Section_1_Contact_Name__c= 'Contact_Name',
                                            Alternate_Section_1_Contact_Email__c= 'Contact_Email',
                                            Alternate_Section_1_Contact_Tel__c= 'Contact_Tel',
                                            Default_Section_2_Address_Line_1__c= 'Address_Line_1',
                                            Default_Section_2_Address_Line_2__c= 'Address_Line_2',
                                            Default_Section_2_Address_Line_3__c= 'Address_Line_3',
                                            Default_Section_2_City__c= 'City',
                                            Default_Section_2_Zip__c= 'Zip',
                                            Default_Section_2_Country__c = 'Country',
                                            Default_Section_2_Other__c= 'Other',
                                            Default_Section_2_Tax_Name__c= 'Tax_Name',
                                            Default_Section_2_Tax_Id__c= 'Tax_Id',
                                            Default_Section_2_Contact_Name__c= 'Contact_Name',
                                            Default_Section_2_Contact_Email__c= 'Contact_Email',
                                            Default_Section_2_Contact_Tel__c= 'Contact_Tel',
                                            Alternate_Section_2_Address_Line_1__c= 'Address_Line_1',
                                            Alternate_Section_2_Address_Line_2__c= 'Address_Line_2',
                                            Alternate_Section_2_Address_Line_3__c= 'Address_Line_3',
                                            Alternate_Section_2_City__c= 'City',
                                            Alternate_Section_2_Zip__c= 'Zip',
                                            Alternate_Section_2_Country__c = 'Country',
                                            Alternate_Section_2_Other__c = 'Other',
                                            Alternate_Section_2_Tax_Name__c = 'Tax_Name',
                                            Alternate_Section_2_Tax_Id__c = 'Tax_Id',
                                            Alternate_Section_2_Contact_Name__c= 'Contact_Name',
                                            Alternate_Section_2_Contact_Email__c= 'Contact_Email',
                                            Alternate_Section_2_Contact_Tel__c= 'Contact_Tel',
                                            Default_Section_3_Address_Line_1__c= 'Address_Line_1',
                                            Default_Section_3_Address_Line_2__c= 'Address_Line_2',
                                            Default_Section_3_Address_Line_3__c= 'Address_Line_3',
                                            Default_Section_3_City__c= 'City',
                                            Default_Section_3_Zip__c= 'Zip',
                                            Default_Section_3_Country__c = 'Country',
                                            Default_Section_3_Other__c= 'Other',
                                            Default_Section_3_Tax_Name__c= 'Tax_Name',
                                            Default_Section_3_Tax_Id__c= 'Tax_Id',
                                            Default_Section_3_Contact_Name__c= 'Contact_Name',
                                            Default_Section_3_Contact_Email__c= 'Contact_Email',
                                            Default_Section_3_Contact_Tel__c= 'Contact_Tel',
                                            Alternate_Section_3_Address_Line_1__c= 'Address_Line_1',
                                            Alternate_Section_3_Address_Line_2__c= 'Address_Line_2',
                                            Alternate_Section_3_Address_Line_3__c= 'Address_Line_3',
                                            Alternate_Section_3_City__c= 'City',
                                            Alternate_Section_3_Zip__c= 'Zip',
                                            Alternate_Section_3_Country__c = 'Country',
                                            Alternate_Section_3_Other__c = 'Other',
                                            Alternate_Section_3_Tax_Name__c = 'Tax_Name',
                                            Alternate_Section_3_Tax_Id__c = 'Tax_Id',
                                            Alternate_Section_3_Contact_Name__c= 'Contact_Name',
                                            Alternate_Section_3_Contact_Email__c= 'Contact_Email',
                                            Alternate_Section_3_Contact_Tel__c= 'Contact_Tel',
                                            Default_Section_4_Address_Line_1__c= 'Address_Line_1',
                                            Default_Section_4_Address_Line_2__c= 'Address_Line_2',
                                            Default_Section_4_Address_Line_3__c= 'Address_Line_3',
                                            Default_Section_4_City__c= 'City',
                                            Default_Section_4_Zip__c= 'Zip',
                                            Default_Section_4_Country__c = 'Country',
                                            Default_Section_4_Other__c= 'Other',
                                            Default_Section_4_Tax_Name__c= 'Tax_Name',
                                            Default_Section_4_Tax_Id__c= 'Tax_Id',
                                            Default_Section_4_Contact_Name__c= 'Contact_Name',
                                            Default_Section_4_Contact_Email__c= 'Contact_Email',
                                            Default_Section_4_Contact_Tel__c= 'Contact_Tel',
                                            Alternate_Section_4_Address_Line_1__c= 'Address_Line_1',
                                            Alternate_Section_4_Address_Line_2__c= 'Address_Line_2',
                                            Alternate_Section_4_Address_Line_3__c= 'Address_Line_3',
                                            Alternate_Section_4_City__c= 'City',
                                            Alternate_Section_4_Zip__c= 'Zip',
                                            Alternate_Section_4_Country__c = 'Country',
                                            Alternate_Section_4_Other__c = 'Other',
                                            Alternate_Section_4_Tax_Name__c = 'Tax_Name',
                                            Alternate_Section_4_Tax_Id__c = 'Tax_Id',
                                            Alternate_Section_4_Contact_Name__c= 'Contact_Name',
                                            Alternate_Section_4_Contact_Email__c= 'Contact_Email',
                                            Alternate_Section_4_Contact_Tel__c= 'Contact_Tel',
                                            VAT_Reclaim_Destination__c = FALSE,
                                            Country__c = country.id

                                            );
        insert cpav2;

        country_price_approval__c cpa = new country_price_approval__c( name ='Brazil', Billing_term__c = 'DAP/CIF - IOR pays',  Airwaybill_Instructions__c = 'User IOR Address', Supplier__c = account2.Id,
                                                                       In_Country_Specialist__c = userObj.Id, Clearance_Destination__c = 'Brazil', Preferred_Supplier__c = TRUE, Destination__c = 'Brazil' );
        insert cpa;

        IOR_Price_List__c iorpl = new IOR_Price_List__c ( Client_Name__c = account2.Id, Name = 'Brazil', IOR_Fee__c = 0.15, On_Charge_Mark_up__c = 0.15,
                                                          TecEx_Shipping_Fee_Markup__c =0.40, IOR_Min_Fee__c = 100,  Admin_Fee__c =100, Set_up_fee__c =100, Bank_Fees__c =200, Tax_Rate__c = 0.20,
                                                          Estimated_Customs_Brokerage__c =250, Estimated_Customs_Clearance__c =250, Estimated_Customs_Handling__c =250, Estimated_Import_License__c =250, Destination__c = 'Brazil' );
        insert iorpl;

        Shipment_Order__c shipmentOrder = new Shipment_Order__c(Account__c = account2.id, Shipment_Value_USD__c = 2000, CPA_v2_0__c = cpav2.Id,of_packages__c = 100,
                                                                Client_Contact_for_this_Shipment__c = contact.Id, Who_arranges_International_courier__c ='Client', Tax_Treatment__c ='DAP/CIF - IOR pays',
                                                                Ship_to_Country__c = cpa.Id, Destination__c = 'Brazil', Service_Type__c = 'IOR', IOR_Price_List__c = iorpl.Id, Chargeable_Weight__c = 200,
                                                                Shipping_Status__c = 'Cost Estimate Abandoned' );
        insert shipmentOrder;

        Currency_Management2__c CM = new Currency_Management2__c (Name = 'US Dollar (USD)', Currency__c = 'US Dollar (USD)', ISO_Code__c = 'EUR', Conversion_Rate__c = 1);
        insert CM;

        Supplier_Invoice__c SIn = New Supplier_invoice__C(Name='SI001',Due_date__c= date.today(),Supplier_Name__c=account2.Id,Invoice_Currency__c='US Dollar (USD)',Shipment_Order_Name__c=shipmentOrder.id,Posted__c= false,Incurred__c=False);
        insert SIn;

        DateTime dT = System.now();
        Date myDate = date.newinstance(dT.year(), dT.month(), dT.day());
        Invoice_New__c SI = new Invoice_New__c(
            Invoice_Name__c = 'Invoice',
            Account__c = account2.Id,
            Invoice_Date__c = myDate,
            Invoice_Currency__c = CM.Currency__C,
            Conversion_Rate__c = CM.Conversion_rate__C,
            Shipment_Order__c = shipmentOrder.Id,
            Invoice_Type__c = 'Invoice',
            Invoice_Status__c = 'New',
            POD_Date_New__c = myDate,
            RecordTypeId = Schema.Sobjecttype.Invoice_New__c.getRecordTypeInfosByDeveloperName().get('Supplier').getRecordTypeId());

        Insert SI;
        // no supplier invoice attached to costing
        List<CPA_Costing__c> costsSO1 = new List<CPA_Costing__c>();

        costsSO1.add(new CPA_Costing__c(recordtypeid = Schema.Sobjecttype.CPA_Costing__c.getRecordTypeInfosByDeveloperName().get('Display').getRecordTypeId(),
                                        Exchange_rate_forecast__c=1,
                                        Invoice_amount_local_currency__c=10,Amount__C=100,Cost_Category__c = 'Licenses',
                                        Manual_Costing_Created__c =true,Trigger__c=FALSE, Applied_to__c = 'Shipment value',
                                        Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'IOR',
                                        Conditional_value__c = 'CIF value', Condition__c = '>=', Floor__c = 5000,
                                        Currency__c = 'US Dollar (USD)',Shipment_Order_Old__c=shipmentOrder.id,CPA_v2_0__c = RelatedCPA.ID,
                                        Invoice__c = SI.Id,Variable_threshold__c=10,VAT_Rate__c=20,Additional_Percent__c=10));

        Insert costsSO1;

    }

    public static testMethod void beforeUpdate_Test(){

        Test.startTest();

        List<CPA_Costing__c> costsSO1 = [Select recordtypeid, Exchange_rate_forecast__c, Invoice_amount_local_currency__c, Amount__C,Cost_Category__c,
                                         Manual_Costing_Created__c,Trigger__c, Applied_to__c,
                                         Cost_Type__c,   Rate__c, IOR_EOR__c,
                                         Conditional_value__c, Condition__c, Floor__c,
                                         Currency__c,Shipment_Order_Old__c,CPA_v2_0__c,
                                         Invoice__c,Variable_threshold__c,VAT_Rate__c,Additional_Percent__c from CPA_Costing__c];

        Map<Id, CPA_Costing__c> oldMap = new Map<Id, CPA_Costing__c>();

        CPA_Costing__c oldCPAC = [Select Rate__c, VAT_Rate__c, Variable_threshold__c, Additional_Percent__c, Applied_to__c from CPA_Costing__c LIMIT 1];

        oldCPAC.Rate__c = 5;
        oldCPAC.VAT_Rate__c = 10;
        oldCPAC.Variable_threshold__c = 5;
        oldCPAC.Additional_Percent__c = 8;
        oldCPAC.Applied_to__c = 'CIF value';

        oldMap.put(oldCPAC.Id, oldCPAC);

        RollupOnCostingHandler.beforeUpdate(costsSO1, oldMap);

        costsSO1[0].Applied_to__c = 'Total Taxes';
        RollupOnCostingHandler.beforeUpdate(costsSO1, oldMap);

        costsSO1[0].Applied_to__c = 'Chargeable weight';
        RollupOnCostingHandler.beforeUpdate(costsSO1, oldMap);

        costsSO1[0].Applied_to__c = '# Parts (line items)';
        RollupOnCostingHandler.beforeUpdate(costsSO1, oldMap);

        costsSO1[0].Applied_to__c = '# of Final Deliveries';
        RollupOnCostingHandler.beforeUpdate(costsSO1, oldMap);

        Test.stopTest();

        CPA_Costing__c cpaCosting = [Select Id, Amount__c from CPA_Costing__c LIMIT 1];
        System.assert (cpaCosting.Amount__c != null);
    }

    public static testMethod void afterInsert_Test(){

        Test.startTest();

        List<CPA_Costing__c> costsSO1 = [Select recordtypeid, Supplier_Invoice__c,Exchange_rate_forecast__c, Invoice_amount_local_currency__c, Amount__C,Cost_Category__c,
                                         Manual_Costing_Created__c,Trigger__c, Applied_to__c,
                                         Cost_Type__c,   Rate__c, IOR_EOR__c,
                                         Conditional_value__c, Condition__c, Floor__c,
                                         Currency__c,Shipment_Order_Old__c,CPA_v2_0__c,
                                         Invoice__c,Variable_threshold__c,VAT_Rate__c,Additional_Percent__c from CPA_Costing__c];
        Supplier_Invoice__c SI = [Select Id from Supplier_Invoice__c LIMIT 1];

        RollupOnCostingHandler.afterInsert(costsSO1);

        costsSO1[0].Invoice__c = null;
        costsSO1[0].Supplier_Invoice__c = SI.Id;
        RollupOnCostingHandler.afterInsert(costsSO1);

        Test.stopTest();

        Invoice_New__c invoice = [Select Id, Roll_Up_Costings__c from Invoice_New__c LIMIT 1];

        System.assertEquals(invoice.Roll_Up_Costings__c, false);
    }

    public static testMethod void afterUpdate_Test(){

        Test.startTest();

        List<CPA_Costing__c> costsSO1 = [Select recordtypeid, Supplier_Invoice__c, Exchange_rate_forecast__c, Invoice_amount_local_currency__c, Amount__c,Cost_Category__c,
                                         Manual_Costing_Created__c,Trigger__c, Applied_to__c,
                                         Cost_Type__c,   Rate__c, IOR_EOR__c,
                                         Conditional_value__c, Condition__c, Floor__c,
                                         Currency__c,Shipment_Order_Old__c,CPA_v2_0__c,
                                         Invoice__c,Variable_threshold__c,VAT_Rate__c,Additional_Percent__c from CPA_Costing__c];

        Supplier_Invoice__c SI = [Select Id from Supplier_Invoice__c LIMIT 1];

        Map<Id, CPA_Costing__c> oldMap = new Map<Id, CPA_Costing__c>();

        CPA_Costing__c oldCPAC = [Select Invoice_amount_local_currency__c, Amount__c, Cost_Category__c, Shipment_Order_Old__c from CPA_Costing__c LIMIT 1];

        oldCPAC.Invoice_amount_local_currency__c = 5;
        oldCPAC.Amount__c = 50;
        oldCPAC.Cost_Category__c = 'Finance';

        oldMap.put(oldCPAC.Id, oldCPAC);

        costsSO1[0].Supplier_Invoice__c = SI.Id;
        costsSO1[0].Invoice__c = null;

        RollupOnCostingHandler.afterUpdate(costsSO1, oldMap);

        costsSO1[0].Supplier_Invoice__c = null;
        costsSO1[0].Invoice__c = null;

        RollupOnCostingHandler.afterUpdate(costsSO1, oldMap);

        Test.stopTest();

        Supplier_Invoice__c supplier = [Select Id, Roll_Up_Costings__c from Supplier_Invoice__c LIMIT 1];
        System.assertEquals(supplier.Roll_Up_Costings__c, false);
    }

    public static testMethod void afterDelete_Test(){

        Test.startTest();

        List<CPA_Costing__c> costsSO1 = [Select recordtypeid, Supplier_Invoice__c, Exchange_rate_forecast__c, Invoice_amount_local_currency__c, Amount__C,Cost_Category__c,
                                         Manual_Costing_Created__c,Trigger__c, Applied_to__c,
                                         Cost_Type__c,   Rate__c, IOR_EOR__c,
                                         Conditional_value__c, Condition__c, Floor__c,
                                         Currency__c,Shipment_Order_Old__c,CPA_v2_0__c,
                                         Invoice__c,Variable_threshold__c, DeleteAll__c,VAT_Rate__c,Additional_Percent__c from CPA_Costing__c LIMIT 1];

        Supplier_Invoice__c SI = [Select Id from Supplier_Invoice__c LIMIT 1];

        RollupOnCostingHandler.afterDelete(costsSO1);

        costsSO1[0].Supplier_Invoice__c = null;
        costsSO1[0].Invoice__c = null;
        costsSO1[0].DeleteAll__c = false;

        RollupOnCostingHandler.afterDelete(costsSO1);

        Test.stopTest();
    }

    public static testMethod void updateRollupAdmin_Test(){

        Test.startTest();
        Shipment_Order__c SO = [Select Id from Shipment_Order__c LIMIT 1];

        Invoice_New__c SI = [Select Id from Invoice_New__c LIMIT 1];

        CPA_v2_0__c RelatedCPA = new CPA_v2_0__c(Name = 'Brazil', Country_Applied_Costings__c=true );
        insert RelatedCPA;

        List<CPA_Costing__c> costsSO1 = new List<CPA_Costing__c>();

        costsSO1.add(new CPA_Costing__c(recordtypeid = Schema.Sobjecttype.CPA_Costing__c.getRecordTypeInfosByDeveloperName().get('Display').getRecordTypeId(),
                                        Exchange_rate_forecast__c=1,
                                        Invoice_amount_local_currency__c=10,Amount__C=100,Cost_Category__c = 'Admin',
                                        Manual_Costing_Created__c =true,Trigger__c=FALSE, Applied_to__c = 'Shipment value',
                                        Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'IOR',
                                        Conditional_value__c = 'CIF value', Condition__c = '>=', Floor__c = 5000,
                                        Currency__c = 'US Dollar (USD)',Shipment_Order_Old__c=SO.id,CPA_v2_0__c = RelatedCPA.ID,
                                        Invoice__c = SI.Id,Variable_threshold__c=10,VAT_Rate__c=20,Additional_Percent__c=10));


        costsSO1.add(new CPA_Costing__c(recordtypeid = Schema.Sobjecttype.CPA_Costing__c.getRecordTypeInfosByDeveloperName().get('Display').getRecordTypeId(),
                                        Exchange_rate_forecast__c=1,
                                        Invoice_amount_local_currency__c=10,Amount__C=100,Cost_Category__c = 'Bank',
                                        Manual_Costing_Created__c =true,Trigger__c=FALSE, Applied_to__c = 'Shipment value',
                                        Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'IOR',
                                        Conditional_value__c = 'CIF value', Condition__c = '>=', Floor__c = 5000,
                                        Currency__c = 'US Dollar (USD)',Shipment_Order_Old__c=SO.id,CPA_v2_0__c = RelatedCPA.ID,
                                        Invoice__c = SI.Id,Variable_threshold__c=10,VAT_Rate__c=20,Additional_Percent__c=10));

        Insert costsSO1;


        RollupOnCostingHandler.updateActualRollupAdmin(new Set<Id> {SO.Id});
        Test.stopTest();

        Shipment_Order__c soObj = [Select Id, FC_Finance_Fee__c, FC_Bank_Fees__c from Shipment_Order__c LIMIT 1];

        System.assert (soObj != null);

    }

}