@IsTest
public class LeadsWebTableControllerTest {
    @TestSetup
    static void makeData(){
        Lead rec = new Lead(FirstName = 'firstName',
                            LastName = 'lastName',
                            Email='email@gmail.com',
                            Company='company',
                            LeadSource='Xant',
                            Country__c='Canada',Client_Type__c = 'Reseller',
                            Phone='1234567890',Status = 'New Lead');
        insert rec;
        
        CountryandRegionMap__c setting = new CountryandRegionMap__c();
        setting.Region_for_Company__c = 'US';
        setting.Country__c = 'Canada';
        setting.Name = 'Canada';
        insert setting;
    }
    static testMethod void getPicklistValuesTest(){
        Test.startTest();
        Map<String,Object> values = LeadsWebTableController.getPicklistsValues('Account', new List<String>{'Type'});
        Test.stopTest();
    }

     static testMethod void checkLeadsDataTest() {
         
        Lead record = [SELECT FirstName,LastName,Email,Company FROM LEAD LIMIT 1];
        Test.startTest();
        List<String> leadsData = LeadsWebTableController.checkLeadsData(new List<String>{record.FirstName}, new List<String>{record.LastName}, new List<String>{record.Email}, new List<String>{record.Company});
        System.assertEquals(2,leadsData.size());
        Test.stopTest();
    }

     static testMethod void newLeadInsertTest() {
        
        Map<String, String> mapName = new Map<String, String>();
        mapName.put('First Name' , 'firstName1');
        mapName.put('Last Name' , 'lastName1');
        mapName.put('Email' , 'email1@gmail.com');
        mapName.put('Company' , 'company1');
        mapName.put('Xant/Not Xant' , 'Xant');
        mapName.put('Country' , 'Canada');
        mapName.put('Client Type' , 'Reseller');
        mapName.put('Phone' , '1234567890');
        mapName.put('Status' , 'New Lead');
        List<Map<String, String>> listName = new List<Map<String, String>>();
        listName.add(mapName);

        Test.startTest();
        Map<String,object> response = LeadsWebTableController.newLeadInsert(JSON.serialize(listName));
        System.assertEquals(1,response.size());
        Test.stopTest();
    }

     static testMethod void getLeadOwnersTest() {
        Test.startTest();
        Map<String,String> users = LeadsWebTableController.getLeadOwners();
        System.assert(users.size() > 0);
        Test.stopTest();
    }
    
    static testMethod void getValidDomainsTest(){
        Company__c com = new Company__c();
        com.Extra_Leads_Approved__c = true;
        com.Name = 'company1';
        com.country__c = 'Canada';
        com.region__c = 'US';
        insert com;
        
        Domain__c newDom = new Domain__c();
        newDom.Name = 'company1.com';
        newDom.Company__c = com.id;
        
        insert newDom;
        
        Test.startTest();
        
        Map<String,Object> data = LeadsWebTableController.getValidDomains(new List<String>{'Canada'},new List<String>{'company1.com'}, '{"0051v000008GNROAA4" : "company1"}');
        System.assert(data.size() > 0);
    }
}