public with sharing class SO_GenerateCostEstimate {
    
    @AuraEnabled
    public static Boolean generateCostEstimate(Id soId){
        System.debug('Inside generateCostEstimate');
        system.debug('soId-->'+soId);
        try {
            List<String> excludeStatus = new List<String> {'Cost Estimate Abandoned','Shipment Abandoned','Roll-Out'};
                List<Shipment_order__c> shipments =  [SELECT Id, Name, RecordType.Name, Cost_Estimate_Number__c FROM Shipment_Order__c WHERE Shipping_status__c NOT IN:excludeStatus AND Id =:soId];
            
            if(shipments.isEmpty()) throw new AuraHandledException('Not a valid record ,please check its status!');
            
            String soCENumber = shipments[0].Cost_Estimate_Number__c;
            
            PageReference costEstimatePdf = shipments[0].RecordType.Name == 'Zee Shipment Order' || shipments[0].RecordType.Name == 'Zee Cost Estimate' ? new PageReference(Site.getPathPrefix() + '/apex/ZeeQuote') : new PageReference(Site.getPathPrefix() + '/apex/acePDF');
            costEstimatePdf.getParameters().put('id', soId);
            Blob pdfPageBlob = (Test.isRunningTest()) ? Blob.valueOf(String.valueOf('This is a test page')) : costEstimatePdf.getContentAsPDF();
            
            /* Attachment costEstimateAttachment = new Attachment();
costEstimateAttachment.Body = pdfPageBlob;
costEstimateAttachment.Name = 'Cost Estimate No.'+ soCENumber + '.pdf';
costEstimateAttachment.ParentId = soId;
costEstimateAttachment.IsPrivate = false;
costEstimateAttachment.Description = 'Cost Estimate'; */
            
            
            String title = 'Cost Estimate No:'+ soCENumber;
            List<ContentVersion> contVers = [SELECT Id,ContentDocumentId FROM ContentVersion WHERE FirstPublishLocationId=:SoId AND Title=: title LIMIT 1];
            
            ContentVersion contentVersion = new ContentVersion(
                versionData = pdfPageBlob,
                title = title,
                pathOnClient = title + '.pdf',
                
                origin = 'C', ReasonForChange = 'Manually Generated');
            
            if(!contVers.isEmpty()) {
                contentVersion.ContentDocumentId = contVers[0].ContentDocumentId;
            }else{
                contentVersion.FirstPublishLocationId =  soId;
            }
            insert contentVersion;
            // insert costEstimateAttachment;
            return true;
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }
    
    public static void generateCostEstimateFromBatch(Shipment_Order__c Shipment){
        
        try {
            String soCENumber = shipment.Cost_Estimate_Number__c;
            PageReference costEstimatePdf = new PageReference(Site.getPathPrefix() + '/apex/acePDF');
            costEstimatePdf.getParameters().put('id', shipment.Id);
            Blob pdfPageBlob = (Test.isRunningTest()) ? Blob.valueOf(String.valueOf('This is a test page')) : costEstimatePdf.getContentAsPDF();
            
            String title = 'Cost Estimate No:'+ soCENumber;
            List<ContentVersion> contVers = [SELECT Id,ContentDocumentId FROM ContentVersion WHERE FirstPublishLocationId=:shipment.Id AND Title=: title LIMIT 1];
            
            ContentVersion contentVersion = new ContentVersion(
                versionData = pdfPageBlob,
                title = title,
                pathOnClient = title + '.pdf',
                origin = 'C');
            
            if(!contVers.isEmpty()) {
                contentVersion.ContentDocumentId = contVers[0].ContentDocumentId;
            }else{
                contentVersion.FirstPublishLocationId =  shipment.Id;
            }
            insert contentVersion;
            
        } catch (Exception e) {
            System.debug('SO_generate Error :'+e.getMessage());
        }
    }
    
    @Future(callout=true)
    public static void generateCostEstimateDocAsync(String profileId, Map<String,String> soMapForCEPDF){
        System.debug('Generate CE PDF generateCostEstimateDocAsync');
        Profile prof= [select Id, name from Profile where id =:profileId];
        network nt;
        if(prof.name == 'Tecex Customer')
        	 nt= [select Id, name from network where name = 'TecEx App'];
        else if(prof.name == 'Zee Customer')
             nt= [select Id, name from network where name = 'Zee App'];
        
        Map<String,Id> docIds = new Map<String,Id>();
        List<String> excludeStatus = new List<String> {'Cost Estimate Abandoned','Shipment Abandoned','Roll-Out'};
        
        List<ContentVersion> docs = new List<ContentVersion>();
        
        for(ContentVersion cntVer : [SELECT Id,ContentDocumentId,Title,FirstPublishLocationId FROM ContentVersion WHERE FirstPublishLocationId IN:soMapForCEPDF.keySet()]) {
            docIds.put(cntVer.FirstPublishLocationId+''+cntVer.Title,cntVer.ContentDocumentId);
        }
       
        for(Shipment_order__c shipment : [SELECT Id, RecordType.Name, Name, Cost_Estimate_Number__c FROM Shipment_Order__c WHERE Shipping_status__c NOT IN:excludeStatus AND Id IN:soMapForCEPDF.keySet()]) {
            
            String title = 'Cost Estimate No:'+ shipment.Cost_Estimate_Number__c;
            PageReference costEstimatePdf = shipment.RecordType.Name == 'Zee Shipment Order' || shipment.RecordType.Name == 'Zee Cost Estimate' ? new PageReference(Site.getPathPrefix() + '/apex/ZeeQuote') : new PageReference(Site.getPathPrefix() + '/apex/acePDF');
            costEstimatePdf.getParameters().put('id', shipment.Id);
            Blob pdfPageBlob = (Test.isRunningTest()) ? Blob.valueOf(String.valueOf('This is a test page')) : costEstimatePdf.getContentAsPDF();
            
            ContentVersion contentVersion = new ContentVersion(
                versionData = pdfPageBlob,
                title = title,
                pathOnClient = title + '.pdf',
                origin = 'C');
            
            if(soMapForCEPDF.containsKey(shipment.Id)){
                contentVersion.ReasonForChange = soMapForCEPDF.get(shipment.Id);
            }            
            
            if(docIds.containsKey(shipment.Id+''+title)) {
                contentVersion.ContentDocumentId = docIds.get(shipment.Id+''+title);
            }else{
                contentVersion.FirstPublishLocationId =  shipment.Id;
            }
            if(nt != null) 
                contentVersion.NetworkId = nt.Id;
            docs.add(contentVersion);
        }
        insert docs;
    }
    
    
    
  @Future(callout=true)
    public static void generateCostEstimateForAPI(Id SoId){
        
        //network nt= [select Id, name from network where name = 'New-ClientPortal'];
     		network nt= [select Id, name from network where name = 'TecEx App'];
     
                system.debug('#communityId-->'+nt.id);
        
        String soCENumber = (!Test.isRunningTest() ? [SELECT Id, Name, Cost_Estimate_Number__c FROM Shipment_Order__c WHERE Shipping_status__c = 'Cost Estimate' AND Id =:SoId LIMIT 1].Cost_Estimate_Number__c:'test');
        system.debug('#122');
        PageReference costEstimatePdf = new PageReference(Site.getPathPrefix() + '/apex/acePDF');
        costEstimatePdf.getParameters().put('id', SoId);
        Blob pdfPageBlob = (Test.isRunningTest()) ? Blob.valueOf(String.valueOf('This is a test page')) : costEstimatePdf.getContentAsPDF();
      
        system.debug('#Site.getPathPrefix-->'+Site.getPathPrefix());
        ContentVersion contentVersion;
        try {
            String title = 'Cost Estimate No:'+ soCENumber;
            List<ContentVersion> contVers = [SELECT Id,ContentDocumentId FROM ContentVersion WHERE FirstPublishLocationId=:SoId AND Title=: title LIMIT 1];
            
            contentVersion = new ContentVersion(
                versionData = pdfPageBlob,
                title = title,
                pathOnClient = title + '.pdf',
                origin = 'C');
            
            if(!contVers.isEmpty()) {
                contentVersion.ContentDocumentId = contVers[0].ContentDocumentId;
            }else{
                contentVersion.FirstPublishLocationId =  SoId;
            }
            
            if(String.isNotBlank(nt.id)) contentVersion.NetworkId =nt.id;
            
            insert contentVersion;
          
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
        
    }
}