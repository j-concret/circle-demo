@isTest
public class Casefeed_Test {
	
    public static testMethod void NCPCasefeed_Test(){
        
        //Test Data 
        
        Access_token__c at = new Access_token__c();
        at.Status__c = 'Active';
        at.Access_Token__c = '1234';
        
        Insert at;
        
        string fileBody = 'My File Body';
        
        //Json Body
        String json = '{"AccessToken":"'+at.Access_Token__c+'","Atts":[{"filename":"Name","filebody":"'+fileBody+'"}]}';
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
        req.requestURI = '/services/apexrest/NCPcasefeedAttachments'; 
        req.httpMethod = 'Post';
        req.addHeader('Content-Type', 'application/json');
        req.requestBody = Blob.valueof(json);
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
        Casefeed.NCPCasefeed();
        Test.stopTest(); 
    }
    
    public static testMethod void NCPCasefeed_Test1(){
        
        //Test Data 
        
        Access_token__c at = new Access_token__c();
        at.Status__c = 'Active';
        at.Access_Token__c = '1234';
        
        Insert at;
        
        string fileBody = '';
        
        //Json Body
        String json = '{"AccessToken":"'+at.Access_Token__c+'","Atts":[{"filename":"Null","filebody":"'+fileBody+'"}]}';
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
        req.requestURI = '/services/apexrest/NCPcasefeedAttachments'; 
        req.httpMethod = 'Post';
        req.addHeader('Content-Type', 'application/json');
        req.requestBody = Blob.valueof(json);
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
        Casefeed.NCPCasefeed();
        Test.stopTest(); 
    }
}