@isTest
public class generateCCDDetailsWrapper_Test {
    
    @isTest
    public static void testgetDetails(){
        Account account = new Account (name='Acme1',  Type ='Client',  CSE_IOR__c = '0050Y000001km5c',  Service_Manager__c = '0050Y000001km5c',  Tax_recovery_client__c  = FALSE, Ship_From_Line_1__c = 'Ship_From_Line_1',
                                       Ship_From_Line_2__c = 'Ship_From_Line_2', Ship_From_Line_3__c = 'Ship_From_Line_3', Ship_From_City__c = 'Ship_From_City',  Ship_From_Zip__c = 'Ship_From_Zip',
                                       Ship_From_Country__c = 'Ship_From_Country',  Ship_From_Other_notes__c = 'Ship_From_Other', Ship_From_Contact_Name__c = 'Ship_From_Contact', Ship_From_Contact_Email__c = 'Ship_From_Email',
                                       Ship_From_Contact_Tel__c = 'Ship_From_Tel', Client_Address_Line_1__c = 'Client_Address_Line_1', Client_Address_Line_2__c = 'Client_Address_Line_2', Client_Address_Line_3__c = 'Client_Address_Line_3',
                                       Client_City__c = 'Client_City', Client_Zip__c = 'Client_Zip', Client_Country__c = 'Client_Country',  Other_notes__c = 'Other_notes',  Tax_Name__c = 'Tax_Name', Tax_ID__c = 'Tax_ID',
                                       Contact_Name__c = 'Contact_Name', Contact_Email__c = 'Contact_Email', Contact_Tel__c = 'Contact_Tel' ); insert account;
        
        
        Account account2 = new Account (name='Test Supplier', Type ='Supplier', ICE__c = '0050Y000001km5c', RecordTypeId = '0120Y0000006KjQQAU'); insert account2;
        
        Contact contact = new Contact ( lastname ='Testing Individual',  AccountId = account.Id); insert contact;
        
        IOR_Price_List__c iorpl = new IOR_Price_List__c ( Client_Name__c = account.Id, Name = 'Brazil', IOR_Fee__c = 0.15, On_Charge_Mark_up__c = 0.15,
                                                         TecEx_Shipping_Fee_Markup__c =0.40, IOR_Min_Fee__c = 100,  Admin_Fee__c =100, Set_up_fee__c =100, Bank_Fees__c =200, Tax_Rate__c = 0.20,
                                                         Estimated_Customs_Brokerage__c =250, Estimated_Customs_Clearance__c =250, Estimated_Customs_Handling__c =250, Estimated_Import_License__c =250,
                                                         Destination__c = 'Brazil' ); insert iorpl;
        
        country_price_approval__c cpa = new country_price_approval__c( name ='Brazil', Billing_term__c = 'DAP/CIF - IOR pays',  Airwaybill_Instructions__c = 'User IOR Address', Supplier__c = account2.Id,
                                                                      In_Country_Specialist__c = '0050Y000001km5c', Clearance_Destination__c = 'Brazil', Preferred_Supplier__c = TRUE,
                                                                      Destination__c = 'Brazil' ); insert cpa;
        
        
        
        Country__c country = new Country__c(Brokerage_Costs__c = 618, CIF_Adjustment_Factor__c = 0.035, CIF_Absolute_value_adjustment__c = 200, Clearance_Costs__c = 488,
                                            Country__c = 'Brazil', Name = 'Brazil', Handling_Costs__c = 1180, License_Permit_Costs__c = 0); insert country;
        
        CPA_v2_0__c aCpav = new CPA_v2_0__c(Name = 'Brazil', Country_Applied_Costings__c = TRUE); insert aCpav;
        
        CPA_v2_0__c cpav2 = new CPA_v2_0__c(Name = 'Brazil Testing 2.0', Supplier__c = account2.Id, Service_Type__c = 'IOR/EOR', Related_Costing_CPA__c = aCpav.Id,
                                            Preferred_Supplier__c = TRUE, VAT_Rate__c = 0.1, Destination__c = 'Brazil',  Final_Destination__c = 'Brazil',
                                            VAT_Reclaim_Destination__c = FALSE, Country__c = country.id, Lead_ICE__c = '0050Y000001km5c'); insert cpav2;
        
        
        Currency_Management2__c CM = new Currency_Management2__c (Name = 'US Dollar (USD)', Currency__c = 'US Dollar (USD)', ISO_Code__c = 'USD', Conversion_Rate__c = 1);
        insert CM;
        
        
        
        
        
        Shipment_Order__c shipmentOrder = new Shipment_Order__c(Account__c = account.id, Shipment_Value_USD__c = 2000, CPA_v2_0__c = cpav2.Id,
                                                                Client_Contact_for_this_Shipment__c = contact.Id, Who_arranges_International_courier__c ='Client', Tax_Treatment__c ='DAP/CIF - IOR pays',
                                                                Ship_to_Country__c = cpa.Id, Destination__c = 'Brazil', Service_Type__c = 'IOR', IOR_Price_List__c = iorpl.Id, Chargeable_Weight__c = 200,
                                                                Shipping_Status__c = 'Cost Estimate Abandoned', Ship_From_Country__c = 'Angola' );
        insert shipmentOrder;
        
        Supplier_Invoice__c supplier_Invoice = New Supplier_invoice__C(Name='SI001',Due_date__c= date.today(),Supplier_Name__c=account2.Id,Invoice_Currency__c='US Dollar (USD)',Shipment_Order_Name__c=shipmentOrder.Id,Posted__c= false,Incurred__c=False);
        insert supplier_Invoice;
        
        Customs_Clearance_Documents__c customs_Clearance_Documents = new Customs_Clearance_Documents__c(Name='Test CCD',Customs_Clearance_Documents__c=shipmentOrder.Id,Date_of_clearance__c=System.today() + 5);
        insert customs_Clearance_Documents;
        
        Tax_Calculation__c tax_Calculation = new Tax_Calculation__c(Tax_Name__c='Test Tax 1',Shipment_Order__c=shipmentOrder.Id,Value__c=123,Rate__c=50,Applied_To_Value__c=677);
        insert tax_Calculation;
        
        Part__c part = new Part__c(Name ='CAB-ETH-S-RJ45',  Quantity__c= 1, Commercial_Value__c = 15000, US_HTS_Code__c = '85444210', 
        Country_of_Origin2__c = 'China', Shipment_Order__c = shipmentOrder.Id, Description_and_Functionality__c ='AB-ETH-S-RJ45');
        insert part;
        
        List<ContentVersion> contentVersionList = new List<ContentVersion>();
        contentVersionList.add(new ContentVersion(
            Title = 'Penguins',
            PathOnClient = 'Penguins.jpg',
            VersionData = Blob.valueOf('Test Content'),
            IsMajorVersion = true
        ));
        contentVersionList.add(new ContentVersion(
            Title = 'Cat',
            PathOnClient = 'Cat.jpg',
            VersionData = Blob.valueOf('Test Content1'),
            IsMajorVersion = true
        ));
        insert contentVersionList;  
        
        List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];
        
        List<ContentDocumentLink> contentDocumentLinkList = new List<ContentDocumentLink>();
        contentDocumentLinkList.add(new ContentDocumentLink(LinkedEntityId=customs_Clearance_Documents.id,ContentDocumentId=documents[0].Id,shareType = 'V'));
        contentDocumentLinkList.add(new ContentDocumentLink(LinkedEntityId=shipmentOrder.id,ContentDocumentId=documents[0].Id,shareType = 'V'));
        contentDocumentLinkList.add(new ContentDocumentLink(LinkedEntityId=shipmentOrder.id,ContentDocumentId=documents[1].Id,shareType = 'V'));
        contentDocumentLinkList.add(new ContentDocumentLink(LinkedEntityId=supplier_Invoice.id,ContentDocumentId=documents[0].Id,shareType = 'V'));
        insert contentDocumentLinkList;     
        
        Test.startTest();
        
        List<Object> testGetDetails =  generateCCDDetailsWrapper.getDetails(customs_Clearance_Documents);
        Test.stopTest(); 
    }

}