public class Lex_ChildOrderCreation {
       @AuraEnabled   
         public static string CreateCE(ID PAccID,ID PConID,String PshipFrom,String Pdestination,String Pint_courier,String PService_required,Decimal Pshipment_value,String Pref_1,String Pref_2,String PPO_Number,String Pchargeable_weight_estimate,List<Shipment_Order_Package__c> Pshipment_order_package_list,string PselectedPickupadddressID,List<Client_Address__c> PselectedFinaldeladddressIDs_List,String PPart_List,string PSecondhandgoods,string PLi_ion_Batteries,string PLi_ion_BatteryTypes ){
             System.debug('JSONString PPart_List-->'+PPart_List);
             string code1 = PPart_List.replace('[','{ "Parts":[').replace(']','] }');
             String  JsonString = code1;
             System.debug('JSONString'+JSONString);
            CPCreatePartsWrapperInternal rw;
             If(!JSONString.contains('Chuck')){
           rw = (CPCreatePartsWrapperInternal)JSON.deserialize(JSONString,CPCreatePartsWrapperInternal.class);
           System.debug('rw.Parts-->'+rw.parts);
             }
           String CreatedCE= 'empty';
             
             double CummActWeight=0;
                  double Actweight=0;
                  double CummVolWeight=0;
                  double Volweight=0;
                  double CummChargableWeight=0;  // Final
                  double charweight=0;
            
            
            //Calculate Chargable weight
                  If(  !Pshipment_order_package_list.isEmpty() && Pint_courier == 'TecEx'){ 
                  
                           for(Integer l=0; Pshipment_order_package_list.size()>l;l++) {
                         
                           if(Pshipment_order_package_list[l].Weight_Unit__c =='KGs') { Actweight = (Pshipment_order_package_list[l].Actual_Weight__c*Pshipment_order_package_list[l].packages_of_same_weight_dims__c); } else{Actweight=(Pshipment_order_package_list[l].Actual_Weight__c*Pshipment_order_package_list[l].packages_of_same_weight_dims__c*0.453592);}
                           if(Pshipment_order_package_list[l].Dimension_Unit__C =='CMs') { Volweight = ((Pshipment_order_package_list[l].Length__C*Pshipment_order_package_list[l].Breadth__C*Pshipment_order_package_list[l].Height__C)/5000)*Pshipment_order_package_list[l].packages_of_same_weight_dims__c; } else{ Volweight = ((Pshipment_order_package_list[l].Length__C*Pshipment_order_package_list[l].Breadth__C*Pshipment_order_package_list[l].Height__C)/305.1200198)*Pshipment_order_package_list[l].packages_of_same_weight_dims__c;}
                           if(Actweight >= Volweight) { charweight =Actweight ; } else{charweight =Volweight;}
                            
                            CummActWeight=CummActWeight+Actweight;
                            CummVolWeight=CummVolWeight+Volweight;
                            CummChargableWeight=CummChargableWeight+charweight;
                                  
                          }
               
              
            }
            
             else If( !Pshipment_order_package_list.isEmpty() && Pint_courier != 'TecEx'){
                      for(Integer l=0; Pshipment_order_package_list.size()>l;l++) {
                           if(Pshipment_order_package_list[l].Weight_Unit__c =='KGs') { Actweight = (Pshipment_order_package_list[l].Actual_Weight__c*Pshipment_order_package_list[l].packages_of_same_weight_dims__c); } else{Actweight=(Pshipment_order_package_list[l].Actual_Weight__c*Pshipment_order_package_list[l].packages_of_same_weight_dims__c*0.453592);}
                           if(Pshipment_order_package_list[l].Dimension_Unit__C =='CMs') { Volweight = ((Pshipment_order_package_list[l].Length__C*Pshipment_order_package_list[l].Breadth__C*Pshipment_order_package_list[l].Height__C)/5000)*Pshipment_order_package_list[l].packages_of_same_weight_dims__c; } else{ Volweight = ((Pshipment_order_package_list[l].Length__C*Pshipment_order_package_list[l].Breadth__C*Pshipment_order_package_list[l].Height__C)/305.1200198)*Pshipment_order_package_list[l].packages_of_same_weight_dims__c;}
                           if(Actweight >= Volweight) { charweight =Actweight ; } else{charweight =Volweight;}
                            
                            CummActWeight=CummActWeight+Actweight;
                            CummVolWeight=CummVolWeight+Volweight;
                            CummChargableWeight=CummChargableWeight+charweight;
                             }
                     }
             
             
             
           // Shipment Order creation
              Shipment_Order__c SO = new Shipment_Order__c();
                Account Ac =[Select ID,Lead_AM__c,Service_Manager__C,Operations_Manager__c,Financial_Manager__c,Financial_Controller__c,cse_IOR__c,finance_Team__C,CDC__c,invoice_Timing__C, Shipping_Premium_Discount__c from Account where Id=:PAccID];
                If(PService_required=='IOR'){ 
                                                            SO.Account__c = PAccID;
                                                            SO.Client_Contact_for_this_Shipment__c = PConID; 
                                                            SO.Destination__c= Pdestination;
                                                            SO.Ship_From_Country__c = PshipFrom;
                                                            SO.Shipment_Value_USD__c= Pshipment_value;
                                                            SO.Who_arranges_International_courier__c= Pint_courier;
               
                                                            SO.RecordTypeId= '0120Y0000009cEz';
                                                            SO.Service_Type__c= PService_required;  
                                                            SO.Client_Reference__c= Pref_1;
                                                            SO.Client_Reference_2__c= Pref_2;
                                                            SO.Client_PO_ReferenceNumber__c= PPO_Number;
                                                            SO.service_Manager__C =ac.Service_Manager__c;
                                                            SO.Financial_Manager__c =ac.Financial_Manager__c;
                                                            SO.Financial_Controller__c =ac.Financial_Controller__c;
                    
                                                            SO.Finance_Team__c =ac.finance_Team__C; //Added new 01092020
                                                            SO.IOR_CSE__c =ac.cse_IOR__c; //Added new 01092020
                                                            SO.CDC__c =ac.CDC__c; //Added new 01092020
                                                            SO.invoice_Timing__C=ac.invoice_Timing__C;//Added new 01092020
                                                            SO.Lead_AM__c=ac.Lead_AM__c;//Added new 01292020
                                                            SO.Operations_Manager__c=AC.Operations_Manager__c; // Added Anil 06222020
                                                            SO.Shipping_Status__c   ='Shipment Abandoned';
                                                     If( Pshipment_order_package_list.isEmpty() ){  SO.Chargeable_Weight__c  =decimal.valueof(Pchargeable_weight_estimate);} else{SO.Chargeable_Weight__c  =CummChargableWeight;}
                                                        //  SO.Final_Deliveries_New__c=rw.NumberOfFinalDel;
                                                            SO.Type_of_Goods__c=PSecondhandgoods;
                                                            SO.Li_ion_Batteries__c=PLi_ion_Batteries;
                                                            SO.Lithium_Battery_Types__c=PLi_ion_BatteryTypes;
                    IF(Pint_courier == 'TecEx' || Pint_courier == 'Client'){SO.NON_Pickup_freight_request_created__c=TRUE; }Else{SO.NON_Pickup_freight_request_created__c= FALSE;}
                                                         
                                                                    if (!PselectedFinaldeladddressIDs_List.isEmpty()) { SO.Final_Deliveries_New__c= PselectedFinaldeladddressIDs_List.size();} else {SO.Final_Deliveries_New__c=0;}         //Added new 02042020       
                                                                    if (!Pshipment_order_package_list.isEmpty()) {  SO.of_packages__c = Pshipment_order_package_list.size();} else {SO.of_packages__c =0;} //Added new 02042020
                                                                    
                                                            Insert SO;
                                                /*      String CPA2=FindCPA2.FindCPA2(SO);       
                                                        So.CPA_v2_0__c= CPA2;
                                                        SO.Lead_ICE__c=So.CPA_v2_0__r.Lead_ICE__c;  
                                                        Update SO;
                                                */
                                      }
            else {
                 
                                                            SO.Account__c = PAccID;
                                                            SO.Client_Contact_for_this_Shipment__c = PConID; 
                                                            SO.Destination__c= PshipFrom;
                                                            SO.Ship_From_Country__c = Pdestination;
                                                            SO.Shipment_Value_USD__c= Pshipment_value;
                                                            SO.Who_arranges_International_courier__c= Pint_courier;
               
                                                            SO.RecordTypeId= '0120Y0000009cEz';
                                                            SO.Service_Type__c= PService_required;  
                                                            SO.Client_Reference__c= Pref_1;
                                                            SO.Client_Reference_2__c= Pref_2;
                                                            SO.Client_PO_ReferenceNumber__c= PPO_Number;
                                                            SO.service_Manager__C =ac.Service_Manager__c;
                                                            SO.Financial_Manager__c =ac.Financial_Manager__c;
                                                            SO.Financial_Controller__c =ac.Financial_Controller__c;
                                                            
                                                            SO.Finance_Team__c =ac.finance_Team__C; //Added new 01092020
                                                            SO.IOR_CSE__c =ac.cse_IOR__c; //Added new 01092020
                                                            SO.CDC__c =ac.CDC__c; //Added new 01092020
                                                            SO.invoice_Timing__C=ac.invoice_Timing__C;//Added new 01092020
                                                            SO.Lead_AM__c=ac.Lead_AM__c;//Added new 01292020
                                                             SO.Operations_Manager__c=AC.Operations_Manager__c; // Added Anil 06222020
                                                             
                                                            SO.Shipping_Status__c   ='Shipment Abandoned';
                                                            If( Pshipment_order_package_list.isEmpty()  ){  SO.Chargeable_Weight__c  =decimal.valueof(Pchargeable_weight_estimate);} else{SO.Chargeable_Weight__c  =CummChargableWeight;}
                                                        //  SO.Final_Deliveries_New__c=rw.NumberOfFinalDel;
                                                            SO.Type_of_Goods__c=PSecondhandgoods;
                                                            SO.Li_ion_Batteries__c=PLi_ion_Batteries;
                                                            SO.Lithium_Battery_Types__c=PLi_ion_BatteryTypes;
                                                            IF(Pint_courier == 'TecEx' || Pint_courier == 'Client'){SO.NON_Pickup_freight_request_created__c=TRUE; }Else{SO.NON_Pickup_freight_request_created__c= FALSE;}
                        
                                                                    if (!PselectedFinaldeladddressIDs_List.isEmpty()) { SO.Final_Deliveries_New__c= PselectedFinaldeladddressIDs_List.size();} else {SO.Final_Deliveries_New__c=0;}         //Added new 02042020       
                                                                    if (!Pshipment_order_package_list.isEmpty()) {  SO.of_packages__c = Pshipment_order_package_list.size();} else {SO.of_packages__c =0;} //Added new 02042020
                                                        
                Insert SO;
                                                        /*      String CPA2=FindCPA2.FindCPA2(SO);       
                                                        So.CPA_v2_0__c= CPA2;
                                                        SO.Lead_ICE__c=So.CPA_v2_0__r.Lead_ICE__c;  
                                                        Update SO;
                                                */
                     
                 }
             
             // Create Freight            
             Freight__c FR = new Freight__c();
            If(Pint_courier == 'TecEx' || Pint_courier == 'Client'){
                 Client_Address__c Pickupadd = [select Id,name,CaseId__c,Contact_Full_Name__c,Contact_Email__c,Contact_Phone_Number__c,AdditionalContactNumber__c,CompanyName__c,Address__c,Address2__c,City__c,Province__c,Postal_Code__c,All_Countries__c,Comments__c from Client_Address__c where id = :PselectedPickupadddressID]; 
              
           
              FR.Pick_up_Contact_Details__c=Pickupadd.Contact_Full_Name__c;
              FR.Ship_From_Country__c = PshipFrom;
              FR.Ship_To__c = Pdestination;
              FR.Client__c=PAccID;
              Fr.Shipment_Order__c = SO.Id;
              Fr.International_Shipping_Discount_Override__c = Ac.Shipping_Premium_Discount__c; 
                
                Fr.Chargeable_weight_in_KGs_packages__c = So.Chargeable_Weight__c;
                FR.Pick_up_Contact_No__c=Pickupadd.Contact_Phone_Number__c;
                FR.Pick_Up_Contact_Email__c=Pickupadd.Contact_Email__c;
                FR.Pick_Up_Contact_Address1__c=Pickupadd.Address__c;
                FR.Pick_Up_Contact_Address2__c=Pickupadd.Address2__c;
                FR.Pick_Up_Contact_City__c=Pickupadd.City__c;
                FR.Pick_Up_Contact_Province__c=Pickupadd.Province__c;
                FR.Pick_Up_Contact_Country__c=Pickupadd.All_Countries__c;
                FR.Pick_Up_Contact_ZIP__c=Pickupadd.Postal_Code__c;
            Insert FR; 
           //    System.enqueueJob(new QueueableInsertFR(FR));
                
            }
             
             
             
             
               // Shipment Order Packages creation
                list<Shipment_Order_Package__c> SOPL = new list<Shipment_Order_Package__c>();
              If(  !Pshipment_order_package_list.isEmpty() && Pint_courier == 'TecEx'){  
              //  list<Shipment_Order_Package__c> SOPL = new list<Shipment_Order_Package__c>();
                      for(Integer l=0; Pshipment_order_package_list.size()>l;l++) {
                         //Create shipmentOrder Packages
                             Shipment_Order_Package__c SOP = new Shipment_Order_Package__c(
                             packages_of_same_weight_dims__c =Pshipment_order_package_list[l].packages_of_same_weight_dims__c,
                             Weight_Unit__c=Pshipment_order_package_list[l].Weight_Unit__c,
                             Actual_Weight__c=Pshipment_order_package_list[l].Actual_Weight__c,
                             Dimension_Unit__c=Pshipment_order_package_list[l].Dimension_Unit__c,
                             Length__c=Pshipment_order_package_list[l].Length__c,
                             Breadth__c=Pshipment_order_package_list[l].Breadth__c,
                             Height__c=Pshipment_order_package_list[l].Height__c,
                                 Created_From__c='Internal wizard',
                             Shipment_Order__c=SO.id,
                             Freight__C=FR.Id
                              
                         );
                           SOPL.add(SOP);
                          }
                Insert SOPL;
            }
            
            else If( !Pshipment_order_package_list.isEmpty() && Pint_courier != 'TecEx'){
                
          //      list<Shipment_Order_Package__c> SOPL = new list<Shipment_Order_Package__c>();
                      for(Integer l=0; Pshipment_order_package_list.size()>l;l++) {
                         //Create shipmentOrder Packages
                             Shipment_Order_Package__c SOP = new Shipment_Order_Package__c(
                             packages_of_same_weight_dims__c =Pshipment_order_package_list[l].packages_of_same_weight_dims__c,
                             Weight_Unit__c=Pshipment_order_package_list[l].Weight_Unit__c,
                             Actual_Weight__c=Pshipment_order_package_list[l].Actual_Weight__c,
                             Dimension_Unit__c=Pshipment_order_package_list[l].Dimension_Unit__c,
                             Length__c=Pshipment_order_package_list[l].Length__c,
                             Breadth__c=Pshipment_order_package_list[l].Breadth__c,
                             Height__c=Pshipment_order_package_list[l].Height__c,
                                 Created_From__c='Internal wizard',
                             Shipment_Order__c=SO.id
                             
                         );
                           SOPL.add(SOP);
                          }
                Insert SOPL;
            }
             
             //Final Deliveries creation
     If(!PselectedFinaldeladddressIDs_List.isEmpty()){
                
                list<Final_Delivery__c> FDL = new list<Final_Delivery__c>();
                      for(Integer l=0; PselectedFinaldeladddressIDs_List.size()>l;l++) {
                         
                             Final_Delivery__c FD = new Final_Delivery__c(
                            
                              Name =PselectedFinaldeladddressIDs_List[l].Name,
                               Company_Name__C =   PselectedFinaldeladddressIDs_List[l].CompanyName__C,
                              Contact_name__c=PselectedFinaldeladddressIDs_List[l].Contact_Full_Name__c,
                              Contact_email__c=PselectedFinaldeladddressIDs_List[l].Contact_Email__c,
                              Contact_number__c    =PselectedFinaldeladddressIDs_List[l].Contact_Phone_Number__c,
                               Address_Line_1__c  =PselectedFinaldeladddressIDs_List[l].Address__c,
                               Address_Line_2__c  =PselectedFinaldeladddressIDs_List[l].Address2__c,
                               City__c =PselectedFinaldeladddressIDs_List[l].City__c + PselectedFinaldeladddressIDs_List[l].Province__c, 
                               Zip__c  =PselectedFinaldeladddressIDs_List[l].Postal_Code__c,
                               Country__c   =PselectedFinaldeladddressIDs_List[l].All_Countries__c,
                                 Created_From__c='Internal wizard',
                               Shipment_Order__c=SO.id
                         
                         );
                           FDL.add(FD);
                          }
                Insert FDL;
            }
             
             //Creating Parts
             list<Part__c> PAL = new list<Part__c>();    
             If(String.isNotBlank(PPart_List) && !JSONString.contains('Chuck') ){
                             
                     
                          for(Integer l=0; rw.Parts.size()>l;l++) {
                             Part__c PA = new Part__c(
                             
                                  Name =rw.Parts[l].PartNumber,
                                  Description_and_Functionality__c =rw.Parts[l].PartDescription,
                                  Quantity__c =rw.Parts[l].Quantity,
                                  Commercial_Value__c =rw.Parts[l].UnitPrice,
                                  US_HTS_Code__c =rw.Parts[l].HSCode,
                                  Country_of_Origin2__c =rw.Parts[l].CountryOfOrigin,
                                  ECCN_NO__c =rw.Parts[l].ECCNNo,
                                  Type_of_Goods__c=rw.Parts[l].Type_of_Goods,
                                  Li_ion_Batteries__c=rw.Parts[l].Li_ion_Batteries,
                                  Lithium_Battery_Types__c=rw.Parts[l].Li_ion_BatteryTypes,
                                  Shipment_Order__c=SO.ID
                         
                                );
                           PAL.add(PA);
                          }
                          
                Insert PAL;
             
             }
                     
          /*   String CPA2=FindCPA2.FindCPA2(SO);       
             So.CPA_v2_0__c= CPA2;
             //SO.Lead_ICE__c=So.CPA_v2_0__r.Lead_ICE__c;  
             Update SO;
             Insert FR; 
             Insert SOPL;
             Insert PAL;
            */           
                          CreatedCE = SO.ID;
               
           system.enqueueJob(new QueueableUpdateSO(SO));
           //  CreatedCE = SO.ID;
             return CreatedCE; 
                 
            
             
                                 
            }
    
    
    
    //fetch Pickupaddress
     @AuraEnabled
            public static List<Client_Address__c> getpickupaddress(ID PAccID,String PshipFrom ){
             
             Id devRecordTypeId = Schema.SObjectType.Client_Address__c.getRecordTypeInfosByName().get('PickUp').getRecordTypeId();
                         
             return[select Id,CaseID__c,CompanyName__c,AdditionalContactNumber__c,Contact_Email__c,Contact_Full_Name__c,Contact_Phone_Number__c,Province__c,City__c,Postal_Code__c,All_Countries__c from Client_Address__c where Client__c=:PAccID and All_Countries__c=:PshipFrom and recordtypeID =:devRecordTypeId ];
         }
    
    
    //fetch Final Delivery address
     @AuraEnabled
            public static List<Client_Address__c> getFinDeladdress(ID PAccID,String Pdestination ){
             
             Id devRecordTypeId = Schema.SObjectType.Client_Address__c.getRecordTypeInfosByName().get('Final Destinations').getRecordTypeId();
                         
             return[select Id,name,CaseID__c,CompanyName__c,AdditionalContactNumber__c,Contact_Email__c,Contact_Full_Name__c,Contact_Phone_Number__c,Province__c,City__c,Postal_Code__c,All_Countries__c from Client_Address__c where Client__c=:PAccID and All_Countries__c=:Pdestination and recordtypeID =:devRecordTypeId ];
         }
    
     //for selectedFinaldeladddressIDs
     @AuraEnabled
            public static List<Client_Address__c> UpdateseleFinaldeladIDs(object[] changes){
             
              list<ID> selectedIdsList;
               
                 string code = String.valueof(changes[0]);
                 string code1 = code.replace('(','').replace(')','').replace(',',' ');
                 List<string> code3 = code1.split(' ');
                 selectedIdsList = code3;
              
              List<Client_Address__c> abcd = [select Id,name,CaseId__c,Contact_Email__c,Contact_Phone_Number__c,AdditionalContactNumber__c,CompanyName__c,Contact_Full_Name__c,Address__c,Address2__c,City__c,Province__c,Postal_Code__c,All_Countries__c,Comments__c from Client_Address__c where id IN :selectedIdsList]; 
              
             return abcd;
            }
    
    
    //Creating pickup address
     @AuraEnabled
            public static List<Client_Address__c> CreatePickAdd(List<Client_Address__c> PPickAddList, string PAccID){
            system.debug('PPickAddList-->'+PPickAddList);
                system.debug('PAccID-->'+PAccID);
                
              List<Client_Address__c> PickaddL = new list<Client_Address__c>();
              Id PickuprecordTypeId = Schema.SObjectType.Client_Address__c.getRecordTypeInfosByDeveloperName().get('PickUp').getRecordTypeId();
            
                
            for(Integer i=0; PPickAddList.size()>i;i++) {
                
                Client_Address__c Pickadd = new Client_Address__c(
            
            recordtypeID=PickuprecordTypeId,   
            Name = PPickAddList[i].Province__c,
            Contact_Full_Name__c= PPickAddList[i].Contact_Full_Name__c,
            Contact_Email__c = PPickAddList[i].Contact_Email__c,
            Contact_Phone_Number__c = PPickAddList[i].Contact_Phone_Number__c,
            CompanyName__c= PPickAddList[i].CompanyName__c,
            Address__c=PPickAddList[i].Address__c,
            City__c=PPickAddList[i].City__c,
            Province__c=PPickAddList[i].Province__c,
            Postal_Code__c=PPickAddList[i].Postal_Code__c,
            All_Countries__c=PPickAddList[i].All_Countries__c,
            Client__c=PAccID
            );
             PickaddL.add(Pickadd); 
                               
                 }
                
              Insert PickaddL;
                 
             return PickaddL;
            }
    
    //Creating Final Del address
     @AuraEnabled
            public static List<Client_Address__c> CreatefinaldelAdd(List<Client_Address__c> PFinalDeladdList, string PAccID){
            system.debug('PFinalDeladdList-->'+PFinalDeladdList);
                system.debug('PAccID-->'+PAccID);
                
              List<Client_Address__c> FinaldeladdL = new list<Client_Address__c>();
              Id PickuprecordTypeId = Schema.SObjectType.Client_Address__c.getRecordTypeInfosByDeveloperName().get('Final_Destinations').getRecordTypeId();
            
                
            for(Integer i=0; PFinalDeladdList.size()>i;i++) {
                
                Client_Address__c Pickadd = new Client_Address__c(
            
            recordtypeID=PickuprecordTypeId,   
            Name = PFinalDeladdList[i].Province__c,
            Contact_Full_Name__c= PFinalDeladdList[i].Contact_Full_Name__c,
            Contact_Email__c = PFinalDeladdList[i].Contact_Email__c,
            Contact_Phone_Number__c = PFinalDeladdList[i].Contact_Phone_Number__c,
            CompanyName__c= PFinalDeladdList[i].CompanyName__c,
            Address__c=PFinalDeladdList[i].Address__c,
            City__c=PFinalDeladdList[i].City__c,
            Province__c=PFinalDeladdList[i].Province__c,
            Postal_Code__c=PFinalDeladdList[i].Postal_Code__c,
            All_Countries__c=PFinalDeladdList[i].All_Countries__c,
            Client__c=PAccID
            );
             FinaldeladdL.add(Pickadd); 
                               
                 }
                
              Insert FinaldeladdL;
                
             return FinaldeladdL;
            }
    
   

}