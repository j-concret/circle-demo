/**
*
* @description This batch apex we can use to fire the record update process with no value change.
* For example : Recalculate the formula(Trigger/PB/Flow) as per new changes.
*/

global class NoChangeRecordUpdateBatch implements Database.Batchable<sObject> {
    
    String query = '';
    List<Id> recordIds = new List<Id>();
        
    /* To use this to direct inject the SOQL here in constructor.
    *****
    Database.executeBatch(new BlankUpdateBatch('Select Id FROM Shipment_Order__c LIMIT 1'), 1);
    *****
    Note : Make sure to set proper chunk size as per requirement.
    */
    
    global NoChangeRecordUpdateBatch(String query){
        this.query = query;
    }
    
    /*Below is the example to pass specific record ids.
    *******
    
    {Add Custom logic here} 
    Database.executeBatch(new BlankUpdateBatch(List<Id> recordIds), 1);
    *******
    Note : Make sure to set proper chunk size as per requirement.
    */
    
    global NoChangeRecordUpdateBatch(List<Id> recordIds){
        this.recordIds = recordIds;
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        
        if(!recordIds.isEmpty()){
            Schema.sObjectType objType = recordIds[0].getSObjectType();
 			String objName = objType.getDescribe().getName();
            query = 'Select Id FROM ' + objName+ ' WHERE Id IN :recordIds';
        }
        
        system.debug('query ' + query); 
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<Sobject> scope) {  
        update scope;    
    }   
    
    global void finish(Database.BatchableContext BC) {
        // execute any post-processing operations
    }
}