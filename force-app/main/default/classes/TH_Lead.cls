/*
* @Description Class will process trigger events on the Lead Object
*/
public class TH_Lead
{
    private static final String EMAIL_TEMPLATE = 'Lead Limit Warning';
    public static String EMAIL_TEXT;
    public static Integer EMAIL_SENT = 0;  
    private static Decimal LIMIT_FACTOR = UTIL_GlobalVariableManifest.WarningFactor;  
    
    /*public void execute () 
    { 
        //Only process after events for Inserts and Updates on Leads (we want database to be altered before we check as we will use SOQL)
        if (Trigger.isAfter)
        {
            Integer limitNo = UTIL_GlobalVariableManifest.LeadLimit;
            
            //Don't process lead limit if it's not greater than zero......
            if (limitNo <= 0){
                return;
            }
            
            List<Lead> newLeads = trigger.new;
            
            //Get a list of Queues and place in a map, we will use this to
            //check if the we should include the Lead in the processing
            List<Group> groups = DAL_User.getAllQueues();
            
            
            Map<Id, Group> queueMap = new Map<Id, Group>(groups);
            Set<Id> usersToCheck = new Set<Id>();
            Map<Id,Integer> limitByUser = new Map<Id,Integer>();
            Integer currentLimit = limitNo;
            
            for (Lead newLead : newLeads) 
            {
                //Only process leads whose owner is User
                if (queueMap.containsKey(newLead.OwnerId))
                    continue;
                
                //Determine if any leads have had their ownerId changed
                //All new leads will be considered
                //And any leads who had their ownerId changed.
                if (Trigger.isInsert){
                    usersToCheck.add(newLead.OwnerId);
                    Features_Switches__c leadLimitRecord = Features_Switches__c.getInstance(newLead.OwnerId);
                    if(leadLimitRecord != null){
                        currentLimit = Integer.valueOf(leadLimitRecord.value__c);
                    }else{
                        currentLimit = limitNo;
                    }
                    limitByUser.put(newLead.OwnerId, currentLimit);
                    
                    
                }else if (Trigger.isUpdate)
                {
                    Lead oldLead = (Lead) Trigger.oldMap.get(newLead.Id);
                    
                    if (newLead.OwnerId != oldLead.OwnerId){
                        usersToCheck.add(newLead.OwnerId);
                        Features_Switches__c leadLimitRecord = Features_Switches__c.getInstance(newLead.OwnerId);
                        if(leadLimitRecord != null){
                            currentLimit = Integer.valueOf(leadLimitRecord.value__c);
                        }else{
                            currentLimit = limitNo;
                        }
                        limitByUser.put(newLead.OwnerId, currentLimit);
                    }
                }
            }
            
            //If we have no users to check than we can exit the handler processing loop
            if (usersToCheck.isEmpty())
                return;
            
            //At this stage we have a list of UserIds that we should check
            //to see if any of these users have too many leads allocated to them
            List<AggregateResult> leadCountsPerUser = DAL_Lead.leadCountsPerUser(limitByUser, limitNo, LIMIT_FACTOR);
            
            
            //Send a warning message to all users who have exceeded 80% of their limit and            
            //put all userId's with a count higher than the limit into a set, 
            //this will be used to compare against incoming leads
            Set<ID> usersWithInvalidCount = new Set<ID>();
            OrgWideEmailAddress owa = DAL_Email.getAnyOrgWideAddress();
            EmailTemplate template = DAL_Email.getTemplateByName(EMAIL_TEMPLATE);
            List<Messaging.SingleEmailMessage> allmsg = new List<Messaging.SingleEmailMessage>();
            
            Integer claimed;
            Id owner;
            
            for (AggregateResult aggregate : leadCountsPerUser)
            {
                claimed = (Integer)aggregate.get(DAL_Lead.AGGREGATE_NUMBER_LEADS);
                owner = (Id)aggregate.get(DAL_Lead.FIELD_OWNERID);
                integer userLimit = limitByUser.get(owner);
                if(claimed > userLimit)                            
                    usersWithInvalidCount.add(owner);                
                else                                  
                    allmsg.add(createWarningEmail(owa, owner, template, String.valueOf(userLimit - claimed)));                                      
            }
            
            Messaging.sendEmail(allmsg, false);
            
            //if we have no aggreggate results then we have no users with an invalid lead count, we can exit handler
            if (leadCountsPerUser.isEmpty())
                return;
            
            for (Lead newLead : newLeads) 
                if (usersWithInvalidCount.contains(newLead.OwnerId))
                newLead.AddError(String.format(DAL_Lead.ERROR_LEAD_ALLOCATION, new String[]{String.valueOf(limitByUser.get(newLead.OwnerId))}));
        }

        if(Trigger.isBefore && Trigger.isInsert){
            List<Lead> newLeadsBefore = trigger.new;
            Set <Id> ownerIds = new Set <Id>();
            Map<String, String> userBranchMap = new Map<String, String>(); 

            if(newLeadsBefore.size()>0){
                
                for(Lead currentlead : newLeadsBefore){
                    ownerIds.add(currentlead.OwnerId);
                }
            }

            if(ownerIds.size()>0){
                for(User currentUser : [Select Id,Branch__c From User where isActive = True and Branch__c != 'PC']){
                    userBranchMap.put(currentUser.Id,currentUser.Branch__c);
                }
            }

            if(newLeadsBefore.size()>0){
                
                for(Lead currentlead : newLeadsBefore){
                    currentlead.Branch_PL__c = userBranchMap.get(currentlead.OwnerId);
                }
            }


        }
    } 
    
    private Messaging.SingleEmailMessage createWarningEmail(OrgWideEmailAddress owa, Id targetObject, EmailTemplate template, String replaceWith)
    {
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String body;
        mail.setOrgWideEmailAddressId(owa.id);
        mail.setSaveAsActivity(false);
        mail.setTargetObjectId(targetObject);  
        body = template.Body;              
        mail.setPlainTextBody(body.replace(DAL_Email.EMIL_TEMPLATE_PLACEHOLDER, replaceWith));
        
        if(Test.isRunningTest()){
            EMAIL_SENT++;
            EMAIL_TEXT = mail.getPlainTextBody();
        }  
        
        return mail;
    }*/
}