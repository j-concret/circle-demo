global class Automated_Supplier_Email_Schedule implements Schedulable {
	global void execute(SchedulableContext SC){
		Automated_Supplier_Email_SQL b = new Automated_Supplier_Email_SQL();
      	database.executebatch(b);
    }
}