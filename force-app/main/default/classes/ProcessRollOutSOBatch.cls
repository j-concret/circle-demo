/**
*
* @description This batch apex we use to change to SO status from rollout to CE.
*/

global class ProcessRollOutSOBatch implements Database.Batchable<sObject>, Database.AllowsCallouts {
    
    String query = '';
    List<Id> recordIds = new List<Id>();
    
    global ProcessRollOutSOBatch(){
    }
    
    global ProcessRollOutSOBatch(List<Id> recordIds){
        this.recordIds = recordIds;
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        
        String fields = 'Id,Cost_Estimate_Number__c, Status__c, Pull_CPA_and_IOR_PL__c, Service_Type__c, Shipment_Value_USD__c, '
            +'Shipping_Status__c, IOR_Price_List__r.EOR_Admin_Fee__c, IOR_Price_List__r.EOR_Bank_Fees__c, '
            +'IOR_Price_List__r.EOR_Bank_Above_10000__c, Recalculate_Costings__c, CPA_Costings_Added__c, '
            +'Handling_and_Admin_Fee__c, Bank_Fees__c, IOR_Price_List__r.Bank_Fees__c,  Who_arranges_International_courier__c, '
            +'IOR_Price_List__r.Admin_When_Tecex_Does_Freight__c, IOR_Price_List__r.Admin_Fee__c, '
            +'IOR_Price_List__r.Admin_Above_10000__c, IOR_Price_List__r.Bank_Above_10000__c ,Roll_Out__c, Chargeable_Weight__c, '
            +'Li_ion_Batteries__c, Ship_From_Country__c, Hub_Country__c, Destination__c,(SELECT Id FROM Freight__r)';
        
        //  String filterCondition = 'Shipping_Status__c = \'Roll-Out\' AND Recalculate_Costings__c = false AND CPA_Costings_Added__c = false AND CreatedDate = TODAY';
        
        query = 'Select '+ fields +' FROM Shipment_order__c WHERE Roll_Out__c IN :recordIds AND Shipping_Status__c = \'Roll-Out\'';
        
        
        system.debug('query ' + query);
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<Shipment_Order__c> shipmentOrderList) {
        
        try{
            
            List<Freight__c> frToInsert = new List<Freight__c>();
            
            for(Shipment_Order__c so : shipmentOrderList) {
                
                
                
                //create freight
                
                Freight__c fr = new Freight__c();
                if(so.Freight__r != null && !(so.Freight__r).isEmpty()){
                    fr.Id = so.Freight__r[0].Id;
                }
                fr.Chargeable_weight_in_KGs_packages__c = so.Chargeable_Weight__c;
                fr.Li_Ion_Batteries__c = so.Li_ion_Batteries__c;
                fr.Ship_From_Country__c = so.Ship_From_Country__c;
                fr.Ship_To__c = so.Hub_Country__c == null ? so.Destination__c : so.Hub_Country__c;
                fr.Shipment_Order__c = so.Id;
                
                frToInsert.add(fr);
                
                so.Pull_CPA_and_IOR_PL__c = true;
                so.Shipping_Status__c = 'Cost Estimate';
                so.Final_Deliveries_New__c = 1;
                so.Number_of_Final_Deliveries_Client__c = 1;
                so.Recalculate_Costings__c = true;
                so.CPA_Costings_Added__c = true;
                
                
                // below code to update Bank_Fees__c and Handling_and_Admin_Fee__c
                if(so.Service_Type__c == 'EOR') {
                    
                    if(so.Shipment_Value_USD__c <= 10000) {
                        so.Handling_and_Admin_Fee__c = so.IOR_Price_List__r.EOR_Admin_Fee__c;
                        
                        so.Bank_Fees__c = so.IOR_Price_List__r.EOR_Bank_Fees__c;
                    }else{
                        so.Handling_and_Admin_Fee__c = so.IOR_Price_List__r.EOR_Bank_Above_10000__c;
                        
                        so.Bank_Fees__c = so.IOR_Price_List__r.EOR_Bank_Above_10000__c;
                    }
                    
                }else{
                    
                    if(so.Shipment_Value_USD__c <= 10000) {
                        so.Bank_Fees__c = so.IOR_Price_List__r.Bank_Fees__c;
                        
                        if(so.Who_arranges_International_courier__c == 'Tecex') {
                            so.Handling_and_Admin_Fee__c = so.IOR_Price_List__r.Admin_When_Tecex_Does_Freight__c;
                            
                        }else{
                            so.Handling_and_Admin_Fee__c = so.IOR_Price_List__r.Admin_Fee__c;
                            
                        }
                    }else{
                        so.Handling_and_Admin_Fee__c = so.IOR_Price_List__r.Admin_Above_10000__c;
                        so.Bank_Fees__c = so.IOR_Price_List__r.Bank_Above_10000__c;
                    }
                    
                }
            }
            RecusrsionHandler.skipTriggerExecution = true;
            upsert frToInsert;
            RecusrsionHandler.skipTriggerExecution = false;
            
            update shipmentOrderList;
            Map<Id,Freight__c> fids = new Map<Id,Freight__c>([SELECT Id FROM Freight__c WHERE shipment_order__c=: shipmentOrderList[0].Id]);
            if(!fids.isEmpty()) {
                System.enqueueJob(new CreateCourierratesV2(new List<Id>(fids.keySet())));
            }
            //Batch processes one record at a time
            SO_GenerateCostEstimate.generateCostEstimateFromBatch(shipmentOrderList[0]);
        }catch(Exception e){
             createLogs(shipmentOrderList[0].Id, e.getMessage(), e.getStackTraceString());
        }    
    }
    
    global void finish(Database.BatchableContext BC) {
        // execute any post-processing operations
    }
    
    public void createLogs(String Id, String errorMessage, String stackTrace){
        API_Log__c Al = New API_Log__c();
        Al.EndpointURLName__c='ProcessRollOutSOBatch ' + Id;
        Al.Response__c='Error Message Start: ' + errorMessage + ' getMessage:END -> \n Stack Trace: ' + stackTrace  + ' :END';
        Al.StatusCode__c= '100';
        Insert Al; 
    }
}