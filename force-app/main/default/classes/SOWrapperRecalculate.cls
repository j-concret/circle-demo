public class SOWrapperRecalculate
    {
        public SOWrapperRecalculate(Shipment_Order__c so)
        {
            this.SO = so;
            this.CAP = [SELECT Related_Costing_CPA__c, VAT_Rate__c  FROM CPA_v2_0__c WHERE ID =: so.CPA_v2_0__c];
           // this.Client = so.Account__r;

      
                recalculateCostings();

        }
 

        public Shipment_Order__c SO { set; get; }
        public CPA_v2_0__c CAP { set; get; }
      //public Account Client { set; get; }
        private List<CPA_Costing__c> relatedCostings;
        private List<CPA_Costing__c> relatedCostingsIOR;
        private List<CPA_Costing__c> relatedCostingsEOR;
        private List<CPA_Costing__c> addedCostings;
       

        private Id recordTypeId = Schema.SObjectType.CPA_Costing__c.getRecordTypeInfosByName().get('Transaction').getRecordTypeId();



         public void recalculateCostings()
        {
            
            relatedCostings = [SELECT Name, DeleteAll__c, Shipment_Order_Old__c, IOR_EOR__c,Amount__c, Applied_to__c, Ceiling__c, Conditional_value__c, Condition__c, Floor__c, 
                               Cost_Category__c, Cost_Type__c, Max__c, Min__c, Rate__c, CostStatus__c, Updating__c, RecordTypeID, VAT_Rate__c, VAT_applicable__c, 
                               Variable_threshold__c, Currency__c, Supplier_Invoice__c, Amended__c  
                               FROM CPA_Costing__c 
                               WHERE Shipment_Order_Old__c =: SO.ID 
                               AND Supplier_Invoice__c = null
                               AND Amended__c = FALSE ];
            System.debug('relatedCostings --- '+relatedCostings.size());
            
            List<CPA_Costing__c> delList = new List<CPA_Costing__c>();
            for(CPA_Costing__c var: relatedCostings )
            {
            
            iF(relatedCostings.size() > 0){
               
                    CPA_Costing__c del = new CPA_Costing__c();
                    del.id =var.id;
                	del.DeleteAll__c = TRUE;
                    delList.add(del);                    
                }
                
                }
            
            system.debug('delList --->'+delList);
               delete delList; 
           
          Shipment_Order__c SO1 = [Select Id from Shipment_Order__c where Id =: SO.ID];  
          
          List<Id> soIds = new List<Id>();
          soIds.add(SO1.id);  
            
            
          CalculateCosts.createSOCosts(soIds);

     
               
        }
    
    
   
    
    
    }