@RestResource(urlMapping='/GetCaseDetails/*')
Global class NCPGetCasedetails {
    
      @Httppost
    global static void NCPGetCasedetails(){
    
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        Blob body = req.requestBody;
    	res.addHeader('Content-Type', 'application/json');
        String requestString = body.toString();
        NCPGetCasedetailsWra rw = (NCPGetCasedetailsWra)JSON.deserialize(requestString,NCPGetCasedetailsWra.class);
          try {
               
               
                Access_token__c At =[select status__c,Access_Token__c from Access_token__c where Access_Token__c=:rw.Accesstoken ];
            	if( at.status__C =='Active'){
                  Set<ID> Ids = New set<ID>();  
                  Set<ID> FeedIds = New set<ID>(); 
                //  Set<ID> contenetVerIds = New set<ID>();
                  List<FeedAttachment> Feedatt= new list<FeedAttachment> ();
                  String jsonData='Test';
                     MAP<String,String> contenetverIds = new map<String,String>(); 
                     MAP<String,List<ContentVersion>> contenetDocIds = new map<String,List<ContentVersion>>(); 
                  //  MAP<String,ContentVersion> FinalIds = new map<String,ContentVersion>(); 
                   
             //   case cs = [select AccountId,ContactId,Id,subject,Assigned_to__c,Assigned_to__r.name,CaseNumber,status,description,parentID,OwnerId,Owner.name,LastClientMsgTime__c,Last_Client_viewed_time__c,LastMessageTime__c,shipment_order__c,shipment_order__r.name,shipment_order__r.Shipping_Status__c,shipment_order__r.NCP_Quote_Reference__c,shipment_order__r.RecordType.name,shipment_order__r.Client_Reference__c,shipment_order__r.Closed_Down_SO__c, (select Id,Name,ContentType from Attachments), (select Id,ParentId,InsertedById,Title,Body,LinkUrl,createddate from Feeds),(select Id,ContentDocumentId,LinkedEntityId,SystemModstamp,Visibility from ContentDocumentLinks), (select Id,ContentDocumentId,Title,FileType,ContentSize,FileExtension,ContentUrl,LastModifiedDate,LastModifiedById from AttachedContentDocuments) from case where id=:rw.CaseID];
         		
                case cs = [select AccountId,FullphotoURL__C,ContactId,Id,subject,Assigned_to__c,Assigned_to__r.name,CaseNumber,status,description,parentID,OwnerId,Owner.name,LastClientMsgTime__c,Last_Client_viewed_time__c,LastMessageTime__c,shipment_order__c,shipment_order__r.name,shipment_order__r.Shipping_Status__c,shipment_order__r.NCP_Quote_Reference__c,shipment_order__r.RecordType.name,shipment_order__r.Client_Reference__c,shipment_order__r.Closed_Down_SO__c from case where id=:rw.CaseID];
         	        cs.Last_Client_viewed_time__c =datetime.now();
                     Update cs;
                    
                  List<Attachment> Att= new list<Attachment> ([select Id,Name,ContentType from Attachment where parentid=:rw.CaseID]);
                  List<FeedItem> Feedsoncase= new list<FeedItem>   ([select Id,ParentId,InsertedById,Title,Body,LinkUrl,createddate,CommentCount,(select Id,Title,FeedEntityId,RecordId from FeedAttachments) from FeedItem where parentid=:rw.CaseID]);
                 
                    For(Integer i =0;Feedsoncase.size()>i;i++){FeedIds.add(Feedsoncase[i].ID);}
                     If(!Feedsoncase.isEmpty()){
                     // Feedatt =[SELECT Id, RecordId, FeedEntityId FROM FeedAttachment WHERE FeedEntityId  in: FeedIds]; 
                     for(FeedAttachment feed:[SELECT Id, RecordId, FeedEntityId FROM FeedAttachment WHERE FeedEntityId  in: FeedIds]){
                                contenetverIds.put(feed.RecordId,Feed.FeedEntityId);//068
                         	}
                         
                     If(!contenetverIds.isEmpty()){
                          for(ContentVersion CD:[SELECT Id,ContentDocumentId,Title from ContentVersion where Id in:contenetverIds.keyset() ]){
                              If(contenetverIds.containskey(CD.ID)  ){
                                  System.debug('contenetDocIds--> 1 IF  -->'+contenetDocIds);
                                   If(contenetDocIds.containskey(contenetverIds.get(CD.ID))  ){
                                       list<ContentVersion> abc= contenetDocIds.get(contenetverIds.get(cd.ID));
                                       abc.add(cd);
                                       contenetDocIds.put(contenetverIds.get(CD.ID) ,abc);//069
                                      
                                   }
                                  else{
                                      
                                       contenetDocIds.put(contenetverIds.get(CD.ID) ,new list<ContentVersion>{CD});//069
                                      
                                      
                                  }
                              }
                         }
                         
                         
                     }
                     
                     }
                      
                  
                  
                    
                    
                    List<EntitySubscription> Subscribers = new  List<EntitySubscription>([select SubscriberId  from EntitySubscription where  parentid=:rw.CaseID ]);
                    For(Integer i =0;Subscribers.size()>i;i++){Ids.add(Subscribers[i].SubscriberId);}
                    list<User> Nofifyusers =new list<User>([Select id, Firstname,Lastname from user where id in:Ids ]);  
                   
                 JSONGenerator gen = JSON.createGenerator(true);
                 gen.writeStartObject();
                 //Creating Array & Object - Shipment Order
                 gen.writeFieldName('CaseDetails');
                 gen.writeStartObject();
                 	if(cs.Id == null) {gen.writeNullField('Id');} else{gen.writeStringField('Id', cs.Id);}
				 	if(cs.AccountId == null) {gen.writeNullField('AccountId');} else{gen.writeStringField('AccountId', cs.AccountId);}
                    if(cs.FullphotoURL__C == null) {gen.writeNullField('FullphotoURL');} else{gen.writeStringField('FullphotoURL', cs.FullphotoURL__C);}
                    if(cs.ContactId == null) {gen.writeNullField('ContactId');} else{gen.writeStringField('ContactId', cs.ContactId);}
                    if(cs.subject == null) {gen.writeNullField('subject');} else{gen.writeStringField('subject', cs.subject);}
                    if(cs.Assigned_to__c == null) {gen.writeNullField('AssignedtoID');} else{gen.writeStringField('AssignedtoID', cs.Assigned_to__c);}
                    if(cs.Assigned_to__c == null) {gen.writeNullField('AssignedName');} else{gen.writeStringField('AssignedName', cs.Assigned_to__r.name);}
                    if(cs.CaseNumber == null) {gen.writeNullField('CaseNumber');} else{gen.writeStringField('CaseNumber', cs.CaseNumber);}
                    if(cs.status == null) {gen.writeNullField('status');} else{gen.writeStringField('status', cs.status);}
                    if(cs.description == null) {gen.writeNullField('description');} else{gen.writeStringField('description', cs.description);}
                    if(cs.parentID == null) {gen.writeNullField('parentID');} else{gen.writeStringField('parentID', cs.parentID);}
                    if(cs.OwnerId == null) {gen.writeNullField('OwnerId');} else{gen.writeStringField('OwnerId', cs.OwnerId);}
                    if(cs.OwnerId == null) {gen.writeNullField('OwnerName');} else{gen.writeStringField('OwnerName', cs.Owner.name);}
                    if(cs.LastClientMsgTime__c == null) {gen.writeNullField('LastClientMsgTime');} else{gen.writeDateTimeField('LastClientMsgTime', cs.LastClientMsgTime__c);}
                    if(cs.Last_Client_viewed_time__c == null) {gen.writeNullField('LastClientviewedtime');} else{gen.writeDateTimeField('LastClientviewedtime', cs.Last_Client_viewed_time__c);}
                    if(cs.LastMessageTime__c == null) {gen.writeNullField('LastMessageTime');} else{gen.writeDateTimeField('LastMessageTime', cs.LastMessageTime__c);}
					if(cs.shipment_order__c == null) {gen.writeNullField('shipmentorderID');} else{gen.writeStringField('shipmentorderID', cs.shipment_order__c);}
                    if(cs.shipment_order__r.name == null) {gen.writeNullField('shipmentOrderName');} else{gen.writeStringField('shipmentOrderName', cs.shipment_order__r.name);}
					if(cs.shipment_order__r.Shipping_Status__c == null) {gen.writeNullField('ShippingStatus');} else{gen.writeStringField('ShippingStatus', cs.shipment_order__r.Shipping_Status__c);}
                    if(cs.shipment_order__r.NCP_Quote_Reference__c == null) {gen.writeNullField('NCPQuoteReference');} else{gen.writeStringField('NCPQuoteReference', cs.shipment_order__r.NCP_Quote_Reference__c);}
					if(cs.shipment_order__r.RecordType.name == null) {gen.writeNullField('RecordTypename');} else{gen.writeStringField('RecordTypename', cs.shipment_order__r.RecordType.name);}
                    if(cs.shipment_order__r.Client_Reference__c == null) {gen.writeNullField('ClientReference');} else{gen.writeStringField('ClientReference', cs.shipment_order__r.Client_Reference__c);}
					if(cs.shipment_order__r.Closed_Down_SO__c == null) {gen.writeNullField('ClosedDownSO');} else{gen.writeBooleanField('ClosedDownSO', cs.shipment_order__r.Closed_Down_SO__c);}
                   
                 gen.writeEndObject();
                 //End of Array & Object - Shipment Order
 
                 If(!Att.isEmpty()){
                     //Creating Array & Object - attachments
                     gen.writeFieldName('NotesandAttachments');
                     gen.writeStartArray();
                     for(Attachment Att1 :Att) {
                         //Start of Object - NotesandAttachments
                         gen.writeStartObject();

                         if(Att1.Id == null) {gen.writeNullField('Id');} else{gen.writeStringField('Id', Att1.Id);}
                         if(Att1.Name == null) {gen.writeNullField('Name');} else{gen.writeStringField('Name', Att1.Name);}
                         if(Att1.ContentType == null) {gen.writeNullField('ContentType');} else{gen.writeStringField('ContentType', Att1.ContentType);}
                        
                         gen.writeEndObject();
                         //End of Object - NotesandAttachments
                     }
                     gen.writeEndArray();
                     //End of Array - NotesandAttachments
                 }


                 If(!Feedsoncase.isEmpty()){
                     //Creating Array & Object - Feedsoncase
                     gen.writeFieldName('Feedsoncase');
                     gen.writeStartArray();
                     for(FeedItem Feedsoncase1 :Feedsoncase) {
                         //Start of Object - Feedsoncase
                         gen.writeStartObject();

                         if(Feedsoncase1.Id == null) {gen.writeNullField('Id');} else{gen.writeStringField('Id', Feedsoncase1.Id);}
                         if(Feedsoncase1.ParentId == null) {gen.writeNullField('ParentId');} else{gen.writeStringField('ParentId', Feedsoncase1.ParentId);}
                         if(Feedsoncase1.InsertedById == null) {gen.writeNullField('InsertedById');} else{gen.writeStringField('InsertedById', Feedsoncase1.InsertedById);}
                         if(Feedsoncase1.Title == null) {gen.writeNullField('Title');} else{gen.writeStringField('Title', Feedsoncase1.Title);}
                         if(Feedsoncase1.Body == null) {gen.writeNullField('Body');} else{gen.writeStringField('Body', Feedsoncase1.Body);}
                         if(Feedsoncase1.LinkUrl == null) {gen.writeNullField('LinkUrl');} else{gen.writeStringField('LinkUrl', Feedsoncase1.LinkUrl);}
                         if(Feedsoncase1.createddate == null) {gen.writeNullField('createddate');} else{ gen.writeDateTimeField('createddate', Feedsoncase1.createddate);}
                         if(Feedsoncase1.CommentCount== null) {gen.writeNullField('CommentCount');} else{gen.writeNumberField('CommentCount', Feedsoncase1.CommentCount);}
                        
                       //  gen.writeEndObject();
                         //End of Object - Feedsoncase
                         
                         System.debug('contenetDocIds--> 2  -->'+contenetDocIds.containskey(Feedsoncase1.id));
                         
                         if(contenetDocIds.containskey(Feedsoncase1.id)){
                             //Creating Array & Object - ContentDocId
                     			gen.writeFieldName('ContentDocId');
                     			gen.writeStartArray();
                             
                             for(ContentVersion CD1 :contenetDocIds.get(Feedsoncase1.id)) {
                                 //Start of Object - ContentDocId
                        		 gen.writeStartObject();
                                 
                                  		if(CD1.Id == null) {gen.writeNullField('Title');} else{gen.writeStringField('Title',CD1.Title);}
                                  		if(CD1.Id == null) {gen.writeNullField('ContentverId');} else{gen.writeStringField('ContentverId',CD1.ID);}
                         				if(CD1.ContentDocumentID == null) {gen.writeNullField('ContentdocId');} else{gen.writeStringField('ContentdocId',CD1.ContentDocumentID);}
                                 
                                  gen.writeEndObject();
                         		//End of Object - ContentDocId
                                  
                              }
                             
                           gen.writeEndArray();
                     //End of Array - ContentDocId 
                         }
                         
                         
                        gen.writeEndObject();
                         //End of Object - Feedsoncase
                            
                     }
                     gen.writeEndArray();
                     //End of Array - Feedsoncase
                 }


/*
                 If(!CDocumentLinks.isEmpty()){
                     //Creating Array & Object - ContentDocumentLink
                     gen.writeFieldName('ContentDocumentLink');
                     gen.writeStartArray();
                     for(ContentDocumentLink CDocumentLinks1 :CDocumentLinks) {
                         //Start of Object - ContentDocumentLink
                         gen.writeStartObject();

                         if(CDocumentLinks1.Id == null) {gen.writeNullField('Id');} else{gen.writeStringField('Id', CDocumentLinks1.Id);}
                         if(CDocumentLinks1.ContentDocumentId == null) {gen.writeNullField('ContentDocumentId');} else{gen.writeStringField('ContentDocumentId', CDocumentLinks1.ContentDocumentId);}
                         if(CDocumentLinks1.LinkedEntityId == null) {gen.writeNullField('LinkedEntityId');} else{gen.writeStringField('LinkedEntityId', CDocumentLinks1.LinkedEntityId);}


                         gen.writeEndObject();
                         //End of Object - ContentDocumentLink
                     }
                     gen.writeEndArray();
                     //End of Array - ContentDocumentLink
                 }

  */
                    If(!Nofifyusers.isEmpty()){
                     //Creating Object - Nofifyusers
                     gen.writeFieldName('Participants');
                     gen.writeStartArray();
                     for(user Nofifyusers1 :Nofifyusers) {
                         //Start of Object - Nofifyusers
                         gen.writeStartObject();
                         if(Nofifyusers1.ID== null) {gen.writeNullField('ID');} else{gen.writeStringField('ID', Nofifyusers1.ID);}
						 if(Nofifyusers1.Firstname== null) {gen.writeNullField('Firstname');} else{gen.writeStringField('Name', Nofifyusers1.Firstname);}
                         if(Nofifyusers1.LastName== null) {gen.writeNullField('LastName');} else{gen.writeStringField('LastName', Nofifyusers1.LastName);}
                       

                         gen.writeEndObject();
                         //End of Object - Nofifyusers
                     }
                     gen.writeEndArray();
                     //End of Array - Nofifyusers

                 }


                 gen.writeEndObject();
                 jsonData = gen.getAsString();

               
                    

                   res.responseBody = Blob.valueOf(jsonData); 
                 // res.responseBody = Blob.valueOf(JSON.serializePretty(cs));
                  res.addHeader('Content-Type', 'application/json');
        		  res.statusCode = 200;
                
                API_Log__c Al = New API_Log__c();
                Al.Account__c=cs.AccountID;
                Al.Login_Contact__c=cs.ContactId;
                Al.EndpointURLName__c='NCP - GetCaseDetails';
                Al.Response__c='Success -GetCaseDetails Details are sent';
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al;
                }
          }
              catch (Exception e){
                  
                   String ErrorString ='Something went wrong while creating cost estimate, please contact support_SF@tecex.com';
               		res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
            		res.addHeader('Content-Type', 'application/json');
        			res.statusCode = 500;
                
                API_Log__c Al = New API_Log__c();
               // Al.Account__c=rw.AccountID;
                //Al.Login_Contact__c=rw.contactId;
                Al.EndpointURLName__c='NCP - GetCaseDetails';
                Al.Response__c=e.getMessage()+' at line number: '+e.getLineNumber();
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al;
              }

    }
 }