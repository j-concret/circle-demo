@RestResource(urlMapping='/UpdateOrder/*')
Global class NCPUpdateSO {

    @Httppost
    global static void CPUpdateCostEstimates(){

        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        Blob body = req.requestBody;
        res.addHeader('Content-Type', 'application/json');
        String requestString = body.toString();
        NCPUpdateSOwra rw = (NCPUpdateSOwra)JSON.deserialize(requestString,NCPUpdateSOwra.class);

        try {
            Access_token__c At =[select status__c,Access_Token__c from Access_token__c where Access_Token__c=:rw.Accesstoken ];
            if( at.status__C =='Active' ) {
                Shipment_Order__c AB =[Select Preferred_Freight_method__c,
                                       Number_of_Final_Deliveries_Auto__c,
                                       Number_of_Final_Deliveries_Client__c,
                                       Service_Type__c,
                                       of_Line_Items__c,
                                       Total_Line_Item_Extended_Value__c,
                                       Ship_From_Country__c,
                                       IOR_Price_List__r.name,
                                       Chargeable_Weight__c,
                                       Final_Deliveries_New__c,
                                       Buyer_Account__r.name,
                                       Roll_Out__c,
                                       NCP_Client_Notes__c,
                                       Client_Contact_for_this_Shipment__c,
                                       Shipping_Status__c,
                                       NCP_Cancel_Quote_Reason__c,
                                       Reason_for_cancellation__c,
                                       id,name,Account__C,
                                       Li_ion_Batteries__c,
                                       Type_of_Goods__c,
                                       Lithium_Battery_Types__c,
                                       Client_Reference__c,
                                       Client_Reference_2__c,
                                       Shipment_Value_USD__c,
                                       Request_Email_Estimate__c,
                                       Email_Estimate_To__c,
                                       Who_arranges_International_courier__c,Buyer_Account__c from Shipment_Order__c where Id =:rw.Id];

                IF(AB.of_Line_Items__c > 0){
                    Decimal DATPCCD1=0;
                    AggregateResult[] groupedResults = [SELECT COUNT(ID) CountID,SUM(Total_Value__c) DATPCCD from part__c where Shipment_Order__c =: rw.ID];
                    system.debug('groupedResults (ShipmentOrder) -->'+groupedResults);
                    for(AggregateResult a : groupedResults) {
                        DATPCCD1 = (Decimal) a.get('DATPCCD');
                    }
                    if(DATPCCD1 !=0 || DATPCCD1 != null) {
                        AB.Total_Line_Item_Extended_Value__c = DATPCCD1;
                        AB.Shipment_Value_USD__c = DATPCCD1;
                    }
                }
                else{
                    AB.Shipment_Value_USD__c=rw.Shipment_Value_USD;
                }
                AB.Intention_with_Goods__c = rw.Intention_with_Goods;
                AB.Contact_name__c = rw.Contact_name;
                AB.Beneficial_Owner_Company_Address__c = rw.Beneficial_Owner_Company_Address;
                AB.Intention_with_Goods_Other__c = rw.Intention_with_Goods_Other;
                AB.Buyer_Retains_Ownership__c = rw.Buyer_Retains_Ownership;
                AB.Buyer_or_BO_Part_of_EU__c = rw.Buyer_or_BO_Part_of_EU == NULL ? FALSE : rw.Buyer_or_BO_Part_of_EU;
                AB.Buyer_or_BO_VAT_Number__c = rw.Buyer_or_BO_VAT_Number;
                AB.Beneficial_Owner_Country__c = rw.Beneficial_Owner_Country;

                AB.Li_ion_Batteries__c = Rw.LionBatteries;
                AB.Type_of_Goods__c= rw.TypeOfGoods;
                AB.Lithium_Battery_Types__c = rw.LionBatteriestype;
                AB.Client_Reference__c = rw.Reference1;
                AB.Client_Reference_2__c=rw.Reference2;
                AB.Client_Contact_for_this_Shipment__c=rw.ClientContactForShipment;
                AB.Service_Type__c=rw.ServiceType;
                AB.Ship_From_Country__c=rw.ShipFromCountry;
                AB.Chargeable_Weight__c=rw.ChargeableWeight;
                AB.Final_Deliveries_New__c = rw.FinalDeliveries > AB.Number_of_Final_Deliveries_Auto__c ? rw.FinalDeliveries : AB.Number_of_Final_Deliveries_Auto__c;
                AB.Number_of_Final_Deliveries_Client__c = rw.FinalDeliveries;
                if(rw.ShippingStatus!= null){AB.Buyer_Account__c=rw.BuyerAccount;}
                AB.NCP_Client_Notes__c=rw.NCPClientNotes;
                if(rw.ShippingStatus== null) {AB.Shipping_Status__c=AB.Shipping_Status__C;} else {AB.Shipping_Status__c=rw.ShippingStatus;}
                AB.Reason_for_cancellation__c=rw.ReasonforCancel;
                AB.NCP_Cancel_Quote_Reason__c = rw.NCPCancelQuoteReason;
                AB.Preferred_Freight_method__c = rw.preferredFreight;
                if(rw.AppName=='Zee' && rw.preferredFreight=='Air') {AB.Who_arranges_International_courier__c='TecEx';} else if(rw.AppName=='Zee' && rw.preferredFreight !='Air') {AB.Who_arranges_International_courier__c='Client';}

                Update AB;


                Shipment_Order__c AB1 =[Select Preferred_Freight_method__c,Service_Type__c,Ship_From_Country__c,IOR_Price_List__r.name,Chargeable_Weight__c,Final_Deliveries_New__c,Buyer_Account__r.name,Roll_Out__c,NCP_Client_Notes__c,Client_Contact_for_this_Shipment__c,Shipping_Status__c,NCP_Cancel_Quote_Reason__c,Reason_for_cancellation__c,
                                        id,name,Account__C,Li_ion_Batteries__c,Type_of_Goods__c,Lithium_Battery_Types__c,Client_Reference__c,Client_Reference_2__c,Shipment_Value_USD__c,Request_Email_Estimate__c,Email_Estimate_To__c,Who_arranges_International_courier__c from Shipment_Order__c where Id =:AB.Id];


                JSONGenerator gen = JSON.createGenerator(true);
                gen.writeStartObject();

                gen.writeFieldName('Success');
                gen.writeStartObject();

                if(AB1.ID == null) {gen.writeNullField('Response');} else{gen.writeStringField('Response', 'Success - Record updated '+ Ab1.Name );}
                if(AB1.Service_Type__c == null || AB1.Service_Type__c == '') {gen.writeNullField('ServiceType');} else{gen.writeStringField('ServiceType', +Ab1.Service_Type__c );}
                if(AB1.Ship_From_Country__c == null || AB1.Ship_From_Country__c =='') {gen.writeNullField('ShipFromCountry');} else{gen.writeStringField('ShipFromCountry', +Ab1.Ship_From_Country__c );}
                if(AB1.IOR_Price_List__r.name == null ||AB1.IOR_Price_List__r.name =='') {gen.writeNullField('IORPriceList');} else{gen.writeStringField('IORPriceList', +Ab1.IOR_Price_List__r.name );}
                if(AB1.Chargeable_Weight__c == null) {gen.writeNullField('ChargeableWeight');} else{gen.writeNumberField('ChargeableWeight', +Ab1.Chargeable_Weight__c );}
                if(AB1.Final_Deliveries_New__c == null) {gen.writeNullField('FinalDeliveriesNew');} else{gen.writenumberField('FinalDeliveriesNew', +Ab1.Final_Deliveries_New__c );}
                if(AB1.Buyer_Account__c == null ) {gen.writeNullField('BuyerAccount');} else{gen.writeStringField('BuyerAccount', +Ab1.Buyer_Account__r.name );}
                if(AB1.Roll_Out__c == null ) {gen.writeNullField('RollOut');} else{gen.writeStringField('RollOut', +Ab1.Roll_Out__c );}
                if(AB1.NCP_Client_Notes__c == null || AB1.NCP_Client_Notes__c=='') {gen.writeNullField('NCPClientNotes');} else{gen.writeStringField('NCPClientNotes', +Ab1.NCP_Client_Notes__c );}
                if(AB1.Client_Contact_for_this_Shipment__c == null ) {gen.writeNullField('ClientContactforthisShipment');} else{gen.writeStringField('ClientContactforthisShipment', +Ab1.Client_Contact_for_this_Shipment__c );}
                if(AB1.Shipping_Status__c == null || AB1.Shipping_Status__c == '') {gen.writeNullField('ShippingStatus');} else{gen.writeStringField('ShippingStatus', +Ab1.Shipping_Status__c );}
                if(AB1.NCP_Cancel_Quote_Reason__c == null || AB1.NCP_Cancel_Quote_Reason__c == '') {gen.writeNullField('NCPCancelQuoteReason');} else{gen.writeStringField('NCPCancelQuoteReason', +Ab1.NCP_Cancel_Quote_Reason__c );}
                if(AB1.Reason_for_cancellation__c == null || AB1.Reason_for_cancellation__c == '') {gen.writeNullField('Reasonforcancellation');} else{gen.writeStringField('Reasonforcancellation', +Ab1.Reason_for_cancellation__c );}
                if(AB1.Preferred_Freight_method__c == null || AB1.Preferred_Freight_method__c == '') {gen.writeNullField('PreferredFreightmethod');} else{gen.writeStringField('PreferredFreightmethod', +Ab1.Preferred_Freight_method__c );}
                if(AB1.Who_arranges_International_courier__c == null) {gen.writeNullField('CourierResponsibility');} else{gen.writeStringField('CourierResponsibility', +Ab1.Who_arranges_International_courier__c );}

                gen.writeEndObject();
                gen.writeEndObject();
                String jsonData = gen.getAsString();
                res.responseBody = Blob.valueOf(jsonData);
                res.addHeader('Content-Type', 'application/json');
                res.statusCode = 200;

                API_Log__c Al = New API_Log__c();
                Al.Account__c=AB.Account__c;
                Al.EndpointURLName__c='NCPUpdateSO';
                Al.Response__c='Record Successfully updated' +AB.ID;
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al;
            }
        }
        catch (Exception e) {
            String ErrorString ='Something went wrong while updating cost estimate, please contact support_SF@tecex.com'+e.getMessage();
            res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
            res.addHeader('Content-Type', 'application/json');
            res.statusCode = 500;
            API_Log__c Al = New API_Log__c();
            Al.EndpointURLName__c='NCPUpdateSO';
            Al.Response__c=e.getMessage()+' at line number: '+e.getLineNumber();
            Al.StatusCode__c=string.valueof(res.statusCode);
            Insert Al;
        }
    }

}