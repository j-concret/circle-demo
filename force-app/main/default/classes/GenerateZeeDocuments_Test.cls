@isTest
public class GenerateZeeDocuments_Test {

    static testmethod void testGenerateDocuments(){

        Account account = new Account (name='TecEx Prospective Client', Type ='Client', IOR_Payment_Terms__c = 20.00, VAT_Number__c = '1', BillingStreet= 'Billing Street', BillingCity = 'Test City', BillingState = 'Test State', BillingPostalCode = '987654', BillingCountry = 'Test Country', Total_Amount_Outstanding__c = 1000.00,
                       Total_Amount_Outstanding_0_Current__c = 2000.00, Total_Amount_Outstanding_1_30_Days__c = 2000.00, Total_Amount_Outstanding_31_60_Days__c = 10000.00, Total_Amount_Outstanding_61_90_Days__c = 80000.00, 
                       Total_Amount_Outstanding_Over_90_Days__c = 20000.00, Total_Amount_Credits__c = 100.00 ,CSE_IOR__c = '0050Y000001LTZO');
        insert account;    
    
        country_price_approval__c cpa = new country_price_approval__c( name ='Aruba', Billing_term__c = 'DAP/CIF - IOR pays',  Airwaybill_Instructions__c = 'User IOR Address');
        insert cpa;
        
        Currency_Management2__c CM1 = New Currency_Management2__c(Currency__c = 'Australian Dollar (AUD)',Name = 'Australian Dollar (AUD)',Conversion_Rate__c =0.70772355200000 );
        Insert CM1;
        
        IOR_Price_List__c iorpl = new IOR_Price_List__c ( Client_Name__c = account.Id, Name = 'Aruba', IOR_Fee__c = 0.15, On_Charge_Mark_up__c = 0.15,
        TecEx_Shipping_Fee_Markup__c =0.40, IOR_Min_Fee__c = 100,  Admin_Fee__c =100, Set_up_fee__c =100, Bank_Fees__c =200, Tax_Rate__c = 0.20,
        Estimated_Customs_Brokerage__c =250, Estimated_Customs_Clearance__c =250, Estimated_Customs_Handling__c =250, Estimated_Import_License__c =250);
        insert iorpl; 
     
        Contact contact = new Contact ( lastname ='Testing Individual',  AccountId = account.Id);
        insert contact; 
        
        Opportunity opportunity = new Opportunity(AccountId = account.id, Name ='120-547', Ship_to_Country__c = cpa.Id, IOR_Price_List__c = iorpl.Id, Shipment_Value_USD__c =1000,
        Ship_From_Country__c = 'Algeria', CloseDate= system.today(), StageName ='POD Received', Client_Contact_for_this_Shipment__c = contact.Id, Who_arranges_International_courier__c ='Client', Tax_Treatment__c ='DAP/CIF - IOR pays' );
        insert opportunity;
     
        Shipment_Order__c shipmentOrder = new Shipment_Order__c(Account__c = account.id, Ship_to_Country__c = cpa.Id, IOR_Price_List__c = iorpl.Id, Shipment_Value_USD__c =1000, Opportunity__c = opportunity.Id,
        Client_Contact_for_this_Shipment__c = contact.Id, Who_arranges_International_courier__c ='Client', Tax_Treatment__c ='DAP/CIF - IOR pays', Shipping_Status__c ='Cost Estimate');
        insert shipmentOrder;
        
        List<Part__c> partList = new List<Part__c>();
        
        partList.add(new Part__c(Name ='CAB-ETH-S-RJ45',  Quantity__c= 1, Commercial_Value__c = 15000, US_HTS_Code__c = '85444210', 
        Country_of_Origin2__c = 'China', Shipment_Order__c = shipmentOrder.Id, Description_and_Functionality__c ='AB-ETH-S-RJ45'));
        
        partList.add(new Part__c(Name ='CAB-ETH-S-RJ46',  Quantity__c= 1, Commercial_Value__c = 15000, US_HTS_Code__c = '85478123', 
        Country_of_Origin2__c = 'China', Shipment_Order__c = shipmentOrder.Id, Description_and_Functionality__c ='AB-ETH-S-RJ46'));
        
        partList.add(new Part__c(Name ='CAB-ETH-S-RJ46',  Quantity__c= 1, Commercial_Value__c = 15000, US_HTS_Code__c = '834512879', 
        Country_of_Origin2__c = 'China', Shipment_Order__c = shipmentOrder.Id, Description_and_Functionality__c ='AB-ETH-S-RJ46'));
        
        partList.add(new Part__c(Name ='Testing',  Quantity__c= 1, Commercial_Value__c = 15000, US_HTS_Code__c = '834512879', 
        Country_of_Origin2__c = 'China', Shipment_Order__c = shipmentOrder.Id, Description_and_Functionality__c ='Testing'));
        
        
        partList.add(new Part__c(Name ='Who CAB-ETH-S',  Quantity__c= 1, Commercial_Value__c = 15000, US_HTS_Code__c = '8471254784', 
        Country_of_Origin2__c = 'China', Shipment_Order__c = shipmentOrder.Id, Description_and_Functionality__c ='Testing'));
        
        insert partList;
        
        Customs_Clearance_Documents__c CCD = New Customs_Clearance_Documents__c(Name='CCD001',Tax_Currency__c='Australian Dollar (AUD)',Foreign_Exchange_Rate__c=CM1.Conversion_Rate__c, Customs_Clearance_Documents__c=shipmentOrder.id, Duties_and_Taxes_per_CCD__c = 1000);
        insert CCD;
 
        test.startTest();
       		GenerateZeeDocuments.generateClearanceLetter(shipmentOrder.Id);
        	GenerateZeeDocuments.generateProofOfValuation(shipmentOrder.Id);
        test.stopTest();
    }

}