@IsTest
public class NCPUpdateclientviewtime_Test {

    static testMethod void  testForNCPUpdateCase(){
        
        Access_token__c at = new Access_token__c(Status__c = 'Active',
            												 Access_Token__c = 'asdfghj-sdfgfds-sfdfgf');
        Insert at;
        
        Account acc = new Account(Name = 'Test Account');
        Insert acc;
        
        Contact con = new Contact(LastName = 'Testcon',Client_Notifications_Choice__c = 'Opt-In',
                                  AccountId = acc.Id);
        Insert con;
        
        Case cs = new Case(Subject = 'Test Subject',
                          Status = 'Waiting',
                          Description = 'Description');
        Insert cs;
        
        
        NCPUpdateclientviewtimeWra wrapper = new NCPUpdateclientviewtimeWra();
        wrapper.Accesstoken = at.Access_Token__c;
        wrapper.RecordID = cs.Id;
        
        RestRequest req1 = new RestRequest();
        RestResponse res1 = new RestResponse();
        req1.requestURI = '/services/apexrest/NCPUpdateclientviewtime/'; 
        req1.httpMethod = 'POST';
        req1.requestBody = Blob.valueOf(JSON.serialize(wrapper));     
        RestContext.request = req1;
        RestContext.response = res1;
        RestContext.request.addHeader('Content-Type', 'application/json');
        
        Test.startTest(); 
        	NCPUpdateclientviewtime.NCPUpdateCase();
        Test.stopTest();
    }
    
    static testMethod void  testErrorForNCPUpdateCase(){
                
        RestRequest req1 = new RestRequest();
        RestResponse res1 = new RestResponse();
        req1.requestURI = '/services/apexrest/NCPUpdateCase/'; 
        req1.httpMethod = 'POST';
        req1.requestBody = Blob.valueOf('{"value":"Test Response"}');     
        RestContext.request = req1;
        RestContext.response = res1;
        RestContext.request.addHeader('Content-Type', 'application/json');
        
        Test.startTest(); 
        	NCPUpdateclientviewtime.NCPUpdateCase();
        Test.stopTest();
    }

}