@isTest
public class ProductSpecificComplianceDocumentsTest {
    @testSetup
    public static void setupTestData(){
        Account account = new Account (name='TecEx Prospective Client',  Type ='Client',  CSE_IOR__c = '0050Y000001km5c',  Service_Manager__c = '0050Y000001km5c',  Tax_recovery_client__c  = FALSE, Ship_From_Line_1__c = 'Ship_From_Line_1',
                                       Ship_From_Line_2__c = 'Ship_From_Line_2', Ship_From_Line_3__c = 'Ship_From_Line_3', Ship_From_City__c = 'Ship_From_City',  Ship_From_Zip__c = 'Ship_From_Zip',
                                       Ship_From_Country__c = 'Ship_From_Country',  Ship_From_Other_notes__c = 'Ship_From_Other', Ship_From_Contact_Name__c = 'Ship_From_Contact', Ship_From_Contact_Email__c = 'Ship_From_Email',
                                       Ship_From_Contact_Tel__c = 'Ship_From_Tel', Client_Address_Line_1__c = 'Client_Address_Line_1', Client_Address_Line_2__c = 'Client_Address_Line_2', Client_Address_Line_3__c = 'Client_Address_Line_3',
                                       Client_City__c = 'Client_City', Client_Zip__c = 'Client_Zip', Client_Country__c = 'Client_Country',  Other_notes__c = 'Other_notes',  Tax_Name__c = 'Tax_Name', Tax_ID__c = 'Tax_ID',
                                       Contact_Name__c = 'Contact_Name', Contact_Email__c = 'Contact_Email', Contact_Tel__c = 'Contact_Tel' ); insert account;


        Account account2 = new Account (name='Test Supplier', Type ='Supplier', ICE__c = '0050Y000001km5c', RecordTypeId = '0120Y0000006KjQQAU');
        insert account2;

        Contact contact = new Contact ( lastname ='Testing Individual',  AccountId = account.Id, Client_Notifications_Choice__c ='Opt-In');
        insert contact;

        IOR_Price_List__c iorpl = new IOR_Price_List__c ( Client_Name__c = account.Id, Name = 'Brazil', IOR_Fee__c = 0.15, On_Charge_Mark_up__c = 0.15,
                                                          TecEx_Shipping_Fee_Markup__c =0.40, IOR_Min_Fee__c = 100,  Admin_Fee__c =100, Set_up_fee__c =100, Bank_Fees__c =200, Tax_Rate__c = 0.20,
                                                          Estimated_Customs_Brokerage__c =250, Estimated_Customs_Clearance__c =250, Estimated_Customs_Handling__c =250, Estimated_Import_License__c =250,
                                                          Destination__c = 'Brazil' );
        insert iorpl;

        country_price_approval__c cpa = new country_price_approval__c( name ='Brazil', Billing_term__c = 'DAP/CIF - IOR pays',
                                                                       Airwaybill_Instructions__c = 'User IOR Address', Supplier__c = account2.Id,In_Country_Specialist__c = '0050Y000001km5c', Clearance_Destination__c = 'Brazil', Preferred_Supplier__c = TRUE,Destination__c = 'Brazil' );
        insert cpa;



        Country__c country = new Country__c(Brokerage_Costs__c = 618, CIF_Adjustment_Factor__c = 0.035, CIF_Absolute_value_adjustment__c = 200, Clearance_Costs__c = 488,
                                            Country__c = 'Brazil', Name = 'Brazil', Handling_Costs__c = 1180, License_Permit_Costs__c = 0);
        insert country;

        CPA_v2_0__c aCpav = new CPA_v2_0__c(Name = 'Brazil', Country_Applied_Costings__c = TRUE);
        insert aCpav;

        CPA_v2_0__c cpav2 = new CPA_v2_0__c(Name = 'Brazil Testing 2.0', Supplier__c = account2.Id, Service_Type__c = 'IOR/EOR', Related_Costing_CPA__c = aCpav.Id,
                                            Preferred_Supplier__c = TRUE, VAT_Rate__c = 0.1, Destination__c = 'Brazil',  Final_Destination__c = 'Brazil',
                                            VAT_Reclaim_Destination__c = FALSE, Country__c = country.id, Lead_ICE__c = '0050Y000001km5c',CPA_Required_Documents_Product_Specific__c='ANRT License;CB/IECEE Test Certificate;CE Permission;CE Certificate;Datasheet');
        insert cpav2;
        
        Currency_Management2__c CM = new Currency_Management2__c (Name = 'US Dollar (USD)', Currency__c = 'US Dollar (USD)', ISO_Code__c = 'USD', Conversion_Rate__c = 1);
        insert CM;

        Shipment_Order__c shipmentOrder = new Shipment_Order__c(Account__c = account.id, Shipment_Value_USD__c = 2000, CPA_v2_0__c = cpav2.Id,
                                                                Client_Contact_for_this_Shipment__c = contact.Id, Who_arranges_International_courier__c ='Client', Tax_Treatment__c ='DAP/CIF - IOR pays',
                                                                Ship_to_Country__c = cpa.Id, Destination__c = 'Brazil', Service_Type__c = 'IOR', IOR_Price_List__c = iorpl.Id, Chargeable_Weight__c = 200,
                                                                Shipping_Status__c = 'Cost Estimate Abandoned', Ship_From_Country__c = 'Angola' );
        insert shipmentOrder;
        Country__c worldwide = new Country__c(Name='Worldwide',Country__c='Worldwide');
        insert worldwide;

        insert new Contentversion(
            Title = 'CZDSTOU',
            PathOnClient = 'test',
            FirstPublishLocationId = shipmentOrder.Id,
            VersionData = EncodingUtil.base64Decode('Unit Test Attachment Body')
            );

        Supplier_Invoice__c supplier_Invoice = New Supplier_invoice__C(Name='SI001',Due_date__c= date.today(),Supplier_Name__c=account2.Id,Invoice_Currency__c='US Dollar (USD)',Shipment_Order_Name__c=shipmentOrder.Id,Posted__c= false,Incurred__c=False);
        insert supplier_Invoice;

        Customs_Clearance_Documents__c customs_Clearance_Documents = new Customs_Clearance_Documents__c(Name='Test CCD',Customs_Clearance_Documents__c=shipmentOrder.Id,Date_of_clearance__c=System.today() + 5);
        insert customs_Clearance_Documents;

        Tax_Calculation__c tax_Calculation = new Tax_Calculation__c(Tax_Name__c='Test Tax 1',Shipment_Order__c=shipmentOrder.Id,Value__c=123,Rate__c=50,Applied_To_Value__c=677);
        insert tax_Calculation;

        RecordType recordType = [SELECT Id, Name, DeveloperName, SobjectType FROM RecordType WHERE SobjectType = 'Account' AND DeveloperName = 'Manufacturer' LIMIT 1];
        Account account3 = new Account (name='Test Supplier', Type ='Manufacturer', ICE__c = '0050Y000001km5c', RecordTypeId = recordType.Id);
        insert account3;

        Product2 product = new Product2(Name = 'TestProduct',ProductCode ='TestCODE',Manufacturer__c = account3.Id,Product_Source__c = 'Created from Part',US_HTS_Code__c = '85444210');
        insert product;

        Part__c part1 = new Part__c(Name ='CAB-ETH-S-RJ45',Product__c = product.Id,Quantity__c= 1, Commercial_Value__c = 15000, US_HTS_Code__c = '85444210',
                                    Country_of_Origin2__c = 'China', Shipment_Order__c = shipmentOrder.Id, Description_and_Functionality__c ='AB-ETH-S-RJ45');
        Part__c part2 = new Part__c(Name ='CAB-ETH-S-RJ46',Quantity__c= 1, Commercial_Value__c = 15000, US_HTS_Code__c = '85444210',
                                    Country_of_Origin2__c = 'China', Shipment_Order__c = shipmentOrder.Id, Description_and_Functionality__c ='AB-ETH-S-RJ45');
        insert new List<Part__c> {part1,part2};

        List<Compliance_Document__c> CCDs = new List<Compliance_Document__c> {
            new Compliance_Document__c(Compliance_Document_Name__c ='ANRT License',Country__c=country.Id,Compliance_type__c='Document',Compliance_status__c = 'Available'),
            new Compliance_Document__c(Compliance_Document_Name__c ='Test Certificate (Not EMC/CB)',Country__c=country.Id,Compliance_type__c='Document',Compliance_status__c = 'Available'),
			new Compliance_Document__c(Compliance_Document_Name__c ='CE Certificate',Country__c=worldwide.Id,Compliance_type__c='Document',Compliance_status__c = 'Available')
        };
        Insert CCDs;
        HS_Codes_and_Associated_Details__c HSC = new HS_Codes_and_Associated_Details__c(Name='95865858585',Country__c=country.Id);
        Insert HSC;

        list<Country_Compliance__c> CCOM = new list<Country_Compliance__c>();
        CCOM.add(new Country_Compliance__c(Product__C=product.Id,Country_Compliance__c=CCDs[0].Id,Name='asdf'));
        CCOM.add(new Country_Compliance__c(Product__C=product.Id,Country_Compliance__c=CCDs[0].Id,Name='asdf',HS_Code_and_Associated_Details__c=HSC.ID));
        CCOM.add(new Country_Compliance__c(Product__C=product.Id,Country_Compliance__c=CCDs[1].Id,Name='asdf'));
        CCOM.add(new Country_Compliance__c(Product__C=product.Id,Country_Compliance__c=CCDs[2].Id,Name='asdf'));
        insert CCOM;

        ContentVersion cv1 = new Contentversion(
            Title = 'CZDSTOU',
            PathOnClient = 'test1',
            FirstPublishLocationId = CCDs[0].Id,
            VersionData = EncodingUtil.base64Decode('Unit Test Attachment Body2')
            );

        ContentVersion cv2 = new Contentversion(
            Title = 'CZDSTOURER',
            PathOnClient = 'test2',
            FirstPublishLocationId = CCDs[0].Id,
            VersionData = EncodingUtil.base64Decode('Unit Test Attachment Body2')
            );

        ContentVersion cv3 = new Contentversion(
            Title = 'CZDSTOURER',
            PathOnClient = 'test2',
            FirstPublishLocationId = CCDs[1].Id,
            VersionData = EncodingUtil.base64Decode('Unit Test Attachment Body2')
            );

        ContentVersion cv4 = new Contentversion(
            Title = 'CZDSTOURER',
            PathOnClient = 'test2',
            FirstPublishLocationId = CCDs[2].Id,
            VersionData = EncodingUtil.base64Decode('Unit Test Attachment Body2')
            );

        ContentVersion cv5 = new Contentversion(
            Title = 'CZDSTOURER',
            PathOnClient = 'test2',
            FirstPublishLocationId = part1.Id,
            VersionData = EncodingUtil.base64Decode('Unit Test Attachment Body2')
            );
        ContentVersion cv6 = new Contentversion(
            Title = 'CZDSTOURERSDFSD',
            PathOnClient = 'test2',
            FirstPublishLocationId = part1.Id,
            VersionData = EncodingUtil.base64Decode('Unit Test Attachment Body2')
            );
        ContentVersion cv7 = new Contentversion(
            Title = 'CZDSTOURERSDFSD',
            PathOnClient = 'test2',
            FirstPublishLocationId = part2.Id,
            VersionData = EncodingUtil.base64Decode('Unit Test Attachment Body2')
            );

        List<ContentVersion> cvl = new List<ContentVersion> {cv1,cv2,cv3,cv4,cv5,cv6,cv7};
        insert cvl;
    }

    @isTest
    public static void getAllPartsDetailTest(){
        Test.startTest();
        Map<String, Object> resultMap = ProductSpecificComplianceDocumentsCtrl.getAllPartsDetail([SELECT Id FROM Shipment_Order__c LIMIT 1].Id,0,10);
        Test.stopTest();
        System.assertEquals('OK', resultMap.get('status'));
    }

    @isTest
    public static void updateShipmentPartsTest(){
        Part__c part = [SELECT Id, Name FROM Part__c LIMIT 1];
        Product2 product = [SELECT Id,Name,ProductCode,Manufacturer__c,Product_Source__c FROM Product2 LIMIT 1];
        Test.startTest();
        Map<String, Object> resultMap = ProductSpecificComplianceDocumentsCtrl.updateShipmentParts(part.Id, product, NULL);
        Test.stopTest();
        System.assertEquals('Successfully Updated', resultMap.get('msg'));
    }

    @isTest
    public static void updateProductNotRequiredFieldTest(){
        RecordType recordType = [SELECT Id, Name, DeveloperName, SobjectType FROM RecordType WHERE SobjectType = 'Account' AND DeveloperName = 'Manufacturer' LIMIT 1];
        Account account = new Account (name='Test Supplier', Type ='Manufacturer', ICE__c = '0050Y000001km5c', RecordTypeId = recordType.Id);
        insert account;
        List<Product2> productList = new List<Product2>();
        for(Integer index = 0; index < 5; index++) {
            productList.add(new Product2(Name = 'Product ' + index,ProductCode = 'Product ' + index,
                                         Manufacturer__c = account.Id,Product_Source__c = 'Created from Part'));
        }
        insert productList;
        Part__c part = [SELECT Id FROM Part__c LIMIT 1];
        Map<Id, List<String> > productWithIsRequiredFieldValues = new Map<Id, List<String> > {
            productList[0].Id => new List<String> {'BIS'},
            productList[1].Id => new List<String> {'BIS'},
            productList[2].Id => new List<String> {'BIS'}
        };
        Map<Id, List<String> > partNotRequiredDocs = new Map<Id, List<String> > {
            part.Id => new List<String> {'BIS'}
        };
        Test.startTest();
		ProductSpecificComplianceDocumentsCtrl.updateProductNotRequiredField(productWithIsRequiredFieldValues);
        Test.stopTest();
        
    }

    @isTest
    public static void createCountryComplianceTest(){
        Part__c part = [SELECT Id, Name FROM Part__c LIMIT 1];
        RecordType recordType = [SELECT Id, Name, DeveloperName, SobjectType FROM RecordType WHERE SobjectType = 'Account' AND DeveloperName = 'Manufacturer' LIMIT 1];
        Account account = new Account (name='Test Supplier', Type ='Manufacturer', ICE__c = '0050Y000001km5c', RecordTypeId = recordType.Id);
        insert account;
        Product2 product = new Product2( Name = part.Name, ProductCode = part.Name,
                                         Manufacturer__c = account.Id,Product_Source__c = 'Created from Part');
        insert product;
        Country__c countryObj = new Country__c(Name = 'India', Country__c = 'India');
        insert countryObj;
        Compliance_Document__c compDoc = new Compliance_Document__c(Country__c = countryObj.Id);
        insert compDoc;

        Test.startTest();
		ProductSpecificComplianceDocumentsCtrl.createCountryCompliance(compDoc.Id, product.Id);
        Test.stopTest();
    }
    
    @isTest
    public static void updateShipmentDocNotRequiredTest(){
		Id shipmentId = [SELECT Id FROM Shipment_Order__c LIMIT 1].Id;
        Test.startTest();
        Map<String, Object> resultMap = ProductSpecificComplianceDocumentsCtrl.updateShipmentDocNotRequired(shipmentId,'End-User Statement');
        Test.stopTest();
        System.assertEquals('Updated Successfully!', resultMap.get('msg'));
    }
    
    @isTest
    public static void deleteComplianceDocTest(){
        Test.startTest();
        Map<String, Object> resultMap = ProductSpecificComplianceDocumentsCtrl.deleteComplianceDoc([SELECT Id,Compliance_Document_Name__c FROM Compliance_Document__c LIMIT 1],[SELECT Id FROM Product2 LIMIT 1].Id);
        Test.stopTest();
        System.assertEquals('Updated Successfully!', resultMap.get('msg'));
    }
    
    @isTest
    public static void upsertComplianceDocTest(){
        Part__c part = [SELECT Id, Name FROM Part__c LIMIT 1];
        RecordType recordType = [SELECT Id, Name, DeveloperName, SobjectType FROM RecordType WHERE SobjectType = 'Account' AND DeveloperName = 'Manufacturer' LIMIT 1];
        Account account = new Account (name='Test Supplier', Type ='Manufacturer', ICE__c = '0050Y000001km5c', RecordTypeId = recordType.Id);
        insert account;
        Product2 product = new Product2( Name = part.Name, ProductCode = part.Name,
                                         Manufacturer__c = account.Id,Product_Source__c = 'Created from Part');
        insert product;
        Country__c countryObj = new Country__c(Name = 'India', Country__c = 'India');
        insert countryObj;

        Compliance_Document__c compDoc = new Compliance_Document__c(Country__c = countryObj.Id);

        Test.startTest();
        String result = ProductSpecificComplianceDocumentsCtrl.upsertComplianceDoc(compDoc, product.Id);
        Test.stopTest();
        System.assert(result != null);
    }

    @isTest
    public static void removeAttachmentsTest(){
        Map<Id,Compliance_Document__c> linkedIds = new Map<Id,Compliance_Document__c>([SELECT Id FROM Compliance_Document__c]);
        List<Id> cntIds = new List<Id>();
        for(ContentDocumentLink lnkDoc : [SELECT ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId IN :linkedIds.keySet()]) {
            cntIds.add(lnkDoc.ContentDocumentId);
        }
        Test.startTest();
        ProductSpecificComplianceDocumentsCtrl.removeAttachments(cntIds);
        Test.stopTest();
        System.assertEquals(0, [SELECT Count() FROM ContentDocumentLink WHERE LinkedEntityId IN: linkedIds.keySet()]);
    }
}