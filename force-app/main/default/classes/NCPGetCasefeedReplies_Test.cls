@IsTest
public class NCPGetCasefeedReplies_Test {

    static testMethod void TestForNCPGetCasedetails(){
        Access_token__c accessTokenObj = new Access_token__c(
        	AccessToken_encoded__c='zmM0MDcxOTItZTJiZi00YWRhLTlkZGQtZGZhMmUxM2FiYTlj',
            Access_Token__c='cc407192-e2bf-4ada-9ddd-dfa2e13aba9c',
            Status__c='Active');
        
        Insert accessTokenObj;
        
         Account acc = new Account(Name = 'Acme Test');
        
        Insert acc; 
        
        Contact con = new Contact(
            LastName = 'Test Contact',Client_Notifications_Choice__c='Opt-In',
            AccountId = acc.Id);
        Insert con;
        
        Profile prof = [select id from profile where name LIKE '%Standard%'];
        
        Id shipmentOrderRecTypeId = Schema.SObjectType.shipment_order__c.getRecordTypeInfosByName().get('Cost Estimate').getRecordTypeId();
        
        CpaRules__c cpaRule = new CpaRules__c();
        cpaRule.Country__c = 'Finland';
        cpaRule.Criteria__c = 'Default';
        
        Insert cpaRule;
        
        IOR_Price_List__c priceList = new IOR_Price_List__c();
        priceList.Destination__c = 'Finland';
        priceList.Client_Name__c = acc.Id;
        priceList.IOR_Fee__c = 10;
        
        Insert priceList;
        
        shipment_order__c shipment = new shipment_order__c();
        shipment.RecordTypeId = shipmentOrderRecTypeId;
        shipment.Destination__c = 'Finland';
        shipment.IOR_Price_List__c = priceList.Id;
        shipment.Shipment_Value_USD__c = 600;
        shipment.Account__c = acc.Id;
        
        Insert shipment;
        
        User user = new User();
        user.firstName = 'Firstname1@';
        user.lastName = 'lastname1@';
        user.profileId = prof.id;
        user.email = '123_1@test.com';
        user.username = '123_1@test.com';
        user.EmailEncodingKey = 'ISO-8859-1';
        user.Alias = 'Alias';
        user.TimeZoneSidKey = 'America/Los_Angeles';
        user.LanguageLocaleKey = 'en_US';
        user.LocaleSidKey = 'en_US';
        
        insert user;
        
        Case c = new Case();
        c.Contactid = con.Id;
        c.Subject = 'Test Case Subject';
        c.Status = 'Waiting';
        c.Description = 'My Description';
        c.AccountId = acc.Id;
        c.Shipment_Order__c = shipment.Id;
        c.OwnerId = user.Id;   
        insert c;
         ContentVersion cv = new ContentVersion();
        cv.Title = 'Test Document';
        cv.PathOnClient = 'TestDocument.pdf';
        cv.VersionData = Blob.valueOf('Test Content');
        cv.IsMajorVersion = true;
        
        Insert cv;
        //create and insert post
        FeedItem post = new FeedItem();
        post.Body = 'HelloThere';
        post.ParentId = c.Id;
        post.Title = 'FileName';
        insert post;
        
        FeedComment comment = new FeedComment();
        comment.FeedItemId = post.Id;
        comment.CommentBody = 'Testting';
        comment.RelatedRecordId=cv.Id;
        insert comment;
        
        NCPGetCasefeedReplieswra wrapper = new NCPGetCasefeedReplieswra();
        wrapper.Accesstoken = accessTokenObj.Access_Token__c;
        wrapper.CasefeedID = post.Id;
       
        
        RestRequest req1 = new RestRequest();
        RestResponse res1 = new RestResponse();
        req1.requestURI = '/services/apexrest/NCPAddParticipants/'; 
        req1.httpMethod = 'POST';
        req1.requestBody = Blob.valueOf(JSON.serialize(wrapper));     
        RestContext.request = req1;
        RestContext.response = res1;
        RestContext.request.addHeader('Content-Type', 'application/json');
        
        Test.startTest(); 
        	NCPGetCasefeedReplies.NCPGetCasedetails();
        Test.stopTest();
    }
    
    
     static testMethod void TestForNCPGetCasedetailsCatch(){
        Access_token__c accessTokenObj = new Access_token__c(
        	AccessToken_encoded__c='zmM0MDcxOTItZTJiZi00YWRhLTlkZGQtZGZhMmUxM2FiYTlj',
            Access_Token__c='cc407192-e2bf-4ada-9ddd-dfa2e13aba9c',
            Status__c='Active');
        
        Insert accessTokenObj;
        
         Account acc = new Account(Name = 'Acme Test');
        
        Insert acc; 
        
        Contact con = new Contact(
            LastName = 'Test Contact',Client_Notifications_Choice__c='Opt-In',
            AccountId = acc.Id);
        Insert con;
        
        Profile prof = [select id from profile where name LIKE '%Standard%'];
        
        Id shipmentOrderRecTypeId = Schema.SObjectType.shipment_order__c.getRecordTypeInfosByName().get('Cost Estimate').getRecordTypeId();
        
        CpaRules__c cpaRule = new CpaRules__c();
        cpaRule.Country__c = 'Finland';
        cpaRule.Criteria__c = 'Default';
        
        Insert cpaRule;
        
        IOR_Price_List__c priceList = new IOR_Price_List__c();
        priceList.Destination__c = 'Finland';
        priceList.Client_Name__c = acc.Id;
        priceList.IOR_Fee__c = 10;
        
        Insert priceList;
        
        shipment_order__c shipment = new shipment_order__c();
        shipment.RecordTypeId = shipmentOrderRecTypeId;
        shipment.Destination__c = 'Finland';
        shipment.IOR_Price_List__c = priceList.Id;
        shipment.Shipment_Value_USD__c = 600;
        shipment.Account__c = acc.Id;
        
        Insert shipment;
        
        User user = new User();
        user.firstName = 'Firstname1@';
        user.lastName = 'lastname1@';
        user.profileId = prof.id;
        user.email = '123_1@test.com';
        user.username = '123_1@test.com';
        user.EmailEncodingKey = 'ISO-8859-1';
        user.Alias = 'Alias';
        user.TimeZoneSidKey = 'America/Los_Angeles';
        user.LanguageLocaleKey = 'en_US';
        user.LocaleSidKey = 'en_US';
        
        insert user;
        
        Case c = new Case();
        c.Contactid = con.Id;
        c.Subject = 'Test Case Subject';
        c.Status = 'Waiting';
        c.Description = 'My Description';
        c.AccountId = acc.Id;
        c.Shipment_Order__c = shipment.Id;
        c.OwnerId = user.Id;   
        insert c;
        
        //create and insert post
        FeedItem post = new FeedItem();
        post.Body = 'HelloThere';
        post.ParentId = c.Id;
        post.Title = 'FileName';
        insert post;
        
        FeedComment comment = new FeedComment();
        comment.FeedItemId = post.Id;
        comment.CommentBody = 'Testting';
       // comment.RelatedRecordId ='068';
        insert comment;
        
        NCPGetCasefeedReplieswra wrapper = new NCPGetCasefeedReplieswra();
        wrapper.Accesstoken = null;
        wrapper.CasefeedID = acc.Id;
       
        
        RestRequest req1 = new RestRequest();
        RestResponse res1 = new RestResponse();
        req1.requestURI = '/services/apexrest/NCPAddParticipants/'; 
        req1.httpMethod = 'POST';
        req1.requestBody = Blob.valueOf(JSON.serialize(wrapper));     
        RestContext.request = req1;
        RestContext.response = res1;
        RestContext.request.addHeader('Content-Type', 'application/json');
        
        Test.startTest(); 
        	NCPGetCasefeedReplies.NCPGetCasedetails();
        Test.stopTest();
    }
}