@RestResource(urlMapping='/NCPAccountRelatedInvoices/*')
global class NCPAccountRelatedInvoices {

    @Httppost
    global static void getAccountRelatedInvoices(){
       
       /* RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        res.addHeader('Content-Type', 'application/json');
        Blob body = req.requestBody;
        String requestString = body.toString();
        RequestWrapper reqBody;
        
        */
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        Blob body = req.requestBody;
    	res.addHeader('Content-Type', 'application/json');
        String requestString = body.toString();
        NCPAccountRelatedInvoicesWra rw = (NCPAccountRelatedInvoicesWra)JSON.deserialize(requestString,NCPAccountRelatedInvoicesWra.class);
        
        try {
         
            System.debug('Accesstoken provided ' +rw.Accesstoken); 
          
            List<Access_token__c> At =[select status__c,Access_Token__c from Access_token__c where Access_Token__c=:rw.Accesstoken and status__c ='Active' limit 1];
           
         //   System.debug('Accesstoken size' +At.size());
         
            if(At.size() > 0) {
                if(String.isBlank(rw.AccountID)) {
                    res.responseBody = Blob.valueOf(Util.getResponseString('Error','Invalid Account Id'));
                     res.addHeader('Content-Type', 'application/json');
                    res.statusCode = 400;
                }else{
                    List<Invoice_New__c> invoices = [SELECT Id,Stripe_Link__c,Account__r.Balance_Outstanding_New__c, recordtype.name,Receipt_Date__c,Credits_Applied_Date__c,Name, Account__c, Shipment_Order__c, Today__c, Shipment_Order__r.Name,Shipment_Order__r.NCP_Quote_Reference__c,Shipment_Order__r.Client_Reference__c,Shipment_Order__r.Client_Reference_2__c, Freight_Request__r.Name, Conversion_Rate__c, PO_Number_Override__c, PO_Number__c, Invoice_Sent_Date__c, Amount_Outstanding_Local_Currency__c, Invoice_Name__c, Prepayment_date__c, Due_Date__c, IOR_Fees__c, EOR_Fees__c, Admin_Fees__c,   Actual_IOREOR_Costs__c, International_Freight_Fee__c, Liability_Cover_Fee__c, Total_Amount_Incl_Potential_Cash_Outlay__c, Cost_of_Sale_Shipping_Insurance__c, Taxes_and_Duties__c, Recharge_Tax_and_Duty_Other__c, Customs_Brokerage_Fees__c, Customs_Clearance_Fees__c, Customs_License_In__c, Customs_Handling_Fees__c, Bank_Fee__c, Miscellaneous_Fee__c, Miscellaneous_Fee_Name__c, Invoice_amount_USD__c, Collection_Administration_Fee__c, Amount_Outstanding__c, Invoice_Type__c, Invoice_Currency__c, Possible_Cash_Outlay__c, Cash_Outlay_Fee__c, Invoice_Amount_Local_Currency__c, IOR_EOR_Compliance__c, Government_and_In_country_Summary__c, Freight_and_Related_Costs__c, Billing_City__c, Billing_Country__c, Billing_Postal_Code__c, Billing_Street__c, Billing_State__c, Invoice_Status__c, Ship_To_Country__c, Invoice_Age_Text__c, Invoice_Age__c FROM Invoice_New__c WHERE Account__c =: rw.AccountID
                                                     AND Invoice_Status__c != NULL
                                                     AND Auto_Reversal_User_Stamp__c = NULL AND Invoice_Being_Credited__c = NULL
                                                     AND Invoice_Type__c != 'Through Charge Invoice'
                                                     ORDER BY Due_Date__c DESC];

                    res.responseBody = Blob.valueOf(JSON.serializePretty(invoices));
                     res.addHeader('Content-Type', 'application/json');
                    res.statusCode = 200;
                    insert new API_Log__c(
                        Account__c = rw.AccountID,
                        EndpointURLName__c = 'NCPAccountRelatedInvoices',
                        Response__c = 'Success - Related invoices sent for this account '+rw.AccountID,
                        StatusCode__c = String.valueof(res.statusCode)
                        );
                }
            }
            else{
                res.responseBody = Blob.valueOf(Util.getResponseString('Error','Access Token is invalid or expired.'));
                 res.addHeader('Content-Type', 'application/json');
                res.statusCode = 400;
                insert new API_Log__c(
                    Account__c = rw.AccountID,
                    EndpointURLName__c = 'NCPAccountRelatedInvoices',
                    Response__c = 'Error - Invalid AccessToken',
                    StatusCode__c = String.valueof(res.statusCode)
                    );
            }
          
        }catch(Exception e) {
            String jsonData = Util.getResponseString('Error','Something went wrong while sending related invoices of Account, please contact support_SF@tecex.com');
            res.responseBody = Blob.valueOf(jsonData);
             res.addHeader('Content-Type', 'application/json');
            res.statusCode = 500;
            insert new API_Log__c(
                Account__c = (rw != null && String.isNotBlank(rw.AccountID)) ? rw.AccountID : null,
                EndpointURLName__c = 'NCPAccountRelatedInvoices',
                Response__c = 'Error - NCPAccountRelatedInvoices :'+e.getMessage(),
                StatusCode__c = String.valueof(res.statusCode)
                );
        }
    }

    public class RequestWrapper {
        public String Accesstoken;
        public String AccountID;
    }
}