@IsTest
public class CPCECreateWrapper_Test {
	
	static testMethod void testParse() {
		String json = '{'+
		'\"AccountID\" :\"0010Y00000PEqvHQAT\",'+
		'\"ContactID\" :\"0031v00001hY02HAAS\",'+
		'\"estimatedChargableweight\":0,'+
		'\"ServiceType\": \"IOR\",'+
		'\"Courier_responsibility\": \"TecEx\",'+
		'\"Reference1\": \"Ref1\",'+
		'\"Reference2\": \"Ref2\",'+
		'\"ShipFrom\": \"France\",'+
		'\"ShipTo\": \"Belgium\",'+
		'\"ShipmentvalueinUSD\": 80,'+
		'\"PONumber\": \"TestPONumber\",'+
		'\"Type_of_Goods\" : \"Refurbished\",'+
		'\"Li_ion_Batteries\" : \"Yes\",'+
		'\"Li_ion_BatteryTypes\" : \"Lithium ION batteries\", '+
		'	'+
		'    \"FRContact_Full_Name\": \"Test Full Name\",'+
		'    \"FRContact_Email\": \"test@test.com\",'+
		'    \"FRContact_Phone_Number\": \"35896\",'+
		'    \"FRAddress1\": \"TestAddress1\",'+
		'    \"FRAddress2\": \"TestAddress2\",'+
		'    \"FRCity\": \"TestCity\",'+
		'    \"FRProvince\": \"TestProvince\",'+
		'    \"FRPostal_Code\": \"1234\",'+
		'    \"FRAll_Countries\": \"South Africa\",'+
		'\"ShipmentOrder_Packages\": ['+
		'    {'+
		'      '+
		'      \"Weight_Unit\": \"KGs\",'+
		'      \"Dimension_Unit\": \"CMs\",'+
		'      \"Packages_of_Same_Weight\": 1,'+
		'      \"Length\": 1.00,'+
		'      \"Height\": 1.00,'+
		'      \"Breadth\": 1.00,'+
		'      \"Actual_Weight\": 1.00'+
		'     '+
		'    },'+
		'    {'+
		'      '+
		'      \"Weight_Unit\": \"KGs\",'+
		'      \"Dimension_Unit\": \"CMs\",'+
		'      \"Packages_of_Same_Weight\": 1,'+
		'      \"Length\": 1.00,'+
		'      \"Height\": 1.00,'+
		'      \"Breadth\": 1.00,'+
		'      \"Actual_Weight\": 1.00'+
		'   '+
		'    },'+
		'    {'+
		'    '+
		'      \"Weight_Unit\": \"KGs\",'+
		'      \"Dimension_Unit\": \"CMs\",'+
		'      \"Packages_of_Same_Weight\": 1,'+
		'      \"Length\": 1.00,'+
		'      \"Height\": 1.00,'+
		'      \"Breadth\": 1.00,'+
		'      \"Actual_Weight\": 1.00'+
		'     '+
		'    },'+
		'    {'+
		'     '+
		'      \"Weight_Unit\": \"KGs\",'+
		'      \"Dimension_Unit\": \"CMs\",'+
		'      \"Packages_of_Same_Weight\": 1,'+
		'      \"Length\": 1.00,'+
		'      \"Height\": 1.00,'+
		'      \"Breadth\": 1.00,'+
		'      \"Actual_Weight\": 1.00'+
		'     '+
		'    }'+
		''+
		'  ],'+
		'\"Parts\": ['+
		'    {'+
		'      '+
		'      \"PartNumber\": \"Test\",'+
		'      \"PartDescription\": \"Test\",'+
		'      \"Quantity\": 1,'+
		'      \"UnitPrice\": 1.00,'+
		'      \"HSCode\": \"8456879658\",'+
		'      \"CountryOfOrigin\": \"USA\",'+
		'      \"ECCNNo\": \"1234\"'+
		'     '+
		'    },'+
		'    {'+
		'      '+
		'      \"PartNumber\": \"Test\",'+
		'      \"PartDescription\": \"Test\",'+
		'      \"Quantity\": 1,'+
		'      \"UnitPrice\": 1.00,'+
		'      \"HSCode\": \"8456879658\",'+
		'      \"CountryOfOrigin\": \"USA\",'+
		'      \"ECCNNo\": \"1234\"'+
		'   '+
		'    },'+
		'    {'+
		'    '+
		'      \"PartNumber\": \"Test\",'+
		'      \"PartDescription\": \"Test\",'+
		'      \"Quantity\": 1,'+
		'      \"UnitPrice\": 1.00,'+
		'      \"HSCode\": \"8456879658\",'+
		'      \"CountryOfOrigin\": \"USA\",'+
		'      \"ECCNNo\": \"1234\"'+
		'     '+
		'    },'+
		'    {'+
		'     '+
		'      \"PartNumber\": \"Test\",'+
		'      \"PartDescription\": \"Test\",'+
		'      \"Quantity\": 1,'+
		'      \"UnitPrice\": 1.00,'+
		'      \"HSCode\": \"8456879658\",'+
		'      \"CountryOfOrigin\": \"USA\",'+
		'      \"ECCNNo\": \"1234\"'+
		'     '+
		'    }'+
		''+
		'  ],'+
		'  \"FinalDelivieries\": ['+
		'    {'+
		'      '+
		'    \"Name\": \"Test\",'+
		'    \"Contact_Full_Name\": \"Test Full Name\",'+
		'    \"Contact_Email\": \"test@test.com\",'+
		'    \"Contact_Phone_Number\": \"3589624216545120\",'+
		'    \"Address1\": \"TestAddress1\",'+
		'    \"Address2\": \"TestAddress2\",'+
		'    \"City\": \"TestCity\",'+
		'    \"Province\": \"TestProvince\",'+
		'    \"Postal_Code\": \"1234\",'+
		'    \"All_Countries\": \"South Africa\"'+
		'   '+
		'     '+
		'    },'+
		'    {'+
		'      '+
		'    \"Name\": \"Test\",'+
		'    \"Contact_Full_Name\": \"Test Full Name\",'+
		'    \"Contact_Email\": \"test@test.com\",'+
		'    \"Contact_Phone_Number\": \"3589624216545120\",'+
		'    \"Address1\": \"TestAddress1\",'+
		'	\"Address2\": \"TestAddress2\",'+
		'    \"City\": \"TestCity\",'+
		'    \"Province\": \"TestProvince\",'+
		'    \"Postal_Code\": \"1234\",'+
		'    \"All_Countries\": \"South Africa\"'+
		'    '+
		'   '+
		'    },'+
		'    {'+
		'    '+
		'    \"Name\": \"Test\",'+
		'    \"Contact_Full_Name\": \"Test Full Name\",'+
		'    \"Contact_Email\": \"test@test.com\",'+
		'    \"Contact_Phone_Number\": \"3589624216545120\",'+
		'    \"Address1\": \"TestAddress1\",'+
		'	\"Address2\": \"TestAddress2\",'+
		'    \"City\": \"TestCity\",'+
		'    \"Province\": \"TestProvince\",'+
		'    \"Postal_Code\": \"1234\",'+
		'    \"All_Countries\": \"South Africa\"'+
		'    '+
		'     '+
		'    },'+
		'    {'+
		'     '+
		'    \"Name\": \"Test\",'+
		'    \"Contact_Full_Name\": \"Test Full Name\",'+
		'    \"Contact_Email\": \"test@test.com\",'+
		'    \"Contact_Phone_Number\": \"3589624216545120\",'+
		'    \"Address1\": \"TestAddress1\",'+
		'    \"Address2\": \"TestAddress2\",'+
		'    \"City\": \"TestCity\",'+
		'    \"Province\": \"TestProvince\",'+
		'    \"Postal_Code\": \"1234\",'+
		'    \"All_Countries\": \"South Africa\"'+
		'   '+
		'     '+
		'    }'+
		''+
		'  ]'+
		''+
		'}';
		CPCECreateWrapper obj = CPCECreateWrapper.parse(json);
		System.assert(obj != null);
	}
}