@RestResource(urlMapping='/GetDefaults/*')
global class CPDefaultsGet {
    
      @Httpget
    global static void CPDefaultsGetRecords(){
        
        String jsonData='Test';
        RestRequest req = RestContext.request;
    	RestResponse res = RestContext.response;
        res.addHeader('Content-Type', 'application/json');
        String ClientID = req.params.get('ClientAccId');
     
        Id recordTypeId = Schema.SObjectType.Client_Address__c.getRecordTypeInfosByDeveloperName().get('PickUp').getRecordTypeId();
        List<CPDefaults__c> CPD = [Select ID,Ship_from__c,Name,Order_Type__c,Client_Account__c,Chargeable_Weight_Units__c,Li_ion_Batteries__c,Package_Dimensions__c,Package_Weight_Units__c,Second_hand_parts__c,Service_Type__c,TecEx_to_handle_freight__c from CPDefaults__c where Client_Account__c=:ClientID];
        List<Client_Address__c> ABs = [select id,Name,Address_Status__c,default__c,Comments__c,CompanyName__c,AdditionalContactNumber__c,Contact_Full_Name__c,Contact_Email__c,Contact_Phone_Number__c,Address__c,Address2__c,City__c,Province__c,Postal_Code__c,All_Countries__c,Client__c    from Client_Address__c where Client__c =:ClientID  and RecordType.ID =:recordTypeId and default__C= true ORDER BY All_Countries__c desc];
           
        
        try {
         If(!CPD.Isempty()|| !ABs.Isempty()){
             
             JSONGenerator gen = JSON.createGenerator(true);
             gen.writeStartObject();
             
             
          If(!CPD.isEmpty()){
             //Creating Array & Object - Client Defaults
             gen.writeFieldName('Client Default values');
             	gen.writeStartArray();
     		         for(CPDefaults__c CPD1 :CPD){
        				//Start of Object - SClient Defaults	
                         gen.writeStartObject();
                          
                          if(CPD1.Id == null) {gen.writeNullField('Id');} else{gen.writeStringField('Id', CPD1.Id);}
                          if(CPD1.Name == null) {gen.writeNullField('Default Name');} else{gen.writeStringField('Default Name', CPD1.Name);}
                          if(CPD1.Chargeable_Weight_Units__c == null) {gen.writeNullField('Chargeable Weight Units');} else{gen.writeStringField('Chargeable Weight Units', CPD1.Chargeable_Weight_Units__c);}
                          if(CPD1.Client_Account__c == null) {gen.writeNullField('Client Account');} else{gen.writeStringField('Client Account', CPD1.Client_Account__c);}
                          if(CPD1.Li_ion_Batteries__c == null) {gen.writeNullField('Li-ion Batteries?');} else{gen.writeStringField('Li-ion Batteries?', CPD1.Li_ion_Batteries__c);}
                          if(CPD1.Order_Type__c == null) {gen.writeNullField('Order Type');} else{gen.writeStringField('Order Type', CPD1.Order_Type__c);}
                          if(CPD1.Package_Dimensions__c == null) {gen.writeNullField('Package Dimensions');} else{gen.writeStringField('Package Dimensions', CPD1.Package_Dimensions__c);}
                          if(CPD1.Package_Weight_Units__c == null) {gen.writeNullField('Package Weight Units');} else{gen.writeStringField('Package Weight Units', CPD1.Package_Weight_Units__c);}
                          if(CPD1.Second_hand_parts__c == null) {gen.writeNullField('Second hand parts?');} else{gen.writeStringField('Second hand parts?', CPD1.Second_hand_parts__c);}
                          if(CPD1.Service_Type__c == null) {gen.writeNullField('Service Type');} else{gen.writeStringField('Service Type', CPD1.Service_Type__c);}
                          if(CPD1.TecEx_to_handle_freight__c == null) {gen.writeNullField('TecEx to handle freight?');} else{gen.writeStringField('TecEx to handle freight?', CPD1.TecEx_to_handle_freight__c);}
                            if(CPD1.ship_From__c == null) {gen.writeNullField('Ship From Country');} else{gen.writeStringField('Ship From Country', CPD1.ship_From__c);}             
        					gen.writeEndObject();
                         //End of Object - Client Defaults
    					}
    		gen.writeEndArray();
                //End of Array - Client Defaults 
         }   
             
             
             
              If(!ABs.isEmpty()){
             //Creating Array & Object - PIckUp Address
             gen.writeFieldName('Pickup Address');
             	gen.writeStartArray();
     		         for(Client_Address__c ABS1 :ABs){
        				//Start of Object - Pickup Address	
                         gen.writeStartObject();
                          
                          if(ABS1.Id == null) {gen.writeNullField('Id');} else{gen.writeStringField('Id', ABS1.Id);}
                          if(ABS1.Name == null) {gen.writeNullField('Pickupaddress Name');} else{gen.writeStringField('Pickupaddress Name', ABS1.Name);}
                          if(ABS1.default__c == null) {gen.writeNullField('Default');} else{gen.writeBooleanField('Default', ABS1.default__c);}
                          if(ABS1.Comments__c == null) {gen.writeNullField('Comments');} else{gen.writeStringField('Comments', ABS1.Comments__c);}
                          if(ABS1.CompanyName__c == null) {gen.writeNullField('CompanyName');} else{gen.writeStringField('CompanyName', ABS1.CompanyName__c);}
                          if(ABS1.AdditionalContactNumber__c == null) {gen.writeNullField('AdditionalContactNumber');} else{gen.writeStringField('AdditionalContactNumber', ABS1.AdditionalContactNumber__c);}
                          if(ABS1.Contact_Full_Name__c == null) {gen.writeNullField('Contact Full Name');} else{gen.writeStringField('Contact Full Name', ABS1.Contact_Full_Name__c);}
                          if(ABS1.Contact_Email__c == null) {gen.writeNullField('Contact Email');} else{gen.writeStringField('Contact Email', ABS1.Contact_Email__c);}
                          if(ABS1.Contact_Phone_Number__c == null) {gen.writeNullField('Contact Phone Number');} else{gen.writeStringField('Contact Phone Number', ABS1.Contact_Phone_Number__c);}
                          if(ABS1.Address__c == null) {gen.writeNullField('Address');} else{gen.writeStringField('Address', ABS1.Address__c);}
                          if(ABS1.Address2__c == null) {gen.writeNullField('Address2');} else{gen.writeStringField('Address2', ABS1.Address2__c);}
                          if(ABS1.City__c == null) {gen.writeNullField('City');} else{gen.writeStringField('City', ABS1.City__c);}
                          if(ABS1.Province__c == null) {gen.writeNullField('Province');} else{gen.writeStringField('Province', ABS1.Province__c);}
                          if(ABS1.Postal_Code__c == null) {gen.writeNullField('Postal Code');} else{gen.writeStringField('Postal Code', ABS1.Postal_Code__c);}
                          if(ABS1.All_Countries__c == null) {gen.writeNullField('Country');} else{gen.writeStringField('Country', ABS1.All_Countries__c);}
                          if(ABS1.Address_Status__c == null) {gen.writeNullField('Address Status');} else{gen.writeStringField('Address Status', ABS1.Address_Status__c);}
                          if(ABS1.Client__c == null) {gen.writeNullField('Client');} else{gen.writeStringField('Client', ABS1.Client__c);}
                          
                         
        					gen.writeEndObject();
                         //End of Object - Pickup Address
    					}
    		gen.writeEndArray();
                //End of Array - Pickup Address 
         }   
             
             
             
             gen.writeEndObject();
    		jsonData = gen.getAsString();
             
             
              res.responseBody = Blob.valueOf(jsonData);
              res.statusCode = 200;
                
                API_Log__c Al = New API_Log__c();
                Al.Account__c=clientID;
                Al.EndpointURLName__c='GetDefaults';
                Al.Response__c='Success - Client Defaults are sent';
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al;
             
             
                  }
         
         
            }
        
        catch(exception e){
            System.debug('Line number ====> '+e.getLineNumber() + 'Exception Message =====> '+e.getMessage());
            
           		  String ErrorString ='Default values are not found';
               	  res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
            	res.statusCode = 500;
                API_Log__c Al = New API_Log__c();
                Al.Account__c=ClientID;
                Al.EndpointURLName__c='SORelatedObjects';
                Al.Response__c=e.getMessage()+' at line number: '+e.getLineNumber();
                Al.StatusCode__c=string.valueof(res.statusCode);
              	Insert Al;
            
            
        }
   }

}