@RestResource(urlMapping='/EditParts/*')
Global class CPPartEdit {
    
    @Httppost
    global static void CPPartEdit(){
    	RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        Blob body = req.requestBody;
        res.addHeader('Content-Type', 'application/json');
        String requestString = body.toString();
        CPPartEditWrapper rw = (CPPartEditWrapper)JSON.deserialize(requestString,CPPartEditWrapper.class);
      try {
           If(!rw.Parts.isEmpty()){
              list<id> PartsRec = new list<id>();
              list<Part__c > AllParts = new list<Part__c >();
              list<Part__c > UpdateParts = new list<Part__c >();
              String Actualshipmentstatus;
               
               shipment_Order__C So = [Select id,RecordTypeId,RecordType.Name,shipping_status__c,Account__c,Client_Contact_for_this_Shipment__c from shipment_order__C where Id =:rw.parts[0].soid];
               Actualshipmentstatus =SO.shipping_status__c;
            /*   if(SO.RecordType.Name=='Shipment Order'){  SO.shipping_status__c='Shipment Abandoned';} else{ SO.shipping_status__c='Cost Estimate Abandoned';}
               System.debug('Actualshipmentstatus1-->'+Actualshipmentstatus);
               Update SO;
             */ 
               for(Integer i=0; rw.Parts.size()>i;i++) {
                   PartsRec.add(rw.Parts[i].PartID);
                   }
           
               AllParts = [Select Name,Description_and_Functionality__c,Quantity__c,Commercial_Value__c,US_HTS_Code__c,Country_of_Origin2__c,ECCN_NO__c,Type_of_Goods__c,Li_ion_Batteries__c,Lithium_Battery_Types__c from Part__c  where id in :PartsRec ];
           
                for(Integer i=0;AllParts.size()>i;i++) {
                   for(Integer j=0;rw.parts.size()>j;j++) { 
                       if(AllParts[i].ID == rw.parts[j].PartID) {
                           
                           AllParts[i].Name =rw.parts[j].PartNumber;
                           AllParts[i].Description_and_Functionality__c =rw.parts[j].PartDescription;
                           AllParts[i].Quantity__c =rw.parts[j].Quantity;
                           AllParts[i].Commercial_Value__c=rw.parts[j].UnitPrice;
                           AllParts[i].US_HTS_Code__c =rw.parts[j].HSCode;
                           AllParts[i].Country_of_Origin2__c =rw.parts[j].CountryOfOrigin;
                           AllParts[i].ECCN_NO__c =rw.parts[j].ECCNNo;
                           AllParts[i].Type_of_Goods__c=rw.parts[j].Type_of_Goods;
                           AllParts[i].Li_ion_Batteries__c=rw.parts[j].Li_ion_Batteries;
                           if(rw.parts[j].Li_ion_Batteries=='YES'){ AllParts[i].Lithium_Battery_Types__c=rw.parts[j].Li_ion_BatteryTypes; }
                       
                           UpdateParts.add(AllParts[i]);
                       }
                    
                   }
                }
               Update UpdateParts;
               
           /*    System.debug('Actualshipmentstatus2-->'+Actualshipmentstatus);
               SO.shipping_status__c=Actualshipmentstatus;
               Update SO;
             */  
                  String ErrorString ='Parts updated';
               	  res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
        	  	  res.statusCode = 200;
               
                API_Log__c Al = New API_Log__c();
                Al.Account__c=SO.Account__C;
                Al.Login_Contact__c=So.Client_Contact_for_this_Shipment__c;
                Al.EndpointURLName__c='EditParts';
                Al.Response__c='Success - Parts updated';
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al;
           }
          
      }
        Catch(Exception e){
            
             System.debug('Line number ====> '+e.getLineNumber() + 'Exception Message =====> '+e.getMessage());
            
           		  String ErrorString ='Something went wrong, Please contact sf';
               	  res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
            	res.statusCode = 500;
                API_Log__c Al = New API_Log__c();
                // Al.Account__c=SO.Account__C;
                //Al.Login_Contact__c=So.Client_Contact_for_this_Shipment__c;
                Al.EndpointURLName__c='EditParts';
                Al.Response__c='error - Parts not updated'+e.getLineNumber()+e.getMessage();
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al;
            
        }
        
        
    }
}