@RestResource(urlMapping='/GetUserProfile/*')
Global class NCPGetUserProfile {
    
    
     @Httppost
      global static void NCPGetCourierServices(){
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        Blob body = req.requestBody;
    	res.addHeader('Content-Type', 'application/json');
        String requestString = body.toString();
        NCPGetUserProfileWra rw  = (NCPGetUserProfileWra)JSON.deserialize(requestString,NCPGetUserProfileWra.class);
          try{
              //Accesstoken test has to come here
              Access_token__c At =[select status__c,Access_Token__c from Access_token__c where Access_Token__c=:rw.Accesstoken ];
            if( at.status__C =='Active'){
                Contact con = new Contact();
             con=[Select Id, name,Include_in_SO_Communication__c,Contact_Role__c,Client_Push_Notifications_Choice__c,New_Task__c,Task_Completion__c,Quote_status_updates__c,Major_Updates__c,Minor_Updates__c, (select Id, Username , LastName , FirstName , Name , CompanyName , Division , Department , Title , Street , City , State , PostalCode , Country , Address , Email , Phone , Fax , MobilePhone , IsActive , UserRoleId , ProfileId , UserType , LastLoginDate ,  ContactId , AccountId , FullPhotoUrl  from Users) from Contact where id=:rw.contactid limit 1];
                
                
              if(con.Id != Null){
             res.responseBody = Blob.valueOf(JSON.serializePretty(Con));
             res.addHeader('Content-Type', 'application/json');
             res.statusCode = 200;
                
                API_Log__c Al = New API_Log__c();
                Al.EndpointURLName__c='GetUserProfile';
                Al.Login_Contact__c=rw.contactID;
                Al.StatusCode__c=string.valueof(res.statusCode);
                AL.Response__c = 'Success - User Profile details are sent';
                Insert Al;  
              }
              Else{
                  
            JSONGenerator gen = JSON.createGenerator(true);
            gen.writeStartObject();
            
             gen.writeFieldName('Success');
             gen.writeStartObject();
            
             	if(rw.ContactID == null) {gen.writeNullField('Response');} else{gen.writeStringField('Response', 'Trackings are not available');}
            
            gen.writeEndObject();
            gen.writeEndObject();
             res.responseBody = Blob.valueOf(gen.getAsString());
            res.addHeader('Content-Type', 'application/json');
            res.statusCode = 400;        
            
                  
                  
              }
              
            }  
            } catch(Exception e){
                
                String ErrorString ='Something went wrong, please contact support_SF@tecex.com';
               		res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
                   res.addHeader('Content-Type', 'application/json');
        			res.statusCode = 400;
                
                API_Log__c Al = New API_Log__c();
                //Al.Account__c=rw.AccountID;
                Al.Login_Contact__c=rw.contactID;
                Al.EndpointURLName__c='GetUserProfile';
                Al.Response__c=ErrorString;
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al;
                      
          }
          
          
      
      
      
      
      
      }


}