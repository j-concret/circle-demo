public class CPfinaldeladdCreationwrapper {
     public List<FianlDelAddress> FianlDelAddress;

	public class FianlDelAddress {
		public String Name;
		public String Contact_Full_Name;
		public String Contact_Email;
		public String Contact_Phone_Number;
		public String Address1;
		public String Address2;
		public String City;
		public String Province;
		public String Postal_Code;
		public String All_Countries;
		public String ClientID;
         public String AdditionalNumber;
     	 public String Comments;
         public String CompanyName;
	}

	
	public static CPfinaldeladdCreationwrapper parse(String json) {
		return (CPfinaldeladdCreationwrapper) System.JSON.deserialize(json, CPfinaldeladdCreationwrapper.class);
	}


}