public class FeedItem_Trigger_Handler {
    public static void updateCaseAndTask(List <FeedItem> feedItemList){
        List<Case> casesToUpdate = new List<Case>();
        List<Task> tasksToUpdate = new List<Task>();
        List<Id> FeedItemIds = new List<Id>();
        List<String> shipmentIds = new List<String>();
        
        List<FeedItem> FeedItemList1 = [SELECT Body,CreatedBy.Profile.Name,ParentId,CreatedDate,InsertedById
                                        FROM FeedItem WHERE Id IN :feedItemList];
        
        User us = [SELECT Id, Name, fullphotourl FROM User WHERE Id =:FeedItemList1[0].InsertedById ];
        
        
        for (FeedItem fi : FeedItemList1) {
            String feedbody = '';
            if(fi.Body != null) {
                feedbody = fi.Body.left(35);
            }
            
            
            if (fi.ParentId.getSObjectType() == Shipment_order__c.SObjectType) {
                shipmentIds.add(fi.ParentId);
            }
            else if (fi.ParentId.getSObjectType() == Case.SObjectType) {                
                
                if(fi.body!=null) {
                    FeedItemIds.add(Fi.Id);
                }
                casesToUpdate.add(
                    (fi.CreatedBy.Profile.Name == 'Tecex Customer') ?
                    new Case(
                        Id = fi.ParentId,
                        LastClientMsgTime__c = fi.CreatedDate,
                        ClientlatestComment__c=feedbody
                        
                    ) :
                    new Case(
                        Id = fi.ParentId,
                        LastMessageTime__c = fi.CreatedDate,
                        FullphotoURL__c = us.fullphotourl,
                        ClientlatestComment__c=feedbody
                    )
                );
            }
            else if (fi.ParentId.getSObjectType() == Task.SObjectType) {
                if(fi.body!=null) {
                    FeedItemIds.add(Fi.Id);
                }
                tasksToUpdate.add((fi.CreatedBy.Profile.Name == 'Tecex Customer') ?
                                  new Task(
                                      Id = fi.ParentId,
                                      LastClientMsgTime__c = fi.CreatedDate,
                                      ClientlatestComment__c=feedbody,
                                      ClientlatestCommentUser__c = us.Name
                                  ) :
                                  new Task(
                                      Id = fi.ParentId,
                                      LastMessageTime__c = fi.CreatedDate,
                                      ClientlatestComment__c=feedbody,
                                      ClientlatestCommentUser__c = us.Name
                                  )
                                 );
            }
            
        }
        if(!casesToUpdate.isEmpty()) {
            update casesToUpdate;
        }
        if(!tasksToUpdate.isEmpty()) {
            update tasksToUpdate;
        }
        if(!shipmentIds.isEmpty()) {
            Eventbus.publish(new List<TaskChange__e> {new TaskChange__e(ShipmentOrderIds__c = String.join(shipmentIds,','),Source__c='FeedItem_Trigger')});
        }
        if(!FeedItemList1.isEmpty()){
            feedItem_CaseMail.sendFeedItemMail(FeedItemList1);
        }
        
    }
}