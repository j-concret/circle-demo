@IsTest
public class QueueableGenerateZeeInvoice_Test {
    
    @testsetup
    public static void  setUpData(){
        
        User thisUser = [select Id from User where Id = :UserInfo.getUserId()]; 
        
        System.runAs(thisUser) 
        { 
            EmailTemplate et = new EmailTemplate(); 
            et.isActive = true; 
            et.Name = 'testTemplate'; 
            et.DeveloperName = 'testTemplate' + System.now().getTime(); 
            et.TemplateType = 'text'; 
            et.FolderId = UserInfo.getUserId(); 
            et.Body = ''; 
            
            insert et; 
        }
        
        List<GL_Account__c> glAccounts = new List<GL_Account__c>();
        glAccounts.add(new GL_Account__c(Name = '70001', GL_Account_Description__c = 'Prepaid Expenses', GL_Account_Short_Description__c = 'PE'));
        glAccounts.add(new GL_Account__c(Name = '70000', GL_Account_Description__c = 'Revenue Received In Advance', GL_Account_Short_Description__c = 'RRIA'));
        glAccounts.add(new GL_Account__c(Name = '123456', GL_Account_Description__c = 'Forex', GL_Account_Short_Description__c = 'Forex'));
        glAccounts.add(new GL_Account__c(Name = 'Net Taxes', GL_Account_Description__c = 'Net Taxes', GL_Account_Short_Description__c = 'Net Taxes'));
        glAccounts.add(new GL_Account__c(Name = '999999', GL_Account_Description__c = 'Revenue Received In Advance', GL_Account_Short_Description__c = 'RevRecInAdv'));
        glAccounts.add(new GL_Account__c(Name = '110000', GL_Account_Description__c = 'Revenue', GL_Account_Short_Description__c = 'Revenue'));
        glAccounts.add(new GL_Account__c(Name = '120000', GL_Account_Description__c = 'Cost of Sales', GL_Account_Short_Description__c = 'Cost of Sale'));
        glAccounts.add(new GL_Account__c(Name = '314500', GL_Account_Description__c = 'Bank Charges', GL_Account_Short_Description__c = 'Bank Charges'));
        glAccounts.add(new GL_Account__c(Name = '610000', GL_Account_Description__c = 'Customer Control Account', GL_Account_Short_Description__c = 'AR Control'));
        glAccounts.add(new GL_Account__c(Name = '910500', GL_Account_Description__c = 'Unallocated Receipts', GL_Account_Short_Description__c = 'Unallocated'));
        glAccounts.add(new GL_Account__c(Name = '910501', GL_Account_Description__c = 'Unallocated Receipts Customers', GL_Account_Short_Description__c = 'UnallocatedC'));
        glAccounts.add(new GL_Account__c(Name = '920000', GL_Account_Description__c = 'Supplier Control Account', GL_Account_Short_Description__c = 'AP Control'));
        glAccounts.add(new GL_Account__c(Name = '921000', GL_Account_Description__c = 'Accruals', GL_Account_Short_Description__c = 'Accruals'));
        insert glAccounts;
        
        
        List<Dimension__c> dimensionList = new List<Dimension__c>();
        dimensionList.add(new Dimension__c(Name = 'MASELIM',  Dimension_Description__c = 'Test 610000', Short_Dimension_Description__c = 'Test 610000', Dimension_Type_Code__c = 'PRO'));
        dimensionList.add(new Dimension__c(Name = 'USA',  Dimension_Description__c = 'Test 110000', Short_Dimension_Description__c = 'Test 110000', Dimension_Type_Code__c = 'PRO'));
        dimensionList.add(new Dimension__c(Name ='CES',Dimension_Description__c ='Catalyst Energy Solutions',Short_Dimension_Description__c = 'CES',Dimension_Type_Code__c = 'BRA'));
        dimensionList.add(new Dimension__c(Name ='CHENR',Dimension_Description__c ='Rita Chen',Short_Dimension_Description__c = 'ChenR',Dimension_Type_Code__c = 'BRA'));
        dimensionList.add(new Dimension__c(Name ='CHICAGO',Dimension_Description__c ='Chicago',Short_Dimension_Description__c = 'Chicago',Dimension_Type_Code__c = 'BRA'));
        dimensionList.add(new Dimension__c(Name ='CHURCHA',Dimension_Description__c ='Andrew Church',Short_Dimension_Description__c = 'ChurchA',Dimension_Type_Code__c = 'BRA'));
        dimensionList.add(new Dimension__c(Name ='COETZEEA',Dimension_Description__c ='Andre Coetzee',Short_Dimension_Description__c = 'CoetzeeA',Dimension_Type_Code__c = 'BRA'));
        dimensionList.add(new Dimension__c(Name ='COLLECTIONS',Dimension_Description__c ='Collections',Short_Dimension_Description__c = 'Collections',Dimension_Type_Code__c = 'BRA'));
        dimensionList.add(new Dimension__c(Name ='CONCUR',Dimension_Description__c ='Concur',Short_Dimension_Description__c = 'Concur',Dimension_Type_Code__c = 'BRA'));
        dimensionList.add(new Dimension__c(Name ='DATA-FILING',Dimension_Description__c ='Data and Filing',Short_Dimension_Description__c = 'Data-Filing',Dimension_Type_Code__c = 'BRA'));
        dimensionList.add(new Dimension__c(Name ='DELHI',Dimension_Description__c ='Delhi',Short_Dimension_Description__c = 'Delhi',Dimension_Type_Code__c = 'BRA'));
        dimensionList.add(new Dimension__c(Name ='DOMAHK',Dimension_Description__c ='Keshav Domah',Short_Dimension_Description__c = 'DomahK',Dimension_Type_Code__c = 'BRA'));
        dimensionList.add(new Dimension__c(Name ='DUBAI',Dimension_Description__c ='Dubai',Short_Dimension_Description__c = 'Dubai',Dimension_Type_Code__c = 'BRA'));
        dimensionList.add(new Dimension__c(Name ='DURBAN',Dimension_Description__c ='Durban',Short_Dimension_Description__c = 'Durban',Dimension_Type_Code__c = 'BRA'));
        dimensionList.add(new Dimension__c(Name ='EDWARDESG',Dimension_Description__c ='Gareth Edwardes',Short_Dimension_Description__c = 'EdwardesG',Dimension_Type_Code__c = 'BRA'));
        dimensionList.add(new Dimension__c(Name ='EUROPEENT',Dimension_Description__c ='Europe Enterprise',Short_Dimension_Description__c = 'EuropeEnt',Dimension_Type_Code__c = 'BRA'));
        dimensionList.add(new Dimension__c(Name ='FERRERJ',Dimension_Description__c ='Jonathan Ferrer',Short_Dimension_Description__c = 'FerrerJ',Dimension_Type_Code__c = 'BRA'));
        dimensionList.add(new Dimension__c(Name ='FINANCE',Dimension_Description__c ='Finance',Short_Dimension_Description__c = 'Finance',Dimension_Type_Code__c = 'BRA'));
        dimensionList.add(new Dimension__c(Name ='FRANKFURT',Dimension_Description__c ='Frankfurt',Short_Dimension_Description__c = 'Frankfurt',Dimension_Type_Code__c = 'BRA'));
        dimensionList.add(new Dimension__c(Name ='GERBERD',Dimension_Description__c ='Dean Gerber',Short_Dimension_Description__c = 'GerberD',Dimension_Type_Code__c = 'BRA'));
        dimensionList.add(new Dimension__c(Name ='GOLDMANB',Dimension_Description__c ='Bradley Goldman',Short_Dimension_Description__c = 'GoldmanB',Dimension_Type_Code__c = 'BRA'));
        dimensionList.add(new Dimension__c(Name ='GROUP',Dimension_Description__c ='Group',Short_Dimension_Description__c = 'Group',Dimension_Type_Code__c = 'BRA'));
        dimensionList.add(new Dimension__c(Name ='GULF',Dimension_Description__c ='Gulf',Short_Dimension_Description__c = 'Gulf',Dimension_Type_Code__c = 'BRA'));
        dimensionList.add(new Dimension__c(Name ='HONGKONG',Dimension_Description__c ='Hong Kong',Short_Dimension_Description__c = 'HongKong',Dimension_Type_Code__c = 'BRA'));
        dimensionList.add(new Dimension__c(Name ='HURVITZG',Dimension_Description__c ='Grant Hurvitz',Short_Dimension_Description__c = 'HurvitzG',Dimension_Type_Code__c = 'BRA'));
        dimensionList.add(new Dimension__c(Name ='IBERIA',Dimension_Description__c ='Iberia',Short_Dimension_Description__c = 'Iberia',Dimension_Type_Code__c = 'BRA'));
        dimensionList.add(new Dimension__c(Name ='ICE',Dimension_Description__c ='In Country Experts',Short_Dimension_Description__c = 'ICE',Dimension_Type_Code__c = 'BRA'));
        dimensionList.add(new Dimension__c(Name ='INT',Dimension_Description__c ='International',Short_Dimension_Description__c = 'INT',Dimension_Type_Code__c = 'BRA'));
        dimensionList.add(new Dimension__c(Name ='INTSALES',Dimension_Description__c ='International Sales',Short_Dimension_Description__c = 'IntSales',Dimension_Type_Code__c = 'BRA'));
        dimensionList.add(new Dimension__c(Name ='IRELAND',Dimension_Description__c ='Ireland',Short_Dimension_Description__c = 'Ireland',Dimension_Type_Code__c = 'BRA'));
        dimensionList.add(new Dimension__c(Name ='ISTANBUL',Dimension_Description__c ='Istanbul',Short_Dimension_Description__c = 'Istanbul',Dimension_Type_Code__c = 'BRA'));
        dimensionList.add(new Dimension__c(Name ='JOHANNESBURG',Dimension_Description__c ='Johannesburg',Short_Dimension_Description__c = 'Johannesburg',Dimension_Type_Code__c = 'BRA'));
        dimensionList.add(new Dimension__c(Name ='STAX-INTGBP',Dimension_Description__c ='STAX ? International GBP',Short_Dimension_Description__c = 'STAX-IntGBP',Dimension_Type_Code__c = 'BRA'));
        dimensionList.add(new Dimension__c(Name ='STAX-INTSGD',Dimension_Description__c ='STAX ? International SGD',Short_Dimension_Description__c = 'STAX-IntSGD',Dimension_Type_Code__c = 'BRA'));
        dimensionList.add(new Dimension__c(Name ='STAX-INTUSD',Dimension_Description__c ='STAX ? International USD',Short_Dimension_Description__c = 'STAX-IntUSD',Dimension_Type_Code__c = 'BRA'));
        dimensionList.add(new Dimension__c(Name ='STAX-STOCK',Dimension_Description__c ='STAX ? Stockholm',Short_Dimension_Description__c = 'STAX-Stock',Dimension_Type_Code__c = 'BRA'));
        dimensionList.add(new Dimension__c(Name ='STOCKHOLM',Dimension_Description__c ='Stockholm',Short_Dimension_Description__c = 'Stockholm',Dimension_Type_Code__c = 'BRA'));
        dimensionList.add(new Dimension__c(Name ='SUSSMANN',Dimension_Description__c ='Noa Sussman',Short_Dimension_Description__c = 'SussmanN',Dimension_Type_Code__c = 'BRA'));
        dimensionList.add(new Dimension__c(Name ='SWITZERLAND',Dimension_Description__c ='Switzerland',Short_Dimension_Description__c = 'Switzerland',Dimension_Type_Code__c = 'BRA'));
        dimensionList.add(new Dimension__c(Name ='SYDNEY',Dimension_Description__c ='Sydney',Short_Dimension_Description__c = 'Sydney',Dimension_Type_Code__c = 'BRA'));
        dimensionList.add(new Dimension__c(Name ='TAIPEI',Dimension_Description__c ='Taipei',Short_Dimension_Description__c = 'Taipei',Dimension_Type_Code__c = 'BRA'));
        dimensionList.add(new Dimension__c(Name ='TECH',Dimension_Description__c ='Tech',Short_Dimension_Description__c = 'Tech',Dimension_Type_Code__c = 'BRA'));
        dimensionList.add(new Dimension__c(Name ='EOR',Dimension_Description__c ='Exporter of Record',Short_Dimension_Description__c = 'EOR',Dimension_Type_Code__c = 'PRO'));
        dimensionList.add(new Dimension__c(Name ='FININSURANCE',Dimension_Description__c ='Financing and Insurance',Short_Dimension_Description__c = 'FinInsurance',Dimension_Type_Code__c = 'PRO'));
        dimensionList.add(new Dimension__c(Name ='FOREXFEES',Dimension_Description__c ='Foreign Exchange Rate Fees',Short_Dimension_Description__c = 'ForexFees',Dimension_Type_Code__c = 'PRO'));
        dimensionList.add(new Dimension__c(Name ='INSURANCE',Dimension_Description__c ='Insurance Fees',Short_Dimension_Description__c = 'Insurance',Dimension_Type_Code__c = 'PRO'));
        dimensionList.add(new Dimension__c(Name ='IOR',Dimension_Description__c ='Importer of Record',Short_Dimension_Description__c = 'IOR',Dimension_Type_Code__c = 'PRO'));
        dimensionList.add(new Dimension__c(Name ='ONCHARGES',Dimension_Description__c ='On Charges',Short_Dimension_Description__c = 'OnCharges',Dimension_Type_Code__c = 'PRO'));
        dimensionList.add(new Dimension__c(Name ='SHIPPING',Dimension_Description__c ='Shipping & Insurance',Short_Dimension_Description__c = 'Shipping',Dimension_Type_Code__c = 'PRO'));
        dimensionList.add(new Dimension__c(Name ='JVBRAZIL',Dimension_Description__c ='JV Brazil',Short_Dimension_Description__c = 'JVBrazil',Dimension_Type_Code__c = 'BRA'));
        dimensionList.add(new Dimension__c(Name ='JVITALY',Dimension_Description__c ='JV Italy',Short_Dimension_Description__c = 'JVItaly',Dimension_Type_Code__c = 'BRA'));
        dimensionList.add(new Dimension__c(Name ='JVRUSSIA',Dimension_Description__c ='JV Russia',Short_Dimension_Description__c = 'JVRussia',Dimension_Type_Code__c = 'BRA'));
        dimensionList.add(new Dimension__c(Name ='JVSPAIN',Dimension_Description__c ='JV Spain',Short_Dimension_Description__c = 'JVSpain',Dimension_Type_Code__c = 'BRA'));
        dimensionList.add(new Dimension__c(Name ='KAIMIWITZJ',Dimension_Description__c ='Jonathan Kaimowitz',Short_Dimension_Description__c = 'KaimiwitzJ',Dimension_Type_Code__c = 'BRA'));
        dimensionList.add(new Dimension__c(Name ='KUALALUMPUR',Dimension_Description__c ='Kuala Lumpur',Short_Dimension_Description__c = 'KualaLumpur',Dimension_Type_Code__c = 'BRA'));
        dimensionList.add(new Dimension__c(Name ='LATORREA',Dimension_Description__c ='Andreas La Torre Ek',Short_Dimension_Description__c = 'LaTorreA',Dimension_Type_Code__c = 'BRA'));
        dimensionList.add(new Dimension__c(Name ='LISBON',Dimension_Description__c ='Lisbon',Short_Dimension_Description__c = 'Lisbon',Dimension_Type_Code__c = 'BRA'));
        dimensionList.add(new Dimension__c(Name ='LONDON',Dimension_Description__c ='London',Short_Dimension_Description__c = 'London',Dimension_Type_Code__c = 'BRA'));
        dimensionList.add(new Dimension__c(Name ='LUXEMBOURG',Dimension_Description__c ='Luxembourg',Short_Dimension_Description__c = 'Luxembourg',Dimension_Type_Code__c = 'BRA'));
        dimensionList.add(new Dimension__c(Name ='SALES',Dimension_Description__c ='Sales',Short_Dimension_Description__c = 'Sales',Dimension_Type_Code__c = 'BRA'));
        dimensionList.add(new Dimension__c(Name ='SEOUL',Dimension_Description__c ='Seoul',Short_Dimension_Description__c = 'Seoul',Dimension_Type_Code__c = 'BRA'));
        dimensionList.add(new Dimension__c(Name ='SERBIA',Dimension_Description__c ='Serbia',Short_Dimension_Description__c = 'Serbia',Dimension_Type_Code__c = 'BRA'));
        dimensionList.add(new Dimension__c(Name ='SERVICE',Dimension_Description__c ='Service',Short_Dimension_Description__c = 'Service',Dimension_Type_Code__c = 'BRA'));
        dimensionList.add(new Dimension__c(Name ='SHANGHAI',Dimension_Description__c ='Shanghai',Short_Dimension_Description__c = 'Shanghai',Dimension_Type_Code__c = 'BRA'));
        dimensionList.add(new Dimension__c(Name ='SHARED',Dimension_Description__c ='Shared Services',Short_Dimension_Description__c = 'Shared',Dimension_Type_Code__c = 'BRA'));
        dimensionList.add(new Dimension__c(Name ='SINGAPORE',Dimension_Description__c ='Singapore',Short_Dimension_Description__c = 'Singapore',Dimension_Type_Code__c = 'BRA'));
        dimensionList.add(new Dimension__c(Name ='SOOKLALM',Dimension_Description__c ='Mika Sooklal',Short_Dimension_Description__c = 'SooklalM',Dimension_Type_Code__c = 'BRA'));
        dimensionList.add(new Dimension__c(Name ='SOURISN',Dimension_Description__c ='Nicholas Souris',Short_Dimension_Description__c = 'SourisN',Dimension_Type_Code__c = 'BRA'));
        dimensionList.add(new Dimension__c(Name ='STAX-INTEUR',Dimension_Description__c ='STAX ? International EUR',Short_Dimension_Description__c = 'STAX-IntEUR',Dimension_Type_Code__c = 'BRA'));
        dimensionList.add(new Dimension__c(Name ='NEWYORK',Dimension_Description__c ='New York',Short_Dimension_Description__c = 'NewYork',Dimension_Type_Code__c = 'BRA'));
        dimensionList.add(new Dimension__c(Name ='NOUWENSO',Dimension_Description__c ='Oliver Nouwens',Short_Dimension_Description__c = 'NouwensO',Dimension_Type_Code__c = 'BRA'));
        dimensionList.add(new Dimension__c(Name ='OOSTHUYSENJ',Dimension_Description__c ='Jean Oosthuysen',Short_Dimension_Description__c = 'OosthuysenJ',Dimension_Type_Code__c = 'BRA'));
        dimensionList.add(new Dimension__c(Name ='PARIS',Dimension_Description__c ='Paris',Short_Dimension_Description__c = 'Paris',Dimension_Type_Code__c = 'BRA'));
        dimensionList.add(new Dimension__c(Name ='PEPPERM',Dimension_Description__c ='Michael Pepper',Short_Dimension_Description__c = 'PepperM',Dimension_Type_Code__c = 'BRA'));
        dimensionList.add(new Dimension__c(Name ='POORANC',Dimension_Description__c ='Cody Pooran',Short_Dimension_Description__c = 'PooranC',Dimension_Type_Code__c = 'BRA'));
        dimensionList.add(new Dimension__c(Name ='RASS',Dimension_Description__c ='Stefan Ras',Short_Dimension_Description__c = 'RasS',Dimension_Type_Code__c = 'BRA'));
        dimensionList.add(new Dimension__c(Name ='REGISTRATION',Dimension_Description__c ='Registrations',Short_Dimension_Description__c = 'Registration',Dimension_Type_Code__c = 'BRA'));
        dimensionList.add(new Dimension__c(Name ='RETOURTAX',Dimension_Description__c ='Retourtax',Short_Dimension_Description__c = 'Retourtax',Dimension_Type_Code__c = 'BRA'));
        dimensionList.add(new Dimension__c(Name ='RODRIGUESJ',Dimension_Description__c ='Juadit Rodrigues',Short_Dimension_Description__c = 'RodriguesJ',Dimension_Type_Code__c = 'BRA'));
        dimensionList.add(new Dimension__c(Name ='TELAVIV',Dimension_Description__c ='Tel Aviv',Short_Dimension_Description__c = 'TelAviv',Dimension_Type_Code__c = 'BRA'));
        dimensionList.add(new Dimension__c(Name ='TOKYO',Dimension_Description__c ='Tokyo',Short_Dimension_Description__c = 'Tokyo',Dimension_Type_Code__c = 'BRA'));
        dimensionList.add(new Dimension__c(Name ='TOPP',Dimension_Description__c ='TOPP Programme',Short_Dimension_Description__c = 'TOPP',Dimension_Type_Code__c = 'BRA'));
        dimensionList.add(new Dimension__c(Name ='TREASURY',Dimension_Description__c ='Treasury',Short_Dimension_Description__c = 'Treasury',Dimension_Type_Code__c = 'BRA'));
        dimensionList.add(new Dimension__c(Name ='UNALLOCATED',Dimension_Description__c ='Unallocated',Short_Dimension_Description__c = 'Unallocated',Dimension_Type_Code__c = 'BRA'));
        dimensionList.add(new Dimension__c(Name ='USA-ARI',Dimension_Description__c ='USA (Ari)',Short_Dimension_Description__c = 'USA-Ari',Dimension_Type_Code__c = 'BRA'));
        dimensionList.add(new Dimension__c(Name ='USA-JEREMY',Dimension_Description__c ='USA (Jeremy)',Short_Dimension_Description__c = 'USA-Jeremy',Dimension_Type_Code__c = 'BRA'));
        dimensionList.add(new Dimension__c(Name ='USA-ROBYN',Dimension_Description__c ='USA (Robyn)',Short_Dimension_Description__c = 'USA-Robyn',Dimension_Type_Code__c = 'BRA'));
        dimensionList.add(new Dimension__c(Name ='VANDERMERWEP',Dimension_Description__c ='Piet van der Merwe',Short_Dimension_Description__c = 'VanDerMerweP',Dimension_Type_Code__c = 'BRA'));
        dimensionList.add(new Dimension__c(Name ='VATRECLAIM',Dimension_Description__c ='VAT Reclaim',Short_Dimension_Description__c = 'VATReclaim',Dimension_Type_Code__c = 'BRA'));
        dimensionList.add(new Dimension__c(Name ='NADIAK',Dimension_Description__c ='Nadia Koen',Short_Dimension_Description__c = 'NK',Dimension_Type_Code__c = 'BRA'));
        dimensionList.add(new Dimension__c(Name ='CANADA',Dimension_Description__c ='Canada',Short_Dimension_Description__c = 'Canada',Dimension_Type_Code__c = 'BRA'));
        dimensionList.add(new Dimension__c(Name ='CAPETOWN',Dimension_Description__c ='Cape Town',Short_Dimension_Description__c = 'CapeTown',Dimension_Type_Code__c = 'BRA'));
        dimensionList.add(new Dimension__c(Name ='MELBOURNE',Dimension_Description__c ='Melbourne',Short_Dimension_Description__c = 'Melbourne',Dimension_Type_Code__c = 'BRA'));
        dimensionList.add(new Dimension__c(Name ='MILAN',Dimension_Description__c ='Milan',Short_Dimension_Description__c = 'Milan',Dimension_Type_Code__c = 'BRA'));
        dimensionList.add(new Dimension__c(Name ='MIRKING',Dimension_Description__c ='Gary Mirkin',Short_Dimension_Description__c = 'MirkinG',Dimension_Type_Code__c = 'BRA'));
        dimensionList.add(new Dimension__c(Name ='MOSCOW',Dimension_Description__c ='Moscow',Short_Dimension_Description__c = 'Moscow',Dimension_Type_Code__c = 'BRA'));
        dimensionList.add(new Dimension__c(Name ='MULONGOK',Dimension_Description__c ='Kenneth Mulongo',Short_Dimension_Description__c = 'MulongoK',Dimension_Type_Code__c = 'BRA'));
        dimensionList.add(new Dimension__c(Name ='MUMBAI',Dimension_Description__c ='Mumbai',Short_Dimension_Description__c = 'Mumbai',Dimension_Type_Code__c = 'BRA'));
        dimensionList.add(new Dimension__c(Name ='MUNICH',Dimension_Description__c ='Munich',Short_Dimension_Description__c = 'Munich',Dimension_Type_Code__c = 'BRA'));
        dimensionList.add(new Dimension__c(Name ='MUTSWARIW',Dimension_Description__c ='Walter Mutswari',Short_Dimension_Description__c = 'MutswariW',Dimension_Type_Code__c = 'BRA'));
        dimensionList.add(new Dimension__c(Name ='NAIDOOK',Dimension_Description__c ='Kimera Naidoo',Short_Dimension_Description__c = 'NaidooK',Dimension_Type_Code__c = 'BRA'));
        dimensionList.add(new Dimension__c(Name ='NAIDOOS',Dimension_Description__c ='Shakira Naidoo',Short_Dimension_Description__c = 'NaidooS',Dimension_Type_Code__c = 'BRA'));
        dimensionList.add(new Dimension__c(Name ='ACCMANAGE',Dimension_Description__c ='Account Management',Short_Dimension_Description__c = 'AccManage',Dimension_Type_Code__c = 'BRA'));
        dimensionList.add(new Dimension__c(Name ='ALBERTYNP',Dimension_Description__c ='Pietman Albertyn',Short_Dimension_Description__c = 'AlbertynP',Dimension_Type_Code__c = 'BRA'));
        dimensionList.add(new Dimension__c(Name ='AMSTERDAM',Dimension_Description__c ='Amsterdam',Short_Dimension_Description__c = 'Amsterdam',Dimension_Type_Code__c = 'BRA'));
        dimensionList.add(new Dimension__c(Name ='AWHT',Dimension_Description__c ='STAX - Africa Withholding Tax',Short_Dimension_Description__c = 'STAX - AWHT',Dimension_Type_Code__c = 'BRA'));
        dimensionList.add(new Dimension__c(Name ='BANGALORE',Dimension_Description__c ='Bangalore',Short_Dimension_Description__c = 'Bangalore',Dimension_Type_Code__c = 'BRA'));
        dimensionList.add(new Dimension__c(Name ='BARCELONA',Dimension_Description__c ='Barcelona',Short_Dimension_Description__c = 'Barcelona',Dimension_Type_Code__c = 'BRA'));
        dimensionList.add(new Dimension__c(Name ='BEIJING',Dimension_Description__c ='Beijing',Short_Dimension_Description__c = 'Beijing',Dimension_Type_Code__c = 'BRA'));
        dimensionList.add(new Dimension__c(Name ='BOOYSENJ',Dimension_Description__c ='Jacques Booysen',Short_Dimension_Description__c = 'BooysenJ',Dimension_Type_Code__c = 'BRA'));
        dimensionList.add(new Dimension__c(Name ='VIRGINIA',Dimension_Description__c ='Virginia',Short_Dimension_Description__c = 'Virginia',Dimension_Type_Code__c = 'BRA'));
        dimensionList.add(new Dimension__c(Name ='WHITEL',Dimension_Description__c ='Lauren White',Short_Dimension_Description__c = 'WhiteL',Dimension_Type_Code__c = 'BRA'));
        dimensionList.add(new Dimension__c(Name ='APAC',Dimension_Description__c ='APAC',Short_Dimension_Description__c = 'APAC',Dimension_Type_Code__c = 'OTH'));
        dimensionList.add(new Dimension__c(Name ='EMEA',Dimension_Description__c ='EMEA',Short_Dimension_Description__c = 'EMEA',Dimension_Type_Code__c = 'OTH'));
        dimensionList.add(new Dimension__c(Name ='LATAM',Dimension_Description__c ='LATAM',Short_Dimension_Description__c = 'LATAM',Dimension_Type_Code__c = 'OTH'));
        dimensionList.add(new Dimension__c(Name ='TAX',Dimension_Description__c ='Tax',Short_Dimension_Description__c = 'Tax',Dimension_Type_Code__c = 'OTH'));
        dimensionList.add(new Dimension__c(Name ='USA',Dimension_Description__c ='United States',Short_Dimension_Description__c = 'USA',Dimension_Type_Code__c = 'OTH'));
        dimensionList.add(new Dimension__c(Name ='VATRECLAIM',Dimension_Description__c ='VAT Reclaim',Short_Dimension_Description__c = 'VATReclaim',Dimension_Type_Code__c = 'OTH'));
        dimensionList.add(new Dimension__c(Name ='ADHOC',Dimension_Description__c ='Ad Hoc Fees',Short_Dimension_Description__c = 'AdHoc',Dimension_Type_Code__c = 'PRO'));
        dimensionList.add(new Dimension__c(Name ='BANKCHARGE',Dimension_Description__c ='Bank Charges Recovered',Short_Dimension_Description__c = 'BankCharge',Dimension_Type_Code__c = 'PRO'));
        dimensionList.add(new Dimension__c(Name ='Penalty',Dimension_Description__c ='Penalty',Short_Dimension_Description__c = 'Penalty',Dimension_Type_Code__c = 'OTH'));
        insert dimensionList;
        
        Account account = new Account (name='Acme1',  Type ='Client',  CSE_IOR__c = '0050Y000001LTZO',  Service_Manager__c = '0050Y000001LTZO',  Tax_recovery_client__c  = FALSE, Ship_From_Line_1__c = 'Ship_From_Line_1',
                                       Ship_From_Line_2__c = 'Ship_From_Line_2', Ship_From_Line_3__c = 'Ship_From_Line_3', Ship_From_City__c = 'Ship_From_City',  Ship_From_Zip__c = 'Ship_From_Zip',
                                       Ship_From_Country__c = 'Ship_From_Country' ,  Ship_From_Other_notes__c = 'Ship_From_Other', Ship_From_Contact_Name__c = 'Ship_From_Contact', Ship_From_Contact_Email__c = 'Ship_From_Email',
                                       Ship_From_Contact_Tel__c = 'Ship_From_Tel', Client_Address_Line_1__c = 'Client_Address_Line_1', Client_Address_Line_2__c = 'Client_Address_Line_2', Client_Address_Line_3__c = 'Client_Address_Line_3',
                                       Client_City__c = 'Client_City', Client_Zip__c = 'Client_Zip', Client_Country__c = 'Client_Country',  Other_notes__c = 'Other_notes',  Tax_Name__c = 'Tax_Name', Tax_ID__c = 'Tax_ID',
                                       Contact_Name__c = 'Contact_Name', Contact_Email__c = 'Contact_Email', Contact_Tel__c = 'Contact_Tel', Default_invoicing_currency__c = 'US Dollar (USD)', IOR_Payment_Terms__c = 7,
                                       Invoice_Timing__c = 'Upfront invoicing', Invoicing_Term_Parameters__c = 'No Terms', Dimension__c = dimensionList[0].ID, Region__c = 'usa', Invoice_Email_Fields__c = 'Invoice Number;Total Amount Due (USD);Due Date;Cash Disbursement Fee;Total Amount Due if paid after Due_Date__c;Final Delivery Address' ); insert account;  
        
        
        Account account2 = new Account (name='TecEx Prospective Client', Type ='Supplier', ICE__c = '0050Y000001km5c', RecordTypeId = '0120Y0000006KjQQAU' ); insert account2;
        
        Account account3 = new Account (name='Test Manufacturer', Type ='Manufacturer', RecordTypeId = '0120Y0000002wa0QAA'); insert account3;
        
        Account account4 = new Account (name='ACME', Type ='Manufacturer', RecordTypeId = '0120Y0000002wa0QAA'); insert account4;
        
        Contact contact = new Contact ( lastname ='Testing Individual',  AccountId = account.Id , Email='Test@Test.com', Include_in_invoicing_emails__c  = true); insert contact;  
        
        IOR_Price_List__c iorpl = new IOR_Price_List__c ( Client_Name__c = account.Id, Name = 'Brazil', IOR_Fee__c = 0.15, On_Charge_Mark_up__c = 0.15,
                                                         TecEx_Shipping_Fee_Markup__c =0.40, IOR_Min_Fee__c = 100,  Admin_Fee__c =100, Set_up_fee__c =100, Bank_Fees__c =200, Tax_Rate__c = 0.20,
                                                         Estimated_Customs_Brokerage__c =250, Estimated_Customs_Clearance__c =250, Estimated_Customs_Handling__c =250, Estimated_Import_License__c =250, 
                                                         Destination__c = 'Brazil' ); insert iorpl; 
        
        /*country_price_approval__c cpa = new country_price_approval__c( name ='Brazil', Billing_term__c = 'DAP/CIF - IOR pays',  Airwaybill_Instructions__c = 'User IOR Address', Supplier__c = account2.Id, 
                                                                      In_Country_Specialist__c = '0050Y000001km5c', Clearance_Destination__c = 'Brazil', Preferred_Supplier__c = TRUE, 
                                                                      Destination__c = 'Brazil' ); insert cpa;*/
        
        Country__c country = new Country__c(Brokerage_Costs__c = 618, CIF_Adjustment_Factor__c = 0.035, CIF_Absolute_value_adjustment__c = 200, Clearance_Costs__c = 488, 
                                            Country__c = 'Brazil', Name = 'Brazil', Handling_Costs__c = 1180, License_Permit_Costs__c = 0); insert country;
        
        Currency_Management2__c conversion = new Currency_Management2__c(Name = 'US Dollar (USD)', Conversion_Rate__c = 1, Currency__c = 'US Dollar (USD)', ISO_Code__c = 'EUR'); insert conversion ;
        
        List<Tax_Structure_Per_Country__c> taxStructure = new List<Tax_Structure_Per_Country__c>();
        taxStructure.add(new Tax_Structure_Per_Country__c(Country__c = country.Id, Applied_to_Value__c = 'CIF', Name = 'I', Order_Number__c = '1', Part_Specific__c = TRUE, Tax_Type__c = 'Duties', Default_Duty_Rate__c = 0.5625)); 
        taxStructure.add(new Tax_Structure_Per_Country__c(Country__c = country.Id, Applied_to_Value__c = 'CIF', Name = 'II', Order_Number__c = '2', Part_Specific__c = FALSE, Tax_Type__c = 'Other', Rate__c = 0.40, Additional_Percent__c = 0.01 ));
        taxStructure.add(new Tax_Structure_Per_Country__c(Country__c = country.Id, Applied_to_Value__c = 'CIF', Name = 'III', Order_Number__c = '3', Part_Specific__c = FALSE, Tax_Type__c = 'VAT', Rate__c = 0.40, Applies_to_Order__c = '1'));
        taxStructure.add(new Tax_Structure_Per_Country__c(Country__c = country.Id, Applied_to_Value__c = 'CIF', Name = 'IV', Order_Number__c = '4', Part_Specific__c = FALSE, Tax_Type__c = 'Other', Amount__c = 400));     
        taxStructure.add(new Tax_Structure_Per_Country__c(Country__c = country.Id, Applied_to_Value__c = 'CIF', Name = 'V', Order_Number__c = '5', Part_Specific__c = FALSE, Tax_Type__c = 'Other', Rate__c = 0.40, Applies_to_Order__c = '1,2,3'));
        taxStructure.add(new Tax_Structure_Per_Country__c(Country__c = country.Id, Applied_to_Value__c = 'CIF', Name = 'VI', Order_Number__c = '6', Part_Specific__c = FALSE, Tax_Type__c = 'Other', Rate__c = 0.40, Applies_to_Order__c = '1,2,3,4', Additional_Percent__c = 0.01));
        taxStructure.add(new Tax_Structure_Per_Country__c(Country__c = country.Id, Applied_to_Value__c = 'CIF', Name = 'VII', Order_Number__c = '7', Part_Specific__c = FALSE, Tax_Type__c = 'Other', Rate__c = 0.40, Max__c = 1000, Min__c = 50));       
        taxStructure.add(new Tax_Structure_Per_Country__c(Country__c = country.Id, Applied_to_Value__c = 'CIF', Name = 'VIII', Order_Number__c = '8', Part_Specific__c = FALSE, Tax_Type__c = 'Other', Rate__c = 0.40, Applies_to_Order__c = '1,2,3,4,5,7'));
        taxStructure.add(new Tax_Structure_Per_Country__c(Country__c = country.Id, Applied_to_Value__c = 'CIF', Name = 'IX', Order_Number__c = '9', Part_Specific__c = FALSE, Tax_Type__c = 'Other', Rate__c = 0.40, Applies_to_Order__c = '1,2,3,4,6'));
        taxStructure.add(new Tax_Structure_Per_Country__c(Country__c = country.Id, Applied_to_Value__c = 'CIF', Name = 'X', Order_Number__c = '10', Part_Specific__c = FALSE, Tax_Type__c = 'Other', Rate__c = 0.40)); 
        insert taxStructure;
        
        
        CPA_v2_0__c aCpav = new CPA_v2_0__c(Name = 'Brazil', Country_Applied_Costings__c = TRUE,CPA_Minimum_Brokerage_Surcharge__c=10); insert aCpav;
        
        CPA_v2_0__c cpav2 = new CPA_v2_0__c(Name = 'Brazil Testing 2.0',CPA_Minimum_Brokerage_Surcharge__c=10, Supplier__c = account2.Id, Service_Type__c = 'IOR/EOR', Related_Costing_CPA__c = aCpav.Id, 
                                            Preferred_Supplier__c = TRUE, VAT_Rate__c = 0.1, Destination__c = 'Brazil',  Final_Destination__c = 'Brazil', 
                                            VAT_Reclaim_Destination__c = FALSE, Country__c = country.id, Lead_ICE__c = '0050Y000001km5c'); insert cpav2;
        
        
        List<CPA_Costing__c> costs = new List<CPA_Costing__c>();
        
        
        costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'Shipment value', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'IOR', Min__c = 375, Max__c = 14000, Conditional_value__c = 'Shipment value', Condition__c = 'Between', Floor__c = 0, Ceiling__c = 10000, Currency__c = 'US Dollar (USD)'));
        costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'Anil', Cost_Type__c = 'Variable',   Rate__c = 10, IOR_EOR__c = 'IOR', Currency__c = 'US Dollar (USD)',Variable_threshold__c = null));
        
        
        insert costs;
        
        
        
        Shipment_Order__c shipmentOrder = new Shipment_Order__c(Account__c = account.id, Shipment_Value_USD__c = 2000, CPA_v2_0__c = cpav2.Id,
                                                                Client_Contact_for_this_Shipment__c = contact.Id, Who_arranges_International_courier__c ='Tecex', Tax_Treatment__c ='DAP/CIF - IOR pays', 
                                                                Destination__c = 'Brazil', Service_Type__c = 'IOR', IOR_Price_List__c = iorpl.Id, Chargeable_Weight__c = 200,  
                                                                Shipping_Status__c = 'Cost Estimate', Miscellaneous_Fee__c = 400,  Miscellaneous_Fee_Name__c = 'Ad Hoc Charge', POD_Date__c =date.today() );  
        test.startTest();
        insert shipmentOrder; 
        test.stopTest();
        
        
        
    }
    
    @IsTest
    public static void saveInvoiceUsingTriggerQueueable_Test(){
        
        Shipment_Order__c shipmentOrder = [select Id from Shipment_Order__c limit 1];
        shipmentOrder.Populate_Invoice__c = TRUE; 
        ShipmentOrder.Minimum_Brokerage_Costs__c  =10;
         ShipmentOrder.Freight_Fee_Calculated_Freight_Request__c =10;
         ShipmentOrder.International_Delivery_Cost__c =5;
         ShipmentOrder.Finance_Fee__c =10;
         ShipmentOrder.Collection_Administration_Fee__c =10;
                
        Test.startTest();
        	update shipmentOrder;
        	Invoice_New__c inv = [select Id from Invoice_New__c where Shipment_Order__c =: shipmentOrder.Id];
       		System.enqueueJob(new QueueableGenerateZeeInvoice(new Set<Id>{inv.Id}));
        Test.stopTest();
        
    }

}