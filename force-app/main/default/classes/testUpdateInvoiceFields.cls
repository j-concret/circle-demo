@isTest
public class testUpdateInvoiceFields {

   private static testMethod void  setUpData(){
   
   test.startTest();
   
        Account account = new Account (name='Acme1', Type ='Client', CSE_IOR__c = '0050Y000001LTZO');
        insert account; 
        
        Account account2 = new Account (name='AcmeSupplier', Type ='Supplier', CSE_IOR__c = '0050Y000001LTZO');
        insert account2;     
          
        country_price_approval__c cpa = new country_price_approval__c( name ='Aruba', Billing_term__c = 'DAP/CIF - IOR pays',  Airwaybill_Instructions__c = 'User IOR Address');
        insert cpa;
        
        IOR_Price_List__c iorpl = new IOR_Price_List__c ( Client_Name__c = account.Id, Name = 'Aruba', IOR_Fee__c = 0.15, On_Charge_Mark_up__c = 0.15,
        TecEx_Shipping_Fee_Markup__c =0.40, IOR_Min_Fee__c = 100,  Admin_Fee__c =100, Set_up_fee__c =100, Bank_Fees__c =200, Tax_Rate__c = 0.20,
        Estimated_Customs_Brokerage__c =250, Estimated_Customs_Clearance__c =250, Estimated_Customs_Handling__c =250, Estimated_Import_License__c =250);
        insert iorpl; 
     
        Contact contact = new Contact ( lastname ='Testing Individual',  AccountId = account.Id);
        insert contact; 
        
        Opportunity opportunity = new Opportunity(AccountId = account.id, Name ='120-547', Ship_to_Country__c = cpa.Id, IOR_Price_List__c = iorpl.Id, Shipment_Value_USD__c =1000,
        Ship_From_Country__c = 'Algeria', CloseDate= system.today(), StageName ='Shipment Pending', Client_Contact_for_this_Shipment__c = contact.Id, Who_arranges_International_courier__c ='Client', Tax_Treatment__c ='DAP/CIF - IOR pays' );
        insert opportunity;
     
        Shipment_Order__c shipmentOrder = new Shipment_Order__c(Account__c = account.id, Ship_to_Country__c = cpa.Id, IOR_Price_List__c = iorpl.Id, Shipment_Value_USD__c =1000, Opportunity__c = opportunity.Id,
        Client_Contact_for_this_Shipment__c = contact.Id, Who_arranges_International_courier__c ='Client', Tax_Treatment__c ='DAP/CIF - IOR pays', Shipping_Status__c ='POD Received' );
        insert shipmentOrder;
      
        Customer_Invoice__c customerInvoice = new Customer_Invoice__c(Client__c = account.id, Shipment_Order__c = shipmentOrder.Id, Invoice_Amount__c = 2000, Billable__c = True);
        insert customerInvoice; 
        
        Supplier_Invoice__c supplierInvoice = new Supplier_Invoice__c(Supplier_Name__c= account2.id, Name= 'Testing Invoice', Due_date__c = system.today());
        insert supplierInvoice ; 
        
        Supplier_Invoice_per_Shipment_Order__c supplierinvoiceso = new Supplier_Invoice_per_Shipment_Order__c(Supplier_Invoice_No__c= supplierInvoice.id, Shipment_Order__c = shipmentOrder.Id, Incurred__c= FALSE);
        insert supplierinvoiceso ; 
         
        
             
        test.stopTest();

     
       
    } 

   }