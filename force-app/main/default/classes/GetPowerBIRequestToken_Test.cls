@isTest
public class GetPowerBIRequestToken_Test  implements HttpCalloutMock {
    
    private static HttpResponse resp;
    
    public GetPowerBIRequestToken_Test(String testBody,Boolean isTest) {
        
        resp = new HttpResponse();
        
        resp.setBody(testBody);
        
        if(isTest == true){
            
            resp.setStatus('OK');
            resp.setStatusCode(200);
            
        }else{
            
            resp.setStatus('Error');
            resp.setStatusCode(404);
            
        }
    }
    
    public HTTPResponse respond(HTTPRequest req) {
        
        return resp;
        
    }
    
    static testMethod void  GetBITokenTest(){
        
        
        Power_Bi_Requests__c pbr = new Power_Bi_Requests__c(
            Name = 'PowerBi',
            client_id__c='03e8300e-2059-4580-b1a6-94bee02e8fb5',
            resource__c = 'https://analysis.windows.net/powerbi/api',
            client_secret__c='jKuyGTWB8G9-Q2v4vE1r4-.4e0__-wdo1C',
            grant_type__c='client_credentials',
            datasetId__c='c35a07b8-c419-4782-b2f2-aaf1baaeeeb9',
            reportId__c='107da9e7-57d9-49e6-9f5e-b7b578f2d589',
            WorkSpace_ID__c='10493185-e77b-4d4a-927a-2646fcf11328'
        );
        insert pbr;
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/GetBIToken'; //Request URL
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response= res;
        
        String jsonData = '{"token_type":"Bearer","expires_in":"3599","ext_expires_in":"3599","expires_on":"1602589548","not_before":"1602585648","resource":"https://analysis.windows.net/powerbi/api","access_token":"eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsIng1dCI6ImtnMkxZczJUMENUaklmajRydDZKSXluZW4zOCIsImtpZCI6ImtnMkxZczJUMENUaklmajRydDZKSXluZW4zOCJ9.eyJhdWQiOiJodHRwczovL2FuYWx5c2lzLndpbmRvd3MubmV0L3Bvd2VyYmkvYXBpIiwiaXNzIjoiaHR0cHM6Ly9zdHMud2luZG93cy5uZXQvNDE2N2RkMTUtNGQxNC00MjY0LTg1ZTQtZTExM2YyZjc2ZTlmLyIsImlhdCI6MTYwMjU4NTY0OCwibmJmIjoxNjAyNTg1NjQ4LCJleHAiOjE2MDI1ODk1NDgsImFpbyI6IkUyUmdZR0F2RGR2dmZVemEva3ZKS2w1L3NhOHlBQT09IiwiYXBwaWQiOiIwM2U4MzAwZS0yMDU5LTQ1ODAtYjFhNi05NGJlZTAyZThmYjUiLCJhcHBpZGFjciI6IjEiLCJpZHAiOiJodHRwczovL3N0cy53aW5kb3dzLm5ldC80MTY3ZGQxNS00ZDE0LTQyNjQtODVlNC1lMTEzZjJmNzZlOWYvIiwib2lkIjoiODFiMDQwNjEtMDFmZi00NTFlLThhNTctMTY4NDg3N2QzMDEwIiwicmgiOiIwLkFBQUFGZDFuUVJSTlpFS0Y1T0VUOHZkdW53NHc2QU5aSUlCRnNhYVV2dUF1ajdVUkFBQS4iLCJzdWIiOiI4MWIwNDA2MS0wMWZmLTQ1MWUtOGE1Ny0xNjg0ODc3ZDMwMTAiLCJ0aWQiOiI0MTY3ZGQxNS00ZDE0LTQyNjQtODVlNC1lMTEzZjJmNzZlOWYiLCJ1dGkiOiJCczA4c0FNbGFVZXpjZFpXMEU4YkFBIiwidmVyIjoiMS4wIn0.YfDzHb9h68AaKN8-2SwETtqJ82cjOuaVCl9Z4PAyQlLckjqcIlD5dK5HBK-qNN62cpQzHguJxVyg-y3lxpgEnB98e9CemipI1VmY5jdg0tnVU92jiaNOEt5c9_g4h_1VDS0g860gHxJD_-oJUr5qKGgZXV8eTtm0J9-lZ4R368SZ6JIWI2OLy1pY3Iwuz5yKNaxXam_mA0NVgojvOLbDn2UXhBQ4oeBPNHhn-3_XdIiO232g3V95aixQJzW9KouzCHRSk5LiLU9UfsgC8DwZ7QbmP-FZ10maWCNhV3YTrod6Wpv8jIgIhazlhvVnfHZjeKqSZ-j3OQPkoGeT9g7mBg"}';
        Test.startTest();
        HttpCalloutMock mock = new GetPowerBIRequestToken_Test(jsonData,true);
        Test.setMock(HttpCalloutMock.class, mock);
        String Token = getPowerBIRequestToken.gettoken();
        getPowerBIRequestToken.getEmbedURL(Token);
        Test.stopTest();
    }
    
}