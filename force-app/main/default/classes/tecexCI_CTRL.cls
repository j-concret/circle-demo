public class tecexCI_CTRL {
    public Shipment_Order__c shipmentOrder { get; set; }

    public Freight__c freightRequest { get; set; }

    public Decimal numberOfPackages {get; set;}

    public Decimal packagesWeight {get; set;}

    public List<Part__c> lineItems { get; set; }

    public List<String> additionalHeaders { get; set; }
    public Map<Id,Map<String,String>> columsnWithData { get; set; }

    public Decimal CIF {get; set;}
    public Decimal shipmentValue {get; set;}
    public Double currencyConvertor {get; set;}
    public String portValue = '';
    public String vatValue = '';
    public String appName {get; set;}

    public tecexCI_CTRL(ApexPages.StandardController controller){
        this.numberOfPackages = 0;
        this.packagesWeight = 0;
        this.appName = 'TecEx';
        this.currencyConvertor = 1;
        if(ApexPages.currentPage().getParameters().get('vatValue') != NULL && ApexPages.currentPage().getParameters().get('portValue') != NULL){
            this.portValue = ApexPages.currentPage().getParameters().get('portValue');
            this.vatValue = ApexPages.currentPage().getParameters().get('vatValue');
        }

        List<Id> shipmentRecordIds = new List<Id>{Schema.Sobjecttype.Shipment_Order__c.getRecordTypeInfosByDeveloperName().get('Zee_Cost_Estimate').getRecordTypeId(),
            Schema.Sobjecttype.Shipment_Order__c.getRecordTypeInfosByDeveloperName().get('Zee_Shipment_Order').getRecordTypeId()};
       	this.additionalHeaders = new List<String>();
        this.columsnWithData = new Map<Id,Map<String,String>>();

        this.shipmentOrder = [SELECT Id,RecordTypeId,SupplierlU__c,CPA_v2_0__r.Name,CPA_v2_0__r.CI_Name__c,Contact_name__c,CI_Input_Liability_Cover__c,CI_Input_Freight__c,Account__r.Name,Account__r.CI_Statement__c,Shipment_Value_USD__c, Account__r.Client_Address_Line_1__c, Who_arranges_International_courier__c, Account__c,CPA_v2_0__r.Valuation_Method__c, CPA_v2_0__r.CIF_Freight_and_Insurance__c, CPA_v2_0__r.CI_Currency__c,CPA_v2_0__r.Section_2_Header__c,  CPA_v2_0__c, Insurance_Fee_USD__c, Actual_Weight_KGs__c,  Destination__c, Service_Type__c, RecordType.Name,Account__r.Registered_Name__c, CPA_v2_0__r.Exporter_Header__c,  CPA_v2_0__r.Ship_From_Header__c,  Ship_From_Country__c FROM Shipment_Order__c WHERE Id = :ApexPages.currentPage().getParameters().get('id') LIMIT 1];

        List<Freight__c> freightRequests = [SELECT Id,International_freight_fee_invoiced__c,Pick_Up_Contact_Address1__c,Pick_Up_Contact_Address2__c,Pick_Up_Contact_Address3__c,Pick_Up_Contact_City__c,Pick_Up_Contact_Country__c,Pick_Up_Contact_ZIP__c,Pick_Up_Contact_Comments__c,Pick_up_Contact_Details__c,Pick_Up_Contact_Email__c,Pick_up_Contact_No__c,
                                                      SF_Company__c,SF_AddressLine1__c ,SF_AddressLine2__c ,SF_City__c ,SF_Country__c ,SF_Postal_Code__c ,SF_Name__c , SF_Email__c ,SF_Phone__c FROM Freight__c WHERE Shipment_Order__c =:shipmentOrder.Id LIMIT 1];

        if(!freightRequests.isEmpty()) {
          this.freightRequest = freightRequests[0];
          this.CIF = freightRequest.International_freight_fee_invoiced__c + shipmentOrder.Insurance_Fee_USD__c;
        }

        if(CIF != null && shipmentOrder.CPA_v2_0__r.CIF_Freight_and_Insurance__c != null && shipmentOrder.Who_arranges_International_courier__c == 'TecEx') {
            this.shipmentValue = shipmentOrder.Shipment_Value_USD__c + CIF;
        } else{
            this.shipmentValue = shipmentOrder.Shipment_Value_USD__c;
        }

        this.lineItems = [SELECT Id,  Name,  Quantity__c, Net_Weight_per_Line_Item__c, Retail_Method_Unit_Price__c, Description_and_Functionality__c,  COO_2_digit__c,  ECCN_NO__c,  US_HTS_Code__c,  Commercial_Value__c,  Total_Value__c,  Shipment_Order__c,additional_CI_Columns__c FROM Part__c WHERE Shipment_Order__c = :shipmentOrder.Id];

        Boolean headerFilled = false;

        if(shipmentRecordIds.contains(shipmentOrder.RecordTypeId) && shipmentOrder.CPA_v2_0__c !=null){

            appName = 'Zee';
            Currency_Management2__c cmList = new Currency_Management2__c();
            String currencyUnit;
            if(shipmentOrder.CPA_v2_0__r.CI_Currency__c != null){
                currencyUnit = shipmentOrder.CPA_v2_0__r.CI_Currency__c;
                currencyUnit = currencyUnit.subString(currencyUnit.indexOf('(') + 1,currencyUnit.indexOf(')'));
                cmList = [SELECT id,Conversion_Rate__c,ISO_Code__c FROM Currency_Management2__c WHERE ISO_Code__c =:currencyUnit];
            }
            if(shipmentOrder.CPA_v2_0__r.Valuation_Method__c == 'Cost Method'){

                if(cmList.Conversion_Rate__c != null && currencyUnit != 'USD'){

                    for(Part__c prt : lineItems){
                        prt.Commercial_Value__c = (prt.Commercial_Value__c / cmList.Conversion_Rate__c).setScale(2);
                    }

                    Formula.recalculateFormulas(lineItems);
                    currencyConvertor = cmList.Conversion_Rate__c;

                }
            }
            else if(shipmentOrder.CPA_v2_0__r.Valuation_Method__c == 'Retail Method'){

                for(Part__c prt : lineItems){
                    prt.Commercial_Value__c = (prt.Retail_Method_Unit_Price__c).setScale(2);
                }

                Formula.recalculateFormulas(lineItems);
                double total = 0.0;
                for(Part__c prt : lineItems){
                    total += prt.Total_Value__c;
                }
                this.shipmentOrder.Shipment_Value_USD__c = total;
                if(CIF != null && shipmentOrder.CPA_v2_0__r.CIF_Freight_and_Insurance__c != null && shipmentOrder.Who_arranges_International_courier__c == 'TecEx') {
                    this.shipmentValue = total + CIF;
                } else{
                    this.shipmentValue = total;
                }
            }

        }
        for(Part__c lineItem : lineItems){
            if(lineItem.additional_CI_Columns__c != NULL && lineItem.additional_CI_Columns__c != ''){
                Map<String,String> data = (Map<String,String>) JSON.deserialize(lineItem.additional_CI_Columns__c, Map<String,String>.class);
                columsnWithData.put(lineItem.Id, data);
                if(!headerFilled){
                    additionalHeaders.addAll(data.keySet());
                    headerFilled = true;
                }
            }
        }
    }


    public List<Client_Address__c> getClientAddresses(){
        //String portValue = 'Sydney';
        //String vatValue = 'VAT';

        List<Client_Address__c> cAddresses = new List<Client_Address__c>();

        List<Registrations__c> regt = [SELECT Id, Name, Company_name__c, Country__c, Finance_contact_Email__c,
                    Finance_contact_Name__c, Finance_contact_Phone__c, Registered_Address_2__c,
                    Registered_Address_City__c, Registered_Address_Country__c,
                    Registered_Address_Postal_Code__c, Registered_Address_Province__c,
                    Registered_Address__c, Type_of_registration__c, VAT_number__c, Verified__c
                    FROM Registrations__c
                   Where Company_name__c = :shipmentOrder.Account__c AND Country__c = : shipmentOrder.Destination__c];

        for(Client_Address__c c_add : [SELECT Id,Naming_Conventions__c, Address__c, Name, City__c, All_Countries__c, Postal_Code__c, Province__c, Address2__c, CompanyName__c, Contact_Full_Name__c, Contact_Phone_Number__c, Contact_Email__c, AdditionalContactNumber__c, Tax_Name_Number__c,VAT_NON_VAT_address__c,Address_Type__c,Port_of_Entry__c,Section_Headers__c FROM Client_Address__c WHERE recordTypeId = :Schema.SObjectType.Client_Address__c.getRecordTypeInfosByName().get('Consignee Address').getRecordTypeId() AND Document_Type__c='CI' AND Client__c=:shipmentOrder.SupplierlU__c AND CPA_v2_0__c =: shipmentOrder.CPA_v2_0__c AND VAT_NON_VAT_address__c=:vatValue AND Address_Type__c != null order by Address_Type__c]) {
            if(c_add.Port_of_Entry__c != portValue && c_add.Address_Type__c == 'Section 1') continue;
            cAddresses.add(c_add);
            system.debug('Data -> ' + JSON.serialize(c_add));
        }
            List<Final_Delivery__c> finalDeliveries = [SELECT Id, Name, Address_Line_1__c, Address_Line_2__c, Address_Line_3__c, City__c, Contact_email__c,
                                                       Contact_name__c, Contact_number__c, Country__c, Delivery_address__c, Delivery_date__c,
                                                       Delivery_Status__c, Other__c,Shipment_Order__c, Tax_Id__c, Tax_Name__c, Zip__c, Company_Name__c,
                                                       Ship_to_address__r.CompanyName__c, Ship_to_address__r.Address__c,Ship_to_address__r.Address2__c,
                                                       Ship_to_address__r.City__c, Ship_to_address__r.Postal_Code__c,  Ship_to_address__r.All_Countries__c,
                                                       Ship_to_address__r.Contact_Full_Name__c,  Ship_to_address__r.Contact_Email__c,
                                                       Ship_to_address__r.Contact_Phone_Number__c,  Ship_to_address__r.Tax_Name_Number__c
                                                       FROM Final_Delivery__c
                                                       WHERE Shipment_Order__c =:shipmentOrder.Id LIMIT 1];
        //This method will transform values of client address records as per the naming convention logic.
        namingConventionTransformation(cAddresses, finalDeliveries, this.shipmentOrder, regt);

        return cAddresses;
    }

      // this method will take associated client addresses and transform as per the naming convention logic.
    public void namingConventionTransformation( List<Client_Address__c> cAddresses,List<Final_Delivery__c> finalDeliveries, Shipment_Order__c shipmentOrder, List<Registrations__c> regt){


        for(Client_Address__c cAddress : cAddresses) {
            if(String.isNotBlank(cAddress.Naming_Conventions__c)) {
                switch on cAddress.Naming_Conventions__c {

                    when 'Use Final Delivery Details' {
                        if(!finalDeliveries.isEmpty()) {
                            cAddress.CompanyName__c = finalDeliveries[0].Ship_to_address__r.CompanyName__c;
                            copyAddressFromFinal(cAddress,finalDeliveries[0]);
                        }
                    }
                    when 'CPA c/o Final Delivery Details' {
                        cAddress.CompanyName__c += ' c/o ';
                        if(!finalDeliveries.isEmpty()) {
                            cAddress.CompanyName__c += finalDeliveries[0].Ship_to_address__r.CompanyName__c;
                            copyAddressFromFinal(cAddress,finalDeliveries[0]);
                        }
                    }

                    when 'Client c/o CPA Details as per Below' {
                        cAddress.CompanyName__c = shipmentOrder.Account__r.Name + ' c/o';
                    }

                    /*when 'Client c/o CPA c/o Final Delivery Details' {
                        cAddress.CompanyName__c = shipmentOrder.Account__r.Name + ' c/o ' + cAddress.CompanyName__c + ' c/o ';
                        if(!finalDeliveries.isEmpty()){
                           cAddress.CompanyName__c += finalDeliveries[0].Company_Name__c;
                        }
                    }*/

					 when 'Client c/o CPA c/o with Final Delivery Details' {
                        cAddress.CompanyName__c = shipmentOrder.Account__r.Name + ' c/o ' + cAddress.CompanyName__c + ' c/o ';
                        if(!finalDeliveries.isEmpty()){
                           cAddress.CompanyName__c += finalDeliveries[0].Ship_to_address__r.CompanyName__c;
                            copyAddressFromFinal(cAddress,finalDeliveries[0]);
                        }
                    }

                    when 'Beneficial Owner c/o Final Delivery Details' {
                        cAddress.CompanyName__c = shipmentOrder.Contact_name__c + ' c/o ';
                        if(!finalDeliveries.isEmpty()) {
                            cAddress.CompanyName__c += finalDeliveries[0].Ship_to_address__r.CompanyName__c;
                            copyAddressFromFinal(cAddress,finalDeliveries[0]);
                        }
                    }

                    when 'Beneficial Owner c/o Use CPA Details as per Below' {
                        //cAddress.CompanyName__c = shipmentOrder.Contact_name__c + ' c/o';  previous values
                        cAddress.CompanyName__c = shipmentOrder.Contact_name__c + ' c/o '+cAddress.CompanyName__c;
                    }

					// Story Id : 4246
                    when 'VAT Claiming Entity c/o with CPA details below' {
                        //cAddress.CompanyName__c = 'VAT Claiming Entity c/o';
                        cAddress.CompanyName__c = 'VAT Claiming Entity c/o ' + cAddress.CompanyName__c;
                        //if(!finalDeliveries.isEmpty()){
                        //    cAddress.CompanyName__c += finalDeliveries[0].Ship_to_address__r.CompanyName__c;
                        //}

                    }


                    when 'VAT Claiming Entity (Tax#) c/o with Final Delivery Details' {
                        cAddress.CompanyName__c = 'VAT Claiming Entity c/o ';
                        if(!finalDeliveries.isEmpty()) {
                             cAddress.CompanyName__c += finalDeliveries[0].Ship_to_address__r.CompanyName__c;
                            copyAddressFromFinal(cAddress,finalDeliveries[0]);
                        }
                    }

                    when 'VAT Claiming Entity (Tax#) c/o CPA c/o with Final Delivery Details' {
                        cAddress.CompanyName__c = 'VAT Claiming Entity c/o ' + cAddress.CompanyName__c + ' c/o ';
                        if(!finalDeliveries.isEmpty()) {
                            cAddress.CompanyName__c += finalDeliveries[0].Ship_to_address__r.CompanyName__c;
                            copyAddressFromFinal(cAddress,finalDeliveries[0]);
                        }
                    }

                     when 'CPA Final Delivery Details' {
                        if(!finalDeliveries.isEmpty()) {
                            cAddress.CompanyName__c += ' '+ finalDeliveries[0].Ship_to_address__r.CompanyName__c;
                            copyAddressFromFinal(cAddress,finalDeliveries[0]);
                        }
                    }

                    when 'TecEx c/o Final Delivery Company' {
                        cAddress.CompanyName__c = 'TecEx c/o ';
                        if(!finalDeliveries.isEmpty()) {
                            cAddress.CompanyName__c += finalDeliveries[0].Ship_to_address__r.CompanyName__c;
                            copyAddressFromFinal(cAddress,finalDeliveries[0]);
                        }
                    }
 when 'Client Registration Address' {

                        if(!regt.isEmpty()){
                            cAddress.CompanyName__c = regt[0].Name;
                            copyAddressFromRegistrations(cAddress, regt[0]);
                        }
                    }
                    when 'Client Registration, Tax # c/o Final Delivery Details' {

                        if(!regt.isEmpty()){
                            cAddress.CompanyName__c = regt[0].Name + ' (' + regt[0].VAT_number__c + ') c/o ';
                            if(!finalDeliveries.isEmpty()) {
                            copyAddressFromFinal(cAddress, finalDeliveries[0]);
                            }
                        }
                    }
                }
            }
        }
    }

  //this method will copy values in clientadddress from final address.
    public void copyAddressFromFinal(Client_Address__c clientAddress, Final_Delivery__c finalAddress){

        clientAddress.Address__c = getNullSafeValue(finalAddress.Ship_to_address__r.Address__c);
        clientAddress.Address2__c = getNullSafeValue(finalAddress.Ship_to_address__r.Address2__c);
        clientAddress.City__c = getNullSafeValue(finalAddress.Ship_to_address__r.City__c);
        clientAddress.Province__c = '';// This field not present on finalAddress object.
        clientAddress.Postal_Code__c = getNullSafeValue(finalAddress.Ship_to_address__r.Postal_Code__c);
        clientAddress.All_Countries__c = getNullSafeValue(finalAddress.Ship_to_address__r.All_Countries__c);
        clientAddress.Contact_Full_Name__c = getNullSafeValue(finalAddress.Ship_to_address__r.Contact_Full_Name__c);
        clientAddress.Contact_Email__c = getNullSafeValue(finalAddress.Ship_to_address__r.Contact_Email__c);
        clientAddress.Contact_Phone_Number__c = getNullSafeValue(finalAddress.Ship_to_address__r.Contact_Phone_Number__c);
        clientAddress.AdditionalContactNumber__c = '';
        clientAddress.Tax_Name_Number__c = getNullSafeValue(finalAddress.Ship_to_address__r.Tax_Name_Number__c);
    }

    //this method will copy values in clientadddress from registrations address.
	public void copyAddressFromRegistrations(Client_Address__c clientAddress, Registrations__c regtAddress){

        clientAddress.Address__c = getNullSafeValue(regtAddress.Registered_Address__c);
        clientAddress.Address2__c = getNullSafeValue(regtAddress.Registered_Address_2__c);
        clientAddress.City__c = getNullSafeValue(regtAddress.Registered_Address_City__c);
        clientAddress.Province__c = getNullSafeValue(regtAddress.Registered_Address_Province__c);
        clientAddress.Postal_Code__c = getNullSafeValue(regtAddress.Registered_Address_Postal_Code__c);
        clientAddress.All_Countries__c = ''; // no field on Registrations__c object
        clientAddress.Contact_Full_Name__c = '';
        clientAddress.Contact_Email__c = '';
        clientAddress.Contact_Phone_Number__c = '';
        clientAddress.AdditionalContactNumber__c = '';
        clientAddress.Tax_Name_Number__c = 'VAT No: '+getNullSafeValue(regtAddress.VAT_number__c);
    }

    public string getNullSafeValue(String val){
        if(val != null) {
            return val;
        }
        return '';
    }

    public List<Shipment_Order_Package__c> getShipmentOrderPackages(){
        List<Shipment_Order_Package__c> sopList = [SELECT Id,  Name,  packages_of_same_weight_dims__c,  Actual_Weight_KGs__c,  Breadth__c,  Height__c,  Length__c,  Dimension_Unit__c,  Shipment_Order__c FROM Shipment_Order_Package__c WHERE Shipment_Order__c = :shipmentOrder.Id];

        for(Shipment_Order_Package__c pckg : sopList) {
            this.numberOfPackages += pckg.packages_of_same_weight_dims__c;
            this.packagesWeight += (1*pckg.Actual_Weight_KGs__c);
            //this.packagesWeight += (pckg.packages_of_same_weight_dims__c*pckg.Actual_Weight_KGs__c);
        }
        return sopList;
    }

}