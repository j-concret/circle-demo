@isTest
public class NCPGetCasedetails_Test {
    
    public static testMethod void NCPSORelatedObjects_Test(){
        
        //Test Data 
        
        Access_token__c at = new Access_token__c();
        at.Status__c = 'Active';
        at.Access_Token__c = '1234';
        
        Insert at;
        
        Contact con = TestDataFactory.myContact();
                
        Profile prof = [select id from profile where name LIKE '%Standard%']; 
                
        Id shipmentOrderRecTypeId = Schema.SObjectType.shipment_order__c.getRecordTypeInfosByName().get('Cost Estimate').getRecordTypeId();
        
        CpaRules__c cpaRule = new CpaRules__c();
        cpaRule.Country__c = 'Finland';
        cpaRule.Criteria__c = 'Default';
        
        Insert cpaRule;
        
        Account acc = new Account(Name = 'Acme Test');
        
        Insert acc;
        
        IOR_Price_List__c priceList = new IOR_Price_List__c();
        priceList.Destination__c = 'Finland';
        priceList.Client_Name__c = acc.Id;
        priceList.IOR_Fee__c = 10;
        
        Insert priceList;
        
        shipment_order__c shipment = new shipment_order__c();
        shipment.RecordTypeId = shipmentOrderRecTypeId;
        shipment.Destination__c = 'Finland';
        shipment.IOR_Price_List__c = priceList.Id;
        shipment.Shipment_Value_USD__c = 600;
        shipment.Account__c = acc.Id;
        
        Insert shipment;
        
        User user = new User();
        user.firstName = 'Firstname1@';
        user.lastName = 'lastname1@';
        user.profileId = prof.id;
        user.email = '123_1@test.com';
        user.username = '123_1@test.com';
        user.EmailEncodingKey = 'ISO-8859-1';
        user.Alias = 'Alias';
        user.TimeZoneSidKey = 'America/Los_Angeles';
        user.LanguageLocaleKey = 'en_US';
        user.LocaleSidKey = 'en_US';
        
        insert user;
        
        
        Case c = new Case();
        c.Contactid = con.Id;
        c.Subject = 'Test Case Subject';
        c.Status = 'Waiting';
        c.Description = 'My Description';
        c.AccountId = acc.Id;
        c.Shipment_Order__c = shipment.Id;
        c.OwnerId = user.Id;   
        insert c;
        
        ContentVersion cv = new ContentVersion();
        cv.Title = 'Test Document';
        cv.PathOnClient = 'TestDocument.pdf';
        cv.VersionData = Blob.valueOf('Test Content');
        cv.IsMajorVersion = true;
        
        Insert cv;
        
        FeedItem fd = new FeedItem(Title = 'Hello',ParentId = c.Id, body='body');
        
        insert fd;
        
        FeedAttachment fda = new FeedAttachment(type='Content',FeedEntityId = fd.id, recordId=cv.Id);
        
        insert fda;
        
        //Json Body
        String json = '{"CaseID":"'+c.Id+'","AccessToken":"'+at.Access_Token__c+'"}';
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
        req.requestURI = '/services/apexrest/GetCaseDetails'; 
        req.httpMethod = 'Post';
        req.addHeader('Content-Type', 'application/json');
        req.requestBody = Blob.valueof(json);
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
        NCPGetCasedetails.NCPGetCasedetails();
        Test.stopTest(); 
    }
    
    public static testMethod void NCPSORelatedObjects_Test1(){
        
        //Test Data 
        
        Access_token__c at = new Access_token__c();
        at.Status__c = 'Active';
        at.Access_Token__c = '1234';
        
        Insert at;
        
        Profile prof = [select id from profile where name LIKE '%Standard%']; 
        
        Contact con = TestDataFactory.myContact();
        
        Id shipmentOrderRecTypeId = Schema.SObjectType.shipment_order__c.getRecordTypeInfosByName().get('Cost Estimate').getRecordTypeId();
        
        CpaRules__c cpaRule = new CpaRules__c();
        cpaRule.Country__c = 'Finland';
        cpaRule.Criteria__c = 'Default';
        
        Insert cpaRule;
        
        Account acc = new Account(Name = 'Acme Test');
        
        Insert acc;
        
        IOR_Price_List__c priceList = new IOR_Price_List__c();
        priceList.Destination__c = 'Finland';
        priceList.Client_Name__c = acc.Id;
        priceList.IOR_Fee__c = 10;
        
        Insert priceList;
        
        shipment_order__c shipment = new shipment_order__c();
        shipment.RecordTypeId = shipmentOrderRecTypeId;
        shipment.Destination__c = 'Finland';
        shipment.IOR_Price_List__c = priceList.Id;
        shipment.Shipment_Value_USD__c = 600;
        shipment.Account__c = acc.Id;
        
        Insert shipment;
        
        User user = new User();
        user.firstName = 'test1_case';
        user.lastName = 'test2_case';
        user.profileId = prof.id;
        user.username = 'test121_case@test.com';
        user.email = 'test@test.com';
        user.Alias = 'test572';
        user.TimeZoneSidKey = 'America/Los_Angeles';
        user.LocaleSidKey = 'en_US';
        user.EmailEncodingKey = 'ISO-8859-1';
        user.LanguageLocaleKey = 'en_US';
        insert user;
        
        
        Case c = new Case();
        c.Contactid = con.Id;
        c.Subject = 'Test Subject';
        c.Status = 'Waiting';
        c.Description = 'Description';
        c.AccountId = acc.Id;
        c.Shipment_Order__c = shipment.Id;
        c.OwnerId = user.Id;   
        insert c;
        
        FeedItem fd = new FeedItem(Title = 'Hello',ParentId = c.Id, body='body');
        
        insert fd;
        
        //Json Body
        String json = '{"CaseID":"'+c.Id+'","AccessToken":"'+at.Access_Token__c+'"}';
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
        req.requestURI = '/services/apexrest/GetCaseDetails'; 
        req.httpMethod = 'Post';
        req.addHeader('Content-Type', 'application/json');
        req.requestBody = Blob.valueof(json);
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
        NCPGetCasedetails.NCPGetCasedetails();
        Test.stopTest(); 
    }
    
    //Covering catch exception
    public static testMethod void NCPSORelatedObjects_Test2(){
        
        //Test Data 
        
        Access_token__c at = new Access_token__c();
        at.Status__c = 'Active';
        at.Access_Token__c = '1234';
        
        Insert at;
        
        Profile prof = [select id from profile where name LIKE '%Standard%']; 
        
        Contact con = TestDataFactory.myContact();
        
        Id shipmentOrderRecTypeId = Schema.SObjectType.shipment_order__c.getRecordTypeInfosByName().get('Cost Estimate').getRecordTypeId();
        
        CpaRules__c cpaRule = new CpaRules__c();
        cpaRule.Country__c = 'Finland';
        cpaRule.Criteria__c = 'Default';
        
        Insert cpaRule;
        
        Account acc = new Account(Name = 'Acme Test');
        
        Insert acc;
        
        IOR_Price_List__c priceList = new IOR_Price_List__c();
        priceList.Destination__c = 'Finland';
        priceList.Client_Name__c = acc.Id;
        priceList.IOR_Fee__c = 10;
        
        Insert priceList;
        
        shipment_order__c shipment = new shipment_order__c();
        shipment.RecordTypeId = shipmentOrderRecTypeId;
        shipment.Destination__c = 'Finland';
        shipment.IOR_Price_List__c = priceList.Id;
        shipment.Shipment_Value_USD__c = 600;
        shipment.Account__c = acc.Id;
        
        Insert shipment;
        
        Case c = new Case();
        c.Contactid = con.Id;
        c.Subject = 'Test Subject';
        c.Status = 'Waiting';
        c.Description = 'Description';
        c.AccountId = acc.Id;
        c.Shipment_Order__c = shipment.Id;
        insert c;
        
        FeedItem fd = new FeedItem(Title = 'Hello',ParentId = c.Id, body='body');
        
        insert fd;
        
        //Json Body
        String json = '{"CaseID":"'+c.Id+'","AccessToken":"'+at.Access_Token__c+'"}';
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
        req.requestURI = '/services/apexrest/GetCaseDetails'; 
        req.httpMethod = 'Post';
        req.addHeader('Content-Type', 'application/json');
        req.requestBody = Blob.valueof(json);
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
        NCPGetCasedetails.NCPGetCasedetails();
        Test.stopTest(); 
    }
}