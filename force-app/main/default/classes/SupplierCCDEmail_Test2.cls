@isTest(SeeAllData=true)
public with sharing class SupplierCCDEmail_Test2{

    static testmethod void testsendEmail(){

           Shipment_Order__c shipmentOrder = new Shipment_Order__c(Account__c ='0010Y00000PEqvHQAT' , Shipment_Value_USD__c =10000, CPA_v2_0__c = 'a261v00000EfZg7AAF',
                                                                Client_Contact_for_this_Shipment__c = '0030Y00000MGG9tQAH', Who_arranges_International_courier__c ='Client', Tax_Treatment__c ='DAP/CIF - IOR pays', Ship_to_Country__c = 'a0Z0Y000000xb2XUAQ',
                                                                Destination__c = 'Brazil', Service_Type__c = 'IOR', IOR_Price_List__c = 'a0L0Y000001tju3UAA', of_Unique_Line_Items__c = 10, Total_Taxes__c = 1500, of_Line_Items__c= 10, Chargeable_Weight__c = 10, of_packages__c = 1, Final_Deliveries_New__c = 1,
                                                                CI_Admin_Fee__c  =100, CI_Bank_Fees__c  = 100, CI_Finance_Fee__c  = 100, CI_Insurance_Fee_USD__c  = 100, CI_International_Delivery_Fee__c  = 100, CI_IOR_and_Import_Compliance_Fee_USD__c  = 100, 
                                                                CI_Miscellaneous_Fee__c  = 100, CI_Recharge_Tax_and_Duty__c  = 100, CI_Total_Clearance_Costs__c  = 100, CI_Total_Customs_Brokerage_Cost__c  = 100, CI_Total_Handling_Cost__c  = 100, 
                                                                CI_Total_Licence_Cost__c  = 100, FC_Admin_Fee__c  = 100, FC_Bank_Fees__c  = 100, FC_Finance_Fee__c  = 100, FC_Insurance_Fee_USD__c   = 100, FC_International_Delivery_Fee__c  = 100, 
                                                                FC_IOR_and_Import_Compliance_Fee_USD__c  = 100, FC_Miscellaneous_Fee__c  = 100, FC_Recharge_Tax_and_Duty__c  = 100, FC_Total_Clearance_Costs__c  = 100, 
                                                                FC_Total_Customs_Brokerage_Cost__c  = 100, FC_Total_Handling_Costs__c  = 100, FC_Total_License_Cost__c  = 100, Total_CIF_Duties__c = 0, Total_FOB_Duties__c = 0, Actual_Admin_Fee__c = 15, 
                                                                Actual_Bank_Fees__c = 15, Actual_Exchange_gain_loss_on_payment__c = 15, Actual_Finance_Fee__c = 15, Actual_Insurance_Fee_USD__c = 15, Actual_International_Delivery_Fee__c = 15, 
                                                                Actual_Miscellaneous_Fee__c = 15, Actual_Total_Clearance_Costs__c = 15, Actual_Total_Customs_Brokerage_Cost__c = 15, Actual_Total_Handling_Costs__c = 15, Actual_Total_IOR_EOR__c = 15, 
                                                                Actual_Total_License_Cost__c = 15, Actual_Total_Tax_and_Duty__c = 15,SupplierlU__c = '0011v000022KjNcAAK', CDC__c = '0051v000005XSIZAA4', Shipping_Status__c = 'Cost Estimate Abandoned');
        insert shipmentOrder;
        List<Tax_Calculation__c> calculatedTaxes = new List<Tax_Calculation__c>();

        calculatedTaxes.add(new Tax_Calculation__c( Shipment_Order__c =  shipmentOrder.Id,   Value__c = 1000, 
                                                                  Order_Number__c = '1', Tax_Name__c = 'I', Tax_Type__c = 'Duties'));

        calculatedTaxes.add(new Tax_Calculation__c( Shipment_Order__c =  shipmentOrder.Id,   Value__c = 1000, Applied_to_Order_Number__c = '1',
                                                                  Order_Number__c = '2', Part_Specific__c = FALSE, Tax_Name__c = 'II', Tax_Type__c = 'Duties'));  


        calculatedTaxes.add(new Tax_Calculation__c( Shipment_Order__c =  shipmentOrder.Id,   Value__c = 1000,
                                                                  Order_Number__c = '3', Part_Specific__c = FALSE, Tax_Name__c = 'III', Tax_Type__c = 'Duties'));

        calculatedTaxes.add(new Tax_Calculation__c( Shipment_Order__c =  shipmentOrder.Id,   Value__c = 1000, Applied_to_Order_Number__c = '1,2,3',
                                                                  Order_Number__c = '4', Part_Specific__c = FALSE, Tax_Name__c = 'IV', Tax_Type__c = 'Duties'));  


        calculatedTaxes.add(new Tax_Calculation__c( Shipment_Order__c =  shipmentOrder.Id,   Value__c = 1000,
                                                                  Order_Number__c = '5', Part_Specific__c = FALSE, Tax_Name__c = 'V', Tax_Type__c = 'Duties'));

        calculatedTaxes.add(new Tax_Calculation__c( Shipment_Order__c =  shipmentOrder.Id,   Value__c = 1000, Applied_to_Order_Number__c = '1,2,3',
                                                                  Order_Number__c = '6', Part_Specific__c = FALSE, Tax_Name__c = 'VI', Tax_Type__c = 'Duties'));  

         calculatedTaxes.add(new Tax_Calculation__c( Shipment_Order__c =  shipmentOrder.Id,   Value__c = 1000, Applied_to_Order_Number__c = '1,2,3',
                                                                  Order_Number__c = '7', Part_Specific__c = FALSE, Tax_Name__c = 'VII', Tax_Type__c = 'Duties'));

         calculatedTaxes.add(new Tax_Calculation__c( Shipment_Order__c =  shipmentOrder.Id,   Value__c = 1000, Applied_to_Order_Number__c = '1,2,3',
                                                                  Order_Number__c = '8', Part_Specific__c = FALSE, Tax_Name__c = 'VII', Tax_Type__c = 'Duties'));

          calculatedTaxes.add(new Tax_Calculation__c( Shipment_Order__c =  shipmentOrder.Id,   Value__c = 1000, Applied_to_Order_Number__c = '1,2,3',
                                                                  Order_Number__c = '9', Part_Specific__c = FALSE, Tax_Name__c = 'VIIII', Tax_Type__c = 'Duties'));                                  

          calculatedTaxes.add(new Tax_Calculation__c( Shipment_Order__c =  shipmentOrder.Id,   Value__c = 1000, Applied_to_Order_Number__c = '1,2,3',
                                                                  Order_Number__c = '10', Part_Specific__c = FALSE, Tax_Name__c = 'X', Tax_Type__c = 'Duties'));  
         
         
          insert calculatedTaxes;
        
        List<Id> SOIdList = new List<Id>{shipmentOrder.id};

        test.startTest();
     
       SupplierCCDEmail.sendEmail(SOIdList);
        


        test.stopTest();
    }
}