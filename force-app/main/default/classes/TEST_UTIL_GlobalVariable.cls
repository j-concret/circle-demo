@isTest(seeAllData = false)
private class TEST_UTIL_GlobalVariable 
{
	private static final Integer STR_LEN = 10;
	
	private static final String VARIABLE_1 = al.RandomStringUtils.randomAlphanumeric(STR_LEN);
	private static final String VARIABLE_2 = al.RandomStringUtils.randomAlphanumeric(STR_LEN);

    private static testmethod void putInsert()
    {
        String previousValue = UTIL_GlobalVariable.put(null);
        assertGlobalVariableExistsWith(null);
        System.assertEquals(null,previousValue);
        System.assertEquals(null,UTIL_GlobalVariable.get());

        previousValue = UTIL_GlobalVariable.put( VARIABLE_1);
        assertGlobalVariableExistsWith(VARIABLE_1);
        System.assertNotEquals(null, previousValue);
        System.assertEquals(VARIABLE_1, UTIL_GlobalVariable.get());

        previousValue = UTIL_GlobalVariable.put(VARIABLE_1);
        assertGlobalVariableExistsWith(VARIABLE_1);
        System.assertEquals(VARIABLE_1, previousValue);
        System.assertEquals(VARIABLE_1, UTIL_GlobalVariable.get());        
    }
    
    private static testmethod void putUpdate()
    {
        Features_Switches__c record = new Features_Switches__c(Value__c = VARIABLE_2);  
        insert record;
        
        assertGlobalVariableExistsWith(VARIABLE_2);

        String previousValue = UTIL_GlobalVariable.put(VARIABLE_1);
        
        assertGlobalVariableExistsWith(VARIABLE_1);
        System.assertNotEquals(VARIABLE_2,previousValue);
        System.assertEquals(VARIABLE_1,UTIL_GlobalVariable.get());
    }

    private static void assertGlobalVariableExistsWith(String value)
    {
        Features_Switches__c record = Features_Switches__c.getInstance();
        
        System.assertNotEquals(null,record);
        System.assertEquals(value, record.Value__c);
    }
}