@isTest
public class NCPUpdateSOPackage_Test {

    static testMethod void testPostMethod(){
        Access_token__c accessTokenObj = new Access_token__c();
        accessTokenObj.Status__c = 'Active';
        accessTokenObj.Access_Token__c = 'asdfghj-sdfgfds-sfdfgf';
        
        Insert accessTokenObj;
        
        Account acc = new Account(Name = 'TecEx Prospective Client');
        
        Insert acc;
        
        Contact con = new Contact(FirstName = 'Testing',LastName = 'contact',AccountId = acc.Id,Email='test@test.com',Phone='123456789');
        insert con;
        
        Shipment_Order_Package__c sop = new Shipment_Order_Package__c();
        sop.Weight_Unit__c = 'KGs';
        sop.Lithium_Batteries__c = true;
        sop.Contains_Batteries__c = false;
        sop.Dimension_Unit__c = 'CMs';
        sop.packages_of_same_weight_dims__c = 1;
        sop.Length__c = 1.0;
        sop.Height__c= 1.0;
        sop.Breadth__c= 1.0;
        sop.Actual_Weight__c = 2.0;
        insert sop;
            
        NCPUpdateSOPackageWra wrapper = new NCPUpdateSOPackageWra();
        NCPUpdateSOPackageWra.SOP sopObj = new NCPUpdateSOPackageWra.SOP();
        sopObj.SOPID = sop.Id;
        sopObj.Weight_Unit = sop.Weight_Unit__c;
        sopObj.Dimension_Unit = sop.Dimension_Unit__c;
        sopObj.Packages_of_Same_Weight = Integer.valueOf(sop.packages_of_same_weight_dims__c);
        sopObj.Length = String.valueOf(sop.Length__c);
        sopObj.Height = String.valueOf(sop.Height__c);
        sopObj.Breadth = String.valueOf(sop.Breadth__c);
        sopObj.Actual_Weight = String.valueOf(sop.Actual_Weight__c);
        sopObj.LithiumBatteries = sop.Lithium_Batteries__c;
        sopObj.Contains_Batteries = sop.Contains_Batteries__c;
        
        List<NCPUpdateSOPackageWra.SOP> sopList = new List<NCPUpdateSOPackageWra.SOP>();
        sopList.add(sopObj);
        
        wrapper.SOP = sopList;
        wrapper.Accesstoken = accessTokenObj.Access_Token__c;
        wrapper.AccountID = acc.Id;
        wrapper.ContactID = con.Id;
        
		RestRequest request = new RestRequest();
        RestResponse res = new RestResponse();
        request.requestUri ='/services/apexrest/api/UpdateSOPackage/';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueOf(JSON.serialize(wrapper));
        RestContext.request = request;
        RestContext.response = res;
        
        Test.startTest();
        NCPUpdateSOPackage.NCPUpdateSOPackage();
        Test.stopTest();        
    }
    
    static testMethod void testPostMethodCatchBlock(){
        Access_token__c accessTokenObj = new Access_token__c();
        accessTokenObj.Status__c = 'Active';
        accessTokenObj.Access_Token__c = 'asdfghj-sdfgfds-sfdfgf';
        
        Insert accessTokenObj;
        
        Account acc = new Account(Name = 'TecEx Prospective Client');
        
        Insert acc;
        
        Contact con = new Contact(FirstName = 'Testing',LastName = 'contact',AccountId = acc.Id,Email='test@test.com',Phone='123456789');
        insert con;
        
        Shipment_Order_Package__c sop = new Shipment_Order_Package__c();
        sop.Weight_Unit__c = 'KGs';
        sop.Lithium_Batteries__c = true;
        sop.Contains_Batteries__c = true;
        sop.Dimension_Unit__c = 'CMs';
        sop.packages_of_same_weight_dims__c = 1;
        sop.Length__c = 1.0;
        sop.Height__c= 1.0;
        sop.Breadth__c= 1.0;
        sop.Actual_Weight__c = 2.0;
        insert sop;
            
        NCPUpdateSOPackageWra wrapper = new NCPUpdateSOPackageWra();
        NCPUpdateSOPackageWra.SOP sopObj = new NCPUpdateSOPackageWra.SOP();
        sopObj.SOPID = sop.Id;
        sopObj.Weight_Unit = 'KG';
        sopObj.Dimension_Unit = sop.Dimension_Unit__c;
        sopObj.Packages_of_Same_Weight = Integer.valueOf(sop.packages_of_same_weight_dims__c);
        sopObj.Length = String.valueOf(sop.Length__c);
        sopObj.Height = String.valueOf(sop.Height__c);
        sopObj.Breadth = String.valueOf(sop.Breadth__c);
        sopObj.Actual_Weight = String.valueOf(sop.Actual_Weight__c);
        sopObj.LithiumBatteries = sop.Lithium_Batteries__c;
        sopObj.Contains_Batteries = sop.Contains_Batteries__c;
        
        List<NCPUpdateSOPackageWra.SOP> sopList = new List<NCPUpdateSOPackageWra.SOP>();
        sopList.add(sopObj);
        
        wrapper.SOP = sopList;
        wrapper.Accesstoken = accessTokenObj.Access_Token__c;
        wrapper.AccountID = acc.Id;
        wrapper.ContactID = con.Id;
        
		RestRequest request = new RestRequest();
        RestResponse res = new RestResponse();
        request.requestUri ='/services/apexrest/api/UpdateSOPackage/';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueOf(JSON.serialize(wrapper));
        RestContext.request = request;
        RestContext.response = res;
        
        Test.startTest();
        NCPUpdateSOPackage.NCPUpdateSOPackage();
        Test.stopTest();        
    }
}