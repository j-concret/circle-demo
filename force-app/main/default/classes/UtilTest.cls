@isTest(SeeAllData=true)
public class UtilTest 
{
	@isTest
    static void testPrepareErrorMap()
    {
        Test.startTest();
        Map<String,Object> result= Util.prepareErrorMap('Error');
        Util.getResponseString('Test','Test');
        Test.stopTest();
        //System.debug(result.get('msg'));
        System.assertEquals('Error', result.get('msg'));
    }
    
    @isTest
    static void testGetAppName(){
        
        Test.startTest();
        Util.skipCodeExecution = false;
        	Util.getAppName();
        Util.skipCodeExecution = true;
        Test.stopTest();
    }
    
    @isTest
    static void testparseCSV(){
        
        Test.startTest();
        String csvBody = 'name,title\na,b,c';
        Util.parseCSV(Blob.valueOf(csvBody),false);
        Test.stopTest();
    }
    
    @isTest
    public static void testLogShipmentAndPlatformECount(){
        List<TaskChange__e> evts = new List<TaskChange__e>();
        evts.add(new TaskChange__e(Source__c='shipmentTrigger'));
        evts.add(new TaskChange__e(Source__c='FeedItem_Trigger'));
        evts.add(new TaskChange__e(Source__c='CreateCourierrates'));
        
        Test.startTest();
        
        // Publish test event
        EventBus.publish(evts);
            
        Test.stopTest();
    }
    
    @isTest
    public static void testCourierRateJobEventAndPlatformECount(){
        List<Courier_Rates_Job_Status__e> evts = new List<Courier_Rates_Job_Status__e>();
        evts.add(new Courier_Rates_Job_Status__e(Source__c='shipmentTrigger'));
        evts.add(new Courier_Rates_Job_Status__e(Source__c='FeedItem_Trigger'));
        evts.add(new Courier_Rates_Job_Status__e(Source__c='CreateCourierrates'));
        
        Test.startTest();
        
        // Publish test event
        EventBus.publish(evts);
            
        Test.stopTest();
    }
}