@RestResource(urlMapping='/SendAllClientShipmentOrders/*')
Global class CPSendAllClientShipmentOrders {
   
    @Httpget
    global static void SendAllClientShipmentOrders(){
        
    	RestRequest req = RestContext.request;
    	RestResponse res = RestContext.response;
        res.addHeader('Content-Type', 'application/json');
        String Username = req.params.get('Username');
        String Password = req.params.get('Password');
        String ClientAccId = req.params.get('ClientAccId');
       	Contact logincontact = [select Id,Account.id, lastname,Email,UserName__c,Password__c,Contact_Status__c from Contact where UserName__c =:username and Contact_status__c = 'Active' limit 1 ];
        try {
        
            if(logincontact.Password__c == Password && logincontact.UserName__c == Username && logincontact.Contact_status__C == 'Active' ){
               
                    
                List<shipment_order__c> SOs = [select id,name,shipping_status__c from shipment_Order__C where Account__c =:ClientAccId ];
                
                    res.responseBody = Blob.valueOf(JSON.serializePretty(SOs));
        			res.statusCode = 200;
                
                API_Log__c Al = New API_Log__c();
                Al.Account__c=logincontact.AccountID;
                Al.Login_Contact__c=logincontact.Id;
                Al.EndpointURLName__c='SendAllClientShipmentOrders';
                Al.Response__c='Success - List of shipment orders are sent';
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al;
               
            
              }
            else{
                    String ErrorString ='Authentication failed, Please check username and password';
               		res.responseBody = Blob.valueOf(JSON.serializePretty(ErrorString));
        			res.statusCode = 400;
                
                API_Log__c Al = New API_Log__c();
                Al.Account__c=logincontact.AccountID;
                Al.Login_Contact__c=logincontact.Id;
                Al.EndpointURLName__c='SendAllClientShipmentOrders';
                Al.Response__c=ErrorString;
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al;
                
            	}
        
            }
            catch (Exception e) {
            res.responseBody = Blob.valueOf(e.getMessage());
            res.statusCode = 500;
                 API_Log__c Al = New API_Log__c();
                Al.Account__c=logincontact.AccountID;
                Al.Login_Contact__c=logincontact.Id;
                Al.EndpointURLName__c='SendAllClientShipmentOrders';
                Al.Response__c=e.getMessage();
                Al.StatusCode__c=string.valueof(res.statusCode);
                Insert Al;
            } 

	}

}