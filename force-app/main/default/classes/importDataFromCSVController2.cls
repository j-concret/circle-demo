public class importDataFromCSVController2 {

public Blob csvFileBody{get;set;}
public string csvAsString{get;set;}
public String[] csvFileLines{get;set;}
Public Roll_Out__c rollout = new Roll_Out__c ();
public List<Cost_Estimate__c> costEstimate {get;set;}
public List<cCostEstimate> costList {get; set;}
public Boolean hasSelAcct {get;set;}



public importDataFromCSVController2(ApexPages.StandardController controller){
this.rollOut =  [Select Id, Name, Client_Name__c, Create_Roll_Out__c,  Destinations__c, Shipment_Value_in_USD__c, Contact_For_The_Quote__c, File_Reference_Name__c  FROM Roll_Out__c WHERE Id = :ApexPages.currentPage().getParameters().get('id') Limit 1];
this.costEstimate =[ Select Id, Name, Admin_Fee__c, Associated_to_Lead__c, Bank_Fee1__c, Branch__c, Calculated_Hub_Shipping_Cost__c, Calculated_International_Delivery_Cost__c, Chargeable_Weight__c, Client_Name__c, My_Reference__c, Client_Reference_2__c, 
                     Company__c, Contact_Name__c, Contact_for_Quote__c, Contact_Last_Name__c, Contact_Telephone__c, Converted_to_SO__c, Country__c, Customs_Brokerage_Cost1__c, Customs_Clearance_Cost1__c, Customs_Handling_Cost1__c, Delivery_Contact_Name__c, 
                     Delivery_Contact_Tel_number__c, Destination__c, Email_Estimate_to__c, Do_you_want_a_Freight_Quote__c, Estimate_Customs_Clearance_Time__c, Estimated_Total_Cost__c, Estimate_Taxes1__c, Estimate_Transit_time__c, Expected_IOR_Cost__c, Final_Delivery_Address__c, 
                     Forecast_Profit__c, Hub__c, Hub_Lane__c, Hub_Rate_KG__c, Hub_Ship_From_Zone__c, Int_Courier_Responsibility_of__c, IOR_Cost__c, IOR_Fee1__c, IOR_PL_Clearance__c, IOR_PL_Brokerage__c, IOR_PL_Handling__c, IOR_PL_License__c, IOR_Supplier__c, Licence_Cost__c, 
                     Licence_Cost1__c, License_Permit_Cost1__c, Li_ion_Batteries__c, Lithium_Battery_Types__c, Manual_Hub_Shipping_Cost__c, Manual_International_Delivery_Cost__c, OwnerIdOld__c, Part_Of_Roll_Out__c, Request_Email_Estimate__c, Roll_Out__c, Set_up_Fee1__c, sfoldid__c, 
                     Ship_From_Zone1__c, Ship_From_Country__c, Ship_from_zone__c, Shipment_Value_in_USD__c, Shipping_Notes__c, Ship_to_Country__c, Ship_to__c, Ship_To_Second_Leg__c, Status__c, Tecex_Freight_Fee__c, Tecex_Freight_Fee1__c, TecEx_Insurance__c, Total_Brokerage_Costs__c, 
                     Total_Brokerage_Costs1__c, Total_Clearance_Costs__c, Total_Clearance_Costs1__c, Total_Handling_Costs__c, Total_Handling_Costs1__c, Value_Hub_Shipping_Cost__c, Value_International_Delivery_Cost__c, Web_Enquiry_Notes__c, Ship_to_Country__r.Name, Client_Name__r.Name
                FROM Cost_Estimate__c where Roll_Out__c = :rollOut.Id];
    csvFileLines = new String[]{};
    getCosts();
  
  }
 
  public List<cCostEstimate> getCosts() {
     
        costList= new List<cCostEstimate>();
        
        If( costEstimate != null)
        
        {
       
        
        for(Cost_Estimate__c CE: [select Id, Name, Admin_Fee__c, Associated_to_Lead__c, Bank_Fee1__c, Branch__c, Calculated_Hub_Shipping_Cost__c, Calculated_International_Delivery_Cost__c, Chargeable_Weight__c, Client_Name__c, My_Reference__c, Client_Reference_2__c, 
                     Company__c, Contact_Name__c, Contact_for_Quote__c, Contact_Last_Name__c, Contact_Telephone__c, Converted_to_SO__c, Country__c, Customs_Brokerage_Cost1__c, Customs_Clearance_Cost1__c, Customs_Handling_Cost1__c, Delivery_Contact_Name__c, 
                     Delivery_Contact_Tel_number__c, Destination__c, Email_Estimate_to__c, Do_you_want_a_Freight_Quote__c, Estimate_Customs_Clearance_Time__c, Estimated_Total_Cost__c, Estimate_Taxes1__c, Estimate_Transit_time__c, Expected_IOR_Cost__c, Final_Delivery_Address__c, 
                     Forecast_Profit__c, Hub__c, Hub_Lane__c, Hub_Rate_KG__c, Hub_Ship_From_Zone__c, Int_Courier_Responsibility_of__c, IOR_Cost__c, IOR_Fee1__c, IOR_PL_Clearance__c, IOR_PL_Brokerage__c, IOR_PL_Handling__c, IOR_PL_License__c, IOR_Supplier__c, Licence_Cost__c, 
                     Licence_Cost1__c, License_Permit_Cost1__c, Li_ion_Batteries__c, Lithium_Battery_Types__c, Manual_Hub_Shipping_Cost__c, Manual_International_Delivery_Cost__c, OwnerIdOld__c, Part_Of_Roll_Out__c, Request_Email_Estimate__c, Roll_Out__c, Set_up_Fee1__c, sfoldid__c, 
                     Ship_From_Zone1__c, Ship_From_Country__c, Ship_from_zone__c, Shipment_Value_in_USD__c, Shipping_Notes__c, Ship_to_Country__c, Ship_to__c, Ship_To_Second_Leg__c, Status__c, Tecex_Freight_Fee__c, Tecex_Freight_Fee1__c, TecEx_Insurance__c, Total_Brokerage_Costs__c, 
                     Total_Brokerage_Costs1__c, Total_Clearance_Costs__c, Total_Clearance_Costs1__c, Total_Handling_Costs__c, Total_Handling_Costs1__c, Value_Hub_Shipping_Cost__c, Value_International_Delivery_Cost__c, Web_Enquiry_Notes__c, Ship_to_Country__r.Name, Client_Name__r.Name
                      FROM Cost_Estimate__c where Roll_Out__c = :ApexPages.currentPage().getParameters().get('id') and Status__c != 'Accepted']) {
            // As each contact is processed we create a new cContact object and add it to the contactList
            costList.add(new cCostEstimate (CE, True));
        }
        return costList;
        
        }
        
        Else
        
        {
        
        return null;
        
        }
  }
  
  
  public Pagereference importCSVFile2(){
       try{
           csvAsString = csvFileBody.toString();
           csvFileLines = csvAsString.split('\n'); 
            
           for(Integer i=1;i<csvFileLines.size();i++){
               Cost_Estimate__c accObj = new Cost_Estimate__c() ;
               string[] csvRecordData = csvFileLines[i].split(',');
               accObj.Roll_Out__c = rollOut.Id;
               accObj.Client_Name__c = rollOut.Client_Name__c;
               accObj.Contact_for_Quote__c = rollOut.Contact_For_The_Quote__c;
               accObj.Destination__c = csvRecordData[0] ;             
               accObj.Shipment_Value_in_USD__c = Decimal.valueOf(csvRecordData[1]);
               accObj.Email_Estimate_to__c = csvRecordData[2];
               accObj.My_Reference__c = csvRecordData[3];   
               
               costEstimate.add(accObj);                                                                             
                 
           }
        upsert costEstimate;
        PageReference pageRef = new ApexPages.StandardController(rollout).view();
        pageRef.setRedirect(true);
        return pageRef;
         
        }
        catch (Exception e)
        {
            ApexPages.Message errorMessage = new ApexPages.Message(ApexPages.severity.ERROR,'An error has occured while importing data Please make sure input csv file is correct');
            ApexPages.addMessage(errorMessage);
            
            return null;
        } 
       
  }
    
  public Pagereference exportAll(){

    return new Pagereference('/apex/ExportRollOut');
    PageReference pageRef = ApexPages.currentPage();
    pageRef.setRedirect(true);
    return pageRef;
    
 }
 
 public PageReference processSelected() {

        //We create a new list of Contacts that we be populated only with Contacts if they are selected
        List<Cost_Estimate__c> selectedCosts = new List<Cost_Estimate__c>();

        //We will cycle through our list of cContacts and will check to see if the selected property is set to true, if it is we add the Contact to the selectedContacts list
        for(cCostEstimate cCost: costList) {
            if(cCost.selected == true) {           
                selectedCosts.add(cCost.cCE);
            }
        }

        // Now we have our list of selected contacts and can perform any type of logic we want, sending emails, updating a field on the Contact, etc
        
        
        System.debug('These are the selected Contacts...'+ selectedCosts );
        
        List<Cost_Estimate__c > c = new List<Cost_Estimate__c>();
        for(Cost_Estimate__c costs: selectedCosts) {
            costs.Status__c ='Accepted';
            costs.Contact_for_Quote__c = rollOut.Contact_For_The_Quote__c;
            c.add(costs);            
        }
        
        try{
        
        update c;
        PageReference pageRef = new ApexPages.StandardController(rollout).view();
        pageRef.setRedirect(true);
        return pageRef;
        costList =null; // we need this line if we performed a write operation  because getContacts gets a fresh list now
        
         }
        catch(Exception ex){
        ApexPages.addMessages(ex);
}
       
        
       
        
        return null;
    }
 
 
 
 
 public class cCostEstimate {
        public Cost_Estimate__c cCE {get; set;}
        public Boolean selected {get; set;}
       
        public cCostEstimate (Cost_Estimate__c CE, Boolean sel) {
            cCE = CE;        
            selected = sel;
        }
    }
    
    


}