@IsTest
public class NCPDefaultsGet5_Test {

    @testSetup
    static void dataSetup() {
        Account acc = new Account(Name = 'Testing');
        insert acc;
    }
    
    
    static testMethod void testGetMethodPositive(){
        Account acc = [SELECT Id from Account LIMIT 1];
        
        List<CPDefaults__c> CPD = new List<CPDefaults__c>();
        List<Client_Address__c> clientAddrs = new List<Client_Address__c>();
        
		Id recordTypeId = Schema.SObjectType.Client_Address__c.getRecordTypeInfosByDeveloperName().get('PickUp').getRecordTypeId();
        Id FinrecordTypeId = Schema.SObjectType.Client_Address__c.getRecordTypeInfosByDeveloperName().get('Final_Destinations').getRecordTypeId();
        
        for(Integer i=0; i<=1; i++){
            CPDefaults__c cpDefaults = new CPDefaults__c(Client_Account__c=acc.Id,Order_Type__c='Final Quote');
            CPD.add(cpDefaults);
        }
        
        for(Integer i=0; i<=1; i++){
            Client_Address__c clientAddr = new Client_Address__c(Client__c=acc.Id,RecordTypeId =recordTypeId,All_Countries__c='Brazil',City__c='Rio',CompanyName__c = 'Testing', Address__c='Adddress',Province__c = 'Trest',Postal_Code__c='304309',Contact_Phone_Number__c = '0722854399',Contact_Email__c ='TEst@test.com',Contact_Full_Name__c= 'TEstName');
            Client_Address__c clientAddr1 = new Client_Address__c(Client__c=acc.Id,RecordTypeId =FinrecordTypeId,All_Countries__c='Brazil',City__c='Rio',CompanyName__c = 'Testing', Address__c='Adddress',Province__c = 'Trest',Postal_Code__c='304309',Contact_Phone_Number__c = '0722854399',Contact_Email__c ='TEst@test.com',Contact_Full_Name__c= 'TEstName');
            clientAddrs.add(clientAddr);
            clientAddrs.add(clientAddr1);
        }
        
        insert CPD;
        insert clientAddrs;
        
        RestRequest request = new RestRequest();
        RestResponse res = new RestResponse();
        request.requestUri ='/services/apexrest/api/NCPGetDefaults/';
        request.httpMethod = 'GET';
        request.addParameter('ClientAccId', acc.Id);
        RestContext.request = request;
        RestContext.response = res;
        
        Test.startTest();
        NCPDefaultsGet5.NCPDefaultsGet5();
        Test.stopTest(); 
         
    }
    
}