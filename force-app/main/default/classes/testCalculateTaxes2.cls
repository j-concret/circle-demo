@isTest
public class testCalculateTaxes2 {
    
   private static testMethod void  setUpData(){
   
   
       Account account = new Account (name='Acme1', Type ='Client', CSE_IOR__c = '0050Y000001LTZO', Service_Manager__c = '0050Y000001LTZO');
        insert account;  
        
        Account account2 = new Account (name='Test Supplier', Type ='Supplier', ICE__c = '0050Y000001km5c', RecordTypeId = '0120Y0000006KjQQAU');
        insert account2;      
         
        Contact contact = new Contact ( lastname ='Testing Individual',  AccountId = account.Id);
        insert contact;  
        
        IOR_Price_List__c iorpl = new IOR_Price_List__c ( Client_Name__c = account.Id, Name = 'Brazil', IOR_Fee__c = 0.15, On_Charge_Mark_up__c = 0.15,
        TecEx_Shipping_Fee_Markup__c =0.40, IOR_Min_Fee__c = 100,  Admin_Fee__c =100, Set_up_fee__c =100, Bank_Fees__c =200, Tax_Rate__c = 0.20,
        Estimated_Customs_Brokerage__c =250, Estimated_Customs_Clearance__c =250, Estimated_Customs_Handling__c =250, Estimated_Import_License__c =250, Destination__c = 'Brazil' );
        insert iorpl; 
        
        IOR_Price_List__c iorpl2 = new IOR_Price_List__c ( Client_Name__c = account.Id, Name = 'France', Destination__c = 'France', IOR_Fee__c = 0.15, On_Charge_Mark_up__c = 0.15,
        TecEx_Shipping_Fee_Markup__c =0.40, IOR_Min_Fee__c = 100,  Admin_Fee__c =100, Set_up_fee__c =100, Bank_Fees__c =200, Tax_Rate__c = 0.20,
        Estimated_Customs_Brokerage__c =250, Estimated_Customs_Clearance__c =250, Estimated_Customs_Handling__c =250, Estimated_Import_License__c =250);
        insert iorpl2;
        
       
        country_price_approval__c cpa = new country_price_approval__c( name ='Brazil', Billing_term__c = 'DAP/CIF - IOR pays',  Airwaybill_Instructions__c = 'User IOR Address', Supplier__c = account2.Id, 
        In_Country_Specialist__c = '0050Y000001km5c', Clearance_Destination__c ='Brazil', Preferred_Supplier__c = TRUE);
        insert cpa;
        
        country_price_approval__c cpa2 = new country_price_approval__c( name ='France', Billing_term__c = 'DAP/CIF - IOR pays',  Airwaybill_Instructions__c = 'User IOR Address', Supplier__c = account2.Id,
        In_Country_Specialist__c = '0050Y000001km5c', Clearance_Destination__c ='France', Preferred_Supplier__c = TRUE);
        insert cpa2; 
        
        CPA_v2_0__c cpav2 = new CPA_v2_0__c(Name = 'Brazil Testing 2.0', Supplier__c = account2.Id, Service_Type__c = 'IOR/EOR', Related_Costing_CPA__c = 'a260Y00000EnDjr', Preferred_Supplier__c = TRUE,
        VAT_Rate__c = 0.1, Destination__c = 'Brazil');
        insert cpav2;
       
        List<CPA_Costing__c> costs = new List<CPA_Costing__c>();
        costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'Shipment value', Cost_Type__c = 'Variable',  Amount__c = null, Rate__c = 10, IOR_EOR__c = 'IOR', Min__c = 375, Max__c = 14000, Conditional_value__c = 'Shipment value', Condition__c = 'Between', Floor__c = 0, Ceiling__c = 10000));
       costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'Shipment value', Cost_Type__c = 'Variable',  Amount__c = null, Rate__c = 10, IOR_EOR__c = 'IOR', Min__c = 563, Max__c = 14000, Conditional_value__c = 'Shipment value', Condition__c = 'Between', Floor__c = 10000, Ceiling__c = 20000));
       costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'Shipment value', Cost_Type__c = 'Variable',  Amount__c = null, Rate__c = 10, IOR_EOR__c = 'IOR', Min__c = 750, Max__c = 14000, Conditional_value__c = 'Shipment value', Condition__c = '>', Floor__c = 20000));
       costs.add(new CPA_Costing__c(Cost_Category__c = 'International freight',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'Chargeable weight', Cost_Type__c = 'Variable',  Amount__c = null, Rate__c = 10, IOR_EOR__c = 'IOR', Min__c = 100, Max__c = 500, Conditional_value__c = 'Chargeable weight', Condition__c = '>', Floor__c = 100));
       costs.add(new CPA_Costing__c(Cost_Category__c = 'International freight',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'Chargeable weight', Cost_Type__c = 'Variable',  Amount__c = null, Rate__c = 10, IOR_EOR__c = 'IOR', Min__c = 55));
       costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'Shipment value', Cost_Type__c = 'Variable',  Amount__c = null, Rate__c = 10, IOR_EOR__c = 'IOR', Conditional_value__c = 'CIF value', Condition__c = '>', Floor__c = 20000));
       costs.add(new CPA_Costing__c(Cost_Category__c = 'Miscellaneous',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'CIF value', Cost_Type__c = 'Variable',  Amount__c = null, Rate__c = 10, IOR_EOR__c = 'IOR'));
       costs.add(new CPA_Costing__c(Cost_Category__c = 'Miscellaneous',CPA_v2_0__c = cpav2.Id, Applied_to__c = '# packages', Cost_Type__c = 'Variable', Amount__c = null, Rate__c = 10, IOR_EOR__c = 'IOR'));
       costs.add(new CPA_Costing__c(Cost_Category__c = 'Miscellaneous',CPA_v2_0__c = cpav2.Id, Applied_to__c = '# Parts (line items)', Cost_Type__c = 'Variable', Amount__c = null, Rate__c = 10, IOR_EOR__c = 'IOR'));
       costs.add(new CPA_Costing__c(Cost_Category__c = 'Miscellaneous',CPA_v2_0__c = cpav2.Id, Applied_to__c = '# Unique HS Codes', Cost_Type__c = 'Variable', Amount__c = null, Rate__c = 10, IOR_EOR__c = 'IOR'));
       costs.add(new CPA_Costing__c(Cost_Category__c = 'Miscellaneous',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'Total Taxes', Cost_Type__c = 'Variable', Amount__c = null, Rate__c = 10, IOR_EOR__c = 'IOR'));
       costs.add(new CPA_Costing__c(Cost_Category__c = 'Miscellaneous',CPA_v2_0__c = cpav2.Id, Variable_threshold__c = 10,  Applied_to__c = 'Shipment value', Cost_Type__c = 'Variable',  Amount__c = null, Rate__c = 10, IOR_EOR__c = 'IOR'));
       costs.add(new CPA_Costing__c(Cost_Category__c = 'Miscellaneous',CPA_v2_0__c = cpav2.Id, Variable_threshold__c = 100,  Applied_to__c = 'Chargeable weight', Cost_Type__c = 'Variable',  Amount__c = null, Rate__c = 10, IOR_EOR__c = 'IOR', VAT_applicable__c = TRUE));
       costs.add(new CPA_Costing__c(Cost_Category__c = 'Miscellaneous',CPA_v2_0__c = cpav2.Id, Cost_Type__c = 'Fixed',  Amount__c = 300, Rate__c = null, IOR_EOR__c = 'IOR'));
       costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'Shipment value', Cost_Type__c = 'Variable',  Amount__c = null, Rate__c = 10, IOR_EOR__c = 'EOR', Min__c = 375, Max__c = 14000, Conditional_value__c = 'Shipment value', Condition__c = 'Between', Floor__c = 0, Ceiling__c = 10000));
       costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'Shipment value', Cost_Type__c = 'Variable',  Amount__c = null, Rate__c = 10, IOR_EOR__c = 'EOR', Min__c = 563, Max__c = 14000, Conditional_value__c = 'Shipment value', Condition__c = 'Between', Floor__c = 10000, Ceiling__c = 20000));
       costs.add(new CPA_Costing__c(Cost_Category__c = 'IOR Cost',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'Shipment value', Cost_Type__c = 'Variable',  Amount__c = null, Rate__c = 10, IOR_EOR__c = 'EOR', Min__c = 750, Max__c = 14000, Conditional_value__c = 'Shipment value', Condition__c = '>', Floor__c = 20000));
       costs.add(new CPA_Costing__c(Cost_Category__c = 'International freight',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'Chargeable weight', Cost_Type__c = 'Variable',  Amount__c = null, Rate__c = 10, IOR_EOR__c = 'EOR', Max__c = 100));
       costs.add(new CPA_Costing__c(Cost_Category__c = 'International freight',CPA_v2_0__c = cpav2.Id, Applied_to__c = '# Packages', Cost_Type__c = 'Variable',  Amount__c = null, Rate__c = 10, IOR_EOR__c = 'EOR', Conditional_value__c = '# of packages', Condition__c = '>', Floor__c = 2));
       costs.add(new CPA_Costing__c(Cost_Category__c = 'Miscellaneous',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'CIF value', Cost_Type__c = 'Variable',  Amount__c = null, Rate__c = 10, IOR_EOR__c = 'EOR'));
       costs.add(new CPA_Costing__c(Cost_Category__c = 'Miscellaneous',CPA_v2_0__c = cpav2.Id, Applied_to__c = '# packages', Cost_Type__c = 'Variable', Amount__c = null, Rate__c = 10, IOR_EOR__c = 'EOR'));
       costs.add(new CPA_Costing__c(Cost_Category__c = 'Miscellaneous',CPA_v2_0__c = cpav2.Id, Applied_to__c = '# Parts (line items)', Cost_Type__c = 'Variable', Amount__c = null, Rate__c = 10, IOR_EOR__c = 'EOR'));
       costs.add(new CPA_Costing__c(Cost_Category__c = 'Miscellaneous',CPA_v2_0__c = cpav2.Id, Applied_to__c = '# Unique HS Codes', Cost_Type__c = 'Variable', Amount__c = null, Rate__c = 10, IOR_EOR__c = 'EOR'));
       costs.add(new CPA_Costing__c(Cost_Category__c = 'Miscellaneous',CPA_v2_0__c = cpav2.Id, Applied_to__c = 'Total Taxes', Cost_Type__c = 'Variable', Amount__c = null, Rate__c = 10, IOR_EOR__c = 'EOR'));
       costs.add(new CPA_Costing__c(Cost_Category__c = 'Miscellaneous',CPA_v2_0__c = cpav2.Id, Variable_threshold__c = 10,  Applied_to__c = 'Shipment value', Cost_Type__c = 'Variable',  Amount__c = null, Rate__c = 10, IOR_EOR__c = 'EOR'));
       costs.add(new CPA_Costing__c(Cost_Category__c = 'Miscellaneous',CPA_v2_0__c = cpav2.Id, Variable_threshold__c = 100,  Applied_to__c = 'Chargeable weight', Cost_Type__c = 'Variable',  Amount__c = null, Rate__c = 10, IOR_EOR__c = 'EOR'));
       costs.add(new CPA_Costing__c(Cost_Category__c = 'Miscellaneous',CPA_v2_0__c = cpav2.Id,  Cost_Type__c = 'Fixed',  Amount__c = 100, Rate__c = null, IOR_EOR__c = 'EOR'));
       insert costs;
        
        Shipment_Order__c shipmentOrder = new Shipment_Order__c(Account__c = account.id, Shipment_Value_USD__c =10000, CPA_v2_0__c = cpav2.Id,
        Client_Contact_for_this_Shipment__c = contact.Id, Who_arranges_International_courier__c ='Client', Tax_Treatment__c ='DAP/CIF - IOR pays', Ship_to_Country__c = cpa.Id,
        Destination__c = 'Brazil', Service_Type__c = 'IOR', IOR_Price_List__c = iorpl.Id, 
        CI_Admin_Fee__c  =100, CI_Bank_Fees__c  = 100, CI_Finance_Fee__c  = 100, CI_Insurance_Fee_USD__c  = 100, CI_International_Delivery_Fee__c  = 100, CI_IOR_and_Import_Compliance_Fee_USD__c  = 100, 
        CI_Miscellaneous_Fee__c  = 100, CI_Recharge_Tax_and_Duty__c  = 100, CI_Total_Clearance_Costs__c  = 100, CI_Total_Customs_Brokerage_Cost__c  = 100, CI_Total_Handling_Cost__c  = 100, 
        CI_Total_Licence_Cost__c  = 100, FC_Admin_Fee__c  = 100, FC_Bank_Fees__c  = 100, FC_Finance_Fee__c  = 100, FC_Insurance_Fee_USD__c   = 100, FC_International_Delivery_Fee__c  = 100, 
        FC_IOR_and_Import_Compliance_Fee_USD__c  = 100, FC_Miscellaneous_Fee__c  = 100, FC_Recharge_Tax_and_Duty__c  = 100, FC_Total_Clearance_Costs__c  = 100, 
        FC_Total_Customs_Brokerage_Cost__c  = 100, FC_Total_Handling_Costs__c  = 100, FC_Total_License_Cost__c  = 100, Total_CIF_Duties__c = 0, Total_FOB_Duties__c = 0);
        insert shipmentOrder; 
       
        Shipment_Order__c shipmentOrder2 = new Shipment_Order__c(Account__c = account.id, Shipment_Value_USD__c =10000, CPA_v2_0__c = cpav2.Id,
        Client_Contact_for_this_Shipment__c = contact.Id, Who_arranges_International_courier__c ='Client', Tax_Treatment__c ='DAP/CIF - IOR pays', Ship_to_Country__c = cpa2.Id,
        Destination__c ='France', Service_Type__c = 'EOR', IOR_Price_List__c = iorpl2.Id, of_Unique_Line_Items__c = 10, Total_Taxes__c= 1500, of_Line_Items__c= 10, Chargeable_Weight__c = 200,
        CI_Admin_Fee__c  =100, CI_Bank_Fees__c  = 100, CI_Finance_Fee__c  = 100, CI_Insurance_Fee_USD__c  = 100, CI_International_Delivery_Fee__c  = 100, CI_IOR_and_Import_Compliance_Fee_USD__c  = 100, 
        CI_Miscellaneous_Fee__c  = 100, CI_Recharge_Tax_and_Duty__c  = 100, CI_Total_Clearance_Costs__c  = 100, CI_Total_Customs_Brokerage_Cost__c  = 100, CI_Total_Handling_Cost__c  = 100, 
        CI_Total_Licence_Cost__c  = 100, FC_Admin_Fee__c  = 100, FC_Bank_Fees__c  = 100, FC_Finance_Fee__c  = 100, FC_Insurance_Fee_USD__c   = 100, FC_International_Delivery_Fee__c  = 100, 
        FC_IOR_and_Import_Compliance_Fee_USD__c  = 100, FC_Miscellaneous_Fee__c  = 100, FC_Recharge_Tax_and_Duty__c  = 100, FC_Total_Clearance_Costs__c  = 100, 
        FC_Total_Customs_Brokerage_Cost__c  = 100, FC_Total_Handling_Costs__c  = 100, FC_Total_License_Cost__c  = 100, Total_CIF_Duties__c = 0, Total_FOB_Duties__c = 0);
        insert shipmentOrder2; 
        
        Country__c country = new Country__c (Name = 'Brazil', Country__c = 'Brazil', CIF_Adjustment_Factor__c = 0.05, CIF_Absolute_value_adjustment__c = 250);
        insert country;

        List<Tax_Structure_Per_Country__c> taxStructure = new List<Tax_Structure_Per_Country__c>();

       //Cif and part specific
       taxStructure.add(new Tax_Structure_Per_Country__c(Name= 'Duties', Country__c = country.Id, Order_Number__c = '1' ,Tax_Type__c = 'Duties', Applied_to_Value__c = 'CIF',  Part_Specific__c = TRUE));
       
       //cif, part specif and additional percent
       taxStructure.add(new Tax_Structure_Per_Country__c(Name= 'Duties', Country__c = country.Id, Order_Number__c = '2' ,Tax_Type__c = 'Duties', Applied_to_Value__c = 'CIF',  Part_Specific__c = TRUE, Additional_Percent__c = 0.1));
       
       //cif and has a rate 
       taxStructure.add(new Tax_Structure_Per_Country__c(Name= 'Duties', Country__c = country.Id, Order_Number__c = '3' ,Tax_Type__c = 'Duties', Applied_to_Value__c = 'CIF', Rate__c = 0.15));
       
       //cif and min and max plus rate
       taxStructure.add(new Tax_Structure_Per_Country__c(Name= 'Duties', Country__c = country.Id, Order_Number__c = '4' ,Tax_Type__c = 'Duties', Applied_to_Value__c = 'CIF', Rate__c = 0.15, Min__c = 100, Max__c  = 1000));
       
       // CIF and Amount     
       taxStructure.add(new Tax_Structure_Per_Country__c(Name= 'Duties', Country__c = country.Id, Order_Number__c = '5' , Tax_Type__c = 'Duties', Applied_to_Value__c = 'CIF', Amount__c  = 1000));
       
       // CIF and Additional Percentage       
      taxStructure.add(new Tax_Structure_Per_Country__c(Name= 'Duties', Country__c = country.Id, Order_Number__c = '6' , Tax_Type__c = 'Duties', Applied_to_Value__c = 'CIF', Rate__c = 0.15, Additional_Percent__c = 0.1));
      
      
       //cif and rate with applies to order 
       taxStructure.add(new Tax_Structure_Per_Country__c(Name= 'Duties', Country__c = country.Id, Order_Number__c = '7', Tax_Type__c = 'Duties', Applied_to_Value__c = 'CIF', Rate__c = 0.15, Applies_to_Order__c = '1,3' ));
       
      
      // CIF and Additional Percentage with Applied to Value 
      
       taxStructure.add(new Tax_Structure_Per_Country__c(Name= 'Duties', Country__c = country.Id, Order_Number__c = '8' ,Tax_Type__c = 'Duties', Applied_to_Value__c = 'CIF', Rate__c = 0.15, Additional_Percent__c = 0.1, Applies_to_Order__c = '1,2,3,4,5,7'));
       
       //cif and rate with applies to order 
       taxStructure.add(new Tax_Structure_Per_Country__c(Name= 'Duties', Country__c = country.Id, Order_Number__c = '9' ,Tax_Type__c = 'Duties', Applied_to_Value__c = 'CIF', Rate__c = 0.15, Applies_to_Order__c = '1,2,3,4'));
       
       //Number and Has a Rate and applies to order
       taxStructure.add(new Tax_Structure_Per_Country__c(Name= 'Duties', Country__c = country.Id, Order_Number__c = '10' ,Tax_Type__c = 'Duties', Applied_to_Value__c = '1', Rate__c = 0.15, Applies_to_Order__c = '1,2,3,4,5'));
       
       insert taxStructure; 
       
       Country__c country2 = new Country__c (Name = 'France', Country__c = 'France', CIF_Adjustment_Factor__c = 0.05, CIF_Absolute_value_adjustment__c = 250);
        insert country2;
       
       List<Tax_Structure_Per_Country__c> taxStructure2 = new List<Tax_Structure_Per_Country__c>();
       
       //FOB and part specifc
       taxStructure2.add(new Tax_Structure_Per_Country__c(Name= 'Duties', Country__c = country2.Id, Order_Number__c = '1' ,Tax_Type__c = 'Duties', Applied_to_Value__c = 'FOB',  Part_Specific__c = TRUE));
       
       //FOB, part specific and additional percent
       taxStructure2.add(new Tax_Structure_Per_Country__c(Name= 'Duties', Country__c = country2.Id, Order_Number__c = '2' ,Tax_Type__c = 'Duties', Applied_to_Value__c = 'FOB',  Part_Specific__c = TRUE, Additional_Percent__c = 0.1));
       
       //FOB and has a rate 
       taxStructure2.add(new Tax_Structure_Per_Country__c(Name= 'Duties', Country__c = country2.Id, Order_Number__c = '3' ,Tax_Type__c = 'Duties', Applied_to_Value__c = 'FOB', Rate__c = 0.15));
       
       //FOB and min and max plus rate
       taxStructure2.add(new Tax_Structure_Per_Country__c(Name= 'Duties', Country__c = country2.Id, Order_Number__c = '4' ,Tax_Type__c = 'Duties', Applied_to_Value__c = 'FOB', Rate__c = 0.15, Min__c = 100, Max__c  = 1000));
       
       // FOB and Amount     
       taxStructure2.add(new Tax_Structure_Per_Country__c(Name= 'Duties', Country__c = country2.Id, Order_Number__c = '5' ,Tax_Type__c = 'Duties', Applied_to_Value__c = 'FOB', Amount__c  = 1000));
       
       // FOB and Additional Percentage       
      taxStructure2.add(new Tax_Structure_Per_Country__c(Name= 'Duties', Country__c = country2.Id, Order_Number__c = '6' ,Tax_Type__c = 'Duties', Applied_to_Value__c = 'FOB', Rate__c = 0.15, Additional_Percent__c = 0.1));
      
      
       //FOB and rate with applies to order 
       taxStructure2.add(new Tax_Structure_Per_Country__c(Name= 'Duties', Country__c = country2.Id, Order_Number__c = '7' ,Tax_Type__c = 'Duties', Applied_to_Value__c = 'FOB', Rate__c = 0.15, Applies_to_Order__c = '2,3,4' ));
       
      
      // FOB and Additional Percentage with Applied to Value 
      
       taxStructure2.add(new Tax_Structure_Per_Country__c(Name= 'Duties', Country__c = country2.Id, Order_Number__c = '8' ,Tax_Type__c = 'Duties', Applied_to_Value__c = 'FOB', Rate__c = 0.15, Additional_Percent__c = 0.1, Applies_to_Order__c = '1,2,3,4,6'));
       
       //FOB and rate with applies to order 
       taxStructure2.add(new Tax_Structure_Per_Country__c(Name= 'Duties', Country__c = country2.Id, Order_Number__c = '9' ,Tax_Type__c = 'Duties', Applied_to_Value__c = 'FOB', Rate__c = 0.15, Applies_to_Order__c = '1,2,3,4,5'));
       
       //Number and Has a Rate
       taxStructure2.add(new Tax_Structure_Per_Country__c(Name= 'Duties', Country__c = country2.Id, Order_Number__c = '10' ,Tax_Type__c = 'Duties', Applied_to_Value__c = '1', Rate__c = 0.15));
       

       insert taxStructure2; 

        
      
        List<Id> ids = new List<Id>();
        ids.add(shipmentOrder.id); 
        
        List<Id> ids2 = new List<Id>();
        ids2.add(shipmentOrder2.id);
        
        
        
         
        test.startTest();        
            CalculateTaxesInvockable.getTaxes(ids);
            CalculateTaxesInvockable.getTaxes(ids2);
            deleteCalculatedTaxes.deleteCalculatedTaxes(ids);
       
       
           
        test.stopTest();

     
       
    }

   }