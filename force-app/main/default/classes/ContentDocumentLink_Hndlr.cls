public class ContentDocumentLink_Hndlr {
 public static void ShareDocumentSOshare(List <ContentDocumentLink> contentList)
 {
     //Get the relevant Content links
     List <ContentDocumentLink> ContentToShare = new List <ContentDocumentLink>();
     Map <Id,ContentDocumentLink> contentMap = new Map <Id,ContentDocumentLink>();
     
     if(contentList.size()>0){
         for (ContentDocumentLink currentDoc : contentList){
             String linkedId = currentDoc.LinkedEntityId;
             System.debug(currentDoc.LinkedEntityId);
             if(linkedId.startsWith('a0R')){
                 contentMap.put(currentDoc.LinkedEntityId,currentDoc);
             }
         }
     }
     
     if(contentMap.size()>0){
         List <Opportunity> oppsList = [Select Id, Shipment_Order__c from Opportunity where Shipment_Order__c IN :contentMap.keySet()];
         
         if(oppsList.size()>0){
             for(Opportunity currentOpp : oppsList){
                 ContentDocumentLink newShare = contentMap.get(currentOpp.Shipment_Order__c);
                 newShare = newShare.clone();
                 newShare.LinkedEntityId = currentOpp.Id;
                 ContentToShare.add(newShare);
             }
         }
     }
     
     if(ContentToShare.size()>0){
         insert ContentToShare;
     }
 }
}