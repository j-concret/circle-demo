trigger RegistrationsTrigger on Registrations__c (before insert, before update) {
    
    
    if((Trigger.isInsert || Trigger.isUpdate) && Trigger.isBefore){
        
        RegistrationsTriggerHandler.checkRegisrationsRecord(Trigger.New);
    
    }
    

}