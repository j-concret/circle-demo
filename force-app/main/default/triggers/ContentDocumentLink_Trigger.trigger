trigger ContentDocumentLink_Trigger on ContentDocumentLink (after insert) {
    if(Trigger.isAfter && Trigger.isInsert){
        ContentDocumentLink_Hndlr.ShareDocumentSOshare(Trigger.New);
    }
}