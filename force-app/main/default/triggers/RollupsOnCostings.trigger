trigger RollupsOnCostings on CPA_Costing__c (after insert,After update,before delete) {
      
    IF(Trigger.Isafter && Trigger.IsInsert){
       
   
        
     List<CPA_Costing__c> CPACostings1 = new List<CPA_Costing__c>();
      decimal FCAdminFee = 0.0;         decimal FCAdminFee2= 0.0;
     decimal FCBankFee = 0.0;           decimal FCBankFee2 = 0.0;
     decimal FCFinanceFee = 0.0;         decimal FCFinanceFee2 = 0.0;
     decimal FCIORandImport = 0.0;         decimal FCIORandImport2 = 0.0;
     decimal FCINsurance = 0.0;         decimal FCINsurance2 = 0.0;
     decimal FCInternationsldeliveryFee = 0.0;  decimal FCInternationsldeliveryFee2 = 0.0;
     decimal FCMiselaneousFee = 0.0;        decimal FCMiselaneousFee2 = 0.0;
     decimal FCRechatgeTaxandDuties = 0.0;    decimal FCRechatgeTaxandDuties2 = 0.0;
     decimal FCTotClearence = 0.0;        decimal FCTotClearence2 = 0.0;
     decimal FCTotCustomsBrokerage = 0.0;      decimal FCTotCustomsBrokerage2 = 0.0;
     decimal FCTotHandlingCost = 0.0;       decimal FCTotHandlingCost2 = 0.0;
     decimal FCTotLicenseCost = 0.0;        decimal FCTotLicenseCost2 = 0.0;
        decimal FCTAXOther = 0.0;        decimal FCTAXOther2 = 0.0;
        decimal FCTAXForex = 0.0;        decimal FCTAXForex2 = 0.0;
        
     decimal ACAdminFee = 0.0;          decimal ACAdminFee2 = 0.0;
     decimal ACBankFee = 0.0;          decimal ACBankFee2 = 0.0;
     decimal ACFinanceFee = 0.0;         decimal ACFinanceFee2 = 0.0;
     decimal ACIORandImport = 0.0;        decimal ACIORandImport2 = 0.0;
     decimal ACINsurance = 0.0;         decimal ACINsurance2 = 0.0;
     decimal ACInternationsldeliveryFee = 0.0;   decimal ACInternationsldeliveryFee2 = 0.0;
     decimal ACMiselaneousFee = 0.0;       decimal ACMiselaneousFee2 = 0.0;
     decimal ACRechatgeTaxandDuties = 0.0;    decimal ACRechatgeTaxandDuties2 = 0.0;
     decimal ACTotClearence = 0.0;        decimal ACTotClearence2 = 0.0;
     decimal ACTotCustomsBrokerage = 0.0;      decimal ACTotCustomsBrokerage2 = 0.0;
     decimal ACTotHandlingCost = 0.0;       decimal ACTotHandlingCost2 = 0.0;
     decimal ACTotLicenseCost = 0.0;        decimal ACTotLicenseCost2 = 0.0;
        decimal ACTAXOther = 0.0;        decimal ACTAXOther2 = 0.0;
        decimal ACTAXForex = 0.0;        decimal ACTAXForex2 = 0.0;
        
        
     String SOIDs = null;
     String SupplierInvoice = null;
        
        // Forecasting fields update on shipmentOrder
           for(CPA_Costing__c CPAC : trigger.new) {
                  if(CPAC.Manual_Costing_Created__c ==TRUE || CPAC.trigger__c ==TRUE){
            
                      
                        List<CPA_Costing__c> CPACostings = [Select Cost_Category__C,Tax_Sub_Category__c,Invoice_amount_USD_New__c,Amount_in_USD__c,Shipment_Order_Old__c,Supplier_Invoice__c from CPA_Costing__c where Shipment_Order_Old__c =: CPAC.Shipment_Order_Old__c];
                        For(Integer i=0;CPACostings.size()>i;i++ ){
                       CPACostings1.add(CPACostings[i]);
                             SOIDs = CPAC.Shipment_Order_Old__c;  
                             SupplierInvoice = CPAC.Supplier_Invoice__c;
                                
                         if(CPACostings[i].Cost_Category__C=='Admin')    { FCAdminFee = FCAdminFee+CPACostings[i].Amount_in_USD__c; ACAdminFee = ACAdminFee+CPACostings[i].Invoice_amount_USD_New__c;}
                     else if(CPACostings[i].Cost_Category__C=='Bank')    { FCBankFee = FCBankFee+CPACostings[i].Amount_in_USD__c; ACBankFee = ACBankFee+CPACostings[i].Invoice_amount_USD_New__c;}
                     else if(CPACostings[i].Cost_Category__C=='Finance')  { FCFinanceFee = FCFinanceFee+CPACostings[i].Amount_in_USD__c; ACFinanceFee = ACFinanceFee+CPACostings[i].Invoice_amount_USD_New__c;}
                     else if(CPACostings[i].Cost_Category__C=='IOR Cost')    { FCIORandImport = FCIORandImport+CPACostings[i].Amount_in_USD__c; ACIORandImport = ACIORandImport+CPACostings[i].Invoice_amount_USD_New__c;}
                     else if(CPACostings[i].Cost_Category__C=='Insurance')  { FCINsurance = FCINsurance+CPACostings[i].Amount_in_USD__c; ACINsurance = ACINsurance+CPACostings[i].Invoice_amount_USD_New__c;}
                     else if(CPACostings[i].Cost_Category__C=='International freight')    { FCInternationsldeliveryFee = FCInternationsldeliveryFee+CPACostings[i].Amount_in_USD__c; ACInternationsldeliveryFee = ACInternationsldeliveryFee+CPACostings[i].Invoice_amount_USD_New__c;}
                     else if(CPACostings[i].Cost_Category__C=='Miscellaneous')    { FCMiselaneousFee = FCMiselaneousFee+CPACostings[i].Amount_in_USD__c;  ACMiselaneousFee = ACMiselaneousFee+CPACostings[i].Invoice_amount_USD_New__c;}
                     else if(CPACostings[i].Cost_Category__C=='Tax')    { FCRechatgeTaxandDuties = FCRechatgeTaxandDuties+CPACostings[i].Amount_in_USD__c;  }
                     else if(CPACostings[i].Cost_Category__C=='Clearance')    { FCTotClearence = FCTotClearence+CPACostings[i].Amount_in_USD__c; ACTotClearence = ACTotClearence+CPACostings[i].Invoice_amount_USD_New__c;}
                     else if(CPACostings[i].Cost_Category__C=='Brokerage')    { FCTotCustomsBrokerage = FCTotCustomsBrokerage+CPACostings[i].Amount_in_USD__c; ACTotCustomsBrokerage = ACTotCustomsBrokerage+CPACostings[i].Invoice_amount_USD_New__c;}
                     else if(CPACostings[i].Cost_Category__C=='Handling')    { FCTotHandlingCost = FCTotHandlingCost+CPACostings[i].Amount_in_USD__c; ACTotHandlingCost = ACTotHandlingCost+CPACostings[i].Invoice_amount_USD_New__c;}
                     else if(CPACostings[i].Cost_Category__C=='Licenses')    { FCTotLicenseCost = FCTotLicenseCost+CPACostings[i].Amount_in_USD__c; ACTotLicenseCost = ACTotLicenseCost+CPACostings[i].Invoice_amount_USD_New__c;}
                       
                            
                        if(CPACostings[i].Cost_Category__C=='Tax' && (CPACostings[i].Tax_Sub_Category__c !='Duties and Taxes Other' || CPACostings[i].Tax_Sub_Category__c !='Tax Forex Spread'))    {  ACRechatgeTaxandDuties = ACRechatgeTaxandDuties+CPACostings[i].Invoice_amount_USD_New__c;}
                        if(CPACostings[i].Cost_Category__C=='Tax' && CPACostings[i].Tax_Sub_Category__c=='Duties and Taxes Other' )    { FCTAXOther = FCTAXOther+CPACostings[i].Amount_in_USD__c;  ACTAXOther = ACTAXOther+CPACostings[i].Invoice_amount_USD_New__c;}                
                        if(CPACostings[i].Cost_Category__C=='Tax' && CPACostings[i].Tax_Sub_Category__c=='Tax Forex Spread' )    { FCTAXForex = FCTAXForex+CPACostings[i].Amount_in_USD__c;  ACTAXForex = ACTAXForex+CPACostings[i].Invoice_amount_USD_New__c;}
                                              }
                        
                        } 
            
            }
   
       
   for(CPA_Costing__c CPAC : trigger.new) {
      if(CPAC.Manual_Costing_Created__c ==TRUE || CPAC.trigger__c ==TRUE){
      //  Shipment_Order__C SO = [Select Actual_Admin_Fee__c,Actual_Bank_Fees__c,Actual_Total_Customs_Brokerage_Cost__c,Actual_Total_Clearance_Costs__c,Actual_Total_Tax_and_Duty__c,Actual_Finance_Fee__c,Actual_Total_Handling_Costs__c,Actual_Insurance_Fee_USD__c,Actual_Total_IOR_EOR__c,Actual_International_Delivery_Fee__c,Actual_Miscellaneous_Fee__c,Actual_Total_License_Cost__c,Actual_Duties_and_Taxes_Other__c,Actual_Exchange_gain_loss_on_payment__c,Actual_Taxes_Forex_Spread__c,FC_Admin_Fee__c,FC_Bank_Fees__c,FC_Finance_Fee__c,FC_IOR_and_Import_Compliance_Fee_USD__c,FC_Insurance_Fee_USD__c,FC_International_Delivery_Fee__c,FC_Miscellaneous_Fee__c,FC_Recharge_Tax_and_Duty__c,FC_Total_Clearance_Costs__c,FC_Total_Customs_Brokerage_Cost__c,FC_Total_Handling_Costs__c,FC_Total_License_Cost__c from Shipment_Order__C where Id =:SOIDs limit 1];
          
            
      Shipment_Order__C SO = [Select Actual_Admin_Fee__c,Actual_Bank_Fees__c,Actual_Total_Customs_Brokerage_Cost__c,Actual_Total_Clearance_Costs__c,Actual_Total_Tax_and_Duty__c,Actual_Finance_Fee__c,Actual_Total_Handling_Costs__c,Actual_Insurance_Fee_USD__c,Actual_Total_IOR_EOR__c,Actual_International_Delivery_Fee__c,Actual_Miscellaneous_Fee__c,Actual_Total_License_Cost__c,Actual_Duties_and_Taxes_Other__c,Actual_Exchange_gain_loss_on_payment__c,Actual_Taxes_Forex_Spread__c,FC_Admin_Fee__c,FC_Bank_Fees__c,FC_Finance_Fee__c,FC_IOR_and_Import_Compliance_Fee_USD__c,FC_Insurance_Fee_USD__c,FC_International_Delivery_Fee__c,FC_Miscellaneous_Fee__c,FC_Recharge_Tax_and_Duty__c,FC_Total_Clearance_Costs__c,FC_Total_Customs_Brokerage_Cost__c,FC_Total_Handling_Costs__c,FC_Total_License_Cost__c from Shipment_Order__C where Id =: CPAC.Shipment_order_Old__C limit 1];
          
        if(SO.id !=Null){
           SO.FC_Admin_Fee__c=FCAdminFee;
           SO.FC_Bank_Fees__c=FCBankFee;
           SO.FC_Finance_Fee__c=FCFinanceFee;
           SO.FC_IOR_and_Import_Compliance_Fee_USD__c=FCIORandImport;
           SO.FC_Insurance_Fee_USD__c=FCINsurance;
           SO.FC_International_Delivery_Fee__c=FCInternationsldeliveryFee;
           SO.FC_Miscellaneous_Fee__c=FCMiselaneousFee;
           SO.FC_Recharge_Tax_and_Duty__c=FCRechatgeTaxandDuties;
           SO.FC_Total_Clearance_Costs__c=FCTotClearence;
           SO.FC_Total_Customs_Brokerage_Cost__c=FCTotCustomsBrokerage;
           SO.FC_Total_Handling_Costs__c=FCTotHandlingCost;
           SO.FC_Total_License_Cost__c=FCTotLicenseCost;
                       
            
           SO.Actual_Admin_Fee__c=ACAdminFee;
           SO.Actual_Bank_Fees__c=ACBankFee;
           SO.Actual_Total_Customs_Brokerage_Cost__c=ACTotCustomsBrokerage;
           SO.Actual_Total_Clearance_Costs__c=ACTotClearence;
           SO.Actual_Total_Tax_and_Duty__c=ACRechatgeTaxandDuties;
           SO.Actual_Finance_Fee__c=ACFinanceFee;
           SO.Actual_Total_Handling_Costs__c=ACTotHandlingCost;
           SO.Actual_Insurance_Fee_USD__c=ACINsurance;
           SO.Actual_Total_IOR_EOR__c=ACIORandImport;
           SO.Actual_International_Delivery_Fee__c=ACInternationsldeliveryFee;
           SO.Actual_Miscellaneous_Fee__c=ACMiselaneousFee;
           SO.Actual_Total_License_Cost__c=ACTotLicenseCost;
                SO.Actual_Duties_and_Taxes_Other__c=ACTAXOther;
              SO.Actual_Taxes_Forex_Spread__c=ACTAXForex;
            
            
           
           Update SO;
            
        }    
    
       }
     }
        
        
         // Forecasting fields update on supplier invoice
           for(CPA_Costing__c CPAC : trigger.new) {
                  if(CPAC.Manual_Costing_Created__c ==FALSE ||CPAC.Supplier_Invoice__c !=NULL){
            
                      
                        List<CPA_Costing__c> CPACostings = [Select Cost_Category__C,Tax_Sub_Category__c,Invoice_amount_USD_New__c,Amount_in_USD__c,Shipment_Order_Old__c,Supplier_Invoice__c from CPA_Costing__c where Shipment_Order_Old__c =: CPAC.Shipment_Order_Old__c and Supplier_Invoice__c =:CPAC.Supplier_Invoice__c and id =:CPAC.Id];
                        For(Integer i=0;CPACostings.size()>i;i++ ){
                       CPACostings1.add(CPACostings[i]);
                             SOIDs = CPAC.Shipment_Order_Old__c;  
                             SupplierInvoice = CPAC.Supplier_Invoice__c;
                                
                         if(CPACostings[i].Cost_Category__C=='Admin')    { FCAdminFee2 = FCAdminFee2+CPACostings[i].Amount_in_USD__c; ACAdminFee2 = ACAdminFee2+CPACostings[i].Invoice_amount_USD_New__c;}
                     else if(CPACostings[i].Cost_Category__C=='Bank')    { FCBankFee2 = FCBankFee2+CPACostings[i].Amount_in_USD__c; ACBankFee2 = ACBankFee2+CPACostings[i].Invoice_amount_USD_New__c;}
                     else if(CPACostings[i].Cost_Category__C=='Finance')  { FCFinanceFee2 = FCFinanceFee2+CPACostings[i].Amount_in_USD__c; ACFinanceFee2 = ACFinanceFee2+CPACostings[i].Invoice_amount_USD_New__c;}
                     else if(CPACostings[i].Cost_Category__C=='IOR Cost')    { FCIORandImport2 = FCIORandImport2+CPACostings[i].Amount_in_USD__c; ACIORandImport2 = ACIORandImport2+CPACostings[i].Invoice_amount_USD_New__c;}
                     else if(CPACostings[i].Cost_Category__C=='Insurance')  { FCINsurance2 = FCINsurance2+CPACostings[i].Amount_in_USD__c; ACINsurance2 = ACINsurance2+CPACostings[i].Invoice_amount_USD_New__c;}
                     else if(CPACostings[i].Cost_Category__C=='International freight')    { FCInternationsldeliveryFee2 = FCInternationsldeliveryFee2+CPACostings[i].Amount_in_USD__c; ACInternationsldeliveryFee2 = ACInternationsldeliveryFee2+CPACostings[i].Invoice_amount_USD_New__c;}
                     else if(CPACostings[i].Cost_Category__C=='Miscellaneous')    { FCMiselaneousFee2 = FCMiselaneousFee2+CPACostings[i].Amount_in_USD__c;  ACMiselaneousFee2 = ACMiselaneousFee2+CPACostings[i].Invoice_amount_USD_New__c;}
                    // else if(CPACostings[i].Cost_Category__C=='Tax')    { FCRechatgeTaxandDuties2 = FCRechatgeTaxandDuties2+CPACostings[i].Amount_in_USD__c; }
                     else if(CPACostings[i].Cost_Category__C=='Clearance')    { FCTotClearence2 = FCTotClearence2+CPACostings[i].Amount_in_USD__c; ACTotClearence2 = ACTotClearence2+CPACostings[i].Invoice_amount_USD_New__c;}
                     else if(CPACostings[i].Cost_Category__C=='Brokerage')    { FCTotCustomsBrokerage2 = FCTotCustomsBrokerage2+CPACostings[i].Amount_in_USD__c; ACTotCustomsBrokerage2 = ACTotCustomsBrokerage2+CPACostings[i].Invoice_amount_USD_New__c;}
                     else if(CPACostings[i].Cost_Category__C=='Handling')    { FCTotHandlingCost2 = FCTotHandlingCost2+CPACostings[i].Amount_in_USD__c; ACTotHandlingCost2 = ACTotHandlingCost2+CPACostings[i].Invoice_amount_USD_New__c;}
                     else if(CPACostings[i].Cost_Category__C=='Licenses')    { FCTotLicenseCost2 = FCTotLicenseCost2+CPACostings[i].Amount_in_USD__c; ACTotLicenseCost2 = ACTotLicenseCost2+CPACostings[i].Invoice_amount_USD_New__c;}
                       
                            
                        if(CPACostings[i].Cost_Category__C=='Tax' && (CPACostings[i].Tax_Sub_Category__c !='Duties and Taxes Other' || CPACostings[i].Tax_Sub_Category__c !='Tax Forex Spread'))    { FCRechatgeTaxandDuties2 = FCRechatgeTaxandDuties2+CPACostings[i].Amount_in_USD__c; ACRechatgeTaxandDuties2 = ACRechatgeTaxandDuties2+CPACostings[i].Invoice_amount_USD_New__c;}
                        if(CPACostings[i].Cost_Category__C=='Tax' && CPACostings[i].Tax_Sub_Category__c=='Duties and Taxes Other' )    { FCTAXOther2 = FCTAXOther2+CPACostings[i].Amount_in_USD__c;  ACTAXOther2 = ACTAXOther2+CPACostings[i].Invoice_amount_USD_New__c;}                
                        if(CPACostings[i].Cost_Category__C=='Tax' && CPACostings[i].Tax_Sub_Category__c=='Tax Forex Spread' )    { FCTAXForex2 = FCTAXForex2+CPACostings[i].Amount_in_USD__c;  ACTAXForex2 = ACTAXForex2+CPACostings[i].Invoice_amount_USD_New__c;}
                                              }
                        
                        } 
            
            }
   for(CPA_Costing__c CPAC : trigger.new) {
       if(CPAC.Manual_Costing_Created__c ==FALSE || CPAC.Supplier_Invoice__c !=NULL){
         if(SupplierInvoice !=Null){
               //   Supplier_Invoice__C SIS =[select id, Actual_Duties_and_Taxes_Other__c,Actual_Tax_Forex_Spread__c,FC_Admin_Fee__c,FC_Bank_Fees__c,FC_Finance_Fee__c,FC_Insurance_Fee_USD__c,FC_Miscellaneous_Fee__c,FC_International_Delivery_Fee__c,Total_Duties_and_Taxes__c,Total_Brokerage_Costs__c,Total_Clearance_Costs__c,Total_Handling_Costs__c,Total_License_Permits_Cost__c,Total_IOR_EOR__c,Actual_Admin_Fee__c,Actual_Bank_Fees__c,Actual_Finance_Fee__c,Actual_Insurance_Fee_USD__c,Actual_Miscellaneous_Fee__c,Actual_International_Delivery_Fee__c,Actual_Duties_and_Taxes__c,Actual_Brokerage_Costs__c,Actual_Clearance_Costs__c,Actual_Handling_Costs__c,Actual_License_Permits_Cost__c,Actual_IOR_EOR_Cost__c  from supplier_Invoice__C where id=:SupplierInvoice];
       Supplier_Invoice__C SIS =[select id, Actual_Duties_and_Taxes_Other__c,Actual_Tax_Forex_Spread__c,FC_Admin_Fee__c,FC_Bank_Fees__c,FC_Finance_Fee__c,FC_Insurance_Fee_USD__c,FC_Miscellaneous_Fee__c,FC_International_Delivery_Fee__c,Total_Duties_and_Taxes__c,Total_Brokerage_Costs__c,Total_Clearance_Costs__c,Total_Handling_Costs__c,Total_License_Permits_Cost__c,Total_IOR_EOR__c,Actual_Admin_Fee__c,Actual_Bank_Fees__c,Actual_Finance_Fee__c,Actual_Insurance_Fee_USD__c,Actual_Miscellaneous_Fee__c,Actual_International_Delivery_Fee__c,Actual_Duties_and_Taxes__c,Actual_Brokerage_Costs__c,Actual_Clearance_Costs__c,Actual_Handling_Costs__c,Actual_License_Permits_Cost__c,Actual_IOR_EOR_Cost__c  from supplier_Invoice__C where id=:CPAC.supplier_invoice__C  ];
      
                 
                 
           SIS.FC_Admin_Fee__c=FCAdminFee2;
           SIS.FC_Bank_Fees__c=FCBankFee2;
           SIS.FC_Finance_Fee__c=FCFinanceFee2;
           SIS.Total_IOR_EOR__c=FCIORandImport2;
           SIS.FC_Insurance_Fee_USD__c=FCINsurance2;
           SIS.FC_International_Delivery_Fee__c=FCInternationsldeliveryFee2;
           SIS.FC_Miscellaneous_Fee__c=FCMiselaneousFee2;
           SIS.Total_Duties_and_Taxes__c=FCRechatgeTaxandDuties2;
           SIS.Total_Clearance_Costs__c=FCTotClearence2;
           SIS.Total_Brokerage_Costs__c=FCTotCustomsBrokerage2;
           SIS.Total_Handling_Costs__c=FCTotHandlingCost2;
           SIS.Total_License_Permits_Cost__c=FCTotLicenseCost2;
                   SIS.FC_Duties_and_Taxes_Other__c=FCTAXOther2;
                SIS.FC_Tax_Forex_Spread__c=FCTAXForex2;
            
            
            
           SIS.Actual_Admin_Fee__c=ACAdminFee2;
           SIS.Actual_Bank_Fees__c=ACBankFee2;
           SIS.Actual_Brokerage_Costs__c=ACTotCustomsBrokerage2;
           SIS.Actual_Clearance_Costs__c=ACTotClearence2;
           SIS.Actual_Duties_and_Taxes__c=ACRechatgeTaxandDuties2;
           SIS.Actual_Finance_Fee__c=ACFinanceFee2;
           SIS.Actual_Handling_Costs__c=ACTotHandlingCost2;
           SIS.Actual_Insurance_Fee_USD__c=ACINsurance2;
           SIS.Actual_IOR_EOR_Cost__c=ACIORandImport2;
           SIS.Actual_International_Delivery_Fee__c=ACInternationsldeliveryFee2;
           SIS.Actual_Miscellaneous_Fee__c=ACMiselaneousFee2;
           SIS.Actual_License_Permits_Cost__c=ACTotLicenseCost2;
                 SIS.Actual_Duties_and_Taxes_Other__c=ACTAXOther2;
                SIS.Actual_Tax_Forex_Spread__c=ACTAXForex2;
            
            

           Update SIS;
           
        }   
    
       }
     }

    }

        
    IF(Trigger.IsAfter && Trigger.IsUpdate){
        
    
        
         List<CPA_Costing__c> CPACostings1 = new List<CPA_Costing__c>();
         List<CPA_Costing__c> CPACostings2 = new List<CPA_Costing__c>();
     decimal FCAdminFee = 0.0;           decimal FCAdminFee2= 0.0;
     decimal FCBankFee = 0.0;           decimal FCBankFee2 = 0.0;
     decimal FCFinanceFee = 0.0;         decimal FCFinanceFee2 = 0.0;
     decimal FCIORandImport = 0.0;         decimal FCIORandImport2 = 0.0;
     decimal FCINsurance = 0.0;         decimal FCINsurance2 = 0.0;
     decimal FCInternationsldeliveryFee = 0.0;  decimal FCInternationsldeliveryFee2 = 0.0;
     decimal FCMiselaneousFee = 0.0;        decimal FCMiselaneousFee2 = 0.0;
     decimal FCRechatgeTaxandDuties = 0.0;    decimal FCRechatgeTaxandDuties2 = 0.0;
     decimal FCTotClearence = 0.0;        decimal FCTotClearence2 = 0.0;
     decimal FCTotCustomsBrokerage = 0.0;      decimal FCTotCustomsBrokerage2 = 0.0;
     decimal FCTotHandlingCost = 0.0;       decimal FCTotHandlingCost2 = 0.0;
     decimal FCTotLicenseCost = 0.0;        decimal FCTotLicenseCost2 = 0.0;
        decimal FCTAXOther = 0.0;           decimal FCTAXOther2 = 0.0;
        decimal FCTAXForex = 0.0;          decimal FCTAXForex2 = 0.0;
        
     decimal ACAdminFee = 0.0;          decimal ACAdminFee2 = 0.0;
     decimal ACBankFee = 0.0;          decimal ACBankFee2 = 0.0;
     decimal ACFinanceFee = 0.0;         decimal ACFinanceFee2 = 0.0;
     decimal ACIORandImport = 0.0;        decimal ACIORandImport2 = 0.0;
     decimal ACINsurance = 0.0;         decimal ACINsurance2 = 0.0;
     decimal ACInternationsldeliveryFee = 0.0;   decimal ACInternationsldeliveryFee2 = 0.0;
     decimal ACMiselaneousFee = 0.0;       decimal ACMiselaneousFee2 = 0.0;
     decimal ACRechatgeTaxandDuties = 0.0;    decimal ACRechatgeTaxandDuties2 = 0.0;
     decimal ACTotClearence = 0.0;        decimal ACTotClearence2 = 0.0;
     decimal ACTotCustomsBrokerage = 0.0;      decimal ACTotCustomsBrokerage2 = 0.0;
     decimal ACTotHandlingCost = 0.0;       decimal ACTotHandlingCost2 = 0.0;
     decimal ACTotLicenseCost = 0.0;        decimal ACTotLicenseCost2 = 0.0;
        decimal ACTAXOther = 0.0;           decimal ACTAXOther2 = 0.0;
        decimal ACTAXForex = 0.0;           decimal ACTAXForex2 = 0.0;
        
        
     String SOIDs = null;
     String SupplierInvoice = null;
        
        // Forecasting fields update on shipmentOrder
           for(CPA_Costing__c CPAC : trigger.new) {
             
             Map<Id,Schema.RecordTypeInfo> rtMap = CPA_Costing__c.sobjectType.getDescribe().getRecordTypeInfosById();  
             Id RecordTypeIdCosting = rtMap.get(CPAC.RecordTypeId).getRecordTypeId();
             if(RecordTypeIdCosting =='0120Y000000ytNmQAI'){
                  
               
             //  If(CPAC.Invoice_amount_USD_New__c != trigger.oldMap.get(CPAC.ID).Invoice_amount_USD_New__c  || CPAC.Amount_in_USD__c != trigger.oldMap.get(CPAC.ID).Amount_in_USD__c || CPAC.Trigger__c != trigger.oldMap.get(CPAC.ID).Trigger__c){
                  If((CPAC.supplier_invoice__c == null && (CPAC.Invoice_amount_USD_New__c != trigger.oldMap.get(CPAC.ID).Invoice_amount_USD_New__c  || CPAC.Amount_in_USD__c != trigger.oldMap.get(CPAC.ID).Amount_in_USD__c)) ||   (CPAC.supplier_invoice__c != null && CPAC.trigger__c == true && (CPAC.Invoice_amount_USD_New__c != trigger.oldMap.get(CPAC.ID).Invoice_amount_USD_New__c  || CPAC.Amount_in_USD__c != trigger.oldMap.get(CPAC.ID).Amount_in_USD__c  || CPAC.Trigger__c != trigger.oldMap.get(CPAC.ID).Trigger__c ))){  
                      
                        List<CPA_Costing__c> CPACostings = [Select Cost_Category__C,Tax_Sub_Category__c,Invoice_amount_USD_New__c,Amount_in_USD__c,Shipment_Order_Old__c,Supplier_Invoice__c from CPA_Costing__c where Shipment_Order_Old__c =: CPAC.Shipment_Order_Old__c  ];
                        For(Integer i=0;CPACostings.size()>i;i++ ){
                       CPACostings1.add(CPACostings[i]);
                             SOIDs = CPAC.Shipment_Order_Old__c;  
                             SupplierInvoice = CPAC.Supplier_Invoice__c;
                                
                         if(CPACostings[i].Cost_Category__C=='Admin')    { FCAdminFee = FCAdminFee+CPACostings[i].Amount_in_USD__c; ACAdminFee = ACAdminFee+CPACostings[i].Invoice_amount_USD_New__c;}
                     else if(CPACostings[i].Cost_Category__C=='Bank')    { FCBankFee = FCBankFee+CPACostings[i].Amount_in_USD__c; ACBankFee = ACBankFee+CPACostings[i].Invoice_amount_USD_New__c;}
                     else if(CPACostings[i].Cost_Category__C=='Finance')  { FCFinanceFee = FCFinanceFee+CPACostings[i].Amount_in_USD__c; ACFinanceFee = ACFinanceFee+CPACostings[i].Invoice_amount_USD_New__c;}
                     else if(CPACostings[i].Cost_Category__C=='IOR Cost')    { FCIORandImport = FCIORandImport+CPACostings[i].Amount_in_USD__c; ACIORandImport = ACIORandImport+CPACostings[i].Invoice_amount_USD_New__c;}
                     else if(CPACostings[i].Cost_Category__C=='Insurance')  { FCINsurance = FCINsurance+CPACostings[i].Amount_in_USD__c; ACINsurance = ACINsurance+CPACostings[i].Invoice_amount_USD_New__c;}
                     else if(CPACostings[i].Cost_Category__C=='International freight')    { FCInternationsldeliveryFee = FCInternationsldeliveryFee+CPACostings[i].Amount_in_USD__c; ACInternationsldeliveryFee = ACInternationsldeliveryFee+CPACostings[i].Invoice_amount_USD_New__c;}
                     else if(CPACostings[i].Cost_Category__C=='Miscellaneous')    { FCMiselaneousFee = FCMiselaneousFee+CPACostings[i].Amount_in_USD__c;  ACMiselaneousFee = ACMiselaneousFee+CPACostings[i].Invoice_amount_USD_New__c;}
                     else if(CPACostings[i].Cost_Category__C=='Tax')    { FCRechatgeTaxandDuties = FCRechatgeTaxandDuties+CPACostings[i].Amount_in_USD__c;  } 
                     else if(CPACostings[i].Cost_Category__C=='Clearance')    { FCTotClearence = FCTotClearence+CPACostings[i].Amount_in_USD__c; ACTotClearence = ACTotClearence+CPACostings[i].Invoice_amount_USD_New__c;}
                     else if(CPACostings[i].Cost_Category__C=='Brokerage')    { FCTotCustomsBrokerage = FCTotCustomsBrokerage+CPACostings[i].Amount_in_USD__c; ACTotCustomsBrokerage = ACTotCustomsBrokerage+CPACostings[i].Invoice_amount_USD_New__c;}
                     else if(CPACostings[i].Cost_Category__C=='Handling')    { FCTotHandlingCost = FCTotHandlingCost+CPACostings[i].Amount_in_USD__c; ACTotHandlingCost = ACTotHandlingCost+CPACostings[i].Invoice_amount_USD_New__c;}
                     else if(CPACostings[i].Cost_Category__C=='Licenses')    { FCTotLicenseCost = FCTotLicenseCost+CPACostings[i].Amount_in_USD__c; ACTotLicenseCost = ACTotLicenseCost+CPACostings[i].Invoice_amount_USD_New__c;}
                        
                         if(CPACostings[i].Cost_Category__C=='Tax' && (CPACostings[i].Tax_Sub_Category__c !='Duties and Taxes Other' || CPACostings[i].Tax_Sub_Category__c !='Tax Forex Spread'))    {  ACRechatgeTaxandDuties = ACRechatgeTaxandDuties+CPACostings[i].Invoice_amount_USD_New__c;}
                   if(CPACostings[i].Cost_Category__C=='Tax' && CPACostings[i].Tax_Sub_Category__c=='Duties and Taxes Other' )    { FCTAXOther = FCTAXOther+CPACostings[i].Amount_in_USD__c;  ACTAXOther = ACTAXOther+CPACostings[i].Invoice_amount_USD_New__c;}                
                        if(CPACostings[i].Cost_Category__C=='Tax' && CPACostings[i].Tax_Sub_Category__c=='Tax Forex Spread' )    { FCTAXForex = FCTAXForex+CPACostings[i].Amount_in_USD__c;  ACTAXForex = ACTAXForex+CPACostings[i].Invoice_amount_USD_New__c;}
                                              }
                        
                        } 
           }
            }
  for(CPA_Costing__c CPAC : trigger.new) {
      
                Map<Id,Schema.RecordTypeInfo> rtMap = CPA_Costing__c.sobjectType.getDescribe().getRecordTypeInfosById();  
             Id RecordTypeIdCosting = rtMap.get(CPAC.RecordTypeId).getRecordTypeId();
             if(RecordTypeIdCosting =='0120Y000000ytNmQAI'){
      //if(CPAC.recordType.Name =='Transaction'){
    
        // If(CPAC.Invoice_amount_USD_New__c != trigger.oldMap.get(CPAC.ID).Invoice_amount_USD_New__c  || CPAC.Amount_in_USD__c != trigger.oldMap.get(CPAC.ID).Amount_in_USD__c || CPAC.Trigger__c != trigger.oldMap.get(CPAC.ID).Trigger__c ){
    If((CPAC.supplier_invoice__c == null && (CPAC.Invoice_amount_USD_New__c != trigger.oldMap.get(CPAC.ID).Invoice_amount_USD_New__c  || CPAC.Amount_in_USD__c != trigger.oldMap.get(CPAC.ID).Amount_in_USD__c)) ||   (CPAC.supplier_invoice__c != null && CPAC.trigger__c == true && (CPAC.Invoice_amount_USD_New__c != trigger.oldMap.get(CPAC.ID).Invoice_amount_USD_New__c  || CPAC.Amount_in_USD__c != trigger.oldMap.get(CPAC.ID).Amount_in_USD__c || CPAC.Trigger__c != trigger.oldMap.get(CPAC.ID).Trigger__c ))){  
      Shipment_Order__C SO = [Select Actual_Admin_Fee__c,Actual_Bank_Fees__c,Actual_Total_Customs_Brokerage_Cost__c,Actual_Total_Clearance_Costs__c,Actual_Total_Tax_and_Duty__c,Actual_Finance_Fee__c,Actual_Total_Handling_Costs__c,Actual_Insurance_Fee_USD__c,Actual_Total_IOR_EOR__c,Actual_International_Delivery_Fee__c,Actual_Miscellaneous_Fee__c,Actual_Total_License_Cost__c,Actual_Duties_and_Taxes_Other__c,Actual_Exchange_gain_loss_on_payment__c,Actual_Taxes_Forex_Spread__c,FC_Admin_Fee__c,FC_Bank_Fees__c,FC_Finance_Fee__c,FC_IOR_and_Import_Compliance_Fee_USD__c,FC_Insurance_Fee_USD__c,FC_International_Delivery_Fee__c,FC_Miscellaneous_Fee__c,FC_Recharge_Tax_and_Duty__c,FC_Total_Clearance_Costs__c,FC_Total_Customs_Brokerage_Cost__c,FC_Total_Handling_Costs__c,FC_Total_License_Cost__c from Shipment_Order__C where Id =:SOIDs limit 1];
           
           
        if(SO.id !=Null){
           SO.FC_Admin_Fee__c=FCAdminFee;
           SO.FC_Bank_Fees__c=FCBankFee;
           SO.FC_Finance_Fee__c=FCFinanceFee;
           SO.FC_IOR_and_Import_Compliance_Fee_USD__c=FCIORandImport;
           SO.FC_Insurance_Fee_USD__c=FCINsurance;
           SO.FC_International_Delivery_Fee__c=FCInternationsldeliveryFee;
           SO.FC_Miscellaneous_Fee__c=FCMiselaneousFee;
           SO.FC_Recharge_Tax_and_Duty__c=FCRechatgeTaxandDuties;
           SO.FC_Total_Clearance_Costs__c=FCTotClearence;
           SO.FC_Total_Customs_Brokerage_Cost__c=FCTotCustomsBrokerage;
           SO.FC_Total_Handling_Costs__c=FCTotHandlingCost;
           SO.FC_Total_License_Cost__c=FCTotLicenseCost;
                       
           
           SO.Actual_Admin_Fee__c=ACAdminFee;
           SO.Actual_Bank_Fees__c=ACBankFee;
           SO.Actual_Total_Customs_Brokerage_Cost__c=ACTotCustomsBrokerage;
           SO.Actual_Total_Clearance_Costs__c=ACTotClearence;
           SO.Actual_Total_Tax_and_Duty__c=ACRechatgeTaxandDuties;
           SO.Actual_Finance_Fee__c=ACFinanceFee;
           SO.Actual_Total_Handling_Costs__c=ACTotHandlingCost;
           SO.Actual_Insurance_Fee_USD__c=ACINsurance;
           SO.Actual_Total_IOR_EOR__c=ACIORandImport;
           SO.Actual_International_Delivery_Fee__c=ACInternationsldeliveryFee;
           SO.Actual_Miscellaneous_Fee__c=ACMiselaneousFee;
           SO.Actual_Total_License_Cost__c=ACTotLicenseCost;
                SO.Actual_Duties_and_Taxes_Other__c=ACTAXOther;
              SO.Actual_Taxes_Forex_Spread__c=ACTAXForex;
            
            
           
           Update SO;
          
              
          }    
    
      }
    }
     }
    
      // Forecasting fields update on SupplierInvoice
      
           for(CPA_Costing__c CPAC : trigger.new) {
               
              Map<Id,Schema.RecordTypeInfo> rtMap = CPA_Costing__c.sobjectType.getDescribe().getRecordTypeInfosById();  
             Id RecordTypeIdCosting = rtMap.get(CPAC.RecordTypeId).getRecordTypeId();
             if(RecordTypeIdCosting =='0120Y000000ytNmQAI'){
               //if(CPAC.recordType.Name =='Transaction'){
                //  If( (CPAC.Invoice_amount_USD_New__c != trigger.oldMap.get(CPAC.ID).Invoice_amount_USD_New__c  || CPAC.Amount_in_USD__c != trigger.oldMap.get(CPAC.ID).Amount_in_USD__c || CPAC.Trigger__c != trigger.oldMap.get(CPAC.ID).Trigger__c )  ){
            If((CPAC.supplier_invoice__c == null && (CPAC.Invoice_amount_USD_New__c != trigger.oldMap.get(CPAC.ID).Invoice_amount_USD_New__c  || CPAC.Amount_in_USD__c != trigger.oldMap.get(CPAC.ID).Amount_in_USD__c)) ||   (CPAC.supplier_invoice__c != null && CPAC.trigger__c == true && (CPAC.Invoice_amount_USD_New__c != trigger.oldMap.get(CPAC.ID).Invoice_amount_USD_New__c  || CPAC.Amount_in_USD__c != trigger.oldMap.get(CPAC.ID).Amount_in_USD__c || CPAC.Trigger__c != trigger.oldMap.get(CPAC.ID).Trigger__c ))){
                      
                        List<CPA_Costing__c> CPACostings = [Select Cost_Category__C,Tax_Sub_Category__c,Invoice_amount_USD_New__c,Amount_in_USD__c,Shipment_Order_Old__c,Supplier_Invoice__c from CPA_Costing__c where Shipment_Order_Old__c =: CPAC.Shipment_Order_Old__c and  Supplier_Invoice__c =: CPAC.Supplier_Invoice__c];
                        For(Integer i=0;CPACostings.size()>i;i++ ){
                       CPACostings2.add(CPACostings[i]);
                             SOIDs = CPAC.Shipment_Order_Old__c;  
                             SupplierInvoice = CPAC.Supplier_Invoice__c;
                                
                         if(CPACostings[i].Cost_Category__C=='Admin')    { FCAdminFee2 = FCAdminFee2+CPACostings[i].Amount_in_USD__c; ACAdminFee2 = ACAdminFee2+CPACostings[i].Invoice_amount_USD_New__c;}
                     else if(CPACostings[i].Cost_Category__C=='Bank')    { FCBankFee2 = FCBankFee2+CPACostings[i].Amount_in_USD__c; ACBankFee2 = ACBankFee2+CPACostings[i].Invoice_amount_USD_New__c;}
                     else if(CPACostings[i].Cost_Category__C=='Finance')  { FCFinanceFee2 = FCFinanceFee2+CPACostings[i].Amount_in_USD__c; ACFinanceFee2 = ACFinanceFee2+CPACostings[i].Invoice_amount_USD_New__c;}
                     else if(CPACostings[i].Cost_Category__C=='IOR Cost')    { FCIORandImport2 = FCIORandImport2+CPACostings[i].Amount_in_USD__c; ACIORandImport2 = ACIORandImport2+CPACostings[i].Invoice_amount_USD_New__c;}
                     else if(CPACostings[i].Cost_Category__C=='Insurance')  { FCINsurance2 = FCINsurance2+CPACostings[i].Amount_in_USD__c; ACINsurance2 = ACINsurance2+CPACostings[i].Invoice_amount_USD_New__c;}
                     else if(CPACostings[i].Cost_Category__C=='International freight')    { FCInternationsldeliveryFee2 = FCInternationsldeliveryFee2+CPACostings[i].Amount_in_USD__c; ACInternationsldeliveryFee2 = ACInternationsldeliveryFee2+CPACostings[i].Invoice_amount_USD_New__c;}
                     else if(CPACostings[i].Cost_Category__C=='Miscellaneous')    { FCMiselaneousFee2 = FCMiselaneousFee2+CPACostings[i].Amount_in_USD__c;  ACMiselaneousFee2 = ACMiselaneousFee2+CPACostings[i].Invoice_amount_USD_New__c;}
                     //else if(CPACostings[i].Cost_Category__C=='Tax')    { FCRechatgeTaxandDuties2 = FCRechatgeTaxandDuties2+CPACostings[i].Amount_in_USD__c;  ACRechatgeTaxandDuties2 = ACRechatgeTaxandDuties2+CPACostings[i].Invoice_amount_USD_New__c;} 
                     else if(CPACostings[i].Cost_Category__C=='Clearance')    { FCTotClearence2 = FCTotClearence2+CPACostings[i].Amount_in_USD__c; ACTotClearence2 = ACTotClearence2+CPACostings[i].Invoice_amount_USD_New__c;}
                     else if(CPACostings[i].Cost_Category__C=='Brokerage')    { FCTotCustomsBrokerage2 = FCTotCustomsBrokerage2+CPACostings[i].Amount_in_USD__c; ACTotCustomsBrokerage2 = ACTotCustomsBrokerage2+CPACostings[i].Invoice_amount_USD_New__c;}
                     else if(CPACostings[i].Cost_Category__C=='Handling')    { FCTotHandlingCost2 = FCTotHandlingCost2+CPACostings[i].Amount_in_USD__c; ACTotHandlingCost2 = ACTotHandlingCost2+CPACostings[i].Invoice_amount_USD_New__c;}
                     else if(CPACostings[i].Cost_Category__C=='Licenses')    { FCTotLicenseCost2 = FCTotLicenseCost2+CPACostings[i].Amount_in_USD__c; ACTotLicenseCost2 = ACTotLicenseCost2+CPACostings[i].Invoice_amount_USD_New__c;}
                        
                         if(CPACostings[i].Cost_Category__C=='Tax' && (CPACostings[i].Tax_Sub_Category__c !='Duties and Taxes Other' || CPACostings[i].Tax_Sub_Category__c !='Tax Forex Spread'))    {  FCRechatgeTaxandDuties2 = FCRechatgeTaxandDuties2+CPACostings[i].Amount_in_USD__c;  ACRechatgeTaxandDuties2 = ACRechatgeTaxandDuties2+CPACostings[i].Invoice_amount_USD_New__c;}
                  if(CPACostings[i].Cost_Category__C=='Tax' && CPACostings[i].Tax_Sub_Category__c=='Duties and Taxes Other' )    { FCTAXOther2 = FCTAXOther2+CPACostings[i].Amount_in_USD__c;  ACTAXOther2 = ACTAXOther2+CPACostings[i].Invoice_amount_USD_New__c;}                
                        if(CPACostings[i].Cost_Category__C=='Tax' && CPACostings[i].Tax_Sub_Category__c=='Tax Forex Spread' )    { FCTAXForex2 = FCTAXForex2+CPACostings[i].Amount_in_USD__c;  ACTAXForex2 = ACTAXForex2+CPACostings[i].Invoice_amount_USD_New__c;}
                               }
                        
                        } 
               }
            }
   for(CPA_Costing__c CPAC : trigger.new) {
       
        Map<Id,Schema.RecordTypeInfo> rtMap = CPA_Costing__c.sobjectType.getDescribe().getRecordTypeInfosById();  
             Id RecordTypeIdCosting = rtMap.get(CPAC.RecordTypeId).getRecordTypeId();
             if(RecordTypeIdCosting =='0120Y000000ytNmQAI'){
       //  if(CPAC.recordType.Name =='Transaction'){
    //  If((CPAC.Invoice_amount_USD_New__c != trigger.oldMap.get(CPAC.ID).Invoice_amount_USD_New__c  || CPAC.Amount_in_USD__c != trigger.oldMap.get(CPAC.ID).Amount_in_USD__c || CPAC.Trigger__c != trigger.oldMap.get(CPAC.ID).Trigger__c )  ){
    If((CPAC.supplier_invoice__c == null && (CPAC.Invoice_amount_USD_New__c != trigger.oldMap.get(CPAC.ID).Invoice_amount_USD_New__c  || CPAC.Amount_in_USD__c != trigger.oldMap.get(CPAC.ID).Amount_in_USD__c)) ||   (CPAC.supplier_invoice__c != null && CPAC.trigger__c == true && (CPAC.Invoice_amount_USD_New__c != trigger.oldMap.get(CPAC.ID).Invoice_amount_USD_New__c  || CPAC.Amount_in_USD__c != trigger.oldMap.get(CPAC.ID).Amount_in_USD__c) || CPAC.Trigger__c != trigger.oldMap.get(CPAC.ID).Trigger__c  )){  
      if(SupplierInvoice !=Null){
                  Supplier_Invoice__C SIS =[select id,Actual_Tax_Forex_Spread__c, Actual_Duties_and_Taxes_Other__c,FC_Admin_Fee__c,FC_Bank_Fees__c,FC_Finance_Fee__c,FC_Insurance_Fee_USD__c,FC_Miscellaneous_Fee__c,FC_International_Delivery_Fee__c,Total_Duties_and_Taxes__c,Total_Brokerage_Costs__c,Total_Clearance_Costs__c,Total_Handling_Costs__c,Total_License_Permits_Cost__c,Total_IOR_EOR__c,Actual_Admin_Fee__c,Actual_Bank_Fees__c,Actual_Finance_Fee__c,Actual_Insurance_Fee_USD__c,Actual_Miscellaneous_Fee__c,Actual_International_Delivery_Fee__c,Actual_Duties_and_Taxes__c,Actual_Brokerage_Costs__c,Actual_Clearance_Costs__c,Actual_Handling_Costs__c,Actual_License_Permits_Cost__c,Actual_IOR_EOR_Cost__c  from supplier_Invoice__C where id=:SupplierInvoice];
      
            SIS.FC_Admin_Fee__c=FCAdminFee2;
           SIS.FC_Bank_Fees__c=FCBankFee2;
           SIS.FC_Finance_Fee__c=FCFinanceFee2;
           SIS.Total_IOR_EOR__c=FCIORandImport2;
           SIS.FC_Insurance_Fee_USD__c=FCINsurance2;
           SIS.FC_International_Delivery_Fee__c=FCInternationsldeliveryFee2;
           SIS.FC_Miscellaneous_Fee__c=FCMiselaneousFee2;
           SIS.Total_Duties_and_Taxes__c=FCRechatgeTaxandDuties2;
           SIS.Total_Clearance_Costs__c=FCTotClearence2;
           SIS.Total_Brokerage_Costs__c=FCTotCustomsBrokerage2;
           SIS.Total_Handling_Costs__c=FCTotHandlingCost2;
           SIS.Total_License_Permits_Cost__c=FCTotLicenseCost2;
                SIS.FC_Duties_and_Taxes_Other__c=FCTAXOther2;
                SIS.FC_Tax_Forex_Spread__c=FCTAXForex2;
            
            
            
           SIS.Actual_Admin_Fee__c=ACAdminFee2;
           SIS.Actual_Bank_Fees__c=ACBankFee2;
           SIS.Actual_Brokerage_Costs__c=ACTotCustomsBrokerage2;
           SIS.Actual_Clearance_Costs__c=ACTotClearence2;
           SIS.Actual_Duties_and_Taxes__c=ACRechatgeTaxandDuties2;
           SIS.Actual_Finance_Fee__c=ACFinanceFee2;
           SIS.Actual_Handling_Costs__c=ACTotHandlingCost2;
           SIS.Actual_Insurance_Fee_USD__c=ACINsurance2;
           SIS.Actual_IOR_EOR_Cost__c=ACIORandImport2;
           SIS.Actual_International_Delivery_Fee__c=ACInternationsldeliveryFee2;
           SIS.Actual_Miscellaneous_Fee__c=ACMiselaneousFee2;
           SIS.Actual_License_Permits_Cost__c=ACTotLicenseCost2;
                 SIS.Actual_Duties_and_Taxes_Other__c=ACTAXOther2;
                SIS.Actual_Tax_Forex_Spread__c=ACTAXForex2;
            
            
              
           Update SIS;
           
                
        }    
    
       }
       }
     }
          
        
        
        
    
    }
    
    IF(Trigger.IsBefore && Trigger.Isdelete){
       
        
     List<CPA_Costing__c> CPACostings1 = new List<CPA_Costing__c>();
     decimal FCAdminFee = 0.0;
     decimal FCBankFee = 0.0;
     decimal FCFinanceFee = 0.0;
     decimal FCIORandImport = 0.0;
     decimal FCINsurance = 0.0;
     decimal FCInternationsldeliveryFee = 0.0;
     decimal FCMiselaneousFee = 0.0;
     decimal FCRechatgeTaxandDuties = 0.0;
     decimal FCTotClearence = 0.0;
     decimal FCTotCustomsBrokerage = 0.0;
     decimal FCTotHandlingCost = 0.0;
     decimal FCTotLicenseCost = 0.0;
        decimal FCTAXOther = 0.0;
        decimal FCTAXForex = 0.0;
        
     decimal ACAdminFee = 0.0;
     decimal ACBankFee = 0.0;
     decimal ACFinanceFee = 0.0;
     decimal ACIORandImport = 0.0;
     decimal ACINsurance = 0.0;
     decimal ACInternationsldeliveryFee = 0.0;
     decimal ACMiselaneousFee = 0.0;
     decimal ACRechatgeTaxandDuties = 0.0;
     decimal ACTotClearence = 0.0;
     decimal ACTotCustomsBrokerage = 0.0;
     decimal ACTotHandlingCost = 0.0;
     decimal ACTotLicenseCost = 0.0;
        decimal ACTAXOther = 0.0;
        decimal ACTAXForex = 0.0;
        
        
     String SOIDs = null;
     String SupplierInvoice = null;
        
        // Forecasting fields update on shipmentOrder
           for(CPA_Costing__c CPAC : trigger.old) {
        
              If(CPAC.Supplier_Invoice__c !=Null ){
                                CPAC.addError('Selected record linked an supplier invoice. It cant be deleted ');
        
            }


                else If(CPAC.Supplier_Invoice__c ==Null && CPAC.DeleteAll__c==FALSE  ){
            
                      
                        List<CPA_Costing__c> CPACostings = [Select Cost_Category__C,Tax_Sub_Category__c,Invoice_amount_USD_New__c,Amount_in_USD__c,Shipment_Order_Old__c,Supplier_Invoice__c from CPA_Costing__c where Shipment_Order_Old__c =: CPAC.Shipment_Order_Old__c and id !=: CPAC.Id];
                        For(Integer i=0;CPACostings.size()>i;i++ ){
                       CPACostings1.add(CPACostings[i]);
                             SOIDs = CPAC.Shipment_Order_Old__c;  
                             SupplierInvoice = CPAC.Supplier_Invoice__c;
                                
                         if(CPACostings[i].Cost_Category__C=='Admin')    { FCAdminFee = FCAdminFee+CPACostings[i].Amount_in_USD__c; ACAdminFee = ACAdminFee+CPACostings[i].Invoice_amount_USD_New__c;}
                     else if(CPACostings[i].Cost_Category__C=='Bank')    { FCBankFee = FCBankFee+CPACostings[i].Amount_in_USD__c; ACBankFee = ACBankFee+CPACostings[i].Invoice_amount_USD_New__c;}
                     else if(CPACostings[i].Cost_Category__C=='Finance')  { FCFinanceFee = FCFinanceFee+CPACostings[i].Amount_in_USD__c; ACFinanceFee = ACFinanceFee+CPACostings[i].Invoice_amount_USD_New__c;}
                     else if(CPACostings[i].Cost_Category__C=='IOR Cost')    { FCIORandImport = FCIORandImport+CPACostings[i].Amount_in_USD__c; ACIORandImport = ACIORandImport+CPACostings[i].Invoice_amount_USD_New__c;}
                     else if(CPACostings[i].Cost_Category__C=='Insurance')  { FCINsurance = FCINsurance+CPACostings[i].Amount_in_USD__c; ACINsurance = ACINsurance+CPACostings[i].Invoice_amount_USD_New__c;}
                     else if(CPACostings[i].Cost_Category__C=='International freight')    { FCInternationsldeliveryFee = FCInternationsldeliveryFee+CPACostings[i].Amount_in_USD__c; ACInternationsldeliveryFee = ACInternationsldeliveryFee+CPACostings[i].Invoice_amount_USD_New__c;}
                     else if(CPACostings[i].Cost_Category__C=='Miscellaneous')    { FCMiselaneousFee = FCMiselaneousFee+CPACostings[i].Amount_in_USD__c;  ACMiselaneousFee = ACMiselaneousFee+CPACostings[i].Invoice_amount_USD_New__c;}
                     else if(CPACostings[i].Cost_Category__C=='Tax')    { FCRechatgeTaxandDuties = FCRechatgeTaxandDuties+CPACostings[i].Amount_in_USD__c;  }
                     else if(CPACostings[i].Cost_Category__C=='Clearance')    { FCTotClearence = FCTotClearence+CPACostings[i].Amount_in_USD__c; ACTotClearence = ACTotClearence+CPACostings[i].Invoice_amount_USD_New__c;}
                     else if(CPACostings[i].Cost_Category__C=='Brokerage')    { FCTotCustomsBrokerage = FCTotCustomsBrokerage+CPACostings[i].Amount_in_USD__c; ACTotCustomsBrokerage = ACTotCustomsBrokerage+CPACostings[i].Invoice_amount_USD_New__c;}
                     else if(CPACostings[i].Cost_Category__C=='Handling')    { FCTotHandlingCost = FCTotHandlingCost+CPACostings[i].Amount_in_USD__c; ACTotHandlingCost = ACTotHandlingCost+CPACostings[i].Invoice_amount_USD_New__c;}
                     else if(CPACostings[i].Cost_Category__C=='Licenses')    { FCTotLicenseCost = FCTotLicenseCost+CPACostings[i].Amount_in_USD__c; ACTotLicenseCost = ACTotLicenseCost+CPACostings[i].Invoice_amount_USD_New__c;}
                          
                   if(CPACostings[i].Cost_Category__C=='Tax' && (CPACostings[i].Tax_Sub_Category__c !='Duties and Taxes Other' || CPACostings[i].Tax_Sub_Category__c !='Tax Forex Spread'))    {  ACRechatgeTaxandDuties = ACRechatgeTaxandDuties+CPACostings[i].Invoice_amount_USD_New__c;}
                   if(CPACostings[i].Cost_Category__C=='Tax' && CPACostings[i].Tax_Sub_Category__c=='Duties and Taxes Other' )    { FCTAXOther = FCTAXOther+CPACostings[i].Amount_in_USD__c;  ACTAXOther = ACTAXOther+CPACostings[i].Invoice_amount_USD_New__c;}                
                         if(CPACostings[i].Cost_Category__C=='Tax' && CPACostings[i].Tax_Sub_Category__c=='Tax Forex Spread' )    { FCTAXForex = FCTAXForex+CPACostings[i].Amount_in_USD__c;  ACTAXForex = ACTAXForex+CPACostings[i].Invoice_amount_USD_New__c;}
                                                      }
                        
                        } 
            
            }
   for(CPA_Costing__c CPAC : trigger.old) {
       If(CPAC.Supplier_Invoice__c ==Null && CPAC.DeleteAll__c==FALSE ){
    //    Shipment_Order__C SO = [Select Actual_Admin_Fee__c,Actual_Bank_Fees__c,Actual_Total_Customs_Brokerage_Cost__c,Actual_Total_Clearance_Costs__c,Actual_Total_Tax_and_Duty__c,Actual_Finance_Fee__c,Actual_Total_Handling_Costs__c,Actual_Insurance_Fee_USD__c,Actual_Total_IOR_EOR__c,Actual_International_Delivery_Fee__c,Actual_Miscellaneous_Fee__c,Actual_Total_License_Cost__c,Actual_Duties_and_Taxes_Other__c,Actual_Exchange_gain_loss_on_payment__c,Actual_Taxes_Forex_Spread__c,FC_Admin_Fee__c,FC_Bank_Fees__c,FC_Finance_Fee__c,FC_IOR_and_Import_Compliance_Fee_USD__c,FC_Insurance_Fee_USD__c,FC_International_Delivery_Fee__c,FC_Miscellaneous_Fee__c,FC_Recharge_Tax_and_Duty__c,FC_Total_Clearance_Costs__c,FC_Total_Customs_Brokerage_Cost__c,FC_Total_Handling_Costs__c,FC_Total_License_Cost__c from Shipment_Order__C where Id =:SOIDs limit 1];
           
            Shipment_Order__C SO = [Select Actual_Admin_Fee__c,Actual_Bank_Fees__c,Actual_Total_Customs_Brokerage_Cost__c,Actual_Total_Clearance_Costs__c,Actual_Total_Tax_and_Duty__c,Actual_Finance_Fee__c,Actual_Total_Handling_Costs__c,Actual_Insurance_Fee_USD__c,Actual_Total_IOR_EOR__c,Actual_International_Delivery_Fee__c,Actual_Miscellaneous_Fee__c,Actual_Total_License_Cost__c,Actual_Duties_and_Taxes_Other__c,Actual_Exchange_gain_loss_on_payment__c,Actual_Taxes_Forex_Spread__c,FC_Admin_Fee__c,FC_Bank_Fees__c,FC_Finance_Fee__c,FC_IOR_and_Import_Compliance_Fee_USD__c,FC_Insurance_Fee_USD__c,FC_International_Delivery_Fee__c,FC_Miscellaneous_Fee__c,FC_Recharge_Tax_and_Duty__c,FC_Total_Clearance_Costs__c,FC_Total_Customs_Brokerage_Cost__c,FC_Total_Handling_Costs__c,FC_Total_License_Cost__c from Shipment_Order__C where Id =:CPAC.shipment_order_old__C   limit 1];
           
        if(SO.id !=Null){
           SO.FC_Admin_Fee__c=FCAdminFee;
           SO.FC_Bank_Fees__c=FCBankFee;
           SO.FC_Finance_Fee__c=FCFinanceFee;
           SO.FC_IOR_and_Import_Compliance_Fee_USD__c=FCIORandImport;
           SO.FC_Insurance_Fee_USD__c=FCINsurance;
           SO.FC_International_Delivery_Fee__c=FCInternationsldeliveryFee;
           SO.FC_Miscellaneous_Fee__c=FCMiselaneousFee;
           SO.FC_Recharge_Tax_and_Duty__c=FCRechatgeTaxandDuties;
           SO.FC_Total_Clearance_Costs__c=FCTotClearence;
           SO.FC_Total_Customs_Brokerage_Cost__c=FCTotCustomsBrokerage;
           SO.FC_Total_Handling_Costs__c=FCTotHandlingCost;
           SO.FC_Total_License_Cost__c=FCTotLicenseCost;
            
            
            
           SO.Actual_Admin_Fee__c=ACAdminFee;
           SO.Actual_Bank_Fees__c=ACBankFee;
           SO.Actual_Total_Customs_Brokerage_Cost__c=ACTotCustomsBrokerage;
           SO.Actual_Total_Clearance_Costs__c=ACTotClearence;
           SO.Actual_Total_Tax_and_Duty__c=ACRechatgeTaxandDuties;
           SO.Actual_Finance_Fee__c=ACFinanceFee;
           SO.Actual_Total_Handling_Costs__c=ACTotHandlingCost;
           SO.Actual_Insurance_Fee_USD__c=ACINsurance;
           SO.Actual_Total_IOR_EOR__c=ACIORandImport;
           SO.Actual_International_Delivery_Fee__c=ACInternationsldeliveryFee;
           SO.Actual_Miscellaneous_Fee__c=ACMiselaneousFee;
           SO.Actual_Total_License_Cost__c=ACTotLicenseCost;
           
              SO.Actual_Duties_and_Taxes_Other__c=ACTAXOther;
              SO.Actual_Taxes_Forex_Spread__c=ACTAXForex;
            
            
            
           Update SO;
        }    
    
       }
     }
      
    
    }

}