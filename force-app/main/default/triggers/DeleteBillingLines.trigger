trigger DeleteBillingLines on AcctSeed__Billing__c (after update) {
    Set<Id> setBillingIds = new Set<Id>();
    for(AcctSeed__Billing__c billing : Trigger.new) {
        if (billing.Customer_Invoice_Changed__c) {
            setBillingIds.add(billing.Id);
        }
    }
    delete [SELECT Id from AcctSeed__Billing_Line__c where AcctSeed__Billing__c in:setBillingIds ];  
    
    }