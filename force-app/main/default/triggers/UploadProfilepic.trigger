trigger UploadProfilepic on User (after update) {
    
    If(Trigger.IsUpdate && Trigger.IsAfter){
        System.debug('--in 1--');
        for(User us : trigger.new) {  
            System.debug('--in 2--');   
            If((us.UpdateProfilepic__c != trigger.oldMap.get(us.ID).UpdateProfilepic__c) &&  us.UpdateProfilepic__c == true && us.ProfilePicId__c != ''){
                System.debug('--in 3--');       
                String communityId = null;
                String userId= Us.ID;
                Integer vnumber=49;
                String PicId =us.ProfilePicId__c;
                updateUserProfilePic(PicId,userId);
                //UpdateProfilepic__c = False
                
                User usr = new User();
                usr.Id = us.Id;
                usr.UpdateProfilepic__c = false;
                usr.ProfilePicId__c = '';
                update usr;
            }
        }
    }
    
    public static Boolean updateUserProfilePic(String userProfilePicString, String userId){
        Boolean updateSuccessful = true;
        System.debug('--in 4--');    
        //System.debug('-------------' + userProfilePicString.length());
        try{
            document d =[select id,body from document where id=:userProfilePicString];
            
            Blob blobImage =d.Body; //EncodingUtil.base64Decode(userProfilePicString);
            
            ConnectApi.BinaryInput fileUpload = new ConnectApi.BinaryInput(blobImage, 'image/jpg', 'userImage.jpg');
           // ConnectApi.Photo photoProfile = ConnectApi.UserProfiles.setPhoto('0DB1q00000000ozGAA', userId,  fileUpload);
          ConnectApi.Photo photoProfile = ConnectApi.UserProfiles.setPhoto(null, userId,  fileUpload);
        }
        catch(Exception exc){
            updateSuccessful = false;
            system.debug('updateSuccessful ?'+updateSuccessful);
            system.debug('updateSuccessful ?'+exc.getMessage());
        }
        
        return updateSuccessful;
        
    }
}