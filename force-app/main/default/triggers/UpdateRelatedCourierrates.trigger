trigger UpdateRelatedCourierrates on Courier_Rates__c (after insert,after update) {
    
    List<ID> SelectedCourierrateID = new List<ID>(); 
    List<ID> FreightID = new List<ID>(); 
    List<Courier_Rates__c> SelectedCourierrate = new List<Courier_Rates__c>();
    List<Courier_Rates__c> UpdateCourierrateID = new List<Courier_Rates__c>(); 
    ID FreightI;
    ID SelectedCourierrateI;
    
    for (Courier_Rates__c a:Trigger.new)
    {
        If(a.status__C=='Selected'){
            SelectedCourierrateID.add(a.id);
            FreightID.add(a.Freight_Request__C);
            SelectedCourierrate.add(a);
            FreightI=a.Freight_Request__C;
            SelectedCourierrateI=a.id;
        }
    }
    IF(SelectedCourierrateID !=Null){
        List<Courier_Rates__c> CR = [select Id,Status__C from Courier_rates__c where id not IN:SelectedCourierrateID and Freight_Request__c IN :FreightID and status__c != 'Expired' ];
        For(Courier_Rates__c cra : CR){
            Cra.status__c='Created';
            UpdateCourierrateID.add(cra);
        }
        
    }
    Update UpdateCourierrateID;
    
    if(SelectedCourierrateID !=null && FreightID !=null){ 
        List<Courier_Rates__c> crsList = [select Id,Status__C,service_type__c,Origin__c,Shipmate_Preference__r.name,Shipmate_Preference__r.zkmulti__AccountNumber__c,rate__c,freight_request__c,Final_rate__C from Courier_rates__c where id  IN :SelectedCourierrateID and Freight_Request__c IN : FreightID];
        Map<Id,Courier_Rates__c> crsMap = new  Map<Id,Courier_Rates__c>();
        for(Courier_Rates__c cr:crsList){
            crsMap.put(cr.Freight_Request__c,cr);
        }
        List<Freight__c> FrList = new List<Freight__c>();
        
        Map<String, String> serviceTypeWithPickup = new Map<String, String>();
        for(Pickup_Options__c pickup : Pickup_Options__c.getall().values()){
            serviceTypeWithPickup.put(pickup.Service_Type__c, pickup.Pickup_Option__c);
        }
        
         Map<String, String> Twodigitcountrycode = new Map<String, String>();
        for(CountryofOriginMap__c COO : CountryofOriginMap__c.getall().values()){
            Twodigitcountrycode.put(COO.Country__c, COO.Two_Digit_Country_Code__c);
        }
        
        for(Freight__c Fr : [Select Id,Courier_Service_Type__c,Shipmate_Preference_Name__c,con_Country__C,SP_Account_number__c,Carrier_Account_Name__c,Selected_Rate_From_Existing__c from Freight__c where Id IN :FreightID]){
            Courier_Rates__c CRS = crsMap.get(Fr.Id);
            Fr.Courier_Service_Type__c=CRS.service_type__c;
            Fr.Shipmate_Preference_Name__c=CRS.Shipmate_Preference__r.name;
            Fr.Carrier_Account_Name__c=CRS.Origin__c;
            Fr.SP_Account_number__c=CRS.Shipmate_Preference__r.zkmulti__AccountNumber__c;
            Fr.Selected_Rate_From_Existing__c=CRS.Final_rate__C;
            if(serviceTypeWithPickup.containsKey(CRS.service_type__c)){
                fr.Pickup_options__c = serviceTypeWithPickup.get(CRS.service_type__c);
            }
            if(Twodigitcountrycode.containsKey(Fr.con_Country__C)){
                fr.Con_Country_2Dig__c = Twodigitcountrycode.get(Fr.con_Country__C);
            }
            
            FrList.add(Fr);
        }
        Update FrList;
    }
    
    
    
}