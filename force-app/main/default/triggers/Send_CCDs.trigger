trigger Send_CCDs on Customs_Clearance_Documents__c (after update) {

for(Customs_Clearance_Documents__c CCD : trigger.new){
    if (CCD.Client_Name__c.contains('Orange Business Services') && CCD.Status__c == 'Reviewed' && CCD.CCD_Document_Sent__c== FALSE) {
    
    //Retrieve Email template
     EmailTemplate et=[Select id from EmailTemplate where name=:'Orange CCD Notification'];
     
     
    //Create email list
        List<Messaging.SingleEmailMessage> emails = new List<Messaging.SingleEmailMessage>();

       //Create attachment list
       List<Messaging.EmailFileAttachment> efalist = new List<Messaging.EmailFileAttachment>();
       
       contact c =[select id, email from Contact where Id = :CCD.Contact_Id__c];
       //Create message
       
       Messaging.SingleEmailMessage singlemail = new Messaging.SingleEmailMessage();
        //add template
       singleMail.setTemplateId(et.Id);              
        //set target object for merge fields
        singleMail.setTargetObjectId(c.Id);
        singlemail.setwhatId(CCD.Id);       
        //set to save as activity or not
        singleMail.setSaveAsActivity(true);
         
         
        //add address's that you are sending the email to
        String []ToAddress= new string[] {CCD.ICE_Email__c,CCD.AM_Email__c };

        //set addresses
        singlemail.setCCAddresses(toAddress);
        
        
            //add mail
                emails.add(singleMail);
        
        {
           
                      

            // fetch attachments for Object
                     
            List<Attachment> allatt = [SELECT id, Name, body, ContentType FROM Attachment WHERE ParentId = : CCD.id];                     
         
         // Attachment att = [SELECT id, Name, body, ContentType FROM Attachment WHERE ParentId = : SHA.id limit 1];

           for(Attachment att : allatt) {
            // List of attachments handler
Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();

   {
      // try{

        

//Messaging.Emailfileattachment efa = new Messaging.Emailfileattachment();
efa.setFileName(att.Name);

efa.setBody(att.Body);

efa.setContentType(att.ContentType);

efaList.add( efa );

  
    
   }

    // Attach files to email instance
   // singlemail.setFileAttachments(attachmentList);
   
  {
    singlemail.setFileAttachments(efaList);
   }
   }
    //send the message
     Messaging.sendEmail(emails);
          }

 } 

 
}
}