trigger partMatchingTrigger on Part__c (before insert, after insert, before update, after update, after delete, before delete) {
    
    Set<Id> partIds = new Set<Id>();
    Set<String> partName = new Set<String>();
    Set<String> partName2 = new Set<String>();
    Set<String> hsCode = new Set<String>();
    Set<String> hsCode4 = new Set<String>();
    Set<String> usHsCode8 = new Set<String>();
    Set<String> accountId = new Set<String>();
    Set<String> clearanceDestination = new Set<String>();
    Set<Id> SoIds = new Set<Id>();
    Set<Id> productId = new Set<Id>();
    String part;
    
    
    Decimal allSOAverage;     Decimal destinationSOAverage;        Decimal clientSOAverage;      Date allSOLastPOD;
    Decimal allSOMin;         Decimal destinationSOMin;            Decimal clientSOMin;          Date clientSOLastPOD;
    Decimal allSOMax;         Decimal destinationSOMax;            Decimal clientSOMax;          Date destinationSOLastPOD;
    Decimal allSOShipped;     Decimal destinationSOShipped;        Decimal clientSOShipped;      String maxAllHSCode;
    String maxAllCOO;         String maxdestHSCode;
    
   
    
  If(Trigger.IsInsert && Trigger.IsBefore){
      
      
      for (Part__c li : Trigger.new) {
        partIds.add(li.Id);
        accountId.add(li.AccountId__c);
        hsCode.add(li.US_HTS_Code__c);
        system.debug('hsCode---> '+ hsCode);
        hsCode4.add(li.US_HTS_Code__c.left(4)+'%');
        usHsCode8.add(li.US_HTS_Code__c.left(8));
        clearanceDestination.add(li.Ship_To_Country__c);
        system.debug('clearanceDestination---> '+ clearanceDestination);
        partName.add(li.Search_Name__c.trim());
        partName2.add(li.Name);
        SoIds.add(li.Shipment_Order__c);}
      
    Map<Id, HS_Codes_and_Associated_Details__c> usHsCodes = new Map<Id, HS_Codes_and_Associated_Details__c>( [select Id, Name, Additional_Part_Specific_Tax_Rate_1__c, Additional_Part_Specific_Tax_Rate_2__c,
                                                                                                               Additional_Part_Specific_Tax_Rate_3__c, Country_Name__c, Description__c, Destination_HS_Code__c, Rate__c,
                                                                                                               Specific_VAT_Rate__c, US_HTS_Code__c, Vat_Exempted__c
                                                                                                               from HS_Codes_and_Associated_Details__c 
                                                                                                               where US_HTS_Code__c Like :hsCode4
                                                                                                               and Country_Name__c in :clearanceDestination
                                                                                                               and Inactive__c = FALSE
                                                                                                               Order by Rate__c desc limit 50000]);
    
     Map<String, Id> allUSCodes = new Map<String, Id>();
    
     For (Id getAllCodes : usHsCodes.keySet()) { 
         
        String usCode = usHsCodes.get(getAllCodes).Country_Name__c + usHsCodes.get(getAllCodes).US_HTS_Code__c;
      
         If(!allUSCodes.containsKey(usCode)){   
         allUSCodes.put(usHsCodes.get(getAllCodes).Country_Name__c + usHsCodes.get(getAllCodes).US_HTS_Code__c, getAllCodes); }
           
       else if(usHsCodes.get(getAllCodes).Rate__c > usHsCodes.get(allUSCodes.get(usCode)).Rate__c){
         
       allUSCodes.remove(usCode); 
       allUSCodes.put(usHsCodes.get(getAllCodes).Country_Name__c + usHsCodes.get(getAllCodes).US_HTS_Code__c, getAllCodes);           
       } 
         
     }
    
    system.debug('allUSCodes.size()---> '+ allUSCodes.size());
    //system.debug('allUSCode---> '+ allUSCodes);
    //
   
    
     Map<Id, HS_Codes_and_Associated_Details__c> destinationHsCodes = new Map<Id, HS_Codes_and_Associated_Details__c>([select Id, Name, Additional_Part_Specific_Tax_Rate_1__c, Additional_Part_Specific_Tax_Rate_2__c,
                                                                                                               Additional_Part_Specific_Tax_Rate_3__c, Country_Name__c, Description__c, Destination_HS_Code__c, Rate__c,
                                                                                                               Specific_VAT_Rate__c, US_HTS_Code__c, Vat_Exempted__c
                                                                                                               from HS_Codes_and_Associated_Details__c 
                                                                                                               where Destination_HS_Code__c Like :hsCode4
                                                                                                               and Country_Name__c in :clearanceDestination
                                                                                                               and Inactive__c = FALSE
                                                                                                               Order by Rate__c desc limit 50000]);
    
    
    
    
    Map<String, Id> allDestinationCodes = new Map<String, Id>();
    Map<String, String> destinationCodeTrunc = new Map<String, String>();
    
     For (Id getAllDestCodes : destinationHsCodes.keySet()) { 
         
          String destCode = destinationHsCodes.get(getAllDestCodes).Country_Name__c + destinationHsCodes.get(getAllDestCodes).Destination_HS_Code__c;
         
           If(!allDestinationCodes.containsKey(destCode)){
           allDestinationCodes.put(destinationHsCodes.get(getAllDestCodes).Country_Name__c + destinationHsCodes.get(getAllDestCodes).Destination_HS_Code__c, getAllDestCodes);
                
            }
         
            String InputString = destinationHsCodes.get(getAllDestCodes).Destination_HS_Code__c;
            String revStr = ' ';
            Integer InputStringsize = InputString.length();

       for (Integer i =0; InputString.length()>i; i++){
            revStr = InputString.substring(0, InputString.length()-i);
              
           If(!allDestinationCodes.containsKey(destinationHsCodes.get(getAllDestCodes).Country_Name__c + revStr)){
              allDestinationCodes.put(destinationHsCodes.get(getAllDestCodes).Country_Name__c + revStr, getAllDestCodes);   
           }
           
           else if(destinationHsCodes.get(getAllDestCodes).Rate__c > destinationHsCodes.get(allDestinationCodes.get(destinationHsCodes.get(getAllDestCodes).Country_Name__c + revStr)).Rate__c){
           
           allDestinationCodes.remove(destinationHsCodes.get(getAllDestCodes).Country_Name__c + revStr);
           allDestinationCodes.put(destinationHsCodes.get(getAllDestCodes).Country_Name__c + revStr, getAllDestCodes);
               
               
           }
           
           
          If((InputStringsize-i)==4){break;}
}
            
           
       } 
    
    
    
    
    
    system.debug('allDestinationCodes.size()---> '+ allDestinationCodes.size());
    system.debug('allDestinationCodes---> '+ allDestinationCodes);
    
    
    
    Map<Id, Tax_Structure_Per_Country__c> taxes = new Map<Id, Tax_Structure_Per_Country__c>([select Id, Name, Applied_to_Value__c,Tax_Type__c, Default_Duty_Rate__c, Country__r.Name,
                                                                                             Country__c, Rate__c
                                                                                            from Tax_Structure_Per_Country__c
                                                                                            where Country__r.Name in :clearanceDestination]);
    
     Map<String, Id> clearanceCountry = new Map<String, Id>();
      
     Map<String, Decimal> rates = new Map<String, Decimal>();
     Map<String, String> appliedTo = new Map<String, String>();
    
    For (Id getDefaults : taxes.keySet()) { 
        
       clearanceCountry.put(taxes.get(getDefaults).Country__r.Name, taxes.get(getDefaults).Country__c);
        
         appliedTo.put(taxes.get(getDefaults).Country__r.Name + taxes.get(getDefaults).Tax_Type__c, taxes.get(getDefaults).Applied_to_Value__c);  

        
        If(taxes.get(getDefaults).Tax_Type__c != 'Duties'){
           rates.put(taxes.get(getDefaults).Country__r.Name + taxes.get(getDefaults).Tax_Type__c, taxes.get(getDefaults).Rate__c);           
       } 
        
        Else{
            rates.put(taxes.get(getDefaults).Country__r.Name + taxes.get(getDefaults).Tax_Type__c, taxes.get(getDefaults).Default_Duty_Rate__c);  
            
        }
        
    }
    

    // Client match final map
    Map<String, Id> matchClientParts = new Map<String, Id>();
    
    // Client match initial map
    Map<Id, Part__c> clientProducts = new Map<Id, Part__c>( [select Id, Product__c, Search_Name__c, Product__r.Manufacturer_Text_Formula__c from Part__c 
                                                         where Search_Name__c in :partName
                                                          and Product__c != null
                                                          and AccountId__c in :accountId]);
    
     system.debug('clientProducts.size()---> '+ clientProducts.size());
    
        For (Id getClientParts : clientProducts.keySet()) {   
           matchClientParts.put(clientProducts.get(getClientParts).Search_Name__c, clientProducts.get(getClientParts).Product__c );           
       } 
    
    
    system.debug('matchClientParts.size()---> '+ matchClientParts.size());
    
    
     Map<String, Id> matchAllParts = new Map<String, Id>();
    
    Map<Id, Part__c> allProducts = new Map<Id, Part__c>( [select Id, Product__c, Search_Name__c, Product__r.Manufacturer_Text_Formula__c 
                                                          from Part__c 
                                                          where Search_Name__c in :partName
                                                          and Product__c != null]);
    
    system.debug('allProducts.size()---> '+ allProducts.size());
    
    For (Id getAllParts : allProducts.keySet()) {   
           matchAllParts.put(allProducts.get(getAllParts).Search_Name__c, allProducts.get(getAllParts).Product__c);           
       } 
    
    system.debug('matchAllParts.size()---> '+ matchAllParts.size());
      
     Map<Id, Product2> searchNameProducts = new Map<Id, Product2>( [select Id, Search_Name__c, Manufacturer_Text_Formula__c, Product_Name_Alias__c
                                                                    from Product2]);
     Map<String, Id> searchProducts = new Map<String, Id>(); 
     Map<String, Id> aliasList = new Map<String, Id>();
     Set<Id> productIdList = new Set<Id>();    
         
       For (Id getProductsBySearch : searchNameProducts.keySet()) { 
           
           searchProducts.put(searchNameProducts.get(getProductsBySearch).Search_Name__c, getProductsBySearch); 
           
            String alias = searchNameProducts.get(getProductsBySearch).Product_Name_Alias__c;
            String aliasId = getProductsBySearch;
            
            If(alias != null){
            
               List<String> mapAlias = alias.split(',');
            
            for (Integer i = 0; i < mapAlias.size(); i++ ){
                
                aliasList.put(mapAlias[i], getProductsBySearch);
                
            }
       
       }  
           
           
       } 
         
    
      
    system.debug('searchProducts.size()---> '+ searchProducts.size());
    system.debug('aliasList.size()---> '+ aliasList.size());
    system.debug('aliasList---> '+ aliasList);  

     for (Part__c li : Trigger.new) {
         
       try { 
        
               
        String partDestination = li.Ship_To_Country__c;
        String searchCode = partDestination + li.US_HTS_Code__c;
        String usSearchCode = partDestination + li.US_HTS_Code__c.left(8);
        Integer searchCodeLength = searchCode.length();
        
        
        
        If(allUSCodes.get(usSearchCode) != null){ 
            
             system.debug('In First Search If ');
            
            li.Matched_HS_Code2__c = allUSCodes.get(usSearchCode);
            li.Matched_HS_Code_Description__c = usHsCodes.get(allUSCodes.get(usSearchCode)).Description__c;
            li.Rate__c = usHsCodes.get(allUSCodes.get(usSearchCode)).Rate__c;
            li.Part_Specific_Rate_1__c = usHsCodes.get(allUSCodes.get(usSearchCode)).Additional_Part_Specific_Tax_Rate_1__c;
            li.Part_Specific_Rate_2__c = usHsCodes.get(allUSCodes.get(usSearchCode)).Additional_Part_Specific_Tax_Rate_2__c;
            li.Part_Specific_Rate_3__c = usHsCodes.get(allUSCodes.get(usSearchCode)).Additional_Part_Specific_Tax_Rate_3__c;
            li.Vat_Exempted__c = usHsCodes.get(allUSCodes.get(usSearchCode)).Vat_Exempted__c;
            li.Specific_VAT_Rate__c = usHsCodes.get(allUSCodes.get(usSearchCode)).Specific_VAT_Rate__c;
            li.Default_Duty_Rate__c = rates.get(partDestination + 'Duties');
            li.VAT_Rate__c = rates.get(partDestination + 'VAT');
            li.VAT_Applied_To_Value__c = appliedTo.get(partDestination + 'VAT');
            li.Applied_To_Value__c = appliedTo.get(partDestination + 'Duties');
            li.Clearance_Country__c = clearanceCountry.get(partDestination);
        }
        
        Else If(allUSCodes.get(usSearchCode) == null && allDestinationCodes.get(searchCode) != null){
            
             system.debug('In Second Search If ');
            
            li.Matched_HS_Code2__c = allDestinationCodes.get(searchCode);
            li.Matched_HS_Code_Description__c = destinationHsCodes.get(allDestinationCodes.get(searchCode)).Description__c;
            li.Rate__c = destinationHsCodes.get(allDestinationCodes.get(searchCode)).Rate__c;
            li.Part_Specific_Rate_1__c = destinationHsCodes.get(allDestinationCodes.get(searchCode)).Additional_Part_Specific_Tax_Rate_1__c;
            li.Part_Specific_Rate_2__c = destinationHsCodes.get(allDestinationCodes.get(searchCode)).Additional_Part_Specific_Tax_Rate_2__c;
            li.Part_Specific_Rate_3__c = destinationHsCodes.get(allDestinationCodes.get(searchCode)).Additional_Part_Specific_Tax_Rate_3__c;
            li.Vat_Exempted__c = destinationHsCodes.get(allDestinationCodes.get(searchCode)).Vat_Exempted__c;
            li.Specific_VAT_Rate__c = destinationHsCodes.get(allDestinationCodes.get(searchCode)).Specific_VAT_Rate__c;
            li.Default_Duty_Rate__c = rates.get(partDestination + 'Duties');
            li.VAT_Rate__c = rates.get(partDestination + 'VAT');
            li.VAT_Applied_To_Value__c = appliedTo.get(partDestination + 'VAT');
            li.Applied_To_Value__c = appliedTo.get(partDestination + 'Duties');
            li.Clearance_Country__c = clearanceCountry.get(partDestination);}
        
        
        
        Else if(allUSCodes.get(usSearchCode) == null && allDestinationCodes.get(searchCode) == null){
            
            system.debug('In Third Search If ');
            
            String InputString2 = searchCode;
            String revStr2 = ' ';
            Integer InputStringsize2 = InputString2.length();

           for (Integer i =0; InputString2.length()>i; i++){
              
               revStr2 = InputString2.substring(0, InputString2.length()-i);
               
               If(allDestinationCodes.get(revStr2) != null){
                   
            li.Matched_HS_Code2__c = allDestinationCodes.get(revStr2);
            li.Matched_HS_Code_Description__c = destinationHsCodes.get(allDestinationCodes.get(revStr2)).Description__c;
            li.Rate__c = destinationHsCodes.get(allDestinationCodes.get(revStr2)).Rate__c;
            li.Part_Specific_Rate_1__c = destinationHsCodes.get(allDestinationCodes.get(revStr2)).Additional_Part_Specific_Tax_Rate_1__c;
            li.Part_Specific_Rate_2__c = destinationHsCodes.get(allDestinationCodes.get(revStr2)).Additional_Part_Specific_Tax_Rate_2__c;
            li.Part_Specific_Rate_3__c = destinationHsCodes.get(allDestinationCodes.get(revStr2)).Additional_Part_Specific_Tax_Rate_3__c;
            li.Vat_Exempted__c = destinationHsCodes.get(allDestinationCodes.get(revStr2)).Vat_Exempted__c;
            li.Specific_VAT_Rate__c = destinationHsCodes.get(allDestinationCodes.get(revStr2)).Specific_VAT_Rate__c;
            li.Default_Duty_Rate__c = rates.get(partDestination + 'Duties');
            li.VAT_Rate__c = rates.get(partDestination + 'VAT');
            li.VAT_Applied_To_Value__c = appliedTo.get(partDestination + 'VAT');
            li.Applied_To_Value__c = appliedTo.get(partDestination + 'Duties');
            li.Clearance_Country__c = clearanceCountry.get(partDestination);
             li.No_HS_Code_Match__c = FALSE;
             break;      
    
               }
               Else iF(allDestinationCodes.get(revStr2) == null){
               
            li.No_HS_Code_Match__c = TRUE;
            li.Default_Duty_Rate__c = rates.get(partDestination + 'Duties');
            li.VAT_Rate__c = rates.get(partDestination + 'VAT');
            li.VAT_Applied_To_Value__c = appliedTo.get(partDestination + 'VAT');
            li.Applied_To_Value__c = appliedTo.get(partDestination + 'Duties');
            li.Clearance_Country__c = clearanceCountry.get(partDestination);     
               }
               
              If((InputStringsize2-i)==4){break;}
                   }
 
        }
           
           
          If(matchClientParts.get(li.Search_Name__c) != null){
        
            system.debug('In First If ');
            li.Product__c = matchClientParts.get(li.Search_Name__c);
            li.Part_Matching_Method__c = 'Auto Matched';
              
             
            li.Manufacturer_Name__c = searchNameProducts.get(matchClientParts.get(li.Search_Name__c)).Manufacturer_Text_Formula__c;
            
            li.Part_Linked_to_Product_Date__c = date.today(); 
              
            
          }
    
          else If(matchClientParts.get(li.Search_Name__c) == null && matchAllParts.get(li.Search_Name__c) != null ){
           system.debug('In Second If ');
            li.Product__c = matchAllParts.get(li.Search_Name__c);
            li.Part_Matching_Method__c = 'Auto Matched';
            li.Manufacturer_Name__c = searchNameProducts.get(matchAllParts.get(li.Search_Name__c)).Manufacturer_Text_Formula__c;
            li.Part_Linked_to_Product_Date__c = date.today();  
            
             system.debug('matchAllParts.get(li.Search_Name__c) -- In If '  + matchAllParts.get(li.Search_Name__c));}
             
           
           
           else If(matchClientParts.get(li.Search_Name__c) == null && matchAllParts.get(li.Search_Name__c) == null && searchProducts.get(li.Search_Name__c) != NULL){
               
            li.Product__c = searchProducts.get(li.Search_Name__c); 
            li.Part_Matching_Method__c = 'Auto Matched';
            li.Manufacturer_Name__c = searchNameProducts.get(searchProducts.get(li.Search_Name__c)).Manufacturer_Text_Formula__c;
            li.Part_Linked_to_Product_Date__c = date.today();  
           }
           
    
            else If(matchClientParts.get(li.Search_Name__c) == null && matchAllParts.get(li.Search_Name__c) == null && searchProducts.get(li.Search_Name__c) == NULL && aliasList.get(li.Search_Name__c) != NULL){
            system.debug('In Last If ');
      
 
             li.Product__c = aliasList.get(li.Search_Name__c); 
             li.Part_Matching_Method__c = 'Auto Matched';
             li.Manufacturer_Name__c = searchNameProducts.get(aliasList.get(li.Search_Name__c)).Manufacturer_Text_Formula__c;
             li.Part_Linked_to_Product_Date__c = date.today(); 
             System.debug('aliasList-->'+aliasList.containsKey(li.Search_Name__c));
          
}
            //v2 block start
           else if(matchClientParts.get(li.Search_Name__c) == null && matchAllParts.get(li.Search_Name__c) == null && searchProducts.get(li.Search_Name__c) == NULL && aliasList.get(li.Search_Name__c) == NULL){
              System.debug('Inside V2 Block');
               li.Start_V2_Product_Match__c = TRUE; 
                Boolean V2Completed = FALSE;
      
        List<String> Finalmatch= new List<String>();
            
       
           
         String s = li.Name+' '+li.Description_and_Functionality__c; 
         String S1 = s.toUpperCase();
           System.debug('-- Actual String--'+S1);
           
          List<Product_matching_Rule__c> PMR = [select id,name,Contains__c,Does_Not_Contains__c,Product_to_be_match__c from Product_matching_Rule__c where Rule_Active__c = TRUE and Rule_Version__c = 'V2'  ];
        Boolean Productmatched = False;
             
         For(Integer i=0; PMR.size()>i;i++){ 
             
        Boolean Allcontainsmatched = FALSE;
        Boolean Doesnotcontainsmatched = FALSE;
        String Producttobeupdated = 'asdf';
       
             
             System.debug('-- Rule Iteration --'+i); 
              List<String> ContainsList = new List<String>();
              List<String> DoesnotContainsList = new List<String>(); 
             
              List<String> ConSplittedbyComma = new List<String>();
              List<String> DoesNotConSplittedbyComma = new List<String>();
             
          /*   System.debug('-- Display List values and size at initiation ---');
             System.debug('-- ListName // List size // List values --');
             System.debug('ContainsList--> //'+ContainsList.size()+' //'+ContainsList);
             System.debug('ConSplittedbyComma--> //'+ConSplittedbyComma.size()+' //'+ConSplittedbyComma);
             System.debug('DoesnotContainsList--> //'+DoesnotContainsList.size()+' //'+DoesnotContainsList);
             System.debug('DoesNotConSplittedbyComma--> //'+DoesNotConSplittedbyComma.size()+' //'+DoesNotConSplittedbyComma);
            */    
                // All words must contain
                 System.debug('-- Contains started--');
                        String Con = PMR[i].Contains__c;
                     String Con1 = Con.toUpperCase();   
                     ConSplittedbyComma = Con1.split(',');
                      System.debug('-- All words must contain-->'+ConSplittedbyComma);
             
              If(PMR[i].Does_Not_Contains__c !=null  || !String.isBlank(PMR[i].Does_Not_Contains__c) ){
                 String notCon = PMR[i].Does_Not_Contains__c;
                     String notCon1 = notCon.toUpperCase(); 
                   DoesNotConSplittedbyComma = notCon1.split(',');
              }
                    System.debug('-- any one of these word must not contain-->'+DoesNotConSplittedbyComma);
             
                   for (String Constr : ConSplittedbyComma){
                           System.debug('-- Constr--'+Constr);  
                               if(S1.contains(Constr)){
                                ContainsList.add(Constr);// Add to Contains list
                                                    }      
                           
                                for (String NotConstr : DoesNotConSplittedbyComma){
                                      System.debug('-- NotConstr--'+NotConstr);
                                         if(S1.contains(NotConstr))    {
                                            DoesnotContainsList.add(NotConstr);// Add to Contains list
                                            }
                              }
                               
                            
                        }
             
      /*      System.debug('-- Display List values and size after ---');
             System.debug('-- ListName // List size // List values --');
             System.debug('ContainsList--> //'+ContainsList.size()+' //'+ContainsList);
             System.debug('ConSplittedbyComma--> //'+ConSplittedbyComma.size()+' //'+ConSplittedbyComma);
             System.debug('DoesnotContainsList--> //'+DoesnotContainsList.size()+' //'+DoesnotContainsList);
             System.debug('DoesNotConSplittedbyComma--> //'+DoesNotConSplittedbyComma.size()+' //'+DoesNotConSplittedbyComma);
        */    
             IF((ConSplittedbyComma.size() == ContainsList.size() ) && ContainsList.size() > 0) { Allcontainsmatched =TRUE; }
             IF( DoesnotContainsList.size() > 0) { Doesnotcontainsmatched =FALSE; } Else if(PMR[i].Does_Not_Contains__c ==null) {Doesnotcontainsmatched =TRUE;} else{Doesnotcontainsmatched =TRUE;}
           //    System.debug('Allcontainsmatched-->'+Allcontainsmatched);
           //    System.debug('Doesnotcontainsmatched-->'+Doesnotcontainsmatched);
               
             IF(( Allcontainsmatched == TRUE && Doesnotcontainsmatched == TRUE && PMR[i].Does_Not_Contains__c !=null )  ){
                 
                  ProductMatched = TRUE;
                  li.Start_V2_Product_Match__c = TRUE;
                  li.product__C = PMR[i].Product_to_be_match__c;
                  li.Part_Matching_Method__c = 'Auto Matched';
                  li.Matched_Rule__c =  PMR[i].Name;
                  System.debug('Matched Rule-->'+PMR[i].Name);
                 V2Completed = TRUE;
                 
                }
              else IF(  Allcontainsmatched == TRUE && Doesnotcontainsmatched == TRUE && PMR[i].Does_Not_Contains__c ==null ){
                  li.Start_V2_Product_Match__c = TRUE;  
                  li.product__C = PMR[i].Product_to_be_match__c;
                  li.Part_Matching_Method__c = 'Auto Matched';
                  li.New_Product__c = TRUE;
                  li.Matched_Rule__c =  PMR[i].Name;
                  ProductMatched = TRUE;
                  System.debug('Matched Rule-->'+PMR[i].Name);
                 V2Completed = TRUE;
                }
             else if((Allcontainsmatched == FALSE || Doesnotcontainsmatched == FALSE) && i==PMR.size()-1 && V2Completed == FALSE){
               
               li.Start_V3_Product_Match__c = TRUE;  
               li.New_Product__c = TRUE;
               list<Part__c> partIds = New List<Part__c>(); // Added Anil 08June2020
               PartIds.add(Li); // Added Anil 08June2020
                 
                 //V3 Part matcing start
              PartMatchingV3.MapTest(partIds);
               //V3 Part Matching End 
               
             }
             
             
             
               
               }
           
             
                   
          
       }
          // v2 block end 
          
           Else{
               
             li.New_Product__c = TRUE;  
               
           }
      
           
           li.All_Matching_Complete__c = TRUE;
           li.CPA_Changed__c = FALSE;
           li.Account_Text__c = li.AccountId__c;
           li.Ship_To_Country_Text__c = li.Ship_To_Country__c; 
          
           productIdList.add(li.Product__c);
           
           
              
    
        }
        
        
        catch(Exception e){
            System.Debug('Error setting region.' +e);
        }  
    
         
     }
      
      system.debug('productIdList--->'+productIdList);
      system.debug('productIdList.size()--->'+productIdList.size());

  
       
      
      
      
  }
    
    
  If(Trigger.IsInsert && Trigger.IsAfter){
  
              Decimal lineItemTotal=0;    
              Decimal hsCodes=0;          
              Decimal cif=0;              
              Decimal fob=0;             
              Decimal totalValue=0; 
              Decimal Qty1=0; //Added by Anil 01062020
              String SOECCNNo='NONE'; //Added by Anil 01062020 
              String  MnfName='NONE'; //Added by Anil 01062020
              String FinQty1='NONE'; //Added by Anil 01062020
              String  FinMnfName='NONE'; //Added by Anil 01062020
      
    for (Part__c li : Trigger.new) {
        partIds.add(li.Id);
        accountId.add(li.AccountId__c);
        hsCode.add(li.US_HTS_Code__c);
        system.debug('hsCode---> '+ hsCode);
        hsCode4.add(li.US_HTS_Code__c.left(4)+'%');
        usHsCode8.add(li.US_HTS_Code__c.left(8));
        clearanceDestination.add(li.Ship_To_Country__c);
        partName.add(li.Search_Name__c);
        SoIds.add(li.Shipment_Order__c);
        productId.add(li.Product__c);
        //Added by Anil 01062020 start
        If(!(String.isBlank(li.Eccn_No__c)) && li.Eccn_No__c.contains('5A002')){ SOECCNNo='5A002';}
        else If(!(String.isBlank(li.Eccn_No__c)) && li.Eccn_No__c.contains('5D002')){ SOECCNNo='5D002';}
        else { SOECCNNo='NONE';}
        //Added by Anil 01062020 end
    
    }
      
    System.Debug('productId----->' +productId);  
   List<Part__c> partsToUpdate =[Select Id, All_SOs_Times_Shipped__c, All_SOs_Average_Unit_Price__c, All_SOs_Max_Unit_Price__c, All_SOs_Min_Unit_Price__c,
                                    Destination_SOs_Times_Shipped__c, Destination_SOs_Average_Unit_Price__c, Destination_SOs_Max_Unit_Price__c, Destination_SOs_Min_Unit_Price__c,
                                    Client_SOs_Times_Shipped__c, Client_SOs_Average_Unit_Price__c, Client_SOs_Max_Unit_Price__c, Client_SOs_Min_Unit_Price__c, 
                                    All_SOs_Last_Shipment_Date__c, Client_SOs_Last_Shipment_Date__c, Destination_SOs_Last_Shipment_Date__c, All_SOs_Most_Shipped_HS__c,
                                    All_SOs_Most_COO__c, Product__c, Ship_To_Country_Text__c, Account_Text__c, Shipment_Order__c
                                    FROM Part__c
                                    WHERE id IN :partIds];
        
  Map<Id, shipment_Order__c> shipmentOrder = new Map<Id, shipment_Order__c>([Select Id, Name, Total_CIF_Duties__c, Total_Line_Item_Extended_Value__c, Total_FOB_Duties__c,
                                              Total_CIF_1_Duties__c, of_Unique_Line_Items__c, of_Line_Items__c, ECCN_No__c, Manufacturer__c, Sum_of_quantity_of_matched_products__c  
                                              from Shipment_Order__c 
                                              where id IN :SoIds ]);
      
      System.Debug('shipmentOrder----->' +shipmentOrder);
      System.Debug('shipmentOrder.size()----->' +shipmentOrder.size());
      
     List<shipment_Order__c> shipmentsToUpdate = new List<shipment_Order__c>();
     List<shipment_Order__c> finalShipmentsToUpdate = new List<shipment_Order__c>();
     Set<shipment_Order__c> shipmentsSet = new Set<shipment_Order__c>();
     List<Part__c> partUpdate = new List<Part__c>(); 
     List<Part__c> FinalpartUpdate = new List<Part__c>(); 
     Set<Part__c> partUpdateSet = new Set<Part__c>(); 
      
      
             
             
     List<AggregateResult> allParts = [SELECT Shipment_Order__c, Manufacturer_Name__c, SUM(Total_Value__C) total, Count_Distinct(US_HTS_Code__c) hsCodes, Sum(CIF_Duties__c) sumCIF,
                                             Count(Id) numberParts, sum(FOB_Duties__c) sumFOB,Sum(Quantity__C) qty 
                                             From Part__c 
                                             where Shipment_Order__c =:SoIds 
                                             GROUP BY Rollup(Shipment_Order__c, Manufacturer_Name__c)];   //Modified with Groupby - Anil 01062020 
       
      
     
          
      for(AggregateResult a : allParts){
          
         Id thisSOSId = string.valueOf(a.get('Shipment_Order__c'));
         Shipment_Order__c thisSO = new Shipment_Order__c(Id = thisSOSId);
          
          
          if(shipmentOrder.containskey(thisSOSId)) 
                {
                    thisSO = shipmentOrder.get(thisSOSId);
                    
                }   
      
         // String shipmentId = (string) a.get('Shipment_Order__c');
          Qty1 = (Decimal) a.get('qty');
          MnfName =(string) a.get('Manufacturer_Name__c');
         // Shipment_Order__c shipment2 = shipmentOrder.get(shipmentId); 
            if(MnfName=='HPE' && Qty1 >=5){ FinQty1 ='>=5';FinMnfName=MnfName;}
            else if(MnfName=='Apple' && Qty1 >=4){ FinQty1 ='>=4';FinMnfName=MnfName;}
            else if(MnfName=='Cisco'){FinMnfName=MnfName;}
            else if(MnfName=='HPE'){FinMnfName=MnfName;}
            else if(MnfName=='HP'){FinMnfName=MnfName;}
          

          
          
            thisSO.of_Line_Items__c = (Decimal) a.get('numberParts');
            thisSO.of_Unique_Line_Items__c = (Decimal) a.get('hsCodes');
            thisSO.Total_CIF_Duties__c = (Decimal) a.get('sumCIF');
            thisSO.Total_FOB_Duties__c = (Decimal) a.get('sumFOB');
            thisSO.Total_Line_Item_Extended_Value__c = (Decimal) a.get('total');
            thisSO.Shipment_Value_USD__c = (Decimal) a.get('total');
          
            
            If(String.isBlank(thisSO.ECCN_No__c) || thisSO.ECCN_No__c.contains('NONE')){ thisSO.ECCN_No__c =SOECCNNo;} //Added by Anil 01062020 
            If(String.isBlank(thisSO.Manufacturer__c) || thisSO.Manufacturer__c.contains('NONE')){ thisSO.Manufacturer__c =FinMnfName;} //Added by Anil 01062020 
            If(String.isBlank(thisSO.Sum_of_quantity_of_matched_products__c) || thisSO.Sum_of_quantity_of_matched_products__c.contains('NONE')){ thisSO.Sum_of_quantity_of_matched_products__c =FinQty1;} //Added by Anil 01062020 
            
          If(shipmentOrder.get(thisSOSId) != null && !shipmentsToUpdate.contains(shipmentOrder.get(thisSOSId))){
            shipmentsToUpdate.add(shipmentOrder.get(thisSOSId));
          
      
      } 
      }     

      update shipmentsToUpdate;
      
     
       

     
             
         }
    
    
    
     
 If(Trigger.IsUpdate && Trigger.IsBefore){ 
     
     Part__c oldPart;
     Part__c lo;
     
     for (Part__c li : Trigger.new) {
     oldPart = Trigger.oldMap.get(li.Id); 
     lo = li;
     partIds.add(li.Id);
     accountId.add(li.AccountId__c);
     hsCode.add(li.US_HTS_Code__c);   
     hsCode4.add(li.US_HTS_Code__c.left(4)+'%');
     usHsCode8.add(li.US_HTS_Code__c.left(8));
     clearanceDestination.add(li.Ship_To_Country__c);
     partName.add(li.Search_Name__c);
     partName2.add(li.Name);
     SoIds.add(li.Shipment_Order__c);
     productId.add(li.Product__c);}
     
     
     
     
   If(lo.US_HTS_Code__c != oldPart.US_HTS_Code__c || lo.CPA_Changed__c == TRUE){
       
   Map<Id, shipment_Order__c> shipmentOrder = new Map<Id,shipment_Order__c>([Select Id, CPA_v2_0__r.Final_Destination__c 
                                           from Shipment_Order__c 
                                           where id IN :SoIds]); 
     System.Debug('shipmentOrder----->' + shipmentOrder);
     
     
     //Map<Id, Product2> searchNameProducts = new Map<Id, Product2>( [select Id, Search_Name__c, Manufacturer_Text_Formula__c, Product_Name_Alias__c
     //                                                               from Product2]);
   
   Map<Id, HS_Codes_and_Associated_Details__c> usHsCodes = new Map<Id, HS_Codes_and_Associated_Details__c>( [select Id, Name, Additional_Part_Specific_Tax_Rate_1__c, Additional_Part_Specific_Tax_Rate_2__c,
                                                                                                               Additional_Part_Specific_Tax_Rate_3__c, Country_Name__c, Description__c, Destination_HS_Code__c, Rate__c,
                                                                                                               Specific_VAT_Rate__c, US_HTS_Code__c, Vat_Exempted__c
                                                                                                              from HS_Codes_and_Associated_Details__c 
                                                                                                               where US_HTS_Code__c Like :hsCode4
                                                                                                               and Country_Name__c =: clearanceDestination
                                                                                                               and Inactive__c = FALSE
                                                                                                               Order by Rate__c desc limit 50000]);
    
     Map<String, Id> allUSCodes = new Map<String, Id>();
    
     For (Id getAllCodes : usHsCodes.keySet()) { 
         
        String usCode = usHsCodes.get(getAllCodes).Country_Name__c + usHsCodes.get(getAllCodes).US_HTS_Code__c;
      
         If(!allUSCodes.containsKey(usCode)){   
         allUSCodes.put(usHsCodes.get(getAllCodes).Country_Name__c + usHsCodes.get(getAllCodes).US_HTS_Code__c, getAllCodes); }
           
       else if(usHsCodes.get(getAllCodes).Rate__c > usHsCodes.get(allUSCodes.get(usCode)).Rate__c){
         
       allUSCodes.remove(usCode); 
       allUSCodes.put(usHsCodes.get(getAllCodes).Country_Name__c + usHsCodes.get(getAllCodes).US_HTS_Code__c, getAllCodes);           
       } 
         
     }
    
    system.debug('allUSCodes.size()---> '+ allUSCodes.size());
    system.debug('allUSCode---> '+ allUSCodes);
    
     Map<Id, HS_Codes_and_Associated_Details__c> destinationHsCodes = new Map<Id, HS_Codes_and_Associated_Details__c>([select Id, Name, Additional_Part_Specific_Tax_Rate_1__c, Additional_Part_Specific_Tax_Rate_2__c,
                                                                                                               Additional_Part_Specific_Tax_Rate_3__c, Country_Name__c, Description__c, Destination_HS_Code__c, Rate__c,
                                                                                                               Specific_VAT_Rate__c, US_HTS_Code__c, Vat_Exempted__c
                                                                                                               from HS_Codes_and_Associated_Details__c 
                                                                                                               where Destination_HS_Code__c Like :hsCode4
                                                                                                               and Country_Name__c =: clearanceDestination
                                                                                                               and Inactive__c = FALSE
                                                                                                               Order by Rate__c desc limit 50000]);
    
    
    
    
    Map<String, Id> allDestinationCodes = new Map<String, Id>();
    Map<String, String> destinationCodeTrunc = new Map<String, String>();
    
      For (Id getAllDestCodes : destinationHsCodes.keySet()) { 
         
          String destCode = destinationHsCodes.get(getAllDestCodes).Country_Name__c + destinationHsCodes.get(getAllDestCodes).Destination_HS_Code__c;
         
           If(!allDestinationCodes.containsKey(destCode)){
           allDestinationCodes.put(destinationHsCodes.get(getAllDestCodes).Country_Name__c + destinationHsCodes.get(getAllDestCodes).Destination_HS_Code__c, getAllDestCodes);
                
            }
         
            String InputString = destinationHsCodes.get(getAllDestCodes).Destination_HS_Code__c;
            String revStr = ' ';
            Integer InputStringsize = InputString.length();

       for (Integer i =0; InputString.length()>i; i++){
            revStr = InputString.substring(0, InputString.length()-i);
              
           If(!allDestinationCodes.containsKey(destinationHsCodes.get(getAllDestCodes).Country_Name__c + revStr)){
              allDestinationCodes.put(destinationHsCodes.get(getAllDestCodes).Country_Name__c + revStr, getAllDestCodes);   
           }
           
           else if(destinationHsCodes.get(getAllDestCodes).Rate__c > destinationHsCodes.get(allDestinationCodes.get(destinationHsCodes.get(getAllDestCodes).Country_Name__c + revStr)).Rate__c){
           
           allDestinationCodes.remove(destinationHsCodes.get(getAllDestCodes).Country_Name__c + revStr);
           allDestinationCodes.put(destinationHsCodes.get(getAllDestCodes).Country_Name__c + revStr, getAllDestCodes);
               
               
           }
           
           
          If((InputStringsize-i)==4){break;}
}
            
           
       }  

    
    system.debug('allDestinationCodes.size()---> '+ allDestinationCodes.size());
    system.debug('allDestinationCodes---> '+ allDestinationCodes);
    
    
    
    Map<Id, Tax_Structure_Per_Country__c> taxes = new Map<Id, Tax_Structure_Per_Country__c>([select Id, Name, Applied_to_Value__c,Tax_Type__c, Default_Duty_Rate__c, Country__r.Name,
                                                                                             Country__c, Rate__c
                                                                                            from Tax_Structure_Per_Country__c
                                                                                            where Country__r.Name in :clearanceDestination]);
    
     Map<String, Id> clearanceCountry = new Map<String, Id>();
      
     Map<String, Decimal> rates = new Map<String, Decimal>();
     Map<String, String> appliedTo = new Map<String, String>();
    
    For (Id getDefaults : taxes.keySet()) { 
        
       clearanceCountry.put(taxes.get(getDefaults).Country__r.Name, taxes.get(getDefaults).Country__c);
        
         appliedTo.put(taxes.get(getDefaults).Country__r.Name + taxes.get(getDefaults).Tax_Type__c, taxes.get(getDefaults).Applied_to_Value__c);  

        
        If(taxes.get(getDefaults).Tax_Type__c != 'Duties'){
           rates.put(taxes.get(getDefaults).Country__r.Name + taxes.get(getDefaults).Tax_Type__c, taxes.get(getDefaults).Rate__c);           
       } 
        
        Else{
            rates.put(taxes.get(getDefaults).Country__r.Name + taxes.get(getDefaults).Tax_Type__c, taxes.get(getDefaults).Default_Duty_Rate__c);  
            
        }
        
    }

    
  for (Part__c li : Trigger.new) {
     
       try { 
           
        String partDestination = li.Ship_To_Country__c;
        String searchCode = partDestination + li.US_HTS_Code__c;
        String usSearchCode = partDestination + li.US_HTS_Code__c.left(8);
        Integer searchCodeLength = searchCode.length();
        
        
        If(allUSCodes.get(usSearchCode) != null){ 
            
             system.debug('In First Search If ');
            
            li.Matched_HS_Code2__c = allUSCodes.get(usSearchCode);
            li.Matched_HS_Code_Description__c = usHsCodes.get(allUSCodes.get(usSearchCode)).Description__c;
            li.Rate__c = usHsCodes.get(allUSCodes.get(usSearchCode)).Rate__c;
            li.Part_Specific_Rate_1__c = usHsCodes.get(allUSCodes.get(usSearchCode)).Additional_Part_Specific_Tax_Rate_1__c;
            li.Part_Specific_Rate_2__c = usHsCodes.get(allUSCodes.get(usSearchCode)).Additional_Part_Specific_Tax_Rate_2__c;
            li.Part_Specific_Rate_3__c = usHsCodes.get(allUSCodes.get(usSearchCode)).Additional_Part_Specific_Tax_Rate_3__c;
            li.Vat_Exempted__c = usHsCodes.get(allUSCodes.get(usSearchCode)).Vat_Exempted__c;
            li.Specific_VAT_Rate__c = usHsCodes.get(allUSCodes.get(usSearchCode)).Specific_VAT_Rate__c;
            li.Default_Duty_Rate__c = rates.get(partDestination + 'Duties');
            li.VAT_Rate__c = rates.get(partDestination + 'VAT');
            li.VAT_Applied_To_Value__c = appliedTo.get(partDestination + 'VAT');
            li.Applied_To_Value__c = appliedTo.get(partDestination + 'Duties');
            li.Clearance_Country__c = clearanceCountry.get(partDestination);
            li.No_HS_Code_Match__c = FALSE;
            li.CPA_Changed__c = FALSE;
        }
        
        Else If(allUSCodes.get(usSearchCode) == null && allDestinationCodes.get(searchCode) != null){
            
             system.debug('In Second Search If ');
            
            li.Matched_HS_Code2__c = allDestinationCodes.get(searchCode);
            li.Matched_HS_Code_Description__c = destinationHsCodes.get(allDestinationCodes.get(searchCode)).Description__c;
            li.Rate__c = destinationHsCodes.get(allDestinationCodes.get(searchCode)).Rate__c;
            li.Part_Specific_Rate_1__c = destinationHsCodes.get(allDestinationCodes.get(searchCode)).Additional_Part_Specific_Tax_Rate_1__c;
            li.Part_Specific_Rate_2__c = destinationHsCodes.get(allDestinationCodes.get(searchCode)).Additional_Part_Specific_Tax_Rate_2__c;
            li.Part_Specific_Rate_3__c = destinationHsCodes.get(allDestinationCodes.get(searchCode)).Additional_Part_Specific_Tax_Rate_3__c;
            li.Vat_Exempted__c = destinationHsCodes.get(allDestinationCodes.get(searchCode)).Vat_Exempted__c;
            li.Specific_VAT_Rate__c = destinationHsCodes.get(allDestinationCodes.get(searchCode)).Specific_VAT_Rate__c;
            li.Default_Duty_Rate__c = rates.get(partDestination + 'Duties');
            li.VAT_Rate__c = rates.get(partDestination + 'VAT');
            li.VAT_Applied_To_Value__c = appliedTo.get(partDestination + 'VAT');
            li.Applied_To_Value__c = appliedTo.get(partDestination + 'Duties');
            li.Clearance_Country__c = clearanceCountry.get(partDestination);
            li.No_HS_Code_Match__c = FALSE;
            li.CPA_Changed__c = FALSE;}
        
        
        
        Else if(allUSCodes.get(usSearchCode) == null && allDestinationCodes.get(searchCode) == null){
            
            system.debug('In Third Search If ');
            
            String InputString2 = searchCode;
            String revStr2 = ' ';
            Integer InputStringsize2 = InputString2.length();

           for (Integer i =0; InputString2.length()>i; i++){
              
               revStr2 = InputString2.substring(0, InputString2.length()-i);
               
               If(allDestinationCodes.get(revStr2) != null){
                   
            li.Matched_HS_Code2__c = allDestinationCodes.get(revStr2);
            li.Matched_HS_Code_Description__c = destinationHsCodes.get(allDestinationCodes.get(revStr2)).Description__c;
            li.Rate__c = destinationHsCodes.get(allDestinationCodes.get(revStr2)).Rate__c;
            li.Part_Specific_Rate_1__c = destinationHsCodes.get(allDestinationCodes.get(revStr2)).Additional_Part_Specific_Tax_Rate_1__c;
            li.Part_Specific_Rate_2__c = destinationHsCodes.get(allDestinationCodes.get(revStr2)).Additional_Part_Specific_Tax_Rate_2__c;
            li.Part_Specific_Rate_3__c = destinationHsCodes.get(allDestinationCodes.get(revStr2)).Additional_Part_Specific_Tax_Rate_3__c;
            li.Vat_Exempted__c = destinationHsCodes.get(allDestinationCodes.get(revStr2)).Vat_Exempted__c;
            li.Specific_VAT_Rate__c = destinationHsCodes.get(allDestinationCodes.get(revStr2)).Specific_VAT_Rate__c;
           li.Default_Duty_Rate__c = rates.get(partDestination + 'Duties');
            li.VAT_Rate__c = rates.get(partDestination + 'VAT');
            li.VAT_Applied_To_Value__c = appliedTo.get(partDestination + 'VAT');
            li.Applied_To_Value__c = appliedTo.get(partDestination + 'Duties');
            li.Clearance_Country__c = clearanceCountry.get(partDestination);
            li.No_HS_Code_Match__c = FALSE;
            li.CPA_Changed__c = FALSE;
             break;      
    
               }
               Else{
               
            li.No_HS_Code_Match__c = TRUE;
            li.Matched_HS_Code2__c = null;
            li.Matched_HS_Code_Description__c = null;
            li.Rate__c = null;
            li.Default_Duty_Rate__c = rates.get(partDestination + 'Duties');
            li.VAT_Rate__c = rates.get(partDestination + 'VAT');
            li.VAT_Applied_To_Value__c = appliedTo.get(partDestination + 'VAT');
            li.Applied_To_Value__c = appliedTo.get(partDestination + 'Duties');
            li.Clearance_Country__c = clearanceCountry.get(partDestination); 
            li.CPA_Changed__c = FALSE;
               }
               
              If((InputStringsize2-i)==4){break;}
                   }
 
        }
        
           li.Account_Text__c = li.AccountId__c;
           li.Ship_To_Country_Text__c = li.Ship_To_Country__c;
        }
        
        
        catch(Exception e){
            System.Debug('Error setting region.' +e);
        }

}

}  
     
     
     If(lo.Name != oldPart.Name || lo.CPA_Changed__c == true){
         
    Map<Id, shipment_Order__c> shipmentOrder = new Map<Id,shipment_Order__c>([Select Id, CPA_v2_0__r.Final_Destination__c 
                                           from Shipment_Order__c 
                                           where id IN :SoIds]); 
     System.Debug('shipmentOrder----->' + shipmentOrder);
     
     
     Map<Id, Product2> searchNameProducts = new Map<Id, Product2>( [select Id, Search_Name__c, Manufacturer_Text_Formula__c, Product_Name_Alias__c
                                                                    from Product2]);
   
   
    // Client match final map
    Map<String, Id> matchClientParts = new Map<String, Id>();
     Map<String, Id> matchAllParts = new Map<String, Id>();
    
    // Client match initial map
    Map<Id, Part__c> clientProducts = new Map<Id, Part__c>( [select Id, Product__c, Search_Name__c from Part__c 
                                                       where Search_Name__c in :partName
                                                       and Product__c != null
                                                       and AccountId__c in :accountId]);
    
     system.debug('clientProducts.size()---> '+ clientProducts.size());
    
        For (Id getClientParts : clientProducts.keySet()) {   
           matchClientParts.put(clientProducts.get(getClientParts).Search_Name__c, clientProducts.get(getClientParts).Product__c );           
       } 
    
    
    system.debug('matchClientParts.size()---> '+ matchClientParts.size());
    
    
    
    
    Map<Id, Part__c> allProducts = new Map<Id, Part__c>( [select Id, Product__c, Search_Name__c from Part__c 
                                                       where Search_Name__c in :partName
                                                       and Product__c != null]);
    
    system.debug('allProducts.size()---> '+ allProducts.size());
    
    For (Id getAllParts : allProducts.keySet()) {   
           matchAllParts.put(allProducts.get(getAllParts).Search_Name__c, allProducts.get(getAllParts).Product__c);           
       } 
    
    system.debug('matchAllParts.size()---> '+ matchAllParts.size());

    
    Map<String, Id> aliasList = new Map<String, Id>();
    Map<String, Id> searchProducts = new Map<String, Id>();   
         
         
       For (Id getProductsBySearch : searchNameProducts.keySet()) {   
           
           searchProducts.put(searchNameProducts.get(getProductsBySearch).Search_Name__c, getProductsBySearch); 
           
           String alias = searchNameProducts.get(getProductsBySearch).Product_Name_Alias__c;
           String aliasId = getProductsBySearch;
            
            If(alias != null){
            
               List<String> mapAlias = alias.split(',');
            
            for (Integer i = 0; i < mapAlias.size(); i++ ){
                
                aliasList.put(mapAlias[i], getProductsBySearch);
                
            }
       
       }  
           
           
       } 
         
    
    system.debug('searchProducts.size()---> '+ searchProducts.size());
    system.debug('aliasList.size()---> '+ aliasList.size());
    system.debug('aliasList---> '+ aliasList);
         
         
    
  
  List<AggregateResult> allSoParts = [SELECT Name, AVG(Commercial_Value__c) average, MIN(Commercial_Value__c) minimum, MAX(Commercial_Value__c) maximum,
                                             Count(Id) numberParts, Max(Shipment_Order__r.POD_Date__c) maxPOD, MAX(US_HTS_Code__c) maxHSCode, max(Country_of_Origin2__c) maxCOO
                                             From Part__c 
                                             where Product__C IN : productId
                                             and (Shipment_Order__r.Shipping_Status__c = 'POD Received'
                                             Or Shipment_Order__r.Shipping_Status__c = 'Customs Clearance Docs Received')
                                             GROUP BY Name];   
         
            system.debug('allSoParts---> '+ allSoParts);
      
            for(AggregateResult a : allSoParts){
                allSOAverage = (Decimal) a.get('average');
                allSOMin = (Decimal) a.get('minimum');
                allSOMax = (Decimal) a.get('maximum');
                allSOShipped = (Decimal) a.get('numberParts'); 
                allSOLastPOD = (Date) a.get('maxPOD');
                maxAllHSCode = (String) a.get('maxHSCode');
                maxAllCOO = (String) a.get('maxCOO');
            }
      
      
       List<AggregateResult> allDestinationParts = [SELECT Name, AVG(Commercial_Value__c) average, MIN(Commercial_Value__c) minimum, MAX(Commercial_Value__c) maximum,
                                             Count(Id) numberParts, Max(Shipment_Order__r.POD_Date__c) maxPOD, MAX(US_HTS_Code__c) maxHSCode
                                             From Part__c 
                                             where Product__C IN : productId
                                             and (Shipment_Order__r.Shipping_Status__c = 'POD Received'
                                             Or Shipment_Order__r.Shipping_Status__c = 'Customs Clearance Docs Received')
                                             and Ship_To_Country__c =: clearanceDestination
                                             GROUP BY Name];
            
            
            for(AggregateResult a : allDestinationParts){
                destinationSOAverage = (Decimal) a.get('average');
                destinationSOMin = (Decimal) a.get('minimum');
                destinationSOMax = (Decimal) a.get('maximum');
                destinationSOShipped = (Decimal) a.get('numberParts'); 
                destinationSOLastPOD = (Date) a.get('maxPOD');
                maxdestHSCode = (String) a.get('maxHSCode');
            }
      
      
     List<AggregateResult> allClientParts = [SELECT Name, AVG(Commercial_Value__c) average, MIN(Commercial_Value__c) minimum, MAX(Commercial_Value__c) maximum,
                                             Count(Id) numberParts, Max(Shipment_Order__r.POD_Date__c) maxPOD
                                             From Part__c 
                                             where Product__C IN : productId
                                             and (Shipment_Order__r.Shipping_Status__c = 'POD Received'
                                             Or Shipment_Order__r.Shipping_Status__c = 'Customs Clearance Docs Received')
                                             and Shipment_Order__r.Account__c =:accountId
                                             GROUP BY Name];      
            
            
             for(AggregateResult a : allClientParts){
                clientSOAverage = (Decimal) a.get('average');
                clientSOMin = (Decimal) a.get('minimum');
                clientSOMax = (Decimal) a.get('maximum');
                clientSOShipped = (Decimal) a.get('numberParts'); 
                clientSOLastPOD = (Date) a.get('maxPOD');
            }  
      
  
  for (Part__c li : Trigger.new) {
     
       try { 
        
               system.debug(' matchClientParts.get(li.Search_Name__c)---> '+  matchClientParts.get(li.Search_Name__c));
               system.debug(' matchAllParts.get(li.Search_Name__c)---> '+  matchAllParts.get(li.Search_Name__c));
               system.debug(' aliasList.get(li.Search_Name__c)---> '+  aliasList.get(li.Search_Name__c));
               system.debug(' aliasList.get(li.Search_Name__c)---> '+  aliasList.get(li.Search_Name__c));
        
       
          If(matchClientParts.get(li.Search_Name__c) != null){
        
           li.Product__c = matchClientParts.get(li.Search_Name__c); 
           li.Part_Matching_Method__c = 'Auto Matched';
           li.Manufacturer_Name__c = searchNameProducts.get(matchClientParts.get(li.Search_Name__c)).Manufacturer_Text_Formula__c;
           li.Part_Linked_to_Product_Date__c = date.today();
            
          }
    
          else If(matchClientParts.get(li.Search_Name__c) == null && matchAllParts.get(li.Search_Name__c) != null ){
           system.debug('In Second If ');
            li.Product__c = matchAllParts.get(li.Search_Name__c);
            li.Part_Matching_Method__c = 'Auto Matched';
            li.Manufacturer_Name__c = searchNameProducts.get(matchAllParts.get(li.Search_Name__c)).Manufacturer_Text_Formula__c;
            li.Part_Linked_to_Product_Date__c = date.today();
          }
           
           else If(matchClientParts.get(li.Search_Name__c) == null && matchAllParts.get(li.Search_Name__c) == null && searchProducts.get(li.Search_Name__c) != NULL){
               
            li.Product__c = searchProducts.get(li.Search_Name__c);  
            li.Part_Matching_Method__c = 'Auto Matched';
            li.Manufacturer_Name__c = searchNameProducts.get(searchProducts.get(li.Search_Name__c)).Manufacturer_Text_Formula__c;
            li.Part_Linked_to_Product_Date__c = date.today();
           }
           
    
            else If(matchClientParts.get(li.Search_Name__c) == null && matchAllParts.get(li.Search_Name__c) == null && searchProducts.get(li.Search_Name__c) == NULL && aliasList.get(li.Search_Name__c) != NULL){
            system.debug('In Last If ');
      
 
             li.Product__c = aliasList.get(li.Search_Name__c); 
             li.Part_Matching_Method__c = 'Auto Matched';
             li.Manufacturer_Name__c = searchNameProducts.get(aliasList.get(li.Search_Name__c)).Manufacturer_Text_Formula__c;
             li.Part_Linked_to_Product_Date__c = date.today();
          
}
    /*      //New block start
           else if(matchClientParts.get(li.Search_Name__c) == null && matchAllParts.get(li.Search_Name__c) == null && searchProducts.get(li.Search_Name__c) == NULL && aliasList.get(li.Search_Name__c) == NULL){
              System.debug('Inside V2 Block');
      
         List<String> DescList = new List<String>();
         
         MAP<String,ID> ContainsMAP = new MAP<String,ID>();
         List<String> ContainsList = new List<String>();
         
         MAP<String,ID> DoesnotContainsMAP = new MAP<String,ID>();
         List<String> DoesnotContainsList = new List<String>();
         
         
         MAP<String,ID> ProductIDMAP = new MAP<String,ID>();
        List<String> Finalmatch= new List<String>();
       
        
         
        
         
         String s = li.Name+' '+li.Description_and_Functionality__c; 
         List<String> SplittedbySpace = s.split(' ');
         for (String str : SplittedbySpace){
                            DescList.add(str);
                    }
         List<Product_matching_Rule__c> AllProductMatchingRules = [select id,Contains__c,Does_Not_Contains__c,Product_to_be_match__c from Product_matching_Rule__c ];
          for (Product_matching_Rule__c PMR : AllProductMatchingRules ){
              
              If(PMR.Contains__c !=null  || !String.isBlank(PMR.Contains__c)){
               String Con = PMR.Contains__c; 
                List<String> ConSplittedbyComma = Con.split(',');
                  System.debug('-- ConSplittedbyComma--'+ConSplittedbyComma);
                for (String Constr : ConSplittedbyComma){
                       System.debug('-- Constr--'+Constr);  
                    if(DescList.contains(Constr))    {
                        ContainsList.add(Constr);// Add to Contains list
                            ContainsMAP.put(Constr+PMR.ID,PMR.ID);// Add to Contains MAP
                        }              
                    }
                }
               If(PMR.Does_Not_Contains__c !=null  || !String.isBlank(PMR.Does_Not_Contains__c) ){
               String Con = PMR.Does_Not_Contains__c; 
                List<String> DoesNotConSplittedbyComma = Con.split(',');
                for (String NotConstr : DoesNotConSplittedbyComma){
                            DoesnotContainsList.add(NotConstr);// Add to Contains list
                            DoesnotContainsMAP.put(NotConstr+PMR.ID,PMR.ID);// Add to Contains MAP
                                        }
                }
              
              
            
             
                        }
     
          System.debug('-- Before--');
         System.debug('DescList'+DescList);
         System.debug('ContainsList'+ContainsList);
         System.debug('ContainsMAP'+ContainsMAP);
         
         System.debug('DoesnotContainsList'+DoesnotContainsList);
         System.debug('DoesnotContainsMap'+DoesnotContainsMAP);
       
         Set<String> FinalContainsList = new set<String>();
        
        // Finding Final Match
        If(DoesnotContainsMAP.size() > 0){
        
        For (String val :DoesnotContainsMAP.values())
         { 
             
             system.debug('does not contain Value'+val);
             for(string CVal :ContainsMAP.values()){
                 
                 IF(val != cval){
                     FinalContainsList.add(CVal);
                 }
                 
             }
             
         }
        }
        
        Else{
            for(string CVal :ContainsMAP.values()){
                                  
                     FinalContainsList.add(CVal);
                 
                 
             }

            
        } // End of Final match
        
        
         System.debug('-- After--');
         System.debug('DescList'+DescList);
         System.debug('ContainsList'+ContainsList);
         System.debug('ContainsMAP'+ContainsMAP);
         
         System.debug('DoesnotContainsList'+DoesnotContainsList);
         System.debug('DoesnotContainsMap'+DoesnotContainsMAP);
         System.debug('FinalContainsList'+FinalContainsList);

         If(FinalContainsList.size() >0){
                for (Product_matching_Rule__c PMR : AllProductMatchingRules ){
                    For (String abc :FinalContainsList){
                        System.debug('PMRID-->' +PMR.ID +'ABC-->' +ABC);
                                if(PMR.ID ==abc ){ Finalmatch.add(PMR.Product_to_be_match__c);
                                                  }
                            }
                }
            
             
              li.product__C = Finalmatch[0];
            li.Part_Matching_Method__c = 'Auto Matched';
             
         }
        Else{
           
            li.New_Product__c = TRUE;
                                 
        }
        
      
          System.debug('Finalmatch-->'+Finalmatch); 
               
               
               
           }    
           
          // new block end 
      */     Else{
               
             li.New_Product__c = TRUE;  
               
           }
      
              li.All_Matching_Complete__c = TRUE;
              li.All_SOs_Times_Shipped__c = allSOShipped == null? 0 : allSOShipped;
              li.All_SOs_Average_Unit_Price__c = allSOAverage == null? 0 : allSOAverage;
              li.All_SOs_Max_Unit_Price__c = allSOMax == null? 0 : allSOMax;
              li.All_SOs_Min_Unit_Price__c = allSOMin ==  null? 0 : allSOMin;
              li.Destination_SOs_Times_Shipped__c = destinationSOShipped == null? 0 : destinationSOShipped;
              li.Destination_SOs_Average_Unit_Price__c = destinationSOAverage ==  null ? 0 : destinationSOAverage;
              li.Destination_SOs_Max_Unit_Price__c = destinationSOMax == null ? 0 : destinationSOMax;
              li.Destination_SOs_Min_Unit_Price__c = destinationSOMin == null ? 0 : destinationSOMin;
              li.Client_SOs_Times_Shipped__c = clientSOShipped == null ? 0 : clientSOShipped;
              li.Client_SOs_Average_Unit_Price__c = clientSOAverage == null ? 0 : clientSOAverage;
              li.Client_SOs_Max_Unit_Price__c = clientSOMax == null ? 0 : clientSOMax;
              li.Client_SOs_Min_Unit_Price__c = clientSOMin == null ? 0 : clientSOMin;
              li.All_SOs_Last_Shipment_Date__c = allSOLastPOD;
              li.Client_SOs_Last_Shipment_Date__c = clientSOLastPOD;
              li.Destination_SOs_Last_Shipment_Date__c = destinationSOLastPOD;
              li.All_SOs_Most_Shipped_HS__c = maxAllHSCode; 
              li.All_SOs_Most_COO__c = maxAllCOO; 
              li.Destination_SOs_Most_Shipped_HS__c = maxdestHSCode;
              li.CPA_Changed__c = FALSE;
    
    
    
  
        }
        
        
        catch(Exception e){
            System.Debug('Error setting region.' +e);
        }


}


}   
     
     
     If(lo.Product__c != oldPart.Product__c ){
         
         Map<Id, shipment_Order__c> shipmentOrder = new Map<Id,shipment_Order__c>([Select Id, CPA_v2_0__r.Final_Destination__c 
                                           from Shipment_Order__c 
                                           where id IN :SoIds]); 
     System.Debug('shipmentOrder----->' + shipmentOrder);
     
     
     Map<Id, Product2> searchNameProducts = new Map<Id, Product2>( [select Id, Search_Name__c, Manufacturer_Text_Formula__c, Product_Name_Alias__c
                                                                    from Product2]);
         
         String manufacturerName = searchNameProducts.get(lo.Product__c).Manufacturer_Text_Formula__c;
         
         System.Debug('lo.Product__c -->' + lo.Product__c);
         System.Debug('manufacturerName -->' + manufacturerName);

  
  List<AggregateResult> allSoParts = [SELECT Name, AVG(Commercial_Value__c) average, MIN(Commercial_Value__c) minimum, MAX(Commercial_Value__c) maximum,
                                             Count(Id) numberParts, Max(Shipment_Order__r.POD_Date__c) maxPOD, MAX(US_HTS_Code__c) maxHSCode, max(Country_of_Origin2__c) maxCOO
                                             From Part__c 
                                             where Product__C IN : productId
                                             and (Shipment_Order__r.Shipping_Status__c = 'POD Received'
                                             Or Shipment_Order__r.Shipping_Status__c = 'Customs Clearance Docs Received')
                                             GROUP BY Name];   
         
            system.debug('allSoParts---> '+ allSoParts);
      
            for(AggregateResult a : allSoParts){
                allSOAverage = (Decimal) a.get('average');
                allSOMin = (Decimal) a.get('minimum');
                allSOMax = (Decimal) a.get('maximum');
                allSOShipped = (Decimal) a.get('numberParts'); 
                allSOLastPOD = (Date) a.get('maxPOD');
                maxAllHSCode = (String) a.get('maxHSCode');
                maxAllCOO = (String) a.get('maxCOO');
            }
      
      
       List<AggregateResult> allDestinationParts = [SELECT Name, AVG(Commercial_Value__c) average, MIN(Commercial_Value__c) minimum, MAX(Commercial_Value__c) maximum,
                                             Count(Id) numberParts, Max(Shipment_Order__r.POD_Date__c) maxPOD,  MAX(US_HTS_Code__c) maxHSCode
                                             From Part__c 
                                             where Product__C IN : productId
                                             and (Shipment_Order__r.Shipping_Status__c = 'POD Received'
                                             Or Shipment_Order__r.Shipping_Status__c = 'Customs Clearance Docs Received')
                                             and Ship_To_Country__c =: clearanceDestination
                                             GROUP BY Name];
            
            
            for(AggregateResult a : allDestinationParts){
                destinationSOAverage = (Decimal) a.get('average');
                destinationSOMin = (Decimal) a.get('minimum');
                destinationSOMax = (Decimal) a.get('maximum');
                destinationSOShipped = (Decimal) a.get('numberParts'); 
                destinationSOLastPOD = (Date) a.get('maxPOD');
                maxdestHSCode =  (String) a.get('maxHSCode');
            }
      
      
     List<AggregateResult> allClientParts = [SELECT Name, AVG(Commercial_Value__c) average, MIN(Commercial_Value__c) minimum, MAX(Commercial_Value__c) maximum,
                                             Count(Id) numberParts, Max(Shipment_Order__r.POD_Date__c) maxPOD
                                             From Part__c 
                                             where Product__C IN : productId
                                             and (Shipment_Order__r.Shipping_Status__c = 'POD Received'
                                             Or Shipment_Order__r.Shipping_Status__c = 'Customs Clearance Docs Received')
                                             and Shipment_Order__r.Account__c =:accountId
                                             GROUP BY Name];      
            
            
             for(AggregateResult a : allClientParts){
                clientSOAverage = (Decimal) a.get('average');
                clientSOMin = (Decimal) a.get('minimum');
                clientSOMax = (Decimal) a.get('maximum');
                clientSOShipped = (Decimal) a.get('numberParts'); 
                clientSOLastPOD = (Date) a.get('maxPOD');
            }  
      
  
  for (Part__c li : Trigger.new) {
     
       try { 
        
   
              li.All_Matching_Complete__c = TRUE;
              li.All_SOs_Times_Shipped__c = allSOShipped == null? 0 : allSOShipped;
              li.All_SOs_Average_Unit_Price__c = allSOAverage == null? 0 : allSOAverage;
              li.All_SOs_Max_Unit_Price__c = allSOMax == null? 0 : allSOMax;
              li.All_SOs_Min_Unit_Price__c = allSOMin ==  null? 0 : allSOMin;
              li.Destination_SOs_Times_Shipped__c = destinationSOShipped == null? 0 : destinationSOShipped;
              li.Destination_SOs_Average_Unit_Price__c = destinationSOAverage ==  null ? 0 : destinationSOAverage;
              li.Destination_SOs_Max_Unit_Price__c = destinationSOMax == null ? 0 : destinationSOMax;
              li.Destination_SOs_Min_Unit_Price__c = destinationSOMin == null ? 0 : destinationSOMin;
              li.Client_SOs_Times_Shipped__c = clientSOShipped == null ? 0 : clientSOShipped;
              li.Client_SOs_Average_Unit_Price__c = clientSOAverage == null ? 0 : clientSOAverage;
              li.Client_SOs_Max_Unit_Price__c = clientSOMax == null ? 0 : clientSOMax;
              li.Client_SOs_Min_Unit_Price__c = clientSOMin == null ? 0 : clientSOMin;
              li.All_SOs_Last_Shipment_Date__c = allSOLastPOD;
              li.Client_SOs_Last_Shipment_Date__c = clientSOLastPOD;
              li.Destination_SOs_Last_Shipment_Date__c = destinationSOLastPOD;
              li.All_SOs_Most_Shipped_HS__c = maxAllHSCode; 
              li.All_SOs_Most_COO__c = maxAllCOO;
              li.Destination_SOs_Most_Shipped_HS__c = maxdestHSCode;
              li.Manufacturer_Name__c = manufacturerName;
              li.Part_Linked_to_Product_Date__c = date.today();
              li.CPA_Changed__c = FALSE;
    
    
    
  
        }
        
        
        catch(Exception e){
            System.Debug('Error setting region.' +e);
        }


}


} 

  }     
   
 
 If(Trigger.IsUpdate && Trigger.IsAfter){
     
     system.debug('INside afer updte');
  
              Decimal lineItemTotal;
              Decimal hsCodes;
              Decimal cif;
              Decimal fob;
              Decimal totalValue;
     
      Part__c oldPart;
      Part__c lo;
    
     
     
     
     
     for (Part__c li : Trigger.new) {
        SoIds.add(li.Shipment_Order__c);
        oldPart = Trigger.oldMap.get(li.Id); 
        lo = li;
     }
    System.Debug('SoIds---->' + SoIds);    
     
     iF(oldPart.Total_Value__C != lo.Total_Value__C || oldPart.US_HTS_Code__c != lo.US_HTS_Code__c ){
         
   shipment_Order__c shipmentOrder = [Select Id, Name, Total_CIF_Duties__c, Total_Line_Item_Extended_Value__c, Total_FOB_Duties__c,
                                              Total_CIF_1_Duties__c, of_Unique_Line_Items__c, of_Line_Items__c 
                                              from Shipment_Order__c 
                                              where id IN :SoIds ];
             
             
   List<AggregateResult> allParts = [SELECT SUM(Total_Value__C) total, Count_Distinct(US_HTS_Code__c) hsCodes, Sum(CIF_Duties__c) sumCIF,
                                             Count(Id) numberParts, sum(FOB_Duties__c) sumFOB
                                             From Part__c 
                                             where Shipment_Order__c =:SoIds];    
             
            for(AggregateResult a : allParts){
                lineItemTotal = (Decimal) a.get('numberParts');
                hsCodes = (Decimal) a.get('hsCodes');
                cif = (Decimal) a.get('sumCIF');
                fob = (Decimal) a.get('sumFOB'); 
                totalValue = (Decimal) a.get('total');
            }       
        
     
     for (Part__c li : Trigger.new) {
  
            shipmentOrder.of_Line_Items__c = lineItemTotal;
            shipmentOrder.of_Unique_Line_Items__c = hsCodes;
            shipmentOrder.Total_CIF_Duties__c = cif > 0? cif: 0;
            shipmentOrder.Total_FOB_Duties__c = fob > 0? fob: 0;
            shipmentOrder.Total_Line_Item_Extended_Value__c = totalValue > 0? totalValue: 0;
            shipmentOrder.Shipment_Value_USD__c = totalValue > 0? totalValue: shipmentOrder.Shipment_Value_USD__c;
            update shipmentOrder;
             
         }
         
         
       
 }
   
    
 }
       If(Trigger.IsDelete && Trigger.IsBefore){
  
              Decimal lineItemTotal;
              Decimal hsCodes;
              Decimal cif;
              Decimal fob;
              Decimal totalValue;
        Set<ID> LIID = New Set<ID>();
        Set<ID> PrID = New Set<ID>();  
          
           
      for (Part__c li : trigger.old) { 
        LIID.add(li.ID);
          PRID.add(Li.Product__c);
      }   
           
      System.debug('LIID'+LIID);
      System.debug('PrID'+PrID);
           
          List<Task> TaskstoDelete = [select Id from Task where Part__c In :LIID];
          System.debug('TaskstoDelete'+TaskstoDelete);
          Delete TaskstoDelete;
     
   /*     
  shipment_Order__c shipmentOrder = [Select Id, Name, Total_CIF_Duties__c, Total_Line_Item_Extended_Value__c, Total_FOB_Duties__c,
                                              Total_CIF_1_Duties__c, of_Unique_Line_Items__c, of_Line_Items__c, CPA2_Override_reason__c, Shipment_Value_USD__c
                                              from Shipment_Order__c 
                                              where id IN :SoIds ];
             
             
    List<AggregateResult> allParts = [SELECT SUM(Total_Value__C) total, Count_Distinct(US_HTS_Code__c) hsCodes, Sum(CIF_Duties__c) sumCIF,
                                             Count(Id) numberParts, sum(FOB_Duties__c) sumFOB
                                             From Part__c 
                                             where Shipment_Order__c =:SoIds];    
             
            for(AggregateResult a : allParts){
                lineItemTotal = (Decimal) a.get('numberParts');
                hsCodes = (Decimal) a.get('hsCodes');
                cif = (Decimal) a.get('sumCIF');
                fob = (Decimal) a.get('sumFOB'); 
                totalValue = (Decimal) a.get('total');
            }       
        
     
    
          System.debug('shipmentOrder.Shipment_Value_USD__c : '+ shipmentOrder.Shipment_Value_USD__c);
          System.debug('totalValue : '+ totalValue);
        
        
            shipmentOrder.of_Line_Items__c = lineItemTotal;
            shipmentOrder.of_Unique_Line_Items__c = hsCodes;
            shipmentOrder.Total_CIF_Duties__c = cif > 0? cif: 0;
            shipmentOrder.Total_FOB_Duties__c = fob > 0? fob: 0;
            shipmentOrder.Total_Line_Item_Extended_Value__c = totalValue > 0? totalValue: 0;
            shipmentOrder.Shipment_Value_USD__c = totalValue == null || totalValue == 0 ? shipmentOrder.Shipment_Value_USD__c: totalValue ;
            update shipmentOrder;
             
  */       
 }
    
    If(Trigger.IsDelete && Trigger.IsAfter){
  
              Decimal lineItemTotal;
              Decimal hsCodes;
              Decimal cif;
              Decimal fob;
              Decimal totalValue;
              
      for (Part__c li : trigger.old) { 
          If(li.Shipment_Order__c != null){
            SoIds.add(li.Shipment_Order__c);}}   
        
         System.debug('SoIds.size() : '+ SoIds.size());
         System.debug('SoIds : '+ SoIds);
                  
        If(SoIds.size() != 0 && SoIds!= null){
            
            
        
  shipment_Order__c shipmentOrder = [Select Id, Name, Total_CIF_Duties__c, Total_Line_Item_Extended_Value__c, Total_FOB_Duties__c,
                                              Total_CIF_1_Duties__c, of_Unique_Line_Items__c, of_Line_Items__c, CPA2_Override_reason__c, Shipment_Value_USD__c
                                              from Shipment_Order__c 
                                              where id IN :SoIds ];
             
             
    List<AggregateResult> allParts = [SELECT SUM(Total_Value__C) total, Count_Distinct(US_HTS_Code__c) hsCodes, Sum(CIF_Duties__c) sumCIF,
                                             Count(Id) numberParts, sum(FOB_Duties__c) sumFOB
                                             From Part__c 
                                             where Shipment_Order__c =:SoIds];    
             
            for(AggregateResult a : allParts){
                lineItemTotal = (Decimal) a.get('numberParts');
                hsCodes = (Decimal) a.get('hsCodes');
                cif = (Decimal) a.get('sumCIF');
                fob = (Decimal) a.get('sumFOB'); 
                totalValue = (Decimal) a.get('total');
            }       
        
     
    
          System.debug('shipmentOrder.Shipment_Value_USD__c : '+ shipmentOrder.Shipment_Value_USD__c);
          System.debug('totalValue : '+ totalValue);
        
        
            shipmentOrder.of_Line_Items__c = lineItemTotal;
            shipmentOrder.of_Unique_Line_Items__c = hsCodes;
            shipmentOrder.Total_CIF_Duties__c = cif > 0? cif: 0;
            shipmentOrder.Total_FOB_Duties__c = fob > 0? fob: 0;
            shipmentOrder.Total_Line_Item_Extended_Value__c = totalValue > 0? totalValue: 0;
            shipmentOrder.Shipment_Value_USD__c = totalValue == null || totalValue == 0 ? shipmentOrder.Shipment_Value_USD__c: totalValue ;
            update shipmentOrder;
             
         
 }
    
    }   

}