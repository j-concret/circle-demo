trigger lastactivityonleads on task (before insert,after insert, after update) {
    
    if(RecusrsionHandler.isTaskTriggerSkip) return;
    
    if(Trigger.isBefore && Trigger.isInsert) {
        System.debug('inside before insert');
         
          String Tskrecordtype = Schema.Sobjecttype.task.getRecordTypeInfosByDeveloperName().get('SalesTasks').getRecordTypeId();
       			for(Task t :trigger.new) {
                    if(! Test.isRunningTest()){
                    if(t.recordtypeid == Tskrecordtype  && (t.Subject.contains('InMail sent from Sales Navigator') || t.Subject.contains('Message sent from Sales Navigator')) ){
                  
                        t.Playbooks_Step_Type__c='LinkedInMail';
                    }
                    else if(t.recordtypeid ==Tskrecordtype  && t.Subject.contains('Connection')  ){
                   
                        t.Playbooks_Step_Type__c='LinkedInConnection';
                    }
                    }
      		 	}
    
		}
    
    List<Id> LeadIds = new List<Id>();
    List<Lead> LeadList = new List<Lead>();

    for(Task t :trigger.new) {
        if(t.whoId != null) {
            Schema.SObjectType tType= t.whoId.getSObjectType();
            if(tType == Lead.Schema.SObjectType) {
                LeadIds.add(t.WhoId);
            }
        }
    }

    if(!Leadids.isEmpty()) {
        Map<Id,Lead> LeadMap =  new Map<Id,Lead>([SELECT Id,Last_Activity_Date__c FROM Lead WHERE Id IN: LeadIds]);
        Map<Id,Task> leadsWithTasks =new Map<Id,Task>([SELECT Id,whoID FROM Task WHERE whoid IN: Leadids]);
        for(Task t :Trigger.new) {
            for(Lead l : LeadMap.Values()) {
                l.Last_Activity_Date__c = Datetime.now();
                l.Last_activity_Subject__c =t.subject;
                l.Number_of_activities__c = leadsWithTasks.size();
                LeadList.add(l);
            }
        }
        if(!LeadList.isEmpty()) {
            update LeadList;
        }
    }

    //code for creation of SO Travel History Objects

    if(Trigger.isAfter && Trigger.isUpdate) {
        List<String> masterNames = new List<String> {'Schedule Pick-up (TecEx) - Auto','Schedule Pick-up (TecEx) - Manual'};
        Set<String> shipIds = new Set<String>();
        Map<String,String> shipmentIdsMap = new Map<String,String>();
        Map<String,Task> tasksMap = new Map<String,Task>();
        Set<Id> masterTaskIds = new Set<Id>();
        Set<Id> materTaskBlockShipLogic = new Set<Id>();
        Set<Id> SoIdsToCheckBlockShipLogic = new Set<Id>();
        Map<Id,Master_Task__c> validateMaster = new Map<Id,Master_Task__c>([SELECT Id,Name FROM Master_Task__c WHERE name IN: masterNames]);

        for(Task t :trigger.new) {
            Task oldTask = Trigger.oldMap.get(t.ID);
            if(t.whatId != null) {
                Schema.SObjectType tType= t.whatId.getSObjectType();
                if(!t.Auto_Resolved__c && validateMaster.containsKey(t.Master_Task__c) && ((t.IsNotApplicable__c && t.IsNotApplicable__c != oldTask.IsNotApplicable__c) || (!t.Blocks_Approval_to_Ship__c && t.Blocks_Approval_to_Ship__c != oldTask.Blocks_Approval_to_Ship__c))) {
                    t.addError('Please provide the schedule pick-up details in order for an accurate ETA to be displayed to the client of when their goods are expected to depart.');
                }

                if(tType == Shipment_Order__c.Schema.SObjectType) {
                    shipIds.add(t.whatId);

                    if(!t.Blocks_Approval_to_Ship__c && t.Blocks_Approval_to_Ship__c != oldTask.Blocks_Approval_to_Ship__c) {
                        materTaskBlockShipLogic.add(t.Master_Task__c);
                        SoIdsToCheckBlockShipLogic.add(t.whatId);
                    }

                }

                if(tType == Shipment_Order__c.Schema.SObjectType
                   && t.State__c  != oldTask.State__c
                   && ( (t.State__c == 'Client Pending' && (oldTask.State__c != 'Under Review' || oldTask.State__c != 'Resolved'))||
                        t.State__c == 'Resolved' ||
                        (t.State__c == 'TecEx Pending' && (oldTask.State__c != 'Under Review' || oldTask.State__c != 'Resolved') ) ||
                        t.State__c == 'Under Review')) {
                    shipmentIdsMap.put(t.Master_Task__c,t.whatId);
                    masterTaskIds.add(t.Master_Task__c);
                    tasksMap.put(t.Master_Task__c,t);
                }
            }
        }

        if(!materTaskBlockShipLogic.isEmpty()) {
            Set<Id> dependentMasterTask = new Set<Id>();
            for(Master_Task_Prerequisite__c mstPre : [SELECT id,Name, Master_Task_Dependent__c, Master_Task_Prerequisite__c FROM Master_Task_Prerequisite__c WHERE Master_Task_Prerequisite__c IN: materTaskBlockShipLogic]) {
                dependentMasterTask.add(mstPre.Master_Task_Dependent__c);

            }

            Integer dependentTasks = Database.countQuery('Select Count() FROM Task WHERE Master_Task__c IN :dependentMasterTask AND WhatId IN :SoIdsToCheckBlockShipLogic AND Blocks_Approval_to_Ship__c = TRUE');
            if(dependentTasks > 0) {
                Trigger.new[0].addError('Please note this task has dependent tasks which Block Approval to ship. Therefore this task must be completed in order for those dependent tasks to be made active, and completed');
            }

        }
        if(shipmentIdsMap != null && !shipmentIdsMap.isEmpty() && masterTaskIds != null && !masterTaskIds.isEmpty()) {
            SoLogOnTaskHelper.getShipmentAndMasterTaskDetails(shipmentIdsMap,masterTaskIds,tasksMap);
        }

        Map<Id,Shipment_Order__c> soMap = new Map<Id, Shipment_Order__c>();

        if(!shipIds.isEmpty()) {
            soMap = new Map<Id,Shipment_Order__c>([SELECT Id,TP_Halt__c,Ship_From_Country__c , TP_Halt_Start_Date__c, Total_TP_Halt_Days__c, Total_TP_Halt_Business_Days__c,Total_Halt_Business_Days__c,Sub_Status_Update__c,of_Line_Items__c,banner_feed__c,Clearance_Destination__c,
                                                   Ship_to_Country_new__c,Invoice_Timing__c,Shipping_Status__c,
                                                   Halt__c, Halt_Start_Date__c, Total_Halt_Days__c,CPA_v2_0__r.Comments_On_Why_No_Final_Quote_Possible__c, Expected_Date_to_Next_Status__c,
                                                   In_Transit_Date__c,cost_Estimate_acceptance_date__c,
                                                   CPA_v2_0__r.Final_Destination__c,CPA_v2_0__r.Tracking_Term__c,
                                                   Client_Contact_for_this_Shipment__r.Client_Notifications_Choice__c,
                                                   Client_Contact_for_this_Shipment__r.Major_Minor_notification__c,
                                                   Client_Contact_for_this_Shipment__r.Client_Tasks_notification_choice__c,
                                                   Shipment_Order_Compliance_Time_Decimal__c,Date_Of_New_CP__c,Invoice_Payment__c,Pick_up_Coordination__c,
                                                   Shipping_Documents__c,Customs_Compliance__c,Mapped_Shipping_status__c,Hub_Lead_Time_Decimal__c,
                                                   Transit_to_Destination_Lead_Time_Decimal__c, Customs_Clearance_Lead_Time_Decimal__c,Final_Delivery_Lead_Time_Decimal__c,
                                                   Customs_Cleared_Date__c, Arrived_in_Customs_Date__c,
                                                   CPA_v2_0__c FROM Shipment_Order__c WHERE Id IN: shipIds AND Shipping_Status__c !='Shipment Abandoned' AND Shipping_Status__c !='Cost Estimate Abandoned' AND Shipping_Status__c !='Roll-Out' ]);

            system.debug('line 107');
            if(!RecusrsionHandler.taskTriggerRun && !RecusrsionHandler.tasksCreated) {
                RecusrsionHandler.taskTriggerRun = true;
                system.debug('line 110');
                ShipmentOrderTriggerHandler.processAfterUpdate(soMap);
            }
        }
    }
}