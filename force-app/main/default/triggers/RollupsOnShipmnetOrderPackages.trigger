/* AS: Trigget to perform
 * -Rollups field sum of Chargeable_Weight__c on SHIPMENTORDER
 * -Rollups field sum of Actual_Weight_KGs__c on SHIPMENTORDER
 * -Rollups field sum of of_packages__c on SHIPMENTORDER
 * -Rollups field sum of Chargeable_weight_in_KGs_packages__c on Freight
 * -Rollups field sum of Actual_Weight_KGs__c on Freight
 * -Rollups field sum of Packages_of_Same_Weight_Dimensions__c on Freight
 */


trigger RollupsOnShipmnetOrderPackages on Shipment_Order_Package__c (After insert,After Update, After delete) {

    //Skip trigger execution in case of SO insertion (rollups already calculated on SO creation )
    if(RecusrsionHandler.skipSOPTriggerExecutionOnSOCreation) return;

    if((Trigger.isInsert || Trigger.isUpdate || Trigger.isDelete) && !RecusrsionHandler.SOPCalled) {
        RecusrsionHandler.SOPCalled = true;
        List<Shipment_Order_Package__c> sopList = Trigger.isDelete ? Trigger.old : Trigger.new;
        Set<Id> freightIds = new Set<Id>();
        Map<Id,Shipment_Order__c> soToUpdate = new Map<Id,Shipment_Order__C>();
        List<Freight__c> freightToUpdate = new List<Freight__c>();

        //Get Id's for Shipping Order Package, related shipment Order and related Freight
        for(Shipment_Order_Package__c sop: sopList) {
			system.debug('sopObj : '+sop);
            if(sop.Freight__c != null) {
                freightIds.add(SOP.Freight__c);
            }
            if(sop.Shipment_Order__c != null && !soToUpdate.containsKey(sop.shipment_Order__c)) {
                soToUpdate.put(SOP.shipment_Order__c,new Shipment_Order__c(Id=SOP.shipment_Order__c,Contains_Batteries_Auto__c = 'Info Unavailable',Package_Height__c='No',Contains_Batteries_Client__c=null, Li_ion_Batteries__c = 'No' ));
            }
			system.debug('soToUpdate :'+soToUpdate);
            if(Trigger.isUpdate && Trigger.oldMap.get(sop.Id).Shipment_Order__c !=null && sop.Shipment_Order__c != Trigger.oldMap.get(sop.Id).Shipment_Order__c && !soToUpdate.containsKey(Trigger.oldMap.get(sop.Id).Shipment_Order__c)) {
                soToUpdate.put(Trigger.oldMap.get(sop.Id).Shipment_Order__c,new Shipment_Order__c(Id=Trigger.oldMap.get(sop.Id).Shipment_Order__c,Contains_Batteries_Auto__c = 'Info Unavailable',Package_Height__c='No',Contains_Batteries_Client__c=null, Li_ion_Batteries__c = sop.Contains_Batteries__c? 'Yes' : 'No'));
            }
        }

        if(!soToUpdate.isEmpty()) {

            for(Shipment_Order_Package__c sop : [SELECT Id,Breadth_Inch__c,Height_Inch__c,Length_Inch__c,Contains_Batteries__c, Shipment_Order__c FROM Shipment_Order_Package__c WHERE Shipment_Order__c IN :soToUpdate.keySet()]) {

                /*if(soToUpdate.containsKey(sop.Shipment_Order__c) && soToUpdate.get(sop.Shipment_Order__c).Package_Height__c!='Yes' && (SOP.Breadth_Inch__c < 62 && SOP.Height_Inch__c < 62 && SOP.Length_Inch__c < 62)) {
                    soToUpdate.get(sop.Shipment_Order__c).Package_Height__c ='Yes';
                }*/
                if(soToUpdate.containsKey(sop.Shipment_Order__c) && (SOP.Breadth_Inch__c <= 47 && SOP.Height_Inch__c <= 47 && SOP.Length_Inch__c <= 47)) {
                    soToUpdate.get(sop.Shipment_Order__c).Package_Height__c ='<=47';
                }else if(soToUpdate.containsKey(sop.Shipment_Order__c) && (SOP.Breadth_Inch__c > 47 && SOP.Height_Inch__c > 47 && SOP.Length_Inch__c > 47)){
                    soToUpdate.get(sop.Shipment_Order__c).Package_Height__c ='>47';
                }

                if(soToUpdate.containsKey(sop.Shipment_Order__c) && (soToUpdate.get(sop.Shipment_Order__c).Contains_Batteries_Client__c == null || soToUpdate.get(sop.Shipment_Order__c).Contains_Batteries_Client__c != 'Yes' || soToUpdate.get(sop.Shipment_Order__c).Li_ion_Batteries__c == null)) {
                    soToUpdate.get(sop.Shipment_Order__c).Contains_Batteries_Client__c = sop.Contains_Batteries__c ? 'Yes' : 'No';
                    soToUpdate.get(sop.Shipment_Order__c).Li_ion_Batteries__c  = sop.Contains_Batteries__c ? 'Yes' : 'No';
                    soToUpdate.get(sop.Shipment_Order__c).Contains_Batteries_Auto__c = null;
                }
                system.debug('soToUpdate :'+soToUpdate);
            }

            List<AggregateResult> aggResults = [SELECT Shipment_Order__c,Sum(Chargeable_Weight_KGs__c) SUMCharWeight,Sum(Actual_Weight_KGs__c) SUMActWeight,Sum(packages_of_same_weight_dims__c) SUMNumberOfPackages FROM Shipment_Order_Package__c WHERE Shipment_Order__c IN:soToUpdate.keySet() GROUP BY Shipment_Order__c];

            if(aggResults.isEmpty()) {
                for (Id shipmentId : soToUpdate.keySet()) {
                    Shipment_Order__c so = soToUpdate.get(shipmentId);
                    so.Chargeable_Weight__c = 0;
                    so.Actual_Weight_KGs__c = 0;
                    so.of_packages__c = 0;
                }
            }else {
                for(AggregateResult a : aggResults) {
                    Shipment_Order__c so = soToUpdate.get(String.valueOf(a.get('Shipment_Order__c')));
                    so.Chargeable_Weight__c = (Decimal) a.get('SUMCharWeight');
                    so.Actual_Weight_KGs__c = (Decimal) a.get('SUMActWeight');
                    so.of_packages__c = (Decimal) a.get('SUMNumberOfPackages');
                }
            }
            Update soToUpdate.values();
        }

        /* if(!freightIds.isEmpty()) {

            for(AggregateResult b : [SELECT Freight__c, Sum(Chargeable_Weight_KGs__c) SUMCharWeight,Sum(Actual_Weight_KGs__c) SUMActWeight,Sum(packages_of_same_weight_dims__c) SUMNumberOfPackages from Shipment_Order_Package__c where Freight__c IN: freightIds GROUP BY Freight__c]) {

                Freight__c fr = new Freight__c(Id = String.valueOf(b.get('Freight__c')));
                fr.Chargeable_weight_in_KGs_packages__c = (Decimal) b.get('SUMCharWeight');
                fr.Actual_Weight_KGs__c = (Decimal) b.get('SUMActWeight');
                fr.Packages_of_Same_Weight_Dimensions__c = (Decimal) b.get('SUMNumberOfPackages');
                freightToUpdate.add(fr);
            }

            Update freightToUpdate;
           } */
    }
}