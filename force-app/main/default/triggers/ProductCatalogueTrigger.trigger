trigger ProductCatalogueTrigger on Product_Catalogue__c (after insert,after update) {

    if(!ProductCatalogueTiggerHandler.isRecursive) {
        ProductCatalogueTiggerHandler.isRecursive = true;
        ProductCatalogueTiggerHandler.process(Trigger.newMap.keySet());
    }

}