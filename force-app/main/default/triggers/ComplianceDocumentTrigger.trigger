trigger ComplianceDocumentTrigger on Compliance_Document__c (before insert,before update) {
    Set<String> certificateNos = new Set<String>();
    Map<String,Compliance_Document__c> existingCertificateNos = new Map<String,Compliance_Document__c>();
    Set<Id> ids = new Set<Id>();
    for(Compliance_Document__c cd : Trigger.new) {
        if(String.isBlank(cd.Certification_number__c)) continue;
        if(certificateNos.contains(cd.Certification_number__c)) cd.addError('You cannot create a CD with this certification number as it already exists');
        certificateNos.add(cd.Certification_number__c);
        if(Trigger.isUpdate) ids.add(cd.Id);
    }

    String query = 'SELECT Id,Certification_number__c,Name,Compliance_Document_Name__c FROM Compliance_Document__c WHERE Certification_number__c IN: certificateNos '+(!ids.isEmpty() ? ' AND Id NOT IN: ids' : '');

    for(Compliance_Document__c cd : Database.query(query)) {
        existingCertificateNos.put(cd.Certification_number__c,cd);
    }

    for(Compliance_Document__c cd : Trigger.new) {
        if(String.isNotBlank(cd.Certification_number__c) && existingCertificateNos.containsKey(cd.Certification_number__c) && cd.Compliance_Document_Name__c == existingCertificateNos.get(cd.Certification_number__c).Compliance_Document_Name__c) cd.addError('You cannot create a CD with this certification number as it already exists in '+existingCertificateNos.get(cd.Certification_number__c).Name);
    }

}