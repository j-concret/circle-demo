trigger aCNRollups on Applied_Credit_Note__c (after insert) {
    
   
    Set<Id> acnIds = new Set<Id>();
    Set<Id> invoiceIds = new Set<Id>();
    Set<Id> creditNoteIds = new Set<Id>();
    Date creditDate;
    Date invoiceDate;
    Decimal invoiceAmount;
    Decimal creditAmount;
    Decimal invoiceAmount1;
    Decimal creditAmount1;
    Decimal penaltyAmount;
    Decimal penaltyAmount1;
    
    List<Applied_Credit_Note__c> acn1 = new List<Applied_Credit_Note__c>();
    
    If(Trigger.IsInsert && Trigger.IsAfter){
        
       for(Applied_Credit_Note__c acn : trigger.new) {
        acnIds.add(acn.id);
        invoiceIds.add(acn.Invoice_New__c);
        creditNoteIds.add(acn.Credit_Note_New__c);
       }
        
   Decimal invAppliedVal;   Date invAppDate;
   Decimal crAppliedVal;   Date crAppDate;
        
   List<Invoice_New__c> updateList = new List<Invoice_New__c>();   
   Map<Id,Invoice_New__c> invoiceMap = new Map<Id, Invoice_New__c>([Select POD_Date_New__c, Conversion_Rate__c, Accounting_Period_new__c, Prepayment_Date__c, Id, Account__c, Due_Date__c, Invoice_Sent_Date__c, Shipment_Order__c, 
                                                                    Possible_Cash_Outlay__c, Invoice_Currency__c, Invoice_Reversal__c, Invoice_Being_Credited__c 
                                                                    FROM Invoice_New__c
                                                                    where Id in :invoiceIds
                                                                    OR Id in :creditNoteIds]) ;
   
        
   AggregateResult[]invAcnMap = [SELECT Sum(Amount_Applied__c)appliedVal, Max(Credit_Applied_Date__c) appDate
                              FROM Applied_Credit_Note__c
                              Where Invoice_New__c in :invoiceIds]; 
        
        
       
         
       for(AggregateResult invAcnAg : invAcnMap){
           
           
        
           invAppliedVal = (Decimal) invAcnAg.get('appliedVal');
           invAppDate = (Date) invAcnAg.get('appDate'); 
       }    
            
            
     AggregateResult[] crAcnMap = [SELECT Sum(Amount_Applied__c)appliedVal, Max(Credit_Applied_Date__c) appDate
                              FROM Applied_Credit_Note__c
                              Where Credit_Note_New__c in :creditNoteIds]; 
        
        
       
         
       for(AggregateResult crAcnAg : crAcnMap){
           
           
        
           crAppliedVal = (Decimal) crAcnAg.get('appliedVal');
           crAppDate = (Date) crAcnAg.get('appDate'); 
       }    
              
    List<Applied_Credit_Note__c> acnList = [Select Id,Amount_Applied__c,Credit_Applied_Date__c, Credit_Note_New__c, Invoice_New__c, Total_Value_Applied_To_Penalty__c   
                                            from Applied_Credit_Note__c 
                                            where Invoice_New__c in :invoiceIds
                                            OR Credit_Note_New__c in :creditNoteIds]; 
        
        
//if(flag.firstRun){     
 
        for(Applied_Credit_Note__c acn : trigger.new) {
     
        //  flag.firstRun = False;
            
            For (Id rollup : invoiceMap.keySet()) {
            If(acn.Invoice_New__c == rollup ) { 
                
                system.debug('invoiceMap.get(rollup).Invoice_Reversal__c'+invoiceMap.get(rollup).Invoice_Reversal__c);
                
            invoiceMap.get(rollup).Credits_Applied_Date__c   = invAppDate;
            invoiceMap.get(rollup).Total_Credits_Applied_To_This_Invoice__c = invAppliedVal;
            updateList.add(invoiceMap.get(rollup));       
                }   
                
                
            If(acn.Credit_Note_New__c == rollup ) {  
             system.debug('invoiceMap.get(rollup).Invoice_Being_Credited__c'+invoiceMap.get(rollup).Invoice_Being_Credited__c);
            invoiceMap.get(rollup).Credits_Applied_Date__c   = crAppDate;
            invoiceMap.get(rollup).Total_Credits_Applied_To_Other_Invoices__c = crAppliedVal ;
            updateList.add(invoiceMap.get(rollup));       
                }      
            
            
            
            }
           
           
           
            
            
            
       // } 
        
        

update updateList;

}

    
    }
        
    }