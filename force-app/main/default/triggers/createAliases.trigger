trigger createAliases on Product2 (before insert, before update) {

    Set<Id> produductIds = new Set<Id>();
    Set<String> alias = new Set<String>();
    List<String> aliasSet = new List<String>();
    List <String> aliasList = new List<String>();
    Product2 pr;

    Map<String,CountryofOriginMap__c> cooMap = new Map<String,CountryofOriginMap__c>();
    for(CountryofOriginMap__c coo : CountryofOriginMap__c.getall().values()) {
        if(!cooMap.containsKey(coo.Country__c)) cooMap.put(coo.Country__c,coo);
    }

    for (Product2 pd : Trigger.new) {
        if(pd.Encryption__c == 'No') {
            pd.Encryption_Status__c = 'Not Applicable';
            pd.Encryption_Strength__c = 'Not Applicable';
            pd.Encryption_Classification__c = 'Not Applicable';
        }
        if(pd.Wireless_capability__c == 'No') {
            pd.Wireless_type__c = 'Not Applicable';
        }
        if(pd.Suggested_Country_of_Origin__c != null &&
           cooMap.containsKey(pd.Suggested_Country_of_Origin__c) &&
           ((Trigger.isInsert) ||
            (Trigger.isUpdate &&
             Trigger.oldMap.get(pd.Id).Suggested_Country_of_Origin__c != pd.Suggested_Country_of_Origin__c))) {

            pd.Country_Two_Digit__c = cooMap.get(pd.Suggested_Country_of_Origin__c).Two_Digit_Country_Code__c;
            pd.Country_Three_Digit__c = cooMap.get(pd.Suggested_Country_of_Origin__c).Three_Digit_Country_Code__c;

        }
        if(pd.Suggested_Country_of_Origin__c == null) {
            pd.Country_Two_Digit__c = null;
            pd.Country_Three_Digit__c = null;
        }
        produductIds.add(pd.Id);

        if(pd.Manufacturer_Alias__c != NULL) {
            alias.add(pd.Manufacturer_Alias__c);
        }

        aliasList.addAll(alias);
        if(aliasList.size() > 0) {
            for (Integer i = 0; i < aliasList.size(); i++ ) {
                aliasSet = aliasList[i].split(',');
            }
        }

        if(Trigger.IsInsert ) {
            if(aliasSet.size() > 0) {
                for (Integer i = 0; i < aliasSet.size(); i++ ) {
                    String abc = pd.Product_Name_Alias__c;
                    pd.Product_Name_Alias__c = abc +','+ aliasSet[i]+ pd.Search_Name__c + ',' +  pd.Search_Name__c + aliasSet[i];
                    pd.Product_Name_Alias__c = pd.Product_Name_Alias__c.replace('null,','');
                }
            }
            String currentAlias = pd.Product_Name_Alias__c;
            String nonManAlias = pd.Non_Manufacturer_Name_Alias__c;
            String nonManAliasUpper = nonManAlias != null ? nonManAlias.toUpperCase() : nonManAlias;
            String noManAliasNoSpace = nonManAliasUpper != null ? nonManAliasUpper.deleteWhitespace() : nonManAliasUpper;
            String aliasAlmostFinal = noManAliasNoSpace != null ? currentAlias + ',' + noManAliasNoSpace : currentAlias;
            String aliasFinal = aliasAlmostFinal != null ? aliasAlmostFinal.replace('null,','') : null;
            String aliasFinal2 = aliasFinal != null ? aliasFinal.trim() : null;
            pd.Product_Name_Alias__c = aliasFinal2;
        }
        if(Trigger.IsUpdate ) {
            Product2 oldProductId = Trigger.oldMap.get(pd.Id);
            If(pd.Manufacturer__c != trigger.oldMap.get(pd.id).Manufacturer__c ||
               pd.Name != trigger.oldMap.get(pd.id).Name ||
               pd.ProductCode != trigger.oldMap.get(pd.id).ProductCode ||
               pd.Non_Manufacturer_Name_Alias__c != trigger.oldMap.get(pd.id).Non_Manufacturer_Name_Alias__c) {
                pd.Product_Name_Alias__c = null;
                for (Integer i = 0; i < aliasSet.size(); i++ ) {
                    String abc = pd.Product_Name_Alias__c;
                    String strippedName = pd.Search_Name__c;
                    pd.Product_Name_Alias__c = abc + ','+ aliasSet[i] + strippedName.trim() + ',' +  strippedName.trim() + aliasSet[i];
                }
                String currentAlias = pd.Product_Name_Alias__c;
                String nonManAlias = pd.Non_Manufacturer_Name_Alias__c;
                String nonManAliasUpper = nonManAlias != null ? nonManAlias.toUpperCase() : nonManAlias;
                String noManAliasNoSpace = nonManAliasUpper != null ? nonManAliasUpper.deleteWhitespace() : nonManAliasUpper;
                String aliasAlmostFinal = noManAliasNoSpace != null ? currentAlias + ',' + noManAliasNoSpace : currentAlias;
                String aliasFinal = aliasAlmostFinal != null ? aliasAlmostFinal.replace('null,','') : null;
                String aliasFinal2 = aliasFinal != null ? aliasFinal.trim() : null;
                pd.Product_Name_Alias__c = aliasFinal2;
            }
        }
    }
}