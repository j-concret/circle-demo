trigger LeadTrigger on Lead (after insert, after update,before insert,before update) {

    if (trigger.isBefore && trigger.isInsert) {

        String zeeRecordType = Schema.Sobjecttype.Lead.getRecordTypeInfosByDeveloperName().get('Client_E_commerce').getRecordTypeId();
        String TecexRecordType = Schema.Sobjecttype.Lead.getRecordTypeInfosByDeveloperName().get('TecEx').getRecordTypeId();
        Group queue = [SELECT Id FROM Group Where Type = 'Queue'AND DeveloperName = 'New_Web_Lead_Queue'];

        for(Lead ld : trigger.new) {
            if(ld.Client_Type__c == 'E-Commerce') {
                //System.debug('Before zee round robin');
                ld.recordtypeID=zeeRecordType;
                // LeadsHelper.ZeeLeadsRoundRobin(Trigger.New);
            }
            else {
                ld.recordtypeID=TecexRecordType;
                if(!RecusrsionHandler.skipBeforeLeadInsert) ld.OwnerId=queue.id;
            }
        }
    }

    if((trigger.isinsert || trigger.isupdate) && trigger.isafter) {
        set<Id> companyIDs = new Set<Id>();
        for(Lead ld : trigger.new) {
            companyIDs.add(ld.Related_Company__c);
        }
        Map<Id, Company__c> companyMap = new Map<Id, Company__c>([SELECT Id,Highest_Lead_Status__c, Number_of_Leads__c,(SELECT Id,Status FROM Leads__r ORDER BY Status) FROM Company__c where Id =: companyIDs ]);

        system.Debug('companyMap1 :'+companyMap);
        for(Company__c company : companyMap.values()) {
            system.debug('company :'+company.Leads__r.size());
            system.debug('status :'+company.Leads__r);
            company.Number_of_Leads__c = (company.Leads__r != null ? company.Leads__r.size() : 0);
            if(company.Leads__r.size() > 0) {
                Lead HighesStatuslead = company.Leads__r[company.Leads__r.size() - 1];
                system.debug('HighesStatuslead :'+HighesStatuslead);
                company.Highest_Lead_Status__c = HighesStatuslead.Status;
                company.Lead_Status_Change_Date__c  = Date.today();
            }
        }
        system.debug('companyMap : '+companyMap);
        update companyMap.values();
    }


}