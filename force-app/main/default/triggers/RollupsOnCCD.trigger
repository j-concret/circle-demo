/* AS: Trigget to perform
* -Rollout field sum of Total_Taxes_CCD__c on SHIPMENTORDER
* - Updates Exchange rate from Currency management
* - Send CCD Email to Orange
*/  
trigger RollupsOnCCD on Customs_Clearance_Documents__c (before insert, before update, after update,Before delete) {
    
    If(Trigger.IsInsert ){
        Customs_Clearance_Documents__c[] P1 = Trigger.new;
        String SOIDs = null;
        Decimal DATPCCD1=0;
        
        //Updates Exchange rate from Currency management
        for(Customs_Clearance_Documents__c CCD : trigger.new) {
            If(CCD.Tax_Currency__c != null){
                list<Currency_Management2__c> CM2 = [Select Name,Conversion_Rate__c,Currency__c from Currency_Management2__c where Currency__c =: CCD.Tax_Currency__c limit 1];
                CCD.Foreign_Exchange_Rate__c = CM2[0].Conversion_Rate__c;
                
            }
            
        }
        //RollOuts to ShipmentOrder
        If(P1[0].Customs_Clearance_Documents__c != null || P1[0].Customs_Clearance_Documents__c != '' ) { SOIDs = P1[0].Customs_Clearance_Documents__c;  }
        
        If(SOIDs!= null ){ 
            AggregateResult[] groupedResults = [SELECT COUNT(ID) CountID,SUM(Taxes_per_CCDs_USD__c) DATPCCD from Customs_Clearance_Documents__c  where  Customs_Clearance_Documents__c =: SOIDs];
            system.debug('groupedResults (ShipmentOrder) -->'+groupedResults);
            
            for(AggregateResult a : groupedResults){
                DATPCCD1 = (Decimal) a.get('DATPCCD');                             
            }
            
            
            Shipment_Order__C SO = [Select Total_Taxes_CCD__c,Customs_Clearance_Docs_Received_Date__c,No_further_client_invoices_required__c,Closed_Down_SO__c,ICE_Incentive_Date__c  from Shipment_Order__C where Id =:SOIDs limit 1];
            
            if(SO.Customs_Clearance_Docs_Received_Date__c == null &&  so.No_further_client_invoices_required__c == TRUE && So.Closed_Down_SO__c == FALSE && SO.ICE_Incentive_Date__c == null && P1[0].status__c == 'Reviewed' ){ 
                SO.ICE_Incentive_Date__c =  system.today();
                
            }
            Else {
                // Rollouts adding   
                SO.Total_Taxes_CCD__c = DATPCCD1;           
                Update SO;
            }
        }
        
        
    }
    
    
    
    
    if(Trigger.IsUpdate && Trigger.IsBefore){
        // Auto-Reviewed CCD checkbox stuff.
        
        Map<Id, Boolean> accountIdToAutoSendCCD = new Map<Id, Boolean>(); 
        Map<Id,Shipment_Order__c> soIdToShipmentOrder = new Map<Id,Shipment_Order__c>();
        
        //first iteration to get the SO ids
        for(Customs_Clearance_Documents__c CCD : Trigger.new){
            if(CCD.Customs_Clearance_Documents__c != null){
                soIdToShipmentOrder.put(CCD.Customs_Clearance_Documents__c, null);
            }
        }
        
        // to collect SO data and Account id
        for(Shipment_Order__c so : [SELECT Id, Account__c, CPA_v2_0__r.Auto_CCD_Review_Allowed__c, Account__r.Auto_CCD_Review_Allowed__c FROM
                                    Shipment_Order__c WHERE Id IN : soIdToShipmentOrder.keySet()
                                   AND CPA_v2_0__r.Auto_CCD_Review_Allowed__c != null
                                   AND Account__r.Auto_CCD_Review_Allowed__c != null]){
                                     soIdToShipmentOrder.put(so.Id, so);
                                      accountIdToAutoSendCCD.put(so.Account__c, false);  
                                    }
        
        // to get the Auto Send CCD as per Account contact. 
        for(Contact cn : [SELECT Id, AccountId, CCD_Auto_Email_to_Client__c FROM Contact WHERE AccountId IN :accountIdToAutoSendCCD.keySet()]){
            if(cn.CCD_Auto_Email_to_Client__c){
                accountIdToAutoSendCCD.put(cn.AccountId, true);
            }
        }
        
        for(Customs_Clearance_Documents__c CCD : Trigger.new){
            
            if(CCD.Customs_Clearance_Documents__c != null && soIdToShipmentOrder.containsKey(CCD.Customs_Clearance_Documents__c) &&
               soIdToShipmentOrder.get(CCD.Customs_Clearance_Documents__c) != null){
                   
                   Shipment_Order__c so  = soIdToShipmentOrder.get(CCD.Customs_Clearance_Documents__c);
                   
                   
                   Boolean isAutoSendCCD = false;
                   if(accountIdToAutoSendCCD.containsKey(so.Account__c)){
                       isAutoSendCCD = accountIdToAutoSendCCD.get(so.Account__c);
                   }
            
                   //below logic to switch on/off auto review check box.
                   if(
                       ((CCD.Tax_Calculation_Difference__c <= 50 && CCD.Tax_Calculation_Difference__c >= -50) 
                        || (CCD.Percentage_Difference_CDC__c <= 3 && CCD.Percentage_Difference_CDC__c >= -3))
                       &&
                       so.CPA_v2_0__r.Auto_CCD_Review_Allowed__c
                       &&
                       so.Account__r.Auto_CCD_Review_Allowed__c
                       &&
                       !isAutoSendCCD
                   ){
                  
                       CCD.Auto_Review__c = true;
                       CCD.status__c = 'Reviewed';
                       CCD.Review_Date2__c = system.now();
                   }else{
                       CCD.Auto_Review__c = false;
                   }
                
               }

        }
        
    }
    
    If(Trigger.IsUpdate && Trigger.IsAfter){
        Customs_Clearance_Documents__c[] P1 = Trigger.new;
        Customs_Clearance_Documents__c CCDemail = P1[0];
        Map<string, string> mimeTypeMap = new Map<string, string>();
        
        
        
        String SOIDs = null;
        Decimal DATPCCD1=0;
        
        Map<Id, Boolean> soIdToAutoReview = new Map<Id, Boolean>();
        
        //Updates Exchange rate from Currency management only if TAX_Currency is changed
        for(Customs_Clearance_Documents__c CCD : trigger.new) {
            
            if(CCD.Customs_Clearance_Documents__c != null){
                soIdToAutoReview.put(CCD.Customs_Clearance_Documents__c, CCD.Auto_Review__c);
            }
                
            If(CCD.Tax_Currency__c != trigger.oldMap.get(CCD.ID).Tax_Currency__c){
                list<Currency_Management2__c> CM2 = [Select Name,Conversion_Rate__c,Currency__c from Currency_Management2__c where Currency__c =: CCD.Tax_Currency__c limit 1];
                Customs_Clearance_Documents__c  CCD1 = [Select Foreign_Exchange_Rate__c from Customs_Clearance_Documents__c where id =: ccd.Id ];              
                CCD1.Foreign_Exchange_Rate__c = CM2[0].Conversion_Rate__c;
                Update CCD1;
                
            }
            
        }
        
        
        //RollOuts to ShipmentOrder
        If(P1[0].Customs_Clearance_Documents__c != null || P1[0].Customs_Clearance_Documents__c != '' ) { SOIDs = P1[0].Customs_Clearance_Documents__c;  }
        
        If(SOIDs!= null ){ 
            AggregateResult[] groupedResults = [SELECT COUNT(ID) CountID,SUM(Taxes_per_CCDs_USD__c) DATPCCD from Customs_Clearance_Documents__c  where  Customs_Clearance_Documents__c =: SOIDs];
            system.debug('groupedResults (ShipmentOrder) -->'+groupedResults);
            
            for(AggregateResult a : groupedResults){
                DATPCCD1 = (Decimal) a.get('DATPCCD');                             
            }
            
            
            Shipment_Order__C SO = [Select Id, Shipping_Status__c ,Tax_Treatment__c,Total_Taxes_CCD__c,
                                    Account__r.DDP_Client_Auto_Status_Update__c, CPA_v2_0__r.DDP_Client_Auto_Status_Update__c,
                                    Customs_Clearance_Docs_Received_Date__c,No_further_client_invoices_required__c,
                                    Closed_Down_SO__c,ICE_Incentive_Date__c  from Shipment_Order__C where Id =:SOIDs limit 1 ];
            
            
            
            if(SO.Customs_Clearance_Docs_Received_Date__c == null &&
               so.No_further_client_invoices_required__c == TRUE &&
               So.Closed_Down_SO__c == FALSE &&
               SO.ICE_Incentive_Date__c == null &&
               P1[0].status__c == 'Reviewed' ){ 
                SO.ICE_Incentive_Date__c =  system.today();
                
            }
            else {
                // Rollouts adding         
                SO.Total_Taxes_CCD__c = DATPCCD1;           
                
            }
            
            if(
                (soIdToAutoReview.containsKey(SO.Id) && soIdToAutoReview.get(SO.Id)
                 ||
                 P1[0].status__c == 'Reviewed'
                 ||
                 (SO.Tax_Treatment__c == 'DDP - Client Account' &&
                  (SO.Account__r.DDP_Client_Auto_Status_Update__c != null && SO.Account__r.DDP_Client_Auto_Status_Update__c) &&
                  (SO.CPA_v2_0__r.DDP_Client_Auto_Status_Update__c != null && SO.CPA_v2_0__r.DDP_Client_Auto_Status_Update__c)
                 )
                )
                &&
                SO.Shipping_Status__c == 'POD Received'
            ){
                SO.Shipping_Status__c = 'Customs Clearance Docs Received'; 
            }
            
            //methodcall(Shipment_Order__c so, Customs_Clearance_Documents__c CCDemail){}
            
            Update SO;
            
        }
        
        
        
        mimeTypeMap.put('POWER_POINT_X', 'application/vnd.openxmlformats-officedocument.presentationml.presentation');
        mimeTypeMap.put('POWER_POINT', 'application/vnd.ms-powerpoint');
        mimeTypeMap.put('EXCEL', 'application/vnd.ms-excel');
        mimeTypeMap.put('PDF', 'application/pdf');
        mimeTypeMap.put('pdf', 'application/pdf');
        
        
        //Send CCD Email new bulkify start CH 10580
        
        //will run ccd email on filtered record only.
        Map<Id, Id> ccdIdToSOId = new Map<Id, Id>();
        for(Customs_Clearance_Documents__c ccd : Trigger.new){
            //filtering CCD records as per criteria below.
            if(ccd.Status__c == 'Reviewed' && !ccd.CCD_Document_Sent__c){
                ccdIdToSOId.put(ccd.Id, ccd.Customs_Clearance_Documents__c);
            }
        }
        
        
        if(!ccdIdToSOId.isEmpty()){
            Map<Id, List<Id>> accountIdToContact = new Map<Id, List<Id>>();
            Map<Id, Id> soToAccountId = new Map<Id, Id>();
            Set<Id> accountIds = new Set<Id>();
            Map<Id,Shipment_Order__c> idToShipmentOrder = new Map<Id,Shipment_Order__c>([SELECT Id,Account__c, Account__r.Incl_Client_Contact_in_CCD_Email__c,Client_Contact_for_this_Shipment__c, IOR_CSE__c,Lead_AM__c, ICE__c,Lead_ICE__c FROM Shipment_Order__c
                                                                                         WHERE Id IN :ccdIdToSOId.values()]);
            for(Shipment_Order__c so : idToShipmentOrder.values()){
                //adding null here to fill later with contact Id.
                accountIdToContact.put(so.Account__c, new List<Id>());
                soToAccountId.put(so.Id,so.Account__c);
            }
            
            
            for(Contact cn : [SELECT Id, Account.Id FROM Contact WHERE Account.Id IN :accountIdToContact.keySet()
                              AND CCD_Auto_Email_to_Client__c = true]){
                                  accountIdToContact.get(cn.Account.Id).add(cn.Id);
                              }
            
            //In Email template need to fix the the merge field, like 'Contact Name'
            EmailTemplate et=[Select id from EmailTemplate where name=:'Orange CCD Notification'];
            
            // preparing attachments with ccdId to add in email
            Map<Id, List<Attachment>> ccdIdToAttachments = new Map<Id, List<Attachment>>();
            List<Attachment> allAttchments = [SELECT id, Name, body, ContentType,ParentId FROM Attachment WHERE ParentId IN :ccdIdToSOId.keySet()];    
            for(Attachment attch : allAttchments){
                if(ccdIdToAttachments.containsKey(attch.ParentId)){
                    ccdIdToAttachments.get(attch.ParentId).add(attch);
                }else{
                    ccdIdToAttachments.put(attch.ParentId, new List<Attachment>{attch});
                }
            }
            
            
            // preparing ContentDocumentLink with ccdId to add in email
            Set<Id> cdId = new Set<Id>();
            Map<Id, List<ContentDocumentLink>> ccdIdToCDL = new Map<Id, List<ContentDocumentLink>>();
            List<ContentDocumentLink> fileslink = [SELECT ContentDocumentId,ContentDocument.Title, ContentDocument.CreatedDate,LinkedEntityId
                                                   FROM ContentDocumentLink
                                                   where LinkedEntityId IN :ccdIdToSOId.keySet()];
            for(ContentDocumentLink CDL : fileslink){
                if(ccdIdToCDL.containsKey(CDL.LinkedEntityId)){
                    ccdIdToCDL.get(CDL.LinkedEntityId).add(CDL);
                }else{
                    cdId.add(CDL.ContentDocumentId);
                    ccdIdToCDL.put(CDL.LinkedEntityId, new List<ContentDocumentLink>{CDL});
                }
            }
            
            
            List<ContentVersion> fileblob = [SELECT Id, ContentDocumentId,VersionData, Title, FileExtension, FileType
                                             FROM ContentVersion 
                                             WHERE ContentDocumentId IN :cdId
                                             And IsLatest = TRUE];
            
            for(Id ccdId : ccdIdToSOId.keySet()){
                
                
                Shipment_Order__c shipment = idToShipmentOrder.get(ccdIdToSOId.get(ccdId));
                Id[] toAddress=  new List<Id>();
                
               
                
                if(!accountIdToContact.get(shipment.Account__c).isEmpty()){
                    toAddress = accountIdToContact.get(shipment.Account__c);
                }else{
                    continue;
                }
                
                if(shipment.Account__r.Incl_Client_Contact_in_CCD_Email__c != null && shipment.Account__r.Incl_Client_Contact_in_CCD_Email__c
                   && shipment.Client_Contact_for_this_Shipment__c != null){
                       toAddress.add(shipment.Client_Contact_for_this_Shipment__c);
                }
                
                List<Messaging.SingleEmailMessage> emails = new List<Messaging.SingleEmailMessage>();
                List<Messaging.EmailFileAttachment> efalist = new List<Messaging.EmailFileAttachment>();
                //contact c =[select id, email from Contact where Id = :CCDemail.Contact_Id__c];
                Messaging.SingleEmailMessage singlemail = new Messaging.SingleEmailMessage();
                
                singleMail.setTemplateId(et.Id);              
                singleMail.setTargetObjectId(toAddress[0]);
                
                
                singleMail.setToAddresses(toAddress); 
                singlemail.setwhatId(ccdId);       
                singleMail.setSaveAsActivity(true);
                
                String[] ccAddress=  new List<String>();
                if(shipment.IOR_CSE__c != null){
                    ccAddress.add(shipment.IOR_CSE__c);
                }else if(shipment.Lead_AM__c != null){
                    ccAddress.add(shipment.Lead_AM__c);
                }
                
                if(shipment.ICE__c != null){
                    ccAddress.add(shipment.ICE__c);
                }else if(shipment.Lead_ICE__c != null){
                    ccAddress.add(shipment.Lead_ICE__c);
                }

                singlemail.setCCAddresses(ccAddress);
                
                
                emails.add(singleMail);
                
                system.debug('ccdIdToAttachments ' + ccdIdToAttachments);
                system.debug('ccdId ' + ccdId);
                system.debug('ccdIdToCDL ' + ccdIdToCDL);
                
                List<ContentDocumentLink> fileslinkList = ccdIdToCDL.containsKey(ccdId) ? ccdIdToCDL.get(ccdId) : null;
                
                If(fileslinkList != null && fileslinkList.size() > 0 ){
                    
                    for(ContentVersion att : fileblob) {
                        if(att.ContentDocumentId == fileslinkList[0].ContentDocumentId){
                            Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
                            {
                                
                                efa.setFileName(att.Title + '.'+ att.FileType);
                                efa.setBody(att.VersionData);
                                //   efa.setContentType(mimeTypeMap.get(att.FileType));
                                efaList.add(efa );
                                System.debug('setContentType =' + efa.ContentType);
                                System.debug('FileName =' + efa.FileName);
                            }
                            
                            {
                                singlemail.setFileAttachments(efaList);
                            }
                        }
                    }
                    
                    Messaging.sendEmail(emails);				 
                    
                }else if(ccdIdToAttachments.containsKey(ccdId)){
                    
                    List<Attachment> allatt = ccdIdToAttachments.get(ccdId);
                    
                    for(Attachment att : allatt) {
                        Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
                        {
                            efa.setFileName(att.Name);
                            efa.setBody(att.Body);
                            efa.setContentType(att.ContentType);
                            efaList.add( efa );
                            System.debug('ContentType =' +att.ContentType);  
                        }
                        
                        {
                            singlemail.setFileAttachments(efaList);
                        }
                    }
                    
                    Messaging.sendEmail(emails);
                }
                
                
                
            }                                       
            
        }
        
        //End CH 10580
        
        
        
        // below commented code implemented above as per new logic.
        /*
        //Send CCD Email 
        if (CCDemail.Client_Name__c.contains('Orange Business ServicesTesttt') && CCDemail.Status__c == 'Reviewed' && CCDemail.CCD_Document_Sent__c== FALSE) {
            
            
            
            
            
            EmailTemplate et=[Select id from EmailTemplate where name=:'Orange CCD Notification'];
            
            List<Messaging.SingleEmailMessage> emails = new List<Messaging.SingleEmailMessage>();
            List<Messaging.EmailFileAttachment> efalist = new List<Messaging.EmailFileAttachment>();
            contact c =[select id, email from Contact where Id = :CCDemail.Contact_Id__c];
            Messaging.SingleEmailMessage singlemail = new Messaging.SingleEmailMessage();
            singleMail.setTemplateId(et.Id);              
            singleMail.setTargetObjectId(c.Id);
            singlemail.setwhatId(CCDemail.Id);       
            singleMail.setSaveAsActivity(true);
            String []ToAddress= new string[] {CCDemail.ICE_Email__c,CCDemail.AM_Email__c,'import.export.scm.nam@orange.com'};
                singlemail.setCCAddresses(toAddress);
            emails.add(singleMail);
            {
                List<Attachment> allatt = [SELECT id, Name, body, ContentType FROM Attachment WHERE ParentId = : CCDemail.id];    
                
                
                List<ContentDocumentLink> fileslink = [SELECT ContentDocumentId,ContentDocument.Title, ContentDocument.CreatedDate,LinkedEntityId
                                                       FROM ContentDocumentLink
                                                       where LinkedEntityId in ( SELECT Id FROM Customs_Clearance_Documents__c where id =:CCDemail.Id)
                                                      ];
                
                If(fileslink != null && fileslink.size() > 0 ){
                    
                    List<ContentVersion> fileblob = [SELECT Id, VersionData, Title, FileExtension, FileType
                                                     FROM ContentVersion 
                                                     WHERE ContentDocumentId = :fileslink[0].ContentDocumentId
                                                     And IsLatest = TRUE
                                                    ];
                    System.debug('FileExtension =' +fileblob[0].FileExtension);
                    System.debug('FileType =' +fileblob[0].FileType);
                    System.debug('FileExtension2 =' +mimeTypeMap.get(fileblob[0].FileExtension));
                    System.debug('FileType2 =' +mimeTypeMap.get(fileblob[0].FileType));
                    
                    for(ContentVersion att : fileblob) {
                        Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
                        {
                            
                            efa.setFileName(att.Title + '.'+ att.FileType);
                            efa.setBody(att.VersionData);
                            //   efa.setContentType(mimeTypeMap.get(att.FileType));
                            efaList.add(efa );
                            System.debug('setContentType =' + efa.ContentType);
                            System.debug('FileName =' + efa.FileName);
                        }
                        
                        {
                            singlemail.setFileAttachments(efaList);
                        }
                    }
                    
                    Messaging.sendEmail(emails);				 
                    
                }      
                
                
                Else{
                    
                    for(Attachment att : allatt) {
                        Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
                        {
                            efa.setFileName(att.Name);
                            efa.setBody(att.Body);
                            efa.setContentType(att.ContentType);
                            efaList.add( efa );
                            System.debug('ContentType =' +att.ContentType);  
                        }
                        
                        {
                            singlemail.setFileAttachments(efaList);
                        }
                    }
                    
                    Messaging.sendEmail(emails);
                }
                
            }		   
        } 
        */
        
    }     
    
    else If(Trigger.Isdelete ){
        
        String SOIDs = null;
        String DeletingCCD = null;
        Decimal DATPCCD1=0;
        
        for (Customs_Clearance_Documents__c ff : Trigger.old) {             
            If(Trigger.IsBefore){                      
                if (ff.id != null) 
                {
                    System.debug('Triggerdelete FF ID =' +ff.Id);
                    Customs_Clearance_Documents__c[] P1 = [Select ID, Customs_Clearance_Documents__c from Customs_Clearance_Documents__c where id =:ff.Id];
                    
                    If(P1[0].Customs_Clearance_Documents__c != null || P1[0].Customs_Clearance_Documents__c != '' ) { SOIDs = P1[0].Customs_Clearance_Documents__c;  }
                    
                    DeletingCCD =  ff.Id; 
                } 
            }
        }
        
        If(SOIDs!= null ){ 
            AggregateResult[] groupedResults = [SELECT COUNT(ID) CountID,SUM(Taxes_per_CCDs_USD__c) DATPCCD from Customs_Clearance_Documents__c  where  Customs_Clearance_Documents__c =: SOIDs and Customs_Clearance_Documents__c.id !=:DeletingCCD];
            system.debug('groupedResults (ShipmentOrder) -->'+groupedResults);
            
            for(AggregateResult a : groupedResults){
                DATPCCD1 = (Decimal) a.get('DATPCCD');                             
            }
            
            Shipment_Order__C SO = [Select Total_Taxes_CCD__c from Shipment_Order__C where Id =:SOIDs];
            SO.Total_Taxes_CCD__c = DATPCCD1;           
            Update SO;
        } 
        
        
        
        
        
        
    }
    
}