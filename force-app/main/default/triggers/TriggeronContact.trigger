trigger TriggeronContact on Contact (after update) {
    Set<Id> Conlist = new Set<Id>();
    if(Trigger.isAfter && Trigger.isUpdate) {
        for (Contact contact : Trigger.new) {
            if(isChanged(contact,'Buyapowa_Criteria_Met__c') 
               && contact.Buyapowa_Criteria_Met__c == true 
               && contact.Buyapowa_submitteddate__c == null 
               && contact.Buyapowa_Status__c !='Submitted'){
                   Conlist.add(contact.Id);
               }
            
        }
        if(Conlist!=null && !Conlist.isEmpty()){
            Buyapowa.submit(Conlist);
        }
    }
    
    public static Boolean isChanged(Contact contact,String fieldName){
        if(Trigger.isExecuting && Trigger.isUpdate) {
            return contact.get(fieldName) != Trigger.oldMap.get(contact.Id).get(fieldName);
        }
        return false;
    }
   }