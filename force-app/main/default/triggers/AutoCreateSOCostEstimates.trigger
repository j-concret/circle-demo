Trigger AutoCreateSOCostEstimates on Roll_Out__c (after insert, after update) {

    List<Shipment_Order__c> interviewers = new List<Shipment_Order__c>();
    List<string> Temp;

 IF(Trigger.IsAfter && Trigger.IsInsert){
    for (Roll_Out__c newPosition: Trigger.New) {
      if (newPosition.Destinations__c != null && newPosition.Create_Roll_Out__c == True){
            // split out the multi-select picklist using the semicolon delimiter
            for(String destinationlist: newPosition.Destinations__c.split(';')){
                interviewers.add(new Shipment_Order__c(
                Account__c = newPosition.Client_Name__c,
                Service_manager__c = newPosition.Client_Name__r.Service_manager__c,
                Shipment_Value_USD__c = newPosition.Shipment_Value_in_USD__c,
                Destination__c = destinationlist,
                Client_Reference__c = newPosition.Client_Reference__c,
                Client_Contact_for_this_Shipment__c = newPosition.Contact_For_The_Quote__c,
                RecordTypeId = '0120Y0000009cEz',
                Shipping_status__C = 'Roll-Out',    
                Service_Type__c = 'IOR',
                //CPA_Costings_Added__c = TRUE,
                Roll_Out__c = newPosition.Id));
                
            }
        }
        
    }
 }
    
    IF(Trigger.IsAfter && Trigger.Isupdate){
        
        for (Roll_Out__c newPosition: Trigger.New) {
      if (newPosition.Destinations__c != null && newPosition.Create_Roll_Out__c == True && (newPosition.Destinations__c != trigger.oldMap.get(newPosition.ID).Destinations__c  )){
            // split out the multi-select picklist using the semicolon delimiter
            for(String destinationlist: newPosition.Destinations__c.split(';')){
                interviewers.add(new Shipment_Order__c(
                Account__c = newPosition.Client_Name__c,
                Service_manager__c = newPosition.Client_Name__r.Service_manager__c,
                Shipment_Value_USD__c = newPosition.Shipment_Value_in_USD__c,
                Destination__c = destinationlist,
                Client_Reference__c = newPosition.Client_Reference__c,
                Client_Contact_for_this_Shipment__c = newPosition.Contact_For_The_Quote__c,
                RecordTypeId = '0120Y0000009cEz',
                Shipping_status__C = 'Roll-Out',    
                Service_Type__c = 'IOR',
                //CPA_Costings_Added__c = TRUE,
                Roll_Out__c = newPosition.Id));
                
            }
        }
        
    }
        
        
        
        
    }
    
    
    
    
  //  insert interviewers;
    
    System.debug('interviewers size...'+ interviewers.size());
    
    for(integer i = 0; interviewers.size()>i; i++  ){
    
    insert interviewers[i];
    
    //List<Id> shipmentsToAdd = new List<Id>();
    // shipmentsToAdd.add(interviewers[i].id); 
    //CalculateCosts.createSOCosts(shipmentsToAdd);
    
    

    }
  
     
    
}