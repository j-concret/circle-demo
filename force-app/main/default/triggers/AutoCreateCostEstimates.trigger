Trigger AutoCreateCostEstimates on Roll_Out__c (after insert, after update) {

    List<Cost_Estimate__c> interviewers = new List<Cost_Estimate__c>();
    List<string> Temp;


    for (Roll_Out__c newPosition: Trigger.New) {
      if (newPosition.Destinations__c != null && newPosition.Create_Roll_Out__c == True){
            // split out the multi-select picklist using the semicolon delimiter
            for(String destinationlist: newPosition.Destinations__c.split(';')){
                interviewers.add(new Cost_Estimate__c(
                Client_Name__c = newPosition.Client_Name__c,
                Shipment_Value_in_USD__c = newPosition.Shipment_Value_in_USD__c,
                Destination__c = destinationlist,
                My_Reference__c= newPosition.Client_Reference__c,
                Roll_Out__c = newPosition.Id));
                
            }
        }
        
    }
    insert interviewers;
    
}