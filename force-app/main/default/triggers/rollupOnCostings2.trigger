trigger rollupOnCostings2 on CPA_Costing__c (after insert,  before update, after update, after delete) {
    
    if(Trigger.isInsert && Trigger.isAfter) {
        
        Set<Id> invIds = new Set<Id>();
        Set<Id> soIds = new Set<Id>();
        Set<Id> oldInvIds = new Set<Id>();
        
        for(CPA_Costing__c CPAC : trigger.new) {
            if(!invIds.contains(CPAC.Invoice__c) && CPAC.Invoice__c != null) {
                invIds.add(CPAC.Invoice__c);
            }
            if(!invIds.contains(CPAC.Shipment_Order_Old__c) && CPAC.Shipment_Order_Old__c != null) {
                soIds.add(CPAC.Shipment_Order_Old__c);
            }
            if(!invIds.contains(CPAC.Supplier_Invoice__c) && CPAC.Supplier_Invoice__c != null) {
                oldInvIds.add(CPAC.Supplier_Invoice__c);
            }
        }
        
        if(soIds.size() > 0 && oldInvIds.size() > 0 && invIds.size() == 0) {
            
            List<Supplier_Invoice__c> sisToUpdate = [SELECT Id, Actual_Duties_and_Taxes_Other__c,Actual_Tax_Forex_Spread__c,FC_Admin_Fee__c,
                                                     FC_Bank_Fees__c,FC_Finance_Fee__c,FC_Insurance_Fee_USD__c,FC_Miscellaneous_Fee__c,FC_International_Delivery_Fee__c,Total_Duties_and_Taxes__c,Total_Brokerage_Costs__c,Total_Clearance_Costs__c,Total_Handling_Costs__c,Total_License_Permits_Cost__c,Total_IOR_EOR__c,Actual_Admin_Fee__c,Actual_Bank_Fees__c,Actual_Finance_Fee__c,Actual_Insurance_Fee_USD__c,Actual_Miscellaneous_Fee__c,Actual_International_Delivery_Fee__c,Actual_Duties_and_Taxes__c,Actual_Brokerage_Costs__c,Actual_Clearance_Costs__c,Actual_Handling_Costs__c,Actual_License_Permits_Cost__c,Actual_IOR_EOR_Cost__c FROM supplier_Invoice__c WHERE id=: oldInvIds];
            
            for (Supplier_Invoice__c SO: sisToUpdate) {
                SO.Roll_Up_Costings__c = true;
            }
            update sisToUpdate;
        }
        
        if((soIds.size() > 0 && invIds.size() == 0 && oldInvIds.size() == 0) || (soIds.size() > 0 && invIds.size() > 0 && oldInvIds.size() == 0)) {
            
            List<Shipment_Order__c> soToUpdate = [SELECT Id, Name, CI_Admin_Fee__c, CI_Bank_Fees__c, CI_Cash_Outlay_Fee__c,
                                                  CI_Collection_Administration_Fee__c,CI_IOR_and_Import_Compliance_Fee_USD__c, CI_International_Delivery_Fee__c, CI_Liability_Cover__c, CI_Miscellaneous_Fee__c,
                                                  CI_Recharge_Tax_and_Duty__c, CI_Total_Clearance_Costs__c, CI_Total_Customs_Brokerage_Cost__c, CI_Total_Handling_Cost__c,
                                                  CI_Total_Licence_Cost__c, Revenue_Ad_Hoc_Fees__c, Revenue_Bank_Charges_Recovered__c, Revenue_Exporter_of_Record__c,
                                                  Revenue_Financing_and_Insurance__c, Revenue_Importer_of_Record__c, Revenue_Insurance_Fees__c, Revenue_On_Charges__c,
                                                  Revenue_Shipping_Insurance__c, Revenue_Tax__c, Actual_Admin_Fee__c, Actual_Bank_Fees__c, Actual_Duties_and_Taxes_Other__c,
                                                  Actual_Finance_Fee__c, Actual_Insurance_Fee_USD__c, Actual_International_Delivery_Fee__c, Actual_Miscellaneous_Fee__c,
                                                  Actual_Total_Clearance_Costs__c, Actual_Total_Customs_Brokerage_Cost__c, Actual_Total_Handling_Costs__c, Actual_Total_IOR_EOR__c,
                                                  Actual_Total_License_Cost__c, Actual_Total_Tax_and_Duty__c, Cost_of_Sale_Ad_Hoc_Fees__c, Cost_of_Sale_Bank_Charges_Recovered__c,
                                                  Cost_of_Sale_Exporter_of_Record__c, Cost_of_Sale_Financing_and_Insurance__c, Cost_of_Sale_Importer_of_Record__c,  Service_Type__c,
                                                  Cost_of_Sale_Insurance_Fees__c, Cost_of_Sale_On_Charges__c, Cost_of_Sale_Shipping_Insurance__c, Cost_of_Sale_Tax__c
                                                  FROM Shipment_Order__c
                                                  WHERE Id IN: soIds];
            
            
            for (Shipment_Order__c SO: soToUpdate) {
                SO.Roll_Up_Costings__c = true;
            }
            update soToUpdate;
        }
        
        if(soIds.size() > 0 && invIds.size() == 0 && oldInvIds.size() > 0) {
            
            List<Shipment_Order__c> soToUpdate = [SELECT Id, Name, CI_Admin_Fee__c, CI_Bank_Fees__c, CI_Cash_Outlay_Fee__c,
                                                  CI_Collection_Administration_Fee__c,
                                                  CI_IOR_and_Import_Compliance_Fee_USD__c, CI_International_Delivery_Fee__c, CI_Liability_Cover__c, CI_Miscellaneous_Fee__c,
                                                  CI_Recharge_Tax_and_Duty__c, CI_Total_Clearance_Costs__c, CI_Total_Customs_Brokerage_Cost__c, CI_Total_Handling_Cost__c,
                                                  CI_Total_Licence_Cost__c, Revenue_Ad_Hoc_Fees__c, Revenue_Bank_Charges_Recovered__c, Revenue_Exporter_of_Record__c,
                                                  Revenue_Financing_and_Insurance__c, Revenue_Importer_of_Record__c, Revenue_Insurance_Fees__c, Revenue_On_Charges__c,
                                                  Revenue_Shipping_Insurance__c, Revenue_Tax__c, Actual_Admin_Fee__c, Actual_Bank_Fees__c, Actual_Duties_and_Taxes_Other__c,
                                                  Actual_Finance_Fee__c, Actual_Insurance_Fee_USD__c, Actual_International_Delivery_Fee__c, Actual_Miscellaneous_Fee__c,
                                                  Actual_Total_Clearance_Costs__c, Actual_Total_Customs_Brokerage_Cost__c, Actual_Total_Handling_Costs__c, Actual_Total_IOR_EOR__c,
                                                  Actual_Total_License_Cost__c, Actual_Total_Tax_and_Duty__c, Cost_of_Sale_Ad_Hoc_Fees__c, Cost_of_Sale_Bank_Charges_Recovered__c,
                                                  Cost_of_Sale_Exporter_of_Record__c, Cost_of_Sale_Financing_and_Insurance__c, Cost_of_Sale_Importer_of_Record__c,  Service_Type__c,
                                                  Cost_of_Sale_Insurance_Fees__c, Cost_of_Sale_On_Charges__c, Cost_of_Sale_Shipping_Insurance__c, Cost_of_Sale_Tax__c
                                                  FROM Shipment_Order__c
                                                  WHERE Id IN: soIds];
            
            
            for (Shipment_Order__c SO:soToUpdate) {
                SO.Roll_Up_Costings_A__c = true;
            }
            update soToUpdate;
        }
        
        if(soIds.size() > 0 && invIds.size() > 0 && oldInvIds.size() == 0) {
            List<Invoice_New__c> invoiceToUpdate = [SELECT Id, Actual_Admin_Costs__c, Actual_Bank_Costs__c, Actual_Customs_Brokerage_Costs__c, Actual_Customs_Clearance_Costs__c,
                                                    Actual_Customs_Handling_Costs__c,Actual_Customs_License_Costs__c, Actual_Duties_and_Taxes__c, Actual_Duties_and_Taxes_Other__c, Actual_Finance_Costs__c, Actual_Insurance_Costs__c, Actual_International_Delivery_Costs__c, Actual_IOREOR_Costs__c, Actual_Miscellaneous_Costs__c, FC_Customs_Brokerage_Costs__c, FC_Customs_Clearance_Costs__c,
                                                    FC_Customs_Handling_Costs__c, FC_Customs_License_Costs__c, FC_Duties_and_Taxes__c, FC_Duties_and_Taxes_Other__c, FC_Finance_Costs__c, FC_Insurance_Costs__c, FC_International_Delivery_Costs__c, FC_IOR_Costs__c, FC_Miscellaneous_Costs__c, Conversion_Rate__c FROM Invoice_New__c WHERE id =: invIds];
            
            for (Invoice_New__c SI: invoiceToUpdate) {
                SI.Roll_Up_Costings__c = true;
            }
            update invoiceToUpdate;
        }
    }
    
    if(Trigger.isUpdate && Trigger.isBefore) {
        Set<Id> invIds = new Set<Id>();
        for(CPA_Costing__c CPAC : trigger.new) {
            Decimal chargeableAmount = 0;
            invIds.add(CPAC.Invoice__c);
            
            if((CPAC.Rate__c != trigger.oldMap.get(CPAC.ID).Rate__c) ||
               (CPAC.VAT_Rate__c != trigger.oldMap.get(CPAC.ID).VAT_Rate__c) ||
               (CPAC.Variable_threshold__c != trigger.oldMap.get(CPAC.ID).Variable_threshold__c) ||
               (CPAC.Additional_Percent__c != trigger.oldMap.get(CPAC.ID).Additional_Percent__c) ||
               (CPAC.Applied_to__c != trigger.oldMap.get(CPAC.ID).Applied_to__c) &&
               CPAC.RecordTypeID == Schema.Sobjecttype.CPA_Costing__c.getRecordTypeInfosByDeveloperName().get('Display').getRecordTypeId()) {
                   
                   Map<Id, Shipment_Order__c> shipmentOrder = new Map<Id,Shipment_Order__c>([SELECT Id, Name, Shipment_Value_USD__c, IOR_Refund_of_Tax_Duty__c, Chargeable_Weight__c,
                                                                                             of_Line_Items__c, of_Unique_Line_Items__c, Final_Deliveries_New__c, of_packages__c
                                                                                             FROM Shipment_Order__c
                                                                                             WHERE Id =: CPAC.Shipment_Order_Old__c]);
                   
                   if( CPAC.Rate__c != null){
                       if( CPAC.Applied_to__c == 'Shipment value') {
                           chargeableAmount = shipmentOrder.get(CPAC.Shipment_Order_Old__c).Shipment_Value_USD__c;
                       }
                       else if( CPAC.Applied_to__c == 'Total Taxes') {
                           chargeableAmount = shipmentOrder.get(CPAC.Shipment_Order_Old__c).IOR_Refund_of_Tax_Duty__c;
                       }
                       else if( CPAC.Applied_to__c == 'CIF value') {
                           chargeableAmount = (shipmentOrder.get(CPAC.Shipment_Order_Old__c).Shipment_Value_USD__c + 250) * 1.05;
                       }
                       else if( CPAC.Applied_to__c == 'Chargeable weight') {
                           chargeableAmount = shipmentOrder.get(CPAC.Shipment_Order_Old__c).Chargeable_Weight__c ;
                       }
                       else if( CPAC.Applied_to__c == '# Parts (line items)') {
                           chargeableAmount =  shipmentOrder.get(CPAC.Shipment_Order_Old__c).of_Line_Items__c ;
                       }
                       else if( CPAC.Applied_to__c == '# Unique HS Codes') {
                           chargeableAmount =  shipmentOrder.get(CPAC.Shipment_Order_Old__c).of_Unique_Line_Items__c;
                       }
                       else if( CPAC.Applied_to__c == '# of Final Deliveries') {
                           chargeableAmount =  shipmentOrder.get(CPAC.Shipment_Order_Old__c).Final_Deliveries_New__c;
                       }
                       else {
                           chargeableAmount = shipmentOrder.get(CPAC.Shipment_Order_Old__c).of_packages__c;
                       }
                       
                       CPAC.Inactive__c = CPAC.Variable_threshold__c > 0 && chargeableAmount < CPAC.Variable_threshold__c;
                       
                       
                       if(CPAC.Applied_to__c == 'Shipment value' || CPAC.Applied_to__c == 'Total Taxes' || CPAC.Applied_to__c == 'CIF value' ){
                           CPAC.Amount__c  =  (chargeableAmount - CPAC.Variable_threshold__c ) * (CPAC.Rate__c /100) * (1+(CPAC.VAT_Rate__c/100) ) * (1+(CPAC.Additional_Percent__c /100));
                           
                       }
                       else if(CPAC.Applied_to__c == 'Chargeable weight'){
                           CPAC.Amount__c  =  (chargeableAmount - CPAC.Variable_threshold__c ) * CPAC.Rate__c  * (1+CPAC.VAT_Rate__c/100 ) * (1+CPAC.Additional_Percent__c );
                           
                       }
                       else if(CPAC.Applied_to__c == '# Parts (line items)' || CPAC.Applied_to__c == '# Unique HS Codes' || CPAC.Applied_to__c == '# of Final Deliveries'){
                           CPAC.Amount__c  =  (chargeableAmount - CPAC.Variable_threshold__c ) * CPAC.Rate__c  * (1+CPAC.VAT_Rate__c ) * (1+CPAC.Additional_Percent__c );
                           
                       }else{
                           CPAC.Amount__c  = (chargeableAmount - CPAC.Variable_threshold__c ) * CPAC.Rate__c  * (1+CPAC.VAT_Rate__c ) * (1+CPAC.Additional_Percent__c );
                       }
                   }
                   
                   
                   //CPAC.Amended__c = true;
               }
            if(CPAC.Amount__c != trigger.oldMap.get(CPAC.ID).Amount__c &&
               CPAC.RecordTypeID == Schema.Sobjecttype.CPA_Costing__c.getRecordTypeInfosByDeveloperName().get('Display').getRecordTypeId()) {
                   //CPAC.Amended__c = true;
               }
        }
    }
    
    if(Trigger.isUpdate && Trigger.IsAfter) {
        Set<Id> invIds = new Set<Id>();
        Set<Id> soIds = new Set<Id>();
        Set<Id> oldInvIds = new Set<Id>();
        
        
        for(CPA_Costing__c CPAC : trigger.new) {
            
            if((CPAC.Invoice_amount_local_currency__c != trigger.oldMap.get(CPAC.Id).Invoice_amount_local_currency__c && CPAC.Invoice_amount_local_currency__c != null) ||
               (CPAC.Amount__c != trigger.oldMap.get(CPAC.Id).Amount__c && CPAC.Amount__c != null) ||
               (CPAC.Cost_Category__c != trigger.oldMap.get(CPAC.id).Cost_Category__c && CPAC.Cost_Category__c != null) && CPAC.Shipment_Order_Old__c != null ) {
                   if(!invIds.contains(CPAC.Invoice__c) && CPAC.Invoice__c != null) {invIds.add(CPAC.Invoice__c);}
                   if(!invIds.contains(CPAC.Shipment_Order_Old__c) && CPAC.Shipment_Order_Old__c != null) {soIds.add(CPAC.Shipment_Order_Old__c);}
                   if(!invIds.contains(CPAC.Supplier_Invoice__c) && CPAC.Supplier_Invoice__c != null) {oldInvIds.add(CPAC.Supplier_Invoice__c);}
               }
        }
        
        if(soIds.size() > 0 && oldInvIds.size() > 0 && invIds.size() == 0) {
            List<Supplier_Invoice__c> sisToUpdate = [SELECT id, Actual_Duties_and_Taxes_Other__c,Actual_Tax_Forex_Spread__c,FC_Admin_Fee__c,FC_Bank_Fees__c,FC_Finance_Fee__c,
                                                     FC_Insurance_Fee_USD__c,FC_Miscellaneous_Fee__c,FC_International_Delivery_Fee__c,Total_Duties_and_Taxes__c,Total_Brokerage_Costs__c,
                                                     Total_Clearance_Costs__c,Total_Handling_Costs__c,Total_License_Permits_Cost__c,Total_IOR_EOR__c,Actual_Admin_Fee__c,Actual_Bank_Fees__c,
                                                     Actual_Finance_Fee__c,Actual_Insurance_Fee_USD__c,Actual_Miscellaneous_Fee__c,Actual_International_Delivery_Fee__c,
                                                     Actual_Duties_and_Taxes__c,Actual_Brokerage_Costs__c,Actual_Clearance_Costs__c,Actual_Handling_Costs__c,Actual_License_Permits_Cost__c,
                                                     Actual_IOR_EOR_Cost__c
                                                     FROM supplier_Invoice__C
                                                     WHERE id=: oldInvIds];
            
            for (Supplier_Invoice__c SO: sisToUpdate) {
                SO.Roll_Up_Costings__c = true;
            }
            update sisToUpdate;
        }
        
        if((soIds.size() > 0 && invIds.size() == 0 && oldInvIds.size() == 0) ||(soIds.size() > 0 && invIds.size() > 0 && oldInvIds.size() == 0)) {
            
            List<Shipment_Order__c> soToUpdate = [SELECT Id, Name, CI_Admin_Fee__c, CI_Bank_Fees__c, CI_Cash_Outlay_Fee__c, CI_Collection_Administration_Fee__c,
                                                  CI_IOR_and_Import_Compliance_Fee_USD__c, CI_International_Delivery_Fee__c, CI_Liability_Cover__c, CI_Miscellaneous_Fee__c,
                                                  CI_Recharge_Tax_and_Duty__c, CI_Total_Clearance_Costs__c, CI_Total_Customs_Brokerage_Cost__c, CI_Total_Handling_Cost__c,
                                                  CI_Total_Licence_Cost__c, Revenue_Ad_Hoc_Fees__c, Revenue_Bank_Charges_Recovered__c, Revenue_Exporter_of_Record__c,
                                                  Revenue_Financing_and_Insurance__c, Revenue_Importer_of_Record__c, Revenue_Insurance_Fees__c, Revenue_On_Charges__c,
                                                  Revenue_Shipping_Insurance__c, Revenue_Tax__c, Actual_Admin_Fee__c, Actual_Bank_Fees__c, Actual_Duties_and_Taxes_Other__c,
                                                  Actual_Finance_Fee__c, Actual_Insurance_Fee_USD__c, Actual_International_Delivery_Fee__c, Actual_Miscellaneous_Fee__c,
                                                  Actual_Total_Clearance_Costs__c, Actual_Total_Customs_Brokerage_Cost__c, Actual_Total_Handling_Costs__c, Actual_Total_IOR_EOR__c,
                                                  Actual_Total_License_Cost__c, Actual_Total_Tax_and_Duty__c, Cost_of_Sale_Ad_Hoc_Fees__c, Cost_of_Sale_Bank_Charges_Recovered__c,
                                                  Cost_of_Sale_Exporter_of_Record__c, Cost_of_Sale_Financing_and_Insurance__c, Cost_of_Sale_Importer_of_Record__c,  Service_Type__c,
                                                  Cost_of_Sale_Insurance_Fees__c, Cost_of_Sale_On_Charges__c, Cost_of_Sale_Shipping_Insurance__c, Cost_of_Sale_Tax__c
                                                  FROM Shipment_Order__c
                                                  WHERE Id IN: soIds];
            
            for (Shipment_Order__c SO: soToUpdate) {
                SO.Roll_Up_Costings__c = true;
            }
            update soToUpdate;
        }
        
        if(soIds.size() > 0 && invIds.size() == 0 && oldInvIds.size() > 0) {
            List<Shipment_Order__c> soToUpdate = [SELECT Id, Name, CI_Admin_Fee__c, CI_Bank_Fees__c, CI_Cash_Outlay_Fee__c, CI_Collection_Administration_Fee__c,
                                                  CI_IOR_and_Import_Compliance_Fee_USD__c, CI_International_Delivery_Fee__c, CI_Liability_Cover__c, CI_Miscellaneous_Fee__c,
                                                  CI_Recharge_Tax_and_Duty__c, CI_Total_Clearance_Costs__c, CI_Total_Customs_Brokerage_Cost__c, CI_Total_Handling_Cost__c,
                                                  CI_Total_Licence_Cost__c, Revenue_Ad_Hoc_Fees__c, Revenue_Bank_Charges_Recovered__c, Revenue_Exporter_of_Record__c,
                                                  Revenue_Financing_and_Insurance__c, Revenue_Importer_of_Record__c, Revenue_Insurance_Fees__c, Revenue_On_Charges__c,
                                                  Revenue_Shipping_Insurance__c, Revenue_Tax__c, Actual_Admin_Fee__c, Actual_Bank_Fees__c, Actual_Duties_and_Taxes_Other__c,
                                                  Actual_Finance_Fee__c, Actual_Insurance_Fee_USD__c, Actual_International_Delivery_Fee__c, Actual_Miscellaneous_Fee__c,
                                                  Actual_Total_Clearance_Costs__c, Actual_Total_Customs_Brokerage_Cost__c, Actual_Total_Handling_Costs__c, Actual_Total_IOR_EOR__c,
                                                  Actual_Total_License_Cost__c, Actual_Total_Tax_and_Duty__c, Cost_of_Sale_Ad_Hoc_Fees__c, Cost_of_Sale_Bank_Charges_Recovered__c,
                                                  Cost_of_Sale_Exporter_of_Record__c, Cost_of_Sale_Financing_and_Insurance__c, Cost_of_Sale_Importer_of_Record__c,  Service_Type__c,
                                                  Cost_of_Sale_Insurance_Fees__c, Cost_of_Sale_On_Charges__c, Cost_of_Sale_Shipping_Insurance__c, Cost_of_Sale_Tax__c
                                                  FROM Shipment_Order__c
                                                  WHERE Id IN: soIds];
            
            for (Shipment_Order__c SO: soToUpdate) {
                SO.Roll_Up_Costings_A__c = true;
            }
            update soToUpdate;
        }
        
        if(soIds.size() > 0 && invIds.size() > 0 && oldInvIds.size() == 0) {
            List<Invoice_New__c> invoiceToUpdate = [SELECT Id, Actual_Admin_Costs__c, Actual_Bank_Costs__c, Actual_Customs_Brokerage_Costs__c, Actual_Customs_Clearance_Costs__c,
                                                    Actual_Customs_Handling_Costs__c, Actual_Customs_License_Costs__c, Actual_Duties_and_Taxes__c, Actual_Duties_and_Taxes_Other__c, Actual_Finance_Costs__c, Actual_Insurance_Costs__c,Actual_International_Delivery_Costs__c, Actual_IOREOR_Costs__c, Actual_Miscellaneous_Costs__c, FC_Customs_Brokerage_Costs__c, FC_Customs_Clearance_Costs__c,
                                                    FC_Customs_Handling_Costs__c, FC_Customs_License_Costs__c, FC_Duties_and_Taxes__c, FC_Duties_and_Taxes_Other__c, FC_Finance_Costs__c, FC_Insurance_Costs__c,FC_International_Delivery_Costs__c, FC_IOR_Costs__c, FC_Miscellaneous_Costs__c, Conversion_Rate__c FROM Invoice_New__c WHERE id =: invIds];
            
            for (Invoice_New__c SI: invoiceToUpdate) {
                SI.Roll_Up_Costings__c = true;
            }
            update invoiceToUpdate;
        }
    }
    
    if(Trigger.IsAfter && Trigger.isDelete) {
        
        Set<Id> invIds = new Set<Id>();
        Set<Id> soIds = new Set<Id>();
        Set<Id> oldInvIds = new Set<Id>();
        
        for(CPA_Costing__c CPAC : trigger.old) {
            if(CPAC.Supplier_Invoice__c != null || CPAC.Invoice__c != null) {
                CPAC.addError('Selected record linked an supplier invoice. It cant be deleted ');
            }
            else if(CPAC.Supplier_Invoice__c == null && CPAC.Invoice__c == null && CPAC.DeleteAll__c == false) {
                if(!invIds.contains(CPAC.Invoice__c) && CPAC.Invoice__c != null) {
                    invIds.add(CPAC.Invoice__c);
                }
                if(!invIds.contains(CPAC.Shipment_Order_Old__c) && CPAC.Shipment_Order_Old__c != null) {
                    soIds.add(CPAC.Shipment_Order_Old__c);
                }
                if(!invIds.contains(CPAC.Supplier_Invoice__c) && CPAC.Supplier_Invoice__c != null) {
                    oldInvIds.add(CPAC.Supplier_Invoice__c);
                }
            }
        }
        
        if(soIds.size() > 0 && oldInvIds.size() > 0 && invIds.size() == 0) {
            List<Supplier_Invoice__C> sisToUpdate = [SELECT id, Actual_Duties_and_Taxes_Other__c,Actual_Tax_Forex_Spread__c,FC_Admin_Fee__c,FC_Bank_Fees__c,FC_Finance_Fee__c,
                                                     FC_Insurance_Fee_USD__c,FC_Miscellaneous_Fee__c,FC_International_Delivery_Fee__c,Total_Duties_and_Taxes__c,Total_Brokerage_Costs__c,
                                                     Total_Clearance_Costs__c,Total_Handling_Costs__c,Total_License_Permits_Cost__c,Total_IOR_EOR__c,Actual_Admin_Fee__c,Actual_Bank_Fees__c,
                                                     Actual_Finance_Fee__c,Actual_Insurance_Fee_USD__c,Actual_Miscellaneous_Fee__c,Actual_International_Delivery_Fee__c,
                                                     Actual_Duties_and_Taxes__c,Actual_Brokerage_Costs__c,Actual_Clearance_Costs__c,Actual_Handling_Costs__c,Actual_License_Permits_Cost__c,
                                                     Actual_IOR_EOR_Cost__c
                                                     FROM supplier_Invoice__C
                                                     WHERE id=: oldInvIds];
            
            for (Supplier_Invoice__C SO: sisToUpdate) {
                SO.Roll_Up_Costings__c = true;
            }
            update sisToUpdate;
        }
        
        if((soIds.size() > 0 && invIds.size() == 0 && oldInvIds.size() == 0) || (soIds.size() > 0 && invIds.size() > 0 && oldInvIds.size() == 0)) {
            
            List<Shipment_Order__c> soToUpdate = [SELECT Id, Name, CI_Admin_Fee__c, CI_Bank_Fees__c, CI_Cash_Outlay_Fee__c, CI_Collection_Administration_Fee__c,
                                                  CI_IOR_and_Import_Compliance_Fee_USD__c, CI_International_Delivery_Fee__c, CI_Liability_Cover__c, CI_Miscellaneous_Fee__c,
                                                  CI_Recharge_Tax_and_Duty__c, CI_Total_Clearance_Costs__c, CI_Total_Customs_Brokerage_Cost__c, CI_Total_Handling_Cost__c,
                                                  CI_Total_Licence_Cost__c, Revenue_Ad_Hoc_Fees__c, Revenue_Bank_Charges_Recovered__c, Revenue_Exporter_of_Record__c,
                                                  Revenue_Financing_and_Insurance__c, Revenue_Importer_of_Record__c, Revenue_Insurance_Fees__c, Revenue_On_Charges__c,
                                                  Revenue_Shipping_Insurance__c, Revenue_Tax__c, Actual_Admin_Fee__c, Actual_Bank_Fees__c, Actual_Duties_and_Taxes_Other__c,
                                                  Actual_Finance_Fee__c, Actual_Insurance_Fee_USD__c, Actual_International_Delivery_Fee__c, Actual_Miscellaneous_Fee__c,
                                                  Actual_Total_Clearance_Costs__c, Actual_Total_Customs_Brokerage_Cost__c, Actual_Total_Handling_Costs__c, Actual_Total_IOR_EOR__c,
                                                  Actual_Total_License_Cost__c, Actual_Total_Tax_and_Duty__c, Cost_of_Sale_Ad_Hoc_Fees__c, Cost_of_Sale_Bank_Charges_Recovered__c,
                                                  Cost_of_Sale_Exporter_of_Record__c, Cost_of_Sale_Financing_and_Insurance__c, Cost_of_Sale_Importer_of_Record__c,  Service_Type__c,
                                                  Cost_of_Sale_Insurance_Fees__c, Cost_of_Sale_On_Charges__c, Cost_of_Sale_Shipping_Insurance__c, Cost_of_Sale_Tax__c
                                                  FROM Shipment_Order__c
                                                  WHERE Id IN: soIds];
            
            for (Shipment_Order__c SO: soToUpdate) {
                SO.Roll_Up_Costings__c = true;
            }
            update soToUpdate;
        }
        
        if(soIds.size() > 0 && invIds.size() == 0 && oldInvIds.size() > 0) {
            List<Shipment_Order__c> soToUpdate = [SELECT Id, Name, CI_Admin_Fee__c, CI_Bank_Fees__c, CI_Cash_Outlay_Fee__c, CI_Collection_Administration_Fee__c,
                                                  CI_IOR_and_Import_Compliance_Fee_USD__c, CI_International_Delivery_Fee__c, CI_Liability_Cover__c, CI_Miscellaneous_Fee__c,
                                                  CI_Recharge_Tax_and_Duty__c, CI_Total_Clearance_Costs__c, CI_Total_Customs_Brokerage_Cost__c, CI_Total_Handling_Cost__c,
                                                  CI_Total_Licence_Cost__c, Revenue_Ad_Hoc_Fees__c, Revenue_Bank_Charges_Recovered__c, Revenue_Exporter_of_Record__c,
                                                  Revenue_Financing_and_Insurance__c, Revenue_Importer_of_Record__c, Revenue_Insurance_Fees__c, Revenue_On_Charges__c,
                                                  Revenue_Shipping_Insurance__c, Revenue_Tax__c, Actual_Admin_Fee__c, Actual_Bank_Fees__c, Actual_Duties_and_Taxes_Other__c,
                                                  Actual_Finance_Fee__c, Actual_Insurance_Fee_USD__c, Actual_International_Delivery_Fee__c, Actual_Miscellaneous_Fee__c,
                                                  Actual_Total_Clearance_Costs__c, Actual_Total_Customs_Brokerage_Cost__c, Actual_Total_Handling_Costs__c, Actual_Total_IOR_EOR__c,
                                                  Actual_Total_License_Cost__c, Actual_Total_Tax_and_Duty__c, Cost_of_Sale_Ad_Hoc_Fees__c, Cost_of_Sale_Bank_Charges_Recovered__c,
                                                  Cost_of_Sale_Exporter_of_Record__c, Cost_of_Sale_Financing_and_Insurance__c, Cost_of_Sale_Importer_of_Record__c,  Service_Type__c,
                                                  Cost_of_Sale_Insurance_Fees__c, Cost_of_Sale_On_Charges__c, Cost_of_Sale_Shipping_Insurance__c, Cost_of_Sale_Tax__c
                                                  FROM Shipment_Order__c WHERE Id IN: soIds];
            for (Shipment_Order__c SO: soToUpdate) {
                SO.Roll_Up_Costings_A__c = true;
            }
            update soToUpdate;
        }
        
        if(soIds.size() > 0 && invIds.size() > 0 && oldInvIds.size() == 0) {
            List<Invoice_New__c> invoiceToUpdate = [SELECT Id, Actual_Admin_Costs__c, Actual_Bank_Costs__c, Actual_Customs_Brokerage_Costs__c, Actual_Customs_Clearance_Costs__c,
                                                    Actual_Customs_Handling_Costs__c,Actual_Customs_License_Costs__c, Actual_Duties_and_Taxes__c, Actual_Duties_and_Taxes_Other__c, Actual_Finance_Costs__c, Actual_Insurance_Costs__c,Actual_International_Delivery_Costs__c, Actual_IOREOR_Costs__c, Actual_Miscellaneous_Costs__c, FC_Customs_Brokerage_Costs__c, FC_Customs_Clearance_Costs__c,
                                                    FC_Customs_Handling_Costs__c, FC_Customs_License_Costs__c, FC_Duties_and_Taxes__c, FC_Duties_and_Taxes_Other__c, FC_Finance_Costs__c, FC_Insurance_Costs__c, FC_International_Delivery_Costs__c, FC_IOR_Costs__c, FC_Miscellaneous_Costs__c, Conversion_Rate__c FROM Invoice_New__c WHERE id =: invIds];
            
            for (Invoice_New__c SI: invoiceToUpdate) {
                SI.Roll_Up_Costings__c = true;
            }
            update invoiceToUpdate;
        }
    }
}