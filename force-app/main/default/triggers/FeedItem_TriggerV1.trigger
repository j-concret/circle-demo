trigger FeedItem_TriggerV1 on FeedItem (after insert) {
    if(Trigger.isAfter && Trigger.isInsert){
        FeedItem_Trigger_Handler.updateCaseAndTask(Trigger.New);
    }
}