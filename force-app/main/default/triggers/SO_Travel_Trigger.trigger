trigger SO_Travel_Trigger on SO_Travel_History__c (after insert) {
    If(Trigger.isAfter && Trigger.isInsert){
        Map<String,String> forFirebaseSOTHmap = new Map<String,String>();
        Map<String,String> SoMap = new Map<String,String>();
        Map<String,String> SoMapAadil = new Map<String,String>();
        
        
        
        for(SO_Travel_History__c soth: Trigger.New){
            
            if( (soth.Type_of_Update__c == 'MAJOR') ||
               (soth.Type_of_Update__c == 'MINOR' && soth.Client_Notifications_Choice__c == 'Opt-In') ||
               ( soth.Source__c =='Tasks (client)' && soth.Client_Tasks_notification_choice__c == true) || 
               (soth.Source__c == 'Manual Status Change' && soth.Type_of_Update__c == 'NONE')|| 
               (soth.Source__c == 'FDA Status changed' && soth.Type_of_Update__c == 'NONE')){
                   
				     SoMap.put(soth.Id,'all'); 
                 
                
               }else if((soth.Type_of_Update__c == 'MINOR' && soth.Client_Notifications_Choice__c == 'Opt-Out') ||
                        ( soth.Source__c =='Tasks (client)' && soth.Client_Tasks_notification_choice__c == false) ||
                        (soth.Source__c == 'Task (internal)') )
               {
                   
                       SoMap.put(soth.Id,'Internal');
                
               }
            //For Pushnotifications - start
             if( soth.Client_Push_Notifications_Choice__c == 'Opt-In' && ((soth.Type_of_Update__c == 'MAJOR') ||
                                                                           (soth.Type_of_Update__c == 'MINOR') ||
                (soth.Source__c == 'Manual Status Change' && soth.Type_of_Update__c == 'NONE')|| 
                (soth.Source__c == 'FDA Status changed' && soth.Type_of_Update__c == 'NONE'))){
                    forFirebaseSOTHmap.put(soth.Id,'all');
                
               }
             else if( soth.Client_Push_Notifications_Choice__c == 'Opt-Out' && ((soth.Type_of_Update__c == 'MAJOR') ||
                                                                          
                (soth.Source__c == 'Manual Status Change' && soth.Type_of_Update__c == 'NONE')|| 
                (soth.Source__c == 'FDA Status changed' && soth.Type_of_Update__c == 'NONE'))){
                                          forFirebaseSOTHmap.put(soth.Id,'all');
                       }
            //For Pushnotifications - end
            
        }
        
        if(SoMap != null && !SoMap.isEmpty()){
            
            SOTravelNotificationHelper.sendEmailNotification(SoMap);
            
        }
        
        if(forFirebaseSOTHmap != null && !forFirebaseSOTHmap.isEmpty()){
            
        FirebaseNotifications.notifyFirebaseInBulkmap(forFirebaseSOTHmap);
        }
       
      
        
        
        
    }
}