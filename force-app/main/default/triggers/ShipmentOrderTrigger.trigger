trigger ShipmentOrderTrigger on Shipment_Order__c (before insert,after insert,before update,after update) {
    /*System.debug('IsInsert ->' + Trigger.isInsert);
       System.debug('IsBefore ->' + Trigger.IsBefore);
       System.debug('IsUpdate ->' + Trigger.IsUpdate);
       System.debug('IsAfter ->' + Trigger.IsAfter);
       System.debug('Trigger new. ' + JSON.serialize(Trigger.new[0]));*/
    if(RecusrsionHandler.skipTriggerExecution) return;
    System.debug('start'+Trigger.new[0].CPA_v2_0__c);
    if(Trigger.isBefore && Trigger.isInsert) {
        List<String> clients = new List<String>();
        Map<String,List<Shipment_Order__c> > destinations = new Map<String,List<Shipment_Order__c> >();
        Map<String,List<Shipment_Order__c> > soWithoutPriceList = new Map<String,List<Shipment_Order__c> >();

        Map<String,CountryandRegionMap__c> countryandRegionMap = CountryandRegionMap__c.getAll();
        Map<String,RegionalFreightValues__c> regionalFreightValueMap = RegionalFreightValues__c.getAll();

        for (Shipment_Order__c shipment : Trigger.new) {
            shipment.Client_Quote_Creator__c= shipment.Client_Contact_for_this_Shipment__c;
            clients.add(shipment.Account__c);
            if(destinations.containsKey(shipment.Destination__c))
                destinations.get(shipment.Destination__c).add(shipment);
            else
                destinations.put(shipment.Destination__c,new List<Shipment_Order__c> {shipment});

            if(soWithoutPriceList.containsKey(shipment.Account__c+shipment.Destination__c))
                soWithoutPriceList.get(shipment.Account__c+shipment.Destination__c).add(shipment);
            else
                soWithoutPriceList.put(shipment.Account__c+shipment.Destination__c,new List<Shipment_order__c> {shipment});

            shipment.Destination_part_of_EU__c = CountryandRegionMap__c.getInstance(shipment.Destination__c) ?.European_Union__c == 'European Union';
            shipment.Ship_From_Part_of_EU__c = CountryandRegionMap__c.getInstance(shipment.Ship_From_Country__c) ?.European_Union__c == 'European Union';
        }

        //Adding Genereic client Id in list to save soql
        Account TecExProspectiveClient = [SELECT Id FROM Account where Name = 'TecEx Prospective Client' LIMIT 1];
        clients.add(TecExProspectiveClient.Id);
        Map<Id,Account> accounts = new Map<Id,Account>([SELECT Id,Client_Type_NL_Product__c,X61_90_Days__c,Freight_Preference_to_Staging_Warehouse__c,Cash_outlay_fee_base__c,Finance_Charge__c,Over_90_Days__c,Balance_outstanding__c,Tax_recovery_client__c,Name,Invoice_Timing__c,Freight_Referrer__c,Client_type__c,CSE_IOR__r.ManagerId,New_Invoicing_Structure__c,Liability_Product_Agreement__c,Insurance_Fee__c,Annual_Liability_Cover_Fee_Formula__c,Staging_Warehouse_Preferred_Method__c FROM Account WHERE Id IN: clients]);
        //Price List population logic
        Map<Id,IOR_Price_List__c> priceLists = new Map<Id,IOR_Price_List__c>([SELECT Id,Name,EOR_Brokerage__c,EOR_Handling__c,EOR_Licenses_permits__c,EOR_Clearance__c,Miscellaneous_Fee_Name__c,Agreed_Pricing__c,Tax_recovery_Premium_Rate__c,Tax_recovery_Premium_Lane__c,Admin_When_Tecex_Does_Freight__c, Zee_Admin_Fee_Shipping_Only__c,Zee_Fixed_IOR_fee__c,EOR_Fee__c,Admin_Above_10000__c,EOR_Bank_Above_10000__c,EOR_Admin_Fee__c,EOR_Admin_Above_10000__c,EOR_Min_USD__c,EOR_Bank_Fees__c,Estimate_Customs_Clearance_Time__c,Estimate_Transit_time__c,Estimated_Customs_Brokerage__c,Estimated_Customs_Clearance__c,Estimated_Customs_Handling__c,Estimated_Import_License__c,IOR_Fee__c,IOR_Fee_as_of__c,Tax_Rate__c,Admin_Fee__c,Bank_Fees__c,TecEx_Shipping_Fee_Markup__c,Bank_Above_10000__c,On_Charge_Mark_up__c,IOR_Min_Fee__c,Set_up_fee__c,Client_Name__c,Destination__c,Miscellaneous_Fee__c FROM IOR_Price_List__c WHERE Active__c = TRUE AND Client_Name__c IN: clients AND Destination__c IN: destinations.keySet() order by Name]);
        Map<String,Id> genericPriceLists = new Map<String,Id>();
        for(Id priceListId : priceLists.keySet()) {
            IOR_Price_List__c priceList = priceLists.get(priceListId);
            if(soWithoutPriceList.containsKey(priceList.Client_Name__c+priceList.Destination__c)) {
                for(Shipment_Order__c shipmentOrder : soWithoutPriceList.get(priceList.Client_Name__c+priceList.Destination__c)) {
                    Account acc = accounts.get(shipmentOrder.Account__c);
                    shipmentOrder.IOR_Price_List__c = priceList.Id;
                }
                soWithoutPriceList.remove(priceList.Client_Name__c+priceList.Destination__c);
            }
            if(priceList.Client_Name__c == TecExProspectiveClient.Id) {
                genericPriceLists.put(priceList.Destination__c,priceList.Id);
            }
        }
        if(!soWithoutPriceList.isEmpty()) {
            Map<String,IOR_Price_List__c> newPriceLists = new Map<String,IOR_Price_List__c>();
            for(String key : soWithoutPriceList.keySet()) {
                Shipment_Order__c shipmentOrder = soWithoutPriceList.get(key)[0];
                if(genericPriceLists.containsKey(shipmentOrder.Destination__c)) {
                    IOR_Price_List__c priceList = priceLists.get(genericPriceLists.get(shipmentOrder.Destination__c));
                    newPriceLists.put(
                        key,
                        new IOR_Price_List__c(
                            Admin_Fee__c = priceList.Admin_Fee__c,
                            Bank_Fees__c = priceList.Bank_Fees__c,
                            Client_Name__c = shipmentOrder.Account__c,
                            Destination__c = shipmentOrder.Destination__c,
                            Estimate_Customs_Clearance_Time__c = priceList.Estimate_Customs_Clearance_Time__c,
                            Estimated_Customs_Brokerage__c = priceList.Estimated_Customs_Brokerage__c,
                            Estimated_Customs_Clearance__c = priceList.Estimated_Customs_Clearance__c,
                            Estimated_Customs_Handling__c = priceList.Estimated_Customs_Handling__c,
                            Estimated_Import_License__c = priceList.Estimated_Import_License__c,
                            IOR_Fee__c = priceList.IOR_Fee__c,
                            IOR_Fee_as_of__c = priceList.IOR_Fee_as_of__c,
                            IOR_Min_Fee__c = priceList.IOR_Min_Fee__c,
                            Name = priceList.Name,
                            On_Charge_Mark_up__c = priceList.On_Charge_Mark_up__c,
                            Set_up_fee__c = priceList.Set_up_fee__c,
                            Tax_Rate__c = priceList.Tax_Rate__c,
                            Tax_recovery_Premium_Lane__c = priceList.Tax_recovery_Premium_Lane__c,
                            Admin_When_Tecex_Does_Freight__c = priceList.Admin_When_Tecex_Does_Freight__c,
                            Admin_Above_10000__c = priceList.Admin_Above_10000__c,
                            EOR_Admin_Fee__c = priceList.EOR_Admin_Fee__c,
                            EOR_Fee__c = priceList.EOR_Fee__c,
                            EOR_Admin_Above_10000__c = priceList.EOR_Admin_Above_10000__c,
                            EOR_Min_USD__c = priceList.EOR_Min_USD__c,
                            EOR_Bank_Fees__c = priceList.EOR_Bank_Fees__c,
                            Estimate_Transit_time__c = priceList.Estimate_Transit_time__c,
                            Bank_Above_10000__c = priceList.Bank_Above_10000__c,
                            Tax_recovery_Premium_Rate__c = priceList.Tax_recovery_Premium_Rate__c,
                            TecEx_Shipping_Fee_Markup__c = priceList.TecEx_Shipping_Fee_Markup__c)
                        );
                }
            }
            if(!newPriceLists.isEmpty()) {
                insert newPriceLists.values(); //DML
                for(String key: newPriceLists.keySet()) {
                    IOR_Price_List__c priceList = newPriceLists.get(key);
                    for(Shipment_Order__c shipmentOrder : soWithoutPriceList.get(key)) {
                        Account acc = accounts.get(shipmentOrder.Account__c);
                        shipmentOrder.IOR_Price_List__c = priceList.Id;
                        priceLists.put(priceList.Id,priceList);
                    }
                }
            }
        }

        List<LiabilityChargeValueAdjustment__c> liabilityRecords = [SELECT Id, Name, Floor_Value__c, CeilingValue__c, Value_Adjustment_Factor__c FROM LiabilityChargeValueAdjustment__c];
        //Update Libility cover fees
        for(Shipment_Order__c shipment : Trigger.new) {
            Account acc = accounts.get(shipment.Account__c);

            if(acc.Client_type__c  == 'Freight forwarder') shipment.Who_arranges_International_courier__c = 'Client';
            IOR_Price_List__c priceList = priceLists.get(shipment.IOR_Price_List__c);
            shipment.Intention_with_Goods__c = acc.Client_Type_NL_Product__c == 'Reseller' ? 'To Sell Goods' : acc.Client_Type_NL_Product__c == 'Beneficial Owner' ? 'Retain Ownership of Goods' : null;

            if(!shipment.BO_NBO_Shipment_Override__c && shipment.Intention_with_Goods__c == 'To Sell Goods' && shipment.Buyer_or_BO_Part_of_EU__c && shipment.Status_of_LOA__c == 'Received') {
                shipment.Contact_number__c = 'Non-Beneficial Owner Shipment';
            }else if(!shipment.BO_NBO_Shipment_Override__c && shipment.Status_of_LOA__c == 'Received' && ((shipment.Intention_with_Goods__c == 'To Sell Goods' && !shipment.Buyer_or_BO_Part_of_EU__c) || (shipment.Intention_with_Goods__c == 'Retain Ownership of Goods'))) {
                shipment.Contact_number__c = 'Beneficial Owner Shipment';
            }
            shipment.Goods_to_be_sent_to_Staging_Warehouse__c = acc.Staging_Warehouse_Preferred_Method__c;
            shipment.Client_Type__c = acc.Client_type__c;
            shipment.Freight_Referrer__c = acc.Freight_Referrer__c;
            if(!isSORecordTypeIsZee(shipment.recordTypeId)) {
                shipment.Invoice_Timing__c = acc.Invoice_Timing__c;
            }

            shipment.SO_Cash_Outlay_Fee_Base__c  = acc.Cash_outlay_fee_base__c;
            shipment.Finance_Fee_Percentage__c   = acc.Finance_Charge__c;
            shipment.Freight_Staging_Warehouse_Responsibility__c = acc.Freight_Preference_to_Staging_Warehouse__c;
            //shipment.Lead_AM__c = acc.CSE_IOR__r == null || acc.CSE_IOR__r.ManagerId == null ? null : acc.CSE_IOR__r.ManagerId;
            Map<String, Countries_Not_Liability_Cover__c> ListCountriesNotHasLiabilityCover = Countries_Not_Liability_Cover__c.getAll();
            if(shipment.Shipping_Status__c != 'Roll-Out' && !ListCountriesNotHasLiabilityCover.containsKey(shipment.Destination__c)) {
                if(acc.New_Invoicing_Structure__c &&  acc.Liability_Product_Agreement__c == 'Annual Rate Agreement') {
                    shipment.Client_Taking_Liability_Cover__c = 'Yes';
                    shipment.Insurance_Cost_CIF__c = 0.125;
                    shipment.Insurance_Fee__c = acc.Insurance_Fee__c == null ? acc.Annual_Liability_Cover_Fee_Formula__c *10000 : acc.Insurance_Fee__c;
                    //shipment.Liability_Cover_Fee_USD__c = shipment.Insurance_Fee_USD__c;
                    //START: formula calculation of Insurance_Fee_USD__c
                    if(!shipment.Liability_Cover_Fee_Added__c)
                        shipment.Liability_Cover_Fee_USD__c = Math.max(shipment.Shipment_Value_USD__c * shipment.Insurance_Fee__c/100,50);
                    else
                        shipment.Liability_Cover_Fee_USD__c = shipment.Shipment_Value_USD__c * shipment.Insurance_Fee__c/100;
                    //END: formula calculation of Insurance_Fee_USD__c
                }
                else if(acc.New_Invoicing_Structure__c && acc.Liability_Product_Agreement__c == 'Ad Hoc Agreement') {
                    Decimal riskAdjFactor = 1;
                    Decimal valueAdjustmentFactor = 1;
                    String toRegion = countryandRegionMap.get(shipment.Destination__c) ?.Region__c;
                    String fromRegion = countryandRegionMap.get(shipment.Ship_From_Country__c) ?.Region__c;

                    for(LiabilityChargeValueAdjustment__c liabilityRecord : liabilityRecords) {

                        // if(liabilityRecord.Floor_Value__c >= shipment.Shipment_Value_USD__c && liabilityRecord.CeilingValue__c <= shipment.Shipment_Value_USD__c) {
                        if(liabilityRecord.Floor_Value__c <= shipment.Shipment_Value_USD__c && liabilityRecord.CeilingValue__c >= shipment.Shipment_Value_USD__c) {
                            valueAdjustmentFactor = liabilityRecord.Value_Adjustment_Factor__c;
                            break;
                        }
                    }
                    if(String.isNotBlank(fromRegion) && String.isNotBlank(fromRegion) && regionalFreightValueMap.containsKey(fromRegion +' to '+toRegion)) {
                        riskAdjFactor = regionalFreightValueMap.get(fromRegion +' to '+toRegion).Risk_Adjustment_Factor__c;
                    }
                    Decimal freightPremium = shipment.Who_arranges_International_courier__c == 'TecEx' ? 0.9 : 1;
                    Decimal effectiveInsuranceRate =  0.250 * valueAdjustmentFactor * freightPremium * riskAdjFactor;
                    shipment.Insurance_Cost_CIF__c = 0.125;


                    shipment.Insurance_Fee__c = effectiveInsuranceRate;
                }
                else if(acc.Liability_Product_Agreement__c == 'Liability Cover Declined') {
                    shipment.Client_Taking_Liability_Cover__c = 'No';
                    shipment.Insurance_Cost_CIF__c = 0;
                    shipment.Insurance_Fee__c = 0;
                }
                //update so fields from priceList
                updateFieldsFromPriceList(shipment,priceList);
            }
            else{
                shipment.Client_Taking_Liability_Cover__c = 'No';
            }

            if(shipment.Shipping_Status__c != 'Roll-Out') {
                updateFieldsFromPriceList(shipment,priceList);
            }


            //Tax Recovery WF
            if(acc.Tax_recovery_client__c && priceList.Tax_recovery_Premium_Lane__c) {
                shipment.Tax_Recovery__c = true;
                shipment.Tax_recovery_Premium_Rate__c = priceList.Tax_recovery_Premium_Rate__c;
            }
            //Total - License Cost WF
            shipment.Total_License_Cost__c = shipment.Recharge_License_Cost__c;
            //Total Customs Brokerage Cost WF
            shipment.Total_Customs_Brokerage_Cost1__c = shipment.Total_Customs_Brokerage_Cost__c;
            //Add Debtors Info WF
            shipment.Balance_outstanding__c = acc.Balance_outstanding__c;
            shipment.Over_60_days__c = acc.X61_90_Days__c + acc.Over_90_Days__c;
            //Auto Populate Address WF
            if(shipment.Account__c == '0010Y00000PEnvn' || Test.isRunningTest()) {
                shipment.Pick_Up_Address__c = 'Orange Business Services U.S. Inc., C/O MDSi,6225 Shiloh Road,Alpharetta, GA 30005';
                shipment.Pick_Up_Contact__c = 'Jasmine Lowe';
                shipment.Pick_Up_contact_Number_new__c = '6783416206'; // there is another field with same label.
                shipment.Ship_From_Country__c = 'United States';
            }
            //Date Loss making SO WF
            if(shipment.Loss_Making_Shipments__c) {
                shipment.Loss_Making_SO_checked_Date__c = System.now();
                shipment.Loss_Making_SO_Checked_by__c = UserInfo.getFirstName() + ' ' + UserInfo.getLastName();
            }
            //Date of escalation WF
            if(shipment.Pressure_Point__c) {
                shipment.Date_of_escalation__c = System.now();
            }
            //liability Cover WF
            if(shipment.Insurance_Fee__c == 0) {
                shipment.Insurance_Cost_CIF__c = 0;
            }
            //Vishal: For updating Domestic_Movement__c
            if(shipment.Ship_From_Country__c != null && shipment.Destination__c != null) {
                if((shipment.Ship_From_Country__c == shipment.Destination__c)
                   || (countryandRegionMap.get(shipment.Ship_From_Country__c) ?.European_Union__c == 'European Union'
                       && countryandRegionMap.get(shipment.Destination__c) ?.European_Union__c == 'European Union')) {
                    shipment.Domestic_Movement__c = true;
                }
                else{
                    shipment.Domestic_Movement__c = false;
                }
            }
            //End
        }
        //START: code from old attachcostestimate
        FindCPA2_V2.searchCPA2(destinations,accounts);


        Set<Id> cpaIds = new Set<Id>();
        Map<Id,CPA_v2_0__c> CPAToUpdate = new Map<Id,CPA_v2_0__c>();

        for(String dest : destinations.keySet()) {
            for(Shipment_Order__c so : destinations.get(dest)) {
                if(so.CPA_v2_0__c != null)
                    cpaIds.add(so.CPA_v2_0__c);
            }
        }

        Map<Id,CPA_v2_0__c> cpas = new Map<Id,CPA_v2_0__c>([SELECT Hub_Country__c,Destination__c, Lead_ICE__c, CPA_Minimum_Brokerage_Costs__c, CPA_Minimum_Brokerage_Surcharge__c, CPA_Minimum_Clearance_Costs__c, CPA_Minimum_Handling_Costs__c, CPA_Minimum_License_Permit_Costs__c, Shipping_Notes__c,Billing_Term__c, Supplier__c, CIF_Freight_and_Insurance__c,Final_Destination__c FROM CPA_v2_0__c WHERE Id IN: cpaIds ]);

        for(String dest : destinations.keySet()) {
            for(Shipment_Order__c so : destinations.get(dest)) {
                if(so.CPA_v2_0__c != null) {
                    fieldsUpdateFromCPA(so,cpas.get(so.CPA_v2_0__c));
                    CPAToUpdate.put(So.CPA_v2_0__c, new CPA_v2_0__c(Id = So.CPA_v2_0__c, Last_Shipment_Date__c = system.today()));
                }
            }
        }
        if(!CPAToUpdate.isEmpty()) {
            updateCPALastDate(CPAToUpdate,Trigger.new);
        }
        //END: code from old attachcostestimate
    }
    if(Trigger.isAfter && Trigger.isInsert) {
        List<String> vatPossibleCountries = new List<String> {'Bulgaria','Croatia','Cyprus','Czech Republic','Estonia','Greece','Hungary','Italy','Latvia','Lithuania','Poland','Portugal','Romania','Slovakia','Slovenia','Spain','via'};
        List<String> vatClaimCountries = new List<String> {'Australia','Austria','Belgium','Denmark','Finland','France','Germany','Iceland','Ireland','Luxembourg','Malta','Monaco','Netherlands','New Zealand','Norway','Spain','Sweden','Switzerland','United Kingdom'};
        Map<String,String> users = new Map<String,String>();
        for(User us : [SELECT Id,Name FROM User where Name IN ('Vat Berkeley','Vat USA','Vat Growth')]) {
            users.put(us.Name,us.Id);
        }

        Map<Id,Set<Id> > ShipmentWithAccounts = new Map<Id,Set<Id> >();

        for(Shipment_Order__c shipment : [SELECT Id,IOR_Price_List__r.Name,Client_Contact_for_this_Shipment__c,VAT_Claiming_Country__c,Account__r.Name FROM Shipment_order__c WHERE Id IN:Trigger.newMap.keySet()]) {
            //VAT Team Possible VAT Recovery WF
            //VAT Team VAT Claiming Country WF
            if(vatPossibleCountries.contains(shipment.IOR_Price_List__r.Name) && !shipment.VAT_Claiming_Country__c && shipment.Account__r.Name != 'Internal Testing Account') {
                if(users.containsKey('Vat Berkeley')) sendMail('VAT Team Possible VAT Recovery', new List<String> {users.get('Vat Berkeley')}, shipment);
                if(vatClaimCountries.contains(shipment.IOR_Price_List__r.Name) && !users.isEmpty()) {
                    sendMail('VAT Team VAT Claiming Country', users.values(), shipment);
                }
            }
            Set<Id> ShipIds = new Set<Id>();

            if(ShipmentWithAccounts.containsKey(shipment.Account__c)) {
                ShipIds = ShipmentWithAccounts.get(shipment.Account__c);
            }

            ShipIds.add(shipment.Id);

            ShipmentWithAccounts.put(shipment.Account__c,ShipIds);

        }
        if(ShipmentWithAccounts !=null && !ShipmentWithAccounts.isEmpty()) {
            ShipmentOrderTriggerHandler.createNotifyParties(ShipmentWithAccounts);
        }


    }
    if(Trigger.isBefore && Trigger.isUpdate) {

        system.debug('From ZEE api before update ---> '+trigger.new[0].shipping_Status__c);
        Map<String,CountryofOriginMap__c> CountryofOriginMap = CountryofOriginMap__c.getAll(); //added to get AllcountryCode for Tracking field
        List<String> clients = new List<String>();
        Map<String,List<Shipment_Order__c> > destinations = new Map<String,List<Shipment_Order__c> >();
        List<id> clientcontacts = new List<id>();
        List<Freight__c> frToInsert = new List<Freight__c>();
        for (Shipment_Order__c shipmentOrder : Trigger.new) {
            if(shipmentOrder.Number_of_Final_Deliveries_Client__c == null) shipmentOrder.Number_of_Final_Deliveries_Client__c =0;
            if(shipmentOrder.Number_of_Final_Deliveries_Auto__c == null) shipmentOrder.Number_of_Final_Deliveries_Auto__c =0;
            shipmentOrder.Final_Deliveries_New__c = shipmentOrder.Number_of_Final_Deliveries_Auto__c > shipmentOrder.Number_of_Final_Deliveries_Client__c ? shipmentOrder.Number_of_Final_Deliveries_Auto__c : shipmentOrder.Number_of_Final_Deliveries_Client__c;
            if((shipmentOrder.shipping_status__C =='POD Received' || shipmentOrder.shipping_status__C =='Customs Clearance Docs Received') && shipmentOrder.Source__c =='Client Platform') {clientcontacts.add(shipmentOrder.Client_Contact_for_this_Shipment__c); }

            if(CountryandRegionMap__c.getValues(shipmentOrder.Ship_From_Country__c) != null && CountryandRegionMap__c.getValues(shipmentOrder.Ship_From_Country__c).European_Union__c == 'European Union') {
                shipmentOrder.Ship_From_Part_of_EU__c = true;
            }
            //If Mapped Status changes we need to reset the TP ETA flags
            if(isChanged(shipmentOrder, 'Mapped_Shipping_status__c') ) {

                ETACalculation.populateTotalHaltFields(shipmentOrder, Trigger.oldMap.get(shipmentOrder.Id).Mapped_Shipping_status__c);

                shipmentOrder.TP_Halt__c = false;
                shipmentOrder.TP_Halt_Start_Date__c = null;
                shipmentOrder.Total_TP_Halt_Days__c = 0;
                shipmentOrder.Total_TP_Halt_Business_Days__c = 0;
            }

            if(isChanged(shipmentOrder, 'Mapped_Shipping_status__c')
               && shipmentOrder.Customs_Cleared_Date__c == null
               && shipmentOrder.Mapped_Shipping_status__c == 'Final Delivery in Transit') {
                shipmentOrder.Customs_Cleared_Date__c = system.today();
            }

            // commented below line story 17308
            //if(shipmentOrder.Client_Type__c == 'Freight forwarder') shipmentOrder.Who_arranges_International_courier__c = 'Client';

            CountryofOriginMap__c twodigit= CountryofOriginMap.get(shipmentOrder.Destination__c); //added to get two digit countryCode for Tracking field
            if(twodigit != null) {
                shipmentOrder.Portal_Tracking_Number__c= shipmentOrder.Name.substring(shipmentOrder.name.indexOf('-')+1,shipmentOrder.Name.length())+twodigit.Two_Digit_Country_Code__c;//added to get Tracking number
            }else{
                shipmentOrder.Portal_Tracking_Number__c ='2digNA';
            }
            /* if(!system.isBatch() && Trigger.oldMap.get(shipmentOrder.Id).Shipping_Status__c == 'Roll-Out'
               && shipmentOrder.Shipping_Status__c == 'Cost Estimate'
               && shipmentOrder.Freight_Fee_Calculated_Freight_Request__c == 0){
               Freight__c fr = new Freight__c();
               fr.Chargeable_weight_in_KGs_packages__c = shipmentOrder.Chargeable_Weight__c;
               fr.Li_Ion_Batteries__c = shipmentOrder.Li_ion_Batteries__c;
               fr.Ship_From_Country__c = shipmentOrder.Ship_From_Country__c;
               fr.Ship_To__c = shipmentOrder.Hub_Country__c == null ? shipmentOrder.Destination__c : shipmentOrder.Hub_Country__c;
               fr.Shipment_Order__c = shipmentOrder.Id;
               frToInsert.add(fr);
               } */
            /*if(shipmentOrder.Shipping_Status__c == 'Shipment Pending' &&
                Trigger.oldMap.get(shipmentOrder.Id).Shipping_Status__c == 'Cost Estimate'
               ){
                   ShipmentOrderTriggerHandler.generateChatterFeed(shipmentOrder);
               }*/
            /*if(isChanged(shipmentOrder, 'CPA_v2_0__c') &&
               String.isNotEmpty(shipmentOrder.CPA2_Override_reason__c)){
                shipmentOrder.CPA2_Override__c = true;

               }
               if(isChanged(shipmentOrder, 'CPA_v2_0__c') &&
               String.isEmpty(shipmentOrder.CPA2_Override_reason__c)){
                   shipmentOrder.addError('Please enter the reason for overriding the CPA in the field CPA2 Override reason.');
               }*/
            if(
                !shipmentOrder.CPA2_Override__c
                &&
                (
                    (shipmentOrder.Shipping_Status__c == 'Shipment Pending' &&
                     Trigger.oldMap.get(shipmentOrder.Id).Shipping_Status__c == 'Cost Estimate'
                    )
                    ||
                    (
                        (shipmentOrder.CPA_v2_0__c != null &&
                         shipmentOrder.Shipping_Status__c != 'Cost Estimate Abandoned')
                        &&
                        (
                            isChanged(shipmentOrder, 'ECCN_No__c') ||
                            isChanged(shipmentOrder, 'Manufacturer__c')  ||
                            isChanged(shipmentOrder, 'Sum_of_quantity_of_matched_products__c')  ||
                            isChanged(shipmentOrder, 'Chargeable_Weight__c')  ||
                            isChanged(shipmentOrder, 'Final_Deliveries_New__c')  ||
                            isChanged(shipmentOrder, 'Shipment_Value_USD__c')  ||
                            isChanged(shipmentOrder, 'NL_Product_User__c')  ||
                            isChanged(shipmentOrder, 'Who_arranges_International_courier__c')  ||
                            isChanged(shipmentOrder, 'Li_ion_Batteries__c')  ||
                            isChanged(shipmentOrder, 'Package_Height__c')  ||
                            isChanged(shipmentOrder, 'Service_Type__c')  ||
                            isChanged(shipmentOrder, 'of_Line_Items__c')  ||
                            isChanged(shipmentOrder, 'Contains_Batteries_Auto__c') ||
                            isChanged(shipmentOrder, 'Contains_Batteries_Client__c')  ||
                            isChanged(shipmentOrder, 'Contains_Encrypted_Goods_Auto__c') ||
                            isChanged(shipmentOrder, 'Contains_Encrypted_Goods_Client__c') ||
                            isChanged(shipmentOrder, 'Contains_Wireless_Goods_Auto__c') ||
                            isChanged(shipmentOrder, 'Contains_Wireless_Goods_Client__c') ||
                            isChanged(shipmentOrder, 'Prohibited_Manufacturer_Auto__c') ||
                            isChanged(shipmentOrder, 'Prohibited_Manufacturer_Client__c') ||
                            isChanged(shipmentOrder, 'Type_of_Goods__c') ||
                            isChanged(shipmentOrder, 'Account__c') ||
                            isChanged(shipmentOrder, 'Destination__c') ||
                            isChanged(shipmentOrder, 'Ship_From_Country__c') ||
                            isChanged(shipmentOrder, 'Client_Type__c')
                        )
                        &&
                        (shipmentOrder.Shipping_Status__c == 'Cost Estimate' ||
                         shipmentOrder.Shipping_Status__c == 'Shipment Pending' ||
                         shipmentOrder.Shipping_Status__c == 'CI, PL and DS Received' ||
                         shipmentOrder.Shipping_Status__c == 'AM Approved CI,PL and DS\'s'
                        )
                    )
                    ||
                    (isChanged(shipmentOrder, 'Preferred_Freight_method__c') && isSORecordTypeIsZee(shipmentOrder.RecordTypeId))
                )
                ) {
                //shipmentOrder.CPA2_Override__c = true;
                clients.add(shipmentOrder.Account__c);
                if(destinations.containsKey(shipmentOrder.Destination__c))
                    destinations.get(shipmentOrder.Destination__c).add(shipmentOrder);
                else
                    destinations.put(shipmentOrder.Destination__c,new List<Shipment_Order__c> {shipmentOrder});
            }
            if(shipmentOrder.Special_Client_Invoicing__c) {shipmentOrder.Forecast_Override__c = true;}
            System.debug(shipmentOrder.CPA_v2_0__c);

        }

        if(!clients.isEmpty() && !destinations.isEmpty()) {
            Map<Id,Account> accounts = new Map<Id,Account>([SELECT Id,X61_90_Days__c,Over_90_Days__c,Balance_outstanding__c,Tax_recovery_client__c,Name,Invoice_Timing__c,Freight_Referrer__c,Client_type__c,CSE_IOR__r.ManagerId,New_Invoicing_Structure__c,Liability_Product_Agreement__c,Insurance_Fee__c,Annual_Liability_Cover_Fee_Formula__c,Staging_Warehouse_Preferred_Method__c FROM Account WHERE Id IN: clients]);
            FindCPA2_V2.searchCPA2(destinations,accounts);
            System.debug('Destinations'+JSON.serialize(destinations));
        }

        if(!clientcontacts.IsEmpty()) {
            List<Contact> conlist = [select Id,Buyapowa_Criteria_Met__c,LeadSource from Contact where Buyapowa_Criteria_Met__c=FALSE and LeadSource='Buyapowa Referral Program' and id in:clientcontacts ];
            for(contact co:conlist) {
                co.Buyapowa_Criteria_Met__c= TRUE;
            }
            if(!conlist.IsEmpty()) {update conlist;}
        }

        System.debug(Trigger.new[0].CPA_v2_0__c);


    }
    //Shipment Update process
    if(Trigger.isAfter && Trigger.isUpdate) {
        system.debug('From ZEE api after update ---> '+trigger.new[0].shipping_Status__c);
        System.debug('After update Run');
        Set<Id> ids = new Set<Id>(Trigger.newMap.keySet());
        Map<Id,Shipment_Order__c> soIdsToCalculateCost = new Map<Id,Shipment_Order__c>();
        Map<Id,Shipment_Order__c> soNewList = new Map<Id,Shipment_Order__c>();
        Set<Id> cpaIds = new Set<Id>();
        Map<Id,Shipment_Order__c> soLogMap = new Map<Id,Shipment_Order__c>();
        List<Invoice_New__c> createFirstInvoices = new List<Invoice_New__c>();
        Map<String,CountryandRegionMap__c> countryandRegionMap = CountryandRegionMap__c.getAll();
        Map<String,RegionalFreightValues__c> regionalFreightValueMap = RegionalFreightValues__c.getAll();
        List<LiabilityChargeValueAdjustment__c> liabilityRecords = [SELECT Id, Name, Floor_Value__c, CeilingValue__c, Value_Adjustment_Factor__c FROM LiabilityChargeValueAdjustment__c];
        List<String> specialDestinations = new List<String> {'Angola','Rwanda','Democratic Republic of Congo','Croatia','Bosnia and Herzegovina','Macedonia','Slovenia','Kosovo','Montenegro','Central African Republic','Haiti','Serbia','False'};
        Map<Id, List<Invoice_New__c> > invoiceMap = new Map<Id, List<Invoice_New__c> >();
        List<Invoice_New__c> updatePOD = new List<Invoice_New__c>();
        Map<Id,Shipment_Order__c> soIdsToCalculateTax = new Map<Id,Shipment_Order__c>();
        Set<Id> soIdsCPAChanged = new Set<Id>();
        Set<Id> soIdsDestinationChanged = new Set<Id>();
        Set<Id> autoReviewCCDIds = new Set<Id>();

        for(Invoice_New__c inv : [SELECT Id, Name, RecordTypeId, POD_Date_New__c, Shipment_Order__c, Penalty_Daily_Percentage__c, Penalty_Status__c, Account__c FROM Invoice_New__c Where Shipment_Order__c IN : ids]) {
            if(invoiceMap.containsKey(inv.Shipment_Order__c))
                invoiceMap.get(inv.Shipment_Order__c).add(inv);
            else
                invoiceMap.put(inv.Shipment_Order__c,new List<Invoice_New__c> {inv});
        }
        Set<Id> soToUpdateFreight = new Set<Id>();
        Map<Id,CPA_v2_0__c> CPAToUpdate = new Map<Id,CPA_v2_0__c>();
        List<Shipment_Order__c> soToCalculateETA = new List<Shipment_Order__c>();
        List<String> vatPossibleCountries = new List<String> {'Bulgaria','Croatia','Cyprus','Czech Republic','Estonia','Greece','Hungary','Italy','Latvia','Lithuania','Poland','Portugal','Romania','Slovakia','Slovenia','Spain','via'};
        List<String> vatClaimCountries = new List<String> {'Australia','Austria','Belgium','Denmark','Finland','France','Germany','Iceland','Ireland','Luxembourg','Malta','Monaco','Netherlands','New Zealand','Norway','Spain','Sweden','Switzerland','United Kingdom'};
        Map<String,String> users = new Map<String,String>();
        for(User us : [SELECT Id,Name FROM User where Name IN ('Vat Berkeley','Vat USA','Vat Growth')]) {
            users.put(us.Name,us.Id);
        }
        Map<String, Decimal> conversionRate = new Map<String, Decimal>();
        for (Currency_Management2__c curr: [SELECT Id, Name, Conversion_Rate__c, Currency__c, ISO_Code__c FROM Currency_Management2__c]) {
            conversionRate.put(curr.Name,curr.Conversion_Rate__c );
        }
        String parentFields = 'CPA_v2_0__r.DDP_Client_Auto_Status_Update__c,Account__r.DDP_Client_Auto_Status_Update__c,Account__r.Pre_Inspection_Responsibility_Preference__c,Account__r.Staging_Warehouse_Preferred_Method__c,Account__r.Client_type__c,IOR_Price_List__r.EOR_Brokerage__c,IOR_Price_List__r.EOR_Handling__c,IOR_Price_List__r.EOR_Licenses_permits__c,IOR_Price_List__r.EOR_Clearance__c,IOR_Price_List__r.Miscellaneous_Fee_Name__c,IOR_Price_List__r.Miscellaneous_Fee__c,IOR_Price_List__r.Estimated_Customs_Brokerage__c,IOR_Price_List__r.Estimated_Customs_Handling__c,IOR_Price_List__r.Estimated_Import_License__c,IOR_Price_List__r.Estimated_Customs_Clearance__c,IOR_Price_List__r.Zee_Fixed_IOR_fee__c,CPA_v2_0__r.Name,CPA_v2_0__r.Freight_Only_Lane__c ,CPA_v2_0__r.CPA_Minimum_Brokerage_Surcharge__c,Account__r.Amount_Outstanding_61_90_Days__c,Account__r.Amount_Outstanding_Over_90_Days__c,Account__r.X61_90_Days__c,Account__r.Over_90_Days__c,Account__r.Balance_outstanding__c,Account__r.Name,Account__r.Tax_recovery_client__c,Account__r.Penalty_Status__c,Account__r.Penalty_Daily_Percentage__c,Account__r.Invoice_Timing__c,Account__r.Default_invoicing_currency__c,Account__r.Invoicing_Term_Parameters__c,Account__r.Freight_Referrer__c,Account__r.Insurance_Fee__c,Account__r.Liability_Product_Agreement__c,Account__r.New_Invoicing_Structure__c,Account__r.Annual_Liability_Cover_Fee_Formula__c,Account__r.NCP_Access__c,CPA_v2_0__r.Final_Destination__c,CPA_v2_0__r.Country__r.Foreign_Currency_Adjustment_Factor__c,CPA_v2_0__r.Country__r.Effective_tax_rate_excl_Duties__c,CPA_v2_0__r.Country__r.Total_fixed_taxes__c,CPA_v2_0__r.Country__r.Default_Duty_Rollup__c,CPA_v2_0__r.Country__r.Max_Duty_Rate__c,IOR_Price_List__r.Name,IOR_Price_List__r.Fixed_Min_Tax_Buffer__c, IOR_Price_List__r.Agreed_Pricing__c,IOR_Price_List__r.Fixed_Max_Tax_Buffer__c,IOR_Price_List__r.Tax_Buffer__c,CPA_v2_0__r.Destination__c,IOR_Price_List__r.EOR_Bank_Fees__c,IOR_Price_List__r.EOR_Bank_Above_10000__c,IOR_Price_List__r.Bank_Fees__c,IOR_Price_List__r.Tax_recovery_Premium_Rate__c,IOR_Price_List__r.Tax_recovery_Premium_Lane__c,IOR_Price_List__r.Bank_Above_10000__c,IOR_Price_List__r.Admin_When_Tecex_does_freight__c,IOR_Price_List__r.Admin_Above_10000__c,IOR_Price_List__r.Admin_Fee__c,IOR_Price_List__r.EOR_Admin_Above_10000__c,IOR_Price_List__r.EOR_Admin_Fee__c,IOR_Price_List__r.Zee_Admin_Fee_Shipping_Only__c,IOR_Price_List__r.EOR_Fee__c,IOR_Price_List__r.EOR_Min_USD__c,IOR_Price_List__r.IOR_Fee__c,IOR_Price_List__r.IOR_Min_Fee__c,IOR_Price_List__r.On_Charge_Mark_up__c,IOR_Price_List__r.Tax_Rate__c,IOR_Price_List__r.TecEx_Shipping_Fee_Markup__c,CPA_v2_0__r.CIF_Freight_and_Insurance__c,CPA_v2_0__r.Lead_ICE__c,CPA_v2_0__r.CPA_Minimum_Brokerage_Costs__c,CPA_v2_0__r.CPA_Minimum_Clearance_Costs__c,CPA_v2_0__r.Hub_Country__c,CPA_v2_0__r.Billing_Term__c,CPA_v2_0__r.CPA_Minimum_Handling_Costs__c,CPA_v2_0__r.CPA_Minimum_License_Permit_Costs__c,CPA_v2_0__r.Shipping_Notes__c,CPA_v2_0__r.Supplier__c,Halt__c, Halt_Start_Date__c, Total_Halt_Days__c, Expected_Date_to_Next_Status__c,In_Transit_Date__c,cost_Estimate_acceptance_date__c,Shipment_Order_Compliance_Approval_Time__c, Date_Of_New_CP__c,Shipping_Status__c,Mapped_Shipping_status__c,Hub_Lead_Time__c,Transit_to_Destination_Lead_Time__c, Customs_Clearance_Lead_Time__c,Final_Delivery_Lead_Time__c,CPA_v2_0__r.Comments_On_Why_No_Final_Quote_Possible__c,CPA_v2_0__r.Tracking_Term__c,Client_Contact_for_this_Shipment__r.Client_Notifications_Choice__c,Client_Contact_for_this_Shipment__r.Email,Client_Contact_for_this_Shipment__r.Major_Minor_notification__c,Client_Contact_for_this_Shipment__r.Client_Tasks_notification_choice__c,CPA_v2_0__c,CPA_v2_0__r.Related_Costing_CPA__c, CPA_v2_0__r.VAT_Rate__c';
        Map<Id,Shipment_Order__c> soWithParents = new Map<Id,Shipment_Order__c>((List<Shipment_Order__c>)Database.query('SELECT Id,'+parentFields+' FROM Shipment_Order__c WHERE Id IN:ids'));
        Set<Id> ShipmentIds = new Set<Id>();
        Set<Id> ShipmentIdsForInvoices = new Set<Id>();
        Map<String, String> soMapForCEPDF = new Map<String,String>();

        Set<Id> soFetchFreight = new Set<Id>();
        Map<Id, List<Freight__c> > soToFreight = new Map<Id, List<Freight__c> >();

        for(Shipment_Order__c shipmentOrder : Trigger.new) {

            //Auto-Review CCD automation
            if(shipmentOrder.Shipping_Status__c == 'POD Received' ) {
                autoReviewCCDIds.add(shipmentOrder.Id);
            }


            //filterning only matched criteria records
            if(isChanged(shipmentOrder,'Who_arranges_International_courier__c') && shipmentOrder.Who_arranges_International_courier__c == 'Tecex') {
                soFetchFreight.add(shipmentOrder.Id);
            }

            //(Vishal) For generating the Document When Quote is Created.
            if(isChanged(shipmentOrder,'Shipping_Status__c') && Trigger.oldMap.get(shipmentOrder.Id).Shipping_Status__c == 'Cost Estimate Abandoned' && shipmentOrder.Shipping_Status__c == 'Cost Estimate') {
                soMapForCEPDF.put(shipmentOrder.Id, 'Initial CE');
            }

            Shipment_Order__c soWithParent = soWithParents.get(shipmentOrder.Id);
            Shipment_Order__c shipment = shipmentOrder.clone(true, true, true, true);
            shipment.putSObject('Account__r',soWithParent.getSobject('Account__r'));
            shipment.putSObject('CPA_v2_0__r',soWithParent.getSobject('CPA_v2_0__r'));
            shipment.putSObject('IOR_Price_List__r',soWithParent.getSobject('IOR_Price_List__r'));
            shipment.putSObject('Client_Contact_for_this_Shipment__r',soWithParent.getSobject('Client_Contact_for_this_Shipment__r'));
            //if CPA2 changes
            //DestinationIsChangedParts
            if(isChanged(shipment,'CPA_v2_0__c') && shipment.CPA_v2_0__c != null) {
                //update so fields from CPA
                fieldsUpdateFromCPA(shipment,(CPA_v2_0__c)shipment.getSobject('CPA_v2_0__r'));
                ZenkraftOnFreightHandler.isCPAChanged = true;
                CPA_v2_0__c cpa = (CPA_v2_0__c)shipment.getSobject('CPA_v2_0__r');
                (ZenkraftOnFreightHandler.ShipIdWithSupplier).put(shipment.Id,cpa.Supplier__c);
                soToUpdateFreight.add(shipment.Id);
                //recalculate tax on cpa change
                soIdsToCalculateTax.put(shipment.Id,shipment);
                soIdsCPAChanged.add(shipment.Id);
                CPAToUpdate.put(shipment.CPA_v2_0__c, new CPA_v2_0__c(Id = shipment.CPA_v2_0__c, Last_Shipment_Date__c = system.today()));
            }
            // Vishal for Updating Domestic_Movement__c
            if((isChanged(shipment, 'Ship_From_Country__c') || isChanged(shipment, 'Destination__c')) && shipment.Ship_From_Country__c != null && shipment.Destination__c != null) {
                if((shipment.Ship_From_Country__c == shipment.Destination__c)
                   || (CountryandRegionMap__c.getInstance(shipment.Ship_From_Country__c) ?.European_Union__c == 'European Union'
                       && CountryandRegionMap__c.getInstance(shipment.Destination__c) ?.European_Union__c == 'European Union')) {
                    shipment.Domestic_Movement__c = true;
                }
                else{
                    shipment.Domestic_Movement__c = false;
                }
            }
            // End
            //Update Existing Freight Requests
            if(isChanged(shipment,'Chargeable_Weight__c')||isChanged(shipment,'Ship_From_Country__c')||isChanged(shipment,'Destination__c')||isChanged(shipment,'Hub_Country__c')||isChanged(shipment,'Li_ion_Batteries__c')||isChanged(shipment,'Lithium_Battery_Types__c')) {
                soToUpdateFreight.add(shipment.Id);
                if(isChanged(shipment,'Destination__c') || isChanged(shipment,'Clearance_Destination_Text__c')) {
                    soIdsDestinationChanged.add(shipment.Id);
                }
            }
            //Send Cost estimate email on request_Email_estimate__C field change
            if(shipment.Request_Email_Estimate__c && isChanged(shipment,'Request_Email_Estimate__c') && String.isNotBlank(Shipment.Email_Estimate_To__c)) {
                ShipmentOrderTriggerHandler.sendCEMail(shipment.Id);
                shipment.Request_Email_Estimate__c = false;
            }
            // For updating the Tax_Treatment__c fields when Who_arranges_International_courier__c field is updated.
            if(shipment.CPA_v2_0__r.Billing_Term__c != 'DAP/CIF - IOR pays' && isChanged(shipment,'Who_arranges_International_courier__c') && shipment.Who_arranges_International_courier__c == 'Tecex') {
                shipment.Tax_Treatment__c = 'DDP - Tecex Account';
            }
            else if(shipment.CPA_v2_0__r.Billing_Term__c != 'DAP/CIF - IOR pays' && isChanged(shipment,'Who_arranges_International_courier__c') && shipment.Who_arranges_International_courier__c == 'Client') {
                shipment.Tax_Treatment__c = 'DDP - Client Account';
            }
            shipment.Destination_part_of_EU__c = CountryandRegionMap__c.getInstance(shipment.Destination__c) ?.European_Union__c == 'European Union';
            //shipment.Ship_From_Part_of_EU__c = CountryandRegionMap__c.getInstance(shipment.Ship_From_Country__c)?.European_Union__c == 'European Union';

            if(!shipment.BO_NBO_Shipment_Override__c && shipment.Intention_with_Goods__c == 'To Sell Goods' && shipment.Buyer_or_BO_Part_of_EU__c && shipment.Status_of_LOA__c == 'Received') {
                shipment.Contact_number__c = 'Non-Beneficial Owner Shipment';
            }else if(!shipment.BO_NBO_Shipment_Override__c && shipment.Status_of_LOA__c == 'Received' && ((shipment.Intention_with_Goods__c == 'To Sell Goods' && !shipment.Buyer_or_BO_Part_of_EU__c) || (shipment.Intention_with_Goods__c == 'Retain Ownership of Goods'))) {
                shipment.Contact_number__c = 'Beneficial Owner Shipment';
            }

            soNewList.put(shipment.Id,shipment);
            /*if(isChanged(shipment,'Shipping_Status__c') && shipment.Shipping_Status__c == 'POD Received') {
                System.debug('POD Received Change Captured');
                 ShipmentIdsForInvoices.add(shipment.Id);
               }*/

        }

        /*if(ShipmentIdsForInvoices != null && !ShipmentIdsForInvoices.isEmpty()){
           List<Invoice_New__c> invoicesPODUpdate = new List<Invoice_New__c>();
           For(Shipment_Order__c shipment: [SELECT POD_Date__c , (SELECT Id,POD_Date_New__c FROM Invoices2__r) FROM Shipment_Order__c where Id=:ShipmentIdsForInvoices]){
               if(shipment.Invoices2__r != null){
                   for(Invoice_New__c invoice : shipment.Invoices2__r){
                       invoice.POD_Date_New__c = shipment.POD_Date__c;
                       invoicesPODUpdate.add(invoice);
                   }
               }
           }

           if(invoicesPODUpdate != null && !invoicesPODUpdate.isEmpty()){
               update invoicesPODUpdate;
           }
            } */

        if(!autoReviewCCDIds.isEmpty()) {

            List<Customs_Clearance_Documents__c> ccdRecords = [SELECT Id, Auto_Review__c, Customs_Clearance_Documents__c, Status__c
                                                               FROM Customs_Clearance_Documents__c
                                                               WHERE Customs_Clearance_Documents__c IN : autoReviewCCDIds
                                                               AND (Auto_Review__c = true OR Status__c = 'Reviewed')];
            autoReviewCCDIds.clear();
            for(Customs_Clearance_Documents__c ccd : ccdRecords) {
                autoReviewCCDIds.add(ccd.Customs_Clearance_Documents__c);
            }

        }


        if(!soFetchFreight.isEmpty()) {
            for(Freight__c fr : Database.query('SELECT '+String.join(new List<String>(Freight__c.sObjectType.getDescribe().fields.getMap().keySet()), ',')+' FROM Freight__c WHERE Shipment_Order__c IN:soFetchFreight')) {
                if(soToFreight.containsKey(fr.Shipment_Order__c)) {
                    soToFreight.get(fr.Shipment_Order__c).add(fr);
                }else{
                    soToFreight.put(fr.Shipment_Order__c,new List<Freight__c> {fr});
                }
            }
        }


        if(!soToUpdateFreight.isEmpty()) {
            List<Freight__c> frList = [Select Id, Chargeable_weight_in_KGs_packages__c, Li_Ion_Batteries__c, Li_Ion_Battery_Type__c,Ship_From_Country__c, Ship_To__c, Shipment_Order__c FROM Freight__c WHERE Pick_Up_Request__c = false AND Shipment_Order__c IN : soToUpdateFreight];
            for(Freight__c fr : frList) {
                Shipment_Order__c so = soNewList.get(fr.Shipment_Order__c);
                CPA_v2_0__c cpa = (CPA_v2_0__c)so.getSobject('CPA_v2_0__r');
                fr.Chargeable_weight_in_KGs_packages__c = so.Chargeable_Weight__c;
                fr.Li_Ion_Batteries__c = so.Li_ion_Batteries__c;
                fr.Li_Ion_Battery_Type__c = so.Lithium_Battery_Types__c;
                fr.Ship_From_Country__c = so.Ship_From_Country__c;
                fr.Ship_To__c = so.Hub_Country__c == null ? so.Destination__c : so.Hub_Country__c;//it similar to cpa change
                fr.Actual_Weight_KGs__c = so.Actual_Weight_KGs__c;
                fr.Packages_of_Same_Weight_Dimensions__c = so.of_packages__c;
                fr.FF_Only_Lane__c  = cpa.Freight_Only_Lane__c;
            }
            // we need to check here is SO getting update back from freight trigger
            // need to handle that.
            RecusrsionHandler.freightUpdateFromSO = true;
            update frList;
            for(Shipment_Order__c shipment: soNewList.values()) {
                if(FreightPBTrigger.soTOUpdateWithCalculatedFrFee.containsKey(shipment.Id)) {
                    Shipment_Order__c so = FreightPBTrigger.soTOUpdateWithCalculatedFrFee.get(shipment.Id);
                    shipment.Freight_Fee_Calculated_Freight_Request__c = so.Freight_Fee_Calculated_Freight_Request__c;
                    shipment.International_Delivery_Cost__c = so.International_Delivery_Cost__c;
                }
            }
        }


        Map<String,List<String> > ContactsWithShipment = new Map<String,List<String> >();

        for(Notify_Parties__c notifyParty :[SELECT Id,Shipment_Order__c,Contact__c,Contact__r.Email,Contact__r.Client_Tasks_notification_choice__c FROM Notify_Parties__c Where Shipment_Order__c IN :soNewList.keySet()]) { // Contact__r.Include_in_SO_Communication__c = true ANDContact__r.Client_Notifications_Choice__c != 'Opt-Out'
            List<String> contactEmails = new List<String>();

            if(ContactsWithShipment.containsKey(notifyParty.Shipment_Order__c)) {
                contactEmails = ContactsWithShipment.get(notifyParty.Shipment_Order__c);
            }

            if(notifyParty.Contact__r.Email != null)
                contactEmails.add(notifyParty.Contact__r.Email);



            ContactsWithShipment.put(notifyParty.Shipment_Order__c,contactEmails);

        }
        for(Shipment_Order__c shipment : soNewList.values()) {

            if(autoReviewCCDIds.contains(shipment.Id) || (shipment.Tax_Treatment__c == 'DDP - Client Account' && shipment.Shipping_Status__c == 'POD Received' &&
                                                          (shipment.CPA_v2_0__r.DDP_Client_Auto_Status_Update__c != null && shipment.CPA_v2_0__r.DDP_Client_Auto_Status_Update__c) &&
                                                          (shipment.Account__r.DDP_Client_Auto_Status_Update__c != null && shipment.Account__r.DDP_Client_Auto_Status_Update__c))) {
                shipment.Shipping_Status__c = 'Customs Clearance Docs Received';
            }


            if(isSOCloseDownConditionMatched(shipment)) {
                shipment.Closed_Down_Date__c = system.today();
                shipment.Closed_Down_SO__c = true;
            }
            //Add Debtors Info WF
            /* shipment.Balance_outstanding__c = shipment.Account__r.Balance_outstanding__c;
               shipment.Over_60_days__c = shipment.Account__r.X61_90_Days__c + shipment.Account__r.Over_90_Days__c;
             *///Commented based on ticket CB - 7415 -> below 2 lines added.
            shipment.Balance_outstanding__c = (shipment.Customer_Value__c - (shipment.Total_Value_Applied_on_Customer_Invoice__c != null ? shipment.Total_Value_Applied_on_Customer_Invoice__c : 0)) - (shipment.Supplier_Value__c - (shipment.Total_Value_Applied_on_Supplier_Invoice__c != null ? shipment.Total_Value_Applied_on_Supplier_Invoice__c : 0));
            shipment.Over_60_days__c = (shipment.Account__r.Amount_Outstanding_61_90_Days__c != null ? shipment.Account__r.Amount_Outstanding_61_90_Days__c : 0) + ( shipment.Account__r.Amount_Outstanding_Over_90_Days__c != null ? shipment.Account__r.Amount_Outstanding_Over_90_Days__c : 0);
            //Auto Populate Address WF
            if((shipment.Account__c == '0010Y00000PEnvn' && isChanged(shipment,'Account__c')) || Test.isRunningTest()) {
                shipment.Pick_Up_Address__c = 'Orange Business Services U.S. Inc., C/O MDSi,6225 Shiloh Road,Alpharetta, GA 30005';
                shipment.Pick_Up_Contact__c = 'Jasmine Lowe';
                shipment.Pick_Up_contact_Number_new__c = '6783416206'; // there is another field with same label.
                shipment.Ship_From_Country__c = 'United States';
            }
            //Date Loss making SO WF
            if(shipment.Loss_Making_Shipments__c && isChanged(shipment,'Loss_Making_Shipments__c')) {
                shipment.Loss_Making_SO_checked_Date__c = System.now();
                shipment.Loss_Making_SO_Checked_by__c = UserInfo.getFirstName() + ' ' + UserInfo.getLastName();
            }
            //Date of escalation WF
            if(shipment.Pressure_Point__c && isChanged(shipment,'Pressure_Point__c')) {
                shipment.Date_of_escalation__c = System.now();
            }
            //Update New Status Date WF
            if(isChanged(shipment,'Shipping_Status__c') && Trigger.oldMap.get(shipment.id).Shipping_Status__c != 'Shipment Abandoned' && Trigger.oldMap.get(shipment.id).Shipping_Status__c != 'Cost Estimate Abandoned' && Trigger.oldMap.get(shipment.id).Shipping_Status__c != 'Roll-Out' ) {
                shipment.New_Status_Date__c = Datetime.now();
            }
            //Update International Delivery Cost WF
            //these field are updated from freight trigger
            /* if(isChanged(shipment,'Value_Hub_Shipping_Cost__c') || isChanged(shipment,'Value_International_Delivery_Cost__c') || isChanged(shipment,'Shipment_Value_USD__c')) {
               shipment.International_Delivery_Cost__c = shipment.Freight_Fee_Calculated_Freight_Request__c != null ? shipment.Freight_Fee_Calculated_Freight_Request__c * 0.6 : null;
               } */
            //code from old attachcostestimate
            if(shipment.In_Transit_Start_Date__c == null && (shipment.Shipping_Status__c == 'In Transit to Hub' || shipment.Shipping_Status__c == 'Arrived at Hub' || shipment.Shipping_Status__c == 'In transit to country')) {
                shipment.In_Transit_Start_Date__c = system.now();
                shipment.In_Transit_Date__c = System.now().date();
            }
            //assign Supplier SOApproved
            //strip Supplier When Cancelled
            if((isChanged(shipment,'Shipping_Status__c') && shipment.Shipping_Status__c == 'Complete Shipment Pack Submitted for Approval') || (shipment.SupplierlU__c == null && shipment.CPA_v2_0__c != null && shipment.Shipping_Status__c != 'Roll-Out')) {
                shipment.SupplierlU__c = shipment.CPA_v2_0__r.Supplier__c;
            }
            else if(shipment.Shipping_Status__c.contains('Cancelled - No')) {
                shipment.SupplierlU__c = null;
            }
            //Node1: map Supplier Shipping Status to Shipping Status
            //Node2:  Map Shipping Status to Supplier Status
            if(isChanged(shipment,'Supplier_Shipment_Status__c')) {
                shipment.Shipping_Status__c = transformShippingStatus(shipment.Supplier_Shipment_Status__c, false);
            }else if(isChanged(shipment,'Shipping_Status__c')) {
                shipment.Supplier_Shipment_Status__c = transformShippingStatus(shipment.Shipping_Status__c, true);
            }
            //aMIncentiveDate
            if(shipment.POD_Date__c != null && shipment.No_further_client_invoices_required__c && isChanged(shipment,'No_further_client_invoices_required__c')) {
                shipment.AM_Incentive_Date__c = system.today();
            }
            //Update Final Supplier Invoice Date
            if(isChanged(shipment,'Final_Supplier_Invoice_Received__c') && shipment.Final_Supplier_Invoice_Received__c) {
                shipment.Final_Supplier_Invoice_Paid_Date__c = system.today();
            }
            //SO Canceled with/without fees
            //Merged from old trigger
            if(isChanged(shipment,'Shipping_Status__c') && (shipment.Shipping_Status__c == 'Cancelled - With fees/costs' || shipment.Shipping_Status__c == 'Cancelled - No fees/costs')) {
                shipment.Cancelled_Date__c = system.today();
                if(shipment.Shipping_Status__c == 'Cancelled - With fees/costs') {
                    shipment.POD_Date__c = system.today();
                }
                if(invoiceMap.containsKey(shipment.Id)) {
                    for (Invoice_New__c inv : invoiceMap.get(shipment.Id)) {
                        inv.POD_Date_New__c = shipment.Cancelled_Date__c;
                        updatePOD.add(inv);
                    }
                }
            }
            else if(invoiceMap.containsKey(shipment.Id) && shipment.Cancelled_Date__c == null && isChanged(shipment,'POD_Date__c')) {
                for (Invoice_New__c inv : invoiceMap.get(shipment.Id)) {
                    inv.POD_Date_New__c = shipment.POD_Date__c;
                    updatePOD.add(inv);
                }
            }

            //(Vishal) For generating the Document When Field is Updated.
            //Start
            List<String> soFieldsForCE = new List<String> {'Chargeable_Weight__c','Client_Type__c','Who_arranges_International_courier__c',
                                                           'ECCN_No__c','Final_Deliveries_New__c','Shipment_Value_USD__c','International_Delivery_Fee__c','Sum_of_quantity_of_matched_products__c',
                                                           'Manufacturer__c','Li_ion_Batteries__c','Package_Height__c','Service_Type__c','of_Line_Items__c',
                                                           'Contains_Batteries_Auto__c','Contains_Batteries_Client__c','Contains_Encrypted_Goods_Auto__c',
                                                           'Contains_Encrypted_Goods_Client__c','Contains_Wireless_Goods_Auto__c',
                                                           'Contains_Wireless_Goods_Client__c','Prohibited_Manufacturer_Auto__c',
                                                           'Prohibited_Manufacturer_Client__c','Type_of_Goods__c','Account__c','Destination__c',
                                                           'NL_Product_User__c','Ship_From_Country__c'};

            for(String fieldName : soFieldsForCE) {
                if(isChanged(shipment,fieldName) && Trigger.oldMap.get(shipment.id).Shipping_Status__c != 'Cost Estimate Abandoned') {
                    Map<String, Schema.SObjectField> fieldMap = Schema.getGlobalDescribe().get('Shipment_Order__c').getDescribe().fields.getMap();
                    soMapForCEPDF.put(shipment.Id, fieldMap.get(fieldName).getDescribe().getLabel());
                    break;
                }
            }
            //End

            //Cost estimate conversion
            if(isChanged(shipment,'Shipping_Status__c') && shipment.Shipping_Status__c != 'Cost Estimate Rejected' && shipment.Shipping_Status__c != 'Cost Estimate Abandoned'
               && shipment.Shipping_Status__c != 'Roll-Out' && (Trigger.oldMap.get(shipment.id).Shipping_Status__c == 'Cost Estimate' || Trigger.oldMap.get(shipment.id).Shipping_Status__c == 'Shipment Abandoned')) {

                system.debug('From ZEE api after update in convertion block ');
                shipment.Go_Live_Date__c = System.today();
                shipment.cost_Estimate_acceptance_date__c = System.today();
                String emailTemplate = 'Quote Converted';
                String emailAddress = 'app@tecex.com';
                if(shipment.RecordTypeId == Schema.Sobjecttype.Shipment_Order__c.getRecordTypeInfosByDeveloperName().get('Zee_Cost_Estimate').getRecordTypeId()) {
                    shipment.RecordTypeId = Schema.Sobjecttype.Shipment_Order__c.getRecordTypeInfosByDeveloperName().get('Zee_Shipment_Order').getRecordTypeId();
                    emailTemplate = 'Zee Quote Converted';
                    emailAddress = 'app@zee.co';
                }
                else{
                    shipment.RecordTypeId = Schema.Sobjecttype.Shipment_Order__c.getRecordTypeInfosByDeveloperName().get('Shipment_Order').getRecordTypeId();
                }

                if(shipment.invoice_Timing__c != 'Post-CCD Invoicing' && shipment.Populate_Invoice__c != true && shipment.RecordTypeId == Schema.Sobjecttype.Shipment_Order__c.getRecordTypeInfosByDeveloperName().get('Zee_Shipment_Order').getRecordTypeId()) {
                    shipment.Populate_Invoice__c = true;
                }
                //(Vishal) For generating document When Quote is Accepted.
                soMapForCEPDF.put(shipment.Id, 'Accepted CE');

                // resetting the halt days once its accepted.
                shipment.Halt__c = false;
                shipment.Halt_Start_Date__c = null;
                shipment.Total_Halt_Business_Days__c = 0;
                shipment.Total_Halt_Days__c = 0;


                shipment.Cost_Estimate_Accepted__c = true;
                shipment.Cost_Estimate_attached__c = false;
                if(shipment.Account__r.NCP_Access__c != null && shipment.Client_Contact_for_this_Shipment__c != null) {



                    List<EmailTemplate> lstEmailTemplates = new List<EmailTemplate>();
                    List<OrgWideEmailAddress> owea = [select Id from OrgWideEmailAddress where Address = : emailAddress];
                    if(shipment.Account__r.NCP_Access__c && !isSORecordTypeIsZee(shipment.RecordTypeId)) {
                        lstEmailTemplates = [SELECT Id, Body, Name, Subject from EmailTemplate where Name = 'SO Creation - TecEx'];
                    }
                    else if(shipment.Account__r.NCP_Access__c && isSORecordTypeIsZee(shipment.RecordTypeId)) {
                        lstEmailTemplates = [SELECT Id, Body, Name, Subject from EmailTemplate where Name = 'SO Creation - Zee'];
                    }

                    else{
                        lstEmailTemplates = [SELECT Id, Body, Name, Subject from EmailTemplate where Name = :emailTemplate];
                    }
                    if(!lstEmailTemplates.isEmpty()) {
                        List<String> toAddresses = new List<String> {shipment.IOR_CSE__c, shipment.Lead_ICE__c, shipment.Service_Manager__c};
                        /*if(shipment.Lead_Pod_Email__c != null) {
                           toAddresses.add(shipment.Lead_Pod_Email__c);
                           }*/

                        List<String> ccAddress = new List<String>();
                        if(shipment.Pod_Email_Formula__c != null) {
                            toAddresses.add(shipment.Pod_Email_Formula__c);
                        }
                        if(shipment.Client_Contact_for_this_Shipment__r.Email != null) {
                            toAddresses.add(shipment.Client_Contact_for_this_Shipment__c);
                        }
                        if(shipment.Freight_co_ordinator__c != null) {
                            toAddresses.add(shipment.Freight_co_ordinator__c);
                        }
                        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                        mail.setTemplateId(lstEmailTemplates[0].Id);
                        mail.setSaveAsActivity(false);
                        if ( owea.size() > 0 ) {
                            mail.setOrgWideEmailAddressId(owea[0].Id);
                        }
                        mail.setTargetObjectId(shipment.Client_Contact_for_this_Shipment__c);
                        mail.setReplyTo('noreplyapp@tecex.com');
                        mail.setToAddresses(toAddresses);
                        mail.setWhatId(shipment.id);
                        if(ContactsWithShipment.get(shipment.Id) != null) {
                            List<String> ccAddressFromNotifyParties = ContactsWithShipment.get(shipment.Id);
                            ccAddress.addAll(ccAddressFromNotifyParties);
                        }
                        //ccAddress.add('luhar.aadil@concret.io');
                        mail.ccaddresses = ccAddress;
                        system.debug('mail --> '+mail);
                        Messaging.SendEmailResult[] resultMail = Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
                    }
                }
            }
            //Clear Freight Costs
            if(isChanged(shipment,'Who_arranges_International_courier__c') && shipment.Who_arranges_International_courier__c == 'Client') {
                shipment.International_Delivery_Cost__c = 0;
                shipment.Value_Hub_Shipping_Cost__c = 0;
                shipment.Value_International_Delivery_Cost__c = 0;
            }

            //filling freight cost if Who_arranges_International_courier__c changes to tecex.
            if(isChanged(shipment,'Who_arranges_International_courier__c') && shipment.Who_arranges_International_courier__c == 'Tecex' && soToFreight.containsKey(shipment.Id)) {
                List<Freight__c> frList = soToFreight.get(shipment.Id);

                Decimal totalOfFreightFee = 0;
                for(Freight__c fr : frList) {
                    totalOfFreightFee += fr.International_freight_fee_invoiced__c;
                }
                shipment.International_Delivery_Cost__c = totalOfFreightFee * 0.6;

            }

            //update priceList
            if(((isChanged(shipment,'Who_arranges_International_courier__c') || isChanged(shipment,'IOR_Price_List__c') || isChanged(shipment,'Service_Type__c') || isChanged(shipment,'Shipment_Value_USD__c')) && shipment.Shipping_Status__c != 'Roll-Out') || shipment.Pull_CPA_and_IOR_PL__c) {
                //VAT Team Possible VAT Recovery WF
                //VAT Team VAT Claiming Country WF
                if(vatPossibleCountries.contains(shipment.IOR_Price_List__r.Name) && !shipment.VAT_Claiming_Country__c && shipment.Account__r.Name != 'Internal Testing Account' ) {
                    if(users.containsKey('Vat Berkeley')) sendMail('VAT Team Possible VAT Recovery', new List<String> {users.get('Vat Berkeley')}, shipment);
                    if(vatClaimCountries.contains(shipment.IOR_Price_List__r.Name) && !users.isEmpty()) {
                        sendMail('VAT Team VAT Claiming Country', users.values(), shipment);
                    }
                }
                //Tax Recovery WF
                if(shipment.Account__r.Tax_recovery_client__c && shipment.IOR_Price_List__r.Tax_recovery_Premium_Lane__c) {
                    shipment.Tax_Recovery__c = true;
                    shipment.Tax_recovery_Premium_Rate__c = shipment.IOR_Price_List__r.Tax_recovery_Premium_Rate__c;
                }
                updateFieldsFromPriceList(shipment, (IOR_Price_List__c)shipment.getSobject('IOR_Price_List__r'));
                shipment.Pull_CPA_and_IOR_PL__c = false;
            }
            //Cancelled No Fees update
            if(isChanged(shipment,'Shipping_Status__c') && Trigger.oldMap.get(shipment.Id).Shipping_Status__c == 'Cancelled - With fees/costs') {
                shipment.POD_Date__c = null;
            }
            System.debug('shipment.Special_Client_Invoicing__c-->' +shipment.Special_Client_Invoicing__c + '-->'+isChanged(shipment,'Special_Client_Invoicing__c'));
            System.debug('shipment.Clear_out_all_costings__c-->'+shipment.Clear_out_all_costings__c + '-->'+isChanged(shipment,'Clear_out_all_costings__c'));
            //Finance amendments for special invoicing
            if((isChanged(shipment,'Clear_out_all_costings__c') && shipment.Clear_out_all_costings__c) ||
               (isChanged(shipment,'Special_Client_Invoicing__c') && shipment.Special_Client_Invoicing__c)) {
                if(shipment.Clear_out_all_costings__c && !shipment.Special_Client_Invoicing__c) {
                    shipment.Bank_Cost_b__c = 0;
                    shipment.Bank_Fees__c = 0;
                    shipment.Cash_Outlay_Cost__c = 0;
                    shipment.Client_Taking_Liability_Cover__c  = 'No';
                    shipment.Collection_Administration_Fee__c = 0;
                    shipment.Courier_Handover_Cost__c = 0;
                    shipment.Customs_Brokerage_Cost_CIF__c = 0;
                    shipment.Customs_Brokerage_Cost_fixed__c = 0;
                    shipment.Customs_Clearance_Cost_b__c = 0;
                    shipment.Customs_Handling_Cost__c = 0;
                    shipment.Customs_Inspection_Cost__c = 0;
                    shipment.Delivery_Order_Cost__c = 0;
                    shipment.Delvier_to_IOR_Warehouse_Cost__c = 0;
                    shipment.EOR_Fee_new__c = 0;
                    shipment.EOR_Min_USD_new__c = 0;
                    shipment.EOR_and_Export_Compliance_Fee_USD_numb__c = 0;
                    shipment.Fixed_IOR_Cost__c = 0;
                    shipment.Forecast_Net_Profit_v3__c = 0;
                    shipment.Forecast_Override__c = true;
                    shipment.Forecast_Supplier_Cost__c = 0;
                    shipment.Freight_Fee_Calculated_Freight_Request__c = 0;
                    shipment.Handling_and_Admin_Fee__c = 0;
                    shipment.Handling_other_costs__c = 0;
                    shipment.IOR_Cost_b__c = 0;
                    shipment.IOR_Fee_new__c = 0;
                    shipment.IOR_Min_USD__c = 0;
                    shipment.IOR_and_Import_Compliance_Fee_USD_numb__c = 0;
                    shipment.Import_Declaration_Cost__c = 0;
                    shipment.Insurance_Cost_CIF__c = 0;
                    shipment.Insurance_Fee__c = 0;
                    shipment.License_Permit_Cost__c = 0;
                    shipment.Loading_Cost__c = 0;
                    shipment.Local_Delivery_Cost__c = 0;
                    shipment.Minimum_Brokerage_Costs__c = 0;
                    shipment.Minimum_Clearance_Costs__c = 0;
                    shipment.Minimum_Handling_Costs__c = 0;
                    shipment.Minimum_License_Permit_Costs__c = 0;
                    shipment.Miscellaneous_Costs__c = 0;
                    shipment.Miscellaneous_Fee__c = 0;
                    shipment.Open_and_Repack_Cost__c = 0;
                    shipment.Override_Calculated_Taxes__c = true;
                    shipment.Quarantine_cost__c = 0;
                    shipment.Storage_Cost__c = 0;
                    shipment.Supplier_Admin_Cost__c = 0;
                    shipment.Tax_Cost__c = 0;
                    shipment.TecEx_Shipping_Fee_Markup__c = 0;
                    shipment.Total_Customs_Brokerage_Cost1__c = 0;
                    shipment.Value_Hub_Shipping_Cost__c = 0;
                    shipment.Value_International_Delivery_Cost__c= 0;
                }
                if(!shipment.Clear_out_all_costings__c && shipment.Special_Client_Invoicing__c) {
                    shipment.Bank_Cost_b__c = shipment.FC_Bank_Fees__c;
                    shipment.Courier_Handover_Cost__c = 0;
                    shipment.Customs_Brokerage_Cost_CIF__c = 0;
                    shipment.Customs_Brokerage_Cost_fixed__c = Math.MAX(shipment.FC_Total_Customs_Brokerage_Cost__c,shipment.Minimum_Brokerage_Costs__c);
                    shipment.Customs_Clearance_Cost_b__c = Math.MAX(shipment.FC_Total_Clearance_Costs__c,shipment.Minimum_Clearance_Costs__c);
                    shipment.Customs_Handling_Cost__c = Math.MAX(shipment.FC_Total_Handling_Costs__c,shipment.Minimum_Handling_Costs__c);
                    shipment.Customs_Inspection_Cost__c = 0;
                    shipment.Delivery_Order_Cost__c = 0;
                    shipment.Delvier_to_IOR_Warehouse_Cost__c = 0;
                    shipment.Examination_Cost__c = shipment.FC_Finance_Fee__c;
                    shipment.Fixed_IOR_Cost__c = 0;
                    shipment.Forecast_Override__c = true;
                    shipment.Handling_other_costs__c = 0;
                    shipment.IOR_Cost_Custom_b__c = 0;
                    shipment.IOR_Cost_If_in_USD_2__c = shipment.FC_IOR_and_Import_Compliance_Fee_USD__c;
                    shipment.IOR_Cost_b__c = 0;
                    shipment.Import_Declaration_Cost__c = 0;
                    shipment.Minimum_License_Permit_Costs__c = Math.MAX(shipment.FC_Total_License_Cost__c,shipment.Minimum_License_Permit_Costs__c );
                    shipment.License_Permit_Cost__c = Math.MAX(shipment.Minimum_License_Permit_Costs__c,shipment.FC_Total_License_Cost__c);
                    shipment.Loading_Cost__c = 0;
                    shipment.Local_Delivery_Cost__c = 0;
                    shipment.Minimum_Brokerage_Costs__c = Math.MAX(shipment.FC_Total_Customs_Brokerage_Cost__c,shipment.Minimum_Brokerage_Costs__c);
                    shipment.Minimum_Clearance_Costs__c = Math.MAX(shipment.FC_Total_Clearance_Costs__c,shipment.Minimum_Clearance_Costs__c);
                    shipment.Minimum_Handling_Costs__c = Math.MAX(shipment.FC_Total_Handling_Costs__c,shipment.Minimum_Handling_Costs__c );
                    shipment.Miscellaneous_Costs__c = shipment.FC_Miscellaneous_Fee__c;
                    shipment.Open_and_Repack_Cost__c = 0;
                    shipment.Quarantine_cost__c = 0;
                    shipment.Storage_Cost__c = 0;
                    shipment.Supplier_Admin_Cost__c = shipment.FC_Admin_Fee__c;
                    shipment.Terminal_Charges_Cost__c = 0;
                }
            }
            if(shipment.Mapped_Shipping_status__c != '' && isChanged(shipment,'Mapped_Shipping_status__c') && (shipment.Mapped_Shipping_status__c == 'Quote Created' || shipment.Mapped_Shipping_status__c == 'Compliance Pending')) {
                soToCalculateETA.add(shipment);
            }
            /*Keshav Added this line*/
            Map<String, Countries_Not_Liability_Cover__c> ListCountriesNotHasLiabilityCover = Countries_Not_Liability_Cover__c.getAll();
            if(!shipment.Liability_Cover_Fee_Added__c && shipment.Shipping_Status__c != 'Roll-Out') {
                /*Keshav updated these condition*/
                if( ((shipment.Client_Taking_Liability_Cover__c == 'Yes' || shipment.Client_Taking_Liability_Cover__c == null)||
                     (shipment.Client_Taking_Liability_Cover__c == 'No'
                      && trigger.oldMap.get(shipment.id).Shipping_status__c == 'Roll-Out'
                      && isChanged(shipment,'Shipping_Status__c') && shipment.Shipping_Status__c == 'Cost Estimate'
                      && !ListCountriesNotHasLiabilityCover.containsKey(shipment.Destination__c) ))
                    && shipment.Account__r.Liability_Product_Agreement__c == 'Annual Rate Agreement') {
                    shipment.Insurance_Cost_CIF__c = 0.125;
                    shipment.Insurance_Fee__c = shipment.Account__r.Insurance_Fee__c == null ? shipment.Account__r.Annual_Liability_Cover_Fee_Formula__c *10000 : shipment.Account__r.Insurance_Fee__c;
                    shipment.Client_Taking_Liability_Cover__c = 'Yes';
                    /*Keshav Added these 4 line*/
                    if(!shipment.Liability_Cover_Fee_Added__c)
                        shipment.Liability_Cover_Fee_USD__c = Math.max(shipment.Shipment_Value_USD__c * shipment.Insurance_Fee__c/100,50);
                    else
                        shipment.Liability_Cover_Fee_USD__c = shipment.Shipment_Value_USD__c * shipment.Insurance_Fee__c/100;
                }
                else if(shipment.Account__r.New_Invoicing_Structure__c
                        && (shipment.Client_Taking_Liability_Cover__c == 'Yes'||
                            ( shipment.Account__r.Liability_Product_Agreement__c == 'Ad Hoc Agreement'
                              && ((shipment.Client_Taking_Liability_Cover__c == 'No'
                                   && trigger.oldMap.get(shipment.id).Shipping_status__c == 'Roll-Out'
                                   && isChanged(shipment,'Shipping_Status__c')
                                   && shipment.Shipping_Status__c == 'Cost Estimate'
                                   && !ListCountriesNotHasLiabilityCover.containsKey(shipment.Destination__c)
                                   ) || shipment.Client_Taking_Liability_Cover__c == null
                                  || isChanged(shipment,'Shipment_Value_USD__c')
                                  )
                            )
                            )
                        ) {

                    Decimal riskAdjFactor = 1;
                    Decimal valueAdjustmentFactor = 1;
                    String toRegion = countryandRegionMap.get(shipment.Destination__c) ?.Region__c;
                    String fromRegion = countryandRegionMap.get(shipment.Ship_From_Country__c) ?.Region__c;
                    for(LiabilityChargeValueAdjustment__c liabilityRecord : liabilityRecords) {

                        //if(liabilityRecord.Floor_Value__c >= shipment.Shipment_Value_USD__c && liabilityRecord.CeilingValue__c <= shipment.Shipment_Value_USD__c) {
                        if(liabilityRecord.Floor_Value__c <= shipment.Shipment_Value_USD__c && liabilityRecord.CeilingValue__c >= shipment.Shipment_Value_USD__c) {
                            valueAdjustmentFactor = liabilityRecord.Value_Adjustment_Factor__c;
                            break;
                        }
                    }
                    if(String.isNotBlank(fromRegion) && String.isNotBlank(fromRegion) && regionalFreightValueMap.containsKey(fromRegion +' to '+toRegion)) {
                        riskAdjFactor = regionalFreightValueMap.get(fromRegion +' to '+toRegion).Risk_Adjustment_Factor__c;
                    }
                    Decimal freightPremium = shipment.Who_arranges_International_courier__c == 'TecEx' ? 0.9 : 1;
                    Decimal effectiveInsuranceRate =  0.250 * valueAdjustmentFactor * freightPremium * riskAdjFactor;
                    System.debug('freightPremium'+freightPremium);
                    shipment.Insurance_Cost_CIF__c = 0.125;
                    System.debug('valueAdjustmentFactor '+valueAdjustmentFactor);
                    System.debug('freightPremium '+freightPremium);
                    System.debug('riskAdjFactor '+riskAdjFactor);
                    System.debug('effectiveInsuranceRate '+effectiveInsuranceRate);

                    shipment.Insurance_Fee__c = effectiveInsuranceRate;
                    shipment.Client_Taking_Liability_Cover__c = 'Yes';
                    if(!shipment.Liability_Cover_Fee_Added__c)
                        shipment.Liability_Cover_Fee_USD__c = Math.max(shipment.Shipment_Value_USD__c * shipment.Insurance_Fee__c/100,50);
                    else
                        shipment.Liability_Cover_Fee_USD__c = shipment.Shipment_Value_USD__c * shipment.Insurance_Fee__c/100;
                }
                else if( shipment.Client_Taking_Liability_Cover__c == 'No'
                         || ((shipment.Client_Taking_Liability_Cover__c == null || shipment.Client_Taking_Liability_Cover__c == 'Yes')
                             && shipment.Account__r.Liability_Product_Agreement__c == 'Liability Cover Declined'
                             )
                         || (shipment.Insurance_Fee_USD__c != 0
                             && specialDestinations.contains(shipment.Destination__c)
                             )
                         ) {
                    System.debug('Inside Liability Cover Declined');
                    System.debug(JSOn.serialize(shipment));
                    shipment.Client_Taking_Liability_Cover__c = 'No';
                    shipment.Insurance_Cost_CIF__c = 0;
                    shipment.Insurance_Fee__c = 0;
                    shipment.Liability_Cover_Cost_USD__c = 0;
                    shipment.Liability_Cover_Fee_USD__c = 0;
                }
            }
            //liability Cover WF
            if(shipment.Insurance_Fee__c == 0) {
                shipment.Insurance_Cost_CIF__c = 0;
            }
            //calculate taxes
            if(shipment.Shipping_Status__c != 'Roll-Out' && !shipment.Taxes_Calculated__c && shipment.CPA_v2_0__c != null ) {
                soIdsToCalculateTax.put(shipment.Id,shipment);
            }
            //CIF, Freight and Insurance
            if(((isChanged(shipment,'Shipping_Status__c') && (shipment.Shipping_Status__c == 'Cost Estimate' || shipment.Shipping_Status__c == 'Shipment Pending')) || RecusrsionHandler.freightUpdateFromSO ||
                isChanged(shipment,'Shipment_Value_USD__c') ||
                isChanged(shipment,'CPA_v2_0__c') ||
                isChanged(shipment,'Freight_Fee_Calculated_Freight_Request__c') ||
                isChanged(shipment,'Who_arranges_International_courier__c') ||
                isChanged(shipment,'Client_Taking_Liability_Cover__c') ||
                isChanged(shipment,'IOR_Price_List__c')
                ) && shipment.Shipping_Status__c != 'Roll-Out' && !shipment.Override_Calculated_Taxes__c) {
                String cifFreight = shipment.CPA_v2_0__r.CIF_Freight_and_Insurance__c;
                if(cifFreight == null) {
                    shipment.Client_Actual_Freight_Value__c = null;
                    shipment.Client_Actual_Insurance_Value__c = null;
                    shipment.CI_Input_Freight__c = null;
                    shipment.CI_Input_Liability_Cover__c = null;
                }else if(cifFreight == 'CIF Amount' || (cifFreight.contains('Insurance Amount') && cifFreight.contains('Freight Amount'))) {
                    shipment.Client_Actual_Freight_Value__c = shipment.Freight_Fee_Calculated_Freight_Request__c;
                    shipment.CI_Input_Freight__c = shipment.Who_arranges_International_courier__c == 'TecEx' ? shipment.Freight_Fee_Calculated_Freight_Request__c : null;
                    shipment.Client_Actual_Insurance_Value__c = Math.max(shipment.Shipment_Value_USD__c * (shipment.Insurance_Fee__c != 0 ? shipment.Insurance_Fee__c/100 : 0.015), 50);
                    if(shipment.Client_Taking_Liability_Cover__c == 'Yes') {
                        shipment.CI_Input_Liability_Cover__c = Math.max(shipment.Shipment_Value_USD__c * (shipment.Insurance_Fee__c != 0 ? shipment.Insurance_Fee__c/100 : 0.015), 50);
                    }else{
                        shipment.CI_Input_Liability_Cover__c = null;
                    }
                }else if(cifFreight == 'Insurance Amount') {
                    shipment.Client_Actual_Insurance_Value__c = Math.max(shipment.Shipment_Value_USD__c * (shipment.Insurance_Fee__c != 0 ? shipment.Insurance_Fee__c/100 : 0.015), 50);
                    if(shipment.Client_Taking_Liability_Cover__c == 'Yes') {
                        shipment.CI_Input_Liability_Cover__c = Math.max(shipment.Shipment_Value_USD__c * (shipment.Insurance_Fee__c != 0 ? shipment.Insurance_Fee__c/100 : 0.015), 50);
                    }else{
                        shipment.CI_Input_Liability_Cover__c = null;
                    }
                    shipment.Client_Actual_Freight_Value__c = 0;
                    shipment.CI_Input_Freight__c = 0;
                }else if(cifFreight == 'Freight Amount') {
                    shipment.CI_Input_Freight__c = shipment.Who_arranges_International_courier__c == 'TecEx' ? shipment.Freight_Fee_Calculated_Freight_Request__c : null;
                    shipment.Client_Actual_Freight_Value__c = shipment.Freight_Fee_Calculated_Freight_Request__c;
                    shipment.Client_Actual_Insurance_Value__c = 0;
                    shipment.CI_Input_Liability_Cover__c = 0;
                }
                soIdsToCalculateTax.put(shipment.Id,shipment);
            }
            //Recalculate taxes
            if(( isChanged(shipment,'Invoice_Timing__c') || isChanged(shipment,'Service_Type__c') || isChanged(shipment,'Total_CIF_Duties__c') || isChanged(shipment,'Total_FOB_Duties__c') || isChanged(shipment,'Shipment_Value_USD__c') || isChanged(shipment,'of_Line_Items__c') || isChanged(shipment,'Total_Line_Item_Extended_Value__c') || shipment.Recalculate_Taxes__c || isChanged(shipment,'Client_Actual_Freight_Value__c') || isChanged(shipment,'Client_Actual_Insurance_Value__c') || (shipment.Ship_From_Country__c != null &&  isChanged(shipment,'Ship_From_Country__c'))) && !shipment.Shipping_Status__c.contains('Abandoned') && !shipment.Closed_Down_SO__c && shipment.Shipping_Status__c != 'Roll-Out') {
                soIdsToCalculateTax.put(shipment.Id,shipment);
            }
            //calculate costing
            if(!shipment.Closed_Down_SO__c && ((isChanged(shipment,'Shipping_Status__c') && shipment.Shipping_Status__c != 'Roll-Out' && shipment.Shipping_Status__c != 'Cost Estimate Abandoned' && shipment.Shipping_Status__c != 'Shipment Abandoned') && !shipment.CPA_Costings_Added__c && shipment.CPA_v2_0__c != null)) {
                shipment.CPA_Costings_Added__c = true;
                soIdsTocalculateCost.put(shipment.Id,shipment);
                cpaIds.add(shipment.CPA_v2_0__c);
            }
            else if(!shipment.Closed_Down_SO__c && (((isChanged(shipment,'Stuck_Shipment__c') || isChanged(shipment,'Ship_From_Country__c') || isChanged(shipment,'of_Unique_Line_Items__c') || isChanged(shipment,'Who_arranges_International_courier__c') || isChanged(shipment,'Invoice_Timing__c') || isChanged(shipment,'Shipping_Complexity__c') || isChanged(shipment,'Li_ion_Batteries__c') || isChanged(shipment,'Contains_Batteries_Auto__c') || isChanged(shipment,'Contains_Batteries_Client__c') || isChanged(shipment,'Contains_Wireless_Goods_Auto__c') || isChanged(shipment,'Contains_Wireless_Goods_Client__c') || isChanged(shipment,'Contains_Encrypted_Goods_Auto__c') || isChanged(shipment,'Contains_Encrypted_Goods_Client__c') ||   isChanged(shipment,'Pre_inspection_Responsibility__c') || isChanged(shipment,'Sub_Status_Update__c') || isChanged(shipment,'of_Line_Items__c') || isChanged(shipment,'Shipment_Value_USD__c') || isChanged(shipment,'CPA_v2_0__c') || isChanged(shipment,'final_Deliveries_New__c') || isChanged(shipment,'Chargeable_Weight__c') || isChanged(shipment,'Service_Type__c')) && shipment.CPA_Costings_Added__c) || shipment.Recalculate_Costings__c)) {
                soIdsTocalculateCost.put(shipment.Id,shipment);
                cpaIds.add(shipment.CPA_v2_0__c);
            }
            //update texes new : moved in NewTaxCalculatorV2 class
        }
        if(!soIdsDestinationChanged.isEmpty()) {
            List<Part__c> parts = [SELECT Id FROM Part__c WHERE Shipment_Order__c IN : soIdsDestinationChanged];
            for(Part__c part : parts)
                part.CPA_Changed__c = true;
            RecusrsionHandler.partUpdateFromSO = true;
            update parts; //DML
            for(Shipment_Order__c shipment : soNewList.values()) {
                if(ShipmentOrderTriggerHandler.soMapFromPartTrigger.containsKey(shipment.Id)) {
                    copyPartFieldsIntoSobject(ShipmentOrderTriggerHandler.soMapFromPartTrigger.get(shipment.Id),shipment);
                }
            }
        }
        if(!soIdsCPAChanged.isEmpty()) {
            setPrefferedOnRates(soIdsCPAChanged);
        }
        if(!soIdsToCalculateTax.isEmpty()) {
            NewTaxCalculatorV2.calculateTaxes(soIdsToCalculateTax);
            Formula.recalculateFormulas(soNewList.values());
            for(Shipment_Order__c shipment : soNewList.values()) {
                Shipment_Order__c soWithParent = soWithParents.get(shipment.Id);
                shipment.putSObject('Account__r',soWithParent.getSobject('Account__r'));
                shipment.putSObject('CPA_v2_0__r',soWithParent.getSobject('CPA_v2_0__r'));
                shipment.putSObject('IOR_Price_List__r',soWithParent.getSobject('IOR_Price_List__r'));
                shipment.putSObject('Client_Contact_for_this_Shipment__r',soWithParent.getSobject('Client_Contact_for_this_Shipment__r'));
            }
        }
        //END : Tax calculation
        if(!soIdsTocalculateCost.isEmpty()) {
            RecusrsionHandler.soCreationProcessStarted = true;
            CalculateCostsV2.createSOCosts(soIdsTocalculateCost,cpaIds);
            for(Shipment_Order__c source : RollupOnCostingHandler.rollupCostingOnSO.values()) {
                copyCostingFieldsIntoSobject(source, soNewList.get(source.Id));
            }
            Formula.recalculateFormulas(soNewList.values());
            for(Shipment_Order__c shipment : soNewList.values()) {
                Shipment_Order__c soWithParent = soWithParents.get(shipment.Id);
                shipment.putSObject('Account__r',soWithParent.getSobject('Account__r'));
                shipment.putSObject('CPA_v2_0__r',soWithParent.getSobject('CPA_v2_0__r'));
                shipment.putSObject('IOR_Price_List__r',soWithParent.getSobject('IOR_Price_List__r'));
                shipment.putSObject('Client_Contact_for_this_Shipment__r',soWithParent.getSobject('Client_Contact_for_this_Shipment__r'));
            }
        }
        for(Shipment_Order__c shipment: soNewList.values()) {
            //Update IOR and EOR fees
            if((isChanged(shipment,'Shipping_Status__c') && shipment.Shipping_Status__c == 'Cost Estimate' ) || ((isChanged(shipment,'IOR_FEE_USD__c') || isChanged(shipment,'EOR_and_Export_Compliance_Fee_USD__c')) && shipment.Shipping_Status__c != 'Roll-Out')) {
                shipment.IOR_and_Import_Compliance_Fee_USD_numb__c = shipment.IOR_FEE_USD__c;
                shipment.EOR_and_Export_Compliance_Fee_USD_numb__c = shipment.EOR_and_Export_Compliance_Fee_USD__c;
                shipment.EOR_and_Export_Compliance_Fee_USD_numb__c = shipment.EOR_and_Export_Compliance_Fee_USD__c;
            }
            //update CI Input Liability Cover
            if(isChanged(shipment,'Insurance_Fee_USD__c') && shipment.Insurance_Fee_USD__c != null) {
                shipment.CI_Input_Liability_Cover__c = shipment.Insurance_Fee_USD__c;
            }
            // Populate Invoice
            if(isChanged(shipment,'Populate_Invoice__c') && shipment.Populate_Invoice__c && !shipment.First_Invoice_Created__c) {
                String invoiceCurrency = shipment.Account__r.Default_invoicing_currency__c;
                Decimal rate = conversionRate.get(invoiceCurrency);
                Invoice_New__c firstInvoice = new Invoice_New__c(
                    Account__c = shipment.Account__c,
                    Admin_Fees__c = shipment.Handling_and_Admin_Fee__c == null ? 0 : shipment.Handling_and_Admin_Fee__c / rate,
                    Bank_Fee__c = shipment.Bank_Fees__c == null ? 0 : shipment.Bank_Fees__c / rate,
                    Conversion_Rate__c = rate,
                    Customs_Brokerage_Fees__c = shipment.Total_Customs_Brokerage_Cost1__c == null ? 0 : shipment.Total_Customs_Brokerage_Cost1__c / rate,
                    Customs_Clearance_Fees__c = shipment.Total_clearance_Costs__c == null ? 0 : shipment.Total_clearance_Costs__c / rate,
                    Customs_Handling_Fees__c = shipment.Recharge_Handling_Costs__c == null ? 0 : shipment.Recharge_Handling_Costs__c / rate,
                    Customs_License_In__c = shipment.Total_License_Cost__c == null ? 0 : shipment.Total_License_Cost__c / rate,
                    EOR_Fees__c = shipment.EOR_and_Export_Compliance_Fee_USD__c == null ? 0 : shipment.EOR_and_Export_Compliance_Fee_USD__c / rate,
                    IOR_Fees__c = shipment.IOR_FEE_USD__c == null ? 0 : shipment.IOR_FEE_USD__c / rate,
                    International_Freight_Fee__c = shipment.International_Delivery_Fee__c == null ? 0 : shipment.International_Delivery_Fee__c / rate,
                    Invoice_Amount_Local_Currency__c =  shipment.Account__r.Invoice_Timing__c == 'Upfront invoicing' && shipment.Account__r.Invoicing_Term_Parameters__c == 'No Terms' ? shipment.Total_Invoice_Amount__c/rate : (shipment.Total_Invoice_Amount__c + shipment.Potential_Cash_Outlay_Fee__c)/rate,
                    Invoice_Currency__c = invoiceCurrency,
                    Invoice_Date__c = date.today(),
                    Invoice_Type__c = 'Invoice',
                    Invoice_amount_USD__c =  shipment.Account__r.Invoice_Timing__c == 'Upfront invoicing' && shipment.Account__r.Invoicing_Term_Parameters__c == 'No Terms' ? shipment.Total_Invoice_Amount__c : shipment.Total_Invoice_Amount__c + shipment.Potential_Cash_Outlay_Fee__c,
                    Liability_Cover_Fee__c = shipment.Insurance_Fee_USD__c == null ? 0 : shipment.Insurance_Fee_USD__c / rate,
                    Miscellaneous_Fee_Name__c = shipment.Miscellaneous_Fee_Name__c,
                    Miscellaneous_Fee__c = shipment.Miscellaneous_Fee__c == null ? 0 : shipment.Miscellaneous_Fee__c / rate,
                    Recharge_Tax_and_Duty_Other__c = shipment.Recharge_Tax_and_Duty_Other__c == null ? 0 : shipment.Recharge_Tax_and_Duty_Other__c / rate,
                    Penalty_Status__c = shipment.Account__r.Penalty_Status__c,
                    Cash_Outlay_Fee__c = shipment.Account__r.Invoice_Timing__c == 'Upfront invoicing' && shipment.Account__r.Invoicing_Term_Parameters__c == 'No Terms' ? 0 : shipment.Potential_Cash_Outlay_Fee__c/rate,
                    Shipment_Order__c = shipment.Id,
                    POD_Date_New__c = shipment.POD_Date__c,
                    Enterprise_Billing__c = shipment.Enterprise_Billing_Id__c,
                    Penalty_Daily_Percentage__c = shipment.Account__r.Penalty_Daily_Percentage__c,
                    RecordTypeId = isSORecordTypeIsZee(shipment.RecordTypeId) ? Schema.Sobjecttype.Invoice_New__c.getRecordTypeInfosByDeveloperName().get('Client_Zee').getRecordTypeId() : Schema.Sobjecttype.Invoice_New__c.getRecordTypeInfosByDeveloperName().get('Client').getRecordTypeId(),
                    Taxes_and_Duties__c = shipment.Recharge_Tax_and_Duty__c == null ? 0 : shipment.Recharge_Tax_and_Duty__c / rate);
                if(isSORecordTypeIsZee(shipment.RecordTypeId)) {
                    firstInvoice.Invoice_Status__c = 'Invoice Sent';
                    ShipmentOrderTriggerHandler.IsZeeInvoiceProcessfromSO = true;
                }
                createFirstInvoices.add(firstInvoice);
                shipment.First_Invoice_Created__c = true;
            }
            //update fees
            if((isChanged(shipment,'Forecast_Net_Fees_Formulas__c')||isChanged(shipment,'Actual_Total_Formula_field__c')||isChanged(shipment,'Salesforce_Supplier_Cost_v2__c') ||isChanged(shipment,'recharge_License_cost__c')|isChanged(shipment,'total_customs_brokerage_cost__c')||isChanged(shipment,'Total_Invoice_Amount__c')  || isChanged(shipment,'IOR_Refund_of_Tax_Duty__c') || isChanged(shipment,'Chargeable_Weight__c') || isChanged(shipment,'Who_arranges_International_courier__c') || isChanged(shipment,'Shipping_Status__c') || isChanged(shipment,'Manual_International_Delivery_Cost__c') || isChanged(shipment,'Manual_Hub_Shipping_Cost__c')) && shipment.Shipping_Status__c != 'Roll-Out' && shipment.Shipping_Status__c != 'Cost Estimate Abandoned' && shipment.Shipping_Status__c != 'Shipment Abandoned') {
                shipment.Actual_Total__c = shipment.Actual_Total_Formula_field__c;
                shipment.xxxTotal_Invoice_Amount__c = shipment.Total_Invoice_Amount__c;
                shipment.IOR_Cost_If_in_USD_2__c = shipment.IOR_Cost_If_in_USD_Formula__c;
                if(!shipment.VAT_Deferment__c) {
                    shipment.IOR_Refund_of_Tax_and_Duty__c = shipment.IOR_Refund_of_Tax_Duty__c;
                }else{
                    shipment.IOR_Refund_of_Tax_and_Duty__c = 0;
                }
                // shipment.Forecast_Supplier_Cost__c = shipment.Salesforce_Supplier_Cost_v2__c;
                if(shipment.Forecast_Override__c) {
                    shipment.Forecast_Supplier_Cost__c = shipment.IOR_Cost_If_in_USD_2__c + shipment.IOR_Cost_Custom_b__c + shipment.IOR_Refund_of_Tax_and_Duty__c+ ( shipment.Variable_Brokerage_Cost__c + shipment.Customs_Brokerage_Cost_fixed__c + shipment.Bank_Cost_b__c + shipment.Supplier_Admin_Cost__c )+( shipment.Customs_Handling_Cost__c + shipment.Courier_Handover_Cost__c + shipment.Delvier_to_IOR_Warehouse_Cost__c + shipment.Local_Delivery_Cost__c + shipment.Examination_Cost__c + shipment.Open_and_Repack_Cost__c + shipment.Loading_Cost__c + shipment.Storage_Cost__c + shipment.Terminal_Charges_Cost__c + shipment.Handling_other_costs__c )+( shipment.Customs_Clearance_Cost_b__c + shipment.Import_Declaration_Cost__c + shipment.Quarantine_cost__c + shipment.Customs_Inspection_Cost__c + shipment.Delivery_Order_Cost__c )+ shipment.License_Permit_Cost__c + (shipment.Who_arranges_International_courier__c == 'TecEx' ? shipment.International_Delivery_Cost__c : 0) + shipment.Insurance_Cost_U__c + shipment.Fixed_IOR_Cost__c + shipment.Miscellaneous_Costs__c;
                }else{
                    shipment.Forecast_Supplier_Cost__c = shipment.FC_Total__c + (shipment.Who_arranges_International_courier__c == 'TecEx' || String.isBlank(shipment.Who_arranges_International_courier__c) ? shipment.International_Delivery_Cost__c : 0) + shipment.IOR_Refund_of_Tax_and_Duty__c - shipment.FC_Admin_Fee__c - shipment.FC_Bank_Fees__c - shipment.FC_Finance_Fee__c - shipment.FC_Insurance_Fee_USD__c - shipment.FC_International_Delivery_Fee__c;
                }
                shipment.Total_Customs_Brokerage_Cost1__c = shipment.Total_Customs_Brokerage_Cost__c;
                shipment.Total_License_Cost__c = shipment.Recharge_License_Cost__c;
                //shipment.Liability_Cover_Cost_USD__c = shipment.Insurance_Cost_U__c;

                System.debug('Shipment_Value_USD__c '+ shipment.Shipment_Value_USD__c);
                System.debug('xxxTotal_Invoice_Amount__c '+ shipment.xxxTotal_Invoice_Amount__c);
                System.debug('Recharge_Tax_and_Duty__c '+ shipment.Recharge_Tax_and_Duty__c);
                System.debug('Finance_Fee__c '+ shipment.Finance_Fee__c);
                System.debug('Insurance_Cost_CIF__c '+ shipment.Insurance_Cost_CIF__c);
                if(shipment.CreatedDate >= Datetime.newInstance(2020,03,03, 01,00,00))
                    shipment.Liability_Cover_Cost_USD__c = (shipment.Shipment_Value_USD__c + shipment.xxxTotal_Invoice_Amount__c - shipment.Recharge_Tax_and_Duty__c - shipment.Finance_Fee__c) * 1.1 * shipment.Insurance_Cost_CIF__c/100;
                else if(shipment.CreatedDate >= Datetime.newInstance(2019,03,01,01,00,00))
                    shipment.Liability_Cover_Cost_USD__c = (shipment.Shipment_Value_USD__c + shipment.International_Delivery_Fee__c) * 1.1 * shipment.Insurance_Cost_CIF__c/100;
                else
                    shipment.Liability_Cover_Cost_USD__c = shipment.Insurance_Cost_CIF__c/100 * shipment.Shipment_Value_USD__c;
                // shipment.Liability_Cover_Fee_USD__c = shipment.Insurance_Fee_USD__c;
                if(shipment.Client_Taking_Liability_Cover__c == 'Yes' && !shipment.Liability_Cover_Fee_Added__c)
                    shipment.Liability_Cover_Fee_USD__c = Math.max(shipment.Shipment_Value_USD__c * shipment.Insurance_Fee__c/100,50);
                else
                    shipment.Liability_Cover_Fee_USD__c = shipment.Shipment_Value_USD__c * shipment.Insurance_Fee__c/100;
                Decimal liability_cover_profit = shipment.Liability_Cover_Fee_USD__c - shipment.Liability_Cover_Cost_USD__c;
                system.debug('shipment.IOR_and_Import_Compliance_Fee_USD_numb__c :'+shipment.IOR_and_Import_Compliance_Fee_USD_numb__c);
                system.debug('shipment.Handling_and_Admin_Fee__c :'+shipment.Handling_and_Admin_Fee__c);
                system.debug('shipment.Bank_Fees__c :'+shipment.Bank_Fees__c);
                system.debug('shipment.EOR_and_Export_Compliance_Fee_USD_numb__c :'+shipment.EOR_and_Export_Compliance_Fee_USD_numb__c);
                system.debug('shipment.Forecast_Override__c :'+shipment.Forecast_Override__c);
                system.debug('shipment.IOR_Cost_If_in_USD_2__c :'+shipment.IOR_Cost_If_in_USD_2__c);
                system.debug('shipment.IOR_Cost_Custom_b__c :'+shipment.IOR_Cost_Custom_b__c);
                system.debug('shipment.FC_IOR_and_Import_Compliance_Fee_USD__c :'+shipment.FC_IOR_and_Import_Compliance_Fee_USD__c);

                Decimal ior_gross_profit = ((shipment.IOR_and_Import_Compliance_Fee_USD_numb__c == null ? 0 : shipment.IOR_and_Import_Compliance_Fee_USD_numb__c) + shipment.Handling_and_Admin_Fee__c + shipment.Bank_Fees__c +  (shipment.EOR_and_Export_Compliance_Fee_USD_numb__c == null ? 0 : shipment.EOR_and_Export_Compliance_Fee_USD_numb__c)) - (shipment.Forecast_Override__c ? (shipment.IOR_Cost_If_in_USD_2__c + shipment.IOR_Cost_Custom_b__c) :  (shipment.FC_IOR_and_Import_Compliance_Fee_USD__c == null ? 0 : shipment.FC_IOR_and_Import_Compliance_Fee_USD__c));
                //shipment.Forecast_Net_Profit_v3__c = shipment.Forecast_Net_Fees_Formulas__c;
                shipment.Forecast_Net_Profit_v3__c = ior_gross_profit + shipment.On_charges_Profit__c + shipment.Shipping_and_Insurance_Profit__c +  shipment.Cash_Outlay_Fee_Profit__c +  shipment.Collection_Administration_Profit__c + liability_cover_profit;
            }
            //End: Added from Before Trigger
            if(isChanged(shipment,'Shipping_status__c')  && shipment.Shipping_status__c == 'Shipment Pending' &&
               (trigger.oldMap.get(shipment.id).Shipping_status__c == 'Shipment Abandoned' ||
                trigger.oldMap.get(shipment.id).Shipping_status__c == 'Cost Estimate Abandoned' ||
                trigger.oldMap.get(shipment.id).Shipping_status__c == 'Cost Estimate')) {
                soLogMap.put(shipment.Id,shipment);
            }
        }
        if(!soMapForCEPDF.isEmpty() && soMapForCEPDF != null && !System.isBatch()) {
            System.debug('Network Source'+String.valueOf(URL.getCurrentRequestUrl()));
            System.debug('User Source'+String.valueOf(UserInfo.getName()+'  '+UserInfo.getProfileId()));
            SO_GenerateCostEstimate.generateCostEstimateDocAsync(UserInfo.getProfileId(),soMapForCEPDF);
        }
        if(!CPAToUpdate.isEmpty()) {
            updateCPALastDate(CPAToUpdate,soNewList.values());
        }
        if(!updatePOD.isEmpty()) {
            update updatePOD;
        }
        if(!soToCalculateETA.isEmpty()) {
            ETACalculation.processQuoteETA(soToCalculateETA);
        }
        if(!createFirstInvoices.isEmpty()) {
            system.debug('createFirstInvoices : '+createFirstInvoices);
            insert createFirstInvoices;
        }
        if(!soLogMap.isEmpty()) {
            CreateSoLogonStatusChange.createSOLOG(soLogMap,'SO Creation');
        }
        if(ShipmentOrderTriggerHandler.IsZeeInvoiceProcessfromSO == true) {
            copyInvoiceSOMap(soNewList, ShipmentOrderTriggerHandler.soMapFromInvoiceTrigger);
        }
        if(!RecusrsionHandler.runTaskInQueueable && !RecusrsionHandler.tasksCreated) {
            ShipmentOrderTriggerHandler.processAfterUpdate(soNewList);
        }else if(!RecusrsionHandler.runTaskIsQueued && RecusrsionHandler.runTaskInQueueable) {
            RecusrsionHandler.runTaskIsQueued = true;
            System.enqueueJob(new MasterTaskQueueable(soNewList.keySet()));
            RecusrsionHandler.skipTriggerExecution = true;
            update soNewList.values();
        }
    }
    public static Boolean isChanged(Shipment_Order__c shipment,String fieldName){
        if(Trigger.isExecuting && Trigger.isUpdate) {
            return shipment.get(fieldName) != Trigger.oldMap.get(shipment.Id).get(fieldName);
        }
        return false;
    }
    public static void fieldsUpdateFromCPA(Shipment_Order__c so,CPA_v2_0__c cpa){
        so.Destination__c = cpa.Destination__c;
        so.Clearance_Destination_Text__c = cpa.Final_Destination__c;
        so.Hub_Country__c = cpa.Hub_Country__c;
        if(cpa.Lead_ICE__c != null) {
            so.Lead_ICE__c = cpa.Lead_ICE__c;
        }
        //changes by Swapnil for 26760
        if(cpa.Shipping_Notes__c != null) {
            so.Shipping_Notes_New__c = cpa.Shipping_Notes__c;
        }
        if(cpa.Supplier__c != null) {
            so.SupplierlU__c = cpa.Supplier__c;
        }
        if(cpa.Billing_Term__c != null && cpa.Billing_Term__c == 'DAP/CIF - IOR pays') {
            so.Tax_Treatment__c = 'DAP/CIF - IOR pays';
        }else if(so.Who_arranges_International_courier__c == 'TecEx') {
            so.Tax_Treatment__c = 'DDP - Tecex Account';
        }else{
            so.Tax_Treatment__c = 'DDP - Client Account';
        }
        if(so.Special_Client_Invoicing__c)
            return;
        //
        if(so.Shipment_Value_USD__c < 50000 && cpa.CPA_Minimum_Brokerage_Costs__c != null) {
            so.Minimum_Brokerage_Costs__c = cpa.CPA_Minimum_Brokerage_Costs__c;
        }else if(cpa.CPA_Minimum_Brokerage_Costs__c != null) {
            so.Minimum_Brokerage_Costs__c = cpa.CPA_Minimum_Brokerage_Costs__c + cpa.CPA_Minimum_Brokerage_Surcharge__c;
        }
        if(cpa.CPA_Minimum_Clearance_Costs__c != null) {
            so.Minimum_Clearance_Costs__c = cpa.CPA_Minimum_Clearance_Costs__c;
        }
        if(cpa.CPA_Minimum_Handling_Costs__c != null) {
            so.Minimum_Handling_Costs__c = cpa.CPA_Minimum_Handling_Costs__c;
        }
        if(cpa.CPA_Minimum_License_Permit_Costs__c != null) {
            so.Minimum_License_Permit_Costs__c = cpa.CPA_Minimum_License_Permit_Costs__c;
        }

    }
    public static void updateFieldsFromPriceList(Shipment_Order__c shipmentOrder,IOR_Price_List__c priceList){
        SYstem.debug('price--> '+priceList);
        shipmentOrder.Admin_TecEx_Freight__c = shipmentOrder.Shipment_Value_USD__c  > 10000 || shipmentOrder.Service_Type__c == 'EOR' ? 0 : priceList.Admin_Fee__c;//null

        if(isSORecordTypeIsZee(shipmentOrder.RecordTypeId)) {

            if(shipmentOrder.Service_Type__c == 'IOR') {
                shipmentOrder.Handling_and_Admin_Fee__c = shipmentOrder.Who_arranges_International_courier__c == 'TecEx' ? priceList.Admin_When_Tecex_does_freight__c : priceList.Admin_Fee__c;
            }
            else{
                shipmentOrder.Handling_and_Admin_Fee__c = priceList.Zee_Admin_Fee_Shipping_Only__c;
            }
            if(shipmentOrder.Shipment_Value_USD__c  <= 10000) {
                shipmentOrder.Bank_Fees__c = priceList.Bank_Fees__c;
            }
            else {
                shipmentOrder.Bank_Fees__c = priceList.Bank_Above_10000__c;
            }
            shipmentOrder.Fixed_IOR_fee__c = priceList.Zee_Fixed_IOR_fee__c;
        }
        else{
            if(shipmentOrder.Service_Type__c == 'EOR') {
                if(shipmentOrder.Shipment_Value_USD__c  <= 10000) {
                    shipmentOrder.Bank_Fees__c = priceList.EOR_Bank_Fees__c;
                    shipmentOrder.Handling_and_Admin_Fee__c = priceList.EOR_Admin_Fee__c;
                }
                else {
                    shipmentOrder.Bank_Fees__c = priceList.EOR_Bank_Above_10000__c;
                    shipmentOrder.Handling_and_Admin_Fee__c = priceList.EOR_Admin_Above_10000__c;
                }
            }else{
                if(shipmentOrder.Shipment_Value_USD__c  <= 10000) {
                    shipmentOrder.Bank_Fees__c = priceList.Bank_Fees__c;
                    shipmentOrder.Handling_and_Admin_Fee__c = shipmentOrder.Who_arranges_International_courier__c == 'TecEx' ? priceList.Admin_When_Tecex_does_freight__c : priceList.Admin_Fee__c;
                }
                else {
                    shipmentOrder.Bank_Fees__c = priceList.Bank_Above_10000__c;
                    shipmentOrder.Handling_and_Admin_Fee__c = priceList.Admin_Above_10000__c;
                }
            }
        }

        shipmentOrder.EOR_Fee_new__c =  priceList.EOR_Fee__c;
        shipmentOrder.EOR_Min_USD_new__c = priceList.EOR_Min_USD__c;
        shipmentOrder.IOR_Fee_new__c = priceList.IOR_Fee__c;
        shipmentOrder.IOR_Min_USD__c = priceList.IOR_Min_Fee__c;
        shipmentOrder.On_Charge_Mark_up__c = priceList.On_Charge_Mark_up__c;
        shipmentOrder.Tax_Rate__c = priceList.Tax_Rate__c;
        // Not Found Tax and duty cost field on SO
        // shipmentOrder.Tax_and_Duty_Cost = ((shipmentOrder.Shipment_Value_USD__c +250) * shipmentOrder.Tax_Rate__c)*1.05;
        shipmentOrder.TecEx_Shipping_Fee_Markup__c = priceList.TecEx_Shipping_Fee_Markup__c;
        //added by swapnil
        shipmentOrder.Special_Client_Invoicing__c =  !isSORecordTypeIsZee(shipmentOrder.RecordTypeId) && priceList.Agreed_Pricing__c ? true : false;
        shipmentOrder.Forecast_Override__c = false;
        if(!isSORecordTypeIsZee(shipmentOrder.RecordTypeId) &&(shipmentOrder.Shipping_Status__c == 'Cost Estimate Abandoned' || shipmentOrder.Shipping_Status__c == 'Cost Estimate')&& shipmentOrder.Special_Client_Invoicing__c && priceList.Agreed_Pricing__c) {
            if(shipmentOrder.Service_Type__c == 'IOR') {
                if(priceList.Estimated_Customs_Clearance__c !=null)
                    shipmentOrder.Minimum_Clearance_Costs__c = priceList.Estimated_Customs_Clearance__c;
                if(priceList.Estimated_Import_License__c  !=null)
                    shipmentOrder.Minimum_License_Permit_Costs__c = priceList.Estimated_Import_License__c;
                if(priceList.Estimated_Customs_Handling__c !=null)
                    shipmentOrder.Minimum_Handling_Costs__c = priceList.Estimated_Customs_Handling__c;
                if(priceList.Estimated_Customs_Brokerage__c  !=null)
                    shipmentOrder.Minimum_Brokerage_Costs__c = priceList.Estimated_Customs_Brokerage__c;
                if(priceList.Miscellaneous_Fee__c !=null)
                    shipmentOrder.Miscellaneous_Fee_Name__c = priceList.Miscellaneous_Fee_Name__c;
                if( priceList.Miscellaneous_Fee__c  !=null)
                    shipmentOrder.Miscellaneous_Fee__c = priceList.Miscellaneous_Fee__c;
            }else if(shipmentOrder.Service_Type__c == 'EOR') {
                if(priceList.EOR_Clearance__c  !=null)
                    shipmentOrder.Minimum_Clearance_Costs__c = priceList.EOR_Clearance__c;
                if(priceList.EOR_Licenses_permits__c  !=null)
                    shipmentOrder.Minimum_License_Permit_Costs__c = priceList.EOR_Licenses_permits__c;
                if(priceList.EOR_Handling__c  !=null)
                    shipmentOrder.Minimum_Handling_Costs__c = priceList.EOR_Handling__c;
                if(priceList.EOR_Brokerage__c  !=null)
                    shipmentOrder.Minimum_Brokerage_Costs__c = priceList.EOR_Brokerage__c;
                if(priceList.Miscellaneous_Fee__c !=null)
                    shipmentOrder.Miscellaneous_Fee_Name__c = priceList.Miscellaneous_Fee_Name__c;
                if( priceList.Miscellaneous_Fee__c  !=null)
                    shipmentOrder.Miscellaneous_Fee__c = priceList.Miscellaneous_Fee__c;
            }

        }
        if(priceList.Agreed_Pricing__c || shipmentOrder.Special_Client_Invoicing__c) {shipmentOrder.Forecast_Override__c = true;}

    }
    public static Boolean isSOCloseDownConditionMatched(Shipment_Order__c so){
        return so.Closed_Down_Date__c == null &&
               (
            (
                ( isChanged(so,'Outstanding_Invoice_Value__c') &&
                  so.Outstanding_Invoice_Value__c < 1 &&
                  so.Final_Supplier_Invoice_Received__c &&
                  so.No_further_client_invoices_required__c &&
                  so.CI_Total__c > 0
                )
                ||
                ( isChanged(so,'Final_Supplier_Invoice_Received__c') &&
                  so.Final_Supplier_Invoice_Received__c &&
                  so.Outstanding_Invoice_Value__c < 1 &&
                  so.No_further_client_invoices_required__c &&
                  so.CI_Total__c > 0
                )
                ||
                ( isChanged(so,'No_further_client_invoices_required__c') &&
                  so.No_further_client_invoices_required__c &&
                  so.Outstanding_Invoice_Value__c < 1 &&
                  so.Final_Supplier_Invoice_Received__c &&
                  so.CI_Total__c > 0
                )
                ||
                (
                    isChanged(so,'Shipping_Status__c') &&
                    so.Outstanding_Invoice_Value__c < 1 &&
                    so.Final_Supplier_Invoice_Received__c &&
                    so.No_further_client_invoices_required__c &&
                    so.CI_Total__c > 0 )
            )
            &&
            (
                ( so.Customs_Clearance_Docs_Received_Date__c != null ||
                  so.Shipping_Status__c == 'Customs Clearance Docs Received' ||
                  so.Shipping_Status__c == 'Cancelled - With fees/costs' ||
                  so.Shipping_Status__c == 'Cancelled - No fees/costs' ||
                  so.Shipping_Status__c == 'Cost Estimate Abandoned' ||
                  so.Shipping_Status__c == 'Cost Estimate Rejected'
                )
            )
               );
    }
    public static String transformShippingStatus(String shippingStatus, Boolean isSOToSupplierSO){
        if(isSOToSupplierSO) {
            switch on shippingStatus {
                when 'Supplier Approved' {
                    return 'Customs Approved (Pending Payment)';
                }
                when 'Shipment Pack Submitted for Approval' {
                    return 'Complete Shipment Pack Submitted for Approval';
                }
            }
        }else{
            switch on shippingStatus {
                when 'Customs Approved (Pending Payment)' {
                    return 'Supplier Approved';
                }
                when 'Complete Shipment Pack Submitted for Approval' {
                    return 'Shipment Pack Submitted for Approval';
                }
            }
        }
        return shippingStatus;
    }
    public static void updateCPALastDate(Map<Id,CPA_v2_0__c> CPAToUpdate,List<Shipment_Order__c> shipmentOrders){
        update CPAToUpdate.values();
        //Fetching Related Records for COPY on SO Level.
        List<String> statusToConsider = new List<String> {'Hub Lead Time','Transit to Destination Lead Time','Customs Clearance Lead Time','Final Delivery Lead Time'};
        List<SO_Status_Description__c> soStatusDescList = [SELECT Id, CPA_v2_0__c, Status__c, Min_Days__c, Max_Days__c FROM SO_Status_Description__c WHERE Type__c = 'Lead Time' AND CPA_v2_0__c IN : CPAToUpdate.keySet() AND Status__c IN : statusToConsider AND Max_Days__c != null];
        Map<Id, List<SO_Status_Description__c> > cpaWithSOStaus = new Map<Id, List<SO_Status_Description__c> >();
        for(SO_Status_Description__c SOSD : soStatusDescList) {
            if(cpaWithSOStaus.containsKey(SOSD.CPA_v2_0__c)) {
                cpaWithSOStaus.get(SOSD.CPA_v2_0__c).add(SOSD);
            }else{
                cpaWithSOStaus.put(SOSD.CPA_v2_0__c, new List<SO_Status_Description__c> {SOSD});
            }
        }
        for(Shipment_Order__c so : shipmentOrders) {
            if(cpaWithSOStaus.containsKey(so.CPA_v2_0__c)) {
                for(SO_Status_Description__c sosd : cpaWithSOStaus.get(so.CPA_v2_0__c)) {
                    switch on sosd.Status__c {
                        when 'Hub Lead Time' {
                            // so.Hub_Lead_Time__c = String.valueOf(sosd.Max_Days__c);
                            so.Hub_Lead_Time_Decimal__c = sosd.Max_Days__c;
                        }
                        when 'Transit to Destination Lead Time' {
                            //so.Transit_to_Destination_Lead_Time__c = String.valueOf(sosd.Max_Days__c);
                            so.Transit_to_Destination_Lead_Time_Decimal__c= sosd.Max_Days__c;
                        }
                        when 'Customs Clearance Lead Time' {
                            //so.Customs_Clearance_Lead_Time__c = String.valueOf(sosd.Max_Days__c);
                            so.Customs_Clearance_Lead_Time_Decimal__c = sosd.Max_Days__c;
                        }
                        when 'Final Delivery Lead Time' {
                            //so.Final_Delivery_Lead_Time__c = String.valueOf(sosd.Max_Days__c);
                            so.Final_Delivery_Lead_Time_Decimal__c = sosd.Max_Days__c;
                        }
                    }
                }
            }else{
                /*so.Hub_Lead_Time__c = '0';
                   so.Transit_to_Destination_Lead_Time__c = '0';
                   so.Customs_Clearance_Lead_Time__c = '0';
                   so.Final_Delivery_Lead_Time__c = '0';*/
                so.Customs_Clearance_Lead_Time_Decimal__c = 0;
                so.Hub_Lead_Time_Decimal__c =0;
                so.Transit_to_Destination_Lead_Time_Decimal__c =0;
                so.Final_Delivery_Lead_Time_Decimal__c = 0;
            }
        }
    }
    public static void copyPartFieldsIntoSobject(SObject source,SObject target){
        List<String> fields = new List<String> {'of_Line_Items__c','of_Unique_Line_Items__c','Total_CIF_Duties__c','Total_FOB_Duties__c','Total_Line_Item_Extended_Value__c','Shipment_Value_USD__c','Recalculate_Taxes__c','Contains_Batteries_Auto__c','Contains_Refurbished_Goods__c','Prohibited_Manufacturer_Auto__c','Number_of_Batteries__c','Number_of_Wireless_Products__c','Restricted_parts__c','Contains_Encrypted_Goods_Auto__c','Contains_Wireless_Goods_Auto__c'};
        copyFieldsIntoSobject(source,target,fields);
    }
    public static void copyCostingFieldsIntoSobject(SObject source,SObject target){
        List<String> fields = new List<String> {'FC_Admin_Fee__c','FC_Bank_Fees__c','FC_Finance_Fee__c','FC_IOR_and_Import_Compliance_Fee_USD__c','FC_Insurance_Fee_USD__c','FC_International_Delivery_Fee__c','FC_Miscellaneous_Fee__c','FC_Recharge_Tax_and_Duty__c','FC_Total_Clearance_Costs__c','FC_Total_Customs_Brokerage_Cost__c','FC_Total_Handling_Costs__c','FC_Total_License_Cost__c','Actual_Admin_Fee__c','Actual_Bank_Fees__c','Actual_Finance_Fee__c','Actual_Total_IOR_EOR__c','Actual_Insurance_Fee_USD__c','Actual_International_Delivery_Fee__c','Actual_Miscellaneous_Fee__c','Actual_Total_Tax_and_Duty__c','Actual_Total_Clearance_Costs__c','Actual_Total_Customs_Brokerage_Cost__c','Actual_Total_Handling_Costs__c','Actual_Total_License_Cost__c','Actual_Duties_and_Taxes_Other__c','Actual_Taxes_Forex_Spread__c'};
        copyFieldsIntoSobject(source,target,fields);
    }
    public static void copyFieldsIntoSobject(SObject source,SObject target,List<String> fields){
        for(String field : fields)
            target.put(field,source.isSet(field) ? source.get(field) : null);
        target.put('CPA_Costings_Added__c', true);
        target.put('Recalculate_Costings__c',false);
    }
    private static void sendMail(String templateName,List<String> toAddresses,Shipment_order__c shipment){
        try{
            List<EmailTemplate> lstEmailTemplates = [SELECT Id, Body, Name, Subject FROM EmailTemplate WHERE Name =:templateName];
            if(!lstEmailTemplates.isEmpty() && !toAddresses.isEmpty()) {
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                mail.setTemplateId(lstEmailTemplates[0].Id);
                mail.setSaveAsActivity(false);
                mail.setTargetObjectId(shipment.Client_Contact_for_this_Shipment__c);
                if(templateName == 'VAT Team VAT Claiming Country' || templateName == 'VAT Team Possible VAT Recovery') {
                    mail.setTreatTargetObjectAsRecipient(false);
                }
                mail.setToAddresses(toAddresses);
                mail.setWhatId(shipment.Id);
                Messaging.SendEmailResult[] resultMail = Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
            }
        }catch(Exception e) {}
    }
    public static void setPrefferedOnRates(Set<Id> CPAChangeSOIds){
        if(!CPAChangeSOIds.isEmpty()) {
            List<Freight__c> freightToUpdate = [SELECT Id, Shipment_Order__r.CPA_v2_0__r.Restricted_Service_Type__c, SF_country__C, Con_country__c,Ship_From_Country__C FROM Freight__c WHERE Shipment_Order__c IN: CPAChangeSOIds];
            if(!freightToUpdate.isEmpty()) {
                CreateCourierratesV2.setPreferredOnRates(freightToUpdate);
            }
        }
    }

    private static Boolean isSORecordTypeIsZee(Id RecordTypeId){

        List<Id> zeeRecordTypes = new List<Id> {
            Schema.SObjectType.Shipment_Order__c.getRecordTypeInfosByDeveloperName().get('Zee_Cost_Estimate').getRecordTypeId(),
            Schema.SObjectType.Shipment_Order__c.getRecordTypeInfosByDeveloperName().get('Zee_Shipment_Order').getRecordTypeId()
        };

        if(zeeRecordTypes.contains(RecordTypeId)) {
            return true;
        }
        else{
            return false;
        }
    }
    private static void copyInvoiceSOMap(Map<Id, Shipment_Order__c> soMap, Map<Id, Shipment_Order__c> soMapFromInvoice){
        Shipment_Order__c shipmentOrder;
        for(Shipment_Order__c so : soMap.values()) {

            if(soMapFromInvoice.containsKey(so.Id)) {
                shipmentOrder = soMapFromInvoice.get(so.Id);
            }

            if(shipmentOrder != null) {

                so.CI_Admin_Fee__c =  shipmentOrder.CI_Admin_Fee__c;
                so.CI_Bank_Fees__c =  shipmentOrder.CI_Bank_Fees__c;
                so.CI_Cash_Outlay_Fee__c =  shipmentOrder.CI_Cash_Outlay_Fee__c;
                so.CI_Collection_Administration_Fee__c =  shipmentOrder.CI_Collection_Administration_Fee__c;
                so.CI_IOR_and_Import_Compliance_Fee_USD__c = shipmentOrder.CI_IOR_and_Import_Compliance_Fee_USD__c;
                so.CI_EOR_and_Export_Compliance_Fee_USD__c = shipmentOrder.CI_EOR_and_Export_Compliance_Fee_USD__c;
                so.CI_International_Delivery_Fee__c =  shipmentOrder.CI_International_Delivery_Fee__c;
                so.CI_Liability_Cover__c =  shipmentOrder.CI_Liability_Cover__c;
                so.CI_Miscellaneous_Fee__c =  shipmentOrder.CI_Miscellaneous_Fee__c;
                so.CI_Recharge_Tax_and_Duty__c =  shipmentOrder.CI_Recharge_Tax_and_Duty__c;
                so.CI_Total_Clearance_Costs__c =  shipmentOrder.CI_Total_Clearance_Costs__c;
                so.CI_Total_Customs_Brokerage_Cost__c = shipmentOrder.CI_Total_Customs_Brokerage_Cost__c;
                so.CI_Total_Handling_Cost__c = shipmentOrder.CI_Total_Handling_Cost__c;
                so.CI_Total_Licence_Cost__c = shipmentOrder.CI_Total_Licence_Cost__c;
                so.Revenue_Ad_Hoc_Fees__c =  shipmentOrder.Revenue_Ad_Hoc_Fees__c;
                so.Revenue_Bank_Charges_Recovered__c =  shipmentOrder.Revenue_Bank_Charges_Recovered__c;
                so.Revenue_Exporter_of_Record__c = shipmentOrder.Revenue_Exporter_of_Record__c;
                so.Revenue_Financing_and_Insurance__c =  shipmentOrder.Revenue_Financing_and_Insurance__c;
                so.Revenue_Importer_of_Record__c =  shipmentOrder.Revenue_Importer_of_Record__c;
                so.Revenue_Insurance_Fees__c =  shipmentOrder.Revenue_Insurance_Fees__c;
                so.Revenue_On_Charges__c =  shipmentOrder.Revenue_On_Charges__c;
                so.Revenue_Shipping_Insurance__c =  shipmentOrder.Revenue_Shipping_Insurance__c;
                so.Revenue_Tax__c =  shipmentOrder.Revenue_Tax__c;
                so.Total_Value_Applied_on_Customer_Invoice__c =  shipmentOrder.Total_Value_Applied_on_Customer_Invoice__c;
            }
        }

    }
}