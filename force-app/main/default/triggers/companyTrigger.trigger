trigger companyTrigger on Company__c (before insert, after update, before update) {
    if(trigger.isbefore && trigger.isinsert){
        String profileName = [SELECT Id, Name FROM Profile WHERE ID =: userinfo.getProfileId() LIMIT 1].Name;
        //  String profileName = [select Id, Name from Profile where Id in (select ProfileId from User where Id = :trigger.new[0].CreatedById)].Name;
        if(profileName == 'TecEx Branch Manager' || profileName == 'TecEx Junior Sales Developer'){
            system.debug('Dayne kennedy');
            List<String> statusToExclude = new List<String>{'Meeting Held', 'Meeting Held Contact at Future Date', 'Contact at Future Date', 'Meeting Result = Contact at Future Date', 'Contract Pending', 'Qualified', 'Contract Signed', 'Client Signed', 'Subsidiary Signed - Do Not Contact'};
                User user = [SELECT ID, Number_of_Companies_to_be_Allocated__c FROM User WHERE ID =: userinfo.getUserId() LIMIT 1];
            Map<Id, AggregateResult> userToCompanyCreatedMap = new Map<Id,AggregateResult>([Select ownerId Id, count(Id) ownerIds from Company__c WHERE Highest_Lead_Status__c NOT IN : statusToExclude AND Exhausted__c = false Group By OwnerId]);
            
            Decimal companyCreationLimit = user.Number_of_Companies_to_be_Allocated__c != null ? user.Number_of_Companies_to_be_Allocated__c: 0;
            Decimal companiesCreated = userToCompanyCreatedMap.containsKey(user.Id) ? (Double)userToCompanyCreatedMap.get(user.Id).get('ownerIds') : 0;
            system.debug('companiesCreated :'+companiesCreated);
                        system.debug('companyCreationLimit :'+companyCreationLimit);
            for(Company__c com : trigger.new){
                if(companyCreationLimit <= companiesCreated){
                    com.adderror('Can not insert record, limit is exceeded');
                }
                com.Allocation_Date__c = Date.today();
            } 
        }else{
            for(Company__c com : trigger.new){
                com.Allocation_Date__c = Date.today();
            }
        }
        
    }
    
    if(trigger.isbefore && trigger.isupdate){
        String profileName = [SELECT Id, Name FROM Profile WHERE ID =: userinfo.getProfileId() LIMIT 1].Name;
        List<String> statusToExclude = new List<String>{'Meeting Held', 'Meeting Held Contact at Future Date', 'Contact at Future Date', 'Meeting Result = Contact at Future Date', 'Contract Pending', 'Qualified', 'Contract Signed', 'Client Signed', 'Subsidiary Signed - Do Not Contact'};
            Map<Id, User> userList = new Map<Id, User>([SELECT ID, Number_of_Companies_to_be_Allocated__c FROM User]);
        Map<Id, AggregateResult> userToCompanyOwnerId = new Map<Id,AggregateResult>([Select OwnerId Id, count(Id) ownerIds from Company__c WHERE Highest_Lead_Status__c NOT IN : statusToExclude AND Exhausted__c = false Group By OwnerId]);
        
        for(Company__c company : trigger.new){
            if(company.ownerId != trigger.oldmap.get(company.Id).ownerId){
                if(profileName == 'System Administrator' || profileName == 'TecEx IP & Automation Team' || userinfo.getName() == 'Jacques Booysen' || userinfo.getName() == 'Patrick Buck' || NewCompanyLeadsStructureCtrl.changeCompanyOwner){
                    system.debug('OwnerId changed');
                    User us = userList.get(company.ownerId);
                    Decimal companyOwnedLimit = us.Number_of_Companies_to_be_Allocated__c != null ? us.Number_of_Companies_to_be_Allocated__c : 0;
                    Decimal companyOwned = userToCompanyOwnerId.containsKey(us.Id) ? (Double)userToCompanyOwnerId.get(us.Id).get('ownerIds') : 0;
                    system.debug('companyOwnedLimit :'+companyOwnedLimit);
                    system.debug('companyOwned :'+companyOwned);
                    if(companyOwnedLimit <= companyOwned){
                        company.adderror('Company Owned limit is exhausted.');
                    }
                }else{
                    if(company.ownerId != trigger.oldmap.get(company.Id).ownerId){
                        trigger.new[0].adderror('Access Denied, Can not change Owner of Company. Please go with approval process.');
                    }
                }
            }
            
            if(company.Request_to_change_owner__c && company.Change_Owner_Approval_Status__c == null ){
                User us = userList.get(Userinfo.getUserId());
                system.debug('Request to owner Checked true');    
                Decimal companyOwnedLimit = us.Number_of_Companies_to_be_Allocated__c != null ? us.Number_of_Companies_to_be_Allocated__c : 0;
                Decimal companyOwned = userToCompanyOwnerId.containsKey(us.Id) ? (Double)userToCompanyOwnerId.get(us.Id).get('ownerIds') : 0;
                system.debug('companyOwnedLimit :'+companyOwnedLimit);
                system.debug('companyOwned :'+companyOwned);
                if(companyOwnedLimit <= companyOwned){
                    company.adderror('Company Owned limit is exhausted.');
                }
            }
            if(company.Highest_Lead_Status__c != trigger.oldmap.get(company.Id).Highest_Lead_Status__c){
                company.Lead_Status_Change_Date__c = Date.today();
                system.debug('company.Lead_Status_Change_Date__c : '+company.Lead_Status_Change_Date__c);
            }
            if(company.Change_Owner_Approval_Status__c == 'Approved'){
                company.OwnerId = company.SubmitterId__c;
                company.Submitter_Email__c  = null;
                company.Change_Owner_Approval_Status__c   = null;
                
            }
            if(company.OwnerId != trigger.oldmap.get(company.Id).OwnerId){
                company.Allocation_Date__c = Date.today();
            }
            system.debug('company : '+company);
        }
        
    }
    
    if(trigger.isafter && trigger.isupdate){
        List<Company__c> companiesToUpdate = new List<Company__c>();
      
        
        for(Company__c company : trigger.new){
            
            if(company.Request_to_change_owner__c && company.Change_Owner_Approval_Status__c == null){
                Approval.ProcessSubmitRequest  approvalProcess = new Approval.ProcessSubmitRequest();
                approvalProcess.setObjectId(company.id);
                
                Approval.ProcessResult result =  Approval.process(approvalProcess);
                Company__c companyToUpdate = new Company__c(Id = company.Id, SubmitterId__c = company.LastModifiedById);
                companiesToUpdate.add(companyToUpdate);
            }
                        
        }
        if(companiesToUpdate.size() > 0){
            system.debug('Company Update');
            update companiesToUpdate;
        }
       
        
    }
    
    
}