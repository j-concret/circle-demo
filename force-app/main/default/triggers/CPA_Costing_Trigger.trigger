trigger CPA_Costing_Trigger on CPA_Costing__c (before insert, before update, before delete, after insert, after update, after delete, after undelete)
{
    TriggerFactory.createHandler(CPA_Costing__c.sobjectType);
}