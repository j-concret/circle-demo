trigger attachCostestimate on Shipment_Order__c (before Insert, after Insert,before update,after update) {

    if(RecusrsionHandler.taskTriggerRun) return;

    if(Trigger.isBefore && Trigger.isInsert) {
        Map<Id,List<Shipment_Order__c> > accShipments = new Map<Id,List<Shipment_Order__c> >();
        for(Shipment_Order__c so : Trigger.New) {
            if(so.Account__c != null) {
                if(accShipments.containsKey(so.Account__c))
                    accShipments.get(so.Account__c).add(so);
                else
                    accShipments.put(so.Account__c,new List<Shipment_Order__c> {so});
            }
        }
        if(!accShipments.isEmpty()) {
            for(Account acc : [SELECT Id,Staging_Warehouse_Preferred_Method__c FROM Account where Id IN:accShipments.keySet()]) {
                for(Shipment_Order__c so : accShipments.get(acc.Id)) {
                    so.Goods_to_be_sent_to_Staging_Warehouse__c = acc.Staging_Warehouse_Preferred_Method__c;
                }
            }
        }
    }

    if(Trigger.isBefore && Trigger.isUpdate) {

        Set <Id> accountIds = new Set<Id>();
        Set <Id> soIds = new Set<Id>();
        Set<Id> invoiceSoIds = new Set<Id>();

        List<Shipment_Order__c> soWithoutCPA = new List<Shipment_Order__c>();
        List<Invoice_New__c> firstInvoiceCreate = new List<Invoice_New__c>();
        List<Invoice_New__c> updatePOD = new List<Invoice_New__c>();
        Boolean createInvoice = false;
        for(Shipment_Order__c so : Trigger.new) {

            if(trigger.oldMap.get(SO.id).Populate_Invoice__c != SO.Populate_Invoice__c && SO.Populate_Invoice__c && !SO.First_Invoice_Created__c) {
                createInvoice = true;
            }

            if(so.In_Transit_Start_Date__c == NULL && ( so.Shipping_Status__c == 'In Transit to Hub' || so.Shipping_Status__c == 'Arrived at Hub' || so.Shipping_Status__c == 'In transit to country')) {
                so.In_Transit_Start_Date__c = system.now();

                DateTime dT = System.now();
                Date todaysDate = date.newinstance(dT.year(), dT.month(), dT.day());
                so.In_Transit_Date__c = todaysDate;

            }
            if(so.CPA_v2_0__c == null && !so.CPA2_override__c ) {
                soWithoutCPA.add(so);
            }
            if((trigger.oldMap.get(SO.id).Roll_Up_Costings__c != SO.Roll_Up_Costings__c && SO.Roll_Up_Costings__c) || (trigger.oldMap.get(SO.id).Roll_Up_Costings_A__c != SO.Roll_Up_Costings_A__c && SO.Roll_Up_Costings_A__c)) {
                soIds.add(SO.id);
            }

            if(trigger.oldMap.get(SO.id).Populate_Invoice__c != SO.Populate_Invoice__c && SO.Populate_Invoice__c && !SO.First_Invoice_Created__c) {
                accountIds.add(SO.Account__c);
            }

            if(trigger.oldMap.get(SO.id).POD_Date__c != SO.POD_Date__c || trigger.oldMap.get(SO.id).Cancelled_Date__c != SO.Cancelled_Date__c) {
                invoiceSoIds.add(SO.Id);
            }

        }

        //find cpa for all shipment order which dont have cpa
        if(!soWithoutCPA.isEmpty()) {
            FindCPA2.findCPA2(soWithoutCPA);
        }

        Map<String, Decimal> conversionRate = new Map<String, Decimal>();
        if(createInvoice) {
            for (Currency_Management2__c curr: [SELECT Id, Name, Conversion_Rate__c, Currency__c, ISO_Code__c FROM Currency_Management2__c]) {
                conversionRate.put(curr.Name,curr.Conversion_Rate__c );
            }
        }
        List<Shipment_Order__c> soToCalculateETA = new List<Shipment_Order__c>();

        Map<Id,List<AggregateResult> > soAggregateResult = new Map<Id,List<AggregateResult> >();

        Map<Id, Account> accountMap = new Map<Id, Account>();
        Map<Id, List<Invoice_New__c> > invoiceMap = new Map<Id, List<Invoice_New__c> >();

        if(!accountIds.isEmpty()) {
            accountMap.putAll([SELECT Id, Name, RecordTypeId, IOR_Payment_Terms__c, Default_invoicing_currency__c, Penalty_Daily_Percentage__c, Penalty_Status__c,Invoice_Timing__c, Invoicing_Term_Parameters__c FROM Account WHERE Id IN: accountIds]);
        }

        if(!invoiceSoIds.isEmpty()) {
            for(Invoice_New__c inv : [SELECT Id, Name, RecordTypeId, POD_Date_New__c, Shipment_Order__c, Penalty_Daily_Percentage__c, Penalty_Status__c, Account__c FROM Invoice_New__c Where Shipment_Order__c IN: invoiceSoIds ]) {
                if(invoiceMap.containsKey(inv.Shipment_Order__c))
                    invoiceMap.get(inv.Shipment_Order__c).add(inv);
                else
                    invoiceMap.put(inv.Shipment_Order__c,new List<Invoice_New__c> {inv});
            }
        }

        if(!soIds.isEmpty()) {

            for(AggregateResult agg : [SELECT Shipment_Order_Old__c,Sum(Invoice_amount_USD_New__c) invAmount, Sum(Amount_in_USD__c) fcAmount,Cost_Category__c costCat, Tax_Sub_Category__c subcat FROM CPA_Costing__c WHERE Shipment_Order_Old__c IN:soIds And RecordTypeID =: Schema.Sobjecttype.CPA_Costing__c.getRecordTypeInfosByDeveloperName().get('Display').getRecordTypeId() Group by Cost_Category__c,Tax_Sub_Category__c,Shipment_Order_Old__c]) {

                Id shipId = String.valueOf(agg.get('Shipment_Order_Old__c'));
                if(soAggregateResult.containsKey(shipId)) {
                    soAggregateResult.get(shipId).add(agg);
                }else{
                    soAggregateResult.put(shipId,new List<AggregateResult> {agg});
                }
            }
        }

        Map<Id,CPA_v2_0__c> CPAToUpdate = new Map<Id,CPA_v2_0__c>();
        //Map<Id, Id> mapSoToCPA = new Map<Id, Id>();
        for(Shipment_Order__c SO : Trigger.new) {
            if(So.CPA_v2_0__c != null && so.CPA_v2_0__c != Trigger.oldMap.get(SO.Id).CPA_v2_0__c && !CPAToUpdate.containsKey(So.CPA_v2_0__c)) {
                CPAToUpdate.put(So.CPA_v2_0__c, new CPA_v2_0__c(Id = So.CPA_v2_0__c, Last_Shipment_Date__c = system.today()));
                //mapSoToCPA.put(SO.Id, So.CPA_v2_0__c);
            }
            // Populate Invoice
            if(trigger.oldMap.get(SO.id).Populate_Invoice__c != SO.Populate_Invoice__c && SO.Populate_Invoice__c && !SO.First_Invoice_Created__c) {

                Integer paymentTerms = accountMap.get(SO.Account__c).IOR_Payment_Terms__c.intValue();
                Date podDate = SO.POD_Date__c;
                String invoiceCurrency = accountMap.get(SO.Account__c).Default_invoicing_currency__c;
                Decimal rate = conversionRate.get(invoiceCurrency);

                Decimal adminFee = SO.Handling_and_Admin_Fee__c == NULL ? 0 : SO.Handling_and_Admin_Fee__c / rate;

                Invoice_New__c firstInvoice = new Invoice_New__c(
                    Account__c = SO.Account__c,
                    Admin_Fees__c = adminFee,
                    Bank_Fee__c = SO.Bank_Fees__c == NULL ? 0 : SO.Bank_Fees__c / rate,
                    Conversion_Rate__c = rate,
                    Customs_Brokerage_Fees__c = SO.Total_Customs_Brokerage_Cost1__c == NULL ? 0 : SO.Total_Customs_Brokerage_Cost1__c / rate,
                    Customs_Clearance_Fees__c = SO.Total_clearance_Costs__c == NULL ? 0 : SO.Total_clearance_Costs__c / rate,
                    Customs_Handling_Fees__c = SO.Recharge_Handling_Costs__c == NULL ? 0 : SO.Recharge_Handling_Costs__c / rate,
                    Customs_License_In__c = SO.Total_License_Cost__c == NULL ? 0 : SO.Total_License_Cost__c / rate,
                    EOR_Fees__c = SO.EOR_and_Export_Compliance_Fee_USD__c == NULL ? 0 : SO.EOR_and_Export_Compliance_Fee_USD__c / rate,
                    IOR_Fees__c =SO.IOR_FEE_USD__c == NULL ? 0 : SO.IOR_FEE_USD__c / rate,
                    International_Freight_Fee__c = SO.International_Delivery_Fee__c == NULL ? 0 : SO.International_Delivery_Fee__c / rate,
                    Invoice_Amount_Local_Currency__c =  accountMap.get(SO.Account__c).Invoice_Timing__c == 'Upfront invoicing' && accountMap.get(SO.Account__c).Invoicing_Term_Parameters__c == 'No Terms' ? SO.Total_Invoice_Amount__c/rate : (SO.Total_Invoice_Amount__c + so.Potential_Cash_Outlay_Fee__c)/rate,
                    Invoice_Currency__c = invoiceCurrency,
                    Invoice_Date__c = date.today(),
                    Invoice_Type__c = 'Invoice',
                    Invoice_amount_USD__c =  accountMap.get(SO.Account__c).Invoice_Timing__c == 'Upfront invoicing' && accountMap.get(SO.Account__c).Invoicing_Term_Parameters__c == 'No Terms' ? SO.Total_Invoice_Amount__c : SO.Total_Invoice_Amount__c + so.Potential_Cash_Outlay_Fee__c,
                    Liability_Cover_Fee__c = SO.Insurance_Fee_USD__c == NULL ? 0 : SO.Insurance_Fee_USD__c / rate,
                    Miscellaneous_Fee_Name__c = SO.Miscellaneous_Fee_Name__c,
                    Miscellaneous_Fee__c = SO.Miscellaneous_Fee__c == NULL ? 0 : SO.Miscellaneous_Fee__c / rate,
                    Recharge_Tax_and_Duty_Other__c = SO.Recharge_Tax_and_Duty_Other__c == NULL ? 0 : SO.Recharge_Tax_and_Duty_Other__c / rate,
                    Penalty_Status__c = accountMap.get(SO.Account__c).Penalty_Status__c,
                    Cash_Outlay_Fee__c = accountMap.get(SO.Account__c).Invoice_Timing__c == 'Upfront invoicing' && accountMap.get(SO.Account__c).Invoicing_Term_Parameters__c == 'No Terms' ? 0 : so.Potential_Cash_Outlay_Fee__c/rate,
                    Shipment_Order__c = so.Id,
                    POD_Date_New__c = so.POD_Date__c,
                    Enterprise_Billing__c = so.Enterprise_Billing_Id__c,
                    Penalty_Daily_Percentage__c = accountMap.get(SO.Account__c).Penalty_Daily_Percentage__c,
                    RecordTypeId = Schema.Sobjecttype.Invoice_New__c.getRecordTypeInfosByDeveloperName().get('Client').getRecordTypeId(),
                    Taxes_and_Duties__c = SO.Recharge_Tax_and_Duty__c == NULL ? 0 : SO.Recharge_Tax_and_Duty__c / rate);

                firstInvoiceCreate.add(firstInvoice);
                so.First_Invoice_Created__c = TRUE;
            }
            // Cancelled Date change
            if(invoiceMap.containsKey(SO.Id) && trigger.oldMap.get(SO.id).Cancelled_Date__c != SO.Cancelled_Date__c) {
                for (Invoice_New__c inv: invoiceMap.get(SO.Id)) {
                    inv.POD_Date_New__c = so.Cancelled_Date__c;
                    updatePOD.add(inv);
                }
            }
            // POD Changes
            else if(invoiceMap.containsKey(SO.Id) && SO.Cancelled_Date__c == null && trigger.oldMap.get(SO.id).POD_Date__c != SO.POD_Date__c) {
                for (Invoice_New__c inv: invoiceMap.get(SO.Id)) {
                    inv.POD_Date_New__c = so.POD_Date__c;
                    updatePOD.add(inv);
                }
            }

            if(soAggregateResult.containsKey(SO.Id)) {

                decimal FCAdminFee = 0.0;
                decimal FCBankFee = 0.0;
                decimal FCFinanceFee = 0.0;
                decimal FCIORandImport = 0.0;
                decimal FCINsurance = 0.0;
                decimal FCInternationsldeliveryFee = 0.0;
                decimal FCMiselaneousFee = 0.0;
                decimal FCRechatgeTaxandDuties = 0.0;
                decimal FCTotClearence = 0.0;
                decimal FCTotCustomsBrokerage = 0.0;
                decimal FCTotHandlingCost = 0.0;
                decimal FCTotLicenseCost = 0.0;
                decimal FCTAXOther = 0.0;
                decimal FCTAXForex = 0.0;

                decimal ACAdminFee = 0.0;
                decimal ACBankFee = 0.0;
                decimal ACFinanceFee = 0.0;
                decimal ACIORandImport = 0.0;
                decimal ACINsurance = 0.0;
                decimal ACInternationsldeliveryFee = 0.0;
                decimal ACMiselaneousFee = 0.0;
                decimal ACRechatgeTaxandDuties = 0.0;
                decimal ACTotClearence = 0.0;
                decimal ACTotCustomsBrokerage = 0.0;
                decimal ACTotHandlingCost = 0.0;
                decimal ACTotLicenseCost = 0.0;
                decimal ACTAXOther = 0.0;
                decimal ACTAXForex = 0.0;

                for(AggregateResult a : soAggregateResult.get(SO.Id)) {

                    if(a.get('costCat') == 'Admin' && a.get('subcat') == NULL) {
                        FCAdminFee = (Decimal) a.get('fcAmount');
                        ACAdminFee = (Decimal) a.get('invAmount');
                    }
                    if(a.get('costCat') == 'Bank' && a.get('subcat') == NULL) {
                        FCBankFee = (Decimal) a.get('fcAmount');
                        ACBankFee = (Decimal) a.get('invAmount');
                    }
                    if(a.get('costCat') == 'Finance' && a.get('subcat') == NULL) {
                        FCFinanceFee = (Decimal) a.get('fcAmount');
                        ACFinanceFee = (Decimal) a.get('invAmount');
                    }
                    if(a.get('costCat') == 'IOR Cost' && a.get('subcat') == NULL) {
                        FCIORandImport = (Decimal) a.get('fcAmount');
                        ACIORandImport = (Decimal) a.get('invAmount');
                    }
                    if(a.get('costCat') == 'Insurance' && a.get('subcat') == NULL) {
                        FCINsurance = (Decimal) a.get('fcAmount');
                        ACINsurance = (Decimal) a.get('invAmount');
                    }
                    if(a.get('costCat') == 'International freight' && a.get('subcat') == NULL) {
                        FCInternationsldeliveryFee = (Decimal) a.get('fcAmount');
                        ACInternationsldeliveryFee = (Decimal)a.get('invAmount');
                    }
                    if(a.get('costCat') == 'Miscellaneous' && a.get('subcat') == NULL) {
                        FCMiselaneousFee = (Decimal) a.get('fcAmount');
                        ACMiselaneousFee = (Decimal) a.get('invAmount');
                    }
                    if(a.get('costCat') == 'Tax' && a.get('subcat') == NULL) {
                        FCRechatgeTaxandDuties = (Decimal) a.get('fcAmount');
                        ACRechatgeTaxandDuties = (Decimal) a.get('invAmount');
                    }
                    if(a.get('costCat') == 'Clearance' && a.get('subcat') == NULL) {
                        FCTotClearence = (Decimal) a.get('fcAmount');
                        ACTotClearence = (Decimal) a.get('invAmount');
                    }
                    if(a.get('costCat') == 'Brokerage' && a.get('subcat') == NULL) {
                        FCTotCustomsBrokerage = (Decimal) a.get('fcAmount');
                        ACTotCustomsBrokerage = (Decimal) a.get('invAmount');
                    }
                    if(a.get('costCat') == 'Handling' && a.get('subcat') == NULL) {
                        FCTotHandlingCost = (Decimal) a.get('fcAmount');
                        ACTotHandlingCost = (Decimal) a.get('invAmount');
                    }
                    if(a.get('costCat') == 'Licenses' && a.get('subcat') == NULL) {
                        FCTotLicenseCost = (Decimal) a.get('fcAmount');
                        ACTotLicenseCost = (Decimal) a.get('invAmount');
                    }
                    if(a.get('costCat') == 'Tax' && a.get('subcat') == 'Duties and Taxes Other') {
                        FCTAXOther = (Decimal) a.get('fcAmount');
                        ACTAXOther = (Decimal) a.get('invAmount');
                    }
                    if(a.get('costCat') == 'Tax' && a.get('subcat') == 'Tax Forex Spread') {
                        FCTAXForex = (Decimal) a.get('fcAmount');
                        ACTAXForex = (Decimal) a.get('invAmount');
                    }
                }

                SO.FC_Admin_Fee__c=FCAdminFee;
                SO.FC_Bank_Fees__c=FCBankFee;
                SO.FC_Finance_Fee__c=FCFinanceFee;
                SO.FC_IOR_and_Import_Compliance_Fee_USD__c=FCIORandImport;
                SO.FC_Insurance_Fee_USD__c=FCINsurance;
                SO.FC_International_Delivery_Fee__c=FCInternationsldeliveryFee;
                SO.FC_Miscellaneous_Fee__c=FCMiselaneousFee;
                SO.FC_Recharge_Tax_and_Duty__c=FCRechatgeTaxandDuties;
                SO.FC_Total_Clearance_Costs__c=FCTotClearence;
                SO.FC_Total_Customs_Brokerage_Cost__c=FCTotCustomsBrokerage;
                SO.FC_Total_Handling_Costs__c=FCTotHandlingCost;
                SO.FC_Total_License_Cost__c=FCTotLicenseCost;

                if(trigger.oldMap.get(SO.id).Roll_Up_Costings_A__c != SO.Roll_Up_Costings_A__c && SO.Roll_Up_Costings_A__c) {
                    SO.Actual_Admin_Fee__c=ACAdminFee;
                    SO.Actual_Bank_Fees__c=ACBankFee;
                    SO.Actual_Total_Customs_Brokerage_Cost__c=ACTotCustomsBrokerage;
                    SO.Actual_Total_Clearance_Costs__c=ACTotClearence;
                    SO.Actual_Total_Tax_and_Duty__c=ACRechatgeTaxandDuties;
                    SO.Actual_Finance_Fee__c=ACFinanceFee;
                    SO.Actual_Total_Handling_Costs__c=ACTotHandlingCost;
                    SO.Actual_Insurance_Fee_USD__c=ACINsurance;
                    SO.Actual_Total_IOR_EOR__c=ACIORandImport;
                    SO.Actual_International_Delivery_Fee__c=ACInternationsldeliveryFee;
                    SO.Actual_Miscellaneous_Fee__c=ACMiselaneousFee;
                    SO.Actual_Total_License_Cost__c=ACTotLicenseCost;
                    SO.Actual_Duties_and_Taxes_Other__c=ACTAXOther;
                    SO.Actual_Taxes_Forex_Spread__c=ACTAXForex;
                    SO.Roll_Up_Costings_A__c = false;
                }else
                    SO.Roll_Up_Costings__c = false;
            }
            if(SO.Mapped_Shipping_status__c != '' && SO.Mapped_Shipping_status__c != Trigger.oldMap.get(so.Id).Mapped_Shipping_status__c && (SO.Mapped_Shipping_status__c == 'Quote Created' || SO.Mapped_Shipping_status__c == 'Compliance Pending')) {
                soToCalculateETA.add(so);
            }
        }

        if(!CPAToUpdate.isEmpty()) {
            update CPAToUpdate.values();

            //Fetching Related Records for COPY on SO Level.
            List<String> statusToConsider = new List<String> {'Hub Lead Time','Transit to Destination Lead Time','Customs Clearance Lead Time','Final Delivery Lead Time'};
            List<SO_Status_Description__c> soStatusDescList = [SELECT Id, CPA_v2_0__c, Status__c, Min_Days__c, Max_Days__c FROM SO_Status_Description__c WHERE Type__c = 'Lead Time' AND CPA_v2_0__c IN :CPAToUpdate.keySet() AND Status__c IN :statusToConsider AND Max_Days__c != NULL];

            Map<Id, List<SO_Status_Description__c> > cpaWithSOStaus = new Map<Id, List<SO_Status_Description__c> >();
            for(SO_Status_Description__c SOSD : soStatusDescList) {
                if(cpaWithSOStaus.containsKey(SOSD.CPA_v2_0__c)) {
                    cpaWithSOStaus.get(SOSD.CPA_v2_0__c).add(SOSD);
                }else{
                    cpaWithSOStaus.put(SOSD.CPA_v2_0__c, new List<SO_Status_Description__c> {SOSD});
                }
            }

            for(Shipment_Order__c so :Trigger.new) {
                if(cpaWithSOStaus.containsKey(so.CPA_v2_0__c)) {

                    for(SO_Status_Description__c sosd : cpaWithSOStaus.get(so.CPA_v2_0__c)) {
                        switch on sosd.Status__c {
                            when 'Hub Lead Time' {
                                so.Hub_Lead_Time__c = String.valueOf(sosd.Max_Days__c);
                            }
                            when 'Transit to Destination Lead Time' {
                                so.Transit_to_Destination_Lead_Time__c = String.valueOf(sosd.Max_Days__c);
                            }
                            when 'Customs Clearance Lead Time' {
                                so.Customs_Clearance_Lead_Time__c = String.valueOf(sosd.Max_Days__c);
                            }
                            when 'Final Delivery Lead Time' {
                                so.Final_Delivery_Lead_Time__c = String.valueOf(sosd.Max_Days__c);
                            }
                        }
                    }
                }else{
                    so.Hub_Lead_Time__c = '0';
                    so.Transit_to_Destination_Lead_Time__c = '0';
                    so.Customs_Clearance_Lead_Time__c = '0';
                    so.Final_Delivery_Lead_Time__c = '0';
                }
            }
        }

        if(!firstInvoiceCreate.isEmpty()) {
            insert firstInvoiceCreate;
        }

        if(!updatePOD.isEmpty()) {
            update updatePOD;
        }
        if(!soToCalculateETA.isEmpty()) {
            ETACalculation.processQuoteETA(soToCalculateETA);
        }
    }

    if(Trigger.isAfter && Trigger.isUpdate) {

        List<Id> soToCalculateETA = new List<Id>();
        List<Id> calculateTaxesList = new List<Id>();

        Set<String> mappedStatusAllowed = new Set<String> {'Approved to Ship','Compliance Pending','In-Transit','Arrived in Country','Cleared Customs','Final Delivery in Transit','Delivered'};

        Map<String,Shipment_order__C> SOlist11 = new Map<String,Shipment_order__C>();
        Map<String,Shipment_order__C> SOlist21 = new Map<String,Shipment_order__C>();
        Map<String,Shipment_order__C> SOlist31= new Map<String,Shipment_order__C>();
        List<Id> soidList = new List<Id>();
        Set<Id> CPAChangeSOIds = new Set<Id>();

        for(shipment_order__c so : Trigger.new) {

            if(so.Shipping_Status__c !='Shipment Abandoned' && so.Shipping_Status__c !='Cost Estimate Abandoned' && so.Shipping_Status__c !='Roll-Out') {
                soidList.add(so.Id);
            }

            // FOR ETA calculation
            if(so.cost_Estimate_acceptance_date__c != NULL && so.Mapped_Shipping_status__c != '' && mappedStatusAllowed.contains(so.Mapped_Shipping_status__c)) {
                soToCalculateETA.add(so.Id);
            }

            if(trigger.oldMap.get(So.id).Shipping_status__c != So.Shipping_status__c  &&
               So.Shipping_status__c=='Client Approved to ship' &&
               trigger.oldMap.get(So.id).mapped_shipping_status__c == 'Compliance Pending') {
                SOlist11.put(So.Id,so);
            }

            //PB Automatically calculate taxes node
            if(((SO.Shipping_Status__c == 'Cost Estimate' || SO.Shipping_Status__c == 'Shipment Pending') && !SO.Taxes_Calculated__c) || ((SO.CPA_V2_0__c != Trigger.oldMap.get(SO.Id).CPA_V2_0__c ||SO.Invoice_Timing__c != Trigger.oldMap.get(SO.Id).Invoice_Timing__c || SO.Service_Type__c != Trigger.oldMap.get(SO.Id).Service_Type__c || SO.Total_CIF_Duties__c != Trigger.oldMap.get(SO.Id).Total_CIF_Duties__c || SO.Total_FOB_Duties__c != Trigger.oldMap.get(SO.Id).Total_FOB_Duties__c || SO.Shipment_Value_USD__c != Trigger.oldMap.get(SO.Id).Shipment_Value_USD__c  || SO.of_Line_Items__c != Trigger.oldMap.get(SO.Id).of_Line_Items__c || (SO.of_Line_Items__c == Trigger.oldMap.get(SO.Id).of_Line_Items__c && SO.Total_Line_Item_Extended_Value__c != Trigger.oldMap.get(SO.Id).Total_Line_Item_Extended_Value__c) || SO.Recalculate_Taxes__c || SO.Client_Actual_Freight_Value__c != Trigger.oldMap.get(SO.Id).Client_Actual_Freight_Value__c || SO.Client_Actual_Insurance_Value__c != Trigger.oldMap.get(SO.Id).Client_Actual_Insurance_Value__c ||(SO.Ship_From_Country__c != null && SO.Ship_From_Country__c != Trigger.oldMap.get(SO.Id).Ship_From_Country__c)) && !SO.Shipping_Status__c.contains('Abandoned') && !SO.Closed_Down_SO__c && SO.Shipping_Status__c != 'Roll-Out')|| (SO.Recalculate_Taxes__c && !SO.Closed_Down_SO__c && SO.Override_Calculated_Taxes__c)) {
            
                calculateTaxesList.add(SO.Id);
            }

            if(trigger.oldMap.get(So.id).Shipping_status__c != So.Shipping_status__c  &&
               So.Shipping_status__c == 'Cost Estimate' &&
               (trigger.oldMap.get(So.id).Shipping_status__c == 'Shipment Abandoned' ||
                trigger.oldMap.get(So.id).Shipping_status__c == 'Cost Estimate Abandoned')) {
                CreateSoLogonStatusChange.isSOLogCreated = true;
            }

            if(trigger.oldMap.get(So.id).Shipping_status__c != So.Shipping_status__c  &&
               So.Shipping_status__c == 'Shipment Pending' &&
               (trigger.oldMap.get(So.id).Shipping_status__c == 'Shipment Abandoned' ||
                trigger.oldMap.get(So.id).Shipping_status__c == 'Cost Estimate Abandoned' ||
                trigger.oldMap.get(So.id).Shipping_status__c == 'Cost Estimate')) {
                SOlist31.put(So.Id,so);
            }

            if(so.Cost_Estimate_Accepted__c == true && so.Cost_Estimate_attached__c == false && attachCostEstimatehelperTOtrigger.firstRun ) {
                try{
                    attachCostEstimatehelperTOtrigger.firstRun = false;
                    attachCostEstimatehelperTOtrigger.sendEmailWithAttachment(new List<Id> {so.Id});
                }
                catch(exception e) {}
            }
            if(so.Shipping_Status__c == 'Cost Estimate'  &&
               so.Taxes_calculated__c == true &&
               so.Request_Email_Estimate__c == true &&
               so.Email_Estimate_To__c!=null &&
               so.Cost_Estimate_attached__c == false &&
               attachCostEstimatehelperTOtrigger.firstRun) {
                try{
                    attachCostEstimatehelperTOtrigger.firstRun = false;
                    attachCostEstimatehelperTOtrigger.sendEmailWithAttachment2(new List<Id> {so.Id});
                }
                catch(exception e) {}
            }

            if(so.CPA_v2_0__c != trigger.oldMap.get(So.id).CPA_v2_0__c) {
                CPAChangeSOIds.add(so.Id);
            }


        }
        // For  SO status changed - Create SOlog record - Start
        if(!SOlist11.isEmpty() ) {
            String source1 = 'SO Status Changed';
            // CreateSoLogonStatusChange.CreateSOLOG(SOlist11.values(),source1);
        }
        // For  SO status changed - Create SOlog record - End

        //  For SO Status changed to Shipment pending - Create SOlog record - Start

        if(!SOlist31.isEmpty() && !CreateSoLogonStatusChange.isSOLogCreated) {
            String source3 = 'SO Creation';
            CreateSoLogonStatusChange.isSOLogCreated = true;
            CreateSoLogonStatusChange.CreateSOLOG(SOlist31.values(),source3);
        }
        // For  SSO Status changed to Shipment pending - Create SOlog record - End

        if(!CPAChangeSOIds.isEmpty()) {
            setPrefferedOnRates(CPAChangeSOIds);
        }

        if(!soidList.isEmpty()) {
            if(!RecusrsionHandler.runTaskInQueueable && !RecusrsionHandler.tasksCreated){
                RecusrsionHandler.tasksCreated = true;
                MasterTaskProcess.taskCreation(soidList);
                Set<Id> cpaIds = new Set<Id>();
                Map<Id,shipment_order__c> soMap = new Map<Id,shipment_order__c>();
                for(Id shipmentId : soidList) {
                    cpaIds.add(Trigger.newMap.get(shipmentId).CPA_v2_0__c);
                    soMap.put(shipmentId,Trigger.newMap.get(shipmentId).clone(true,true,true,true));
                }
    
                for(AggregateResult a : [SELECT count(Id) client_tasks,Shipment_Order__c from Task where Master_Task__c != null AND State__c='Client Pending' AND IsNotApplicable__c = false AND Shipment_Order__c IN: soMap.keySet() group by Shipment_Order__c]) {
                    String sId = String.valueOf(a.get('Shipment_Order__c'));
                    soMap.get(sId).Client_Task__c = (Decimal) a.get('client_tasks');
                }
                AttachCostEstimateHandler.bindingQoute(soMap,cpaIds); // Binding Quote update field logic
                SoLogOnTaskHelper.getSummary(soMap);
            }else if(!RecusrsionHandler.runTaskIsQueued && RecusrsionHandler.runTaskInQueueable){
                RecusrsionHandler.runTaskIsQueued = true;
                System.enqueueJob(new MasterTaskQueueable(soidList));
            }
        }

        if((!ETACalculation.isETACalculated && !soToCalculateETA.isEmpty())) {
            ETACalculation.processETA(soToCalculateETA);
        }
        RecusrsionHandler.isAfterUpdate = true;

        if(!calculateTaxesList.isEmpty())   newTaxCalculator.calculateTaxesV2(calculateTaxesList);
    }



    public static void setPrefferedOnRates(Set<Id> CPAChangeSOIds){
        if(!CPAChangeSOIds.isEmpty()) {
            List<Freight__c> freightToUpdate = [SELECT Id, Shipment_Order__r.CPA_v2_0__r.Restricted_Service_Type__c, SF_country__C, Con_country__c FROM Freight__c WHERE Shipment_Order__c IN: CPAChangeSOIds];
            if(!freightToUpdate.isEmpty()) {
                CreateCourierrates.setPreferredOnRates(freightToUpdate);
            }
        }
    }
    
}