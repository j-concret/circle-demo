trigger invoiceTrigger on Invoice_New__c ( before insert, after insert, before update, after update) {
    
    
    
    
    Set<Id> shipmentOrderIds = new Set<Id>();
    Set<Id> accountIds = new Set<Id>();
    Set<Id> freightIds = new Set<Id>();
    Set<Id> invoiceIds = new Set<Id>();
    Set<Id> invoiceIdsToUpdate = new Set<Id>();
    
    
    
    for (Invoice_New__c invoice : Trigger.new) {
        invoiceIds.add(invoice.Id);
        shipmentOrderIds.add(invoice.Shipment_Order__c);
        accountIds.add(invoice.Account__c);
        freightIds.add(invoice.Freight_Request__c); 
        if(invoice.Invoice_Type__c == 'Credit Note' && invoice.Invoice_Being_Credited__c != null){
                invoiceIdsToUpdate.add(invoice.Invoice_Being_Credited__c);
        }
    }
    
    
    If(Trigger.IsInsert && Trigger.IsBefore){
        
        Map<Id, Account> accMapForInvoice = new Map<Id, Account>([Select Id, IOR_Payment_Terms__c from Account Where Id IN :accountIds]);

        Map<Id, Shipment_Order__c> shipmentOrder = new Map<Id,Shipment_Order__c>([Select Id, Name, POD_Date__c,
                                                                                  Account__r.Eligible_for_Tipalti_Autopay__c
                                                                                  From Shipment_Order__c
                                                                                  Where Id in :shipmentOrderIds]);
        
        Map<Id, Accounting_Period__c> accPeriodMap = new Map<Id, Accounting_Period__c>([Select Id, Name From Accounting_Period__c Where Status__c = 'Open']); 
        
        Map<String, Id> accPeriod = new Map<String, Id>();  
        
        For (Id getAccPeriod : accPeriodMap.keySet()) { accPeriod.put(accPeriodMap.get(getAccPeriod).Name, getAccPeriod ); } 
        
        String supplierRTypeId = Schema.Sobjecttype.Invoice_New__c.getRecordTypeInfosByDeveloperName().get('Supplier').getRecordTypeId();
        
        for (Invoice_New__c invoice : Trigger.new) {
            If(shipmentOrder.size() > 0 && shipmentOrder.size() != NULL){
                If(shipmentOrder.get(invoice.Shipment_Order__c).POD_Date__c != null && invoice.POD_Date_New__c == null){
                    invoice.POD_Date_New__c = shipmentOrder.get(invoice.Shipment_Order__c).POD_Date__c;
                } 
                
                If(shipmentOrder.get(invoice.Shipment_Order__c).POD_Date__c  != null && invoice.Accounting_Period_new__c == null){
                    invoice.Accounting_Period_new__c =  accPeriod.get(invoice.Accounting_Period_Formula__c);
                }
                
                if(shipmentOrder.get(invoice.Shipment_Order__c).Account__r.Eligible_for_Tipalti_Autopay__c == true && invoice.RecordTypeId == supplierRTypeId){
                    invoice.Auto_Pay__c = 'Yes';
                }
                
            }
            if(invoice.Invoice_Status__c == 'Invoice Sent' && invoice.RecordTypeId == Schema.Sobjecttype.Invoice_New__c.getRecordTypeInfosByDeveloperName().get('Client_Zee').getRecordTypeId()){
                invoice.Invoice_Sent_Date__c = invoice.Invoice_Sent_Date__c==null ? date.today() : invoice.Invoice_Sent_Date__c;
                invoice.Due_Date__c = invoice.Invoice_Sent_Date__c + accMapForInvoice.get(invoice.Account__c).IOR_Payment_Terms__c.intValue();
            }
        }  
    }   
    
    
    
    
    
    If(Trigger.IsInsert && Trigger.IsAfter){ 
      Set<Id> populateInvoiceIds = new Set<Id>(); 
        Set<Id> StripeInvoices= new Set<Id>();
        Id zeeInvoiceRecordTypeId = Schema.Sobjecttype.Invoice_New__c.getRecordTypeInfosByDeveloperName().get('Client_Zee').getRecordTypeId();
        
        
        List<Invoice_New__c> InvoiceList = [select Id,Invoice_amount_USD__c,Invoice_Type__c  from Invoice_New__c where Id in :invoiceIds AND Invoice_amount_USD__c < 0 AND Invoice_Type__c != 'Credit Note'];
        

        for(Invoice_New__c invoice:InvoiceList){
            invoice.Invoice_Type__c = 'Credit Note';
           
        }
        
        
        

        Map<Id, Invoice_New__c> invoiceMap;
        
        if(!invoiceIdsToUpdate.isEmpty()){
            
           invoiceMap = new Map<Id, Invoice_New__c>([Select Id, Invoice_Note__c from Invoice_New__c Where Id IN :invoiceIdsToUpdate]);
                
        }
        
        for(Invoice_New__c invoice : Trigger.New){
            if(invoice.Invoice_Type__c == 'Credit Note' && invoice.Invoice_Being_Credited__c != null){
                Invoice_New__c inv = invoiceMap.get(invoice.Invoice_Being_Credited__c);
                inv.Invoice_Note__c = 'This invoice has been reversed and credit note '+ invoice.Name +' has been raised.';
                inv.Invoice_Status__c = 'Paid';
                
                InvoiceList.add(inv);
            }
            if(invoice.Invoice_Status__c == 'Invoice Sent' && invoice.RecordTypeId == zeeInvoiceRecordTypeId){
                
                //populateInvoiceIds.add(invoice.Id);
                 StripeInvoices.add(invoice.Id);  
                
            }
        }

         if(StripeInvoices !=null && !StripeInvoices.isEmpty()){ 
             if(!Test.isRunningTest()){
            StripeCheckout.submit(StripeInvoices);  
             }
        }
       
        
        if(InvoiceList != null && !InvoiceList.isEmpty()){
            update InvoiceList;
        }
        
        Map<Id, Currency_Management2__c> currencyMap = new Map<Id, Currency_Management2__c>([Select Id, Name, Conversion_Rate__c, Currency__c, ISO_Code__c
                                                                                             From Currency_Management2__c]); 
        
        Map<String, Decimal> conversionRate = new Map<String, Decimal>();
        
        For (Id getRates : currencyMap.keySet()) {conversionRate.put(currencyMap.get(getRates).Name,currencyMap.get(getRates).Conversion_Rate__c );}     
        
        Map<Id, Accounting_Period__c> accPeriodMap = new Map<Id, Accounting_Period__c>([Select Id, Name From Accounting_Period__c Where Status__c = 'Open']); 
        
        Map<String, Id> accPeriod = new Map<String, Id>();  
        
        For (Id getAccPeriod : accPeriodMap.keySet()) { accPeriod.put(accPeriodMap.get(getAccPeriod).Name, getAccPeriod ); } 
        
        
        Map<Id, Dimension__c> dimensionMap = new Map<Id, Dimension__c>([Select Id, Name From Dimension__c]);
        
        Map<String, Id> dimension = new Map<String, Id>(); 
        
        For (Id getDimension : dimensionMap.keySet()) { dimension.put(dimensionMap.get(getDimension).Name, getDimension );} 
        
        Map<Id, GL_Account__c> glAccountMap = new Map<Id, GL_Account__c>([Select Id, Name, GL_Account_Description__c,GL_Account_Short_Description__c,Currency__c From GL_Account__c]); 
        
        
        Map<String, Id> glAccount = new Map<String, Id>(); 
        
        For (Id getGlAccount : glAccountMap.keySet()) { glAccount.put(glAccountMap.get(getGlAccount).Name, getGlAccount ); } 
        
        
        Map<Id, Account> accountMap = new Map<Id,Account>([Select Id, Name, Region__c, Dimension__c, Invoicing_Term_Parameters__c, IOR_Payment_Terms__c, Invoice_Timing__c
                                                           From Account
                                                           Where Id in :accountIds]);
        
        List<Transaction_Line_New__c> transactionLines = new List<Transaction_Line_New__c>();
        
        List<Shipment_Order__c> shipmentToUpdate = new List<Shipment_Order__c>();
        
        
        Map<Id, Shipment_Order__c> shipmentOrder = new Map<Id,Shipment_Order__c>([Select Id, Name, CI_Admin_Fee__c, CI_Bank_Fees__c, CI_Cash_Outlay_Fee__c, CI_Collection_Administration_Fee__c,
                                                                                  CI_IOR_and_Import_Compliance_Fee_USD__c, CI_International_Delivery_Fee__c, CI_Liability_Cover__c, CI_Miscellaneous_Fee__c,
                                                                                  CI_Recharge_Tax_and_Duty__c, CI_Total_Clearance_Costs__c, CI_Total_Customs_Brokerage_Cost__c, CI_Total_Handling_Cost__c,
                                                                                  CI_Total_Licence_Cost__c, Revenue_Ad_Hoc_Fees__c, Revenue_Bank_Charges_Recovered__c, Revenue_Exporter_of_Record__c, 
                                                                                  Revenue_Financing_and_Insurance__c, Revenue_Importer_of_Record__c, Revenue_Insurance_Fees__c, Revenue_On_Charges__c,
                                                                                  Revenue_Shipping_Insurance__c, Revenue_Tax__c, Actual_Admin_Fee__c, Actual_Bank_Fees__c, Actual_Duties_and_Taxes_Other__c,
                                                                                  Actual_Finance_Fee__c, Actual_Insurance_Fee_USD__c, Actual_International_Delivery_Fee__c, Actual_Miscellaneous_Fee__c,
                                                                                  Actual_Total_Clearance_Costs__c, Actual_Total_Customs_Brokerage_Cost__c, Actual_Total_Handling_Costs__c, Actual_Total_IOR_EOR__c,
                                                                                  Actual_Total_License_Cost__c, Actual_Total_Tax_and_Duty__c, Cost_of_Sale_Ad_Hoc_Fees__c, Cost_of_Sale_Bank_Charges_Recovered__c,
                                                                                  Cost_of_Sale_Exporter_of_Record__c, Cost_of_Sale_Financing_and_Insurance__c, Cost_of_Sale_Importer_of_Record__c,
                                                                                  Cost_of_Sale_Insurance_Fees__c, Cost_of_Sale_On_Charges__c, Cost_of_Sale_Shipping_Insurance__c, Cost_of_Sale_Tax__c
                                                                                  From Shipment_Order__c
                                                                                  Where Id in :shipmentOrderIds]);
        
        
        List<AggregateResult> allInvoices = [SELECT Sum(Revenue_Ad_Hoc_Fees__c) revAdHoc, SUM(Revenue_Bank_Charges_Recovered__c) revBank, Sum(Revenue_Exporter_of_Record__c) revEOR,
                                             SUM(Revenue_Financing_and_Insurance__c) revFinancing, sum(Revenue_Importer_of_Record__c) revIOR, sum(Revenue_Insurance_Fees__c) revInsurance,
                                             sum(Revenue_On_Charges__c) revOnCharge, sum(Revenue_Shipping_Insurance__c) revShipping, sum(Revenue_Tax__c) revTax, sum(Admin_Fees_USD__c) adminFee,
                                             sum(Bank_Fees_USD__c) bankFee, sum(Cash_Outlay_Fee_USD__c) cashOut, sum(Collection_Administration_Fee_USD__c) collectionAdmin, sum(Customs_Brokerage_Fees_USD__c) brokerage,
                                             sum(Customs_Clearance_Fees_USD__c) clearance, sum(Customs_Handling_Fees_USD__c) handling, sum(Customs_License_Fees_USD__c) license, sum(EOR_Fees_USD__c) eor,
                                             sum(International_Freight_Fee_USD__c) freight, sum(IOR_Fees_USD__c) ior, sum(Liability_Cover_Fee_USD__c) liability, sum(Miscellaneous_Fee_USD__c) misc,
                                             sum(Recharge_Tax_and_Duty_Other_USD__c) taxOther, sum(Taxes_and_Duties_USD__c) tax, sum(Cost_of_Sale_Ad_Hoc_Fees__c) costAdHoc, sum(Cost_of_Sale_Bank_Charges_Recovered__c) costBank,
                                             sum(Cost_of_Sale_Exporter_of_Record__c) costEOR, sum(Cost_of_Sale_Financing_and_Insurance__c) costFinancing, sum(Cost_of_Sale_Importer_of_Record__c) costIOR,
                                             SUM(Cost_of_Sale_Insurance_Fees__c) costInsurance, sum(Cost_of_Sale_On_Charges__c) costOnCharge, sum(Shipment_Value_Invoice_Line__c) shipLine, sum(Cost_of_Sale_Shipping_Insurance__c) costShipping,
                                             sum(Cost_of_Sale_Tax__c) costTax, sum(Actual_Admin_Costs__c) actualAdmin, sum(Actual_Customs_Brokerage_Costs__c) actualBrokerage, sum(Actual_Bank_Costs__c) actualBank,
                                             sum(Actual_Customs_Clearance_Costs__c) actualClearance, sum(Actual_Customs_Handling_Costs__c) actualHandling, sum(Actual_Customs_License_Costs__c) actualLicense,
                                             sum(Actual_Duties_and_Taxes__c) actualTax, sum(Actual_Duties_and_Taxes_Other__c) actualTaxOther, sum(Actual_Finance_Costs__c) actualFinance, 
                                             sum(Actual_Insurance_Costs__c) actualInsurance, sum(Actual_International_Delivery_Costs__c) actualFreight, sum(Actual_IOREOR_Costs__c) actualIEOR,
                                             sum(Actual_Miscellaneous_Costs__c) actualmisc, sum(Total_Value_Applied__c) totalApplied, Shipment_Order__c, RecordTypeId 
                                             From Invoice_New__c 
                                             where Shipment_Order__c in :shipmentOrderIds
                                             And (Invoice_Status__c ='Invoice Sent'
                                                  OR Invoice_Status__c = 'FM Approved'
                                                  OR Invoice_Status__c = 'Paid'
                                                  OR Invoice_Status__c = 'Partial Payment'
                                                  OR Invoice_Status__c = 'Paid by Tipalti'
                                                  OR Invoice_Status__c = 'Paid via Stripe')
                                             Group by Shipment_Order__c, RecordTypeId];   
       
        
        
        
        for (Invoice_New__c invoice :[ SELECT Id, IsDeleted, Name, RecordTypeId, CreatedDate, CreatedById, LastModifiedDate, LastModifiedById, SystemModstamp, LastActivityDate, LastViewedDate, LastReferencedDate, 
                                      Account__c, AM__c, Accounting_Period_Formula__c, Accounting_Period_new__c, Actual_Admin_Costs__c, Actual_Bank_Costs__c, 
                                      Actual_Customs_Brokerage_Costs__c, Actual_Customs_Clearance_Costs__c, Actual_Customs_Handling_Costs__c, Actual_Customs_License_Costs__c, 
                                      Actual_Duties_and_Taxes_Other__c, Actual_Duties_and_Taxes__c, Actual_Finance_Costs__c, Actual_IOREOR_Costs__c, Actual_Insurance_Costs__c, 
                                      Actual_International_Delivery_Costs__c, Actual_Miscellaneous_Costs__c, Admin_Fees__c, Amount_Outstanding_Local_Currency__c, Amount_Outstanding__c, 
                                      Auto_Reversal_Date_Stamp__c, Auto_Reversal_User_Stamp__c, Bank_Fee__c, Billing_City__c, Billing_Country__c, Billing_Postal_Code__c, Billing_State__c, 
                                      Billing_Street__c, Cash_Outlay_Fee__c, Cash_Outlay_Invoice_Created__c, Collection_Administration_Fee__c, Conversion_Rate__c, Cost_of_Sale_Ad_Hoc_Fees__c, 
                                      Cost_of_Sale_Bank_Charges_Recovered__c, Cost_of_Sale_Exporter_of_Record__c, Cost_of_Sale_Financing_and_Insurance__c, Cost_of_Sale_Foreign_Exchange_Rate_Fee__c, 
                                      Cost_of_Sale_Importer_of_Record__c, Cost_of_Sale_Insurance_Fees__c, Cost_of_Sale_On_Charges__c, Cost_of_Sale_Shipping_Insurance__c, Cost_of_Sale_Tax__c, 
                                      Credits_Applied_Date__c, Customs_Brokerage_Fees__c, Customs_Clearance_Fees__c, Customs_Handling_Fees__c, Customs_License_In__c, Due_Date__c, EOR_Fees__c, 
                                      Entered_Cash_Outlay_Step__c, FC_Admin_Costs__c, FC_Bank_Costs__c, FC_Customs_Brokerage_Costs__c, FC_Customs_Clearance_Costs__c, FC_Customs_Handling_Costs__c, FC_Customs_License_Costs__c, 
                                      FC_Duties_and_Taxes_Other__c, FC_Duties_and_Taxes__c, FC_Finance_Costs__c, FC_IOR_Costs__c, FC_Insurance_Costs__c, FC_International_Delivery_Costs__c, FC_Miscellaneous_Costs__c, 
                                      Foreign_Exchange_gainloss_on_payment__c, Freight_Request__c, Freight_and_Related_Costs__c, Government_and_In_country_Summary__c, IOR_EOR_Compliance__c, IOR_Fees__c, International_Freight_Fee__c,
                                      Invoice_Age_Text__c, Invoice_Age__c, Invoice_Amount_Local_Currency__c, Invoice_Being_Credited__c, Invoice_Currency__c, Invoice_Date__c, Invoice_Name__c, Invoice_Reversal__c, Invoice_Sent_Date__c, 
                                      Invoice_Status__c, Invoice_Type__c, Invoice_amount_USD__c, Liability_Cover_Fee__c, Miscellaneous_Fee_Name__c, Miscellaneous_Fee__c, Neg_Transaction_Total__c, New_Penalty_Date__c, Overdue__c, 
                                      POD_Date_New__c, POD_Date__c, PO_Number_Override__c, PO_Number__c, Penalty_Amount__c, Penalty_Daily_Percentage__c, Penalty_Days__c, Penalty_Outstanding_Amount__c, Penalty_Status__c, Possible_Cash_Outlay__c,
                                      Prepayment_Date__c, Prior_Penalty_Date__c, Re_Populate_Invoice__c, Recharge_Tax_and_Duty_Other__c, Revenue_Ad_Hoc_Fees__c, Revenue_Bank_Charges_Recovered__c, Revenue_Exporter_of_Record__c, Revenue_Financing_and_Insurance__c,
                                      Revenue_Importer_of_Record__c, Revenue_Insurance_Fees__c, Revenue_On_Charges__c, Revenue_Shipping_Insurance__c, Revenue_Tax__c, Ship_To_Country__c, Shipment_Order__c, Shipment_Status__c, Taxes_and_Duties__c, To_Due__c, To_Prepaid__c,
                                      Today__c, Total_Amount_Incl_Potential_Cash_Outlay__c, Total_Credits_Applied_To_Other_Invoices__c, Total_Credits_Applied_To_This_Invoice__c, Total_Value_Applied__c, Transaction_Line_Balance__c, Transaction_Lines_Created__c, 
                                      Transaction_Lines_Penalty_Amount__c, Transaction_Total__c, Upfront_No_Terms__c, isPostSend__c, Paid_Date__c, Penalty_Bank_Transaction_Amount_Applied__c, Penalty_Credit_Note_Amount_Applied__c, Receipt_Date__c, Total_Cash_Applied__c,
                                      Roll_Up_Costings__c, IOR_Fees_USD__c, Admin_Fees_USD__c, Bank_Fees_USD__c, EOR_Fees_USD__c, Cash_Outlay_Fee_USD__c, Collection_Administration_Fee_USD__c, Liability_Cover_Fee_USD__c, Customs_Brokerage_Fees_USD__c, Customs_Clearance_Fees_USD__c,
                                      Customs_Handling_Fees_USD__c, Customs_License_Fees_USD__c, International_Freight_Fee_USD__c, Taxes_and_Duties_USD__c, Recharge_Tax_and_Duty_Other_USD__c, Miscellaneous_Fee_USD__c, Total_Invoice_Amount_Formula__c, Payment_Tracking__c,
                                      Financial_Controller__c, SI_CI_Created__c, Bill_To__c, Enterprise_Billing__c, Invoice_Note__c, Shipment_Value_Invoice_Line__c, Submitted_to_Tipalti__c, Auto_Pay_Total_Forecast_Amount__c, Auto_Pay_Total_Invoiced_Amount__c, 
                                      Auto_Pay__c, Auto_pay_Threshold__c, Date_Time_Stamp__c, Sent_From_System__c, User_Stamp__c, Tipalti_validation_completed__c, Do_you_want_to_Export__c, FreightAmount__c, AmountWithTax__c, Record_exported__c, Bank_reference__c, Tipalti_Paid_Date__c 
                                      FROM Invoice_New__c where Id In :Trigger.new]) {
                                         
                                          
                                          String clientRTypeId = Schema.Sobjecttype.Invoice_New__c.getRecordTypeInfosByDeveloperName().get('Client').getRecordTypeId();
                                          String supplierRTypeId = Schema.Sobjecttype.Invoice_New__c.getRecordTypeInfosByDeveloperName().get('Supplier').getRecordTypeId();
                                          
                                          String dimension3 = accountMap.get(invoice.Account__c).Region__c; 
                                          String dimension1 = accountMap.get(invoice.Account__c).Dimension__c; 
                                          //updateDueDateForSupplier(invoice);
                                          
                                          Decimal debitTotal = invoice.Revenue_Ad_Hoc_Fees__c + invoice.Revenue_Bank_Charges_Recovered__c + invoice.Revenue_Exporter_of_Record__c + invoice.Revenue_Financing_and_Insurance__c + invoice.Revenue_Importer_of_Record__c + invoice.Revenue_Insurance_Fees__c + invoice.Revenue_On_Charges__c + invoice.Revenue_Shipping_Insurance__c + invoice.Revenue_Tax__c + invoice.Shipment_Value_Invoice_Line__c;
                                          Decimal creditTotal = (invoice.Cost_of_Sale_Ad_Hoc_Fees__c + invoice.Cost_of_Sale_Bank_Charges_Recovered__c + invoice.Cost_of_Sale_Exporter_of_Record__c + invoice.Cost_of_Sale_Financing_and_Insurance__c + invoice.Cost_of_Sale_Importer_of_Record__c + invoice.Cost_of_Sale_Insurance_Fees__c + invoice.Cost_of_Sale_On_Charges__c + invoice.Cost_of_Sale_Shipping_Insurance__c + invoice.Cost_of_Sale_Tax__c + invoice.Shipment_Value_Invoice_Line__c) * -1;
                                          Decimal transactionTotal = invoice.Revenue_Ad_Hoc_Fees__c + invoice.Revenue_Bank_Charges_Recovered__c  + invoice.Revenue_Exporter_of_Record__c +
                                              invoice.Revenue_Financing_and_Insurance__c  + invoice.Revenue_Importer_of_Record__c  + invoice.Revenue_Tax__c +
                                              invoice.Revenue_Shipping_Insurance__c  + invoice.Revenue_On_Charges__c + invoice.Revenue_Insurance_Fees__c + invoice.Shipment_Value_Invoice_Line__c;
                                          Decimal transactionTotalSI = invoice.Cost_of_Sale_Ad_Hoc_Fees__c + invoice.Cost_of_Sale_Bank_Charges_Recovered__c  + invoice.Cost_of_Sale_Exporter_of_Record__c +
                                              invoice.Cost_of_Sale_Financing_and_Insurance__c + invoice.Cost_of_Sale_Importer_of_Record__c   + invoice.Cost_of_Sale_Tax__c  + invoice.Shipment_Value_Invoice_Line__c +
                                              invoice.Cost_of_Sale_Shipping_Insurance__c  + invoice.Cost_of_Sale_On_Charges__c  + invoice.Cost_of_Sale_Insurance_Fees__c;
                                          
                                          Date maxDate = (invoice.POD_Date_New__c != null && invoice.POD_Date_New__c > invoice.Invoice_Sent_Date__c ) ? 
                                              invoice.POD_Date_New__c : 
                                          (invoice.Invoice_Sent_Date__c == null ? date.today() : invoice.Invoice_Sent_Date__c); 
                                              
                                              
                                              
                                              If(((invoice.Invoice_Type__c == 'Cash Outlay Invoice' || invoice.Invoice_Type__c == 'Credit Note') && (invoice.RecordTypeId == clientRTypeId || invoice.RecordTypeId == zeeInvoiceRecordTypeId) && invoice.Invoice_Being_Credited__c == null && invoice.Invoice_Status__c == 'Invoice Sent') || 
               (invoice.RecordTypeId == clientRTypeId || invoice.RecordTypeId == zeeInvoiceRecordTypeId) && invoice.Invoice_Status__c == 'Invoice Sent' && invoice.Invoice_Type__c == 'Invoice'){ 
                                                     
                                                     
                                                     
                                                     If(invoice.Invoice_amount_USD__c != 0){
                                                         
                                                         Transaction_Line_New__c ciTL = new Transaction_Line_New__c(
                                                             Amount__c = debitTotal ,   
                                                             Business_Partner__c = invoice.Account__c,
                                                             Description__c = 'Customer Control Account',
                                                             Accounting_Period_New__c = accPeriod.get(invoice.Accounting_Period_Formula__c),
                                                             Date__c = maxDate,
                                                             Dimension_1__c = dimension1,
                                                             Dimension_3__c = dimension.get(dimension3),
                                                             GL_Account__c = glAccount.get('610000'),
                                                             Invoice__c = invoice.Id,
                                                             RecordTypeId = Schema.Sobjecttype.Transaction_Line_New__c.getRecordTypeInfosByDeveloperName().get('Invoice').getRecordTypeId(),
                                                             Revenue_Type__c = 'Actual',
                                                             Short_Description__c = 'AR Control',
                                                             Status__c ='Posted',
                                                             Type_Code__c = 'CI');
                                                         transactionLines.add(ciTL);}
                                                     
                                                     If(invoice.Revenue_Ad_Hoc_Fees__c != 0){
                                                         Transaction_Line_New__c ciTL = new Transaction_Line_New__c(
                                                             Amount__c = invoice.Revenue_Ad_Hoc_Fees__c * -1,   
                                                             Business_Partner__c = invoice.Account__c,
                                                             Description__c = 'Revenue',
                                                             Accounting_Period_New__c = accPeriod.get(invoice.Accounting_Period_Formula__c),
                                                             Date__c = maxDate,
                                                             Dimension_1__c = dimension1,
                                                             Dimension_2__c = dimension.get('ADHOC'),
                                                             Dimension_3__c = dimension.get(dimension3),
                                                             GL_Account__c = glAccount.get('110000'),
                                                             Invoice__c = invoice.Id,
                                                             RecordTypeId = Schema.Sobjecttype.Transaction_Line_New__c.getRecordTypeInfosByDeveloperName().get('Invoice').getRecordTypeId(),
                                                             Revenue_Type__c = 'Actual',
                                                             Short_Description__c = 'Revenue',
                                                             Status__c ='Posted',
                                                             Type_Code__c = 'CI');
                                                         transactionLines.add(ciTL);}
                                                     
                                                     
                                                     
                                                     
                                                     If(invoice.Revenue_Exporter_of_Record__c != 0){
                                                         Transaction_Line_New__c ciTL = new Transaction_Line_New__c(
                                                             Amount__c = invoice.Revenue_Exporter_of_Record__c * -1,   
                                                             Business_Partner__c = invoice.Account__c,
                                                             Description__c = 'Revenue',
                                                             Accounting_Period_New__c = accPeriod.get(invoice.Accounting_Period_Formula__c),
                                                             Date__c = maxDate,
                                                             Dimension_1__c = dimension1,
                                                             Dimension_2__c = dimension.get('EOR'),
                                                             Dimension_3__c = dimension.get(dimension3),
                                                             GL_Account__c = glAccount.get('110000'),
                                                             Invoice__c = invoice.Id,
                                                             RecordTypeId = Schema.Sobjecttype.Transaction_Line_New__c.getRecordTypeInfosByDeveloperName().get('Invoice').getRecordTypeId(),
                                                             Revenue_Type__c = 'Actual',
                                                             Short_Description__c = 'Revenue',
                                                             Status__c ='Posted',
                                                             Type_Code__c = 'CI');
                                                         transactionLines.add(ciTL);            
                                                     }   
                                                     
                                                     If(invoice.Revenue_Financing_and_Insurance__c != 0){
                                                         Transaction_Line_New__c ciTL = new Transaction_Line_New__c(
                                                             Amount__c = invoice.Revenue_Financing_and_Insurance__c * -1,   
                                                             Business_Partner__c = invoice.Account__c,
                                                             Description__c = 'Revenue',
                                                             Accounting_Period_New__c = accPeriod.get(invoice.Accounting_Period_Formula__c),
                                                             Date__c = maxDate,
                                                             Dimension_1__c = dimension1,
                                                             Dimension_2__c = dimension.get('FININSURANCE'),
                                                             Dimension_3__c = dimension.get(dimension3),
                                                             GL_Account__c = glAccount.get('110000'),
                                                             Invoice__c = invoice.Id,
                                                             RecordTypeId = Schema.Sobjecttype.Transaction_Line_New__c.getRecordTypeInfosByDeveloperName().get('Invoice').getRecordTypeId(),
                                                             Revenue_Type__c = 'Actual',
                                                             Short_Description__c = 'Revenue',
                                                             Status__c ='Posted',
                                                             Type_Code__c = 'CI');
                                                         transactionLines.add(ciTL);            
                                                     }    
                                                     
                                                     
                                                     If(invoice.Revenue_Importer_of_Record__c != 0){
                                                         Transaction_Line_New__c ciTL = new Transaction_Line_New__c(
                                                             Amount__c = invoice.Revenue_Importer_of_Record__c * -1,   
                                                             Business_Partner__c = invoice.Account__c,
                                                             Description__c = 'Revenue',
                                                             Accounting_Period_New__c = accPeriod.get(invoice.Accounting_Period_Formula__c),
                                                             Date__c = maxDate,
                                                             Dimension_1__c = dimension1,
                                                             Dimension_2__c = dimension.get('IOR'),
                                                             Dimension_3__c = dimension.get(dimension3),
                                                             GL_Account__c = glAccount.get('110000'),
                                                             Invoice__c = invoice.Id,
                                                             RecordTypeId = Schema.Sobjecttype.Transaction_Line_New__c.getRecordTypeInfosByDeveloperName().get('Invoice').getRecordTypeId(),
                                                             Revenue_Type__c = 'Actual',
                                                             Short_Description__c = 'Revenue',
                                                             Status__c ='Posted',
                                                             Type_Code__c = 'CI');
                                                         transactionLines.add(ciTL);            
                                                     }       
                                                     
                                                     
                                                     If(invoice.Revenue_Tax__c != 0){
                                                         Transaction_Line_New__c ciTL = new Transaction_Line_New__c(
                                                             Amount__c = invoice.Revenue_Tax__c * -1,   
                                                             Business_Partner__c = invoice.Account__c,
                                                             Description__c = 'Revenue - Client taxes paid',
                                                             Accounting_Period_New__c = accPeriod.get(invoice.Accounting_Period_Formula__c),
                                                             Date__c = maxDate,
                                                             Dimension_1__c = dimension1,
                                                             Dimension_2__c = dimension.get('TAX'),
                                                             Dimension_3__c = dimension.get(dimension3),
                                                             GL_Account__c = glAccount.get('110151'),
                                                             Invoice__c = invoice.Id,
                                                             RecordTypeId = Schema.Sobjecttype.Transaction_Line_New__c.getRecordTypeInfosByDeveloperName().get('Invoice').getRecordTypeId(),
                                                             Revenue_Type__c = 'Actual',
                                                             Short_Description__c = 'Revenue-ClientTax',
                                                             Status__c ='Posted',
                                                             Type_Code__c = 'CI');
                                                         transactionLines.add(ciTL);            
                                                     }    
                                                     
                                                     If(invoice.Shipment_Value_Invoice_Line__c != 0){
                                                         Transaction_Line_New__c ciTL = new Transaction_Line_New__c(
                                                             Amount__c = invoice.Shipment_Value_Invoice_Line__c * -1,   
                                                             Business_Partner__c = invoice.Account__c,
                                                             Description__c = 'Cash Book Suspense',
                                                             Accounting_Period_New__c = accPeriod.get(invoice.Accounting_Period_Formula__c),
                                                             Date__c = maxDate,
                                                             Dimension_1__c = dimension1,
                                                             Dimension_3__c = dimension.get(dimension3),
                                                             GL_Account__c = glAccount.get('914500'),
                                                             Invoice__c = invoice.Id,
                                                             RecordTypeId = Schema.Sobjecttype.Transaction_Line_New__c.getRecordTypeInfosByDeveloperName().get('Invoice').getRecordTypeId(),
                                                             Revenue_Type__c = 'Actual',
                                                             Short_Description__c = 'Cash Book Suspense',
                                                             Status__c ='Posted',
                                                             Type_Code__c = 'CI');
                                                         transactionLines.add(ciTL);            
                                                     }    
                                                     
                                                     
                                                     If(invoice.Revenue_Shipping_Insurance__c != 0){
                                                         Transaction_Line_New__c ciTL = new Transaction_Line_New__c(
                                                             Amount__c = invoice.Revenue_Shipping_Insurance__c * -1,   
                                                             Business_Partner__c = invoice.Account__c,
                                                             Description__c = 'Revenue',
                                                             Accounting_Period_New__c = accPeriod.get(invoice.Accounting_Period_Formula__c),
                                                             Date__c = maxDate,
                                                             Dimension_1__c = dimension1,
                                                             Dimension_2__c = dimension.get('SHIPPING'),
                                                             Dimension_3__c = dimension.get(dimension3),
                                                             GL_Account__c = glAccount.get('110000'),
                                                             Invoice__c = invoice.Id,
                                                             RecordTypeId = Schema.Sobjecttype.Transaction_Line_New__c.getRecordTypeInfosByDeveloperName().get('Invoice').getRecordTypeId(),
                                                             Revenue_Type__c = 'Actual',
                                                             Short_Description__c = 'Revenue',
                                                             Status__c ='Posted',
                                                             Type_Code__c = 'CI');
                                                         transactionLines.add(ciTL);            
                                                     }    
                                                     
                                                     
                                                     If(invoice.Revenue_On_Charges__c != 0){
                                                         Transaction_Line_New__c ciTL = new Transaction_Line_New__c(
                                                             Amount__c = invoice.Revenue_On_Charges__c * -1,   
                                                             Business_Partner__c = invoice.Account__c,
                                                             Description__c = 'Revenue',
                                                             Accounting_Period_New__c = accPeriod.get(invoice.Accounting_Period_Formula__c),
                                                             Date__c = maxDate,
                                                             Dimension_1__c = dimension1,
                                                             Dimension_2__c = dimension.get('ONCHARGES'),
                                                             Dimension_3__c = dimension.get(dimension3),
                                                             GL_Account__c = glAccount.get('110000'),
                                                             Invoice__c = invoice.Id,
                                                             RecordTypeId = Schema.Sobjecttype.Transaction_Line_New__c.getRecordTypeInfosByDeveloperName().get('Invoice').getRecordTypeId(),
                                                             Revenue_Type__c = 'Actual',
                                                             Short_Description__c = 'Revenue',
                                                             Status__c ='Posted',
                                                             Type_Code__c = 'CI');
                                                         transactionLines.add(ciTL);            
                                                     }    
                                                     
                                                     
                                                 }
                                          
                                          
                                          If( invoice.Invoice_Type__c == 'Credit Note' && invoice.RecordTypeId == supplierRTypeId && invoice.Invoice_Status__c == 'FM Approved' ){ 
                                              If(invoice.Invoice_amount_USD__c != 0){
                                                  Transaction_Line_New__c ciTL = new Transaction_Line_New__c(
                                                      Amount__c = creditTotal,
                                                      Accounting_Period_New__c = accPeriod.get(invoice.Accounting_Period_Formula__c),
                                                      Date__c = maxDate,
                                                      Business_Partner__c = invoice.Account__c,
                                                      Description__c = 'Supplier Control Account',
                                                      Dimension_1__c = dimension1,
                                                      Dimension_3__c = dimension.get(dimension3),
                                                      GL_Account__c = glAccount.get('920000'),
                                                      Invoice__c = invoice.Id,
                                                      RecordTypeId = Schema.Sobjecttype.Transaction_Line_New__c.getRecordTypeInfosByDeveloperName().get('Invoice').getRecordTypeId(),
                                                      Revenue_Type__c = 'Actual',
                                                      Short_Description__c = 'AP Control',
                                                      Status__c ='Posted',
                                                      Type_Code__c = 'SI');
                                                  transactionLines.add(ciTL);}
                                              
                                              If(invoice.Cost_of_Sale_Ad_Hoc_Fees__c != 0){
                                                  Transaction_Line_New__c ciTL = new Transaction_Line_New__c(
                                                      Amount__c = invoice.Cost_of_Sale_Ad_Hoc_Fees__c,  
                                                      Accounting_Period_New__c = accPeriod.get(invoice.Accounting_Period_Formula__c),
                                                      Date__c = maxDate,
                                                      Business_Partner__c = invoice.Account__c,
                                                      Description__c = 'Cost of Sales',
                                                      Dimension_1__c = dimension1,
                                                      Dimension_2__c = dimension.get('ADHOC'),
                                                      Dimension_3__c = dimension.get(dimension3),
                                                      GL_Account__c = glAccount.get('120000'),
                                                      Invoice__c = invoice.Id,
                                                      RecordTypeId = Schema.Sobjecttype.Transaction_Line_New__c.getRecordTypeInfosByDeveloperName().get('Invoice').getRecordTypeId(),
                                                      Revenue_Type__c = 'Actual',
                                                      Short_Description__c = 'Cost of Sales',
                                                      Status__c ='Posted',
                                                      Type_Code__c = 'SI');
                                                  transactionLines.add(ciTL);}
                                              
                                              If(invoice.Cost_of_Sale_Bank_Charges_Recovered__c != 0){
                                                  Transaction_Line_New__c ciTL = new Transaction_Line_New__c(
                                                      Amount__c = invoice.Cost_of_Sale_Bank_Charges_Recovered__c , 
                                                      Accounting_Period_New__c = accPeriod.get(invoice.Accounting_Period_Formula__c),
                                                      Date__c = maxDate,
                                                      Business_Partner__c = invoice.Account__c,
                                                      Description__c = 'Cost of Sales',
                                                      Dimension_1__c = dimension1,
                                                      Dimension_2__c = dimension.get('BANKCHARGE'),
                                                      Dimension_3__c = dimension.get(dimension3),
                                                      GL_Account__c = glAccount.get('120000'),
                                                      Invoice__c = invoice.Id,
                                                      RecordTypeId = Schema.Sobjecttype.Transaction_Line_New__c.getRecordTypeInfosByDeveloperName().get('Invoice').getRecordTypeId(),
                                                      Revenue_Type__c = 'Actual',
                                                      Short_Description__c = 'Cost of Sales',
                                                      Status__c ='Posted',
                                                      Type_Code__c = 'SI');
                                                  transactionLines.add(ciTL);            
                                              }   
                                              
                                              
                                              If(invoice.Cost_of_Sale_Exporter_of_Record__c != 0){
                                                  Transaction_Line_New__c ciTL = new Transaction_Line_New__c(
                                                      Amount__c = invoice.Cost_of_Sale_Exporter_of_Record__c,
                                                      Accounting_Period_New__c = accPeriod.get(invoice.Accounting_Period_Formula__c),
                                                      Date__c = maxDate,
                                                      Business_Partner__c = invoice.Account__c,
                                                      Description__c = 'Cost of Sales',
                                                      Dimension_1__c = dimension1,
                                                      Dimension_2__c = dimension.get('EOR'),
                                                      Dimension_3__c = dimension.get(dimension3),
                                                      GL_Account__c = glAccount.get('120000'),
                                                      Invoice__c = invoice.Id,
                                                      RecordTypeId = Schema.Sobjecttype.Transaction_Line_New__c.getRecordTypeInfosByDeveloperName().get('Invoice').getRecordTypeId(),
                                                      Revenue_Type__c = 'Actual',
                                                      Short_Description__c = 'Cost of Sales',
                                                      Status__c ='Posted',
                                                      Type_Code__c = 'SI');
                                                  transactionLines.add(ciTL);            
                                              }   
                                              
                                              If(invoice.Cost_of_Sale_Financing_and_Insurance__c != 0){
                                                  Transaction_Line_New__c ciTL = new Transaction_Line_New__c(
                                                      Amount__c = invoice.Cost_of_Sale_Financing_and_Insurance__c,
                                                      Accounting_Period_New__c = accPeriod.get(invoice.Accounting_Period_Formula__c),
                                                      Date__c = maxDate,
                                                      Business_Partner__c = invoice.Account__c,
                                                      Description__c = 'Cost of Sales',
                                                      Dimension_1__c = dimension1,
                                                      Dimension_2__c = dimension.get('FININSURANCE'),
                                                      Dimension_3__c = dimension.get(dimension3),
                                                      GL_Account__c = glAccount.get('120000'),
                                                      Invoice__c = invoice.Id,
                                                      RecordTypeId = Schema.Sobjecttype.Transaction_Line_New__c.getRecordTypeInfosByDeveloperName().get('Invoice').getRecordTypeId(),
                                                      Revenue_Type__c = 'Actual',
                                                      Short_Description__c = 'Cost of Sales',
                                                      Status__c ='Posted',
                                                      Type_Code__c = 'SI');
                                                  transactionLines.add(ciTL);            
                                              }    
                                              
                                              
                                              If(invoice.Cost_of_Sale_Importer_of_Record__c != 0){
                                                  Transaction_Line_New__c ciTL = new Transaction_Line_New__c(
                                                      Amount__c = invoice.Cost_of_Sale_Importer_of_Record__c,
                                                      Accounting_Period_New__c = accPeriod.get(invoice.Accounting_Period_Formula__c),
                                                      Date__c = maxDate,
                                                      Business_Partner__c = invoice.Account__c,
                                                      Description__c = 'Cost of Sales',
                                                      Dimension_1__c = dimension1,
                                                      Dimension_2__c = dimension.get('IOR'),
                                                      Dimension_3__c = dimension.get(dimension3),
                                                      GL_Account__c = glAccount.get('120000'),
                                                      Invoice__c = invoice.Id,
                                                      RecordTypeId = Schema.Sobjecttype.Transaction_Line_New__c.getRecordTypeInfosByDeveloperName().get('Invoice').getRecordTypeId(),
                                                      Revenue_Type__c = 'Actual',
                                                      Short_Description__c = 'Cost of Sales',
                                                      Status__c ='Posted',
                                                      Type_Code__c = 'SI');
                                                  transactionLines.add(ciTL);            
                                              }       
                                              
                                              
                                              If(invoice.Cost_of_Sale_Tax__c != 0){
                                                  Transaction_Line_New__c ciTL = new Transaction_Line_New__c(
                                                      Amount__c = invoice.Cost_of_Sale_Tax__c,
                                                      
                                                      Accounting_Period_New__c = accPeriod.get(invoice.Accounting_Period_Formula__c),
                                                      Date__c = maxDate,
                                                      Business_Partner__c = invoice.Account__c,
                                                      Description__c = 'Revenue - Client taxes paid',
                                                      Dimension_1__c = dimension1,
                                                      Dimension_2__c = dimension.get('TAX'),
                                                      Dimension_3__c = dimension.get(dimension3),
                                                      GL_Account__c = glAccount.get('110151'),
                                                      Invoice__c = invoice.Id,
                                                      RecordTypeId = Schema.Sobjecttype.Transaction_Line_New__c.getRecordTypeInfosByDeveloperName().get('Invoice').getRecordTypeId(),
                                                      Revenue_Type__c = 'Actual',
                                                      Short_Description__c = 'Revenue-ClientTax',
                                                      Status__c ='Posted',
                                                      Type_Code__c = 'SI');
                                                  transactionLines.add(ciTL);            
                                              }    
                                              
                                              If(invoice.Shipment_Value_Invoice_Line__c != 0){
                                                  Transaction_Line_New__c ciTL = new Transaction_Line_New__c(
                                                      Amount__c = invoice.Shipment_Value_Invoice_Line__c,
                                                      
                                                      Accounting_Period_New__c = accPeriod.get(invoice.Accounting_Period_Formula__c),
                                                      Date__c = maxDate,
                                                      Business_Partner__c = invoice.Account__c,
                                                      Description__c = 'Cash Book Suspense',
                                                      Dimension_1__c = dimension1,
                                                      Dimension_3__c = dimension.get(dimension3),
                                                      GL_Account__c = glAccount.get('914500'),
                                                      Invoice__c = invoice.Id,
                                                      RecordTypeId = Schema.Sobjecttype.Transaction_Line_New__c.getRecordTypeInfosByDeveloperName().get('Invoice').getRecordTypeId(),
                                                      Revenue_Type__c = 'Actual',
                                                      Short_Description__c = 'Cash Book Suspense',
                                                      Status__c ='Posted',
                                                      Type_Code__c = 'SI');
                                                  transactionLines.add(ciTL);            
                                              }    
                                              
                                              If(invoice.Cost_of_Sale_Shipping_Insurance__c != 0){
                                                  Transaction_Line_New__c ciTL = new Transaction_Line_New__c(
                                                      Amount__c = invoice.Cost_of_Sale_Shipping_Insurance__c, 
                                                      Accounting_Period_New__c = accPeriod.get(invoice.Accounting_Period_Formula__c),
                                                      Date__c = maxDate,
                                                      Business_Partner__c = invoice.Account__c,
                                                      Description__c = 'Cost of Sales',
                                                      Dimension_1__c = dimension1,
                                                      Dimension_2__c = dimension.get('SHIPPING'),
                                                      Dimension_3__c = dimension.get(dimension3),
                                                      GL_Account__c = glAccount.get('120000'),
                                                      Invoice__c = invoice.Id,
                                                      RecordTypeId = Schema.Sobjecttype.Transaction_Line_New__c.getRecordTypeInfosByDeveloperName().get('Invoice').getRecordTypeId(),
                                                      Revenue_Type__c = 'Actual',
                                                      Short_Description__c = 'Cost of Sales',
                                                      Status__c ='Posted',
                                                      Type_Code__c = 'SI');
                                                  transactionLines.add(ciTL);            
                                              }    
                                              
                                              
                                              If(invoice.Cost_of_Sale_On_Charges__c != 0){
                                                  Transaction_Line_New__c ciTL = new Transaction_Line_New__c(
                                                      Amount__c = invoice.Cost_of_Sale_On_Charges__c, 
                                                      Accounting_Period_New__c = accPeriod.get(invoice.Accounting_Period_Formula__c),
                                                      Date__c = maxDate,
                                                      Business_Partner__c = invoice.Account__c,
                                                      Description__c = 'Cost of Sales',
                                                      Dimension_1__c = dimension1,
                                                      Dimension_2__c = dimension.get('ONCHARGES'),
                                                      Dimension_3__c = dimension.get(dimension3),
                                                      GL_Account__c = glAccount.get('120000'),
                                                      Invoice__c = invoice.Id,
                                                      RecordTypeId = Schema.Sobjecttype.Transaction_Line_New__c.getRecordTypeInfosByDeveloperName().get('Invoice').getRecordTypeId(),
                                                      Revenue_Type__c = 'Actual',
                                                      Short_Description__c = 'Cost of Sales',
                                                      Status__c ='Posted',
                                                      Type_Code__c = 'SI');
                                                  transactionLines.add(ciTL);            
                                              }    
                                              
                                              
                                              If(invoice.Cost_of_Sale_Insurance_Fees__c != 0){
                                                  Transaction_Line_New__c ciTL = new Transaction_Line_New__c(
                                                      Amount__c = invoice.Cost_of_Sale_Insurance_Fees__c,
                                                      Accounting_Period_New__c = accPeriod.get(invoice.Accounting_Period_Formula__c),
                                                      Date__c = maxDate,
                                                      Business_Partner__c = invoice.Account__c,
                                                      Description__c = 'Cost of Sales',
                                                      Dimension_1__c = dimension1,
                                                      Dimension_2__c = dimension.get('INSURANCE'),
                                                      Dimension_3__c = dimension.get(dimension3),
                                                      GL_Account__c = glAccount.get('120000'),
                                                      Invoice__c = invoice.Id,
                                                      RecordTypeId = Schema.Sobjecttype.Transaction_Line_New__c.getRecordTypeInfosByDeveloperName().get('Invoice').getRecordTypeId(),
                                                      Revenue_Type__c = 'Actual',
                                                      Short_Description__c = 'Cost of Sales',
                                                      Status__c ='Posted',
                                                      Type_Code__c = 'SI');
                                                  transactionLines.add(ciTL); }  
                                              
                                              invoice.Transaction_Lines_Created__c = TRUE;
                                              invoice.Transaction_Total__c = invoice.Transaction_Total__c == null? 0 + debitTotal: invoice.Transaction_Total__c + debitTotal;
                                              invoice.Neg_Transaction_Total__c =  invoice.Neg_Transaction_Total__c +  (transactionTotal * -1); }
                                          
                                          
                                          
                                          //Roll up Value applied to SO
                                          iF(invoice.Invoice_Status__c == 'Invoice Sent'){
                                              
                                              
                                              for(AggregateResult a : allInvoices){
                                                  
                                                  
                                                  
                                                  Id thisSoId = string.valueOf(a.get('Shipment_Order__c'));
                                                  Shipment_Order__c thisSo = new Shipment_Order__c(Id=thisSoId);
                                                  
                                                  if(shipmentOrder.containskey(thisSoId))
                                                  {
                                                      thisSo = shipmentOrder.get(thisSoId);
                                                      
                                                  }
                                                  
                                                  if(a.get('RecordTypeId') == Schema.Sobjecttype.Invoice_New__c.getRecordTypeInfosByDeveloperName().get('Supplier').getRecordTypeId())
                                                  { 
                                                      
                                                      shipmentOrder.get(thisSoId).Actual_Admin_Fee__c =  (Decimal) a.get('actualAdmin');
                                                      shipmentOrder.get(thisSoId).Actual_Bank_Fees__c =  (Decimal) a.get('actualBank');
                                                      shipmentOrder.get(thisSoId).Actual_Duties_and_Taxes_Other__c =  (Decimal) a.get('actualTaxOther')  ;
                                                      shipmentOrder.get(thisSoId).Actual_Finance_Fee__c =  (Decimal) a.get('actualFinance');
                                                      shipmentOrder.get(thisSoId).Actual_Insurance_Fee_USD__c = (Decimal) a.get('actualInsurance')  ;
                                                      shipmentOrder.get(thisSoId).Actual_International_Delivery_Fee__c =  (Decimal) a.get('actualFreight') ;    
                                                      shipmentOrder.get(thisSoId).Actual_Miscellaneous_Fee__c =  (Decimal) a.get('actualmisc');
                                                      shipmentOrder.get(thisSoId).Actual_Total_Clearance_Costs__c =  (Decimal) a.get('actualClearance') ;
                                                      shipmentOrder.get(thisSoId).Actual_Total_Customs_Brokerage_Cost__c =  (Decimal) a.get('actualBrokerage') ;
                                                      shipmentOrder.get(thisSoId).Actual_Total_Handling_Costs__c =  (Decimal) a.get('actualHandling') ;
                                                      shipmentOrder.get(thisSoId).Actual_Total_IOR_EOR__c =  (Decimal) a.get('actualIEOR')  ;
                                                      shipmentOrder.get(thisSoId).Actual_Total_License_Cost__c =  (Decimal) a.get('actualLicense') ;
                                                      shipmentOrder.get(thisSoId).Actual_Total_Tax_and_Duty__c =  (Decimal) a.get('actualTax') ;    
                                                      shipmentOrder.get(thisSoId).Cost_of_Sale_Ad_Hoc_Fees__c =  (Decimal) a.get('costAdHoc') ;
                                                      shipmentOrder.get(thisSoId).Cost_of_Sale_Bank_Charges_Recovered__c =  (Decimal) a.get('costBank') ;
                                                      shipmentOrder.get(thisSoId).Cost_of_Sale_Exporter_of_Record__c =  (Decimal) a.get('costEOR') ;
                                                      shipmentOrder.get(thisSoId).Cost_of_Sale_Financing_and_Insurance__c =  (Decimal) a.get('costFinancing') ;
                                                      shipmentOrder.get(thisSoId).Cost_of_Sale_Importer_of_Record__c =  (Decimal) a.get('costIOR') ;
                                                      shipmentOrder.get(thisSoId).Cost_of_Sale_Insurance_Fees__c =  (Decimal) a.get('costInsurance') ;
                                                      shipmentOrder.get(thisSoId).Cost_of_Sale_On_Charges__c =  (Decimal) a.get('costOnCharge') ;    
                                                      shipmentOrder.get(thisSoId).Cost_of_Sale_Shipping_Insurance__c =  (Decimal) a.get('costShipping') ;
                                                      shipmentOrder.get(thisSoId).Cost_of_Sale_Tax__c =  (Decimal) a.get('costTax') ;      
                                                      shipmentOrder.get(thisSoId).Total_Value_Applied_on_Supplier_Invoice__c =  (Decimal) a.get('totalApplied'); 
                                                      
                                                      
                                                  }
                                                  
                                                  if(a.get('RecordTypeId') == zeeInvoiceRecordTypeId || a.get('RecordTypeId') == Schema.Sobjecttype.Invoice_New__c.getRecordTypeInfosByDeveloperName().get('Client').getRecordTypeId())
                                                  { 
                                                      Decimal taxValue = (Decimal) a.get('tax') != null? (Decimal) a.get('tax'): 0;
                                                      Decimal taxOtherValue = (Decimal) a.get('taxOther') != null? (Decimal) a.get('taxOther'): 0;
                                                      Decimal finalTax = taxValue + taxOtherValue;
                                                      Decimal rate = invoice.Conversion_Rate__c;
                                                      
                                                      shipmentOrder.get(thisSoId).CI_Admin_Fee__c =  (Decimal) a.get('adminFee');
                                                      shipmentOrder.get(thisSoId).CI_Bank_Fees__c =  (Decimal) a.get('bankFee');
                                                      shipmentOrder.get(thisSoId).CI_Cash_Outlay_Fee__c =  (Decimal) a.get('cashOut');
                                                      shipmentOrder.get(thisSoId).CI_Collection_Administration_Fee__c =  (Decimal) a.get('collectionAdmin');
                                                      shipmentOrder.get(thisSoId).CI_IOR_and_Import_Compliance_Fee_USD__c = (Decimal) a.get('ior');
                                                      shipmentOrder.get(thisSoId).CI_EOR_and_Export_Compliance_Fee_USD__c = (Decimal) a.get('eor');
                                                      shipmentOrder.get(thisSoId).CI_International_Delivery_Fee__c =  (Decimal) a.get('freight'); 
                                                      shipmentOrder.get(thisSoId).CI_Liability_Cover__c =  (Decimal) a.get('liability');
                                                      shipmentOrder.get(thisSoId).CI_Miscellaneous_Fee__c =  (Decimal) a.get('misc');
                                                      shipmentOrder.get(thisSoId).CI_Recharge_Tax_and_Duty__c =  finalTax ;
                                                      shipmentOrder.get(thisSoId).CI_Total_Clearance_Costs__c =  (Decimal) a.get('clearance');
                                                      shipmentOrder.get(thisSoId).CI_Total_Customs_Brokerage_Cost__c = (Decimal) a.get('brokerage');
                                                      shipmentOrder.get(thisSoId).CI_Total_Handling_Cost__c = (Decimal) a.get('handling');
                                                      shipmentOrder.get(thisSoId).CI_Total_Licence_Cost__c = (Decimal) a.get('license');
                                                      shipmentOrder.get(thisSoId).Revenue_Ad_Hoc_Fees__c =  (Decimal) a.get('revAdHoc');
                                                      shipmentOrder.get(thisSoId).Revenue_Bank_Charges_Recovered__c =  (Decimal) a.get('revBank');
                                                      shipmentOrder.get(thisSoId).Revenue_Exporter_of_Record__c = (Decimal) a.get('revEOR');
                                                      shipmentOrder.get(thisSoId).Revenue_Financing_and_Insurance__c =  (Decimal) a.get('revFinancing');
                                                      shipmentOrder.get(thisSoId).Revenue_Importer_of_Record__c =  (Decimal) a.get('revIOR');
                                                      shipmentOrder.get(thisSoId).Revenue_Insurance_Fees__c =  (Decimal) a.get('revInsurance');
                                                      shipmentOrder.get(thisSoId).Revenue_On_Charges__c =  (Decimal) a.get('revOnCharge');    
                                                      shipmentOrder.get(thisSoId).Revenue_Shipping_Insurance__c =  (Decimal) a.get('revShipping');
                                                      shipmentOrder.get(thisSoId).Revenue_Tax__c =  (Decimal) a.get('revTax');    
                                                      shipmentOrder.get(thisSoId).Total_Value_Applied_on_Customer_Invoice__c =  (Decimal) a.get('totalApplied'); 
                                                      
                                                      
                                                  }
                                                  If(!shipmentToUpdate.contains(shipmentOrder.get(thisSoId))){
                                                      shipmentToUpdate.add(shipmentOrder.get(thisSoId));
                                                  }
                                                  
                                                  
                                              }     
                                              
                                              
                                              
                                              
                                          }
                                          
                                          
                                      }   
       /* if(!populateInvoiceIds.IsEmpty()){
            CTRL_generateAndSendInvoice.SaveInvoiceUsingTrigger(populateInvoiceIds);
        }*/
        
        insert transactionLines;
        
        if(ShipmentOrderTriggerHandler.IsZeeInvoiceProcessfromSO == true){
            ShipmentOrderTriggerHandler.soMapFromInvoiceTrigger = new Map<Id, Shipment_Order__c>(shipmentToUpdate);
        }
        else{
            update shipmentToUpdate;
        }   
        
        
        
    } 
    
    If(Trigger.IsUpdate && Trigger.IsBefore){
        if(ShipmentOrderTriggerHandler.isRecurssion){
        return;
    }
        
        Decimal posTls;
        Decimal negTls;
        
        Id zeeInvoiceRecordTypeId = Schema.Sobjecttype.Invoice_New__c.getRecordTypeInfosByDeveloperName().get('Client_Zee').getRecordTypeId();        

        Map<Id, Shipment_Order__c> shipmentOrder = new Map<Id,Shipment_Order__c>([Select Id, Name, Handling_and_Admin_Fee__c, Account__c, Bank_Fees__c, Total_Customs_Brokerage_Cost1__c,
                                                                                  Total_clearance_Costs__c, Recharge_Handling_Costs__c, Total_License_Cost__c, EOR_and_Export_Compliance_Fee_USD__c,
                                                                                  IOR_FEE_USD__c, International_Delivery_Fee__c, Total_Invoice_Amount__c, Insurance_Fee_USD__c, Miscellaneous_Fee__c,
                                                                                  Miscellaneous_Fee_Name__c, Recharge_Tax_and_Duty_Other__c, Recharge_Tax_and_Duty__c, POD_Date__c, 
                                                                                  Account__r.Default_invoicing_currency__c, Account__r.IOR_Payment_Terms__c, Potential_Cash_Outlay_Fee__c,
                                                                                  Total_Value_Applied_on_Customer_Invoice__c, Total_Value_Applied_on_Supplier_Invoice__c 
                                                                                  From Shipment_Order__c
                                                                                  Where Id in :shipmentOrderIds]);  
        
        
        List<Shipment_Order__c> shipmentOrderUpdateCN = new List<Shipment_Order__c>();
        
        List<Shipment_Order__c> appliedSO = new List<Shipment_Order__c>();
        
        
        Map<Id, Freight__c> freightRequestMap = new Map<Id,Freight__c>([Select Id, Name, Populate_Invoice__c
                                                                        From Freight__c
                                                                        Where Id in :freightIds]);
        
        List<Freight__c> freightRequestUpdateCN = new List<Freight__c>();
        
        
        
        List<Transaction_Line_New__c> transactionLines = new List<Transaction_Line_New__c>();
        
        List<Transaction_Line_New__c> existingTLUpdate = new List<Transaction_Line_New__c>();
        
        List<Invoice_New__c> creditNotes = new List<Invoice_New__c>();
        
        List<Applied_Credit_Note__c> appliedCreditNotes = new List<Applied_Credit_Note__c>();
        
        Map<Id, Transaction_Line_New__c> existingTL = new Map<Id, Transaction_Line_New__c>([Select Id, Name, Absolute_Amount__c, Accounting_Period_New__c, Amount__c, Analytical_Dimension_1__c, 
                                                                                            Analytical_Dimension_2__c, Analytical_Dimension_3__c, Bank_Transaction__c, Bank_Transaction_Applied__c, 
                                                                                            Business_Partner__c, Conversion_Rate__c, Currency__c, Customer_Supplier__c, Date__c, Description__c, 
                                                                                            Dimension_1__c, Dimension_2__c, Dimension_3__c, Document_Number__c, Freight_Request__c, Freight_Request_Name__c, 
                                                                                            GL_Account__c, Invoice__c, Invoice_Aging__c, Invoice_amount_USD__c, Invoice_Type__c, Payment_Method__c, Reference__c, 
                                                                                            Shipment_Order__c, Shipment_Order_Name__c, Short_Description__c, Sign__c, Site__c, Status__c, Revenue_Type__c, Type_Code__c, 
                                                                                            RecordtypeId
                                                                                            From Transaction_Line_New__c
                                                                                            Where Invoice__c in :invoiceIds]);
        
        
        
        
        
        Map<Id, CPA_Costing__c> costingsMap = new Map<Id, CPA_Costing__c>([Select Id, Name, Exchange_rate_payment_date__c,Within_Threshold__c , Invoice_amount_local_currency__c,Invoice__c
                                                                           From CPA_Costing__c
                                                                           Where Invoice__c in :invoiceIds]); 
        
        List<CPA_Costing__c> costingsToEdit = new List<CPA_Costing__c>();
        
        
        Map<Id, Currency_Management2__c> currencyMap = new Map<Id, Currency_Management2__c>([Select Id, Name, Conversion_Rate__c, Currency__c, ISO_Code__c
                                                                                             From Currency_Management2__c]); 
        
        Map<String, Decimal> conversionRate = new Map<String, Decimal>();
        
        For (Id getRates : currencyMap.keySet()) {conversionRate.put(currencyMap.get(getRates).Name,currencyMap.get(getRates).Conversion_Rate__c );}     
        
        Map<Id, Accounting_Period__c> accPeriodMap = new Map<Id, Accounting_Period__c>([Select Id, Name From Accounting_Period__c Where Status__c = 'Open']); 
        
        Map<String, Id> accPeriod = new Map<String, Id>();  
        
        For (Id getAccPeriod : accPeriodMap.keySet()) { accPeriod.put(accPeriodMap.get(getAccPeriod).Name, getAccPeriod ); } 
        
        
        Map<Id, Dimension__c> dimensionMap = new Map<Id, Dimension__c>([Select Id, Name From Dimension__c]);
        
        Map<String, Id> dimension = new Map<String, Id>(); 
        
        For (Id getDimension : dimensionMap.keySet()) { dimension.put(dimensionMap.get(getDimension).Name, getDimension );} 
        
        Map<Id, GL_Account__c> glAccountMap = new Map<Id, GL_Account__c>([Select Id, Name, GL_Account_Description__c,GL_Account_Short_Description__c,Currency__c From GL_Account__c]); 
        
        
        Map<String, Id> glAccount = new Map<String, Id>(); 
        
        For (Id getGlAccount : glAccountMap.keySet()) { glAccount.put(glAccountMap.get(getGlAccount).Name, getGlAccount ); } 
        
        Map<Id, Account> accountMap = new Map<Id,Account>([Select Id,RecordType.Name, Name, Region__c, Dimension__c, Invoicing_Term_Parameters__c, IOR_Payment_Terms__c, Invoice_Timing__c
                                                           From Account
                                                           Where Id in :accountIds]);
        
        Map<Id,List<CPA_Costing__c>> costingsPerInvoice = new Map<Id,List<CPA_Costing__c>>();
        
        for(CPA_Costing__c costing:costingsMap.values()){
            List<CPA_Costing__c> costingList = new List<CPA_Costing__c>();
            if(costingsPerInvoice.containsKey(costing.Invoice__c)){
                costingList = costingsPerInvoice.get(costing.Invoice__c);
            }
            costingList.add(costing);
            costingsPerInvoice.put(costing.Invoice__c,costingList);
        }
        
        /* Boolean costingsWithinThreshold = true;
if(costingsMap!=null && !costingsMap.isEmpty()){
for(CPA_Costing__c costing:costingsMap.values()){
if(costing.Within_Threshold__c == 'No'){
costingsWithinThreshold = false;
}
}  
}else{
costingsWithinThreshold = false;
}
*/
        For (Invoice_New__c invoice : Trigger.new) {
            
            
            
            List<CPA_Costing__c> costingList = costingsPerInvoice.get(invoice.Id);
            Boolean costingsWithinThreshold = true;
            if(costingList!=null && !costingList.isEmpty()){
                for(CPA_Costing__c costing:costingList){
                    if(costing.Within_Threshold__c == null ||  costing.Within_Threshold__c == 'No'){
                        costingsWithinThreshold = false;
                    }
                }  
            }else{
                costingsWithinThreshold = false;
            }
            
            String clientRTypeId = Schema.Sobjecttype.Invoice_New__c.getRecordTypeInfosByDeveloperName().get('Client').getRecordTypeId();
            String supplierRTypeId = Schema.Sobjecttype.Invoice_New__c.getRecordTypeInfosByDeveloperName().get('Supplier').getRecordTypeId();
            String dimension3 = accountMap.get(invoice.Account__c).Region__c; 
            String dimension1 = accountMap.get(invoice.Account__c).Dimension__c; 
            updateDueDateForSupplier(invoice);
            
            Decimal debitTotal = invoice.Revenue_Ad_Hoc_Fees__c + invoice.Revenue_Bank_Charges_Recovered__c + invoice.Shipment_Value_Invoice_Line__c + invoice.Revenue_Exporter_of_Record__c + invoice.Revenue_Financing_and_Insurance__c + invoice.Revenue_Importer_of_Record__c + invoice.Revenue_Insurance_Fees__c + invoice.Revenue_On_Charges__c + invoice.Revenue_Shipping_Insurance__c + invoice.Revenue_Tax__c;
            Decimal creditTotal = (invoice.Cost_of_Sale_Ad_Hoc_Fees__c + invoice.Shipment_Value_Invoice_Line__c + invoice.Cost_of_Sale_Bank_Charges_Recovered__c + invoice.Cost_of_Sale_Exporter_of_Record__c + invoice.Cost_of_Sale_Financing_and_Insurance__c + invoice.Cost_of_Sale_Importer_of_Record__c + invoice.Cost_of_Sale_Insurance_Fees__c + invoice.Cost_of_Sale_On_Charges__c + invoice.Cost_of_Sale_Shipping_Insurance__c + invoice.Cost_of_Sale_Tax__c) * -1;
            Decimal transactionTotal = invoice.Revenue_Ad_Hoc_Fees__c + invoice.Revenue_Bank_Charges_Recovered__c  + invoice.Revenue_Exporter_of_Record__c +
                invoice.Revenue_Financing_and_Insurance__c  + invoice.Revenue_Importer_of_Record__c  + invoice.Revenue_Tax__c +
                invoice.Revenue_Shipping_Insurance__c  + invoice.Revenue_On_Charges__c + invoice.Revenue_Insurance_Fees__c + invoice.Shipment_Value_Invoice_Line__c ;
            Decimal transactionTotalSI = invoice.Cost_of_Sale_Ad_Hoc_Fees__c + invoice.Cost_of_Sale_Bank_Charges_Recovered__c  + invoice.Cost_of_Sale_Exporter_of_Record__c +
                invoice.Cost_of_Sale_Financing_and_Insurance__c + invoice.Cost_of_Sale_Importer_of_Record__c   + invoice.Cost_of_Sale_Tax__c  +
                invoice.Cost_of_Sale_Shipping_Insurance__c  + invoice.Cost_of_Sale_On_Charges__c  + invoice.Cost_of_Sale_Insurance_Fees__c + invoice.Shipment_Value_Invoice_Line__c;
            
            Date maxDate = (invoice.POD_Date_New__c != null && invoice.POD_Date_New__c > invoice.Invoice_Sent_Date__c ) ? 
                invoice.POD_Date_New__c : 
            (invoice.Invoice_Sent_Date__c == null ? date.today() : invoice.Invoice_Sent_Date__c);
                
                
                //Auto review of supplier invoices                   
                if(!invoice.Auto_Reviewed_Override__c 
                   && costingsWithinThreshold 
                   && invoice.Auto_Pay__c == 'Yes' 
                   && (invoice.Invoice_Status__c == 'New' || invoice.Invoice_Status__c == 'FC Approved') 
                   && invoice.Invoice_Amount_Local_Currency__c == invoice.Total_Invoice_Amount_per_Document_LC__c ){
                       invoice.Auto_Reviewed__c = true;
                       invoice.Payment_Tracking__c = 'Ready For Payment';
                       invoice.Invoice_Status__c = 'FM Approved';
                       //System.debug('Auto review of supplier invoices true ');
                   }
            /*else{
                       invoice.Auto_Reviewed__c = false;
                       
                   }*/
            //Assign Accounting Period
            If((invoice.POD_Date_New__c != null &&  invoice.Accounting_Period_new__c == null) || invoice.POD_Date_New__c != trigger.oldMap.get(invoice.ID).POD_Date_New__c) {
                
                invoice.Accounting_Period_new__c =  accPeriod.get(invoice.Accounting_Period_Formula__c);
            }
            
            
            
            // Update TL Invoices
            If((invoice.Invoice_Status__c == 'Invoice Sent' || invoice.Invoice_Status__c == 'FM Approved' || invoice.Invoice_Status__c == 'Released From Bank' || invoice.Invoice_Status__c == 'Partial Payment' || invoice.Invoice_Status__c == 'Paid'|| invoice.Invoice_Status__c == 'Paid by Tipalti') &&
               invoice.POD_Date_New__c != trigger.oldMap.get(invoice.ID).POD_Date_New__c && invoice.POD_Date_New__c != null){ 
                   
                      
                   
                   For (Id currentTL : existingTL.keySet()) {  
                       
                       If(existingTL.get(currentTL).Invoice__c == invoice.id && existingTL.get(currentTL).Type_Code__c != 'Penalty' && existingTL.get(currentTL).Status__c != 'Posted'){
                           
                           existingTL.get(currentTL).Accounting_Period_New__c = accPeriod.get(invoice.Accounting_Period_Formula__c);
                           existingTL.get(currentTL).Date__c = maxDate;
                           existingTL.get(currentTL).Status__c = 'Posted';
                           existingTLUpdate.add(existingTL.get(currentTL));}}}
            
            
            //  Get ConversionRate Invoice
            If(invoice.Conversion_Rate__c == null || invoice.Invoice_Currency__c != trigger.oldMap.get(invoice.ID).Invoice_Currency__c){
                
                
                invoice.Conversion_Rate__c = conversionRate.get(invoice.Invoice_Currency__c);
                invoice.Invoice_amount_USD__c = conversionRate.get(invoice.Invoice_Currency__c) * invoice.Invoice_Amount_Local_Currency__c;}
            
            
            //Create CI Transaction Lines In Process
            If((invoice.RecordTypeId == clientRTypeId || invoice.RecordTypeId == zeeInvoiceRecordTypeId) &&  invoice.Invoice_Status__c == 'Invoice Sent' && 
               invoice.POD_Date__c == null && invoice.Transaction_Lines_Created__c == false){  
                   
                   
                   invoice.Invoice_Sent_Date__c = invoice.Invoice_Sent_Date__c==null ? date.today() : invoice.Invoice_Sent_Date__c;
                   
                   If(invoice.Invoice_amount_USD__c != 0){
                       Transaction_Line_New__c ciTL = new Transaction_Line_New__c(
                           Amount__c = debitTotal ,   
                           Business_Partner__c = invoice.Account__c,
                           Description__c = 'Customer Control Account',
                           Dimension_1__c = dimension1,
                           Dimension_3__c = dimension.get(dimension3),
                           GL_Account__c = glAccount.get('610000'),
                           Invoice__c = invoice.Id,
                           RecordTypeId = Schema.Sobjecttype.Transaction_Line_New__c.getRecordTypeInfosByDeveloperName().get('Invoice').getRecordTypeId(),
                           Revenue_Type__c = 'Actual',
                           Short_Description__c = 'AR Control',
                           Status__c ='In Process',
                           Type_Code__c = 'CI');
                       transactionLines.add(ciTL);}
                   
                   If(invoice.Revenue_Ad_Hoc_Fees__c != 0){
                       Transaction_Line_New__c rAd = new Transaction_Line_New__c(
                           Amount__c = invoice.Revenue_Ad_Hoc_Fees__c * -1,   
                           Business_Partner__c = invoice.Account__c,
                           Description__c = 'Revenue',
                           Dimension_1__c = dimension1,
                           Dimension_2__c = dimension.get('ADHOC'),
                           Dimension_3__c = dimension.get(dimension3),
                           GL_Account__c = glAccount.get('110000'),
                           Invoice__c = invoice.Id,
                           RecordTypeId = Schema.Sobjecttype.Transaction_Line_New__c.getRecordTypeInfosByDeveloperName().get('Invoice').getRecordTypeId(),
                           Revenue_Type__c = 'Actual',
                           Short_Description__c = 'Revenue',
                           Status__c ='In Process',
                           Type_Code__c = 'CI');
                       transactionLines.add(rAd);}
                   
                   /*  If(invoice.Revenue_Bank_Charges_Recovered__c != 0){
Transaction_Line_New__c rBank = new Transaction_Line_New__c(
Amount__c = invoice.Revenue_Bank_Charges_Recovered__c * -1,   
Business_Partner__c = invoice.Account__c,
Description__c = 'Revenue',
Dimension_1__c = dimension1,
Dimension_2__c = dimension.get('BANKCHARGE'),
Dimension_3__c = dimension.get(dimension3),
GL_Account__c = glAccount.get('110000'),
Invoice__c = invoice.Id,
RecordTypeId = Schema.Sobjecttype.Transaction_Line_New__c.getRecordTypeInfosByDeveloperName().get('Invoice').getRecordTypeId(),
Revenue_Type__c = 'Actual',
Short_Description__c = 'Revenue',
Status__c ='In Process',
Type_Code__c = 'CI');
transactionLines.add(rBank);            
}   */
                   
                   
                   If(invoice.Revenue_Exporter_of_Record__c != 0){
                       Transaction_Line_New__c rEOR = new Transaction_Line_New__c(
                           Amount__c = invoice.Revenue_Exporter_of_Record__c * -1,   
                           Business_Partner__c = invoice.Account__c,
                           Description__c = 'Revenue',
                           Dimension_1__c = dimension1,
                           Dimension_2__c = dimension.get('EOR'),
                           Dimension_3__c = dimension.get(dimension3),
                           GL_Account__c = glAccount.get('110000'),
                           Invoice__c = invoice.Id,
                           RecordTypeId = Schema.Sobjecttype.Transaction_Line_New__c.getRecordTypeInfosByDeveloperName().get('Invoice').getRecordTypeId(),
                           Revenue_Type__c = 'Actual',
                           Short_Description__c = 'Revenue',
                           Status__c ='In Process',
                           Type_Code__c = 'CI');
                       transactionLines.add(rEOR);            
                   }   
                   
                   If(invoice.Revenue_Financing_and_Insurance__c != 0){
                       Transaction_Line_New__c rFinance = new Transaction_Line_New__c(
                           Amount__c = invoice.Revenue_Financing_and_Insurance__c * -1,   
                           Business_Partner__c = invoice.Account__c,
                           Description__c = 'Revenue',
                           Dimension_1__c = dimension1,
                           Dimension_2__c = dimension.get('FININSURANCE'),
                           Dimension_3__c = dimension.get(dimension3),
                           GL_Account__c = glAccount.get('110000'),
                           Invoice__c = invoice.Id,
                           RecordTypeId = Schema.Sobjecttype.Transaction_Line_New__c.getRecordTypeInfosByDeveloperName().get('Invoice').getRecordTypeId(),
                           Revenue_Type__c = 'Actual',
                           Short_Description__c = 'Revenue',
                           Status__c ='In Process',
                           Type_Code__c = 'CI');
                       transactionLines.add(rFinance);            
                   }    
                   
                   
                   If(invoice.Revenue_Importer_of_Record__c != 0){
                       Transaction_Line_New__c rIOR = new Transaction_Line_New__c(
                           Amount__c = invoice.Revenue_Importer_of_Record__c * -1,   
                           Business_Partner__c = invoice.Account__c,
                           Description__c = 'Revenue',
                           Dimension_1__c = dimension1,
                           Dimension_2__c = dimension.get('IOR'),
                           Dimension_3__c = dimension.get(dimension3),
                           GL_Account__c = glAccount.get('110000'),
                           Invoice__c = invoice.Id,
                           RecordTypeId = Schema.Sobjecttype.Transaction_Line_New__c.getRecordTypeInfosByDeveloperName().get('Invoice').getRecordTypeId(),
                           Revenue_Type__c = 'Actual',
                           Short_Description__c = 'Revenue',
                           Status__c ='In Process',
                           Type_Code__c = 'CI');
                       transactionLines.add(rIOR);            
                   }       
                   
                   
                   If(invoice.Revenue_Tax__c != 0){
                       Transaction_Line_New__c RtAX = new Transaction_Line_New__c(
                           Amount__c = invoice.Revenue_Tax__c * -1,   
                           Business_Partner__c = invoice.Account__c,
                           Description__c = 'Revenue - Client taxes paid',
                           Dimension_1__c = dimension1,
                           Dimension_2__c = dimension.get('TAX'),
                           Dimension_3__c = dimension.get(dimension3),
                           GL_Account__c = glAccount.get('110151'),
                           Invoice__c = invoice.Id,
                           RecordTypeId = Schema.Sobjecttype.Transaction_Line_New__c.getRecordTypeInfosByDeveloperName().get('Invoice').getRecordTypeId(),
                           Revenue_Type__c = 'Actual',
                           Short_Description__c = 'Revenue-ClientTax',
                           Status__c ='In Process',
                           Type_Code__c = 'CI');
                       transactionLines.add(RtAX);            
                   }    
                   
                   If(invoice.Shipment_Value_Invoice_Line__c != 0){
                       Transaction_Line_New__c RtAX = new Transaction_Line_New__c(
                           Amount__c = invoice.Shipment_Value_Invoice_Line__c * -1,   
                           Business_Partner__c = invoice.Account__c,
                           Description__c = 'Cash Book Suspense',
                           Dimension_1__c = dimension1,
                           Dimension_3__c = dimension.get(dimension3),
                           GL_Account__c = glAccount.get('914500'),
                           Invoice__c = invoice.Id,
                           RecordTypeId = Schema.Sobjecttype.Transaction_Line_New__c.getRecordTypeInfosByDeveloperName().get('Invoice').getRecordTypeId(),
                           Revenue_Type__c = 'Actual',
                           Short_Description__c = 'Cash Book Suspense',
                           Status__c ='In Process',
                           Type_Code__c = 'CI');
                       transactionLines.add(RtAX);            
                   }    
                   
                   
                   If(invoice.Revenue_Shipping_Insurance__c != 0){
                       Transaction_Line_New__c rShip = new Transaction_Line_New__c(
                           Amount__c = invoice.Revenue_Shipping_Insurance__c * -1,   
                           Business_Partner__c = invoice.Account__c,
                           Description__c = 'Revenue',
                           Dimension_1__c = dimension1,
                           Dimension_2__c = dimension.get('SHIPPING'),
                           Dimension_3__c = dimension.get(dimension3),
                           GL_Account__c = glAccount.get('110000'),
                           Invoice__c = invoice.Id,
                           RecordTypeId = Schema.Sobjecttype.Transaction_Line_New__c.getRecordTypeInfosByDeveloperName().get('Invoice').getRecordTypeId(),
                           Revenue_Type__c = 'Actual',
                           Short_Description__c = 'Revenue',
                           Status__c ='In Process',
                           Type_Code__c = 'CI');
                       transactionLines.add(rShip);            
                   }    
                   
                   
                   If(invoice.Revenue_On_Charges__c != 0){
                       Transaction_Line_New__c rOnC = new Transaction_Line_New__c(
                           Amount__c = invoice.Revenue_On_Charges__c * -1,   
                           Business_Partner__c = invoice.Account__c,
                           Description__c = 'Revenue',
                           Dimension_1__c = dimension1,
                           Dimension_2__c = dimension.get('ONCHARGES'),
                           Dimension_3__c = dimension.get(dimension3),
                           GL_Account__c = glAccount.get('110000'),
                           Invoice__c = invoice.Id,
                           RecordTypeId = Schema.Sobjecttype.Transaction_Line_New__c.getRecordTypeInfosByDeveloperName().get('Invoice').getRecordTypeId(),
                           Revenue_Type__c = 'Actual',
                           Short_Description__c = 'Revenue',
                           Status__c ='In Process',
                           Type_Code__c = 'CI');
                       transactionLines.add(rOnC);            
                   }    
                   
                   
                   /*  If(invoice.Revenue_Insurance_Fees__c != 0){
Transaction_Line_New__c rIns = new Transaction_Line_New__c(
Amount__c = invoice.Revenue_Insurance_Fees__c * -1,   
Business_Partner__c = invoice.Account__c,
Description__c = 'Revenue',
Dimension_1__c = dimension1,
Dimension_2__c = dimension.get('INSURANCE'),
Dimension_3__c = dimension.get(dimension3),
GL_Account__c = glAccount.get('110000'),
Invoice__c = invoice.Id,
RecordTypeId = Schema.Sobjecttype.Transaction_Line_New__c.getRecordTypeInfosByDeveloperName().get('Invoice').getRecordTypeId(),
Revenue_Type__c = 'Actual',
Short_Description__c = 'Revenue',
Status__c ='In Process',
Type_Code__c = 'CI');
transactionLines.add(rIns); 
}    */
                   
                   
                   invoice.Transaction_Lines_Created__c = TRUE;
                   invoice.Transaction_Total__c = invoice.Transaction_Total__c == null? 0 + debitTotal: invoice.Transaction_Total__c + debitTotal;
                   invoice.Neg_Transaction_Total__c =  invoice.Neg_Transaction_Total__c +  (transactionTotal * -1);}
            
            
            
            //Create CI Transaction Lines Posted
            If(((invoice.RecordTypeId == clientRTypeId || invoice.RecordTypeId == zeeInvoiceRecordTypeId) &&  invoice.Invoice_Status__c == 'Invoice Sent' && 
                invoice.POD_Date__c != null && invoice.Transaction_Lines_Created__c == false)){  
                    
                    
                    invoice.Invoice_Sent_Date__c = invoice.Invoice_Sent_Date__c == null ? date.today() : invoice.Invoice_Sent_Date__c;
                    
                    If(invoice.Invoice_amount_USD__c != 0){
                        Transaction_Line_New__c ciTL = new Transaction_Line_New__c(
                            Amount__c = debitTotal ,   
                            Business_Partner__c = invoice.Account__c,
                            Description__c = 'Customer Control Account',
                            Accounting_Period_New__c = accPeriod.get(invoice.Accounting_Period_Formula__c),
                            Date__c = maxDate,
                            Dimension_1__c = dimension1,
                            Dimension_3__c = dimension.get(dimension3),
                            GL_Account__c = glAccount.get('610000'),
                            Invoice__c = invoice.Id,
                            RecordTypeId = Schema.Sobjecttype.Transaction_Line_New__c.getRecordTypeInfosByDeveloperName().get('Invoice').getRecordTypeId(),
                            Revenue_Type__c = 'Actual',
                            Short_Description__c = 'AR Control',
                            Status__c ='Posted',
                            Type_Code__c = 'CI');
                        transactionLines.add(ciTL);}
                    
                    If(invoice.Revenue_Ad_Hoc_Fees__c != 0){
                        Transaction_Line_New__c ciTL = new Transaction_Line_New__c(
                            Amount__c = invoice.Revenue_Ad_Hoc_Fees__c * -1,   
                            Business_Partner__c = invoice.Account__c,
                            Description__c = 'Revenue',
                            Accounting_Period_New__c = accPeriod.get(invoice.Accounting_Period_Formula__c),
                            Date__c = maxDate,
                            Dimension_1__c = dimension1,
                            Dimension_2__c = dimension.get('ADHOC'),
                            Dimension_3__c = dimension.get(dimension3),
                            GL_Account__c = glAccount.get('110000'),
                            Invoice__c = invoice.Id,
                            RecordTypeId = Schema.Sobjecttype.Transaction_Line_New__c.getRecordTypeInfosByDeveloperName().get('Invoice').getRecordTypeId(),
                            Revenue_Type__c = 'Actual',
                            Short_Description__c = 'Revenue',
                            Status__c ='Posted',
                            Type_Code__c = 'CI');
                        transactionLines.add(ciTL);}
                    
                    /*  If(invoice.Revenue_Bank_Charges_Recovered__c != 0){
Transaction_Line_New__c ciTL = new Transaction_Line_New__c(
Amount__c = invoice.Revenue_Bank_Charges_Recovered__c * -1,   
Business_Partner__c = invoice.Account__c,
Description__c = 'Revenue',
Accounting_Period_New__c = accPeriod.get(invoice.Accounting_Period_Formula__c),
Date__c = maxDate,
Dimension_1__c = dimension1,
Dimension_2__c = dimension.get('BANKCHARGE'),
Dimension_3__c = dimension.get(dimension3),
GL_Account__c = glAccount.get('110000'),
Invoice__c = invoice.Id,
RecordTypeId = Schema.Sobjecttype.Transaction_Line_New__c.getRecordTypeInfosByDeveloperName().get('Invoice').getRecordTypeId(),
Revenue_Type__c = 'Actual',
Short_Description__c = 'Revenue',
Status__c ='Posted',
Type_Code__c = 'CI');
transactionLines.add(ciTL);            
}   
*/
                    
                    If(invoice.Revenue_Exporter_of_Record__c != 0){
                        Transaction_Line_New__c ciTL = new Transaction_Line_New__c(
                            Amount__c = invoice.Revenue_Exporter_of_Record__c * -1,   
                            Business_Partner__c = invoice.Account__c,
                            Description__c = 'Revenue',
                            Accounting_Period_New__c = accPeriod.get(invoice.Accounting_Period_Formula__c),
                            Date__c = maxDate,
                            Dimension_1__c = dimension1,
                            Dimension_2__c = dimension.get('EOR'),
                            Dimension_3__c = dimension.get(dimension3),
                            GL_Account__c = glAccount.get('110000'),
                            Invoice__c = invoice.Id,
                            RecordTypeId = Schema.Sobjecttype.Transaction_Line_New__c.getRecordTypeInfosByDeveloperName().get('Invoice').getRecordTypeId(),
                            Revenue_Type__c = 'Actual',
                            Short_Description__c = 'Revenue',
                            Status__c ='Posted',
                            Type_Code__c = 'CI');
                        transactionLines.add(ciTL);            
                    }   
                    
                    If(invoice.Revenue_Financing_and_Insurance__c != 0){
                        Transaction_Line_New__c ciTL = new Transaction_Line_New__c(
                            Amount__c = invoice.Revenue_Financing_and_Insurance__c * -1,   
                            Business_Partner__c = invoice.Account__c,
                            Description__c = 'Revenue',
                            Accounting_Period_New__c = accPeriod.get(invoice.Accounting_Period_Formula__c),
                            Date__c = maxDate,
                            Dimension_1__c = dimension1,
                            Dimension_2__c = dimension.get('FININSURANCE'),
                            Dimension_3__c = dimension.get(dimension3),
                            GL_Account__c = glAccount.get('110000'),
                            Invoice__c = invoice.Id,
                            RecordTypeId = Schema.Sobjecttype.Transaction_Line_New__c.getRecordTypeInfosByDeveloperName().get('Invoice').getRecordTypeId(),
                            Revenue_Type__c = 'Actual',
                            Short_Description__c = 'Revenue',
                            Status__c ='Posted',
                            Type_Code__c = 'CI');
                        transactionLines.add(ciTL);            
                    }    
                    
                    
                    If(invoice.Revenue_Importer_of_Record__c != 0){
                        Transaction_Line_New__c ciTL = new Transaction_Line_New__c(
                            Amount__c = invoice.Revenue_Importer_of_Record__c * -1,   
                            Business_Partner__c = invoice.Account__c,
                            Description__c = 'Revenue',
                            Accounting_Period_New__c = accPeriod.get(invoice.Accounting_Period_Formula__c),
                            Date__c = maxDate,
                            Dimension_1__c = dimension1,
                            Dimension_2__c = dimension.get('IOR'),
                            Dimension_3__c = dimension.get(dimension3),
                            GL_Account__c = glAccount.get('110000'),
                            Invoice__c = invoice.Id,
                            RecordTypeId = Schema.Sobjecttype.Transaction_Line_New__c.getRecordTypeInfosByDeveloperName().get('Invoice').getRecordTypeId(),
                            Revenue_Type__c = 'Actual',
                            Short_Description__c = 'Revenue',
                            Status__c ='Posted',
                            Type_Code__c = 'CI');
                        transactionLines.add(ciTL);            
                    }       
                    
                    
                    If(invoice.Revenue_Tax__c != 0){
                        Transaction_Line_New__c ciTL = new Transaction_Line_New__c(
                            Amount__c = invoice.Revenue_Tax__c * -1,   
                            Business_Partner__c = invoice.Account__c,
                            Description__c = 'Revenue - Client taxes paid',
                            Accounting_Period_New__c = accPeriod.get(invoice.Accounting_Period_Formula__c),
                            Date__c = maxDate,
                            Dimension_1__c = dimension1,
                            Dimension_2__c = dimension.get('TAX'),
                            Dimension_3__c = dimension.get(dimension3),
                            GL_Account__c = glAccount.get('110151'),
                            Invoice__c = invoice.Id,
                            RecordTypeId = Schema.Sobjecttype.Transaction_Line_New__c.getRecordTypeInfosByDeveloperName().get('Invoice').getRecordTypeId(),
                            Revenue_Type__c = 'Actual',
                            Short_Description__c = 'Revenue-ClientTax',
                            Status__c ='Posted',
                            Type_Code__c = 'CI');
                        transactionLines.add(ciTL);            
                    }    
                    
                    If(invoice.Shipment_Value_Invoice_Line__c != 0){
                        Transaction_Line_New__c ciTL = new Transaction_Line_New__c(
                            Amount__c = invoice.Shipment_Value_Invoice_Line__c * -1,   
                            Business_Partner__c = invoice.Account__c,
                            Description__c = 'Cash Book Suspense',
                            Accounting_Period_New__c = accPeriod.get(invoice.Accounting_Period_Formula__c),
                            Date__c = maxDate,
                            Dimension_1__c = dimension1,
                            Dimension_3__c = dimension.get(dimension3),
                            GL_Account__c = glAccount.get('914500'),
                            Invoice__c = invoice.Id,
                            RecordTypeId = Schema.Sobjecttype.Transaction_Line_New__c.getRecordTypeInfosByDeveloperName().get('Invoice').getRecordTypeId(),
                            Revenue_Type__c = 'Actual',
                            Short_Description__c = 'Cash Book Suspense',
                            Status__c ='Posted',
                            Type_Code__c = 'CI');
                        transactionLines.add(ciTL);            
                    }    
                    
                    
                    If(invoice.Revenue_Shipping_Insurance__c != 0){
                        Transaction_Line_New__c ciTL = new Transaction_Line_New__c(
                            Amount__c = invoice.Revenue_Shipping_Insurance__c * -1,   
                            Business_Partner__c = invoice.Account__c,
                            Description__c = 'Revenue',
                            Accounting_Period_New__c = accPeriod.get(invoice.Accounting_Period_Formula__c),
                            Date__c = maxDate,
                            Dimension_1__c = dimension1,
                            Dimension_2__c = dimension.get('SHIPPING'),
                            Dimension_3__c = dimension.get(dimension3),
                            GL_Account__c = glAccount.get('110000'),
                            Invoice__c = invoice.Id,
                            RecordTypeId = Schema.Sobjecttype.Transaction_Line_New__c.getRecordTypeInfosByDeveloperName().get('Invoice').getRecordTypeId(),
                            Revenue_Type__c = 'Actual',
                            Short_Description__c = 'Revenue',
                            Status__c ='Posted',
                            Type_Code__c = 'CI');
                        transactionLines.add(ciTL);            
                    }    
                    
                    
                    If(invoice.Revenue_On_Charges__c != 0){
                        Transaction_Line_New__c ciTL = new Transaction_Line_New__c(
                            Amount__c = invoice.Revenue_On_Charges__c * -1,   
                            Business_Partner__c = invoice.Account__c,
                            Description__c = 'Revenue',
                            Accounting_Period_New__c = accPeriod.get(invoice.Accounting_Period_Formula__c),
                            Date__c = maxDate,
                            Dimension_1__c = dimension1,
                            Dimension_2__c = dimension.get('ONCHARGES'),
                            Dimension_3__c = dimension.get(dimension3),
                            GL_Account__c = glAccount.get('110000'),
                            Invoice__c = invoice.Id,
                            RecordTypeId = Schema.Sobjecttype.Transaction_Line_New__c.getRecordTypeInfosByDeveloperName().get('Invoice').getRecordTypeId(),
                            Revenue_Type__c = 'Actual',
                            Short_Description__c = 'Revenue',
                            Status__c ='Posted',
                            Type_Code__c = 'CI');
                        transactionLines.add(ciTL);            
                    }    
                    
                    
                    /* If(invoice.Revenue_Insurance_Fees__c != 0){
Transaction_Line_New__c ciTL = new Transaction_Line_New__c(
Amount__c = invoice.Revenue_Insurance_Fees__c * -1,   
Business_Partner__c = invoice.Account__c,
Description__c = 'Revenue',
Accounting_Period_New__c = accPeriod.get(invoice.Accounting_Period_Formula__c),
Date__c = maxDate,
Dimension_1__c = dimension1,
Dimension_2__c = dimension.get('INSURANCE'),
Dimension_3__c = dimension.get(dimension3),
GL_Account__c = glAccount.get('110000'),
Invoice__c = invoice.Id,
RecordTypeId = Schema.Sobjecttype.Transaction_Line_New__c.getRecordTypeInfosByDeveloperName().get('Invoice').getRecordTypeId(),
Revenue_Type__c = 'Actual',
Short_Description__c = 'Revenue',
Status__c ='Posted',
Type_Code__c = 'CI');
transactionLines.add(ciTL); } */
                    
                    
                    invoice.Transaction_Lines_Created__c = TRUE;
                    invoice.Transaction_Total__c = invoice.Transaction_Total__c == null? 0 + debitTotal: invoice.Transaction_Total__c + debitTotal;
                    invoice.Neg_Transaction_Total__c =   invoice.Neg_Transaction_Total__c +  (transactionTotal * -1);}
            
            
            //Unpost invoice TL
            If((invoice.POD_Date_New__c != trigger.oldMap.get(invoice.ID).POD_Date_New__c && invoice.POD_Date_New__c == null) || 
               (invoice.Accounting_Period_new__c != trigger.oldMap.get(invoice.ID).Accounting_Period_new__c && invoice.Accounting_Period_new__c == null)){  
                   
                    
                   
                   For (Id currentTL : existingTL.keySet()) { 
                       
                       if(existingTL.get(currentTL).Status__c != 'In Process'){ 
                           
                           existingTL.get(currentTL).Accounting_Period_New__c = accPeriod.get(invoice.Accounting_Period_Formula__c);
                           existingTL.get(currentTL).Date__c = invoice.POD_Date_New__c != null ? invoice.POD_Date_New__c : null;
                           existingTL.get(currentTL).Status__c = 'In Process';
                           existingTLUpdate.add(existingTL.get(currentTL));
                       } 
                   }
                   invoice.Accounting_Period_new__c = null;
                   invoice.Transaction_Total__c = invoice.Transaction_Total__c == null? 0 + debitTotal: invoice.Transaction_Total__c + debitTotal;
                   invoice.Neg_Transaction_Total__c = invoice.Neg_Transaction_Total__c +  (transactionTotal * -1);}
            
            
            
            
            //SI Transaction lines Creation
            If(invoice.RecordTypeId == supplierRTypeId && invoice.Invoice_Status__c != trigger.oldMap.get(invoice.ID).Invoice_Status__c && invoice.Invoice_Status__c == 'FM Approved' && 
               invoice.POD_Date__c == null && invoice.Transaction_Lines_Created__c == false){
                   
                   
                   invoice.Invoice_Sent_Date__c = invoice.Invoice_Sent_Date__c== null ? date.today(): invoice.Invoice_Sent_Date__c;
                   
                   If(invoice.Invoice_amount_USD__c != 0){
                       Transaction_Line_New__c ciTL = new Transaction_Line_New__c(
                           Amount__c = creditTotal,   
                           Business_Partner__c = invoice.Account__c,
                           Description__c = 'Supplier Control Account',
                           Dimension_1__c = dimension1,
                           Dimension_3__c = dimension.get(dimension3),
                           GL_Account__c = glAccount.get('920000'),
                           Invoice__c = invoice.Id,
                           RecordTypeId = Schema.Sobjecttype.Transaction_Line_New__c.getRecordTypeInfosByDeveloperName().get('Invoice').getRecordTypeId(),
                           Revenue_Type__c = 'Actual',
                           Short_Description__c = 'AP Control',
                           Status__c ='In Process',
                           Type_Code__c = 'SI');
                       transactionLines.add(ciTL);}
                   
                   If(invoice.Cost_of_Sale_Ad_Hoc_Fees__c != 0){
                       Transaction_Line_New__c ciTL = new Transaction_Line_New__c(
                           Amount__c = invoice.Cost_of_Sale_Ad_Hoc_Fees__c,   
                           Business_Partner__c = invoice.Account__c,
                           Description__c = 'Cost of Sales',
                           Dimension_1__c = dimension1,
                           Dimension_2__c = dimension.get('ADHOC'),
                           Dimension_3__c = dimension.get(dimension3),
                           GL_Account__c = glAccount.get('120000'),
                           Invoice__c = invoice.Id,
                           RecordTypeId = Schema.Sobjecttype.Transaction_Line_New__c.getRecordTypeInfosByDeveloperName().get('Invoice').getRecordTypeId(),
                           Revenue_Type__c = 'Actual',
                           Short_Description__c = 'Cost of Sales',
                           Status__c ='In Process',
                           Type_Code__c = 'SI');
                       transactionLines.add(ciTL);}
                   
                   If(invoice.Cost_of_Sale_Bank_Charges_Recovered__c != 0){
                       Transaction_Line_New__c ciTL = new Transaction_Line_New__c(
                           Amount__c = invoice.Cost_of_Sale_Bank_Charges_Recovered__c ,   
                           Business_Partner__c = invoice.Account__c,
                           Description__c = 'Cost of Sales',
                           Dimension_1__c = dimension1,
                           Dimension_2__c = dimension.get('BANKCHARGE'),
                           Dimension_3__c = dimension.get(dimension3),
                           GL_Account__c = glAccount.get('120000'),
                           Invoice__c = invoice.Id,
                           RecordTypeId = Schema.Sobjecttype.Transaction_Line_New__c.getRecordTypeInfosByDeveloperName().get('Invoice').getRecordTypeId(),
                           Revenue_Type__c = 'Actual',
                           Short_Description__c = 'Cost of Sales',
                           Status__c ='In Process',
                           Type_Code__c = 'SI');
                       transactionLines.add(ciTL);            
                   }   
                   
                   
                   If(invoice.Cost_of_Sale_Exporter_of_Record__c != 0){
                       Transaction_Line_New__c ciTL = new Transaction_Line_New__c(
                           Amount__c = invoice.Cost_of_Sale_Exporter_of_Record__c,   
                           Business_Partner__c = invoice.Account__c,
                           Description__c = 'Cost of Sales',
                           Dimension_1__c = dimension1,
                           Dimension_2__c = dimension.get('EOR'),
                           Dimension_3__c = dimension.get(dimension3),
                           GL_Account__c = glAccount.get('120000'),
                           Invoice__c = invoice.Id,
                           RecordTypeId = Schema.Sobjecttype.Transaction_Line_New__c.getRecordTypeInfosByDeveloperName().get('Invoice').getRecordTypeId(),
                           Revenue_Type__c = 'Actual',
                           Short_Description__c = 'Cost of Sales',
                           Status__c ='In Process',
                           Type_Code__c = 'SI');
                       transactionLines.add(ciTL);            
                   }   
                   
                   If(invoice.Cost_of_Sale_Financing_and_Insurance__c != 0){
                       Transaction_Line_New__c ciTL = new Transaction_Line_New__c(
                           Amount__c = invoice.Cost_of_Sale_Financing_and_Insurance__c,   
                           Business_Partner__c = invoice.Account__c,
                           Description__c = 'Cost of Sales',
                           Dimension_1__c = dimension1,
                           Dimension_2__c = dimension.get('FININSURANCE'),
                           Dimension_3__c = dimension.get(dimension3),
                           GL_Account__c = glAccount.get('120000'),
                           Invoice__c = invoice.Id,
                           RecordTypeId = Schema.Sobjecttype.Transaction_Line_New__c.getRecordTypeInfosByDeveloperName().get('Invoice').getRecordTypeId(),
                           Revenue_Type__c = 'Actual',
                           Short_Description__c = 'Cost of Sales',
                           Status__c ='In Process',
                           Type_Code__c = 'SI');
                       transactionLines.add(ciTL);            
                   }    
                   
                   
                   If(invoice.Cost_of_Sale_Importer_of_Record__c != 0){
                       Transaction_Line_New__c ciTL = new Transaction_Line_New__c(
                           Amount__c = invoice.Cost_of_Sale_Importer_of_Record__c,   
                           Business_Partner__c = invoice.Account__c,
                           Description__c = 'Cost of Sales',
                           Dimension_1__c = dimension1,
                           Dimension_2__c = dimension.get('IOR'),
                           Dimension_3__c = dimension.get(dimension3),
                           GL_Account__c = glAccount.get('120000'),
                           Invoice__c = invoice.Id,
                           RecordTypeId = Schema.Sobjecttype.Transaction_Line_New__c.getRecordTypeInfosByDeveloperName().get('Invoice').getRecordTypeId(),
                           Revenue_Type__c = 'Actual',
                           Short_Description__c = 'Cost of Sales',
                           Status__c ='In Process',
                           Type_Code__c = 'SI');
                       transactionLines.add(ciTL);            
                   }       
                   
                   
                   If(invoice.Cost_of_Sale_Tax__c != 0){
                       Transaction_Line_New__c ciTL = new Transaction_Line_New__c(
                           Amount__c = invoice.Cost_of_Sale_Tax__c,   
                           Business_Partner__c = invoice.Account__c,
                           Description__c = 'Revenue - Client taxes paid',
                           Dimension_1__c = dimension1,
                           Dimension_2__c = dimension.get('TAX'),
                           Dimension_3__c = dimension.get(dimension3),
                           GL_Account__c = glAccount.get('110151'),
                           Invoice__c = invoice.Id,
                           RecordTypeId = Schema.Sobjecttype.Transaction_Line_New__c.getRecordTypeInfosByDeveloperName().get('Invoice').getRecordTypeId(),
                           Revenue_Type__c = 'Actual',
                           Short_Description__c = 'Revenue-ClientTax',
                           Status__c ='In Process',
                           Type_Code__c = 'SI');
                       transactionLines.add(ciTL);            
                   }  
                   
                   If(invoice.Shipment_Value_Invoice_Line__c != 0){
                       Transaction_Line_New__c ciTL = new Transaction_Line_New__c(
                           Amount__c = invoice.Shipment_Value_Invoice_Line__c,   
                           Business_Partner__c = invoice.Account__c,
                           Description__c = 'Cash Book Suspense',
                           Dimension_1__c = dimension1,
                           Dimension_3__c = dimension.get(dimension3),
                           GL_Account__c = glAccount.get('914500'),
                           Invoice__c = invoice.Id,
                           RecordTypeId = Schema.Sobjecttype.Transaction_Line_New__c.getRecordTypeInfosByDeveloperName().get('Invoice').getRecordTypeId(),
                           Revenue_Type__c = 'Actual',
                           Short_Description__c = 'Cash Book Suspense',
                           Status__c ='In Process',
                           Type_Code__c = 'SI');
                       transactionLines.add(ciTL);            
                   }  
                   
                   
                   If(invoice.Cost_of_Sale_Shipping_Insurance__c != 0){
                       Transaction_Line_New__c ciTL = new Transaction_Line_New__c(
                           Amount__c = invoice.Cost_of_Sale_Shipping_Insurance__c,   
                           Business_Partner__c = invoice.Account__c,
                           Description__c = 'Cost of Sales',
                           Dimension_1__c = dimension1,
                           Dimension_2__c = dimension.get('SHIPPING'),
                           Dimension_3__c = dimension.get(dimension3),
                           GL_Account__c = glAccount.get('120000'),
                           Invoice__c = invoice.Id,
                           RecordTypeId = Schema.Sobjecttype.Transaction_Line_New__c.getRecordTypeInfosByDeveloperName().get('Invoice').getRecordTypeId(),
                           Revenue_Type__c = 'Actual',
                           Short_Description__c = 'Cost of Sales',
                           Status__c ='In Process',
                           Type_Code__c = 'SI');
                       transactionLines.add(ciTL);            
                   }    
                   
                   
                   If(invoice.Cost_of_Sale_On_Charges__c != 0){
                       Transaction_Line_New__c ciTL = new Transaction_Line_New__c(
                           Amount__c = invoice.Cost_of_Sale_On_Charges__c,   
                           Business_Partner__c = invoice.Account__c,
                           Description__c = 'Cost of Sales',
                           Dimension_1__c = dimension1,
                           Dimension_2__c = dimension.get('ONCHARGES'),
                           Dimension_3__c = dimension.get(dimension3),
                           GL_Account__c = glAccount.get('120000'),
                           Invoice__c = invoice.Id,
                           RecordTypeId = Schema.Sobjecttype.Transaction_Line_New__c.getRecordTypeInfosByDeveloperName().get('Invoice').getRecordTypeId(),
                           Revenue_Type__c = 'Actual',
                           Short_Description__c = 'Cost of Sales',
                           Status__c ='In Process',
                           Type_Code__c = 'SI');
                       transactionLines.add(ciTL);            
                   }    
                   
                   
                   If(invoice.Cost_of_Sale_Insurance_Fees__c != 0){
                       Transaction_Line_New__c ciTL = new Transaction_Line_New__c(
                           Amount__c = invoice.Cost_of_Sale_Insurance_Fees__c,   
                           Business_Partner__c = invoice.Account__c,
                           Description__c = 'Cost of Sales',
                           Dimension_1__c = dimension1,
                           Dimension_2__c = dimension.get('INSURANCE'),
                           Dimension_3__c = dimension.get(dimension3),
                           GL_Account__c = glAccount.get('120000'),
                           Invoice__c = invoice.Id,
                           RecordTypeId = Schema.Sobjecttype.Transaction_Line_New__c.getRecordTypeInfosByDeveloperName().get('Invoice').getRecordTypeId(),
                           Revenue_Type__c = 'Actual',
                           Short_Description__c = 'Cost of Sales',
                           Status__c ='In Process',
                           Type_Code__c = 'SI');
                       transactionLines.add(ciTL); } 
                   
                   
                   
                   invoice.Transaction_Lines_Created__c = TRUE;
                   invoice.Transaction_Total__c =  invoice.Transaction_Total__c + transactionTotalSI;
                   invoice.Neg_Transaction_Total__c =  invoice.Neg_Transaction_Total__c + creditTotal;}
            
            //Create SI Transaction Lines Posted
            If(invoice.RecordTypeId == supplierRTypeId && invoice.Invoice_Status__c != trigger.oldMap.get(invoice.ID).Invoice_Status__c && invoice.Invoice_Status__c == 'FM Approved' && 
               invoice.POD_Date__c != null && invoice.Transaction_Lines_Created__c == false){
                   
                   
                   
                   invoice.Invoice_Sent_Date__c = invoice.Invoice_Sent_Date__c==null ? 
                       date.today() : 
                   invoice.Invoice_Sent_Date__c ; 
                   
                   If(invoice.Invoice_amount_USD__c != 0){
                       Transaction_Line_New__c ciTL = new Transaction_Line_New__c(
                           Amount__c = creditTotal,
                           Accounting_Period_New__c = accPeriod.get(invoice.Accounting_Period_Formula__c),
                           Date__c = maxDate,
                           Business_Partner__c = invoice.Account__c,
                           Description__c = 'Supplier Control Account',
                           Dimension_1__c = dimension1,
                           Dimension_3__c = dimension.get(dimension3),
                           GL_Account__c = glAccount.get('920000'),
                           Invoice__c = invoice.Id,
                           RecordTypeId = Schema.Sobjecttype.Transaction_Line_New__c.getRecordTypeInfosByDeveloperName().get('Invoice').getRecordTypeId(),
                           Revenue_Type__c = 'Actual',
                           Short_Description__c = 'AP Control',
                           Status__c ='Posted',
                           Type_Code__c = 'SI');
                       transactionLines.add(ciTL);}
                   
                   If(invoice.Cost_of_Sale_Ad_Hoc_Fees__c != 0){
                       Transaction_Line_New__c ciTL = new Transaction_Line_New__c(
                           Amount__c = invoice.Cost_of_Sale_Ad_Hoc_Fees__c,  
                           Accounting_Period_New__c = accPeriod.get(invoice.Accounting_Period_Formula__c),
                           Date__c =maxDate,
                           Business_Partner__c = invoice.Account__c,
                           Description__c = 'Cost of Sales',
                           Dimension_1__c = dimension1,
                           Dimension_2__c = dimension.get('ADHOC'),
                           Dimension_3__c = dimension.get(dimension3),
                           GL_Account__c = glAccount.get('120000'),
                           Invoice__c = invoice.Id,
                           RecordTypeId = Schema.Sobjecttype.Transaction_Line_New__c.getRecordTypeInfosByDeveloperName().get('Invoice').getRecordTypeId(),
                           Revenue_Type__c = 'Actual',
                           Short_Description__c = 'Cost of Sales',
                           Status__c ='Posted',
                           Type_Code__c = 'SI');
                       transactionLines.add(ciTL);}
                   
                   If(invoice.Cost_of_Sale_Bank_Charges_Recovered__c != 0){
                       Transaction_Line_New__c ciTL = new Transaction_Line_New__c(
                           Amount__c = invoice.Cost_of_Sale_Bank_Charges_Recovered__c , 
                           Accounting_Period_New__c = accPeriod.get(invoice.Accounting_Period_Formula__c),
                           Date__c =maxDate,
                           Business_Partner__c = invoice.Account__c,
                           Description__c = 'Cost of Sales',
                           Dimension_1__c = dimension1,
                           Dimension_2__c = dimension.get('BANKCHARGE'),
                           Dimension_3__c = dimension.get(dimension3),
                           GL_Account__c = glAccount.get('120000'),
                           Invoice__c = invoice.Id,
                           RecordTypeId = Schema.Sobjecttype.Transaction_Line_New__c.getRecordTypeInfosByDeveloperName().get('Invoice').getRecordTypeId(),
                           Revenue_Type__c = 'Actual',
                           Short_Description__c = 'Cost of Sales',
                           Status__c ='Posted',
                           Type_Code__c = 'SI');
                       transactionLines.add(ciTL);            
                   }   
                   
                   
                   If(invoice.Cost_of_Sale_Exporter_of_Record__c != 0){
                       Transaction_Line_New__c ciTL = new Transaction_Line_New__c(
                           Amount__c = invoice.Cost_of_Sale_Exporter_of_Record__c,
                           Accounting_Period_New__c = accPeriod.get(invoice.Accounting_Period_Formula__c),
                           Date__c = maxDate,
                           Business_Partner__c = invoice.Account__c,
                           Description__c = 'Cost of Sales',
                           Dimension_1__c = dimension1,
                           Dimension_2__c = dimension.get('EOR'),
                           Dimension_3__c = dimension.get(dimension3),
                           GL_Account__c = glAccount.get('120000'),
                           Invoice__c = invoice.Id,
                           RecordTypeId = Schema.Sobjecttype.Transaction_Line_New__c.getRecordTypeInfosByDeveloperName().get('Invoice').getRecordTypeId(),
                           Revenue_Type__c = 'Actual',
                           Short_Description__c = 'Cost of Sales',
                           Status__c ='Posted',
                           Type_Code__c = 'SI');
                       transactionLines.add(ciTL);            
                   }   
                   
                   If(invoice.Cost_of_Sale_Financing_and_Insurance__c != 0){
                       Transaction_Line_New__c ciTL = new Transaction_Line_New__c(
                           Amount__c = invoice.Cost_of_Sale_Financing_and_Insurance__c,
                           Accounting_Period_New__c = accPeriod.get(invoice.Accounting_Period_Formula__c),
                           Date__c = maxDate,
                           Business_Partner__c = invoice.Account__c,
                           Description__c = 'Cost of Sales',
                           Dimension_1__c = dimension1,
                           Dimension_2__c = dimension.get('FININSURANCE'),
                           Dimension_3__c = dimension.get(dimension3),
                           GL_Account__c = glAccount.get('120000'),
                           Invoice__c = invoice.Id,
                           RecordTypeId = Schema.Sobjecttype.Transaction_Line_New__c.getRecordTypeInfosByDeveloperName().get('Invoice').getRecordTypeId(),
                           Revenue_Type__c = 'Actual',
                           Short_Description__c = 'Cost of Sales',
                           Status__c ='Posted',
                           Type_Code__c = 'SI');
                       transactionLines.add(ciTL);            
                   }    
                   
                   
                   If(invoice.Cost_of_Sale_Importer_of_Record__c != 0){
                       Transaction_Line_New__c ciTL = new Transaction_Line_New__c(
                           Amount__c = invoice.Cost_of_Sale_Importer_of_Record__c,
                           Accounting_Period_New__c = accPeriod.get(invoice.Accounting_Period_Formula__c),
                           Date__c = maxDate,
                           Business_Partner__c = invoice.Account__c,
                           Description__c = 'Cost of Sales',
                           Dimension_1__c = dimension1,
                           Dimension_2__c = dimension.get('IOR'),
                           Dimension_3__c = dimension.get(dimension3),
                           GL_Account__c = glAccount.get('120000'),
                           Invoice__c = invoice.Id,
                           RecordTypeId = Schema.Sobjecttype.Transaction_Line_New__c.getRecordTypeInfosByDeveloperName().get('Invoice').getRecordTypeId(),
                           Revenue_Type__c = 'Actual',
                           Short_Description__c = 'Cost of Sales',
                           Status__c ='Posted',
                           Type_Code__c = 'SI');
                       transactionLines.add(ciTL);            
                   }       
                   
                   
                   If(invoice.Cost_of_Sale_Tax__c != 0){
                       Transaction_Line_New__c ciTL = new Transaction_Line_New__c(
                           Amount__c = invoice.Cost_of_Sale_Tax__c,
                           
                           Accounting_Period_New__c = accPeriod.get(invoice.Accounting_Period_Formula__c),
                           Date__c = maxDate,
                           Business_Partner__c = invoice.Account__c,
                           Description__c = 'Revenue - Client taxes paid',
                           Dimension_1__c = dimension1,
                           Dimension_2__c = dimension.get('TAX'),
                           Dimension_3__c = dimension.get(dimension3),
                           GL_Account__c = glAccount.get('110151'),
                           Invoice__c = invoice.Id,
                           RecordTypeId = Schema.Sobjecttype.Transaction_Line_New__c.getRecordTypeInfosByDeveloperName().get('Invoice').getRecordTypeId(),
                           Revenue_Type__c = 'Actual',
                           Short_Description__c = 'Revenue-ClientTax',
                           Status__c ='Posted',
                           Type_Code__c = 'SI');
                       transactionLines.add(ciTL);            
                   }    
                   
                   If(invoice.Shipment_Value_Invoice_Line__c != 0){
                       Transaction_Line_New__c ciTL = new Transaction_Line_New__c(
                           Amount__c = invoice.Shipment_Value_Invoice_Line__c,
                           
                           Accounting_Period_New__c = accPeriod.get(invoice.Accounting_Period_Formula__c),
                           Date__c = maxDate,
                           Business_Partner__c = invoice.Account__c,
                           Description__c = 'Cash Book Suspense',
                           Dimension_1__c = dimension1,
                           Dimension_3__c = dimension.get(dimension3),
                           GL_Account__c = glAccount.get('914500'),
                           Invoice__c = invoice.Id,
                           RecordTypeId = Schema.Sobjecttype.Transaction_Line_New__c.getRecordTypeInfosByDeveloperName().get('Invoice').getRecordTypeId(),
                           Revenue_Type__c = 'Actual',
                           Short_Description__c = 'Cash Book Suspense',
                           Status__c ='Posted',
                           Type_Code__c = 'SI');
                       transactionLines.add(ciTL);            
                   }  
                   
                   If(invoice.Cost_of_Sale_Shipping_Insurance__c != 0){
                       Transaction_Line_New__c ciTL = new Transaction_Line_New__c(
                           Amount__c = invoice.Cost_of_Sale_Shipping_Insurance__c, 
                           Accounting_Period_New__c = accPeriod.get(invoice.Accounting_Period_Formula__c),
                           Date__c = maxDate,
                           Business_Partner__c = invoice.Account__c,
                           Description__c = 'Cost of Sales',
                           Dimension_1__c = dimension1,
                           Dimension_2__c = dimension.get('SHIPPING'),
                           Dimension_3__c = dimension.get(dimension3),
                           GL_Account__c = glAccount.get('120000'),
                           Invoice__c = invoice.Id,
                           RecordTypeId = Schema.Sobjecttype.Transaction_Line_New__c.getRecordTypeInfosByDeveloperName().get('Invoice').getRecordTypeId(),
                           Revenue_Type__c = 'Actual',
                           Short_Description__c = 'Cost of Sales',
                           Status__c ='Posted',
                           Type_Code__c = 'SI');
                       transactionLines.add(ciTL);            
                   }    
                   
                   
                   If(invoice.Cost_of_Sale_On_Charges__c != 0){
                       Transaction_Line_New__c ciTL = new Transaction_Line_New__c(
                           Amount__c = invoice.Cost_of_Sale_On_Charges__c, 
                           Accounting_Period_New__c = accPeriod.get(invoice.Accounting_Period_Formula__c),
                           Date__c = maxDate,
                           Business_Partner__c = invoice.Account__c,
                           Description__c = 'Cost of Sales',
                           Dimension_1__c = dimension1,
                           Dimension_2__c = dimension.get('ONCHARGES'),
                           Dimension_3__c = dimension.get(dimension3),
                           GL_Account__c = glAccount.get('120000'),
                           Invoice__c = invoice.Id,
                           RecordTypeId = Schema.Sobjecttype.Transaction_Line_New__c.getRecordTypeInfosByDeveloperName().get('Invoice').getRecordTypeId(),
                           Revenue_Type__c = 'Actual',
                           Short_Description__c = 'Cost of Sales',
                           Status__c ='Posted',
                           Type_Code__c = 'SI');
                       transactionLines.add(ciTL);            
                   }    
                   
                   
                   If(invoice.Cost_of_Sale_Insurance_Fees__c != 0){
                       Transaction_Line_New__c ciTL = new Transaction_Line_New__c(
                           Amount__c = invoice.Cost_of_Sale_Insurance_Fees__c,
                           Accounting_Period_New__c = accPeriod.get(invoice.Accounting_Period_Formula__c),
                           Date__c = maxDate,
                           Business_Partner__c = invoice.Account__c,
                           Description__c = 'Cost of Sales',
                           Dimension_1__c = dimension1,
                           Dimension_2__c = dimension.get('INSURANCE'),
                           Dimension_3__c = dimension.get(dimension3),
                           GL_Account__c = glAccount.get('120000'),
                           Invoice__c = invoice.Id,
                           RecordTypeId = Schema.Sobjecttype.Transaction_Line_New__c.getRecordTypeInfosByDeveloperName().get('Invoice').getRecordTypeId(),
                           Revenue_Type__c = 'Actual',
                           Short_Description__c = 'Cost of Sales',
                           Status__c ='Posted',
                           Type_Code__c = 'SI');
                       transactionLines.add(ciTL); }  
                   
                   
                   
                   invoice.Transaction_Lines_Created__c = TRUE;
                   invoice.Transaction_Total__c = invoice.Transaction_Total__c + transactionTotalSI;
                   invoice.Neg_Transaction_Total__c =  invoice.Neg_Transaction_Total__c + creditTotal;}
            
            
            //Update Invoice
            If(invoice.Shipment_Order__c != NULL && invoice.Invoice_Status__c != 'Invoice Sent' && invoice.Invoice_Status__c != 'Partial Payment' && invoice.Invoice_Status__c != 'Paid' &&
               invoice.Re_Populate_Invoice__c != trigger.oldMap.get(invoice.ID).Re_Populate_Invoice__c && invoice.Re_Populate_Invoice__c == TRUE ){
                   
                      
                   Integer paymentTerms = shipmentOrder.get(invoice.Shipment_Order__c).Account__r.IOR_Payment_Terms__c.intValue();
                   Date podDate = shipmentOrder.get(invoice.Shipment_Order__c).POD_Date__c;
                   String invoiceCurrency = shipmentOrder.get(invoice.Shipment_Order__c).Account__r.Default_invoicing_currency__c;
                   Decimal rate = conversionRate.get(invoiceCurrency);
                   
                   invoice.Admin_Fees__c = shipmentOrder.get(invoice.Shipment_Order__c).Handling_and_Admin_Fee__c / rate;
                   invoice.Bank_Fee__c = shipmentOrder.get(invoice.Shipment_Order__c).Bank_Fees__c / rate;
                   invoice.Conversion_Rate__c = rate;
                   invoice.Customs_Brokerage_Fees__c = shipmentOrder.get(invoice.Shipment_Order__c).Total_Customs_Brokerage_Cost1__c / rate;
                   invoice.Customs_Clearance_Fees__c = shipmentOrder.get(invoice.Shipment_Order__c).Total_clearance_Costs__c / rate;
                   invoice.Customs_Handling_Fees__c = shipmentOrder.get(invoice.Shipment_Order__c).Recharge_Handling_Costs__c / rate;
                   invoice.Customs_License_In__c = shipmentOrder.get(invoice.Shipment_Order__c).Total_License_Cost__c / rate;   
                   invoice.EOR_Fees__c = shipmentOrder.get(invoice.Shipment_Order__c).EOR_and_Export_Compliance_Fee_USD__c / rate;   
                   invoice.IOR_Fees__c = shipmentOrder.get(invoice.Shipment_Order__c).IOR_FEE_USD__c / rate;
                   invoice.International_Freight_Fee__c = shipmentOrder.get(invoice.Shipment_Order__c).International_Delivery_Fee__c / rate;
                   invoice.Invoice_Amount_Local_Currency__c = accountMap.get(invoice.Account__c).Invoice_Timing__c == 'Upfront invoicing' && accountMap.get(invoice.Account__c).Invoicing_Term_Parameters__c == 'No Terms'?  shipmentOrder.get(invoice.Shipment_Order__c).Total_Invoice_Amount__c / rate : (shipmentOrder.get(invoice.Shipment_Order__c).Total_Invoice_Amount__c + shipmentOrder.get(invoice.Shipment_Order__c).Potential_Cash_Outlay_Fee__c)/ rate;
                   invoice.Invoice_Currency__c = invoiceCurrency;
                   invoice.Invoice_Date__c = date.today();
                   invoice.Invoice_amount_USD__c = accountMap.get(invoice.Account__c).Invoice_Timing__c == 'Upfront invoicing' && accountMap.get(invoice.Account__c).Invoicing_Term_Parameters__c == 'No Terms'?  shipmentOrder.get(invoice.Shipment_Order__c).Total_Invoice_Amount__c : (shipmentOrder.get(invoice.Shipment_Order__c).Total_Invoice_Amount__c + shipmentOrder.get(invoice.Shipment_Order__c).Potential_Cash_Outlay_Fee__c);
                       invoice.Liability_Cover_Fee__c = shipmentOrder.get(invoice.Shipment_Order__c).Insurance_Fee_USD__c / rate;
                   invoice.Miscellaneous_Fee_Name__c = shipmentOrder.get(invoice.Shipment_Order__c).Miscellaneous_Fee_Name__c;
                   invoice.Miscellaneous_Fee__c = shipmentOrder.get(invoice.Shipment_Order__c).Miscellaneous_Fee__c / rate;
                   invoice.Recharge_Tax_and_Duty_Other__c = shipmentOrder.get(invoice.Shipment_Order__c).Recharge_Tax_and_Duty_Other__c / rate;
                   invoice.Taxes_and_Duties__c = shipmentOrder.get(invoice.Shipment_Order__c).Recharge_Tax_and_Duty__c / rate;
                   invoice.Cash_Outlay_Fee__c = accountMap.get(invoice.Account__c).Invoice_Timing__c == 'Upfront invoicing' && accountMap.get(invoice.Account__c).Invoicing_Term_Parameters__c == 'No Terms'?  0: shipmentOrder.get(invoice.Shipment_Order__c).Potential_Cash_Outlay_Fee__c / rate ;
                   if(!Test.isRunningTest()){
                       invoice.Re_Populate_Invoice__c = FALSE;
                   }
               }  
            
            //Update BTAs
            If(invoice.Shipment_Order__c != NULL && invoice.Invoice_Status__c != 'Invoice Sent' && invoice.Invoice_Status__c != 'Partial Payment' && invoice.Invoice_Status__c != 'Paid' &&
               invoice.Re_Populate_Invoice__c != trigger.oldMap.get(invoice.ID).Re_Populate_Invoice__c && invoice.Re_Populate_Invoice__c == TRUE ){
                   
                     
                   Integer paymentTerms = shipmentOrder.get(invoice.Shipment_Order__c).Account__r.IOR_Payment_Terms__c.intValue();
                   Date podDate = shipmentOrder.get(invoice.Shipment_Order__c).POD_Date__c;
                   String invoiceCurrency = shipmentOrder.get(invoice.Shipment_Order__c).Account__r.Default_invoicing_currency__c;
                   Decimal rate = conversionRate.get(invoiceCurrency);
                   
                   
                   invoice.Admin_Fees__c = shipmentOrder.get(invoice.Shipment_Order__c).Handling_and_Admin_Fee__c / rate;
                   invoice.Bank_Fee__c = shipmentOrder.get(invoice.Shipment_Order__c).Bank_Fees__c / rate;
                   invoice.Conversion_Rate__c = rate;
                   invoice.Customs_Brokerage_Fees__c = shipmentOrder.get(invoice.Shipment_Order__c).Total_Customs_Brokerage_Cost1__c / rate;
                   invoice.Customs_Clearance_Fees__c = shipmentOrder.get(invoice.Shipment_Order__c).Total_clearance_Costs__c / rate;
                   invoice.Customs_Handling_Fees__c = shipmentOrder.get(invoice.Shipment_Order__c).Recharge_Handling_Costs__c / rate;
                   invoice.Customs_License_In__c = shipmentOrder.get(invoice.Shipment_Order__c).Total_License_Cost__c / rate;   
                   invoice.EOR_Fees__c = shipmentOrder.get(invoice.Shipment_Order__c).EOR_and_Export_Compliance_Fee_USD__c / rate;   
                   invoice.IOR_Fees__c = shipmentOrder.get(invoice.Shipment_Order__c).IOR_FEE_USD__c / rate;
                   invoice.International_Freight_Fee__c = shipmentOrder.get(invoice.Shipment_Order__c).International_Delivery_Fee__c / rate;
                   invoice.Invoice_Amount_Local_Currency__c = accountMap.get(invoice.Account__c).Invoice_Timing__c == 'Upfront invoicing' && accountMap.get(invoice.Account__c).Invoicing_Term_Parameters__c == 'No Terms'?  shipmentOrder.get(invoice.Shipment_Order__c).Total_Invoice_Amount__c / rate : (shipmentOrder.get(invoice.Shipment_Order__c).Total_Invoice_Amount__c + shipmentOrder.get(invoice.Shipment_Order__c).Potential_Cash_Outlay_Fee__c)/ rate;
                   invoice.Invoice_Currency__c = invoiceCurrency;
                   invoice.Invoice_Date__c = date.today();
                   invoice.Invoice_amount_USD__c = accountMap.get(invoice.Account__c).Invoice_Timing__c == 'Upfront invoicing' && accountMap.get(invoice.Account__c).Invoicing_Term_Parameters__c == 'No Terms'?  shipmentOrder.get(invoice.Shipment_Order__c).Total_Invoice_Amount__c : (shipmentOrder.get(invoice.Shipment_Order__c).Total_Invoice_Amount__c + shipmentOrder.get(invoice.Shipment_Order__c).Potential_Cash_Outlay_Fee__c);
                       invoice.Liability_Cover_Fee__c = shipmentOrder.get(invoice.Shipment_Order__c).Insurance_Fee_USD__c / rate;
                   invoice.Miscellaneous_Fee_Name__c = shipmentOrder.get(invoice.Shipment_Order__c).Miscellaneous_Fee_Name__c;
                   invoice.Miscellaneous_Fee__c = shipmentOrder.get(invoice.Shipment_Order__c).Miscellaneous_Fee__c / rate;
                   invoice.Recharge_Tax_and_Duty_Other__c = shipmentOrder.get(invoice.Shipment_Order__c).Recharge_Tax_and_Duty_Other__c / rate;
                   invoice.Taxes_and_Duties__c = shipmentOrder.get(invoice.Shipment_Order__c).Recharge_Tax_and_Duty__c / rate;
                   invoice.Cash_Outlay_Fee__c = accountMap.get(invoice.Account__c).Invoice_Timing__c == 'Upfront invoicing' && accountMap.get(invoice.Account__c).Invoicing_Term_Parameters__c == 'No Terms'?  0: shipmentOrder.get(invoice.Shipment_Order__c).Potential_Cash_Outlay_Fee__c / rate ;
                   invoice.Re_Populate_Invoice__c = FALSE;}
            
            
          
            
            
            //Due Date
            //Remaining Charges Invoice and Taxes Invoice
            If(invoice.Invoice_Sent_Date__c != Null && invoice.Due_Date__c == null){
                
                invoice.Due_Date__c = (invoice.RecordTypeId == clientRTypeId || invoice.RecordTypeId == zeeInvoiceRecordTypeId) ? invoice.Invoice_Sent_Date__c + accountMap.get(invoice.Account__c).IOR_Payment_Terms__c.intValue() :  invoice.Invoice_Date__c + accountMap.get(invoice.Account__c).IOR_Payment_Terms__c.intValue() ;
                invoice.New_Penalty_Date__c = (invoice.RecordTypeId == clientRTypeId || invoice.RecordTypeId == zeeInvoiceRecordTypeId) ? invoice.Invoice_Sent_Date__c + accountMap.get(invoice.Account__c).IOR_Payment_Terms__c.intValue() : NULL;
            }
            
            //Paid by Tipalti
            If((invoice.Invoice_Status__c == 'Paid by Tipalti' && invoice.Paid_Date_Tipalti__c == null)){
                  
                invoice.Paid_Date_Tipalti__c = Date.today();
                
            }
            
            //Partial Payment
            If((invoice.Amount_Outstanding__c != trigger.oldMap.get(invoice.ID).Amount_Outstanding__c && invoice.Amount_Outstanding__c != 0 && math.abs(invoice.Amount_Outstanding__c - invoice.Invoice_amount_USD__c) > 1 )){         
                
                
                invoice.Invoice_Status__c = 'Partial Payment';
                
            }
            
            //Paid
            If((invoice.Amount_Outstanding__c != trigger.oldMap.get(invoice.ID).Amount_Outstanding__c && invoice.Amount_Outstanding__c == 0 && invoice.Invoice_Status__c != 'Paid by Tipalti' )){
                 
                invoice.Invoice_Status__c = 'Paid';
                
            }
            
            
            
            
            /*      
//Roll up Value applied to SO
iF(((trigger.oldMap.get(invoice.ID).Total_Cash_Applied__c != invoice.Total_Cash_Applied__c && invoice.Total_Cash_Applied__c != null) ||
(trigger.oldMap.get(invoice.ID).Total_Credits_Applied_To_Other_Invoices__c != invoice.Total_Credits_Applied_To_Other_Invoices__c && invoice.Total_Credits_Applied_To_Other_Invoices__c != null) ||
(trigger.oldMap.get(invoice.ID).Total_Credits_Applied_To_This_Invoice__c != invoice.Total_Credits_Applied_To_This_Invoice__c && invoice.Total_Credits_Applied_To_This_Invoice__c != null) )){


For (Id appliedValues : shipmentOrder.keySet()) {

If(shipmentOrder.get(appliedValues).Id == invoice.Shipment_Order__c){

Decimal supplierInvoiceVal = shipmentOrder.get(invoice.Shipment_Order__c).Total_Value_Applied_on_Supplier_Invoice__c != null ?shipmentOrder.get(invoice.Shipment_Order__c).Total_Value_Applied_on_Supplier_Invoice__c + invoice.Total_Value_Applied__c : invoice.Total_Value_Applied__c;
Decimal customerInvoiceVal = shipmentOrder.get(invoice.Shipment_Order__c).Total_Value_Applied_on_Customer_Invoice__c != null ?shipmentOrder.get(invoice.Shipment_Order__c).Total_Value_Applied_on_Customer_Invoice__c + invoice.Total_Value_Applied__c : invoice.Total_Value_Applied__c;


shipmentOrder.get(invoice.Shipment_Order__c).Total_Value_Applied_on_Supplier_Invoice__c =  invoice.RecordTypeId == supplierRTypeId ? supplierInvoiceVal : shipmentOrder.get(invoice.Shipment_Order__c).Total_Value_Applied_on_Supplier_Invoice__c;
shipmentOrder.get(invoice.Shipment_Order__c).Total_Value_Applied_on_Customer_Invoice__c =  invoice.RecordTypeId == clientRTypeId ? customerInvoiceVal: shipmentOrder.get(invoice.Shipment_Order__c).Total_Value_Applied_on_Customer_Invoice__c  ;
update  shipmentOrder.get(invoice.Shipment_Order__c);



}

}}


*/     
            
            
            
            //Auto Reversal 
            If((invoice.Invoice_Reversal__c != trigger.oldMap.get(invoice.ID).Invoice_Reversal__c && invoice.Invoice_Reversal__c == TRUE) && 
               math.abs(invoice.Amount_Outstanding__c - invoice.Invoice_amount_USD__c) < 1 && 
               invoice.Invoice_Status__c == 'FM Approved' && invoice.RecordTypeId == supplierRTypeId){
                   
                   For (Id invoiceCostings : costingsMap.keySet()) { 
                       
                       If(costingsMap.get(invoiceCostings).Invoice__c == invoice.Id){
                           costingsMap.get(invoiceCostings).Exchange_rate_payment_date__c = null;
                           costingsMap.get(invoiceCostings).Invoice__c = null;
                           costingsMap.get(invoiceCostings).Invoice_amount_local_currency__c = null;
                           costingsMap.get(invoiceCostings).Trigger__c = TRUE;
                           costingsToEdit.add(costingsMap.get(invoiceCostings));}}}
            
            
            //Invoice Value Changes
            If((invoice.IOR_Fees__c != trigger.oldMap.get(invoice.ID).IOR_Fees__c||invoice.Shipment_Value_Invoice_Line__c != trigger.oldMap.get(invoice.ID).Shipment_Value_Invoice_Line__c || invoice.Admin_Fees__c != trigger.oldMap.get(invoice.ID).Admin_Fees__c || invoice.Bank_Fee__c != trigger.oldMap.get(invoice.ID).Bank_Fee__c ||
                invoice.EOR_Fees__c != trigger.oldMap.get(invoice.ID).EOR_Fees__c || invoice.Cash_Outlay_Fee__c != trigger.oldMap.get(invoice.ID).Cash_Outlay_Fee__c || invoice.Collection_Administration_Fee__c != trigger.oldMap.get(invoice.ID).Collection_Administration_Fee__c ||
                invoice.Liability_Cover_Fee__c != trigger.oldMap.get(invoice.ID).Liability_Cover_Fee__c || invoice.Customs_Brokerage_Fees__c != trigger.oldMap.get(invoice.ID).Customs_Brokerage_Fees__c || invoice.Customs_Clearance_Fees__c != trigger.oldMap.get(invoice.ID).Customs_Clearance_Fees__c ||
                invoice.Customs_Handling_Fees__c != trigger.oldMap.get(invoice.ID).Customs_Handling_Fees__c || invoice.Customs_License_In__c != trigger.oldMap.get(invoice.ID).Customs_License_In__c || invoice.International_Freight_Fee__c != trigger.oldMap.get(invoice.ID).International_Freight_Fee__c ||
                invoice.Taxes_and_Duties__c != trigger.oldMap.get(invoice.ID).Taxes_and_Duties__c || invoice.Recharge_Tax_and_Duty_Other__c != trigger.oldMap.get(invoice.ID).Recharge_Tax_and_Duty_Other__c || invoice.Miscellaneous_Fee__c != trigger.oldMap.get(invoice.ID).Miscellaneous_Fee__c)
               && invoice.RecordTypeId == clientRTypeId) {
                   
                   
                   invoice.Invoice_amount_USD__c = (invoice.IOR_Fees__c + invoice.Shipment_Value_Invoice_Line__c + invoice.Admin_Fees__c + invoice.Bank_Fee__c + invoice.EOR_Fees__c + invoice.Cash_Outlay_Fee__c + invoice.Collection_Administration_Fee__c + invoice.Liability_Cover_Fee__c +
                                                    invoice.Customs_Brokerage_Fees__c + invoice.Customs_Clearance_Fees__c + invoice.Customs_Handling_Fees__c + invoice.Customs_License_In__c + invoice.International_Freight_Fee__c +
                                                    invoice.Taxes_and_Duties__c + invoice.Recharge_Tax_and_Duty_Other__c + invoice.Miscellaneous_Fee__c) * invoice.Conversion_Rate__c;
                   invoice.Invoice_Amount_Local_Currency__c = (invoice.Shipment_Value_Invoice_Line__c + invoice.IOR_Fees__c + invoice.Admin_Fees__c + invoice.Bank_Fee__c + invoice.EOR_Fees__c + invoice.Cash_Outlay_Fee__c + invoice.Collection_Administration_Fee__c + invoice.Liability_Cover_Fee__c +
                                                               invoice.Customs_Brokerage_Fees__c + invoice.Customs_Clearance_Fees__c + invoice.Customs_Handling_Fees__c + invoice.Customs_License_In__c + invoice.International_Freight_Fee__c +
                                                               invoice.Taxes_and_Duties__c + invoice.Recharge_Tax_and_Duty_Other__c + invoice.Miscellaneous_Fee__c);
                   
               }
            
            //Rollup
            If(invoice.Roll_Up_Costings__c != trigger.oldMap.get(invoice.ID).Roll_Up_Costings__c && invoice.Roll_Up_Costings__c == TRUE){
                
                
                decimal INVFCAdminFee = 0.0;         
                decimal INVFCBankFee = 0.0;           
                decimal INVFCFinanceFee = 0.0;         
                decimal INVFCIORandImport = 0.0;         
                decimal INVFCINsurance = 0.0;        
                decimal INVFCInternationsldeliveryFee = 0.0;  
                decimal INVFCMiselaneousFee = 0.0;        
                decimal INVFCRechatgeTaxandDuties = 0.0;   
                decimal INVFCTotClearence = 0.0;        
                decimal INVFCTotCustomsBrokerage = 0.0;      
                decimal INVFCTotHandlingCost = 0.0;       
                decimal INVFCTotLicenseCost = 0.0;        
                decimal INVFCTAXOther = 0.0;        
                decimal INVFCTAXForex = 0.0;        
                
                decimal INVACAdminFee = 0.0;         
                decimal INVACBankFee = 0.0;         
                decimal INVACFinanceFee = 0.0;         
                decimal INVACIORandImport = 0.0;      
                decimal INVACINsurance = 0.0;        
                decimal INVACInternationsldeliveryFee = 0.0;  
                decimal INVACMiselaneousFee = 0.0;      
                decimal INVACRechatgeTaxandDuties = 0.0;    
                decimal INVACTotClearence = 0.0;        
                decimal INVACTotCustomsBrokerage = 0.0;      
                decimal INVACTotHandlingCost = 0.0;       
                decimal INVACTotLicenseCost = 0.0;        
                decimal INVACTAXOther = 0.0;        
                decimal INVACTAXForex = 0.0;
                
                List<AggregateResult> allInvCosts = [SELECT Sum(Invoice_amount_USD_New__c) invAmount, Sum(Amount_in_USD__c) fcAmount, 
                                                     Cost_Category__c costCat, Tax_Sub_Category__c subcat
                                                     FROM CPA_Costing__c
                                                     Where Invoice__c =:invoice.Id
                                                     And Shipment_Order_Old__c != null
                                                     And Invoice__c != null
                                                     And RecordTypeID =: Schema.Sobjecttype.CPA_Costing__c.getRecordTypeInfosByDeveloperName().get('Display').getRecordTypeId()
                                                     Group by Cost_Category__c,Tax_Sub_Category__c];
                
                
                for(AggregateResult a : allInvCosts){
                    
                    If(a.get('costCat') == 'Admin' && a.get('subcat') == NULL){  INVFCAdminFee = (Decimal) a.get('fcAmount');   INVACAdminFee = (Decimal) a.get('invAmount');}
                    If(a.get('costCat') == 'Bank' && a.get('subcat') == NULL){  INVFCBankFee = (Decimal) a.get('fcAmount');  INVACBankFee = (Decimal) a.get('invAmount');}
                    If(a.get('costCat') == 'Finance' && a.get('subcat') == NULL){  INVFCFinanceFee = (Decimal) a.get('fcAmount');   INVACFinanceFee = (Decimal) a.get('invAmount');}
                    If(a.get('costCat') == 'IOR Cost' && a.get('subcat') == NULL){  INVFCIORandImport = (Decimal) a.get('fcAmount');  INVACIORandImport = (Decimal) a.get('invAmount');}
                    If(a.get('costCat') == 'Insurance' && a.get('subcat') == NULL){  INVFCINsurance = (Decimal) a.get('fcAmount');  INVACINsurance = (Decimal) a.get('invAmount');}
                    If(a.get('costCat') == 'International freight' && a.get('subcat') == NULL){  INVFCInternationsldeliveryFee = (Decimal) a.get('fcAmount');  INVACInternationsldeliveryFee = (Decimal) a.get('invAmount');}
                    If(a.get('costCat') == 'Miscellaneous' && a.get('subcat') == NULL){  INVFCMiselaneousFee = (Decimal) a.get('fcAmount');   INVACMiselaneousFee = (Decimal) a.get('invAmount');}
                    If(a.get('costCat') == 'Tax' && a.get('subcat') == NULL){  INVFCRechatgeTaxandDuties = (Decimal) a.get('fcAmount'); INVACRechatgeTaxandDuties = (Decimal) a.get('invAmount');}
                    If(a.get('costCat') == 'Clearance' && a.get('subcat') == NULL){  INVFCTotClearence = (Decimal) a.get('fcAmount');   INVACTotClearence = (Decimal) a.get('invAmount');}
                    If(a.get('costCat') == 'Brokerage' && a.get('subcat') == NULL){  INVFCTotCustomsBrokerage = (Decimal) a.get('fcAmount'); INVACTotCustomsBrokerage = (Decimal) a.get('invAmount');}
                    If(a.get('costCat') == 'Handling' && a.get('subcat') == NULL){  INVFCTotHandlingCost = (Decimal) a.get('fcAmount');  INVACTotHandlingCost = (Decimal) a.get('invAmount');}
                    If(a.get('costCat') == 'Licenses' && a.get('subcat') == NULL){  INVFCTotLicenseCost = (Decimal) a.get('fcAmount');  INVACTotLicenseCost = (Decimal) a.get('invAmount');}
                    If(a.get('costCat') == 'Tax' && a.get('subcat') == 'Duties and Taxes Other'){  INVFCTAXOther = (Decimal) a.get('fcAmount');   INVACTAXOther = (Decimal) a.get('invAmount');}
                    If(a.get('costCat') == 'Tax' && a.get('subcat') == 'Tax Forex Spread'){  INVFCTAXForex = (Decimal) a.get('fcAmount');  INVACTAXForex = (Decimal) a.get('invAmount');}    
                }       
                
                
                invoice.FC_Admin_Costs__c=INVFCAdminFee;
                invoice.FC_Bank_Costs__c=INVFCBankFee;
                invoice.FC_Finance_Costs__c=INVFCFinanceFee;
                invoice.FC_IOR_Costs__c=INVFCIORandImport;
                invoice.FC_Insurance_Costs__c = INVFCINsurance;
                invoice.FC_International_Delivery_Costs__c=INVFCInternationsldeliveryFee;
                invoice.FC_Miscellaneous_Costs__c=INVFCMiselaneousFee;
                invoice.FC_Duties_and_Taxes__c=INVFCRechatgeTaxandDuties;
                invoice.FC_Customs_Clearance_Costs__c=INVFCTotClearence;
                invoice.FC_Customs_Brokerage_Costs__c=INVFCTotCustomsBrokerage;
                invoice.FC_Customs_Handling_Costs__c=INVFCTotHandlingCost;
                invoice.FC_Customs_License_Costs__c=INVFCTotLicenseCost;
                invoice.FC_Duties_and_Taxes_Other__c=INVFCTAXOther;
                
                invoice.Actual_Admin_Costs__c=INVACAdminFee;
                invoice.Actual_Bank_Costs__c=INVACBankFee;
                invoice.Actual_Customs_Brokerage_Costs__c=INVACTotCustomsBrokerage;
                invoice.Actual_Customs_Clearance_Costs__c=INVACTotClearence;
                invoice.Actual_Duties_and_Taxes__c=INVACRechatgeTaxandDuties;
                invoice.Actual_Finance_Costs__c=INVACFinanceFee;
                invoice.Actual_Customs_Handling_Costs__c=INVACTotHandlingCost;
                invoice.Actual_Insurance_Costs__c=INVACINsurance;
                invoice.Actual_IOREOR_Costs__c=INVACIORandImport;
                invoice.Actual_International_Delivery_Costs__c=INVACInternationsldeliveryFee;
                invoice.Actual_Miscellaneous_Costs__c=INVACMiselaneousFee;
                invoice.Actual_Customs_License_Costs__c=INVACTotLicenseCost;
                invoice.Actual_Duties_and_Taxes_Other__c=INVACTAXOther;
                invoice.Invoice_amount_USD__c= INVACAdminFee + INVACBankFee + INVACTotCustomsBrokerage + INVACTotClearence + 
                    INVACRechatgeTaxandDuties + INVACFinanceFee + INVACTotHandlingCost + INVACINsurance + INVACIORandImport+ INVACInternationsldeliveryFee + 
                    INVACMiselaneousFee + INVACTotLicenseCost + INVACTAXOther;
                invoice.Invoice_Amount_Local_Currency__c= (INVACAdminFee + INVACBankFee + INVACTotCustomsBrokerage + INVACTotClearence + 
                                                           INVACRechatgeTaxandDuties + INVACFinanceFee + INVACTotHandlingCost + INVACINsurance + INVACIORandImport+ INVACInternationsldeliveryFee + 
                                                           INVACMiselaneousFee + INVACTotLicenseCost + INVACTAXOther)/  invoice.Conversion_Rate__c ;
                invoice.Roll_Up_Costings__c = FALSE;
                
                
                
            }
            if(accountMap.get(invoice.Account__c).RecordType.Name == 'Client' && invoice.RecordTypeId == clientRTypeId && invoice.Send_Reminder_Mail__c && !invoice.Send_Reminder_Mail_Override__c ){
                invoice.Send_Reminder_Mail__c = false;
            }
            
            
        }
        
        
        
        If(existingTLUpdate.size()>0){
            update existingTLUpdate;}  
        insert transactionLines;   
        insert creditNotes; 
        update shipmentOrderUpdateCN;
        update costingsToEdit;
        update freightRequestUpdateCN;
        update appliedSO;
        
        /*    For(Integer i=0;creditNotes.size()>i;i++ ){

Applied_Credit_Note__c appliedCreditNote = new Applied_Credit_Note__c(
Amount_Applied__c = creditNotes[i].Invoice_amount_USD__c < 0 ? creditNotes[i].Invoice_amount_USD__c * -1 : creditNotes[i].Invoice_amount_USD__c,
Credit_Applied_Date__c = creditNotes[i].Invoice_Sent_Date__c,
Credit_Note_New__c = creditNotes[i].Id,
Invoice_New__c = creditNotes[i].Invoice_Being_Credited__c); 
appliedCreditNotes.add(appliedCreditNote); }

insert appliedCreditNotes;

*/
        
    }
    
    If(Trigger.IsUpdate && Trigger.IsAfter){
        if(ShipmentOrderTriggerHandler.isRecurssion){
        return;
    }
        Decimal revAdHoc;         Decimal revOnCharge;       Decimal brokerage;     Decimal freight;
        Decimal revBank;          Decimal revShipping;       Decimal clearance;     Decimal ior;
        Decimal revEOR;           Decimal revTax;            Decimal handling;      Decimal liability;
        Decimal revFinancing;     Decimal adminFee;          Decimal license;       Decimal misc;
        Decimal revIOR;           Decimal cashOut;           Decimal eor;           Decimal taxOther;
        Decimal revInsurance;     Decimal collectionAdmin;   Decimal tax;           Decimal bankFee;
        
        Decimal costAdHoc;         Decimal costBank;         Decimal costEOR;          Decimal costFinancing;
        Decimal costIOR;           Decimal costInsurance;    Decimal costOnCharge;     Decimal costShipping;
        Decimal costTax;           Decimal actualAdmin;      Decimal actualBrokerage;  Decimal actualBank;
        Decimal actualClearance;   Decimal actualHandling;   Decimal actualLicense;    Decimal actualTax;
        Decimal actualTaxOther;    Decimal actualFinance;    Decimal actualInsurance;  Decimal actualFreight;
        Decimal actualIEOR;        Decimal actualmisc;       Decimal totalApplied;     Decimal posTls;   
        Invoice_New__c oldInvoice;  Decimal negTls;          Invoice_New__c currentInvoice;
        
        String TecexSupplierTypeId = Schema.Sobjecttype.Invoice_New__c.getRecordTypeInfosByDeveloperName().get('Supplier').getRecordTypeId();
        String TecexClientTypeId = Schema.Sobjecttype.Invoice_New__c.getRecordTypeInfosByDeveloperName().get('Client').getRecordTypeId();
       Id zeeInvoiceRecordTypeId1 = Schema.Sobjecttype.Invoice_New__c.getRecordTypeInfosByDeveloperName().get('Client_Zee').getRecordTypeId();
        
        Set<Id> populateInvoiceIds = new Set<Id>();
        Map<Id,Invoice_New__c> InvoicePODMap = new Map<Id,Invoice_New__c>();
        Map<Id,Id> shipmentIds = new Map<Id,Id>();
        Set<Id> StripeInvoices= new Set<Id>();
        
        for (Invoice_New__c invoice : Trigger.new) {
            invoiceIds.add(invoice.Id);
            shipmentOrderIds.add(invoice.Shipment_Order__c);
            oldInvoice = Trigger.oldMap.get(invoice.Id);
            currentInvoice = invoice;
            
            if(invoice.recordtypeid != TecexSupplierTypeId && ((isChanged(invoice,'Invoice_Status__c') && invoice.Invoice_Status__c == 'Invoice Sent')  
                                                               || (isChanged(invoice,'Amount_Outstanding_Local_Currency__c')  && (invoice.Invoice_Status__c == 'Invoice Sent' || invoice.Invoice_Status__c == 'Partial Payment') ))){ 
                                                                   
                                                                   
                                                                   StripeInvoices.add(invoice.Id);  
                                                                   
                                                                   If(isChanged(invoice,'Amount_Outstanding_Local_Currency__c')  && (invoice.Invoice_Status__c == 'Invoice Sent' || invoice.Invoice_Status__c == 'Partial Payment')  && invoice.Stripe_Paymnet_Intent__c !=null && invoice.Client_Stripe_Payment_Status__c!='succeeded'){
                                                                       if(!Test.isRunningTest()){
                                    StripeCheckout.CancelPaymentIntent(invoice.Stripe_Paymnet_Intent__c);
                                                                       }
                                                                   }
                                                                   
                                                               }
            
            if((invoice.recordtypeid==zeeInvoiceRecordTypeId1 || invoice.recordtypeid == TecexClientTypeId)  && Invoice.Invoice_Reversal__c==TRUE && isChanged(invoice,'Invoice_Reversal__c')){
                if(!Test.isRunningTest()){
                                    StripeCheckout.CancelPaymentIntent(invoice.Stripe_Paymnet_Intent__c);
                                                                       }
            }
            
            if((isChanged(invoice,'Invoice_Status__c') && invoice.Invoice_Status__c == 'Paid by Tipalti') 
               || isChanged(invoice,'POD_Date_New__c') ){
                   
                   InvoicePODMap.put(invoice.Id,invoice);
                   shipmentIds.put(invoice.Id,invoice.Shipment_Order__c);
               }
            
            if((isChanged(invoice, 'Stripe_Link__c') && invoice.Invoice_Status__c == 'Invoice Sent' && invoice.recordtypeid==zeeInvoiceRecordTypeId1)){
                populateInvoiceIds.add(invoice.Id);
            }
            
        }     
        
        if(StripeInvoices !=null && !StripeInvoices.isEmpty()){ 
             if(!Test.isRunningTest()){
            StripeCheckout.submit(StripeInvoices);  
             }
        }
        
        // ******This code is shifted to separate method below (createTransactionLines is the Method Name)*******
        
      /*  Map<Id, Accounting_Period__c> accPeriodMap = new Map<Id, Accounting_Period__c>([Select Id, Name From Accounting_Period__c Where Status__c = 'Open']); 
        Map<String, Id> accPeriod = new Map<String, Id>();  
        For (Id getAccPeriod : accPeriodMap.keySet()) { accPeriod.put(accPeriodMap.get(getAccPeriod).Name, getAccPeriod ); } 
        
        
        Map<Id, GL_Account__c> glAccountMap = new Map<Id, GL_Account__c>([Select Id, Name, GL_Account_Description__c,GL_Account_Short_Description__c,Currency__c From GL_Account__c]); 
        Map<String, Id> glAccount = new Map<String, Id>(); 
        For (Id getGlAccount : glAccountMap.keySet()) { glAccount.put(glAccountMap.get(getGlAccount).Name, getGlAccount );}
        
        
        Map<Id,Shipment_Order__c> shipmentMap = new Map<Id,Shipment_Order__c>([SELECT Id,POD_Date__c FROM Shipment_Order__c where Id=:shipmentIds.values()]);
        
        List<Transaction_Line_New__c> transactionLines = new List< Transaction_Line_New__c >();   */
        
        createTransactionLines(InvoicePODMap.values(), shipmentIds);
        
        /*for(Invoice_New__c invoice: InvoicePODMap.values()){
            Date maxDate = (invoice.POD_Date_New__c != null && invoice.POD_Date_New__c > invoice.Invoice_Sent_Date__c ) ? 
                invoice.POD_Date_New__c : 
            (invoice.Invoice_Sent_Date__c == null ? date.today() : invoice.Invoice_Sent_Date__c); 
                if(isChanged(invoice,'Invoice_Status__c') && invoice.Invoice_Status__c == 'Paid by Tipalti') {
                    
                    if(invoice.POD_Date_New__c == null){
                        String AccountingPeriod = '';
                        if(invoice.Paid_Date_Tipalti__c != null){
                            String Year = invoice.Paid_Date_Tipalti__c.year()+'-';
                            String Month = invoice.Paid_Date_Tipalti__c.month()+'';
                            if(Month.length() == 1){
                                Month = '0'+ Month;
                            }
                            AccountingPeriod =  Year + Month;
                        }
                        
                        transactionLines.add(new Transaction_Line_New__c(
                            Amount__c = invoice.Amount_Paid_via_Tipalti__c  ,   
                            Business_Partner__c = invoice.Account__c,
                            Description__c = 'Prepayments',
                            Accounting_Period_New__c = invoice.Accounting_Period_Formula__c == null ? accPeriod.get(AccountingPeriod) : accPeriod.get(invoice.Accounting_Period_Formula__c),
                            Date__c = maxDate,
                            GL_Account__c = glAccount.get('611500'),
                            Invoice__c = invoice.Id,
                            RecordTypeId = Schema.Sobjecttype.Transaction_Line_New__c.getRecordTypeInfosByDeveloperName().get('Tipalti_Payment').getRecordTypeId(),
                            Revenue_Type__c = 'Actual',
                            Short_Description__c = 'Prepayments',
                            Status__c ='Posted',
                            Type_Code__c = 'CP'
                        ));
                        
                        transactionLines.add(new Transaction_Line_New__c(
                            Amount__c = (invoice.Amount_Paid_via_Tipalti__c * -1)  ,    
                            Business_Partner__c = invoice.Account__c,
                            Description__c = 'JPM Turnkey Export Compliance UK Ltd',
                            Accounting_Period_New__c = invoice.Accounting_Period_Formula__c == null ? accPeriod.get(AccountingPeriod) : accPeriod.get(invoice.Accounting_Period_Formula__c),
                            Date__c = maxDate,
                            GL_Account__c = glAccount.get('67105363'),
                            Invoice__c = invoice.Id,
                            RecordTypeId = Schema.Sobjecttype.Transaction_Line_New__c.getRecordTypeInfosByDeveloperName().get('Tipalti_Payment').getRecordTypeId(),
                            Revenue_Type__c = 'Actual',
                            Short_Description__c = 'JPM Turnkey Export Compliance UK Ltd',
                            Status__c ='Posted',
                            Type_Code__c = 'CP'
                        ));
                        
                    }
                    else if(invoice.POD_Date_New__c == shipmentMap.get(shipmentIds.get(invoice.Id)).POD_Date__c){
                        
                        
                        transactionLines.add(new Transaction_Line_New__c(
                            Amount__c = invoice.Amount_Paid_via_Tipalti__c  ,   
                            Business_Partner__c = invoice.Account__c,
                            Description__c = 'Supplier Control Account',
                            Accounting_Period_New__c = accPeriod.get(invoice.Accounting_Period_Formula__c),
                            Date__c = maxDate,
                            GL_Account__c = glAccount.get('920000'),
                            Invoice__c = invoice.Id,
                            RecordTypeId = Schema.Sobjecttype.Transaction_Line_New__c.getRecordTypeInfosByDeveloperName().get('Tipalti_Payment').getRecordTypeId(),
                            Revenue_Type__c = 'Actual',
                            Short_Description__c = 'AP Control',
                            Status__c ='Posted',
                            Type_Code__c = 'CP'
                        ));
                        
                        transactionLines.add(new Transaction_Line_New__c(
                            Amount__c = (invoice.Amount_Paid_via_Tipalti__c * -1)  ,   
                            Business_Partner__c = invoice.Account__c,
                            Description__c = 'JPM Turnkey Export Compliance UK Ltd',
                            Accounting_Period_New__c = accPeriod.get(invoice.Accounting_Period_Formula__c),
                            Date__c = maxDate,
                            GL_Account__c = glAccount.get('67105363'),
                            Invoice__c = invoice.Id,
                            RecordTypeId = Schema.Sobjecttype.Transaction_Line_New__c.getRecordTypeInfosByDeveloperName().get('Tipalti_Payment').getRecordTypeId(),
                            Revenue_Type__c = 'Actual',
                            Short_Description__c = 'JPM Turnkey Export Compliance UK Ltd',
                            Status__c ='Posted',
                            Type_Code__c = 'CP'
                        ));
                        
                    }
                    
                }else if( (invoice.Invoice_Status__c == 'Paid by Tipalti')
                         && (isChanged(invoice,'POD_Date_New__c') &&  invoice.POD_Date_New__c == shipmentMap.get(shipmentIds.get(invoice.Id)).POD_Date__c)){
                             
                             
                             transactionLines.add(new Transaction_Line_New__c(
                                 Amount__c = (invoice.Amount_Paid_via_Tipalti__c * -1)  ,     
                                 Business_Partner__c = invoice.Account__c,
                                 Description__c = 'Prepayments',
                                 Accounting_Period_New__c = accPeriod.get(invoice.Accounting_Period_Formula__c),
                                 Date__c = maxDate,
                                 GL_Account__c = glAccount.get('611500'),
                                 Invoice__c = invoice.Id,
                                 RecordTypeId = Schema.Sobjecttype.Transaction_Line_New__c.getRecordTypeInfosByDeveloperName().get('Tipalti_Payment').getRecordTypeId(),
                                 Revenue_Type__c = 'Actual',
                                 Short_Description__c = 'Prepayments',
                                 Status__c ='Posted',
                                 Type_Code__c = 'CP'
                             ));
                             transactionLines.add(new Transaction_Line_New__c(
                                 Amount__c = invoice.Amount_Paid_via_Tipalti__c  ,   
                                 Business_Partner__c = invoice.Account__c,
                                 Description__c = 'Supplier Control Account',
                                 Accounting_Period_New__c = accPeriod.get(invoice.Accounting_Period_Formula__c),
                                 Date__c = maxDate,
                                 GL_Account__c = glAccount.get('920000'),
                                 Invoice__c = invoice.Id,
                                 RecordTypeId = Schema.Sobjecttype.Transaction_Line_New__c.getRecordTypeInfosByDeveloperName().get('Tipalti_Payment').getRecordTypeId(),
                                 Revenue_Type__c = 'Actual',
                                 Short_Description__c = 'AP Control',
                                 Status__c ='Posted',
                                 Type_Code__c = 'CP'
                             ));
                             
                         }
            
        }
        
        if(transactionLines != null && !transactionLines.isEmpty()){
            insert transactionLines;
        }*/   
        
        updateBTAPrePayments();
        
        List<Invoice_New__c> InvoiceList = [select Id,Invoice_amount_USD__c,Invoice_Type__c  from Invoice_New__c where Id in :invoiceIds AND Invoice_amount_USD__c < 0 AND Invoice_Type__c != 'Credit Note'];
        
        for(Invoice_New__c invoice:InvoiceList){
            invoice.Invoice_Type__c = 'Credit Note';
        }
        ShipmentOrderTriggerHandler.isRecurssion = true;
        update InvoiceList;
        
        Map<Id, Currency_Management2__c> currencyMap = new Map<Id, Currency_Management2__c>([Select Id, Name, Conversion_Rate__c, Currency__c, ISO_Code__c
                                                                                             From Currency_Management2__c]); 
        
        Map<String, Decimal> conversionRate = new Map<String, Decimal>();
        
        For (Id getRates : currencyMap.keySet()) {conversionRate.put(currencyMap.get(getRates).Name,currencyMap.get(getRates).Conversion_Rate__c );} 
        
        Map<Id, CPA_Costing__c> costingsMap = new Map<Id, CPA_Costing__c>([Select Id, Name, Exchange_rate_payment_date__c, Invoice_amount_local_currency__c,Invoice__c
                                                                           From CPA_Costing__c
                                                                           Where Invoice__c in :invoiceIds]); 
        
        List<CPA_Costing__c> costingsToEdit = new List<CPA_Costing__c>(); 
        
        
        Map<Id, Shipment_Order__c> shipmentOrder = new Map<Id,Shipment_Order__c>([Select Id, Name, CI_Admin_Fee__c, CI_Bank_Fees__c, CI_Cash_Outlay_Fee__c, CI_Collection_Administration_Fee__c,
                                                                                  CI_IOR_and_Import_Compliance_Fee_USD__c, CI_International_Delivery_Fee__c, CI_Liability_Cover__c, CI_Miscellaneous_Fee__c,
                                                                                  CI_Recharge_Tax_and_Duty__c, CI_Total_Clearance_Costs__c, CI_Total_Customs_Brokerage_Cost__c, CI_Total_Handling_Cost__c,
                                                                                  CI_Total_Licence_Cost__c, Revenue_Ad_Hoc_Fees__c, Revenue_Bank_Charges_Recovered__c, Revenue_Exporter_of_Record__c, 
                                                                                  Revenue_Financing_and_Insurance__c, Revenue_Importer_of_Record__c, Revenue_Insurance_Fees__c, Revenue_On_Charges__c,
                                                                                  Revenue_Shipping_Insurance__c, Revenue_Tax__c, Actual_Admin_Fee__c, Actual_Bank_Fees__c, Actual_Duties_and_Taxes_Other__c,
                                                                                  Actual_Finance_Fee__c, Actual_Insurance_Fee_USD__c, Actual_International_Delivery_Fee__c, Actual_Miscellaneous_Fee__c,
                                                                                  Actual_Total_Clearance_Costs__c, Actual_Total_Customs_Brokerage_Cost__c, Actual_Total_Handling_Costs__c, Actual_Total_IOR_EOR__c,
                                                                                  Actual_Total_License_Cost__c, Actual_Total_Tax_and_Duty__c, Cost_of_Sale_Ad_Hoc_Fees__c, Cost_of_Sale_Bank_Charges_Recovered__c,
                                                                                  Cost_of_Sale_Exporter_of_Record__c, Cost_of_Sale_Financing_and_Insurance__c, Cost_of_Sale_Importer_of_Record__c,
                                                                                  Cost_of_Sale_Insurance_Fees__c, Cost_of_Sale_On_Charges__c, Cost_of_Sale_Shipping_Insurance__c, Cost_of_Sale_Tax__c
                                                                                  From Shipment_Order__c
                                                                                  Where Id in :shipmentOrderIds]);
        
        List<Shipment_Order__c> shipmentToUpdate = new List<Shipment_Order__c>();
        List<Invoice_New__c> invoiceToUpdate = new List<Invoice_New__c>();
        
        String clientRTypeId = Schema.Sobjecttype.Invoice_New__c.getRecordTypeInfosByDeveloperName().get('Client').getRecordTypeId();
        String supplierRTypeId = Schema.Sobjecttype.Invoice_New__c.getRecordTypeInfosByDeveloperName().get('Supplier').getRecordTypeId();
        
        Map<Id, Freight__c> freightRequestMap = new Map<Id,Freight__c>([Select Id, Name, Populate_Invoice__c
                                                                        From Freight__c
                                                                        Where Id in :freightIds]);
        
        List<Shipment_Order__c> shipmentOrderUpdateCN = new List<Shipment_Order__c>();
        List<Invoice_New__c> creditNotes = new List<Invoice_New__c>();
        
        List<Freight__c> freightRequestUpdateCN = new List<Freight__c>(); 
        
        List<Applied_Credit_Note__c> appliedCreditNotes = new List<Applied_Credit_Note__c>();
        
        Id zeeInvoiceRecordTypeId = Schema.Sobjecttype.Invoice_New__c.getRecordTypeInfosByDeveloperName().get('Client_Zee').getRecordTypeId();
        
        for (Invoice_New__c invoice : Trigger.new) { 
            
            
            //Auto Reversal 
            If((invoice.Invoice_Reversal__c != trigger.oldMap.get(invoice.ID).Invoice_Reversal__c && invoice.Invoice_Reversal__c == TRUE) && 
               math.abs(invoice.Amount_Outstanding__c - invoice.Invoice_amount_USD__c) < 1 && 
               (invoice.Invoice_Status__c == 'Invoice Sent' || invoice.Invoice_Status__c == 'FM Approved')){
                   
                  
                   
                   Invoice_New__c creditNote = new Invoice_New__c(
                       Account__c = invoice.Account__c, 
                       Admin_Fees__c = invoice.Admin_Fees__c != null?  invoice.Admin_Fees__c * -1: 0,
                       Bank_Fee__c = invoice.Bank_Fee__c != null?  invoice.Bank_Fee__c * -1: 0,
                       Cash_Outlay_Fee__c = invoice.Cash_Outlay_Fee__c != null?  invoice.Cash_Outlay_Fee__c * -1: 0,
                       Collection_Administration_Fee__c = invoice.Collection_Administration_Fee__c != null?  invoice.Collection_Administration_Fee__c * -1: 0, 
                       Conversion_Rate__c = invoice.Conversion_Rate__c,
                       Customs_Brokerage_Fees__c = invoice.Customs_Brokerage_Fees__c != null?  invoice.Customs_Brokerage_Fees__c * -1: 0,
                       Customs_Clearance_Fees__c = invoice.Customs_Clearance_Fees__c != null?  invoice.Customs_Clearance_Fees__c * -1: 0,
                       Customs_Handling_Fees__c = invoice.Customs_Handling_Fees__c != null?  invoice.Customs_Handling_Fees__c * -1: 0,
                       Customs_License_In__c = invoice.Customs_License_In__c != null?  invoice.Customs_License_In__c * -1: 0,
                       EOR_Fees__c = invoice.EOR_Fees__c != null?  invoice.EOR_Fees__c * -1: 0,
                       IOR_Fees__c = invoice.IOR_Fees__c != null?  invoice.IOR_Fees__c * -1: 0,
                       International_Freight_Fee__c = invoice.International_Freight_Fee__c != null?  invoice.International_Freight_Fee__c * -1: 0,
                       Invoice_Amount_Local_Currency__c = invoice.Invoice_Amount_Local_Currency__c != null?  invoice.Invoice_Amount_Local_Currency__c * -1: 0,
                       Invoice_Currency__c = invoice.invoice_Currency__c,
                       Invoice_Date__c = Date.today(),
                       Invoice_Sent_Date__c = Date.today(),
                       Invoice_Type__c = 'Credit Note',
                       Invoice_amount_USD__c = invoice.Invoice_amount_USD__c != null?  invoice.Invoice_amount_USD__c * -1: 0,
                       Liability_Cover_Fee__c = invoice.Liability_Cover_Fee__c != null?  invoice.Liability_Cover_Fee__c * -1: 0,
                       Miscellaneous_Fee_Name__c = invoice.Miscellaneous_Fee_Name__c,
                       Miscellaneous_Fee__c = invoice.Miscellaneous_Fee__c != null?  invoice.Miscellaneous_Fee__c * -1: 0,
                       POD_Date_New__c = invoice.Shipment_Order__r.POD_Date__c,
                       Freight_Request__c = invoice.Freight_Request__c,
                       Recharge_Tax_and_Duty_Other__c = invoice.Recharge_Tax_and_Duty_Other__c != null?  invoice.Recharge_Tax_and_Duty_Other__c * -1: 0,
                       RecordTypeId = invoice.RecordTypeId,
                       Shipment_Order__c = invoice.Shipment_Order__c,
                       Actual_Admin_Costs__c = invoice.Actual_Admin_Costs__c != null? invoice.Actual_Admin_Costs__c * -1: 0 ,
                       Actual_Bank_Costs__c = invoice.Actual_Bank_Costs__c != null? invoice.Actual_Bank_Costs__c * -1: 0,
                       Actual_Customs_Brokerage_Costs__c = invoice.Actual_Customs_Brokerage_Costs__c != null? invoice.Actual_Customs_Brokerage_Costs__c * -1: 0,
                       Actual_Customs_Clearance_Costs__c = invoice.Actual_Customs_Clearance_Costs__c != null? invoice.Actual_Customs_Clearance_Costs__c * -1: 0,
                       Actual_Customs_Handling_Costs__c = invoice.Actual_Customs_Handling_Costs__c != null? invoice.Actual_Customs_Handling_Costs__c * -1: 0,
                       Actual_Customs_License_Costs__c = invoice.Actual_Customs_License_Costs__c != null? invoice.Actual_Customs_License_Costs__c * -1: 0,
                       Actual_Duties_and_Taxes_Other__c = invoice.Actual_Duties_and_Taxes_Other__c != null?  invoice.Actual_Duties_and_Taxes_Other__c * -1: 0,
                       Actual_Duties_and_Taxes__c = invoice.Actual_Duties_and_Taxes__c != null?  invoice.Actual_Duties_and_Taxes__c * -1: 0,
                       Actual_Finance_Costs__c = invoice.Actual_Finance_Costs__c != null?  invoice.Actual_Finance_Costs__c * -1: 0,
                       Actual_IOREOR_Costs__c = invoice.Actual_IOREOR_Costs__c != null?  invoice.Actual_IOREOR_Costs__c * -1: 0,
                       Actual_Insurance_Costs__c = invoice.Actual_Insurance_Costs__c != null?  invoice.Actual_Insurance_Costs__c * -1: 0,
                       Actual_International_Delivery_Costs__c = invoice.Actual_International_Delivery_Costs__c != null?  invoice.Actual_International_Delivery_Costs__c * -1: 0,
                       Actual_Miscellaneous_Costs__c = invoice.Actual_Miscellaneous_Costs__c != null?  invoice.Actual_Miscellaneous_Costs__c * -1: 0,
                       Shipment_Value_Invoice_Line__c = invoice.Shipment_Value_Invoice_Line__c != null?  invoice.Shipment_Value_Invoice_Line__c * -1: 0,
                       Invoice_Name__c = invoice.Invoice_Name__c==null ?invoice.Name + '-'+'CN' : invoice.Invoice_Name__c + '-'+'CN', 
                       Invoice_Being_Credited__c = invoice.id,
                       Invoice_Status__c = invoice.RecordTypeId == supplierRTypeId ?'FM Approved': 'Paid',
                       Invoice_Note__c = 'This Credit Note has been raised to reverse invoice '+ invoice.Name +'.',
                       Taxes_and_Duties__c = invoice.Taxes_and_Duties__c != null?  invoice.Taxes_and_Duties__c * -1: 0);  
                   
                   creditNotes.add(creditNote);
                   
                   
                   If((invoice.RecordTypeId == Schema.Sobjecttype.Invoice_New__c.getRecordTypeInfosByDeveloperName().get('Client').getRecordTypeId()||invoice.RecordTypeId == zeeInvoiceRecordTypeId) && invoice.Shipment_Order__c != null && invoice.Invoice_Type__c == 'Invoice'){
                       
                       For (Id creditShipments : shipmentOrder.keySet()) {
                           
                           If(shipmentOrder.get(creditShipments).Id == invoice.Shipment_Order__c){  
                               shipmentOrder.get(creditShipments).First_Invoice_Created__c = FALSE; 
                               shipmentOrder.get(creditShipments).Populate_Invoice__c = FALSE; 
                               shipmentOrderUpdateCN.add(shipmentOrder.get(creditShipments));}}} 
                   
                   
                   
                   If((invoice.RecordTypeId == Schema.Sobjecttype.Invoice_New__c.getRecordTypeInfosByDeveloperName().get('Client').getRecordTypeId()||invoice.RecordTypeId == zeeInvoiceRecordTypeId) && invoice.Freight_Request__c != null && invoice.Invoice_Type__c == 'Invoice'){
                       
                       For (Id freightRequests : freightRequestMap.keySet()) {
                           If(freightRequestMap.get(freightRequests).Id == invoice.Freight_Request__c){
                               freightRequestMap.get(freightRequests).Populate_Invoice__c = FALSE; 
                               freightRequestUpdateCN.add(freightRequestMap.get(freightRequests));}}}  }  
            
            
            if(invoice.Invoice_Status__c == 'Invoice Sent' && invoice.Invoice_Status__c != trigger.oldMap.get(invoice.ID).Invoice_Status__c && invoice.RecordTypeId == zeeInvoiceRecordTypeId){
                populateInvoiceIds.add(invoice.Id);
            }
        }
                
        insert creditNotes; 
        update shipmentOrderUpdateCN;
        update costingsToEdit;
        update freightRequestUpdateCN;
        
        
        
        
        For(Integer i=0;creditNotes.size()>i;i++ ){
            Applied_Credit_Note__c appliedCreditNote = new Applied_Credit_Note__c(
                Amount_Applied__c = creditNotes[i].Invoice_amount_USD__c * -1,
                Credit_Applied_Date__c = Date.today(),
                Credit_Note_New__c = creditNotes[i].Id,
                Invoice_New__c = creditNotes[i].Invoice_Being_Credited__c); 
            appliedCreditNotes.add(appliedCreditNote); 
            populateInvoiceIds.add(creditNotes[i].Id);
        }
        
        insert appliedCreditNotes;
        
                
        List<AggregateResult> allInvoices = [SELECT Sum(Revenue_Ad_Hoc_Fees__c) revAdHoc, SUM(Revenue_Bank_Charges_Recovered__c) revBank, Sum(Revenue_Exporter_of_Record__c) revEOR,
                                             SUM(Revenue_Financing_and_Insurance__c) revFinancing, sum(Revenue_Importer_of_Record__c) revIOR, sum(Revenue_Insurance_Fees__c) revInsurance,
                                             sum(Revenue_On_Charges__c) revOnCharge, sum(Revenue_Shipping_Insurance__c) revShipping, sum(Revenue_Tax__c) revTax, sum(Admin_Fees_USD__c) adminFee,
                                             sum(Bank_Fees_USD__c) bankFee, sum(Cash_Outlay_Fee_USD__c) cashOut, sum(Collection_Administration_Fee_USD__c) collectionAdmin, sum(Customs_Brokerage_Fees_USD__c) brokerage,
                                             sum(Customs_Clearance_Fees_USD__c) clearance, sum(Customs_Handling_Fees_USD__c) handling, sum(Customs_License_Fees_USD__c) license, sum(EOR_Fees_USD__c) eor,
                                             sum(International_Freight_Fee_USD__c) freight, sum(IOR_Fees_USD__c) ior, sum(Liability_Cover_Fee_USD__c) liability, sum(Miscellaneous_Fee_USD__c) misc,
                                             sum(Recharge_Tax_and_Duty_Other_USD__c) taxOther, sum(Taxes_and_Duties_USD__c) tax, sum(Cost_of_Sale_Ad_Hoc_Fees__c) costAdHoc, sum(Cost_of_Sale_Bank_Charges_Recovered__c) costBank,
                                             sum(Cost_of_Sale_Exporter_of_Record__c) costEOR, sum(Cost_of_Sale_Financing_and_Insurance__c) costFinancing, sum(Cost_of_Sale_Importer_of_Record__c) costIOR,
                                             SUM(Cost_of_Sale_Insurance_Fees__c) costInsurance, sum(Cost_of_Sale_On_Charges__c) costOnCharge, sum(Cost_of_Sale_Shipping_Insurance__c) costShipping,
                                             sum(Cost_of_Sale_Tax__c) costTax, sum(Actual_Admin_Costs__c) actualAdmin, sum(Actual_Customs_Brokerage_Costs__c) actualBrokerage, sum(Actual_Bank_Costs__c) actualBank,
                                             sum(Actual_Customs_Clearance_Costs__c) actualClearance, sum(Actual_Customs_Handling_Costs__c) actualHandling, sum(Actual_Customs_License_Costs__c) actualLicense,
                                             sum(Actual_Duties_and_Taxes__c) actualTax, sum(Actual_Duties_and_Taxes_Other__c) actualTaxOther, sum(Actual_Finance_Costs__c) actualFinance, 
                                             sum(Actual_Insurance_Costs__c) actualInsurance, sum(Actual_International_Delivery_Costs__c) actualFreight, sum(Actual_IOREOR_Costs__c) actualIEOR,
                                             sum(Actual_Miscellaneous_Costs__c) actualmisc, sum(Total_Value_Applied__c) totalApplied, Shipment_Order__c, RecordTypeId 
                                             From Invoice_New__c 
                                             where Shipment_Order__c in :shipmentOrderIds
                                             And (Invoice_Status__c ='Invoice Sent'
                                                  OR Invoice_Status__c = 'FM Approved'
                                                  OR Invoice_Status__c = 'Paid'
                                                  OR Invoice_Status__c = 'Partial Payment' 
                                                  OR Invoice_Status__c = 'Paid by Tipalti'
                                                  OR Invoice_Status__c = 'Paid via Stripe')
                                             Group by Shipment_Order__c, RecordTypeId];   
        
        
        //Roll up Value applied to SO
        iF((trigger.oldMap.get(currentInvoice.id).Invoice_Reversal__c != currentInvoice.Invoice_Reversal__c && currentInvoice.Invoice_Reversal__c == TRUE)  ||
           (trigger.oldMap.get(currentInvoice.id).Invoice_Status__c != currentInvoice.Invoice_Status__c && 
            (currentInvoice.Invoice_Status__c == 'FM Approved' || currentInvoice.Invoice_Status__c == 'Invoice Sent' || currentInvoice.Invoice_Status__c == 'Paid'|| currentInvoice.Invoice_Status__c == 'Paid by Tipalti'|| currentInvoice.Invoice_Status__c == 'Paid via Stripe'|| currentInvoice.Invoice_Status__c == 'Partial Payment')) ||
           (trigger.oldMap.get(currentInvoice.ID).Total_Cash_Applied__c != currentInvoice.Total_Cash_Applied__c && currentInvoice.Total_Cash_Applied__c != null) ||
           (trigger.oldMap.get(currentInvoice.ID).Total_Credits_Applied_To_Other_Invoices__c != currentInvoice.Total_Credits_Applied_To_Other_Invoices__c && currentInvoice.Total_Credits_Applied_To_Other_Invoices__c != null) ||
           (trigger.oldMap.get(currentInvoice.ID).Total_Credits_Applied_To_This_Invoice__c != currentInvoice.Total_Credits_Applied_To_This_Invoice__c && currentInvoice.Total_Credits_Applied_To_This_Invoice__c != null)){
               
               
               for(AggregateResult a : allInvoices){
                   
                   
                   
                   Id thisSoId = string.valueOf(a.get('Shipment_Order__c'));
                   Shipment_Order__c thisSo = new Shipment_Order__c(Id=thisSoId);
                   
                   if(shipmentOrder.containskey(thisSoId))
                   {
                       thisSo = shipmentOrder.get(thisSoId);
                       
                   }
                   
                   if(a.get('RecordTypeId') == Schema.Sobjecttype.Invoice_New__c.getRecordTypeInfosByDeveloperName().get('Supplier').getRecordTypeId())
                   { 
                       if(shipmentOrder.get(thisSoId) != null){  
                           shipmentOrder.get(thisSoId).Actual_Admin_Fee__c =  (Decimal) a.get('actualAdmin');
                           shipmentOrder.get(thisSoId).Actual_Bank_Fees__c =  (Decimal) a.get('actualBank');
                           shipmentOrder.get(thisSoId).Actual_Duties_and_Taxes_Other__c =  (Decimal) a.get('actualTaxOther')  ;
                           shipmentOrder.get(thisSoId).Actual_Finance_Fee__c =  (Decimal) a.get('actualFinance');
                           shipmentOrder.get(thisSoId).Actual_Insurance_Fee_USD__c = (Decimal) a.get('actualInsurance')  ;
                           shipmentOrder.get(thisSoId).Actual_International_Delivery_Fee__c =  (Decimal) a.get('actualFreight') ;    
                           shipmentOrder.get(thisSoId).Actual_Miscellaneous_Fee__c =  (Decimal) a.get('actualmisc');
                           shipmentOrder.get(thisSoId).Actual_Total_Clearance_Costs__c =  (Decimal) a.get('actualClearance') ;
                           shipmentOrder.get(thisSoId).Actual_Total_Customs_Brokerage_Cost__c =  (Decimal) a.get('actualBrokerage') ;
                           shipmentOrder.get(thisSoId).Actual_Total_Handling_Costs__c =  (Decimal) a.get('actualHandling') ;
                           shipmentOrder.get(thisSoId).Actual_Total_IOR_EOR__c =  (Decimal) a.get('actualIEOR')  ;
                           shipmentOrder.get(thisSoId).Actual_Total_License_Cost__c =  (Decimal) a.get('actualLicense') ;
                           shipmentOrder.get(thisSoId).Actual_Total_Tax_and_Duty__c =  (Decimal) a.get('actualTax') ;    
                           shipmentOrder.get(thisSoId).Cost_of_Sale_Ad_Hoc_Fees__c =  (Decimal) a.get('costAdHoc') ;
                           shipmentOrder.get(thisSoId).Cost_of_Sale_Bank_Charges_Recovered__c =  (Decimal) a.get('costBank') ;
                           shipmentOrder.get(thisSoId).Cost_of_Sale_Exporter_of_Record__c =  (Decimal) a.get('costEOR') ;
                           shipmentOrder.get(thisSoId).Cost_of_Sale_Financing_and_Insurance__c =  (Decimal) a.get('costFinancing') ;
                           shipmentOrder.get(thisSoId).Cost_of_Sale_Importer_of_Record__c =  (Decimal) a.get('costIOR') ;
                           shipmentOrder.get(thisSoId).Cost_of_Sale_Insurance_Fees__c =  (Decimal) a.get('costInsurance') ;
                           shipmentOrder.get(thisSoId).Cost_of_Sale_On_Charges__c =  (Decimal) a.get('costOnCharge') ;    
                           shipmentOrder.get(thisSoId).Cost_of_Sale_Shipping_Insurance__c =  (Decimal) a.get('costShipping') ;
                           shipmentOrder.get(thisSoId).Cost_of_Sale_Tax__c =  (Decimal) a.get('costTax') ;      
                           shipmentOrder.get(thisSoId).Total_Value_Applied_on_Supplier_Invoice__c =  (Decimal) a.get('totalApplied'); 
                           
                       }
                   }
                   
                   if(a.get('RecordTypeId') == zeeInvoiceRecordTypeId || a.get('RecordTypeId') == Schema.Sobjecttype.Invoice_New__c.getRecordTypeInfosByDeveloperName().get('Client').getRecordTypeId())
                   { 
                       Decimal taxValue = (Decimal) a.get('tax') != null? (Decimal) a.get('tax'): 0;
                       Decimal taxOtherValue = (Decimal) a.get('taxOther') != null? (Decimal) a.get('taxOther'): 0;
                       Decimal finalTax = taxValue + taxOtherValue;
                       Decimal rate = currentInvoice.Conversion_Rate__c;
                       if(shipmentOrder.get(thisSoId) != null){
                           shipmentOrder.get(thisSoId).CI_Admin_Fee__c =  (Decimal) a.get('adminFee')!= null? (Decimal) a.get('adminFee') : 0 ;
                           shipmentOrder.get(thisSoId).CI_Bank_Fees__c =  (Decimal) a.get('bankFee');
                           shipmentOrder.get(thisSoId).CI_Cash_Outlay_Fee__c =  (Decimal) a.get('cashOut');
                           shipmentOrder.get(thisSoId).CI_Collection_Administration_Fee__c =  (Decimal) a.get('collectionAdmin');
                           shipmentOrder.get(thisSoId).CI_IOR_and_Import_Compliance_Fee_USD__c = (Decimal) a.get('ior');
                           shipmentOrder.get(thisSoId).CI_EOR_and_Export_Compliance_Fee_USD__c = (Decimal) a.get('eor');
                           shipmentOrder.get(thisSoId).CI_International_Delivery_Fee__c =  (Decimal) a.get('freight'); 
                           shipmentOrder.get(thisSoId).CI_Liability_Cover__c =  (Decimal) a.get('liability');
                           shipmentOrder.get(thisSoId).CI_Miscellaneous_Fee__c =  (Decimal) a.get('misc');
                           shipmentOrder.get(thisSoId).CI_Recharge_Tax_and_Duty__c =  finalTax ;
                           shipmentOrder.get(thisSoId).CI_Total_Clearance_Costs__c =  (Decimal) a.get('clearance');
                           shipmentOrder.get(thisSoId).CI_Total_Customs_Brokerage_Cost__c = (Decimal) a.get('brokerage');
                           shipmentOrder.get(thisSoId).CI_Total_Handling_Cost__c = (Decimal) a.get('handling');
                           shipmentOrder.get(thisSoId).CI_Total_Licence_Cost__c = (Decimal) a.get('license');
                           shipmentOrder.get(thisSoId).Revenue_Ad_Hoc_Fees__c =  (Decimal) a.get('revAdHoc');
                           shipmentOrder.get(thisSoId).Revenue_Bank_Charges_Recovered__c =  (Decimal) a.get('revBank');
                           shipmentOrder.get(thisSoId).Revenue_Exporter_of_Record__c = (Decimal) a.get('revEOR');
                           shipmentOrder.get(thisSoId).Revenue_Financing_and_Insurance__c =  (Decimal) a.get('revFinancing');
                           shipmentOrder.get(thisSoId).Revenue_Importer_of_Record__c =  (Decimal) a.get('revIOR');
                           shipmentOrder.get(thisSoId).Revenue_Insurance_Fees__c =  (Decimal) a.get('revInsurance');
                           shipmentOrder.get(thisSoId).Revenue_On_Charges__c =  (Decimal) a.get('revOnCharge');    
                           shipmentOrder.get(thisSoId).Revenue_Shipping_Insurance__c =  (Decimal) a.get('revShipping');
                           shipmentOrder.get(thisSoId).Revenue_Tax__c =  (Decimal) a.get('revTax');    
                           shipmentOrder.get(thisSoId).Total_Value_Applied_on_Customer_Invoice__c =  (Decimal) a.get('totalApplied'); 
                       }
                       
                   }
                   If(shipmentOrder.get(thisSoId) != null && !shipmentToUpdate.contains(shipmentOrder.get(thisSoId))){
                       shipmentToUpdate.add(shipmentOrder.get(thisSoId));
                   }
                   
                   
               }     
           }
        
        
        update shipmentToUpdate;     
        
        if(!populateInvoiceIds.IsEmpty()){
             if(!Test.isRunningTest()) {
                    System.enqueueJob(new QueueableGenerateZeeInvoice(populateInvoiceIds));
                }
            //CTRL_generateAndSendInvoice.SaveInvoiceUsingTrigger(populateInvoiceIds);
        }
        
    }
    
    public void updateDueDateForSupplier(Invoice_New__c invoice){
        //Account Account = invoice.Account__c;
        String supplierRTypeId = Schema.Sobjecttype.Invoice_New__c.getRecordTypeInfosByDeveloperName().get('Supplier').getRecordTypeId();
        if(supplierRTypeId == invoice.RecordTypeId && invoice.Invoice_Sent_Date__c != null ){
            if(invoice.Invoice_Date__c != null && invoice.Account__r.IOR_Payment_Terms__c != null )
                invoice.Due_Date__c  = invoice.Invoice_Date__c + invoice.Account__r.IOR_Payment_Terms__c.intValue();
        }
    }
    
    public void updateBTAPrePayments(){
        List<Id> InvoiceIdsWithPOD = new List<Id>();
        List<Id> InvoiceIdsWithoutPOD = new List<Id>();
        List<Bank_Transaction_Applied__c> BTAList = new List<Bank_Transaction_Applied__c>();
        for(Invoice_New__c invoice : Trigger.New){
            if(invoice.Amount_Outstanding__c != trigger.oldMap.get(invoice.ID).Amount_Outstanding__c || invoice.Accounting_Period_new__c != NULL
               && (invoice.Invoice_Status__c == 'Partial Payment' 
                   || invoice.Invoice_Status__c == 'Paid'|| invoice.Invoice_Status__c == 'Paid by Tipalti') 
               && invoice.Total_Cash_Applied__c != 0){
                   if(invoice.POD_Date_New__c == NULL){
                       InvoiceIdsWithoutPOD.add(invoice.Id);
                   }else{
                       InvoiceIdsWithPOD.add(invoice.Id);
                   }  
               }
            
        }
        for(Bank_Transaction_Applied__c BTA : [select Id,Prepayment_RRIA__c FROM Bank_Transaction_Applied__c where Invoice__c IN :InvoiceIdsWithoutPOD]){
            BTA.Prepayment_RRIA__c = True;
            BTAList.add(BTA);
        }
        for(Bank_Transaction_Applied__c BTA : [select Id,Prepayment_RRIA_Reversal__c,Invoice__c,Invoice__r.POD_Date_New__c FROM Bank_Transaction_Applied__c where Invoice__c IN :InvoiceIdsWithPOD]){
            //if(trigger.oldMap.get(BTA.Invoice__c).Accounting_Period_New__c == null && BTA.Invoice__r.Accounting_Period_New__c != null ){
            BTA.Prepayment_RRIA_Reversal__c = True;
            BTAList.add(BTA);
            //  }
        }
        update BTAList;
    }
    public static Boolean isChanged(Invoice_New__c invoice,String fieldName){
        if(Trigger.isExecuting && Trigger.isUpdate) {
            return invoice.get(fieldName) != Trigger.oldMap.get(invoice.Id).get(fieldName);
        }
        return false;
    }
    
    public void createTransactionLines( List<Invoice_New__c> invoiceList, Map<Id,Id> shipmentIds){
        
        Map<Id, Accounting_Period__c> accPeriodMap = new Map<Id, Accounting_Period__c>([Select Id, Name From Accounting_Period__c Where Status__c = 'Open']); 
        Map<String, Id> accPeriod = new Map<String, Id>();  
        For (Id getAccPeriod : accPeriodMap.keySet()) { accPeriod.put(accPeriodMap.get(getAccPeriod).Name, getAccPeriod ); } 
        
        
        Map<Id, GL_Account__c> glAccountMap = new Map<Id, GL_Account__c>([Select Id, Name, GL_Account_Description__c,GL_Account_Short_Description__c,Currency__c From GL_Account__c]); 
        Map<String, Id> glAccount = new Map<String, Id>(); 
        For (Id getGlAccount : glAccountMap.keySet()) { glAccount.put(glAccountMap.get(getGlAccount).Name, getGlAccount );}
        
        
        Map<Id,Shipment_Order__c> shipmentMap = new Map<Id,Shipment_Order__c>([SELECT Id,POD_Date__c FROM Shipment_Order__c where Id=:shipmentIds.values()]);
        
        List<Transaction_Line_New__c> transactionLines = new List< Transaction_Line_New__c >();
        
        for(Invoice_New__c invoice: invoiceList){
            Date maxDate = (invoice.POD_Date_New__c != null && invoice.POD_Date_New__c > invoice.Invoice_Sent_Date__c ) ? 
                invoice.POD_Date_New__c : 
            (invoice.Invoice_Sent_Date__c == null ? date.today() : invoice.Invoice_Sent_Date__c); 
                if(isChanged(invoice,'Invoice_Status__c') && invoice.Invoice_Status__c == 'Paid by Tipalti') {
                    
                    if(invoice.POD_Date_New__c == null){
                        String AccountingPeriod = '';
                        if(invoice.Paid_Date_Tipalti__c != null){
                            String Year = invoice.Paid_Date_Tipalti__c.year()+'-';
                            String Month = invoice.Paid_Date_Tipalti__c.month()+'';
                            if(Month.length() == 1){
                                Month = '0'+ Month;
                            }
                            AccountingPeriod =  Year + Month;
                        }
                        
                        transactionLines.add(new Transaction_Line_New__c(
                            Amount__c = invoice.Amount_Paid_via_Tipalti__c  ,   
                            Business_Partner__c = invoice.Account__c,
                            Description__c = 'Prepayments',
                            Accounting_Period_New__c = invoice.Accounting_Period_Formula__c == null ? accPeriod.get(AccountingPeriod) : accPeriod.get(invoice.Accounting_Period_Formula__c),
                            Date__c = maxDate,
                            GL_Account__c = glAccount.get('611500'),
                            Invoice__c = invoice.Id,
                            RecordTypeId = Schema.Sobjecttype.Transaction_Line_New__c.getRecordTypeInfosByDeveloperName().get('Tipalti_Payment').getRecordTypeId(),
                            Revenue_Type__c = 'Actual',
                            Short_Description__c = 'Prepayments',
                            Status__c ='Posted',
                            Type_Code__c = 'CP'
                        ));
                        
                        transactionLines.add(new Transaction_Line_New__c(
                            Amount__c = (invoice.Amount_Paid_via_Tipalti__c * -1)  ,    
                            Business_Partner__c = invoice.Account__c,
                            Description__c = 'JPM Turnkey Export Compliance UK Ltd',
                            Accounting_Period_New__c = invoice.Accounting_Period_Formula__c == null ? accPeriod.get(AccountingPeriod) : accPeriod.get(invoice.Accounting_Period_Formula__c),
                            Date__c = maxDate,
                            GL_Account__c = glAccount.get('67105363'),
                            Invoice__c = invoice.Id,
                            RecordTypeId = Schema.Sobjecttype.Transaction_Line_New__c.getRecordTypeInfosByDeveloperName().get('Tipalti_Payment').getRecordTypeId(),
                            Revenue_Type__c = 'Actual',
                            Short_Description__c = 'JPM Turnkey Export Compliance UK Ltd',
                            Status__c ='Posted',
                            Type_Code__c = 'CP'
                        ));
                        
                    }
                    else if(invoice.POD_Date_New__c == shipmentMap.get(shipmentIds.get(invoice.Id)).POD_Date__c){
                        
                        
                        transactionLines.add(new Transaction_Line_New__c(
                            Amount__c = invoice.Amount_Paid_via_Tipalti__c  ,   
                            Business_Partner__c = invoice.Account__c,
                            Description__c = 'Supplier Control Account',
                            Accounting_Period_New__c = accPeriod.get(invoice.Accounting_Period_Formula__c),
                            Date__c = maxDate,
                            GL_Account__c = glAccount.get('920000'),
                            Invoice__c = invoice.Id,
                            RecordTypeId = Schema.Sobjecttype.Transaction_Line_New__c.getRecordTypeInfosByDeveloperName().get('Tipalti_Payment').getRecordTypeId(),
                            Revenue_Type__c = 'Actual',
                            Short_Description__c = 'AP Control',
                            Status__c ='Posted',
                            Type_Code__c = 'CP'
                        ));
                        
                        transactionLines.add(new Transaction_Line_New__c(
                            Amount__c = (invoice.Amount_Paid_via_Tipalti__c * -1)  ,   
                            Business_Partner__c = invoice.Account__c,
                            Description__c = 'JPM Turnkey Export Compliance UK Ltd',
                            Accounting_Period_New__c = accPeriod.get(invoice.Accounting_Period_Formula__c),
                            Date__c = maxDate,
                            GL_Account__c = glAccount.get('67105363'),
                            Invoice__c = invoice.Id,
                            RecordTypeId = Schema.Sobjecttype.Transaction_Line_New__c.getRecordTypeInfosByDeveloperName().get('Tipalti_Payment').getRecordTypeId(),
                            Revenue_Type__c = 'Actual',
                            Short_Description__c = 'JPM Turnkey Export Compliance UK Ltd',
                            Status__c ='Posted',
                            Type_Code__c = 'CP'
                        ));
                        
                    }
                    
                }else if( (invoice.Invoice_Status__c == 'Paid by Tipalti')
                         && (isChanged(invoice,'POD_Date_New__c') &&  invoice.POD_Date_New__c == shipmentMap.get(shipmentIds.get(invoice.Id)).POD_Date__c)){
                             
                             
                             transactionLines.add(new Transaction_Line_New__c(
                                 Amount__c = (invoice.Amount_Paid_via_Tipalti__c * -1)  ,     
                                 Business_Partner__c = invoice.Account__c,
                                 Description__c = 'Prepayments',
                                 Accounting_Period_New__c = accPeriod.get(invoice.Accounting_Period_Formula__c),
                                 Date__c = maxDate,
                                 GL_Account__c = glAccount.get('611500'),
                                 Invoice__c = invoice.Id,
                                 RecordTypeId = Schema.Sobjecttype.Transaction_Line_New__c.getRecordTypeInfosByDeveloperName().get('Tipalti_Payment').getRecordTypeId(),
                                 Revenue_Type__c = 'Actual',
                                 Short_Description__c = 'Prepayments',
                                 Status__c ='Posted',
                                 Type_Code__c = 'CP'
                             ));
                             transactionLines.add(new Transaction_Line_New__c(
                                 Amount__c = invoice.Amount_Paid_via_Tipalti__c  ,   
                                 Business_Partner__c = invoice.Account__c,
                                 Description__c = 'Supplier Control Account',
                                 Accounting_Period_New__c = accPeriod.get(invoice.Accounting_Period_Formula__c),
                                 Date__c = maxDate,
                                 GL_Account__c = glAccount.get('920000'),
                                 Invoice__c = invoice.Id,
                                 RecordTypeId = Schema.Sobjecttype.Transaction_Line_New__c.getRecordTypeInfosByDeveloperName().get('Tipalti_Payment').getRecordTypeId(),
                                 Revenue_Type__c = 'Actual',
                                 Short_Description__c = 'AP Control',
                                 Status__c ='Posted',
                                 Type_Code__c = 'CP'
                             ));
                             
                         }
            
        }
        
        if(transactionLines != null && !transactionLines.isEmpty()){
            insert transactionLines;
        }
    }
    
    
}