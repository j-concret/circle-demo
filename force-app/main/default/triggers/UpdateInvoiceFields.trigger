trigger  UpdateInvoiceFields on Shipment_Order__c (after insert, after update) {
    
    Set<ID> soIds= new Set<ID>();
    
    for(Shipment_Order__c  so: Trigger.new){
        if(so.Shipping_Status__c == 'POD Received' || so.Shipping_Status__c == 'Customs Clearance Docs Received' || so.Shipping_Status__c == 'Cancelled - With fees/costs'){
            soIds.add(so.Id);
        }
    }
    List<Customer_Invoice__c> relatedCI =  [Select Id, Billable__c, Shipment_Order__c from Customer_Invoice__c where Shipment_Order__c in :soIds];
    List<Supplier_Invoice_per_Shipment_Order__c> relatedSIPSO= [Select Id, Incurred__c, Shipment_Order__c from Supplier_Invoice_per_Shipment_Order__c];
    
    List<Customer_Invoice__c> relatedInvoicesToUpdate = new List<Customer_Invoice__c>();
    List<Supplier_Invoice_per_Shipment_Order__c> relatedSupplierInvoicesToUpdate = new List<Supplier_Invoice_per_Shipment_Order__c>();
    
    if(relatedCI.size()>0){
        for(Customer_Invoice__c rd : relatedCI){
            rd.Billable__c = True;
            relatedInvoicesToUpdate.add(rd);
        }
    }
    
    if(relatedSIPSO.size()>0){
        for(Supplier_Invoice_per_Shipment_Order__c rrd : relatedSIPSO){
            rrd.Incurred__c = True;
            relatedSupplierInvoicesToUpdate.add(rrd);
        }
    }
    
    if(relatedInvoicesToUpdate.size()>0){
    update relatedInvoicesToUpdate;
    }
    
    if(relatedSupplierInvoicesToUpdate.size()>0){
    update relatedSupplierInvoicesToUpdate;
    }
}