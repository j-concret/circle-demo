trigger AccountTrigger on Account (After insert,After Update,before update, Before Insert) {
    
    if(trigger.isBefore && trigger.isUpdate){
        for(Account acc : trigger.new){
           if(acc.Customer_Success_Executive__c  != trigger.oldMap.get(acc.id).Customer_Success_Executive__c){
            	acc.CSE_Allocated__c = System.now().date();
        	} 
        }
        
    }
    if(trigger.isBefore && trigger.isInsert){
                
        for(Account acc : trigger.new){
           if(acc.RecordTypeText__c == 'Client (E-commerce)'){
            	acc.Site__c = 'LU01E';
        	} 
        }
        
    }
    if(Trigger.isAfter){
        MasterTaskProcess.taskCreation(new List<Id>(Trigger.newMap.keySet()));
    }
    
    
}