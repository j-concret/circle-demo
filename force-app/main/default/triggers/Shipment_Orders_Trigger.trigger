trigger Shipment_Orders_Trigger on Shipment_Order__c (before insert, before update, before delete, after insert, after update, after delete, after undelete)
{
    TriggerFactory.createHandler(Shipment_Order__c.sobjectType);
}