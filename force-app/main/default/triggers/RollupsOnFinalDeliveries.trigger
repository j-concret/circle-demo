trigger RollupsOnFinalDeliveries on Final_Delivery__c (After insert, After update,Before delete) {
    if(RecusrsionHandler.skipFDTriggerExecutionOnSOCreation) return;
    List<Final_Delivery__c> finalDels = (Trigger.isInsert || Trigger.isUpdate) ? Trigger.new : Trigger.old;
    List<Id> soIds = new List<Id>();
    List<Id> soIdsFORDel = new List<Id>();
    List<Id> shipmentOrderIdsForSOLog = new List<Id>();
    List<Shipment_Order__c> shipmentOrders = new List<Shipment_Order__c>();
    Map<Id, Shipment_Order__c> shipmentOrdersToUpdate = new Map<Id, Shipment_Order__c> ();
    Map<Id,Final_Delivery__c> shipmentsWithFinalDelivery = new Map<Id,Final_Delivery__c>();
    for(Final_Delivery__c finalDel : finalDels) {
        if(String.isNotBlank(finalDel.shipment_Order__c)) {
            soIds.add(finalDel.shipment_Order__c);
        }
    }
    if(!soIds.isEmpty()) {
        shipmentOrdersToUpdate = new Map<Id, Shipment_Order__c>([SELECT Id,Invoice_Payment__c,Number_of_Final_Deliveries_Client__c,Sub_Status_Update__c,Pick_up_Coordination__c,Shipping_Documents__c,Customs_Compliance__c,Mapped_Shipping_status__c, Shipping_Status__c FROM Shipment_Order__c WHERE Id IN: soIds]);
    }
    for(Final_Delivery__c finalDel : finalDels) {
        if(Trigger.isUpdate) {
            if(trigger.oldMap.get(finalDel.id).Delivery_Status__c != finalDel.Delivery_Status__c && !CreateSoLogonStatusChange.isSOStatusChanged && (finalDel.Delivery_Status__c == 'Awaiting POD' || finalDel.Delivery_Status__c == 'POD Received')) {
                if(shipmentOrdersToUpdate.get(finalDel.shipment_Order__c).Invoice_Payment__c != 'Green' || shipmentOrdersToUpdate.get(finalDel.shipment_Order__c).Pick_up_Coordination__c != 'Green' || shipmentOrdersToUpdate.get(finalDel.shipment_Order__c).Shipping_Documents__c != 'Green' || shipmentOrdersToUpdate.get(finalDel.shipment_Order__c).Customs_Compliance__c != 'Green') {
                    throw new RecusrsionHandler.CustomException('Please complete all pending tasks first.');
                }
                shipmentOrderIdsForSOLog.add(finalDel.shipment_Order__c);
                shipmentsWithFinalDelivery.put(finalDel.shipment_Order__c,finalDel);
                CreateSoLogonStatusChange.isSOStatusChanged= TRUE;
            }
        }
    }
    if(!soIds.isEmpty()) {
        Map<String, Integer> shippingStatusWithOrder = new Map<String, Integer>();
        shippingStatusWithOrder.put('Final Delivery in Progress', 1);
        shippingStatusWithOrder.put('Awaiting POD', 2);
        shippingStatusWithOrder.put('POD Received', 3);
        shippingStatusWithOrder.put('Customs Clearance Docs Received', 4);
        Map<String, Integer> FDAStatusWithOrder = new Map<String, Integer>();
        FDAStatusWithOrder.put('Awaiting POD',1);
        FDAStatusWithOrder.put('POD Received',2);
		//FDAStatusWithOrder.put('Final Delivery in Progress',3);
        Set<Id> soIdsToIgnore = new Set<Id>();
        Map<Id, Set<String> > soIdWithAllFinalDeliveries = new Map<Id, Set<String> >();
        for(Final_Delivery__c fd : [SELECT Id, Delivery_Status__c, shipment_order__c FROM Final_Delivery__c
                                    WHERE shipment_order__c IN :soIds ]) {
            if(String.isBlank(fd.Delivery_Status__c) ||
               (String.isNotBlank(fd.Delivery_Status__c) && !FDAStatusWithOrder.containsKey(fd.Delivery_Status__c))) {
                soIdsToIgnore.add(fd.shipment_order__c);
                continue;
            }
            if(soIdWithAllFinalDeliveries.containsKey(fd.shipment_order__c)) {
                soIdWithAllFinalDeliveries.get(fd.shipment_order__c).add(fd.Delivery_Status__c);
            }else{
                soIdWithAllFinalDeliveries.put(fd.shipment_order__c, new Set<String> {fd.Delivery_Status__c});
            }
        }
        for(AggregateResult aggResult : [SELECT COUNT(ID) CountID,Shipment_Order__c FROM Final_Delivery__c WHERE Shipment_Order__c IN:soIds GROUP BY Shipment_Order__c]) {
            Decimal cnt = (Decimal) aggResult.get('CountID');
            String soid = String.valueOf(aggResult.get('Shipment_Order__c'));
            shipmentOrders.add(new Shipment_Order__c(Id = soid,
                                                     Final_Deliveries_New__c = cnt > shipmentOrdersToUpdate.get(soid).Number_of_Final_Deliveries_Client__c ? cnt : shipmentOrdersToUpdate.get(soid).Number_of_Final_Deliveries_Client__c,
                                                     Number_of_Final_Deliveries_Auto__c = cnt)
                               );
        }
        for(Shipment_Order__c so : shipmentOrders) {
            
            if(soIdWithAllFinalDeliveries.containsKey(so.Id) &&
               !soIdsToIgnore.contains(so.Id)) {
                String newStatusToPopulate = '';
                if(soIdWithAllFinalDeliveries.get(so.Id).size() == 1) {
                    for(String status : soIdWithAllFinalDeliveries.get(so.Id)) {
                        newStatusToPopulate = status;
                    }
                }else{
                    List<Integer> FDStatusOrder = new List<Integer>();
                    for(String status : soIdWithAllFinalDeliveries.get(so.Id)) {
                        FDStatusOrder.add(FDAStatusWithOrder.get(status));
                    }
                    FDStatusOrder.sort();
                    for(String fdsStatus : FDAStatusWithOrder.keySet()) {
                        if(FDAStatusWithOrder.get(fdsStatus) == FDStatusOrder[0]) {
                            newStatusToPopulate = fdsStatus;
                            break;
                        }
                    }
                }
                Shipment_Order__c currentSO = shipmentOrdersToUpdate.get(so.Id);
                   So.Sub_Status_Update__c = 'NONE';
                if(!shippingStatusWithOrder.containsKey(newStatusToPopulate) || (shippingStatusWithOrder.containsKey(currentSO.Shipping_Status__c)
                                                                                 && shippingStatusWithOrder.get(currentSO.Shipping_Status__c) >= shippingStatusWithOrder.get(newStatusToPopulate))) {
                    continue;
                }
                if(currentSO.Mapped_Shipping_status__c != 'Delivered') {
                    soIdsFORDel.add(so.Id);
                }
                so.Shipping_Status__c = newStatusToPopulate;
                if(newStatusToPopulate == 'Awaiting POD' && So.Final_Delivery_Date__c ==null) {
                    So.Final_Delivery_Date__c=system.today();
                }else if(newStatusToPopulate == 'POD Received' && So.POD_Date__c ==null) {
                    So.POD_Date__c=system.today();
                }
            }
        }
    }
    if(!shipmentOrderIdsForSOLog.isEmpty()) {
        list<Shipment_Order__C> SOList = [select Id,Shipping_status__c,Sub_Status_Update__c,Clearance_Destination__c,Ship_to_Country_new__c,Cost_Estimate_Acceptance_Date__c,Mapped_Shipping_status__c,CPA_v2_0__c,CPA_v2_0__r.Final_Destination__c,CPA_v2_0__r.Tracking_Term__c,Client_Contact_for_this_Shipment__r.Client_Notifications_Choice__c,Client_Contact_for_this_Shipment__r.Major_Minor_notification__c,Client_Contact_for_this_Shipment__r.Client_Tasks_notification_choice__c,Expected_Date_to_Next_Status__c from Shipment_Order__c where id in:shipmentOrderIdsForSOLog];
        SOTravelCreation.forFDAStatusChange(SOlist,shipmentsWithFinalDelivery);
    }
    if(soIdsFORDel !=null && !soIdsFORDel.isEmpty()) {
        Map<Id, Shipment_Order__c> SoMap = SOTravelCreation.forAllFDDeliveredStatusChange(soIdsFORDel);
        for(Shipment_Order__c shiporder : shipmentOrders) {
            Shipment_Order__c shipmentOrder = soMap.get(shiporder.Id);
            if(shipmentOrder != null && shipmentOrder.Id != null) {
                shiporder.banner_feed__c = shipmentOrder.banner_feed__c;
                shiporder.Sub_Status_Update__c = 'NONE';
            }
        }
    }
    if(shipmentOrders != null && !shipmentOrders.isEmpty()) {
        update shipmentOrders;
    }
}