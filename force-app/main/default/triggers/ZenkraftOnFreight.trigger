/* AS: Trigget to perform
 * - Create Zenkraft rates on after creation
 * - Mark existing rates as inactive before update freight status
 */


trigger ZenkraftOnFreight on Freight__c (after insert,before insert, before update, after update, after delete) {


    
    if(ZenkraftOnFreightHandler.skipTriggerExecution) {
        return;
    }

    if(Trigger.IsInsert && Trigger.isBefore) {
        ZenkraftOnFreightHandler.calculateCourierType(Trigger.new);
        ZenkraftOnFreightHandler.populateMarkup(Trigger.new);
        ZenkraftOnFreightHandler.populateDefaultAddress(Trigger.new); //new Code for rollout
    }

    if(Trigger.IsInsert && Trigger.isAfter) {

        // calling it explicitly in case of insertion (Handling RECORD LOCK ERROR)
        //ZenkraftOnFreightHandler.generateCourierRates(Trigger.new, Trigger.oldMap, false);
        MasterTaskProcess.taskCreation(new List<Id>(Trigger.newMap.keySet()));
        FreightPBTrigger.freightUpdateFlowCode(Trigger.new, true);
        ZenkraftOnFreightHandler.skipTriggerExecution = true;
    }

    if(Trigger.IsUpdate && Trigger.isAfter) {
        MasterTaskProcess.taskCreation(new List<Id>(Trigger.newMap.keySet()));

        Set<Id> soIds = new Set<Id>();

        for(Freight__c f : Trigger.new) {
            if(f.Shipment_order__c != null && (f.Courier_Type__c != Trigger.oldMap.get(f.Id).Courier_Type__c)
               || (f.Create_Pickup__c != Trigger.oldMap.get(f.Id).Create_Pickup__c) || ZenkraftOnFreightHandler.addPickupAndFD
               || f.Ship_From_Address__c != Trigger.oldMap.get(f.Id).Ship_From_Address__c
               ||f.SF_Company__c!= Trigger.oldMap.get(f.Id).SF_Company__c
               || f.Pickup_availability_Close__c != Trigger.oldMap.get(f.Id).Pickup_availability_Close__c) {
                soIds.add(f.Shipment_Order__c);
            }
        }
        if(!soIds.isEmpty() && !RecusrsionHandler.freightUpdateFromSO) {
            MasterTaskProcess.taskCreation(new List<Id>(soIds));
        }
    }

    if(Trigger.isUpdate && Trigger.isBefore) {
        
        //in both 2 method so getting updated so need to join them or other below code as well.
        ZenkraftOnFreightHandler.populateMarkup(Trigger.new);
        FreightPBTrigger.freightUpdateFlowCode(Trigger.new, false);
        FreightPBTrigger.updateSo(Trigger.new);

        FreightPBTrigger.updateSupplier(Trigger.new);

        ZenkraftOnFreightHandler.calculateCourierType(Trigger.new);
        //ZenkraftOnFreightHandler.updateFreightFields(Trigger.new);
        
        // running the same method again to check if ZK value changes.
        //FreightPBTrigger.freightUpdateFlowCode(Trigger.new, false);
        ZenkraftOnFreightHandler.populateDefaultAddress(Trigger.new);
        ZenkraftOnFreightHandler.updateAddresses(Trigger.new, Trigger.oldMap);

        
        ZenkraftOnFreightHandler.generateCourierRates(Trigger.new, Trigger.oldMap, true);

    }
}