trigger PartMatchingTrigger_V3 on Part__c (before insert, after insert, before update, after update, after delete, before delete) {
    Map<String,String> categoryUS_HTS_Code = new Map<String,String> {
        'Audio devices'=>'851810',
        'Cameras'=>'852580',
        'Cellphones'=>'851712',
        'Gaming and virtual reality'=>'847160',
        'Power equipment'=>'850440',
        'Printers, scanners, copiers'=>'844332',
        'Rack (cabinet only), brackets and tools'=>'830249',
        'Servers, controllers and parts'=>'847150',
        'Storage devices'=>'852351',
        'Visual devices'=>'852859',
        'Routers, switches and transceivers'=>'851762',
        'Laptops & PCs'=>'847130',
        'Firewalls'=>'851762'
    };
    Set<String> partName = new Set<String>();
    Set<String> hsCode4 = new Set<String>();
    Set<String> accountId = new Set<String>();
    Set<String> clearanceDestination = new Set<String>();

    Set<Id> SoIds = new Set<Id>();
    Set<Id> productId = new Set<Id>();

    if(Trigger.isInsert && Trigger.isBefore) {
        Set<Id> zeeSOIds = new Set<Id>();
        for (Part__c li : Trigger.new) {
            li.Search_Name_Text__c = getSearchName(li.name);
            accountId.add(li.AccountId__c);
            partName.add(li.Search_Name_Text__c.trim());
            if(li.Shipment_Order__c != null) {
                zeeSOIds.add(li.Shipment_Order__c);
            }
        }
        System.debug('Before  Insert  In ');
        zeeSOIds = new Map<Id,Shipment_Order__c>([Select Id FROM Shipment_Order__c WHERE Id IN: zeeSOIds AND (RecordType.Name = 'Zee Cost Estimate' OR RecordType.Name = 'Zee Shipment Order')]).keySet();

        //Map<String, Id> matchClientParts = new Map<String, Id>();
        //Map<String, Id> matchAllParts = new Map<String, Id>();
        Map<String,Part__c> catalogueProds = new Map<String,Part__c>();
        Boolean v0Matched = false;

        for(Product_Catalogue__c proCat : [SELECT Id,Part__c,Part__r.Manufacturer_Name__c,Part__r.Possible_Countries_of_Origin__c,Part__r.Country_of_Origin2__c,Part__r.COO_2_digit__c,Part__r.US_HTS_Code__c,Part__r.ECCN_NO__c,Part__r.Encryption__c,Part__r.Wireless_Capability__c,Part__r.Sub_Category__c,Part__r.Lithium_Batteries_product__c,Part__r.Li_ion_Batteries__c,Part__r.Lithium_Battery_Types__c,Part__r.Search_Name_Text__c,Account__c,Part__r.Product__c FROM Product_Catalogue__c WHERE Part__r.Product__c != null AND Part__r.Search_Name_Text__c IN:partName AND Account__c IN: accountId AND Part__r.Part_eligible_for_V0_matching__c = true]) {
            catalogueProds.put(proCat.Part__r.Search_Name_Text__c + proCat.Account__c,(Part__c)proCat.getSobject('Part__r'));
        }

        /* for (Part__c prt: [SELECT Id, AccountId__c,Product__c, Search_Name__c, Product__r.Manufacturer_Text_Formula__c FROM Part__c WHERE Search_Name__c in: partName AND Product__c != null]) {
            if(accountId.contains(prt.AccountId__c))
                matchClientParts.put(prt.Search_Name__c, prt.Product__c );
            else
                matchAllParts.put(prt.Search_Name__c, prt.Product__c);
           } */
        Map<Id, Product2> searchNameProducts = new map<Id, Product2>();
        if(PartMatchingV3.searchNameProducts.isEmpty()) {
            searchNameProducts = new Map<Id, Product2>( [SELECT Id, Extended_description__c,Country_Two_Digit__c,Search_Name__c,Country_of_Origin2__c,US_HTS_Code__c,ECCN__c,Encryption__c,Wireless_capability__c,Sub_Category__c,Suggested_Country_of_Origin__c,Manufacturer_Text_Formula__c,Li_on_batteries__c,Li_ion_Battery_Types__c, Product_Name_Alias__c FROM Product2]);
            PartMatchingV3.searchNameProducts = searchNameProducts;
        }else{
            searchNameProducts = PartMatchingV3.searchNameProducts;
        }
        Map<String, Id> searchProducts = new Map<String, Id>();
        Map<String, Id> aliasList = new Map<String, Id>();

        for (Id getProductsBySearch: searchNameProducts.keySet()) {
            searchProducts.put(searchNameProducts.get(getProductsBySearch).Search_Name__c, getProductsBySearch);
            String alias = searchNameProducts.get(getProductsBySearch).Product_Name_Alias__c;
            if(alias != null) {
                for (String mapAlias : alias.split(',')) {
                    aliasList.put(mapAlias, getProductsBySearch);
                }
            }
        }
        List<Product_matching_Rule__c> PMR = new List<Product_matching_Rule__c>();
        List<Product_matching_Rule__c> PMRV3 = new List<Product_matching_Rule__c>();
        for(Product_matching_Rule__c pmrule :  [SELECT id,name,Fields_to_be_update__c,Rule_Version__c,Contains__c,Does_Not_Contains__c,Product_to_be_match__c FROM Product_matching_Rule__c WHERE Rule_Active__c = true AND Rule_Version__c IN ('V2','V3')]) {
            if(pmrule.Rule_Version__c == 'V2')
                PMR.add(pmrule);
            else
                PMRV3.add(pmrule);

        }

        List<Part__c> unmatchedParts = new List<Part__c>();
        for (Part__c li : Trigger.new) {
            v0Matched = false;
            // Changing the Record Type for Zee
            if(li.Shipment_Order__c != null && zeeSOIds.Contains(li.Shipment_Order__c)) {
                li.RecordTypeId = PartMatchingV3.zeeRecordType;
            }

            //V0 Start
            if(catalogueProds.containsKey(li.Search_Name_Text__c + li.AccountId__c)) {
                System.debug('V0 Matched');
                v0Matched = true;
                Part__c catPart = catalogueProds.get(li.Search_Name_Text__c + li.AccountId__c);
                li.Catalogue_Part__c = catPart.Id;
                li.Product__c = catPart.Product__c;
                li.Possible_Countries_of_Origin__c = catPart.Possible_Countries_of_Origin__c;
                // li.Country_of_Origin2__c = catPart.Country_of_Origin2__c;
                li.COO_2_digit__c = catPart.COO_2_digit__c;
                li.US_HTS_Code__c = catPart.US_HTS_Code__c;
                li.ECCN_NO__c = catPart.ECCN_NO__c;
                li.Li_ion_Batteries__c = catPart.Li_ion_Batteries__c;
                li.Lithium_Battery_Types__c = catPart.Lithium_Battery_Types__c;

                li.Encryption__c = searchNameProducts.get(li.product__c).Encryption__c;
                li.Manufacturer_Name__c = searchNameProducts.get(li.product__c).Manufacturer_Text_Formula__c;
                li.Wireless_Capability__c = searchNameProducts.get(li.product__c).Wireless_capability__c;
                li.Sub_Category__c = searchNameProducts.get(li.product__c).Sub_Category__c;
                li.Lithium_Batteries_product__c = searchNameProducts.get(li.product__c).Li_on_batteries__c;

                li.Part_Matching_Method__c = 'Auto Matched';
                li.Part_Linked_to_Product_Date__c = date.today();
                hsCode4.add(li.US_HTS_Code__c.left(4)+'%');
            }
            //v1 start
            /* else if(matchClientParts.get(li.Search_Name__c) != null) {
                li.Product__c = matchClientParts.get(li.Search_Name__c);
                li.Part_Matching_Method__c = 'Auto Matched';
                li.Part_Linked_to_Product_Date__c = date.today();
               }
               else if(matchAllParts.get(li.Search_Name__c) != null) {
                li.Product__c = matchAllParts.get(li.Search_Name__c);
                li.Part_Matching_Method__c = 'Auto Matched';
                li.Part_Linked_to_Product_Date__c = date.today();
               }*/
            else if(searchProducts.get(li.Search_Name_Text__c) != null) {
                li.Product__c = searchProducts.get(li.Search_Name_Text__c);
                li.Part_Matching_Method__c = 'Auto Matched';
                li.Part_Linked_to_Product_Date__c = date.today();
            }
            else if(aliasList.get(li.Search_Name_Text__c) != null) {
                li.Product__c = aliasList.get(li.Search_Name_Text__c);
                li.Part_Matching_Method__c = 'Auto Matched';
                li.Part_Linked_to_Product_Date__c = date.today();
            }
            //v2 block start
            else if(li.Shipment_Order__c != null && li.RecordTypeId != PartMatchingV3.zeeRecordType) {


                li.Start_V2_Product_Match__c = true;

                List<String> Finalmatch= new List<String>();
                String s = li.Name+' '+li.Description_and_Functionality__c;
                String S1 = s.toUpperCase();

                for(Integer i=0; PMR.size()>i; i++) {

                    Boolean Allcontainsmatched = false;
                    Boolean Doesnotcontainsmatched = false;

                    List<String> ContainsList = new List<String>();
                    List<String> DoesnotContainsList = new List<String>();

                    List<String> ConSplittedbyComma = new List<String>();
                    List<String> DoesNotConSplittedbyComma = new List<String>();

                    // All words must contain
                    String Con = PMR[i].Contains__c;
                    String Con1 = Con.toUpperCase();
                    ConSplittedbyComma = Con1.split(',');

                    if(PMR[i].Does_Not_Contains__c != null  || !String.isBlank(PMR[i].Does_Not_Contains__c) ) {
                        String notCon = PMR[i].Does_Not_Contains__c;
                        String notCon1 = notCon.toUpperCase();
                        DoesNotConSplittedbyComma = notCon1.split(',');
                    }
                    for (String Constr : ConSplittedbyComma) {
                        if(S1.contains(Constr)) {
                            ContainsList.add(Constr);
                        }
                        for (String NotConstr : DoesNotConSplittedbyComma) {
                            if(S1.contains(NotConstr)) {
                                DoesnotContainsList.add(NotConstr);
                            }
                        }
                    }

                    if((ConSplittedbyComma.size() == ContainsList.size()) && ContainsList.size() > 0) {
                        Allcontainsmatched = true;
                    }
                    if( DoesnotContainsList.size() > 0) {
                        Doesnotcontainsmatched = false;
                    } else {
                        Doesnotcontainsmatched = true;//PMR[i].Does_Not_Contains__c == null ;
                    }

                    if(( Allcontainsmatched && Doesnotcontainsmatched && PMR[i].Does_Not_Contains__c != null )) {
                        li.Start_V2_Product_Match__c = true;
                        li.product__c = PMR[i].Product_to_be_match__c;
                        li.Part_Matching_Method__c = 'Auto Matched';
                        li.Matched_Rule__c =  PMR[i].Name;
                        break;
                    }
                    else if( Allcontainsmatched && Doesnotcontainsmatched && PMR[i].Does_Not_Contains__c == null ) {
                        li.Start_V2_Product_Match__c = true;
                        li.product__c = PMR[i].Product_to_be_match__c;
                        li.Part_Matching_Method__c = 'Auto Matched';
                        li.New_Product__c = true;
                        li.Matched_Rule__c =  PMR[i].Name;
                        break;
                    }
                }
            }
            // v2 block end

            li.All_Matching_Complete__c = true;
            li.CPA_Changed__c = false;
            li.Account_Text__c = li.AccountId__c;
            li.Ship_To_Country_Text__c = li.Ship_To_Country__c;
            if(li.Ship_To_Country__c != null)
                clearanceDestination.add(li.Ship_To_Country__c);

            if(li.product__c != null && !v0Matched) {
                li.Manufacturer_Name__c = searchNameProducts.get(li.product__c).Manufacturer_Text_Formula__c;
                li.Possible_Countries_of_Origin__c = searchNameProducts.get(li.product__c).Country_of_Origin2__c;
                // li.Country_of_Origin2__c = searchNameProducts.get(li.product__c).Suggested_Country_of_Origin__c;
                li.COO_2_digit__c = searchNameProducts.get(li.product__c).Country_Two_Digit__c;
                li.US_HTS_Code__c = searchNameProducts.get(li.product__c).US_HTS_Code__c;
                li.ECCN_NO__c = searchNameProducts.get(li.product__c).ECCN__c;
                li.Encryption__c = searchNameProducts.get(li.product__c).Encryption__c;
                li.Wireless_Capability__c = searchNameProducts.get(li.product__c).Wireless_capability__c;
                li.Sub_Category__c = searchNameProducts.get(li.product__c).Sub_Category__c;
                li.Lithium_Batteries_product__c = searchNameProducts.get(li.product__c).Li_on_batteries__c;
                li.Li_ion_Batteries__c = searchNameProducts.get(li.product__c).Li_on_batteries__c;
                li.Lithium_Battery_Types__c = searchNameProducts.get(li.product__c).Li_ion_Battery_Types__c;
                // getting matched US_HTS_Code for populating rate fields
                hsCode4.add(li.US_HTS_Code__c.left(4)+'%');
            }else{
                if(li.RecordTypeId != PartMatchingV3.zeeRecordType && !v0Matched) {
                    //tecex
                    li.Start_V3_Product_Match__c = true;
                    unmatchedParts.add(li);
                }
                else if(li.US_HTS_Code__c != null || (String.isBlank(li.US_HTS_Code__c) && String.isNotBlank(li.Client_Provided_Category__c) && categoryUS_HTS_Code.containsKey(li.Client_Provided_Category__c))) {
                    if(String.isBlank(li.US_HTS_Code__c)) {
                        li.US_HTS_Code__c = categoryUS_HTS_Code.get(li.Client_Provided_Category__c);
                    }
                    //zee
                    CountryofOriginMap__c countryOfOrigin = CountryofOriginMap__c.getInstance(li.Country_of_Origin__c);
                    if(countryOfOrigin != null && countryOfOrigin.Two_Digit_Country_Code__c != null) {
                        li.COO_2_digit__c = countryOfOrigin.Two_Digit_Country_Code__c;
                    }
                    li.Start_V3_Product_Match__c = false;
                    hsCode4.add(li.US_HTS_Code__c.left(4)+'%');
                }
                li.New_Product__c = true;
            }
            if(li.COO_2_digit__c != null) {
                CountryofOriginMap__c countryOfOrigin = CountryofOriginMap__c.getInstance(li.COO_2_digit__c);
                if(countryOfOrigin != null && countryOfOrigin.Country__c != null) {
                    li.Country_of_Origin2__c = countryOfOrigin.Country__c;
                }
            }else if(li.Country_of_Origin__c != null) {
                CountryofOriginMap__c countryOfOrigin = CountryofOriginMap__c.getInstance(li.Country_of_Origin__c);
                if(countryOfOrigin != null && countryOfOrigin.Two_Digit_Country_Code__c != null) {
                    li.COO_2_digit__c = countryOfOrigin.Two_Digit_Country_Code__c;
                }
                if(countryOfOrigin != null && countryOfOrigin.Country__c != null) {
                    li.Country_of_Origin2__c = countryOfOrigin.Country__c;
                }
            }
            li.Simple_Word__c = li.product__c == null ? null : searchNameProducts.get(li.product__c).Extended_description__c;
        }
        //V3 Part matcing start
        if(!unmatchedParts.isEmpty()) {
            Set<String> newHscode = PartMatchingV3.MapTest(unmatchedParts,PMRV3);
            hsCode4.addAll(newHscode);
        }
        //V3 Part Matching End

        //pupulating rate related fields after US_HTS_Code matched
        Map<Id, HS_Codes_and_Associated_Details__c> usHsCodes = new Map<Id, HS_Codes_and_Associated_Details__c>(
            [SELECT Id,Name, Additional_Part_Specific_Tax_Rate_1__c,
             Additional_Part_Specific_Tax_Rate_2__c,Additional_Part_Specific_Tax_Rate_3__c,
             Country_Name__c, Description__c, Destination_HS_Code__c, Rate__c,Specific_VAT_Rate__c,status__c,
             US_HTS_Code__c, Vat_Exempted__c FROM HS_Codes_and_Associated_Details__c
             WHERE Rate__c != null AND US_HTS_Code__c Like: hsCode4 AND Country_Name__c IN: clearanceDestination
             AND Inactive__c = false Order by Rate__c desc LIMIT 50000]);

        Map<String, Id> allUSCodes = new Map<String, Id>();

        for (Id getAllCodes: usHsCodes.keySet()) {
            String usCode = usHsCodes.get(getAllCodes).Country_Name__c + usHsCodes.get(getAllCodes).US_HTS_Code__c;

            if(!allUSCodes.containsKey(usCode)) {
                allUSCodes.put(usHsCodes.get(getAllCodes).Country_Name__c + usHsCodes.get(getAllCodes).US_HTS_Code__c, getAllCodes);
            }
            else if(usHsCodes.get(getAllCodes).Rate__c > usHsCodes.get(allUSCodes.get(usCode)).Rate__c) {
                allUSCodes.remove(usCode);
                allUSCodes.put(usHsCodes.get(getAllCodes).Country_Name__c + usHsCodes.get(getAllCodes).US_HTS_Code__c, getAllCodes);
            }
        }

        Map<Id, HS_Codes_and_Associated_Details__c> destinationHsCodes = new Map<Id, HS_Codes_and_Associated_Details__c>([SELECT Id, Name, Additional_Part_Specific_Tax_Rate_1__c, Additional_Part_Specific_Tax_Rate_2__c, Additional_Part_Specific_Tax_Rate_3__c, Country_Name__c, Description__c,status__c, Destination_HS_Code__c, Rate__c,Specific_VAT_Rate__c, US_HTS_Code__c, Vat_Exempted__c FROM HS_Codes_and_Associated_Details__c WHERE Destination_HS_Code__c Like: hsCode4 AND Country_Name__c IN: clearanceDestination AND Inactive__c = FALSE Order by Rate__c desc limit 50000]);

        Map<String, Id> allDestinationCodes = new Map<String, Id>();
        Map<String, String> destinationCodeTrunc = new Map<String, String>();

        for (Id getAllDestCodes: destinationHsCodes.keySet()) {
            String destCode = destinationHsCodes.get(getAllDestCodes).Country_Name__c + destinationHsCodes.get(getAllDestCodes).Destination_HS_Code__c;
            if(!allDestinationCodes.containsKey(destCode)) {
                allDestinationCodes.put(destinationHsCodes.get(getAllDestCodes).Country_Name__c + destinationHsCodes.get(getAllDestCodes).Destination_HS_Code__c, getAllDestCodes);
            }

            String InputString = destinationHsCodes.get(getAllDestCodes).Destination_HS_Code__c;
            String revStr = ' ';
            Integer InputStringsize = InputString.length();

            for (Integer i =0; InputString.length()>i; i++) {
                revStr = InputString.substring(0, InputString.length()-i);

                if(!allDestinationCodes.containsKey(destinationHsCodes.get(getAllDestCodes).Country_Name__c + revStr)) {
                    allDestinationCodes.put(destinationHsCodes.get(getAllDestCodes).Country_Name__c + revStr, getAllDestCodes);
                }
                else if(destinationHsCodes.get(getAllDestCodes).Rate__c > destinationHsCodes.get(allDestinationCodes.get(destinationHsCodes.get(getAllDestCodes).Country_Name__c + revStr)).Rate__c) {
                    allDestinationCodes.remove(destinationHsCodes.get(getAllDestCodes).Country_Name__c + revStr);
                    allDestinationCodes.put(destinationHsCodes.get(getAllDestCodes).Country_Name__c + revStr, getAllDestCodes);
                }
                if((InputStringsize-i) == 4) {
                    break;
                }
            }
        }

        Map<Id, Tax_Structure_Per_Country__c> taxes = new Map<Id, Tax_Structure_Per_Country__c>([SELECT Id, Name, Applied_to_Value__c,Tax_Type__c, Default_Duty_Rate__c, Country__r.Name, Country__c, Rate__c FROM Tax_Structure_Per_Country__c WHERE Country__r.Name IN: clearanceDestination]);

        Map<String, Id> clearanceCountry = new Map<String, Id>();

        Map<String, Decimal> rates = new Map<String, Decimal>();
        Map<String, String> appliedTo = new Map<String, String>();

        for (Id getDefaults: taxes.keySet()) {
            clearanceCountry.put(taxes.get(getDefaults).Country__r.Name, taxes.get(getDefaults).Country__c);
            appliedTo.put(taxes.get(getDefaults).Country__r.Name + taxes.get(getDefaults).Tax_Type__c, taxes.get(getDefaults).Applied_to_Value__c);

            if(taxes.get(getDefaults).Tax_Type__c != 'Duties') {
                rates.put(taxes.get(getDefaults).Country__r.Name + taxes.get(getDefaults).Tax_Type__c, taxes.get(getDefaults).Rate__c);
            }
            else {
                rates.put(taxes.get(getDefaults).Country__r.Name + taxes.get(getDefaults).Tax_Type__c, taxes.get(getDefaults).Default_Duty_Rate__c);
            }
        }

        for (Part__c li : Trigger.new) {
            String partDestination = li.Ship_To_Country__c;
            String searchCode = partDestination + (li.US_HTS_Code__c == null ? '' : li.US_HTS_Code__c);
            String usSearchCode = partDestination + (li.US_HTS_Code__c == null ? '' : li.US_HTS_Code__c.left(8));

            if(allUSCodes.get(usSearchCode) != null) {
                li.Matched_HS_Code2__c = allUSCodes.get(usSearchCode);
                li.Matched_HS_Code_Description__c = usHsCodes.get(allUSCodes.get(usSearchCode)).Description__c;
                if(usHsCodes.get(allUSCodes.get(usSearchCode)).status__c != NULL) {
                    li.Restricted_status__c = usHsCodes.get(allUSCodes.get(usSearchCode)).status__c;
                }

                li.Rate__c = usHsCodes.get(allUSCodes.get(usSearchCode)).Rate__c;
                li.Part_Specific_Rate_1__c = usHsCodes.get(allUSCodes.get(usSearchCode)).Additional_Part_Specific_Tax_Rate_1__c;
                li.Part_Specific_Rate_2__c = usHsCodes.get(allUSCodes.get(usSearchCode)).Additional_Part_Specific_Tax_Rate_2__c;
                li.Part_Specific_Rate_3__c = usHsCodes.get(allUSCodes.get(usSearchCode)).Additional_Part_Specific_Tax_Rate_3__c;
                li.Vat_Exempted__c = usHsCodes.get(allUSCodes.get(usSearchCode)).Vat_Exempted__c;
                li.Specific_VAT_Rate__c = usHsCodes.get(allUSCodes.get(usSearchCode)).Specific_VAT_Rate__c;
                li.Default_Duty_Rate__c = rates.get(partDestination + 'Duties');
                li.VAT_Rate__c = rates.get(partDestination + 'VAT');
                li.VAT_Applied_To_Value__c = appliedTo.get(partDestination + 'VAT');
                li.Applied_To_Value__c = appliedTo.get(partDestination + 'Duties');
                li.Clearance_Country__c = clearanceCountry.get(partDestination);
                li.No_HS_Code_Match__c = false;
                li.CPA_Changed__c = false;
            }
            else if(allDestinationCodes.get(searchCode) != null) {
                li.Matched_HS_Code2__c = allDestinationCodes.get(searchCode);
                li.Matched_HS_Code_Description__c = destinationHsCodes.get(allDestinationCodes.get(searchCode)).Description__c;
                if(destinationHsCodes.get(allDestinationCodes.get(searchCode)).status__c != NULL) {
                    li.Restricted_status__c = destinationHsCodes.get(allDestinationCodes.get(searchCode)).status__c;
                }
                li.Rate__c = destinationHsCodes.get(allDestinationCodes.get(searchCode)).Rate__c;
                li.Part_Specific_Rate_1__c = destinationHsCodes.get(allDestinationCodes.get(searchCode)).Additional_Part_Specific_Tax_Rate_1__c;
                li.Part_Specific_Rate_2__c = destinationHsCodes.get(allDestinationCodes.get(searchCode)).Additional_Part_Specific_Tax_Rate_2__c;
                li.Part_Specific_Rate_3__c = destinationHsCodes.get(allDestinationCodes.get(searchCode)).Additional_Part_Specific_Tax_Rate_3__c;
                li.Vat_Exempted__c = destinationHsCodes.get(allDestinationCodes.get(searchCode)).Vat_Exempted__c;
                li.Specific_VAT_Rate__c = destinationHsCodes.get(allDestinationCodes.get(searchCode)).Specific_VAT_Rate__c;
                li.Default_Duty_Rate__c = rates.get(partDestination + 'Duties');
                li.VAT_Rate__c = rates.get(partDestination + 'VAT');
                li.VAT_Applied_To_Value__c = appliedTo.get(partDestination + 'VAT');
                li.Applied_To_Value__c = appliedTo.get(partDestination + 'Duties');
                li.Clearance_Country__c = clearanceCountry.get(partDestination);
                li.No_HS_Code_Match__c = false;
                li.CPA_Changed__c = false;
            }
            else {
                String InputString2 = searchCode;
                String revStr2 = ' ';
                Integer InputStringsize2 = InputString2.length();

                for (Integer i =0; InputString2.length()>i; i++) {
                    revStr2 = InputString2.substring(0, InputString2.length()-i);
                    if(allDestinationCodes.get(revStr2) != null) {
                        li.Matched_HS_Code2__c = allDestinationCodes.get(revStr2);
                        li.Matched_HS_Code_Description__c = destinationHsCodes.get(allDestinationCodes.get(revStr2)).Description__c;
                        li.Restricted_status__c = destinationHsCodes.get(allDestinationCodes.get(revStr2)).status__c;
                        li.Rate__c = destinationHsCodes.get(allDestinationCodes.get(revStr2)).Rate__c;
                        li.Part_Specific_Rate_1__c = destinationHsCodes.get(allDestinationCodes.get(revStr2)).Additional_Part_Specific_Tax_Rate_1__c;
                        li.Part_Specific_Rate_2__c = destinationHsCodes.get(allDestinationCodes.get(revStr2)).Additional_Part_Specific_Tax_Rate_2__c;
                        li.Part_Specific_Rate_3__c = destinationHsCodes.get(allDestinationCodes.get(revStr2)).Additional_Part_Specific_Tax_Rate_3__c;
                        li.Vat_Exempted__c = destinationHsCodes.get(allDestinationCodes.get(revStr2)).Vat_Exempted__c;
                        li.Specific_VAT_Rate__c = destinationHsCodes.get(allDestinationCodes.get(revStr2)).Specific_VAT_Rate__c;
                        li.Default_Duty_Rate__c = rates.get(partDestination + 'Duties');
                        li.VAT_Rate__c = rates.get(partDestination + 'VAT');
                        li.VAT_Applied_To_Value__c = appliedTo.get(partDestination + 'VAT');
                        li.Applied_To_Value__c = appliedTo.get(partDestination + 'Duties');
                        li.Clearance_Country__c = clearanceCountry.get(partDestination);
                        li.No_HS_Code_Match__c = false;
                        li.CPA_Changed__c = false;
                        break;
                    }
                    else {
                        li.No_HS_Code_Match__c = true;
                        li.Matched_HS_Code2__c = null;
                        li.Matched_HS_Code_Description__c = null;
                        li.Restricted_status__c = 'None';
                        li.Rate__c = null;
                        li.Default_Duty_Rate__c = rates.get(partDestination + 'Duties');
                        li.VAT_Rate__c = rates.get(partDestination + 'VAT');
                        li.VAT_Applied_To_Value__c = appliedTo.get(partDestination + 'VAT');
                        li.Applied_To_Value__c = appliedTo.get(partDestination + 'Duties');
                        li.Clearance_Country__c = clearanceCountry.get(partDestination);
                        li.CPA_Changed__c = false;
                    }
                    if((InputStringsize2-i) == 4) {
                        break;
                    }
                }
            }
        }
    }


    if(Trigger.isInsert && Trigger.isAfter) {

        List<String> partNames = new List<String>();
        Map<Id,String> shipmentIdsEccn = new Map<Id,String>();
        Set<Id> shipmentIds = new Set<Id>();
        Decimal partValues = 0;
        System.debug('After Insert  In ');
        for (Part__c li : Trigger.new) {
            partNames.add(li.Name);
            if(li.AccountId__c != null)
                accountId.add(li.AccountId__c);

            String SOECCNNo = 'NONE';

            if(!(String.isBlank(li.Eccn_No__c)) && li.Eccn_No__c.contains('5A002')) {
                SOECCNNo='5A002';
            }
            else if(!(String.isBlank(li.Eccn_No__c)) && li.Eccn_No__c.contains('5D002')) {
                SOECCNNo='5D002';
            }
            if(li.Shipment_Order__c != null ) {
                shipmentIds.add(li.Shipment_Order__c);
            }
            if(SOECCNNo != 'NONE' ) {
                shipmentIdsEccn.put(li.Shipment_Order__c,SOECCNNo);
            }

            if(li.US_HTS_Code__c == null) {
                partValues += li.Total_Value__c;
            }
        }
        //if(shipmentIdsEccn.isEmpty()) return;
        Map<Id,shipment_Order__c> shipmentOrders = new Map<Id,shipment_Order__c>([SELECT Id,Sum_Unmatched_HS_Codes__c, Contains_Batteries_Auto__c, Prohibited_Manufacturer_Auto__c, CPA_v2_0__r.COO_Prohibitions_Text__c, CPA_v2_0__r.Manufacturer_Prohibitions__c,Sum_of_quantity_of_matched_products__c,Manufacturer__c,ECCN_No__c, Number_of_Batteries__c, Number_of_Wireless_Products__c, Restricted_parts__c, Contains_Encrypted_Goods_Auto__c,Contains_Refurbished_Goods__c,(SELECT Id, Li_ion_Batteries__c, Encryption__c, Restricted_status__c,Type_of_Goods__c,Manufacturer_Name__c,Wireless_Capability__c, Shipment_Order__c FROM Parts__r) FROM Shipment_Order__c WHERE Id IN: shipmentIds]);

        populateFormulaFieldsOnSO(shipmentOrders);

        List<AggregateResult> allParts = [SELECT Shipment_Order__c, Manufacturer_Name__c, SUM(Total_Value__C) total, Count_Distinct(US_HTS_Code__c) hsCodes, Sum(CIF_Duties__c) sumCIF,Count(Id) numberParts, sum(FOB_Duties__c) sumFOB,Sum(Quantity__C) qty FROM Part__c WHERE Shipment_Order__c =:shipmentIds GROUP BY Rollup(Shipment_Order__c, Manufacturer_Name__c)];
        System.debug('allParts '+allParts);
        for(AggregateResult a : allParts) {
            if(a.get('Shipment_Order__c') == null ) continue;
            Shipment_Order__c thisSO = shipmentOrders.get(String.valueOf(a.get('Shipment_Order__c')));
            String finQty = 'NONE';
            thisSO.Sum_Unmatched_HS_Codes__c  = partValues;
            Decimal qty = (Decimal) a.get('qty');
            String FinMnfName = (string) a.get('Manufacturer_Name__c');

            if(FinMnfName == 'HPE' && qty >=5) {
                finQty ='>=5';
            }
            else if(FinMnfName == 'Apple' && qty >=4) {
                finQty ='>=4';
            }

            thisSO.of_Line_Items__c = (Decimal) a.get('numberParts');
            thisSO.of_Unique_Line_Items__c = (Decimal) a.get('hsCodes');
            thisSO.Total_CIF_Duties__c = (Decimal) a.get('sumCIF');
            thisSO.Total_FOB_Duties__c = (Decimal) a.get('sumFOB');
            thisSO.Total_Line_Item_Extended_Value__c = (Decimal) a.get('total');
            thisSO.Shipment_Value_USD__c = (Decimal) a.get('total');

            if(String.isBlank(thisSO.ECCN_No__c) || thisSO.ECCN_No__c.contains('NONE')) {
                thisSO.ECCN_No__c = !shipmentIdsEccn.containsKey(thisSO.Id) ? 'NONE' : shipmentIdsEccn.get(thisSO.Id);
            }

            if(String.isBlank(thisSO.Manufacturer__c) || thisSO.Manufacturer__c.contains('NONE')) {
                thisSO.Manufacturer__c = FinMnfName;
            }
            if(String.isBlank(thisSO.Sum_of_quantity_of_matched_products__c) || thisSO.Sum_of_quantity_of_matched_products__c.contains('NONE')) {
                thisSO.Sum_of_quantity_of_matched_products__c = finQty;
            }
        }
        update shipmentOrders.values();

        //Number_of_times_shipped__c updation on product catalogue logic
        if(!partNames.IsEmpty()) {
            List<Product_Catalogue__c> updateCatalogues = new List<Product_Catalogue__c>();
            Map<String,Decimal> catalogueResults = new Map<String,Decimal>();
            for(AggregateResult a :  [SELECT Name, count(Shipment_Order__c) total, Shipment_Order__r.Account__c client FROM Part__c Where Name IN:partNames AND Shipment_Order__r.Account__c IN: accountId GROUP BY Name, Shipment_Order__r.Account__c]) {

                String key = String.valueOf(a.get('Name'))+String.valueOf(a.get('client'));
                catalogueResults.put(key,(Decimal) a.get('total'));
            }

            for(Product_Catalogue__c a : [SELECT Id,Account__c,Number_of_times_shipped__c,Part__r.Name FROM Product_Catalogue__c WHERE Account__c =: accountId AND Part__r.Name IN:partNames]) {
                String key = a.Part__r.Name + a.Account__c;
                if(catalogueResults.containsKey(key)) {
                    a.Number_of_times_shipped__c = catalogueResults.get(key);
                    updateCatalogues.add(a);
                }
            }

            if(!updateCatalogues.isEmpty()) update updateCatalogues;
        }
    }

    if(Trigger.isUpdate && Trigger.isBefore) {
        List<Product2> productsUpdate = new List<Product2>();

        Set<Id> inValidSOSet = new Set<Id>();
        for(Part__c li : Trigger.new) {
            if(li.Shipment_Order__c != null) {
                inValidSOSet.add(li.Shipment_Order__c);
            }
        }


        set<String> invalidSOStatus = new set<String> {'Customs Clearance Docs Received', 'Cancelled - With fees/costs', 'Cancelled - No fees/costs', 'Cost Estimate Abandoned', 'Cost Estimate Rejected'};

        inValidSOSet = new Map<Id, Shipment_Order__c>([SELECT Id FROM Shipment_Order__c WHERE Id IN: inValidSOSet
                                                       AND (Closed_Down_SO__c = true OR Shipping_Status__c IN: invalidSOStatus)]).keySet();



        for (Part__c li : Trigger.new) {

            if(li.Shipment_Order__c != null && inValidSOSet.contains(li.Shipment_Order__c)) {
                continue;
            }
            li.Search_Name_Text__c = getSearchName(li.name);
            li.Ship_To_Country_Text__c = li.Ship_To_Country__c;
            accountId.add(li.AccountId__c);
            if(String.isNotBlank(li.Ship_To_Country__c))
                clearanceDestination.add(li.Ship_To_Country__c);

            partName.add(li.Search_Name_Text__c);
            productId.add(li.Product__c);

        }

        Map<String,Part__c> catalogueProds = new Map<String,Part__c>();
        Boolean v0Matched = false;

        for(Product_Catalogue__c proCat : [SELECT Id,Part__c,Part__r.Manufacturer_Name__c,Part__r.Possible_Countries_of_Origin__c,Part__r.Country_of_Origin2__c,Part__r.COO_2_digit__c,Part__r.US_HTS_Code__c,Part__r.ECCN_NO__c,Part__r.Encryption__c,Part__r.Wireless_Capability__c,Part__r.Sub_Category__c,Part__r.Lithium_Batteries_product__c,Part__r.Li_ion_Batteries__c,Part__r.Lithium_Battery_Types__c,Part__r.Search_Name_Text__c,Account__c,Part__r.Product__c FROM Product_Catalogue__c WHERE Part__r.Product__c != null AND Part__r.Search_Name_Text__c IN:partName AND Account__c IN: accountId AND Part__r.Part_eligible_for_V0_matching__c = true]) {
            catalogueProds.put(proCat.Part__r.Search_Name_Text__c + proCat.Account__c,(Part__c)proCat.getSobject('Part__r'));
        }

        Map<Id, Tax_Structure_Per_Country__c> taxes = new Map<Id, Tax_Structure_Per_Country__c>([SELECT Id, Name, Applied_to_Value__c,Tax_Type__c, Default_Duty_Rate__c, Country__r.Name, Country__c, Rate__c FROM Tax_Structure_Per_Country__c WHERE Country__r.Name in: clearanceDestination]);

        Map<String, Id> clearanceCountry = new Map<String, Id>();

        Map<String, Decimal> rates = new Map<String, Decimal>();
        Map<String, String> appliedTo = new Map<String, String>();
        List<Product_matching_Rule__c> PMR = new List<Product_matching_Rule__c>();
        for(Product_matching_Rule__c pmrule :  [SELECT id,name,Fields_to_be_update__c,Rule_Version__c,Contains__c,Does_Not_Contains__c,Product_to_be_match__c FROM Product_matching_Rule__c WHERE Rule_Active__c = true AND Rule_Version__c = 'V2']) {
            PMR.add(pmrule);
        }

        for (Id getDefaults: taxes.keySet()) {
            clearanceCountry.put(taxes.get(getDefaults).Country__r.Name, taxes.get(getDefaults).Country__c);
            appliedTo.put(taxes.get(getDefaults).Country__r.Name + taxes.get(getDefaults).Tax_Type__c, taxes.get(getDefaults).Applied_to_Value__c);

            if(taxes.get(getDefaults).Tax_Type__c != 'Duties') {
                rates.put(taxes.get(getDefaults).Country__r.Name + taxes.get(getDefaults).Tax_Type__c, taxes.get(getDefaults).Rate__c);
            }
            else {
                rates.put(taxes.get(getDefaults).Country__r.Name + taxes.get(getDefaults).Tax_Type__c, taxes.get(getDefaults).Default_Duty_Rate__c);
            }
        }

        Map<Id, Product2> searchNameProducts = new map<Id, Product2>();
        if(PartMatchingV3.searchNameProducts.isEmpty()) {
            searchNameProducts = new Map<Id, Product2>( [SELECT Id,Extended_description__c, Country_Two_Digit__c,Search_Name__c,Country_of_Origin2__c,US_HTS_Code__c,ECCN__c,Encryption__c,Wireless_capability__c,Sub_Category__c,Suggested_Country_of_Origin__c,Manufacturer_Text_Formula__c,Li_on_batteries__c,Li_ion_Battery_Types__c, Product_Name_Alias__c FROM Product2]);
            PartMatchingV3.searchNameProducts = searchNameProducts;
        }else{
            searchNameProducts = PartMatchingV3.searchNameProducts;
        }

        // Client match final map
        /*  Map<String, Id> matchClientParts = new Map<String, Id>();
           Map<String, Id> matchAllParts = new Map<String, Id>();

           // Client match
           for (Part__c clientPart: [SELECT Id, Product__c,Search_Name__c FROM Part__c WHERE Search_Name__c in: partName AND Product__c != null AND AccountId__c in: accountId]) {
             matchClientParts.put(clientPart.Search_Name__c,clientPart.Product__c);
           }

           for (Part__c matchPrt: [SELECT Id, Product__c,Search_Name__c FROM Part__c WHERE Search_Name__c in: partName AND Product__c != null AND AccountId__c NOT IN: accountId]) {
             matchAllParts.put(matchPrt.Search_Name__c, matchPrt.Product__c);
           } */

        Map<String, Id> aliasList = new Map<String, Id>();
        Map<String, Id> searchProducts = new Map<String, Id>();

        for (Id getProductsBySearch: searchNameProducts.keySet()) {
            searchProducts.put(searchNameProducts.get(getProductsBySearch).Search_Name__c, getProductsBySearch);
            String alias = searchNameProducts.get(getProductsBySearch).Product_Name_Alias__c;
            String aliasId = getProductsBySearch;

            if(alias != null) {
                List<String> mapAlias = alias.split(',');
                for (Integer i = 0; i < mapAlias.size(); i++ ) {
                    aliasList.put(mapAlias[i], getProductsBySearch);
                }
            }
        }

        Map<String,AggregateResult> allSoParts = new Map<String,AggregateResult>();

        for(AggregateResult a: [SELECT Name,Product__c,AVG(Commercial_Value__c) average, MIN(Commercial_Value__c) minimum, MAX(Commercial_Value__c) maximum,Count(Id) numberParts, Max(Shipment_Order__r.POD_Date__c) maxPOD, MAX(US_HTS_Code__c) maxHSCode, max(Country_of_Origin2__c) maxCOO FROM Part__c WHERE Name IN : partName AND Product__c IN : productId AND (Shipment_Order__r.Shipping_Status__c = 'POD Received' Or Shipment_Order__r.Shipping_Status__c = 'Customs Clearance Docs Received') GROUP BY Name,Product__c]) {
            allSoParts.put((String)a.get('Name'),a);
        }

        Map<String,AggregateResult> allDestinationParts = new Map<String,AggregateResult>();

        for(AggregateResult a: [SELECT Name,Ship_To_Country_Text__c, AVG(Commercial_Value__c) average, MIN(Commercial_Value__c) minimum, MAX(Commercial_Value__c) maximum,Count(Id) numberParts, Max(Shipment_Order__r.POD_Date__c) maxPOD, MAX(US_HTS_Code__c) maxHSCode FROM Part__c WHERE Name IN : partName AND Product__c IN : productId AND (Shipment_Order__r.Shipping_Status__c = 'POD Received' Or Shipment_Order__r.Shipping_Status__c = 'Customs Clearance Docs Received') AND Ship_To_Country__c IN : clearanceDestination GROUP BY Name,Ship_To_Country_Text__c]) {
            String key = (String)a.get('Name') + (String)a.get('Ship_To_Country_Text__c');
            allDestinationParts.put(key,a);
        }

        Map<String,AggregateResult> allClientParts = new Map<String,AggregateResult>();

        for(AggregateResult a: [SELECT Name,Shipment_Order__r.Account__c account, AVG(Commercial_Value__c) average, MIN(Commercial_Value__c) minimum, MAX(Commercial_Value__c) maximum,Count(Id) numberParts, Max(Shipment_Order__r.POD_Date__c) maxPOD FROM Part__c WHERE Name IN : partName AND Product__c IN : productId AND (Shipment_Order__r.Shipping_Status__c = 'POD Received' Or Shipment_Order__r.Shipping_Status__c = 'Customs Clearance Docs Received') AND Shipment_Order__r.Account__c IN:accountId GROUP BY Name,Shipment_Order__r.Account__c]) {
            String key = (String)a.get('Name') + (String)a.get('account');
            allClientParts.put(key,a);
        }
        for (Part__c li : Trigger.new) {

            if(li.Shipment_Order__c != null && inValidSOSet.contains(li.Shipment_Order__c)) {
                continue;
            }

            v0Matched = false;
            if(li.Name != Trigger.oldMap.get(li.Id).Name || li.Product__c != Trigger.oldMap.get(li.Id).Product__c || li.Description_and_Functionality__c != Trigger.oldMap.get(li.Id).Description_and_Functionality__c) {

                if(li.Product__c != null && Trigger.oldMap.get(li.Id).Product__c == null) {
                    Product2 proUpdate = searchNameProducts.get(li.product__c);

                    if(proUpdate.Product_Name_Alias__c != null && !proUpdate.Product_Name_Alias__c.contains(li.Search_Name_Text__c)) {
                        proUpdate.Product_Name_Alias__c += ','+li.Search_Name_Text__c;
                        productsUpdate.add(proUpdate);
                    }else if(proUpdate.Product_Name_Alias__c == null) {
                        proUpdate.Product_Name_Alias__c = li.Search_Name_Text__c;
                        productsUpdate.add(proUpdate);
                    }
                }

                if(allSoParts.containsKey(li.Name) && allSoParts.get(li.Name) != null) {
                    AggregateResult a = allSoParts.get(li.Name);
                    li.All_SOs_Average_Unit_Price__c = (Decimal) a.get('average');
                    li.All_SOs_Max_Unit_Price__c = (Decimal) a.get('maximum');
                    li.All_SOs_Min_Unit_Price__c = (Decimal) a.get('minimum');
                    li.All_SOs_Times_Shipped__c = (Decimal) a.get('numberParts');
                    li.All_SOs_Last_Shipment_Date__c = (Date) a.get('maxPOD');
                    li.All_SOs_Most_Shipped_HS__c = (String) a.get('maxHSCode');
                    li.All_SOs_Most_COO__c = (String) a.get('maxCOO');
                }

                if(allDestinationParts.containsKey(li.Name + li.Ship_To_Country__c) && allDestinationParts.get(li.Name + li.Ship_To_Country__c) != null) {
                    AggregateResult a = allDestinationParts.get(li.Name + li.Ship_To_Country__c);
                    li.Destination_SOs_Times_Shipped__c = (Decimal) a.get('numberParts');
                    li.Destination_SOs_Average_Unit_Price__c = (Decimal) a.get('average');
                    li.Destination_SOs_Max_Unit_Price__c = (Decimal) a.get('maximum');
                    li.Destination_SOs_Min_Unit_Price__c = (Decimal) a.get('minimum');
                    li.Destination_SOs_Last_Shipment_Date__c = (Date) a.get('maxPOD');
                    li.Destination_SOs_Most_Shipped_HS__c = (String) a.get('maxHSCode');
                }

                if(allClientParts.containsKey(li.Name + li.AccountId__c) && allClientParts.get(li.Name + li.AccountId__c) != null) {
                    AggregateResult a = allClientParts.get(li.Name + li.AccountId__c);
                    li.Client_SOs_Times_Shipped__c = (Decimal) a.get('numberParts');
                    li.Client_SOs_Average_Unit_Price__c = (Decimal) a.get('average');
                    li.Client_SOs_Max_Unit_Price__c = (Decimal) a.get('maximum');
                    li.Client_SOs_Min_Unit_Price__c = (Decimal) a.get('minimum');
                    li.Client_SOs_Last_Shipment_Date__c = (Date) a.get('maxPOD');
                }

                if(li.Name != Trigger.oldMap.get(li.Id).Name ) {
                    //V0 Start
                    if(catalogueProds.containsKey(li.Search_Name_Text__c + li.AccountId__c)) {
                        System.debug('V0 Matched');
                        v0Matched = true;
                        Part__c catPart = catalogueProds.get(li.Search_Name_Text__c + li.AccountId__c);
                        li.Catalogue_Part__c = catPart.Id;
                        li.Product__c = catPart.Product__c;
                        li.Possible_Countries_of_Origin__c = catPart.Possible_Countries_of_Origin__c;
                        // li.Country_of_Origin2__c = catPart.Country_of_Origin2__c;
                        li.COO_2_digit__c = catPart.COO_2_digit__c;
                        li.US_HTS_Code__c = catPart.US_HTS_Code__c;
                        li.ECCN_NO__c = catPart.ECCN_NO__c;
                        li.Li_ion_Batteries__c = catPart.Li_ion_Batteries__c;
                        li.Lithium_Battery_Types__c = catPart.Lithium_Battery_Types__c;

                        li.Encryption__c = searchNameProducts.get(li.product__c).Encryption__c;
                        li.Manufacturer_Name__c = searchNameProducts.get(li.product__c).Manufacturer_Text_Formula__c;
                        li.Wireless_Capability__c = searchNameProducts.get(li.product__c).Wireless_capability__c;
                        li.Sub_Category__c = searchNameProducts.get(li.product__c).Sub_Category__c;
                        li.Lithium_Batteries_product__c = searchNameProducts.get(li.product__c).Li_on_batteries__c;

                        li.Part_Matching_Method__c = 'Auto Matched';
                        li.Part_Linked_to_Product_Date__c = date.today();
                        hsCode4.add(li.US_HTS_Code__c.left(4)+'%');
                    }
                    /* else if(matchClientParts.get(li.Search_Name__c) != null) {
                        li.Product__c = matchClientParts.get(li.Search_Name__c);
                        li.Part_Matching_Method__c = 'Auto Matched';
                        li.Manufacturer_Name__c = searchNameProducts.get(matchClientParts.get(li.Search_Name__c)).Manufacturer_Text_Formula__c;
                        li.Part_Linked_to_Product_Date__c = date.today();
                       }
                       else if(matchAllParts.get(li.Search_Name__c) != null) {
                        li.Product__c = matchAllParts.get(li.Search_Name__c);
                        li.Part_Matching_Method__c = 'Auto Matched';
                        li.Manufacturer_Name__c = searchNameProducts.get(matchAllParts.get(li.Search_Name__c)).Manufacturer_Text_Formula__c;
                        li.Part_Linked_to_Product_Date__c = date.today();
                       }
                       else */
                    if(searchProducts.get(li.Search_Name_Text__c) != null) {
                        li.Product__c = searchProducts.get(li.Search_Name_Text__c);
                        li.Part_Matching_Method__c = 'Auto Matched';
                        li.Manufacturer_Name__c = searchNameProducts.get(searchProducts.get(li.Search_Name_Text__c)).Manufacturer_Text_Formula__c;
                        li.Part_Linked_to_Product_Date__c = date.today();
                    }
                    else if(aliasList.get(li.Search_Name_Text__c) != null) {
                        li.Product__c = aliasList.get(li.Search_Name_Text__c);
                        li.Part_Matching_Method__c = 'Auto Matched';
                        li.Manufacturer_Name__c = searchNameProducts.get(aliasList.get(li.Search_Name_Text__c)).Manufacturer_Text_Formula__c;
                        li.Part_Linked_to_Product_Date__c = date.today();
                    }
                    else {
                        li.New_Product__c = true;
                    }
                    if(!v0Matched) li.Catalogue_Part__c = null;
                }
                else if( li.Description_and_Functionality__c != Trigger.oldMap.get(li.Id).Description_and_Functionality__c && li.Shipment_Order__c != null && li.RecordTypeId != PartMatchingV3.zeeRecordType) {


                    li.Start_V2_Product_Match__c = true;

                    List<String> Finalmatch= new List<String>();
                    String s = li.Name+' '+li.Description_and_Functionality__c;
                    String S1 = s.toUpperCase();

                    for(Integer i=0; PMR.size()>i; i++) {

                        Boolean Allcontainsmatched = false;
                        Boolean Doesnotcontainsmatched = false;

                        List<String> ContainsList = new List<String>();
                        List<String> DoesnotContainsList = new List<String>();

                        List<String> ConSplittedbyComma = new List<String>();
                        List<String> DoesNotConSplittedbyComma = new List<String>();

                        // All words must contain
                        String Con = PMR[i].Contains__c;
                        String Con1 = Con.toUpperCase();
                        ConSplittedbyComma = Con1.split(',');

                        if(PMR[i].Does_Not_Contains__c != null  || !String.isBlank(PMR[i].Does_Not_Contains__c) ) {
                            String notCon = PMR[i].Does_Not_Contains__c;
                            String notCon1 = notCon.toUpperCase();
                            DoesNotConSplittedbyComma = notCon1.split(',');
                        }
                        for (String Constr : ConSplittedbyComma) {
                            if(S1.contains(Constr)) {
                                ContainsList.add(Constr);
                            }
                            for (String NotConstr : DoesNotConSplittedbyComma) {
                                if(S1.contains(NotConstr)) {
                                    DoesnotContainsList.add(NotConstr);
                                }
                            }
                        }

                        if((ConSplittedbyComma.size() == ContainsList.size()) && ContainsList.size() > 0) {
                            Allcontainsmatched = true;
                        }
                        if( DoesnotContainsList.size() > 0) {
                            Doesnotcontainsmatched = false;
                        } else {
                            Doesnotcontainsmatched = true;//PMR[i].Does_Not_Contains__c == null ;
                        }

                        if(( Allcontainsmatched && Doesnotcontainsmatched && PMR[i].Does_Not_Contains__c != null )) {
                            li.Start_V2_Product_Match__c = true;
                            li.product__c = PMR[i].Product_to_be_match__c;
                            li.Part_Matching_Method__c = 'Auto Matched';
                            li.Matched_Rule__c =  PMR[i].Name;
                            break;
                        }
                        else if( Allcontainsmatched && Doesnotcontainsmatched && PMR[i].Does_Not_Contains__c == null ) {
                            li.Start_V2_Product_Match__c = true;
                            li.product__c = PMR[i].Product_to_be_match__c;
                            li.Part_Matching_Method__c = 'Auto Matched';
                            li.New_Product__c = true;
                            li.Matched_Rule__c =  PMR[i].Name;
                            break;
                        }
                    }
                }
                if(li.Product__c != null && !v0Matched) {
                    li.Catalogue_Part__c = null;
                    li.All_Matching_Complete__c = true;
                    li.Possible_Countries_of_Origin__c = searchNameProducts.get(li.Product__c).Country_of_Origin2__c;
                    //li.Country_of_Origin2__c = searchNameProducts.get(li.Product__c).Suggested_Country_of_Origin__c;
                    li.COO_2_digit__c = searchNameProducts.get(li.Product__c).Country_Two_Digit__c;
                    li.US_HTS_Code__c = searchNameProducts.get(li.Product__c).US_HTS_Code__c;
                    li.ECCN_NO__c = searchNameProducts.get(li.Product__c).ECCN__c;
                    li.Encryption__c = searchNameProducts.get(li.Product__c).Encryption__c;
                    li.Wireless_Capability__c = searchNameProducts.get(li.Product__c).Wireless_capability__c;
                    li.Sub_Category__c = searchNameProducts.get(li.Product__c).Sub_Category__c;
                    li.Manufacturer_Name__c = searchNameProducts.get(li.Product__c).Manufacturer_Text_Formula__c;
                    if(searchNameProducts.get(li.Product__c).Li_on_batteries__c != null) {
                        li.Lithium_Batteries_product__c = searchNameProducts.get(li.Product__c).Li_on_batteries__c;
                        li.Li_ion_Batteries__c = searchNameProducts.get(li.Product__c).Li_on_batteries__c;
                        li.Lithium_Battery_Types__c = searchNameProducts.get(li.Product__c).Li_ion_Battery_Types__c;
                    }
                    li.Part_Linked_to_Product_Date__c = date.today();
                }
                else{
                    //li.Country_of_Origin2__c = li.Country_of_Origin__c;
                    CountryofOriginMap__c countryOfOrigin = CountryofOriginMap__c.getInstance(li.Country_of_Origin__c);
                    if(countryOfOrigin != null && countryOfOrigin.Two_Digit_Country_Code__c != null) {
                        li.COO_2_digit__c = countryOfOrigin.Two_Digit_Country_Code__c;
                    }
                }
            }
            if(String.isBlank(li.US_HTS_Code__c) && String.isNotBlank(li.Client_Provided_Category__c) && categoryUS_HTS_Code.containsKey(li.Client_Provided_Category__c)) {
                li.US_HTS_Code__c = categoryUS_HTS_Code.get(li.Client_Provided_Category__c);
            }
            if(String.isNotBlank(li.US_HTS_Code__c) && (li.US_HTS_Code__c != Trigger.oldMap.get(li.Id).US_HTS_Code__c || li.CPA_Changed__c))
                hsCode4.add(li.US_HTS_Code__c.left(4)+'%');
            li.Simple_Word__c = li.product__c == null ? null : searchNameProducts.get(li.product__c).Extended_description__c;
            if(li.COO_2_digit__c != null) {
                CountryofOriginMap__c countryOfOrigin = CountryofOriginMap__c.getInstance(li.COO_2_digit__c);
                if(countryOfOrigin != null && countryOfOrigin.Country__c != null) {
                    li.Country_of_Origin2__c = countryOfOrigin.Country__c;
                }
            }
            else if(li.Country_of_Origin__c != null) {
                CountryofOriginMap__c countryOfOrigin = CountryofOriginMap__c.getInstance(li.Country_of_Origin__c);
                if(countryOfOrigin != null && countryOfOrigin.Two_Digit_Country_Code__c != null) {
                    li.COO_2_digit__c = countryOfOrigin.Two_Digit_Country_Code__c;
                }
                if(countryOfOrigin != null && countryOfOrigin.Country__c != null) {
                    li.Country_of_Origin2__c = countryOfOrigin.Country__c;
                }
            }
        }
        if(!productsUpdate.isEmpty()) update productsUpdate;

        Map<Id, HS_Codes_and_Associated_Details__c> usHsCodes = new Map<Id, HS_Codes_and_Associated_Details__c>();
        Map<Id, HS_Codes_and_Associated_Details__c> destinationHsCodes = new Map<Id, HS_Codes_and_Associated_Details__c>();

        if(!hsCode4.isEmpty() && !clearanceDestination.isEmpty()) {
            usHsCodes.putAll([SELECT Id, Name, Additional_Part_Specific_Tax_Rate_1__c,status__c, Additional_Part_Specific_Tax_Rate_2__c,Additional_Part_Specific_Tax_Rate_3__c, Country_Name__c, Description__c, Destination_HS_Code__c, Rate__c,Specific_VAT_Rate__c, US_HTS_Code__c, Vat_Exempted__c FROM HS_Codes_and_Associated_Details__c WHERE US_HTS_Code__c Like: hsCode4 AND Country_Name__c IN: clearanceDestination AND Inactive__c = false Order by Rate__c desc LIMIT 50000]);

            destinationHsCodes.putAll([SELECT Id, Name, Additional_Part_Specific_Tax_Rate_1__c,status__c, Additional_Part_Specific_Tax_Rate_2__c, Additional_Part_Specific_Tax_Rate_3__c, Country_Name__c, Description__c, Destination_HS_Code__c, Rate__c,Specific_VAT_Rate__c, US_HTS_Code__c, Vat_Exempted__c FROM HS_Codes_and_Associated_Details__c WHERE Destination_HS_Code__c Like: hsCode4 AND Country_Name__c =: clearanceDestination AND Inactive__c = FALSE Order by Rate__c desc limit 50000]);
        }

        Map<String, Id> allUSCodes = new Map<String, Id>();

        for (Id getAllCodes: usHsCodes.keySet()) {
            String usCode = usHsCodes.get(getAllCodes).Country_Name__c + usHsCodes.get(getAllCodes).US_HTS_Code__c;

            if(!allUSCodes.containsKey(usCode)) {
                allUSCodes.put(usHsCodes.get(getAllCodes).Country_Name__c + usHsCodes.get(getAllCodes).US_HTS_Code__c, getAllCodes);
            }
            else if(usHsCodes.get(getAllCodes).Rate__c > usHsCodes.get(allUSCodes.get(usCode)).Rate__c) {
                allUSCodes.remove(usCode);
                allUSCodes.put(usHsCodes.get(getAllCodes).Country_Name__c + usHsCodes.get(getAllCodes).US_HTS_Code__c, getAllCodes);
            }
        }

        Map<String, Id> allDestinationCodes = new Map<String, Id>();
        Map<String, String> destinationCodeTrunc = new Map<String, String>();

        for (Id getAllDestCodes: destinationHsCodes.keySet()) {
            String destCode = destinationHsCodes.get(getAllDestCodes).Country_Name__c + destinationHsCodes.get(getAllDestCodes).Destination_HS_Code__c;
            if(!allDestinationCodes.containsKey(destCode)) {
                allDestinationCodes.put(destinationHsCodes.get(getAllDestCodes).Country_Name__c + destinationHsCodes.get(getAllDestCodes).Destination_HS_Code__c, getAllDestCodes);
            }
            String InputString = destinationHsCodes.get(getAllDestCodes).Destination_HS_Code__c;
            String revStr = ' ';
            Integer InputStringsize = InputString.length();

            for (Integer i =0; InputString.length()>i; i++) {
                revStr = InputString.substring(0, InputString.length()-i);

                if(!allDestinationCodes.containsKey(destinationHsCodes.get(getAllDestCodes).Country_Name__c + revStr)) {
                    allDestinationCodes.put(destinationHsCodes.get(getAllDestCodes).Country_Name__c + revStr, getAllDestCodes);
                }
                else if(destinationHsCodes.get(getAllDestCodes).Rate__c > destinationHsCodes.get(allDestinationCodes.get(destinationHsCodes.get(getAllDestCodes).Country_Name__c + revStr)).Rate__c) {
                    allDestinationCodes.remove(destinationHsCodes.get(getAllDestCodes).Country_Name__c + revStr);
                    allDestinationCodes.put(destinationHsCodes.get(getAllDestCodes).Country_Name__c + revStr, getAllDestCodes);
                }
                if((InputStringsize-i) == 4) {
                    break;
                }
            }
        }

        for (Part__c li : Trigger.new) {

            if(li.Shipment_Order__c != null && inValidSOSet.contains(li.Shipment_Order__c)) {
                continue;
            }


            if(li.US_HTS_Code__c != Trigger.oldMap.get(li.Id).US_HTS_Code__c || li.CPA_Changed__c) {
                String partDestination = li.Ship_To_Country__c;
                String searchCode = partDestination + li.US_HTS_Code__c;

                String usSearchCode;
                if(li.US_HTS_Code__c !=null)
                    usSearchCode = partDestination + li.US_HTS_Code__c.left(8);

                if(allUSCodes.get(usSearchCode) != null) {
                    li.Matched_HS_Code2__c = allUSCodes.get(usSearchCode);
                    li.Matched_HS_Code_Description__c = usHsCodes.get(allUSCodes.get(usSearchCode)).Description__c;
                    if(usHsCodes.get(allUSCodes.get(usSearchCode)).status__c != NULL) {
                        li.Restricted_status__c = usHsCodes.get(allUSCodes.get(usSearchCode)).status__c;
                    }
                    li.Rate__c = usHsCodes.get(allUSCodes.get(usSearchCode)).Rate__c;
                    li.Part_Specific_Rate_1__c = usHsCodes.get(allUSCodes.get(usSearchCode)).Additional_Part_Specific_Tax_Rate_1__c;
                    li.Part_Specific_Rate_2__c = usHsCodes.get(allUSCodes.get(usSearchCode)).Additional_Part_Specific_Tax_Rate_2__c;
                    li.Part_Specific_Rate_3__c = usHsCodes.get(allUSCodes.get(usSearchCode)).Additional_Part_Specific_Tax_Rate_3__c;
                    li.Vat_Exempted__c = usHsCodes.get(allUSCodes.get(usSearchCode)).Vat_Exempted__c;
                    li.Specific_VAT_Rate__c = usHsCodes.get(allUSCodes.get(usSearchCode)).Specific_VAT_Rate__c;
                    li.Default_Duty_Rate__c = rates.get(partDestination + 'Duties');
                    li.VAT_Rate__c = rates.get(partDestination + 'VAT');
                    li.VAT_Applied_To_Value__c = appliedTo.get(partDestination + 'VAT');
                    li.Applied_To_Value__c = appliedTo.get(partDestination + 'Duties');
                    li.Clearance_Country__c = clearanceCountry.get(partDestination);
                    li.No_HS_Code_Match__c = false;
                }
                else if(allDestinationCodes.get(searchCode) != null) {
                    li.Matched_HS_Code2__c = allDestinationCodes.get(searchCode);
                    li.Matched_HS_Code_Description__c = destinationHsCodes.get(allDestinationCodes.get(searchCode)).Description__c;
                    if(destinationHsCodes.get(allDestinationCodes.get(searchCode)).status__c != NULL) {
                        li.Restricted_status__c = destinationHsCodes.get(allDestinationCodes.get(searchCode)).status__c;
                    }
                    li.Rate__c = destinationHsCodes.get(allDestinationCodes.get(searchCode)).Rate__c;
                    li.Part_Specific_Rate_1__c = destinationHsCodes.get(allDestinationCodes.get(searchCode)).Additional_Part_Specific_Tax_Rate_1__c;
                    li.Part_Specific_Rate_2__c = destinationHsCodes.get(allDestinationCodes.get(searchCode)).Additional_Part_Specific_Tax_Rate_2__c;
                    li.Part_Specific_Rate_3__c = destinationHsCodes.get(allDestinationCodes.get(searchCode)).Additional_Part_Specific_Tax_Rate_3__c;
                    li.Vat_Exempted__c = destinationHsCodes.get(allDestinationCodes.get(searchCode)).Vat_Exempted__c;
                    li.Specific_VAT_Rate__c = destinationHsCodes.get(allDestinationCodes.get(searchCode)).Specific_VAT_Rate__c;
                    li.Default_Duty_Rate__c = rates.get(partDestination + 'Duties');
                    li.VAT_Rate__c = rates.get(partDestination + 'VAT');
                    li.VAT_Applied_To_Value__c = appliedTo.get(partDestination + 'VAT');
                    li.Applied_To_Value__c = appliedTo.get(partDestination + 'Duties');
                    li.Clearance_Country__c = clearanceCountry.get(partDestination);
                    li.No_HS_Code_Match__c = false;
                }
                else {
                    String InputString2 = searchCode;
                    String revStr2 = ' ';
                    Integer InputStringsize2 = InputString2.length();

                    for (Integer i =0; InputString2.length()>i; i++) {
                        revStr2 = InputString2.substring(0, InputString2.length()-i);
                        if(allDestinationCodes.get(revStr2) != null) {
                            li.Matched_HS_Code2__c = allDestinationCodes.get(revStr2);
                            li.Matched_HS_Code_Description__c = destinationHsCodes.get(allDestinationCodes.get(revStr2)).Description__c;
                            if(destinationHsCodes.get(allDestinationCodes.get(revStr2)).status__c != NULL) {
                                li.Restricted_status__c = destinationHsCodes.get(allDestinationCodes.get(revStr2)).status__c;
                            }
                            li.Rate__c = destinationHsCodes.get(allDestinationCodes.get(revStr2)).Rate__c;
                            li.Part_Specific_Rate_1__c = destinationHsCodes.get(allDestinationCodes.get(revStr2)).Additional_Part_Specific_Tax_Rate_1__c;
                            li.Part_Specific_Rate_2__c = destinationHsCodes.get(allDestinationCodes.get(revStr2)).Additional_Part_Specific_Tax_Rate_2__c;
                            li.Part_Specific_Rate_3__c = destinationHsCodes.get(allDestinationCodes.get(revStr2)).Additional_Part_Specific_Tax_Rate_3__c;
                            li.Vat_Exempted__c = destinationHsCodes.get(allDestinationCodes.get(revStr2)).Vat_Exempted__c;
                            li.Specific_VAT_Rate__c = destinationHsCodes.get(allDestinationCodes.get(revStr2)).Specific_VAT_Rate__c;
                            li.Default_Duty_Rate__c = rates.get(partDestination + 'Duties');
                            li.VAT_Rate__c = rates.get(partDestination + 'VAT');
                            li.VAT_Applied_To_Value__c = appliedTo.get(partDestination + 'VAT');
                            li.Applied_To_Value__c = appliedTo.get(partDestination + 'Duties');
                            li.Clearance_Country__c = clearanceCountry.get(partDestination);
                            li.No_HS_Code_Match__c = false;
                            break;
                        }
                        else {
                            li.No_HS_Code_Match__c = true;
                            li.Matched_HS_Code2__c = null;
                            li.Matched_HS_Code_Description__c = null;
                            li.Restricted_status__c = 'None';
                            li.Rate__c = null;
                            li.Default_Duty_Rate__c = rates.get(partDestination + 'Duties');
                            li.VAT_Rate__c = rates.get(partDestination + 'VAT');
                            li.VAT_Applied_To_Value__c = appliedTo.get(partDestination + 'VAT');
                            li.Applied_To_Value__c = appliedTo.get(partDestination + 'Duties');
                            li.Clearance_Country__c = clearanceCountry.get(partDestination);
                        }
                        if((InputStringsize2-i) == 4) {
                            break;
                        }
                    }
                }
                li.Account_Text__c = li.AccountId__c;
            }
            li.CPA_Changed__c = false;
        }
    }

    if((Trigger.isUpdate && Trigger.isAfter)|| (Trigger.isDelete && Trigger.isAfter)) {

        List<String> partNames = new List<String>();
        Map<Id,String> shipmentIdsEccn = new Map<Id,String>();
        List<part__c> parts = Trigger.isUpdate ? Trigger.new : Trigger.old;
        Decimal partValues = 0;
        Set<Id> inValidSOSet = new Set<Id>();

        for(Part__c li : parts) {
            if(li.Shipment_Order__c != null) {
                inValidSOSet.add(li.Shipment_Order__c);
            }
            if(li.US_HTS_Code__c == null) {
                partValues += li.Total_Value__c;
            }
        }

        set<String> invalidSOStatus = new set<String> {'Customs Clearance Docs Received', 'Cancelled - With fees/costs', 'Cancelled - No fees/costs', 'Cost Estimate Abandoned', 'Cost Estimate Rejected'};

        inValidSOSet = new Map<Id, Shipment_Order__c>([SELECT Id FROM Shipment_Order__c WHERE Id IN: inValidSOSet
                                                       AND (Closed_Down_SO__c = true OR Shipping_Status__c IN: invalidSOStatus)]).keySet();




        for (Part__c li : parts) {

            if(li.Shipment_Order__c != null && inValidSOSet.contains(li.Shipment_Order__c)) {
                continue;
            }

            if(Trigger.isDelete ||
               (Trigger.isUpdate && (Trigger.oldMap.get(li.Id).AccountId__c != li.AccountId__c ||
                                     Trigger.oldMap.get(li.Id).Name != li.Name))) {
                accountId.add(li.AccountId__c);
                partNames.add(li.Name);
            }

            if(Trigger.isUpdate) {
                String SOECCNNo = 'NONE';

                if(!(String.isBlank(li.Eccn_No__c)) && li.Eccn_No__c.contains('5A002')) {
                    SOECCNNo='5A002';
                }
                else if(!(String.isBlank(li.Eccn_No__c)) && li.Eccn_No__c.contains('5D002')) {
                    SOECCNNo='5D002';
                }
                if(li.Shipment_Order__c != null)
                    shipmentIdsEccn.put(li.Shipment_Order__c,SOECCNNo);
            }

            if(li.Shipment_Order__c != null) {
                SoIds.add(li.Shipment_Order__c);
                if(Trigger.isUpdate && Trigger.oldMap.get(li.Id).Shipment_Order__c != null && li.Shipment_Order__c != Trigger.oldMap.get(li.Id).Shipment_Order__c)
                    SoIds.add(Trigger.oldMap.get(li.Id).Shipment_Order__c);
            }
        }


        if(SoIds.isEmpty()) return;

        if(Trigger.isDelete) {
            for(Part__c li : [SELECT Id,Shipment_Order__c,Eccn_No__c FROM Part__c WHERE Shipment_Order__c IN: SoIds]) {
                String SOECCNNo = 'NONE';

                if(!(String.isBlank(li.Eccn_No__c)) && li.Eccn_No__c.contains('5A002')) {
                    SOECCNNo='5A002';
                }
                else if(!(String.isBlank(li.Eccn_No__c)) && li.Eccn_No__c.contains('5D002')) {
                    SOECCNNo='5D002';
                }
                if(SOECCNNo != 'NONE') {
                    shipmentIdsEccn.put(li.Shipment_Order__c,SOECCNNo);
                }
            }
        }

        Map<Id,shipment_Order__c> shipmentOrders = new Map<Id,shipment_Order__c>([SELECT Id,Sum_Unmatched_HS_Codes__c, Contains_Batteries_Auto__c, Prohibited_Manufacturer_Auto__c, CPA_v2_0__r.COO_Prohibitions_Text__c, CPA_v2_0__r.Manufacturer_Prohibitions__c,Sum_of_quantity_of_matched_products__c,Manufacturer__c,ECCN_No__c, Number_of_Batteries__c, Number_of_Wireless_Products__c, Restricted_parts__c, Contains_Encrypted_Goods_Auto__c,Contains_Refurbished_Goods__c,(SELECT Id, Li_ion_Batteries__c, Encryption__c, Restricted_status__c,Type_of_Goods__c,Manufacturer_Name__c,Wireless_Capability__c, Shipment_Order__c FROM Parts__r) FROM Shipment_Order__c WHERE Id IN: SoIds]);

        populateFormulaFieldsOnSO(shipmentOrders);

        List<AggregateResult> partsTODelete = [SELECT Shipment_Order__c,SUM(Total_Value__c) total, Count_Distinct(US_HTS_Code__c) hsCodes, Sum(CIF_Duties__c) sumCIF,Count(Id) numberParts, sum(FOB_Duties__c) sumFOB FROM Part__c WHERE Shipment_Order__c IN:SoIds Group By Shipment_Order__c];

        if(partsTODelete.isEmpty()) {//empty means all parts deleted
            for(Id sid : shipmentOrders.keySet()) {
                Shipment_Order__c shipmentOrder = shipmentOrders.get(sid);
                shipmentOrder.Sum_Unmatched_HS_Codes__c  = 0;
                shipmentOrder.of_Line_Items__c = 0;//(Decimal) a.get('numberParts');
                shipmentOrder.of_Unique_Line_Items__c = 0;//(Decimal) a.get('hsCodes');
                shipmentOrder.Total_CIF_Duties__c = 0;//cif > 0 ? cif : 0;
                shipmentOrder.Total_FOB_Duties__c = 0;//fob > 0 ? fob : 0;
                shipmentOrder.Total_Line_Item_Extended_Value__c = 0;//totalValue > 0 ? totalValue : 0;
                shipmentOrder.ECCN_No__c = 'NONE';
                // shipmentOrder.Shipment_Value_USD__c = 0;//totalValue > 0 ? totalValue : shipmentOrder.Shipment_Value_USD__c;
            }
        }else{
            for (AggregateResult a : partsTODelete) {
                Decimal cif = (Decimal) a.get('sumCIF');
                Decimal fob = (Decimal) a.get('sumFOB');
                Decimal totalValue = (Decimal) a.get('total');

                Shipment_Order__c shipmentOrder = shipmentOrders.get((String)a.get('Shipment_Order__c'));
                shipmentOrder.Sum_Unmatched_HS_Codes__c = partValues;
                shipmentOrder.of_Line_Items__c = (Decimal) a.get('numberParts');
                shipmentOrder.of_Unique_Line_Items__c = (Decimal) a.get('hsCodes');
                shipmentOrder.Total_CIF_Duties__c = cif > 0 ? cif : 0;
                shipmentOrder.Total_FOB_Duties__c = fob > 0 ? fob : 0;
                shipmentOrder.Total_Line_Item_Extended_Value__c = totalValue > 0 ? totalValue : 0;
                shipmentOrder.Shipment_Value_USD__c = totalValue > 0 ? totalValue : shipmentOrder.Shipment_Value_USD__c;
                shipmentOrder.Recalculate_Taxes__c = true;
                if( !shipmentIdsEccn.isEmpty() && shipmentIdsEccn.ContainsKey(shipmentOrder.Id)) {
                    shipmentOrder.ECCN_No__c = shipmentIdsEccn.get(shipmentOrder.Id);
                }else{
                    shipmentOrder.ECCN_No__c = 'NONE';
                }
            }
        }

        if(RecusrsionHandler.partUpdateFromSO) {
            ShipmentOrderTriggerHandler.soMapFromPartTrigger = shipmentOrders;
        }else{
            update shipmentOrders.values();
        }

        //Number_of_times_shipped__c updation on product catalogue logic

        List<Product_Catalogue__c> updateCatalogues = new List<Product_Catalogue__c>();
        Map<String,Decimal> catalogueResults = new Map<String,Decimal>();
        for(AggregateResult a :  [SELECT Name, count(Shipment_Order__c) total, Shipment_Order__r.Account__c client FROM Part__c Where Name IN:partNames AND Shipment_Order__r.Account__c IN: accountId GROUP BY Name, Shipment_Order__r.Account__c]) {

            String key = String.valueOf(a.get('Name'))+String.valueOf(a.get('client'));
            catalogueResults.put(key,(Decimal) a.get('total'));
        }

        for(Product_Catalogue__c a : [SELECT Id,Account__c,Number_of_times_shipped__c,Part__r.Name FROM Product_Catalogue__c WHERE Account__c =: accountId AND Part__r.Name IN:partNames]) {
            String key = a.Part__r.Name + a.Account__c;
            if(catalogueResults.containsKey(key)) {
                a.Number_of_times_shipped__c = catalogueResults.get(key);
                updateCatalogues.add(a);
            }
        }

        if(!updateCatalogues.isEmpty()) update updateCatalogues;
    }

    if(Trigger.isDelete && Trigger.isBefore) {
        Delete [SELECT Id FROM Task WHERE Part__c IN:Trigger.oldMap.keySet()];
    }

    public static void populateFormulaFieldsOnSO(Map<Id,shipment_order__c> shipmentOrders){

        for(Id soId : shipmentOrders.keySet()) {
            shipment_Order__c so = shipmentOrders.get(soId);
            if(so.Parts__r != null) {
                // Part record found. below code will perform logics.
                String containsBatteries = '';
                String containsRefurbished = '';
                String containsEncryptedGoods = '';
                String containsWirelessGoods = '';
                String prohibitedManufacturer = '';
                Integer numberOfBatteries = 0;
                Integer numberOfWirelessCapability = 0;
                Integer restrictedStatusBlocked = 0;
                Integer restrictedStatusBlacklist = 0;

                for(Part__c part : so.Parts__r) {
                    //Contains Batteries - Auto | Start
                    if(part.Li_ion_Batteries__c == 'Yes') {
                        containsBatteries = 'Yes';
                    }else if( (part.Li_ion_Batteries__c == '' || part.Li_ion_Batteries__c == NULL) && containsBatteries != 'Yes') {
                        containsBatteries = 'Info Unavailable';
                    }else{
                        if(String.isBlank(containsBatteries)) {
                            containsBatteries = 'No';
                        }
                    }
                    //Contains Batteries - Auto | End

                    //Contains Refurbished Goods | Start
                    if(part.Type_of_Goods__c == 'Refurbished') {
                        containsRefurbished = 'Yes';
                    }else if((part.Type_of_Goods__c == 'Info Unavailable' || part.Type_of_Goods__c == '' || part.Type_of_Goods__c == NULL) && containsRefurbished != 'Yes') {
                        containsRefurbished = 'Info Unavailable';
                    }else{
                        if(String.isBlank(containsRefurbished)) {
                            containsRefurbished = 'No';
                        }
                    }
                    //Contains Refurbished Goods | End


                    //Contains Encrypted Goods - Auto | Start
                    if(part.Encryption__c == 'Yes') {
                        containsEncryptedGoods = 'Yes';
                    }else if((part.Encryption__c == 'Info Unavailable' || part.Encryption__c == '' || part.Encryption__c == NULL) && containsEncryptedGoods != 'Yes') {
                        containsEncryptedGoods = 'Info Unavailable';
                    }else{
                        if(String.isBlank(containsEncryptedGoods)) {
                            containsEncryptedGoods = 'No';
                        }
                    }
                    //Contains Encrypted Goods - Auto | End

                    //Contains Wireless Goods - Auto | Start
                    if(part.Wireless_Capability__c == 'Yes') {
                        containsWirelessGoods = 'Yes';
                    }else if((part.Wireless_Capability__c == 'Info Unavailable' || part.Wireless_Capability__c == '' || part.Wireless_Capability__c == NULL) && containsWirelessGoods != 'Yes') {
                        containsWirelessGoods = 'Info Unavailable';
                    }else{
                        if(String.isBlank(containsWirelessGoods)) {
                            containsWirelessGoods = 'No';
                        }
                    }
                    //Contains Wireless Goods - Auto | End

                    //Prohibited Manufacturer - Auto - Start
                    if(so.CPA_v2_0__r.Manufacturer_Prohibitions__c != NULL &&
                       part.Manufacturer_Name__c != NULL &&
                       so.CPA_v2_0__r.Manufacturer_Prohibitions__c.contains(part.Manufacturer_Name__c)) {
                        prohibitedManufacturer = 'Yes';
                    }else if((part.Manufacturer_Name__c == NULL || part.Manufacturer_Name__c == '') && prohibitedManufacturer != 'Yes') {
                        prohibitedManufacturer = 'Info Unavailable';
                    }else{
                        if(String.isBlank(prohibitedManufacturer)) {
                            prohibitedManufacturer = 'No';
                        }
                    }
                    //Prohibited Manufacturer - Auto - END

                    //Number of Batteries - Start
                    if(part.Li_ion_Batteries__c == 'Yes') {
                        numberOfBatteries++;
                    }
                    //Number of Batteries - End

                    //Number of Wireless Products - Start
                    if(part.Wireless_Capability__c == 'Yes') {
                        numberOfWirelessCapability++;
                    }
                    //Number of Wireless Products - End

                    //Restricted parts - Start
                    if(part.Restricted_status__c == 'Blocked') {
                        restrictedStatusBlocked++;
                    }
                    if(part.Restricted_status__c == 'Blacklist (Requires Compliance)') {
                        restrictedStatusBlacklist++;
                    }
                    //Restricted parts - End

                }

                //Updating final information on SO
                so.Contains_Batteries_Auto__c = containsBatteries;
                so.Prohibited_Manufacturer_Auto__c = prohibitedManufacturer;
                so.Number_of_Batteries__c = numberOfBatteries;
                so.Number_of_Wireless_Products__c = numberOfWirelessCapability;
                so.Contains_Encrypted_Goods_Auto__c = containsEncryptedGoods;
                so.Contains_Wireless_Goods_Auto__c = containsWirelessGoods;
                so.Contains_Refurbished_Goods__c = containsRefurbished;

                if(restrictedStatusBlocked > 0) {
                    so.Restricted_parts__c = 'Blocked';
                }else if(restrictedStatusBlacklist > 0) {
                    so.Restricted_parts__c = 'Blacklisted';
                }else{
                    so.Restricted_parts__c = 'None';
                }

            }else{
                //Reset The formula field to default. As no part record found
                so.Contains_Batteries_Auto__c = 'Info Unavailable';
                so.Contains_Refurbished_Goods__c = 'Info Unavailable';
                so.Prohibited_Manufacturer_Auto__c = 'Info Unavailable';
                so.Number_of_Batteries__c = 0;
                so.Number_of_Wireless_Products__c = 0;
                so.Restricted_parts__c = 'None';
                so.Contains_Encrypted_Goods_Auto__c = 'Info Unavailable';
                so.Contains_Wireless_Goods_Auto__c = 'Info Unavailable';
            }
        }
    }

    private static string getSearchName(String name){
        name = name.toUpperCase().trim();
        List<String> strippedChars = new List<String> {'−','+','_','‚','"','%','：','|','*','}',']','™','/','\\','<','&','Â','€','Ž','¦',' ',';','”','$','‐','-','‑','·',':','模','块','–','’','^','(','（','）','®','{','?','分','万','兆','电','口','双','>',',','È','`',')','“','°','~','Í','[','Ü','•','¬','Μ','±','É','—','С','Λ','英','业','达','́','路','由','器','交','换','机','服','务','单','光','米','线','尾','纤','转','.'};

        if(name.startsWith('EC-')) return name;
        name = name.deleteWhitespace();

        for(String strippedChar : strippedChars) name = name.replace(strippedChar,'');

        if(name.endsWith('OSI')) name = name.replace('OSI','');
        else if(name.endsWith('HIL')) name = name.replace('HIL','');
        if(name.indexOf('=') != -1) name = name.left(name.indexOf('='));

        if(name.indexOf('#AB') != -1) name = name.left(name.indexOf('#AB'));
        if(name.indexOf('#BG') != -1) name = name.left(name.indexOf('#BG'));

        return name;
    }
}