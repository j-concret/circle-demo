trigger attachCostestimate_V2 on Shipment_Order__c (before Insert, after Insert,before update,after update) {
   
    IF(Trigger.Isbefore && Trigger.IsInsert){
         System.debug('Inside before insert trigger--->');
             for(Shipment_Order__C SO : Trigger.new){
                 If(So.Destination__c != Null && So.Shipping_Status__c == 'Roll-Out'){
                 RolloutHandler.isBeforeInsert();
             }
             }
    }
    IF(Trigger.IsAfter && Trigger.IsInsert){
        for(Shipment_Order__C SO : Trigger.new){
            If(So.Shipping_Status__c == 'Roll-Out'){
                
                RolloutHandler.Updatefromlookups();
            }
          
        }
    }
     
 IF(Trigger.IsBefore && Trigger.IsUpdate){
    // Add all Forecast amounts from costings to Shipment order after costs created    
     List<CPA_Costing__c> CPACostings1 = new List<CPA_Costing__c>();
     decimal FCAdminFee = 0.0;
     decimal FCBankFee = 0.0;
     decimal FCFinanceFee = 0.0;
     decimal FCIORandImport = 0.0;
     decimal FCINsurance = 0.0;
     decimal FCInternationsldeliveryFee = 0.0;
     decimal FCMiselaneousFee = 0.0;
     decimal FCRechatgeTaxandDuties = 0.0;
     decimal FCTotClearence = 0.0;
     decimal FCTotCustomsBrokerage = 0.0;
     decimal FCTotHandlingCost = 0.0;
     decimal FCTotLicenseCost = 0.0;
            decimal FCTAXOther = 0.0;
            decimal FCTAXForex = 0.0;
     
           
        //Get all costings records
         for(Shipment_Order__C SO : Trigger.new){
             If( (SO.CPA_Costings_Added__c != trigger.oldMap.get(SO.ID).CPA_Costings_Added__c  ) && SO.CPA_Costings_Added__c == TRUE && SO.FC_Total__c == 0 ) {
                 List<CPA_Costing__c> CPACostings = [Select Cost_Category__C,Amount_in_USD__c from CPA_Costing__c where Shipment_Order_Old__c =: SO.id];
                 For(Integer i=0;CPACostings.size()>i;i++ ){
                 CPACostings1.add(CPACostings[i]);
                          if(CPACostings[i].Cost_Category__C=='Admin')      { FCAdminFee = FCAdminFee+CPACostings[i].Amount_in_USD__c;}
                     else if(CPACostings[i].Cost_Category__C=='Bank')       { FCBankFee = FCBankFee+CPACostings[i].Amount_in_USD__c;}
                     else if(CPACostings[i].Cost_Category__C=='Finance')    { FCFinanceFee = FCFinanceFee+CPACostings[i].Amount_in_USD__c;}
                     else if(CPACostings[i].Cost_Category__C=='IOR Cost')       { FCIORandImport = FCIORandImport+CPACostings[i].Amount_in_USD__c;}
                     else if(CPACostings[i].Cost_Category__C=='Insurance')  { FCINsurance = FCINsurance+CPACostings[i].Amount_in_USD__c;}
                     else if(CPACostings[i].Cost_Category__C=='International freight')      { FCInternationsldeliveryFee = FCInternationsldeliveryFee+CPACostings[i].Amount_in_USD__c;}
                     else if(CPACostings[i].Cost_Category__C=='Miscellaneous')      { FCMiselaneousFee = FCMiselaneousFee+CPACostings[i].Amount_in_USD__c;}
                     else if(CPACostings[i].Cost_Category__C=='Tax')        { FCRechatgeTaxandDuties = FCRechatgeTaxandDuties+CPACostings[i].Amount_in_USD__c;  }
                     else if(CPACostings[i].Cost_Category__C=='Clearance')      { FCTotClearence = FCTotClearence+CPACostings[i].Amount_in_USD__c;}
                     else if(CPACostings[i].Cost_Category__C=='Brokerage')      { FCTotCustomsBrokerage = FCTotCustomsBrokerage+CPACostings[i].Amount_in_USD__c;}
                     else if(CPACostings[i].Cost_Category__C=='Handling')       { FCTotHandlingCost = FCTotHandlingCost+CPACostings[i].Amount_in_USD__c;}
                     else if(CPACostings[i].Cost_Category__C=='Licenses')       { FCTotLicenseCost = FCTotLicenseCost+CPACostings[i].Amount_in_USD__c;}
                     
                     //   if(CPACostings[i].Cost_Category__C=='Tax' && (CPACostings[i].Tax_Sub_Category__c !='Duties and Taxes Other' || CPACostings[i].Tax_Sub_Category__c !='Tax Forex Spread'))      {  FCRechatgeTaxandDuties = FCRechatgeTaxandDuties+CPACostings[i].Amount_in_USD__c;}
                     //   if(CPACostings[i].Cost_Category__C=='Tax' && CPACostings[i].Tax_Sub_Category__c=='Duties and Taxes Other' )       { FCTAXOther = FCTAXOther+CPACostings[i].Amount_in_USD__c;  }                
                     //   if(CPACostings[i].Cost_Category__C=='Tax' && CPACostings[i].Tax_Sub_Category__c=='Tax Forex Spread' )     { FCTAXForex = FCTAXForex+CPACostings[i].Amount_in_USD__c;  }
                          
                }
            }
            }
      
         System.debug('CPACostings1 Size -->' +CPACostings1.size());  
     if(CPACostings1.size()>0){
       For(Shipment_Order__C SO : Trigger.new){
           
           If( (SO.CPA_Costings_Added__c != trigger.oldMap.get(SO.ID).CPA_Costings_Added__c  ) && SO.CPA_Costings_Added__c == TRUE && SO.FC_Total__c == 0 ) {
           SO.FC_Admin_Fee__c=FCAdminFee;
           SO.FC_Bank_Fees__c=FCBankFee;
           SO.FC_Finance_Fee__c=FCFinanceFee;
           SO.FC_IOR_and_Import_Compliance_Fee_USD__c=FCIORandImport;
           SO.FC_Insurance_Fee_USD__c=FCINsurance;
           SO.FC_International_Delivery_Fee__c=FCInternationsldeliveryFee;
           SO.FC_Miscellaneous_Fee__c=FCMiselaneousFee;
           SO.FC_Recharge_Tax_and_Duty__c=FCRechatgeTaxandDuties;
           SO.FC_Total_Clearance_Costs__c=FCTotClearence;
           SO.FC_Total_Customs_Brokerage_Cost__c=FCTotCustomsBrokerage;
           SO.FC_Total_Handling_Costs__c=FCTotHandlingCost;
           SO.FC_Total_License_Cost__c=FCTotLicenseCost;
           }
        }
     } 
     
     
     
     
    
}
    
   
    If(Trigger.IsAfter && Trigger.IsUpdate){
        
    /* $$$$$$$$$$$$$$$ beginning of code Commented out by Perfect $$$$$$$$$$$$$$
    // For Attach cost estimate
        
    Shipment_Order__c[] P1 = Trigger.new;
    List<ID> soid = new list<ID>();
    system.debug('--Debug--'); 
  //  system.debug(P1); 
    system.debug(P1[0].id); 
    soid.add(P1[0].id);
    
    system.debug(soid);
    
    if(P1[0].Cost_Estimate_Accepted__c == true && P1[0].Cost_Estimate_attached__c == False && attachCostEstimatehelperTOtrigger.firstRun ){
        try{ 
        attachCostEstimatehelperTOtrigger.firstRun = false;
        attachCostEstimatehelperTOtrigger.sendEmailWithAttachment(soid);
       
        system.debug('Inside if');
            
       
        }
        catch(exception e){}
        
    } $$$$$$$$$$$$$$$$$ End of code Commented out by Perfect $$$$$$$$$$$$$$$$$$ */

    
    //############### Begining of code added by Perfect Mathenjwa ##############################

    for(shipment_order__c so : Trigger.new){
        
        List<Id> soid = new List<Id>{so.Id};
            
       
        
        
        if(so.Cost_Estimate_Accepted__c == true && so.Cost_Estimate_attached__c == false && attachCostEstimatehelperTOtrigger.firstRun ){
            try{ 
                attachCostEstimatehelperTOtrigger.firstRun = false;
                attachCostEstimatehelperTOtrigger.sendEmailWithAttachment(soid);
                system.debug('Inside if');
            }
            catch(exception e){}
        }

        if(so.Shipping_Status__c == 'Cost Estimate'  && so.Taxes_calculated__c == true && so.Request_Email_Estimate__c == true && so.Email_Estimate_To__c!=null && so.Cost_Estimate_attached__c == false && attachCostEstimatehelperTOtrigger.firstRun){
                
            try{ 
                attachCostEstimatehelperTOtrigger.firstRun = false;
                attachCostEstimatehelperTOtrigger.sendEmailWithAttachment2(soid);
                
            }
            catch(exception e){}

        }
        
            //############### End of code added by Perfect Mathenjwa ###################################
            
      
    

    }

        

}
    
}

/*
 * trigger attachCostestimate on Shipment_Order__c (after update) {
    
     Shipment_Order__c[] P1 = Trigger.new;
    List<ID> soid = new list<ID>();
    system.debug('--Debug--'); 
    system.debug(P1); 
    system.debug(P1[0].id); 
    soid.add(P1[0].id);
    
    system.debug(soid);
    
    if(P1[0].Cost_Estimate_Accepted__c == TRUE && P1[0].Cost_Estimate_attached__c == False ){
        try{ 
        attachCostEstimatehelperTOtrigger.sendEmailWithAttachment(soid);
       
        system.debug('Inside if');
            
       
        }
        catch(exception e){}
        
    }
     

}
 * 
*/