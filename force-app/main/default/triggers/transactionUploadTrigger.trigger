trigger transactionUploadTrigger on Attachment (after insert) {
    
String Title;
    
Id pId;
    
Attachment theFile;

String AttId;
    
    

for(Attachment att: Trigger.new){
        
      Title=att.Name;
        
      pId=att.ParentId;
        
      theFile = att;
    
}
    
    


System.debug('Got name: ' + Title);
    
System.debug('Got ID: ' + pID);
        
    


List<Attachment__c> u=[select Id, Attachment_Type__c, Initial_HTTP_Response__c, Shipment_Order__c,  Shipment_Order__r.Ship_to_Country__r.Shipping_Terms__c,  
Shipment_Order__r.Ship_From_Country__c,  Shipment_Order__r.Pick_Up_Address__c,  Shipment_Order__r.Shipment_Value_USD__c,  Shipment_Order__r.Final_Delivery_Address__c,  
Shipment_Order__r.Id,  Shipment_Order__r.IOR_Price_List__r.Destination__c from Attachment__c where Id=:pId];
    
  If(u.size() == 1){  

//assuming one record is fetched.
    
u[0].Attachment_Type__c =Title;
    
    
update u[0];

// Create XML file

 Xmlstreamwriter xmlW = new Xmlstreamwriter();
            xmlW.writeStartDocument('utf-8','1.0');
            xmlW.writeStartElement(null,'BatchMetadata', null);



                    xmlW.writeStartElement(null,'Field',null);
                    xmlW.writeStartElement(null,'Name',null);
                    xmlW.writeCharacters('SF_Shipment_Value');
                    xmlW.writeEndElement(); //close field
                    xmlW.writeStartElement(null,'Value',null);
                    xmlW.writeCharacters(String.valueOf(u[0].Shipment_Order__r.Shipment_Value_USD__c));
                    xmlW.writeEndElement(); //Close value
                    xmlW.writeEndElement(); //Close Field

                    

                    xmlW.writeStartElement(null,'Field',null);
                    xmlW.writeStartElement(null,'Name',null);
                    xmlW.writeCharacters('ParentID');
                    xmlW.writeEndElement(); //close field
                    xmlW.writeStartElement(null,'Value',null);
                    xmlW.writeCharacters(u[0].Shipment_Order__r.Id);
                    xmlW.writeEndElement(); //Close value
                    xmlW.writeEndElement(); //Close Field

                    xmlW.writeStartElement(null,'Field',null);
                    xmlW.writeStartElement(null,'Name',null);
                    xmlW.writeCharacters('RecordID');
                    xmlW.writeEndElement(); //close field
                    xmlW.writeStartElement(null,'Value',null);
                    xmlW.writeCharacters(u[0].Id);
                    xmlW.writeEndElement(); //Close value
                    xmlW.writeEndElement(); //Close Field

                    xmlW.writeStartElement(null,'Field',null);
                    xmlW.writeStartElement(null,'Name',null);
                    xmlW.writeCharacters('ShipmentOrderNumber');
                    xmlW.writeEndElement(); //close field
                    xmlW.writeStartElement(null,'Value',null);
                    xmlW.writeCharacters(u[0].Shipment_Order__r.Id);
                    xmlW.writeEndElement(); //Close value
                    xmlW.writeEndElement(); //Close Field

                    xmlW.writeStartElement(null,'Field',null);
                    xmlW.writeStartElement(null,'Name',null);
                    xmlW.writeCharacters('SF_ShipmentOrderNumber');
                    xmlW.writeEndElement(); //close field
                    xmlW.writeStartElement(null,'Value',null);
                    xmlW.writeCharacters(u[0].Shipment_Order__r.Id);
                    xmlW.writeEndElement(); //Close value
                    xmlW.writeEndElement(); //Close Field

         
                    
                    
         xmlW.writeEndElement(); //Close BatchMetadata
         xmlW.writeEndDocument(); //Close XML document
   
         
    String xmlStringxmlRes = xmlW.getXmlString();
    System.debug('The XML :'+xmlW.getXmlString());     
    xmlW.close();
    

 
String xmlString = xmlStringxmlRes;
    
Blob xmlBlob = Blob.valueOf(xmlString);
    
    

Blob fileBody = theFile.Body;
    
// Now Upload the file:

    
uploadFileToTransact.uploadFile(fileBody, Title, xmlBlob, 'BatchMetadata.xml', 'http://34.244.26.115:8080/tecexsfws/uploadBatch', String.valueof(pID));

}

}