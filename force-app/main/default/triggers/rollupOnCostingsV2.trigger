trigger rollupOnCostingsV2 on CPA_Costing__c (after insert,  before update, after update, after delete) {

    if(Trigger.isInsert && Trigger.isAfter) {
        RollupOnCostingHandler.afterInsert(Trigger.new);
    }

    if(Trigger.isUpdate && Trigger.isBefore) {
        RollupOnCostingHandler.beforeUpdate(Trigger.new,Trigger.oldMap);
    }

    if(Trigger.isUpdate && Trigger.IsAfter) {
        RollupOnCostingHandler.afterUpdate(Trigger.new,Trigger.oldMap);
    }

    if(Trigger.IsAfter && Trigger.isDelete) {
        RollupOnCostingHandler.afterDelete(Trigger.old);
    }
}