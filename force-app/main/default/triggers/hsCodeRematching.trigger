trigger hsCodeRematching on Tax_Calculator_Line_Item__c (before update) {
    
    
    Set<Id> liIds = new Set<Id>();
    Set<String> hsCode = new Set<String>();
    Set<String> hsCode4 = new Set<String>();
    Set<String> usHsCode8 = new Set<String>();
    Set<String> clearanceDestination = new Set<String>();
    Set<Id> tsIds = new Set<Id>();
    
    
    
    for (Tax_Calculator_Line_Item__c li : Trigger.new) {
        liIds.add(li.Id);
        hsCode.add(li.HS_Code__c);
        system.debug('hsCode---> '+ hsCode);
        hsCode4.add(li.HS_Code__c.left(4)+'%');
        usHsCode8.add(li.HS_Code__c.left(8));
        clearanceDestination.add(li.Clearance_Destination__c);
        system.debug('clearanceDestination---> '+ clearanceDestination);
        tsIds.add(li.Tax_Calculator__c);}
    
    
    Map<Id, HS_Codes_and_Associated_Details__c> usHsCodes = new Map<Id, HS_Codes_and_Associated_Details__c>( [select Id, Name, Additional_Part_Specific_Tax_Rate_1__c, Additional_Part_Specific_Tax_Rate_2__c,
                                                                                                               Additional_Part_Specific_Tax_Rate_3__c, Country_Name__c, Description__c, Destination_HS_Code__c, Rate__c,
                                                                                                               Specific_VAT_Rate__c, US_HTS_Code__c, Vat_Exempted__c
                                                                                                               from HS_Codes_and_Associated_Details__c 
                                                                                                               where US_HTS_Code__c Like :hsCode4
                                                                                                               and Country_Name__c in :clearanceDestination
                                                                                                               and Inactive__c = FALSE
                                                                                                               Order by Rate__c desc limit 50000]);
    
     Map<String, Id> allUSCodes = new Map<String, Id>();
    
     For (Id getAllCodes : usHsCodes.keySet()) { 
         
        String usCode = usHsCodes.get(getAllCodes).US_HTS_Code__c;
      
         If(!allUSCodes.containsKey(usCode)){   
         allUSCodes.put(usHsCodes.get(getAllCodes).US_HTS_Code__c, getAllCodes); }
           
       else if(usHsCodes.get(getAllCodes).Rate__c > usHsCodes.get(allUSCodes.get(usCode)).Rate__c){
         
       allUSCodes.remove(usCode); 
       allUSCodes.put(usHsCodes.get(getAllCodes).US_HTS_Code__c, getAllCodes);           
       } 
         
     }
    
    system.debug('allUSCodes.size()---> '+ allUSCodes.size());
    system.debug('allUSCode---> '+ allUSCodes);
    
     Map<Id, HS_Codes_and_Associated_Details__c> destinationHsCodes = new Map<Id, HS_Codes_and_Associated_Details__c>([select Id, Name, Additional_Part_Specific_Tax_Rate_1__c, Additional_Part_Specific_Tax_Rate_2__c,
                                                                                                               Additional_Part_Specific_Tax_Rate_3__c, Country_Name__c, Description__c, Destination_HS_Code__c, Rate__c,
                                                                                                               Specific_VAT_Rate__c, US_HTS_Code__c, Vat_Exempted__c
                                                                                                               from HS_Codes_and_Associated_Details__c 
                                                                                                               where Destination_HS_Code__c Like :hsCode4
                                                                                                               and Country_Name__c in :clearanceDestination
                                                                                                               and Inactive__c = FALSE
                                                                                                               Order by Rate__c desc limit 50000]);
    
    
    
    
    Map<String, Id> allDestinationCodes = new Map<String, Id>();
    Map<String, String> destinationCodeTrunc = new Map<String, String>();
    
     For (Id getAllDestCodes : destinationHsCodes.keySet()) { 
         
            If(!allDestinationCodes.containsKey(destinationHsCodes.get(getAllDestCodes).Destination_HS_Code__c)){
           allDestinationCodes.put(destinationHsCodes.get(getAllDestCodes).Destination_HS_Code__c, getAllDestCodes);
                
            }
         
            String InputString = destinationHsCodes.get(getAllDestCodes).Destination_HS_Code__c;
            String revStr = ' ';
            Integer InputStringsize = InputString.length();

       for (Integer i =0; InputString.length()>i; i++){
            revStr = InputString.substring(0, InputString.length()-i);
              
           If(!allDestinationCodes.containsKey(revStr)){
              allDestinationCodes.put(revStr, getAllDestCodes);   
           }
           
           else if(destinationHsCodes.get(getAllDestCodes).Rate__c > destinationHsCodes.get(allDestinationCodes.get(revStr)).Rate__c){
           
           allDestinationCodes.remove(revStr);
           allDestinationCodes.put(revStr, getAllDestCodes);
               
               
           }
           
           
          If((InputStringsize-i)==4){break;}
}
            
           
       } 
    
    
    
    
    
    system.debug('allDestinationCodes.size()---> '+ allDestinationCodes.size());
    system.debug('allDestinationCodes---> '+ allDestinationCodes);
    
    
    
    Map<Id, Tax_Structure_Per_Country__c> taxes = new Map<Id, Tax_Structure_Per_Country__c>([select Id, Name, Applied_to_Value__c,Tax_Type__c, Default_Duty_Rate__c, Country__r.Name,
                                                                                             Country__c, Rate__c, Order_Number__c
                                                                                            from Tax_Structure_Per_Country__c
                                                                                            where Country__r.Name in :clearanceDestination]);
    
     system.debug('taxes---> '+ taxes);
     system.debug('taxes.size()---> '+ taxes.size());
    
     Map<String, Id> clearanceCountry = new Map<String, Id>();
    
    For (Id clearance : taxes.keySet()) { 
           clearanceCountry.put(taxes.get(clearance).Country__r.Name, taxes.get(clearance).Country__c);           
       } 
    
     Map<String, Decimal> dutyrates = new Map<String, Decimal>();
     Map<String, Decimal> rates = new Map<String, Decimal>();
    Map<String, Decimal> vatrates = new Map<String, Decimal>();
    
    For (Id getDefaults : taxes.keySet()) { 
        
        If(taxes.get(getDefaults).Tax_Type__c == 'Duties'){
         rates.put(taxes.get(getDefaults).Country__r.Name, taxes.get(getDefaults).Rate__c);
         dutyrates.put(taxes.get(getDefaults).Country__r.Name, taxes.get(getDefaults).Default_Duty_Rate__c);
       } 
        
        Else if(taxes.get(getDefaults).Tax_Type__c == 'VAT') { 
        vatrates.put(taxes.get(getDefaults).Country__r.Name, taxes.get(getDefaults).Rate__c); }
            
            
        }
        
    
    
     
    Map<String, String> appliedTo = new Map<String, String>();
    Map<String, String> vatAppliedTo = new Map<String, String>();
    
    For (Id getAppliedTo : taxes.keySet()) { 
        
        If(taxes.get(getAppliedTo).Tax_Type__c == 'Duties'){
        appliedTo.put(taxes.get(getAppliedTo).Country__r.Name, taxes.get(getAppliedTo).Applied_to_Value__c); 
       } 
        
        Else if(taxes.get(getAppliedTo).Tax_Type__c == 'VAT') { 
        vatAppliedTo.put(taxes.get(getAppliedTo).Country__r.Name, taxes.get(getAppliedTo).Applied_to_Value__c); }
 
    }
    
    for (Tax_Calculator_Line_Item__c li : Trigger.new) {
         
       try { 

        
        String searchCode = li.HS_Code__c;
        String usSearchCode = li.HS_Code__c.left(8);
        Integer searchCodeLength = searchCode.length();
        String partDestination = li.Clearance_Destination__c;
        
        
        If(allUSCodes.get(usSearchCode) != null){ 
            
             system.debug('In First Search If ');
            
            li.Re_matched_HS_Code__c = usHsCodes.get(allUSCodes.get(usSearchCode)).Name;
            li.Re_matched_HS_Code_Description__c = usHsCodes.get(allUSCodes.get(usSearchCode)).Description__c;
            li.Re_matched_Duty_Rate__c = usHsCodes.get(allUSCodes.get(usSearchCode)).Rate__c;
            li.Re_matched_Part_Specific_Rate_1__c = usHsCodes.get(allUSCodes.get(usSearchCode)).Additional_Part_Specific_Tax_Rate_1__c;
            li.Re_matched_Part_Specific_Rate_2__c = usHsCodes.get(allUSCodes.get(usSearchCode)).Additional_Part_Specific_Tax_Rate_2__c;
            li.Re_matched_Part_Specific_Rate_3__c = usHsCodes.get(allUSCodes.get(usSearchCode)).Additional_Part_Specific_Tax_Rate_3__c;
            li.Re_matched_VAT_Exempted__c = usHsCodes.get(allUSCodes.get(usSearchCode)).Vat_Exempted__c;
            li.Re_matched_Specific_VAT_Rate__c = usHsCodes.get(allUSCodes.get(usSearchCode)).Specific_VAT_Rate__c;
            li.Default_Duty_Rate__c = dutyrates.get(li.Clearance_Destination__c);
            li.VAT_Rate__c = vatrates.get(li.Clearance_Destination__c);
            li.VAT_Applied_To_Value__c = vatAppliedTo.get(li.Clearance_Destination__c);
            li.Duty_Applied_To_Value__c = appliedTo.get(li.Clearance_Destination__c);
          //  li.Clearance_Country__c = clearanceCountry.get(partDestination);
        }
        
        Else If(allUSCodes.get(usSearchCode) == null && allDestinationCodes.get(searchCode) != null){
            
             system.debug('In Second Search If ');
            
            li.Re_matched_HS_Code__c = destinationHsCodes.get(allDestinationCodes.get(searchCode)).Name;
            li.Re_matched_HS_Code_Description__c = destinationHsCodes.get(allDestinationCodes.get(searchCode)).Description__c;
            li.Re_matched_Duty_Rate__c = destinationHsCodes.get(allDestinationCodes.get(searchCode)).Rate__c;
            li.Re_matched_Part_Specific_Rate_1__c = destinationHsCodes.get(allDestinationCodes.get(searchCode)).Additional_Part_Specific_Tax_Rate_1__c;
            li.Re_matched_Part_Specific_Rate_2__c = destinationHsCodes.get(allDestinationCodes.get(searchCode)).Additional_Part_Specific_Tax_Rate_2__c;
            li.Re_matched_Part_Specific_Rate_3__c = destinationHsCodes.get(allDestinationCodes.get(searchCode)).Additional_Part_Specific_Tax_Rate_3__c;
            li.Re_matched_VAT_Exempted__c = destinationHsCodes.get(allDestinationCodes.get(searchCode)).Vat_Exempted__c;
            li.Re_matched_Specific_VAT_Rate__c = destinationHsCodes.get(allDestinationCodes.get(searchCode)).Specific_VAT_Rate__c;
            li.Default_Duty_Rate__c = dutyrates.get(li.Clearance_Destination__c);
            li.VAT_Rate__c = vatrates.get(li.Clearance_Destination__c);
            li.VAT_Applied_To_Value__c = vatAppliedTo.get(li.Clearance_Destination__c);
            li.Duty_Applied_To_Value__c = appliedTo.get(li.Clearance_Destination__c);
            li.No_HS_Code_Re_match__c = FALSE;
           // li.Clearance_Country__c = clearanceCountry.get(partDestination);
           }
        
        
        
        Else if(allUSCodes.get(usSearchCode) == null && allDestinationCodes.get(searchCode) == null){
            
            system.debug('In Third Search If ');
            
            String InputString2 = searchCode;
            String revStr2 = ' ';
            Integer InputStringsize2 = InputString2.length();

           for (Integer i =0; InputString2.length()>i; i++){
              
               revStr2 = InputString2.substring(0, InputString2.length()-i);
               
               If(allDestinationCodes.get(revStr2) != null){
                   
            li.Re_matched_HS_Code__c = destinationHsCodes.get(allDestinationCodes.get(revStr2)).Name;
            li.Re_matched_HS_Code_Description__c = destinationHsCodes.get(allDestinationCodes.get(revStr2)).Description__c;
            li.Re_matched_Duty_Rate__c = destinationHsCodes.get(allDestinationCodes.get(revStr2)).Rate__c;
            li.Re_matched_Part_Specific_Rate_1__c = destinationHsCodes.get(allDestinationCodes.get(revStr2)).Additional_Part_Specific_Tax_Rate_1__c;
            li.Re_matched_Part_Specific_Rate_2__c = destinationHsCodes.get(allDestinationCodes.get(revStr2)).Additional_Part_Specific_Tax_Rate_2__c;
            li.Re_matched_Part_Specific_Rate_3__c = destinationHsCodes.get(allDestinationCodes.get(revStr2)).Additional_Part_Specific_Tax_Rate_3__c;
            li.Re_matched_VAT_Exempted__c = destinationHsCodes.get(allDestinationCodes.get(revStr2)).Vat_Exempted__c;
            li.Re_matched_Specific_VAT_Rate__c = destinationHsCodes.get(allDestinationCodes.get(revStr2)).Specific_VAT_Rate__c;
            li.Default_Duty_Rate__c = dutyrates.get(li.Clearance_Destination__c);
            li.VAT_Rate__c = vatrates.get(li.Clearance_Destination__c);
            li.VAT_Applied_To_Value__c = vatAppliedTo.get(li.Clearance_Destination__c);
            li.Duty_Applied_To_Value__c = appliedTo.get(li.Clearance_Destination__c);
           // li.Clearance_Country__c = clearanceCountry.get(partDestination);
             li.No_HS_Code_Re_match__c = FALSE;
             break;      
    
               }
               
               Else iF(allDestinationCodes.get(revStr2) == null){
               
            li.No_HS_Code_Re_match__c = TRUE;
            li.Re_matched_HS_Code__c = ' ';
            li.Re_matched_HS_Code_Description__c = ' ';
            li.Re_matched_Duty_Rate__c = null;
            li.Re_matched_Part_Specific_Rate_1__c = null;
            li.Re_matched_Part_Specific_Rate_2__c = null;
            li.Re_matched_Part_Specific_Rate_3__c = null;
            li.Re_matched_VAT_Exempted__c = false;
            li.Re_matched_Specific_VAT_Rate__c = null;
            li.Default_Duty_Rate__c = dutyrates.get(li.Clearance_Destination__c);
            li.VAT_Rate__c = vatrates.get(li.Clearance_Destination__c);
            li.VAT_Applied_To_Value__c = vatAppliedTo.get(li.Clearance_Destination__c);
            li.Duty_Applied_To_Value__c = appliedTo.get(li.Clearance_Destination__c);
           // li.Clearance_Country__c = clearanceCountry.get(partDestination);     
               
               
              If((InputStringsize2-i)==4){break;}
                   }
 
           }
        }
           

      
          // li.All_Matching_Complete__c = TRUE;
         //  li.CPA_Changed__c = FALSE;
              

  
        }
        
        
        catch(Exception e){
            System.Debug('Error setting region.' +e);
        }  
    
         
     }
    

}