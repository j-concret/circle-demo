trigger updatePartProduct on Part__c (before insert, before update) {

    List<Shipment_Order__c> shipmentOrder = [ Select Id, Ship_to_Country_new__c from Shipment_Order__c where id =:Trigger.new[0].Shipment_Order__c];
    List<HS_Codes_and_Associated_Details__c> coutryList= [select id, Country__c, Description__c, Destination_HS_Code__c, Rate__c, US_HTS_Code__c, Country__R.Country__c FROM
    HS_Codes_and_Associated_Details__c where Country__R.Country__c =:Trigger.new[0].Ship_To_Country__c AND Destination_HS_Code__c = :Trigger.new[0].US_HTS_Code__c]; 
                     
              iF( coutryList.size() == 1 && Trigger.new[0].US_HTS_Code__c != null){
                
                 Trigger.new[0].Matched_HS_Code2__c = coutryList[0].id;
                 Trigger.new[0].Rate__c = coutryList[0].Rate__c;
                 Trigger.new[0].Matched_HS_Code_Description__c = coutryList[0].Description__c;
                        
                     //Shipment_Order__r.IOR_Price_List__r.Destination__c
                     String exactHsCode = Trigger.new[0].US_HTS_Code__c;
                     system.debug('----var id: '+Trigger.new[0].Id);
                     system.debug('----var country: '+Trigger.new[0].Shipment_Order__r.Ship_To_Country__c);
                     system.debug(' -- var us hts code: '+Trigger.new[0].US_HTS_Code__c);
                     System.debug(' -- coutryList: '+coutryList);
                     System.debug(' -- coutryList: '+coutryList.size());


}

String exactHsCode = Trigger.new[0].US_HTS_Code__c;

iF( coutryList.size() == 0 && Trigger.new[0].US_HTS_Code__c != null) {

String exactHsCode6 = exactHsCode.substring(0,6);





List<HS_Codes_and_Associated_Details__c> coutryList2 = [select id, Country__c, Description__c, Destination_HS_Code__c, Rate__c, US_HTS_Code__c, Country__R.Country__c FROM
    HS_Codes_and_Associated_Details__c where Country__R.Country__c =:Trigger.new[0].Ship_To_Country__c AND Name Like :exactHsCode6 + '%' ]; 
    
    If( coutryList2.size() == 1){
                
                 Trigger.new[0].Matched_HS_Code2__c = coutryList2[0].id;
                 Trigger.new[0].Rate__c = coutryList2[0].Rate__c;
                 Trigger.new[0].Matched_HS_Code_Description__c = coutryList2[0].Description__c;
                        
                     //Shipment_Order__r.IOR_Price_List__r.Destination__c
                     system.debug('----var id: '+Trigger.new[0].Id);
                     system.debug('----var country: '+Trigger.new[0].Shipment_Order__r.Ship_To_Country__c);
                     system.debug(' -- var us hts code: '+Trigger.new[0].X6_Digit_Code__c);
                     System.debug(' -- coutryList: '+coutryList2);
                     System.debug(' -- coutryList: '+coutryList2.size());
//country
//hs codes
//Country__r.Country__c= :part.Shipment_Order__r.IOR_Price_List__r.Destination__c];

}

If( coutryList2.size() > 1){

Double highestRate = 0;
String highestRateId;
String highestRateDescription;
Boolean firstOne; 

for(HS_Codes_and_Associated_Details__c r : coutryList2 ){

                    if(r.Rate__c > highestRate){
                        highestRate = r.Rate__c;
                        highestRateId= r.Id;
                        highestRateDescription = r.Description__c;
                
                 Trigger.new[0].Matched_HS_Code2__c = highestRateId;
                 Trigger.new[0].Rate__c = highestRate;
                 Trigger.new[0].Matched_HS_Code_Description__c = highestRateDescription;
                        
                     //Shipment_Order__r.IOR_Price_List__r.Destination__c
                     system.debug('----var id: '+Trigger.new[0].Id);
                     system.debug('----var country: '+Trigger.new[0].Shipment_Order__r.Ship_To_Country__c);
                     system.debug(' -- var us hts code: '+Trigger.new[0].US_HTS_Code__c);
                     System.debug(' -- highestrateId: '+highestRateId);
                     System.debug(' -- highestRate: '+highestRate);
                     System.debug(' -- coutryList: '+coutryList2);
                     System.debug(' -- coutryList: '+coutryList2.size());
//country
//hs codes
//Country__r.Country__c= :part.Shipment_Order__r.IOR_Price_List__r.Destination__c];

}
}
If(highestRate == 0){

List<HS_Codes_and_Associated_Details__c> coutryList3 = [select id, Country__c, Description__c, Destination_HS_Code__c, Rate__c, US_HTS_Code__c, Country__R.Country__c FROM
    HS_Codes_and_Associated_Details__c where Country__R.Country__c =:Trigger.new[0].Ship_To_Country__c AND Name Like :exactHsCode6 + '%' LIMIT 1]; 

                 Trigger.new[0].Matched_HS_Code2__c = coutryList3[0].id;
                 Trigger.new[0].Rate__c = coutryList3[0].Rate__c;
                 Trigger.new[0].Matched_HS_Code_Description__c = coutryList3[0].Description__c;
                        
                     //Shipment_Order__r.IOR_Price_List__r.Destination__c
                     system.debug('----var id: '+Trigger.new[0].Id);
                     system.debug('----var country: '+Trigger.new[0].Shipment_Order__r.Ship_To_Country__c);
                     system.debug(' -- var us hts code: '+Trigger.new[0].X6_Digit_Code__c);
                     System.debug(' -- coutryList: '+coutryList3);
                     System.debug(' -- coutryList: '+coutryList3.size());



}

}

iF (coutryList2.size() == 0) {

String exactHsCode4 = exactHsCode.substring(0,4);



List<HS_Codes_and_Associated_Details__c> coutryList4 = [select id, Country__c, Description__c, Destination_HS_Code__c, Rate__c, US_HTS_Code__c, Country__R.Country__c FROM
    HS_Codes_and_Associated_Details__c where Country__R.Country__c =:Trigger.new[0].Ship_To_Country__c AND Name Like :exactHsCode4 + '%' ]; 
    
    If( coutryList4.size() == 1){
                
                 Trigger.new[0].Matched_HS_Code2__c = coutryList4[0].id;
                 Trigger.new[0].Rate__c = coutryList4[0].Rate__c;
                 Trigger.new[0].Matched_HS_Code_Description__c = coutryList4[0].Description__c;
                        
                     //Shipment_Order__r.IOR_Price_List__r.Destination__c
                     system.debug('----var id: '+Trigger.new[0].Id);
                     system.debug('----var country: '+Trigger.new[0].Shipment_Order__r.Ship_To_Country__c);
                     system.debug(' -- var us hts code: '+Trigger.new[0].X6_Digit_Code__c);
                     System.debug(' -- coutryList: '+coutryList4);
                     System.debug(' -- coutryList: '+coutryList4.size());


}

If( coutryList4.size() > 1){

Double highestRate2 = 0;
String highestRateId2;
String highestRateDescription2;
Boolean firstOne2; 

for(HS_Codes_and_Associated_Details__c r2 : coutryList4 ){

                    if(r2.Rate__c > highestRate2){
                        highestRate2 = r2.Rate__c;
                        highestRateId2= r2.Id;
                        highestRateDescription2 = r2.Description__c;
                
                 Trigger.new[0].Matched_HS_Code2__c = highestRateId2;
                 Trigger.new[0].Rate__c = highestRate2;
                 Trigger.new[0].Matched_HS_Code_Description__c = highestRateDescription2;
                        
                     //Shipment_Order__r.IOR_Price_List__r.Destination__c
                     system.debug('----var id: '+Trigger.new[0].Id);
                     system.debug('----var country: '+Trigger.new[0].Shipment_Order__r.Ship_To_Country__c);
                     system.debug(' -- var us hts code: '+Trigger.new[0].US_HTS_Code__c);
                     System.debug(' -- highestrateId2: '+highestRateId2);
                     System.debug(' -- highestRate2: '+highestRate2);
                     System.debug(' -- coutryList: '+coutryList4);
                     System.debug(' -- coutryList: '+coutryList4.size());


}
}
If(highestRate2 == 0){

List<HS_Codes_and_Associated_Details__c> coutryList5 = [select id, Country__c, Description__c, Destination_HS_Code__c, Rate__c, US_HTS_Code__c, Country__R.Country__c FROM
    HS_Codes_and_Associated_Details__c where Country__R.Country__c =:Trigger.new[0].Ship_To_Country__c AND Name Like :exactHsCode4 + '%' LIMIT 1]; 

                 Trigger.new[0].Matched_HS_Code2__c = coutryList5[0].id;
                 Trigger.new[0].Rate__c = coutryList5[0].Rate__c;
                 Trigger.new[0].Matched_HS_Code_Description__c = coutryList5[0].Description__c;
                        
                     //Shipment_Order__r.IOR_Price_List__r.Destination__c
                     system.debug('----var id: '+Trigger.new[0].Id);
                     system.debug('----var country: '+Trigger.new[0].Shipment_Order__r.Ship_To_Country__c);
                     system.debug(' -- var us hts code: '+exactHsCode4);
                     System.debug(' -- coutryList: '+coutryList5);
                     System.debug(' -- coutryList: '+coutryList5.size());



}

}

Else iF (coutryList2.size() == 0 && coutryList4.size() == 0) {

Trigger.new[0].No_HS_Code_Match__c = TRUE;

  }          
    


}
    
    }
    
    
    }