trigger runFlagsTrigger on Shipment_Order__c (after update) {
    
    
    If(Trigger.IsAfter && Trigger.IsUpdate){
    List<Id> soIdList = new List<Id>();   
    
   
    for(shipment_order__c so : Trigger.new){
        
        List<Id> soid = new List<Id>{so.Id};
        soIdList.add(so.Id); 
        
        
        
        if((so.Shipping_Status__c != 'Cost Estimate Abandoned' || so.Shipping_Status__c != 'Shipment Abandoned') &&   
        (( so.of_Line_Items__c != trigger.oldMap.get(so.Id).of_Line_Items__c &&  (trigger.oldMap.get(so.Id).of_Line_Items__c == 0 || trigger.oldMap.get(so.Id).of_Line_Items__c == NULL) || (so.of_Line_Items__c == 0 || so.of_Line_Items__c == NULL)) || 
        ( so.of_packages__c != trigger.oldMap.get(so.Id).of_packages__c &&  (trigger.oldMap.get(so.Id).of_packages__c == 0 || trigger.oldMap.get(so.Id).of_packages__c == NULL) || (so.of_packages__c == 0 || so.of_packages__c == NULL))) || 
        ( so.Final_Deliveries_New__c != trigger.oldMap.get(so.Id).Final_Deliveries_New__c &&  (trigger.oldMap.get(so.Id).Final_Deliveries_New__c == 0 || trigger.oldMap.get(so.Id).Final_Deliveries_New__c == NULL) || (so.Final_Deliveries_New__c == 0 || so.Final_Deliveries_New__c == NULL))){                
         try{ 
                
             shipmentOrderApproval.firstRun = false;
             shipmentOrderApproval.approvalRules(soid);
                
            }
           catch(exception e){}
                 
      }
    
            //############### End of code added by Masechaba Maseli  ###################################
    }

}
    
}