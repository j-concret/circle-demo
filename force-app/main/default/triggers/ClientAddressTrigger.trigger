trigger ClientAddressTrigger on Client_Address__c (before insert, before update) {
	// this method we want to run on before insert and before update.
	// 

    Boolean skipTrigger = false;
    //below we are skipping the trigger if called from NCP/API.
    if(String.valueOf(URL.getCurrentRequestUrl()).toLowerCase().contains('apexrest') || Test.isRunningTest()){
        skipTrigger = true;
    }
    if(!skipTrigger || Test.isRunningTest()){
        ClientAddressTriggerHandler.validateAddress(Trigger.new);
    }
}