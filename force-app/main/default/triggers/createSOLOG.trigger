trigger createSOLOG on zkmulti__MCCheckpoint__c (after insert) {
    
    if(Trigger.isAfter && Trigger.isInsert) {
        //System.debug('Checkpoint Updation Started ==>');
        Map<String,List<String>> SOIDs = new Map<String,List<String>>();
        String desCHTR = '';
        Map<String,String> MappedStatusMap = new Map<String,String>{
            'Cost Estimate'=>'Quote Created',
                'Shipment Pending'=>'Compliance Pending',
                'CI, PL and DS Received'=>'Compliance Pending',
                'AM Approved CI,PL and DS\'s'=>'Compliance Pending',
                'Complete Shipment Pack Submitted for Approval'=>'Compliance Pending',
                'License Pending'=>'Compliance Pending',
                'Awaiting AWB'=>'Compliance Pending',
                'Customs Approved (Pending Payment)'=>'Compliance Pending',
                'Client Approved to ship'=>'Approved to Ship',	
                'In Transit to Hub'=>'In-Transit',
                'Arrived at Hub'=>'In-Transit',
                'In transit to country'=>'In-Transit',
                'Arrived in Country, Awaiting Customs Clearance'=>'Arrived in Country',
                'Cleared Customs'=>'Cleared Customs',
                'Final Delivery in Progress'=>'Final Delivery in Transit',
                'Awaiting POD'=>'Delivered',
                'POD Received'=>'Delivered',
                'Customs Clearance Docs Received'=>'Delivered',
                'On Hold'=>'HALT'
                };
                    Set<ID> CPAIDs = new Set<ID>();
        Map<String,Tracking_rules__c> shipmentsToUpdate = new Map<String,Tracking_rules__c>();
        Map<String,String> CPAValuesMap = new Map<String,String>();
        Map<String,List<SO_Status_Description__c>> SOSDValuesMap = new Map<String,List<SO_Status_Description__c>>();
        // Set<ID> ZKSO = new Set<ID>();
        Set<String> SOStatus = New Set<String>();
        String Choice='Not applicable';
        MAP<ID,zkmulti__MCCheckpoint__c> CPrecord = new MAP<ID,zkmulti__MCCheckpoint__c>();
        MAP<String,zkmulti__MCCheckpoint__c> threeDigitCodeMap = new MAP<String,zkmulti__MCCheckpoint__c>();
        MAP<String,zkmulti__MCCheckpoint__c> twoDigitCodeMap = new MAP<String,zkmulti__MCCheckpoint__c>();
        Map<String,List<String>> CountryMapWithCP = new Map<String,List<String>>();
        Map<String,String> CPATrackingTerms = new Map<String,String>();
        Map<String,zkmulti__MCShipment__c> ZKShipmentOnCP = new Map<String,zkmulti__MCShipment__c>();
        Map<String,SO_Travel_History__c> SoTravelMap = new Map<String,SO_Travel_History__c>();
        Map<ID,zkmulti__MCCheckpoint__c> ZKCheckPointsWithSOMap = new Map<ID,zkmulti__MCCheckpoint__c>();
        Map<String,shipment_order__c> shipmentMap = new Map<String,shipment_order__c>();
        List<String> cpIdlist = new List<String>();        
        For(zkmulti__MCCheckpoint__c CP: Trigger.new){
            
            if(SOIDs != null && SOIDs.get(CP.SOID__c) != null ){
                List<String> cpIdlist1 = SOIDs.get(CP.SOID__c);
                cpIdlist1.add(cp.id);
                SOIDs.put(CP.SOID__c,cpIdlist1);
            }else{
                cpIdlist.add(cp.id);
                SOIDs.put(CP.SOID__c,cpIdlist);
            }
            
            CPrecord.put(Cp.Id,CP);
            SoTravelMap.put(Cp.Id,
                            new SO_Travel_History__c (
                                Source__c = 'Checkpoints',
                                Date_Time__c = System.now(),
                                Location__c = (CP.zkmulti__Address__c != null ) ? CP.zkmulti__Address__c : '',
                                Description__c = 'Courier Status update: '+((CP.zkmulti__Message__c != null) ?CP.zkmulti__Message__c :''   ) +' '+((CP.zkmulti__Status__c != null) ? CP.zkmulti__Status__c : '' ),//SOSD.Description_ETA__c,
                                Type_of_Update__c = 'NONE',
                                TecEx_SO__c = CP.SOID__c,
                                Freight_Request__c = CP.FrId__c
                            )
                           );
            //ZKCheckPointsWithSOMap.put(CP.zkmulti__Shipment__c,CP); Carrier_Name
            
            if (CP.zkmulti__Address__c != null ){ // ? CP.zkmulti__Address__c : '';//CP.zkmulti__Address__c;
                String address = CP.zkmulti__Address__c;
                
                if(CP.Carrier_Name__c == 'DHL Express'){
                    
                    String threeDigitCode = address.substring(address.length()-3,address.length());
                    threeDigitCodeMap.put(threeDigitCode,CP);
                    
                }else if(CP.Carrier_Name__c == 'FedEx'){
                    
                    String twoDigitCode = address.substring(address.length()-2,address.length());
                    twoDigitCodeMap.put(twoDigitCode,cp); 
                    
                }
            }
            
        }
        
        //   System.debug('SoTravelMap with Checkpoint values ==>');
        //  System.debug(JSON.serialize(SoTravelMap));
        
        
        /*  for(zkmulti__MCShipment__c zkShipment:[Select Id, zkmulti__Carrier__c from zkmulti__MCShipment__c where Id in:ZKCheckPointsWithSOMap.keySet()]){

zkmulti__MCCheckpoint__c CP = ZKCheckPointsWithSOMap.get(zkShipment.Id);
ZKShipmentOnCP.put(CP.Id,zkShipment);
if (CP.zkmulti__Address__c != null ){// ? CP.zkmulti__Address__c : '';//CP.zkmulti__Address__c;
String address = CP.zkmulti__Address__c;

if(zkShipment.zkmulti__Carrier__c == 'DHL Express'){

String threeDigitCode = address.substring(address.length()-3,address.length());
threeDigitCodeMap.put(threeDigitCode,CP);

}else if(zkShipment.zkmulti__Carrier__c == 'FedEx'){

String twoDigitCode = address.substring(address.length()-2,address.length());
twoDigitCodeMap.put(twoDigitCode,cp); 

}
}

}*/
        
        System.debug('threeDigitCodeMap values ==>');
        System.debug(JSON.serialize(threeDigitCodeMap));
        
        System.debug('twoDigitCodeMap values ==>');
        System.debug(JSON.serialize(twoDigitCodeMap));
        
        if(threeDigitCodeMap != null && !threeDigitCodeMap.isEmpty()){
            List<String> cooList = new List<String>();
            
            for(CountryofOriginMap__c coo:[SELECT Id,Country__c,Three_Digit_Country_Code__c FROM CountryofOriginMap__c where Three_Digit_Country_Code__c IN :threeDigitCodeMap.keySet()]){
                zkmulti__MCCheckpoint__c CP = threeDigitCodeMap.get(coo.Three_Digit_Country_Code__c);
                if(CountryMapWithCP.get(CP.Id) != null){
                    cooList = CountryMapWithCP.get(CP.Id);
                    cooList.add(coo.Country__c);
                    CountryMapWithCP.put(CP.Id,cooList);
                }else{
                    cooList.add(coo.Country__c);
                    CountryMapWithCP.put(CP.Id,cooList);
                }
                
            }
            
        }
        if(twoDigitCodeMap != null && !twoDigitCodeMap.isEmpty()){
            List<String> cooList = new List<String>();
            for(CountryofOriginMap__c coo:[SELECT Id,Country__c, Two_Digit_Country_Code__c FROM CountryofOriginMap__c where Two_Digit_Country_Code__c IN :twoDigitCodeMap.keySet()]){
                zkmulti__MCCheckpoint__c CP = twoDigitCodeMap.get(coo.Two_Digit_Country_Code__c);
                if(CountryMapWithCP.get(CP.Id) != null){
                    cooList = CountryMapWithCP.get(CP.Id);
                    cooList.add(coo.Country__c);
                    CountryMapWithCP.put(CP.Id,cooList);
                }else{
                    cooList.add(coo.Country__c);
                    CountryMapWithCP.put(CP.Id,cooList);
                }
            }
        }        
        
        //  System.debug('CountryMapWithCP values ==>');
        //  System.debug(JSON.serialize(CountryMapWithCP));
        
        
        List<shipment_order__c> SoList =[select Id,Shipping_status__c,of_packages__c ,Ship_From_Country__c,Clearance_Destination__c,Mapped_Shipping_status__c,Ship_to_Country_new__c,CPA_v2_0__c,CPA_v2_0__r.Final_Destination__c ,CPA_v2_0__r.Tracking_Term__c,Client_Contact_for_this_Shipment__r.Client_Notifications_Choice__c,Client_Contact_for_this_Shipment__r.Major_Minor_notification__c,Client_Contact_for_this_Shipment__r.Client_Tasks_notification_choice__c ,Expected_Date_to_Next_Status__c   from Shipment_Order__c where id in:SOIDs.keySet()];
        
        //  System.debug('SoList ==>');
        //  System.debug(JSON.serialize(SoList));
        
        For(shipment_order__c So :SoList){
            
            
            
            DateTime date1 = System.now(); 
            
            if(So.Expected_Date_to_Next_Status__c != null){
                date1 = So.Expected_Date_to_Next_Status__c; 
            }
            
            //  System.debug('Complete So.Id ==>');
            //  System.debug(JSON.serialize(So));
            
            String soId = So.Id;
            String shipId = soId.substring(0,soId.length()-3);
            shipmentMap.put(shipId,So);
            
            CPAValuesMap.put(So.CPA_v2_0__c,shipId);
            //   CPATrackingTerms.put(shipId,So.CPA_v2_0__r.Tracking_Term__c);
            List<String> cpListMap = SOIDs.get(shipId);
            For(String CPKey: cpListMap){
                if(SoTravelMap.get(CPKey) != null){
                    //      System.debug('Inside HIstory creation');
                    if(shipId != null){
                        SO_Travel_History__c SOTH = SoTravelMap.get(CPKey);
                        SOTH.Mapped_Status__c = So.Mapped_Shipping_status__c;
                        SOTH.Client_Notifications_Choice__c =So.Client_Contact_for_this_Shipment__r.Client_Notifications_Choice__c != null ? So.Client_Contact_for_this_Shipment__r.Client_Notifications_Choice__c :'Not applicable';
                        SOTH.Major_Minor_notification__c = So.Client_Contact_for_this_Shipment__r.Major_Minor_notification__c != null ? So.Client_Contact_for_this_Shipment__r.Major_Minor_notification__c :'';
                        SOTH.Client_Tasks_notification_choice__c = So.Client_Contact_for_this_Shipment__r.Client_Tasks_notification_choice__c;
                        SOTH.Expected_Date_to_Next_Status__c = date1;
                        SoTravelMap.put(CPKey,SOTH);
                    }
                }
                
            }
            
            
        }
        
        System.debug([SELECT Id , Name ,Description_ETA__c,Expected_Days__C,Status__c,CPA_v2_0__c,CPA_v2_0__r.Id FROM SO_Status_Description__c]);
        for(SO_Status_Description__c SOSD : [SELECT Id , Name ,Description_ETA__c,Expected_Days__C,Status__c,CPA_v2_0__c,CPA_v2_0__r.Id FROM SO_Status_Description__c where CPA_v2_0__r.Id IN :CPAValuesMap.keySet() AND Type__c = 'Shipping status' ]){
            if(SOSDValuesMap.containsKey(CPAValuesMap.get(SOSD.CPA_v2_0__c))){
                List<SO_Status_Description__c> listSOSD = SOSDValuesMap.get(CPAValuesMap.get(SOSD.CPA_v2_0__c));
                listSOSD.add(SOSD);
                SOSDValuesMap.put(CPAValuesMap.get(SOSD.CPA_v2_0__c),listSOSD);
            }else{
                List<SO_Status_Description__c> listSOSD = new List<SO_Status_Description__c>();
                listSOSD.add(SOSD);
                SOSDValuesMap.put(CPAValuesMap.get(SOSD.CPA_v2_0__c),listSOSD);
            }
        }
        List<Tracking_rules__c> trackingRules = [SELECT Id, Name, Tracking_term__c, Checkpoint_Message_Contains__c, Checkpoint_Location_country_contains__c, Current_Status__c, Mapped_Status__c, Description__c, Type_of_Update__c, Eligible_Carriers__c,Shipping_status_to_be_update__c FROM Tracking_rules__c];
        
        for(zkmulti__MCCheckpoint__c CP : CPrecord.values()){
            //  zkmulti__MCShipment__c zkShipment = ZKShipmentOnCP.get(CP.Id);
            List<String> clearanceCountryList = new List<String>();
            if(CountryMapWithCP != null && CountryMapWithCP.get(CP.Id) != null){
                clearanceCountryList = CountryMapWithCP.get(CP.Id);
            }
            shipment_order__c So = new shipment_order__c();
            if(shipmentMap.get(CP.SOID__c) !=null){
                So = shipmentMap.get(CP.SOID__c) ;
            }
            String CPATrackingTerm = (So.CPA_v2_0__r.Tracking_Term__c != null)? So.CPA_v2_0__r.Tracking_Term__c : '';
            
            //  System.debug('zkShipment IN CP==>');
            //   System.debug(JSON.serialize(zkShipment));
            
            //   System.debug('CPATrackingTerm IN CP ==>');
            //   System.debug(JSON.serialize(CPATrackingTerm));
            
            //  System.debug('clearanceCountry IN CP ==>');
            //   System.debug(JSON.serialize(clearanceCountryList));
            //   System.debug(clearanceCountryList.contains(So.CPA_v2_0__r.Final_Destination__c));
            
            //   System.debug('So IN CP ==>');
            //   System.debug(JSON.serialize(So));            
            System.debug('INSIDE Mapped_Shipping_status__c ==>');
            System.debug(JSON.serialize(So.Mapped_Shipping_status__c));
            System.debug('Tracking Rules TO check ==>');
            System.debug(JSON.serialize(trackingRules));
            for(Tracking_rules__c tr:trackingRules){
                System.debug('Checpoint Location Country ==> Clearance Country');
                System.debug(tr.Checkpoint_Location_country_contains__c+' v/s clearance country');
                System.debug('clearanceCountryList ==> Clearance Country');
                System.debug(JSON.serialize(clearanceCountryList)+' v/s '+So.CPA_v2_0__r.Final_Destination__c);
                //  System.debug('zkShipment.zkmulti__Carrier__c ==> tr.Eligible_Carriers__c');
                // System.debug(zkShipment.zkmulti__Carrier__c+' v/s '+tr.Eligible_Carriers__c);
                System.debug('CPATrackingTerm ==> tr.Tracking_term__c');
                System.debug(CPATrackingTerm+' v/s '+tr.Tracking_term__c);
                System.debug('CP.zkmulti__Message__c ==> tr.Checkpoint_Message_Contains__c');
                System.debug(CP.zkmulti__Message__c+' v/s '+tr.Checkpoint_Message_Contains__c);
                System.debug('So.Mapped_Shipping_status__c ==> tr.Current_Status__c');
                System.debug(So.Mapped_Shipping_status__c+' v/s '+tr.Current_Status__c);
                //     System.debug('tr IN CP ==>');
                //  System.debug(JSON.serialize(tr)); 
                //  System.debug(JSON.serialize(So.CPA_v2_0__r.Final_Destination__c)); 
                //System.debug(JSON.serialize(clearanceCountry));
                //  System.debug(JSON.serialize(tr.Checkpoint_Location_country_contains__c));
                String cpaFinalDest = (So.CPA_v2_0__r.Final_Destination__c != null ) ? So.CPA_v2_0__r.Final_Destination__c : '';
                // String checkPointLocationCountry = (tr.Checkpoint_Location_country_contains__c != null) ? tr.Checkpoint_Location_country_contains__c :'';
                
                if( (clearanceCountryList != null && tr.Checkpoint_Location_country_contains__c.toLowercase().contains('clearance country') 
                     && clearanceCountryList.contains(cpaFinalDest)) || (tr.Checkpoint_Location_country_contains__c.toLowercase().contains('none')) ){
                         
                         /*  System.debug(tr.id);
System.debug(JSON.serialize(tr.Current_Status__c)); 
System.debug(JSON.serialize(tr.Checkpoint_Message_Contains__c));
System.debug(JSON.serialize(So.Mapped_Shipping_status__c.toLowercase().contains(tr.Current_Status__c.toLowercase())));
System.debug(tr.Tracking_term__c);*/
                         
                         if( (tr.Eligible_Carriers__c == 'NONE' || CP.Carrier_Name__c.toLowercase().contains(tr.Eligible_Carriers__c.toLowercase())) 
                            && (tr.Tracking_term__c == 'NONE' || CPATrackingTerm.toLowerCase().contains(tr.Tracking_term__c.toLowercase())) 
                            && (tr.Checkpoint_Message_Contains__c == 'NONE' || CP.zkmulti__Message__c.toLowercase().contains(tr.Checkpoint_Message_Contains__c.toLowercase()) )
                            && (tr.Current_Status__c == 'NONE' || So.Mapped_Shipping_status__c.toLowercase().contains(tr.Current_Status__c.toLowercase()) 
                            && so.of_packages__c <=1  )
                           ){
                               System.debug('INSIDE MAPPED ==>');
                               
                               SO_Travel_History__c SOTH = SoTravelMap.get(CP.Id);
                               
                               System.debug('SOSDValuesMap.get(SOTH.TecEx_SO__c)');
                               String soId = SOTH.TecEx_SO__c;
                               String shipId = soId.substring(0,soId.length()-3);
                               System.debug(JSON.serializePretty(SOSDValuesMap.get(shipId)));
                               System.debug(JSON.serializePretty(SOSDValuesMap));
                               System.debug(JSON.serializePretty(shipId));
                               if(SOSDValuesMap.get(shipId) != null){
                               for(SO_Status_Description__c SOSD : SOSDValuesMap.get(shipId)){
                                   System.debug('@@ TR SOSD '+JSON.serializePretty(SOSD));
                                   System.debug('TR ID '+tr.Id);
                                   if(SOSD != null){
                                       System.debug('Mapped Status for new Shipping '+MappedStatusMap.get(tr.Shipping_status_to_be_update__c));
                                       if(SOSD.Status__c == MappedStatusMap.get(tr.Shipping_status_to_be_update__c)){
                                           desCHTR = (SOSD.Description_ETA__c).replace('[Ship to Country]',So.Ship_to_Country_new__c).replace('[Clearance Destination]',So.Clearance_Destination__c).replace('[Ship from Country]',So.Ship_From_Country__c );
                                       }
                                   }
                               }
                               }
                               // String des = tr.Description__c.replace('[Destination Country]',cpaFinalDest);
                               
                               SOTH.Source__c = 'Checkpoints + TR';
                               SOTH.Description__c = desCHTR;//tr.Description__c;
                               SOTH.Mapped_Status__c = tr.Mapped_Status__c;
                               SOTH.SO_Shipping_Status__c = tr.Shipping_status_to_be_update__c;
                               SOTH.Type_of_Update__c = tr.Type_of_Update__c;
                               SoTravelMap.put(CP.Id,SOTH);
                               shipmentsToUpdate.put(SOTH.TecEx_SO__c,tr);	
                               break;
                           }
                     }
            }
        }
        
        
        
        
        if(shipmentsToUpdate !=null && !shipmentsToUpdate.isEmpty()){
            List<Shipment_Order__c> shipListtoUpdate = new List<Shipment_Order__c>();
            for(Shipment_Order__c So : [select Id,Shipping_status__C,Time_Shipment_Live__c,Time_CSE_Approved_Pack__c,Time_ICE_Submitted_to_Supplier__c,Time_Customs_Approved__c,Time_Client_Approved_to_Ship__c,Shipment_Date__c,
                                        Arrived_at_Hub__c,Cancelled_Date__c,Date_Departed_Hub__c,Arrived_in_Customs_Date__c,Customs_Cleared_Date__c,Final_Delivery_Date__c,POD_Date__c,Customs_Clearance_Docs_Received_Date__c
                                        FROM Shipment_Order__c Where Id IN : shipmentsToUpdate.keySet()]) {
                                            Tracking_rules__c tr = shipmentsToUpdate.get(So.Id);
                                            So.Shipping_Status__c = tr.Shipping_status_to_be_update__c;//'Client Approved to ship';
                                            So.banner_feed__c = desCHTR;
                                            If(tr.Shipping_status_to_be_update__c== 'CI, PL and DS Received' && So.Time_Shipment_Live__c ==null){So.Time_Shipment_Live__c=system.now();}else
                                                if(tr.Shipping_status_to_be_update__c== 'AM Approved CI,PL and DS\'s' && So.Time_CSE_Approved_Pack__c ==null){So.Time_CSE_Approved_Pack__c=system.now();}else
                                                    if(tr.Shipping_status_to_be_update__c== 'Complete Shipment Pack Submitted for Approval' && So.Time_ICE_Submitted_to_Supplier__c ==null){So.Time_ICE_Submitted_to_Supplier__c=system.now();}else
                                                        if(tr.Shipping_status_to_be_update__c== 'Customs Approved (Pending Payment)' && So.Time_Customs_Approved__c ==null){So.Time_Customs_Approved__c=system.now();}else
                                                            if(tr.Shipping_status_to_be_update__c== 'Client Approved to ship' && So.Time_Client_Approved_to_Ship__c ==null){So.Time_Client_Approved_to_Ship__c=system.now();}else
                                                                if(tr.Shipping_status_to_be_update__c== 'In Transit to Hub' && So.Shipment_Date__c ==null){So.Shipment_Date__c=system.today();}else
                                                                    if(tr.Shipping_status_to_be_update__c== 'Arrived at Hub' && So.Arrived_at_Hub__c ==null){So.Arrived_at_Hub__c=system.today();}else
                                                                        if(tr.Shipping_status_to_be_update__c== 'Cancelled - No fees/costs' && So.Cancelled_Date__c ==null){So.Cancelled_Date__c=system.today();}else
                                                                            if(tr.Shipping_status_to_be_update__c== 'In transit to country' && So.Date_Departed_Hub__c ==null){So.Date_Departed_Hub__c=system.today();}else
                                                                                if(tr.Shipping_status_to_be_update__c== 'Arrived in Country, Awaiting Customs Clearance' && So.Arrived_in_Customs_Date__c ==null){So.Arrived_in_Customs_Date__c=system.today();}else
                                                                                    if(tr.Shipping_status_to_be_update__c== 'Cleared Customs' && So.Customs_Cleared_Date__c ==null){So.Customs_Cleared_Date__c=system.today();}else
                                                                                        if(tr.Shipping_status_to_be_update__c== 'Awaiting POD' && So.Final_Delivery_Date__c ==null){So.Final_Delivery_Date__c=system.today();}else
                                                                                            if(tr.Shipping_status_to_be_update__c== 'POD Received' && So.POD_Date__c ==null){So.POD_Date__c=system.today();}else
                                                                                                if(tr.Shipping_status_to_be_update__c== 'Customs Clearance Docs Received' && So.Customs_Clearance_Docs_Received_Date__c ==null){So.Customs_Clearance_Docs_Received_Date__c=system.today();}   
                                            
                                            shipListtoUpdate.add(So);     
                                        }
            update shipListtoUpdate;
        }  
        
        
        if(SoTravelMap != null){
            
            System.debug('Complete SoTravelMap ==>');
            System.debug(JSON.serialize(SoTravelMap));
            insert SoTravelMap.values();
            
        }
        
        //        Country name from custom settings based on zkmulti__MCShipment__r.zkmulti__Carrier__c (Dhl Express || FedEx)
        //        SOTH.Mapped_Status__c = So.Shipping_status__c;
        
        // Check  Eligible_Carriers__c.lowercase() contains  zkmulti__MCShipment__r.zkmulti__Carrier__c.lowercase()
        // Tracking_term__c.lowercase()  contains cpa.tracking_term__C.lowercase()
        // Checkpoint_Message_Contains__c.lowercase() contains cp.zkmulti__Message__c.lowercase()
        //  Checkpoint_Location_country_contains__c   --> (Null ||Clearance Country )
        //	If Clearance Country --> Get from CPA Final_destination__C contains   Country name from custom settings based on zkmulti__MCShipment__r.zkmulti__Carrier__c (Dhl Express || FedEx)
        //SELECT Id, Country__c, Two_Digit_Country_Code__c, Currency_ISO_Code__c, Three_Digit_Country_Code__c FROM CountryofOriginMap__c  where Two_Digit_Country_Code__c='US'
        //If NONE --> dont consider Ex: none contains US
        //Current_Status__c contains (SOTH.Mapped_Status__c || So.Shipping_status__c)
        //Mapped trackingRule---> Name
        
        // Finally if trackrule matched then We update SOTH.descrtiption with matched tracking rule description  
        //   && We update SOTH.Mapped status with matched tracking rule mapped status 
        //   && We update SOTH.TypeofUpdate with matched tracking rule TypeofUpdate 
        
        
        
        
        
    }
    
}