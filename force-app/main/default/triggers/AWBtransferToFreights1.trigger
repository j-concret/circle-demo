trigger AWBtransferToFreights1 on Attachment (after insert) {
    
    List<zkmulti__MCShipment__c> ZenShipmentList = new List<zkmulti__MCShipment__c>();
    List<Attachment> AttachmentIDs = new List<Attachment>();
    
    List<ID> ZenShipmentIds = new List<ID>();
    id ZenShipmentId;
    for(Attachment attach : trigger.New){
         //Check if added attachment is related to Lead or not
         if(attach.ParentId.getSobjectType() == zkmulti__MCShipment__c.SobjectType){
		       ZenShipmentId=attach.ParentId;
               ZenShipmentIds.add(attach.ParentId);
             AttachmentIDs.add(attach);
          System.debug('ZenShipmentId-->'+ZenShipmentId);           
         }
    }

    IF(ZenShipmentIds.size() > 0){
       set<Attachment> InsertAttachment= new set<Attachment>();
        string FreightID = 'asdf';
        zkmulti__MCShipment__c ZEnSO = [Select Id,freight_request__c from zkmulti__MCShipment__c where Id =: ZenShipmentId];
        FreightID = ZEnSO.freight_request__c;
        List<Attachment> attList = [SELECT id, name, body,parentid from Attachment where parentid in :ZenShipmentIds and id in :AttachmentIDs];
        for(integer i=0;attList.size()>i;i++ ){
            
            Attachment b = New Attachment();
            b.Name = attList[i].Name;
            b.body = attList[i].body;
            b.parentid=FreightID;
        InsertAttachment.add(b);
        }
       
        List<Attachment> lStrings = new List<Attachment>(InsertAttachment);
         INsert lStrings;
    }
    
    
}