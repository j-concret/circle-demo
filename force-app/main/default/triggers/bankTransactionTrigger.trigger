trigger bankTransactionTrigger on Bank_transactions__c (before insert, after insert, before update, after update) {
    
    Set<Id> bankTransactions = new Set<Id>();
    
    
    Map<Id, Currency_Management2__c> currencyMap = new Map<Id, Currency_Management2__c>([Select Id, Name, Conversion_Rate__c, Currency__c, ISO_Code__c
                                                                                         From Currency_Management2__c]); 
    
    Map<String, Decimal> conversionRate = new Map<String, Decimal>();
    
    
    Map<Id, GL_Account__c> glAccountMap = new Map<Id, GL_Account__c>([Select Id, Name, GL_Account_Description__c,GL_Account_Short_Description__c,Currency__c 
                                                                      From GL_Account__c]); 
    
    Map<String, Id> glAccount = new Map<String, Id>(); 
    
    For (Id getGlAccount : glAccountMap.keySet()) { 
        glAccount.put(glAccountMap.get(getGlAccount).Name, getGlAccount );           
    }
    
    For (Id getRates : currencyMap.keySet()) { 
        conversionRate.put(currencyMap.get(getRates).Name,currencyMap.get(getRates).Conversion_Rate__c );           
    }  
    
    
    Map<Id, Accounting_Period__c> accPeriodMap = new Map<Id, Accounting_Period__c>([Select Id, Name From Accounting_Period__c Where Status__c = 'Open']); 
    
    Map<String, Id> accPeriod = new Map<String, Id>();  
    
    For (Id getAccPeriod : accPeriodMap.keySet()) { accPeriod.put(accPeriodMap.get(getAccPeriod).Name, getAccPeriod ); } 
    
    
    If(Trigger.IsInsert && Trigger.IsBefore){
        
        for (Bank_transactions__c bts : Trigger.new) {
            bankTransactions.add(bts.Id);            
            
        }  
        
        
        for (Bank_transactions__c bts : Trigger.new) {
            
            bts.Conversion_Rate__c = conversionRate.get(glAccountMap.get(bts.Bank_Account_New__c).Currency__c);
            bts.Accounting_Period_New__c = accPeriod.get(bts.Accounting_Period_Formula__c);
            
        }
        
    }
    
    If(Trigger.IsInsert && Trigger.IsAfter){}
    
    
    If(Trigger.IsUpdate && Trigger.IsBefore){
        
        Bank_transactions__c oldBts;
        Set<Id> glAccountList = new Set<Id>();
        for (Bank_transactions__c bts : Trigger.new) {
            bankTransactions.add(bts.Id); 
            oldBts = Trigger.oldMap.get(bts.Id);    
            glAccountList.add(bts.Bank_Account_New__c);        
        }
        
        Map<Id, GL_Account__c> glAccountMap= new Map<Id, GL_Account__c>([Select Id, GL_Account_Description__c, GL_Account_Short_Description__c
                                                                           FROM GL_Account__c
                                                                           Where Id in :glAccountList]);
        
        Map<Id, Bank_Transaction_Applied__c> appliedBts = new Map<Id, Bank_Transaction_Applied__c>([Select Id, Name, Accounting_Period_New__c, Amount__c, Bank_Transaction__c, Date__c,  Invoice__c
                                                                                                    FROM Bank_Transaction_Applied__c
                                                                                                    Where Bank_Transaction__c in :bankTransactions]);
        
        
        system.debug('appliedBts'+appliedBts);
        
        Map<Id, Account> accountMap = new Map<Id, Account>([Select Id, Name, RecordTypeId
                                                            FROM Account]);
        
        
        List<Transaction_Line_New__c> createdTls = new List<Transaction_Line_New__c>();
        
        for (Bank_transactions__c bts : Trigger.new) {
            
            String Descript = glAccountMap.get(bts.Bank_Account_New__c).GL_Account_Description__c;
            String ShortDescript = glAccountMap.get(bts.Bank_Account_New__c).GL_Account_Short_Description__c;
            Decimal BankCharge =  bts.RecordTypeId == Schema.Sobjecttype.Bank_transactions__c.getRecordTypeInfosByDeveloperName().get('Payment').getRecordTypeId()?bts.Bank_charges__c*-1:bts.Bank_charges__c;
            
            system.debug('bts'+bts);
            
            If(bts.Currency__c != oldBts.Currency__c){     
                bts.Conversion_Rate__c = conversionRate.get(bts.Currency__c);
                bts.Accounting_Period_New__c = accPeriod.get(bts.Accounting_Period_Formula__c);
                
            }
            
            
            
            system.debug('bts.BT_Status__c'+bts.BT_Status__c);
            system.debug('trigger.oldMap.get(bts.ID).BT_Status__c'+trigger.oldMap.get(bts.ID).BT_Status__c);
            if(bts.BT_Status__c != trigger.oldMap.get(bts.ID).BT_Status__c && bts.BT_Status__c == 'Closed' && bts.Bank_charges__c != 0){
                Id accountRecordType = Schema.Sobjecttype.Account.getRecordTypeInfosByDeveloperName().get('Client').getRecordTypeId();
                Id btAccountRT = accountMap.get(bts.Supplier__c).RecordTypeId;
                
                Transaction_Line_New__c DTTL = new Transaction_Line_New__c(
                    Accounting_Period_New__c  = bts.Accounting_Period_New__c,
                    Amount__c = BankCharge,
                    Bank_Transaction__c = bts.Id,
                    Business_Partner__c = bts.Supplier__c,
                    Date__c = bts.Date__c,
                    Description__c = 'Bank Charges',
                    GL_Account__c = glAccount.get('314500'),
                    RecordTypeId = Schema.Sobjecttype.Transaction_Line_New__c.getRecordTypeInfosByDeveloperName().get('Bank_Transaction_Applied').getRecordTypeId(),
                    Revenue_Type__c = 'Actual',
                    Short_Description__c  =  'Bank Charges',
                    Status__c ='Posted',
                    Type_Code__c = 'BC');
                createdTls.add(DTTL);
                
                Transaction_Line_New__c CTTL = new Transaction_Line_New__c(
                    Accounting_Period_New__c  = bts.Accounting_Period_New__c,
                    Amount__c = BankCharge * -1,
                    Bank_Transaction__c = bts.Id,
                    Business_Partner__c = bts.Supplier__c,
                    Date__c = bts.Date__c,
                    Description__c =  Descript ,
                    GL_Account__c =  bts.Bank_Account_New__c,
                    RecordTypeId = Schema.Sobjecttype.Transaction_Line_New__c.getRecordTypeInfosByDeveloperName().get('Bank_Transaction_Applied').getRecordTypeId(),
                    Revenue_Type__c = 'Actual',
                    Short_Description__c  = ShortDescript,
                    Status__c ='Posted',
                    Type_Code__c = 'BC');
                createdTls.add(CTTL);
            }
            System.debug(' bts ');
            System.debug(JSON.serializePretty(bts));
            If(bts.BT_Status__c != trigger.oldMap.get(bts.ID).BT_Status__c && bts.BT_Status__c == 'Forex difference' && bts.Unapplied__c != 0 && bts.Applied_amount__c != 0){
                
                system.debug('In Forex If');
                Id accountRecordType = Schema.Sobjecttype.Account.getRecordTypeInfosByDeveloperName().get('Client').getRecordTypeId();
                Id btAccountRT = accountMap.get(bts.Supplier__c).RecordTypeId;
                
                For (Id bankTransApp : appliedBts.keySet()) { 
                    
                    If(appliedBts.get(bankTransApp).Bank_Transaction__c == bts.Id){
                        
                        Decimal tlAmount = bts.Unapplied__c / (bts.Applied_amount__c) * appliedBts.get(bankTransApp).Amount__c;
                        system.debug('tlAmount---->'+tlAmount);
                        system.debug('bts.Unapplied__c---->'+bts.Unapplied__c);
                        system.debug('bts.Applied_amount__c---->'+bts.Applied_amount__c);
                        system.debug('appliedBts.get(bankTransApp).Amount__c---->'+appliedBts.get(bankTransApp).Amount__c);
                        
                        IF (bts.RecordTypeId == Schema.Sobjecttype.Bank_transactions__c.getRecordTypeInfosByDeveloperName().get('Payment').getRecordTypeId()){
                            Transaction_Line_New__c DTTL = new Transaction_Line_New__c(
                                Accounting_Period_New__c  = appliedBts.get(bankTransApp).Accounting_Period_New__c,
                                Amount__c = tlAmount, 
                                Bank_Transaction__c = bts.Id,
                                Business_Partner__c = bts.Supplier__c,
                                Date__c = appliedBts.get(bankTransApp).Date__c,
                                Description__c = 'Forex',
                                GL_Account__c = glAccount.get('240000'),
                                RecordTypeId = Schema.Sobjecttype.Transaction_Line_New__c.getRecordTypeInfosByDeveloperName().get('Bank_Transaction_Applied').getRecordTypeId(),
                                Revenue_Type__c = 'Actual',
                                Short_Description__c  =  'Forex',
                                Status__c ='Posted',
                                Type_Code__c = 'Forex');
                            createdTls.add(DTTL);
                            
                            Transaction_Line_New__c CBTL = new Transaction_Line_New__c(
                                Accounting_Period_New__c  = appliedBts.get(bankTransApp).Accounting_Period_New__c,
                                Amount__c = tlAmount * -1, 
                                Bank_Transaction__c = bts.Id,
                                Business_Partner__c = bts.Supplier__c,
                                Date__c = appliedBts.get(bankTransApp).Date__c,
                                Description__c = Descript,
                                GL_Account__c = bts.Bank_Account_New__c,
                                RecordTypeId = Schema.Sobjecttype.Transaction_Line_New__c.getRecordTypeInfosByDeveloperName().get('Bank_Transaction_Applied').getRecordTypeId(),
                                Revenue_Type__c = 'Actual',
                                Short_Description__c  =  ShortDescript,
                                Status__c ='Posted',
                                Type_Code__c = 'Forex');
                            createdTls.add(CBTL);
                            
                            
                            bts.Forex_total__c = bts.Unapplied__c;
                            bts.Post_Forex__c = TRUE; }
                        
                        IF (bts.RecordTypeId == Schema.Sobjecttype.Bank_transactions__c.getRecordTypeInfosByDeveloperName().get('Receipt').getRecordTypeId()){
                            Transaction_Line_New__c DTTL = new Transaction_Line_New__c(
                                Accounting_Period_New__c  = appliedBts.get(bankTransApp).Accounting_Period_New__c,
                                Amount__c = tlAmount * -1, 
                                Bank_Transaction__c = bts.Id,
                                Business_Partner__c = bts.Supplier__c,
                                Date__c = appliedBts.get(bankTransApp).Date__c,
                                Description__c = 'Forex',
                                GL_Account__c = glAccount.get('240000'),
                                RecordTypeId = Schema.Sobjecttype.Transaction_Line_New__c.getRecordTypeInfosByDeveloperName().get('Bank_Transaction_Applied').getRecordTypeId(),
                                Revenue_Type__c = 'Actual',
                                Short_Description__c  =  'Forex',
                                Status__c ='Posted',
                                Type_Code__c = 'Forex');
                            createdTls.add(DTTL);
                            
                            Transaction_Line_New__c CBTL = new Transaction_Line_New__c(
                                Accounting_Period_New__c  = appliedBts.get(bankTransApp).Accounting_Period_New__c,
                                Amount__c = tlAmount, 
                                Bank_Transaction__c = bts.Id,
                                Business_Partner__c = bts.Supplier__c,
                                Date__c = appliedBts.get(bankTransApp).Date__c,
                                Description__c = Descript,
                                GL_Account__c = bts.Bank_Account_New__c,
                                RecordTypeId = Schema.Sobjecttype.Transaction_Line_New__c.getRecordTypeInfosByDeveloperName().get('Bank_Transaction_Applied').getRecordTypeId(),
                                Revenue_Type__c = 'Actual',
                                Short_Description__c  = ShortDescript,
                                Status__c ='Posted',
                                Type_Code__c = 'Forex');
                            createdTls.add(CBTL);
                            
                            
                            bts.Forex_total__c = bts.Unapplied__c;
                            bts.Post_Forex__c = TRUE;
                        }
                        
                        
                    }
                }
                
            }
        }    
        
        insert createdTls;  
        
    }
    
    If(Trigger.IsUpdate && Trigger.IsAfter){}
    
    
}