trigger RollupsOnSupplierInvoice on Supplier_Invoice__c ( before update, before delete) {
   
    IF(Trigger.Isbefore && Trigger.Isdelete){
         List<CPA_Costing__c> CPACostings1 = new List<CPA_Costing__c>();
        for(Supplier_Invoice__c SIS : trigger.old) {
        
        List<CPA_Costing__c> CPACostings = [Select Cost_Category__C,Tax_Sub_Category__c,Amount_in_USD__c,Shipment_Order_Old__c,Supplier_Invoice__c from CPA_Costing__c where Supplier_Invoice__c =: SIS.ID];
            For(Integer i=0;CPACostings.size()>i;i++ ){ CPACostings[i].Invoice_amount_local_currency__c=0.00; CPACostings1.add(CPACostings[i]); }

        }
       
        
     
        
     update CPACostings1;   
        
    }

    If(Trigger.IsUpdate && Trigger.IsBefore){
        
      Set<Id> oldInvIds = new Set<Id>();
        
       for(Supplier_Invoice__c supplierInvoice : Trigger.new) {
        oldInvIds.add(supplierInvoice.Id);} 
        
        
        for(Supplier_Invoice__c supplierInvoice : Trigger.new){ 
        
        If(supplierInvoice.Roll_Up_Costings__c != trigger.oldMap.get(supplierInvoice.ID).Roll_Up_Costings__c && supplierInvoice.Roll_Up_Costings__c == TRUE){
            
            
             
     decimal OLDINVFCAdminFee = 0.0;         
     decimal OLDINVFCBankFee = 0.0;           
     decimal OLDINVFCFinanceFee = 0.0;         
     decimal OLDINVFCIORandImport = 0.0;         
     decimal OLDINVFCINsurance = 0.0;        
     decimal OLDINVFCInternationsldeliveryFee = 0.0;  
     decimal OLDINVFCMiselaneousFee = 0.0;        
     decimal OLDINVFCRechatgeTaxandDuties = 0.0;   
     decimal OLDINVFCTotClearence = 0.0;        
     decimal OLDINVFCTotCustomsBrokerage = 0.0;      
     decimal OLDINVFCTotHandlingCost = 0.0;       
     decimal OLDINVFCTotLicenseCost = 0.0;        
     decimal OLDINVFCTAXOther = 0.0;        
     decimal OLDINVFCTAXForex = 0.0;        
        
     decimal OLDINVACAdminFee = 0.0;         
     decimal OLDINVACBankFee = 0.0;         
     decimal OLDINVACFinanceFee = 0.0;         
     decimal OLDINVACIORandImport = 0.0;      
     decimal OLDINVACINsurance = 0.0;        
     decimal OLDINVACInternationsldeliveryFee = 0.0;  
     decimal OLDINVACMiselaneousFee = 0.0;      
     decimal OLDINVACRechatgeTaxandDuties = 0.0;    
     decimal OLDINVACTotClearence = 0.0;        
     decimal OLDINVACTotCustomsBrokerage = 0.0;      
     decimal OLDINVACTotHandlingCost = 0.0;       
     decimal OLDINVACTotLicenseCost = 0.0;        
     decimal OLDINVACTAXOther = 0.0;        
     decimal OLDINVACTAXForex = 0.0;
            
            
    List<AggregateResult> allOldInvCosts = [SELECT Sum(Invoice_amount_USD_New__c) invAmount, Sum(Amount_in_USD__c) fcAmount, 
                                             Cost_Category__c costCat, Tax_Sub_Category__c subcat
                                             FROM CPA_Costing__c
                                             Where Supplier_Invoice__C =:supplierInvoice.Id 
                                             And Shipment_Order_Old__c != null
                                             And Supplier_Invoice__C != null
                                             And RecordTypeID =: Schema.Sobjecttype.CPA_Costing__c.getRecordTypeInfosByDeveloperName().get('Display').getRecordTypeId()
                                             Group by Cost_Category__c,Tax_Sub_Category__c];
        
        
        for(AggregateResult a : allOldInvCosts){
            
            If(a.get('costCat') == 'Admin' && a.get('subcat') == NULL){  OLDINVFCAdminFee = (Decimal) a.get('fcAmount');   OLDINVACAdminFee = (Decimal) a.get('invAmount');}
            If(a.get('costCat') == 'Bank' && a.get('subcat') == NULL){  OLDINVFCBankFee = (Decimal) a.get('fcAmount');  OLDINVACBankFee = (Decimal) a.get('invAmount');}
            If(a.get('costCat') == 'Finance' && a.get('subcat') == NULL){  OLDINVFCFinanceFee = (Decimal) a.get('fcAmount');   OLDINVACFinanceFee = (Decimal) a.get('invAmount');}
            If(a.get('costCat') == 'IOR Cost' && a.get('subcat') == NULL){  OLDINVFCIORandImport = (Decimal) a.get('fcAmount');  OLDINVACIORandImport = (Decimal) a.get('invAmount');}
            If(a.get('costCat') == 'Insurance' && a.get('subcat') == NULL){  OLDINVFCINsurance = (Decimal) a.get('fcAmount');  OLDINVACINsurance = (Decimal) a.get('invAmount');}
            If(a.get('costCat') == 'International freight' && a.get('subcat') == NULL){  OLDINVFCInternationsldeliveryFee = (Decimal) a.get('fcAmount');  OLDINVACInternationsldeliveryFee = (Decimal) a.get('invAmount');}
            If(a.get('costCat') == 'Miscellaneous' && a.get('subcat') == NULL){ OLDINVFCMiselaneousFee = (Decimal) a.get('fcAmount');   OLDINVACMiselaneousFee = (Decimal) a.get('invAmount');}
            If(a.get('costCat') == 'Tax' && a.get('subcat') == NULL){  OLDINVFCRechatgeTaxandDuties = (Decimal) a.get('fcAmount'); OLDINVACRechatgeTaxandDuties = (Decimal) a.get('invAmount');}
            If(a.get('costCat') == 'Clearance' && a.get('subcat') == NULL){  OLDINVFCTotClearence = (Decimal) a.get('fcAmount');   OLDINVACTotClearence = (Decimal) a.get('invAmount');}
            If(a.get('costCat') == 'Brokerage' && a.get('subcat') == NULL){  OLDINVFCTotCustomsBrokerage = (Decimal) a.get('fcAmount');  OLDINVACTotCustomsBrokerage = (Decimal) a.get('invAmount');}
            If(a.get('costCat') == 'Handling' && a.get('subcat') == NULL){  OLDINVFCTotHandlingCost = (Decimal) a.get('fcAmount');   OLDINVACTotHandlingCost = (Decimal) a.get('invAmount');}
            If(a.get('costCat') == 'Licenses' && a.get('subcat') == NULL){  OLDINVFCTotLicenseCost = (Decimal) a.get('fcAmount');  OLDINVACTotLicenseCost = (Decimal) a.get('invAmount');}
            If(a.get('costCat') == 'Tax' && a.get('subcat') == 'Duties and Taxes Other'){  OLDINVFCTAXOther = (Decimal) a.get('fcAmount');   OLDINVACTAXOther = (Decimal) a.get('invAmount');}
            If(a.get('costCat') == 'Tax' && a.get('subcat') == 'Tax Forex Spread'){  OLDINVFCTAXForex = (Decimal) a.get('fcAmount');  OLDINVACTAXForex = (Decimal) a.get('invAmount');}    
            }	   
            
            
           supplierInvoice.FC_Admin_Fee__c= OLDINVFCAdminFee;
           supplierInvoice.FC_Bank_Fees__c= OLDINVFCBankFee;
           supplierInvoice.FC_Finance_Fee__c= OLDINVFCFinanceFee;
           supplierInvoice.Total_IOR_EOR__c= OLDINVFCIORandImport;
           supplierInvoice.FC_Insurance_Fee_USD__c= OLDINVFCINsurance;
           supplierInvoice.FC_International_Delivery_Fee__c= OLDINVFCInternationsldeliveryFee;
           supplierInvoice.FC_Miscellaneous_Fee__c= OLDINVFCMiselaneousFee;
           supplierInvoice.Total_Duties_and_Taxes__c= OLDINVFCRechatgeTaxandDuties;
           supplierInvoice.Total_Clearance_Costs__c=OLDINVFCTotClearence;
           supplierInvoice.Total_Brokerage_Costs__c=OLDINVFCTotCustomsBrokerage;
           supplierInvoice.Total_Handling_Costs__c= OLDINVFCTotHandlingCost;
           supplierInvoice.Total_License_Permits_Cost__c= OLDINVFCTotLicenseCost;
           supplierInvoice.FC_Duties_and_Taxes_Other__c= OLDINVFCTAXOther;
           supplierInvoice.FC_Tax_Forex_Spread__c= OLDINVFCTAXForex;
            
           supplierInvoice.Actual_Admin_Fee__c= OLDINVACAdminFee;
           supplierInvoice.Actual_Bank_Fees__c= OLDINVACBankFee;
           supplierInvoice.Actual_Brokerage_Costs__c=OLDINVACTotCustomsBrokerage;
           supplierInvoice.Actual_Clearance_Costs__c=OLDINVACTotClearence;
           supplierInvoice.Actual_Duties_and_Taxes__c=OLDINVACRechatgeTaxandDuties;
           supplierInvoice.Actual_Finance_Fee__c=OLDINVACFinanceFee;
           supplierInvoice.Actual_Handling_Costs__c=OLDINVACTotHandlingCost;
           supplierInvoice.Actual_Insurance_Fee_USD__c=OLDINVACINsurance;
           supplierInvoice.Actual_IOR_EOR_Cost__c=OLDINVACIORandImport;
           supplierInvoice.Actual_International_Delivery_Fee__c=OLDINVACInternationsldeliveryFee;
           supplierInvoice.Actual_Miscellaneous_Fee__c=OLDINVACMiselaneousFee;
           supplierInvoice.Actual_License_Permits_Cost__c=OLDINVACTotLicenseCost;
           supplierInvoice.Actual_Duties_and_Taxes_Other__c=OLDINVACTAXOther;
           supplierInvoice.Actual_Tax_Forex_Spread__c=OLDINVACTAXForex;
           supplierInvoice.Roll_Up_Costings__c = FALSE;
            
            
            
        
        
        }
        
        
        
    }

}
    
}