trigger CP_Default_Trigger on CPDefaults__c (after insert,after update, after delete) {
    
    if(Trigger.isDelete){
        populateFormulaFieldsOnAccount(Trigger.old, NULL);
    }else{
        populateFormulaFieldsOnAccount(Trigger.new, Trigger.old);
    }
    
    
    public static void populateFormulaFieldsOnAccount(List<CPDefaults__c> newList, List<CPDefaults__c> oldList){
        Set<Id> accToUpdate = new Set<Id>();
        Map<Id, List<CPDefaults__c>> accWithCPDefaults = new Map<Id, List<CPDefaults__c>>();
        for(Integer i = 0 ; newList.size() > i ; i++){
            if(newList.get(i).Client_Account__c != NULL){
                accToUpdate.add(newList.get(i).Client_Account__c);
            }
            if(oldList != NULL && oldList.get(i).Client_Account__c != NULL){
                accToUpdate.add(oldList.get(i).Client_Account__c);
            }
        }
        
        List<CPDefaults__c> cpDefaultList = [SELECT Id, Client_Account__c  FROM CPDefaults__c
                                             WHERE Client_Account__c IN :accToUpdate
                                             AND Order_Type__c = 'Final Quote'];
        
        for(CPDefaults__c cpDefault : cpDefaultList){
            if(accWithCPDefaults.containsKey(cpDefault.Client_Account__c)){
                accWithCPDefaults.get(cpDefault.Client_Account__c).add(cpDefault);
            }else{
                accWithCPDefaults.put(cpDefault.Client_Account__c, new List<CPDefaults__c>{cpDefault}); 
            }
        }                        
        
        List<Account> accList = [SELECT Id, Count_CPDefaults__c FROM Account
                                 WHERE Id IN :accToUpdate];
        for(Account acc : accList){
            if(accWithCPDefaults.containsKey(acc.Id)){
                // Part record found. below code will perform logics.
                List<CPDefaults__c> relatedCPDefault = accWithCPDefaults.get(acc.Id);
                
                
                Integer numberOfCPDefault = 0;
                
                for(CPDefaults__c cp : relatedCPDefault){
                    numberOfCPDefault++;
                }
                
                //Updating final information on SO
                acc.Count_CPDefaults__c  = numberOfCPDefault; 
            }else{
                //Reset The formula field to default. As no part record found
                acc.Count_CPDefaults__c  = 0;
                
            }
            
        }
        system.debug('accList ' + JSON.serializePretty(accList));
        update accList;  
    }
    
    
}