trigger CaseTrigger on Case (Before Insert,After insert,After update) {
    if(RecusrsionHandler.isCaseTriggerSkip) return;
    if(Trigger.isBefore && Trigger.isInsert) {
      for(Case cs : Trigger.New){
        if(cs.Reason == 'IT Hardware' || cs.Reason == 'IT Software')
        cs.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Tech Support').getRecordTypeId();
      }
    }
    if(Trigger.isAfter)
      JIRARestService.createJiraIssue(Trigger.newMap.keySet());
}