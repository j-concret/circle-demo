({
    parseCsv: function( cmp,cntId) {
        let self = this;
        self.makeServerCall(cmp,'saveRecord',{
            cntId : cntId
        },function(response){
            var toastEvent = $A.get("e.force:showToast");
            if(response.jobId){
                toastEvent.setParams({
                    mode: 'sticky',
                    message: 'This is a required message',
                    messageTemplate: 'Your request has been submitted. Click {0}  to track the progress.',
                    messageTemplateData: [{
                        url: '/changemgmt/monitorDeploymentsDetails.apexp?asyncId=' + response.jobId,
                        label: 'Deployment Status'
                    }]
                });
                toastEvent.fire();
            }else{
                 self.showToast('error','ERROR',response.error || 'Unknown error');
            }
        },function(err){
            cmp.set('v.showSpinner',false);
            self.showToast('error','ERROR',err);
        });
    }
    
    
})