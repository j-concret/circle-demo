({
    handleUploadFinished: function (cmp, event,helper) {
        // Get the list of uploaded files
        var uploadedFiles = event.getParam("files");
        console.log('documentId ',uploadedFiles[0].documentId);
        helper.parseCsv(cmp,uploadedFiles[0].documentId);
    }
})