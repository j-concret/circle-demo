({
    doInitHlpr: function(component, helper) {
        component.set("v.showSpinner", true);
        var params = {
            shipmentId: component.get("v.recordId")
        };
        this.makeServerCall(component, "getFinanceData", params, function(result) {
            if(result.status === 'OK') {
                if(result.appName == 'Zee'){
                    component.set("v.business", result.appName);
                }
                if(result.data.length> 0){
                    if(result.data[0].Customs_Clearance_Documents__r){
                        component.set("v.CustomsClearance_Documents_ID",result.data[0].Customs_Clearance_Documents__r[0].Id);        
                    }
                    component.set("v.TaxCalculation", result.data[0].Tax_Calculations__r);
                }
                
                
            }
            else {
                helper.showToast("error", "Error Occured Helper", result.msg);
            }
            component.set("v.showSpinner", false);
        });
    },
    activeSections: function(component, event){
        var selTab = component.get("v.selTabId");
        if(selTab == 'Tax Total'){
            var editFormGeneralInformation = component.find("editFormTaxInformation");
            $A.util.toggleClass(editFormGeneralInformation, "slds-hide");
            var viewFormGeneralInformation = component.find("viewFormTaxInformation");
            $A.util.toggleClass(viewFormGeneralInformation, "slds-hide");
        }else if(selTab == 'CCD'){
            var editFormGeneralInformation = component.find("editFormCCD");
            $A.util.toggleClass(editFormGeneralInformation, "slds-hide");
            var viewFormGeneralInformation = component.find("viewFormCCD");
            $A.util.toggleClass(viewFormGeneralInformation, "slds-hide");   
        }
    }
})