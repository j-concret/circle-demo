({
	 fetchPartsdata: function (cmp,event,helper) {
      var action = cmp.get("c.getpartdata");
          action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var data = response.getReturnValue();
                cmp.set('v.partsdata',data);
                console.log(data);
                 
            }
           
        });
        $A.enqueueAction(action);
     
     },
    UpdatePartsData: function (cmp,event,helper) {
      
      cmp.set("v.is_loading",true);
        var CPartl = cmp.get("v.partsdata");
        console.log('CPart_List',CPartl);
        
        var action = cmp.get("c.updatepartdata");
        action.setParams({"PPartl" : CPartl});
        
          action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                cmp.set("v.is_loading",false);
                var data = response.getReturnValue();
               cmp.find("toastCmp").showToastModel(data, "success");
                helper.fetchPartsdata(cmp,event,helper);
                }
           
        });
        $A.enqueueAction(action);
     
     },
   
    
    
})