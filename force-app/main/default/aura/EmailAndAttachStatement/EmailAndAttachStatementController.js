({
doInit : function(component, event, helper) {
    
    component.set("v.showLoadingSpinner", true);
    //var record = component.set("v.simpleRecord","v.recordId");
    //var penaltyFeild = component.get("v.simpleRecord.Penalty_Status__c");
    //console.log("Simple record Field### :" + JSON.stringify(penaltyFeild));
    let penaltyPromise = Promise.resolve(helper.promiseIsPenaltyApproved(component));
    penaltyPromise.then( $A.getCallback(
                            function (result) {
                                console.log("Penalty Result from Apex CTRL### :" + JSON.stringify(result));
                                component.set("v.disablePenalty", result);
                                component.set("v.showLoadingSpinner", false);
                            }
                        ),
                        $A.getCallback(
                                function (status) {
                                    component.set("v.showLoadingSpinner", false);
                                    component.find('notifLib').showToast({ "variant": "error",
                                                                            "message": "Unable to get Penalty Status! "+status.message
                                                                        });
                                }
                        )
    ).catch(function (error) {
        component.set("v.showLoadingSpinner", false);
        $A.reportError("Unable to initialise component!", error);
    });
        
    //Get contacts
    let contactPromise = Promise.resolve(helper.promiseGetRelatedContacts(component));
    contactPromise.then( $A.getCallback(
                            function (result) {
                                //let options = result.map(record=>({label:record.Name,value:record.Email}));
                                component.set("v.relatedContactList", result);
                            component.set("v.showLoadingSpinner", false);
                            }
                            

                        ),
                        $A.getCallback(
                            function (status) {
                                component.set("v.showLoadingSpinner", false);
                                component.find('notifLib').showToast({ "variant": "error",
                                                                        "message": "Unable to add Statement or contacts! "+status.message
                                                                    });
                            }
                        )
        ).catch(function (error) {
            component.set("v.showLoadingSpinner", false);
            $A.reportError("Unable to initialise component!", error);
        });
},
    
handleCCMe: function  (component, event){

    let isActive = event.getSource().get("v.value");

    if (isActive) {
        let action = component.get("c.getUserEmail");
        action.setCallback( this, 
                            function(response) {
                                let state = response.getState();
                                if (state === "SUCCESS") {
                                    component.set("v.toEmail", response.getReturnValue());
                            }
                        });
                        $A.enqueueAction(action);
    } else {
            component.set("v.toEmail", "");
    }
},

handlePenaltyCheck: function  (component, event){

    let isActive = event.getSource().get("v.value");
    if (isActive) {
            component.set("v.sendButtonVariant", "destructive");
            component.set("v.sendButtonLabel", "Send Penalty Invoice");
            
            component.set("v.previewOverFlowValue", "previewPenalty");
            component.set("v.previewOverFlowLabel", "Preview Penalty Invoice");
            
            component.set("v.saveOverFlowValue", "savePenalty");
            component.set("v.saveOverFlowLabel", "Save Penalty Invoice");

        } else {
            component.set("v.sendButtonVariant", "success");
            component.set("v.sendButtonLabel", "Send Statement");
            
            component.set("v.previewOverFlowValue", "previewStatement");
            component.set("v.previewOverFlowLabel", "Preview Statement");
            
            component.set("v.saveOverFlowValue", "saveStatement");
            component.set("v.saveOverFlowLabel", "Save Statement");
        }
    },

checkboxSelect: function(component, event, helper) {
    
},

handleSelectAll: function(component, event, helper) {

    let isActive = event.getSource().get("v.value");
    var getAllId  = component.find("checkBox");
    if(Array.isArray(getAllId)){
        if(isActive==true){
            for(var i = 0; i < getAllId.length; i++){
                component.find("checkBox")[i].set("v.value", true);
            }
        
        }else{
                for(var i = 0; i < getAllId.length; i++) {
                    component.find("checkBox")[i].set("v.value", false);
                }
        }
    
    } else if(!Array.isArray(getAllId)){
        if(isActive==true){
            component.find("checkBox").set("v.value", true);
            
        }else{
            component.find("checkBox").set("v.value", false);
        }    
    }
    
},
    
doSend : function(component, event, helper) {

    var penaltyTicked = component.get("v.isPenaltyChecked");
    component.set("v.showLoadingSpinner", true);
    
    // @@@@@@@@@@@@ Email big Promise
    let invUpdateEmailPromise = Promise.resolve(helper.promiseInvoiceUpdate(component));
    invUpdateEmailPromise.then( $A.getCallback(
                            function (result) {
                                console.log("Penalty Result from Apex CTRL### :" + JSON.stringify(result));
                                if(result === true){
                                    if(!penaltyTicked){
                                        let emailPromise = Promise.resolve(helper.promiseEmailStatement(component));
                                        emailPromise.then(
                                            $A.getCallback(function (result) {
                                                let isSent = result;
                                                if (!isSent) {
                                                    component.set("v.showLoadingSpinner", false);
                                                    component.find('notifLib').showToast({
                                                                                            "variant": "error",
                                                                                            "message": "Unable to email Statement!"
                                                                                        });
                                                }else {
                                                        component.set("v.showLoadingSpinner", false);
                                                        component.find('notifLib').showToast({
                                                                                                "variant": "success",
                                                                                                "message": "Statement emailed! "
                                                                                        });
                                                }
                                                
                                            }),
                                            $A.getCallback(function (status) {
                                                component.set("v.showLoadingSpinner", false);
                                                component.find('notifLib').showToast({
                                                                                        "variant": "error",
                                                                                        "message": "1. Unable to email Statement "+status.message
                                                                                    });
                                            })
                                        ).catch(function (error) {
                                            component.set("v.showLoadingSpinner", false);
                                            $A.reportError("2. Unable to email Statement!", error);
                                        });
                                        $A.get('e.force:refreshView').fire();    

                                    }else{
                                        
                                        let penaltyEmailPromise = Promise.resolve(helper.promiseEmailPenalty(component));
                                        penaltyEmailPromise.then(
                                                                    $A.getCallback(function (result) {
                                                                        let isSent = result;
                                                                        if (!isSent) {
                                                                            component.set("v.showLoadingSpinner", false);
                                                                            component.find('notifLib').showToast({
                                                                                                                    "variant": "error",
                                                                                                                    "message": "Unable to email Penalty Invoice!"
                                                                                                                });
                                                                        }else {
                                                                            component.set("v.showLoadingSpinner", false);
                                                                                component.find('notifLib').showToast({
                                                                                                                        "variant": "success",
                                                                                                                        "message": "Penalty Invoice emailed! "
                                                                                                                    });
                                                                        }
                    
                                                                    }),
                                                                    $A.getCallback(function (status) {
                                                                        component.set("v.showLoadingSpinner", false);
                                                                        component.find('notifLib').showToast({
                                                                                                                "variant": "error",
                                                                                                                "message": "1. Unable to email Penalty Invoice "+status.message
                                                                                                            });
                                                                    })
                                        ).catch(function (error) {
                                        component.set("v.showLoadingSpinner", false);
                                        $A.reportError("Unable to email Penalty Invoice!", error);
                                        });
                                        $A.get('e.force:refreshView').fire();

                                    }
                                }
                                else if(result === false){
                                    component.set("v.showLoadingSpinner", false);
                                    component.find('notifLib').showToast({
                                                                            "variant": "warning",
                                                                            "message": "Unable to Update Account Invoices"
                                                                        });
                                }
                            }
                        ),
                        $A.getCallback(
                                function (status) {
                                    component.set("v.showLoadingSpinner", false);
                                    component.find('notifLib').showToast({ "variant": "error",
                                                                            "message": "1. Unable to Update Account Invoices! "+status.message
                                                                        });
                                }
                        )
    ).catch(function (error) {
        component.set("v.showLoadingSpinner", false);
        $A.reportError("2. Unable to Update Account Invoices!", error);
    });
},

handleRecordChange : function(component, event, helper) {

    var eventParams = event.getParams();
    
    if(eventParams.changeType === "CHANGED") {
        // get the fields that are changed for this record
        //var changedFields = eventParams.changedFields;
        //console.log('Fields that are changed: ' + JSON.stringify(changedFields));
        //component.find("recordLoader").reloadRecord();
        var penaltyFeild = component.get("v.simpleRecord.Penalty_Status__c");
        var outstandPenalty = component.get("v.simpleRecord.Penalty_Amount_Due__c");
        console.log('Penalty Status inside rec update : ' + JSON.stringify(penaltyFeild));
        console.log('Penalty Due inside rec update : ' + JSON.stringify(outstandPenalty));

        var recUpdate = $A.get("e.c:recordUpdated");
        recUpdate.fire();

        /*if(penaltyFeild === 'Approved' && outstandPenalty > 0 ){
            component.set("v.disablePenalty", true);
        }*/
    }

},

openViewStatementModal: function(component, event, helper) {
    
    component.set("v.showLoadingSpinner", true);
    var selectedValue = event.getParam("value");
    
    //////////Big Promise Start////////////////////////
    let invUpdatePromise = Promise.resolve(helper.promiseInvoiceUpdate(component));
    invUpdatePromise.then( $A.getCallback(
                            function (result) {
                                console.log("Results from Apex CTRL updateInvoices method###:" + JSON.stringify(result));
                                
                                if(result === true){
                                    if(selectedValue === "previewStatement"){
                                        component.set("v.showLoadingSpinner", false);
                                        component.set("v.isOpenViewStatement", true);
                                    
                                    }else if(selectedValue === "saveStatement"){
                                        
                                        let savePromise = Promise.resolve( helper.promiseSaveStatement(component));
                                        savePromise.then(
                                            $A.getCallback(function (result) {
                                                let isSoSaved = result;
                                                if (!isSoSaved) {
                                                    component.set("v.showLoadingSpinner", false);
                                                    component.find('notifLib').showToast({
                                                                                            "variant": "error",
                                                                                            "message": "Unable to save Statement!"
                                                                                            });
                                                
                                                }else {
                                                    component.set("v.showLoadingSpinner", false);
                                                    component.find('notifLib').showToast({
                                                                                            "variant": "success",
                                                                                            "message": "Statement Saved! "
                                                                                        });
                                                }
                                                
                                            }),
                                            $A.getCallback(function (status) {
                                                component.set("v.showLoadingSpinner", false);
                                                component.find('notifLib').showToast({
                                                    "variant": "error",
                                                    "message": "Unable to save Statement! "+status.message
                                                });
                                            })
                                        ).catch(function (error) {
                                            component.set("v.showLoadingSpinner", false);
                                            $A.reportError("Unable to save Statement!", error);
                                        });
                                        $A.get('e.force:refreshView').fire();
                                    }
                                    else if(selectedValue === "previewPenalty"){
                                        component.set("v.showLoadingSpinner", false);
                                        component.set("v.isOpenViewPenaltyInvoice", true);
                                    }
                                    else if(selectedValue === "savePenalty"){
                                        let savePromise = Promise.resolve(helper.promiseSavePenalty(component));
                                        savePromise.then(
                                            $A.getCallback(function (result) {
                                                let isSoSaved = result;
                                                if (!isSoSaved) {
                                                    component.set("v.showLoadingSpinner", false);
                                                    component.find('notifLib').showToast({
                                                                                            "variant": "error",
                                                                                            "message": "Unable to save Penalty Invoice!"
                                                                                            });
                                                }else {
                                                    component.set("v.showLoadingSpinner", false);
                                                    component.find('notifLib').showToast({
                                                                                            "variant": "success",
                                                                                            "message": "Penalty Invoice Saved! "
                                                                                        });
                                                }
                                                
                                            }),
                                            $A.getCallback(function (status) {
                                                component.set("v.showLoadingSpinner", false);
                                                component.find('notifLib').showToast({
                                                    "variant": "error",
                                                    "message": "Unable to save Penalty Invoice!! "+status.message
                                                });
                                            })
                                        ).catch(function (error) {
                                            component.set("v.showLoadingSpinner", false);
                                            $A.reportError("Unable to save Penalty Invoice!!", error);
                                        });
                                        $A.get('e.force:refreshView').fire();
                                            

                                    }
                                    
                                }
                                else if(result === false){
                                    component.set("v.showLoadingSpinner", false);
                                    component.find('notifLib').showToast({
                                                                            "variant": "warning",
                                                                            "message": "Unable to Update Account Invoices"
                                                                        });

                                }
                                
                            }
                        ),
                        $A.getCallback(
                                function (status) {
                                    component.set("v.showLoadingSpinner", false);
                                    component.find('notifLib').showToast({ "variant": "error",
                                                                            "message": "1. Unable to Update Account Invoices "+status.message
                                                                        });
                                }
                        )
    ).catch(function (error) {
        component.set("v.showLoadingSpinner", false);
        $A.reportError("2. Unable to Update Account Invoices ", error);
    });
    //////////Big Promise End////////////////////////
    
},

closeViewStatementModal: function(component, event, helper) {
    // for Hide/Close Model,set the "isOpen" attribute to "Fasle"  
    component.set("v.isOpenViewStatement", false);
    component.set("v.isOpenViewPenaltyInvoice", false);
},  
})