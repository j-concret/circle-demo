({
promiseGetRelatedContacts : function(component) {
    return this.createPromise(component, "c.getRelatedContacts", {clientId: component.get("v.recordId")});
},

promiseIsPenaltyApproved: function(component) {
    return this.createPromise(component, "c.isPenaltyApproved", {accId: component.get("v.recordId")});
},


promiseEmailStatement : function(component) {
    //CC Email Processing
    var ccEmailTo = component.get('v.toEmail');
    if(ccEmailTo !=""){
        var emailTo = component.get('v.toEmail').split(";");
        console.log("Inside if Email### :" + JSON.stringify(emailTo));
        } else{
            var emailTo = component.get('v.toEmail');
            console.log("Inside else Email### :" + JSON.stringify(emailTo));
        }

        //Email To Processing
        var emaAddresses = [];
        var getEmaAddresses = component.find("checkBox");
        if(Array.isArray(getEmaAddresses)){
            for (var i = 0; i < getEmaAddresses.length; i++) {
                
                if (getEmaAddresses[i].get("v.value") == true) {
                    emaAddresses.push(getEmaAddresses[i].get("v.text"));
                    }
                }
            } 
            else if(!Array.isArray(getEmaAddresses)){
                if (getEmaAddresses.get("v.value") == true) {
                    emaAddresses.push(getEmaAddresses.get("v.text"));
                }
                
        }
        
        if(emaAddresses.length > 0){
            return this.createPromise(component, "c.sendStatement", {
            customerId: component.get("v.recordId"),
            toAddress: emaAddresses,
            ccAddress : emailTo
            
            });
        } else{
            component.find('notifLib').showToast({
                "variant": "warning",
                "message": "Please select at least one Contact"
            });
            component.set("v.showLoadingSpinner", false);
        }
    },

promiseEmailPenalty : function(component) {
        //CC Email Processing
        var ccEmailTo = component.get('v.toEmail');
        if(ccEmailTo !=""){
            var emailTo = component.get('v.toEmail').split(";");
            console.log("Inside if Email### :" + JSON.stringify(emailTo));
        } else{
            var emailTo = component.get('v.toEmail');
            console.log("Inside else Email### :" + JSON.stringify(emailTo));
        }
        
        //Email To Processing
        var emaAddresses = [];
        var getEmaAddresses = component.find("checkBox");
        if(Array.isArray(getEmaAddresses)){
            for (var i = 0; i < getEmaAddresses.length; i++) {
                
                if (getEmaAddresses[i].get("v.value") == true) {
                    emaAddresses.push(getEmaAddresses[i].get("v.text"));
                    }
                }
            } 
            else if(!Array.isArray(getEmaAddresses)){
                if (getEmaAddresses.get("v.value") == true) {
                    emaAddresses.push(getEmaAddresses.get("v.text"));
                }
                
        }
        
        if(emaAddresses.length > 0){
            return this.createPromise(component, "c.sendPenalty", {
            customerId: component.get("v.recordId"),
            toAddress: emaAddresses,
            ccAddress : emailTo
            
            });
        } else{
            component.find('notifLib').showToast({
                "variant": "warning",
                "message": "Please select at least one Contact"
            });
            component.set("v.showLoadingSpinner", false);
        }
    },
    
promiseSaveStatement : function(component) {

        return this.createPromise(component, "c.saveStatement", {
        accId: component.get("v.recordId")
            
            });
    },

promiseSavePenalty : function(component) {

        return this.createPromise(component, "c.savePenalty", {
        accId: component.get("v.recordId")
            
            });
    },

    promiseInvoiceUpdate: function(component) {
        return this.createPromise(component, "c.updateInvoices", {cliendId: component.get("v.recordId")});
    },

createPromise : function(component, name, params) {
        return new Promise(function(resolve, reject) {
            let action = component.get(name);
            if (params) {
                action.setParams(params);
            }
            action.setCallback(this, function(response) {
                let state = response.getState();
                if (component.isValid() && state === "SUCCESS") {
                    let result = response.getReturnValue();
                    resolve(result);
                }
                else {
                    reject(response.getError());
                }
            });
            $A.enqueueAction(action);
        });
    },

updateInvoices : function(component, event) {
    
    component.set("v.showLoadingSpinner", true);
    let action = component.get("c.updateInvoices");
    action.setParams({cliendId: component.get("v.recordId")});
    action.setCallback(this, function(response) {
        let state = response.getState();
        if (component.isValid() && state === "SUCCESS") {
            console.log("Results from Apex CTRL updateInvoices method###: " + JSON.stringify(response.getReturnValue()));
            component.set("v.areInvRecUpdated", response.getReturnValue());
            
        }
        else {
            
            var error = response.getError();
            component.find('notifLib').showToast({ "variant": "success",
                                                    "message": "Error while quirying Invoices: " + error
                                                });
        }
    });
    $A.enqueueAction(action);

},
})