({
    makeServerCall: function(component, method, params, successCallback, errorCallback) {
        var action = component.get(('c.' + method));
        if(!action) return;
        if(Object.keys(params)) {
            action.setParams(params);
        }
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === 'SUCCESS') {
                successCallback(response.getReturnValue());
            }
            else if(state === 'ERROR') {
                if(errorCallback) {
                    errorCallback(response.getError() ? response.getError()[0].message : 'Unknown Error Occurred!');
                }
            }
                else if(response.getState() === 'INCOMPLETE') {
                    if(errorCallback) {
                        errorCallback('Server could not be reached. Check your internet connection.');
                    }
                }
                    else {
                        if(errorCallback) {
                            errorCallback('Unknown error');
                        }
                    }
        });
        $A.enqueueAction(action);
    },
    showToast: function(type,title,msg)
    {
        var toastEvent = $A.get('e.force:showToast');
        toastEvent.setParams({
            'title': title,
            'type': type,
            'message': msg
        });
        toastEvent.fire();
    }
});