({
  doInit : function(cmp, event, helper) {
    helper.renderCmp(cmp);
  },
  handleClick:function(cmp, event, helper) {
    helper.navigateToPage(cmp,event);
  },
  onuploadfinishedHandler : function(cmp, event, helper) {
    try{
    var files = event.getParam("files");
    cmp.set('v.files',files);
    cmp.set('v.showPanel',true);
    }catch(e){
      console.log(e.message);
    }
  },
  downloadDoc : function(cmp,event,helper){
    var block = cmp.get('v.block');
    var hiddenElement = document.createElement('a');
    hiddenElement.href = block.url;
    hiddenElement.target = '_self';
    hiddenElement.download = block.docName;
    document.body.appendChild(hiddenElement);
    hiddenElement.click();
  }
});