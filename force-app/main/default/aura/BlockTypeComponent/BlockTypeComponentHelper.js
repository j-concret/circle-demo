({
  config:{
    'Part Sub-Category Table':{
      navigation:true,
      attributes:[{
        attr_name:'c__soId',
        api_name:'Shipment_Order__c'
      },
      {
        attr_name:'c__tskId',
        api_name:'Id'
      }],
      pageReference:{
        type: 'standard__component',
        attributes: {
          componentName: 'c__PartSubCategoryTable'
        },
        state:{}
      }
    },
    'Add Pick-up Address':{
      navigation:true,
      attributes:[{
        attr_name:'c__soId',
        api_name:'Shipment_Order__c'
      },
      {
        attr_name:'c__type',
        api_name:'Pickup',
        from:'static'
      }],
      pageReference:{
        type: 'standard__component',
        attributes: {
          componentName: 'c__AddPickupAndFinalDeliveryAdd'
        },
        state:{}
      }
    },
    'Add Final Delivery Addresses':{
      navigation:true,
      attributes:[{
        attr_name:'c__soId',
        api_name:'Shipment_Order__c'
      },
                  {
                      attr_name:'c__type',
                      api_name:'Final',
                      from:'static'
                  }],
      pageReference:{
        type: 'standard__component',
        attributes: {
          componentName: 'c__AddPickupAndFinalDeliveryAdd'
        },
        state:{}
      }
    },
    'Generate Clearance Letter':{
      navigation:true,
      attributes:[{
        attr_name:'c__soId',
        api_name:'Shipment_Order__c'
      },
                 {
                      attr_name:'c__type',
                      api_name:'Clearance Letter',
                      from:'static'
                  }],
      pageReference:{
        type: 'standard__component',
        attributes: {
          componentName: 'c__GenerateDocument'
        },
        state:{}
      }
    },
    'Proof Of Valuation':{
      navigation:true,
      attributes:[{
        attr_name:'c__soId',
        api_name:'Shipment_Order__c'
      },
                 {
                      attr_name:'c__type',
                      api_name:'Proof Of Valuation',
                      from:'static'
                  }],
      pageReference:{
        type: 'standard__component',
        attributes: {
          componentName: 'c__GenerateDocument'
        },
        state:{}
      }
    },
    'Add Packages':{
      navigation:true,
      defaultFieldValues:[
        {
          attr_name:'Shipment_Order__c',
          api_name:'Shipment_Order__c'
        },
        {
          attr_name:'Freight__c',
          api_name:'Freight__c'
        }
      ],
      pageReference:{
        type: 'standard__objectPage',
        attributes: {
          actionName: 'new',
          objectApiName: 'Shipment_Order_Package__c'
        },
        state:{}
      }
    },
    'Pre-inspection Responsibility & Instruction':{
      navigation:true,
      attributes:[{
        attr_name:'c__tskId',
        api_name:'Id'
      }],
      pageReference:{
        type: 'standard__component',
        attributes: {
          componentName: 'c__PreInspectionInstruction'
        },
        state:{}
      }
    },
    'Manufacturer Address Table':{
      navigation:false,
    },
    'Generate CI':{
        navigation:true,
        attributes:[{
            attr_name:'c__soId',
            
            api_name:'Shipment_Order__c',
        },
                    {
                        attr_name:'c__newSOPage',
                       
                    }],
        pageReference:{
        type: 'standard__component',
        attributes: {
          componentName: 'c__tecexCIComponent_V2'
        },
        state:{
             
        }
      }
    },
    'Upload Document':{
      navigation:false,
    },
    'Download Document':{
      navigation:false,
    },
    'Generate AWB':{
      navigation:true,
      attributes:[{
        attr_name:'c__soId',
        api_name:'Shipment_Order__c'
      }],
      pageReference:{
        type: 'standard__component',
        attributes: {
          componentName: 'c__tecexFreightComponent'
        },
        state:{}
      }
    },
    'Carrier Choice and AWB Instruction (No Notify Party)​':{
      navigation:true,
      attributes:[{
        attr_name:'c__tskId',
        api_name:'Id'
      }],
      pageReference:{
        type: 'standard__component',
        attributes: {
          componentName: 'c__CarrierChoiceInstruction'
        },
        state:{}
      }
    },
    'Carrier Choice and AWB Instruction (With Notify Party)':{
      navigation:true,
      attributes:[{
        attr_name:'c__tskId',
        api_name:'Id'
      }],
      pageReference:{
        type: 'standard__component',
        attributes: {
          componentName: 'c__CarrierChoiceInstructionWithNotify'
        },
        state:{}
      }
    },
    'Battery PI Numbers Table':{
      navigation:true,
      attributes:[{
        attr_name:'c__soId',
        api_name:'Shipment_Order__c'
      }],
      pageReference:{
        type: 'standard__component',
        attributes: {
          componentName: 'c__BatteryPINumber'
        },
        state:{}
      }
    },
    'Compliance View Link':{
      navigation:true,
      attributes:[{
        attr_name:'c__soId',
        api_name:'Shipment_Order__c'
      }],
      pageReference:{
        type: 'standard__component',
        attributes: {
          componentName: 'c__ComplianceShipment'
        },
        state:{}
      }
    },
    'Add Part Weight':{
        navigation:true,
        attributes:[{
            attr_name:'c__soId',
            
            api_name:'Shipment_Order__c',
        }],
        pageReference:{
        type: 'standard__component',
        attributes: {
          componentName: 'c__EnterPartWeight'
        },
        state:{}
      }
    },
'Capture Data Center Details':{
      navigation:true,
      attributes:[{
        attr_name:'c__soId',
        api_name:'Shipment_Order__c'
      },
      {
        attr_name:'c__componentType',     
    }],
      pageReference:{
        type: 'standard__component',
        attributes: {
          componentName: 'c__DataCenterAndFTZ'
        },
        state:{}
      }
    },
    'Capture FTZ':{
      navigation:true,
      attributes:[{
        attr_name:'c__soId',
        api_name:'Shipment_Order__c'
      },
      {
        attr_name:'c__componentType',    
    }],
      pageReference:{
        type: 'standard__component',
        attributes: {
          componentName: 'c__DataCenterAndFTZ'
        },
        state:{}
      }
    },
    'Capture Pickup Preference':{
      navigation:true,
      attributes:[{
        attr_name:'c__soId',
        api_name:'Shipment_Order__c'
      },
      {
        attr_name:'c__tskId',
        api_name:'Id'
      }],
      pageReference:{
        type: 'standard__component',
        attributes: {
          componentName: 'c__PickupPreference'
        },
        state:{}
      }
    },
    'NL Product BO':{
      navigation:true,
      attributes:[{
        attr_name:'c__tskId',
        api_name:'Id'
      }],
      pageReference:{
        type: 'standard__component',
        attributes: {
          componentName: 'c__NLProductBO'
        },
        state:{}
      }
    },
    'Add Serial Number':{
      navigation:true,
      attributes:[{
        attr_name:'c__soId',
        api_name:'Shipment_Order__c'
      }],
      pageReference:{
        type: 'standard__component',
        attributes: {
          componentName: 'c__AddSerialNumber'
        },
        state:{}
      }
    },
  },
  renderCmp: function(cmp){
    var block = cmp.get('v.block');
    var cmpName = block.cmpName;
    var dynamicCmpName;
    var dynamicAttrs;
    if(this.config[cmpName]){

      var obj = this.config[cmpName];
      if(obj.navigation){
        dynamicCmpName = 'lightning:button';
        dynamicAttrs = {
          'aura:id': 'cmpBTN',
          'label': cmpName,
          'name':cmpName,
          'disabled':cmp.get('v.inActive'),
          'onclick': cmp.getReference('c.handleClick')
        };

      }else if(cmpName == 'Upload Document'){
        dynamicCmpName = 'lightning:fileUpload';
        dynamicAttrs = {
          'aura:id': 'cmpBTN',
          'label': 'Upload Document',
          'fileFieldName':'IsTaskDoc_fileupload__c',
          'fileFieldValue':'Yes',
          'name':'uploadDoc',
          'disabled':cmp.get('v.inActive'),
          'multiple' : true,
          'recordId' : block.sourceId,
          'onuploadfinished': cmp.getReference('c.onuploadfinishedHandler')
        };
      }else if(cmpName == 'Download Document'){
        dynamicCmpName = 'lightning:button';
        dynamicAttrs = {
          'aura:id': 'cmpBTN',
          'label': cmpName,
          'disabled':(cmp.get('v.inActive') || !block.url),
          'name':cmpName,
          'onclick': cmp.getReference('c.downloadDoc')
        };
      }
      else{
        return;
      }

      $A.createComponent(
        dynamicCmpName,dynamicAttrs,
        function(newButton, status, errorMessage){
          //Add the new button to the body array
          if (status === 'SUCCESS') {
            var body = cmp.get('v.firstPanel');
            body.push(newButton);
            cmp.set('v.firstPanel', body);
          }
          else if (status === 'INCOMPLETE') {
            console.log('No response from server or client is offline.');
            // Show offline error
          }
          else if (status === 'ERROR') {
            console.log('Error: ' + errorMessage);
            // Show error message
          }
        }
      );
    }

  },
  navigateToPage: function(cmp,event){
    var tsk = cmp.get('v.task');
    var cmpName = event.getSource().get('v.name');
    var obj = this.config[cmpName];
    var pageRef = obj.pageReference;

    if(obj.defaultFieldValues){
      var defaultFieldValues = {};
      var freight = cmp.get('v.freight');
      for(var q=0;q<obj.defaultFieldValues.length;q++){
        if(obj.defaultFieldValues[q].attr_name == 'Freight__c'){
          if(freight)
            defaultFieldValues[obj.defaultFieldValues[q].attr_name] = freight.Id;
        }
        else
          defaultFieldValues[obj.defaultFieldValues[q].attr_name] = tsk[obj.defaultFieldValues[q].api_name];
      };
      pageRef.state.defaultFieldValues = cmp.find('pageRefUtils').encodeDefaultFieldValues(defaultFieldValues);
    }

    if(obj.attributes){
      for(var i=0;i<obj.attributes.length;i++){
        pageRef.state[obj.attributes[i].attr_name] = obj.attributes[i].from && obj.attributes[i].from == 'static' ? obj.attributes[i].api_name : tsk[obj.attributes[i].api_name];
      }
        if(cmpName === 'Generate CI'){
            pageRef.state[obj.attributes[1].attr_name] = true;
        }
        if(cmpName === 'Capture Data Center Details'){
          pageRef.state[obj.attributes[1].attr_name] = 'Data Center';
        }
        if(cmpName === 'Capture FTZ'){
        pageRef.state[obj.attributes[1].attr_name] = 'FTZ';
        }
    }
    cmp.find('navService').navigate(pageRef);
  },
  showToast : function(title,type,msg) {
    var toastEvent = $A.get('e.force:showToast');
    toastEvent.setParams({
      'title': title,
      'type' :type,
      'message': msg
    });
    toastEvent.fire();
  }
});