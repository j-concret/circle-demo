({
  getSoTasks : function(cmp,event,isRender) {
    var self = this;
    cmp.set('v.showSpinner',true);
    self.serverCall(cmp,'getTasks',{
      soId:cmp.get('v.recordId'),
      displayedTo : cmp.get('v.displayedTo'),
      assignedToId : cmp.get('v.assignedToId'),
      hiddenTask : JSON.stringify(cmp.get('v.hiddenTask')),
    },function(response){
      cmp.set('v.showSpinner',false);
      cmp.set('v.categories',response.categories);
      cmp.set('v.shipmentOrder',response.so);
      if(isRender){
          //console.log('component Name',event.getSource().get("v.name"));
        cmp.set('v.tecexPersons',response.tecexPersons);
      }
    },function(err){
      self.showToast('error','ERROR',err);
    });
  },
  showToast : function(type, title , msg) {
    var toastEvent = $A.get('e.force:showToast');
    toastEvent.setParams({
      'type':type,
      'title': title ,
      'message': msg
    });
    toastEvent.fire();
  },
  redirectToObject : function(cmp,recId) {
    var pageRef = {
      type: 'standard__recordPage',
      attributes: {
        recordId: recId,
        objectApiName: 'Shipment_Order__c',
        actionName: 'view'
      }
    };
    cmp.find('navService').navigate(pageRef);
  }
});