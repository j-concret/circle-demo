({
    doInit: function(component, event, helper) {
        if(!component.get("v.newSOPage")){
            var pageReference = component.get('v.pageReference');
            component.set('v.recordId', pageReference.state.c__recId);
            var workspaceAPI = component.find('workspace');
            workspaceAPI.getAllTabInfo().then(function(tabs){
                var focusedTabId;
                for(var i=0;i<tabs.length;i++){
                    if(tabs[i].pageReference.attributes.componentName && tabs[i].pageReference.attributes.componentName.includes('MTKanbanView')){
                        focusedTabId = tabs[i].tabId;
                        break;
                    }
                }
                workspaceAPI.setTabLabel({
                    tabId: focusedTabId,
                    label: ' Tasks' //set label you want to set
                });
                workspaceAPI.setTabIcon({
                    tabId: focusedTabId,
                    icon: 'utility:task', //set icon you want to set
                    iconAlt: 'Tasks' //set label tooltip you want to set
                });
            });
        }
        helper.getSoTasks(component,event,true);
    },
    changeHandler: function(component,event,helper){
        helper.getSoTasks(component,event, event.getSource().get("v.name") == 'select1'?true:false);
    },
    redirectToRelated: function(cmp, event, helper) {
        var selectedItem = event.currentTarget;// this will give current element
        var recId = selectedItem.dataset.redirectid;
        helper.redirectToObject(cmp,recId);
    },
});