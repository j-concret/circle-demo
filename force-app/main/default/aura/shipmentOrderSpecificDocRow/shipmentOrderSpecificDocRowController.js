({
  downloadPDFCtrl: function(component, event, helper) {
    var rowDetail = component.get('v.rowDetail');
    var hiddenElement = document.createElement('a');

    if(rowDetail.attachment.id) {
   //   hiddenElement.href = 'sfc/servlet.shepherd/document/download/' + rowDetail.attachment.id+'?operationContext=S1';
        hiddenElement.href ='https://tecex.lightning.force.com/sfc/servlet.shepherd/document/download/'+ rowDetail.attachment.id+'?operationContext=S1';
    }
    hiddenElement.target = '_self';
    hiddenElement.download = rowDetail.attachment.fileName;
    document.body.appendChild(hiddenElement);
    hiddenElement.click();

  },
  handleUploadFinished: function(component, event, helper) {
    var uploadedFiles = event.getParam('files');
    var rowDetail = component.get('v.rowDetail');
    if(uploadedFiles[0]) {
      rowDetail['attachment'] = {id: uploadedFiles[0].documentId, fileName: uploadedFiles[0].name};
      rowDetail['availableIcon'] = 'action:approval';
      component.set('v.rowDetail', rowDetail);
    }
  }
})