/**
 * Created by Khonology on 2019-04-22.
 */
({
    MAX_FILE_SIZE: 4500000, //Max file size 4.5 MB
    CHUNK_SIZE: 750000,      //Chunk Max size 750Kb

    uploadHelper: function(component, event) {
        // start/show the loading spinner
        component.set("v.showLoadingSpinner", true);
        // get the selected files using aura:id [return array of files]
        let fileInput = component.find("fileId").get("v.files");
        // get the first file using array index[0]
        let file = fileInput[0];
        let self = this;
        // check the selected file size, if select file size greater than MAX_FILE_SIZE,
        // then show a alert msg to user,hide the loading spinner and return from function
        if (file.size > self.MAX_FILE_SIZE) {
            component.set("v.showLoadingSpinner", false);
            component.set("v.fileName", 'Alert : File size cannot exceed ' + self.MAX_FILE_SIZE + ' bytes.\n' + ' Selected file size: ' + file.size);
            return;
        }

        // create a FileReader object
        let objFileReader = new FileReader();
        // set onload function of FileReader object
        objFileReader.onload = $A.getCallback(function() {
            let fileContents = objFileReader.result;
            let base64 = 'base64,';
            let dataStart = fileContents.indexOf(base64) + base64.length;

            fileContents = fileContents.substring(dataStart);
            // call the uploadProcess method
            self.uploadProcess(component, file, fileContents);
        });

        objFileReader.readAsDataURL(file);
    },

    uploadProcess: function(component, file, fileContents) {

        let startPosition = 0;

        let endPosition = Math.min(fileContents.length, startPosition + this.CHUNK_SIZE);

        // start with the initial chunk, and set the attachId(last parameter)is null in begin
        this.uploadInChunk(component, file, fileContents, startPosition, endPosition, '');
    },


    uploadInChunk: function(component, file, fileContents, startPosition, endPosition, attachId) {
        // call the apex method 'saveChunk'
        let getchunk = fileContents.substring(startPosition, endPosition);
        let action = component.get("c.saveChunk");
        action.setParams({
            parentId: component.get("v.recordId"),
            fileName: file.name,
            base64Data: encodeURIComponent(getchunk),
            contentType: file.type,
            fileId: attachId
        });

        // set call back
        action.setCallback(this, function(response) {

            attachId = response.getReturnValue();
            let state = response.getState();
            if (state === "SUCCESS") {

                startPosition = endPosition;
                endPosition = Math.min(fileContents.length, startPosition + this.CHUNK_SIZE);

                if (startPosition < endPosition) {
                    this.uploadInChunk(component, file, fileContents, startPosition, endPosition, attachId);
                } else {
                    alert('your File is uploaded successfully');
                    component.set("v.showLoadingSpinner", false);
                }
            } else if (state === "INCOMPLETE") {
                alert("From server: " + response.getReturnValue());
            } else if (state === "ERROR") {
                let errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },

    promiseGetRelatedContacts : function(component) {
        return this.createPromise(component, "c.getRelatedContacts", {
            clientId: component.get("v.recordId")
        });
    },

    promiseGetStatement : function(component) {
        return this.createPromise(component, "c.getStatement", {
            clientId: component.get("v.recordId"),
            customerName: component.get("v.simpleRecord.Name"),
            statementType : "General"
        });
    },

    promiseGenerateStatement : function(component) {
        return this.createPromise(component, "c.generateStatement", {
            clientId: component.get("v.recordId"),
            customerName: component.get("v.simpleRecord.Name"),
            statementType : "General"
        });
    },

    promiseEmailStatement : function(component) {
        return this.createPromise(component, "c.sendStatement", {
            customerId: component.get("v.recordId"),
            toAddress: component.get("v.selectedRecipients"),
            ccAddress : component.get("v.ccEmail"),
            statementId: component.get("v.statementId")
        });
    },

    createPromise : function(component, name, params) {
        return new Promise(function(resolve, reject) {
            let action = component.get(name);
            if (params) {
                action.setParams(params);
            }
            action.setCallback(this, function(response) {
                let state = response.getState();
                if (component.isValid() && state === "SUCCESS") {
                    let result = response.getReturnValue();
                    resolve(result);
                }
                else {
                    reject(response.getError());
                }
            });
            $A.enqueueAction(action);
        });
    },

    displayToast: function (component, type, message) {

        component.find('notifLib').showToast({
            "variant": type,
            "message": message
        });
    },

})