/**
 * ─────────────────────────────────────────────────────────────────────────────────────────────────┐
 * methods:
 *
 *
 *
 *
 * ──────────────────────────────────────────────────────────────────────────────────────────────────
 * @author         Khonology01
 * @version        *.*
 * @created        2019/04/17
 * ──────────────────────────────────────────────────────────────────────────────────────────────────
 *
 * ─────────────────────────────────────────────────────────────────────────────────────────────────┘
 */
({
    doSave: function (component, event, helper) {

        component.set("v.showLoadingSpinner", true);
        let generatePromise = Promise.resolve(
            helper.promiseGenerateStatement(component)
        );
        generatePromise.then(
            $A.getCallback(function(result) {
                let statementId = result;
                component.set("v.statementId", statementId);
                component.set("v.showLoadingSpinner", false);
                component.find('notifLib').showToast({
                    "variant": "success",
                    "message": "Statement Added with Id: "+statementId
                });
            }),
            $A.getCallback(function(status) {
                component.set("v.showLoadingSpinner", false);
                component.find('notifLib').showToast({
                    "variant": "error",
                    "message": "Unable to add Statement! "+status.message
                });
            })
        ).catch(function(error) {
            component.set("v.showLoadingSpinner", false);
            $A.reportError("Unable to add Statement!", error);
        });

    },

    doSend: function (component, event, helper) {

        component.set("v.showLoadingSpinner", true);
        let emailPromise = Promise.resolve(
            helper.promiseEmailStatement(component)
        );
        emailPromise.then(
            $A.getCallback(function (result) {
                let isSent = result;
                if (!isSent) {
                    component.find('notifLib').showToast({
                        "variant": "error",
                        "message": "Unable to email Statement!"
                    });
                }else {
                    component.find('notifLib').showToast({
                        "variant": "success",
                        "message": "Statement emailed! "
                    });
                }
                component.set("v.showLoadingSpinner", false);
            }),
            $A.getCallback(function (status) {
                component.set("v.showLoadingSpinner", false);
                component.find('notifLib').showToast({
                    "variant": "error",
                    "message": "Unable to email Statement! "+status.message
                });
            })
        ).catch(function (error) {
            component.set("v.showLoadingSpinner", false);
            $A.reportError("Unable to email Statement!", error);
        });

        //TODO: change to handle multiple files

        if (component.find("fileId").get("v.files")) {
            if (component.find("fileId").get("v.files").length > 0) {
                helper.uploadHelper(component, event);
            } else {
                alert('Please Select a Valid File');
            }
        }
    },

    doInit:  function(component, event, helper) {
        component.set("v.showLoadingSpinner", true);
        let contactPromise = Promise.resolve(
             helper.promiseGetRelatedContacts(component)
         );
        contactPromise.then(
             $A.getCallback(function (result) {
                 component.set("v.showLoadingSpinner", false);
                 let options = result.map(record=>({label:record.Name,value:record.Email}));
                 component.set("v.relatedContactList", options);

             }),
             $A.getCallback(function (status) {
                 component.set("v.showLoadingSpinner", false);
                 component.find('notifLib').showToast({
                     "variant": "error",
                     "message": "Unable to add Statement or contacts! "+status.message
                 });
             })
         ).catch(function (error) {
            component.set("v.showLoadingSpinner", false);
             $A.reportError("Unable to initialise component!", error);
         });
    },

    handleRecordChanged: function(component, event, helper) {
        component.set("v.showLoadingSpinner", true);
        switch(event.getParams().changeType) {
            case "ERROR":
                // handle error - not needed
                break;
            case "LOADED":
                let statementPromise = Promise.resolve(helper.promiseGetStatement(component));
                statementPromise.then(
                    $A.getCallback(function (result) {
                        component.set("v.showLoadingSpinner", false);
                        component.set("v.statementId", result);
                    }),
                    $A.getCallback(function (status) {
                        component.set("v.showLoadingSpinner", false);
                        component.find('notifLib').showToast({
                            "variant": "error",
                            "message": "Unable to add Statement! "+status.message
                        });
                    })
                ).catch(function (error) {
                    component.set("v.showLoadingSpinner", false);
                    $A.reportError("Unable to initialise component!", error);
                });
                break;
            case "REMOVED":
                // not needed
                break;
            case "CHANGED":
                // not needed
                break;
        }
    },

    handleCCMe: function  (component, event){

        let isActive = event.getSource().get("v.value");

        if (isActive) {
            let action = component.get("c.getUserEmail");

            action.setCallback(this, function(response) {

                let state = response.getState();
                if (state === "SUCCESS") {
                    component.set("v.ccEmail", response.getReturnValue());
                }
            });
            $A.enqueueAction(action);
        } else {
            component.set("v.ccEmail", "");
        }
    },

    handleFilesChange: function(component, event, helper) {
        let fileName = 'No File Selected..';
        if (event.getSource().get("v.files").length > 0) {
            fileName = event.getSource().get("v.files")[0]['name'];
        }
        component.set("v.fileName", fileName);
    },


});