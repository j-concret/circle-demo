/**
 * Created by Chibuye Kunda on 2018/11/22.
 */
({

    /** this function will be called on init of component
     * @param componentP this is the component we are initialising
     * @param eventP this is the event that occured
     * @param helperP this is helper class
     */
    doInit: function( componentP, eventP, helperP ){

        var picklist_values_action = componentP.get( "c.getDestinationValues" );            //this is picklist action

        //define our callback function
        picklist_values_action.setCallback( this, function( response ){

            var state = response.getState();                        //get the state

            //check if we have success
            if( state === "SUCCESS" ){

                var server_response = response.getReturnValue();                //get the return value

                //check if we have an error
                if( server_response.is_error ){

                    helperP.showToastMessage( "error", "Error Occured", server_response.result_message );
                    return;                         //terminate on error

                }//end of if-block

                helperP.shouldShowAccountField( componentP );                                           //should we show/hide
                helperP.populateAccountAndContact( componentP );
                helperP.processPicklistValues( componentP, server_response.result_message );            //process the pickist values
                helperP.getListViewID( componentP );                                                    //get the list view ID

            }
            //check if have errors
            else if( state === "ERROR" )
                helperP.processServerErrors( response.getError() );             //process the server errors

        });//end of callback definition

        $A.enqueueAction( picklist_values_action );
        
    },//end of function definition





    /** this function will handle selection or removal of
     *  values
     *  @param componentP is the component we are updating
     *  @eventP is the event that occured
     */
    handleDestinationChange : function( componentP, eventP ){

        componentP.set( "v.selected_destinations", eventP.getParam( "value" ) );              //set the selected values

    },//end of function definition





    /** this function will save new Roll out record
     *  @param componentP is the component we are updating
     *  @param helperP is the helper object
     */
    handleSave : function( componentP, eventP, helperP ){

        var invalid_fields = helperP.validateUserInput( componentP );

        if( invalid_fields !== "" ) {

            helperP.showToastMessage("error", "Please Populate Following: ", invalid_fields );
            return;

        }//end of if-block

        var account_id = componentP.get( "v.lookup_account_record" ).Id;                    //get the Account ID
        var contact_id = componentP.get( "v.lookup_contact_record" ).Id;                    //get the Contact ID
        var client_reference = componentP.find( "client_reference_input" ).get( "v.value" );    //get the client reference input
        var shipment_value = componentP.find( "shipment_value_input" ).get( "v.value" );     //get the shipment value input

        var save_action = componentP.get( "c.createRolloutRecord" );        //get the record create action

        //check if we are external user
        if( !componentP.get( "v.show_account" ) )
            account_id = componentP.get( "v.account_id" );

        //set action parameters
        save_action.setParams({
            "account_idP":account_id,
            "contact_idP":contact_id,
            "client_referenceP":client_reference,
            "shipment_valueP":shipment_value,
            "destinationP":componentP.get( "v.selected_destinations" ),
            "create_rolloutP":"true"
        });

        //define the callback action
        save_action.setCallback( this, function( response ){

            componentP.set( "v.is_loading", false );             //hide the spinner
            var state = response.getState();                    //get the state returned

            //check if we have success
            if( state === "SUCCESS" ){

                var server_response = response.getReturnValue();                //get the server response
                var navigate_event;                             //this will hold our navigation event

                //check if an error had occured
                if( server_response.is_error ){

                    helperP.showToastMessage( "error", "Error Occured", server_response.result_message );
                    return;             //terminate function

                }//end of if-block

                navigate_event = $A.get( "e.force:navigateToSObject" );             //want to go to newly created SObject

                //set event parameters
                navigate_event.setParams({
                    "recordId":server_response.result_message,
                    "slideDevName":"detail"
                });

                navigate_event.fire();                                          //fire naivagation event

            }
            //check if have errors
            else if( state === "ERROR" )
                helperP.processServerErrors( response.getError() );             //process the server errors

        });//end of callback definition

        componentP.set( "v.is_loading", true );                 //show the spinner
        $A.enqueueAction( save_action );                        //execute the save action

    },//end of function definition





    /** this function will save new Roll out record
     *  @param componentP is the component we are updating
     *  @param helperP is the helper object
     */
    handleSaveAndNew : function( componentP, eventP, helperP ){

        var invalid_fields = helperP.validateUserInput( componentP );

        if( invalid_fields !== "" ) {

            helperP.showToastMessage("error", "Please Populate Following: ", invalid_fields );
            return;

        }//end of if-block

        var account_id = componentP.get( "v.lookup_account_record" ).Id;                    //get the Account ID
        var contact_id = componentP.get( "v.lookup_contact_record" ).Id;                    //get the Contact ID
        var client_reference = componentP.find( "client_reference_input" ).get( "v.value" );    //get the client reference input
        var shipment_value = componentP.find( "shipment_value_input" ).get( "v.value" );     //get the shipment value input

        var save_action = componentP.get( "c.createRolloutRecord" );        //get the record create action

        //check if we are external user
        if( !componentP.get( "v.show_account" ) )
            account_id = componentP.get( "v.account_id" );

        //set action parameters
        save_action.setParams({
            "account_idP":account_id,
            "contact_idP":contact_id,
            "client_referenceP":client_reference,
            "shipment_valueP":shipment_value,
            "destinationP":componentP.get( "v.selected_destinations" ),
            "create_rolloutP":"true"
        });

        //define the callback action
        save_action.setCallback( this, function( response ){

            componentP.set( "v.is_loading", false );             //hide the spinner
            var state = response.getState();                    //get the state returned

            //check if we have success
            if( state === "SUCCESS" ){

                var server_response = response.getReturnValue();                //get the server response
                var navigate_event;                             //this will hold our navigation event

                //check if an error had occured
                if( server_response.is_error ){

                    helperP.showToastMessage( "error", "Error Occured", server_response.result_message );
                    return;             //terminate function

                }//end of if-block

                helperP.clearForm( componentP );                //clear the form
                helperP.showToastMessage( "success", "", "New Rollout Successfully Created" );

            }
            //check if have errors
            else if( state === "ERROR" )
                helperP.processServerErrors( response.getError() );             //process the server errors

        });//end of callback definition

        componentP.set( "v.is_loading", true );                 //show the spinner
        $A.enqueueAction( save_action );                        //execute the save action

    },//end of function definition





    /** this function will handle cancel button
     *  @param componentP is the component that we are updating
     *  @param eventP is the event we are processing
     *  @param helperP is the helper object
     */
    handleCancel : function( componentP, eventP, helperP ){

        var navigation_event = $A.get( "e.force:navigateToList" );

        //set parameters
        navigation_event.setParams({
            "listViewId":componentP.get( "v.list_view_id" ),
            "listViewName":null,
            "scope":"Roll_Out__c"
        });//end of parameters

        navigation_event.fire();                //fire the event

    }//end of function definition

});