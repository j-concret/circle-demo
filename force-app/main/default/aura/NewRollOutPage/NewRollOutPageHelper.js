/**
 * Created by Chibuye Kunda on 2018/11/23.
 */
({


    /** this function will populate the account and contact fields
     *  @param componentP is the component we are updated
     */
    populateAccountAndContact : function( componentP ){

        var get_records = componentP.get( "c.getAccountAndContact" );                    //action to return account and contact record

        console.log( "getting contact and account" );

        get_records.setCallback( this, function( response ){

            var state = response.getState();                            //get the state

            //check if we have success
            if( state === "SUCCESS" ){

                console.log( "Success getting account and contact" );

                var server_response = response.getReturnValue();                //get the return value
                var account_populate_method = componentP.find( "account_lookup" );              //get the account lookup field
                var contact_populate_method = componentP.find( "contact_lookup" );              //get the contact lookup field


                //check if we have an error
                if( server_response.is_error ){

                    this.showToastMessage( "error", "Error Occured", server_response.result_message );
                    return;                         //indicate failure

                }
                //check if auto populate is not required
                else if( server_response.result_message === "NA" ) {

                    console.log("In NA");
                    return;             //terminate function

                }//end of if-else block

                console.log( server_response.sobject_list[0] );
                console.log( server_response.sobject_list[1] );

                console.log( "Before the auto set" );
                console.log( "Account: " + account_populate_method );
                console.log( "Test Value: " + componentP.get( "v.shipment_value" ) );

                componentP.set( "v.account_id", server_response.sobject_list[0].Id );
                contact_populate_method.setSelectedRecord( server_response.sobject_list[1] );       //set the selected contact record

                console.log( "Account ID: " + componentP.get( "v.account_id" ) );

            }
            //check if have errors
            else if( state === "ERROR" )
                this.processServerErrors( response.getError() );             //process the server errors

        });//end of

        $A.enqueueAction( get_records );                    //execute the action

    },//end of function definition





    /** this function will get the the list view ID
     *  @param componentP is the component we are updating
     */
    getListViewID : function( componentP ){

        var get_list_view_id = componentP.get( "c.getRecordListViewID" );               //function that will return the list view

        //set the function callback
        get_list_view_id.setCallback( this, function( response ){

            var state = response.getState();                //get the server state

            //check if we have success
            if( state === "SUCCESS" ) {

                var server_response = response.getReturnValue();                //get the return value

                //check if we have an error
                if( server_response.is_error ){

                    this.showMessage( this.setMessageParameters( componentP, server_response.result_message, "error", "Error Occured" ) );
                    return;                         //indicate failure

                }//end of if-block

                componentP.set( "v.list_view_id", server_response.result_message );             //get the result message

            }
            //check if have errors
            else if( state === "ERROR" )
                this.processServerErrors( response.getError() );             //process the server errors

        });//end of callback definition

        $A.enqueueAction( get_list_view_id );                               //enqueue the action

    },//end of function definition








    /** this function will clear the form for a new record
     *  @param componentP is the component that we are updating
     */
    clearForm : function( componentP ){

        //check if account field is visible
        if( componentP.get( "v.show_account" ) )
            componentP.find( "account_lookup" ).clearSelectedRecord();          //this function will clear selected account record

        componentP.find( "contact_lookup" ).clearSelectedRecord();          //this function will clear selected contact record

        componentP.find( "dual_list" ).set( "v.value", [] );        //clear the list
        componentP.set( "v.client_reference", "" );                 //clear the client reference
        componentP.set( "v.shipment_value", "0" );                  //clear shipment value

    },//end of function definition





    /** this function will process returned picklist values
     *  @param componentP is the component we are updating
     *  @param picklist_stringP this is a string containing picklist values
     */
    processPicklistValues : function( componentP, picklist_stringP ){

        var picklist_values_array;                                      //this is the picklist values
        var options_array = [];                                         //this is the options array

        picklist_values_array = picklist_stringP.split( "," );                        //split the array

        //loop through the picklist array
        for( var i=0; i<picklist_values_array.length; ++i )
            options_array.push( { value:picklist_values_array[i], label:picklist_values_array[i] } );           //add to options array

        componentP.set( "v.destination_options", options_array );               //set the options array

    },//end of function definition






    /** this function will show toast message when not embedded in VF page
     *  @param typeP is the toast message type
     *  @param titleP is the title of the toast
     *  @param messageP is the message for the toast message
     */
    showToastMessage : function( typeP, titleP, messageP ){

        var toast_event = $A.get( "e.force:showToast" );            //we want to show a toast message

        //setup function parameters
        toast_event.setParams( {
            title : titleP,
            message: messageP,
            duration: "5000",
            key: "info_alt",
            type: typeP,
            mode: "sticky"
        });

        toast_event.fire();        //execute the toast event

    },//end of function definition







    /** this function will process server errors that are returned
     *  @param errors_listP is the list of errors that are returned
     */
    processServerErrors: function( errors_listP ){

        //check if we have errors in list
        if( errors_listP ){

            let error_string = "";              //this will hold all errors listed

            //loop through the errors list
            for( let i=0; i < errors_listP.length; ++i ) {

                //check that the current error is valid
                if (errors_listP[i] && errors_listP[i].message);
                    error_string = error_string + "-" + errors_listP[i].message + "\n";             //append to error string

            }//end of for-block

            //check that the error messages have been added
            if( error_string !== "" ) {

                this.showToastMessage( "error", "Error Occured", error_string );
                return;

            }//end of if-block

        }//end of if-block

        this.showToastMessage( "error", "Error Occured", "Unknown Error" );

    },//end of function definition





    /** this function will determine whether or not the account field should be shown
     *  @param componentP this is the component we are updating
     */
    shouldShowAccountField : function( componentP ){

        var server_action = componentP.get( "c.isCommunityUser" );      //make a call to server action

        server_action.setCallback( this, function( response ){

            var state = response.getState();                //get the state of callout
            var not_community_user;

            //check if we have success
            if( state === "SUCCESS" ){

                var server_response = response.getReturnValue();                    //get the returned value

                //check if we have an error
                if( server_response.is_error ){

                    this.showToastMessage( "error", "Error Occured", server_response.result_message );
                    return;                         //terminate on error

                }//end of if-block

                not_community_user = ( server_response.result_message !== "true" );
                componentP.set( "v.show_account", not_community_user );

            }
            //check if have errors
            else if( state === "ERROR" )
                this.processServerErrors( response.getError() );             //process the server errors

        });

        $A.enqueueAction( server_action );                  //execute the action

    },//end of function definition





    /** this function will validate user input
     *  @param componentP is the component we are updating
     *  @return true if the input is valid
     */
    validateUserInput : function( componentP ){

        var invalid_fields = [];                          //this is our error string
        var error_string = "";                              //this is our error string

        //check if account is being shown and if ID is not set
        if( componentP.get( "v.show_account" ) && !( "Id" in componentP.get( "v.lookup_account_record" ) ) )
            invalid_fields.push( "Account Name" );

        //check if contact ID has been set
        if( !( "Id" in componentP.get( "v.lookup_contact_record" ) ) )
            invalid_fields.push( "Contact For Quote" );

        //check if we have client reference
        if( componentP.get( "v.client_reference" ) === "" )
            invalid_fields.push( "Client Reference" );

        //check if we have shipment value
        if( ( componentP.get( "v.shipment_value" ) === 0 ) || !( componentP.get( "v.shipment_value" ) ) )
            invalid_fields.push( "Shipment Value( cannot be zero )" );

        //check if we have no selected destinations
        if( componentP.get( "v.selected_destinations" ) === "" )
            invalid_fields.push(  "Destination" );

        //check if we have invalid fields
        for( var i=0; i < invalid_fields.length; ++i ){

            //check if we have processed first index
            if( i > 0 )
                error_string = error_string + ", " + invalid_fields[i];         //append the fields
            else
                error_string = error_string + invalid_fields[i];                //append the fields

        }//end of for-loop

        console.log( "error string: " + error_string );
        return error_string;

    }//end of function definition





})