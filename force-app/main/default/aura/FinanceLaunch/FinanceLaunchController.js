({
    doInit:function(component,event,helper){
        var workspaceAPI = component.find("workspace");
        workspaceAPI.getFocusedTabInfo ().then(function(response){
            
            var focusedTabId = response.tabId;
            
            workspaceAPI.setTabLabel({
                tabId: focusedTabId,
                label: " Finance" //set label you want to set
            });
            workspaceAPI.setTabIcon({
                tabId: focusedTabId,
                icon: "utility:currency", //set icon you want to set
                iconAlt: "Finance" //set label tooltip you want to set
            });
        });
    },
    gotoURL:function(component,event,helper){
        var outerDiv = component.find('outerDiv');
        $A.createComponent("c:BankTransactionProcess",{ 
            'aura:id': 'newPartDiv',
        },function(newButton, status, errorMessage){
            if (status === "SUCCESS") {
                var body = outerDiv.get("v.body");
                body.push(newButton);
                outerDiv.set("v.body", body);
                $A.util.removeClass(outerDiv, 'backgroundImage');
                $A.util.addClass(outerDiv, 'relPos');
            }
        });
    },
    gotoEB:function(component,event,helper){
        var outerDiv = component.find('outerDiv');
        $A.createComponent("c:ShipmentInvoiceBilling",{ 
            'aura:id': 'newPartDiv',
        },function(newButton, status, errorMessage){
            if (status === "SUCCESS") {
                var body = outerDiv.get("v.body");
                body.push(newButton);
                outerDiv.set("v.body", body);
                $A.util.removeClass(outerDiv, 'backgroundImage');
                $A.util.addClass(outerDiv, 'relPos');
            }
        });
    }
})