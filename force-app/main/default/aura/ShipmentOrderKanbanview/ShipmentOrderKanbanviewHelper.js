({
  getData: function(component) {
    var self = this;
    self.serverCall(component, 'getKanbanViewData', {'recordId': component.get('v.recordId')}, function(response) {
      if(response.status === 'OK'){
        if(response.data && Object.keys(response.data).length){
          var data = self.processData(response.data,response.statusvals,response.flagVals);
          component.set('v.data',data);
        }
        else{
          component.set('v.msg','No Tasks associated with this record.')
          component.set('v.type','info')
        }
      }
      else{
        component.set('v.msg',response.msg);
        component.set('v.type','error')
      }
    });
  },
  processData: function(data,statusvals,flagVals) {
    var self = this;
    var index = -1;
    var finalData = [];
    var statusOrder = {'In Progress':1,'Completed':2,'Not Applicable':3};
    var flagOrder = {'Process':1,'Documentary':2,'Compliance':3,'Freight':4,'Finance':5};
    var roleOrder = {'AM':1,'ICE':2,'FREIGHT':3,'FM':4};

    for(var key in data) {
      var allStatus = JSON.parse(JSON.stringify(statusvals));

      var roleData = data[key];
      var shipmentStatuses = roleData.shipmentStatuses;
      var shipStats = [];
      for(var status in shipmentStatuses) {
        var allFlags = JSON.parse(JSON.stringify(flagVals));

        index = allStatus.indexOf(status);
        if (index > -1) {
          allStatus.splice(index, 1);
        }

        var count = 0;
        var statusData = shipmentStatuses[status];
        var tasks = [];

        if(statusData.isFlag) {
          var flagData = statusData['tasks'];
          for(var flagType in flagData) {

            var ind = allFlags.indexOf(flagType);
            if (ind > -1) {
              allFlags.splice(ind, 1);
            }

            var groupedTasks = flagData[flagType];
            var grpTask = self.groupingTasks(JSON.parse(JSON.stringify(groupedTasks)));
            count+=grpTask.count;
            tasks.push({name:flagType,flagTasks:grpTask.tasks,order:flagOrder[flagType],colorName:grpTask.colorName});
          }

          for(var i=0;i<allFlags.length;i++){
            tasks.push({name:allFlags[i],flagTasks:[],order:flagOrder[allFlags[i]],colorName:'green'});
          }
          tasks.sort((a,b) => (a.order > b.count) ? 1 : ((b.order > a.order) ? -1 : 0));
        } else {
          var groupedTasks = JSON.parse(JSON.stringify(statusData['tasks']));
          var grpTask = self.groupingTasks(groupedTasks);
          tasks = JSON.parse(JSON.stringify(grpTask.tasks));
          count = grpTask.count;
        }

        shipStats.push({
          'status': status,
          'count':count,
          'isOpen':true,
          'order': statusOrder[status],
          'isFlag': statusData.isFlag,
          'tasks': tasks
        });
      }

      for(var i=0;i<allStatus.length;i++){
        var inPtasks = [];
        if(allStatus[i] == 'In Progress'){
          for(var p=0;p<flagVals.length;p++){
            inPtasks.push({name:flagVals[p],flagTasks:[],order:flagOrder[flagVals[p]],colorName:'green'});
          }
          inPtasks.sort((a,b) => (a.order > b.order) ? 1 : ((b.order > a.order) ? -1 : 0));
        }
        shipStats.push({
          'status': allStatus[i],
          'count':0,
          'isOpen':true,
          'order': statusOrder[allStatus[i]],
          'isFlag': allStatus[i] == 'In Progress',
          'tasks': allStatus[i] == 'In Progress' ? inPtasks : []
        });
      }
      shipStats.sort((a,b) => (a.order > b.order) ? 1 : ((b.order > a.order) ? -1 : 0));
      finalData.push({'title': roleData['title'],'name': roleData['name'], 'shipmentStatuses': shipStats,order:roleOrder[roleData['title']]});
    }
    finalData.sort((a,b) => (a.order > b.order) ? 1 : ((b.order > a.order) ? -1 : 0));
    return finalData;
  },

  groupingTasks : function(groupedTasks){
    var tasks = [];
    var count = 0;
    for(var grpName in groupedTasks){
      var len = groupedTasks[grpName].length;
      groupedTasks[grpName].sort((a,b) => (a.colorCode > b.colorCode) ? 1 : ((b.colorCode > a.colorCode) ? -1 : 0));
      var tskObj = groupedTasks[grpName][0];

      if(len == 1 || grpName === 'others')
        tasks.push(...groupedTasks[grpName]);
      else
        tasks.push({
          ruleName: tskObj.ruleName,
          isGrouped: true,
          grpName:grpName,
          groupedTasks: groupedTasks[grpName],
          colorCode:tskObj.colorCode
        });
      count += len;
    }
    tasks.sort((a,b) => (a.colorCode > b.colorCode) ? 1 : ((b.colorCode > a.colorCode) ? -1 : 0));
    var colorName = 'green';
    if(tasks[0])
      colorName = tasks[0].colorCode == 1 ? 'red' : tasks[0].colorCode == 2 ? 'orange' : tasks[0].colorCode == 3 ? 'yellow' : 'green';
    return {'tasks':tasks,'count':count,'colorName':colorName};
  }
})