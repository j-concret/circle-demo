({
    doInit: function(component, event, helper) {
        var pageReference = component.get("v.pageReference");
        component.set("v.recordId", pageReference.state.c__recId);
        var workspaceAPI = component.find("workspace");
        workspaceAPI.getAllTabInfo().then(function(tabs){
            console.log(tabs);
            var focusedTabId;
            for(var i=0;i<tabs.length;i++){
                if(tabs[i].pageReference.attributes.componentName && tabs[i].pageReference.attributes.componentName.includes('ShipmentOrderKanbanview')){
                    focusedTabId = tabs[i].tabId;
                    break;
                }
            }
            workspaceAPI.setTabLabel({
                tabId: focusedTabId,
                label: " Flags" //set label you want to set
            });
            workspaceAPI.setTabIcon({
                tabId: focusedTabId,
                icon: "utility:priority", //set icon you want to set
                iconAlt: "Flags" //set label tooltip you want to set
            });
        });
        helper.getData(component);
    }
})