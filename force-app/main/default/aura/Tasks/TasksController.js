({
  allowDrop: function(component, event, helper) {
    event.preventDefault();
  },
  onDrop: function(component, event, helper) {
    event.preventDefault();
    var status = component.get('v.taskStatus');
    var role = component.get('v.role');
    var task = JSON.parse(event.dataTransfer.getData('task'));
    if(task.status !== status && role == task.role){
      if(status.includes('Applicable')){
        component.set('v.taskObj',task);
        component.set('v.showModal',true);
      }
      else{
        task.reasonForNotApplicable = ''
        helper.updateTasks(component,status,task);
      }
    }
  },
  updateNotApplicable:function(component, event, helper) {
    component.set('v.showModal',false);
    helper.updateTasks(component,component.get('v.taskStatus'),component.get('v.taskObj'));//updateTasks(component,'Not Applicable');
  }
})