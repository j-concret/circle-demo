({
    updateTasks: function(component, status, task) {
        component.set('v.showSpinner', true);
        var self = this;
        self.serverCall(component, 'updateTask', {'taskId': task.id, 'status': status,reason:task.reasonForNotApplicable}, function(response) {
            component.set('v.showSpinner', false);
            if(response.status === 'OK') {
                //$A.get('e.force:refreshView').fire();
                $A.get("e.c:refreshEvent").fire();
                var updateTskEvt = component.getEvent('updateTaskEvent');
                var oldStatus = task.status;
                if(oldStatus.includes('Applicable'))
                    task.reasonForNotApplicable = '';
                task.status = status;
                updateTskEvt.setParams({
                    'task': task,
                    'OldStatus': oldStatus
                });
                updateTskEvt.fire();
            }else{
                self.showToast(response.status,'error',response.msg);
            }
        });
        
    },
    showToast : function(title, type , msg) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": title,
            "type":type,
            "message": msg
        });
        toastEvent.fire();
    }
})