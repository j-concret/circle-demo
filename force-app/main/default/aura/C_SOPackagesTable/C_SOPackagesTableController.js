({
	doInitCtrl : function(component, event, helper) {
		helper.getSOPackagesRecords(component, event, helper);
	},
    save : function(component, event, helper) {
        helper.saveSOPackages(component, event, helper);
    }
})