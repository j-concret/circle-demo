({
    getSOPackagesRecords: function(component, event, helper) {
        component.set('v.showSpinner',true);
        
        var action = component.get("c.getShipmentPackages");
        action.setParams({
            shipmentId: component.get('v.recordId')
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.shipmentPackages",response.getReturnValue());
                
            }
            component.set('v.showSpinner',false);
        });
        $A.enqueueAction(action);
    },
    saveSOPackages : function(component, event, helper) {
        component.set('v.showSpinner',true);
        
        var action = component.get("c.saveShipmentPackages");
        action.setParams({
            shipmentPackages: component.get('v.shipmentPackages')
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                alert('Success');
                
            }
            component.set('v.showSpinner',false);
        });
        $A.enqueueAction(action);
    }
})