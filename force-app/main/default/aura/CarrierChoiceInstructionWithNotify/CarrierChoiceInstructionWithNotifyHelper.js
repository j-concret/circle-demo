({
  getTask : function(cmp,taskId) {

    var self = this;
    cmp.set('v.showSpinner',true);
    self.serverCall(cmp,'getTaskWithConsignee',{
      taskId : taskId
    },function(response){
      cmp.set('v.showSpinner',false);
      cmp.set('v.freight',response.freight ?response.freight : null);
      if(response.task && response.task.State__c === 'Under Review'){
        cmp.set('v.type','Courier');
      }
    },function(err){
      cmp.set('v.showSpinner',false);
      self.showToast('error','ERROR',err);
    });
  },
  updateTaskState:function(cmp){
    var self = this;
    cmp.set('v.showSpinner',true);
    self.serverCall(cmp,'updateTask',{
      taskId : cmp.get('v.taskId'),
      state : 'Under Review'
    },function(task){
      cmp.set('v.showSpinner',false);
      self.showToast('success','SUCCESS','Task Updated.');
      self.closeTab(cmp);
    },function(err){
      cmp.set('v.showSpinner',false);
      self.showToast('error','ERROR',err);
    });
  },
  showToast : function(title,type,msg) {
    var toastEvent = $A.get('e.force:showToast');
    toastEvent.setParams({
      'title': title,
      'type' :type,
      'message': msg
    });
    toastEvent.fire();
  },
  closeTab : function(cmp){
    var workspaceAPI = cmp.find('workspace');
    var focusedTabId = cmp.get('v.tabId');

    if(focusedTabId){
      workspaceAPI.closeTab({tabId: focusedTabId});
    }
  }
})