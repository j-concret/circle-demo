/**
 * Created by Chibuye Kunda on 2018/12/31.
 */
({


    /** this function will add a row to table
     *  @param componentP is the component we are setting
     *  @param eventP is the event we are running
     *  @param helperP is the helper object
     */
    addNewRow : function( componentP, eventP, helperP ){

        componentP.getEvent( "AddFDRow" ).fire();             //fire add event

    },//end of function definition





    /** this function will delete a row
     *  @param componentP is the component we are setting
     *  @param eventP is the event we are running
     *  @param helperP is the helper object
     */
    deleteRow : function( componentP, eventP, helperP ){

        componentP.getEvent( "DeleteRow" ).setParams({ "index" : componentP.get( "v.row_index" ) }).fire();

    }//end of function definition

})