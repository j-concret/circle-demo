({
    addNewRow : function( cmp, event, helper ){
      cmp.getEvent( "AddRow" ).fire();
    },
    deleteRow : function( cmp, event, helper ){
      cmp.getEvent( "DeleteRow" ).setParams({ "index" : cmp.get( "v.row_index" ) }).fire();
    }
})