({	
    doInitCtrl: function(component, event, helper) {
        helper.doInitHlpr(component, helper);
        helper.subscribe(component, helper);
    },
    
    //Handle Chatter
    handleShowChatterSectionCtrl: function(component, event, helper) {
        component.set("v.showChatterSection", !component.get("v.showChatterSection"));
    },
    
    //Handle Contacts
    contactsVisibility: function(component, event, helper) {
        component.set("v.showContacts", !component.get("v.showContacts"));
    },
    //Required Fields
    showRequiredFields: function(component, event, helper){
        $A.util.removeClass(component.find("Account__c"), "none");
        $A.util.removeClass(component.find("Ship_to_Country__c"), "none");
        $A.util.removeClass(component.find("CPA_v2_0__c"), "none");
        $A.util.removeClass(component.find("Ship_From_Country__c"), "none");
        $A.util.removeClass(component.find("Service_Type__c"), "none");
        
    },
    
    //onsubmit
    onRecordSubmit: function (component, event, helper) {
        component.set("v.showSpinner", true);
        
    },                
    
    
    //onsuccess
    handleSuccess : function(component, event, helper) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({ "title": "Success!",
                              "message": "Saved.",
                              "type": "success"});
        toastEvent.fire();
        component.set("v.editHeader", false);
        component.set("v.showSpinner", false);
    },
    
    //OnError
    onError : function(component, event, helper) {
        
        var toastEvent2 = $A.get("e.force:showToast");
        toastEvent2.setParams({
            "title": "Error!",
            "message": "Error."
        });
        toastEvent2.fire();
        component.set("v.showSpinner", false);
    },
    
    //Handle Edit Cancel
    handleCancel : function(component, event, helper) {
        component.set("v.editHeader", false);
    },
    
    //Handle Edit
    edit: function(component, event, helper) {
        component.set("v.editHeader", true);  
    },
    recordUpdated : function(component, event, helper) {
      var changeType = event.getParams().changeType;
      if (changeType === "CHANGED") {
        component.find("shipmentOrderRecord").reloadRecord();}
    }
})