({
    doInitHlpr: function(component, helper) {
        component.set('v.showInnerSpinner', true);
        var params = {
            SOID: component.get('v.recordId')
        };
        this.makeServerCall(component, 'GetSoLog', params, function(result) {
            if(result.status === 'OK') {
                if(result.soLogData[0]){
                    component.set("v.soLogData", result.soLogData[0]);
                 /*   if(result.soRec && result.soRec.Mapped_Shipping_status__c == 'Compliance Pending'){
                        let etaDays = result.soRec.ETA_Days_Business__c;
                        var txt2 = etaDays.slice(0, 9) + "*" + etaDays.slice(9);
						component.set("v.etaTextForCompliance", txt2);                        
                    }*/
                } 
            }
            else {
                helper.showToast('error', 'Error Occured', result.msg);
            }
            component.set('v.showInnerSpinner', false);
        });
    },
    subscribe : function(component, helper) {
      const self = this;
      var userId = $A.get("$SObjectType.CurrentUser.Id");
      var recordId = component.get('v.recordId');
      // Get the empApi component
      const empApi = component.find('empApi');
      // Get the channel from the input box
      const channel = '/event/TaskChange__e';//component.find('channel').get('v.value');
      // Replay option to get new events
      const replayId = -1;

      // Subscribe to an event
      empApi.subscribe(channel, replayId, $A.getCallback(eventReceived => {
          // Process event (this is called each time we receive an event)
          console.log('Received event ', JSON.stringify(eventReceived));
          if(userId == eventReceived.data.payload.CreatedById && eventReceived.data.payload.ShipmentOrderIds__c){
            let shipIds = eventReceived.data.payload.ShipmentOrderIds__c.split(',');
            if(shipIds.includes(recordId)){
              $A.get('e.force:refreshView').fire();
              //Fire app event to refresh other related child/Parent cmps
              $A.get("e.c:refreshEvent").fire();
              // window.location.reload();
            }
          }
      }))
      .then(subscription => {
          // Subscription response received.
          // We haven't received an event yet.
          console.log('Subscription request sent to: ', subscription.channel);
          // Save subscription to unsubscribe later
          component.set('v.subscription', subscription);
      });
    }
})