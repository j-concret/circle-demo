({
	 component :null,
    partslist:null,
    
    sethelper : function(componentP, eventP){this.component =componentP;  },
    //*******  shipment Package  Start****//
	 createTableRow : function( componentP, eventP ){

        var records = componentP.get( "v.shipment_order_package_list" );           //get the record list

        //add to records list
        records.push({
            "sobjectType":"Shipment_Order_Package__c",
            "packages_of_same_weight_dims__c":0,
            "Weight_Unit__c":"KGs",
            "Actual_Weight__c":0,
            "Dimension_Unit__c":"CMs",
            "Length__c":0,
            "Breadth__":0,
            "Height__":0

        });

        componentP.set( "v.shipment_order_package_list", records );

        console.log( "Records: " + records );
        console.log( " Size of the shipment_order_package_list " + records.length );
        componentP.set('v.ShipmentOrderPackageSize',records.length);

       // this.createObjectString( records );

    },
    receiveMessage: function (event) {
     
        var count1 = parseInt(this.component.get("v.count"));
        count1 = count1+1;
        this.component.set("v.count",count1);
        
     var myJSON = JSON.stringify(event.data.Parts);
     
        if(myJSON && count1 <=1){
       this.component.set("v.parts_list",myJSON);
       this.component.set("v.Secondhandgoods",event.data.Type_of_Goods);
       this.component.set("v.Li_ion_Batteries",event.data.Li_ion_Batteries);
       this.component.set("v.Li_ion_BatteryTypes",event.data.Li_ion_BatteryTypes);
     
        if(event.data.Parts){
       this.component.set("v.PartlistSize",event.data.Parts.length);
        }else{
            this.component.set("v.PartlistSize",0);
        }
        
         console.log("parts_list - Json ::", myJSON);
        console.log("parts_list ::", event.data.Parts);
        console.log("Secondhandgoods ::", event.data.Type_of_Goods);
        console.log("Li_ion_Batteries ::", event.data.Li_ion_Batteries);
        console.log("Li_ion_BatteryTypes ::", event.data.Li_ion_BatteryTypes);
         
      
        var a= this.component.get("v.parts_list");
        console.log("a----->", a);
       
      var b  = a.includes("Chuck");
       
            console.log("b----->", b);
        if(b == false){
            this.component.set("v.ReadytoSubmitorder","Yes");
            
            this.component.set("v.isLoading",true);
          
             var action = this.component.get("c.CreateParts");
     
        action.setParams({
                          "PPart_List" : a,
            				"shipId": this.component.get("v.recordId")
                         });
         action.setCallback(this, function(response) {  
           
            var state = response.getState();
             
         
            if (state === 'SUCCESS'){
                event.data.Parts = "{ name: 'Chuck'}";
                this.component.set('v.parts_list',"{ name: 'Chuck'}");
               this.fireToast('Success !', 'success', 'Successfully created Parts !');
                this.component.set("v.isLoading",false);
            
     				
                 var hostname = window.location.hostname;
				var arr = hostname.split(".");
				var instance = arr[0];
                
                 var address = "https://"+instance+".lightning.force.com/lightning/r/Shipment_Order__c/"+this.component.get("v.recordId")+"/view"; 
                 window.open(address,'_self');
                
               }
            else{
                this.fireToast('ERROR', 'error', "Error while creating Parts");
                 this.component.set("v.isLoading",false);
            }
            
        });
        $A.enqueueAction(action);
        }
        }
      },
    
    fireToast: function(title, type, message) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": title,
            "type": type,
            "message": message
        });
        toastEvent.fire();
    }
})