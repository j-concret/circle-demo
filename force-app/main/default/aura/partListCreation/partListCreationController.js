({
	doInit : function( componentP, eventP, helperP ){
         var recordId = componentP.get('v.recordId');
        
        if(!recordId){
            var myPageRef = componentP.get("v.pageReference");
        	recordId = myPageRef.state.c__shipID;
            if(!recordId){
                helper.showToast('Error','error','Shipment record Id is missing!');
                return;
            }
            componentP.set('v.recordId',recordId);
        }
		helperP.sethelper(componentP, eventP);
        window.addEventListener("message", helperP.receiveMessage.bind(helperP), false);
        componentP.set( "v.table_headings",
                        [ "#","How many packages?", "Weight Unit", "Actual Weight",
                          "Dimension Unit", "Length", "Breadth", "Height" ] );
        
 			
    },
})