({
  doInit: function(cmp, statues) {
    var self = this;
    cmp.set('v.showSpinner',true);
    self.serverCall(cmp,'getUnmappedProductParts',{
      'statuses': (statues && statues.length > 0) ? JSON.stringify(statues) : '',
      'reviewMember': cmp.get('v.reviewMember') ? cmp.get('v.reviewMember') : '',
      'partMatchingReview': typeof cmp.get('v.partMatchingReview') == 'boolean' ? cmp.get('v.partMatchingReview') : null
    },function(result){
      let matchingOptins = [];
      let reasonOptins = [];
      if(result.partMatchingMethodOptions){
        matchingOptins = result.partMatchingMethodOptions;
        matchingOptins.unshift({label:'--none--',value:'none'});
      }
      if(result.reasonForManualMatchOptions){
        reasonOptins = result.reasonForManualMatchOptions;
        reasonOptins.unshift({label:'--none--',value:'none'});
      }
      cmp.set('v.showSpinner',false);
      cmp.set('v.totalRecord', result.totalRecord);
      cmp.set('v.totalReviewed', result.totalReviewed);
      cmp.set('v.statuses', result.statuses);
      cmp.set('v.partMatchingMethodOptions', matchingOptins);
      cmp.set('v.reasonForManualMatchOptions', reasonOptins);
      cmp.set('v.partList', result.data);
    },function(err){
      cmp.set('v.showSpinner',false);
      self.showToast('error','ERROR',err);
    });
  },
  updateParts: function(cmp) {
    var self = this;
    cmp.set('v.showSpinner', true);
    let updatedParts = [];
    var partList = cmp.get('v.partList');

    for(let part of partList) {
      if(part.updated) {
        updatedParts.push(part);
      }
    }
    if(updatedParts .length < 1) {
      cmp.set('v.showSpinner',false);
      self.showToast('ERROR', 'error', "No Part updated");
      return;
    }


    self.serverCall(cmp,'updatePartsAndProducts',{
      'partJson': JSON.stringify(updatedParts)
    },function(response){
      cmp.set('v.showSpinner',false);
      self.showToast('Success', 'success', 'Parts mapped successfully!');
      self.doInit(cmp);
    },function(err){
      cmp.set('v.showSpinner',false);
      self.showToast('error','ERROR',err);
    });

  },
  handleGrpUngrp: function(component, event) {
    try {
      var grpIndex = event.getParam('grpIndex');
      var childpIndex = event.getParam('childpIndex');
      var partList = component.get('v.partList');
      var ind = -1;
      var updated = false;
      var part = JSON.parse(JSON.stringify(partList[grpIndex]));
      if(typeof childpIndex === 'undefined' && typeof grpIndex !== 'undefined' && part.isGrpMember) {

        for(var i = 0; i < partList.length; i++) {
          if(i == grpIndex) continue;
          if(partList[i].partName == part.partName) {
            if(!partList[i].product && part.product) return;
            if(!part.product || (part.product && partList[i].product && partList[i].product.Id == part.product.Id)) {
              ind = i;
              break;
            }
          }
        }

        if(ind != -1) {
          partList[ind].groupedParts.push(part);
          partList[ind].isOpen = true;
          partList.splice(grpIndex, 1);
          updated = true;
        }

      }
      else if(typeof childpIndex === 'undefined' && typeof grpIndex !== 'undefined' && !part.isGrpMember) {
        var newParts = [];
        if(!part.product && part.groupedParts && part.groupedParts.length > 0) {
          var grpPartsCpy = JSON.parse(JSON.stringify(part.groupedParts));
          var q = 0;
          for(var i = 0; i < grpPartsCpy.length; i++) {
            var grpPart = grpPartsCpy[i];
            if(grpPart.product) {
              q += i;
              newParts.push(JSON.parse(JSON.stringify(grpPart)));
              partList[grpIndex].groupedParts.splice(q, 1);
              q = -1;
            }
          }
          if(newParts.length > 0) {
            for(var j = 0; j < newParts.length; j++) {
              partList.splice(grpIndex + 1 + j, 0, newParts[j]);
            }
            updated = true;
          }
        } else if(part.product) {

          var grpPartsCpy = JSON.parse(JSON.stringify(part.groupedParts));
          var q = 0;
          for(var i = 0; i < grpPartsCpy.length; i++) {
            var grpPart = grpPartsCpy[i];
            if(grpPart.product && part.product.Id != grpPart.product.Id) {
              q += i;
              newParts.push(JSON.parse(JSON.stringify(grpPart)));
              partList[grpIndex].groupedParts.splice(q, 1);
              q = -1;
            }
          }
          if(newParts.length > 0) {
            for(var j = 0; j < newParts.length; j++) {
              partList.splice(grpIndex + 1 + j, 0, newParts[j]);
            }
            updated = true;
          }

          var indexes = [];
          for(var i = 0; i < partList.length; i++) {
            if(i == grpIndex) continue;
            if(partList[i].partName == part.partName && partList[i].product && part.product.Id == partList[i].product.Id) {
              var pp = JSON.parse(JSON.stringify(partList[i]));
              partList[grpIndex].groupedParts.push(pp);
              indexes.push(i);
            }
          }
          var p = 0;
          if(indexes.length > 0) {
            for(var j = 0; j < indexes.length; j++) {
              p += j;
              partList.splice(indexes[p], 1);
              p = -1;
            }
            updated = true;
          }
        }
      } else {
        var ele = JSON.parse(JSON.stringify(partList[grpIndex].groupedParts[childpIndex]));
        if(part.product && ele.product && part.product.Id == ele.product.Id) return;
        partList[grpIndex].isOpen = true;
        partList[grpIndex].groupedParts.splice(childpIndex, 1);
        partList.splice(grpIndex + 1, 0, ele);
        updated = true;
      }
      if(updated) {
        //component.set('v.partList', []);
        component.set('v.partList', partList);
      }
    } catch(e) {
      console.log(e.message);
    }
  },
  showToast: function(title, type, message) {
    var toastEvent = $A.get("e.force:showToast");
    toastEvent.setParams({
      "title": title,
      "type": type,
      "message": message
    });
    toastEvent.fire();
  },
  toogleFilter: function(cmp) {
    var showFilter = cmp.get('v.showFilter');
    cmp.set('v.showFilter', !showFilter);
  }
})