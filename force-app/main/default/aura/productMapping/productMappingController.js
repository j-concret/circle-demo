({
  doInit: function(component, event, helper) {
      var workspaceAPI = component.find("workspace");
      workspaceAPI.getAllTabInfo().then(function(tabs) {
          tabs.forEach(function(tab,ind){
              (tab.subtabs).forEach(function(response,ind){
                  if((response.isSubtab && response.title == 'Loading...') || response.subtabs){
                      var focusedTabId = response.isSubtab ? response.tabId : response.subtabs[0].tabId;
                      workspaceAPI.setTabLabel({
                          tabId: focusedTabId,
                          label: " Product"
                      });
                      workspaceAPI.setTabIcon({
                          tabId: focusedTabId,
                          icon: "standard:product",
                          iconAlt: "Product"
                      });
                  }
              });
          });
      })
      .catch(function(error) {
          console.log(error);
      });

    helper.doInit(component,null);
  },
  updateParts: function(component, event, helper) {
    helper.updateParts(component);
  },
  handleGrpUngrp: function(component, event, helper) {
    helper.handleGrpUngrp(component, event);
    event.stopPropagation();
  },
  handleSelect: function(cmp, event) {
    var menuIte = event.getParam("value");
    var parcedValue = event.getParam("value").split(',');
    var selectedMenuItemValue = parcedValue[0];
    var selectedMenuLabelValue = parcedValue[1];
    cmp.set("v.selectedMenuItem", selectedMenuItemValue);
    cmp.set("v.selectedMenuLabel", selectedMenuLabelValue);
    var menuItems = cmp.find("menuItems");
    menuItems.forEach(function(menuItem) {
      if(menuItem.get("v.checked")) {
        menuItem.set("v.checked", false);
      }
      if(menuItem.get("v.value") === menuIte) {
        menuItem.set("v.checked", true);
      }
    });
  },
  showFilter: function(component, event, helper) {
    helper.toogleFilter(component);
  },
  refresh:function(component,event,helper){
    component.set('v.showSpinner', true);
    component.set('v.selectedStatuses',[]);
    component.set('v.reviewMember',null);
    component.set('v.partMatchingReview',null);
    component.set('v.showFilter', false);
    helper.doInit(component,null);
  },
  applyFilters: function(component,event,helper){
    var selectedStatuses = component.get('v.selectedStatuses');
    helper.toogleFilter(component);
    component.set('v.showSpinner', true);
    helper.doInit(component,selectedStatuses);
  }
});