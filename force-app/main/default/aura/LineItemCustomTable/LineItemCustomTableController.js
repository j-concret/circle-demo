({
    doInit : function(component, event, helper) {
        var recordId = component.get("v.customsClearanceDocumentId");
        var action = component.get("c.getLineItems");
        action.setParams({"recordId" : recordId});
        action.setCallback(this, function(response) { 
            var state = response.getState();
            var toastEvent = $A.get("e.force:showToast");
            if (state === 'SUCCESS'){
                var responseMap = response.getReturnValue();
                if(responseMap.status == 'OK'){
                    var   dataMap =   responseMap.data;
                  component.set("v.captureLineItemsList",dataMap);
                 }else{
                    toastEvent.setParams({
                        title : 'Error',
                        message:responseMap['message'],
                        duration:' 5000',
                        key: 'info_alt',
                        type: 'error',
                        mode: 'pester'
                    });
                    toastEvent.fire();
                }
            }
            else if (state === "INCOMPLETE") {
                toastEvent.setParams({
                    title : 'Error',
                    message:"Unknown error",
                    duration:' 5000',
                    key: 'info_alt',
                    type: 'error',
                    mode: 'pester'
                });
                toastEvent.fire();
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            toastEvent.setParams({
                                title : 'Error',
                                message:errors[0].message,
                                duration:' 5000',
                                key: 'info_alt',
                                type: 'error',
                                mode: 'pester'
                            });
                            toastEvent.fire();
                        }
                    } else {
                        toastEvent.setParams({
                            title : 'Error',
                            message:"Unknown error",
                            duration:' 5000',
                            key: 'info_alt',
                            type: 'error',
                            mode: 'pester'
                        });
                        toastEvent.fire();
                    }
                }
            
        });
        $A.enqueueAction(action);
    },
    updatePart: function(component, event, helper) {
   		var validityList = component.find("inputCmp");
       
        if(Array.isArray(validityList)){//if stament added by perfect Mathenjwa
        
            for(let validity of validityList){
            let validF = validity.get("v.validity");
            if(validF.patternMismatch){
                validity.setCustomValidity("Only numbers can be entered");
                validity.reportValidity();
             	return;
            }
        } 
    }else{
        let validF = validityList.get("v.validity");
        if(validF.patternMismatch){
            validityList.setCustomValidity("Only numbers can be entered");
            validityList.reportValidity();
            return;
        }
    }
        
        component.set('v.loaded',false);
        var action = component.get("c.updateParts");
        action.setParams({"parts" : component.get("v.captureLineItemsList")});
        action.setCallback(this, function(response) { 
            var state = response.getState();
            var toastEvent = $A.get("e.force:showToast");
            if (state === 'SUCCESS'){
                component.set('v.loaded',true);
                var responseMap = response.getReturnValue();
                if(responseMap.status == 'OK'){
                    var   dataMap =   responseMap.data;
                    component.set("v.captureLineItemsList",dataMap);
                    toastEvent.setParams({
                            title : 'Update',
                            message:'Line Items Updated Successfully !',
                            duration:' 5000',
                            key: 'info_alt',
                            type: 'success',
                            mode: 'pester'
                        });
                        toastEvent.fire(); 
                    
                }else{
                    
                    component.set('v.loaded',true);
                    toastEvent.setParams({
                        title : 'Error',
                        message:responseMap['message'],
                        duration:' 5000',
                        key: 'info_alt',
                        type: 'error',
                        mode: 'pester'
                    });
                    toastEvent.fire();
                }
            }
            else if (state === "INCOMPLETE") {
                component.set('v.loaded',true);
                toastEvent.setParams({
                    title : 'Error',
                    message:"Unknown error",
                    duration:' 5000',
                    key: 'info_alt',
                    type: 'error',
                    mode: 'pester'
                });
                toastEvent.fire();
            }
                else if (state === "ERROR") {
                    component.set('v.loaded',true);
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            toastEvent.setParams({
                                title : 'Error',
                                message:errors[0].message,
                                duration:' 5000',
                                key: 'info_alt',
                                type: 'error',
                                mode: 'pester'
                            });
                            toastEvent.fire();
                        }
                    } else {
                        component.set('v.loaded',true);
                        toastEvent.setParams({
                            title : 'Error',
                            message:"Unknown error",
                            duration:' 5000',
                            key: 'info_alt',
                            type: 'error',
                            mode: 'pester'
                        });
                        toastEvent.fire();
                    }
                }
            
        });
        $A.enqueueAction(action);
        
    }
})