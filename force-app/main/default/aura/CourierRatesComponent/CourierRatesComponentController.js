({
	doInit: function(component, event, helper) {
        helper.doInit(component, event);
    },
    onRowSelection: function (component, event) {
        var selectedRows = event.getParam('selectedRows');
        
        component.set("v.selectedRows", selectedRows[0].Id);
    },
    closeRateModal: function(component, event, helper) {
        component.set('v.isOpenRateComponent', false);
        var appEvent = $A.get('e.c:closeModal');
        appEvent.fire();
    },
    updateRate : function(component, event, helper) {
        helper.updateSelectedCourierHelper(component, event, helper);
    }
})