({
    doInit: function(component, event){
        component.set('v.showSpinner',true);
        component.set('v.columns', [
            {label: 'Courier Rates Name', fieldName: 'Name', type: 'text'},
            {label: 'Origion', fieldName: 'Origin__c', type: 'text'},
            {label: 'Carrier Name', fieldName: 'Carrier_Name__c', type: 'text'},
            {label: 'Service Type', fieldName: 'service_type__c', type: 'text'},
            {label: 'Final Rate', fieldName: 'Final_Rate__c', type: 'number'},
            {label: 'Status', fieldName: 'Status__c', type: 'text'},
            {label: 'Preferred', fieldName: 'Preferred__c', type: 'boolean'}
        ]);
        var action = component.get("c.getRatesData");
        action.setParams({
            recId: component.get('v.recordId')
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                component.set("v.rateList", result.rates);
                
                if(result.rates.length == 0){
                    this.showToast('Error','error','Preferred rates are not available.');
                }
                for (var index in result.rates) {
                    if(result.rates[index].Status__c === 'Selected'){
                        component.set("v.selectedRows", result.rates[index].Id);
                        break;
                    }
                }
                
                
            }
            component.set('v.showSpinner',false);
        });
        $A.enqueueAction(action);
    },
    updateSelectedCourierHelper : function(component, event, helper) {
        component.set('v.showSpinner',true);
        
        var action = component.get("c.updateSelectedCourierRate");
        action.setParams({
            rates : JSON.stringify(component.get('v.rateList')),
            selectedId : JSON.stringify(component.get('v.selectedRows'))
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            
            if(state == 'SUCCESS') {
                var result = response.getReturnValue();
                if(result.status == 'OK'){
                    helper.showToast('Rate Selected Successfully','success','New rate has been selected.');
                    helper.doInit(component, event, helper);
                }else if(state === "ERROR") {
                    var errors = response.getError();
                    if(errors) {
                        if(errors[0] && errors[0].message) {
                            this.showToast('Error','error',errors[0].message);
                        }
                    } else {
                        this.showToast('Error','error','Unknown Error');
                    }
                    
                }
            }
            component.set('v.showSpinner',false);
        });
        $A.enqueueAction(action);
    },
    showToast : function(title,type,msg) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title:title,
            type:type,
            message:msg
        });
        toastEvent.fire();
    }
})