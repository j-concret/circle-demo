({
    doInitHlpr: function(component, helper, insertCom, updateCom, searchCompany, searchUser) {
        component.set("v.showSpinner", true);
        var params = {searchCompanyString : searchCompany,
                      searchUserString : searchUser
                     };
        this.makeServerCall(component, "getData", params, function(result) {
            if(result.wrapperClass) {                
                component.set('v.userHasAccessToUpdateLeadCheckbox', result.userHasAccessToUpdateLeadCheckbox);
                component.set('v.userHasAccessToUpdateDNCCheckbox', result.userHasAccessToUpdateDNCCheckbox);
                component.set('v.userHasAccessToUpdateOwner', result.userHasAccessToUpdateOwner);
                var allCompanies = [];
                if(insertCom){
                    component.set('v.allCompanyUserAssignMData', result.wrapperClass.concat(result.companyWithNoUserAllocation));
                    component.set('v.allCompanyUserAssignMDataCopy', result.wrapperClass.concat(result.companyWithNoUserAllocation));
                    component.set('v.allCompanyUserAssignMDataCountryCopy', result.wrapperClass.concat(result.companyWithNoUserAllocation));
                    component.set("v.totalNoRecords", component.get("v.allCompanyUserAssignMData").length);
                    if(result.newUserToAllocate) {  
                        component.set('v.newUserToAllocate', result.newUserToAllocate);
                        component.set('v.newUserToAllocateCopy', result.newUserToAllocate);
                    }
                    if(result.allCompanies) {  
                        component.set('v.allCompanies', result.allCompanies);
                        component.set('v.allCompaniesCopy', result.allCompanies);
                    }
                }
                
                if(updateCom){
                    component.set('v.companyData', result.wrapperClass.concat(result.companyWithNoUserAllocation));
                    component.set('v.copyCompanyData', result.wrapperClass.concat(result.companyWithNoUserAllocation));
                    component.set('v.copyCompanyDataCountryCopy', result.wrapperClass.concat(result.companyWithNoUserAllocation));
                    component.set("v.totalNoRecords", component.get("v.companyData").length);
                    if(result.companyNames) {  
                        component.set('v.companyNamesCopy', result.companyNames);
                        component.set('v.companyNames', result.companyNames);
                    }
                    if(result.allocatedTo) {  
                        component.set('v.allocatedTo', result.allocatedTo);
                        component.set('v.allocatedToCopy', result.allocatedTo);
                    }
                }                
                
                if(result.stausPicklistValues) {  
                    component.set('v.stausPicklistValues', result.stausPicklistValues);
                    component.set('v.stausPicklistValuesCopy', result.stausPicklistValues);
                }
                                
            }
            else {
                helper.showToast("error", "Error Occured Helper", result.msg);
            }
            component.set("v.showSpinner", false);
        });
    },
    
    
    getUserLimitsData : function(component) {        
        component.set("v.showSpinner", true);
        var params = {};
        this.makeServerCall(component, "getAllUsers", params, function(result) {
            component.set("v.showSpinner", false);
            component.set( 'v.allUsersName', result.userNameList );
            component.set( 'v.allUsersNameCopy', result.userNameList );
            component.set('v.usersData', result.usersData);
            component.set('v.usersDataCopy', result.usersData);
        });
        
        
    },
    UpdateUsersLimit : function(component,helper) {
        var data = component.get('v.usersDateToSave');
        for(var i=0;i<data.length;i++){
            if(data[i].allocationLimit < data[i].companyAllocated){
                helper.showToast("Error","Error",'Increase Allocation limit of '+data[i].userName+"'s");
                return;
            }
         /*   if(data[i].creationLimit < data[i].companyCreated){
                helper.showToast("Error","Error",'Increase Creation limit of '+data[i].userName+"'s");
                return;
            }*/
        }
        component.set("v.showSpinner", true);
        var params = {usersData : JSON.stringify(component.get('v.usersDateToSave'))};
        this.makeServerCall(component, "updateUsers", params, function(result) {
            component.set("v.showSpinner", false);
            helper.showToast("SUCCESS","SUCCESS","Record Saved");
            $A.get('e.force:refreshView').fire();
        });
        
    },
    
    
    handlerChangeDualListUpdate: function(component,event,helper) {
        component.set("v.showSpinner", true);
        var selectedCompanyName = component.find("companyNames").get("v.value");
        
        var selectedStatusName = component.find("statusNames").get("v.value");
        var selectedUserNames = component.find("UserNames").get("v.value");
        var selectedKYCScore = component.find("KYCScore").get("v.value");
        
        let companyData = component.get("v.copyCompanyData");
        
        var filteredData = [];
        if(selectedCompanyName.length != 0 || selectedStatusName.length != 0 || selectedUserNames.length != 0 ||
           selectedKYCScore.length != 0){
            if(selectedCompanyName.length != 0){
                var filteredCompnayDataName = [];
                for(var i=0;i<companyData.length;i++){
                    for(var j=0;j<selectedCompanyName.length;j++){
                        if(companyData[i].companyData.Name == selectedCompanyName[j]){
                            filteredCompnayDataName.push(companyData[i]);
                        }
                    }
                }
                companyData =  filteredCompnayDataName;  
            }
            
            if(selectedStatusName.length != 0){
                var filteredStatusData = []
                for(var i=0;i<companyData.length;i++){
                    for(var j=0;j<selectedStatusName.length;j++){
                        if(companyData[i].companyData.Highest_Lead_Status__c == selectedStatusName[j]){
                            filteredStatusData.push(companyData[i]);
                        }
                    }
                }
                companyData =  filteredStatusData;  
            }
            if(selectedUserNames.length != 0){   
                var filteredUserNameData = [];
                for(var i=0;i<companyData.length;i++){
                    for(var j=0;j<selectedUserNames.length;j++){
                        if(companyData[i].userName == selectedUserNames[j]){
                            filteredUserNameData.push(companyData[i]);
                        }else if(companyData[i].companyData.Owner.Name == selectedUserNames[j]){
                            filteredUserNameData.push(companyData[i]);
                        }
                    }
                }
                companyData =  filteredUserNameData;  
            }
            
            if(selectedKYCScore.length != 0){   
                var filteredKYCScore = [];
                for(var i=0;i<companyData.length;i++){
                    for(var j=0;j<selectedKYCScore.length;j++){
                        if(selectedKYCScore[j] == 2){
                            if(companyData[i].companyData.KYC_Ranking_Score__c >= 0 && companyData[i].companyData.KYC_Ranking_Score__c <= 2){
                                filteredKYCScore.push(companyData[i]); 
                            }
                        }
                        if(selectedKYCScore[j] == 4){
                            if(companyData[i].companyData.KYC_Ranking_Score__c > 2  && companyData[i].companyData.KYC_Ranking_Score__c <= 4){
                                filteredKYCScore.push(companyData[i]); 
                            }
                        }
                        if(selectedKYCScore[j] == 6){
                            if(companyData[i].companyData.KYC_Ranking_Score__c > 4  && companyData[i].companyData.KYC_Ranking_Score__c <= 6){
                                filteredKYCScore.push(companyData[i]); 
                            }
                        }
                        
                        if(selectedKYCScore[j] == 8){
                            if(companyData[i].companyData.KYC_Ranking_Score__c > 6  && companyData[i].companyData.KYC_Ranking_Score__c <= 8){
                                filteredKYCScore.push(companyData[i]); 
                            }
                        }
                        if(selectedKYCScore[j] == 10){
                            if(companyData[i].companyData.KYC_Ranking_Score__c >8  && companyData[i].companyData.KYC_Ranking_Score__c <= 10){
                                filteredKYCScore.push(companyData[i]); 
                            }
                        }
                    }
                }
                companyData =  filteredKYCScore;  
            }
            
            component.set("v.companyData", companyData); 
            component.set("v.totalNoRecords", component.get("v.companyData").length);
            component.set("v.copyCompanyDataCountryCopy", companyData); 
            
        }else{
            component.set("v.companyData", companyData); 
            component.set("v.totalNoRecords", component.get("v.companyData").length);
            component.set("v.copyCompanyDataCountryCopy", companyData); 
        }
        component.set("v.showSpinner", false);        
    },
    
    handlerSelectRows: function(component,event,helper) {
        
        component.set("v.showSpinner", true);  
        let index = event.getSource().get('v.name');
        var selectedRecords = [];
        var companyRec;
        if(event.getSource().get('v.label') == 'toInsert'){
            
            selectedRecords = component.get("v.dataToInsert");
            companyRec = component.get("v.allCompanyUserAssignMData")[index];
            if(event.getSource().get('v.checked')){
                selectedRecords.push(companyRec);
                component.set("v.dataToInsert", selectedRecords);
                component.set("v.showSpinner", false);  
                return;
            }
        } else if(event.getSource().get('v.label') == 'toSave'){
            selectedRecords = component.get("v.dataToSave");
            companyRec = component.get("v.companyData")[index];
            if(event.getSource().get('v.checked')){
                selectedRecords.push(companyRec);
                component.set("v.dataToSave", selectedRecords);
                component.set("v.showSpinner", false);  
                return;
            }
        }
        
        let newSelectedRecords = [];
        for(var i=0;i<selectedRecords.length;i++){
            if(companyRec.compnayUserAssignmId  != selectedRecords[i].compnayUserAssignmId){
                newSelectedRecords.push(selectedRecords[i]);
            }
        }
        
        if(event.getSource().get('v.label') == 'toInsert'){
            component.set("v.dataToInsert", newSelectedRecords);
            component.set("v.showSpinner", false);
        }else if(event.getSource().get('v.label') == 'toSave'){
            component.set("v.dataToSave", newSelectedRecords);
            component.set("v.showSpinner", false);
        }          
        
    },
    validateUserWithLimitsUpdate:function(component,event,helper) {
        //  let selectedUserNames = component.find("UserNamesForInsert").get("v.value");
        let dataToSave = component.get("v.dataToSave");
        let userRecords = component.get("v.usersData");
        
        for(var i=0;i<dataToSave.length;i++){
            /*if(dataToSave[i].selectedNewUser){
                let filtered = userRecords.filter(function (e) {
                    return (e.userId == dataToSave[i].selectedNewUser.Id && ((e.companyAllocated + 1) > e.allocationLimit))
                }).map(function (obj) {
                    return obj;
                });
                if(filtered.length > 0){
                    helper.showToast("error", "Error", filtered[0].userName +' has Exhausted Allocation Limit.');
                    return;
                    
                }
            }*/
            if(dataToSave[i].newCompanyOwner){
                let filtered = userRecords.filter(function (e) {
                    return (e.userId == dataToSave[i].newCompanyOwner.Id && ((e.companyAllocated + 1) > e.allocationLimit))
                }).map(function (obj) {
                    return obj;
                });
                if(filtered.length > 0){
                    helper.showToast("error", "Error", filtered[0].userName +' has Exhausted Allocation Limit.');
                    return;
                    
                }
            }
        }
        helper.checkDuplicateRecords(component, event, helper);
    },
    checkDuplicateRecords: function(component,event,helper) {
        component.set("v.showSpinner", true);
        let data = component.get("v.dataToSave");
        let dataCopy = component.get("v.dataToSave");
        var objForDuplicates = new Object();
        for(var i=0;i<data.length;i++){
            for(var j=0;j<dataCopy.length;j++){
                if(dataCopy[j].companyData.Name == data[i].companyData.Name && 
                   dataCopy[j].companyData.Highest_Lead_Status__c == data[i].companyData.Highest_Lead_Status__c &&
                   dataCopy[j].companyData.Country__c == data[i].companyData.Country__c &&
                   dataCopy[j].companyData.userName == data[i].companyData.userName && 
                   dataCopy[j].companyData.KYC_Ranking_Score__c == data[i].companyData.KYC_Ranking_Score__c){
                    if(dataCopy[j].companyData.Extra_Leads_Approved__c != data[i].companyData.Extra_Leads_Approved__c ||
                       dataCopy[j].companyData.Strategic_DNC__c != data[i].companyData.Strategic_DNC__c){
                        helper.showToast("Error","Error",dataCopy[j].companyData.Name+' has duplicates record. CheckBox values are different.');
                        component.set("v.showSpinner", false);
                        return;
                    }
                    
                }
            }
        }
        
        helper.handlerSave(component, event, helper);
        
        
    },
    handlerSave: function(component,event,helper) {
        component.set("v.showSpinner", true);
        var data = component.get("v.dataToSave");
        for(var i=0;i<data.length;i++){
            if(data[i].companyData.Leads__r){
                delete data[i].companyData.Leads__r;
            }
            if(data[i].companyData.Owner){
                delete data[i].companyData.Owner;
            }
        }
        var params = {
            dataToSave : JSON.stringify(data)
        };
        this.makeServerCall(component, "updateRecords", params, function(result){
            if(result.status == 'Ok'){                
                helper.showToast("SUCCESS","SUCCESS","Record Saved");
                // helper.doInitHlpr(component, helper);
                window.location.reload()
                
            }
            else if(result.error){
                helper.showToast("error", "Error", result.error);
            }
            component.set("v.showSpinner", false);
        });
        
    },
    //ShowToast
    toastShow : function(title, type, msg) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title : title,
            message: msg,
            duration:' 5000',
            key: 'info_alt',
            type: type,
            mode: 'dismissible'
        });
        toastEvent.fire();
    },
    
    validateUserWithLimitsInsert:function(component,event,helper) {
        /*let selectedUserNames = component.find("UserNamesForInsert").get("v.value");
        let noOfRecords = component.get("v.dataToInsert").length;
        let userRecords = component.get("v.usersData");
        
        for(var i=0;i<selectedUserNames.length;i++){
            let filtered = userRecords.filter(function (e) {
                return (e.userId == selectedUserNames[i] && ((e.companyAllocated + noOfRecords) > e.allocationLimit))
            }).map(function (obj) {
                return obj;
            });
            if(filtered.length > 0){
                helper.showToast("error", "Error", filtered[0].userName +' has Exhausted Allocation Limit.');
                return;
            }
        }*/
        helper.insertCompanyUserAssignM(component, event, helper);
        
    },
    
    insertCompanyUserAssignM: function(component,event,helper) {
        var selectedUserNames = component.find("UserNamesForInsert").get("v.value");
        if(selectedUserNames.length > 0){            
            component.set("v.showSpinner", true);
            var data = component.get("v.dataToInsert");
            for(var i=0;i<data.length;i++){
                if(data[i].companyData.Leads__r){
                    delete data[i].companyData.Leads__r;
                    
                }
                if(data[i].companyData.Owner){
                    delete data[i].companyData.Owner;
                }
            }
            
            var params = {
                dataToInsert : JSON.stringify(data),
                userNames : selectedUserNames
            };
            this.makeServerCall(component, "insertCompanyUserAssignM", params, function(result){
                if(result.status == 'Ok'){                
                    helper.showToast("SUCCESS","SUCCESS","Record Saved");
                    window.location.reload()
                    
                    
                }
                else if(result.error){
                    helper.showToast("error", "Error", result.error);
                }
                component.set("v.showSpinner", false);
            });
        }else{
            helper.showToast("error", "Error", 'Select User for Allocation.');
        }
        
    },
    
    SearchRowiseInsertCompany: function(component,event,helper,searchString,records) {
        let filtered = records.filter(function (e) {
            if(e.companyData.Country__c){
                return (e.companyData.Country__c.toLowerCase().includes(searchString.toLowerCase()))
            }
        }).map(function (obj) {
            return obj;
        });
        return filtered;            
    },
    sortByCompanyFields: function(component,helper,event,field,records) {
        component.set("v.showSpinner", true);
        var sortAsc = component.get("v.sortAsc");        
        records.sort(function(a,b){
            var t1 = a.companyData[field] == b.companyData[field],
                t2 = (!a.companyData[field] && b.companyData[field]) || (a.companyData[field] < b.companyData[field]);
            return t1? 0: (sortAsc?-1:1)*(t2?1:-1);
        });
        component.set("v.sortAsc", !sortAsc);
        component.set("v.showSpinner", false);
        return records;
    },
    sortByCompanyOwnerName: function(component,helper,event,field,records) {
        component.set("v.showSpinner", true);
        var sortAsc = component.get("v.sortAsc");        
        records.sort(function(a,b){
            var t1 = a.companyData.Owner[field] == b.companyData.Owner[field],
                t2 = (!a.companyData.Owner[field] && b.companyData.Owner[field]) || (a.companyData.Owner[field] < b.companyData.Owner[field]);
            return t1? 0: (sortAsc?-1:1)*(t2?1:-1);
        });
        component.set("v.sortAsc", !sortAsc);
        component.set("v.showSpinner", false);
        return records;
    },
    sortByWrapperField: function(component,helper,event,field,records) {
        component.set("v.showSpinner", true);
        var sortAsc = component.get("v.sortAsc");
        // sortField = component.get("v.sortField"),
        
        //sortAsc = sortField != field || !sortAsc;
        records.sort(function(a,b){
            var t1 = a[field] == b[field],
                t2 = (!a[field] && b[field]) || (a[field] < b[field]);
            return t1? 0: (sortAsc?-1:1)*(t2?1:-1);
        });
        component.set("v.sortAsc", !sortAsc);
        // component.set("v.sortField", field);
        component.set("v.showSpinner", false);
        return records;
        
    },
    doSorting: function(component,event,helper) {
        switch(event.target.getAttribute("id")) {
            case 'CompanyNameInsert':  
                var records = helper.sortByCompanyFields(component,event, helper, 'Name',component.get("v.allCompanyUserAssignMData"));
                component.set("v.allCompanyUserAssignMData", records);
                if(component.get("v.sortAsc")){
                    component.find("CompanyNameInsert").set("v.iconName","utility:arrowup");
                }else{
                    component.find("CompanyNameInsert").set("v.iconName","utility:arrowdown");
                }
                
                break;
            case 'CountryNameInsert':
                var records=  helper.sortByCompanyFields(component,event, helper, 'Country__c',component.get("v.allCompanyUserAssignMData"));
                component.set("v.allCompanyUserAssignMData", records);   
                if(component.get("v.sortAsc")){
                    component.find("CountryNameInsert").set("v.iconName","utility:arrowup");
                }else{
                    component.find("CountryNameInsert").set("v.iconName","utility:arrowdown");
                }
                break;
            case 'CompanyOwnerInsert':
                var records=  helper.sortByCompanyOwnerName(component,event, helper, 'Name',component.get("v.allCompanyUserAssignMData"));
                component.set("v.allCompanyUserAssignMData", records);   
                if(component.get("v.sortAsc")){
                    component.find("CountryNameInsert").set("v.iconName","utility:arrowup");
                }else{
                    component.find("CountryNameInsert").set("v.iconName","utility:arrowdown");
                }
                break;
            case 'AllocationInsert':
                var records= helper.sortByWrapperField(component,event, helper, 'userName',component.get("v.allCompanyUserAssignMData"));
                component.set("v.allCompanyUserAssignMData", records);   
                if(component.get("v.sortAsc")){
                    component.find("AllocationInsert").set("v.iconName","utility:arrowup");
                }else{
                    component.find("AllocationInsert").set("v.iconName","utility:arrowdown");
                }
                break; 
            case 'StatusInsert':
                var records= helper.sortByCompanyFields(component,event, helper, 'Highest_Lead_Status__c',component.get("v.allCompanyUserAssignMData"));
                component.set("v.allCompanyUserAssignMData", records);   
                if(component.get("v.sortAsc")){
                    component.find("StatusInsert").set("v.iconName","utility:arrowup");
                }else{
                    component.find("StatusInsert").set("v.iconName","utility:arrowdown");
                }
                break;
            case 'NoOfLeadsInsert':
                var records= helper.sortByWrapperField(component,event, helper, 'noOfLeads',component.get("v.allCompanyUserAssignMData"));
                component.set("v.allCompanyUserAssignMData", records);   
                if(component.get("v.sortAsc")){
                    component.find("NoOfLeadsInsert").set("v.iconName","utility:arrowup");
                }else{
                    component.find("NoOfLeadsInsert").set("v.iconName","utility:arrowdown");
                }
                break;
            case 'AllocationDateInsert':
                var records= helper.sortByCompanyFields(component,event, helper, 'Allocation_Date__c',component.get("v.allCompanyUserAssignMData"));
                component.set("v.allCompanyUserAssignMData", records);   
                if(component.get("v.sortAsc")){
                    component.find("AllocationDateInsert").set("v.iconName","utility:arrowup");
                }else{
                    component.find("AllocationDateInsert").set("v.iconName","utility:arrowdown");
                }
                break;
            case 'ExpiryDateInsert':
                var records= helper.sortByCompanyFields(component,event, helper, 'Expiry_Date__c',component.get("v.allCompanyUserAssignMData"));
                component.set("v.allCompanyUserAssignMData", records);   
                if(component.get("v.sortAsc")){
                    component.find("ExpiryDateInsert").set("v.iconName","utility:arrowup");
                }else{
                    component.find("ExpiryDateInsert").set("v.iconName","utility:arrowdown");
                }
                break;
            case 'KYCRankingInsert':
                var records= helper.sortByCompanyFields(component,event, helper, 'KYC_Ranking_Score__c',component.get("v.allCompanyUserAssignMData"));
                component.set("v.allCompanyUserAssignMData", records);   
                if(component.get("v.sortAsc")){
                    component.find("KYCRankingInsert").set("v.iconName","utility:arrowup");
                }else{
                    component.find("KYCRankingInsert").set("v.iconName","utility:arrowdown");
                }
                break;
            case 'ExtraLeadApprovedInsert':
                var records= helper.sortByCompanyFields(component,event, helper, 'Extra_Leads_Approved__c',component.get("v.allCompanyUserAssignMData"));
                component.set("v.allCompanyUserAssignMData", records);   
                if(component.get("v.sortAsc")){
                    component.find("ExtraLeadApprovedInsert").set("v.iconName","utility:arrowup");
                }else{
                    component.find("ExtraLeadApprovedInsert").set("v.iconName","utility:arrowdown");
                }
                break;
            case 'StratgicDNCInsert':
                var records= helper.sortByCompanyFields(component,event, helper, 'Strategic_DNC__c',component.get("v.allCompanyUserAssignMData"));
                component.set("v.allCompanyUserAssignMData", records);   
                if(component.get("v.sortAsc")){
                    component.find("StratgicDNCInsert").set("v.iconName","utility:arrowup");
                }else{
                    component.find("StratgicDNCInsert").set("v.iconName","utility:arrowdown");
                }
                break;
                
            case 'CompanyNameUpdate':  
                var records =helper.sortByCompanyFields(component,event, helper, 'Name',component.get("v.companyData"));
                component.set("v.companyData", records);   
                if(component.get("v.sortAsc")){
                    component.find("CompanyNameUpdate").set("v.iconName","utility:arrowup");
                }else{
                    component.find("CompanyNameUpdate").set("v.iconName","utility:arrowdown");
                }
                
                break;
            case 'CountryNameUdpate':
                var records =helper.sortByCompanyFields(component,event, helper, 'Country__c',component.get("v.companyData"));
                component.set("v.companyData", records);   
                if(component.get("v.sortAsc")){
                    component.find("CountryNameUdpate").set("v.iconName","utility:arrowup");
                }else{
                    component.find("CountryNameUdpate").set("v.iconName","utility:arrowdown");
                }
                break;
            case 'CompanyOwnerNameUdpate':
                var records =helper.sortByCompanyOwnerName(component,event, helper, 'Name',component.get("v.companyData"));
                component.set("v.companyData", records);   
                if(component.get("v.sortAsc")){
                    component.find("CompanyOwnerNameUdpate").set("v.iconName","utility:arrowup");
                }else{
                    component.find("CompanyOwnerNameUdpate").set("v.iconName","utility:arrowdown");
                }
                break; 
            case 'AllocationUpdate':
                var records =helper.sortByWrapperField(component,event, helper, 'userName',component.get("v.companyData"));
                component.set("v.companyData", records);   
                if(component.get("v.sortAsc")){
                    component.find("AllocationUpdate").set("v.iconName","utility:arrowup");
                }else{
                    component.find("AllocationUpdate").set("v.iconName","utility:arrowdown");
                }
                break; 
            case 'HighestLeadUpdate':
                var records =helper.sortByCompanyFields(component,event, helper, 'Highest_Lead_Status__c',component.get("v.companyData"));
                component.set("v.companyData", records);   
                if(component.get("v.sortAsc")){
                    component.find("HighestLeadUpdate").set("v.iconName","utility:arrowup");
                }else{
                    component.find("HighestLeadUpdate").set("v.iconName","utility:arrowdown");
                }
                break;
            case 'NoOfLeadUpdate':
                var records =helper.sortByWrapperField(component,event, helper, 'noOfLeads',component.get("v.companyData"));
                component.set("v.companyData", records);   
                if(component.get("v.sortAsc")){
                    component.find("NoOfLeadUpdate").set("v.iconName","utility:arrowup");
                }else{
                    component.find("NoOfLeadUpdate").set("v.iconName","utility:arrowdown");
                }
                break;
            case 'AllocationDateUpdate':
                var records =helper.sortByCompanyFields(component,event, helper, 'Allocation_Date__c',component.get("v.companyData"));
                component.set("v.companyData", records);   
                if(component.get("v.sortAsc")){
                    component.find("AllocationDateUpdate").set("v.iconName","utility:arrowup");
                }else{
                    component.find("AllocationDateUpdate").set("v.iconName","utility:arrowdown");
                }
                break;
            case 'ExpiryDateUdpate':
                var records =helper.sortByCompanyFields(component,event, helper, 'Expiry_Date__c',component.get("v.companyData"));
                component.set("v.companyData", records);   
                if(component.get("v.sortAsc")){
                    component.find("ExpiryDateUdpate").set("v.iconName","utility:arrowup");
                }else{
                    component.find("ExpiryDateUdpate").set("v.iconName","utility:arrowdown");
                }
                break;
            case 'KYCRankingUdpate':
                var records =helper.sortByCompanyFields(component,event, helper, 'KYC_Ranking_Score__c',component.get("v.companyData"));
                component.set("v.companyData", records);   
                if(component.get("v.sortAsc")){
                    component.find("KYCRankingUdpate").set("v.iconName","utility:arrowup");
                }else{
                    component.find("KYCRankingUdpate").set("v.iconName","utility:arrowdown");
                }
                break;
            case 'ExtraLeadApproveUpdate':
                var records =helper.sortByCompanyFields(component,event, helper, 'Extra_Leads_Approved__c',component.get("v.companyData"));
                component.set("v.companyData", records);   
                if(component.get("v.sortAsc")){
                    component.find("ExtraLeadApproveUpdate").set("v.iconName","utility:arrowup");
                }else{
                    component.find("ExtraLeadApproveUpdate").set("v.iconName","utility:arrowdown");
                }
                break;
            case 'StratgicDNCUpdate':
                var records =helper.sortByCompanyFields(component,event, helper, 'Strategic_DNC__c',component.get("v.companyData"));
                component.set("v.companyData", records);   
                if(component.get("v.sortAsc")){
                    component.find("StratgicDNCUpdate").set("v.iconName","utility:arrowup");
                }else{
                    component.find("StratgicDNCUpdate").set("v.iconName","utility:arrowdown");
                }
                break;
            case 'UserNameInfo':
                var records =helper.sortByWrapperField(component,event, helper, 'userName',component.get("v.usersData"));
                component.set("v.usersData", records);   
                if(component.get("v.sortAsc")){
                    component.find("UserNameInfo").set("v.iconName","utility:arrowup");
                }else{
                    component.find("UserNameInfo").set("v.iconName","utility:arrowdown");
                }
                break;
            case 'CompanyAllocationLimitInfo':
                var records =helper.sortByWrapperField(component,event, helper, 'allocationLimit',component.get("v.usersData"));
                component.set("v.usersData", records);   
                if(component.get("v.sortAsc")){
                    component.find("CompanyAllocationLimitInfo").set("v.iconName","utility:arrowup");
                }else{
                    component.find("CompanyAllocationLimitInfo").set("v.iconName","utility:arrowdown");
                }
                break;
            case 'CompanyAllocatedInfo':
                var records =helper.sortByWrapperField(component,event, helper, 'companyAllocated',component.get("v.usersData"));
                component.set("v.usersData", records);   
                if(component.get("v.sortAsc")){
                    component.find("CompanyAllocatedInfo").set("v.iconName","utility:arrowup");
                }else{
                    component.find("CompanyAllocatedInfo").set("v.iconName","utility:arrowdown");
                }
                break;
            case 'CompanyCreationLimitInfo':
                var records =helper.sortByWrapperField(component,event, helper, 'creationLimit',component.get("v.usersData"));
                component.set("v.usersData", records);   
                if(component.get("v.sortAsc")){
                    component.find("CompanyCreationLimitInfo").set("v.iconName","utility:arrowup");
                }else{
                    component.find("CompanyCreationLimitInfo").set("v.iconName","utility:arrowdown");
                }
                break;
            case 'CompanyCreatedInfo':
                var records =helper.sortByWrapperField(component,event, helper, 'companyCreated',component.get("v.usersData"));
                component.set("v.usersData", records);   
                if(component.get("v.sortAsc")){
                    component.find("CompanyCreatedInfo").set("v.iconName","utility:arrowup");
                }else{
                    component.find("CompanyCreatedInfo").set("v.iconName","utility:arrowdown");
                }
                break;
        }
    }
    
})