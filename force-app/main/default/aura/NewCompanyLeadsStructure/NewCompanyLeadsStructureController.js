({
    doInitCtrl: function(component, event, helper) {
        helper.doInitHlpr(component, helper, true, true, '', '');
        helper.getUserLimitsData(component);
    },
    
    handleChangeDualListUpdate: function(component, event, helper) {
        helper.handlerChangeDualListUpdate(component, event, helper);
    },
    selectRows: function(component, event, helper) {
        helper.handlerSelectRows(component, event, helper);
    },
    onSelectTab: function(component, event, helper) {
        if(component.get("v.selectedTab") == 'Company Allocation Insert'){
            component.set("v.totalNoRecords", component.get("v.allCompanyUserAssignMData").length);
        }else if(component.get("v.selectedTab") == 'Company Allocation Update'){
            component.set("v.totalNoRecords", component.get("v.companyData").length);
        }else if(component.get("v.selectedTab") == 'User Information'){
            component.set("v.totalNoRecords", component.get("v.usersData").length);
        }
    },
    save: function(component, event, helper) {
        if(component.get("v.selectedTab") == 'Company Allocation Insert'){
            if(component.get("v.dataToInsert").length > 0){
                helper.validateUserWithLimitsInsert(component, event, helper);
                //helper.insertCompanyUserAssignM(component, event, helper);
            }else{
                helper.showToast("error", "Error", 'Select Records !');
            }
            
        }else if(component.get("v.selectedTab") == 'Company Allocation Update'){
            if(component.get("v.dataToSave").length > 0){
                helper.validateUserWithLimitsUpdate(component, event, helper);
                
            }else{
                helper.showToast("error", "Error", 'Select Records !');
            }
        }
        
    },
    handleClickNavigate: function(component, event, helper) {
        var navService = component.find("navService");
        var pageReference = {    
            "type": "standard__recordPage", //example for opening a record page, see bottom for other supported types
            "attributes": {
                "recordId": event.currentTarget.name, //place your record id here that you wish to open
                "actionName": "view"
            }
        }
        navService.navigate(pageReference);
    },
    searchCompany: function(component, event, helper) {
        let searchString = event.getSource().get('v.value');
        let companyName = component.get("v.companyNamesCopy");
        var selectedCompanyName;
        selectedCompanyName = component.find("companyNames").get("v.value");
        
        if(searchString != null && searchString != '' && companyName.length > 0){
            let filtered = companyName.filter(function (e) {
                return (e.label.toLowerCase().includes(searchString.toLowerCase()) || selectedCompanyName.includes(e.value))
            }).map(function (obj) {
                return obj;
            });
            component.set("v.companyNames", filtered);
        }else{
            component.set("v.companyNames", component.get("v.companyNamesCopy"));
        }
        
    },
    searchUser: function(component, event, helper) {
        let searchString = event.getSource().get('v.value');
        var selectedUserNames;
        selectedUserNames = component.find("UserNames").get("v.value");
        let userNames = component.get("v.allocatedToCopy");
        if(searchString != null && searchString != '' && userNames.length > 0){
            let filtered = userNames.filter(function (e) {
                return (e.label.toLowerCase().includes(searchString.toLowerCase()) || selectedUserNames.includes(e.value))
            }).map(function (obj) {
                return obj;
            });
            component.set("v.allocatedTo", filtered);
        }else{
            component.set("v.allocatedTo", component.get("v.allocatedToCopy"));
        }
        
    },
    searchStatus: function(component, event, helper) {
        let searchString = event.getSource().get('v.value');
        let statusNames = component.get("v.stausPicklistValuesCopy");
        var selectedStatusName;
        selectedStatusName = component.find("statusNames").get("v.value");
        if(searchString != null && searchString != '' && statusNames.length > 0){
            let filtered = statusNames.filter(function (e) {
                return (e.label.toLowerCase().includes(searchString.toLowerCase()) || selectedStatusName.includes(e.value))
            }).map(function (obj) {
                return obj;
            });
            component.set("v.stausPicklistValues", filtered);
        }else{
            component.set("v.stausPicklistValues", component.get("v.stausPicklistValuesCopy"));
        }
        
    },
    searchFromServer: function(component, event, helper) {
        if(component.get("v.selectedTab") == 'Company Allocation Insert'){
            
            if(!component.find('searchCompanyInsertDualist').get('v.value')){
                helper.showToast("Error","Error","Please type something");
            }else{
                helper.doInitHlpr(component, helper, true, false, component.find('searchCompanyInsertDualist').get('v.value'), '');
            }
        }
        else if(component.get("v.selectedTab") == 'Company Allocation Update'){
            if(!component.find('searchCompanyUpdateDualist').get('v.value') && !component.find('searchUserUpdateDualist').get('v.value')){
                helper.showToast("Error","Error","Please type something");
            }else {
                helper.doInitHlpr(component, helper, false, true,component.find('searchCompanyUpdateDualist').get('v.value'), component.find('searchUserUpdateDualist').get('v.value'));
            }   
        }
    },
    searchUserInsert: function(component, event, helper) {
        let searchString = event.getSource().get('v.value');
        var selectedUserNames;
        selectedUserNames = component.find("UserNamesForInsert").get("v.value");
        let userNames = component.get("v.newUserToAllocateCopy");
        if(searchString != null && searchString != '' && userNames.length > 0){
            let filtered = userNames.filter(function (e) {
                return (e.label.toLowerCase().includes(searchString.toLowerCase()) || selectedUserNames.includes(e.value))
            }).map(function (obj) {
                return obj;
            });
            component.set("v.newUserToAllocate", filtered);
        }else{
            component.set("v.newUserToAllocate", component.get("v.newUserToAllocateCopy"));
        }
        
    },
    handleChangeCompanyNameForInsert:function(component, event, helper) {
        var selectedCompanyName;
        selectedCompanyName = component.find("companyNamesForInsert").get("v.value");
        let options=component.get("v.allCompanies");
        let filtered = options.filter(function (e) {
            return (selectedCompanyName.includes(e.value))
        }).map(function (obj) {
            return obj;
        });        
        let companyData = component.get("v.allCompanyUserAssignMDataCopy");
        var filteredData = [];
        if(filtered.length != 0 ){
            var filteredCompnayDataName = [];
            for(var i=0;i<companyData.length;i++){
                for(var j=0;j<filtered.length;j++){
                    if(companyData[i].companyData.Name == filtered[j].label){
                        filteredCompnayDataName.push(companyData[i]);
                    }
                }
            }
            component.set("v.allCompanyUserAssignMData",filteredCompnayDataName); 
            component.set("v.allCompanyUserAssignMDataCountryCopy",filteredCompnayDataName); 
            component.set("v.totalNoRecords", filteredCompnayDataName.length);
            
        }else{
            component.set("v.allCompanyUserAssignMDataCountryCopy",companyData); 
            component.set("v.allCompanyUserAssignMData",companyData); 
            component.set("v.totalNoRecords", companyData.length);
        }
        
    },
    handlerSelectRows: function(component,event,helper) {
        
        // component.set("v.showSpinner", true);  
        let index = event.getSource().get('v.name');
        let selectedRecords = component.get('v.usersDateToSave');
        let userRec = component.get('v.usersData')[index];
        if(!selectedRecords){
            selectedRecords = [];
        }
        if(event.getSource().get('v.checked')){
            selectedRecords.push(userRec);
            component.set("v.usersDateToSave", selectedRecords);
            //component.set("v.showSpinner", false);  
            return;
        }
        
        for(var i=0;i<selectedRecords.length;i++){
            if(userRec.userName == selectedRecords[i].userName){
                selectedRecords.splice(index, 1);
            }
        }
        component.set("v.usersDateToSave", selectedRecords);
        //component.set("v.showSpinner", false);        
        console.log(component.get('v.usersDateToSave'));  
    },
    saveUserLimits : function(component,event, helper) {
        if(!component.get("v.usersDateToSave").length > 0){
            helper.showToast("Error","Error","Select Records !");
            return;
        }
        helper.UpdateUsersLimit(component,helper);    
        
    },
    handleDualListChangeForUserLimit: function(component,event, helper) {
        var selectedCompanyName = component.find("userTabUserList").get("v.value");
        let userData = component.get("v.usersDataCopy");
        var filteredData = [];
        if(selectedCompanyName.length != 0 ){
            var filteredCompnayDataName = [];
            for(var i=0;i<userData.length;i++){
                for(var j=0;j<selectedCompanyName.length;j++){
                    if(userData[i].userName == selectedCompanyName[j]){
                        filteredCompnayDataName.push(userData[i]);
                    }
                }
            }
            component.set("v.usersData",filteredCompnayDataName);  
            component.set("v.totalNoRecords", filteredCompnayDataName.length);
        }else{
            component.set("v.usersData",userData); 
            component.set("v.totalNoRecords", userData.length);
        }
    },
    userSelectedForLimitChange: function(component,event, helper) {
        let searchString = event.getSource().get('v.value');
        var selectedUserNames;
        selectedUserNames = component.find("userTabUserList").get("v.value");
        let userNames = component.get("v.allUsersNameCopy");
        if(searchString != null && searchString != '' && userNames.length > 0){
            let filtered = userNames.filter(function (e) {
                return (e.label.toLowerCase().includes(searchString.toLowerCase()) || selectedUserNames.includes(e.value))
            }).map(function (obj) {
                return obj;
            });
            component.set("v.allUsersName", filtered);
        }else{
            component.set("v.allUsersName", component.get("v.allUsersNameCopy"));
        }
    },
    searchCountryInsert: function(component,event, helper) {
        let searchString = event.getSource().get('v.value');
        component.set("v.showSpinner", true);
        if(event.getSource().get('v.title') == 'Search Country Insert'){
            if(searchString != null && searchString != ''){
                var result = helper.SearchRowiseInsertCompany(component,event, helper,searchString,component.get("v.allCompanyUserAssignMDataCountryCopy"));
                component.set("v.allCompanyUserAssignMData", result);
                component.set("v.totalNoRecords", result.length);
            }else{
                component.set("v.allCompanyUserAssignMData", component.get("v.allCompanyUserAssignMDataCountryCopy"));
                component.set("v.totalNoRecords", component.get("v.allCompanyUserAssignMDataCountryCopy").length);
            }
        }else if(event.getSource().get('v.title') == 'Search Country Update'){
            if(searchString != null && searchString != ''){
                var result = helper.SearchRowiseInsertCompany(component,event, helper,searchString,component.get("v.companyData"));
                component.set("v.companyData", result);
                component.set("v.totalNoRecords", result.length);
            }else{
                component.set("v.companyData", component.get("v.copyCompanyDataCountryCopy"));
                component.set("v.totalNoRecords", component.get("v.copyCompanyDataCountryCopy").length);
            }
        }
        component.set("v.showSpinner", false);
        
        
    },
    sortingHandler: function(component,event, helper) {
        helper.doSorting(component,event,helper);
    },
    
    checkInputString: function(component,event, helper) {
        switch(event.getSource().get('v.name')){
            case 'SearchcompanyNameInsert':
                if(!event.getSource().get('v.value')){
                    helper.doInitHlpr(component, helper, true, false, component.find('searchCompanyInsertDualist').get('v.value'), '');
                }
                break;
            case 'SearchUserNameInsert':
                if(!event.getSource().get('v.value')){
                    helper.doInitHlpr(component, helper, true, false, component.find('searchCompanyInsertDualist').get('v.value'), '');
                }
                break;
            case 'SearchCompanyNameUpdate':
                if(!event.getSource().get('v.value')){
                    helper.doInitHlpr(component, helper, false, true,component.find('searchCompanyUpdateDualist').get('v.value'), component.find('searchUserUpdateDualist').get('v.value'));
                }
                break;
            case 'SearchUserNameUpdate':
                if(!event.getSource().get('v.value')){
                    helper.doInitHlpr(component, helper, false, true,component.find('searchCompanyUpdateDualist').get('v.value'), component.find('searchUserUpdateDualist').get('v.value'));
                }
                break;
        }
    }
    
})