({
  doInit : function(cmp, event, helper) {
    var state = cmp.get('v.state');
    var color = (state === 'Not Started' || state === 'TecEx Pending' || state === 'Under Review') ? '#fde699' : state === 'Client Pending'?'#f1a5a5':'#c5e0b5';
    cmp.set('v.color',color);
    var states = cmp.get('v.states');
      console.log('states');
		console.log(states);
    if(states && states[state]){
      var objs = JSON.parse(JSON.stringify(states[state]));
      objs.sort((a,b) => (a.sortOrder > b.sortOrder) ? 1 : ((b.sortOrder > a.sortOrder) ? -1 : 0));
      console.log('objs');
		console.log(objs);
        cmp.set('v.tasks',objs);
    }
  },
  redirectToRelated: function(cmp, event, helper) {
    var selectedItem = event.currentTarget;// this will give current element
    var recId = selectedItem.dataset.redirectid;
    helper.redirectToObject(cmp,recId);
  },
  handleSelect : function(cmp,event,helper){
    var val = event.getParam('value');
    helper.updateSoTask(cmp,val);
  }
});