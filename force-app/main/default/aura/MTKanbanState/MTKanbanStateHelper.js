({
  redirectToObject : function(cmp,recId) {
    var pageRef = {
      type: 'standard__recordPage',
      attributes: {
        recordId: recId,
        objectApiName: 'Task',
        actionName: 'view'
      },
      state: {
        c__view: cmp.get('v.displayedTo')
      }
    };
    cmp.find('navService').navigate(pageRef);
  },
  updateSoTask : function(cmp,val) {
    var self = this;
    cmp.set('v.showSpinner',true);
    self.serverCall(cmp,'updateTask',{
      taskIdState : val
    },function(response){
      cmp.getEvent('refreshKanban').fire();
    },function(err){
      cmp.set('v.showSpinner',false);
      self.showToast('error','ERROR',err);
    });
  },
  showToast : function(type, title , msg) {
    var toastEvent = $A.get('e.force:showToast');
    toastEvent.setParams({
      'type':type,
      'title': title ,
      'message': msg
    });
    toastEvent.fire();
  }
});