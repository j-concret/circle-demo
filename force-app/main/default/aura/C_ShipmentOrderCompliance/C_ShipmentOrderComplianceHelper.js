({
    doInitHlpr: function(component, helper) {
        component.set("v.showSpinner", true);
        var params = {
            shipmentId: component.get("v.recordId")
        };
        this.makeServerCall(component, "getComplianceData", params, function(result) {
            if(result.status === 'OK') {
                
                
                
                if(result.data){
                    if(result.data.length> 5){
                        component.set("v.scroll", true);
                    }
                }
                //shelper.setLookups
                component.set("v.billsOfMaterial", result.data);                
                component.set("v.productSpecs", result.data);
                component.set("v.copyData", result.data);
                component.set("v.business", result.appName);
            }
            else {
                helper.showToast("error", "Error Occured Helper", result.msg);
            }
            component.set("v.showSpinner", false);
        });
    },
    
    //Handle Picklist value of category and Sub category.
    getPicklistValues : function(component, event, helper) {
        var par = {object_name : 'Product2', 
                   field_names : ['Category__c', 'Wireless_capability__c', 'Encryption__c','Li_on_batteries__c','Suggested_Country_of_Origin__c']};
        this.makeServerCall(component, 'getPicklistsValues', par , function(response) {
            component.set("v.categoryPicklist", response.Category__c);
            component.set("v.wirelessCapabilityPicklist", response.Wireless_capability__c);
            component.set("v.encryption", response.Encryption__c);
            component.set("v.lionBatteries", response.Li_on_batteries__c);
            component.set("v.suggestedCountry", response.Suggested_Country_of_Origin__c);
        });
    },
    
    //Server call to get dependent Picklist value
    getDependentPicklist : function(component, event, helper) {
        var par = {sObjectName : 'Product2',
                   fieldName : 'Sub_Category__c'};
        this.makeServerCall(component, 'getDependentPicklistValues', par , function(response) {
            console.log('Hello');
            console.log(response);
            component.set("v.controlledPicklistValues", response);
            
        });
    },
    
    // Call server to save records
    saveRecords  : function(component, event, helper) {
        var lst =[];
        var parts = component.get("v.productSpecs");
        for(var i=0; i<parts.length; i++){
            delete  parts[i].subCat;
            if(!parts[i].Product__r || !parts[i].Name){
                if(!parts[i].Product__r.Name.trim() || !parts[i].Name.trim()){
                    helper.toastShow('info', 'info', 'Fill Required fields');
                    return;
                }
            }
            
        }
        for(var i=0; i<parts.length; i++){
            
            if(!parts[i].Product__c){
                
                delete parts[i].Product__r.Name;
                delete parts[i].Product__r.Description;
                delete parts[i].Product__r.Manufacturer__c;
                delete parts[i].Product__r.Category__c;
                delete parts[i].Product__r.Sub_Category__c;
                delete parts[i].Product__r.US_HTS_Code__c;
                delete parts[i].Product__r.ECCN__c;
                delete parts[i].Product__r.MSRP__c;
                delete parts[i].Product__r.Regulatory_Number__c;
                delete parts[i].Product__r.Wireless_capability__c;
                delete parts[i].Product__r.Encryption__c;
                delete parts[i].Product__r.Li_on_batteries__c;
                delete parts[i].Product__r.Suggested_Country_of_Origin__c;
                delete parts[i].Product__r.Manufacturer__r;
            }
            if(parts[i].Product__c){
                if(parts[i].Product__r.Manufacturer__c && parts[i].Product__r.Manufacturer__c.val){
                    var id = parts[i].Product__r.Manufacturer__c.val;
                    parts[i].Product__r.Manufacturer__c = id;
                    delete parts[i].Product__r.Manufacturer__r;
                }else{
                    parts[i].Product__r.Manufacturer__c = '';
                }
            }
            if(parts[i].Matched_HS_Code2__c && parts[i].Matched_HS_Code2__c.val){
                var id = parts[i].Matched_HS_Code2__c.val;
                parts[i].Matched_HS_Code2__c = id;
            }
            else{
                parts[i].Matched_HS_Code2__c = '';
            }
            lst.push(parts[i]);
        }
        
        component.set("v.showSpinner", true);
        var params = {
            partObj : JSON.stringify(lst)
        };
        this.makeServerCall(component, 'updatePartData', params, function(result) {
            if(result.status === 'OK') {
                
                helper.showToast('Success', 'Record Updated', 'Success');
                component.set("v.showEditProductSpecs", false);
                helper.doInitHlpr(component, event, helper);
            }
            else {
                helper.showToast('error', 'Error Occured', result.data);
            }
            component.set("v.showSpinner", false);
        });
    },
    
    //Handles sub category picklist
    handleSubPicklist: function(component, event, helper) {
        var data = component.get("v.productSpecs");
        try{
            for(var i=0; i<data.length; i++){
                if(data[i].Product__r){
                    var subCat = component.get("v.controlledPicklistValues")[0][data[i].Product__r.Category__c];
                    data[i].subCat =  subCat;
                }
                
            }
            component.set("v.productSpecs", data); 
            component.set("v.showSpinner", false);    
        }catch(e){
            component.set("v.showSpinner", false);
            helper.showToast('error', 'Error while handling SubPicklist', '');
            return;
        }
        
    },
    
    subCategoryvalue: function(component, event, helper) {
        var data = component.get("v.productSpecs");
        var cat = event.getSource().get('v.value');
        var index = event.getSource().get('v.name');
        data[index].subCat = component.get("v.controlledPicklistValues")[0][cat];
        
        component.set("v.productSpecs", data);
    },
    
    //ShowToast
    toastShow : function(title, type, msg) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title : title,
            message: msg,
            duration:' 5000',
            key: 'info_alt',
            type: type,
            mode: 'dismissible'
        });
        toastEvent.fire();
    },
    
    ProductDoinit: function(component, event, helper) {
        component.set("v.showSpinner", true);
        var params = {
            recordId: component.get("v.recordId")
        };
        this.makeServerCall(component, "getShipmentDetails", params, function(result) {
            if(result.status === 'OK') {
                
                component.set("v.shipmentOrderData", result.shipmentOrder);
                component.set("v.attachments", result.attachments);
                component.set("v.shipmentOrderSpecficDocBool", true);
            }
            else {
                helper.showToast("error", "Error Occured", result.msg);
            }
            component.set("v.showSpinner", false);
        });
    },
    saveParts: function(component, event, helper) {
        if(component.get("v.billsOfMaterial").length > 0 || component.get("v.deletedParts").length > 0){
            component.set("v.showSpinner", true);
            
            var params = {
                partDataToUpdate: component.get("v.billsOfMaterial"),
                partDataToDelete: component.get("v.deletedParts")
            };
            this.makeServerCall(component, "updatePartsData", params, function(result) {
                if(result.status === 'OK') {
                    component.set("v.showEditBillOfMaterials", !component.get("v.showEditBillOfMaterials"));
                    helper.doInitHlpr(component, event, helper);
                }
                else {
                    helper.showToast("error", "Error Occured", result.data);
                    
                }
                component.set("v.showSpinner", false);
            });
        }
        
    },
    
   
    handleDeleteRow: function(component, event, helper) {
        var index = event.getSource().get('v.value');
        var deletedpart = component.get("v.deletedParts");
        var parts = component.get("v.billsOfMaterial");
        deletedpart.push(parts[index]);
        component.set("v.deletedParts", deletedpart);
        parts.splice(index, 1);
        if(!parts[0]){
            component.set("v.billsOfMaterial", []);
            return;
            
        }
        component.set("v.billsOfMaterial", parts);
        
        
    },
    deleteLineItems : function(component,recordId,helper){
        component.set("v.showSpinner", true);
        var params = {
            recordId: component.get("v.recordId")
        };
        this.makeServerCall(component,"deleteAllLineItems",params, function(result){
            component.set("v.showSpinner", false);
            helper.showToast("success", "Success", "All LineItems Deleted Successfully");
            component.set("v.billsOfMaterial",[]);
            $A.get('e.force:refreshView').fire();
        },function(err){
            component.set("v.showSpinner", false);
            helper.showToast("error", "Error Occured", err);
        });
    }
    
})