({
    doInitCtrl: function(component, event, helper) {
        if(component.get("v.recordId")) {
            helper.doInitHlpr(component, helper);
            helper.getPicklistValues(component, event, helper);
            helper.getDependentPicklist(component, event, helper);
            helper.ProductDoinit(component, event, helper);
        }
        else {
            helper.showToast("error", "Error Occured", "Invalid record!");
        }
    },
    
    //Make Editable to Product Spec tab and Vice versa
    editProductSpecs : function(component, event, helper) {
        if(component.get("v.productSpecs").length > 0){
            component.set("v.showSpinner", true);
            if(component.get("v.showEditProductSpecs")){
                component.set("v.productSpecs", component.get("v.copyData"));
                component.set("v.showEditProductSpecs", false);
            }else{
            helper.handleSubPicklist(component, event, helper);
            component.set("v.showEditProductSpecs", true);
            }
            component.set("v.showSpinner", false);
        }
    },
    
    //Save Product Records
    Save: function(component, event, helper) {
        helper.saveRecords(component, event, helper);
    },
    
    //Handle Sub Cateroy Value when category value selected
    selectCategoryValue: function(component, event, helper) {
        helper.subCategoryvalue(component, event, helper);
    },
    
    //Naviagte to Product/Parts records
    handleClickNavigate: function(component, event, helper) {
        var navService = component.find("navService");
        var pageReference = {    
            "type": "standard__recordPage", //example for opening a record page, see bottom for other supported types
            "attributes": {
                "recordId": event.currentTarget.name, //place your record id here that you wish to open
                "actionName": "view"
            }
        }
        
        navService.generateUrl(pageReference)
        .then($A.getCallback(function(url) {
            console.log('success: ' + url); 
            window.open(url,'_blank'); 
        }), 
              $A.getCallback(function(error) {
                  console.log('error: ' + error);
              }));
    },
    
    //Shows Shipment Order Chatter functionality
    handleShowChatterSectionCtrl: function(component, event, helper) {
        component.set("v.showChatterSection", !component.get("v.showChatterSection"));
    },
    
    //Open Add line item component in new tab.
    addLineItems: function(component, event, helper) {
        var pageReference = {
            type: 'standard__component',
            attributes: {
                componentName: 'c__AddLineItemOnNewSO',
            },
            state: {
                "c__recordId": component.get("v.recordId"),
                "c__newSOPage": true,
                "c__business": component.get("v.business")
            }
        };
        
        var navService = component.find("navService").navigate(pageReference);;    
    },
    
    //Edit bill of material tab records
    editBillOfMaterials : function(component, event, helper) {
        if(component.get("v.billsOfMaterial").length > 0){
            component.set("v.showEditBillOfMaterials", !component.get("v.showEditBillOfMaterials"));
            if(!component.get("v.showEditBillOfMaterials")){
                component.set("v.billsOfMaterial", component.get("v.copyData"));    
            }
        }
        
        
    },
    
    //Update Bill of material tab
    UpdateBillsOfMaterial: function(component, event, helper) {
        helper.saveParts(component, event, helper);
    },
    
    //delete row of Bills of material tab's records
    handleDeletePart: function(component, event, helper) {
        helper.handleDeleteRow(component, event, helper);
    },
    deleteLineItems : function(component, event, helper){
        if(component.get("v.billsOfMaterial").length > 0){
            helper.deleteLineItems(component,component.get("v.recordId"),helper);
        }
    },
    recordUpdated : function(component, event, helper) {
      var changeType = event.getParams().changeType;
      if (changeType === "CHANGED") {
        component.find("shipmentOrderRecord").reloadRecord();}
    },
    refreshCmp: function(component, event, helper) {
      component.set('v.showChatterSection',!component.get('v.showChatterSection'));
      component.set('v.showChatterSection',!component.get('v.showChatterSection'));
    }
})