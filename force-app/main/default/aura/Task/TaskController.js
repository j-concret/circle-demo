({
  doInit:function(component,event,helper){
    component.set('v.style','<style>.slds-popover_tooltip {top: -500px;}</style>')
  },
  onDragStart: function(component, event, helper) {
    if(component.get('v.isGrouped')) {
      event.preventDefault();
      return;
    }
    event.dataTransfer.dropEffect = "move";
    var item = component.get('v.taskObj');
    event.dataTransfer.setData('task', JSON.stringify(item));
    //component.set('v.task',{});
  },
  redirectToRelated: function(component, event, helper) {
    var selectedItem = event.currentTarget;// this will give current element
    var recId = selectedItem.dataset.redirectid;
    helper.redirectToObject(recId);
  },
  handleSelect: function(component, event, helper) {
    var selectedMenuItemValue = event.getParam("value");
    switch(selectedMenuItemValue) {
      case 'mark_In_Progress':
        helper.updateTaskStatus(component, 'In Progress');
        break;
      case 'mark_Complete':
        helper.updateTaskStatus(component, 'Completed');
        break;
      case 'mark_Not_Applicable':
        component.set('v.showModal', true);
        break;
    }
  },
  updateNotApplicable: function(component, event, helper) {
    component.set('v.showModal', false);
    helper.updateTaskStatus(component, 'Not Applicable');
  },
  expandTasks: function(component, event, helper) {
    var updateTskEvt = component.getEvent('expanCollapseEvt');
    updateTskEvt.fire();
  }
})