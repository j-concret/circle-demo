({
    updateTaskStatus: function(component, status) {
        component.set('v.showSpinner',true);
        var self = this;
        var task = component.get('v.taskObj');
        self.serverCall(component, 'updateTask', {'taskId': task.id, 'status': status,'reason':task.reasonForNotApplicable}, function(response) {
            component.set('v.showSpinner',false);
            if(response.status === 'OK') {
                //$A.get('e.force:refreshView').fire();
                $A.get("e.c:refreshEvent").fire();
                var updateTskEvt = component.getEvent('updateTaskEvent');
                var oldStatus = task.status;
                task.status = status;
                if(oldStatus.includes('Applicable'))
                    task.reasonForNotApplicable = '';
                updateTskEvt.setParams({
                    'task': task,
                    'OldStatus':oldStatus
                });
                updateTskEvt.fire();
            }else{
                self.showToast(response.status,'error',response.msg);
            }
        });
    },
    redirectToObject : function(objId){
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            "recordId": objId,
            "slideDevName": "Detail"
        });
        navEvt.fire();
    },
    showToast : function(title, type , msg) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": title,
            "type":type,
            "message": msg
        });
        toastEvent.fire();
    }
})