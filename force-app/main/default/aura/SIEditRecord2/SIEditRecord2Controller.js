({
	init: function (cmp, event, helper) {
       
         var SupplierInvoiceIDfromrecord = cmp.get("v.recordId");
         console.log("SupplierInvoiceIDfromrecord",SupplierInvoiceIDfromrecord);
        cmp.set("v.soidFromFlow1", SupplierInvoiceIDfromrecord);
        helper.fetchData(cmp,event, helper);
        // Supplier Invoice Summmary
        var SupplierInvoiceID = cmp.get("v.soidFromFlow1"); // supplierINvoice ID for record attach
       
        var actionSupplierInvoice = cmp.get("c.getSupplierInvoice"); 
            
       
        cmp.set("v.iframeUrl2","https://tecex.force.com/SupplierPortal/s/supplier-invoice/"+ SupplierInvoiceID+"/view");
        
        actionSupplierInvoice.setParams({
            SupplierInvoiceID1 : cmp.get("v.soidFromFlow1")
        });
        
        //   Add callback behavior for when response is received
    	actionSupplierInvoice.setCallback(this, function(response) {
        	var state = response.getState();
             console.log("success state ",state); // debug
        	if (cmp.isValid() && state === "SUCCESS") {
      			cmp.set("v.SupplierInvoice", response.getReturnValue());
                var Output = response.getReturnValue();
                cmp.set("v.Output", Output);
                 console.log("Output",Output); // debug
              //  cmp.set("v.SupplierInvoice", Output);
              //  console.log("Output",v.SupplierInvoice); // debug
             
        	}
        	else {
            	console.log("Failed with state: " + state);
        	     }
    	})
        
        $A.enqueueAction(actionSupplierInvoice);
        
        
        
	},
    
    navigateToeDiscoverySearchCmp : function(component, event, helper) {
        var evt = $A.get("e.force:navigateToComponent");
        evt.setParams({
            componentDef : "c:SIE",
        });
        evt.fire();
    }, 
   
    
    save: function (cmp, event, helper) {
        var draftValues = cmp.get("v.data");
        console.log(draftValues);
        var action = cmp.get("c.updateCostings");
        action.setParams({"editedAccountList" : draftValues});
        action.setCallback(this, function(response) {
            
            var state = response.getState();
            if (state === 'SUCCESS'){
            
           alert("Costings records saved succesfully ");
                location.reload();
         //  $A.get('e.force:refreshView').fire();
            }
            else{
                alert("We are in fail block");
            }
            
        });
        $A.enqueueAction(action);
        
    },
    
   //Adding New Costs 
     addRow: function(component, event, helper) {
        helper.addAccountRecord(component, event);
    },
     
    removeRow: function(component, event, helper) {
        //Get the account list
        var accountList = component.get("v.accountList");
        //Get the target object
        var selectedItem = event.currentTarget;
        //Get the selected item index
        var index = selectedItem.dataset.record;
        accountList.splice(index, 1);
        component.set("v.accountList", accountList);
    },
     
    save1: function(component, event, helper) {
        if (helper.validateAccountList(component, event)) {
            helper.saveAccountList(component, event);
             
        }
    },
    // Keep calm.I know this is nonsence and not best practice but Just to satisfy one requirement:)
    save3: function (component, event, helper) {
         component.set("v.is_loading", true);
        var draftValues = component.get("v.data");
        var CCurr = component.get("v.invoice_currency");
         var CSIID = component.get("v.CreatedSupplierInvoice");
        
        console.log(draftValues);
        var action = component.get("c.updateCostings");
        action.setParams({
            "editedAccountList" : draftValues,
            "PCurr" : CCurr,
            "PSIID" : CSIID
            
                         });
        action.setCallback(this, function(response) {
             component.set("v.is_loading", false);
            
            var state = response.getState();
            if (state === 'SUCCESS'){
            
          // alert("Costings records saved succesfully ");
         //  $A.get('e.force:refreshView').fire();
            }
            else{
                alert("We are in fail block-Update costings");
            }
            
        });
        $A.enqueueAction(action);
        
    }
   
})