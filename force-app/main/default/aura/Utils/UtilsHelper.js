({
  serverCall: function(component, method, params, successCallback, errorCallback) {
    var action = component.get(('c.' + method));
    if(Object.keys(params)) {
      action.setParams(params);
    }

    action.setCallback(this, function(response) {
      var state = response.getState();
      if(state === 'SUCCESS') {
        successCallback(response.getReturnValue());
      }
      else if(state === 'ERROR') {
        var errors = response.getError();
        if(errors) {
          if(errors[0] && errors[0].message && errorCallback) {
            errorCallback(errors[0].message);
          }
        } else if(errorCallback){
          errorCallback('Unknown error');
        }
      }
    });

    $A.enqueueAction(action);
  }
})