({
    doInit: function(component, event){
        component.set('v.showSpinner',true);
        
        var action = component.get("c.getPickUpData");
        action.setParams({
            recId: component.get('v.recordId')
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                component.set("v.freightRequest", result.freight);
                component.set("v.gateRequired", result.gateRequired);
                component.set("v.zkShipmentList", result.zkShipmentList);
                if(result.freight && result.freight.Status__c != 'Live'){
                    component.set("v.isFreightNotLive", true); 
                }
                var zkOptions = [];
                for(let zkshipment of result.zkShipmentList){
                    zkOptions.push({'label':zkshipment.Name,'value':zkshipment.Id});
                 }
                component.set("v.value",result.zkShipmentList[0].Id);
                component.set("v.options",zkOptions);
            }
            component.set('v.showSpinner',false);
        });
        $A.enqueueAction(action);
    },
    createPickupCtrl: function(component, event, helper){
        helper.createPickup(component, event, helper);
        
    },
    closePickUpModal : function(component, event){
        component.set("v.isOpenPickUpComponent", false);
        component.set("v.showSchedulePickup", false);
        if(!component.get("v.newSOPage")){
        var appEvent = $A.get('e.c:closeModal');
        appEvent.fire();  
        }
        
    }
})