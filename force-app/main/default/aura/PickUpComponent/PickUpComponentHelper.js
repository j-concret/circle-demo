({
    createPickup : function(component, event, helper) {
        var freight = component.get("v.freightRequest");
        
        if(!freight.Pickup_availability_Ready__c){
            this.showToast('Required Info Missing.','error','Pickup availability Ready cannot be empty');
            return false;
        }
        if(!freight.Pickup_availability_Close__c){
            this.showToast('Required Info Missing.','error','Pickup availability Close cannot be empty');
            return false;
        }
        var selectedZKShip = component.get("v.value");
        console.log('selectedZKShip '+selectedZKShip);
        component.set('v.showSpinner',true);
        //
       //    
        var action = component.get("c.schedulePickup");
        action.setParams({
            freight: component.get("v.freightRequest"),
            liftGateRequired : component.get("v.gateRequired"),
             selectedZKs : selectedZKShip
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                
                this.showToast('Success','success','Pickup created sucessfully.'); 
            }else if(state === "ERROR") {
                    var errors = response.getError();
                    if(errors) {
                        if(errors[0] && errors[0].message) {
                            this.showToast('Error','error',errors[0].message);
                        }
                    } else {
                        this.showToast('Error','error','Pick-up not created successfully, please see freight request for error details: Freight__c.Pickup_Error_Handler__c');
                    }
                    
                }
            component.set('v.showSpinner',false);
        });
        $A.enqueueAction(action);
    },
    showToast : function(title,type,msg) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title:title,
            type:type,
            message:msg
        });
        toastEvent.fire();
    }
})