/**
 * Created by Chibuye Kunda on 2018/12/31.
 */
({

    /** this function will be called to initialise our component
     * @param componentP this is the component we are updating
     * @param eventP this is the event that occured
     * @param helperP this is the helper object
     */
    doInit : function( componentP, eventP, helperP ){

        componentP.set( "v.table_headings",
                        [ "#", "Contact Name", "Contact Email", "Contact Number",
                          "Delivery Address"] );

       // helperP.createTableRow( componentP, eventP );

    },//end of function definition





    /** this function will add a new row to our grid
     *  @param componentP is the component we are updating
     *  @param eventP is the event we are processing
     *  @param helperP is the helper object
     */
    addNewRow : function( componentP, eventP, helperP ){

        helperP.createTableRow( componentP, eventP );

        var package_list = componentP.get( "v.final_deliveries_list" );

        componentP.set( "v.object_list_string", helperP.createObjectString( package_list ) );

    },//end of function definition





    /** this function will remove a row from the table
     *  @param componentP is the component we are updating
     *  @param eventP is the event we are processing
     *  @param helperP is the helper object
     */
    deleteRow : function( componentP, eventP, helperP ){

        var index = eventP.getParam( "index" );         //get the index
        var rows_list = componentP.get( "v.final_deliveries_list" );

        rows_list.splice( index, 1 );           //remove the value

        componentP.set( "v.final_deliveries_list", rows_list );
        componentP.set( "v.object_list_string", helperP.createObjectString( componentP.get( "v.final_deliveries_list" ) ) );

    }//end of function definition

   
})