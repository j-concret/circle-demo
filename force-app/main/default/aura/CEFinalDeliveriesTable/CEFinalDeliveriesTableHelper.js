/**
 * Created by Chibuye Kunda on 2018/12/31.
 */
({

    /** this function will create a new map and add it to the list
     *  @param componentP is the component we are updating
     *  @param eventP is the event we are processing
     */
    createTableRow : function( componentP, eventP ){

        var records = componentP.get( "v.final_deliveries_list" );           //get the record list

        //add to records list
        records.push({
            "sobjectType":"Final_Delivery__c",
            "Contact_name__c":"",
            "Contact_email__c":"",
            "Contact_number__c":"",
            "Delivery_address__c":""

        });

        componentP.set( "v.final_deliveries_list", records );

        console.log( "Records: " + records );
        console.log( " Size of the final_deliveries_list " + records.length );

       // this.createObjectString( records );

    },//end of function definition





    /** this function will convert object list into a string object
     * @param object_listP this is list o objects we are converting to a string
     */
    createObjectString : function( object_listP ){

        var list_string = "";

        if( object_listP === undefined )
            return;                 //terminate function

        console.log( "List Count: " + object_listP.length );

        //loop through the object list
        for( var i=0; i < object_listP.length; ++i ){

            //add to list string
            list_string = list_string + object_listP[i].Contact_email__c + "|" + object_listP[i].Contact_name__c + "|" +
                                        object_listP[i].Contact_number__c + "|" + object_listP[i].Delivery_address__c + ":";

        }//end of if block

        console.log( "List String: " + list_string );

        return list_string;                 //return the list string

    }//end of function definition

})