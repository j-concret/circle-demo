({
	fetchcomplaincedocument : function(component, event, helper) {
        var CID = component.get("{!v.recordId}");
        var action = component.get("c.getcomplaincedocument");
        action.setParams({"PID" : CID});
          action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var data = response.getReturnValue();
                component.set("v.ComplainceDoc",data);
                console.log(data);
                 
                 
                
            }
           
        });
        $A.enqueueAction(action);
    },
    CountryComplaince : function(component, event, helper) {
        var CCCList = component.get("{!v.CountryComplainceList}");
        var CID = component.get("{!v.recordId}");
        var one = component.get("{!v.ComplainceDoc.ProductName__c}");
        		 var two = component.get("{!v.ComplainceDoc.Name__c}");
        
        var CName = one+' '+two;
            console.log('CName-->',CName);    
       			
        
        console.log('CCCList-->',CCCList);
        
        var action = component.get("c.CreateCC");
          action.setParams({"PCCList" : CCCList,"PID" : CID,"PName" : CName });
          action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var data = response.getReturnValue();
                component.set("v.ComplainceDoc",data);
                console.log(data);
                
                
                 var hostname = window.location.hostname;
				var arr = hostname.split(".");
				var instance = arr[0];
                
                component.set("v.iframeUrl2","https://"+instance+".lightning.force.com/lightning/r/Compliance_Document__c/"+ CID+"/view");
               var address =  component.get("v.iframeUrl2");
                 window.open(address,'_top');
                 
            }
           
        });
        $A.enqueueAction(action);
    },
    addAccountRecord: function(component, event) {
        //get the account List from component  
        var accountList = component.get("v.CountryComplainceList");
        //Add New Account Record
        accountList.push({
            'sobjectType': 'Country_Compliance__c',
            'Product__c': '',
            'HS_Code_and_Associated_Details__c': ''
        });
        component.set("v.CountryComplainceList", accountList);
    },
})