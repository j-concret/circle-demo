({
	doInit : function(component, event, helper) {
		//var CDID = component.get("v.recordId");
		 helper.fetchcomplaincedocument(component, event, helper);
        helper.addAccountRecord(component, event);
    },
    
    CreateCountryComplaince :function(component, event, helper) {
        
        var FinalCountryComplainceList = component.get("v.CountryComplainceList");
       
        
        console.log('CountryComplaince',FinalCountryComplainceList);
        helper.CountryComplaince(component, event, helper);
     },
    addRow: function(component, event, helper) {
        helper.addAccountRecord(component, event);
    },
    removeRow: function(component, event, helper) {
        //Get the account list
        var accountList = component.get("v.CountryComplainceList");
        //Get the target object
        var selectedItem = event.currentTarget;
        //Get the selected item index
        var index = selectedItem.dataset.record;
        accountList.splice(index, 1);
        component.set("v.CountryComplainceList", accountList);
    },
	
})