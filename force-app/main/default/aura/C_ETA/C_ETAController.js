({
    editETA_Form : function(component, event, helper) {helper.activeSections(component)},
    //required Fields
    showRequiredFields: function(component, event, helper){   
        
    },
    
    //onsubmit
    onRecordSubmit: function (component, event, helper) {
        component.set("v.showSpinner", true);
    },                
    
    
    //onsuccess
    handleSuccess : function(component, event, helper) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({ "title": "Success!",
                              "message": "Saved.",
                              "type": "success"});
        toastEvent.fire();
        helper.activeSections(component);
        component.set("v.showSpinner", false);
    },
    
    onError : function(component, event, helper) {
        
        var toastEvent2 = $A.get("e.force:showToast");
        toastEvent2.setParams({
            "title": "Error!",
            "message": "Error."
        });
        toastEvent2.fire();
        component.set("v.showSpinner", false);
    },
    
    handleCancel : function(component, event, helper) {
        helper.activeSections(component);
        event.preventDefault();
    }
})