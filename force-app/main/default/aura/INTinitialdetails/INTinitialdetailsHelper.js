({
	
    /** this function will populate the account and contact fields
     *  @param componentP is the component we are updated
     */
/*    populateAccountAndContact : function( componentP ){

        var get_records = componentP.get( "c.getAccountAndContact" );                    //action to return account and contact record

        console.log( "getting contact and account" );

        get_records.setCallback( this, function( response ){

            var state = response.getState();                            //get the state

            //check if we have success
            if( state === "SUCCESS" ){

                console.log( "Success getting account and contact" );

                var server_response = response.getReturnValue();                //get the return value
                var account_populate_method = componentP.find( "account_lookup" );              //get the account lookup field
                var contact_populate_method = componentP.find( "contact_lookup" );              //get the contact lookup field


                //check if we have an error
                if( server_response.is_error ){

                    this.showToastMessage( "error", "Error Occured", server_response.result_message );
                    return;                         //indicate failure

                }
                //check if auto populate is not required
                else if( server_response.result_message === "NA" ) {

                    console.log("In NA");
                    return;             //terminate function

                }//end of if-else block

                console.log( server_response.sobject_list[0] );
                console.log( server_response.sobject_list[1] );

                console.log( "Before the auto set" );
                console.log( "Account: " + account_populate_method );
                console.log( "Test Value: " + componentP.get( "v.shipment_value" ) );

                componentP.set( "v.account_id", server_response.sobject_list[0].Id );
                contact_populate_method.setSelectedRecord( server_response.sobject_list[1] );       //set the selected contact record

                console.log( "Account ID: " + componentP.get( "v.account_id" ) );

            }
            //check if have errors
            else if( state === "ERROR" )
                this.processServerErrors( response.getError() );             //process the server errors

        });//end of

        $A.enqueueAction( get_records );                    //execute the action

    },//end of function definition
    */
})