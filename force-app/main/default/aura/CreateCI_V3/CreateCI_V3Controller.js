({
  doInit : function(component, event, helper) {
    var headerData = [
      {'type':'defined','colName':'Qty','colspan':1,'style':'padding-right:1rem;min-width:5rem;white-space: nowrap; color:white; background-color: #797171;','apiName':'Quantity__c'},
      {'type':'defined','colName':'Part No.','colspan':2,'style':'padding-right:1rem;min-width:10rem;white-space: nowrap; color:white; background-color: #797171;','apiName':'Name'},
      {'type':'defined','colName':'Description','colspan':2,'style':'padding-right:1rem;min-width:10rem;white-space: nowrap; color:white; background-color: #797171;','apiName':'Description_and_Functionality__c'},
      {'type':'defined','colName':'Country of Origin','colspan':1,'style':'padding-right:1rem;min-width:5rem;white-space: nowrap; color:white; background-color: #797171;','apiName':'COO_2_digit__c'},
      {'type':'defined','colName':'ECCN','colspan':1,'style':'padding-right:1rem;min-width:5rem;white-space: nowrap; color:white; background-color: #797171;','apiName':'ECCN_NO__c'},
      {'type':'defined','colName':'HS Codes','colspan':1,'style':'padding-right:1rem;min-width:5rem;white-space: nowrap; color:white; background-color: #797171;','apiName':'US_HTS_Code__c'},
      {'type':'defined','colName':'Unit Price','colspan':1,'style':'padding-right:1rem;min-width:5rem;white-space: nowrap; color:white; background-color: #797171;','apiName':'Commercial_Value__c','dataType':'currency'},
      {'type':'defined','colName':'Extended Value','colspan':1,'style':'padding-right:1rem;min-width:5rem;white-space: nowrap; color:white; background-color: #797171;','apiName':'Total_Value__c','dataType':'currency'}
    ];
      
      var shipment = component.get('v.shipmentOrder');
      if((shipment.RecordType.Name == 'Cost Estimate' || shipment.RecordType.Name == 'Shipment Order') && shipment.Service_Type__c == 'IOR' && shipment.Destination__c == 'Australia'){
          headerData.push({'type':'defined','colName':'Net Weight (kg)','colspan':1,'style':'padding-right:1rem;min-width:5rem;white-space: nowrap; color:white; background-color: #797171;','apiName':'Net_Weight_per_Line_Item__c'});
      }
      
    component.set('v.headerData',headerData);

    var today = $A.localizationService.formatDate(new Date(), 'DD/MM/YYYY');
    component.set('v.today', today);

    helper.doInit(component);

    var shipmentOrder = component.get('v.shipmentOrder');
    var freightRequest = component.get('v.freightRequest');
    var cif = shipmentOrder.CI_Input_Freight__c + shipmentOrder.CI_Input_Liability_Cover__c;//freightRequest.International_freight_fee_invoiced__c + shipmentOrder.Insurance_Fee_USD__c;
    var shipmentValue = shipmentOrder.Shipment_Value_USD__c;
    var cif_freight_insurance = {'cif':false,'freight':false,'insurance':false};

    if(shipmentOrder.CPA_v2_0__r.CIF_Freight_and_Insurance__c && shipmentOrder.CPA_v2_0__r.CIF_Freight_and_Insurance__c.includes('CIF')){
      cif_freight_insurance.cif = true;
    }
    if(shipmentOrder.CPA_v2_0__r.CIF_Freight_and_Insurance__c && shipmentOrder.CPA_v2_0__r.CIF_Freight_and_Insurance__c.includes('Freight')){
      cif_freight_insurance.freight = true;
    }
    if(shipmentOrder.CPA_v2_0__r.CIF_Freight_and_Insurance__c && shipmentOrder.CPA_v2_0__r.CIF_Freight_and_Insurance__c.includes('Insurance')){
      cif_freight_insurance.insurance = true;
    }
    if(cif && shipmentOrder.CPA_v2_0__r.CIF_Freight_and_Insurance__c && shipmentOrder.Who_arranges_International_courier__c == 'TecEx') {
      shipmentValue += cif;
    }
    component.set('v.cif_freight_insurance',cif_freight_insurance);
    component.set('v.cif',cif);
    component.set('v.shipmentValue',shipmentValue.toFixed(2));
  },
  downloadSaveHandler:function(component,event,helper){
    var params = event.getParam('arguments');
    if (params) {
      var actionName = params.actionName;
      switch (actionName) {
        case 'savePdf':
          helper.downloadSavePdf(component,true);
          break;
        case 'saveExcel':
          helper.downloadSaveExcel(component,true);
          break;
        case 'downloadPdf':
          helper.downloadSavePdf(component,false);
          break;
        case 'downloadExcel':
          helper.downloadSaveExcel(component,false);
          break;
        default:
          helper.showToast('ERROR','error','No Action Found!');
      }
    }else{
      helper.showToast('ERROR','error','No Action Found!');
    }

  },
  addCol:function(component,event,helper){
    var headerData = component.get('v.headerData');
    if(2+headerData.length<14){
      var customFieldName = 'custom'+(headerData.length - 8+1);
      headerData.splice(headerData.length-1,0,{'type':'custom','colName':'','colspan':1,'style':'padding-right:1rem;min-width:5rem;white-space: nowrap; color:white; background-color: #797171;','apiName':customFieldName});
      component.set('v.headerData',headerData);
    }else{
      helper.showToast('ERROR','error','You can\'t add more columns');
    }
  },
  removeCol:function(component,event,helper){
    var selectedItem = event.currentTarget;
    var indx = selectedItem.dataset.index;
    var headerData = component.get('v.headerData');
    headerData.splice(indx,1);
    component.set('v.headerData',headerData);
  },
  onRender: function(component){
    if(document.getElementById('logoImg')){
      var img = document.getElementById('logoImg');
      if(img.height > 200) component.set('v.width','width:150px;');

    }
  }
    
	
});