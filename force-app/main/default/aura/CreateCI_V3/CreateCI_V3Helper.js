({
  doInit: function (component) {
    component.set('v.showSpinner', true);
    var action = component.get('c.getShipmentPckgLineItem');
    action.setParams({
      recId: component.get('v.shipmentOrder').Id,
      vatValue: component.get('v.vatValue'),
      portValue: component.get('v.portValue')
    });

    action.setCallback(this, function (response) {
      component.set('v.showSpinner', false);
      var result = response.getReturnValue();
      if (result.status === 'OK') {
        component.set('v.logoName', result.logoName ? result.logoName : '');
        var allLineItems = [];
        // Below fetching headers details.
        var headers;
        if (result.lineItems.length > 0) {
          var additionColumnsData = result.lineItems[0].Additional_CI_Columns__c;
          if (additionColumnsData && additionColumnsData != '') {
            var data = JSON.parse(additionColumnsData);
            headers = Object.keys(data);
            var headerData = component.get('v.headerData');
            headers.reverse();

            for (var i in headers) {
              var customFieldName = 'custom' + (headerData.length - 8 + 1);
              headerData.splice(headerData.length - 1, 0, {
                'type': 'custom',
                'colName': headers[i],
                'colspan': 1,
                'style': 'padding-right:1rem;min-width:5rem;white-space: nowrap; color:white; background-color: #797171;',
                'apiName': customFieldName
              });
            }
            component.set('v.headerData', headerData);
          }

          allLineItems = result.lineItems;
          for (var i in allLineItems) {
            var additionColumnsData = allLineItems[i].Additional_CI_Columns__c;
            if (headers && additionColumnsData && additionColumnsData !== '') {
              var data = JSON.parse(additionColumnsData);
              for (var j = 0; headers.length > j; j++) {
                var customElement = 'custom' + (j + 1);
                allLineItems[i][customElement] = data[headers[j]];
              }
            }
          }
        }

        if (result.ciCount > 0) {
          component.set('v.ciFound', true);
        }
        component.set('v.lineItems', allLineItems);

        component.set('v.sopList', result.sopList);
        component.set('v.numberOfPackages', result.numberOfPackages);
        component.set('v.packagesWeight', result.packagesWeight);
        component.set('v.business', result.business);
        component.set('v.currencyConvertor', result.currencyConvertor);  

        if (result.business == 'Zee') {
            var totalvalue = 0;
          var shipmentOrder = component.get("v.shipmentOrder");
          var freightRequest = component.get("v.freightRequest");
           
            if(shipmentOrder.CPA_v2_0__r.Valuation_Method__c == 'Retail Method'){
                for (let item of result.lineItems) {
                   totalvalue += item.Total_Value__c;
                }
                shipmentOrder.Shipment_Value_USD__c = Number(Math.round(totalvalue+'e2')+'e-2');
                var cif = freightRequest.International_freight_fee_invoiced__c + shipmentOrder.Insurance_Fee_USD__c;
                var shipmentValue = shipmentOrder.Shipment_Value_USD__c;
                if(cif && shipmentOrder.CPA_v2_0__r.CIF_Freight_and_Insurance__c && shipmentOrder.Who_arranges_International_courier__c == 'TecEx') {
                    shipmentValue += cif;
                }
                component.set('v.cif',cif);
                component.set("v.shipmentOrder",shipmentOrder);
                component.set('v.shipmentValue',shipmentValue.toFixed(2));
            }else if(shipmentOrder.CPA_v2_0__r.Valuation_Method__c == 'Cost Method'){
                shipmentOrder.Shipment_Value_USD__c = (shipmentOrder.Shipment_Value_USD__c / result.currencyConvertor).toFixed(2);
                shipmentOrder.Insurance_Fee_USD__c = (shipmentOrder.Insurance_Fee_USD__c / result.currencyConvertor).toFixed(2);
                freightRequest.International_freight_fee_invoiced__c = (freightRequest.International_freight_fee_invoiced__c / result.currencyConvertor).toFixed(2);
                component.set("v.shipmentOrder",shipmentOrder);
                component.set("v.shipmentValue",((component.get("v.shipmentValue") / result.currencyConvertor).toFixed(2)));
                component.set("v.freightRequest",freightRequest);
            }
                        
        }
        
       // component.set('v.registrations', result.registrations);  
        var cAddresses = result.cAddresses;
        cAddresses.sort((a, b) => (a.Address_Type__c > b.Address_Type__c) ? 1 : ((b.Address_Type__c > a.Address_Type__c) ? -1 : 0));
        component.set('v.cAddresses', cAddresses);
      }
    });
    $A.enqueueAction(action);
  },
  downloadSaveExcel: function (component, isSave) {
    var fmt = '#,##0.00';
    var e_col = 14; //no. of columns
    var headerData = component.get('v.headerData');
    var sopList = component.get('v.sopList');
    var lineItems = component.get('v.lineItems');
     
    var cAddresses = component.get('v.cAddresses');
    var freightRequest = component.get('v.freightRequest');
    var cif_freight_insurance = component.get('v.cif_freight_insurance');
    var shipmentOrder = component.get('v.shipmentOrder');
    var tbl = document.getElementById('CIDataToExport');
    console.log('tbl' ,tbl);  
    var wb = XLSX.utils.table_to_book(tbl);
    var sheetName = wb.SheetNames[0];
    var ws = wb.Sheets[sheetName];
    var max = Math.max(this.getColWidth(freightRequest), this.getColWidth(cAddresses[1] || {}));
    ws['!cols'][3] = {
      width: Math.ceil(max / 2)
    };

    var ec_of_lineitm = 0;
    headerData.forEach(element => {
      ec_of_lineitm += element.colspan;
    });

    ws['!gridlines'] = false;
    var formatRows = {
      1: {
        'sc': 1,
        'ec': 14,
        'borders': ['top', 'right', 'bottom', 'left']
      },
      //LineItems Table header border
      7: {
        'sc': 1,
        'ec': ec_of_lineitm,
        'borders': ['top', 'bottom']
      },
      //lineItems Table footer border
      [7 + lineItems.length]: {
        'sc': 1,
        'ec': ec_of_lineitm,
        'borders': ['bottom']
      },
      // SOP Header Border
      [10 + lineItems.length]: {
        'sc': 2,
        'ec': 8,
        'borders': ['top', 'bottom']
      },
      // SOP Footer Border
      [11 + lineItems.length + sopList.length]: {
        'sc': 2,
        'ec': 8,
        'borders': ['top', 'bottom']
      },
      //value of goods Border
      [12 + lineItems.length + sopList.length]: {
        'sc': 11,
        'ec': 13,
        'borders': ['top', 'right', 'bottom', 'left']
      }
    };

    //column=>formatting
    var numberFormat = {
      9: {
        sr: 8,
        er: 7 + lineItems.length,
        t: "n",
        alignment: {
          horizontal: "right"
        },
        z: fmt
      },
      10: {
        sr: 8,
        er: 7 + lineItems.length,
        t: "n",
        alignment: {
          horizontal: "right"
        },
        z: fmt
      },
      [ec_of_lineitm]: {
        sr: 8,
        er: 7 + lineItems.length,
        t: "n",
        alignment: {
          horizontal: "right"
        },
        z: fmt
      }
    };

    var varLines = 13;
    // Dynamic row formatting on basis of its rendering
    if (cif_freight_insurance.freight) {
      // freight amount column Border
      formatRows[(varLines + lineItems.length + sopList.length)] = {
        'sc': 11,
        'ec': 13,
        'borders': ['top', 'right', 'bottom', 'left']
      };
      varLines++;
    }
    if (cif_freight_insurance.insurance) {
      // insurance amount column Border
      formatRows[(varLines + lineItems.length + sopList.length)] = {
        'sc': 11,
        'ec': 13,
        'borders': ['top', 'right', 'bottom', 'left']
      };
      varLines++;
    }
    if (cif_freight_insurance.cif) {
      // CIF amount column Border
      formatRows[(varLines + lineItems.length + sopList.length)] = {
        'sc': 11,
        'ec': 13,
        'borders': ['top', 'right', 'bottom', 'left']
      };
      varLines++;
    }

    //Remaining rows and columns for formatting
    // Total Value of Goods
    formatRows[varLines + lineItems.length + sopList.length] = {
      'sc': 11,
      'ec': 13,
      'borders': ['top', 'right', 'bottom', 'left']
    }
    //currency formatting for dynamic columns (upto Total Value of Goods columns)
    numberFormat[13] = {
      sr: (12 + lineItems.length + sopList.length),
      er: (varLines + lineItems.length + sopList.length),
      t: "n",
      alignment: {
        horizontal: "right"
      },
      z: fmt
    };

    varLines++;
    //extra space row border
    formatRows[varLines + lineItems.length + sopList.length] = {
      'sc': 11,
      'ec': 13,
      'borders': ['right', 'left']
    };
    varLines++;
    //one more extra space row border
    formatRows[varLines + lineItems.length + sopList.length] = {
      'sc': 11,
      'ec': 13,
      'borders': ['right', 'left']
    };
    varLines++;
    //one more extra space row border
    formatRows[varLines + lineItems.length + sopList.length] = {
      'sc': 11,
      'ec': 13,
      'borders': ['top', 'right', 'bottom', 'left']
    };
    varLines++;
    //Authorized Signature row border
    formatRows[varLines + lineItems.length + sopList.length] = {
      'sc': 11,
      'ec': 13,
      'borders': ['top', 'right', 'bottom', 'left']
    };

    var rows = Object.keys(formatRows);
    for (var i = 0; i < rows.length; i++) {
      var r = rows[i];
      var col = formatRows[r];
      for (var c = col.sc; c <= col.ec; c++) {

        var chr = String.fromCharCode(65 + c - 1);
        if (!ws[chr + '' + r]) {
          ws[chr + '' + r] = {
            v: '',
            s: {}
          };
        }

        for (var j = 0; j < col.borders.length; j++) {
          ws[chr + '' + r]['s'][col.borders[j]] = {
            style: "thin",
            color: {
              rgb: "#000000"
            }
          };
        }
      }
    }
    XLSX.utils.sheet_set_range_style(ws, ws['!fullref'], {
      alignment: {
        wrapText: true
      }
    });


    //Number formatting and alignment
    var columns = Object.keys(numberFormat);
    for (var i = 0; i < columns.length; i++) {
      var c = columns[i];
      var f = numberFormat[c];
      if (f.er >= f.sr) {
        var chr = String.fromCharCode(65 + parseInt(c - 1));
        for (var j = f.sr; j <= f.er; j++) {
          if (!ws[chr + '' + j]['s']) ws[chr + '' + j]['s'] = {};
          ws[chr + '' + j]['s']['alignment'] = f.alignment;
          ws[chr + '' + j]['t'] = f.t;
          ws[chr + '' + j]['z'] = f.z;
        }
      }
    }

    //adding logo image in sheet
    if (document.getElementById('logoImg')) {
      this.getBase64Image(document.getElementById('logoImg'), ws);
    }
    wb.Sheets[sheetName] = ws;
    if (isSave) {
      var data = this.getAdditionColumnData(component);
      var wbout = XLSX.write(wb, {
        bookImages: true,
        cellStyles: true,
        bookSST: true,
        bookType: 'xlsx',
        type: 'base64'
      });
      var params = {
        'blobData': wbout,
        'soId': shipmentOrder.Id,
        'soNumber': shipmentOrder.Name,
        'type': 'xls',
        'additionalColumnData':data ? data :''
      };

      this.saveCI(component, params);
    } else {
      XLSX.writeFile(wb, 'CI-T' + new Date().getTime() + '.xls', {
        bookImages: true,
        cellStyles: true,
        bookSST: true,
        bookType: 'xlsx'
      });
    }
  },
  getBase64Image: function (img, ws) {
    var colWidth = (ws['!cols'][0]['wpx'] || 0) + (ws['!cols'][1]['wpx'] || 0) + (ws['!cols'][2]['wpx'] || 0);
    var colHeight = ws['!rows'][0]['hpx'] || 0;

    var canvas = document.createElement('canvas');
    canvas.width = img.width;
    canvas.height = img.height;
    var ctx = canvas.getContext('2d');
    ctx.drawImage(img, 0, 0);
    var dataURL = canvas.toDataURL('image/png');
    var dd = dataURL.replace(/^data:image\/(png|jpg);base64,/, '');
    var newSize = this.calculateImgSize(img.width, img.height, colWidth - 5, colHeight - 3);
    var x = (colWidth - newSize.w) / 2;
    var y = (colHeight - newSize.h) / 2;
    ws['!images'] = [{
      '!pos': {
        c: 0,
        r: 0,
        x: x,
        y: y,
        w: newSize.w,
        h: newSize.h
      },
      '!datatype': 'base64',
      '!data': dd
    }];
  },
  calculateImgSize: function (width, height, maxWidth, maxHeight) {
    //var maxWidth = 200; // Max width for the image
    //var maxHeight = 80;    // Max height for the image
    var ratio = 0; // Used for aspect ratio

    // Check if the current width is larger than the max
    if (width > maxWidth) {
      ratio = maxWidth / width; // get ratio for scaling image
      height = height * ratio; // Reset height to match scaled image
      width = width * ratio; // Reset width to match scaled image
    }

    // Check if current height is larger than max
    if (height > maxHeight) {
      ratio = maxHeight / height; // get ratio for scaling image
      width = width * ratio; // Reset width to match scaled image
      height = height * ratio; // Reset height to match scaled image
    }
    return {
      w: width,
      h: height
    };

  },
  getColWidth: function (obj) {
    var value = Object.values(obj);
    var maxLen = 0;

    for (let j = 0; j < value.length; j++) {
      if (typeof value[j] == "number") {
        maxLen = 10;
      } else {
        maxLen = maxLen >= value[j].length ? maxLen : value[j].length;
      }
    }
    return maxLen;
  },
  downloadSavePdf: function (component, isSave) {
    this.pdfGenerationCallback(component, isSave)
  },
  pdfGenerationCallback: function (component, isSave) {
    if (isSave) {
      var data = this.getAdditionColumnData(component);
      var post  
      var shipmentOrder = component.get('v.shipmentOrder');
      var params = {
        'blobData': null,
        'soId': shipmentOrder.Id,
        'soNumber': shipmentOrder.Name,
        'type': 'pdf',
        'additionalColumnData':data ? data :'',
        'vatValue': component.get('v.vatValue'),
        'portValue': component.get('v.portValue')
      };
      this.saveCI(component, params);
    } else {
      //for future download pdf
    }
  },
  saveCI: function (component, params) {
    component.set('v.showSpinner', true);
    var action = component.get("c.saveCI");
    action.setParams(params);

    action.setCallback(this, function (response) {
      component.set('v.showSpinner', false);
      var state = response.getState();
      if (state === "SUCCESS") {
        var result = response.getReturnValue();
        if (result.status === "OK") {
          this.showToast('Success', 'success', result.msg);
          //this.updateAdditionColumnData(component);
        } else {
          this.showToast('Error', 'error', result.msg);
        }
      } else if (state === "ERROR") {
        var errors = response.getError();
        if (errors) {
          if (errors[0] && errors[0].message) {
            this.showToast('Error', 'error', errors[0].message);
          }
        } else {
          this.showToast('Error', 'error', 'Unknown Error');
        }

      }
    });
    $A.enqueueAction(action);
  },
  showToast: function (title, type, msg) {
    var toastEvent = $A.get("e.force:showToast");
    toastEvent.setParams({
      title: title,
      type: type,
      message: msg
    });
    toastEvent.fire();
  },
  getAdditionColumnData: function (component) {
    var headerData = component.get('v.headerData');
    var d = {};

    headerData.forEach(element => {
      if (element.type == 'custom') {
        d[element.apiName] = element.colName;
      }
    });

    var lineItems = component.get('v.lineItems');
      
    var apiNames = Object.keys(d);
    var data = [];
    lineItems.forEach(item => {
      var cc = {};
      apiNames.forEach(ele => {
        cc[d[ele]] = item[ele] ? item[ele] : '';
      });
      data.push({Id:item.Id,Additional_CI_Columns__c : (Object.keys(cc).length === 0?'':JSON.stringify(cc))});
    });
    
    return JSON.stringify(data);
  }
});