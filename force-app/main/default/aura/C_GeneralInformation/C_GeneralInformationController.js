({
    doInit : function (cmp,event,helper){
        helper.makeServerCall(cmp,"getAppName",{},function(response){
            cmp.set("v.business",response);
            console.log("business ", response);
        },function(error){
            console.log(error);
        });
    },
    editGeneralInfoRecord : function(component, event, helper) {helper.activeSections(component)},
    //required Fields
    showRequiredFields: function(component, event, helper){
        $A.util.removeClass(component.find("Account__c"), "none");
        $A.util.removeClass(component.find("Ship_to_Country__c"), "none");
        $A.util.removeClass(component.find("CPA_v2_0__c"), "none");
        $A.util.removeClass(component.find("Ship_From_Country__c"), "none");
        $A.util.removeClass(component.find("Service_Type__c"), "none");
        
        var cpa                      = component.find("CPA_v2_0__c");
        var cpaValue                 = cpa.get("v.value");
        component.set("v.cpaChange", cpaValue);
        
        
    },
    
    
    //onsubmit
    onRecordSubmit: function (component, event, helper) {
        event.preventDefault();
        var invalidFields = [];
        //Required Fields
        var account                      = component.find("Account__c");
        var accountValue                 = account.get("v.value");
        if (!accountValue || accountValue.trim().length=== 0) {invalidFields.push(account);}
        
        
        
        var shipFromCountry              = component.find("Ship_From_Country__c") ;
        var shipFromCountryValue              = shipFromCountry.get("v.value");
        if (!shipFromCountryValue || shipFromCountryValue.trim().length=== 0) {invalidFields.push(shipFromCountry);}
        
        var serviceType             = component.find("Service_Type__c");
        var serviceTypeValue        = serviceType.get("v.value");
        if (!serviceTypeValue || serviceTypeValue.trim().length === 0) {invalidFields.push(serviceType);}
        
        var fields = event.getParam('fields');
        
        var cpa                      = component.find("CPA_v2_0__c");
        var cpaValue                 = cpa.get("v.value");
        if(component.get("v.cpaChange") != cpaValue){
            fields.CPA2_Override__c  = true;
            var CPA2_Override_reason             = component.find("CPA2_Override_reason__c");
            var CPA2_Override_reasonValue        = CPA2_Override_reason.get("v.value");
            if (!CPA2_Override_reasonValue || CPA2_Override_reasonValue.trim().length === 0) {invalidFields.push(CPA2_Override_reason);}
        }
        
        if(invalidFields.length >0 ){
            for(var i=0; i<invalidFields.length; i++){ $A.util.addClass(invalidFields[i], 'slds-has-error');}
            event.preventDefault();
        }else{            
            component.find('editFormGeneralInformation').submit(fields);
            component.set("v.showSpinner", true);
        }
        
    },                
    
    
    //onsuccess
    handleSuccess : function(component, event, helper) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({ "title": "Success!",
                              "message": "Saved.",
                              "type": "success"});
        toastEvent.fire();
        helper.activeSections(component);
        component.set("v.showSpinner", false);
    },
    
    //OnError 
    onError : function(component, event, helper) {
        
        var toastEvent2 = $A.get("e.force:showToast");
        toastEvent2.setParams({
            "title": "Error!",
            "message": "Error."
        });
        toastEvent2.fire();
        component.set("v.showSpinner", false);
    },
    
    //Handles visibility input and outputs fields
    handleCancel : function(component, event, helper) {
        helper.activeSections(component);
        event.preventDefault();
    },
    recordUpdated : function(component, event, helper) {
      var changeType = event.getParams().changeType;
      if (changeType === "CHANGED") {
        component.find("shipmentOrderRecord").reloadRecord();}
    }
})