({
    doInit : function(component, event, helper) {
        component.set('v.loading',true);
        var recordId = component.get("v.recordId");
        var action = component.get("c.CCDListRecords");
        console.log('CCDListRecords ',recordId);
        action.setParams({"recordId" : recordId});
        action.setCallback(this, function(response) { 
            var state = response.getState();
            var toastEvent = $A.get("e.force:showToast");
            if (state === 'SUCCESS'){
                var responseMap = response.getReturnValue();
                if(responseMap.status == 'OK'){
                    component.set('v.tabData',responseMap.data);
                    console.log('tabData ',responseMap.data);
                     component.set('v.loading',false);
                }else{
                     component.set('v.loading',false);
                    toastEvent.setParams({
                        title : 'Error',
                        message:responseMap['message'],
                        duration:' 5000',
                        key: 'info_alt',
                        type: 'error',
                        mode: 'pester'
                    });
                    toastEvent.fire();
                }
            }
            else if (state === "INCOMPLETE") {
                 component.set('v.loading',false);
                toastEvent.setParams({
                    title : 'Error',
                    message:"Unknown error",
                    duration:' 5000',
                    key: 'info_alt',
                    type: 'error',
                    mode: 'pester'
                });
                toastEvent.fire();
            }
                else if (state === "ERROR") {
                     component.set('v.loading',false);
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            toastEvent.setParams({
                                title : 'Error',
                                message:errors[0].message,
                                duration:' 5000',
                                key: 'info_alt',
                                type: 'error',
                                mode: 'pester'
                            });
                            toastEvent.fire();
                        }
                    } else {
                         component.set('v.loading',false);
                        toastEvent.setParams({
                            title : 'Error',
                            message:"Unknown error",
                            duration:' 5000',
                            key: 'info_alt',
                            type: 'error',
                            mode: 'pester'
                        });
                        toastEvent.fire();
                    }
                }
            
        });
        $A.enqueueAction(action);
    }
})