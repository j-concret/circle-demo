({
    doInit : function(component, event, helper) {
        component.set('v.isLoading',true);
        if(!component.get("v.newSOPage")){
            var workspaceAPI = component.find("workspace");
            workspaceAPI.getFocusedTabInfo ().then(function(response){
                
                var focusedTabId = response.tabId;
                
                workspaceAPI.setTabLabel({
                    tabId: focusedTabId,
                    label: "Costings" //set label you want to set
                });
                workspaceAPI.setTabIcon({
                    tabId: focusedTabId,
                    icon: "", //set icon you want to set
                    iconAlt: "Costings" //set label tooltip you want to set
                });
            });
            var myPageRef = component.get("v.pageReference");
            var	recordId = myPageRef.state.c__recordId;
            component.set('v.recordId',recordId);
        }
        helper.getCurrencies(component);
        helper.getCategories(component);
        helper.getCostType(component);
        helper.getAppliedTo(component);
        helper.getIOREOR(component);
        helper.getCostings(component);
        
    },
    onCostingsCreation: function(component, event, helper) {
        if(component.get('v.isNewCostingsCreated')){
            component.set('v.isLoading',true);
            helper.getCostings(component);
            component.get('v.isNewCostingsCreated',false);    
        }
    },
    updateCostings : function(component, event, helper) {
        component.set('v.isLoading',true);
        helper.updateCostingsHelper(component);
    },
    openCostingModal : function(component, event, helper) {
        component.set('v.isCreatingCosting',true);
        component.set('v.isRecordLoading',true);
    },
    onRecordSuccess: function(component, event, helper) {
        component.set('v.costings',[]);
        component.set('v.isLoading',true);
        helper.fireToast('SUCCESS', 'success', 'New Costings Created SuccessFully !');
        helper.getCostings(component);
        component.set('v.isCreatingCosting',false);
    },
    onCancelCosting: function(component, event, helper) {
        component.set('v.isCreatingCosting',false);
    },
    onLoad : function(component, event, helper) {
        component.set('v.isRecordLoading',false);
    },
    onRecordSubmit: function(component, event, helper) {
        event.preventDefault(); // stop form submission
        var eventFields = event.getParam("fields");
        console.log("eventFields");
        console.log(JSON.stringify(eventFields));
        component.find('myform').submit(eventFields);
        
    }
})