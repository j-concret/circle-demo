({
    getCostings: function(component) {
        var that = this;
        var action = component.get("c.getCostings");
        action.setParams({
            'ShipmentId': component.get('v.recordId')
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS") {
                var result = response.getReturnValue(); if(result.status === "OK") {
                    console.log(result.shipmentOrders);
                    let fAgreedcurrency = 0;
                    let fAmount =0;
                    let invoiceAmount = 0;
                    component.set('v.shipmentObject', result.shipmentOrders);
                    component.set('v.Supplier',result.shipmentOrders.SupplierlU__c);
                    component.set('v.costings', result.shipmentOrders.CPA_Costing__r);
                    console.log(result.shipmentOrders.CPA_Costing__r);
                    if(result.shipmentOrders.CPA_Costing__r){
                    	for(let cpc of result.shipmentOrders.CPA_Costing__r){
                        cpc.Amount__c ? fAgreedcurrency +=cpc.Amount__c : '' ;
                        cpc.Amount_in_USD__c ? fAmount +=cpc.Amount_in_USD__c : '' ;
                        cpc.Invoice_amount_USD_New__c ? invoiceAmount +=cpc.Invoice_amount_USD_New__c : '' ;
                    	}    
                    }
                   	component.set('v.fAgreedcurrency',fAgreedcurrency);
                    component.set('v.fAmount',fAmount);
                    component.set('v.invoiceAmount',invoiceAmount);
                    console.log(component.get('v.shipmentObject'));
                    component.set('v.isLoading',false);
                } else {
                    console.log(result.msg);
                    component.set('v.isLoading',false);
                }
                component.set('v.isDataAvailable',true);
            }
            else if(state === "ERROR") {
                var errors = response.getError();
                if(errors) {
                    if(errors[0] && errors[0].message) {
                        console.log(errors[0].message);
                        component.set('v.isLoading',false);
                        //that.fireToast('ERROR', 'error', errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                    component.set('v.isLoading',false);
                    //that.fireToast('ERROR', 'error', "Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    updateCostingsHelper: function(component) {
        var costings = component.get('v.costings');
        console.log('Updating');
		console.log(costings);
        console.log(JSON.stringify(costings));
        var that = this;
        var action = component.get("c.updateCostingsList");
        action.setParams({
            'costingsToUpdate': costings
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS") {
                var result = response.getReturnValue(); if(result.status === "OK") {
                    console.log('update Success');
                    that.getCostings(component);
                     that.fireToast('SUCCESS', 'success', 'Costings Updated Successfully !');
                    component.set('v.isLoading',false);
                } else {
                    console.log(result.msg);
                     that.fireToast('ERROR', 'error', result.msg);
                    component.set('v.isLoading',false);
                }
            }
            else if(state === "ERROR") {
                var errors = response.getError();
                if(errors) {
                    if(errors[0] && errors[0].message) {
                        console.log(errors[0].message);
                        component.set('v.isLoading',false);
                        that.fireToast('ERROR', 'error', errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                    component.set('v.isLoading',false);
                    that.fireToast('ERROR', 'error', "Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    getCurrencies: function(component) {
        var that = this;
        var action = component.get("c.getPicklistValues");
        action.setParams({
            'objectName': 'CPA_Costing__c',
            'fieldName': 'Currency__c'
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS") {
                var result = response.getReturnValue(); if(result.status === "OK") {
                    /*var json = {
                        label:"--None--",
                        value:""
                    };
                    (result.statuses).unshift(json);*/
                    component.set('v.currencyPicklist', result.statuses);
                } else {
                    console.log(result.msg);
                }
            }
            else if(state === "ERROR") {
                var errors = response.getError();
                if(errors) {
                    if(errors[0] && errors[0].message) {
                        console.log(errors[0].message);
                        //that.fireToast('ERROR', 'error', errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                    //that.fireToast('ERROR', 'error', "Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    getCategories: function(component) {
        var that = this;
        var action = component.get("c.getPicklistValues");
        action.setParams({
            'objectName': 'CPA_Costing__c',
            'fieldName': 'Cost_Category__c'
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS") {
                var result = response.getReturnValue(); if(result.status === "OK") {
                     /*var json = {
                        label:"--None--",
                        value:""
                    };
                    (result.statuses).unshift(json);*/
                    component.set('v.categoryPicklist', result.statuses);
                } else {
                    console.log(result.msg);
                }
            }
            else if(state === "ERROR") {
                var errors = response.getError();
                if(errors) {
                    if(errors[0] && errors[0].message) {
                        console.log(errors[0].message);
                        //that.fireToast('ERROR', 'error', errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                    //that.fireToast('ERROR', 'error', "Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    getCostType: function(component) {
        var that = this;
        var action = component.get("c.getPicklistValues");
        action.setParams({
            'objectName': 'CPA_Costing__c',
            'fieldName': 'cost_type__c'
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS") {
                var result = response.getReturnValue(); if(result.status === "OK") {
                     /*var json = {
                        label:"--None--",
                        value:""
                    };
                    (result.statuses).unshift(json);*/
                    component.set('v.costTypePicklist', result.statuses);
                } else {
                    console.log(result.msg);
                }
            }
            else if(state === "ERROR") {
                var errors = response.getError();
                if(errors) {
                    if(errors[0] && errors[0].message) {
                        console.log(errors[0].message);
                        //that.fireToast('ERROR', 'error', errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                    //that.fireToast('ERROR', 'error', "Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    getAppliedTo: function(component) {
        var that = this;
        var action = component.get("c.getPicklistValues");
        action.setParams({
            'objectName': 'CPA_Costing__c',
            'fieldName': 'Applied_to__c'
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS") {
                var result = response.getReturnValue(); if(result.status === "OK") {
                     /*var json = {
                        label:"--None--",
                        value:""
                    };
                    (result.statuses).unshift(json);*/
                    component.set('v.appliedToPicklist', result.statuses);
                } else {
                    console.log(result.msg);
                }
            }
            else if(state === "ERROR") {
                var errors = response.getError();
                if(errors) {
                    if(errors[0] && errors[0].message) {
                        console.log(errors[0].message);
                        //that.fireToast('ERROR', 'error', errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                    //that.fireToast('ERROR', 'error', "Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    getIOREOR: function(component) {
        var that = this;
        var action = component.get("c.getPicklistValues");
        action.setParams({
            'objectName': 'CPA_Costing__c',
            'fieldName': 'IOR_EOR__c'
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS") {
                var result = response.getReturnValue(); if(result.status === "OK") {
                     /*var json = {
                        label:"--None--",
                        value:""
                    };
                    (result.statuses).unshift(json);*/
                    component.set('v.IOREORPicklist', result.statuses);
                } else {
                    console.log(result.msg);
                }
            }
            else if(state === "ERROR") {
                var errors = response.getError();
                if(errors) {
                    if(errors[0] && errors[0].message) {
                        console.log(errors[0].message);
                        //that.fireToast('ERROR', 'error', errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                    //that.fireToast('ERROR', 'error', "Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    fireToast: function(title, type, message) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": title,
            "type": type,
            "message": message
        });
        toastEvent.fire();
    }
})