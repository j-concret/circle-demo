/**
 * @description       : 
 * @author            : ChangeMeIn@UserSettingsUnder.SFDoc
 * @group             : 
 * @last modified on  : 03-03-2021
 * @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
 * Modifications Log 
 * Ver   Date         Author                               Modification
 * 1.0   02-24-2021   ChangeMeIn@UserSettingsUnder.SFDoc   Initial Version
**/
({
	doInit:function(cmp,event,helper){
		cmp.set('v.items', [1]);
		var picklistArray = ["Client_Type__c","Country__c"];
		helper.getPicklistData(cmp, "Lead", picklistArray);
		helper.getLeadOwner(cmp);
		//helper.getValidDomain(cmp);
		//helper.getCompanyWithLeads(cmp);
  },
	showHeader : function(cmp,event,helper){
    cmp.set('v.isFirstRowVisible',true);
	},
	afterScriptsLoaded : function(cmp, event, helper) {
    	helper.loadScript(cmp);
	},
	validate: function(component, event, helper) {
    	helper.validateData(component);
	},
	dataCheck: function(component, event, helper) {
    	helper.dataCheck(component);
	},
	doInsert: function (component, event, helper) {
		helper.insertLeads(component);
	}
})