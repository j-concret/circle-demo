/**
 * @description       : 
 * @author            : ChangeMeIn@UserSettingsUnder.SFDoc
 * @group             : 
 * @last modified on  : 03-15-2021
 * @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
 * Modifications Log 
 * Ver   Date         Author                               Modification
 * 1.0   02-24-2021   ChangeMeIn@UserSettingsUnder.SFDoc   Initial Version
**/
({
    loadScript : function(cmp) {
        var self = this;
        
        $('#catTableData').on('paste', 'input', function(e){
            var $this = $(this);
            cmp.set('v.showSpinner', true);
            $.each(e.originalEvent.clipboardData.items, function(i, v){
                if (v.type === 'text/plain'){
                    v.getAsString(function(text){
                        var x = $this.closest('td').index(),
                            y = $this.closest('tr').index(),
                            obj = {};
                        text = text.trim('\n');
                        console.log('text',text.split('\n').length)
                        
                        $.each(text.split('\n'), function (i2, v2) {
                            
                            $.each(v2.split('\t'), function(i3, v3){
                                
                                var row = y + i2, col = x + i3;
                                obj['cell-' + row + '-' + col] = v3;
                                $this.closest('table').find('tr:eq('+row+') td:eq('+col+') input').val(v3);
                                
                            });
                            
                            if ($this.closest('table').find('tr:eq(' + (y + i2 + 1) + ')').length <= 0 && i2 != text.split('\n').length-1) {
                                $this.closest('table').append("<tr><td><input class='slds-input' type='text'/></td><td><input class='slds-input' type='text'/></td><td><input class='slds-input' type='text'/></td><td><input class='slds-input' type='text'/></td><td><input class='slds-input' type='text'/></td><td><input class='slds-input' type='text'/></td><td><input class='slds-input' type='text'/></td><td><input class='slds-input' type='text'/></td><td><input class='slds-input' type='text'/></td><td><input class='slds-input' type='text'/></td><td><input class='slds-input' type='text'/></td><td><input class='slds-input' type='text'/></td><td><input class='slds-input' type='text'/></td><td><input class='slds-input' type='text'/></td><td><input class='slds-input' type='text'/></td><td><input class='slds-input' type='text'/></td></tr>");
                            }
                        });
                        
                        self.retriveDomainAndCompanyFromTable(cmp);
                    });
                    
                }
            });
            
            return false;
        })
        
    },
    retriveDomainAndCompanyFromTable :function(cmp){
        var self = this; 
        var domains = [];
        let country = [];
        var leadOwners = cmp.get('v.leadOwners');
        var ownersWithCompany = {};
        var headers = cmp.get('v.columns');
        var v3;
        $('#catTableData tr').has('td').each(function(index) {
            let arr = [] ;
            
            var domaintext = '';
            $('td', $(this)).each(function (i3, item) {
                v3 = $(item).find("input").length > 0 ? $(item).find("input").val().trim() : '';
                if(headers.includes(v3))
                    return;
                if (i3 == 1) {
                    domaintext = v3;
                }
                if (i3 == 5 && v3 != '' && !country.includes(v3)) {
                    country.push(v3);
                }
                if (i3 == 7 && v3 != '' && !domains.includes(v3.substr(v3.indexOf('@')+1))) {
                    domains.push(v3.substr(v3.indexOf('@')+1));
                }
                if (i3 == 13) {
                    if (ownersWithCompany[leadOwners[v3.toLowerCase()]]) {
                        arr = ownersWithCompany[leadOwners[v3.toLowerCase()]];
                    } 
                    if(!arr.includes(domaintext))
                        arr.push(domaintext);
                    
                    ownersWithCompany[leadOwners[v3.toLowerCase()]] = arr;
                    
                }
                
            });
        });
        console.log('domains',domains);
        for (const [key, value] of Object.entries(ownersWithCompany)) {
            ownersWithCompany[key] = Object.values(value).join(',');
            console.log(typeof(ownersWithCompany[key]));
        }
        console.log('ownersWithCompany',ownersWithCompany);
        console.log('country',country);
        self.getCompanyAndDomainData(cmp,country,domains,ownersWithCompany);
        cmp.set('v.showSpinner', false);
    },
    validateData:function(cmp) {
        cmp.set('v.showSpinner', true);
        cmp.set("v.isSave", false);
        const mailformat = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        var phone = /^(\+|00)[1-9][0-9() -]{8,16}[0-9]$/;
        var val,isValidationPassed = false;
        var DomainRegionMap = cmp.get('v.DomainRegionMap');
        var validRegionCountries = cmp.get('v.validRegionCountries');
        var validDomain = Object.keys(DomainRegionMap);
        var firstName = [], lastName = [], company = [], email = [];
        var leadOwners = cmp.get('v.leadOwners');
        $('#catTableData tr').has('td').each(function(index) {
            var lineItems = [];
            var self = this;
            var companyLabel = '';
            var countryLabel = '';
            var validate = true, error = [],count = 1;
            $('td', $(this)).each(function (index, item) {
                
                val = $(item).find("input").length > 0 ? $(item).find("input").val().trim() : '';
                
                switch (index) {
                    case 0:
                        validate = cmp.get('v.xant').includes(val.toLowerCase());
                        if (!validate) {
                            error.push('xant error');
                        }
                        break;
                    case 1:
                        companyLabel = val;
                        break;
                    case 2:
                        validate = cmp.get('v.clientType').includes(val);
                        if (!validate) {
                            error.push('The client type is invalid');
                        }
                        break;
                    case 5:
                        countryLabel = val;
                        validate = cmp.get('v.country').includes(val) && (validRegionCountries.length != 0 && validRegionCountries.includes(countryLabel)) ? true : false;
                        
                        if (!validate) {
                            error.push('The Country is invalid');
                        }
                        break;
                    case 7:  
                        if (mailformat.test(val)) {
                            var domain_val = (val.substr(val.indexOf('@')+1)).toLowerCase();
                            if (validDomain.includes(domain_val)) {
                                if (email != null && email.includes(val)) {
                                    error.push('Duplicate email');
                                }else {
                                    var domainObj = DomainRegionMap[domain_val];
                                    if(domainObj.Company__r.Number_of_Leads__c == null || domainObj.Company__r.Number_of_Leads__c < 50 || domainObj.Company__r.Extra_Leads_Approved__c){
                                        if (domainObj.Company__r.Number_of_Leads__c == null) {
                                            domainObj.Company__r.Number_of_Leads__c = 0;
                                        }
                                        domainObj.Company__r.Number_of_Leads__c += 1;
                                    }else{
                                        error.push('Maximum Lead Limit Reached');
                                    }
                                } 
                            }else{
                                error.push('Invalid Company');
                            }
                            
                        } else {
                            
                            error.push('Email is invalid');
                        }
                        
                        break;
                    case 8:
                        validate = val == '' || phone.test(val) ? true : false;
                        if (!validate) {
                            error.push('Phone is invalid');
                        }
                        break;
                    case 13:
                        if (leadOwners[val.toLowerCase()] == '') {
                            error.push('Invalid Lead Owner');
                        }else if (!cmp.get('v.companyWithOwners').includes(companyLabel+'-'+val)) {
                            error.push('Lead Owner not Authorized');
                        } else {
                            val = leadOwners[val.toLowerCase()];
                        }
                        break;
                        
                }
                lineItems.push(val)
                //console.log('lineItems', lineItems);
                
            });
            if (error != null && error.length > 0) {
                isValidationPassed = true;
                $(this).find('td:eq(14) input').val(error.join(';'));
                $(this).find('td:eq(15) input').val('Fail');
                $(this).removeClass('pass').addClass("validatefail");
                lineItems['15'] = 'Fail';
            } else {
                $(this).find('td:eq(14) input').val('');
                $(this).find('td:eq(15) input').val('Pass');
                $(this).removeClass('validatefail').addClass("pass");
                lineItems['15'] = 'Pass';
            }
            if (!lineItems.includes('Fail')) {
                //arrayItem.set(index, lineItems.join(','));
                company.push(lineItems[1]);
                firstName.push(lineItems[3]);
                lastName.push(lineItems[4])
                email.push(lineItems[7]);
            }
            cmp.set('v.showSpinner', false);
            //cmp.set('v.data', arrayItem);
            cmp.set('v.firstName', firstName);
            cmp.set('v.lastName', lastName);
            cmp.set('v.email', email);
            cmp.set('v.company', company);
        });
        
    },
    
    getPicklistData : function(cmp, object, arr) {
        var self = this;
        self.serverCall(cmp, 'getPicklistsValues', { object_name: object, field_names: arr },
                        function(response) {
                            console.log(response);
                            for (const [key, value] of Object.entries(response)) {
                                if (key === 'Client_Type__c') {
                                    cmp.set('v.clientType', response[key]);
                                }
                                if (key === 'Country__c') {
                                    cmp.set('v.country', response[key]);
                                }
                            }
                        }, function (err) {
                            console.log(err);
                            //self.showToast('Error','ERROR',err)
                        });
    },
    
    dataCheck: function (cmp) {
        cmp.set('v.showSpinner', true);
        var self = this;
        var leadOwners = cmp.get('v.leadOwners');
        //console.log('dataCheck first name', cmp.get('v.firstName'));
        self.serverCall(cmp, 'checkLeadsData', { firstName: cmp.get('v.firstName'), lastName: cmp.get('v.lastName'), email: cmp.get('v.email'), company: cmp.get('v.company') },
                        function (response) { 
                            cmp.set('v.showSpinner', false);
                            if (response != []) {
                                $('#catTableData tr').has('td').each(function (index) { 
                                    var lineItems = [],val, email = '';
                                    $('td', $(this)).each(function (index, item) { 
                                        var v = $(item).find("input").length > 0 ? $(item).find("input").val().trim() : '';
                                        if (index == 1 || index ==3 || index ==4) {
                                            val = (val ? val + '-' : '') + v;
                                        }
                                        if (index ==7) {
                                            email = v;
                                        }
                                        if (index == 13) {
                                            
                                            v = leadOwners[v.toLowerCase()];
                                        }
                                        lineItems.push(v);
                                        
                                    });
                                    
                                    if (response.includes(val)||response.includes(email)) {
                                        $(this).find('td:eq(14) input').val('Invalid Lead');
                                        $(this).find('td:eq(15) input').val('Fail');
                                        $(this).removeClass('pass').addClass("datacheckfail");
                                    } 
                                    
                                });
                                
                                //console.log('checkLeadsData', arrayItem.values());
                                cmp.set('v.isInsert', true);
                            }
                        },
                        function (err) {
                            cmp.set('v.showSpinner', false);
                            //console.log(err);
                            self.showToast('Error', 'ERROR', err);
                        });
    },
    insertLeads: function (cmp) {
        cmp.set('v.showSpinner', true);
        var self = this;
        var newleads = [], val;
        var headers = cmp.get('v.columns');
        var DomainRegionMap = cmp.get('v.DomainRegionMap');
        var leadOwners = cmp.get('v.leadOwners');
        $('#catTableData tr').has('td').each(function(ind) {
            var self = this;
            var colData = {ind};
                           var flag = true;
                           $('td', $(this)).each(function (index, item) {
                           
                           val = $(item).find("input").length > 0 ? $(item).find("input").val().trim() : '';
                           if (index == 13) {
                           colData[headers[index]] = leadOwners[val.toLowerCase()] ? leadOwners[val.toLowerCase()] : '';
                          }
            else if (index == 15 && val == 'Fail') {
                flag = false;
            } else {
                colData[headers[index]] = val;
            }
            
        });
        if (flag) {
            var email = colData['Email'];
            var domain_val = (email.substr(email.indexOf('@')+1)).toLowerCase();
            var domainObj = DomainRegionMap[domain_val];
            if (domainObj) {
                colData['Related_Company'] = domainObj.Company__c;
            }
            newleads.push(colData);
        }
        
    });
    //console.log(JSON.stringify(newleads));
    self.serverCall(cmp, 'newLeadInsert', { newLeads: JSON.stringify(newleads) },
 function (response) {
     cmp.set('v.showSpinner', false);
     if (Object.keys(response).length === 0 && response.constructor === Object) {
         self.showToast('Success', 'SUCCESS', 'Leads Inserted Successfully');
         $A.get('e.force:refreshView').fire();
     } else {
         self.showToast('Error', 'ERROR', Object.keys(response).length+' Leads Failed');
         for (const [key, value] of Object.entries(response)) {
             $('#catTableData').find('tr').eq(key).find('td:eq(14) input').val(value.join(','));
             $('#catTableData').find('tr').eq(key).find('td:eq(15) input').val('Fail');
             $('#catTableData').find('tr').eq(key).removeClass('pass').addClass("fail");
         }
     }
     
     //console.log(response);
     //$A.get('e.force:refreshView').fire();
 },
 function (err) {
    cmp.set('v.showSpinner', false);
    self.showToast('Error', 'ERROR', err);
});


},
    getLeadOwner: function (cmp) {
        cmp.set('v.showSpinner', true);
        var self = this;
        self.serverCall(cmp, 'getLeadOwners', {}, function (response) { 
            cmp.set('v.showSpinner', false);
            //console.log('leadOwners', JSON.stringify(response));
            cmp.set('v.leadOwners', response);
        }, function (err) { 
            cmp.set('v.showSpinner', false);
            self.showToast('Error', 'ERROR', err);
        });
    },
        getCompanyAndDomainData: function (cmp,country,domains,ownersWithCompany) {
            cmp.set('v.showSpinner', true);
            var self = this;
            self.serverCall(cmp, 'getValidDomains', {countries : country,domains : domains,ownersWithCompany : JSON.stringify(ownersWithCompany)}, function (response) { 
                cmp.set('v.showSpinner', false);
                //console.log('leadOwners', JSON.stringify(response));
                cmp.set('v.validRegionCountries', response['validRegionCountries']);
                cmp.set('v.companyWithOwners',response['companyWithOwners']);
                cmp.set('v.DomainRegionMap',response['DomainRegionMap']);
            }, function (err) { 
                cmp.set('v.showSpinner', false);
                self.showToast('Error', 'ERROR', err);
            });
        },
            showToast : function(title,type,msg) {
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": title,
                    "type" :type,
                    "message": msg
                });
                toastEvent.fire();
            },
})