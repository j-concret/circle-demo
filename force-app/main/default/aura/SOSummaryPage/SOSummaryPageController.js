({
	doInit : function(component, event, helper) {
        
   //  var ShipmentOrder = component.get("v.soidFromFlow");

       var actionShipmentOrder = component.get("c.getShipmentOrder");
       var soId = component.get("v.soidFromFlow");
       component.set("v.iframeUrl","https://tecex.force.com/CustomerPortal/s/shipment-order/"+ soId);
        component.set("v.iframeUrl2","https://tecex--account2--c.cs108.visual.force.com//flow/Community_Shipment_Edit?ShipmentIdFromAnotherFlow="+ soId);

        
        
       actionShipmentOrder.setParams({
            recordId : component.get("v.soidFromFlow")
   	});
   
      //   Add callback behavior for when response is received
    	actionShipmentOrder.setCallback(this, function(response) {
        	var state = response.getState();
        	if (component.isValid() && state === "SUCCESS") {
      			component.set("v.ShipmentOrder", response.getReturnValue());
        	}
        	else {
            	console.log("Failed with state: " + state);
        	}
    	})
        
        $A.enqueueAction(actionShipmentOrder);
    }
    	
})