({
	addAccountRecord: function(component, event) {
        //get the account List from component  
        var accountList = component.get("v.accountList");
        //Add New Account Record
        accountList.push({
            'sobjectType': 'Shipment_Order_Package__c',
            'packages_of_same_weight_dims__c': 0,
            'Weight_Unit__c': 'KGs',
            'Actual_Weight__c':0,
            'Dimension_Unit__c': 'CMs',
            'Length__c': 0,
            'Breadth__c': 0,
            'Height__c': 0
        });
        component.set("v.accountList", accountList);
    },
     
 /*   validateAccountList: function(component, event) {
        //Validate all account records
        var isValid = true;
        var accountList = component.get("v.accountList");
        for (var i = 0; i < accountList.length; i++) {
            if (accountList[i].Name == '') {
                isValid = false;
                alert('Please enter a Costing Name ' + (i + 1));
            }
        }
        return isValid;
    },
  */   
    saveAccountList: function(component, event, helper) {
        component.set("v.is_loading", true); 
        //Call Apex class and pass account list parameters
        var action5 = component.get("c.savePackages");
        var accountList = component.get("v.accountList");
        var CrRolloutId = component.get("v.CreatedRollout");
        
        
        action5.setParams({
            "accList": component.get("v.accountList"),
            RolloutId : component.get("v.CreatedRollout")
        });
        action5.setCallback(this, function(response) {
            component.set("v.is_loading", false); 
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.accountList", []);
                alert('Packages successfully added');
                window.location.replace("https://tecex.lightning.force.com/lightning/r/Roll_Out__c/"+CrRolloutId+"/view");
               // location.reload();  window.location.replace("https://tecex.lightning.force.com/lightning/r/Roll_Out__c/"+CrRolloutId+"/view");
            }
            else{
                
                 alert('Packages are not created, Please send detailed email to support_SF@tecex.com');
                
            }
            
        }); 
        $A.enqueueAction(action5);
    }
    
})