({	doInit : function(component, event, helper) {
    var recordId = component.get("v.customsClearanceDocumentId");
        var action = component.get("c.getParts");
    	action.setParams({"recordId" : recordId});
        action.setCallback(this, function(response) { 
            var state = response.getState();
            var toastEvent = $A.get("e.force:showToast");
            if (state === 'SUCCESS'){
                var responseMap = response.getReturnValue();
                if(responseMap.status == 'OK'){
                    var   dataMap =   responseMap.data;
                    component.set('v.parts_List',dataMap); 
                   }else{
                    toastEvent.setParams({
                        title : 'Error',
                        message:responseMap['message'],
                        duration:' 5000',
                        key: 'info_alt',
                        type: 'error',
                        mode: 'pester'
                    });
                    toastEvent.fire();
                }
            }
            else if (state === "INCOMPLETE") {
                toastEvent.setParams({
                    title : 'Error',
                    message:"Unknown error",
                    duration:' 5000',
                    key: 'info_alt',
                    type: 'error',
                    mode: 'pester'
                });
                toastEvent.fire();
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            toastEvent.setParams({
                                title : 'Error',
                                message:errors[0].message,
                                duration:' 5000',
                                key: 'info_alt',
                                type: 'error',
                                mode: 'pester'
                            });
                            toastEvent.fire();
                        }
                    } else {
                        toastEvent.setParams({
                            title : 'Error',
                            message:"Unknown error",
                            duration:' 5000',
                            key: 'info_alt',
                            type: 'error',
                            mode: 'pester'
                        });
                        toastEvent.fire();
                    }
                }
            
        });
        $A.enqueueAction(action);
    },
    createComponent : function(component, event, helper) {
        var partTable = component.find('partTable');
        var isOpen = component.get('v.isOpen');
		var isCreatingCmp = component.get('v.isCreatingCmp');
        if(isCreatingCmp) return;
        if(isOpen && !isCreatingCmp){
            component.find("newPartDiv").destroy();
            component.set('v.isOpen',false);
            $A.util.addClass(partTable, 'slds-hide');
        }else{
            // Dynamic component creation
            component.set('v.isCreatingCmp',true);
            var outerDiv = component.find('outerDiv');
            component.set('v.isCreatingCmp',true);
            $A.createComponent("c:TaxLineItemsTable",{ 
                'aura:id': 'newPartDiv',
                'customsClearanceDocumentId': component.get("v.customsClearanceDocumentId"),
                'parts_List':component.get('v.parts_List')
            },function(newButton, status, errorMessage){
                if (status === "SUCCESS") {
                    var body = outerDiv.get("v.body");
                    body.push(newButton);
                    outerDiv.set("v.body", body);
                    component.set('v.isOpen',true);
                    component.set('v.isCreatingCmp',false);
                    $A.util.removeClass(partTable, 'slds-hide');
                }
            });
        }
    }
})