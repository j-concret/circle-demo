({
  getTaskSummary : function(component) {
    var self = this;
      component.set('v.showSpinner',true);
    self.serverCall(component,'getSummary',{'soId':component.get('v.recordId')},function(response){
      component.set('v.showSpinner',false);
      component.set('v.categories',response);
    },function(err){
      self.showToast('error','ERROR',err);
    });
  },
  refreshAllFlags : function(component) {
    var self = this;
      component.set('v.showSpinner',true);
    self.serverCall(component,'refreshFlags',{'recordId':component.get('v.recordId')},function(response){
      component.set('v.showSpinner',false);
      if(response.status == 'OK'){
        self.showToast('success','Success','Refreshed Successfully!');
        $A.get('e.force:refreshView').fire();
      }
      else{
        component.set('v.msg',response.msg);
        component.set('v.type','error');
      }
    });
  },
  showToast : function(type, title , msg) {
    var toastEvent = $A.get('e.force:showToast');
    toastEvent.setParams({
      'type':type,
      'title': title ,
      'message': msg
    });
    toastEvent.fire();
  },
  navigateToCmponent: function(component) {
    var navService = component.find('navService');
    // Uses the pageReference definition in the init handler
    var pageReference = component.get('v.pageReference');
    navService.navigate(pageReference);
  },
     getStatus : function(component) {
        var self = this;
         component.set('v.showSpinner',true);
        self.serverCall(component,'getShipmentStatus',{'soId':component.get('v.recordId')},function(response){
            component.set('v.showSpinner',false);
            if(response.data[0].Shipping_Status__c){
                if(response.data[0].Shipping_Status__c === 'In transit to country' || response.data[0].Shipping_Status__c === 'In Transit to Hub' || response.data[0].Shipping_Status__c === 'Arrived at Hub'){
                    var lst=[];
                    component.set("v.shippingStatusAvail", true);
                    lst.push({"Name" : 'In Transit', "colour" : 2});
                    lst.push({"Name" : 'Arrived in Country', "colour" : 1});//Arrived at Customs
                    lst.push({"Name" : 'Cleared Customs', "colour" : 1});
                    lst.push({"Name" : 'Final Delivery in Progress', "colour" : 1}); //Final Delivery in Transit  
                    lst.push({"Name" : 'Delivered', "colour" : 1});
                    component.set("v.status", lst);
                }else if(response.data[0].Shipping_Status__c == 'Arrived in Country, Awaiting Customs Clearance'){
                    var lst=[];
                    component.set("v.shippingStatusAvail", true);
                    lst.push({"Name" : 'In Transit', "colour" : 2});
                    lst.push({"Name" : 'Arrived in Country', "colour" : 2});//Arrived at Customs
                    lst.push({"Name" : 'Cleared Customs', "colour" : 1});
                    lst.push({"Name" : 'Final Delivery in Progress', "colour" : 1}); //Final Delivery in Transit 
                    lst.push({"Name" : 'Delivered', "colour" : 1});
                    component.set("v.status", lst);
                }else if(response.data[0].Shipping_Status__c === 'Cleared Customs'){
                    var lst=[];
                    component.set("v.shippingStatusAvail", true);
                    lst.push({"Name" : 'In Transit', "colour" : 2});
                    lst.push({"Name" : 'Arrived in Country', "colour" : 2});//Arrived at Customs
                    lst.push({"Name" : 'Cleared Customs', "colour" : 2});
                    lst.push({"Name" : 'Final Delivery in Progress', "colour" : 1}); //Final Delivery in Transit 
                    lst.push({"Name" : 'Delivered', "colour" : 1});
                    component.set("v.status", lst);
                }else if(response.data[0].Shipping_Status__c === 'Final Delivery in Progress'){
                    var lst=[];
                    component.set("v.shippingStatusAvail", true);
                    lst.push({"Name" : 'In Transit', "colour" : 2});
                    lst.push({"Name" : 'Arrived in Country', "colour" : 2});//Arrived at Customs
                    lst.push({"Name" : 'Cleared Customs', "colour" : 2});
                    lst.push({"Name" : 'Final Delivery in Progress', "colour" : 2}); //Final Delivery in Transit  
                    lst.push({"Name" : 'Delivered', "colour" : 1});
                    component.set("v.status", lst);
                }else if(response.data[0].Shipping_Status__c === 'Awaiting POD' || response.data[0].Shipping_Status__c === 'POD Received' || response.data[0].Shipping_Status__c === 'Customs Clearance Docs Received'){
                    var lst=[];
                    component.set("v.shippingStatusAvail", true);
                    lst.push({"Name" : 'In Transit', "colour" : 2});
                    lst.push({"Name" : 'Arrived in Country', "colour" : 2});//Arrived at Customs
                    lst.push({"Name" : 'Cleared Customs', "colour" : 2});
                    lst.push({"Name" : 'Final Delivery in Progress', "colour" : 2}); //Final Delivery in Transit  
                    lst.push({"Name" : 'Delivered', "colour" : 2});
                    component.set("v.status", lst);
                }
                
                
            }
            console.log(JSON.stringify(response));
        },function(err){
            self.showToast('error','ERROR',err);
        });
    },
});