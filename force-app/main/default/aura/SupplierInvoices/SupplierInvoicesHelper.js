({
    checkForCommunity : function(component) {
        var that = this;
        var action = component.get("c.isCommunity");
        action.setParams({
            'shipmentId': component.get('v.shipmentId')
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS") {
                var result = response.getReturnValue(); 
                if(result.status === "OK") {
                    component.set('v.isCommunityOpen', result.isCommunityOpen);
                    if(result.isCommunityOpen){
                        let contact = result.comContact;
                        let account = contact.Account;
                          
                        let shipmentOrder = result.comShip;
                        component.set('v.accountLookUpRecord',account);
                        component.set('v.shipmentOrderLookUpRecord',shipmentOrder);
                         console.log(JSON.stringify(component.get('v.accountLookUpRecord')));
        console.log(JSON.stringify(component.get('v.shipmentOrderLookUpRecord')));
                    }else{
                        let shipment = result.comShip;  
                        let account = shipment.SupplierlU__r;
                        
                        let shipmentOrder = shipment;
                        console.log(JSON.stringify(account));
                         console.log(JSON.stringify(shipmentOrder));
                         component.set('v.accountLookUpRecord',account);
                        component.set('v.shipmentOrderLookUpRecord',shipmentOrder);
                        console.log(JSON.stringify(component.get('v.accountLookUpRecord')));
                         console.log(JSON.stringify(component.get('v.accountLookUpRecord')));
        console.log(JSON.stringify(component.get('v.shipmentOrderLookUpRecord')));
                    }
                    
                } else {
                    console.log(result.msg);
                }
            }
            else if(state === "ERROR") {
                var errors = response.getError();
                if(errors) {
                    if(errors[0] && errors[0].message) {
                        console.log(errors[0].message);
                        //that.fireToast('ERROR', 'error', errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                    //that.fireToast('ERROR', 'error', "Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    getCurrencies : function(component) {
        var that = this;
        var action = component.get("c.getPicklistValues");
        action.setParams({
            'objectName': 'Invoice_New__c',
            'fieldName': 'Invoice_Currency__c'
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS") {
                var result = response.getReturnValue(); if(result.status === "OK") {
                    /*var json = {
                        label:"--None--",
                        value:""
                    };
                    (result.statuses).unshift(json);*/
                    component.set('v.invoiceCurrency',result.statuses[0].value);
                    component.set('v.currencyPicklist', result.statuses);
                } else {
                    console.log(result.msg);
                }
            }
            else if(state === "ERROR") {
                var errors = response.getError();
                if(errors) {
                    if(errors[0] && errors[0].message) {
                        console.log(errors[0].message);
                        //that.fireToast('ERROR', 'error', errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                    //that.fireToast('ERROR', 'error', "Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    onShipmentChanged: function(component) {
        var that = this;
        var action = component.get("c.getCreatedInvoicesOnShipment");
        action.setParams({
            'shipmentId': component.get('v.shipmentOrderLookUpRecord').Id,
            'accountId':component.get('v.accountLookUpRecord').Id
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS") {
                var result = response.getReturnValue(); if(result.status === "OK") {
                    /*var json = {
                        label:"--None--",
                        value:""
                    };
                    (result.statuses).unshift(json);*/
                    console.log('invoices Changes ');
                    
                    for(let invoice of result.invoicesList){
                        invoice.Invoice_Amount_Local_Currency__c = parseFloat((invoice.Invoice_Amount_Local_Currency__c)).toFixed(2);
                        invoice.Invoice_amount_USD__c = parseFloat((invoice.Invoice_amount_USD__c)).toFixed(2);
                    }
                    component.set('v.invoicesOnShipment', result.invoicesList);
                } else {
                    console.log(result.msg);
                }
            }
            else if(state === "ERROR") {
                var errors = response.getError();
                if(errors) {
                    if(errors[0] && errors[0].message) {
                        console.log(errors[0].message);
                        //that.fireToast('ERROR', 'error', errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                    //that.fireToast('ERROR', 'error', "Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    onCreateSupplierInvoice: function(component) {
        var that = this;
        var isInvoiceNotSame = true;
        var invoiceNum = component.find("invoiceNum");
        var invoiceList = component.get('v.invoicesOnShipment');
        
        for(let invoice of invoiceList){
            if(invoice.Invoice_Name__c == component.get('v.InvoiceNumber')){
                isInvoiceNotSame = false;
                break;
            }
        }
        
        var action = component.get("c.CreateSInvoices");
        
        action.setParams({
            'shipmentId': component.get('v.shipmentOrderLookUpRecord').Id,
            'accId':component.get('v.accountLookUpRecord').Id,
            'curr':component.get('v.invoiceCurrency'),
            'invDate':component.get('v.invoiceDate'),
            'invNum':component.get('v.InvoiceNumber'),
            'totalInvPerDoc':component.get('v.TotalInvoiceAmountperDocument'),
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS") {
                var result = response.getReturnValue(); if(result.status === "OK") {
                    /*var json = {
                        label:"--None--",
                        value:""
                    };
                    (result.statuses).unshift(json);*/
                    console.log('invoices Changes ');
                    component.set('v.createdInvoiceObject', result.createdInvoice);
                    that.fireToast('SUCCESS', 'success', 'Invoice Created Successfully !');
                    component.set('v.isLoading',false);
                    component.set('v.isNewInvoiceCreated',true);
                    that.onShipmentChanged(component);
                } else {
                    console.log(result.msg);
                    that.fireToast('ERROR', 'error', result.msg);
                    component.set('v.isLoading',false);
                }
            }
            else if(state === "ERROR") {
                var errors = response.getError();
                if(errors) {
                    if(errors[0] && errors[0].message) {
                        console.log(errors[0].message);
                        component.set('v.isLoading',false);
                        that.fireToast('ERROR', 'error', errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                    component.set('v.isLoading',false);
                    that.fireToast('ERROR', 'error', "Unknown error");
                }
            }
        });
        if(isInvoiceNotSame){
            invoiceNum.setCustomValidity("");
            $A.enqueueAction(action);
        }else{
            invoiceNum.setCustomValidity("Invoice already exists with this invoice number");
        }
        invoiceNum.reportValidity();
    },
    fireToast: function(title, type, message) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": title,
            "type": type,
            "message": message
        });
        toastEvent.fire();
    }
})