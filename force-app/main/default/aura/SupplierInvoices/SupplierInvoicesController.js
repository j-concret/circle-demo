({
	doInit : function(component, event, helper) {
        var shipmentId = component.get('v.shipmentId');
        if(!shipmentId){
            var myPageRef = component.get("v.pageReference");
            if(myPageRef != null){
                shipmentId = myPageRef.state.c__shipmentId;
            	component.set('v.shipmentId',shipmentId);
                helper.checkForCommunity(component);
            }
        }
        helper.getCurrencies(component);
	},
    shipmentChanged : function(component, event, helper) {
        console.log(JSON.stringify(component.get('v.accountLookUpRecord')));
        console.log(JSON.stringify(component.get('v.shipmentOrderLookUpRecord')));
        if(component.get('v.accountLookUpRecord') != undefined && component.get('v.shipmentOrderLookUpRecord') != undefined){
            helper.onShipmentChanged(component);}
	},
    CreateSupplierInvoice: function(component, event, helper) {
        component.set('v.isLoading',true);
		helper.onCreateSupplierInvoice(component);
	},
})