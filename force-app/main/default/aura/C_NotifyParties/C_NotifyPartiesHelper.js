({
    handleDeleteRow: function(component, event, helper) {
        var index = event.getSource().get('v.value');
        var allNotifyParties = component.get("v.notifyParties");
        var notifyPartyToDelete = allNotifyParties[index];
        allNotifyParties.splice(index, 1);
        
        
        var params = {
            notifyId : notifyPartyToDelete.Id
        };
        this.makeServerCall(component, 'deleteNotifyParty', params, function(result) {
            if(result.status === 'OK') {
                
            }
            else {
                helper.showToast('error', 'Error Occured', result.msg);
            }
            
        });
        if(!allNotifyParties[0]){
            component.set("v.notifyParties", []);
            return;
            
        }
        component.set("v.notifyParties", allNotifyParties);
        
    },
    
    //Server call to get Notify Parties
    notifyAllParties: function(component, helper) {
        component.set('v.showInnerSpinner', true);
        component.set("v.notifyParties", []);
        var params = {
            SOID: component.get('v.recordId')
        };
        this.makeServerCall(component, 'GetNotifyPartiesContacts', params, function(result) {
            if(result.status === 'OK') {
                component.set('v.notifyParties', result.notifyParties);
                if(result.notifyParties){
                    if(result.notifyParties.length > 4){
                        component.set("v.scroll", true);
                    }
                    
                }
                
            }
            else {
                helper.showToast('error', 'Error Occured', result.msg);
            }
            component.set('v.showInnerSpinner', false);
        });
    },
})