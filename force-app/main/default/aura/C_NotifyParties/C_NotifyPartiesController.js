({
    onDeletionOfNP : function(component, event, helper) {
        helper.handleDeleteRow(component, event, helper);		
    }, 
    createNotifyParties: function (component) {
        var createRecordEvent = $A.get('e.force:createRecord');
        if ( createRecordEvent ) {
            createRecordEvent.setParams({
                'entityApiName': 'Notify_Parties__c'
            });
            createRecordEvent.fire();
        } else {
            alert("Notify Parties creation not supported");
        }
    },
    onRefreshOfNP: function(component, event, helper) {
        helper.notifyAllParties(component, event);		
    },
    doInitCtrl : function(component, event, helper) {
        
        var baseUrl = window.location.origin;
        component.set("v.domainUrl",baseUrl);
    },
    //Open Add NotifyParties component in new tab.
    onAddUpdate: function(component, event, helper) {
        var pageReference = {
            type: 'standard__component',
            attributes: {
                componentName: 'c__c_notifyUpdateParties',
            },
            state: {
                "c__recordId": component.get("v.recordId")
            }
        };
        var workspaceAPI = component.find("workspace");
        var navService = component.find("navService");
        workspaceAPI
        .isConsoleNavigation()
        .then(function(isConsole) {
            if (isConsole) {
                //  // in a console app - generate a URL and then open a subtab of the currently focused parent tab
                navService.generateUrl(pageReference).then(function(cmpURL) {
                    workspaceAPI
                    .getEnclosingTabId()
                    .then(function(tabId) {
                        return workspaceAPI.openSubtab({
                            parentTabId: tabId,
                            url: cmpURL,
                            focus: true
                        });
                    })
                    .then(function(subTabId) {
                        // the subtab has been created, use the Id to set the label
                        
                        workspaceAPI.setTabLabel({
                            tabId: subTabId,
                            label: "Client Contacts"
                            
                        });
                        workspaceAPI.setTabIcon({
                            tabId: subTabId,
                            icon: "standard:contact",
                            iconAlt: "Contact"
                        });
                    });
                });
            } else {
                // this is standard navigation, use the navigate method to open the component
                navService.navigate(pageReference, false);
            }
        })
        .catch(function(error) {
            console.log(error);
        });
        // var navService = component.find("navService").navigate(pageReference);   
    },
})