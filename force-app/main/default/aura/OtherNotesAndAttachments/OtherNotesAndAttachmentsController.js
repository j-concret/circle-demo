({
    doInitCtrl : function(component, event, helper) {
        var shipmentOrder = component.get('v.shipmentOrder');
        var dd = shipmentOrder.Name+' Commercial Invoice';
        var allRequiredDocsName = shipmentOrder.CPA_v2_0__r.Required_Documents_Shipment_Specific__c? shipmentOrder.CPA_v2_0__r.Required_Documents_Shipment_Specific__c.split(';'):[];
    	
        var newDocReq = [dd,'AWB - Air Waybill'];
        for(var q=0;q<allRequiredDocsName.length;q++){
            newDocReq.push(allRequiredDocsName[q].replace(/[&\/\\#,+()$~%.:*?<>{}]/g,''));
        }
        var attachments = component.get("v.attachments");
        var files = [];
        for(var index in attachments){
            let name = attachments[index].fileName;
            name = name.replace(/\.[^/.]+$/, "");
		    if(!newDocReq.includes(name)) {
                files.push(attachments[index]);
            }
        }
        component.set("v.files", files);
    },
    downloadPDFCtrl : function(component, event, helper) {
        var index = event.target.name;
        var files = component.get("v.files");
        var hiddenElement = document.createElement('a');

        if(files[index].id){
            hiddenElement.href = '/sfc/servlet.shepherd/document/download/'+files[index].id;
            hiddenElement.target = '_blank';
            hiddenElement.download = files[index].fileName;
            document.body.appendChild(hiddenElement);
            hiddenElement.click();
        }
    },
    removeAttachmentsCtrl : function(component, event, helper) {
        var index = event.getSource().get("v.name");
        var files = component.get("v.files");
        let file = files.splice(index, 1);
        component.set("v.files", files);
        if(file && file[0].id){
            helper.removeAttachmentsHlpr(component, helper, file[0].id);
        }
    },
    handleUploadFinished:function(component, event, helper) {
        var uploadedFiles = event.getParam("files");
        var files = component.get('v.files');
        for(var i=0;i<uploadedFiles.length;i++){
            files.push({fileSize:'NA',fileLastModifiedDate:(new Date().toJSON().slice(0,10)),ownerName:component.get('v.userName'),id:uploadedFiles[i].documentId ,fileName:uploadedFiles[i].name});
        }
        component.set('v.files',files);
    }
})