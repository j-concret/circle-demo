({
    removeAttachmentsHlpr : function(component, helper, attachmentId){
        component.set("v.showInnerSpinner", true);
        var params = {
            attachmentIds: [attachmentId]
        };
        this.makeServerCall(component,'removeAttachments',params,function(result){
            if(result.status === 'OK'){
                //helper.showToast('success', 'Success', result.msg);
            }
            else{
                helper.showToast('error', 'Error Occured', result.msg);
            }
            component.set("v.showInnerSpinner", false);
        },function(error){
            helper.showToast('error', 'Error Occured', error);
            component.set("v.showInnerSpinner", false);
        });
    }
})