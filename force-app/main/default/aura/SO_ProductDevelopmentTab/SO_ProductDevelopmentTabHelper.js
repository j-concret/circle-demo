({
    
    activeSections: function(component, event){
        var openSections = component.find("ProductDevelopment").get("v.activeSectionName");
        if(openSections.length>0){
            for(var i =0; i<openSections.length; i++){
                if(openSections[i]==="cashManagement") {
                    var editFormCashManagement = component.find("editFormCashManagement");
                    $A.util.toggleClass(editFormCashManagement, "slds-hide");
                    var viewFormCashManagement = component.find("viewFormCashManagement");
                    $A.util.toggleClass(viewFormCashManagement, "slds-hide");
                }else if(openSections[i]==="liabilityCover") {
                    var editFormLiabilityCover = component.find("editFormLiabilityCover");
                    $A.util.toggleClass(editFormLiabilityCover, "slds-hide");
                    var viewFormLiabilityCover = component.find("viewFormLiabilityCover");
                    $A.util.toggleClass(viewFormLiabilityCover, "slds-hide");
                }
                    else if(openSections[i]==="vatRecovery") {
                        var editFormVatRecovery = component.find("editFormVatRecovery");
                        $A.util.toggleClass(editFormVatRecovery, "slds-hide");
                        var viewFormVatRecovery = component.find("viewFormVatRecovery");
                        $A.util.toggleClass(viewFormVatRecovery, "slds-hide");
                    }
                        else if(openSections[i] === 'Unslotted') {
                            var editFormUnslotted = component.find("editFormUnslotted");
                            $A.util.toggleClass(editFormUnslotted, "slds-hide");
                            var viewFormUnslotted = component.find("viewFormUnslotted");
                            $A.util.toggleClass(viewFormUnslotted, "slds-hide");
                        }
            }}}})