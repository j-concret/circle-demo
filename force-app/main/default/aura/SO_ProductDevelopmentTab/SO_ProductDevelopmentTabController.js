({
    //Edit Record
    editCashManagementRecord : function(component, event, helper) {helper.activeSections(component)},
    editLiabilityCoverRecord : function(component, event, helper) {helper.activeSections(component)},
    editVatRecoveryRecord : function(component, event, helper) {helper.activeSections(component)},
    editUnslottedRecord : function(component, event, helper) {helper.activeSections(component)},
    
    //required Fields
    showRequiredFields: function(component, event, helper){
        $A.util.removeClass(component.find("Account__c"), "none");
        $A.util.removeClass(component.find("Ship_to_Country__c"), "none");
        $A.util.removeClass(component.find("IOR_Price_List__c"), "none");
        $A.util.removeClass(component.find("Ship_From_Country__c"), "none");
        $A.util.removeClass(component.find("Shipment_Value_USD__c"), "none");
        $A.util.removeClass(component.find("Client_Contact_for_this_Shipment__c"), "none");
        
    },
    
    //onsubmit
    onRecordSubmit: function (component, event, helper) {
        
        var invalidFields = [];
        //Required Fields
        
       
        
        
        
        
        if(invalidFields.length >0 ){
            for(var i=0; i<invalidFields.length; i++){ $A.util.addClass(invalidFields[i], 'slds-has-error');}
            event.preventDefault();
        }
    },                
    
    
    //onsuccess
    handleSuccess : function(component, event, helper) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({ "title": "Success!",
                              "message": "Saved.",
                              "type": "success"});
        toastEvent.fire();
        helper.activeSections(component);
    },
    
    onError : function(component, event, helper) {
        
        var toastEvent2 = $A.get("e.force:showToast");
        toastEvent2.setParams({
            "title": "Error!",
            "message": "Error."
        });
        toastEvent2.fire();
    },
    
    handleCancel : function(component, event, helper) {
        helper.activeSections(component);
        event.preventDefault();
    },
    RefreshSuccess : function(component, event, helper) {
        var ToastEvent = $A.get("e.force:showToast");
        ToastEvent.setParams({
            "title": "Success!",
            "type": "success",
            "message": "Record save successful."});
        ToastEvent.fire();
        $A.get('e.force:refreshView').fire();
    },
    OnSubmit : function(component, event, helper) {
    },
    OnLoad : function(component, event, helper) {
        var ToastEvent = $A.get("e.force:showToast");
        ToastEvent.setParams({
            "title": "Loaded!",
            "message": ""});
        ToastEvent.fire();
    }
})