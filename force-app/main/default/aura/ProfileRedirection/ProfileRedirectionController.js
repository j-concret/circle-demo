({
    doInit : function(component, event, helper) {
        console.log('Profile redirection');
        var StagingUrl = 'https://app-staging.tecex.com'; // Account2
        var InternalURL = 'https://tecex--staging.my.salesforce.com'; //Account2
        
        //    document.location.href= StagingUrl;
        
        // var StagingUrl = 'https://app.tecex.com'; // Production
        // var InternalURL = 'https://tecex.my.salesforce.com'; //Production
        var siteUrl = window.location.href; //'https://account2-tecex.cs107.force.com/ncpo/s/detail/0051q000004NBzb?fromEmail=1&s1oid=00D1q0000008awq&s1nid=0DB1q00000000oz&s1uid=0051q000004NBzb&s1ext=0&emkind=chatterCommentNotification&emtm=1610615394717'
        var Id = siteUrl.substring(
            siteUrl.lastIndexOf("detail/") + 7,  
            siteUrl.lastIndexOf("?")
        );
        var  NotifyId = siteUrl.substring(
            siteUrl.lastIndexOf("notification?id=") + 16,  
            siteUrl.length
        );
        
        var feedId = siteUrl.substring(
            siteUrl.lastIndexOf("feed/") + 5,  
            siteUrl.lastIndexOf("?")
        );
        //0D5FeedItem, 0D7 feed comment
        //https://account2-tecex.cs102.force.com/ncpo/s/feed/0D51j00000NUZ6g?fromEmail=1&s1oid=00D1j000000Cuo4&s1nid=0DB1v000000kB98&s1uid=0051v0000092v0O&s1ext=0&emkind=chatterPostNotification&emtm=1624689652166&OpenCommentForEdit=1
        // https://account2-tecex.cs160.force.com/ncpo/s/feed/0D75r0000008Uol?fromEmail=1&s1oid=00D5r0000004fHS&s1nid=0DB1v000000kB98&s1uid=0051v0000092v0O&s1ext=0&emkind=chatterCommentNotification&emtm=1619165236684
        console.log('NotifyId');
        console.log(NotifyId);
        if(NotifyId.startsWith('0D5') || NotifyId.startsWith('0D7')){
            feedId = NotifyId;
        }
        var action1 = component.get(
            "c.getUserInfo");
        action1.setCallback(this,
                            function (response) {
                                var state1 =
                                    response.getState();
                                var result1 = response.getReturnValue(); 
                                console.log('result1');
                                console.log(result1);
                                if(! result1.InternalUser){
                                    
                                    console.log(Id.startsWith('005'));
                                    console.log('id ',Id);
                                    if(Id.startsWith('005')){
                                        document.location.href = StagingUrl+'/profile/profile-and-notifications';
                                    }else if(Id.startsWith('500') || Id.startsWith('00T') || feedId.startsWith('0D7') || feedId.startsWith('0D5')){
                                        var action = component.get(
                                            "c.getShipmentId");
                                        action.setParams({
                                            IdToSql: feedId.startsWith('0D7') || feedId.startsWith('0D5') ? feedId: Id,
                                            isTask: Id.startsWith('00T')? true : false
                                        });
                                        action.setCallback(this,
                                                           function (response) {
                                                               var state = response.getState();
                                                               var result = response.getReturnValue(); 
                                                               
                                                               console.log(result);
                                                               Id = result.IdToSql;
                                                               if(result.LinkedRecordId && result.LinkedRecordId.startsWith('a68') && Id.startsWith('500') && result.Origin =='NCP Invoice'){
                                                                   
                                                                   //For Invoice
                                                                   document.location.href = StagingUrl+'/invoices#'+result.LinkedRecordId+'/message/'+Id;
                                                                   
                                                               }else if(result.shipId){
                                                                   if(Id.startsWith('500') && result.shipStatus && result.shipStatus == 'Cost Estimate' && result.Origin =='NCP Order'){
                                                                       
                                                                       //For Quote Case
                                                                       document.location.href = StagingUrl+'/quotes/'+result.shipId +'#message/'+Id;
                                                                   }
                                                                   else if(Id.startsWith('500') && result.shipStatus && result.shipStatus != 'Cost Estimate' && result.Origin =='NCP Order'){
                                                                       
                                                                       //For Order Case
                                                                       document.location.href = StagingUrl+'/shipments-list/'+result.shipId +'#message/'+Id;
                                                                       
                                                                   }
                                                                       else if(Id.startsWith('00T') && result.shipStatus && result.shipStatus != 'Cost Estimate'){
                                                                           
                                                                           //For Order Task
                                                                           document.location.href = StagingUrl+'/shipments-list/'+result.shipId +'#task/'+Id;
                                                                           
                                                                       }
                                                                           else if(Id.startsWith('500') && result.Origin =='NCP General'){
                                                                               //For General Case
                                                                               document.location.href = StagingUrl+'/messages/'+Id;	
                                                                           }
                                                                   //  document.location.href = StagingUrl+'/shipments-list/'+result.shipId ;
                                                               }else if(Id.startsWith('500') && result.Origin =='NCP General'){
                                                                   //For General Case
                                                                   document.location.href = StagingUrl+'/messages/'+Id;	
                                                               }
                                                                   else{
                                                                       document.location.href = StagingUrl;
                                                                   }
                                                               
                                                           });
                                        
                                        
                                        $A.enqueueAction(action);
                                    }else if(NotifyId.startsWith('a0R')){
                                        document.location.href = StagingUrl+'/quotes/'+NotifyId;
                                    }    
                                }else{
                                    
                                    if(NotifyId.startsWith('a0R')){
                                        document.location.href = InternalURL+'/'+NotifyId;
                                    }   else if(NotifyId.startsWith('0D5') || NotifyId.startsWith('0D7')){
                                        document.location.href = InternalURL+'/'+NotifyId ;
                                    }else{
                                        document.location.href = InternalURL+'/'+Id ;
                                    }
                                }
                            });
        
        
        $A.enqueueAction(action1);  
        
    }
    
})