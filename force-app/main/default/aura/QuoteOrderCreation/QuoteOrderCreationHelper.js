({
    doInit : function (cmp){
        var self = this;
        
        self.serverCall(cmp,"getAppName",{},function(response){
            cmp.set("v.appName",response);
            console.log("appName ", response);
            cmp.set('v.showSpinner',false);
        },function(error){
            console.log(error);
            cmp.set('v.showSpinner',false);
        });
    }
})