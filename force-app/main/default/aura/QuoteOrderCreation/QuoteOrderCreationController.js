({
    doInit: function( cmp, event, helper ){
        //var appName = document.querySelector('.slds-context-bar__app-name').textContent;
        cmp.set('v.showSpinner',true);
        helper.doInit(cmp);
    },
    
    QuoteClick : function (cmp, event, helper) { cmp.set("v.displayedSection","QuotePage"); },
    OrderClick : function (cmp, event, helper) { cmp.set("v.displayedSection","OrderPage"); },
    RolloutClick : function (cmp, event, helper) { cmp.set("v.displayedSection","RolloutPage"); },
    ProductMapClick : function (cmp, event, helper) { cmp.set("v.displayedSection","ProductMapPage"); },
    Homereturn :function (cmp, event, helper) { cmp.set("v.displayedSection","HomePage"); }
    
})