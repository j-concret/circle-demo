({
    doInit : function(component, event, helper) {
        
        var responseMap = component.get("v.ccdResponse");
        component.set('v.customs_Clearance_Document',responseMap.data);
        component.set('v.Reason_For_Difference_CDC_Options',responseMap.Reason_For_Difference_CDC_Options);
        component.set('v.Reason_For_Difference_Supplier_Options',responseMap.Reason_For_Difference_Supplier_Options);
        component.set('v.Status_Options',responseMap.Status_Options);
        component.set('v.Reason_For_Query_Options',responseMap.Reason_For_Query_Options);
    },
    clickCreate: function(component, event, helper) {
        component.set('v.loaded',false);
        component.set('v.apiField','');
        component.set('v.msgField','');
        
        var toastEvent = $A.get("e.force:showToast");
        var activeSectionsList = component.get('v.activeSections'); 
        var cSection = ['Reason_For_Difference_CDC__c','Reason_For_Difference_Supplier__c','Elaboration_On_Reason_CDC__c','Reason_For_Supplier_Over_Undercharge__c'];
        var dSection = ['Status__c','Reason_For_Query__c','Elaboration_on_Queried_Status__c','Review_Attachment__c'];
        var fieldRFQ = component.find('Reason_For_Query__c'); 
        $A.util.removeClass(fieldRFQ, 'slds-has-error');
        
        var fieldEOQS = component.find('Elaboration_on_Queried_Status__c'); 
        $A.util.removeClass(fieldEOQS, 'slds-has-error');
        
         var fields = component.get('v.fields');
                        for(let i=0;i<fields.length;i++){
                            var fieldValidity = component.find(fields[i]);
                            if(typeof fieldValidity.setCustomValidity == 'function'){
                                fieldValidity.setCustomValidity('');
                                fieldValidity.reportValidity();
                            }else{
                                $A.util.removeClass(fieldValidity, 'slds-has-error');
                            }
                        }

        if(component.get("v.customs_Clearance_Document.Status__c") == 'Queried' && component.get("v.customs_Clearance_Document.Reason_For_Query__c") == 'None'){
            component.set('v.loaded',true);
            component.set('v.isReasonForQueryError',true);
            
            var fieldRQ = component.find('Reason_For_Query__c'); 
            $A.util.addClass(fieldRQ, 'slds-has-error');
            
            if(activeSectionsList.indexOf('D') == -1){
        	activeSectionsList.push('D');
    		}
            
            component.set('v.isElaborationOnQueriedStatusRequired',false);
            component.set('v.activeSections',activeSectionsList);
            
        }else if(component.get("v.customs_Clearance_Document.Status__c") == 'Queried' && component.get("v.customs_Clearance_Document.Reason_For_Query__c") == 'Other' && (component.get("v.customs_Clearance_Document.Elaboration_on_Queried_Status__c") == '' || component.get("v.customs_Clearance_Document.Elaboration_on_Queried_Status__c") == undefined || component.get("v.customs_Clearance_Document.Elaboration_on_Queried_Status__c") == null)){
            component.set('v.loaded',true);
            component.set('v.isReasonForQueryError',false);
            
            var fieldEOQ = component.find('Elaboration_on_Queried_Status__c'); 
            $A.util.addClass(fieldEOQ, 'slds-has-error');
            
            if(activeSectionsList.indexOf('D') == -1){
        	activeSectionsList.push('D');
    		}
            
            component.set('v.isElaborationOnQueriedStatusRequired',true);
            component.set('v.activeSections',activeSectionsList);
        }else{
            component.set('v.isReasonForQueryError',false);
            component.set('v.isElaborationOnQueriedStatusRequired',false);
            
            var action = component.get("c.updateCCDDetails");
            
            var customs_Clearance_Document=component.get("v.customs_Clearance_Document");
            
            action.setParams({"customsClearanceDocument" :customs_Clearance_Document });
            action.setCallback(this, function(response) { 
                
                var state = response.getState();
                
                var toastEvent = $A.get("e.force:showToast");
                
                component.set('v.loaded',true);
                
                if (state === 'SUCCESS'){
                    
                    var responseMap = response.getReturnValue();
                    console.log(responseMap.status);
                    
                    if(responseMap.status == 'OK'){
                        
                      
                        component.set('v.customs_Clearance_Document',responseMap.data);
                        toastEvent.setParams({
                            title : 'Update',
                            message:'Custom Clearance Document Updated Successfully !',
                            duration:' 5000',
                            key: 'info_alt',
                            type: 'success',
                            mode: 'pester'
                        });
                        toastEvent.fire();  
                    }else if(responseMap.status == 'DMLERROR' && responseMap.field){
                        
                        var field = component.find(responseMap.field[0]);
                        
                        if(cSection.indexOf(responseMap.field[0]) !== -1  && activeSectionsList.indexOf('C') == -1){
                            activeSectionsList.push('C');
                        }else if(dSection.indexOf(responseMap.field[0]) !== -1 && activeSectionsList.indexOf('D') == -1){
                            activeSectionsList.push('D');
                        }
                        
                        component.set('v.activeSections',activeSectionsList);
                        
                        fields.push(responseMap.field[0]);
                        component.set('v.fields',fields);
                        var msg = (responseMap.message).replace(/&quot;/g, '\"');
                        if(typeof field.setCustomValidity == 'function'){
                            field.setCustomValidity(msg);
                            field.reportValidity();
                        }else{
                            $A.util.addClass(field, 'slds-has-error');
                            component.set('v.apiField',responseMap.field[0]);
                            component.set('v.msgField',msg);
                        }
                        
                    }else{
                        component.set('v.loaded',true);
                        toastEvent.setParams({
                            title : 'Error',
                            message:responseMap['message'],
                            duration:' 5000',
                            key: 'info_alt',
                            type: 'error',
                            mode: 'pester'
                        });
                        toastEvent.fire();
                    }
                }
                else if (state === "INCOMPLETE") {
                    component.set('v.loaded',true);
                    toastEvent.setParams({
                        title : 'Error',
                        message:"Unknown error",
                        duration:' 5000',
                        key: 'info_alt',
                        type: 'error',
                        mode: 'pester'
                    });
                    toastEvent.fire();
                }
                    else if (state === "ERROR") {
                        component.set('v.loaded',true);
                        var errors = response.getError();
                        if (errors) {
                            if (errors[0] && errors[0].message) {
                                toastEvent.setParams({
                                    title : 'Error',
                                    message:errors[0].message,
                                    duration:' 5000',
                                    key: 'info_alt',
                                    type: 'error',
                                    mode: 'pester'
                                });
                                toastEvent.fire();
                            }
                        } else {
                            component.set('v.loaded',true);
                            toastEvent.setParams({
                                title : 'Error',
                                message:"Unknown error",
                                duration:' 5000',
                                key: 'info_alt',
                                type: 'error',
                                mode: 'pester'
                            });
                            toastEvent.fire();
                        }
                    }
            });
            $A.enqueueAction(action);
            // component.set('v.loaded',true);
        }
    },
    onChange : function(component, event, helper) {
        console.log('component.get ',component.get('v.customs_Clearance_Document.Review_Attachment__c'));
        //  component.set('v.customs_Clearance_Document.Review_Attachment__c',component.get('v.customs_Clearance_Document.Review_Attachment__c'));
        console.log('component.set ',component.get('v.customs_Clearance_Document.Review_Attachment__c'));
    }
    
    
})