({
	doInit  : function(component, event, helper) {
        
        // Receiving Freight recordID from GetFreightOptions apex class         
        var soId1 = component.get("v.soidFromFlow1");
        var FRID = component.get("c.GetFreightOptionsRecordID");      
              
         FRID.setParams({
         
         SOId : component.get("v.soidFromFlow1")
        });
        
        FRID.setCallback(this,function(a){
            var state = a.getState();
        	if (component.isValid() && state === "SUCCESS") {
            	
                component.set("v.FreightRecordID",a.getReturnValue());
            }
            else {
            	console.log("Failed with state: " + state);
        	}
        }); 
       $A.enqueueAction(FRID); 
        
        
        
                   
    },
    
              
        // This works for the lightening RecordForm 
    onSuccess : function(component, event, helper) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Success!",
            "message": "The record has been Saved successfully."
        });
        toastEvent.fire();
    },
    onSubmit : function(component, event, helper) {
    },
    onLoad : function(component, event, helper) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Loaded!",
            "message": "The record has been Loaded successfully ."
        });
        toastEvent.fire();
    },
    onError : function(component, event, helper) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Error!",
            "message": "Error."
        });
        toastEvent.fire();
    },
    
    
     
   
	
})