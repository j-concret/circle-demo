/**
 * Created by Chibuye Kunda on 2018/11/24.
 */
({

    /** this function will carry out search
     * @param componentP this is the component we updating
     * @param eventP this is component event
     * @param keywordP this is target keyword
     */
    searchForMatches : function( componentP, eventP, keywordP ){

        var fetch_action = componentP.get( "c.fetchMatchingContacts" );
        fetch_action.setParams({
            "search_keywordP" : keywordP,
            "object_nameP" : componentP.get( "v.object_api_name" ),
            "AccIdP":componentP.get( "v.Selected_Acc_id" )
        });//end of parameter

        console.log( 'Keyword: ' + keywordP );

        //define the callback function
        fetch_action.setCallback( this, function( response ){

            var state = response.getState();                //get repsonse state

            //check if we success
            if( state === "SUCCESS" ){

                var store_response = response.getReturnValue();         //get the response

                console.log( 'Response List: ' + store_response );

                //check if we have a length of zero
                if( store_response.length == 0 )
                    componentP.set( "v.message", "No Result Found..." );        //no message to show
                else
                    componentP.set( "v.message", "" );                  //no message

                componentP.set( "v.list_of_records", store_response );

                console.log( "list of records: " + componentP.get( "v.list_of_records" ) )

            }//end if-block

            $A.util.removeClass( componentP.find( "load_spinner" ), "slds-show" );      //remove spinner

        });//end of callback function

        $A.enqueueAction( fetch_action );           //enqueue the action

    }//end of function definition


})