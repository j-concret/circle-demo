({
	init: function (cmp, event, helper) {
        cmp.set('v.columns', [
            
            {label: 'Contact name', fieldName: 'Contact_name__c', editable:'true', type: 'string'},
            {label: 'Contact number', fieldName: 'Contact_number__c', editable:'true', type: 'phone'},
            {label: 'Contact email', fieldName: 'Contact_email__c', editable:'true', type: 'email'},
            {label: 'Delivery address', fieldName: 'Delivery_address__c', editable:'true', type: 'string'}
           
        ]);    
        helper.fetchData(cmp,event, helper);
    },
     handleSaveEdition: function (cmp, event, helper) {
        var draftValues = event.getParam('draftValues');
        console.log(draftValues);
        var action = cmp.get("c.updateSOFinalDeliveries");
        action.setParams({"editedAccountList" : draftValues});
        action.setCallback(this, function(response) {
            
            var state = response.getState();
            if (state === 'SUCCESS'){
            
          // alert("Shipment Order Packages are updated ");
           $A.get('e.force:refreshView').fire();
            }
            else{
                alert("We are in fail block");
            }
            
        });
        $A.enqueueAction(action);
        
    },
    /*page refresh after data save*/    
    isRefreshed: function(component, event, helper) {
       location.reload();
      
        //   $A.get('e.force:refreshView').fire(); 
    },
     
    
    
    
    
})