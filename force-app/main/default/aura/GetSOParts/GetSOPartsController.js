({
	init: function (cmp, event, helper) {
        cmp.set('v.columns', [
            
            {label: 'Part Number', fieldName: 'Name', editable:'true', type: 'string'},
            {label: 'Part Description', fieldName: 'Description_and_Functionality__c', editable:'true', type: 'string'},
            {label: 'Quantity', fieldName: 'Quantity__c', editable:'true', type: 'number'},
            {label: 'Unit Price', fieldName: 'Commercial_Value__c', editable:'true', type: 'number'},
            {label: 'HTS Code', fieldName: 'US_HTS_Code__c', editable:'true', type: 'string'},
            {label: 'Country of Origin', fieldName: 'Country_of_Origin2__c', editable:'true', type: 'string'},
            {label: 'ECCN No', fieldName: 'ECCN_NO__c', editable:'true', type: 'string'}
           
        ]);    
        helper.fetchData(cmp,event, helper);
    },
     handleSaveEdition: function (cmp, event, helper) {
        var draftValues = event.getParam('draftValues');
        console.log(draftValues);
        var action = cmp.get("c.updateSOParts");
        action.setParams({"editedAccountList" : draftValues});
        action.setCallback(this, function(response) {
            
            var state = response.getState();
            if (state === 'SUCCESS'){
            
          // alert("Shipment Order Packages are updated ");
           $A.get('e.force:refreshView').fire();
            }
            else{
                alert("We are in fail block");
            }
            
        });
        $A.enqueueAction(action);
        
    },
    /*page refresh after data save*/    
    isRefreshed: function(component, event, helper) {
       location.reload();
      
        //   $A.get('e.force:refreshView').fire(); 
    },
     
    
    
    
    
})