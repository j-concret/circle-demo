({
    getHeaders: function(cmp){
        var self = this;
        self.serverCall(cmp,'getHeaderJSON',{business : cmp.get("v.business")},function(response){
            if(response){
                console.log("Headers",JSON.parse(response.trim()));
                cmp.set('v.headers',JSON.parse(response.trim()));
            }
        },function(err){
            cmp.set('v.showSpinner',false);
            self.showToast('error','ERROR',err);
        });
    },
    loadScript : function(cmp) {
        var self = this;
        $('#catTableData').on('paste', 'input', function(e){
            var headerColumns = cmp.get('v.headers');
            var $this = $(this);
            var matchedCols = [];
            var matchedColsCols = [];
            $.each(e.originalEvent.clipboardData.items, function(i, v){
                if (v.type === 'text/plain'){
                    v.getAsString(function(text){
                        var x = $this.closest('td').index(),
                            y = $this.closest('tr').index(),
                            obj = {};
                        //text = text.trim('\n');
                        $.each(text.split('\n'), function(i2, v2){
                            $.each(v2.split('\t'), function(i3, v3){
                                v3 = v3.trim();
                                var row = y+i2, col = x+i3;
                                if((i2 == 0 && i3 == 0) || (i2 == 0 && i3 != 0 && matchedCols.length > 0) || row == 0 ){
                                    var isMatched = false;
                                    for (const colmn in headerColumns) {
                                        if(self.contains(headerColumns[colmn],v3)){
                                            isMatched = true;
                                            matchedCols.push(colmn);
                                            matchedColsCols.push(col);
                                            break;
                                        }
                                    }
                                  var splitedHeaders = v2.split('\t');
                                    var coled = i3+1;
                                    let count = Object.keys(headerColumns).length;
                                    if(splitedHeaders.length == coled && matchedColsCols.length < count){
                                        if(matchedColsCols.length == 0){
                                            for(var i=0;i<=count;i++){
                                                for(var j=i; j<splitedHeaders.length;j++){
                                                        if(splitedHeaders[j] != '' && !matchedColsCols.includes(j)){
                                                        matchedColsCols.push(j);
                                            			//matchedCols.push('');
                                                            obj['cell-'+0+'-'+i] = splitedHeaders[j];
                                    $this.closest('table').find('tr:eq('+0+') td:eq('+i+') input').val(splitedHeaders[j]);
                                                           break;
                                                        }
                                                            }
                                        }
                                        }
                                        else{
                                        var diff = count - (matchedColsCols.length);
                                        var minHeaderCol = Math.min(...matchedColsCols);
                                        var maxHeaderCol = Math.max(...matchedColsCols);
                                        if(minHeaderCol != 0 && splitedHeaders[0] != ''){
                                            matchedColsCols.push(0);
                                            matchedCols.push('');
                                        }else{
                                            let countDiff = 0;
                                            for(var i=0;i<matchedColsCols.length;i++){
                                                if(matchedColsCols[i+1] != null){
                                                    var cdiff = matchedColsCols[i+1] - matchedColsCols[i];
                                                    if(cdiff != 1 && countDiff < diff){
                                                        countDiff++;
                                                        for(var j=matchedColsCols[i]+1; j<splitedHeaders.length;j++){
                                                        if(splitedHeaders[j] != '' && !matchedColsCols.includes(j)){
                                                        matchedColsCols.push(j);
                                            			matchedCols.push('');
                                     
                                    
                                                           break;
                                                        }
                                                            }
                                                    }
                                                }
                                            }
                                            if(countDiff < diff){
                                                for(var i=0;i<diff;i++){
                                                 countDiff++;
                                                    for(var j=maxHeaderCol+1; j<splitedHeaders.length;j++){
                                                        if(splitedHeaders[j] != '' && !matchedColsCols.includes(j)){
                                                        matchedColsCols.push(j);
                                            			matchedCols.push('');
                                                           break;
                                                        }
                                                            }
                                                 
                                                }
                                            }
                                        }
                                    }
                                    }
                                     //if(!isMatched && matchedCols.length >0){matchedCols.push('')}
                                }
                                if(i2 != 0 || matchedCols.length == 0){
                                    if(matchedCols.length > 0)row -= 1;
                                    var col1;
                                    if(matchedColsCols.includes(col) || matchedColsCols.length == 0){
                                    if(matchedColsCols.length > 0){
                                    col1 = matchedColsCols.indexOf(col) ;   
                                    }else{
                                    col1 = col;
                                    }
                                    obj['cell-'+row+'-'+col1] = v3;
                                    $this.closest('table').find('tr:eq('+row+') td:eq('+col1+') input').val(v3);
                                    }
                                   var maxHeaderCol = Math.max(...matchedColsCols);
                                   var minHeaderCol = Math.min(...matchedColsCols);
                                   
                                }
                            });

                            if(cmp.get('v.business') != 'Zee' && $this.closest('table').find('tr:eq('+(y+i2+1)+')').length <= 0 ){
                                $this.closest('table').append("<tr><td><input class='slds-input' type='text'/></td><td><input class='slds-input' type='text'/></td><td><input class='slds-input' type='text'/></td><td><input class='slds-input' type='text'/></td></tr>");
                            }
                            if(cmp.get('v.business') == 'Zee' && $this.closest('table').find('tr:eq('+(y+i2+1)+')').length <= 0){
                                $this.closest('table').append("<tr><td><input class='slds-input' type='text'/></td><td><input class='slds-input' type='text'/></td><td><input class='slds-input' type='text'/></td><td><input class='slds-input' type='text'/></td><td><input class='slds-input' type='text'/></td><td><input class='slds-input' type='text'/></td></tr>");
                            }
                        });
                        if(matchedCols.length == 0) {cmp.set('v.isFirstRowVisible',true); cmp.set('v.isFirstVisible',true);}
                        else {
                            var droppedColumns = cmp.get('v.droppedColumns');
                            var columns;
                            if (cmp.get('v.business') != 'Zee') {
                                columns = ['Product Code','Description','Quantity','Unit Price'];
                            } else {
                                columns = ['Part No. / ASIN','Part Description','Quantity','Unit Price','HS code','Country of Origin'];
                            }


                            //cmp.set('v.isFirstRowVisible',false);
                            for(var ind = 0;matchedCols.length >ind;ind++){
                                var val = self.capitalize(matchedCols[ind]);
                                droppedColumns[ind] = val;
                                var iid = columns.indexOf(val);
                                if(iid > -1){
                                    columns.splice(iid,1);
                                }
                            }
                            cmp.set('v.droppedColumns',droppedColumns);
                            cmp.set('v.columns',columns);
                            cmp.set('v.isFirstRowVisible',true);
                            cmp.set('v.isFirstVisible',false);
                        }
                    });
                }
            });
            return false;
        })
        .on('focus change', 'tr:last :input', function() {
            var $tr  = $(this).closest('tr');
            var $clone = $tr.clone();
            $clone.find(':text').val('');
            $tr.after($clone);
        });
    },
    capitalize : function(words) {
        words = words.replace('_',' ');
        var separateWord = words.toLowerCase().split(' ');
        for (var i = 0; i < separateWord.length; i++) {
            separateWord[i] = separateWord[i].charAt(0).toUpperCase() +
                separateWord[i].substring(1);
        }
        return separateWord.join(' ');
    },
    saveLineItems:function(cmp,isSaveNew) {
        //product_code,description,unit_price,quantity
        cmp.set('v.showSpinner',true);
        var isheaderRow = true;
        var lineItems = [];
        var lineItemsWithoutHeaders = [];
        var headerColumns = cmp.get('v.headers');
        var headers = [];
        var self = this;
        
        $('#catTableData tr').has('td').each(function() {
            var arrayItem = {};
            
            $('td', $(this)).each(function(index, item) {
                var val = $(item).find("input").length > 0 ? $(item).find("input").val().trim() : $(item).find("div").length > 0 ? $(item).find("div")[0].innerText : '';
                
                if(val == '') return false;
                if(isheaderRow){
                    for (const col in headerColumns) {
                        if(headerColumns[col].includes(val)){
                            headers.push(col);
                        }
                    }
                }
                else {
                    if(headers[index] == 'quantity'){
                        var removedSpacesText = val.split(" ").join("");
                        val = parseInt(removedSpacesText.replace(/[,$]/g,'').trim());
                    }
                    if(headers[index] == 'unit_price'){ 
                        var removedSpacesText = val.split(" ").join("");
                        
                        var len1 = removedSpacesText.length - 3;
                        var len =removedSpacesText.substring(len1)[0];
                        if(len == ','){
                            removedSpacesText = removedSpacesText.substring(0, len1) + '.' + removedSpacesText.substring(len1 + 1);
                        }
                        
                        val = parseFloat(removedSpacesText.replace(/[,$]/g,'').trim());
                    }
                    arrayItem[headers[index]] = val;
                }
            });
            isheaderRow = false;
            
            if(Object.keys(arrayItem).length == 6 || Object.keys(arrayItem).length == 4){ //
                lineItems.push(arrayItem);
            }else if(Object.keys(arrayItem).length){
                lineItemsWithoutHeaders.push(arrayItem);
            }
        });
        console.log(lineItems);
        /*if (cmp.get('v.business') == 'Zee' && (lineItems.length == 0 || lineItemsWithoutHeaders.length > 0)) {
      cmp.set('v.showSpinner',false);

      self.showToast('Warning','WARNING','Please Add Line Items to Proceed');
      return;
    }*/
      if(lineItemsWithoutHeaders.length == 0){
          if(lineItems.length > 0){
              if (cmp.get('v.business') == 'Zee' && !cmp.get('v.pageReference')) {
                  cmp.set('v.lineItems',lineItems);
                  //Get the event using registerEvent name.
                  var cmpEvent = cmp.getEvent("callParent");
                  //Set event attribute value
                  cmpEvent.setParams({"isSaveNew" : isSaveNew});
                  cmpEvent.fire();
                  cmp.set('v.showSpinner',false);
              }else{
                  cmp.set('v.lineItems',lineItems);
                  cmp.set('v.showSpinner',true);
                  self.saveParts(cmp,isSaveNew);
              }

          }else{
              if (cmp.get('v.business') == 'Zee') {

                  self.showToast('Warning','WARNING','Please Add Line Items to Proceed');
              } else {
                if (!cmp.get('v.isShipment')) {
                  self.serverCall(cmp,'processRolloutBatch',{
                      RolloutId: cmp.get('v.recordId')
                  },function(response){
                      console.log('Rollout job started');
                  },function(err){
                      self.showToast('error','ERROR',err);
                  });

                }
                  if(isSaveNew){
                      window.location.reload();
                  }else{
                      self.showToast('success','SUCCESS','An estimated cost range will be provided if no Line Items are added on quote creation. Please note that this quote cannot be accepted until Line Items have been added.');
                      if(cmp.get("v.newSOPage")){
                        self.closeTab(cmp);
                    }
                    else{
                      self.navigateTab(cmp);
                    }
                  }
              }
              cmp.set('v.showSpinner',false);
          }
      }else{
          self.showToast('error','Error','Please Add Headers');
          cmp.set('v.showSpinner',false);
      }
  },
    saveParts : function(cmp,isSaveNew){
        var self = this;

        self.serverCall(cmp,'insertParts',{
            recordId: cmp.get('v.recordId'),
            parts : JSON.stringify(cmp.get('v.lineItems')),
            isShipment : cmp.get('v.isShipment'),
            RolloutSoids: ( cmp.get('v.RolloutSoids') != null ? cmp.get('v.RolloutSoids') :[])
        },function(response){
            cmp.set('v.showSpinner',false);
            console.log('Line items inserted successfully');
            self.showToast('success','SUCCESS','Line Items inserted successfully!');
            self.closeTab(cmp);
            self.navigateTab(cmp);
        },function(err){
            cmp.set('v.showSpinner',false);
            self.showToast('error','ERROR',err);

        });
    },
    showToast : function(title,type,msg) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": title,
            "type" :type,
            "message": msg
        });
        toastEvent.fire();
    },
    contains: function(a, obj) {
        var i = a.length;
        while (i--) {
            if (a[i].localeCompare(obj) == 0) {
                return true;
            }
        }
        return false;
    },
    closeTab : function(cmp){
        var workspaceAPI = cmp.find("workspace");

        workspaceAPI.openTab({
            pageReference: {
                "type": "standard__recordPage",
                "attributes": {
                    "recordId": cmp.get('v.recordId'),
                    "actionName":"view"
                }
            },
            focus: true
        }).then(function(newTabId){
            workspaceAPI.getEnclosingTabId()
            .then(function(enclosingTabId) {
                workspaceAPI.closeTab({tabId : enclosingTabId});
                workspaceAPI.focusTab(newTabId);
            });
        });
        window.location.href = window.location.origin+'/lightning/r/Shipment_Order__c/'+cmp.get('v.recordId')+'/view';

},
    navigateTab : function(cmp){
        var navService = cmp.find("navService");
        navService.navigate({
            "type": "standard__recordPage",
            "attributes": {
                "recordId": cmp.get('v.recordId'),
                "objectApiName": cmp.set('v.isShipment')?'Shipment_Order__c':'Roll_Out__c',
                "actionName": "view"
            }
        });
    }

})