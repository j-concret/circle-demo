({
    doInit:function(cmp,event,helper){
        var pageRef = cmp.get("v.pageReference");
        if(pageRef && pageRef.state.c__recordId){
            cmp.set("v.recordId",pageRef.state.c__recordId);
            cmp.set("v.newSOPage",pageRef.state.c__newSOPage);
            cmp.set("v.business",pageRef.state.c__business);
        }
        cmp.set('v.items',[1,2,3,4,5]);
        if (cmp.get('v.business') == 'Zee') {
            cmp.set('v.droppedColumns',['','','','','',''] );
            cmp.set('v.columns',['Part No. / ASIN','Part Description','Quantity','Unit Price','HS code','Country of Origin']);
        }else{
            cmp.set('v.droppedColumns',['','','',''] );
            cmp.set('v.columns',['Product Code','Description','Quantity','Unit Price']);
        }
        helper.getHeaders(cmp);
    },
    afterScriptsLoaded : function(cmp, event, helper) {
        helper.loadScript(cmp);
    },
    clearData:function(cmp,event,helper){
        var pageRef = cmp.get("v.pageReference");
        if(pageRef && pageRef.state.c__recordId){
            $A.get('e.force:refreshView').fire();
        }else{
            cmp.set('v.Step','1');
        cmp.set('v.currentStep','1');
        cmp.set('v.Step','2');
        cmp.set('v.currentStep','2');
        }
        
        
        /*cmp.set('v.refreshData',false);
         cmp.set('v.items',[1,2,3,4,5]);
    helper.getHeaders(cmp);
        cmp.set('v.refreshData',true);*/
    } ,
    getMessage : function(cmp, event,helper){
         var params = event.getParam('arguments');
         console.log("params",params.isSaveNew);
        /*var params = event.getParam('arguments');
        if (params && params.isSaveNew) {
            helper.saveLineItems(component,true);
        }else{
            helper.saveLineItems(component,false);
        }*/
        cmp.set('v.showSpinner',true);
        helper.saveParts(cmp,params.isSaveNew);
    },
    save: function(component, event, helper) {
        helper.saveLineItems(component,false);
    },
    saveNew: function(component, event, helper) {
        helper.saveLineItems(component,true);
    },
    allowDrop: function(cmp, event, helper){
        event.preventDefault();
    },
    drag: function(cmp, event, helper){
        event.dataTransfer.dropEffect = "move";
        var data = {'ind':event.target.id,'direction':'u2d'};
        event.dataTransfer.setData("data", JSON.stringify(data));
    },
    drag2: function(cmp, event, helper){
        event.dataTransfer.dropEffect = "move";
        var data = {'ind':event.target.id,'direction':'d2u'};
        event.dataTransfer.setData("data", JSON.stringify(data));
    },
    drop: function(cmp, event, helper){
        event.preventDefault();
        var data = JSON.parse(event.dataTransfer.getData("data"));
        
        if(data.direction == 'u2d'){
            
            var tar = event.target;
            var tarInd = tar.getAttribute('id');
            var droppedColumns = cmp.get('v.droppedColumns');
            if( droppedColumns[tarInd] != null &&  droppedColumns[tarInd] == ''){
                var columns = cmp.get('v.columns');
                var item = columns[data.ind];
                
                droppedColumns[tarInd] = item;
                cmp.set('v.droppedColumns',droppedColumns);
                
                columns.splice(data.ind,1);
                cmp.set('v.columns',columns);
            }
        }else{
            var droppedColumns = cmp.get('v.droppedColumns');
            var columns = cmp.get('v.columns');
            var item = droppedColumns[data.ind];
            columns.push(item);
            droppedColumns[data.ind] = '';
            cmp.set('v.droppedColumns',droppedColumns);
            cmp.set('v.columns',columns);
        }
        if(cmp.get('v.columns').length == 0){
            cmp.set('v.isColumnsVisible',false);
        }else{
            cmp.set('v.isColumnsVisible',true);
        }
        
    },
    showHeader : function(cmp,event,helper){
        cmp.set('v.isFirstRowVisible',true);
    }
})