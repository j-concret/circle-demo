({
    handleSelection: function(component, event, helper) {
        component.set('v.isKeyPressed',event.shiftKey);
        console.log(component.get('v.isKeyPressed'));
    },
	invoiceChange: function(component, event, helper) {
		var selectedInvoiceRecordCount = component.get('v.selectedInvoiceRecordCount');
        var selectedInvoiceRecordCountChange = component.get('v.selectedInvoiceRecordCount');
        var isKeyPressed = component.get('v.isKeyPressed');
        var invoice = component.get('v.invoice');
        var lastSelectedIndex = component.get('v.lastSelectedIndex');
        var invoiceObjects =component.get('v.invoiceObjects');
        var rowIndex = component.get('v.rowIndex');
        if(!invoice.processStatus){
            if(isKeyPressed && invoice.Invocing){
            var start = 0;
            var end = 0;
            if(lastSelectedIndex <  (rowIndex + 1)){
                start = lastSelectedIndex;
                end = rowIndex;
            }else{
                start = rowIndex;
                end = lastSelectedIndex;
            }
                
                if(((end-start)+selectedInvoiceRecordCount) <= 100){
                selectedInvoiceRecordCount--;
                for(let row = start ; row <= end ; row++){
                    (!invoiceObjects[row].Invocing || rowIndex == row || lastSelectedIndex == row) ? selectedInvoiceRecordCount++ : '';    
                    invoiceObjects[row].Invocing = true;
                }
                component.set('v.lastSelectedIndex',rowIndex);
            }else{
                selectedInvoiceRecordCount = selectedInvoiceRecordCountChange;
                invoice.Invocing =false;
                console.log('Max limit reached');
                helper.fireToast('Info !', 'info', 'Max limit of 100 selection reached');
            }
                
            }else{
                 if(invoice.Invocing){
                    if(selectedInvoiceRecordCount < 100){
                        selectedInvoiceRecordCount++;
                        component.set('v.lastSelectedIndex',rowIndex);
                    }else{
                        invoice.Invocing = false; 
                        component.set('v.invoice',invoice);
                        helper.fireToast('Info !', 'info', 'Max limit of 100 selection reached');
                        
                    }
                }else{
                    selectedInvoiceRecordCount--;
                    component.set('v.lastSelectedIndex',undefined);
                }
                
                /*if(invoice.Invocing){
            selectedInvoiceRecordCount++;
           } else{
               selectedInvoiceRecordCount--;
           }*/
            }
           
        }
        
         component.set('v.invoice',invoice);
        component.set('v.invoiceObjects',invoiceObjects);
         component.set('v.selectedInvoiceRecordCount',selectedInvoiceRecordCount);
	}
})