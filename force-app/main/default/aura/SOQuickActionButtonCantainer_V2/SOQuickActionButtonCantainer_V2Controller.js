({  
    //onload
    
    init : function(cmp, event, helper) {
       // var soId = cmp.get("v.recordId");
       helper.fetchSOStatusHelper(cmp, event, helper);
    },
    //View Finance
    
    openViewFinanceModal: function(component, event, helper) {
      // for Display Model,set the "isOpen" attribute to "true"
      component.set("v.isOpenViewFinance", true);
   },
 
   closeOpenViewFinanceModal: function(component, event, helper) {
      // for Hide/Close Model,set the "isOpen" attribute to "Fasle"  
      component.set("v.isOpenViewFinance", false);
   },
   
  // Create CI
  openCreateCIModal: function(component, event, helper) {
      // for Display Model,set the "isOpen" attribute to "true"
      component.set("v.isOpenCreateCI", true);
   },
  
  // Create CI
  openCreateAWBModal: function(component, event, helper) {
      // for Display Model,set the "isOpen" attribute to "true"
      component.set("v.isOpenCreateAWB", true);
   },  
    
 
   closeCreateCIModal: function(component, event, helper) {
      // for Hide/Close Model,set the "isOpen" attribute to "Fasle"  
      component.set("v.isOpenCreateCI", false);
   },
   
    closeCreateAWBModal: function(component, event, helper) {
      // for Hide/Close Model,set the "isOpen" attribute to "Fasle"  
      component.set("v.isOpenCreateAWB", false);
   },
    

   // Add Line items CI
   openAddLineItemsModal: function(component, event, helper) {
    // for Display Model,set the "isOpen" attribute to "true"
    var soId = component.get("v.recordId");
        var navService = component.find("navService");
        var pageReference = {
            
            "type": "standard__component",
            "attributes": {
                "componentName": "c__AddLineItemOnNewSO"    
            },    
            "state": {
                "c__recordId": soId ,
                "c__newSOPage":true
            }
        };
        component.set("v.pageReference", pageReference);
        var defaultUrl = "#";
        navService.generateUrl(pageReference)
        .then($A.getCallback(function(url) {
            component.set("v.url", url ? url : defaultUrl);
        }), $A.getCallback(function(error) {
            component.set("v.url", defaultUrl);
        }));
       var navService = component.find("navService");
        // Uses the pageReference definition in the init handler
        var pageReference = component.get("v.pageReference");
        event.preventDefault();
        navService.navigate(pageReference);
    //component.set("v.isOpenAddLineItems", true);
    //component.set("v.iframeUrl","https://tecex.lightning.force.com/apex/PartCopyAndPaste?sobjectName=Part__c&id="+ soId);  
       
 },

 closeAddLineItemsModal: function(component, event, helper) {
    // for Hide/Close Model,set the "isOpen" attribute to "Fasle"  
    component.set("v.isOpenAddLineItems", false);
 },
    

handleCloseModal: function(component, event, helper) {

     var closePopup = event.getParam("isOpenCreateCI");
     component.set("v.isOpenCreateCI", closePopup);
    component.set("v.isOpenCreateAWB", closePopup);
  }
    
})