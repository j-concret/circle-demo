({
    fetchSOStatusHelper : function(component, event, helper) {
        component.set('v.showSpinner',true);
        
        var action = component.get("c.getShipmentStatus");
        action.setParams({
            recId : component.get('v.recordId'),
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            
            if(state == 'SUCCESS') {
                var result = response.getReturnValue();
                if(result.status == 'OK'){
                    
                    var allowedSOStatusArray = ['Shipment Pending', 'CI, PL and DS Received', 'AM Approved CI, PL and DS\'s',
                                               'Complete Shipment Pack Submitted for Approval', 'License Pending', 'Awaiting AWB',
                                               'Customs Approved (Pending Payment)', 'Client Approved to Ship', 'Cost Estimate'];
                    
                    component.set('v.shipmentStatus', result.shipmentStatus);
                    component.set('v.enableFreightWizard', allowedSOStatusArray.includes(result.shipmentStatus));
                }else if(state === "ERROR") {
                    var errors = response.getError();
                    if(errors) {
                        if(errors[0] && errors[0].message) {
                            this.showToast('Error','error',errors[0].message);
                        }
                    } else {
                        this.showToast('Error','error','Unknown Error');
                    }
                    
                }
            }
            component.set('v.showSpinner',false);
        });
        $A.enqueueAction(action);
    }
})