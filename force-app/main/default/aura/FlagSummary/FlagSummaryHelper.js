({
    getFlagCount : function(component) {
        this.serverCall(component,'getFlagCounts',{'recordId':component.get('v.recordId')},function(response){
            component.set('v.showSpinner',false);
            if(response.status == 'OK'){
                component.set('v.flags',response.data);
                component.set('v.record',response.shipmentOrder);
            }
            else{
                component.set('v.msg',response.msg);
                component.set('v.type','error')
            }
        });
    },
    refreshAllFlags : function(component) {
        var self = this;
        this.serverCall(component,'refreshFlags',{'recordId':component.get('v.recordId')},function(response){
            component.set('v.showSpinner',false);
            if(response.status == 'OK'){
                self.showToast('success','Success','Refreshed Successfully!');
                $A.get('e.force:refreshView').fire();
            }
            else{
                component.set('v.msg',response.msg);
                component.set('v.type','error')
            }
        });
    },
    showToast : function(type, title , msg) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            'type':type,
            'title': title ,
            'message': msg
        });
        toastEvent.fire();
    },
    navigateToCmponent: function(component) {
        var navService = component.find("navService");
        // Uses the pageReference definition in the init handler
        var pageReference = component.get("v.pageReference");
        navService.navigate(pageReference);
    },
})