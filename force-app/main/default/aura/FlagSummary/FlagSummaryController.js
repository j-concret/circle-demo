({
    doInit: function(component, event, helper) {
        var navService = component.find("navService");
        var pageReference = {
            "type": "standard__component",
            "attributes": {
                "componentName": "c__ShipmentOrderKanbanview"    
            },    
            "state": {
                "c__recId": component.get("v.recordId")
            }
        };
        component.set("v.pageReference",pageReference);
        component.set('v.showSpinner',true);
        helper.getFlagCount(component);
    },
    refreshHandler: function(component, event, helper) {
        component.set('v.showSpinner',true);
        helper.refreshAllFlags(component);
    },
    navigateToSobject: function(component, event, helper) {
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            "recordId": component.get('v.recordId'),
            "slideDevName": "detail"
        });
        navEvt.fire();
    },
    openTab : function(component, event, helper) {
        
        
        var workspaceAPI = component.find("workspace");
        workspaceAPI.isConsoleNavigation().then(function(result){
            if(result){
                workspaceAPI.getFocusedTabInfo().then(function(response){
                    workspaceAPI.openSubtab({
                        parentTabId :response.tabId,
                        pageReference : component.get("v.pageReference"),
                        focus: true
                    }).then(function(response) {
                        console.log("The recordId for this tab is: " + tabInfo.recordId);
                    }).catch(function(error) {
                        console.log(error);
                    });
                });
                
            }else{
                helper.navigateToCmponent(component);
            }
        }).catch(function(error) {
            console.log('err',JSON.stringify(error));
        });
        
    }
})