({
  applyFilter: function(component, event, helper) {
    component.getEvent("applyFilter").fire();
  },
  handleChange: function(component, event) {
    var selectedOptionValue = event.getParam("value");
    console.log(selectedOptionValue);
    component.set('v.selectedStatuses',selectedOptionValue);
  },
  closeFilter:function(component,event,helper){
    component.set('v.showFilter',!component.get('v.showFilter'));
  }
})