({
  init: function (cmp, event, helper) {

    var soId = cmp.get('v.pageReference').state.c__soId;
    var tskId = cmp.get('v.pageReference').state.c__tskId;
    cmp.set('v.soId',soId);
    cmp.set('v.tskId',tskId);
    var workspaceAPI = cmp.find('workspace');

    workspaceAPI.getAllTabInfo().then(function(response) {
      var focusedTabId;
      for(var i=0;i<response.length;i++){
          if(response[i].isSubtab && response[i].title == 'Loading...' && response[i].pageReference.attributes && response[i].pageReference.attributes.componentName && response[i].pageReference.attributes.componentName == 'c__PartSubCategoryTable'){
            focusedTabId = response[i].tabId;
            break;
          }else if(response[i].subtabs){
            var subtabs = response[i].subtabs;
            for(var j=0;j<subtabs.length;j++){
              if(subtabs[j].title === 'Loading...' && subtabs[j].pageReference.attributes && subtabs[j].pageReference.attributes.componentName && subtabs[j].pageReference.attributes.componentName == 'c__PartSubCategoryTable'){
                focusedTabId = subtabs[j].tabId;
                break;
              }
            }
          }
      }

      if(focusedTabId){
        cmp.set('v.tabId',focusedTabId);
        workspaceAPI.setTabLabel({
          tabId: focusedTabId,
          label: 'PartSubcategory'
        });
        workspaceAPI.setTabIcon({
          tabId: focusedTabId,
          icon: 'standard:document',
          iconAlt: 'PartSubcategory'
        });
      }
    }).catch(function(error) {
      console.log(error);
    });

    helper.getPartRecords(cmp);

  },
  updateHandler: function(cmp,event,helper){
    helper.updateParts(cmp);
  }
});