({
  getPartRecords : function(cmp) {
    cmp.set('v.showSpinner',true);
    var action = cmp.get('c.getParts');
    action.setParams({
      soId : cmp.get('v.soId'),
      tskId : cmp.get('v.tskId')
    });
    action.setCallback(this, function(response) {
      cmp.set('v.showSpinner',false);
      var state = response.getState();
      if (state === 'SUCCESS') {
        var res = response.getReturnValue();
        cmp.set('v.parts',res.parts);
        cmp.set('v.options',res.options);

      }
      else if (state === 'INCOMPLETE') {
        this.showToast('Error','error','Incomplete request.');
      }
      else if (state === 'ERROR') {
        var errors = response.getError();
        if (errors) {
          if (errors[0] && errors[0].message) {
            this.showToast('Error','error','Error message: ' + errors[0].message);
          }
        } else {
          this.showToast('Error','error','Unknown error');
        }
      }
      cmp.set('v.showSpinner',false);
    });
    $A.enqueueAction(action);
  },
  updateParts:function(cmp){
    cmp.set('v.showSpinner',true);
    var action = cmp.get('c.updatePartCategory');
    action.setParams({
      parts : cmp.get('v.parts')
    });
    action.setCallback(this, function(response) {
      cmp.set('v.showSpinner',false);
      var state = response.getState();
      if (state === 'SUCCESS') {
        this.showToast('Sucess','success','Part sub-category updated successfully!');
        this.closeTab(cmp);
      }
      else if (state === 'INCOMPLETE') {
        this.showToast('Error','error','Incomplete request.');
      }
      else if (state === 'ERROR') {
        var errors = response.getError();
        if (errors) {
          if (errors[0] && errors[0].message) {
            this.showToast('Error','error','Error message: ' + errors[0].message);
          }
        } else {
          this.showToast('Error','error','Unknown error');
        }
      }
      cmp.set('v.showSpinner',false);
    });
    $A.enqueueAction(action);
  },
  showToast : function(title,type,msg) {
    var toastEvent = $A.get('e.force:showToast');
    toastEvent.setParams({
      'title': title,
      'type' :type,
      'message': msg
    });
    toastEvent.fire();
  },
  closeTab : function(cmp){
    var workspaceAPI = cmp.find('workspace');
    var focusedTabId = cmp.get('v.tabId');

    if(focusedTabId){
      workspaceAPI.closeTab({tabId: focusedTabId});
    }
  }
});