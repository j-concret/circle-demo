({
	toogleFilter: function(component, event, helper) {
        //var showFilter = component.get('v.showFilter');
        component.set('v.showFilter', false);
    },
    getShipmentObjects: function(component, event, helper) {
       var action = component.get("c.getTopUpShipmentOrders");
        action.setParams({
            'account': component.get('v.clientAccount').Id,
            'invoiceTiming': component.get('v.selectedInvoiceTiming'),
            'EnterpriseId': component.get('v.enterpriseObject').Id,
            'statuses':component.get('v.selectedStatuses')
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS") {
                var result = response.getReturnValue(); 
                if(result.status === "OK") {
                    component.set('v.shipmentObjects', result.shipmentOrders );
                     component.set('v.filteredShipmentObjects', result.shipmentOrders );
                    component.set('v.isShipmentApplied',true);
                    component.set('v.isShipmentLoading',false); 
                    console.log(result);
                } else {
                    console.log(result.msg);
                    component.set('v.isShipmentLoading',false);
                }
            }
            else if(state === "ERROR") {
                var errors = response.getError();
                if(errors) {
                    if(errors[0] && errors[0].message) {
                        console.log(errors[0].message);
                        component.set('v.isShipmentLoading',false);
                        //that.fireToast('ERROR', 'error', errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                    component.set('v.isShipmentLoading',false);
                    //that.fireToast('ERROR', 'error', "Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    generateInvoicesPDFObjects:function(component,successInvoices){
        component.set('v.isInvoicePDFLoading',true);
        var that = this;
        var successInvoicesChunks = that.chunk(successInvoices,10);
        var chunkIndex = 0;
        if(successInvoicesChunks.length > 0){
            for(let successInvoice of successInvoicesChunks){   
        var action = component.get("c.generateInvoicesPDFForTopUps");
        action.setParams({
            'invoiceList':successInvoice
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS") {
                chunkIndex++;
                var result = response.getReturnValue(); 
                if(result.status === "OK") {
                    if(successInvoicesChunks.length == chunkIndex){
                    component.set('v.isInvoicePDFLoading',false);
                    component.set('v.isShipmentButtonEnabled',false); 
                    that.fireToast('Success !', 'success', 'Invoices created successfully!');
                    console.log(result);
                    }
                } else {
                    //console.log();
                    that.fireToast('Error !', 'error', result.msg);
                    component.set('v.isInvoicePDFLoading',false);
                }
            }
            else if(state === "ERROR") {
                var errors = response.getError();
                if(errors) {
                    if(errors[0] && errors[0].message) {
                        //console.log();
                        that.fireToast('Error !', 'error', errors[0].message);
                        component.set('v.isInvoicePDFLoading',false);
                        //that.fireToast('ERROR', 'error', errors[0].message);
                    }
                } else {
                    //console.log();
                    that.fireToast('Error !', 'error', "Unknown error");
                    component.set('v.isInvoicePDFLoading',false);
                    //that.fireToast('ERROR', 'error', "Unknown error");
                }
            }
        });
            $A.enqueueAction(action);
                }
        }else{
           // console.log();
            that.fireToast('Error !', 'error', "ERROR IN CHUNKS IN INVOICES");
            component.set('v.isShipmentButtonEnabled',false);
        }
    },
    processInvoiceObjects: function(component, event, helper) {
        var that=this;
     var shipOrders = component.get('v.shipmentObjects');
        var invoiceList = [];
        for(let shipOrder of shipOrders){
            let shipJson = {};
            if(shipOrder.Invocing && !shipOrder.processStatus){
                shipJson.IOR_Fees__c = shipOrder.IOR_Fees__c;
                shipJson.EOR_Fees__c = shipOrder.EOR_Fees__c;
                shipJson.Bank_Fee__c = shipOrder.Bank_Fee__c;
                shipJson.Admin_Fees__c = shipOrder.Admin_Fees__c;
                shipJson.Customs_Handling_Fees__c = shipOrder.Customs_Handling_Fees__c;
                shipJson.Customs_Brokerage_Fees__c = shipOrder.Customs_Brokerage_Fees__c;
                shipJson.Customs_Clearance_Fees__c = shipOrder.Customs_Clearance_Fees__c;
                shipJson.Customs_License_In__c = shipOrder.Customs_License_In__c;
                shipJson.International_Freight_Fee__c = shipOrder.International_Freight_Fee__c;
                shipJson.Cash_Outlay_Fee__c = shipOrder.Cash_Outlay_Fee__c;
                shipJson.Liability_Cover_Fee__c = shipOrder.Liability_Cover_Fee__c;
                shipJson.Taxes_and_Duties__c = shipOrder.Taxes_and_Duties__c;
                shipJson.Recharge_Tax_and_Duty_Other__c = shipOrder.Recharge_Tax_and_Duty_Other__c;
                shipJson.Miscellaneous_Fee__c = shipOrder.Miscellaneous_Fee__c;
                shipJson.Miscellaneous_Fee_Name__c = shipOrder.Miscellaneous_Fee_Name__c;
                shipJson.Invoice_amount_USD__c = shipOrder.Invoice_amount_USD__c;
                shipJson.Account__c = component.get('v.clientAccount').Id;
                shipJson.Conversion_Rate__c = 1;
                shipJson.Invoice_Amount_Local_Currency__c = shipOrder.Invoice_amount_USD__c;
                shipJson.Invoice_Currency__c = 'US Dollar (USD)';
                shipJson.Shipment_Order__c = shipOrder.Id;
                shipJson.Invoice_Type__c = 'Top Up Invoice';
                shipJson.RecordTypeId  = component.get('v.recordTypeInvoice');
                shipJson.Invoice_Status__c = 'Invoice Sent'
                shipJson.Enterprise_Billing__c = shipOrder.Enterprise_Billing_Id__c;
                invoiceList.push(shipJson);
            }
            
        }
        var invoiceListChunks = that.chunk(invoiceList,10);
        console.log('invoiceListChunks');
        console.log(invoiceListChunks);
        var chunkIndex = 0;
        var successInvoices = [];
        var successShipments = [];
         if(invoiceListChunks.length > 0){
            for(let invoices of invoiceListChunks){
          var action = component.get("c.processInvoices");
        action.setParams({
            'invoiceList': invoices
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS") {
                var result = response.getReturnValue(); 
                if(result.status === "OK") {
                    chunkIndex++;
                    var invoiceIds = Object.keys(result.successList);
                    var invoiceList = result.invoiceList;
                    for(let invoice of invoiceList){
                        if(invoice.Id){
                        successInvoices.push(invoice);
                        successShipments.push(invoice.Shipment_Order__c);
                        }
                    }
                    console.log('invoiceIds '+invoiceIds);
                    if(invoiceListChunks.length == chunkIndex){
                    component.set('v.isShipmentUpdateLoading',false);
                    component.set('v.isInvoicePDFLoading',true);
                    for(let shipOrder of shipOrders){
                        if(shipOrder.Invocing && !shipOrder.processStatus){
                            shipOrder.Status = successShipments.includes(shipOrder.Id);
                            shipOrder.processStatus = true;
                        }
                    }
                        if(successInvoices.length >0){
                    component.set('v.isProcessStatus',true);    
                    component.set('v.selectedShipmentRecordCount',0);
                    component.set('v.shipmentObjects',shipOrders);
                    component.set('v.filteredShipmentObjects',shipOrders);
                    that.generateInvoicesPDFObjects(component,successInvoices);
                        }else{
                            component.set('v.isInvoicePDFLoading',false);
                            that.fireToast('Error !', 'error', result.invoiceError);
                        }
                    }
                    //if()
              /*       var Statuses = result.successList;
                     var ShipmentListId = [];
                    for(let invoice of result.invoiceList){
                        if(invoice.Id != 'undefined'){
                        ShipmentListId.push(invoice.Shipment_Order__c);
                        }
                    }
                    component.set('v.selectedShipmentRecordCount',0);
                    that.generateInvoicesPDFObjects(component,component.get('v.enterpriseObject').Id,ShipmentListId);
                    */
                    //component.set('v.shipmentObjects', result.shipmentOrders );
                    //
                    //component.set('v.isShipmentApplied',true);
                    //component.set('v.isShipmentLoading',false); 
                    console.log(result);
                } else {
                   // console.log(result.msg);
                     that.fireToast('Error !', 'error', result.msg);
                     component.set('v.isShipmentUpdateLoading',false);
                    //component.set('v.isShipmentLoading',false);
                }
            }
            else if(state === "ERROR") {
                var errors = response.getError();
                if(errors) {
                    if(errors[0] && errors[0].message) {
                        //console.log();
                        that.fireToast('Error !', 'error', errors[0].message);
                         component.set('v.isShipmentUpdateLoading',false);
                      //  component.set('v.isShipmentLoading',false);
                        //that.fireToast('ERROR', 'error', errors[0].message);
                    }
                } else {
                    //console.log();
                    that.fireToast('Error !', 'error', "Unknown error");
                     component.set('v.isShipmentUpdateLoading',false);
                   // component.set('v.isShipmentLoading',false);
                    //that.fireToast('ERROR', 'error', "Unknown error");
                }
            }
        });
            $A.enqueueAction(action);
            }
        }else{
            console.log('ERROR IN CHUNK');
             component.set('v.isShipmentUpdateLoading',false);
        }
	},
    getRecordTypesObjects: function(component){
        var action = component.get("c.getRecordTypes");
        action.setParams({
            'objectName': 'Invoice_New__c'
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS") {
                var result = response.getReturnValue(); 
                if(result.status === "OK") {
                    //component.set('v.shipmentObjects', result.shipmentOrders );
                    var RecordTypeList = result.RecordTypeList;
                    for(let record of RecordTypeList){
                        if(record.Name == 'Client'){
                        component.set('v.recordTypeInvoice',record.Id);
                    }
                    }
                    //component.set('v.isShipmentApplied',true);
                    //component.set('v.isShipmentLoading',false); 
                    console.log(result);
                } else {
                    console.log(result.msg);
                    component.set('v.isShipmentLoading',false);
                }
            }
            else if(state === "ERROR") {
                var errors = response.getError();
                if(errors) {
                    if(errors[0] && errors[0].message) {
                        console.log(errors[0].message);
                        component.set('v.isShipmentLoading',false);
                        //that.fireToast('ERROR', 'error', errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                    component.set('v.isShipmentLoading',false);
                    //that.fireToast('ERROR', 'error', "Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    chunk: function(array, count) {
        if (count == null || count < 1) return [];
        var result = [];
        var i = 0, length = array.length;
        while (i < length) {
            result.push(Array.prototype.slice.call(array, i, i += count));
        }
        return result;
    },
    fireToast: function(title, type, message) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": title,
            "type": type,
            "message": message
        });
        toastEvent.fire();
    }
})