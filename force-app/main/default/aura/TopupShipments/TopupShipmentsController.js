({
    doInit: function(component, event, helper) {
       helper.getRecordTypesObjects(component);
    },
    onFilterSO: function(component, event, helper) {
        var shipmentObjects = component.get('v.shipmentObjects');
        var filter = component.get('v.filter');
        var filteredData = [];
        if(filter){
           
                filteredData = shipmentObjects.filter(data => {
                  return data.Name ? data.Name.toLowerCase().includes(filter.toLowerCase()) : false;
              });
                component.set('v.filteredShipmentObjects',filteredData);
          
        }else{
            component.set('v.filteredShipmentObjects',shipmentObjects);
        }
    },
    accountChanged: function(component, event, helper) {
        var clientAccount = component.get('v.clientAccount');
        if(typeof clientAccount.Id != 'undefined'){
            component.set('v.selectedInvoiceTiming',clientAccount.Invoice_Timing__c);
            console.log('clientAccount');
            console.log(clientAccount.Name);
            console.log(clientAccount.Invoice_Timing__c);
            console.log(component.get('v.selectedInvoiceTiming'));
        }
    },
	onInvoiceTimingSelect: function(component, event, helper) {
        console.log(component.get('v.selectedInvoiceTiming'));
        if(component.get('v.selectedInvoiceTiming') == 'Upfront invoicing'){
            component.set('v.isUpfrontInvoicing',true);
        }else{
            component.set('v.isUpfrontInvoicing',false);
        }
         console.log(component.get('v.isUpfrontInvoicing'));
    },
    showEmailComponent: function(component, event, helper) {
       component.set('v.isShipmentComponent',false);
    },
    getShipments: function(component,event,helper){
        //var selectedStatuses = component.get('v.selectedStatuses');
        component.set('v.isShipmentLoading',true);
        helper.getShipmentObjects(component, event, helper);
    },
    processInvoiceObj: function(component,event,helper){
        //var selectedStatuses = component.get('v.selectedStatuses');
        component.set('v.isShipmentUpdateLoading',true);
        if(component.get('v.selectedShipmentRecordCount') > 0){
        helper.processInvoiceObjects(component, event, helper);
        }else{
            helper.fireToast('Info !', 'info', 'select Shipment Order to process');
            component.set('v.isShipmentUpdateLoading',false);
        }
    },
    applyFilters: function(component,event,helper){
        var selectedStatuses = component.get('v.selectedStatuses');
        helper.toogleFilter(component);
    },
    showFilter: function(component, event, helper) {
        var showFilter = component.get('v.showFilter');
        component.set('v.showFilter', !showFilter);
    },
    toggleSOFields: function(component,event,helper){
        component.set('v.isSOVisible',!component.get('v.isSOVisible'));
    },
    toggleTecExCharges: function(component,event,helper){
        component.set('v.isTecExChargesVisible',!component.get('v.isTecExChargesVisible'));
    },
    toggleOnCharges: function(component,event,helper){
        component.set('v.isOnChargesVisible',!component.get('v.isOnChargesVisible'));
    },
    toggleVAService: function(component,event,helper){
        component.set('v.isVAServicesVisible',!component.get('v.isVAServicesVisible'));
    },
    toggleTax: function(component,event,helper){
        component.set('v.isTaxVisible',!component.get('v.isTaxVisible'));
    },
    toggleOther: function(component,event,helper){
        component.set('v.isOtherVisible',!component.get('v.isOtherVisible'));
    },
    refreshCmp: function(component, event, helper) {
       component.set('v.isLoading',true);
        component.set('v.shipmentObjects',[]);
        component.set('v.filteredShipmentObjects',[]);
        component.set('v.selectedShipmentRecordCount',0);
        component.set('v.clientAccount',{});
        component.set('v.enterpriseObject',{});
        component.set('v.isShipmentApplied',false);
        component.set('v.isShipmentButtonEnabled',true)
        component.set('v.filter','');
        component.set('v.lastSelectedIndex',undefined);
        component.set('v.selectedInvoiceTiming','Upfront Invoicing');
        component.set('v.isUpfrontInvoicing',true);
        component.set('v.reviewMember',false);
        component.set('v.selectedStatuses',['Customs Clearance Docs Received','Cancelled - With fees/costs']);
        component.set('v.isLoading',false);
      
    }
})