({
     
    doInit : function(component, event, helper) {
        
       var SupplierInvoiceID = component.get("v.soidFromFlow"); // supplierINvoice ID for record attach
       var MatchedExchangeRate = component.get("v.Exc1");
       var SelectedCurrency = component.get("v.Cur1"); 
       
       console.log("SupplierInvoiceID",SupplierInvoiceID);
        
       component.set("v.iframeUrl2","https://tecex.lightning.force.com/flow/Supplier_Invoice_Costings?soidFromAnotherFlow="+ SupplierInvoiceID+"&MatchedExchangeRateFromAnotherFlow="+MatchedExchangeRate+"&SelectedCurrencyFromAnotherFlow="+SelectedCurrency);
        
    },
     handleUploadFinished: function (cmp, event) {
        // This will contain the List of File uploaded data and status
        var uploadedFiles = event.getParam("files");
        alert("Files uploaded : " + uploadedFiles.length);
    }
      
        
})