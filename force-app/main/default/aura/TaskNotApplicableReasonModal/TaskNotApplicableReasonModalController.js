({
  onCancel: function(component, event, helper) {
    component.set('v.task.reasonForNotApplicable','');
    component.set('v.showModal',false);
  }
})