({
  init: function (cmp, event, helper) {

    var soId = cmp.get('v.pageReference').state.c__soId;
    cmp.set('v.soId',soId);
    var workspaceAPI = cmp.find('workspace');

    workspaceAPI.getAllTabInfo().then(function(response) {
      var focusedTabId;
      for(var i=0;i<response.length;i++){
          if(response[i].isSubtab && response[i].title == 'Loading...' && response[i].pageReference.attributes && response[i].pageReference.attributes.componentName && response[i].pageReference.attributes.componentName == 'c__BatteryPINumber'){
            focusedTabId = response[i].tabId;
            break;
          }else if(response[i].subtabs){
            var subtabs = response[i].subtabs;
            for(var j=0;j<subtabs.length;j++){
              if(subtabs[j].title === 'Loading...' && subtabs[j].pageReference.attributes && subtabs[j].pageReference.attributes.componentName && subtabs[j].pageReference.attributes.componentName == 'c__BatteryPINumber'){
                focusedTabId = subtabs[j].tabId;
                break;
              }
            }
          }
      }

      if(focusedTabId){
        cmp.set('v.tabId',focusedTabId);
        workspaceAPI.setTabLabel({
          tabId: focusedTabId,
          label: 'BatteryPI'
        });
        workspaceAPI.setTabIcon({
          tabId: focusedTabId,
          icon: 'standard:document',
          iconAlt: 'BatteryPI'
        });
      }
    }).catch(function(error) {
      console.log(error);
    });

    helper.getPackageRecords(cmp);

  },
  updateHandler: function(cmp,event,helper){
    helper.updatePackages(cmp);
  },
  handleCheckChange:function(cmp,event,helper){
    var packages = cmp.get("v.packages");
    var checked = event.getSource().get("v.checked");
    var index = event.getSource().get("v.label");
    if(!checked){
      packages[index].ION_PI966__c = false;
      packages[index].ION_PI967__c = false;
      packages[index].Metal_PI969__c = false;
      packages[index].Metal_PI970__c = false;
      cmp.set('v.packages',packages);
    }
  }
});