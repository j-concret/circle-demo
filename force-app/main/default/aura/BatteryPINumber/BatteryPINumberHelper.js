({
  getPackageRecords : function(cmp) {
    var self = this;
    cmp.set('v.showSpinner',true);
    self.serverCall(cmp,'getPackages',{
      soId : cmp.get('v.soId')
    },function(response){
      cmp.set('v.showSpinner',false);
      cmp.set('v.packages',response);
    },function(err){
      cmp.set('v.showSpinner',false);
      self.showToast('error','ERROR',err);
    });

  },
  updatePackages:function(cmp){
    var self = this;
    cmp.set('v.showSpinner',true);
    self.serverCall(cmp,'updateBatteryPI',{
      packages : cmp.get('v.packages')
    },function(response){
      cmp.set('v.showSpinner',false);
      self.showToast('Sucess','success','Battery PI Number updated successfully!');
      self.closeTab(cmp);

    },function(err){
      cmp.set('v.showSpinner',false);
      self.showToast('error','ERROR',err);
    });

  },
  showToast : function(title,type,msg) {
    var toastEvent = $A.get('e.force:showToast');
    toastEvent.setParams({
      'title': title,
      'type' :type,
      'message': msg
    });
    toastEvent.fire();
  },
  closeTab : function(cmp){
    var workspaceAPI = cmp.find('workspace');
    var focusedTabId = cmp.get('v.tabId');

    if(focusedTabId){
      workspaceAPI.closeTab({tabId: focusedTabId});
    }
  }
});