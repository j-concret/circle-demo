({
    doinitCtrl : function(component, event, helper) {
        component.set("v.loading", true);
        var params = {
            shipmentId: component.get('v.recordId')
        };
        this.makeServerCall(component, 'getFinalDeliveryData', params, function(result) {
            if(result.status === 'OK') {
                component.set("v.finalDeliveryData", result.data);
                for (let index = 0; index < result.data.length; index++) {
                    if(result.data[index].Domestic_Courier_Tracking_Link__c ){
//                        
                        let track = result.data[index].Domestic_Courier_Tracking_Link__c.split('"')[1];
                        if(track.includes('&amp;')){
                            track = track.replaceAll('&amp;','&');
                        }
                        result.data[index].Domestic_Courier_Tracking_Link__c = track;
                    }
                }
                component.set("v.finalDeliveryData", result.data);
            }
            else {
                helper.showToast('error', 'Error Occured', result.msg);
            }
            component.set("v.loading", false);
        });
        
    },
    getPicklistValues : function(component, event, helper) {
        var par = {object_name : 'Final_Delivery__c', 
                   field_names : ['Delivery_Status__c']};
        this.makeServerCall(component, 'getPicklistsValues', par , function(response) {
            
            component.set("v.statusPicklist", response.Delivery_Status__c);
            
                var optionsArray = [];
                for (let index = 0; index < response.Delivery_Status__c.length; index++) {
                    var element = {"label" : response.Delivery_Status__c[index],"value" : response.Delivery_Status__c[index]};
                    optionsArray.push(element);
                }
            component.set("v.statusPicklist", optionsArray);
        });
    },
    saveFD : function(component, event, helper) {
        component.set("v.loading", true);
        var params = {
            finalDelObj : component.get("v.seletedFD") 
        };
        this.makeServerCall(component, 'updateFinalDeliveryData', params, function(result) {
            if(result.status === 'OK') {
                
                helper.showToast('Success', 'Delivery Record Updated', 'Success');
                component.set("v.showUploadDocument", false);
                location.reload();

               // helper.doinitCtrl(component, event, helper);
            }
            else {
                helper.showToast('error', 'Error Occured', result.msg);
            }
            component.set("v.loading", false);
        });
    },
})