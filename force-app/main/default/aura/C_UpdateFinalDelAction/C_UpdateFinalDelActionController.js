({
    doinit : function(component, event, helper) {
       // helper.getPicklistValues(component, event, helper);
        helper.doinitCtrl(component, event, helper);

    },

    closeUploadEOD :function(component, event, helper) {

        component.set("v.closeModal", false);
        component.set("v.showUploadDocument", false);
        if(!component.get("v.newSOPage")){
        	$A.get("e.force:closeQuickAction").fire();
        }

    },
    closeUploadDocument :function(component, event, helper) {

        component.set("v.showUploadDocument", false);
        component.set("v.showUploadPOD", true);
    },

    uploadPOD :function(component, event, helper) {
        component.set("v.showUploadDocument", true);
        component.set("v.showUploadPOD", false);
        var target = component.get("v.finalDeliveryData")[event.getSource().get("v.value")];
        component.set("v.seletedFD", target);

    },
    showRequiredFields: function(component, event, helper){

    },
    onRecordSubmit: function (component, event, helper) {
       component.set("v.loading", true);
    },
     handleSuccess : function(component, event, helper) {
        var ToastEvent = $A.get("e.force:showToast");
        ToastEvent.setParams({
            "title": "Success!",
            "type": "success",
            "message": "Record save successful."});
        ToastEvent.fire();
        component.set("v.loading", false);
         component.set("v.showUploadDocument", false);
         component.set("v.showUploadPOD", true);
    },

    onError : function(component, event, helper) {
      component.set("v.loading", false);
      var error = event.getParams();
      var errorMessage = error.detail;
      if(errorMessage.includes('pending tasks')) errorMessage = 'Please complete all pending tasks first.';

        var toastEvent2 = $A.get("e.force:showToast");
        toastEvent2.setParams({
            "title": "Error!",
            "type": "error",
            "message": errorMessage
        });
        toastEvent2.fire();
    },
    handleTypeChange : function(component, event, helper) {
        var clientAddressId = component.find("Ship_to_address__c").get("v.value");
        component.set("v.clientAddressId", clientAddressId);
    },
    handleClickNavigate: function(component, event, helper) {
        if(component.find("Ship_to_address__c").get("v.value")){
            var navService = component.find("navService");
           var pageReference = {
            "type": "standard__recordPage", //example for opening a record page, see bottom for other supported types
            "attributes": {
                "recordId": component.find("Ship_to_address__c").get("v.value"), //place your record id here that you wish to open
                "actionName": "view"
            }
        }

        navService.generateUrl(pageReference)
        .then($A.getCallback(function(url) {
            console.log('success: ' + url);
            window.open(url,'_blank');
        }),
              $A.getCallback(function(error) {
                  console.log('error: ' + error);
              }));
        }else{
        var toastEvent2 = $A.get("e.force:showToast");
        toastEvent2.setParams({
            "title": "info!",
            "message": "Select Client Address"
        });
        toastEvent2.fire();
        }
    }
})