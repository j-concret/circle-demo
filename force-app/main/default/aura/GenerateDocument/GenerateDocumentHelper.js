({
    generateDoc : function(cmp){
        var methodName = '';
        var msg = '';
      var pageRef = cmp.get("v.pageReference");
        if(pageRef && pageRef.state.c__soId){
          cmp.set("v.recordId",pageRef.state.c__soId);
          cmp.set("v.type", pageRef.state.c__type);  
        }  
        if(cmp.get("v.type") === 'Clearance Letter'){
            methodName = 'generateClearanceLetter';
            msg = 'clearance letter';
        }
        else if(cmp.get("v.type") === 'Proof Of Valuation'){
            methodName = 'generateProofOfValuation';
            msg = 'proof of valuation';
        }
        
      var self = this;
      self.serverCall(cmp,methodName,{
        soId : cmp.get('v.recordId')
      },function(response){
        if(response){
          cmp.set('v.showSpinner',false);
          cmp.set('v.msg','Your ' + msg +  ' document generate successfully , you will find under notes and attachement related list.');
            setTimeout(function(){
                var workspaceAPI = cmp.find("workspace");
                workspaceAPI.getFocusedTabInfo().then(function(response) {
                    workspaceAPI.isSubtab({
                        tabId: response.tabId
                    }).then(function(response) {
                        workspaceAPI.closeTab({tabId: response.tabId});
                    });
                })
                .catch(function(error) {
                    console.log(error);
                });
            },800);
          
        }
      },function(err){
        cmp.set('v.showSpinner',false);
        cmp.set('v.error',true);
        cmp.set('v.msg',err);
      });
    }
  })