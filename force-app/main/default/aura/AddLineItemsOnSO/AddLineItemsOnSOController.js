({
  doInit:function(component,event,helper){
      var pageRef = component.get("v.pageReference");
      if(pageRef && pageRef.state.c__recordId){
        component.set("v.recordId",pageRef.state.c__recordId);
      }
    helper.sethelper(component);
    window.addEventListener("message", helper.receiveMessage.bind(helper), false);
    component.set("v.cval", "<style>.slds-modal__container{width: 75%;max-width: 70rem;}</style>");
  },
  afterScriptsLoaded : function(component, event, helper) {
    helper.loadScript();
  },
  createPartLineItems: function(component, event, helper) {
    helper.createLI(component);
  },
  handleBack: function(component, event, helper) {
    component.set('v.fromProCatalogue',null);
  }
})