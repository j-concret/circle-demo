({
  component :null,
  sethelper : function(component){
    this.component = component;
  },
  loadScript : function() {
      $('#partTableData').on('paste', 'input', function(e){
        var $this = $(this);
        var len = $this.closest('tr')[0].cells.length || 0;
        var extraRow = "<tr><td><input type='text'/></td><td><input type='text'/></td><td><input type='text'/></td>";
        if(len>3)
          extraRow += "<td><input type='text'/></td>";
        extraRow+="</tr>";

        $.each(e.originalEvent.clipboardData.items, function(i, v){
            if (v.type === 'text/plain'){
                v.getAsString(function(text){
                    var x = $this.closest('td').index(),
                        y = $this.closest('tr').index()+1,
                        obj = {};
                    text = text.trim('\n');
                    $.each(text.split('\n'), function(i2, v2){
                      if($this.closest('table').find('tr:eq('+(y+i2)+')').length <= 0 )
                        $this.closest('table').find('tbody').append(extraRow);
                      $.each(v2.split('\t'), function(i3, v3){
                          var row = y+i2, col = x+i3;
                          obj['cell-'+row+'-'+col] = v3;

                          $this.closest('table').find('tr:eq('+row+') td:eq('+col+') input').val(v3);
                      });
                    });

                });
            }
        });
        return false;

    });
    },
    createLI:function(component) {
      component.set('v.showSpinner',true);
      var lineItems = [];
      var fromProCatalogue = component.get('v.fromProCatalogue') == 'Yes';
      var headers;
      var ind = 3;//unit Price index for trimming currency symbol and separators
      if(fromProCatalogue){
        headers = ['internal_reference','quantity','unit_price'];
        ind = 2;
      }
      else{
        headers = ['Name','Description_and_Functionality__c','Quantity__c','Commercial_Value__c'];
        //headers.splice(0,0,'name','description');
      }

      var msg;
      $('#partTableData tr').has('td').each(function() {
          var arrayItem = {'Shipment_Order__c':component.get('v.recordId')};
          var isFirstColEmpty = false;
          $('td', $(this)).each(function(index, item) {
              var val = $(item).find("input").val() ? $(item).find("input").val().trim() :'';
              if(index == 0 && val == '')
                isFirstColEmpty = true;
              else {
                if(fromProCatalogue && ((isFirstColEmpty && index == 1 && val != '') || (!isFirstColEmpty && index == 1 && val == ''))){
                  msg = 'Please enter required fields (Internal Reference, Quantity)';
                  return false;
                }
                else if(!fromProCatalogue && ((isFirstColEmpty && (index == 2 || index == 3 ) && val !='') ||(!isFirstColEmpty && (index == 2 || index == 3 ) && val == ''))){
                  msg = 'Please enter required fields (Name, Quantity, Unit Price)';
                  return false;
                }else if(isFirstColEmpty && val == ''){
                  return false;
                }
                if(index == ind)
                  val = val != '' ? parseFloat(val.replace(/[,$]/g,'').trim()) : 0;

                arrayItem[headers[index]] = val;
              }
          });
          if(msg)
            return false;
          if(Object.keys(arrayItem).length > 1)
            lineItems.push(arrayItem);
      });

      if(msg || lineItems.length == 0 ){
        this.showToast('Error','error',msg ? msg:lineItems.length == 0 ? 'No LineItems or invalid data entered!':'Some Error occurred!');
        component.set('v.showSpinner',false);
        return;
      }
      this.serverCallCreateLineItem(component,lineItems,fromProCatalogue);

    },
    serverCallCreateLineItem:function(component,lineItems,fromProCatalogue){
      var action = component.get("c.createLineItems");
        action.setParams({
          fromProCatalogue: fromProCatalogue,
          recordId : component.get('v.recordId'),
          lineItems : JSON.stringify(lineItems)
      });
        action.setCallback(this, function(response) {
          component.set('v.showSpinner',false);
            var state = response.getState();
            if (state === "SUCCESS") {
              this.showToast('Success','success',response.getReturnValue());
              $A.get("e.force:closeQuickAction").fire();
            }
            else if (state === "INCOMPLETE") {
              this.showToast('Error','error','Request is incomplete');
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                      this.showToast('Error','error',("Error message: " + errors[0].message));
                    }
                } else {
                    this.showToast('Error','error',"Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    showToast : function(title,type,msg) {
      var toastEvent = $A.get("e.force:showToast");
      toastEvent.setParams({
          "title": title,
          "type" :type,
          "message": msg
      });
      toastEvent.fire();
  },
  receiveMessage: function (event) {

    var part_list = JSON.stringify(event.data.Parts);
    if(!part_list || part_list == '[]' || part_list.includes("Chuck")){
      this.showToast('Error','error','No Line Items entered.');
      return;
    }
    var parts = JSON.parse(part_list);
    var lineItems = [];
    for(var i=0;i<parts.length;i++){
      lineItems.push({
        Shipment_Order__c:this.component.get('v.recordId'),
        Name:parts[i].PartNumber,
        Description_and_Functionality__c:parts[i].PartDescription,
        Quantity__c:parts[i].Quantity,
        Commercial_Value__c:parts[i].UnitPrice,
      });
    }
    this.component.set('v.showSpinner',true);
    this.serverCallCreateLineItem(this.component,lineItems,false);
    //event.data.Type_of_Goods;
    //event.data.Li_ion_Batteries;
    //event.data.Li_ion_BatteryTypes;

  },
})