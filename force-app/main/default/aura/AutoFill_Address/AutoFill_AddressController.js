({
    // Auto Fill Address 
    OpenModal : function(component, event, helper) {
        component.set('v.searchKey', null);
        component.set('v.AddressList', null);
        component.set("v.showModalBox", true);
        
        component.set("v.address1", "");
        component.set("v.address2", "");
        component.set("v.city", "");
        component.set("v.state", "");
        component.set("v.zip", "");
        component.set("v.country", "");
        
    },
    //Clear the address list on the user interface
    clear: function(component, event, helper) {
        helper.clearAddressList(component, event);
    },
    //On search of address get address list from the API
    keyPressController: function(component, event, helper) {
        helper.getAddressRecommendations(component,event,false);
    },
    selectOption:function(component, event, helper) {
        console.log('select Option clicked');
        helper.getAddressDetailsByPlaceId(component, event,false);
    },
    addPlace: function(component, event, helper) {
        /*   var addIndex = component.get('v.pickIndex');*/
        //helper.addAddressRecord(component, event);
       // var addressList = component.get('v.PickaddList');
        //var addIndex = (addressList.length-1);
        console.log('addressList ');
        //console.log(addressList[addIndex]);
        
        
         if(! component.get("v.address1") || component.get("v.address1") == '  '){
              var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Error!",
                        "type": "error",
                        "message": "Address 1 is a required field"
                    });
                    toastEvent.fire();
              //alert('Add City');
              return;
          }
           if(! component.get("v.city")){
              var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Error!",
                        "type": "error",
                        "message": "City is a required field"
                    });
                    toastEvent.fire();
              //alert('Add City');
              return;
          }
           if(! component.get("v.state")){
              var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Error!",
                        "type": "error",
                        "message": "State is a required field"
                    });
                    toastEvent.fire();
              //alert('Add City');
              return;
          }
           if(! component.get("v.country")){
              var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Error!",
                        "type": "error",
                        "message": "Country is a required field"
                    });
                    toastEvent.fire();
              //alert('Add City');
              return;
          }
           if(! component.get("v.zip")){
              var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Error!",
                        "type": "error",
                        "message": "Zip/Postal Code is a required field"
                    });
                    toastEvent.fire();
              //alert('Add City');
              return;
          }
        
        
        
        var jsonObj = {};
        jsonObj.Address__c  = component.get("v.address1");
        jsonObj.Address2__c  = component.get("v.address2"); 
        jsonObj.City__c  = component.get("v.city");
        jsonObj.Postal_Code__c  = component.get("v.zip");
        jsonObj.Province__c  = component.get("v.state");
        jsonObj.All_Countries__c  = component.get("v.country");
        component.set("v.showModalBox", false);
        //addressList.push(jsonObj);
        component.set('v.PickaddList',jsonObj);
        console.log('addIndex');
        //console.log(JSON.stringify(addressList));
        console.log('PickaddList');
        console.log(JSON.stringify( component.get('v.PickaddList')));
        //  component.set('v.isPickupaddressModelOpen',true);
        // component.set('v.isNewPickAddOpen',true);
    }, 
    closeModal: function(component, event, helper) {
        component.set("v.showModalBox", false);
        // component.set('v.isPickupaddressModelOpen',true);
        //  component.set('v.isNewPickAddOpen',true);
    },
    finalClear: function(component, event, helper) {
        helper.clearFinalAddressList(component, event);
    },
    finalKeyPressController: function(component, event, helper) {
        helper.getAddressRecommendations(component,event,true);
    }, 
    selectFinalOption:function(component, event, helper) {
        console.log('select Option clicked');
        helper.getAddressDetailsByPlaceId(component, event,true);
    },
    addFinalPlace: function(component, event, helper) {
        var addIndex = component.get('v.finalPickIndex');
        var addressFinalList = component.get('v.FinalDeladdList');
        console.log('addressList ');
        console.log(addressFinalList[addIndex]);
        
         if(! component.get("v.finalAddress1") || component.get("v.finalAddress1") == '  '){
              var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Error!",
                        "type": "error",
                        "message": "Address 1 is a required field"
                    });
                    toastEvent.fire();
              //alert('Add City');
              return;
          }
           if(! component.get("v.finalCity")){
              var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Error!",
                        "type": "error",
                        "message": "City is a required field"
                    });
                    toastEvent.fire();
              //alert('Add City');
              return;
          }
           if(! component.get("v.finalState")){
              var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Error!",
                        "type": "error",
                        "message": "State is a required field"
                    });
                    toastEvent.fire();
              //alert('Add City');
              return;
          }
           if(! component.get("v.finalCountry")){
              var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Error!",
                        "type": "error",
                        "message": "Country is a required field"
                    });
                    toastEvent.fire();
              //alert('Add City');
              return;
          }
           if(! component.get("v.finalZip")){
              var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Error!",
                        "type": "error",
                        "message": "Zip/Postal Code is a required field"
                    });
                    toastEvent.fire();
              //alert('Add City');
              return;
          }
        
        
        addressFinalList[addIndex].Address__c  = component.get("v.finalAddress1");
        addressFinalList[addIndex].Address2__c  = component.get("v.finalAddress2"); 
        addressFinalList[addIndex].City__c  = component.get("v.finalCity");
        addressFinalList[addIndex].Postal_Code__c  = component.get("v.finalZip");
        addressFinalList[addIndex].Province__c  = component.get("v.finalState");
        addressFinalList[addIndex].All_Countries__c  = component.get("v.finalCountry");
        component.set("v.showFinalModalBox", false);
        component.set('v.FinalDeladdList',addressFinalList);
        component.set('v.isfinaldeliveryaddModelOpen',true);
        component.set('v.isNewFinDelAddOpen',true);
    }, 
    closeFinalModal: function(component, event, helper) {
        component.set("v.showFinalModalBox", false);
        component.set('v.isfinaldeliveryaddModelOpen',true);
        component.set('v.isNewFinDelAddOpen',true);
    },
    OpenFinalModal : function(component, event, helper){
        component.set('v.finalSearchKey', null);
        component.set('v.FinalAddressList', null);
        component.set("v.showFinalModalBox", true);
        var selectedItem = event.currentTarget;
        //Get the selected item index
        var indexvar = selectedItem.dataset.record;
        //var indexvar = event.getSource().get("v.label");
        //  console.log("indexvar:::" + indexvar);
        component.set('v.finalPickIndex',parseInt(indexvar));
        component.set('v.isNewFinDelAddOpen',false);
        component.set('v.isfinaldeliveryaddModelOpen',false);
        var addressFinalList = component.get('v.FinalDeladdList');
        console.log('addressList '+indexvar);
        console.log(JSON.stringify(addressFinalList[indexvar]));
        component.set("v.finalAddress1", "");
        component.set("v.finalAddress2", "");
        component.set("v.finalCity", "");
        component.set("v.finalState", "");
        component.set("v.finalZip", "");
        component.set("v.finalCountry", "");
        if(addressFinalList && addressFinalList[indexvar]){
            component.set("v.finalAddress1",addressFinalList[indexvar].Address__c);
            component.set("v.finalAddress2",addressFinalList[indexvar].Address2__c); 
            component.set("v.finalCity",addressFinalList[indexvar].City__c);
            component.set("v.finalState",addressFinalList[indexvar].Province__c);
            component.set("v.finalZip",addressFinalList[indexvar].Postal_Code__c);
            component.set("v.finalCountry",addressFinalList[indexvar].All_Countries__c);
        }
        console.log('addressList after '+indexvar);
        console.log(JSON.stringify(addressFinalList[indexvar]));
    },
    
    
    // Auto Fill Address End Here
    handleClick : function(component, event, helper) {
        
    }
    
})