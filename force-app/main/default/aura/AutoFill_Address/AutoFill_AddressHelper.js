({
    //Adding pickup new Address
    
    addAddressRecord: function(component, event) {
        //get the account List from component  
        var PickaddList = component.get("v.PickaddList");
        //Add New Account Record
        PickaddList.push({
            'sobjectType': 'Client_Address__c',
            'Contact_Full_Name__c': '',
            'Contact_email__c': '',
            'Contact_Phone_Number__c':'' ,
            'CompanyName__c': '',
            'Address__c': '',
            'Address2__c': '',
            'City__c': '',
            'Province__c': '',
            'Postal_Code__c':'',
            'All_Countries__c': ''
            
        });
        component.set("v.PickaddList", PickaddList);
    },
    
    //Adding final del new Address
    
    addFinaldelAddressRecord: function(component, event) {
        //get the account List from component  
        var FinalDeladdList = component.get("v.FinalDeladdList");
        //Add New Account Record
        FinalDeladdList.push({
            'sobjectType': 'Client_Address__c',
            'Contact_Full_Name__c': '',
            'Contact_email__c': '',
            'Contact_Phone_Number__c':'' ,
            'CompanyName__c': '',
            'Address__c': '',
            'City__c': '',
            'Province__c': '',
            'Postal_Code__c':'',
            'All_Countries__c': ''
            
        });
        component.set("v.FinalDeladdList", FinalDeladdList);
    },
    //AutoPLACE HELPER
    getAddressDetailsByPlaceId: function(component,event,isFinal){
        
        var selectedValue = event.currentTarget.dataset.value;
        var action = component.get("c.getAddressDetailsByPlaceId");
        action.setParams({
            PlaceID:selectedValue 
        });
        action.setCallback(this, function(response){
            var res = response.getState();
            if(res == 'SUCCESS'){
                console.log(response.getReturnValue());
                var response = JSON.parse(response.getReturnValue());
                var postalCode = '', state = '', country= '', city = '', street1 = '',street2 = '', street_number = '', route = '', subLocal1 = '', subLocal2 = '' , subLocal3 = '',neighbor='' ;
                
                for(var i=0; i < response.result.address_components.length ; i++){
                    var FieldLabel = response.result.address_components[i].types[0];
                    //console.log(FieldLabel);
                    //debugger;
                    if( FieldLabel == 'neighborhood' || FieldLabel == 'sublocality_level_3' || FieldLabel == 'sublocality_level_2' || FieldLabel == 'sublocality_level_1' || FieldLabel == 'street_number' || FieldLabel == 'route' || FieldLabel == 'locality' || FieldLabel == 'country' || FieldLabel == 'postal_code' || FieldLabel == 'administrative_area_level_1'){
                        
                        switch(FieldLabel){
                            case 'neighborhood':
                                neighbor = response.result.address_components[i].long_name;
                                break;
                            case 'sublocality_level_3':
                                subLocal3 = response.result.address_components[i].long_name;
                                break;
                            case 'sublocality_level_2':
                                subLocal2 = response.result.address_components[i].long_name;
                                break;
                            case 'sublocality_level_1':
                                subLocal1 = response.result.address_components[i].long_name;
                                break;
                            case 'street_number':
                                street_number = response.result.address_components[i].long_name;
                                break;
                            case 'route':
                                route = response.result.address_components[i].short_name;
                                break;
                            case 'postal_code':
                                postalCode = response.result.address_components[i].long_name;
                                break;
                            case 'administrative_area_level_1':
                                state = response.result.address_components[i].short_name;
                                break;
                            case 'country':
                                country = response.result.address_components[i].long_name;
                                break;
                            case 'locality':
                                city = response.result.address_components[i].long_name;
                                break;
                            default:
                                break;
                        }
                    }
                }
                
                street1 = street_number + ' ' + route + ' ' +neighbor;
                street2 = subLocal3+ ' ' +subLocal2 + ' ' + subLocal1;
                /*if(street == null || street == '' || street == ' '){
                    street = subLocal2 + ' ' + subLocal1;
                }*/
                //console.log(street);
                
                
                /*   var addIndex = component.get('v.pickIndex');
                var addressList = component.get('v.PickaddList');
                console.log('addressList ');
                console.log(addressList[addIndex]);
                addressList[addIndex].Address__c  = street;
                addressList[addIndex].City__c  = city;
                addressList[addIndex].Postal_Code__c  = postalCode;
                addressList[addIndex].Province__c  = state;
                addressList[addIndex].All_Countries__c  = country;*/
                // addressList.All_Countries__c  = country;
                // component.set('v.addressDetails.Street__c', street);
                // component.set('v.addressDetails.PostalCode__c', postalCode);
                // component.set('v.addressDetails.State__c', state);
                // component.set('v.addressDetails.Country__c', country);
                //component.set('v.addressDetails.City__c', city);
                console.log('countryName '+country);
                if(postalCode == null || postalCode == '' || postalCode == ' '){
                    var action1 = component.get("c.getZipCodeByCountry");
                    action1.setParams({
                        countryName: country
                    });
                    
                    action1.setCallback(this, function (response) {
                        var state1 = response.getState();
                        if (state1 === "SUCCESS") {
                            var result = response.getReturnValue();
                            
                            console.log('result');
                            console.log(JSON.stringify(result));
                            if(result.no_zip_code_countries && result.no_zip_code_countries.zip_code__c){
                                postalCode = result.no_zip_code_countries.zip_code__c;
                                component.set('v.searchKey', null)
                                component.set('v.AddressList', null);
                                //component.set('v.FinalAddressList', null);
                                /* component.set("v.showModalBox", false);
                    component.set('v.PickaddList',addressList);*/
                                if(isFinal){
                                    component.set("v.finalAddress1", street1);
                                    component.set("v.finalAddress2", street2);
                                    component.set("v.finalCity", city);
                                    component.set("v.finalState", state);
                                    component.set("v.finalZip", postalCode);
                                    component.set("v.finalCountry", country);
                                }else{
                                    component.set("v.address1", street1);
                                    component.set("v.address2", street2);
                                    component.set("v.city", city);
                                    component.set("v.state", state);
                                    component.set("v.zip", postalCode);
                                    component.set("v.country", country);
                                }
                            }
                            else{
                                alert('Postal code/ Zip Code/ Pin Code Not found Please do deep search');
                            }
                        }
                    });
                    $A.enqueueAction(action1);
                    
                    
                    /*var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Error!",
                        "type": "error",
                        "message": "Postal code/ Zip Code/ Pin Code Not found Please do deep search"
                    });
                    toastEvent.fire();*/
                }else{
                    component.set('v.searchKey', null)
                    component.set('v.AddressList', null);
                    //component.set('v.FinalAddressList', null);
                    /* component.set("v.showModalBox", false);
                    component.set('v.PickaddList',addressList);*/
                    if(isFinal){
                        component.set("v.finalAddress1", street1);
                        component.set("v.finalAddress2", street2);
                        component.set("v.finalCity", city);
                        component.set("v.finalState", state);
                        component.set("v.finalZip", postalCode);
                        component.set("v.finalCountry", country);
                    }else{
                        component.set("v.address1", street1);
                        component.set("v.address2", street2);
                        component.set("v.city", city);
                        component.set("v.state", state);
                        component.set("v.zip", postalCode);
                        component.set("v.country", country);
                    }
                    
                }
            }
        });
        $A.enqueueAction(action);
    },
    getAddressRecommendations: function(component, event,isFinal){
        var key = isFinal ? component.get("v.finalSearchKey"): component.get("v.searchKey");
        var action = component.get("c.getAddressFrom");
        action.setParams({
            "SearchText": key
        });
        
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var response = JSON.parse(response.getReturnValue());
                var predictions = response.predictions;
                var addresses = [];
                if (predictions.length > 0) {
                    for (var i = 0; i < predictions.length; i++) {
                        var bc =[];
                        addresses.push(
                            {
                                main_text: predictions[i].structured_formatting.main_text, 
                                secondary_text: predictions[i].structured_formatting.secondary_text,
                                place_id: predictions[i].place_id
                            });
                        
                    }
                }
                for(var i=0; i <addresses.length; i++){
                    console.log(addresses[i].main_text);
                    console.log(addresses[i].secondary_text);
                    console.log(addresses[i].place_id);
                }
                isFinal ? component.set("v.FinalAddressList", addresses) : component.set("v.AddressList", addresses);
                
            }
        });
        $A.enqueueAction(action);
    },
    clearAddressList : function(component) {
        console.log("Clearing the list!");
        component.set('v.searchKey', null)
        component.set('v.AddressList', null);
    },
    clearFinalAddressList : function(component) {
        console.log("Clearing the list!");
        component.set('v.finalSearchKey', null)
        component.set('v.FinalAddressList', null);
    }, 
})