({
    component :null,
    partslist:null,
    
    sethelper : function(componentP, eventP){this.component =componentP;  },
     //*******  shipment Package  Start****//
	 createTableRow : function( componentP, eventP ){

        var records = componentP.get( "v.shipment_order_package_list" );           //get the record list

        //add to records list
        records.push({
            "sobjectType":"Shipment_Order_Package__c",
            "packages_of_same_weight_dims__c":0,
            "Weight_Unit__c":"KGs",
            "Actual_Weight__c":0,
            "Dimension_Unit__c":"CMs",
            "Length__c":0,
            "Breadth__":0,
            "Height__":0

        });

        componentP.set( "v.shipment_order_package_list", records );

        console.log( "Records: " + records );
        console.log( " Size of the shipment_order_package_list " + records.length );

       // this.createObjectString( records );

    },//end of function definition





    /** this function will convert object list into a string object
     * @param object_listP this is list o objects we are converting to a string
     */
    createObjectString : function( object_listP ){

        var list_string = "";

        if( object_listP === undefined )
            return;                 //terminate function

        console.log( "List Count: " + object_listP.length );

        //loop through the object list
        for( var i=0; i < object_listP.length; ++i ){

            //add to list string
            list_string = list_string + object_listP[i].packages_of_same_weight_dims__c + "|" + object_listP[i].Weight_Unit__c + "|" +
                                        object_listP[i].Actual_Weight__c + "|" + object_listP[i].Dimension_Unit__c + "|" +
                                        object_listP[i].Length__c + "|" + object_listP[i].Breadth__c + "|" +
                                        object_listP[i].Height__c + ":";

        }//end of if block      

        console.log( "List String: " + list_string );

        return list_string;                 //return the list string

    },//end of function definition
    //*******  shipment Package End****//
     receiveMessage: function (event) {
      
     var myJSON = JSON.stringify(event.data.Parts);
      // this.component.set("v.parts_list",event.data.Parts);
       this.component.set("v.parts_list",myJSON);
       this.component.set("v.Secondhandgoods",event.data.Type_of_Goods);
       this.component.set("v.Li_ion_Batteries",event.data.Li_ion_Batteries);
       this.component.set("v.Li_ion_BatteryTypes",event.data.Li_ion_BatteryTypes);
       this.component.set("v.isPartModelOpen",false); 
        
        
         console.log("parts_list - Json ::", myJSON);
        console.log("parts_list ::", event.data.Parts);
        console.log("Secondhandgoods ::", event.data.Type_of_Goods);
        console.log("Li_ion_Batteries ::", event.data.Li_ion_Batteries);
        console.log("Li_ion_BatteryTypes ::", event.data.Li_ion_BatteryTypes);
      			              
    },


	
	
})