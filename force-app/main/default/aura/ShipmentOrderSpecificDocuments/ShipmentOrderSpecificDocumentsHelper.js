({
  checkIsComplianceReady: function(component) {
    var allRowsDetails = component.get('v.allRowsDetails');
    var isComplianceReady = false;

    for(var index in allRowsDetails) {
      var allRowsDetail = allRowsDetails[index];
      if(!(allRowsDetail.isNotRequired || allRowsDetail.attachment != '')) {
        isComplianceReady = true;
        break;
      }
    }
    component.set('v.isComplianceReady', isComplianceReady);
  },

  removeAttachmentsHlpr: function(component, helper, attachmentId) {
    component.set('v.showInnerSpinner', true);
    var params = {
      attachmentIds: [attachmentId]
    };
    this.makeServerCall(component, 'removeAttachments', params, function(result) {
      if(result.status === 'OK') {
        //helper.showToast('success', 'Success', result.msg);
      }
      else {
        helper.showToast('error', 'Error Occured', result.msg);
      }
      component.set('v.showInnerSpinner', false);
    });
  },
  updateShipmentDocs:function(component, docsNotRequired,helper) {
    var shipmentOrder = component.get('v.shipmentOrder');
    component.set('v.showInnerSpinner', true);
    var params = {
      shipmentId: shipmentOrder.Id,
      docsNotRequired:docsNotRequired
    };
    this.makeServerCall(component, 'updateShipmentDocNotRequired', params, function(result) {
      if(result.status === 'OK') {
        helper.showToast('success', 'Success', result.msg);
      }
      else {
        helper.showToast('error', 'Error Occured', result.msg);
      }
      component.set('v.showUpdateButton', false);
      component.set('v.showInnerSpinner', false);
    });
  }
})