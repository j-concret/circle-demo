({
  doInitCtrl: function(component, event, helper) {
    var shipmentOrder = component.get('v.shipmentOrder');
    var allRowsDetails = [];
    var dd = shipmentOrder.Name+' Commercial Invoice';
    var allRequiredDocsName = shipmentOrder.CPA_v2_0__r.Required_Documents_Shipment_Specific__c? shipmentOrder.CPA_v2_0__r.Required_Documents_Shipment_Specific__c.split(';'):[];
    var newDocReq = [dd,'AWB - Air Waybill'];
    for(var q=0;q<allRequiredDocsName.length;q++){
        newDocReq.push(allRequiredDocsName[q].replace(/[&\/\\#,+()$~%.:*?<>{}]/g,''));
    }
    var notRequiredDocsName = shipmentOrder.Documents_Not_Required__c?shipmentOrder.Documents_Not_Required__c.split(';'):[];
    var attachments = component.get('v.attachments');
    var reqAttachments = {};
    for(var index in attachments) {
      var name = attachments[index].fileName;
      name = name.replace(/\.[^/.]+$/, "");
      if(newDocReq.includes(name)) {
        reqAttachments[name] = attachments[index];
      }
    }

    allRowsDetails.push({name: dd, availableIcon: reqAttachments[dd]?'action:approval':'action:close', attachment: reqAttachments[dd]?reqAttachments[dd]:'', isNotRequired: false, isAutoGenerate: true, isDocNotGenerated: !reqAttachments[dd],isDisabled:true});
    allRowsDetails.push({name: 'AWB - Air Waybill', availableIcon: reqAttachments['AWB - Air Waybill']?'action:approval':'action:close', attachment: reqAttachments['AWB - Air Waybill']?reqAttachments['AWB - Air Waybill']:'', isNotRequired: false, isAutoGenerate: false, isDocNotGenerated: false,isDisabled:true});
    for(var index in allRequiredDocsName) {
      var name = allRequiredDocsName[index];
      var nm = name.replace(/[&\/\\#,+()$~%.:*?<>{}]/g,'');
      var obj = {};
      if(reqAttachments[nm]) {
        obj ={name: name,attachment : reqAttachments[nm],isNotRequired: notRequiredDocsName.includes(name),isAutoGenerate: false,availableIcon : 'action:approval',isDocNotGenerated : false,isDisabled:false};
      }else{
        obj = {name: name, availableIcon: 'action:close', attachment: '', isNotRequired: notRequiredDocsName.includes(name), isAutoGenerate: false, isDocNotGenerated: true,isDisabled:false};
      }
      obj.availableIcon = (obj.isNotRequired ? 'action:reject' : (obj.attachment != '' ? 'action:approval' : 'action:close'));
      allRowsDetails.push(obj);
    }
    component.set('v.allRowsDetails', allRowsDetails);
    helper.checkIsComplianceReady(component);
  },
  autoGenerateCtrl: function(component, event, helper) {
    component.set('v.showInnerSpinner', true);
    var index = event.getSource().get('v.name');
    var allRowsDetails = component.get('v.allRowsDetails');

    allRowsDetails[index].isDocNotGenerated = false;

    component.set((index == 0 ? 'v.isOpenCreateCI' : 'v.isOpenCreateAWB'), true);
    component.set('v.allRowsDetails', allRowsDetails);
    setTimeout(
      $A.getCallback(function() {
        component.set('v.showInnerSpinner', false);
      }, 5000)
    );
  },
  handleIsNotRequiredChangeCtrl: function(component, event, helper) {
    var isChecked = event.getSource().get('v.checked');
    var index = event.getSource().get('v.name');
    var allRowsDetails = component.get('v.allRowsDetails');
    allRowsDetails[index].availableIcon = (isChecked ? 'action:reject' : (allRowsDetails[index].attachment != '' ? 'action:approval' : 'action:close'));
    component.set('v.allRowsDetails', allRowsDetails);
    component.set('v.showUpdateButton', true);
    helper.checkIsComplianceReady(component);
  },
  handleIsComplianceReadyCtrl: function(component, event, helper) {
    if(!component.get('v.isComplianceReady')) {
      helper.checkIsComplianceReady(component);
    }
  },
  closeCreateCIModalCtrl: function(component, event, helper) {
    component.set('v.isOpenCreateCI', false);
    component.set('v.isOpenCreateAWB', false);
  },
  removeAttachmentsCtrl: function(component, event, helper) {
    var index = event.getSource().get('v.name');
    var allRowsDetails = component.get('v.allRowsDetails');
    var attachmentId;

    if(allRowsDetails[index].attachment && allRowsDetails[index].attachment.id) {
      attachmentId = allRowsDetails[index].attachment.id;
    }
    allRowsDetails[index].attachment = '';
    allRowsDetails[index].availableIcon = 'action:close';

    component.set('v.allRowsDetails', allRowsDetails);
    helper.checkIsComplianceReady(component);

    if(attachmentId) {
      helper.removeAttachmentsHlpr(component, helper, attachmentId);
    }
  },
  updateShipmentSpecificRecords: function(component, event, helper) {
    var allRowsDetails = component.get('v.allRowsDetails');
    var docsNotRequired = [];
    for (var index = 0; index < allRowsDetails.length; index++) {
      var element = allRowsDetails[index];
      if(element.isNotRequired){
        docsNotRequired.push(element.name);
      }
    }
    helper.updateShipmentDocs(component,docsNotRequired.join(';'),helper);
  }
});