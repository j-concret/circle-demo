({
    activeSections: function(component, event){
        var openSections = component.find("InvoiceRollUps").get("v.activeSectionName");
        if(openSections.length>0){
            for(var i =0; i<openSections.length; i++){
    
    
    if(openSections[i]==="customerInvoiceSummary") {
        var editFormCustomerInvoiceSummary = component.find("editFormCustomerInvoiceSummary");
        $A.util.toggleClass(editFormCustomerInvoiceSummary, "slds-hide");
        var viewFormCustomerInvoiceSummary = component.find("viewFormCustomerInvoiceSummary");
        $A.util.toggleClass(viewFormCustomerInvoiceSummary, "slds-hide");
}
else if(openSections[i]==="actualCosts") {
    var editFormActualCosts = component.find("editFormActualCosts");
    $A.util.toggleClass(editFormActualCosts, "slds-hide");
    var viewFormActualCosts = component.find("viewFormActualCosts");
    $A.util.toggleClass(viewFormActualCosts, "slds-hide");
}
else if(openSections[i]==="supplierInvoicing") {
    var editFormSupplierInvoicing = component.find("editFormSupplierInvoicing");
    $A.util.toggleClass(editFormSupplierInvoicing, "slds-hide");
    var viewFormSupplierInvoicing = component.find("viewFormSupplierInvoicing");
    $A.util.toggleClass(viewFormSupplierInvoicing, "slds-hide");
}else {
    var editFormProfit = component.find("editFormProfit");
    $A.util.toggleClass(editFormProfit, "slds-hide");
    var viewFormProfit = component.find("viewFormProfit");
    $A.util.toggleClass(viewFormProfit, "slds-hide");
}}}}
})