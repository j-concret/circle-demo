({
    createObjectData: function(component, event) {
        // get the contactList from component and add(push) New Object to List  
        var RowItemList = component.get("v.costingList");
        var Shipment = component.get('v.shipmentObject');
        var SupplierId = Shipment.SupplierlU__r;
		
        RowItemList.push({
            'sobjectType': 'CPA_Costing__c',
            'Name': '',
            'Supplier__c':SupplierId,
            'IOR_EOR__c': 'IOR',
            'Cost_Category__c': 'IOR Cost',
            'Cost_Type__c': 'Fixed',
            'Applied_to__c': 'Shipment value',
            'Amended__c':false,
            'Currency__c': 'US Dollar (USD)'
        });
        // set the updated list to attribute (contactList) again    
        component.set("v.costingList", RowItemList);
        component.set("v.isCostingDataAvailable",true);
        
    },
    // helper function for check if first Name is not null/blank on save  
    validateRequired: function(component, event) {
        var isValid = true;
        var allContactRows = component.get("v.costingList");
        for (var indexVar = 0; indexVar < allContactRows.length; indexVar++) {
            if (allContactRows[indexVar].Name == '') {
                isValid = false;
                alert('First Name Can\'t be Blank on Row Number ' + (indexVar + 1));
            }
        }
        return isValid;
    },
    validatingCostings: function(component, event){
        var that = this;
        var costingList = component.get('v.costingList');
        var nameElements = [];
        var accountElements = [];
        var rateElements = [];
        var amountElements = [];
        var noteElements = [];
        var categoryElements = [];
        var costTypeElements = [];
        var currencyElements = [];
        
        
        
        (component.find("costName") && component.find("costName").length > 0 )?
            nameElements=component.find("costName") :
        nameElements.push(component.find("costName")) ;
        
        (component.find("costRate") && component.find("costRate").length > 0) ?
            rateElements=component.find("costRate") :
        rateElements.push(component.find("costRate")) ;
        
        (component.find("costAmount") && component.find("costAmount").length > 0) ?
            amountElements=component.find("costAmount") :
        amountElements.push(component.find("costAmount")) ;
        
        (component.find("costNote") && component.find("costNote").length > 0 ) ?
            noteElements=component.find("costNote") :
        noteElements.push(component.find("costNote")) ;
        
        (component.find("category") && component.find("category").length > 0 )?
            categoryElements=component.find("category") :
        categoryElements.push(component.find("category")) ;
        
        ( component.find("costType") && component.find("costType").length > 0 )?
            costTypeElements=component.find("costType") :
        costTypeElements.push(component.find("costType")) ;
        
        (component.find("currency") && component.find("currency").length > 0) ?
            currencyElements=component.find("currency") :
        currencyElements.push(component.find("currency")) ;
        
        var isValid = true;
        for(var index=0; index < costingList.length ; index++){
            var costing = costingList[index];
            if(! costing.Name){
                isValid =false;
                nameElements[index].setCustomValidity("Complete this field");
                nameElements[index].reportValidity();
            }else{
                nameElements[index].setCustomValidity("");
                nameElements[index].reportValidity();
            }
            
             if(costing.Supplier__c.Id == null){
                isValid =false;
                  that.fireToast('ERROR', 'error', "Please Enter Supplier at row "+(index+1));
            }
            //if(! categoryElements[index].get("v.value")){
              //  isValid =false;
                //categoryElements[index].set("v.errors", [{message:"Complete this field"}]);
                /*categoryElements[index].setCustomValidity("Complete this field");
                categoryElements[index].reportValidity();*/
           // }else{
               // categoryElements[index].set("v.errors", null);
                /*categoryElements[index].setCustomValidity("");
                categoryElements[index].reportValidity();*/
           // }
            
            //if(! currencyElements[index].get("v.value")){
              //  isValid =false;
              //  currencyElements[index].set("v.errors", [{message:"Complete this field"}]);
                /*currencyElements[index].setCustomValidity("Complete this field");
                currencyElements[index].reportValidity();*/
            //}else{
              //  currencyElements[index].set("v.errors", null);
                /*currencyElements[index].setCustomValidity("");
                currencyElements[index].reportValidity();*/
            //}
            
            //if(costing.Cost_Type__c){
             //   costTypeElements[index].set("v.errors", null);
                /*costTypeElements[index].setCustomValidity("");
                costTypeElements[index].reportValidity();*/
                if(costing.Cost_Type__c == "Fixed")
                {
                    if(!costing.Amount__c){
                        isValid =false;
                       // rateElements[index].setCustomValidity("");
                      //  rateElements[index].reportValidity();
                        amountElements[index].setCustomValidity("Complete this field");
                        amountElements[index].reportValidity();
                    }else{
                        amountElements[index].setCustomValidity("");
                        amountElements[index].reportValidity();
                       
                    }
                }
                if(costing.Cost_Type__c == "Variable"){
                    if(!costing.Rate__c){
                        isValid =false;
                        //amountElements[index].setCustomValidity("");
                        //amountElements[index].reportValidity();
                        rateElements[index].setCustomValidity("Complete this field");
                        rateElements[index].reportValidity();
                    }else{
                        
                        rateElements[index].setCustomValidity("");
                        rateElements[index].reportValidity();
                    }
                }
            //}else{
              //  isValid =false;
              //  costTypeElements[index].set("v.errors", [{message:"Complete this field"}]);
                /*costTypeElements[index].setCustomValidity("Complete this field");
                costTypeElements[index].reportValidity(); */
            //}
        }
        if(isValid){
            that.createCosting(component, event);
        }else{
            component.set('v.isLoading',false);
        }
        
    },
    createCosting: function(component, event){
        var that = this;
        var costingList = component.get('v.costingList');
        var shipmentObject = component.get('v.shipmentObject');
            
        for(let costing of costingList){
             costing['CPA_v2_0__c']= component.get('v.shipmentObject').CPA_v2_0__r.Related_Costing_CPA__c;
            costing['Supplier__c']= costing['Supplier__c'].Id;//component.get('v.shipmentObject').SupplierlU__c;
            costing['Shipment_Order_Old__c']= component.get('v.shipmentObject').Id;
            costing['Variable_threshold__c']=0;
            costing['Additional_Percent__c']=0;
            costing['isManualCosting__c'] = 'true';
            costing['RecordTypeId']="0120Y000000ytNmQAI";
            if(costing['Cost_Type__c'] == "Fixed"){
               costing['Applied_to__c'] = null;
            }
            if(costing['Cost_Type__c'] && costing['Cost_Type__c'] == "Variable"){
                costing['Rate__c'] = ( costing['Rate__c'] == null ?  0 : costing['Rate__c']);
                if( costing['Applied_to__c'] == 'Shipment value') {
                    costing['Amount__c'] = (shipmentObject.Shipment_Value_USD__c) * (costing.Rate__c/100) ;
                }
                else if( costing['Applied_to__c']  == 'Total Taxes') {
                   costing['Amount__c'] = (shipmentObject.IOR_Refund_of_Tax_Duty__c)  * (costing.Rate__c/100) ;
                }
                else if( costing['Applied_to__c']  == 'CIF value') {
                    costing['Amount__c'] = (((shipmentObject.Shipment_Value_USD__c + 250) * 1.05) )  * (costing.Rate__c/100)  ;
                }
                else if( costing['Applied_to__c']  == 'Chargeable weight') {
                    costing['Amount__c'] =  (shipmentObject.Chargeable_Weight__c ) * costing.Rate__c  ;
                }
                else if( costing['Applied_to__c']  == '# Parts (line items)') {
                    costing['Amount__c'] =  (shipmentObject.of_Line_Items__c ) * costing.Rate__c  ;
                }
                else if( costing['Applied_to__c']  == '# Unique HS Codes') {
                    costing['Amount__c'] =  (shipmentObject.of_Unique_Line_Items__c  ) * costing.Rate__c ;
                }
                else if( costing['Applied_to__c']  == '# of Final Deliveries') {
                    costing['Amount__c'] =  (shipmentObject.Final_Deliveries_New__c ) * costing.Rate__c ;
                }
                else {costing['Amount__c'] = (shipmentObject.of_packages__c  ) * costing.Rate__c  ;}
            }
        }
        console.log('costingList',costingList);
        component.set('v.costingList',costingList);
        var action = component.get("c.updateCostingsList");
        action.setParams({
            'costingsToUpdate': costingList
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS") {
                var result = response.getReturnValue(); if(result.status === "OK") {
                    console.log('update Success');
                    component.set('v.costingList',[]);
                    that.createObjectData(component, event);
                    component.set('v.isNewCostingsCreated',true);
                     component.set('v.isLoading',false);
                    that.fireToast('SUCCESS', 'success', 'Costings Created Successfully !');
                    //component.set('v.isLoading',false);
                } else {
                    console.log(result.msg);
                     component.set('v.isLoading',false);
                     that.fireToast('ERROR', 'error', result.msg);
                    // component.set('v.isLoading',false);
                }
            }
            else if(state === "ERROR") {
                var errors = response.getError();
                if(errors) {
                    if(errors[0] && errors[0].message) {
                        console.log(errors[0].message);
                         component.set('v.isLoading',false);
                        //component.set('v.isLoading',false);
                        that.fireToast('ERROR', 'error', errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                     component.set('v.isLoading',false);
                    // component.set('v.isLoading',false);
                    that.fireToast('ERROR', 'error', "Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
        
    },
    fireToast: function(title, type, message) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": title,
            "type": type,
            "message": message
        });
        toastEvent.fire();
    }
})