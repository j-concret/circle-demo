({
    AddNewRow : function(component, event, helper){
       // fire the AddNewRowEvt Lightning Event 
        component.getEvent("AddRowEvt").fire();     
    },
    
    removeRow : function(component, event, helper){
        var index = event.target.dataset.index;
       console.log('index ',index)
       // fire the DeleteRowEvt Lightning Event and pass the deleted Row Index to Event parameter/attribute
       component.getEvent("DeleteRowEvt").setParams({"indexVar" : index }).fire();
    },
    // function call on component Load
    doInit: function(component, event, helper) {
        // create a Default RowItem [Contact Instance] on first time Component Load
        // by call this helper function  
        helper.createObjectData(component, event);
        //component.getEvent("AddRowEvt").fire(); 
       /* var costingList = component.get("v.costingList");
        var Shipment = component.get('v.shipmentObject');
        console.log('Shipment');
        console.log(JSON.stringify(Shipment));
        var SupplierId = Shipment.SupplierlU__r;
        costingList[0].Supplier__c = SupplierId;
        component.set("v.costingList",costingList);*/
            //'Supplier__c':SupplierId,
    },
 	 afterRender : function(cmp,helper){
        this.superAfterRender();
        var costingList = component.get('v.costingList');
       for(var index=0; index < costingList.length ; index++){
           var inputCmp = document.getElementsByClassName("inputCmp"+index);
            console.log(JSON.stringify(inputCmp));
           //inputCmp.setCustomValidity('You gotta fill this out, yo!');
           //inputCmp.reportValidity();
       }
    },
   createNewCostings: function(component, event, helper) {
       component.set('v.isLoading',true);
       component.set('v.isNewCostingsCreated',false);
      helper.validatingCostings(component, event);
    },
    // function for create new object Row in Contact List 
    addNewRow: function(component, event, helper) {
        // call the comman "createObjectData" helper method for add new Object Row to List  
        helper.createObjectData(component, event);
    },
    // function for delete the row 
    removeDeletedRow: function(component, event, helper) {
        // get the selected row Index for delete, from Lightning Event Attribute  
        var index = event.getParam("indexVar");
        // get the all List (contactList attribute) and remove the Object Element Using splice method    
        var AllRowsList = component.get("v.costingList");
        AllRowsList.splice(index, 1);
        // set the contactList after remove selected row element  
        component.set("v.costingList", AllRowsList);
    },
  
})