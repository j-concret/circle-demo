({
    doInit: function(cmp, event, helper) {
        var pageRef = cmp.get("v.pageReference");
        if(pageRef){
            cmp.set("v.recordId",pageRef.state.c__soId);
            cmp.set("v.isOpenCreateCI", true);
            if(pageRef.state.c__newSOPage){
                cmp.set("v.newSOPage", pageRef.state.c__newSOPage);
            }
        }
        helper.doInit(cmp, event);
    },
    handleRefresh: function(component, event, helper) {
        helper.doInit(component, event);
    },
    doPreview: function (component, event, helper) {
        
        if(component.get('v.isMissingDataChecked')){
            component.set('v.hideOptions', true);
            component.set('v.disableBack', false);
            component.set('v.disableDownload', false);
            component.set('v.disableRefresh', true);
        }else{
            helper.checkDataError(component, event);
        }
        
    },
    handleBack: function(component, event, helper) {
        component.set('v.hideOptions', false);
        component.set('v.disableDownload', true);
        component.set('v.disableBack', true);
        component.set('v.disableRefresh', false);
        //helper.doInit(component, event);
    },
    
    saveDownload: function(component, event, helper) {
        if(component.get('v.isEditMode')){
            helper.showToast('Error','error','Please disable edit mode!');
            return;
        }
        var selectedValue = event.getParam('value');
        if(selectedValue){
            var childComp = component.find('CISheetCmp');
            childComp.downloadSave(selectedValue);
        }
        
    },
    downloadPdf : function(component, event, helper){
        // Build url for Visualforce page
        var vfURL = 'https://' + component.get("v.visualforceDomain") + '/apex/RenderCI_PDF?';
        vfURL = vfURL + "shipmentOrder=" + JSON.stringify(component.get("v.shipmentOrder"));
        vfURL = vfURL + "&freightRequest=" + JSON.stringify(component.get("v.freightRequest"));
        vfURL = vfURL + "&vatValue=" + JSON.stringify(component.get("v.vatValue"));
        vfURL = vfURL + "&portValue=" + JSON.stringify(component.get("v.portValue"));
        vfURL = vfURL + "&isEditMode=" + JSON.stringify(component.get("v.isEditMode"));
        vfURL = vfURL + "&showSpinner=" + JSON.stringify(component.get("v.showSpinner"));
        console.log("VF URL: ",vfURL);
        
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": vfURL
        });
        urlEvent.fire();
        
    },
    doDownload:function(component, event, helper) {
        var childComp = component.find('CISheetCmp');
        childComp.downloadSave('downloadExcel');
    },
    closeCreateCIModal: function(component, event, helper) {
        component.set('v.isOpenCreateCI', false);
        if(!component.get('v.newSOPage')){
            var appEvent = $A.get('e.c:closeModal');
            appEvent.fire();
        }
        
    },
    handleEditMode:function(component, event, helper) {
        component.set('v.isEditMode', !component.get('v.isEditMode'));
        
    }
});