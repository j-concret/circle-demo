({
    doInit: function(component, event){
        component.set('v.showSpinner',true);
        var action = component.get('c.getShipmentRelatedData');
        action.setParams({recId: component.get('v.recordId')});
        action.setCallback(this, function(response) {
            component.set('v.showSpinner',false);
            var state = response.getState();
            if(state == 'SUCCESS') {
                var result = response.getReturnValue();
                if(result.status == 'OK'){
                    component.set('v.shipmentOrder', result.shipmentOrder);
                    component.set('v.freightRequest', result.freightRequest);
                    //component.set('v.missingValueList', result.missingDataList);  
                    
                    var ports = result.ports;
                    var vatPortOptions = [];
                    var nonVatPortOptions = [];
                    if(ports && ports['VAT'])
                        vatPortOptions = ports['VAT'].map(record=>({label:record,value:record}));
                    
                    if(ports && ports['NON-VAT'])
                        nonVatPortOptions = ports['NON-VAT'].map(record=>({label:record,value:record}));
                    
                    if(ports){
                        var vatOptions = (Object.keys(ports)).map(record=>({label:record,value:record}));
                        component.set('v.vatOptions',vatOptions);
                    }
                    
                    component.set('v.vatPortOptions', vatPortOptions);
                    component.set('v.nonVatPortOptions', nonVatPortOptions);
                }else if(state === "ERROR") {
                    var errors = response.getError();
                    if(errors) {
                        if(errors[0] && errors[0].message) {
                            this.showToast('Error','error',errors[0].message);
                        }
                    } else {
                        this.showToast('Error','error','Unknown Error');
                    }
                    
                }
            }
        });
        $A.enqueueAction(action);
    },
    showToast : function(title,type,msg) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title:title,
            type:type,
            message:msg
        });
        toastEvent.fire();
    },
    checkDataError : function(component, event){
        component.set('v.showSpinner',true);
        var action = component.get('c.checkMissingData');
        action.setParams({
            recId: component.get('v.shipmentOrder').Id,
            vatValue: component.get('v.vatValue'),
            portValue: component.get('v.portValue')       
        });
        action.setCallback(this, function(response) {
            component.set('v.showSpinner',false);
            var state = response.getState();
            if(state == 'SUCCESS') {
                var result = response.getReturnValue();
                if(result.status == 'OK'){
                    if(result.missingDataList.length > 0){
                        component.set('v.missingValueList', result.missingDataList); 
                        
                        var label = event.getSource().get("v.label");
               			event.getSource().set("v.label","Create CI Anyway");
                        
                    }else{
                        component.set('v.hideOptions', true);
    					component.set('v.disableBack', false);
    					component.set('v.disableDownload', false);
    					component.set('v.disableRefresh', true);
                    } 
                    
                    component.set('v.isMissingDataChecked', true);
                    
                    
                }else if(state === "ERROR") {
                    var errors = response.getError();
                    if(errors) {
                        if(errors[0] && errors[0].message) {
                            this.showToast('Error','error',errors[0].message);
                        }
                    } else {
                        this.showToast('Error','error','Unknown Error');
                    }
                    
                }
            }
        });
        $A.enqueueAction(action);
    }
 });