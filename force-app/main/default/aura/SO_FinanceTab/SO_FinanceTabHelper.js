({
    
    
    
    activeSections: function(component, event){
                      var openSections = component.find("FinanceAccordion").get("v.activeSectionName");
                      if(openSections.length>0){
                        for(var i =0; i<openSections.length; i++){
                        
                        if(openSections[i]==="invoicing"){
                                var editFormInvoicing = component.find("editFormInvoicing");
                                $A.util.toggleClass(editFormInvoicing, "slds-hide");
                                var viewFormInvoicing = component.find("viewFormInvoicing");
                                $A.util.toggleClass(viewFormInvoicing, "slds-hide");
                            } 
                            else if(openSections[i]==="clientInvoiceDetails") {
                                        var editFormClientInvoiceDetails = component.find("editFormClientInvoiceDetails");
                                        $A.util.toggleClass(editFormClientInvoiceDetails, "slds-hide");
                                        var viewFormClientInvoiceDetails = component.find("viewFormClientInvoiceDetails");
                                        $A.util.toggleClass(viewFormClientInvoiceDetails, "slds-hide");
                                    }
                                    else if(openSections[i]==="invoicingParameters") {
                                        var editFormInvoicingParameters = component.find("editFormInvoicingParameters");
                                        $A.util.toggleClass(editFormInvoicingParameters, "slds-hide");
                                        var viewFormInvoicingParameters = component.find("viewFormInvoicingParameters");
                                        $A.util.toggleClass(viewFormInvoicingParameters, "slds-hide");
                                    }
                                    else if(openSections[i]==="taxesAndDuties") {
                                   var editFormTaxesAndDuties = component.find("editFormTaxesAndDuties");
                                   $A.util.toggleClass(editFormTaxesAndDuties, "slds-hide");
                                   var viewFormTaxesAndDuties = component.find("viewFormTaxesAndDuties");
                                   $A.util.toggleClass(viewFormTaxesAndDuties, "slds-hide");
                                   }
                                   else if (openSections[i]==="forecastCosts") {
    var editFormForecastCosts = component.find("editFormForecastCosts");
    $A.util.toggleClass(editFormForecastCosts, "slds-hide");
    var viewFormForecastCosts = component.find("viewFormForecastCosts");
    $A.util.toggleClass(viewFormForecastCosts, "slds-hide");
}  else {
    var editFormTecExShipping = component.find("editFormTecExShipping");
    $A.util.toggleClass(editFormTecExShipping, "slds-hide");
    var viewFormTecExShipping = component.find("viewFormTecExShipping");
    $A.util.toggleClass(viewFormTecExShipping, "slds-hide");
}}}}

})