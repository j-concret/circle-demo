({
    //Edit Record
    editInvoicingRecord : function(component, event, helper) {helper.activeSections(component)},
    editClientInvoiceDetailsRecord : function(component, event, helper) {helper.activeSections(component)},
    editInvoicingParametersRecord : function(component, event, helper) {helper.activeSections(component)},
    editTaxesAndDutiesRecord : function(component, event, helper) {helper.activeSections(component)},
    editForecastCostsRecord : function(component, event, helper) {helper.activeSections(component)},
    editTecExShippingRecord : function(component, event, helper) {helper.activeSections(component)},

    //required Fields
    showRequiredFields: function(component, event, helper){
        $A.util.removeClass(component.find("Account__c"), "none");
        $A.util.removeClass(component.find("Ship_to_Country__c"), "none");
        $A.util.removeClass(component.find("IOR_Price_List__c"), "none");
        $A.util.removeClass(component.find("Ship_From_Country__c"), "none");
        $A.util.removeClass(component.find("Shipment_Value_USD__c"), "none");
        $A.util.removeClass(component.find("Client_Contact_for_this_Shipment__c"), "none");
        
},

//onsubmit
onRecordSubmit: function (component, event, helper) {
    
    var invalidFields = [];
    //Required Fields
    /*var account                      = component.find("Account__c");
    var accountValue                 = account.get("v.value");
    if (!accountValue || accountValue.trim().length=== 0) {invalidFields.push(account);}
    
    var shipToCountry                = component.find("Ship_to_Country__c");
    var shipToCountryValue           = shipToCountry.get("v.value");
    if (!shipToCountryValue || shipToCountryValue.trim().length=== 0) {invalidFields.push(shipToCountry);}

    var iorPriceList                 = component.find("IOR_Price_List__c") ;
    var iorPriceListValue            = iorPriceList.get("v.value");
    if (!iorPriceListValue || iorPriceListValue.trim().length=== 0) {invalidFields.push(iorPriceList);}

    var shipFromCountry              = component.find("Ship_From_Country__c") ;
    var shipFromCountryValue              = shipFromCountry.get("v.value");
    if (!shipFromCountryValue || shipFromCountryValue.trim().length=== 0) {invalidFields.push(shipFromCountry);}

   /* var shipmentValueUSD             = component.find("Shipment_Value_USD__c");
    var shipmentValueUSDValue        = shipmentValueUSD.get("v.value");
    if (!shipmentValueUSDValue || shipmentValueUSDValue.trim().length=== 0) {invalidFields.push(shipmentValueUSD);}*/

    //var clientContactForThisShipment = component.find("Client_Contact_for_this_Shipment__c");
    //var clientContactForThisShipmentValue = clientContactForThisShipment.get("v.value");
    //if (!clientContactForThisShipmentValue || clientContactForThisShipmentValue.trim().length=== 0) {invalidFields.push(clientContactForThisShipment);}
   
    
   
   
   if(invalidFields.length >0 ){
       for(var i=0; i<invalidFields.length; i++){ $A.util.addClass(invalidFields[i], 'slds-has-error');}
    event.preventDefault();
    }
},                


//onsuccess
handleSuccess : function(component, event, helper) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({ "title": "Success!",
                              "message": "Saved.",
                              "type": "success"});
toastEvent.fire();
helper.activeSections(component);
},

onError : function(component, event, helper) {

var toastEvent2 = $A.get("e.force:showToast");
toastEvent2.setParams({
"title": "Error!",
"message": "Error."
});
toastEvent2.fire();
},

handleCancel : function(component, event, helper) {
helper.activeSections(component);
event.preventDefault();
}
})