({
    
    
    activeSections: function(component, event){
        
        var editFormGeneralInformation = component.find("viewFormShipmentContacts");
        $A.util.toggleClass(editFormGeneralInformation, "slds-hide");
        var viewFormGeneralInformation = component.find("editFormShipmentContacts");
        $A.util.toggleClass(viewFormGeneralInformation, "slds-hide");
    },
    refreshFocusedTab : function(component, event, helper) {
        var workspaceAPI = component.find("workspace");
        workspaceAPI.getFocusedTabInfo().then(function(response) {
            var focusedTabId = response.tabId;
            workspaceAPI.refreshTab({
                      tabId: focusedTabId,
                      includeAllSubtabs: true
             });
        })
        .catch(function(error) {
            console.log(error);
        });
    }
    
})