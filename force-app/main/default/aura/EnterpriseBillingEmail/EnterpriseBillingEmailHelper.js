({
    getContactDetails:function(component){
        var action = component.get("c.getRelatedContacts");
        action.setParams({
            'eId':component.get('v.eId')
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS") {
                var result = response.getReturnValue(); 
                if(result.status === "OK") {
                    component.set('v.enterpriseObject',result.enterRecord);
                    component.set('v.contactsList',result.contactsList);
                    component.set('v.activeUser',result.activeUser);
                    this.setTemplate(component);
                } else {
                    console.log(result.msg);
                }
            }
            else if(state === "ERROR") {
                var errors = response.getError();
                if(errors) {
                    if(errors[0] && errors[0].message) {
                        console.log(errors[0].message);
                        //that.fireToast('ERROR', 'error', errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                    //that.fireToast('ERROR', 'error', "Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
        
    },
    setTemplate: function(component){
        var date = new Date(component.get('v.enterpriseObject.CreatedDate'));
        component.set('v.subject',component.get('v.enterpriseObject.Short_Name__c')+' - '+component.get('v.enterpriseObject.Account__r.Name')+' - '+date.toDateString());  
        component.set('v.bodyText','Good day <br>I trust you are well. <br>Kindly see attached Consolidated Invoice '+component.get('v.enterpriseObject.Short_Name__c')+' for your attention. <br>This Consolidated Invoice is due on the [enter date] <br>I’ve also attached '+component.get('v.enterpriseObject.Account__r.Name')+'’s Invoices for this billing run. Please add the estimated payment dates once the attached invoices have been reviewed and processed.<br>Have a great day further.'); 
    },
    closeOnClick: function(component) {
        component.set('v.isShipmentComponentClose',true);
        var outerDiv = document.getElementById('divOuter');
        outerDiv.classList.add("backgroundImage");
        outerDiv.classList.remove("relPos");
        component.destroy();
    },
    sendEmail : function(component, event) {
        var that = this;
        var toAddresses = [];
        var ccAddresses = [];
        
        var contactsList = component.get('v.contactsList');
        var ccEmails = component.get('v.ccEmails');
        var subject = component.get('v.subject');
        var bodyText = component.get('v.bodyText');
        var eId = component.get('v.eId');
        
        //toAddresses.push(component.get('v.activeUser').Email)
        
        if(ccEmails){
            ccAddresses = ccEmails.split(",");
        }
        
        component.get('v.isCCMe') ? toAddresses.push(component.get('v.activeUser').Email) : '';
        
        for(let contact of contactsList){
            if(contact.selected){
                toAddresses.push(contact.Email);
            }
        }
        
        var action = component.get("c.sendEmailWithAttachements");
        
        action.setParams({
            'subject':subject,
            'bodyText':bodyText,
            'eId': eId,
            'toAddresses':toAddresses,
            'ccAddresses': ccAddresses
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS") {
                var result = response.getReturnValue(); if(result.status === "OK") {
                    
                    that.fireToast('Success !', 'success', 'Email Send Successfully!');
                } else {
                    
                    that.fireToast('Error !', 'error', result.msg);
                }
            }
            else if(state === "ERROR") {
                var errors = response.getError();
                if(errors) {
                    if(errors[0] && errors[0].message) {
                        
                        that.fireToast('Error !', 'error', errors[0].message);
                        
                    }
                } else {
                    
                    that.fireToast('Error !', 'error', "Unknown error");
                    
                }
            }
        });
        
        if(toAddresses.length > 0){
            
            if(typeof bodyText != 'undefined' && bodyText != ''){
                
                $A.enqueueAction(action);
                
            }else{
                
                that.fireToast('Error !', 'error', "Please enter body for the mail");
                
            }
            
        }else{
            
            that.fireToast('Error !', 'error', "Please select atleast one contact");
            
        }
        
    },
    fireToast: function(title, type, message) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": title,
            "type": type,
            "message": message
        });
        toastEvent.fire();
    }
})