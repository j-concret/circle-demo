({
    doInit: function(component, event, helper) {
        helper.getContactDetails(component);
    },
    selectAllContact : function(component, event, helper) {
        var isAllSelection = !component.get('v.isAllSelection');
        var contactsList = component.get('v.contactsList');
        for(var contact of contactsList){
            contact.selected = isAllSelection;
        }
        component.set('v.contactsList',contactsList);
    },
    doSend : function(component, event, helper) {
        helper.sendEmail(component, event);
    },
    naviageToRecord:function(component){
        
        var navService = component.find("navService");
        // Uses the pageReference definition in the init handler
        var pageReference = {
            "type": "standard__recordPage",
            "attributes": {
                "recordId": component.get('v.eId'),
                "objectApiName": "Enterprise_Billing__c",
                "actionName": "view"
            }
        };
        navService.navigate(pageReference);
        $A.get('e.force:refreshView').fire();
    }
})