/**
 * Created by Chibuye Kunda on 2019/01/16.
 */
({


    /** this function will handle picklist change event
     *  @param componentP this is the component we are updating
     *  @param eventP this is the event we are processing
     *  @param helperP is the helper object
     */
    handlePicklistEvent : function( componentP, eventP ){

        var picklist_value = eventP.getParam( "picklist_value" );                //get the parameter from event

        console.log( "Running" );

        //check if we dont have tecEX or client picklist values
        if( ( picklist_value !== "TecEx" ) && ( picklist_value !== "Client" ) )
            return;                 //terminate function

        //check if the picklist value is TecEx
        if( picklist_value === "TecEx" )
            componentP.set( "v.is_tecex", true );
        else
            componentP.set( "v.is_tecex", false );

    }//end of function definition


})