({
    updateInvoiceData :function(component, event, helper) {
        var that=this;
        var invoiceObjects = component.get('v.invoiceObjects');
        var invoiceList = [];
        for(let invoice of invoiceObjects){
            let invoiceJson = {};
            if(invoice.Invocing && !invoice.processStatus){
                invoiceJson.Invoice_Name__c=invoice.Invoice_Name__c;
                invoiceJson.Id = invoice.Id;
                invoiceJson.IOR_Fees__c = invoice.IOR_Fees__c;
                invoiceJson.EOR_Fees__c = invoice.EOR_Fees__c;
                invoiceJson.Bank_Fee__c = invoice.Bank_Fee__c;
                invoiceJson.Admin_Fees__c = invoice.Admin_Fees__c;
                invoiceJson.Customs_Handling_Fees__c = invoice.Customs_Handling_Fees__c;
                invoiceJson.Customs_Brokerage_Fees__c = invoice.Customs_Brokerage_Fees__c;
                invoiceJson.Customs_Clearance_Fees__c = invoice.Customs_Clearance_Fees__c;
                invoiceJson.Customs_License_In__c = invoice.Customs_License_In__c;
                invoiceJson.International_Freight_Fee__c = invoice.International_Freight_Fee__c;
                invoiceJson.Cash_Outlay_Fee__c = invoice.Cash_Outlay_Fee__c;
                invoiceJson.Liability_Cover_Fee__c = invoice.Liability_Cover_Fee__c;
                invoiceJson.Taxes_and_Duties__c = invoice.Taxes_and_Duties__c;
                invoiceJson.Recharge_Tax_and_Duty_Other__c = invoice.Recharge_Tax_and_Duty_Other__c;
                invoiceJson.Miscellaneous_Fee__c = invoice.Miscellaneous_Fee__c;
                invoiceJson.Miscellaneous_Fee_Name__c = invoice.Miscellaneous_Fee_Name__c;
                invoiceJson.Invoice_amount_USD__c = invoice.Invoice_amount_USD__c;
                invoiceJson.Enterprise_Billing__c = component.get('v.eId');
                invoiceJson.Shipment_Order__r = invoice.Shipment_Order__r;
                invoiceList.push(invoiceJson);
            }
        }
        var invoiceListChunks = that.chunk(invoiceList,10);
        console.log('invoiceListChunks');
        console.log(invoiceListChunks);
        var chunkIndex = 0;
        var successInvoices = [];
        var successShipments = [];
         if(invoiceListChunks.length > 0){
            for(let invoices of invoiceListChunks){
          var action = component.get("c.updateInvoices");
        action.setParams({
            'invoiceList': invoices
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS") {
                var result = response.getReturnValue(); 
                if(result.status === "OK") {
                    chunkIndex++;
                    var invoiceIds = Object.keys(result.successList);
                    var invoiceList = result.invoiceList;
                    for(let invoice of invoiceList){
                        if(invoice.Id != 'undefined'){
                        successInvoices.push(invoice);
                        successShipments.push(invoice.Id);
                        }
                    }
                    console.log('invoiceIds '+invoiceIds);
                    if(invoiceListChunks.length == chunkIndex){
                    component.set('v.isShipmentUpdateLoading',false);
                    component.set('v.isInvoicePDFLoading',true);
                   component.set('v.isInvoiceUpdateLoading',false);
                    for(let shipOrder of invoiceObjects){
                        if(shipOrder.Invocing && !shipOrder.processStatus){
                            shipOrder.Status = successShipments.includes(shipOrder.Id);
                            shipOrder.processStatus = true;
                        }
                    }
                    that.fireToast('Success !', 'success', 'Invoices Update Successfully !');
                    component.set('v.isProcessStatus',true);    
                    component.set('v.selectedInvoiceRecordCount',0);
                    component.set('v.invoiceObjects',invoiceObjects);
                    component.set('v.filteredInvoiceObjects',invoiceObjects);
                   // that.generateInvoicesPDFObjects(component,successInvoices);
                    }
                    //if()
              /*       var Statuses = result.successList;
                     var ShipmentListId = [];
                    for(let invoice of result.invoiceList){
                        if(invoice.Id != 'undefined'){
                        ShipmentListId.push(invoice.Shipment_Order__c);
                        }
                    }
                    component.set('v.selectedShipmentRecordCount',0);
                    that.generateInvoicesPDFObjects(component,component.get('v.enterpriseObject').Id,ShipmentListId);
                    */
                    //component.set('v.shipmentObjects', result.shipmentOrders );
                    //
                    //component.set('v.isShipmentApplied',true);
                    //component.set('v.isShipmentLoading',false); 
                    console.log(result);
                } else {
                   // console.log(result.msg);
                     that.fireToast('Error !', 'error', result.msg);
                     component.set('v.isShipmentUpdateLoading',false);
                     component.set('v.isInvoiceUpdateLoading',false);
                    //component.set('v.isShipmentLoading',false);
                }
            }
            else if(state === "ERROR") {
                var errors = response.getError();
                if(errors) {
                    if(errors[0] && errors[0].message) {
                        //console.log();
                        that.fireToast('Error !', 'error', errors[0].message);
                         component.set('v.isShipmentUpdateLoading',false);
                         component.set('v.isInvoiceUpdateLoading',false);
                      //  component.set('v.isShipmentLoading',false);
                        //that.fireToast('ERROR', 'error', errors[0].message);
                    }
                } else {
                    //console.log();
                    that.fireToast('Error !', 'error', "Unknown error");
                     component.set('v.isShipmentUpdateLoading',false);
                     component.set('v.isInvoiceUpdateLoading',false);
                   // component.set('v.isShipmentLoading',false);
                    //that.fireToast('ERROR', 'error', "Unknown error");
                }
            }
        });
            $A.enqueueAction(action);
            }
        }else{
            console.log('ERROR IN CHUNK');
             component.set('v.isShipmentUpdateLoading',false);
            component.set('v.isInvoiceUpdateLoading',false);
        }
    },
    fireToast: function(title, type, message) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": title,
            "type": type,
            "message": message
        });
        toastEvent.fire();
    },
    chunk: function(array, count) {
        if (count == null || count < 1) return [];
        var result = [];
        var i = 0, length = array.length;
        while (i < length) {
            result.push(Array.prototype.slice.call(array, i, i += count));
        }
        return result;
    }
})