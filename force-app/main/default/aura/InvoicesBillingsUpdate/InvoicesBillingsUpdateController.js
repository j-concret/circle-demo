({
    doInit: function(component, event, helper) {
        var filteredInvoiceObjects= component.get('v.filteredInvoiceObjects');
        component.set('v.invoiceObjects',filteredInvoiceObjects);
    },
    onFilterSO: function(component, event, helper) {
        var invoiceObjects = component.get('v.invoiceObjects');
        var filter = component.get('v.filter');
        var filteredData = [];
        if(filter){
           
                filteredData = invoiceObjects.filter(data => {
                  return data.Shipment_Order__r.Name ? data.Shipment_Order__r.Name.toLowerCase().includes(filter.toLowerCase()) : false;
              });
                component.set('v.filteredInvoiceObjects',filteredData);
          
        }else{
            component.set('v.filteredInvoiceObjects',invoiceObjects);
        }
    },
    updateInvoicesD: function(component, event, helper) { 
        if(component.get('v.eId')){
        if(component.get('v.selectedInvoiceRecordCount') > 0){
            component.set('v.isInvoiceUpdateLoading',true);
            helper.updateInvoiceData(component, event, helper);
        }else{
            helper.fireToast('Info !', 'info', 'Select Invoice to process');
           // component.set('v.isShipmentUpdateLoading',false); 
        }
        }else{
          helper.fireToast('Info !', 'info', 'Please Generate Billing Run First');  
        }

    },
	toggleSOFields: function(component,event,helper){
        component.set('v.isInvoiceVisible',!component.get('v.isInvoiceVisible'));
    },
    toggleTecExCharges: function(component,event,helper){
        component.set('v.isTecExChargesVisible',!component.get('v.isTecExChargesVisible'));
    },
    toggleOnCharges: function(component,event,helper){
        component.set('v.isOnChargesVisible',!component.get('v.isOnChargesVisible'));
    },
    toggleVAService: function(component,event,helper){
        component.set('v.isVAServicesVisible',!component.get('v.isVAServicesVisible'));
    },
    toggleTax: function(component,event,helper){
        component.set('v.isTaxVisible',!component.get('v.isTaxVisible'));
    },
    toggleOther: function(component,event,helper){
        component.set('v.isOtherVisible',!component.get('v.isOtherVisible'));
    }
})