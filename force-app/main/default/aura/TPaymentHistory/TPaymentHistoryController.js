({
	doInit: function( component, event, helper ){
        var action = component.get("c.PaymentDetails");
       
        action.setCallback(this, function(response) {          
            var state = response.getState();
            if (state === 'SUCCESS'){
                
                var data = response.getReturnValue();
                var n = data.length;
                var URL = data.substring(4, n);
                var check = data.substring(0, 4);
                if(check=='PASS'){
                	component.set('v.control',true);
                	component.set('v.iframeUrl2',URL);
                	console.log(data);
                	console.log(URL);
                }
                 else{
                	component.set('v.control',false);
                	
                	console.log(data);
                	console.log(check);
                }
                
               }
            else{
                component.set('v.control',false);
              //  alert("We are in fail block");
            }
            
        });
        $A.enqueueAction(action);
        }
})