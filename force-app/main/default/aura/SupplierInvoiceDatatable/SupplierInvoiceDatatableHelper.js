({
	fetchData: function (cmp,event,helper) {
        var SupplierInvoiceID1 = cmp.get("v.soidFromFlow"); // supplierInvoiceID
        
           
        
        
        var action = cmp.get("c.getCostings");
       action.setParams({
        
         SupplierInvoiceID1 : cmp.get("v.soidFromFlow1"),
           
        });
        
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var data = response.getReturnValue();
                cmp.set('v.data',data);
                  
            }
            // error handling when state is "INCOMPLETE" or "ERROR"
        });
        $A.enqueueAction(action);
    }
})