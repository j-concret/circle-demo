({
	init: function (cmp, event, helper) {
        cmp.set('v.columns', [
            
            {label: 'Costing Name', fieldName: 'Name', editable:false, type: 'string'},
            {label: 'Cost Category', fieldName: 'Cost_Category__c', editable:false, type: 'string'},
            {label: 'Invoice Amount ', fieldName: 'Invoice_amount_local_currency__c', type: 'number', editable: 'true'}
        ]);    
        helper.fetchData(cmp,event, helper);
    },
     handleSaveEdition: function (cmp, event, helper) {
        var draftValues = event.getParam('draftValues');
        console.log("costings to be update",draftValues);
        var action = cmp.get("c.updateCostings");
        action.setParams({"editedAccountList" : draftValues});
        action.setCallback(this, function(response) {
            
            var state = response.getState();
            if (state === 'SUCCESS'){
            
       //    alert("Shipment Order Packages are updated ");
           $A.get('e.force:refreshView').fire();
         
            }
            else{
                alert("We are in fail block");
            }
            
        });
        $A.enqueueAction(action);
        
    },
    /*page refresh after data save*/    
    isRefreshed: function(component, event, helper) {
       location.reload();
      
       //    $A.get('e.force:refreshView').fire(); 
    },
     
    
    
    
    
})