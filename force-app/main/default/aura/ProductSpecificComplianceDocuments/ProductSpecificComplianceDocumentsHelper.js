({
  doInitHlpr: function(component, helper) {
    component.set('v.showInnerSpinner', true);
    var pageSize = component.get('v.pageSize');
    var params = {
      shipmentOrderId: component.get('v.recordId'),
      offset: parseInt(component.get('v.currentPage')) - 1,
      pageSize: pageSize
    };
    this.makeServerCall(component, 'getAllPartsDetail', params, function(result) {
      if(result.status === 'OK') {
        component.set('v.worldwide', result.worldwide);
        component.set('v.complianceFieldRules', result.complianceFieldRules);
        component.set('v.catWithSubCatValues', result.catWithSubCatValues);
        component.set('v.wirelessPickValues', result.wirelessPickValues);
        component.set('v.encryptionPickValues', result.encryptionPickValues);
        component.set('v.matchedHSStatusPickValues', result.matchedHSStatusPickValues);
        component.set('v.productCategoryValues', Object.keys(result.catWithSubCatValues));
        component.set('v.totalPages', Math.ceil(parseInt(result.totalRecords) / pageSize));
        component.set('v.parts', result.parts);
        helper.checkIsComplianceReady(component);
      }
      else {
        helper.showToast('error', 'Error Occured', result.msg);
      }
      component.set('v.showInnerSpinner', false);
    });
  },
  getNextPageData: function(component, helper) {
    component.set('v.showInnerSpinner', true);
    var pageSize = component.get('v.pageSize');
    var params = {
      shipmentOrderId: component.get('v.recordId'),
      offset: parseInt(component.get('v.currentPage')) - 1,
      pageSize: pageSize
    };
    this.makeServerCall(component, 'getPartRecords', params, function(result) {
      if(result.status === 'OK') {
        component.set('v.parts', result.parts);
        helper.checkIsComplianceReady(component);
      }
      else {
        helper.showToast('error', 'Error Occured', result.msg);
      }
      component.set('v.showInnerSpinner', false);
    });
  },
  checkIsComplianceReady: function(component) {
    var parts = component.get('v.parts');
    var isComplianceReady = false;

    partsLoop:
    for(var index in parts) {
      var requiredDocs = parts[index].requiredDocs;
      for(var i in requiredDocs) {
        if(!(requiredDocs[i].isNotRequired || (requiredDocs[i].attachment && requiredDocs[i].attachment.length > 0))) {
          isComplianceReady = true;
          break partsLoop;
        }
      }
    }
    component.set('v.isComplianceReady', isComplianceReady);
  },
  createAttachmentType: function(component, comDoc, uploadedFiles) {
    var comDocData = comDoc.fields;
    var indexes = component.get('v.indexesData.indexes');
    var comType = comDocData.Compliance_type__c.value;
    var lNumber = comDocData.Certification_number__c.value;
    var fileLink = comDocData.Link__c.value;

    var parts = component.get('v.parts');
    var cmpWrap = JSON.parse(JSON.stringify(parts[indexes[0]].requiredDocs[indexes[1]]));
    cmpWrap.cId = comDoc.id;
    cmpWrap.fileDocType = 'pdf';
    if(comDocData.Compliance_status__c && comDocData.Compliance_status__c.value)
      cmpWrap.status = comDocData.Compliance_status__c.value;
    cmpWrap.hasAttachment = (cmpWrap.cId) && !cmpWrap.isNotRequired && (cmpWrap.nature == 'WW_WW' || cmpWrap.nature == 'Specific_Specific');
    cmpWrap.licenseNumber = '';
    cmpWrap.fileLink = '';
    //cmpWrap.availableIcon = 'action:approval';
    if(!cmpWrap.attachment) cmpWrap.attachment = [];

    if(comType == 'Certification Number') {
      cmpWrap.fileDocType = '';
      if(lNumber) {
        cmpWrap.licenseNumber = lNumber;
      }
    }
    else if(comType == 'Link') {
      cmpWrap.fileLink = fileLink;
    } else if(uploadedFiles) {
      for(var i = 0; i < uploadedFiles.length; i++)
        cmpWrap.attachment.push({fileName: uploadedFiles[i].name, id: uploadedFiles[i].documentId});
    }
    for(var index in parts) {
      var part = parts[index];
      if(part.productId && part.productId == parts[indexes[0]].productId) {
        part.requiredDocs[indexes[1]] = cmpWrap;
        part.requiredDocs[indexes[1]].availableIcon = (cmpWrap.isNotRequired ? 'action:reject' : (cmpWrap.cId) ? 'action:approval' : 'action:close');
        //part.requiredDocs[indexes[1]].availableIcon = (cmpWrap.isNotRequired ? 'action:reject' : (parts[indexes[0]].requiredDocs[indexes[1]].attachment.length > 0 ? 'action:approval' : 'action:close'));
      }
    }
    component.set('v.parts', parts);
  },
  createCountryComplianceRecord: function(component, helper, productId, comDocData, uploadedFiles) {
    component.set('v.showInnerSpinner', true);
    var params = {
      comDocId: comDocData.id,
      productId: productId
    };
    this.makeServerCall(component, 'createCountryCompliance', params, function(result) {
        helper.createAttachmentType(component, comDocData, uploadedFiles);
        helper.showToast('success', 'Success', 'Successfully Created');
        component.set('v.comDocRequired', false);
        component.set('v.complianceDoc', null);
        component.set('v.isNewCompliance', false);
        component.set('v.showInnerSpinner', false);
      },
      function(error) {
        helper.showToast('error', 'Error Occured', error);
        component.set('v.comDocRequired', false);
        component.set('v.complianceDoc', null);
        component.set('v.isNewCompliance', false);
        component.set('v.showInnerSpinner', false);
    });
  },
  updateIsRequiredChangesHlpr: function(component, helper) {
    var natureUse = component.get('v.natureUse');
    component.set('v.showInnerSpinner', true);
    var parts = component.get('v.parts');
    var productNotRequiredDocs = {};
    var products = [];

    for(var index in parts) {
      var part = parts[index];
      if(part.productId && products.includes(part.productId)) continue;

      if(part.Product__r && part.Product__r.Product_Documents_Not_Required__c) productNotRequiredDocs[part.productId] = part.Product__r.Product_Documents_Not_Required__c.split(';');

      for(var jIndex in part.requiredDocs) {
        var doc = part.requiredDocs[jIndex];
        if(!doc.nature || doc.nature != natureUse.ps) continue;
        if(doc.isNotRequired) {
          if(part.productId && !products.includes(part.productId) && doc.nature != natureUse.wws) {
            if(productNotRequiredDocs[part.productId]) {
              var docs = productNotRequiredDocs[part.productId];
              if(!docs.includes(doc.name))
                productNotRequiredDocs[part.productId].push(doc.name);
            }
            else {
              productNotRequiredDocs[part.productId] = [doc.name];
            }
          }
        } else {
           if(part.productId && !products.includes(part.productId) && productNotRequiredDocs[part.productId]) {
            var docs = productNotRequiredDocs[part.productId];
            if(docs.indexOf(doc.name) > -1){
              docs.splice(docs.indexOf(doc.name),1);
              productNotRequiredDocs[part.productId] = JSON.parse(JSON.stringify(docs));
            }
          }
        }
      }
      if(part.productId) products.push(part.productId);
    }

    this.makeServerCall(component, 'updateProductNotRequiredField', {productNotRequiredDocs}, function(result) {
      helper.showToast('success', 'Success','Parts updated successfully!');
      component.set('v.showUpdateButton', false);
      component.set('v.showInnerSpinner', false);
    },
    function(error) {
      component.set('v.showInnerSpinner', false);
      helper.showToast('error', 'Error Occured', error);
    });
  },
  calculateNewComplianceData: function(component, changedComplianceName) {
    var notRequired = component.get('v.indexesData.isChecked');
    var natureUse = component.get('v.natureUse');
    var indexes = component.get('v.indexesData.indexes');
    var parts = component.get('v.parts');
    var shipmentOrder = component.get('v.shipmentOrder');
    var defaultComplianceData = {
      complianceFieldRule: {},
      isSaveDisabled: false
    };
    var complianceFieldRules = component.get('v.complianceFieldRules');

    if(parts[indexes[0]].requiredDocs[indexes[1]].cId)
      defaultComplianceData.cId = parts[indexes[0]].requiredDocs[indexes[1]].cId;

    defaultComplianceData.name = (changedComplianceName ? changedComplianceName : parts[indexes[0]].requiredDocs[indexes[1]].name);
    defaultComplianceData.complianceFieldRule = complianceFieldRules[defaultComplianceData.name];

    defaultComplianceData.sub_status = null;
    defaultComplianceData.Supplier__c = shipmentOrder.SupplierlU__c;

    if(!notRequired && defaultComplianceData.complianceFieldRule.Nature_Use__c && defaultComplianceData.complianceFieldRule.Nature_Use__c.includes('WW'))
      defaultComplianceData.country = component.get('v.worldwide');
    else if(notRequired && defaultComplianceData.complianceFieldRule.Nature_Use__c && defaultComplianceData.complianceFieldRule.Nature_Use__c == 'WW_WW'){
      defaultComplianceData.country = component.get('v.worldwide');
    }
    else if(shipmentOrder.CPA_v2_0__r && shipmentOrder.CPA_v2_0__r.Country__c) {
      defaultComplianceData.country = shipmentOrder.CPA_v2_0__r.Country__c;
    }
    defaultComplianceData.showDelete = (typeof notRequired !== 'undefined' && !notRequired && defaultComplianceData.complianceFieldRule.Nature_Use__c && defaultComplianceData.complianceFieldRule.Nature_Use__c == natureUse.wws);
    defaultComplianceData.status = notRequired ? 'Not Required' : 'Available';
    component.set('v.defaultComplianceData', defaultComplianceData);
  },
  redirectToObject: function(cmp, event) {
    var selectedItem = event.currentTarget;// this will give current element
    var recId = selectedItem.dataset.redirectid;
    var objectApi = selectedItem.dataset.objectApi;
    var navService = cmp.find('navService');
    var pageReference = {
      type: 'standard__recordPage',
      attributes: {
        'recordId': recId,
        'objectApiName': objectApi,
        'actionName': 'view'
      }
    };
    event.preventDefault();
    navService.navigate(pageReference);
  },
  insertCDRecord: function(component, helper,isUpsert) {
    var indexesData = component.get('v.indexesData');
    var indexes = indexesData.indexes;
    var parts = component.get('v.parts');
    var productId = parts[indexes[0]].productId;
    var defaultComplianceData = component.get('v.defaultComplianceData');

    var cdRecord = {
      SObjectType: 'Compliance_Document__c',
      Compliance_Document_Name__c: defaultComplianceData.name,
      Country__c: defaultComplianceData.country,
      Compliance_status__c: defaultComplianceData.status,
      Compliance_type__c: defaultComplianceData.complianceFieldRule.Compliance_Type__c
    };

    if(defaultComplianceData.complianceFieldRule.General_Comments_Required__c)
      cdRecord['General_Comments__c'] = 'Not Required';

    if(defaultComplianceData.complianceFieldRule.Modal_Number_Required__c)
      cdRecord['Model_Number__c'] = 'Not Required';

    if(defaultComplianceData.complianceFieldRule.Factory_Specific_Required__c)
      cdRecord['Factory_name__c'] = 'Not Required';

    if(defaultComplianceData.complianceFieldRule.Date_of_issue_Required__c)
      cdRecord['Date_of_issue__c'] = new Date();

    if(defaultComplianceData.complianceFieldRule.Expiry_Date_Visible__c)
      cdRecord['Expiry__c'] = new Date();

    if(defaultComplianceData.complianceFieldRule.Compliance_Type__c == 'Link')
      cdRecord['Link__c'] = 'Not Required';

    if(defaultComplianceData.complianceFieldRule.Compliance_Type__c == 'Certification Number')
      cdRecord['Certification_number__c'] == 'Not Required';

    if(isUpsert && defaultComplianceData.cId)
      cdRecord['Id'] = defaultComplianceData.cId;

    var params = {
      doc : cdRecord,
      productId:productId
    };
    component.set('v.showInnerSpinner', true);

    this.makeServerCall(component, 'upsertComplianceDoc', params, function(data) {

        helper.createAttachmentType(component, JSON.parse(data),null);
        component.set('v.showInnerSpinner', false);
      },
       function(error){
        helper.showToast('error', 'Error Occured', error);
        component.set('v.showInnerSpinner', false);
      });

  },
  updatePartsRequiredField: function(component, helper, isChecked) {
    var natureUse = component.get('v.natureUse');
    var indexesData = component.get('v.indexesData');
    var indexes = indexesData.indexes;
    var parts = component.get('v.parts');
    var doc = parts[indexes[0]].requiredDocs[indexes[1]];

    var complianceFieldRules = component.get('v.complianceFieldRules');
    var rule = complianceFieldRules[doc.name];

    if(rule.Nature_Use__c && rule.Nature_Use__c == natureUse.ps) {
      parts[indexes[0]].requiredDocs[indexes[1]].availableIcon = (isChecked ? 'action:reject' : (parts[indexes[0]].requiredDocs[indexes[1]].attachment.length > 0 ? 'action:approval' : 'action:close'));
      component.set('v.parts', parts);
      component.set('v.showUpdateButton', true);
    }
    else{
      indexesData['nature'] = rule.Nature_Use__c;
      indexesData['isChecked'] = isChecked;
      component.set('v.indexesData', indexesData);
      helper.calculateNewComplianceData(component, null);
      if(isChecked){
        helper.insertCDRecord(component, helper,doc.status == 'Document Required, but Unavailable');
      }
      else{
        helper.deleteComplianceDoc(component, helper);
      }
    }
  },
  handleUploadAndCCCreation: function(component, helper, uploadedFiles) {
    var indexes = component.get('v.indexesData.indexes');
    var comDocData = component.get('v.complianceDoc');
    var parts = component.get('v.parts');
    var productId = parts[indexes[0]].productId;
    if(!parts[indexes[0]].requiredDocs[indexes[1]].cId)
      helper.createCountryComplianceRecord(component, helper, productId, comDocData, uploadedFiles);
    else {
      helper.createAttachmentType(component, comDocData, uploadedFiles);
      component.set('v.comDocRequired', false);
      component.set('v.complianceDoc', null);
      component.set('v.isNewCompliance', false);
      component.set('v.showInnerSpinner', false);
    }
  },
  deleteComplianceDoc:function(component,helper){
    component.set('v.isNewCompliance', false);
    var indexesData = component.get('v.indexesData');
    var indexes = indexesData.indexes;
    var parts = component.get('v.parts');
    var productId = parts[indexes[0]].productId;
    var doc = parts[indexes[0]].requiredDocs[indexes[1]];
    if(!doc.cId) return;

    component.set('v.showInnerSpinner', true);
    var cdRecord = {
      SObjectType: 'Compliance_Document__c',
      Compliance_Document_Name__c: doc.name,
      Id : doc.cId
    };

    var params = {
      doc : cdRecord,
      productId:productId
    };

    this.makeServerCall(component, 'deleteComplianceDoc', params, function(result) {
      if(result.status === 'OK') {
        if(result.doc){
          doc = result.doc;
        }
        else{
          doc.cId = null;
          doc.attachment = [];
          doc.isNotRequired = false;
          doc.availableIcon = 'action:close';
        }

        for(var index in parts) {
          var part = parts[index];
          if(part.productId && part.productId == parts[indexes[0]].productId) {
            part.requiredDocs[indexes[1]] = doc;
          }
        }
        component.set('v.parts', parts);
      }
      else {
        helper.showToast('error', 'Error Occured', result.msg);
      }
      component.set('v.showInnerSpinner', false);
    });

  }
})