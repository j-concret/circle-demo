({
  doInitCtrl: function(component, event, helper) {
    var shipmentOrder = component.get('v.shipmentOrder');
    var reqDocs = [];
    if(shipmentOrder.CPA_v2_0__r.CPA_Required_Documents_Product_Specific__c){
      let allRequiredDocsName = shipmentOrder.CPA_v2_0__r.CPA_Required_Documents_Product_Specific__c;
      if(allRequiredDocsName)
        reqDocs = allRequiredDocsName.split(';');
    }

    if(shipmentOrder.CPA_v2_0__r.CPA_Required_Documents_Part_Specific__c){
      let allpartDocsName = shipmentOrder.CPA_v2_0__r.CPA_Required_Documents_Part_Specific__c;
      if(allpartDocsName) {
        let reqPartDocs = allpartDocsName.split(';');
        reqDocs.push(...reqPartDocs);
      }
    }

    component.set('v.requiredDocs', reqDocs);
    helper.doInitHlpr(component, helper);
  },
  redirectToRelated: function(component, event, helper) {
    helper.redirectToObject(component, event);
  },
  expandRowCtrl: function(component, event, helper) {
    var index = event.getSource().get('v.name');
    var parts = component.get('v.parts');
    parts[index].isExpanded = !parts[index].isExpanded;
    component.set('v.parts', parts);
  },
  handleIsNotRequiredChangeCtrl: function(component, event, helper) {
    var indexes = event.getSource().get('v.name').split('___');
    var indexesData = {'indexes': indexes};
    component.set('v.indexesData', indexesData);
    helper.updatePartsRequiredField(component, helper, event.getSource().get('v.checked'));
  },
  updateIsRequiredChangesCtrl: function(component, event, helper) {
    helper.updateIsRequiredChangesHlpr(component, helper);
  },
  handleComplianceNameChange: function(component, event, helper) {
    helper.calculateNewComplianceData(component, event.getSource().get('v.value'));
  },
  closePartUploadCtrl: function(component, event, helper) {
    component.set('v.showPartUpload', false);
  },
  handlePartUploadFinished: function(component, event, helper) {
    var uploadedFiles = event.getParam('files');
    var indexes = component.get('v.indexesData.indexes');
    var parts = component.get('v.parts');
    var atts = [];
    for(var i = 0; i < uploadedFiles.length; i++)
      atts.push({fileName: uploadedFiles[i].name, id: uploadedFiles[i].documentId});

    parts[indexes[0]].requiredDocs[indexes[1]].attachment = atts;
    parts[indexes[0]].requiredDocs[indexes[1]].availableIcon = 'action:approval';
    component.set('v.parts', parts);
    component.set('v.showPartUpload', false);
  },
  showPartUploadModalCtrl: function(component, event, helper) {
    var indexes = event.target.name.split('___');
    var indexesData = {'indexes': indexes};
    component.set('v.indexesData', indexesData);
    var parts = component.get('v.parts');
    component.set('v.selectedPartId', parts[indexes[0]].Id);
    component.set('v.showPartUpload', true);
  },
  showNewComplianceModalCtrl: function(component, event, helper) {
    var indexes = event.target.name.split('___');
    var indexesData = {'indexes': indexes};
    component.set('v.indexesData', indexesData);
    helper.calculateNewComplianceData(component, null);
    component.set('v.isNewCompliance', true);
  },
  downloadPDFCtrl: function(component, event, helper) {
    var indexes = event.target.name.split('___');
    var part = component.get('v.parts')[indexes[0]];

    if(part.requiredDocs[indexes[1]].attachment[indexes[2]].fileName) {
      var hiddenElement = document.createElement('a');

      if(part.requiredDocs[indexes[1]].attachment[indexes[2]].id) {
        hiddenElement.href = '/sfc/servlet.shepherd/document/download/' + part.requiredDocs[indexes[1]].attachment[indexes[2]].id;
        hiddenElement.target = '_self';
        hiddenElement.download = part.requiredDocs[indexes[1]].attachment[indexes[2]].fileName;
        document.body.appendChild(hiddenElement);
        hiddenElement.click();
      }
    }
    else {
      helper.showToast('error', 'Error Occured', 'Nothing to donwload!');
    }
  },
  downloadZipCtrl: function(component, event, helper) {
    var indexes = event.getSource().get('v.name').split('___');
    var part = component.get('v.parts')[indexes[0]];
    var allAttachments = part.requiredDocs[indexes[1]].attachment;
    var isContainsId = false;

    var hiddenElement = document.createElement('a');
    hiddenElement.href = '/sfc/servlet.shepherd/document/download/';
    for(var index in allAttachments) {
      if(allAttachments[index].id) {
        isContainsId = true;
        hiddenElement.href += '/' + allAttachments[index].id;
      }
    }
    if(!isContainsId) return;
    hiddenElement.target = '_self';
    hiddenElement.download = 'AllComplianceDocuments';
    document.body.appendChild(hiddenElement);
    hiddenElement.click();
  },
  handleIsComplianceReadyCtrl: function(component, event, helper) {
    if(!component.get('v.isComplianceReady')) {
      helper.checkIsComplianceReady(component);
    }
  },
  closeNewComplianceCtrl: function(component, event, helper) {
    var data = component.get('v.indexesData');
    var natureUse = component.get('v.natureUse');
    var indexes = data.indexes;
    var parts = component.get('v.parts');
    if(data.nature && data.nature == natureUse.wws && typeof data.isChecked !== 'undefined') {
      parts[indexes[0]].requiredDocs[indexes[1]].isNotRequired = !(data.isChecked);
      component.set('v.indexesData', {});
      component.set('v.parts', parts);
    }
    component.set('v.isNewCompliance', false);
  },
  handleUploadFinished: function(component, event, helper) {
    var uploadedFiles = event.getParam('files');
    helper.handleUploadAndCCCreation(component, helper, uploadedFiles);
  },
  handleComplianceDocSuccessCtrl: function(component, event, helper) {
    var indexesData = component.get('v.indexesData');
    component.set('v.complianceDoc', event.getParams().response);
    if(event.getParams().response.fields.Country__r.value.fields.Name.value) {
      component.set('v.countryName', event.getParams().response.fields.Country__r.value.fields.Name.value);
    }
    var defaultComplianceData = component.get('v.defaultComplianceData');
    if((!defaultComplianceData.sub_status || defaultComplianceData.sub_status == 'undefined' || defaultComplianceData.sub_status =='' ) && defaultComplianceData && defaultComplianceData.complianceFieldRule.Compliance_Type__c == 'Document' && !indexesData.isChecked){
      //typeof indexesData.isChecked == "undefined") {
      component.set('v.comDocRequired', true);
      return;
    }
    helper.handleUploadAndCCCreation(component, helper, null);
  },
  handleFirstCtrl: function(component, event, helper) {
    component.set('v.currentPage', 1);
    helper.getNextPageData(component,helper);
  },
  handlePreviousCtrl: function(component, event, helper) {
    var currentPage = component.get('v.currentPage') - 1;
    if(currentPage <= 0) {
      currentPage = 1;
    }
    component.set('v.currentPage', currentPage);
    helper.getNextPageData(component,helper);
  },
  handleNextCtrl: function(component, event, helper) {
    var currentPage = component.get('v.currentPage') + 1;
    var totalPages = component.get('v.totalPages');

    if(currentPage > totalPages) {
      currentPage = totalPages;
    }
    component.set('v.currentPage', currentPage);
    helper.getNextPageData(component,helper);
  },
  handleLastCtrl: function(component, event, helper) {
    component.set('v.currentPage', component.get('v.totalPages'));
    helper.getNextPageData(component,helper);
  },
  deleteAttachedDoc : function(component, event, helper) {
    helper.deleteComplianceDoc(component,helper);
  },
  onSubStatusChange: function(cmp,event,helper){
    var defaultComplianceData = cmp.get('v.defaultComplianceData');
    var shipmentOrder = cmp.get('v.shipmentOrder');
    if(!defaultComplianceData.sub_status || defaultComplianceData.sub_status == 'undefined' || defaultComplianceData.sub_status ==''){
      defaultComplianceData.Supplier__c = null;
    }else{
      defaultComplianceData.Supplier__c = shipmentOrder.SupplierlU__c;
    }
    cmp.set('v.defaultComplianceData', defaultComplianceData);
  }
})