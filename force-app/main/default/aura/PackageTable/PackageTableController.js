/**
 * Created by Chibuye Kunda on 2018/12/31.
 */
({

    /** this function will be called to initialise our component
     * @param componentP this is the component we are updating
     * @param eventP this is the event that occured
     * @param helperP this is the helper object
     */
    doInit : function( componentP, eventP, helperP ){

        componentP.set( "v.table_headings",
                        [ "#","How many packages?", "Weight Unit", "Actual Weight",
                          "Dimension Unit", "Length", "Breadth", "Height" ] );

       // helperP.createTableRow( componentP, eventP );

    },//end of function definition





    /** this function will add a new row to our grid
     *  @param componentP is the component we are updating
     *  @param eventP is the event we are processing
     *  @param helperP is the helper object
     */
    addNewRow : function( componentP, eventP, helperP ){

        helperP.createTableRow( componentP, eventP );

        var package_list = componentP.get( "v.shipment_order_package_list" );

        componentP.set( "v.object_list_string", helperP.createObjectString( package_list ) );

    },//end of function definition





    /** this function will remove a row from the table
     *  @param componentP is the component we are updating
     *  @param eventP is the event we are processing
     *  @param helperP is the helper object
     */
    deleteRow : function( componentP, eventP, helperP ){

        var index = eventP.getParam( "index" );         //get the index
        var rows_list = componentP.get( "v.shipment_order_package_list" );

        rows_list.splice( index, 1 );           //remove the value

        componentP.set( "v.shipment_order_package_list", rows_list );
        componentP.set( "v.object_list_string", helperP.createObjectString( componentP.get( "v.shipment_order_package_list" ) ) );

    },//end of function definition

     // For Custom Footer
    handleClick : function(component, event, helper) {
        		console.log('entering handleclick');
        		component.set("v.fire", true);
              	var destType = component.get("v.destinationType");
              	if (destType == 'url') { 
                    console.log('destType is url');
                    var urlEvent = $A.get("e.force:navigateToURL");
             		console.log('destination is url');
    				var destUrl = component.get("v.destinationURL");
             		console.log('url is '+destUrl)	
          			if( (typeof sforce != 'undefined') && (sforce.one != null) ) {
						//handle lightning experience on desktop
						console.log("running navigation for lightning desktop");
                		urlEvent.setParams({
            				"url": destUrl
        	 			});      
                        console.log("Firing urlEvent: "+urlEvent)
                    //appears to be broken
            		urlEvent.fire();
        			}
    				else {
            			var device = $A.get("$Browser.formFactor");
            			if (device=="DESKTOP") {
             		   		console.log("running navigation for classic desktop");
            		    	window.location = destUrl;
           				}
					}
                }
            	else {
                    if (destType == 'record') {
                        var urlEvent = $A.get("e.force:navigateToSObject");
                        console.log('destination is record');
                        var destObject = component.get("v.targetRecordId");
                        console.log('recordId is:' +destObject)
                        urlEvent.setParams({
                            "recordId": destObject,
                            "slideDevName": "related",
                            "isredirect": true
                        });
                        console.log("running urlEvent for Record ID")
                        if( (typeof sforce != 'undefined') && (sforce.one != null) ) {
                            //handle lightning experience on desktop
                            console.log("running navigation for lightning desktop");
                            //appears to be broken
                            urlEvent.fire();
                        }
                        else {
                            console.log("running navigation for classic")
                            window.location = '/'+destObject; 
                        }
                    }
                    
                    else {
                        if (destType == 'standard') {
                            console.log('entering standard');
                            var navigationGoal = component.get("v.navigationType");
        
                            //component.set("v.fire", true);
                            var navigate = component.get('v.navigateFlow');
                            
                            if (navigationGoal.toLowerCase() == 'finish') {
                              navigate("FINISH");  
                            }
                            
                            if (navigationGoal.toLowerCase() == 'next') {
                                
                            var Option = component.get("v.value");
                            var Charweight = component.get("v.chargeable_weight_estimate");
                            var listSize = component.get("v.shipment_order_package_list");
                                
                            if(Option == 'No' ){
                            
                           	var allValid = component.find('fieldId').reduce(function (validSoFar, inputCmp)
                            {
           					inputCmp.showHelpMessageIfInvalid();
            				 return validSoFar && !inputCmp.get('v.validity').valueMissing;
            				
        					}, true);
                                
        						if (allValid && Charweight > 0) {
            								//	alert('All form entries look valid. Ready to submit!');
            									navigate("NEXT");
        										} else {
            								//	alert('Please enter below required fields');
                                              alertify.alert('', 'Chargeable Weight must more than zero');      
        												}
    
                                }                               
                             
                                else if(Option == 'Yes' && listSize == 0) {
                                    alertify.alert('', 'Please enter at least one package'); 
                                }
                                    else{    navigate("NEXT");
                                      }
                            
                             
                            }  
                            if (navigationGoal.toLowerCase() == 'back') {
                              navigate("BACK");  
                            }  
                        }
                    }
            	}
    }  
   // End of Custom Footer 	


    
    
    
    
    
    
    
    
    
    
})