({
	doInitHlpr: function(component, helper) {
        component.set("v.showSpinner", true);
        var params = {
            shipmentId: component.get("v.recordId")
        };
        this.makeServerCall(component, "getFinanceData", params, function(result) {
            if(result.status === 'OK') {
                if(result.data.length> 0){
                    if(result.data[0].Customs_Clearance_Documents__r){
                        component.set("v.CustomsClearance_Documents_ID",result.data[0].Customs_Clearance_Documents__r[0].Id);        
                    }
                }
                
                
            }
            else {
                helper.showToast("error", "Error Occured Helper", result.msg);
            }
            component.set("v.showSpinner", false);
        });
    },
    activeSections: function(component, event){
                var openSections = component.get("v.selectedTab");
        if(openSections ==='Invoicing Parameters'){
            var editFormGeneralInformation = component.find("editFormInvoicingParameter");
                                        $A.util.toggleClass(editFormGeneralInformation, "slds-hide");
                                        var viewFormGeneralInformation = component.find("viewFormInvoicingParameter");
                                        $A.util.toggleClass(viewFormGeneralInformation, "slds-hide");
        }else if(openSections ==='Finance Summary'){
            var editFormFinanceSummary = component.find("editFormFinanceSummary");
                                        $A.util.toggleClass(editFormFinanceSummary, "slds-hide");
                                        var viewFormFinanceSummary = component.find("viewFormFinanceSummary");
                                        $A.util.toggleClass(viewFormFinanceSummary, "slds-hide");
        }
                                        
    }
})