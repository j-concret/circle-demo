({
    doInitCtrl: function(component, event, helper) {
        if(component.get("v.recordId")) {
            helper.doInitHlpr(component, helper);
        }
        else {
            helper.showToast("error", "Error Occured", "Invalid record!");
        }
    },
    
    editInvoicingParameter : function(component, event, helper) {helper.activeSections(component)},
    
    showRequiredFields : function(component, event, helper) {
        
    },
    onRecordSubmit  : function(component, event, helper) {
        component.set("v.showSpinner", true);
    },
    //OnSuccess
    handleSuccess : function(component, event, helper) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({ "title": "Success!",
                              "message": "Saved.",
                              "type": "success"});
        toastEvent.fire();
        helper.activeSections(component);
        component.set("v.showSpinner", false);
    },
    //OnError
    onError : function(component, event, helper) {
        
        var toastEvent2 = $A.get("e.force:showToast");
        toastEvent2.setParams({
            "title": "Error!",
            "message": "Error."
        });
        toastEvent2.fire();
        component.set("v.showSpinner", false);
    },
    handleCancel : function(component, event, helper) {
        helper.activeSections(component);
        event.preventDefault();
    },
    
    //Navigate to CCD capture
    navigateCCDCaptureComp : function(component, event, helper) {
        var navigateEvent = $A.get("e.force:navigateToComponent");
        navigateEvent.setParams({
            componentDef: "c:CCDCapture",
            componentAttributes :{customsClearanceDocumentId : component.get("v.CustomsClearance_Documents_ID"),
                                  newSOPage : true}
        });
        navigateEvent.fire();
    },
    
    //Shows Generate invoice modal
    navigateGenerateInvoice : function(component, event, helper) {
        component.set("v.genInvoice", true);
    },
    
    //Shows shipment order Chatter
    handleShowChatterSectionCtrl: function(component, event, helper) {
        component.set("v.showChatterSection", !component.get("v.showChatterSection"));
    },
    recordUpdated : function(component, event, helper) {
      var changeType = event.getParams().changeType;
      if (changeType === "CHANGED") {
        component.find("shipmentOrderRecord").reloadRecord();}
    },
    refreshCmp: function(component, event, helper) {
      component.set('v.showChatterSection',!component.get('v.showChatterSection'));
      component.set('v.showChatterSection',!component.get('v.showChatterSection'));
    }

})