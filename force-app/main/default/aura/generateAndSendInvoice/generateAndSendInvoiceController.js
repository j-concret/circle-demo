({
    doInit : function(component, event, helper) {
        
        component.set("v.showLoadingSpinner", true);
        
        let contactPromise = Promise.resolve(helper.promiseGetRelatedContacts(component));
        contactPromise.then(
            $A.getCallback(function (result) {
                component.set("v.showLoadingSpinner", false);
               helper.emailBodyCreation(component,result);
                
            }),
            $A.getCallback(function (status) {
                component.set("v.showLoadingSpinner", false);
                component.find('notifLib').showToast({
                    "variant": "error",
                    "message": "Unable to add Statement or contacts! "+status.message
                });
            })
        ).catch(function (error) {
            component.set("v.showLoadingSpinner", false);
            $A.reportError("Unable to initialise component!", error);
        });
    },
    
    handleCCMe: function  (component, event){
        
        let isActive = event.getSource().get("v.value");
        
        if (isActive) {
            let action = component.get("c.getUserEmail");
            
            action.setCallback(this, function(response) {
                
                let state = response.getState();
                if (state === "SUCCESS") {
                    component.set("v.toEmail", response.getReturnValue());
                }
            });
            $A.enqueueAction(action);
        } else {
            component.set("v.toEmail", "");
        }
    },
    
    checkboxSelect: function(component, event, helper) {
        
    },
    
    handleSelectAll: function(component, event, helper) {
        
        let isActive = event.getSource().get("v.value");
        
        // ##### Note I still need to cover the event where there is only on contact i.e is not an array ##################
        var getAllId  = component.find("checkBox");
        if(Array.isArray(getAllId)){
            if(isActive==true){
                for(var i = 0; i < getAllId.length; i++){
                    component.find("checkBox")[i].set("v.value", true);
                }
                
            }else{
                for(var i = 0; i < getAllId.length; i++) {
                    component.find("checkBox")[i].set("v.value", false);
                }
            }
            
        } else if(!Array.isArray(getAllId)){
            if(isActive==true){
                component.find("checkBox").set("v.value", true);
                
            }else{
                component.find("checkBox").set("v.value", false);
            }    
        }
        
    },
    
    
    doSend : function(component, event, helper) {
        if(component.get('v.isHavingDueDate')){
        component.set("v.showLoadingSpinner", true);
        let emailPromise = Promise.resolve(
            helper.promiseEmailInvoice(component));
        
        emailPromise.then(
            $A.getCallback(function (result) {
                let isSent = result;
                if (!isSent) {
                    component.find('notifLib').showToast({
                        "variant": "error",
                        "message": "Unable to email Invoice!"
                    });
                }else {
                    component.find('notifLib').showToast({
                        "variant": "success",
                        "message": "Invoice emailed! "
                    });
                }
                component.set("v.showLoadingSpinner", false);
            }),
            $A.getCallback(function (status) {
                component.set("v.showLoadingSpinner", false);
                component.find('notifLib').showToast({
                    "variant": "error",
                    "message": "Unable to email Invoice "+status.message
                });
            })
        ).catch(function (error) {
            component.set("v.showLoadingSpinner", false);
            $A.reportError("Unable to email Invoice!", error);
        });
        $A.get('e.force:refreshView').fire();
        }else{
           component.set("v.showLoadingSpinner", false);
                component.find('notifLib').showToast({
                    "variant": "error",
                    "message": "The invoice has not been marked as sent. Please mark it as sent and try again"
                });
        }
    },
    
    handleRecordChanged : function(component, event, helper) {
        
    },
    
    // ################ Preview Invoice ################
    openViewInvoiceModal: function(component, event, helper) {
        
        var selectedValue = event.getParam("value");
        
        if(selectedValue == "previewInvoice"){
            
            component.set("v.isOpenViewInvoice", true);
            
        } 
        else if(selectedValue == "saveInvoice"){
            
            component.set("v.showLoadingSpinner", true);
            let savePromise = Promise.resolve( helper.promiseSaveInvoice(component));
            
            savePromise.then(
                $A.getCallback(
                    function (result) {
                        let isSoSaved = result;
                        if (!isSoSaved) {
                            component.find('notifLib').showToast({ "variant": "error",
                                                                  "message": "Unable to save Invoice!"
                                                                 });
                            
                        }else {
                            component.find('notifLib').showToast({ "variant": "success",
                                                                  "message": "Invoice Saved! "
                                                                 });
                            
                        }
                        component.set("v.showLoadingSpinner", false);
                    }
                ),
                $A.getCallback(
                    function (status) {
                        component.set("v.showLoadingSpinner", false);
                        component.find('notifLib').showToast({  "variant": "error",
                                                              "message": "Unable to save Invoice! "+status.message
                                                             });
                        
                    }
                )
            ).catch(
                function (error) {
                    component.set("v.showLoadingSpinner", false);
                    $A.reportError("Unable to save Invoice!", error);
                }
            );
            $A.get('e.force:refreshView').fire();
            
        }
    },
    
    closeViewInvoiceModal: function(component, event, helper) {
        // for Hide/Close Model,set the "isOpen" attribute to "Fasle"  
        component.set("v.isOpenViewInvoice", false);
        component.set("v.isOpenViewPenaltyInvoice", false);
    },  
    
})