({
    promiseGetRelatedContacts : function(component) {
        return this.createPromise(component, "c.getRelatedContacts", {invId: component.get("v.recordId")});
    },
    
    emailBodyCreation: function(component,result){
           //let options = result.map(record=>({label:record.Name,value:record.Email}));
                component.set("v.relatedContactList", result.contactList);
                console.log('result.emailFields');
        console.log(result.emailFields);
                var fieldsBody = '<br/>'
                if(result.defaultEmail){
                    let defaultFieldsArray = (result.defaultEmail).replace(/<p>/g, '');
                    let defaultFieldsArray12 = (defaultFieldsArray).replace(/<br><\/p>/g, '<br/>');
                    let defaultFieldsArray1 = (defaultFieldsArray12).replace(/<\/p>/g, '<br/>');
                   // for(let def of defaultFieldsArray){
                         fieldsBody += defaultFieldsArray1;
                    //}
                   
                }
                fieldsBody += '<ul>'
                for (var key in result.emailFields) {
                    
                    if (result.emailFields.hasOwnProperty(key)) {
                        var fieldKey = result.emailFields[key];
                        var invoiceField = '';
                        if(fieldKey && fieldKey.includes('.')){
                            var values = fieldKey.split('.');
                            if(result.Invoice_New__c[values[0]][values[1]]){
                                invoiceField = result.Invoice_New__c[values[0]][values[1]];
                            }
                        }else if(result.Invoice_New__c[fieldKey]) { 
                            invoiceField = result.Invoice_New__c[fieldKey];
                        }
                        if(invoiceField !=null && invoiceField != ''){
                        var field = '<li>' +key+ ' : ' + (isNaN(invoiceField) ? invoiceField : invoiceField.toLocaleString()) + '</li>';
                        console.log(field);
                        fieldsBody += field;
                        }
                    }
                }
        //isHavingDueDate
        if(result.Invoice_New__c['Due_Date__c']){
            component.set('v.isHavingDueDate',true);
        }else{
            component.set('v.isHavingDueDate',false);
        }
                fieldsBody += '</ul>';
                fieldsBody += 'For any queries please reach out to your designated TecEx finance team member.<br/>';
                fieldsBody += 'Kind regards <br/>TecEx Finance Team';
                


                console.log("fieldsBody");
                console.log(fieldsBody);
                component.set('v.emailBody',fieldsBody);
                console.log(JSON.stringify(result));
                console.log(result);  
    },
    promiseEmailInvoice : function(component) {
        //CC Email Processing
        var ccEmailTo = component.get('v.toEmail');
        if(ccEmailTo !=""){
            var emailTo = component.get('v.toEmail').split(";");
            console.log("Inside if Email### :" + JSON.stringify(emailTo));
        } else{
            var emailTo = component.get('v.toEmail');
            console.log("Inside else Email### :" + JSON.stringify(emailTo));
        }
        
        

        //Email To Processing
        var emaAddresses = [];
        var getEmaAddresses = component.find("checkBox");
        
        if(Array.isArray(getEmaAddresses)){
        for (var i = 0; i < getEmaAddresses.length; i++) {
            
            if (getEmaAddresses[i].get("v.value") == true) {
                emaAddresses.push(getEmaAddresses[i].get("v.text"));
                }
            }
        } 
        else if(!Array.isArray(getEmaAddresses)){
            if (getEmaAddresses.get("v.value") == true) {
                emaAddresses.push(getEmaAddresses.get("v.text"));
                }
            
        }
        
         let defaultFieldsArray = (component.get("v.emailBody")).replace(/<p>/g, '');
		let defaultFieldsArray1 = (defaultFieldsArray).replace(/<br><\/p>/g, '<br/>');
                    let emailBody = (defaultFieldsArray1).replace(/<\/p>/g, '<br/>');

        if(emaAddresses.length > 0){
            return this.createPromise(component, "c.sendInvoice", {
            invId: component.get("v.recordId"),
            emailType: component.get("v.radioValue"),
            toAddress: emaAddresses,
            ccAddress : emailTo,
            bodyEmail : emailBody,
            subjectEmail : component.get("v.subject"),
            
            });
        } else{
            component.find('notifLib').showToast({
                "variant": "warning",
                "message": "Please select at least one Contact"
            });
            component.set("v.showLoadingSpinner", false);
        }
},
    
promiseSaveInvoice : function(component){

    return this.createPromise(component, "c.saveInvoice", {invId: component.get("v.recordId")});
},

createPromise : function(component, name, params) {
        
    return new Promise(
                function(resolve, reject) {
                    let action = component.get(name);
                    if (params) {
                        action.setParams(params);
                    }
                    
                    action.setCallback(this, function(response) {
                        let state = response.getState();
                        if (component.isValid() && state === "SUCCESS") {
                            let result = response.getReturnValue();
                            resolve(result);
                        }
                        else {
                            reject(response.getError());
                        }
                    });
                    $A.enqueueAction(action);
                });
},
})