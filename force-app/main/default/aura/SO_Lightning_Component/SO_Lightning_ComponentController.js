({
	RefreshSuccess : function(component, event, helper) {
		var ToastEvent = $A.get("e.force:showToast");
        ToastEvent.setParams({
            "title": "Success!",
            "type": "success",
            "message": "Record save successful."});
        ToastEvent.fire();
        $A.get('e.force:refreshView').fire();
	},
    OnSubmit : function(component, event, helper) {
    },
    OnLoad : function(component, event, helper) {
        var ToastEvent = $A.get("e.force:showToast");
        ToastEvent.setParams({
            "title": "Loaded!",
            "message": ""});
        ToastEvent.fire();
    },
    OnError : function(component, event, helper) {
        var ToastEvent = $A.get("e.force:showToast");
        ToastEvent.setParams({
            "title": "Error!",
            "type": "error",
            "message": "An error occurred. Please review the error message on the page."});
        ToastEvent.fire();
    },
})