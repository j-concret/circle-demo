({
    doInitCtrl: function(component, event, helper) {
        if(component.get("v.recordId")) {
            helper.doInitHlpr(component, helper);
        }
        else {
            helper.showToast("error", "Error Occured", "Invalid record!");
        }
    },
    
    //Handle editability of fields
    editFreightRecord : function(component, event, helper) {helper.activeSections(component)},
    editFreightReq : function(component, event, helper) {helper.freightReq(component)},
    editPickup  : function(component, event, helper) {helper.editPickupAddress(component)},
    
    //required Fields
    showRequiredFields: function(component, event, helper){
        $A.util.removeClass(component.find("Name"), "none");
        $A.util.removeClass(component.find("Notify_Select_Option__c"), "none");
        $A.util.removeClass(component.find("Courier_Rate_KG_Override__c"), "none");
        $A.util.removeClass(component.find("Freight_Rate_KG_override__c"), "none");
        
        
    },
    
    //onsubmit
    onRecordSubmit: function (component, event, helper) {
        
        var invalidFields = [];
        //Required Fields
        var name                     = component.find("Name");
        if(name){
            var nameValue                 = name.get("v.value");
            if (!nameValue || nameValue.trim().length=== 0) {invalidFields.push(nameValue);}
        }
        
        if(invalidFields.length >0 ){
            for(var i=0; i<invalidFields.length; i++){ $A.util.addClass(invalidFields[i], 'slds-has-error');}
            event.preventDefault();
        }else{
        	component.set("v.showSpinner", true);    
        }
        
    }, 
    
    //onsuccess
    handleSuccess : function(component, event, helper) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({ "title": "Success!",
                              "message": "Saved.",
                              "type": "success"});
        toastEvent.fire();
        helper.doInitHlpr(component, helper);
        helper.activeSections(component);
        event.preventDefault();
        component.set("v.showSpinner", false);
    },
    
    //OnSuccess of pickup tab
    handleSuccessPickup : function(component, event, helper) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({ "title": "Success!",
                              "message": "Saved.",
                              "type": "success"});
        toastEvent.fire();
        helper.doInitHlpr(component, helper);
        helper.editPickupAddress(component);
        event.preventDefault();
        component.set("v.showSpinner", false);
    },
    
    //OnSuccess of Freight tab
    handleSuccessFreight : function(component, event, helper) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({ "title": "Success!",
                              "message": "Saved.",
                              "type": "success"});
        toastEvent.fire();
        helper.doInitHlpr(component, helper);
        helper.freightReq(component)
        event.preventDefault();
        component.set("v.showSpinner", false);
    },
    
    //Show Shipment order chatter
    handleShowChatterSectionCtrl: function(component, event, helper) {
        component.set("v.showChatterSection", !component.get("v.showChatterSection"));
    },
    
    //OnError
    onError : function(component, event, helper) {
        
        var toastEvent2 = $A.get("e.force:showToast");
        toastEvent2.setParams({
            "title": "Error!",
            "message": "Error."
        });
        toastEvent2.fire();
        component.set("v.showSpinner", false);
    },
    
    handleCancel : function(component, event, helper) {
        helper.activeSections(component);
        event.preventDefault();
    },
    handleCancelFreightReq: function(component, event, helper) {
        helper.freightReq(component);
        event.preventDefault();
    },
    handleCanceladdress: function(component, event, helper) {
        helper.editPickupAddress(component);
        event.preventDefault();
    },
    
    //Show Schedulepickup  modal
    navigate : function(component, event, helper) {
        component.set("v.showSchedulePickup", true);
    },
    editCourierRecords : function(component, event, helper) {
        if(component.get("v.courierRates").length > 0){
            if(!component.get("v.showEditToCourierRates")){
                component.set("v.showEditToCourierRates", true);    
            }else{
                var originalData = component.get("v.courierRatesCopy");
                component.set("v.courierRates",originalData);
                component.set("v.showEditToCourierRates", false);
            }
        }
    },
    
    //Save courier Data
    handleSave: function(component, event, helper) {
        helper.saveCourierData(component, event, helper);
    },
    navigateNotes: function(component, event, helper) {
        
    },
    
    //Navigate to record
    handleClickNavigate: function(component, event, helper){
        var navService = component.find("navService");
        var pageReference = {    
            "type": "standard__recordPage", //example for opening a record page, see bottom for other supported types
            "attributes": {
                "recordId": event.currentTarget.name, //place your record id here that you wish to open
                "actionName": "view"
            }
        }
        navService.navigate(pageReference);
    },
    refreshCmp: function(component, event, helper) {
      component.set('v.showChatterSection',!component.get('v.showChatterSection'));
      component.set('v.showChatterSection',!component.get('v.showChatterSection'));
      helper.doInitHlpr(component, helper);
    }
    
})