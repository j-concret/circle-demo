({
    doInitHlpr: function(component, helper) {
       component.set("v.showSpinner", true);
        var params = {
            shipmentId: component.get("v.recordId")
        };
        this.makeServerCall(component, "getFrieghtData", params, function(result) {
            if(result.status === 'OK') {
                if(result.datafreightData.length> 0){
                component.set("v.freightID", result.datafreightData[0].Id);    
                    if(result.datafreightData[0].Courier_Rate__r){
                        component.set("v.courierRates",result.datafreightData[0].Courier_Rate__r);
                        component.set("v.courierRatesCopy",result.datafreightData[0].Courier_Rate__r);
                        helper.getPicklistValues(component, event, helper);
                    }
                    if(result.dataPickup.length > 0){
                        component.set("v.zkmulti_Pickup_Number", result.dataPickup[0]);
                    }
                    if(result.showPickUpButton){
                        component.set("v.showPickUpButton", result.showPickUpButton);
                    }
                    if(result.taskPickScheduled){
                        component.set("v.PickReschedule", result.taskPickScheduled);
                    }
                }
                
                
             
            }
            else {
                helper.showToast("error", "Error Occured Helper", result.msg);
            }
            component.set("v.showSpinner", false);
        });
    },
    getPicklistValues : function(component, event, helper) {
        var par = {object_name : 'Courier_Rates__c', 
                   field_names : ['Status__c']};
        this.makeServerCall(component, 'getPicklistsValues', par , function(response) {
            
            component.set("v.statusPicklist", response.Status__c);
            
                var optionsArray = [];
                for (let index = 0; index < response.Status__c.length; index++) {
                    var element = {"label" : response.Status__c[index],"value" : response.Status__c[index]};
                    optionsArray.push(element);
                }
            component.set("v.statusPicklist", optionsArray);
        });
    },
    activeSections: function(component, event){
        var openSections = component.get("v.selectedTab");
        
        if(openSections === "Freight Details") {
            var editFormFreight = component.find("editFormFreightDetail");
            $A.util.toggleClass(editFormFreight, "slds-hide");
            var viewFormFreight = component.find("viewFormFreightDetail");
            $A.util.toggleClass(viewFormFreight, "slds-hide");
        }else if(openSections === "Courier Rates"){
            var editFormFreight = component.find("editFormCourierRates");
            $A.util.toggleClass(editFormFreight, "slds-hide");
            var viewFormFreight = component.find("viewFormCourierRates");
            $A.util.toggleClass(viewFormFreight, "slds-hide");
        }else if(openSections === "Consignee Address - FF"){
            var editFormFreight = component.find("editFormCourierFF");
            $A.util.toggleClass(editFormFreight, "slds-hide");
            var viewFormFreight = component.find("viewFormCourierFF");
            $A.util.toggleClass(viewFormFreight, "slds-hide");
        }else if(openSections === "Consignee Address - Courier"){
            var editFormFreight = component.find("editFormCourier");
            $A.util.toggleClass(editFormFreight, "slds-hide");
            var viewFormFreight = component.find("viewFormCourier");
            $A.util.toggleClass(viewFormFreight, "slds-hide");
        }else if(openSections === "Internal Courier Model"){
            var editFormFreight = component.find("editFormInternalCourierModel");
            $A.util.toggleClass(editFormFreight, "slds-hide");
            var viewFormFreight = component.find("viewFormInternalCourierModel");
            $A.util.toggleClass(viewFormFreight, "slds-hide");
        }else if(openSections === "Internal Freight Model"){
            var editFormFreight = component.find("editFormInternalFreightModel");
            $A.util.toggleClass(editFormFreight, "slds-hide");
            var viewFormFreight = component.find("viewFormInternalFreightModel");
            $A.util.toggleClass(viewFormFreight, "slds-hide");
        }else if(openSections === "Freight Fields"){
            var editFormFreightFields = component.find("editFormFreightFields");
            $A.util.toggleClass(editFormFreightFields, "slds-hide");
            var viewFormFreightFields = component.find("viewFormFreightFields");
            $A.util.toggleClass(viewFormFreightFields, "slds-hide");
        }
        
    },
    freightReq: function(component, event){
        var editFormFreight = component.find("editFormFreightReq");
            $A.util.toggleClass(editFormFreight, "slds-hide");
            var viewFormFreight = component.find("viewFormFreightReq");
            $A.util.toggleClass(viewFormFreight, "slds-hide");
     /*   var editFormSO = component.find("editFormSO");
        $A.util.toggleClass(editFormSO, "slds-hide");
            var viewFormSO = component.find("viewFormSO");
            $A.util.toggleClass(viewFormSO, "slds-hide");*/
    },
    editPickupAddress: function(component, event){
        
        var selectedTabPickup = component.get("v.selectedTabPickup");
        if(selectedTabPickup == "Details"){
        	var editFormFreight = component.find("editPickUp");
            $A.util.toggleClass(editFormFreight, "slds-hide");
            var viewFormFreight = component.find("viewPickUp");
            $A.util.toggleClass(viewFormFreight, "slds-hide");    
        }else if(selectedTabPickup == "Pick Up Address"){
            var editFormFreight = component.find("editPickupAddress");
            $A.util.toggleClass(editFormFreight, "slds-hide");
            var viewFormFreight = component.find("viewPickupAddress");
            $A.util.toggleClass(viewFormFreight, "slds-hide");    
        }
    },
    saveCourierData: function(component, event, helper){
        component.set("v.showSpinner", true);
        var params = {
            courierRatesObj: component.get("v.courierRates")
        };
        this.makeServerCall(component, "updateCourierRatesData", params, function(result) {
            if(result.status === 'OK') {
            helper.showToast("Success", "Record Updated", '');
                component.set("v.showEditToCourierRates", false);
            }
            else {
                helper.showToast("error", "Error Occured Helper", result.msg);
            }
            component.set("v.showSpinner", false);
        });
    },
        
})