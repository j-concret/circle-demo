({
  getData : function(cmp) {
    cmp.set('v.showSpinner',true);
    var self = this;
    self.serverCall(cmp,'getPicklistValues',{},function(response){
      cmp.set('v.showSpinner',false);
      cmp.set( "v.fromCountryOption", response.fromCountry );
      cmp.set( "v.toCountryOption", response.toCountry );
      cmp.set( "v.countryValidation", response.countryValidation );
    },function(err){
      cmp.set('v.showSpinner',false);
      self.showToast('error','ERROR',err);
    });
  },
  getZeeData : function(cmp) {
    cmp.set('v.showSpinner',true);
    var self = this;
    self.serverCall(cmp,'getZeePicklistValues',{},function(response){
      cmp.set('v.showSpinner',false);
      cmp.set( "v.fromCountryOption", response.fromCountry );
      cmp.set( "v.toCountryOption", response.toCountry );
      cmp.set( "v.countryValidation", response.countryValidation );
    },function(err){
      cmp.set('v.showSpinner',false);
      self.showToast('error','ERROR',err);
    });
  },  
  getDefault : function(cmp) {
    var self = this;
    cmp.set('v.showSpinner',true);
    self.serverCall(cmp,'getCPDefaults',{
      clientId: cmp.get('v.accountId')
    },function(response){
      cmp.set('v.showSpinner',false);
      var units = ['KGs','LBS'];
      if(response.pickupDefault){
        cmp.set('v.pickupAddIds',[response.pickupDefault]);
          cmp.set('v.pickupAddName',response.pickupDefaultName);
          console.log(cmp.get('v.pickupAddName'));
      }
      if(response.cpdefaults && response.cpdefaults.length > 0){
        var defval = response.cpdefaults[0];
        //cmp.set('v.',defval.TecEx_to_handle_freight__c);
        //cmp.set('v.',defval.Second_hand_parts__c);
        cmp.set('v.packageWeightUnit',defval.Package_Weight_Units__c);
        cmp.set('v.packageDimensionUnit',defval.Package_Dimensions__c);
        //cmp.set('v.',defval.Li_ion_Batteries__c);
        cmp.set('v.unit',(defval.Chargeable_Weight_Units__c && units.includes(defval.Chargeable_Weight_Units__c) ) ? defval.Chargeable_Weight_Units__c:'KGs');
        cmp.set('v.fromCountry',defval.Ship_From__c);
        if(defval.Service_Type__c)
          cmp.set('v.service_Type',defval.Service_Type__c);
      }else{
        //cmp.set('v.','');
        //cmp.set('v.','');
        cmp.set('v.packageWeightUnit','');
        cmp.set('v.packageDimensionUnit','');
        //cmp.set('v.','');
        cmp.set('v.unit','KGs');
        cmp.set('v.fromCountry','');
        cmp.set('v.service_Type','');
      }
    },function(err){
      cmp.set('v.showSpinner',false);
      self.showToast('error','ERROR',err);
    });
  },
  createSO: function(cmp,currentStep,isSaveNew){
    var self = this;
    var shipmentOrder = {
      sobjectType : 'Shipment_Order__c',
      Account__c: cmp.get('v.accountId'),
      Client_Contact_for_this_Shipment__c: cmp.get('v.client_contact'),
      Service_Type__c: cmp.get('v.service_Type'),
      Ship_From_Country__c : cmp.get('v.fromCountry'),
      Shipment_Value_USD__c : parseFloat(cmp.get('v.shipment_value')),
      //Client_Taking_Liability_Cover__c : cmp.get('v.liability_Cover'),
      Who_arranges_International_courier__c  : cmp.get('v.international_Freight'),
      //Source__c : cmp.get('v.'),
      No_Of_Freight__c : 1,
      //RecordTypeId : cmp.get('v.'),
      of_packages__c : cmp.get('v.noOfPackages'),
      Actual_Weight_KGs__c : cmp.get('v.actualChargeableWeight') ? parseFloat(cmp.get('v.actualChargeableWeight')):0,
      Chargeable_Weight__c : cmp.get('v.unit') == 'LBS' ? cmp.get('v.chargeableWeight')*0.453592 : cmp.get('v.chargeableWeight'),//parseFloat(cmp.get('v.chargeableWeight')),
      Buyer_Account__c : cmp.get('v.client_buyerId'),
      Client_Reference__c : cmp.get('v.client_Reference_1'),
      Client_Reference_2__c : cmp.get('v.client_Reference_2'),
      Number_of_Final_Deliveries_Client__c : cmp.get('v.no_of_delivery_add_client') || 0,
      Final_Deliveries_New__c : cmp.get('v.no_of_delivery_add_client') || 0,
      Package_Height__c : cmp.get('v.package_Height'),
      Contains_Batteries_Client__c : cmp.get('v.contains_Batteries_Client'),
      Stuck_Shipment__c : cmp.get('v.StuckShipment')
      //Client_PO_ReferenceNumber__c : cmp.get('v.'),
    };
    var typeGood = cmp.get('v.typeOfGoods');
    if(typeGood && typeGood != 'Neither of them'){
      shipmentOrder.Type_of_Goods__c = typeGood;
    }
    if(cmp.get('v.no_of_delivery_add_auto') && cmp.get('v.no_of_delivery_add_auto') != 0){
      shipmentOrder.Number_of_Final_Deliveries_Auto__c = cmp.get('v.no_of_delivery_add_auto') || 0;
      if(shipmentOrder.Number_of_Final_Deliveries_Auto__c > shipmentOrder.Final_Deliveries_New__c){
        shipmentOrder.Final_Deliveries_New__c = shipmentOrder.Number_of_Final_Deliveries_Auto__c;
      }
    }

    if(cmp.get('v.business') == 'Zee'){
        shipmentOrder.Preferred_Freight_method__c	= cmp.get('v.preferredFreight');
        shipmentOrder.Service_Type__c =  cmp.get('v.service_Type') == 'Both' ? 'IOR':cmp.get('v.service_Type');
        shipmentOrder.Who_arranges_International_courier__c  = cmp.get('v.service_Type') == 'IOR' ? 'Client' : 'TecEx';
    }else{
      shipmentOrder.Service_Type__c =  cmp.get('v.service_Type');
      shipmentOrder.Who_arranges_International_courier__c  = cmp.get('v.international_Freight');
    }
    var packages = cmp.get('v.shipment_order_package_list');
    var pickupAddIds = cmp.get('v.pickupAddIds');
    var finalAddIds = cmp.get('v.finalAddIds');
    var toCountry = cmp.get('v.toCountry');
    var action = 'insertShipmentOrder';
    var recordId = cmp.get('v.recordId');
       var rolloutId = cmp.get('v.RolloutId');
    var desinations = [];
    toCountry.forEach(function (arrayItem) {
      if (cmp.get('v.business') == 'Zee') {
        desinations.push(arrayItem);
      }else
      desinations.push(arrayItem.Name);
    });
    var data = {shipmentOrder,desinations,packages,pickupAddIds,finalAddIds};
    if(recordId && recordId != '' && (rolloutId == '' || rolloutId == null)){
     				 action = 'updateShipmentOrder';
      				 data['recordId'] = recordId;
      				 data['isShipment'] = cmp.get('v.isShipment');
               }else if(rolloutId && rolloutId != ''){
                	 data['ExistingRolloutId'] = rolloutId;
               }
    
      cmp.set('v.showSpinner',true);
      self.serverCall(cmp,action,data,function(response){
        cmp.set('v.showSpinner',false);
        if(recordId && recordId !=''){
          self.showToast('sucess','SUCCESS','Updated Successfully!');
        }else{
          if(response.rolloutId){
            cmp.set('v.recordId',response.rolloutId);
                  if(response.soids){
                  cmp.set('v.RolloutSoids',response.soids);
                  //RolloutSoids
                }
            cmp.set('v.isShipment',false);
          }else{
            cmp.set('v.recordId',response.shipmentId);
            cmp.set('v.isShipment',true);
          }
        }
        if(response.packages)
            cmp.set('v.shipment_order_package_list',response.packages);

        if (cmp.get('v.business') == 'Zee') {
          var childComponent = cmp.find("childComponent");
          var message = childComponent.insertParts(isSaveNew);
        }else
          cmp.set('v.currentStep', currentStep.toString());
      },function(err){
        cmp.set('v.showSpinner',false);
        self.showToast('error','ERROR',err);
      });
    
    
  },
  showToast : function(title,type,msg) {
    var toastEvent = $A.get('e.force:showToast');
    toastEvent.setParams({
      'title': title,
      'type' :type,
      'message': msg
    });
    toastEvent.fire();
  },
  checkValidation: function(cmp) {
      var accountId = cmp.get('v.accountId');
      var client_contact = cmp.get('v.client_contact');
      var fromCountry = cmp.get('v.fromCountry');
      var toCountry = cmp.get('v.toCountry');
      var preferredFreight = cmp.get('v.preferredFreight');
      var isAllValid = true;
      if(((!preferredFreight || preferredFreight == '') && cmp.get('v.business') == 'Zee')||!accountId || accountId == '' || !client_contact || client_contact == '' || !fromCountry || fromCountry == '--none--' || !toCountry || toCountry.length == 0)
        isAllValid = false;
      var isValid =  cmp.find('fieldId').reduce(function (validSoFar, inputCmp) {
          //inputCmp.reportValidity();
          return validSoFar && inputCmp.checkValidity();
      }, true);
      return isValid && isAllValid;
  },
  getClientBuyer : function(cmp) {
    cmp.set('v.showSpinner',true);
    var self = this;
    self.serverCall(cmp,'createClientBuyer',{
        BuyerAccountId:cmp.get('v.BuyerAccountRecordId'),
        AccountId: cmp.get('v.accountId')
    },function(response){
      cmp.set('v.showSpinner',false);
      cmp.set('v.client_buyerId', response.Buyer_Account__c);
        cmp.set('v.rerender',false);
        cmp.set('v.rerender',true);
    console.log('response.ClientBuyer.Id');
	console.log(response.Id)
    },function(err){
      cmp.set('v.showSpinner',false);
      self.showToast('error','ERROR',err);
    });
  },
  getRolloutDetailsonCMP : function(cmp) {
    cmp.set('v.showSpinner',true);
    var self = this;
    self.serverCall(cmp,'getRolloutDetails',
                    {
                     RolloutIds:cmp.get('v.RolloutId')
                    },
                    function(response){
      cmp.set('v.showSpinner',false);
      cmp.set( "v.Rollout", response.Rollout[0]);
      cmp.set('v.accountId',response.Rollout[0].Client_Name__c);
      cmp.set('v.client_contact',response.Rollout[0].Contact_For_The_Quote__c);
     },function(err){
      cmp.set('v.showSpinner',false);
      self.showToast('error','ERROR',err);
    });
  },
  getPreferredFreight: function(cmp) {
      var freightList = [{'label':'Air', 'value': 'Air'}];
      var shipFrom = cmp.get('v.fromCountry');
      var shipTo = cmp.get('v.toCountry')[0];
      var self = this;
      self.serverCall(cmp,'getPreferredFreight',{
          shipFrom: shipFrom,
          shipTo: shipTo
      },function(response){
          if(response != ''){
              freightList.push({'label':response, 'value': response});
              cmp.set('v.preferredOptions',freightList);
              cmp.set('v.preferredFreight',' ');
          }
          else{
              cmp.set('v.preferredOptions',freightList);
          }
    },function(err){
      cmp.set('v.showSpinner',false);
      self.showToast('error','ERROR',err);
    });
      
  },
  checkRegistrations : function(cmp,toCountry,accountId){
    var self = this;
    cmp.set('v.showSpinner',true);
    self.serverCall(cmp,'getRegistrations',{toCountry : toCountry,accountId : accountId},function(response){
      cmp.set('v.showSpinner',false);
      if(response != ''){
        console.log("checkRegistrations",response);
        var temp = response.split(';');
        cmp.set('v.RegistrationName',temp[0]);
        cmp.set("v.RegistrationID",'/lightning/r/Registrations__c/'+temp[1]+'/view');
        cmp.set('v.hasRegistration',true);
      }else{
        cmp.set('v.hasRegistration',false);
      }
    },function(err){
      cmp.set('v.showSpinner',false);
      self.showToast('error','ERROR',err);
    });
  } 
})