({
    doInit : function(cmp, event, helper) {
        cmp.set('v.additionalFilters',{'RecordTypeId':['0120Y0000009cEvQAI','0125E000000bANcQAM','0121v000000lYJyAAM']});//cmp.set('v.additionalFilters',{'RecordTypeId':['0120Y0000009cEvQAI','0125r0000004Kx6AAE']}); //next record type = 0125E000000bANcQAM
        cmp.set( "v.table_headings",
                [ "#","How many packages?", "Weight Unit", "Actual Weight",
                 "Dimension Unit", "Length", "Breadth", "Height","Contains Batteries?" ] );
        helper.getData(cmp);
        if(cmp.get("v.recordROId")){
            cmp.set('v.RolloutId',cmp.get("v.recordROId"));
            helper.getRolloutDetailsonCMP(cmp);
        }
        
    },
    changeServiceOptions : function(cmp,event,helper){
        var serviceOptions;
        var business = cmp.get("v.business");
        if (business == 'Zee') {
            serviceOptions = [
                {'label': 'IOR', 'value': 'IOR'},
                {'label': 'Shipping Only', 'value': 'Shipping Only'},
                {'label': 'Both', 'value': 'Both'}
                ];
            cmp.set("v.serviceOptions",serviceOptions);
            helper.getZeeData(cmp);
            cmp.set('v.additionalFilters',{'RecordTypeId':['0121v000000lYJyAAM']});
            cmp.set('v.no_of_delivery_add_client',1);
            cmp.find("finalDelivery").set("v.disabled", true);
            cmp.set( "v.table_headings",
                [ "#","How many packages?", "Weight Unit", "Actual Weight",
                 "Dimension Unit", "Length", "Breadth", "Height","Dangerous Goods ?" ] );
        } else {
            serviceOptions = [
                {'label': 'IOR', 'value': 'IOR'},
                {'label': 'EOR', 'value': 'EOR'}
                ];
                cmp.set("v.serviceOptions",serviceOptions); 
        }
    },
    packageChangeHandler:function(cmp,event,helper){
        var records = cmp.get( "v.shipment_order_package_list" );
        var CummActWeight=0;
        var Actweight=0;
        var CummVolWeight=0;
        var Volweight=0;
        var no_of_packages = 0;
        var CummChargableWeight=0;
        var charweight=0;
        var package_Height = 'No';
        var contains_Batteries_Client;
        var errorMessages = cmp.get('v.errorMessages');
        var packRow = 0;
        records.forEach(function (ShipPackage){
            packRow = packRow+1;
            if(ShipPackage.Weight_Unit__c =='KGs') {
                Actweight = (ShipPackage.Actual_Weight__c*ShipPackage.packages_of_same_weight_dims__c);
            } else{
                Actweight=(ShipPackage.Actual_Weight__c*ShipPackage.packages_of_same_weight_dims__c*0.453592);
            }
            if(ShipPackage.Dimension_Unit__c =='CMs') {
                var breath_inch = parseFloat(ShipPackage.Breadth__c) * 0.393701;
                var length_inch = parseFloat(ShipPackage.Length__c) * 0.393701;
                var height_inch = parseFloat(ShipPackage.Height__c) * 0.393701;
                Volweight = ((ShipPackage.Length__c*ShipPackage.Breadth__c*ShipPackage.Height__c)/5000)*ShipPackage.packages_of_same_weight_dims__c;
                //if(breath_inch < 47 && height_inch < 47 && length_inch < 47 && package_Height !='Yes') {
                if(breath_inch <= 47 && height_inch <= 47 && length_inch <= 47) {
                    package_Height ='<=47';
                }else if(breath_inch > 47 && height_inch > 47 && length_inch > 47){
                    package_Height ='>47';
                }
            } else {
                Volweight = ((ShipPackage.Length__c*ShipPackage.Breadth__c*ShipPackage.Height__c)/305.1200198)*ShipPackage.packages_of_same_weight_dims__c;
                //if(ShipPackage.Breadth__c < 47 && ShipPackage.Height__c < 47 && ShipPackage.Length__c < 47 && package_Height !='Yes') {
                if(ShipPackage.Breadth__c <= 47 && ShipPackage.Height__c <= 47 && ShipPackage.Length__c <= 47) {
                    package_Height ='<=47';
                }else if(ShipPackage.Breadth__c > 47 && ShipPackage.Height__c > 47 && ShipPackage.Length__c > 47){
                    package_Height ='>47';
                }
            }
            if(Actweight >= Volweight) {
                charweight = Actweight;
            } else{
                charweight = Volweight;
            }
            if(!contains_Batteries_Client || contains_Batteries_Client != 'Yes') {
                contains_Batteries_Client = ShipPackage.Contains_Batteries__c == true ? 'Yes' : 'No';
            }
            no_of_packages += parseInt(ShipPackage.packages_of_same_weight_dims__c);
            CummActWeight += Actweight;
            CummVolWeight += Volweight;
            CummChargableWeight += charweight;
        var errorMessages = cmp.get('v.errorMessages');
            if(errorMessages.indexOf('No. of Packages Can not be zero at row-'+packRow) <= -1){
            if(parseFloat(ShipPackage.packages_of_same_weight_dims__c) <= 0){
                errorMessages.push('No. of Packages Can not be zero at row-'+packRow);
                cmp.set('v.errorMessages',errorMessages);
            }
            }else if(parseFloat(ShipPackage.packages_of_same_weight_dims__c) > 0){
                var result = errorMessages.filter(function(x) {
                return x !== 'No. of Packages Can not be zero at row-'+packRow
            });
            cmp.set('v.errorMessages',result);
            
            }
            
             var errorMessages = cmp.get('v.errorMessages');
             if(errorMessages.indexOf('Actual Weight Can not be zero at row-'+packRow) <= -1){
            if(parseFloat(ShipPackage.Actual_Weight__c) <= 0){
                errorMessages.push('Actual Weight Can not be zero at row-'+packRow);
                cmp.set('v.errorMessages',errorMessages);
            }
            }else if(parseFloat(ShipPackage.Actual_Weight__c) > 0){
                var result = errorMessages.filter(function(x) {
                return x !== 'Actual Weight Can not be zero at row-'+packRow
            });
            cmp.set('v.errorMessages',result);
            
            }
            
             var errorMessages = cmp.get('v.errorMessages');
            if(errorMessages.indexOf('Length Can not be zero at row-'+packRow) <= -1){
            if(parseFloat(ShipPackage.Length__c) <= 0 ){
                errorMessages.push('Length Can not be zero at row-'+packRow);
                cmp.set('v.errorMessages',errorMessages);
            }
                }else if(parseFloat(ShipPackage.Length__c) > 0){
                var result = errorMessages.filter(function(x) {
                return x !== 'Length Can not be zero at row-'+packRow
            });
            cmp.set('v.errorMessages',result);
            
            }
            
             var errorMessages = cmp.get('v.errorMessages');
            if(errorMessages.indexOf('Breadth Can not be zero at row-'+packRow) <= -1){
            if(parseFloat(ShipPackage.Breadth__c) <= 0){
                errorMessages.push('Breadth Can not be zero at row-'+packRow);
                cmp.set('v.errorMessages',errorMessages);
            }
                 }else if(parseFloat(ShipPackage.Breadth__c) > 0){
                var result = errorMessages.filter(function(x) {
                return x !== 'Breadth Can not be zero at row-'+packRow;
            });
            cmp.set('v.errorMessages',result);
            
            }
            
             var errorMessages = cmp.get('v.errorMessages');
            if(errorMessages.indexOf('Height Can not be zero at row-'+packRow) <= -1){
            if(parseFloat(ShipPackage.Height__c) <= 0){
                errorMessages.push('Height Can not be zero at row-'+packRow);
                cmp.set('v.errorMessages',errorMessages);
            }
            }else if(parseFloat(ShipPackage.Height__c) > 0){
                var result = errorMessages.filter(function(x) {
                return x !== 'Height Can not be zero at row-'+packRow
            });
            cmp.set('v.errorMessages',result);
            
            }
           
            
           
        });
        cmp.set('v.package_Height',package_Height);
        cmp.set('v.contains_Batteries_Client',contains_Batteries_Client);
        cmp.set('v.chargeableWeight',CummChargableWeight.toFixed(2));
        cmp.set('v.actualChargeableWeight',CummActWeight.toFixed(2));
        cmp.set('v.noOfPackages',no_of_packages);
        cmp.set('v.unit','KGs');
        cmp.set("v.isPackageModelOpen", false);
    },
    changeHandler:function(cmp,event,helper){
        cmp.set('v.client_contact',null);
        cmp.find('clientContact').clearSelectedRecord();
        helper.getDefault(cmp);
    },
    addPackageRow : function(cmp,event,helper){
        var records = cmp.get( "v.shipment_order_package_list" );
        var weightUnit = cmp.get( "v.packageWeightUnit" );
        var dimUnit = cmp.get( "v.packageDimensionUnit" );
        //add to records list
        records.push({
            "sobjectType":"Shipment_Order_Package__c",
            "packages_of_same_weight_dims__c":0,
            "Weight_Unit__c": weightUnit ? weightUnit :"KGs",
            "Actual_Weight__c":0,
            "Dimension_Unit__c": dimUnit && dimUnit=='Inch' ? 'INs' : "CMs",
            "Length__c":0,
            "Breadth__c":0,
            "Height__c":0,
            "Contains_Batteries__c":false,
            "Dangerous_Goods__c":false
        });
        cmp.set( "v.shipment_order_package_list", records );
    },
    deleteRow : function( cmp, event, helper ){
        var index = event.getParam( "index" );
        var rows_list = cmp.get( "v.shipment_order_package_list" );
        rows_list.splice( index, 1 );
        cmp.set( "v.shipment_order_package_list", rows_list );
    },
    openPackageModal: function(cmp,event,helper){
        cmp.set('v.isPackageModelOpen',true);
    },
    openPickupModal: function(cmp,event,helper){
        cmp.set('v.isPickupModalOpen',true);
    },
    openFinalModal: function(cmp,event,helper){
        cmp.set('v.isFinalModalOpen',true);
    },
    openRegistrationModal: function(cmp,event,helper){
        cmp.set('v.isRegistrationModalOpen',true);
    },
    openClientBuyerModal: function(cmp,event,helper){
        cmp.set('v.isClientBuyerOpen',true);
    },
    handleOnSubmit: function(cmp,event,helper){
        //cmp.set('v.isClientBuyerOpen',false);
    },
    handleOnSuccess: function(cmp,event,helper){
        //cmp.set('v.isClientBuyerOpen',false);
        cmp.set('v.isClientBuyerOpen',false);
        console.log('clientRecordId '+cmp.get('v.clientRecordId'));
        var payload = event.getParams().response;
        console.log(payload.id);
        cmp.set('v.BuyerAccountRecordId',payload.id);
        if(cmp.get('v.accountId')){
            helper.getClientBuyer(cmp);
        }else{
            helper.showToast('error','ERROR','Please select Account.');
        }
        //accountId
    },
    handleWeightChange : function(cmp,event,helper){
        if(cmp.get('v.unit') == 'LBS' && cmp.get( "v.noOfPackages") == 0){
            var Actweight = cmp.get('v.chargeableWeight') ? cmp.get('v.chargeableWeight')/0.453592 : 0;
            cmp.set('v.chargeableWeight',Actweight.toFixed(2));
            
        }else{
            var Actweight = cmp.get('v.chargeableWeight') ? cmp.get('v.chargeableWeight')*0.453592 : 0;
            cmp.set('v.chargeableWeight',Actweight.toFixed(2));
            
        }
    },
    closeClientBuyerModel: function(cmp,event,helper){
        cmp.set('v.isClientBuyerOpen',false);
    },
    closeModal:function(cmp, event, helper) {
        var name = event.getSource().get("v.name");
        switch (name) {
            case 'cancelPackage':
                cmp.set("v.shipment_order_package_list", []);
                cmp.set("v.isPackageModelOpen", false);
                break;
            case 'submitPackage':
                cmp.set("v.isPackageModelOpen", false);
                break;
            case 'cancelPickup':
                cmp.set('v.pickupAddIds',[]);
                cmp.set("v.isPickupModalOpen", false);
                break;
            case 'cancelFinal':
                cmp.set('v.finalAddIds',[]);
                cmp.set("v.isFinalModalOpen", false);
                break;
            case 'submitPickup':
                cmp.set("v.isPickupModalOpen", false);
                break;
            case 'submitFinal':
                cmp.set("v.isFinalModalOpen", false);
                break;
            case 'cancelRegistration':
                cmp.set("v.isRegistrationModalOpen", false);    
        }
    },
    showClientRef:function(cmp, event, helper){
        cmp.set('v.show2ndClientRef',true);
    },
    proceedToLineItem:function(cmp, event, helper){
        var currentStep = parseInt(cmp.get('v.currentStep'));
        var records = cmp.get( "v.shipment_order_package_list" );
        
        
        var errorMessages = cmp.get('v.errorMessages');
        if(cmp.get('v.chargeableWeight') <= 0){
            errorMessages.push('Chargable Weight Can not be zero');
            cmp.set('v.errorMessages',errorMessages);
        }else{
            var errorMessages = cmp.get('v.errorMessages');
            /**Start Of Removal of Chargable Weight Can not be zero **/
			var result = errorMessages.filter(function(x) {
                return x !== 'Chargable Weight Can not be zero';
            });
            console.log(result);
            cmp.set('v.errorMessages',result);
            /**End Of Removal of Chargable Weight Can not be zero **/
             var packRow = 0;
        records.forEach(function (ShipPackage){
            packRow = packRow+1;
               
             var errorMessages = cmp.get('v.errorMessages');
            if(errorMessages.indexOf('No. of Packages Can not be zero at row-'+packRow) <= -1){
            if(parseFloat(ShipPackage.packages_of_same_weight_dims__c) <= 0){
                errorMessages.push('No. of Packages Can not be zero at row-'+packRow);
                cmp.set('v.errorMessages',errorMessages);
            }
            }else if(parseFloat(ShipPackage.packages_of_same_weight_dims__c) > 0){
                var result = errorMessages.filter(function(x) {
                return x !== 'No. of Packages Can not be zero at row-'+packRow
            });
            cmp.set('v.errorMessages',result);
            
            }
            
             var errorMessages = cmp.get('v.errorMessages');
             if(errorMessages.indexOf('Actual Weight Can not be zero at row-'+packRow) <= -1){
            if(parseFloat(ShipPackage.Actual_Weight__c) <= 0){
                errorMessages.push('Actual Weight Can not be zero at row-'+packRow);
                cmp.set('v.errorMessages',errorMessages);
            }
            }else if(parseFloat(ShipPackage.Actual_Weight__c) > 0){
                var result = errorMessages.filter(function(x) {
                return x !== 'Actual Weight Can not be zero at row-'+packRow
            });
            cmp.set('v.errorMessages',result);
            
            }
            
             var errorMessages = cmp.get('v.errorMessages');
            if(errorMessages.indexOf('Length Can not be zero at row-'+packRow) <= -1){
            if(parseFloat(ShipPackage.Length__c) <= 0 ){
                errorMessages.push('Length Can not be zero at row-'+packRow);
                cmp.set('v.errorMessages',errorMessages);
            }
                }else if(parseFloat(ShipPackage.Length__c) > 0){
                var result = errorMessages.filter(function(x) {
                return x !== 'Length Can not be zero at row-'+packRow
            });
            cmp.set('v.errorMessages',result);
            
            }
            
             var errorMessages = cmp.get('v.errorMessages');
            if(errorMessages.indexOf('Breadth Can not be zero at row-'+packRow) <= -1){
            if(parseFloat(ShipPackage.Breadth__c) <= 0){
                errorMessages.push('Breadth Can not be zero at row-'+packRow);
                cmp.set('v.errorMessages',errorMessages);
            }
                 }else if(parseFloat(ShipPackage.Breadth__c) > 0){
                var result = errorMessages.filter(function(x) {
                return x !== 'Breadth Can not be zero at row-'+packRow;
            });
            cmp.set('v.errorMessages',result);
            
            }
            
             var errorMessages = cmp.get('v.errorMessages');
            if(errorMessages.indexOf('Height Can not be zero at row-'+packRow) <= -1){
            if(parseFloat(ShipPackage.Height__c) <= 0){
                errorMessages.push('Height Can not be zero at row-'+packRow);
                cmp.set('v.errorMessages',errorMessages);
            }
            }else if(parseFloat(ShipPackage.Height__c) > 0){
                var result = errorMessages.filter(function(x) {
                return x !== 'Height Can not be zero at row-'+packRow
            });
            cmp.set('v.errorMessages',result);
            
            }
           
            
           
        });
           
            
        }
        //cmp.set('v.currentStep',currentStep.toString());
        if(helper.checkValidation(cmp))
        {
            
            console.log('cmp.get v.errorMessages');
            console.log(cmp.get('v.errorMessages'));
            if(cmp.get('v.errorMessages').length == 0){
                currentStep++;
                if (cmp.get('v.business') == 'Zee') {
                    cmp.set('v.currentStep', currentStep.toString());
                }else
                    helper.createSO(cmp,currentStep,false);
                
            }
        }
        else
            helper.showToast('error','ERROR','Complete all required fields.');
    },
    parentComponentEvent : function(cmp,event,helper){
        var message = event.getParam("isSaveNew");
        helper.createSO(cmp,cmp.get('v.currentStep'),message);
    },
    finalDelchangeHandler:function(cmp, event, helper){
        var finalAddIds = cmp.get('v.finalAddIds');
        if (cmp.get('v.business') == 'Zee') {
            cmp.set('v.no_of_delivery_add_client', 1);
        }else
            cmp.set('v.no_of_delivery_add_client', 0);
        cmp.set('v.no_of_delivery_add_auto',finalAddIds.length);
    },
    checkValidation:function(cmp, event, helper){
        var toCountry = cmp.get('v.toCountry');
        var typeOfGoods = cmp.get('v.typeOfGoods');
        var fromCountry = cmp.get('v.fromCountry');
        var shipment_value = cmp.get('v.shipment_value') || 0;
        var serviceType = cmp.get('v.service_Type') ? cmp.get('v.service_Type') :'';
        var isVisible = false;
        var oldtoCountry = cmp.get('v.oldtoCountry');
        var oldfromCountry = cmp.get('v.oldfromCountry');
        var errorMessages = [];
        var countryValidation = cmp.get('v.countryValidation');

      if(!countryValidation) return;
      if(serviceType == 'EOR'){
          for(var i=0;i<toCountry.length;i++){
              if(countryValidation[fromCountry] && (countryValidation[fromCountry].EOR_Second_Hand_Goods__c  == 'Ask' || countryValidation[fromCountry].EOR_Second_Hand_Goods__c  == 'Ask and Validate')){
                  isVisible = true;
                  if(countryValidation[fromCountry].EOR_Refurbished_Goods__c  == 'Ask and Validate' && typeOfGoods == 'Refurbished'){
                      errorMessages.push('Refurbished goods may not be shipped into '+toCountry[i].Name+', TecEx cannot provide a quote with these preferences.');
                  } else if(countryValidation[fromCountry].EOR_Second_Hand_Goods__c == 'Ask and Validate' && typeOfGoods == 'Second Hand'){
                      errorMessages.push('Second hand goods may not be shipped into '+toCountry[i].Name+', TecEx cannot provide a quote with these preferences.');
                  }
              }
              if(countryValidation[fromCountry] && countryValidation[fromCountry].EOR_Fields_to_Consider__c == 'Shipment value' && countryValidation[fromCountry].EOR__c){
                  var val = parseFloat(countryValidation[fromCountry].EOR__c);
                  if(countryValidation[fromCountry].EOR_Condition__c  == 'Equals' && shipment_value == val){
                      errorMessages.push('This shipment will need to be split up into shipments with a value no higher than $ '+val);
                  }else if(countryValidation[fromCountry].EOR_Condition__c  == 'Greater than' && shipment_value > val){
                      errorMessages.push('This shipment will need to be split up into shipments with a value no higher than $ '+val);
                  }else if(countryValidation[fromCountry].EOR_Condition__c  == 'Less than' && shipment_value < val){
                      errorMessages.push('The shipment value needs to reach $'+ val);
                  }else if(countryValidation[fromCountry].EOR_Condition__c  == 'Contains'){
                  }
              }else if(countryValidation[fromCountry] && countryValidation[fromCountry].EOR_Fields_to_Consider__c  == 'Ship From Country' && countryValidation[fromCountry].EOR__c ){
                  var val = countryValidation[fromCountry].EOR__c ;
                  if(countryValidation[fromCountry].EOR_Condition__c == 'Equals' && fromCountry == val){
                      errorMessages.push('Unfortunately, EOR services out of '+fromCountry +' cannot be provided.');
                  }else if(countryValidation[fromCountry].EOR_Condition__c == 'Contains' && val.includes(fromCountry)){
                      errorMessages.push('Unfortunately, EOR services out of '+fromCountry +' cannot be provided.');
                  }
              }
                  else if(countryValidation[fromCountry] && countryValidation[fromCountry].EOR_Fields_to_Consider__c  == 'Ship to Country' && countryValidation[fromCountry].EOR__c ){
                      var val = countryValidation[fromCountry].EOR__c ;
                      if(countryValidation[fromCountry].EOR_Condition__c == 'Equals' && toCountry[i].Name == val){
                          errorMessages.push('Shipments from '+fromCountry +' to '+toCountry[i].Name +' are restricted.');
                      }else if(countryValidation[fromCountry].EOR_Condition__c == 'Contains' && val.includes(fromCountry)){
                          errorMessages.push('Shipments from '+fromCountry +' to '+toCountry[i].Name +' are restricted.');
                      }
                  }
          }
      }
      if(serviceType == 'IOR'){
          for(var i=0;i<toCountry.length;i++){
              if(countryValidation[toCountry[i].Name] && (countryValidation[toCountry[i].Name].Second_Hand_Goods__c == 'Ask' || countryValidation[toCountry[i].Name].Second_Hand_Goods__c == 'Ask and Validate')){
                  isVisible = true;
                  if(countryValidation[toCountry[i].Name].Refurbished_Goods__c == 'Ask and Validate' && typeOfGoods == 'Refurbished'){
                      errorMessages.push('Refurbished goods may not be shipped into '+toCountry[i].Name+', TecEx cannott provide a quote with these preferences.');
                  } else if(countryValidation[toCountry[i].Name].Second_Hand_Goods__c == 'Ask and Validate' && typeOfGoods == 'Second Hand'){
                      errorMessages.push('Second hand goods may not be shipped into '+toCountry[i].Name+', TecEx cannot provide a quote with these preferences.');
                  }
              }
              if(countryValidation[toCountry[i].Name] && countryValidation[toCountry[i].Name].Field_Validation_has_to_run_on__c == 'Shipment value' && countryValidation[toCountry[i].Name].Condition_Value__c){
                  var val = parseFloat(countryValidation[toCountry[i].Name].Condition_Value__c);
                  if(countryValidation[toCountry[i].Name].Condition__c == 'Equals' && shipment_value == val){
                      errorMessages.push('This shipment will need to be split up into shipments with a value no higher than $ '+val);
                  }else if(countryValidation[toCountry[i].Name].Condition__c == 'Greater than' && shipment_value > val){
                      errorMessages.push('This shipment will need to be split up into shipments with a value no higher than $ '+val);
                  }else if(countryValidation[toCountry[i].Name].Condition__c == 'Less than' && shipment_value < val){
                      errorMessages.push('The shipment value needs to reach $'+ val);
                  }else if(countryValidation[toCountry[i].Name].Condition__c == 'Contains'){
                  }
              }else if(countryValidation[toCountry[i].Name] && countryValidation[toCountry[i].Name].Field_Validation_has_to_run_on__c == 'Ship From Country' && countryValidation[toCountry[i].Name].Condition_Value__c){
                  var val = countryValidation[toCountry[i].Name].Condition_Value__c;
                  if(countryValidation[toCountry[i].Name].Condition__c == 'Equals' && fromCountry == val){
                      errorMessages.push('Shipments from '+fromCountry +' to '+toCountry[i].Name +' are restricted.');
                  }else if(countryValidation[toCountry[i].Name].Condition__c == 'Contains' && val.includes(fromCountry)){
                      errorMessages.push('Shipments from '+fromCountry +' to '+toCountry[i].Name +' are restricted.');
                  }
              }
          }
      }
      cmp.set('v.showTypeOfGood',isVisible);
      if(!isVisible){
          cmp.set('v.typeOfGoods',null);
      }
      cmp.set('v.errorMessages',errorMessages);
      if(cmp.get('v.business') == 'Zee' && cmp.get('v.fromCountry') != '' && cmp.get('v.toCountry') != '' ){
          if (cmp.get('v.fromCountry') != oldfromCountry || cmp.get('v.toCountry')[0] != oldtoCountry) {
                helper.getPreferredFreight(cmp);
                cmp.set('v.oldtoCountry', cmp.get('v.toCountry')[0]);
                cmp.set('v.oldfromCountry',cmp.get('v.fromCountry'));
                helper.checkRegistrations(cmp,cmp.get('v.toCountry')[0],cmp.get('v.accountId'));
          }
        
        }
  },
    lineItemChangeHandler:function(cmp,event,helper){
        console.log(JSON.stringify(cmp.get('v.lineItems')));
    },
    handleConfirmDialogYes: function(cmp,event,helper){
        cmp.set('v.currentStep', cmp.get('v.Step'));
        cmp.set('v.showConfirmDialog', false);
    },
    handleConfirmDialogNo: function(cmp,event,helper){
        cmp.set('v.showConfirmDialog', false);
    },
    onEdit : function(cmp,event,helper){
        var step = event.getSource().get("v.name");
        console.log('current Step',step);
        if(step == 1){
            cmp.set('v.showConfirmDialog', true);
            cmp.set('v.Step',step);
        }else{
            cmp.set('v.currentStep', step.toString());
        }
    }
})