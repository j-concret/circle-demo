({
  generateDoc : function(cmp){
    var self = this;
    self.serverCall(cmp,'generateCostEstimate',{
      soId : cmp.get('v.recordId')
    },function(response){
      if(response){
        cmp.set('v.showSpinner',false);
        cmp.set('v.msg','Your cost estimate document generate successfully , you will find under notes and attachement related list.');
      }
    },function(err){
      cmp.set('v.showSpinner',false);
      cmp.set('v.error',true);
      cmp.set('v.msg',err);
    });
  }
})