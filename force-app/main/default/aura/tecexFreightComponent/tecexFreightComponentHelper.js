({
    doInit: function(component, event){
        component.set('v.showSpinner',true);
        var action = component.get('c.getShipmentRelatedData');
        action.setParams({recId: component.get('v.recordId')});
        action.setCallback(this, function(response) {
            component.set('v.showSpinner',false);
            var state = response.getState();
            if(state == 'SUCCESS') {
                var result = response.getReturnValue();
                console.log('result ', result);
                if(result.status == 'OK'){
                    
                    console.log('DD ' + result.BDPickListWrapper.pickListMap);
                    console.log(' KK ' + result.BDPickListWrapper);
                    
                    component.set("v.pickListMap",result.BDPickListWrapper.pickListMap);
                    component.set("v.parentFieldLabel",result.BDPickListWrapper.parentFieldLabel);
                    component.set("v.childFieldLabel",result.BDPickListWrapper.childFieldLabel);
                    
                    // create a empty array for store parent picklist values 
                    var parentkeys = []; // for store all map keys 
                    var parentField = []; // for store parent picklist value to set on lightning:select. 
                    
                    // Iterate over map and store the key
                    for (var pickKey in result.BDPickListWrapper.pickListMap) {
                        parentkeys.push(pickKey);
                    }
                    
                    //set the parent field value for lightning:select
                    if (parentkeys != undefined && parentkeys.length > 0) {
                        parentField.push('--- None ---');
                    }
                    
                    for (var i = 0; i < parentkeys.length; i++) {
                        parentField.push(parentkeys[i]);
                    }  
                    // set the parent picklist
                    component.set("v.parentList", parentField);
                    
                    
                    component.set('v.shipmentOrder', result.shipmentOrder);
                    component.set('v.freightRequest', result.freightRequest);
                    component.set('v.sopList', result.sopList);
                    
                    component.set('v.dhlOptions', result.dhlContentPickList);
                    component.set('v.fedexOptions', result.fedexContentPickList);
                    
                    component.set('v.zkShipmentCount', result.zkShipmentCount);
                    component.set('v.portOfEntryWithId', result.portOfEntryWithId);
                    
                    component.set('v.oversized', result.freightRequest.Oversized__c);
                    component.set('v.nonStackable', result.freightRequest.Non_Stackable__c);
                    component.set('v.dangerousGoods', result.freightRequest.Dangerous_Goods__c);
                    
                    if(result.freightRequest.Ship_To_Address__c){
                        var finalAddress = {Id : result.freightRequest.Ship_To_Address__c,
                                            Name : result.freightRequest.Ship_To_Address__r.Name };
                    } 
                    
                    if(result.freightRequest.Ship_From_Address__c){
                        var pickUp = {Id : result.freightRequest.Ship_From_Address__c,
                                      Name : result.freightRequest.Ship_From_Address__r.Name };
                    }
                    
                    
                    component.set('v.pickUpAddress', pickUp);
                    component.set('v.finalAddress', finalAddress);
                    
                    
                    if(result.freightRequest.Lift_Gate_Required__c === 'Yes'){
                        component.set('v.liftGateRequired', true); 
                    }else{
                        component.set('v.liftGateRequired', false); 
                    }
                    
                    
                    //below handling screens visiblity order wise.
                    var steps = ['PortEntry'];
                    
                    // if(result.freightRequest.LITHIUM_BATTERIES_CELLS__c){
                    //     steps.push('SOPQuestions');
                    // }
                    
                    steps.push('SOPQuestions','SOQuestions', 'CourierRates','CourierDetails');
                    component.set('v.showAES', true);
                    component.set('v.isHSAmountMax', result.showCourierDetails);
                    
                    
                    component.set('v.stepControllerList', steps);
                    var ports = result.ports;
                    var vatPortOptions = [];
                    var nonVatPortOptions = [];
                    if(ports && ports['VAT'])
                        vatPortOptions = ports['VAT'].map(record=>({label:record,value:record}));
                    
                    if(ports && ports['NON-VAT'])
                        nonVatPortOptions = ports['NON-VAT'].map(record=>({label:record,value:record}));
                    
                    if(ports){
                        var vatOptions = (Object.keys(ports)).map(record=>({label:record,value:record}));
                        component.set('v.vatOptions',vatOptions);
                    }
                    
                    component.set('v.vatPortOptions', vatPortOptions);
                    component.set('v.nonVatPortOptions', nonVatPortOptions);
                }else if(state === "ERROR") {
                    var errors = response.getError();
                    if(errors) {
                        if(errors[0] && errors[0].message) {
                            this.showToast('Error','error',errors[0].message);
                        }
                    } else {
                        this.showToast('Error','error','Unknown Error');
                    }
                    
                }
            }
        });
        $A.enqueueAction(action);
    },
    saveDataTable : function(component, event, helper) {
        component.set('v.showSpinner',true);
        component.set('v.isNeedToFetchNewRates', true); 
        var editedRecords =  component.find("sopDataTable").get("v.draftValues");
        var totalRecordEdited = editedRecords.length;
        var action = component.get("c.updateSOP");
        action.setParams({
            'sopList' : editedRecords
        });
        action.setCallback(this,function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                //if update is successful
                var result = response.getReturnValue();
                if(result.status == 'OK'){
                    helper.showToast(
                        'SOP Record Update',
                        'success',
                        totalRecordEdited + ' Records Updated'
                    );
                    helper.reloadDataTable();
                } else{ //if update got failed
                    helper.showToast('Error', 'error','Error in SOP update!!!');
                }
            }
            component.set('v.showSpinner',false);
        });
        $A.enqueueAction(action);
    },
    /*
     * reload data table
     * */
    reloadDataTable : function(){
        var refreshEvent = $A.get("e.force:refreshView");
        if(refreshEvent){
            refreshEvent.fire();
        }
    },
    showToast : function(title,type,msg) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title:title,
            type:type,
            message:msg
        });
        toastEvent.fire();
    },
    initSOPScreen : function(component, event, helper){
        component.set('v.sopColumns', [
            {label: 'Name', fieldName: 'Name', editable:'false', type: 'text'},
            {label: '#Packages of Same Weight/Dimensions', fieldName: 'packages_of_same_weight_dims__c', editable:'true', type: 'number'},
            {label: 'Weight Unit', fieldName: 'Weight_Unit__c', editable:'true', type: 'number'},
            {label: 'Actual_Weight', fieldName: 'Actual_Weight__c', editable:'true', type: 'number'},
            {label: 'Dimension Unit', fieldName: 'Dimension_Unit__c', editable:'true', type: 'number'},
            {label: 'Length', fieldName: 'Length__c', editable:'true', type: 'number'},
            {label: 'Breadth', fieldName: 'Breadth__c', editable:'true', type: 'number'},
            {label: 'Height', fieldName: 'Height__c', editable:'true', type: 'number'},
            {label: 'ION PI966', fieldName: 'ION_PI966__c', editable:'true', type: 'boolean'},
            {label: 'ION PI967', fieldName: 'ION_PI967__c', editable:'true', type: 'boolean'},
            {label: 'Metal PI969', fieldName: 'Metal_PI969__c', editable:'true', type: 'boolean'},
            {label: 'Metal PI970', fieldName: 'Metal_PI970__c', editable:'true', type: 'boolean'}
            
        ]);  
        
    },
    fetchRatesHelper : function(component, event, helper) {
        component.set('v.showSpinner',true);
        component.set('v.columns', [
            {label: 'Courier Rates Name', fieldName: 'Name', type: 'text'},
            {label: 'Origion', fieldName: 'Origin__c', type: 'text'},
            {label: 'Carrier Name', fieldName: 'Carrier_Name__c', type: 'text'},
            {label: 'Service Type', fieldName: 'service_type__c', type: 'text'},
            {label: 'Final Rate', fieldName: 'Final_Rate__c', type: 'number'},
            {label: 'Status', fieldName: 'Status__c', type: 'text'},
            {label: 'Preferred', fieldName: 'Preferred__c', type: 'boolean'}
        ]);
        var action = component.get("c.getCourierRateData");
        action.setParams({
            recId: component.get('v.freightRequest').Id
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                component.set("v.rateList", result.rates);
                
                if(result.rates.length == 0){
                    this.showToast('Error','error','Preferred rates are not available');
                }
                for (var index in result.rates) {
                    if(result.rates[index].Status__c === 'Selected'){
                        component.set("v.selectedRows", result.rates[index].Id);
                        component.set('v.selectedCourierName', result.rates[index].Carrier_Name__c);
                        break;
                    }
                }
                
                
            }
            component.set('v.showSpinner',false);
        });
        $A.enqueueAction(action);
    },
    fetchAWBStatus : function(component, event, helper) {
        component.set('v.showSpinner',true);
        
        var action = component.get("c.getAWBCreationStatus");
        action.setParams({
            freightId: component.get('v.freightRequest').Id,
            shipmentCount : component.get('v.zkShipmentCount')
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            
            if(state == 'SUCCESS') {
                var result = response.getReturnValue();
                if(result.status == 'OK'){
                    if(result.AWBStatus === 'Created'){
                        helper.showToast('AWB Created Sucessfully','success','Freight Status Changes to Live.');  
                    }else if(result.AWBStatus === 'Failed'){
                        helper.showToast('AWB Not Created','error','Please check freight record for error information.');
                    }else{
                        helper.showToast('Manual AWB Check','error','Please check freight record for more information.');
                    }
                    
                }else if(state === "ERROR") {
                    var errors = response.getError();
                    if(errors) {
                        if(errors[0] && errors[0].message) {
                            this.showToast('Error','error',errors[0].message);
                        }
                    } else {
                        this.showToast('Error','error','Unknown Error');
                    }
                    
                }
            }
            component.set('v.showSpinner',false);
        });
        $A.enqueueAction(action);
    },
    updateSOPHelper : function(component, event, helper) {
        component.set('v.showSpinner',true);
        
        var action = component.get("c.updateSOPBatteryType");
        action.setParams({
            sop: JSON.stringify(component.get('v.sopOptionList'))
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            
            if(state == 'SUCCESS') {
                var result = response.getReturnValue();
                if(result.status == 'OK'){
                    
                }else if(state === "ERROR") {
                    var errors = response.getError();
                    if(errors) {
                        if(errors[0] && errors[0].message) {
                            this.showToast('Error','error',errors[0].message);
                        }
                    } else {
                        this.showToast('Error','error','Unknown Error');
                    }
                    
                }
            }
            component.set('v.showSpinner',false);
        });
        $A.enqueueAction(action);
    },
    checkAddressUpdation : function(component, event, helper) {
        component.set('v.showSpinner',true);
        var pickUpAdd = component.get('v.pickUpAddress');
        var fianlAdd = component.get('v.finalAddress');
        var freight = component.get('v.freightRequest');
        var addressUpdated = false;
        try{
            if(pickUpAdd.Id != freight.Ship_From_Address__c){
                component.set('v.isNeedToFetchNewRates', true);
                addressUpdated = true;
            }
            if(fianlAdd.Id != freight.Ship_To_Address__c){
                component.set('v.isNeedToFetchNewRates', true);
                addressUpdated = true;
            }
            console.log(component.get('v.isConsigneeDetailsChanged'));
            
            if(addressUpdated || component.get('v.isConsigneeDetailsChanged') || component.get('v.isConsigneeAddressChanged')){
                var action = component.get("c.updateAddresses");
                action.setParams({
                    recordId : component.get('v.freightRequest').Id,
                    selectedPort : component.get('v.portOfEntryWithId')[component.get('v.vatValue') +'##'+ component.get('v.portValue')],
                    shipTo : fianlAdd.Id,
                    shipFrom : pickUpAdd.Id, 
                    frieghtObjectString : JSON.stringify(component.get('v.freightRequest')),
                    updateConsignee : component.get('v.isConsigneeAddressChanged')
                });
                action.setCallback(this, function(response){
                    var state = response.getState();
                    
                    if(state == 'SUCCESS') {
                        var result = response.getReturnValue();
                        if(result.status == 'OK'){
                            
                            component.set('v.freightRequest', result.freightRequest);
                            component.set('v.isConsigneeDetailsChanged', false);
                            component.set('v.isConsigneeAddressChanged', false);
                            helper.doInit(component, event, helper);
                            helper.showToast('Addresses Data Refreshed','success','Please check updated addresses information.');
                            
                            
                        }
                    }else if(state === "ERROR") {
                        var errors = response.getError();
                        if(errors) {
                            if(errors[0] && errors[0].message) {
                                this.showToast('Error','error',errors[0].message);
                            }
                        } else {
                            this.showToast('Error','error','Unknown Error');
                        }
                        
                    }
                    component.set('v.showSpinner',false);
                });
                $A.enqueueAction(action);
            }else{
                component.set('v.showSpinner',false);
                this.showToast('Error','error','None of the addressees changed !!');
            }
        }catch(e){
            this.showToast('Issues Found in Address','error','Issues Found in Address');
            component.set('v.showSpinner',false);
            return;
        }
    },
    
    updateFreightDetails : function(component, event, helper) {
        component.set('v.showSpinner',true);
        
        var action = component.get("c.updateFreightData");
        action.setParams({
            oversized : component.get('v.oversized'),
            nonStackable: component.get('v.nonStackable'),
            dangerousGoods: component.get('v.dangerousGoods'),
            liftGateRequired: component.get('v.liftGateRequired'),
            recordId : component.get('v.freightRequest').Id,
            selectedPort : component.get('v.portOfEntryWithId')[component.get('v.vatValue') +'##'+ component.get('v.portValue')]
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            
            if(state == 'SUCCESS') {
                var result = response.getReturnValue();
                if(result.status == 'OK'){
                    
                }else if(state === "ERROR") {
                    var errors = response.getError();
                    if(errors) {
                        if(errors[0] && errors[0].message) {
                            this.showToast('Error','error',errors[0].message);
                        }
                    } else {
                        this.showToast('Error','error','Unknown Error');
                    }
                    
                }
            }
            
        });
        $A.enqueueAction(action);
    },
    updateCourierDetailsHelper : function(component, event, helper) {
        component.set('v.showSpinner',true);
        
        var action = component.get("c.updateCourierDetails");
        action.setParams({
            frieghtObjectString : JSON.stringify(component.get('v.freightRequest'))
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            
            if(state == 'SUCCESS') {
                var result = response.getReturnValue();
                if(result.status == 'OK'){
                    
                    window.setTimeout(
                        $A.getCallback(function() {
                            helper.fetchAWBStatus(component,event, helper)
                        }), 20000
                    );
                    
                    
                    //window.setTimeout(function(){ helper.fetchAWBStatus(component,event, helper)}, 10000);
                    //if(result.zkShipmentCount > component.get('v.zkShipmentCount')){
                    helper.showToast('Creating AWB','success','Please wait for 20 seconds.');
                    //}else{
                    //   helper.showToast('AWB Not Created','error','Please check fright record for error information');
                    //}
                }else if(state === "ERROR") {
                    var errors = response.getError();
                    if(errors) {
                        if(errors[0] && errors[0].message) {
                            this.showToast('Error','error',errors[0].message);
                        }
                    } else {
                        this.showToast('Error','error','Unknown Error');
                    }
                    
                }
            }
            component.set('v.showSpinner',false);
        });
        $A.enqueueAction(action);
    },
    updateSelectedCourierHelper : function(component, event, helper) {
        component.set('v.showSpinner',true);
        
        var action = component.get("c.updateSelectedCourierRate");
        action.setParams({
            rates : JSON.stringify(component.get('v.rateList')),
            selectedId : JSON.stringify(component.get('v.selectedRows'))
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            
            if(state == 'SUCCESS') {
                var result = response.getReturnValue();
                if(result.status == 'OK'){
                    
                }else if(state === "ERROR") {
                    var errors = response.getError();
                    if(errors) {
                        if(errors[0] && errors[0].message) {
                            this.showToast('Error','error',errors[0].message);
                        }
                    } else {
                        this.showToast('Error','error','Unknown Error');
                    }
                    
                }
            }
            component.set('v.showSpinner',false);
        });
        $A.enqueueAction(action);
    },
    
    validateConsignee : function(component, event, helper){
        let freight = component.get('v.freightRequest');
        let validationFaild = false;
        let regExp = /[a-zA-Z]/g;
        try{
            if(!freight.Con_Company__c || freight.Con_Company__c.length > 35){
                //show error
                validationFaild = true;
                this.showToast('Validation Error','error','35 Character limit exceed. Company Name and required.');
            }else if(!freight.Con_Name__c || (freight.Con_Name__c && freight.Con_Name__c.length > 35)){
                validationFaild = true;
                this.showToast('Validation Error','error','35 Character limit exceed Name and required.');
            }else if(!freight.Con_Email__c || freight.Con_Email__c.length == 0 || !freight.Con_Email__c.includes('@')){
                validationFaild = true;
                this.showToast('Validation Error','error','Consignee Email should not empty and should valid.');
            }else if(!freight.Con_Phone__c || freight.Con_Phone__c.length == 0 || regExp.test(freight.Con_Phone__c)){
                validationFaild = true;
                this.showToast('Validation Error','error','Consignee Phone should not empty and connot contains text.');
            }else if(!freight.Con_AddressLine1__c || (freight.Con_AddressLine1__c && freight.Con_AddressLine1__c.length > 35)){
                validationFaild = true;
                this.showToast('Validation Error','error','35 Character limit exceed Address line 1 and required');
            }else if(freight.Con_AddressLine2__c && freight.Con_AddressLine2__c.length > 35){
                validationFaild = true;
                this.showToast('Validation Error','error','35 Character limit exceed Address line 2.');
            }
        }catch (e){
            this.showToast('Insufficient Data','error','Insufficient Data validate consignee');
            component.set('v.validationFaild', true);
            return;
        }
        
        if(validationFaild){
            component.set('v.validationFaild', true);
            return;
        }else{
            component.set('v.validationFaild', false);
        }
        
    },
    validateSFAddress : function(component, event, helper){
        let freight = component.get('v.freightRequest');
        let validationFaild = false;
        let regExp = /[a-zA-Z]/g;
        try{
            if(!freight.SF_Company__c || freight.SF_Company__c.length > 35){
                //show error
                validationFaild = true;
                this.showToast('Validation Error','error','35 Character limit exceed. SF Company Name and required.');
            }else if(!freight.SF_Name__c || (freight.SF_Name__c && freight.SF_Name__c.length > 35)){
                validationFaild = true;
                this.showToast('Validation Error','error','35 Character limit exceed SF Name and required.');
            }else if(!freight.SF_Email__c || freight.SF_Email__c.length == 0 || !freight.SF_Email__c.includes('@')){
                validationFaild = true;
                this.showToast('Validation Error','error','SF Email should not empty and should valid.');
            }else if(!freight.SF_Phone__c || freight.SF_Phone__c.length == 0 || regExp.test(freight.SF_Phone__c)){
                validationFaild = true;
                this.showToast('Validation Error','error','SF Phone should not empty. cannot contains text');
            }else if(!freight.SF_AddressLine1__c || (freight.SF_AddressLine1__c && freight.SF_AddressLine1__c.length > 35)){
                validationFaild = true;
                this.showToast('Validation Error','error','35 Character limit exceed SF Address line 1 and required');
            }else if(freight.SF_AddressLine2__c && freight.SF_AddressLine2__c.length > 35){
                validationFaild = true;
                this.showToast('Validation Error','error','35 Character limit exceed SF Address line 2.');
            }else if(!freight.SF_City__c || freight.SF_City__c.length == 0){
                validationFaild = true;
                this.showToast('Validation Error','error','SF City should not empty.');
            }else if(!freight.SF_State__c || freight.SF_State__c.length == 0){
                validationFaild = true;
                this.showToast('Validation Error','error','SF State should not empty.');
            }else if(!freight.SF_Country__c || freight.SF_Country__c.length == 0){
                validationFaild = true;
                this.showToast('Validation Error','error','SF Country should not empty.');
            }else if(!freight.SF_Postal_Code__c || freight.SF_Postal_Code__c.length == 0){
                validationFaild = true;
                this.showToast('Validation Error','error','SF Postal code should not empty.');
            }
        }catch (e){
            this.showToast('Insufficient Data','error','Insufficient Data validateSFAddress');
            component.set('v.validationFaild', true);
            return;
        }
        
        if(validationFaild){
            component.set('v.validationFaild', true);
            return;
        }else{
            component.set('v.validationFaild', false);
        }
        
    },
    validateSTAddress : function(component, event, helper){
        let freight = component.get('v.freightRequest');
        let validationFaild = false;
        let regExp = /[a-zA-Z]/g;
        try{
            if(!freight.ST_Company__c || freight.ST_Company__c.length > 35){
                //show error
                validationFaild = true;
                this.showToast('Validation Error','error','35 Character limit exceed. ST Company Name and required.');
            }else if(!freight.ST_Name__c || (freight.ST_Name__c && freight.ST_Name__c.length > 35)){
                validationFaild = true;
                this.showToast('Validation Error','error','35 Character limit exceed ST Name and required.');
            }else if(!freight.ST_Email__c || freight.ST_Email__c.length == 0 || !freight.ST_Email__c.includes('@')){
                validationFaild = true;
                this.showToast('Validation Error','error','ST Email should not empty and should valid. ');
            }else if(!freight.ST_Phone__c || freight.ST_Phone__c.length == 0 || regExp.test(freight.ST_Phone__c)){
                validationFaild = true;
                this.showToast('Validation Error','error','ST Phone should not empty. Cannot contais text.');
            }else if(!freight.ST_AddressLine1__c || (freight.ST_AddressLine1__c && freight.ST_AddressLine1__c.length > 35)){
                validationFaild = true;
                this.showToast('Validation Error','error','35 Character limit exceed ST Address line 1 and required');
            }else if(freight.ST_AddressLine2__c && freight.ST_AddressLine2__c.length > 35){
                validationFaild = true;
                this.showToast('Validation Error','error','35 Character limit exceed ST Address line 2.');
            }else if(!freight.ST_City__c || freight.ST_City__c.length == 0){
                validationFaild = true;
                this.showToast('Validation Error','error','ST City should not empty.');
            }else if(!freight.ST_State__c || freight.ST_State__c.length == 0){
                validationFaild = true;
                this.showToast('Validation Error','error','ST State should not empty.');
            }else if(!freight.ST_Country__c || freight.ST_Country__c.length == 0){
                validationFaild = true;
                this.showToast('Validation Error','error','ST Country should not empty.');
            }else if(!freight.ST_Postal_Code__c || freight.ST_Postal_Code__c.length == 0){
                validationFaild = true;
                this.showToast('Validation Error','error','ST Postal code should not empty.');
            }
        }catch (e){
            this.showToast('Insufficient Data','error','Insufficient Data validateSTAddress');
            component.set('v.validationFaild', true);
            return;
        }
        
        if(validationFaild){
            component.set('v.validationFaild', true);
            return;
        }else{
            component.set('v.validationFaild', false);
        }
        
    },
    
    checkAESValidation : function(component, event, helper){
        let freight = component.get('v.freightRequest');
        let shipment = component.get('v.shipmentOrder');
        let validationFaild = false;
        
        let destinationCountries = ['China', 'Hong Kong', 'Venezuela', 'Russia'];
        
        try{
            if(
                shipment.Ship_From_Country__c === 'United States' 
                &&
                shipment.Destination__c !== 'Canada'
                &&
                shipment.Service_Type__c === 'IOR'
                &&
                ( 
                    (destinationCountries.includes(shipment.Destination__c))
                    ||
                    component.get('v.isHSAmountMax')
                )
                &&  
                (!freight.AES_ITN_Number__c || freight.AES_ITN_Number__c === '')
            ){
                //show error
                validationFaild = true;
                this.showToast('Validation Error','error','AES filing is required !');
            }
        }catch (e){
            console.log('error ', e);
            this.showToast('Insufficient Data','error','Insufficient Data checkAESValidation');
            component.set('v.validationFaild', true);
            return;
        }
        
        if(validationFaild){
            component.set('v.validationFaild', true);
            return;
        }else{
            component.set('v.validationFaild', false);
        }
        
    },
    
    
    subscribe : function(component, event, helper) {
        // Get the empApi component
        const empApi = component.find('empApi');
        // Get the channel from the input box
        const channel = component.find('channel').get('v.value');
        // Replay option to get new events
        const replayId = -1;
        
        // Subscribe to an event
        empApi.subscribe(channel, replayId, $A.getCallback(eventReceived => {
            // Process event (this is called each time we receive an event)
            //component.set('v.results', JSON.stringify(eventReceived.data.payload));
            console.log('Received event 1 ', eventReceived.data);
            
            console.log('Received event2  ', eventReceived.data.payload.Freight_Ids__c);
            
            
            if(eventReceived.data.payload.Freight_Ids__c && eventReceived.data.payload.Freight_Ids__c.includes(component.get('v.freightRequest').Id)){
            console.log('Here');
            if(eventReceived.data.payload.zkError__c){
            this.showToast('Error Received from zenkraft','error',eventReceived.data.payload.zkError__c);
            component.set('v.showSpinner',false);
        }else{
            helper.fetchRatesHelper(component, event, helper); 
        }	
            
            
        }
            
        }))
            .then(subscription => {
            // Confirm that we have subscribed to the event channel.
            // We haven't received an event yet.
            console.log('Subscribed to channel ', subscription.channel);
            // Save subscription to unsubscribe later
            component.set('v.subscription', subscription);
        });
        },
            
            // Invokes the unsubscribe method on the empApi component
    unsubscribe : function(component, event, helper) {
                // Get the empApi component
                const empApi = component.find('empApi');
                // Get the subscription that we saved when subscribing
                const subscription = component.get('v.subscription');
                
                // Unsubscribe from event
                empApi.unsubscribe(subscription, $A.getCallback(unsubscribed => {
                    // Confirm that we have unsubscribed from the event channel
                    console.log('Unsubscribed from channel '+ unsubscribed.subscription);
                    component.set('v.subscription', null);
                }));
           }
});