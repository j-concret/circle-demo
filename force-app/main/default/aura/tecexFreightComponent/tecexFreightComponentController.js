({
    doInit: function(component, event, helper) {
      var pageRef = component.get("v.pageReference");
      
        if(pageRef){
            if(!component.get("v.newSOPage")){
                component.set("v.recordId",pageRef.state.c__soId);
            }    
        
        component.set("v.isOpenCreateCI", true);
        component.set("v.isFromTask", true);
      }
        helper.doInit(component, event);
    },
    parentFieldChange : function(component, event, helper) {
    	var controllerValue = component.find("parentField").get("v.value");// We can also use event.getSource().get("v.value")
        var pickListMap = component.get("v.pickListMap");
        

        if (controllerValue != '--- None ---') {
             //get child picklist value
            var childValues = pickListMap[controllerValue];
            if(childValues){
                
            
            var childValueList = [];
            childValueList.push('--- None ---');
            for (var i = 0; i < childValues.length; i++) {
                childValueList.push(childValues[i]);
            }
            // set the child list
            component.set("v.childList", childValueList);
            
            if(childValues.length > 0){
                component.set("v.disabledChildField" , false);  
            }else{
                component.set("v.disabledChildField" , true); 
            }
            } 
            
        } else {
            component.set("v.childList", ['--- None ---']);
            component.set("v.disabledChildField" , true);
        }
	},
    openPickUPAddress : function (component, event, helper) {
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            "recordId": component.get("v.pickUpAddress").Id,
            "slideDevName": "related"
        });
        navEvt.fire();
    },
    openFinalAddress : function (component, event, helper) {
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            "recordId": component.get("v.finalAddress").Id,
            "slideDevName": "related"
        });
        navEvt.fire();
    }

    ,
    fetchRates: function(component, event, helper) {
        helper.fetchRatesHelper(component, event, helper);
    },
    openPortSelection: function(component, event, helper){
        component.set('v.showPortSelection', true);
    },
    closePortSelection: function(component, event, helper){
        component.set('v.showPortSelection', false);
    },
    handleConfirmDialogYes : function(component, event, helper) {
        component.set('v.showConfirmDialog', false);
        helper.updateCourierDetailsHelper(component, event, helper);
    },
    handleOnSuccess : function(component, event, helper) {
      helper.showToast('Success','success','AWB Created Sucessfully.');
    },

    handleOnError : function(component, event, helper) {
         helper.showToast('Error','error','BSO Form submission failed!');
    },

    handleConfirmDialogNo : function(component, event, helper) {
        component.set('v.showConfirmDialog', false);
    },
    createAWB: function(component, event, helper) {
   
        
        helper.checkAESValidation(component, event, helper); 
        if(component.get('v.validationFaild')){
            return;
        } 
        
        
        component.set('v.showConfirmDialog', true);

    },
    handleRefresh: function(component, event, helper) {
        helper.doInit(component, event);
    },
    handleOnSubmit : function(component, event, helper) {
        alert('Here');
    },
    onSOPSave : function (component, event, helper) {
        helper.saveDataTable(component, event, helper);
    },
    handleBack: function(component, event, helper) {
        var currentScreen = component.get('v.currentScreen');
        var steps = component.get('v.stepControllerList');
        var currentIndex = steps.indexOf(currentScreen);

        if(currentScreen == 'CourierRates'){
           component.set('v.rateList', '');
        }

        if(steps[currentIndex - 1]){
            component.set('v.currentScreen', steps[currentIndex - 1]);

            if(steps[currentIndex - 1] == 'CourierRates'){

            }

            if(steps[currentIndex - 2]){
            component.set('v.disableBack' , false);
            component.set('v.disableNext' , false);
            }else{
             component.set('v.disableBack' , true);
            }

        }else{
             component.set('v.disableBack' , true);
        }
    },

    updateAddress : function(component, event, helper) {
        helper.checkAddressUpdation(component, event, helper);
    },

    handleNext: function(component, event, helper) {
        var currentScreen = component.get('v.currentScreen');
        var steps = component.get('v.stepControllerList');
        var currentIndex = steps.indexOf(currentScreen);
        var serverCallDone = component.get('v.serverCallDone');

        //All the validation and server calls goes here.. on current Screen
        if(currentScreen == 'PortEntry'){
            helper.validateConsignee(component, event, helper);
            if(component.get('v.validationFaild')){
                return;
            }
            helper.validateSFAddress(component, event, helper);
            if(component.get('v.validationFaild')){
                return;
            }
            helper.validateSTAddress(component, event, helper);

            if(component.get('v.validationFaild')){
                return;
            }
            if(component.get('v.isConsigneeDetailsChanged')){
                helper.showToast('Data Changed!','error','Consignee Details changed please click on Amend Consignee Button First.');
                return;
            }

        }
        

        if(currentScreen == 'SOPQuestions'){
            //if(component.get('v.isNeedToFetchNewRates')){
            //    helper.updateSOPHelper(component, event, helper);
            //}
        }
        if(currentScreen == 'SOQuestions'){
            if(component.get('v.isNeedToFetchNewRates')){
                helper.updateFreightDetails(component, event, helper);
            }
        }
        if(currentScreen == 'CourierRates'){
            helper.updateSelectedCourierHelper(component, event, helper);
            helper.unsubscribe(component, event, helper);
        }

        if(steps[currentIndex + 1]){
            component.set('v.currentScreen', steps[currentIndex + 1]);
            if(steps[currentIndex + 1] == 'CourierRates'){
                if(component.get('v.isNeedToFetchNewRates')){
                    helper.subscribe(component, event, helper);
                    helper.showToast('Fetching Rates','success','Please do not close or back. Rates will be appear soon.');
                }else{
                helper.fetchRatesHelper(component, event, helper);
                }
            }

            if(steps[currentIndex + 1] == 'SOPQuestions'){
                  helper.initSOPScreen(component, event, helper);
            }

            if(steps[currentIndex + 2]){
                component.set('v.disableBack' , false);
                component.set('v.disableNext' , false);
            }else{
                component.set('v.disableNext' , true);
            }
        }else{
            component.set('v.disableNext' , true);
        }


    },

    onRowSelection: function (component, event) {
        var selectedRows = event.getParam('selectedRows');
        if(selectedRows[0].Carrier_Name__c){
            component.set('v.selectedCourierName', selectedRows[0].Carrier_Name__c);
            component.set("v.selectedRows", selectedRows[0].Id);
        }
    }
    ,
    sopOptionOnchange : function(component, event, helper){
       component.set('v.isNeedToFetchNewRates', true);
    },
    soQuestionOnChange : function(component, event, helper){
       component.set('v.isNeedToFetchNewRates', true);
    },
    handleConsigneeUpdate : function(component, event, helper){
       component.set('v.isConsigneeDetailsChanged', true);
    }
    ,
    portEntryOnChange: function(component, event, helper) {
      //var currentScreen = component.get('v.currentScreen');
      //var steps = component.get('v.stepControllerList');
      //var currentIndex = steps.indexOf(currentScreen);
      //if(steps[currentIndex + 1]){
      //      component.set('v.disableNext' , false);
      //}
      component.set('v.isNeedToFetchNewRates', true);
      component.set('v.isConsigneeAddressChanged', true);

    },
    closeCreateCIModal: function(component, event, helper) {
        component.set('v.isOpenCreateCI', false);
        component.set('v.isOpenCreateAWB', false);
        if(component.get('v.newSOPage')){
        }else{
            if(!component.get("v.isFromTask")){
          var appEvent = $A.get('e.c:closeModal');
          appEvent.fire();
        }
        }
    }
 });