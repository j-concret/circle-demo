({
  doInit:function(component, event, helper) {
    var selectedOptions = component.get("v.selectedOptions");
    var msoptions = component.get("v.msoptions");
    var values;

    if(selectedOptions && selectedOptions.length > 0){
      for(var i=0;i<msoptions.length;i++){
        for(var j=0;j<selectedOptions.length;j++){
          if(msoptions[i].value == selectedOptions[j].Name){
            msoptions[i].selected = true;
            values = values ? values+';'+msoptions[i].value: msoptions[i].value;
          }
        }
      }
      component.set("v.fieldVal",values);
      component.set("v.selectedOptions",selectedOptions);
      component.set("v.msoptions",msoptions);
    }
  },
  onRender : function(component, event, helper) {
    if(!component.get('v.initializationCompleted')){
      //Attaching document listener to detect clicks
      component.getElement().addEventListener('click', function(event){
        //handle click component
        helper.handleClick(component, event, 'component');
      });
      //Document listner to detect click outside multi select component
      document.addEventListener('click', function(event){
        helper.handleClick(component, event, 'document');
      });
      //Marking initializationCompleted property true
      component.set('v.initializationCompleted', true);
      //Set picklist name
      helper.setPickListName(component, component.get('v.selectedOptions'));
    }

  },
  onInputChange : function(component, event, helper) {
    //get input box's value
    var inputText = event.target.value;
    //Filter options
    helper.filterDropDownValues(component, inputText);
  },
  onRefreshClick : function(component, event, helper) {
    //clear selected options
    component.set('v.selectedOptions', []);
    component.set('v.fieldVal',null);
    component.set('v.selectedLabel','Select a value..');
    var msoptions = component.get("v.msoptions");


      for(var i=0;i<msoptions.length;i++){
              msoptions[i].selected = false;
             }
      component.set("v.msoptions",msoptions);
    //Clear check mark from drop down items
    helper.rebuildPicklist(component);
    //Set picklist name
    helper.setPickListName(component, component.get('v.selectedOptions'));
  },
  onClearClick : function(component, event, helper) {
    //clear filter input box
    component.getElement().querySelector('#ms-filter-input').value = '';
    //reset filter
    helper.resetAllFilters(component);
  },
});