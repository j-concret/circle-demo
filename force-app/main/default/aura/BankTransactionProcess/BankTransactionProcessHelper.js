({
    closeOnClick: function(component) {
        //location.reload();
        
        
        
        var outerDiv = document.getElementById('divOuter');
        outerDiv.classList.add("backgroundImage");
        outerDiv.classList.remove("relPos");
        component.destroy();
    },
    getWorkingDay: function(cmp) {
        var additionalDays =  -1 ;
        // Check if parameter 'daysFromToday' is correct type
        var days;
        if(!isNaN(parseInt(additionalDays))) 
            days = parseInt(additionalDays);
        else days = 0;
        
        // Get today's date and set new date based on parameter value
        var today = new Date();
        today.setDate(today.getDate() + days);
        
        // Sun, minus two days to get to Friday
        if (today.getDay() == 0) {
            today.setDate(today.getDate() - 2);
        }
        // Sat, minus one day to get to Friday
        else if (today.getDay() == 6) {
            today.setDate(today.getDate() - 1);
        } 
        cmp.set('v.bankTransaction.bankOpeningBalance',0);
        cmp.set('v.bankTransaction.bankClosingBalance',0);
        // Return required date string
        //2020-09-07
        //var bankTransation = cpm.get('v.bankTransaction');
        cmp.set('v.bankTransaction.Date__c',(today.getFullYear()+"-"+(today.getMonth()+1) + "-" + today.getDate())) 
        //return (today.getMonth() + "/" + today.getDate() + "/" + today.getFullYear());
    },
    bankTransactionListChange: function(component, event) {
        var bankTransactionList = component.get("v.bankTransactionList");
        var payment = 0;
        var receipt = 0;
        component.set('v.isProcessAccounts',true);
        for(let bankTransaction of bankTransactionList){
            if(bankTransaction.Supplier__c != "undefined" && bankTransaction.Supplier__c.Name){
                component.set('v.isProcessAccounts',false);
            }
            let amount = bankTransaction.Amount__c ?  bankTransaction.Amount__c :0;
            let bankCharge = bankTransaction.Bank_charges__c ? bankTransaction.Bank_charges__c : 0;
            let recordTypeMap = component.get('v.recordTypeMap');
            let recId = bankTransaction.RecordTypeId;
            let record = recordTypeMap[recId];
            if(record.Name == "Receipt"){
                receipt += (parseFloat(amount));
            }else if(record.Name == "Payment"){
                payment += (parseFloat(amount));
            }
        }
        let actualMovement = ((parseFloat(receipt)-parseFloat(payment))).toFixed(2);
        actualMovement = actualMovement ? actualMovement : 0;
        component.set('v.bankTransaction.bankTotalReceipt',receipt.toFixed(2)?receipt.toFixed(2):0);
        component.set('v.bankTransaction.bankTotalPayment',payment.toFixed(2)?payment.toFixed(2):0);
        component.set('v.bankTransaction.bankActualMovement',actualMovement?actualMovement:0);
        var movementDiv = component.find('movement');
        let movement = component.get('v.bankTransaction.bankBalanceMovement')?component.get('v.bankTransaction.bankBalanceMovement'):0;
        if(movement == actualMovement){
            $A.util.removeClass(movementDiv, 'unmatchedBackground');
            $A.util.addClass(movementDiv, 'matchedBackground');
            component.set('v.isMovementsNotEqual', false);
            component.set('v.isCaptureClicked', false);
        }
        else{
            $A.util.removeClass(movementDiv, 'matchedBackground');
            $A.util.addClass(movementDiv, 'unmatchedBackground');
            component.set('v.isMovementsNotEqual', true);
            component.set('v.isCaptureClicked', true);
        }
        
    },
    getAllRecordTypes: function(component, event) {
        var that = this;
        var action = component.get("c.getRecordTypes");
        action.setParams({
            'objectName':'Bank_transactions__c'
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS") {
                var result = response.getReturnValue();
                if(result.status === "OK") {
                    component.set('v.recordTypeMap',result.recordTypeList);
                    component.set('v.user',result.user);
                    component.set('v.recordTypeList',Object.values(result.recordTypeList));
                    that.createObjectData(component, event);
                } else {
                    //that.fireToast('ERROR', 'error', result.msg);
                    console.log(result.msg);
                }
            }
            else if(state === "ERROR") {
                var errors = response.getError();
                if(errors) {
                    if(errors[0] && errors[0].message) {
                        console.log(errors[0].message);
                        //that.fireToast('ERROR', 'error', errors[0].message);
                    }
                } else {
                    //component.set('v.loaded', !component.get('v.loaded'));
                    // that.fireToast('ERROR', 'error', "Unknown error");
                }
                
            }
        });
        $A.enqueueAction(action);
    },
    getBankTransactionAppliedRecordTypes: function(component, event) {
        var that = this;
        var action = component.get("c.getRecordTypes");
        action.setParams({
            'objectName':'Bank_Transaction_Applied__c'
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS") {
                var result = response.getReturnValue();
                if(result.status === "OK") {
                    component.set('v.bTARecordTypeMap',result.recordTypeList);
                } else {
                    //that.fireToast('ERROR', 'error', result.msg);
                    console.log(result.msg);
                }
            }
            else if(state === "ERROR") {
                var errors = response.getError();
                if(errors) {
                    if(errors[0] && errors[0].message) {
                        console.log(errors[0].message);
                        //that.fireToast('ERROR', 'error', errors[0].message);
                    }
                } else {
                    //component.set('v.loaded', !component.get('v.loaded'));
                    // that.fireToast('ERROR', 'error', "Unknown error");
                }
                
            }
        });
        $A.enqueueAction(action);
    },
    createObjectData: function(component, event) {
        // get the bankTransactionList from component and add(push) New Object to List  
        var RowItemList = component.get("v.bankTransactionList");
        var bankTransaction = component.get("v.bankTransaction");
        var RecordId = '';
        for(let record of component.get('v.recordTypeList')){
            if(record.Name == 'Receipt'){
                RecordId = record.Id;
            }
        }
        RowItemList.push({
            'Supplier__c': '',
            'Amount__c':0,
            'Bank_charges__c':0,
            'RecordTypeId':RecordId
        });
        // set the updated list to attribute (bankTransactionList) again    
        component.set("v.bankTransactionList", RowItemList);
    },
     getCapture: function(component, event) {
        var user = component.get("v.user");
        var bankTransactionList = component.get("v.bankTransactionList");
        var bankTransaction = component.get("v.bankTransaction");
        var commentRow = 0;
        var wb = XLSX.utils.book_new();
        wb.Props = {
            Title: "SheetJS Tutorial",
            Subject: "Test",
            Author: "Red Stapler",
            CreatedDate: new Date()
        };
         var fmt = '$#,##0.00';
        wb.SheetNames.push("Bank Transactions");
        var ws_data = [['Bank Account','Date','Opening Balance','Closing Balance','Movement'],
                       [bankTransaction.Bank_Account_New__c.Name,bankTransaction.Date__c,Number(bankTransaction.bankOpeningBalance),Number(bankTransaction.bankClosingBalance),Number(bankTransaction.bankBalanceMovement)],
                       [],[],
                       ['ACCOUNT','AMOUNT','BANK CHARGES','TYPE','TOTAL TO BE APPLIED']
                      ];
        var recordTypeMap = component.get('v.recordTypeMap');
        
        for(var bnkTransaction of bankTransactionList){
            
            var recId = bnkTransaction.RecordTypeId;
            var record = recordTypeMap[recId];
            var row = [bnkTransaction.Supplier__c.Name,
                       Number(bnkTransaction.Amount__c),
                       Number(bnkTransaction.Bank_charges__c),
                       record.Name,
                       Number(bnkTransaction.bankBalanceMovement)];
            ws_data.push(row);
        }
        ws_data.push([]);
        ws_data.push(['Total Receipt',Number(bankTransaction.bankTotalReceipt)]);
        ws_data.push(['Total Payment',Number(bankTransaction.bankTotalPayment)]);
        ws_data.push(['Actual Movement',Number(bankTransaction.bankActualMovement)]);
        
        var comments = bankTransaction.comment;
        //for(var i=0;i<comments.length;i++){
            ws_data.push(['Comment',comments]);
         commentRow = ws_data.length;
        //ws_data.push([comments]);
        
            
        //}
        
        ws_data.push([]);
        ws_data.push(['User',user.Username]);
        ws_data.push(['Email',user.Email]);
        var timezone = $A.get("$Locale.timezone");
        var mydate = new Date().toLocaleString("en-US", {timeZone: timezone});
        //var current_datetime = new Date()
        //var formatted_date = (current_datetime.getMonth() + 1)+'/'+current_datetime.getDate()+'/'+current_datetime.getFullYear() + " " + current_datetime.getHours() + ":" + current_datetime.getMinutes();
        ws_data.push(['Date',mydate]);
        
        
        console.log('commentRow ',commentRow);
         console.log('ws_data ',ws_data.length);
        var ws = XLSX.utils.aoa_to_sheet(ws_data);
         for(var i =1; i <6 ; i++){
         ws[String.fromCharCode(64+i)+1]['s'] = {
                bold:true,
                alignment: {horizontal: "center"}
            };
             ws[String.fromCharCode(64+i)+5]['s'] = {
                bold:true,
                alignment: {horizontal: "center"}
            };
         }
         if(ws['C'+2]){
             ws['C'+2]['z'] = fmt;
         }
         if(ws['D'+2]){
             ws['D'+2]['z'] = fmt;
         }
         if(ws['E'+2]){
             ws['E'+2]['z'] = fmt;
         }
         
         var rowDataValues = 5+bankTransactionList.length;
         for(var i=6 ; i <=rowDataValues ; i++ ){
              if(ws['B'+i]){
             ws['B'+i]['z'] = fmt;
         }
         if(ws['C'+i]){
             ws['C'+i]['z'] = fmt;
         }
         if(ws['E'+i]){
             ws['E'+i]['z'] = fmt;
         }
         }
           if(ws['B'+(rowDataValues+2)]){
             ws['B'+(rowDataValues+2)]['z'] = fmt;
         }
         if(ws['B'+(rowDataValues+3)]){
             ws['B'+(rowDataValues+3)]['z'] = fmt;
         }
         if(ws['B'+(rowDataValues+4)]){
             ws['B'+(rowDataValues+4)]['z'] = fmt;
         }
         if(ws['A'+(rowDataValues+2)]){
             ws['A'+(rowDataValues+2)]['s'] ={ bold:true,
                                              alignment: {horizontal: "center"}
                                             };
         }
         if(ws['A'+(rowDataValues+3)]){
             ws['A'+(rowDataValues+3)]['s'] ={ bold:true,
                                              alignment: {horizontal: "center"}
                                             };
         }
         if(ws['A'+(rowDataValues+4)]){
             ws['A'+(rowDataValues+4)]['s'] ={ bold:true,
                                              alignment: {horizontal: "center"}
                                             };
         }
        if( ws['B'+commentRow] ){
        ws['B'+commentRow]['s'] = {alignment:{ wrapText: true }};
        }
         if( ws['A'+commentRow] ){
        ws['A'+commentRow]['s'] ={ bold:true,
                                              alignment: {vertical: "center",horizontal: "center"}
                                             };
        }
          if( ws['A'+(commentRow+2)] ){
        ws['A'+(commentRow+2)]['s'] ={ bold:true,
                                              alignment: {horizontal: "center"}
                                             };
        }
          if( ws['A'+(commentRow+3)] ){
        ws['A'+(commentRow+3)]['s'] ={ bold:true,
                                              alignment: {horizontal: "center"}
                                             };
        }
          if( ws['A'+(commentRow+4)] ){
        ws['A'+(commentRow+4)]['s'] ={ bold:true,
                                              alignment: {horizontal: "center"}
                                             };
        }
        var wscols = [
            {wch:20},
            {wch:20},
            {wch:20},
            {wch:20},
            {wch:20}
        ];
        
        ws['!cols'] = wscols;
        wb.Sheets["Bank Transactions"] = ws;
        //XLSX.writeFile(wb, 'Test.xls', {bookType:'xlsx',cellStyles: true});
        var wbout = XLSX.write(wb, { bookType:'xlsx', cellStyles: true,bookSST:false, type:'base64'});
        var action = component.get("c.sendBankTransactions");
        action.setParams({
            'blobData':wbout
        });
        action.setCallback(this, function(response) {
            
            var state = response.getState();
            if(state === "SUCCESS") {
                var result = response.getReturnValue();
                if(result.status === "OK") {
                    console.log('Email Sent');
                    component.set('v.isEmailSent',true);
                    component.set('v.isCommentEmpty',false);
                    component.set('v.isCaptureClicked', false);
                    //that.fireToast('Success', 'success', 'Parts mapped successfully!');
                    
                } else {
                    //that.fireToast('ERROR', 'error', result.msg);
                    console.log(result.msg);
                }
            }
            else if(state === "ERROR") {
                var errors = response.getError();
                if(errors) {
                    if(errors[0] && errors[0].message) {
                        console.log(errors[0].message);
                        //that.fireToast('ERROR', 'error', errors[0].message);
                    }
                } else {
                    //component.set('v.loaded', !component.get('v.loaded'));
                    // that.fireToast('ERROR', 'error', "Unknown error");
                }
                
            }
        });
        $A.enqueueAction(action);
    },
    sortTrackingNo:function (array, order) {
    return array.sort(order === 'DESC'
        ? function (b, a) {
            a = (a.name);
            b = (b.name);
            return isNaN(b) - isNaN(a) || a > b || -(a < b);
        }
        : function (a, b) {
            a = (a.name);
            b = (b.name);
            return isNaN(a) - isNaN(b) || a > b || -(a < b);
        });
    }
})