({
    doInit : function(component, event, helper) {
        helper.getWorkingDay(component);
        helper.getAllRecordTypes(component, event);
        helper.getBankTransactionAppliedRecordTypes(component, event);    
    },
    Capture: function(component, event, helper) {
        //var comment = ;
        if(component.get('v.bankTransaction.comment')){
            component.set('v.isCommentEmpty',false);
            component.set('v.isEmailSent',false);
             helper.getCapture(component, event);
        }else{
            component.set('v.isCommentEmpty',true);
            component.set('v.isEmailSent',false);
        }
        
       
    },
    closedChanged:function(component, event, helper){
        if(component.get('v.isExistingComponentClosed')){
            helper.closeOnClick(component);
        }
        
    },
    Close:function(component, event, helper){
        helper.closeOnClick(component);
    },
    keyCheck: function(component, event, helper){
        let bankCloseBal = component.get('v.bankTransaction.bankClosingBalance') ? component.get('v.bankTransaction.bankClosingBalance') : 0;
        let bankOpenBal = component.get('v.bankTransaction.bankOpeningBalance') ? component.get('v.bankTransaction.bankOpeningBalance') : 0;
        var bankBalanceMovement = bankCloseBal  - bankOpenBal;
        component.set('v.bankTransaction.bankBalanceMovement', bankBalanceMovement.toFixed(2) ? bankBalanceMovement.toFixed(2) : 0);
        helper.bankTransactionListChange(component, event);
    },
    // function for create new object Row in Contact List 
    addNewRow: function(component, event, helper) {
        // call the comman "createObjectData" helper method for add new Object Row to List  
        helper.createObjectData(component, event);
    },
    AddNewRow : function(component, event, helper){
        // fire the AddNewRowEvt Lightning Event 
        component.getEvent("AddRowEvt").fire();     
    },
    bankTransactionChange: function(component, event, helper){
        var bankTransaction = component.get('v.bankTransaction');
        if(typeof bankTransaction.Bank_Account_New__c != 'undefined' && typeof bankTransaction.Bank_Account_New__c.Name != 'undefined') {
            component.set("v.bankTransactionList", []);
            helper.createObjectData(component, event);
        }
    },
    // function for delete the row 
    removeDeletedRow: function(component, event, helper) {
        // get the selected row Index for delete, from Lightning Event Attribute  
        var index = event.getParam("indexVar");
        // get the all List (bankTransactionList attribute) and remove the Object Element Using splice method    
        var AllRowsList = component.get("v.bankTransactionList");
        AllRowsList.splice(index, 1);
        // set the bankTransactionList after remove selected row element  
        component.set("v.bankTransactionList", AllRowsList);
    },
    bankTransactionListChange: function(component, event, helper) {
        helper.bankTransactionListChange(component, event);
        
        
    },
    // function for save the Records 
    Save: function(component, event, helper) {
        var that = this;
        component.set('v.isLoading',true);
        console.log('Bank Transaction List');
        console.log(JSON.stringify(component.get("v.bankTransactionList")));
        console.log(JSON.stringify(component.get("v.bankTransaction")));
        var bankTransactionList = component.get("v.bankTransactionList");
        var bankTransaction = component.get("v.bankTransaction");
        var bankTransList = [];
        for(let bnkTransaction of bankTransactionList){
            let bnkJson = {};
            if(bnkTransaction.Supplier__c){
                bnkJson.Bank_Account_New__c = bankTransaction.Bank_Account_New__c.Id;
                bnkJson.Date__c = bankTransaction.Date__c;
                bnkJson.Supplier__c = bnkTransaction.Supplier__c.Id;
                bnkJson.Amount__c = bnkTransaction.Amount__c;
                bnkJson.Bank_charges__c = bnkTransaction.Bank_charges__c;
                bnkJson.RecordTypeId = bnkTransaction.RecordTypeId;
                bankTransList.push(bnkJson);
            }
            
        }
        var action = component.get("c.createBankTransactions");
        action.setParams({
            'bankTransactionJson':JSON.stringify(bankTransList)
        });
        action.setCallback(this, function(response) {
            
            var state = response.getState();
            if(state === "SUCCESS") {
                var result = response.getReturnValue();
                if(result.status === "OK") {
                    console.log('Okay')
                    var sortedTrack = helper.sortTrackingNo(result.data.bTInvoiceList);
                    console.log(sortedTrack);
                    component.set('v.bTInvoiceList',sortedTrack);
                    component.set('v.isBTAEnabled',true);
                    component.set('v.isLoading',false);
                    //that.fireToast('Success', 'success', 'Parts mapped successfully!');
                    
                } else {
                    //that.fireToast('ERROR', 'error', result.msg);
                    console.log(result.msg);
                    component.set('v.isLoading',false);
                }
            }
            else if(state === "ERROR") {
                var errors = response.getError();
                if(errors) {
                    if(errors[0] && errors[0].message) {
                        console.log(errors[0].message);
                        component.set('v.isLoading',false);
                        //that.fireToast('ERROR', 'error', errors[0].message);
                    }
                } else {
                    //component.set('v.loaded', !component.get('v.loaded'));
                    // that.fireToast('ERROR', 'error', "Unknown error");
                    component.set('v.isLoading',false);
                }
                
            }
        });
        $A.enqueueAction(action);
        
        
    } 
    
})