({
    doInit:function(cmp,event,helper){
         let button = cmp.find('disablebuttonid');
         var CreatedRollOutID = cmp.get("v.recordId");
         console.log("CreatedRollOutID -->" +CreatedRollOutID);
         cmp.set('v.SelectedRollOut_id',CreatedRollOutID);
         
         button.set('v.disabled',true);
         helper.Fetchexistingaccount(cmp,event,helper);
           
    },
    
    onMultiSelectChange: function(cmp) {
         var selectCmp = cmp.find("InputSelectMultiple");
         var resultCmp = cmp.find("multiResult");
         resultCmp.set("v.value", selectCmp.get("v.value"));
        
        
        var button1 = cmp.find("multiResult").get("v.value");
        var button2 = cmp.get("v.data");
         console.log(button1);
       
        // Ship to list size
        var Shipto = cmp.find("multiResult").get("v.value");
        var shiptoarr = Shipto.split(";");
        console.log("Shipto to size --->" +shiptoarr.length);
        cmp.set('v.ShiptoSize',shiptoarr.length);
        
        
         if(button1 !== ""){
         		let button = cmp.find('disablebuttonid');
         		button.set('v.disabled',false);
        	}
        else{
            	let button = cmp.find('disablebuttonid');
        		button.set('v.disabled',true);
        	}
        
        
    },
    onAddMoreMultiSelectChange: function(cmp) {
      
         var selectCmp = cmp.find("AddMoreInputSelectMultiple");
         var resultCmp = cmp.find("multiResult1");
         resultCmp.set("v.value", selectCmp.get("v.value"));
           
    },
    
    save: function (cmp, event, helper) {
         
         cmp.set("v.is_loading", true); 
         var draftValues = cmp.find("multiResult").get("v.value");
         var accId = cmp.get("v.account_id");
         var conId = cmp.get("v.contact_id");
         var shipmentvalueUSD = cmp.get("v.shipment_value");
         var Courierresponsible = cmp.get("v.courierResponsible");
         var ShipFromCountry = cmp.get("v.ship_from");
         
       
         
        console.log("Shipto Countries " +draftValues);
        
        console.log("Account ID "+accId);
        console.log("Contact ID "+conId);
        console.log("SO Value "+shipmentvalueUSD);
        console.log("Courier Resp "+Courierresponsible);
        console.log("Shipfrom " +ShipFromCountry);
        
         cmp.set('v.ShipTo',draftValues);
       
        
        var action = cmp.get("c.CreateROSO");
        action.setParams({
        "AccID" : accId,
        "ConId" : conId,
        "Destinations" : draftValues,
        "ShipFrom" : ShipFromCountry,
        "SOValue" : cmp.get("v.shipment_value"),
        "Courierresp" : Courierresponsible,
        "Reff1" : cmp.get("v.Ref1"),
        "Reff2" : cmp.get("v.Ref2"),
         "ExistingrolloutID" : cmp.get("v.SelectedRollOut_id"),
          
        
        });
        action.setCallback(this, function(response) {
           
            var state = response.getState();
            if (state === 'SUCCESS'){
                   cmp.set("v.is_loading", false);         
                var data = response.getReturnValue();
                cmp.set('v.data',data);
                cmp.set('v.CreatedRollout',data[0].Roll_Out__c);
                cmp.set('v.Uship_from',data[0].Ship_From_Country__c);
                
                console.log(data);
                /* if(data !== ""){
                	let button = cmp.find('disablebuttonid');
                	button.set('v.disabled',true);
                    let Addbutton = cmp.find('Addmorebuttonid');
        		  	Addbutton.set('v.disabled',false);
                        }
                */
            }
            else{
                alert("Can we please review these");
            }
            
        });
        $A.enqueueAction(action);
        
    },
   
    update: function (cmp, event, helper) {
          cmp.set("v.is_loading", true); 
         var UdraftValues = cmp.find("multiResult1").get("v.value");
         var action = cmp.get("c.CreateMoreROSO");
       
        
        action.setParams({
        "AccID" : cmp.get("v.account_id"),
        "ConId" : cmp.get("v.contact_id"),
        "Destinations" : UdraftValues,
        "ShipFrom" : cmp.get("v.ship_from"),
        "SOValue" : cmp.get("v.Ushipment_value"),
        "Courierresp" : cmp.get("v.UcourierResponsible"),
        "Reff1" : cmp.get("v.URef1"),
        "Reff2" : cmp.get("v.URef2"),
        "RolloutId" : cmp.get("v.CreatedRollout")
                 
        });
        action.setCallback(this, function(response) {
             cmp.set("v.is_loading", false); 
           
            var state = response.getState();
            if (state === 'SUCCESS'){
                           
                var data = response.getReturnValue();
                console.log(data);
                alertify.alert('New Cost Estimates are created and added to existing Rollout');
                cmp.set("v.MoreDestinations", 'No');
               
            }
            else{
                alert("Can we please review these");
            }
            
        });
        $A.enqueueAction(action);
        
    },
    UpdateChargableWeightJ: function (cmp, event, helper) {
        cmp.set("v.is_loading", true);
         
        
         var action3 = cmp.get("c.UpdateChargableWeight");
        var CrRolloutId = cmp.get("v.CreatedRollout");
        
        action3.setParams({
        
         Chweight : cmp.get("v.EnteredChargableWeight"),
         RolloutId : cmp.get("v.CreatedRollout")
                 
        });
        
        
        action3.setCallback(this, function(response) {
            cmp.set("v.is_loading", false); 
            var state = response.getState();
            if (state === 'SUCCESS'){
                           
                var data3 = response.getReturnValue();
                console.log(data3);
                alertify.alert('Chargable Weight updated to all cost estimates');
                window.location.replace("https://tecex.lightning.force.com/lightning/r/Roll_Out__c/"+CrRolloutId+"/view");
              //  cmp.set("v.ChargableWorPackages", 'No');
               
            }
            else{
                alert("Can we please review these");
            }
            
        });
        $A.enqueueAction(action3);
        
    },
    addRow: function(component, event, helper) {
        helper.addAccountRecord(component, event);
    },
     
    removeRow: function(component, event, helper) {
        //Get the account list
        var accountList = component.get("v.accountList");
        //Get the target object
        var selectedItem = event.currentTarget;
        //Get the selected item index
        var index = selectedItem.dataset.record;
        accountList.splice(index, 1);
        component.set("v.accountList", accountList);
    },
     
    CreatePacakges: function(component, event, helper) {
      //  if (helper.validateAccountList(component, event)) {
            helper.saveAccountList(component, event);
             
       // }
    },
    waiting: function(component, event, helper) {
  component.set("v.HideSpinner", true);
 },
 doneWaiting: function(component, event, helper) {
  component.set("v.HideSpinner", false);
 }
    
    
})