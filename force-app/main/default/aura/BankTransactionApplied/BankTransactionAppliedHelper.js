({
	sortInvoicesByDisabledInvoices : function(component, event) {
        var filterInvoiceList = component.get('v.filteredInvoiceList');
		 filterInvoiceList.sort(function(x, y) {
        // true values first
        return x.isInvoiceDisabled ? -1 : 1;
        // false values first
        // return (x === y)? 0 : x? 1 : -1;
    });
        component.set('v.filteredInvoiceList',filterInvoiceList);
	},
    chunk: function(array, count) {
    if (count == null || count < 1) return [];
    var result = [];
    var i = 0, length = array.length;
    while (i < length) {
      result.push(Array.prototype.slice.call(array, i, i += count));
    }
    return result;
  	},
    showToast : function(title, type, msg) {
    var toastEvent = $A.get("e.force:showToast");
    toastEvent.setParams({
        "title": title,
        "message": msg,
        "type":type
    });
    toastEvent.fire();
}
})