({
    doInit : function(component, event, helper) {
    
    var bTInvoiceList = component.get('v.bTInvoice.invoiceList')
    for(let bTInvoice of bTInvoiceList){
        bTInvoice.isInvoiceSelected = false;
        bTInvoice.isInvoiceDisabled = false;
    }
    component.set('v.bTInvoice.invoiceList',bTInvoiceList);
    component.set('v.filteredInvoiceList',bTInvoiceList);
  
},
  showInvoicesDetail : function(component, event, helper) {
      component.set('v.isOpen',!component.get('v.isOpen'));
  },
  onFilterInvoices: function(component, event, helper) {
      var selectedCount =  component.get('v.selectedInvoices');
      var invoicelist = component.get('v.bTInvoice.invoiceList');
      var filter = component.get('v.filter');
      var filteredData = [];
      var searchType = component.get('v.searchType');
      if(filter){
          if(searchType == 'Invoice'){
              filteredData = invoicelist.filter(data => {
                  return data.Name ? data.Name.toLowerCase().includes(filter.toLowerCase()) : false;
              });
              component.set('v.filteredInvoiceList',filteredData);
          }else{
              filteredData = invoicelist.filter(data => {
                  return data.Invoice_Name__c ? data.Invoice_Name__c.toLowerCase().includes(filter.toLowerCase()) : false;
              });
              component.set('v.filteredInvoiceList',filteredData);
          }
      }else{
          component.set('v.filteredInvoiceList',component.get('v.bTInvoice.invoiceList'));
      }
        component.set('v.selectedInvoices',selectedCount);
      helper.sortInvoicesByDisabledInvoices(component, event);
  },
  updateBTStatus: function(component, event, helper) {
      component.set('v.isLoading',true);
      var bTInvoice = component.get('v.bTInvoice');
      var bankTransactionObj = {};
      bankTransactionObj.Id = bTInvoice.bankTransactionId;
      bankTransactionObj.BT_Status__c = "Forex difference";
      bankTransactionObj.Post_Forex__c = true; 
      bankTransactionObj.Supplier__c = bTInvoice.supplierId;
      var bankTransactionList = [bankTransactionObj];
      var action = component.get("c.createBankTransactions");
      action.setParams({
          'bankTransactionJson':JSON.stringify(bankTransactionList)
      });
      action.setCallback(this, function(response) {
          
          var state = response.getState();
          if(state === "SUCCESS") {
              var result = response.getReturnValue();
              if(result.status === "OK") {
                  var resultantbT = result.data.bTInvoiceList[0];
                  var bT = component.get('v.bTInvoice');
                  bT.name = resultantbT.name;
                  bT.supplierName = resultantbT.supplierName;
                  bT.unapplied = resultantbT.unapplied;
                  bT.appliedAmount = resultantbT.appliedAmount;
                  bT.btStatus = resultantbT.btStatus;
                  for(let bTInvoice of bT.invoiceList){
                      if(bTInvoice.isInvoiceSelected && !bTInvoice.isInvoiceDisabled){
                         bTInvoice.isInvoiceSelected = false;
                      }
                      bTInvoice.isInvoiceDisabled = true;
                  }
                  component.set('v.filteredInvoiceList',bT.invoiceList);
                  component.set('v.bTInvoice',bT);
                  component.set('v.isLoading',false);
                  component.set('v.selectedInvoices',0);
              } else {
                  //that.fireToast('ERROR', 'error', result.msg);
                  console.log(result.msg);
                  component.set('v.isLoading',false);
              }
          }
          else if(state === "ERROR") {
              var errors = response.getError();
              if(errors) {
                  if(errors[0] && errors[0].message) {
                      console.log(errors[0].message);
                      component.set('v.isLoading',false);
                      //that.fireToast('ERROR', 'error', errors[0].message);
                  }
              } else {
                  component.set('v.isLoading',false);
                  //component.set('v.loaded', !component.get('v.loaded'));
                  // that.fireToast('ERROR', 'error', "Unknown error");
              }
              
          }
      });
      $A.enqueueAction(action);
  },
  createBankTransactionApplied : function(component, event, helper) {
      component.set('v.isLoading',true);
      var bTInvoiceList = component.get('v.bTInvoice');
      var bankTransactionAppliedList = [];
      var bTARecordTypeList = Object.values(component.get('v.bTARecordTypeMap'));
      var recordTypeId = '';
      
      for(let recordType of bTARecordTypeList){
          if(recordType.Name.includes(bTInvoiceList.RecordTypeName)){
              recordTypeId = recordType.Id;
          }
      }
      var filterInvoiceList = component.get('v.filteredInvoiceList');
      for(let bTInvoice of bTInvoiceList.invoiceList){
          var bankTransactionApplied = {};
          if(bTInvoice.isInvoiceSelected && !bTInvoice.isInvoiceDisabled)
          {
              bTInvoice.isInvoiceDisabled = true;
              bankTransactionApplied.Account__c = bTInvoiceList.supplierId;
              bankTransactionApplied.RecordTypeId = recordTypeId;
              bankTransactionApplied.Invoice__c = bTInvoice.Id ;
              bankTransactionApplied.Amount__c = bTInvoice.Amount_Outstanding__c ;
              bankTransactionApplied.Total_Value_Applied_To_Penalty__c  = bTInvoice.Penalty_Outstanding_Amount__c ;
              bankTransactionApplied.Bank_Transaction__c = bTInvoiceList.bankTransactionId;
              bankTransactionAppliedList.push(bankTransactionApplied);
          }
          
      }
      component.set('v.filteredInvoiceList',filterInvoiceList);
      component.set('v.bankTransactionAppliedList',bankTransactionAppliedList);
      component.set('v.bTInvoice.invoiceList',bTInvoiceList.invoiceList);
      console.log('createBankTransactionApplied');
      console.log(JSON.stringify(component.get('v.bankTransactionAppliedList')));
      helper.sortInvoicesByDisabledInvoices(component, event);
      var bankTransactionAppliedListOfChunk = helper.chunk(bankTransactionAppliedList,25);
      console.log('bankTransactionAppliedListOfChunk');
      console.log(bankTransactionAppliedListOfChunk);
      console.log('bankTransactionAppliedList');
      console.log(bankTransactionAppliedList);
      /*bankTransactionAppliedListOfChunk.forEach((bankTransactionAList)=>{
          
      })*/
       var chunkIndex = 0;
      for(let bankTransactionAList of bankTransactionAppliedListOfChunk){
         
      var action = component.get("c.createBankTransactionsApplied");
      action.setParams({
          'bankTransactionId':bTInvoiceList.bankTransactionId,
          'SupplierId':bTInvoiceList.supplierId,
          'bankTransactionJson':JSON.stringify(bankTransactionAList)
      });
      action.setCallback(this, function(response) {
          
          var state = response.getState();
          if(state === "SUCCESS") {
              var result = response.getReturnValue();
              if(result.status === "OK") {
                  chunkIndex++;
                  console.log('Okay bTInvoiceList');
                  console.log(JSON.stringify(result.data));
                  var bTInvoice =  component.get('v.bTInvoice');
                  console.log(JSON.stringify(bTInvoice));
                  var resultantbT = result.data.bTInvoiceList[0];
                  bTInvoice.name = resultantbT.name;
                  bTInvoice.supplierName = resultantbT.supplierName;
                  bTInvoice.unapplied = resultantbT.unapplied;
                  bTInvoice.appliedAmount = resultantbT.appliedAmount;
                  bTInvoice.btStatus = resultantbT.btStatus;
                  bTInvoice.isBTApplied = true;
                  if(bTInvoice.unapplied != 0){
                      component.set('v.isUnapplied',true);
                  }
                  component.set('v.bTInvoice', bTInvoice);
                  if(bankTransactionAppliedListOfChunk.length == chunkIndex){
                      component.set('v.isLoading',false);
                      helper.showToast('Success', 'success', 'Bank Transactions Applied created successfully!');
                  }
                  
                  component.set('v.selectedInvoices',0);
                  //helper.showToast('Success', 'success', 'Parts mapped successfully!');
                  
              } else {
                 helper.showToast('ERROR', 'error', result.msg);
                  console.log(result.msg);
                  component.set('v.isLoading',false);
              }
          }
          else if(state === "ERROR") {
              var errors = response.getError();
              if(errors) {
                  if(errors[0] && errors[0].message) {
                      console.log(errors[0].message);
                      component.set('v.isLoading',false);
                     helper.showToast('ERROR', 'error', errors[0].message);
                  }
              } else {
                  component.set('v.isLoading',false);
                  //component.set('v.loaded', !component.get('v.loaded'));
                  helper.showToast('ERROR', 'error', "Unknown error");
              }
              
          }
      });
      $A.enqueueAction(action);
      }
     //component.set('v.isLoading',false);
  }
 })