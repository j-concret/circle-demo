({
	doInit : function(component, event, helper) {
        
        var SupplierInvoiceID = component.get("v.soidFromFlow1"); // supplierINvoice ID for record attach
        var actionSupplierInvoice = component.get("c.getSupplierInvoice"); 
        
        
        console.log("SupplierInvoiceID",SupplierInvoiceID);
        
        component.set("v.iframeUrl2","https://tecex.lightning.force.com/lightning/r/Supplier_Invoice__c/"+ SupplierInvoiceID+"/view");
        
        actionSupplierInvoice.setParams({
            SupplierInvoiceID1 : component.get("v.soidFromFlow1")
        });
        
        //   Add callback behavior for when response is received
    	actionSupplierInvoice.setCallback(this, function(response) {
        	var state = response.getState();
             console.log("success state ",state); // debug
        	if (component.isValid() && state === "SUCCESS") {
      			component.set("v.SupplierInvoice", response.getReturnValue());
                var Output = response.getReturnValue();
                component.set("v.Output", Output);
                 console.log("Output",Output); // debug
              //  component.set("v.SupplierInvoice", Output);
              //  console.log("Output",v.SupplierInvoice); // debug
             
        	}
        	else {
            	console.log("Failed with state: " + state);
        	     }
    	})
        
        $A.enqueueAction(actionSupplierInvoice);
        
    }
})