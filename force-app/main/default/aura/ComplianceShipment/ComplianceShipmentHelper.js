({
  doInitHlpr: function(component, helper) {
    component.set("v.showSpinner", true);
    var params = {
      recordId: component.get("v.recordId")
    };
    this.makeServerCall(component, "getShipmentDetails", params, function(result) {
      if(result.status === 'OK') {
        var salesforceBaseURL = result.salesforceBaseURL;
        salesforceBaseURL = salesforceBaseURL.substring(salesforceBaseURL.indexOf('=') + 1, salesforceBaseURL.length - 1);
        component.set("v.userName", result.userName);
        component.set("v.attachments", result.attachments);
        component.set("v.salesforceBaseURL", salesforceBaseURL);
        component.set("v.shipmentOrder", result.shipmentOrder);
      }
      else {
        helper.showToast("error", "Error Occured", result.msg);
      }
      component.set("v.showSpinner", false);
    });
  },
  handleShipmentOrderReviewHlpr: function(component, helper) {
    component.set("v.showSpinner", true);
    var params = {
      recordId: component.get("v.recordId"),
      value: component.get("v.shipmentOrder.I_have_checked_the_HS_Codes__c")
    };
    this.makeServerCall(component, "updateShipmentOrderReview", params, function(result) {
      if(result.status === 'OK') {
        helper.showToast("success", "SUCCESS", result.msg);
      }
      else {
        helper.showToast("error", "Error Occured", result.msg);
      }
      component.set("v.showSpinner", false);
    });
  }
})