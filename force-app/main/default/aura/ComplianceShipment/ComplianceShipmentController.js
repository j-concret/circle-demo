({
  doInitCtrl: function(component, event, helper) {
    var recordId = component.get("v.pageReference").state.c__soId;
    var workspaceAPI = component.find("workspace");

    workspaceAPI.getFocusedTabInfo().then(function(response) {
      if((response.isSubtab && response.title == 'Loading...') || response.subtabs) {
        var focusedTabId = response.isSubtab ? response.tabId : response.subtabs[0].tabId;
        workspaceAPI.setTabLabel({
          tabId: focusedTabId,
          label: "Compliance"
        });
        workspaceAPI.setTabIcon({
          tabId: focusedTabId,
          icon: "standard:document",
          iconAlt: "Shipment"
        });
      }
    }).catch(function(error) {
      console.log(error);
    });

    if(recordId) {
      component.set("v.recordId", recordId);
      helper.doInitHlpr(component, helper);
    }
    else {
      helper.showToast("error", "Error Occured", "Invalid record!");
    }
  },
  handleIsComplianceReadyCtrl: function(component, event, helper) {
    if(!component.get("v.isComplianceReady")) {
      component.set("v.complianceButtonTitle", "Compliance Ready");
    }
    else {
      component.set("v.complianceButtonTitle", "Compliance Pending");
    }
  },
  handleShowChatterSectionCtrl: function(component, event, helper) {
    component.set("v.showChatterSection", !component.get("v.showChatterSection"));
  },
  handleShipmentOrderReviewCtrl: function(component, event, helper) {
    helper.handleShipmentOrderReviewHlpr(component, helper);
  }
})