({
	doInitCtrl : function(component, event, helper) {
		helper.getInvoices(component, event, helper);
	},
    save : function(component, event, helper) {
        helper.saveInvoices(component, event, helper);
    }
})