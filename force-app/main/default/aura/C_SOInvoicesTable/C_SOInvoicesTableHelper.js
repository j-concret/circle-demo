({
    getInvoices: function(component, event, helper) {
        component.set('v.showSpinner',true);
        var action = component.get("c.getInvoices");
        action.setParams({
            shipmentId: component.get('v.recordId')
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.shipmentInvoices",response.getReturnValue());
            }
            component.set('v.showSpinner',false);
        });
        $A.enqueueAction(action);
    },
    saveInvoices : function(component, event, helper) {
        component.set('v.showSpinner',true);
        
        var action = component.get("c.saveInvoices");
        action.setParams({
            invoices: component.get('v.shipmentInvoices')
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                alert('Success');
            }
            component.set('v.showSpinner',false);
        });
        $A.enqueueAction(action);
    }
})