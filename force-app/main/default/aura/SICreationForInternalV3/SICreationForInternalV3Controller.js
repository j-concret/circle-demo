({
	/** this function will be called on init of component
     * @param componentP this is the component we are initialising
     * @param eventP this is the event that occured
     * @param helperP this is helper class
     */
    doInit: function( componentP, eventP, helperP ){
/*
       var get_records = componentP.get( "c.getAccountAndContact" );                    //action to return account and contact record

        console.log( "getting contact and account" );

        get_records.setCallback( this, function( response ){

            var state = response.getState();                            //get the state

            //check if we have success
            if( state === "SUCCESS" ){

                console.log( "Success getting account and contact" );

                var server_response = response.getReturnValue();                //get the return value
                var account_populate_method = componentP.find( "account_lookup" );              //get the account lookup field
                var contact_populate_method = componentP.find( "contact_lookup" );              //get the contact lookup field


                //check if we have an error
                if( server_response.is_error ){

                    this.showToastMessage( "error", "Error Occured", server_response.result_message );
                    return;                         //indicate failure

                }
                //check if auto populate is not required
                else if( server_response.result_message === "NA" ) {

                    console.log("In NA");
                    return;             //terminate function

                }//end of if-else block

                console.log( server_response.sobject_list[0] );
                console.log( server_response.sobject_list[1] );

               //console.log( "Before the auto set" );
               // console.log( "Account: " + account_populate_method );
                //console.log( "Test Value: " + componentP.get( "v.shipment_value" ) );

                componentP.set( "v.account_id", server_response.sobject_list[0].Id );
                componentP.set( "v.contact_id", server_response.sobject_list[1].Id ); // test
                
                account_populate_method.setSelectedRecord( server_response.sobject_list[0] );       //set the selected Account record
                contact_populate_method.setSelectedRecord( server_response.sobject_list[1] );       //set the selected contact record

                console.log( "Account ID: " + componentP.get( "v.account_id" ) ); 
                console.log( "COntact ID: " + componentP.get( "v.contact_id" ) );//

            }
            //check if have errors
            else if( state === "ERROR" )
                this.processServerErrors( response.getError() );             //process the server errors

        });//end of

        $A.enqueueAction( get_records );                    //execute the action
*/
    },//end of function definition
    
    SONameChanged : function(component, event, helper) {
     
        var SelectedSOID = component.get( "v.ShipmentOrderId" );
      //  alert("Expense name changed from" +SelectedSOID);
        var action = component.get("c.getCreatedSIs");
        action.setParams({"SOID1" : SelectedSOID});
        action.setCallback(this, function(response) {          
            var state = response.getState();
            if (state === 'SUCCESS'){
                
                var data = response.getReturnValue();
                component.set('v.ExistingSupplierInvoices',data);
                console.log(data);
             //    alert("in success" +data);
                
               }
            else{
                alert("We are in fail block");
            }
            
        });
        $A.enqueueAction(action);
    },
    
    //Create SI
    CreateSIButton : function(component, event, helper) {
        component.set("v.is_loading", true);
     
        var CSelectedSOID = component.get("v.ShipmentOrderId");
        var CAccID = component.get("v.account_id");
        var CConSOID = component.get("v.contact_id");
        var CCurrSOID = component.get("v.invoice_currency");
        var CDateSOID = component.get("v.invoice_date");
        var CInvNumSOID = component.get("v.Invoice_Number");
        
        var action = component.get("c.CreateSI");
      
        action.setParams({"PSelectedSOID" : CSelectedSOID,
                          "PAccID" : CAccID,
                          "PConID" : CConSOID,
                          "PCurr" : CCurrSOID,
                          "PDate" : CDateSOID,
                          "PInvNum" : CInvNumSOID
                         });
        
        
               
        action.setCallback(this, function(response) {  
             component.set("v.is_loading", false);
            var state = response.getState();
            if (state === 'SUCCESS'){
                
                var data = response.getReturnValue();
                component.set('v.CreatedSupplierInvoice',data);
                var SIIDtoClose = component.get("v.CreatedSupplierInvoice");
                component.set("v.iframeUrl2","https://tecex.lightning.force.com/lightning/r/Invoice_New__c/"+ SIIDtoClose+"/view");
                console.log(data);
             //    alert("in success" +data);
                  helper.fetchData(component,event, helper);
                
               }
            else{
                alert("Invoice not created, Please fill required information");
            }
            
        });
        $A.enqueueAction(action);
    },
    
    //Upload Documents
    
    handleUploadFinished: function (cmp, event) {
        // This will contain the List of File uploaded data and status
        var uploadedFiles = event.getParam("files");
        alert("Files uploaded : " + uploadedFiles.length);
    },
    
    //UPdate Existing Costings
    save: function (component, event, helper) {
         component.set("v.is_loading", true);
        var draftValues = component.get("v.data");
        var CCurr = component.get("v.invoice_currency");
         var CSIID = component.get("v.CreatedSupplierInvoice");
        
        console.log('CSIID',CSIID);
        console.log(draftValues);
        var action = component.get("c.updateCostings");
        action.setParams({
            "editedAccountList" : draftValues,
            "PCurr" : CCurr,
            "PSIID" : CSIID
            
                         });
        action.setCallback(this, function(response) {
             component.set("v.is_loading", false);
            
            var state = response.getState();
            if (state === 'SUCCESS'){
            
           alert("Costings records saved succesfully ");
         //  $A.get('e.force:refreshView').fire();
            }
            else{
                alert("We are in fail block-Update costings");
            }
            
        });
        $A.enqueueAction(action);
        
    },
    //Adding New Costs 
     addRow: function(component, event, helper) {
        helper.addAccountRecord(component, event);
    },
     
    removeRow: function(component, event, helper) {
        //Get the account list
        var accountList = component.get("v.accountList");
        //Get the target object
        var selectedItem = event.currentTarget;
        //Get the selected item index
        var index = selectedItem.dataset.record;
        accountList.splice(index, 1);
        component.set("v.accountList", accountList);
    },
     
    save1: function(component, event, helper) {
        if (helper.validateAccountList(component, event)) {
            helper.saveAccountList(component, event);
            helper.fetchData(component,event, helper);
             
        }
    },
    
    // Keep calm.I know this is nonsence and not best practice but Just to satisfy one requirement:)
    save3: function (component, event, helper) {
         component.set("v.is_loading", true);
        var draftValues = component.get("v.data");
        var CCurr = component.get("v.invoice_currency");
         var CSIID = component.get("v.CreatedSupplierInvoice");
        
        console.log(draftValues);
        var action = component.get("c.updateCostings");
        action.setParams({
            "editedAccountList" : draftValues,
            "PCurr" : CCurr,
            "PSIID" : CSIID
            
                         });
        action.setCallback(this, function(response) {
             component.set("v.is_loading", false);
            
            var state = response.getState();
            if (state === 'SUCCESS'){
            
          // alert("Costings records saved succesfully ");
         //  $A.get('e.force:refreshView').fire();
            }
            else{
                alert("We are in fail block-Update costings");
            }
            
        });
        $A.enqueueAction(action);
        
    }
    
    
    
})