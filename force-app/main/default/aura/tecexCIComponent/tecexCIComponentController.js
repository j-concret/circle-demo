({
   init: function(component, event, helper) {
      
      let servisTypeAction = component.get("c.fetchServirceType");
      servisTypeAction.setParams({recId: component.get("v.recordId")});
      servisTypeAction.setCallback(this, function(response) {
 
         let state = response.getState();
         console.log('State ===> '+JSON.stringify(state));
         if (state === "SUCCESS") {
            let result = response.getReturnValue();
            console.log('Results ===> '+JSON.stringify(result));
            component.set("v.servType", result);
         }
      });
      $A.enqueueAction(servisTypeAction);
      
      /*let action = component.get("c.fetchConsignee");
      action.setParams({recId: component.get("v.recordId")});
 
             action.setCallback(this, function(response) {
 
                let state = response.getState();
                console.log('State ===> '+JSON.stringify(state));
                if (state === "SUCCESS") {
                   let result = response.getReturnValue();
                   console.log('Results ===> '+JSON.stringify(result));
                   component.set("v.conAdresses", result);
                }
             });
             $A.enqueueAction(action);*/
 
       /*let action2 = component.get("c.fetchpickUpAddr");
       action2.setParams({recId: component.get("v.recordId")});
 
             action2.setCallback(this, function(response) {
 
                let state = response.getState();
                console.log('State ===> '+JSON.stringify(state));
                if (state === "SUCCESS") {
                   let result = response.getReturnValue();
                   console.log('Results ===> '+JSON.stringify(result));
                   component.set("v.pickAdresses", result);
                }
             });
             $A.enqueueAction(action2);*/
    },

   doPreview: function (component, event, helper) {
      
      console.log('The selected value is ===> '+JSON.stringify(component.get('v.selectedValue')));
      var message = []
      var message1 = component.get('v.selectedValue');
      var message2 = component.get('v.selectedPickValue');
      message.push(message1);
      message.push(message2);
      console.log('The message value is ===> '+JSON.stringify(message));
      var vfOrigin = "https://" + component.get("v.vfHost");
      var vfWindow = component.find("vfFrame").getElement().contentWindow;
      vfWindow.postMessage(message, vfOrigin);
   },
   
   doDownload: function(component, event, helper) {
      var ToastEvent = $A.get("e.force:showToast");
    var ciType = component.get("v.value");
    console.log('The ciType value is ===> '+JSON.stringify(ciType));
    if(ciType =="Custom"){
      var message = "DownloadCI"
      console.log('The message value is ===> '+JSON.stringify(message));
      var vfOrigin = "https://" + component.get("v.vfHost");
      var vfWindow = component.find("vfFrame").getElement().contentWindow;
      vfWindow.postMessage(message, vfOrigin);
      
      ToastEvent.setParams({
         "title": "Success!",
         "type": "success",
         "message": "CI Downloaded"});
         ToastEvent.fire();
    }
    else{

     var sType = component.get("v.servType");

     if(sType  != 'Freight forwarder'){

      var recordId = component.get("v.recordId");
      window.open('/apex/tecexCIExcel?id=' + recordId,'_blank');
     }
     else if(sType  == 'Freight forwarder'){
        var recordId = component.get("v.recordId");
      window.open('/apex/tecexCIFFExcel?id=' + recordId,'_blank');
     }
      /*
      //####Using $A.get("e.force:navigateToURL") ####
      var url = '/apex/tecexCIExcel?id=' + recordId;
      var urlEvent = $A.get("e.force:navigateToURL");
      urlEvent.setParams({
            "url": url
         });
         urlEvent.fire();*/
         
         /*
         //####Using  lightning:navigation ####
         let pageReference = {
         type: 'standard__webPage',
         attributes: {
            url: '/apex/tecexCIExcel?id=' + recordId  
         },

      };

      var navService = component.find("navService");

      var navService = component.find("navService");
         navService.generateUrl(pageReference)
            .then($A.getCallback(function(url) {
               console.log(url);
               navService.navigate(pageReference);


            }), $A.getCallback(function(error) {
               console.log(error); 
            }));*/



   
   
        ToastEvent.setParams({
         "title": "Success!",
         "type": "success",
         "message": "CI Downloaded"});
         ToastEvent.fire();
         
         /*let savePromise = Promise.resolve(
            helper.promiseSaveCI(component)
        );
        savePromise.then(
            $A.getCallback(function (result) {
                 let isCISaved = result;
                 if (!isCISaved) {
                      component.find('notifLib').showToast({
                            "variant": "error",
                            "message": "Unable to save CI"
                      });
                 }else {
                      component.find('notifLib').showToast({
                            "variant": "success",
                            "message": "CI Saved! "
                      });
                 }
                 
            }),
            $A.getCallback(function (status) {
                 
                 component.find('notifLib').showToast({
                      "variant": "error",
                      "message": "Unable to save CI! "+status.message
                 });
            })
      ).catch(function (error) {
            
            $A.reportError("Unable to save CI!", error);
      });*/
    }
      
      
      
      
      /*var   ToastEvent = $A.get("e.force:showToast");
              ToastEvent.setParams({
                 "title": "Under Development!",
                 "type": "success",
                 "message": "Validate and show validation, success or error/warning messages at the click of this button"});
              ToastEvent.fire();*/
     },
     
      
      
overFlowActions: function(component, event, helper) {
         
        var selectedValue = event.getParam("value");
        var ToastEvent = $A.get("e.force:showToast");
        
        if(selectedValue == "saveCI"){
           /*ToastEvent.setParams({
              "title": "Success!",
              "type": "success",
              "message": "Under Development."});
              ToastEvent.fire();*/
            //Get contacts
      component.set("v.showLoadingSpinner", true);
      let savePromise  = Promise.resolve(helper.promiseSaveCI(component));
      savePromise.then( $A.getCallback(
                            function (result) {
                                //let options = result.map(record=>({label:record.Name,value:record.Email}));
                              //component.set("v.relatedContactList", result);
                            component.set("v.showLoadingSpinner", false);
                            ToastEvent.setParams({
                              "title": "Success!",
                              "type": "success",
                              "message": "Saved!."});
                              ToastEvent.fire();
                            }
                            

                        ),
                        $A.getCallback(
                            function (status) {
                                component.set("v.showLoadingSpinner", false);
                                component.find('notifLib').showToast({ "variant": "error",
                                                                        "message": "Unable to save CI! "+status.message
                                                                    });
                            }
                        )
        ).catch(function (error) {
            component.set("v.showLoadingSpinner", false);
            $A.reportError("Unable to initialise component!", error);
        });
            
        
           } else if(selectedValue == "doSomething"){
           ToastEvent.setParams({
              "title": "Success!",
              "type": "success",
              "message": "Under Development."});
              ToastEvent.fire();
     
           
           
        }
      },

      closeCreateCIModal: function(component, event, helper) {
         
         //component.set("v.isOpenCreateCI", false); // for Hide/Close Model,set the "isOpen" attribute to "Fasle"  
        var closeModalAtt = component.set("v.isOpenCreateCI", false);
        var appEvent = $A.get("e.c:closeModal");
        //appEvent.setParams({"isOpenCreateCI" : closeModalAtt});
        appEvent.fire();
     }
        
})