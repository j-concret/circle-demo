({
    promiseSaveCI : function(component) {
        console.log('The recordId value is ===> '+JSON.stringify(component.get("v.recordId")));
        return this.createPromise(component, "c.saveCI", {
        soId: component.get("v.recordId"),
        SoNumber : component.get("v.simpleRecord.Name"),
        sType: component.get("v.servType")
            
            });
    },

    createPromise : function(component, name, params) {
        return new Promise(function(resolve, reject) {
            let action = component.get(name);
            if (params) {
                action.setParams(params);
            }
            action.setCallback(this, function(response) {
                let state = response.getState();
                if (component.isValid() && state === "SUCCESS") {
                    let result = response.getReturnValue();
                    resolve(result);
                }
                else {
                    reject(response.getError());
                }
            });
            $A.enqueueAction(action);
        });
    },
})