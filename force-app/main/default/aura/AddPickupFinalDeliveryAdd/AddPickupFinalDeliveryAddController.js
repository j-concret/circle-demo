({
  doInit : function(cmp, event, helper) {
    cmp.set('v.showSpinner',true);
    helper.getRecords(cmp);
    helper.getpicklist(cmp);
  },
  addNewAddressRow: function(cmp, event,helper) {
    var newAddressList = cmp.get("v.newAddressList");
    newAddressList.push({
        'sobjectType': 'Client_Address__c',
        'Contact_Full_Name__c': '',
        'Contact_email__c': '',
        'Contact_Phone_Number__c':'' ,
        'CompanyName__c': '',
        'Pickup_Preference__c':'NONE',
        'Address__c': '',
        'Address2__c': '',
        'City__c': '',
        'Province__c': '',
        'Postal_Code__c':'',
        'All_Countries__c': ''
    });
    cmp.set("v.newAddressList", newAddressList);
  },
  openAddNewModal : function(cmp,event,helper){
    cmp.set('v.isMainModalOpen',false);
    cmp.set('v.isAddNewModalOpen',true);
  },
  closeAddNewModal : function(cmp,event,helper){
    cmp.set('v.isAddNewModalOpen',false);
    cmp.set('v.isMainModalOpen',true);
  },
  closeSearchModal : function(cmp,event,helper){
    cmp.set('v.showSearchModal',false);
    cmp.set('v.isAddNewModalOpen',true);
  },
  OpenSearchModal : function(cmp,event,helper){
    cmp.set('v.searchKey', null);
    cmp.set('v.AddressList', null);
    var selectedItem = event.currentTarget;
    var indexvar = selectedItem.dataset.record;
    cmp.set('v.pickIndex',parseInt(indexvar));
    var addressList = cmp.get('v.newAddressList');
    if(addressList && addressList[indexvar]){
      cmp.set("v.newAddress",addressList[indexvar]);
    }
    cmp.set('v.showSearchModal',true);
    cmp.set('v.isAddNewModalOpen',false);
  },
  clear : function(cmp) {
    cmp.set('v.searchKey', null);
    cmp.set('v.AddressList', null);
  },
  keyPressController: function(cmp, event, helper) {
    helper.getAddressRecommendations(cmp);
  },
  addPlace: function(cmp, event, helper) {
    var addIndex = cmp.get('v.pickIndex');
    var addressList = cmp.get('v.newAddressList');

    var allValid = cmp.find('newAdd').reduce(function (validSoFar, inputCmp) {
      inputCmp.reportValidity();
      return validSoFar && inputCmp.checkValidity();
    }, true);

    if(!allValid) return;

    addressList[addIndex] = cmp.get("v.newAddress");

    cmp.set('v.newAddressList',addressList);
    cmp.set("v.showSearchModal", false);
    cmp.set('v.isAddNewModalOpen',true);
  },
  selectOption:function(cmp, event, helper) {
    helper.getAddressDetailsByPlaceId(cmp, event);
  },
  removePickaddRow: function(cmp, event, helper) {
      var newAddressList = cmp.get("v.newAddressList");
      var selectedItem = event.currentTarget;
      var index = selectedItem.dataset.record;
      newAddressList.splice(index, 1);
      cmp.set("v.newAddressList", newAddressList);
  },
  saveNewAddList: function(cmp, event, helper) {
    helper.createAddresses(cmp);
  },
  onSelectionChange:function(cmp,event) {
    var isPickup = cmp.get('v.isPickup');
    var selected = isPickup || (cmp.get('v.business') == 'Zee')? event.getSource().get("v.value") :event.getSource().get("v.checked");
    var selectedId = event.getSource().get("v.label");
	var intialBool = cmp.get('v.isInitial');
    var selectedAddIds = cmp.get('v.selectedAddIds');
    if(!selectedAddIds || isPickup) selectedAddIds = [];

    if(selected){
      if(cmp.get('v.business') == 'Zee') selectedAddIds = [];
      selectedAddIds.push(selectedId);
    }else{
      const index = selectedAddIds.indexOf(selectedId);
      if (index > -1) {
        selectedAddIds.splice(index, 1);
      }
    }
    cmp.set('v.selectedAddIds',selectedAddIds);
      cmp.set('v.isInitial',false);
      var dataList = cmp.get('v.data');
      if(selectedAddIds.length == 1){
      for(let data1 of dataList){
          if(data1.Id == selectedAddIds[0]){
              if(isPickup){
              cmp.set('v.selectedAddName',data1.Contact_Full_Name__c);
              }else{
                  if(cmp.get('v.business') == 'Zee'){
                      cmp.set('v.selectedFinalAddName',data1.Name);
                  }
                  else{
                      cmp.set('v.selectedFinalAddName',data1.Contact_Full_Name__c);
                  }
                  
              }
          }
      }
      }
      
  }
})