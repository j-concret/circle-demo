({
  getpicklist: function (cmp) {
    var self = this;
    self.serverCall(cmp,'getPicklistValue',{
      objectName : 'Client_Address__c',
      fieldName : 'Pickup_Preference__c'
    },function(response){
        if(Object.entries(response).length !== 0){
            if(response.labels.length > 0){
            cmp.set('v.pickupPref',response.labels);
            }
        }
        
      cmp.set('v.showSpinner',false);
      
    },function(err){
      cmp.set('v.showSpinner',false);
      self.showToast('error','ERROR',err);
    });

  },	
  getRecords: function (cmp) {
    var self = this;
    self.serverCall(cmp,'getsfRecords',{
      soId : cmp.get('v.recordId'),
      isPickup : cmp.get('v.isPickup')
    },function(response){
        if(Object.entries(response).length !== 0){
            if(response.ClientAddress.length > 0){
            cmp.set('v.data',response.ClientAddress);
            }
            if(response.business == 'Zee'){
                cmp.set('v.business', response.business);
            }
            if(response.hideNewFinalDeliveryButton){
                cmp.set('v.hideAddFinalDeliveryButton',response.hideNewFinalDeliveryButton);
            }
        }
        
      cmp.set('v.showSpinner',false);
      
    },function(err){
      cmp.set('v.showSpinner',false);
      self.showToast('error','ERROR',err);
    });

  },
  showToast : function(type, title , msg) {
    var toastEvent = $A.get('e.force:showToast');
    toastEvent.setParams({
      'type':type,
      'title': title ,
      'message': msg
    });
    toastEvent.fire();
  },
  getAddressRecommendations: function(component){
    var key = component.get("v.searchKey");
    var action = component.get("c.getAddressFrom");
    action.setParams({
        "SearchText": key
    });

    action.setCallback(this, function (response) {
        var state = response.getState();
        if (state === "SUCCESS") {
            var response = JSON.parse(response.getReturnValue());
            var predictions = response.predictions;
            var addresses = [];
            if (predictions.length > 0) {
                for (var i = 0; i < predictions.length; i++) {
                    addresses.push({
                          main_text: predictions[i].structured_formatting.main_text,
                          secondary_text: predictions[i].structured_formatting.secondary_text,
                          place_id: predictions[i].place_id
                        });

                }
            }
            component.set("v.AddressList", addresses);
        }
    });
    $A.enqueueAction(action);
  },
  getAddressDetailsByPlaceId: function(component,event){

    var selectedValue = event.currentTarget.dataset.value;
    var action = component.get("c.getAddressDetailsByPlaceId");
    action.setParams({
        PlaceID:selectedValue
    });
    action.setCallback(this, function(response){
        var res = response.getState();
        if(res == 'SUCCESS'){
            console.log(response.getReturnValue());
            var response = JSON.parse(response.getReturnValue());
            var postalCode = '', state = '', country= '', city = '', street1 = '',street2 = '', street_number = '', route = '', subLocal1 = '', subLocal2 = '' , subLocal3 = '',neighbor='' ;

            for(var i=0; i < response.result.address_components.length ; i++){
                var FieldLabel = response.result.address_components[i].types[0];

                if( FieldLabel == 'neighborhood' || FieldLabel == 'sublocality_level_3' || FieldLabel == 'sublocality_level_2' || FieldLabel == 'sublocality_level_1' || FieldLabel == 'street_number' || FieldLabel == 'route' || FieldLabel == 'locality' || FieldLabel == 'country' || FieldLabel == 'postal_code' || FieldLabel == 'administrative_area_level_1'){

                  switch(FieldLabel){
                      case 'neighborhood':
                        neighbor = response.result.address_components[i].long_name;
                        break;
                        case 'sublocality_level_3':
                        subLocal3 = response.result.address_components[i].long_name;
                        break;
                      case 'sublocality_level_2':
                          subLocal2 = response.result.address_components[i].long_name;
                          break;
                      case 'sublocality_level_1':
                          subLocal1 = response.result.address_components[i].long_name;
                          break;
                      case 'street_number':
                          street_number = response.result.address_components[i].long_name;
                          break;
                      case 'route':
                          route = response.result.address_components[i].short_name;
                          break;
                      case 'postal_code':
                          postalCode = response.result.address_components[i].long_name;
                          break;
                      case 'administrative_area_level_1':
                          state = response.result.address_components[i].short_name;
                          break;
                      case 'country':
                          country = response.result.address_components[i].long_name;
                          break;
                      case 'locality':
                          city = response.result.address_components[i].long_name;
                          break;
                      default:
                          break;
                  }
                }
            }

            street1 = street_number + ' ' + route + ' ' +neighbor;
            street2 = subLocal3+ ' ' +subLocal2 + ' ' + subLocal1;

            if(postalCode == null || postalCode == '' || postalCode == ' '){
                //alert('Postal code/ Zip Code/ Pin Code Not found Please do deep search');
                var action1 = component.get("c.getZipCodeByCountry");
                action1.setParams({
                    countryName: country
                });

                action1.setCallback(this, function (response) {
                    var state1 = response.getState();
                    if (state1 === "SUCCESS") {
                      var result = response.getReturnValue();
                      if(result.no_zip_code_countries && result.no_zip_code_countries.zip_code__c){
                        postalCode = result.no_zip_code_countries.zip_code__c;
                        component.set('v.AddressList', null);
                        component.set("v.newAddress.Address__c", street1);
                        component.set("v.newAddress.Address2__c", street2);
                        component.set("v.newAddress.City__c", city);
                        component.set("v.newAddress.Province__c", state);
                        component.set("v.newAddress.Postal_Code__c", postalCode);
                        component.set("v.newAddress.All_Countries__c", country);
                      }
                      else{
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            "title": "Error!",
                            "type": "error",
                            "message": "Postal code/ Zip Code/ Pin Code Not found Please do deep search"
                        });
                        toastEvent.fire();
                      }
                    }
                });
                $A.enqueueAction(action1);

            }else{
                component.set('v.AddressList', null);
                component.set("v.newAddress.Address__c", street1);
                component.set("v.newAddress.Address2__c", street2);
                component.set("v.newAddress.City__c", city);
                component.set("v.newAddress.Province__c", state);
                component.set("v.newAddress.Postal_Code__c", postalCode);
                component.set("v.newAddress.All_Countries__c", country);
            }
        }
    });
    $A.enqueueAction(action);
  },
  updateAddOnFreight : function(cmp){
    cmp.set('v.showSpinner',true);
    var self = this;
    self.serverCall(cmp,'updateFreight',{
      selectedAddIds : cmp.get('v.selectedAddIds'),
      soId : cmp.get('v.recordId'),
      isPickup : cmp.get('v.isPickup')
    },function(response){
      cmp.set('v.showSpinner',false);
      self.showToast('success','SUCCESS','Address added successfully!');
        if(cmp.get('v.showCancel')){
            self.navigateTab(cmp);
        }
        else{
            self.closeTab(cmp);
        }
    },function(err){
      cmp.set('v.showSpinner',false);
      self.showToast('error','ERROR',err);
    });

  },
  createAddresses : function(cmp){
    var newAddressList = cmp.get("v.newAddressList");
    var self = this;

    self.serverCall(cmp,'createNewAddress',{
      newAddressList: newAddressList,
      soId: cmp.get('v.recordId'),
      isPickup : cmp.get('v.isPickup')
    },function(response){
      //cmp.set('v.showSpinner',false);
      cmp.set("v.newAddressList", []);
      cmp.set("v.isAddNewModalOpen", false);
      self.getRecords(cmp);
    },function(err){
      cmp.set('v.showSpinner',false);
      self.showToast('error','ERROR',err);
    });

  },
  closeTab : function(cmp){
    var workspaceAPI = cmp.find('workspace');
    var focusedTabId = cmp.get('v.tabId');

    if(focusedTabId){
      workspaceAPI.closeTab({tabId: focusedTabId});
    }
  },
    navigateTab : function(cmp){
        var workspaceAPI = cmp.find("workspace");

        workspaceAPI.openTab({
            pageReference: {
                "type": "standard__recordPage",
                "attributes": {
                    "recordId": cmp.get('v.recordId'),
                    "actionName":"view"
                }
            },
            focus: true
        }).then(function(newTabId){
            workspaceAPI.getEnclosingTabId()
            .then(function(enclosingTabId) {
                workspaceAPI.closeTab({tabId : enclosingTabId});
                workspaceAPI.focusTab(newTabId);
            });
        });
        window.location.href = window.location.origin+'/lightning/r/Shipment_Order__c/'+cmp.get('v.recordId')+'/view';
    }
})