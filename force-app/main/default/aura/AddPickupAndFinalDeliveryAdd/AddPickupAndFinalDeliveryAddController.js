({
    doInit : function(cmp, event, helper) {
      var workspaceAPI = cmp.find('workspace');
      workspaceAPI.getAllTabInfo().then(function(response) {
        var focusedTabId;
        for(var i=0;i<response.length;i++){
            if(response[i].isSubtab && response[i].title == 'Loading...' && response[i].pageReference.attributes && response[i].pageReference.attributes.componentName && response[i].pageReference.attributes.componentName == 'c__AddPickupAndFinalDeliveryAdd'){
              focusedTabId = response[i].tabId;
              break;
            }else if(response[i].subtabs){
              var subtabs = response[i].subtabs;
              for(var j=0;j<subtabs.length;j++){
                if(subtabs[j].title === 'Loading...' && subtabs[j].pageReference.attributes && subtabs[j].pageReference.attributes.componentName && subtabs[j].pageReference.attributes.componentName == 'c__AddPickupAndFinalDeliveryAdd'){
                  focusedTabId = subtabs[j].tabId;
                  break;
                }
              }
            }
        }

        if(focusedTabId){
          cmp.set('v.tabId',focusedTabId);
          workspaceAPI.setTabLabel({
            tabId: focusedTabId,
            label: 'Add Address'
          });
          workspaceAPI.setTabIcon({
            tabId: focusedTabId,
            icon: 'standard:document',
            iconAlt: 'AddAddress'
          });
        }
      }).catch(function(error) {
        console.log(error);
      });

      cmp.set('v.showSpinner',true);
      var pageRef = cmp.get("v.pageReference");
      if(pageRef && pageRef.state.c__soId){
        cmp.set("v.recordId",pageRef.state.c__soId);
        cmp.set("v.isPickup",(pageRef.state.c__type && pageRef.state.c__type == 'Pickup'));
          cmp.set("v.showCancel",pageRef.state.c__showCancel);
        helper.getRecords(cmp);
        helper.getpicklist(cmp);
      }
      //helper.getRecords(cmp);
    },
    addNewAddressRow: function(cmp, event,helper) {
      var newAddressList = cmp.get("v.newAddressList");
      newAddressList.push({
          'sobjectType': 'Client_Address__c',
          'Contact_Full_Name__c': '',
          'Contact_email__c': '',
          'Contact_Phone_Number__c':'' ,
          'CompanyName__c': '',
          'Pickup_Preference__c':'NONE',
          'Address__c': '',
          'Address2__c': '',
          'City__c': '',
          'Province__c': '',
          'Postal_Code__c':'',
          'All_Countries__c': ''
      });
      cmp.set("v.newAddressList", newAddressList);
    },
    openAddNewModal : function(cmp,event,helper){
      cmp.set('v.isAddNewModalOpen',true);
    },
    closeAddNewModal : function(cmp,event,helper){
      cmp.set('v.isAddNewModalOpen',false);
    },
    closeSearchModal : function(cmp,event,helper){
      cmp.set('v.showSearchModal',false);
      cmp.set('v.isAddNewModalOpen',true);
    },
    OpenSearchModal : function(cmp,event,helper){
      cmp.set('v.searchKey', null);
      cmp.set('v.AddressList', null);
      var selectedItem = event.currentTarget;
      var indexvar = selectedItem.dataset.record;
      cmp.set('v.pickIndex',parseInt(indexvar));
      var addressList = cmp.get('v.newAddressList');
      if(addressList && addressList[indexvar]){
        cmp.set("v.newAddress",addressList[indexvar]);
      }
      cmp.set('v.showSearchModal',true);
      cmp.set('v.isAddNewModalOpen',false);
    },
    clear : function(cmp) {
      console.log("Clearing the list!");
      cmp.set('v.searchKey', null);
      cmp.set('v.AddressList', null);
    },
    keyPressController: function(cmp, event, helper) {
      helper.getAddressRecommendations(cmp);
    },
    addPlace: function(cmp, event, helper) {
      var addIndex = cmp.get('v.pickIndex');
      var addressList = cmp.get('v.newAddressList');

      var allValid = cmp.find('newAdd').reduce(function (validSoFar, inputCmp) {
        inputCmp.reportValidity();
        return validSoFar && inputCmp.checkValidity();
      }, true);

      if(!allValid) return;

      addressList[addIndex] = cmp.get("v.newAddress");

      cmp.set('v.newAddressList',addressList);
      cmp.set("v.showSearchModal", false);
      cmp.set('v.isAddNewModalOpen',true);
    },
    selectOption:function(cmp, event, helper) {
      helper.getAddressDetailsByPlaceId(cmp, event);
    },
    removePickaddRow: function(cmp, event, helper) {
        var newAddressList = cmp.get("v.newAddressList");
        var selectedItem = event.currentTarget;
        var index = selectedItem.dataset.record;
        newAddressList.splice(index, 1);
        cmp.set("v.newAddressList", newAddressList);
    },
    saveNewAddList: function(cmp, event, helper) {
      helper.createAddresses(cmp);
    },
    updateSelectedAddress: function(cmp, event, helper) {
      var selectedAddIds = cmp.get('v.selectedAddIds');
      if(!selectedAddIds || (selectedAddIds && selectedAddIds.length == 0)){
        helper.showToast('error','ERROR','Select atleast one address.');
      }
      helper.updateAddOnFreight(cmp);
    },
    onSelectionChange:function(cmp,event) {
      var isPickup = cmp.get('v.isPickup');
      var selected = isPickup || (cmp.get('v.business') == 'Zee') ? event.getSource().get("v.value") :event.getSource().get("v.checked");
      var selectedId = event.getSource().get("v.label");

      var selectedAddIds = cmp.get('v.selectedAddIds');
      if(!selectedAddIds || isPickup) selectedAddIds = [];

      if(selected){
          if(cmp.get('v.business') == 'Zee') selectedAddIds = [];
        selectedAddIds.push(selectedId);
      }else{
        const index = selectedAddIds.indexOf(selectedId);
        if (index > -1) {
          selectedAddIds.splice(index, 1);
        }
      }
      cmp.set('v.selectedAddIds',selectedAddIds);
    },
    handleCancel : function(cmp, event, helper){
        helper.navigateTab(cmp);
    }

})