({
    doInit : function(component, event, helper) {
        var evt = $A.get("e.force:navigateToComponent");
        var RID = component.get("v.recordId");
        evt.setParams({
            componentDef : "c:ShipmentOrderNew",//"c:AddMoreShipmentsToRollout",
            componentAttributes: {
                recordROId: RID,
            }});
        evt.fire();
    }
})