({
  searchForMatches : function(cmp,keyword){
    var self = this;
    var add_filters = cmp.get( "v.additionalFilters");

    self.serverCall(cmp,'fetchMatchingRecords',{
      "search_keyword" : keyword,
      "object_name" : cmp.get( "v.object_api_name"),
      "id_field_name" : cmp.get( "v.id_field_name"),
      "label_field_name" : cmp.get( "v.label_field_name"),
      "additionalFilters" : add_filters ? JSON.stringify(add_filters) : '',
      'filterField' : cmp.get('v.filterField'),
      'filterValue' : cmp.get('v.filterValue')
    },function(store_response){
      if(!store_response || store_response.length == 0)
          cmp.set( "v.message", "No Result Found." );
      else
          cmp.set( "v.message", "" );//no message
      cmp.set( "v.list_of_records", store_response );
    },function(err){
      cmp.set('v.showSpinner',false);
      self.showToast('error','ERROR',err);
    });
  },
  getSelected:function(cmp,record_id){
    console.log('getSelectedRecord');
    var self = this;
    var add_filters = cmp.get( "v.additionalFilters");
    self.serverCall(cmp,'getSelectedRecord',{
      "record_id" : record_id,
      "object_name" : cmp.get( "v.object_api_name"),
      "id_field_name" : cmp.get( "v.id_field_name"),
      "label_field_name" : cmp.get( "v.label_field_name"),
      "additionalFilters" : add_filters ? JSON.stringify(add_filters) : '',
      'filterField' : cmp.get('v.filterField'),
      'filterValue' : cmp.get('v.filterValue')
    },function(store_response){
      if(store_response && store_response.length > 0)
       self.makeSelection(cmp,store_response[0]);
    },function(err){
      cmp.set('v.showSpinner',false);
      self.showToast('error','ERROR',err);
    });

  },
  makeSelection : function( cmp, selected_account){

    var current_element;

    cmp.set( "v.selected_record", selected_account );
      cmp.set( "v.selectedAccountName", selected_account.Name);
    //cmp.set( "v.record_id", selected_account.Id );

    current_element = cmp.find( "lookup_pill" );
    $A.util.addClass( current_element, "slds-show" );
    $A.util.removeClass( current_element, "slds-hide" );

    current_element = cmp.find( "search_result" );
    $A.util.addClass( current_element, "slds-is-close");
    $A.util.removeClass( current_element, "slds-is-open");
    current_element = cmp.find( "lookup_field" );
    $A.util.addClass( current_element, "slds-hide");
    $A.util.removeClass( current_element, "slds-show");
},
  showToast : function(title,type,msg) {
    var toastEvent = $A.get('e.force:showToast');
    toastEvent.setParams({
      'title': title,
      'type' :type,
      'message': msg
    });
    toastEvent.fire();
  }
})