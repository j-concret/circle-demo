({
  doInit: function(cmp,event,helper){
    var record_id = cmp.get('v.record_id');
    if(record_id){
      helper.getSelected(cmp,record_id);
    }
  },
  handleMouseClick : function( cmp, evt, helper ){
      var results = cmp.find( "search_result" );
      $A.util.addClass( cmp.find( "load_spinner" ), "slds-show" );
      $A.util.addClass( results, "slds-is-open" );
      $A.util.removeClass( results, "slds-is-close" );
      helper.searchForMatches( cmp, "" );

  },
  handleMouseLeave : function( cmp ){
      var results = cmp.find( "search_result" );
      cmp.set( "v.list_of_records", null );
      $A.util.addClass( results, 'slds-is-close' );
      $A.util.removeClass( results, 'slds-is-open' );
  },
  handleKeyUp : function( cmp, evt, helper ){
      var keyword_input = cmp.get( "v.search_keyword" );
      var results = cmp.find( "search_result" );

      if( keyword_input.length > 0 ){
          $A.util.addClass( results, "slds-is-open" );
          $A.util.removeClass( results, "slds-is-close" );
          helper.searchForMatches( cmp, keyword_input );
      }
      else{
          cmp.set( "v.list_of_records", null );
          $A.util.addClass( results, "slds-is-close" );
          $A.util.removeClass( results, "slds-is-open" );
      }
  },
  handleEvent : function( cmp, evt, helper ){
      var selected_account = evt.getParam( "record_event" );
      var current_element;

      cmp.set( "v.selected_record", selected_account );
      cmp.set( "v.record_id", selected_account.Id );

      current_element = cmp.find( "lookup_pill" );
      $A.util.addClass( current_element, "slds-show" );
      $A.util.removeClass( current_element, "slds-hide" );

      current_element = cmp.find( "search_result" );
      $A.util.addClass( current_element, "slds-is-close");
      $A.util.removeClass( current_element, "slds-is-open");
      current_element = cmp.find( "lookup_field" );
      $A.util.addClass( current_element, "slds-hide");
      $A.util.removeClass( current_element, "slds-show");
  },
  clearSelection : function( cmp, evt, helper ){
      var current_element = cmp.find( "lookup_pill" );
      $A.util.addClass( current_element, "slds-hide" );
      $A.util.removeClass( current_element, "slds-show" );

      current_element = cmp.find( "lookup_field" );
      $A.util.addClass( current_element, "slds-show" );
      $A.util.removeClass( current_element, "slds-hide" );

      //clear component attributes
      cmp.set( "v.search_keyword", null );
      cmp.set( "v.list_of_records", null );
      cmp.set( "v.selected_record", {} );
      cmp.set( "v.record_id", null);
  },
  setRecord : function( cmp, evt, helper ){
      var current_element;
      var parameters = evt.getParam( "arguments" );

      cmp.set( "v.selected_record", parameters.target_record );

      current_element = cmp.find( "lookup_pill" );
      $A.util.addClass( current_element, "slds-show" );
      $A.util.removeClass( current_element, "slds-hide" );

      current_element = cmp.find( "search_result" );
      $A.util.addClass( current_element, "slds-is-close" );
      $A.util.removeClass( current_element, "slds-is-open" );

      current_element = cmp.find( "lookup_field" );
      $A.util.addClass( current_element, "slds-hide" );
      $A.util.removeClass( current_element, "slds-show");
  }
});