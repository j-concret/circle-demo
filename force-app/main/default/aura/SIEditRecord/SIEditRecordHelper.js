({
	fetchData: function (cmp,event,helper) {
        var SupplierInvoiceID1 = cmp.get("v.soidFromFlow1"); // supplierInvoiceID
        var action = cmp.get("c.getCostings");
       action.setParams({  
           SupplierInvoiceID1 : cmp.get("v.soidFromFlow1"),
            });
         console.log(SupplierInvoiceID1);
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var data = response.getReturnValue();
                cmp.set('v.data',data);
            console.log(data);
                  
            }
            // error handling when state is "INCOMPLETE" or "ERROR"
        });
        $A.enqueueAction(action);
    },
   
    //Insert New Costings
    addAccountRecord: function(component, event) {
        //get the account List from component  
        var accountList = component.get("v.accountList");
        //Add New Account Record
        accountList.push({
            'sobjectType': 'CPA_Costing__c',
            'Name': '',
            'Cost_Category__c': '',
            'Invoice_amount_local_currency__c': ''
        });
        component.set("v.accountList", accountList);
    },
     
    validateAccountList: function(component, event) {
        //Validate all account records
        var isValid = true;
        var accountList = component.get("v.accountList");
        for (var i = 0; i < accountList.length; i++) {
            if (accountList[i].Name == '') {
                isValid = false;
                alert('Please enter a Costing Name ' + (i + 1));
            }
        }
        return isValid;
    },
     
    saveAccountList: function(component, event, helper) {
        //Call Apex class and pass account list parameters
        var action = component.get("c.saveCostings");
        var accountList = component.get("v.accountList");
        var SupplierInvoiceID1 = component.get("v.soidFromFlow1");
        
        action.setParams({
            "accList": component.get("v.accountList"),
            SupplierInvoiceID1 : component.get("v.soidFromFlow1")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.accountList", []);
                alert('Costings records saved successfully');
                location.reload();
            }
            else{
                
                 alert('Costings are not created, Please send detailed email to support_SF@tecex.com');
                
            }
            
        }); 
        $A.enqueueAction(action);
    },
})