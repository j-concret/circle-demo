({
    doInitHlpr: function(component, helper) {
        component.set("v.showSpinner", true);
        var params = {
            shipmentId: component.get("v.recordId")
        };
        this.makeServerCall(component, "getFinanceData", params, function(result) {
            if(result.status === 'OK') {
                if(result.data.length> 0){
                    if(result.data[0].Customs_Clearance_Documents__r){
                        component.set("v.CustomsClearance_Documents_ID",result.data[0].Customs_Clearance_Documents__r[0].Id);        
                    }
                   component.set("v.clientAccountId", result.data[0].Account__c);
                }
                
                
            }
            else {
                helper.showToast("error", "Error Occured Helper", result.msg);
            }
            component.set("v.showSpinner", false);
        });
    },
    activeSections: function(component, event){
        var selTab = component.get("v.selTabId");
        if(selTab == 'Finance Fields'){
            var editFormGeneralInformation = component.find("editFormAccount");
            $A.util.toggleClass(editFormGeneralInformation, "slds-hide");
            var viewFormGeneralInformation = component.find("viewFormAccount");
            $A.util.toggleClass(viewFormGeneralInformation, "slds-hide");
        }else if(selTab == 'CCD Fields'){
            var editFormGeneralInformation = component.find("editFormCCD");
            $A.util.toggleClass(editFormGeneralInformation, "slds-hide");
            var viewFormGeneralInformation = component.find("viewFormCCD");
            $A.util.toggleClass(viewFormGeneralInformation, "slds-hide");   
        }
        else if(selTab == 'Minimums'){
            var editFormGeneralInformation = component.find("editFormMinimums");
            $A.util.toggleClass(editFormGeneralInformation, "slds-hide");
            var viewFormGeneralInformation = component.find("viewFormMinimums");
            $A.util.toggleClass(viewFormGeneralInformation, "slds-hide");   
        }else if(selTab == 'Quotes'){
            var editFormGeneralInformation = component.find("editFormQuotes");
            $A.util.toggleClass(editFormGeneralInformation, "slds-hide");
            var viewFormGeneralInformation = component.find("viewFormQuotes");
            $A.util.toggleClass(viewFormGeneralInformation, "slds-hide");   
        }
    }
})