({
  onTaskUpdateHandler: function(component, event, helper) {
    var task = event.getParam('task');// new status is in task object
    var OldStatus = event.getParam('OldStatus'); // oldStatus that is changed from
    //var matched = false;
    var sections = component.get('v.sectionData');
    var sectionData = JSON.parse(JSON.stringify(sections.shipmentStatuses));

    if(sectionData) {
      for(var i = 0; i < sectionData.length; i++) {
        // if new updated/dragged task status matched in array, adding into it at appropriate location
        if(sectionData[i].status == task.status) {
          sectionData[i].isOpen = true;
          if(sectionData[i].isFlag) {
            var tasks = sectionData[i].tasks;
            for(var j = 0; j < tasks.length; j++) {
              if(tasks[j].name == task.flagType) {
                var isMatched = false;
                //matched = true;
                var flagTasks = tasks[j].flagTasks;
                for(var k = 0; k < flagTasks.length; k++) {
                  if(task.partProdKey == 'others') break; // no need to group these types of tasks
                  if(flagTasks[k].isGrouped && flagTasks[k].grpName == task.partProdKey) {
                    flagTasks[k].groupedTasks.push(task);
                    flagTasks[k].colorCode = task.colorCode < flagTasks[k].colorCode ? task.colorCode : flagTasks[k].colorCode
                    isMatched = true;
                    break;
                  } else if(!flagTasks[k].isGrouped && flagTasks[k].partProdKey == task.partProdKey) {
                    isMatched = true;
                    var existTask = JSON.parse(JSON.stringify(flagTasks[k]));
                    var grpTask = {
                      ruleName: task.ruleName,
                      isGrouped: true,
                      grpName: task.partProdKey,
                      groupedTasks: [existTask, task],
                      colorCode: task.colorCode < flagTasks[k].colorCode ? task.colorCode : flagTasks[k].colorCode
                    };
                    flagTasks[k] = grpTask;
                    break;
                  }
                }
                if(!isMatched)
                  flagTasks.push(task);

                flagTasks.sort((a,b) => (a.colorCode > b.colorCode) ? 1 : ((b.colorCode > a.colorCode) ? -1 : 0));
                if(flagTasks[0])
                  tasks[j].colorName = flagTasks[0].colorCode == 1 ? 'red' : flagTasks[0].colorCode == 2 ? 'orange' : flagTasks[0].colorCode == 3 ? 'yellow' : 'green';
                else
                  tasks[j].colorName = 'green';

                sectionData[i].count += 1;
                break;
              }
            }
            /* if(!matched) {
              sectionData[i].count += 1;
              tasks.push({'name': task.flagType, 'flagTasks': [task]});
            } */
          } else {
            var isMatched = false;
            sectionData[i].count += 1;
            var tasks = sectionData[i].tasks;
            for(var j = 0; j < tasks.length; j++) {
              if(task.partProdKey == 'others') break;
              if(tasks[j].isGrouped && tasks[j].grpName == task.partProdKey) {
                tasks[j].groupedTasks.push(task);
                isMatched = true;
                break;
              } else if(!tasks[j].isGrouped && tasks[j].partProdKey == task.partProdKey) {
                isMatched = true;
                var existTask = JSON.parse(JSON.stringify(tasks[j]));
                var grpTask = {
                  ruleName: task.ruleName,
                  isGrouped: true,
                  grpName: task.partProdKey,
                  groupedTasks: [existTask, task]
                };
                tasks[j] = grpTask;
              }
            }
            if(!isMatched)
              tasks.push(task);
          }
        }
        // if new updated/dragged task old status matched in array, removing from array data from appropriate location
        else if(sectionData[i].status == OldStatus) {
          sectionData[i].isOpen = true;
          var tskInd = -1;
          var tasks = sectionData[i].tasks;
          if(sectionData[i].isFlag) {
            for(var j = 0; j < tasks.length; j++) {
              if(tasks[j].name == task.flagType) {
                var innerTasks = tasks[j].flagTasks;
                var ind = -1;
                var indd = -1;
                for(var k = 0; k < innerTasks.length; k++) {
                  if(innerTasks[k].isGrouped) {
                    var grpTasks = innerTasks[k].groupedTasks;
                    for(var l = 0; l < grpTasks.length; l++) {
                      if(grpTasks[l].id == task.id) {
                        indd = l;
                        break;
                      }
                    }
                    if(indd != -1) {
                      sectionData[i].count -= 1;
                      grpTasks.splice(indd, 1);
                      if(grpTasks.length == 1) {
                        innerTasks[k] = JSON.parse(JSON.stringify(grpTasks[0]));
                      }
                      else {
                        grpTasks.sort((a,b) => (a.colorCode > b.colorCode) ? 1 : ((b.colorCode > a.colorCode) ? -1 : 0));
                        innerTasks[k].colorCode = grpTasks[0].colorCode;
                      }
                      break;
                    }

                  } else {
                    if(innerTasks[k].id == task.id) {
                      ind = k;
                      break;
                    }
                  }
                }
                if(ind != -1) {
                  sectionData[i].count -= 1;
                  innerTasks.splice(ind, 1);
                  //if(innerTasks.length == 0) tskInd = j;
                }
                //updating colorName of flag on task removal
                innerTasks.sort((a,b) => (a.colorCode > b.colorCode) ? 1 : ((b.colorCode > a.colorCode) ? -1 : 0));
                if(innerTasks[0])
                  tasks[j].colorName = innerTasks[0].colorCode == 1 ? 'red' : innerTasks[0].colorCode == 2 ? 'orange' : innerTasks[0].colorCode == 3 ? 'yellow' : 'green';
                else
                tasks[j].colorName = 'green';
                break;
              }
            }
          } else {
            var ind = -1;
            for(var k = 0; k < tasks.length; k++) {
              if(tasks[k].isGrouped) {
                var indd = -1;
                var grpTasks = tasks[k].groupedTasks;
                for(var l = 0; l < grpTasks.length; l++) {
                  if(grpTasks[l].id == task.id) {
                    indd = l;
                    break;
                  }
                }
                if(indd != -1) {
                  grpTasks.splice(indd, 1);
                  if(grpTasks.length == 1) tasks[k] = JSON.parse(JSON.stringify(grpTasks[0]));
                  else if(grpTasks.length == 0) ind = k;
                  break;
                }
              } else {
                if(tasks[k].id == task.id) {
                  ind = k;
                  break;
                }
              }
            }
            if(ind != -1) {
              tasks.splice(ind, 1);
            }
            sectionData[i].count -= 1;
          }
        }
      }
      sections.shipmentStatuses = sectionData;
      component.set('v.sectionData', sections);
    }
  }
})