({

    /** this function will handle component mouse click
     *  @param componentP is the component we are updating
     *  @param helperP is the helper object
     */
    handleMouseClick : function( componentP, eventP, helperP ){

        console.log( "handleMouseClick" );

        var results = componentP.find( "search_result" );         //find tag with given ID

        $A.util.addClass( componentP.find( "load_spinner" ), "slds-show" );        //show spinner
        $A.util.addClass( results, "slds-is-open" );                            //add open class
        $A.util.removeClass( results, "slds-is-close" );                        //remove the close class

        helperP.searchForMatches( componentP, eventP, "" );                     //clear the search for matches

    },//end of function definition





    /** this function will handle when mouse leaves component
     *  componentP is the component we are updating
     */
    handleMouseLeave : function( componentP ){

        var results = componentP.find( "search_result" );         //find tag with given ID
        componentP.set( "v.list_of_records", null );                //clear current list of records

        $A.util.addClass( results, 'slds-is-close' );               //add close class
        $A.util.removeClass( results, 'slds-is-open' );             //remove the open class

    },//end of function definition





    /** this function will handle key up event
     * @param componentP is component we are updating
     * @param eventP is the event that just occured
     * @param helperP is the helper object
     */
    handleKeyUp : function( componentP, eventP, helperP ){

        var keyword_input = componentP.get( "v.search_keyword" );           //this is the keyword input
        var results = componentP.find( "search_result" );             //find the search result ID

        //check if we have input
        if( keyword_input.length > 0 ){

            console.log( 'In handleUp' );

            $A.util.addClass( results, "slds-is-open" );            //add open class
            $A.util.removeClass( results, "slds-is-close" );        //remove close class

            helperP.searchForMatches( componentP, eventP, keyword_input );         //perform search

        }
        else{

            componentP.set( "v.list_of_records", null );            //clear list of records from server

            $A.util.addClass( results, "slds-is-close" );           //add close class
            $A.util.removeClass( results, "slds-is-open" );            //remove open class

        }//end of if-else block

    },//end of function definition





    /** this function will handle component event
     *  @param componentP is the component we are updating
     *  @param eventP is the component event that occured
     *  @param helperP is the helper class object
     */
    handleEvent : function( componentP, eventP, helperP ){

        var selected_account = eventP.getParam( "record_event" );           //get the record in given event
        var current_element;                          //this will indicate what we are closing

        componentP.set( "v.selected_record", selected_account );            //set the selected record
        componentP.set( "v.record_id", selected_account.Id );               //set the record ID

        console.log( "ID "+ componentP.get( "v.selected_record" ).Id );

        current_element = componentP.find( "lookup_pill" );                 //find pill
        $A.util.addClass( current_element, "slds-show" );                   //we show pill
        $A.util.removeClass( current_element, "slds-hide" );                //removing hide

        current_element = componentP.find( "search_result" );               //find the search result
        $A.util.addClass( current_element, "slds-is-close" );               //add close class
        $A.util.removeClass( current_element, "slds-is-open" );             //remove open class

        current_element = componentP.find( "lookup_field" );                //find the lookup field
        $A.util.addClass( current_element, "slds-hide" );                   //add hide class
        $A.util.removeClass( current_element, "slds-show" );                //remove show class

    },//end of function definition





    /** function that will clear the Record Selection
     *  @param componentP is the component we are updating
     *  @param eventP is the event we are looking at
     *  @param helperP is the helper object
     */
    clearSelection : function( componentP, eventP, helperP ){

        var current_element = componentP.find( "lookup_pill" );                //find lookup_pill ID
        $A.util.addClass( current_element, "slds-hide" );           //add hide class
        $A.util.removeClass( current_element, "slds-show" );        //remove show class

        current_element = componentP.find( "lookup_field" );            //find the lookup field
        $A.util.addClass( current_element, "slds-show" );               //add show class
        $A.util.removeClass( current_element, "slds-hide" );            //remove add class

        //clear component attributes
        componentP.set( "v.search_keyword", null );                 // clear the search keyword
        componentP.set( "v.list_of_records", null );                //clear the list of records
        componentP.set( "v.selected_record", {} );                //clear selected record

    },//end of function definition





    /** this function will set selected record
     *  @param componentP this is the component we are updating
     *  @param eventP is the event we are looking at
     *  @param helperP is the helper object
     */
    setRecord : function( componentP, eventP, helperP ){

        console.log( "set the record" );

        var current_element;                                                //this will indicate what we are closing
        var parameters = eventP.getParam( "arguments" );                         //get the parameters

        componentP.set( "v.selected_record", parameters.target_record );                           //set the selected record to target record

        current_element = componentP.find( "lookup_pill" );                 //find pill
        $A.util.addClass( current_element, "slds-show" );                   //we show pill
        $A.util.removeClass( current_element, "slds-hide" );                //removing hide

        current_element = componentP.find( "search_result" );               //find the search result
        $A.util.addClass( current_element, "slds-is-close" );               //add close class
        $A.util.removeClass( current_element, "slds-is-open" );             //remove open class

        current_element = componentP.find( "lookup_field" );                //find the lookup field
        $A.util.addClass( current_element, "slds-hide" );                   //add hide class
        $A.util.removeClass( current_element, "slds-show" );                //remove show class

    },//end of function definition





    /** this function will clear selected record
     *  @param componentP this is the component we are updating
     *  @param eventP is the event we are looking at
     *  @param helperP is the helper object
     */
    clearRecord : function( componentP, eventP, helperP ){

        var current_element = componentP.find( "lookup_pill" );                //find lookup_pill ID

        $A.util.addClass( current_element, "slds-hide" );           //add hide class
        $A.util.removeClass( current_element, "slds-show" );        //remove show class

        current_element = componentP.find( "lookup_field" );            //find the lookup field
        $A.util.addClass( current_element, "slds-show" );               //add show class
        $A.util.removeClass( current_element, "slds-hide" );            //remove add class

        //clear component attributes
        componentP.set( "v.search_keyword", null );                 // clear the search keyword
        componentP.set( "v.list_of_records", null );                //clear the list of records
        componentP.set( "v.selected_record", {} );                //clear selected record

    }//end function definition

})