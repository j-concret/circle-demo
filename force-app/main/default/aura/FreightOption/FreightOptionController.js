/**
 * Created by Chibuye Kunda on 2019/01/16.
 */
({


    /** this function will handle picklist change event
     *  @param componentP this is the component we are updating
     *  @param eventP this is the event we are processing
     *  @param helperP is the helper object
     */
    handlePicklistEvent : function( componentP, eventP ){

        var picklist_value = eventP.getParam( "picklist_value" );                //get the parameter from event

        console.log( "Running" );

        //check if we dont have tecEX or client picklist values
        if( ( picklist_value !== "TecEx" ) && ( picklist_value !== "Client" ) )
            return;                 //terminate function

        //check if the picklist value is TecEx
        if( picklist_value === "TecEx" )
            componentP.set( "v.is_tecex", true );
        else
            componentP.set( "v.is_tecex", false );

    },//end of function definition
    
     // For Custom Footer
    handleClick : function(component, event, helper) {
        		console.log('entering handleclick');
        		component.set("v.fire", true);
              	var destType = component.get("v.destinationType");
              	if (destType == 'url') { 
                    console.log('destType is url');
                    var urlEvent = $A.get("e.force:navigateToURL");
             		console.log('destination is url');
    				var destUrl = component.get("v.destinationURL");
             		console.log('url is '+destUrl)	
          			if( (typeof sforce != 'undefined') && (sforce.one != null) ) {
						//handle lightning experience on desktop
						console.log("running navigation for lightning desktop");
                		urlEvent.setParams({
            				"url": destUrl
        	 			});      
                        console.log("Firing urlEvent: "+urlEvent)
                    //appears to be broken
            		urlEvent.fire();
        			}
    				else {
            			var device = $A.get("$Browser.formFactor");
            			if (device=="DESKTOP") {
             		   		console.log("running navigation for classic desktop");
            		    	window.location = destUrl;
           				}
					}
                }
            	else {
                    if (destType == 'record') {
                        var urlEvent = $A.get("e.force:navigateToSObject");
                        console.log('destination is record');
                        var destObject = component.get("v.targetRecordId");
                        console.log('recordId is:' +destObject)
                        urlEvent.setParams({
                            "recordId": destObject,
                            "slideDevName": "related",
                            "isredirect": true
                        });
                        console.log("running urlEvent for Record ID")
                        if( (typeof sforce != 'undefined') && (sforce.one != null) ) {
                            //handle lightning experience on desktop
                            console.log("running navigation for lightning desktop");
                            //appears to be broken
                            urlEvent.fire();
                        }
                        else {
                            console.log("running navigation for classic")
                            window.location = '/'+destObject; 
                        }
                    }
                    
                    else {
                        if (destType == 'standard') {
                            console.log('entering standard');
                            var navigationGoal = component.get("v.navigationType");
        
                            //component.set("v.fire", true);
                            var navigate = component.get('v.navigateFlow');
                            
                            if (navigationGoal.toLowerCase() == 'finish') {
                              navigate("FINISH");  
                            }
                            
                            if (navigationGoal.toLowerCase() == 'next') {
                            var IntCourier = component.get("v.int_courier");
                            if(IntCourier == 'TecEx'){
                           	var allValid = component.find('fieldId').reduce(function (validSoFar, inputCmp)
                            {
           					inputCmp.showHelpMessageIfInvalid();
            				return validSoFar && !inputCmp.get('v.validity').valueMissing;
        					}, true);
                                
        						if (allValid) {
            								//	alert('All form entries look valid. Ready to submit!');
            									navigate("NEXT");
        										} else {
            								//	alert('Please enter below required fields');
                                              alertify.alert('', 'Please enter below required fields');      
        												}
    
                                }
                                else {
                                    navigate("NEXT");
                                }
                                
                                
                            }  
                            if (navigationGoal.toLowerCase() == 'back') {
                              navigate("BACK");  
                            }  
                        }
                    }
            	}
    }  
   // End of Custom Footer 	


})