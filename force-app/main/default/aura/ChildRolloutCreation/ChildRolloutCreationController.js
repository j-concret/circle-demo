({
     Homereturn :function (cmp, event, helper) { cmp.set("v.displayedSection","HomePage"); },
	 doInit: function(componentP, eventP, helperP ) {
        helperP.sethelper(componentP, eventP);
        window.addEventListener("message", helperP.receiveMessage.bind(helperP), false);
       
        componentP.set( "v.table_headings",
                        [ "#","How many packages?", "Weight Unit", "Actual Weight",
                          "Dimension Unit", "Length", "Breadth", "Height" ] );
         
        
        var action = componentP.get("c.getDestinationValues");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS"){
                var result = response.getReturnValue();
                var plValues = [];
                for (var i = 0; i < result.length; i++) {
                    plValues.push({
                        label: result[i],
                        value: result[i]
                    });
                }
                componentP.set("v.GenreList", plValues);
            }
        });
        $A.enqueueAction(action);
    },
    handleGenreChange: function (component, event, helper) {
        //Get the Selected values   
        var selectedValues = event.getParam("value");
          var CselectedGenreListSize =selectedValues.length;
         component.set("v.selectedGenreListSize",CselectedGenreListSize);
        
        //Update the Selected Values  
        component.set("v.selectedGenreList", selectedValues);
    },
     
    getSelectedGenre : function(component, event, helper){
        //Get selected Genre List on button click 
        var selectedValues = component.get("v.selectedGenreList");
        console.log('Selectd Genre-' + selectedValues);
    },
    
    
       
    //******** Package Model Body Start***********//
   
    addNewRow : function( componentP, eventP, helperP ){

        helperP.createTableRow( componentP, eventP );

        var package_list = componentP.get( "v.shipment_order_package_list" );

        componentP.set( "v.object_list_string", helperP.createObjectString( package_list ) );

    },
     deleteRow : function( componentP, eventP, helperP ){

        var index = eventP.getParam( "index" );         //get the index
        var rows_list = componentP.get( "v.shipment_order_package_list" );

        rows_list.splice( index, 1 );           //remove the value

        componentP.set( "v.shipment_order_package_list", rows_list );
        componentP.set( "v.object_list_string", helperP.createObjectString( componentP.get( "v.shipment_order_package_list" ) ) );

    },
    
  //******** Package Model Body end***********//
    
    //******** Package ModelBOX  start***********//
     openPackageModel : function(cmp, event, helper) {
         
         
     var a = event.getSource().get("v.value");
         if(a=="No"){
         			cmp.set("v.isPackageModelOpen",false);
                     }
         else{
             cmp.set("v.isPackageModelOpen",true);
             
         }
         
         
     },
     
     closePackageModel : function(cmp, event, helper) {cmp.set("v.isPackageModelOpen", false);},
     submitPackageDetails : function(cmp, event, helper) {
      var list_string = "";
         var object_listP = cmp.get("v.shipment_order_package_list");     
    
         //loop through the object list
        for( var i=0; i < object_listP.length; ++i ){
	
            
            //add to list string
            list_string = list_string + object_listP[i].packages_of_same_weight_dims__c + "|" + object_listP[i].Weight_Unit__c + "|" +
                                        object_listP[i].Actual_Weight__c + "|" + object_listP[i].Dimension_Unit__c + "|" +
                                        object_listP[i].Length__c + "|" + object_listP[i].Breadth__c + "|" +
                                        object_listP[i].Height__c + ":";

           if (object_listP[i].packages_of_same_weight_dims__c.length==0 ||
              object_listP[i].Weight_Unit__c.length==0 ||
              object_listP[i].Actual_Weight__c.length==0 ||
              object_listP[i].Dimension_Unit__c.length==0 ||
              object_listP[i].Length__c.length==0 ||
              object_listP[i].Breadth__c.length==0 ||
              object_listP[i].Height__c.length==0) {
               
               console.log( "list_string in Submit package details - Has Blank " );
               cmp.find('notifLib').showToast({
				"variant": "warning",
				"message": "Please Enter all required fields"
			});
           }
            
        }//end of if block       
         
         console.log( "list_string in Submit package details " + list_string );
         
         cmp.set("v.isPackageModelOpen", false);
     
     
     },
//******** Package ModelBOX  End***********//

     //******** Create Rollout***********//
    CreateCEButton : function(component, event, helper) {
				//component.set("v.is_loading", true);
				component.set("v.showLoadingSpinner", true);
        
        var CselectedGenreList = component.get("v.selectedGenreList");
      
        var myJSON = JSON.stringify(CselectedGenreList);
        
        component.set("v.destination",myJSON);
       
        
       
        var CAccID = component.get("v.account_id");
        var CConID = component.get("v.client_contact");
        var CshipFrom = component.get("v.ship_from");
        var Cdestination = component.get("v.destination");
        var Cint_courier = component.get("v.int_courier");
        var CService_required = component.get("v.Service_required");
        var Cshipment_value = component.get("v.shipment_value");
        var Cref_1 = component.get("v.ref_1");
        var Cref_2 = component.get("v.ref_2");
        var Cchargeable_weight_estimate = component.get("v.chargeable_weight_estimate");
        var Cshipment_order_package_list = component.get("v.shipment_order_package_list");
        var CPart_List = component.get("v.parts_list");// part list 
        var CSecondhandgoods = component.get("v.Secondhandgoods");
        var CLi_ion_Batteries = component.get("v.Li_ion_Batteries");
        var CLi_ion_BatteryTypes = component.get("v.Li_ion_BatteryTypes");
        var CDestinationlistsize = component.get("v.selectedGenreListSize");
        var CNumber_of_FinalDeliveries = component.get("v.Number_of_FinalDeliveries");
       
          console.log('CAccID',CAccID);
          console.log('CConID',CConID);
          console.log('CshipFrom',CshipFrom);
          console.log('Cdestination',Cdestination);
          console.log('Cint_courier',Cint_courier);
          console.log('CService_required',CService_required);
          console.log('Cshipment_value',Cshipment_value);
          console.log('Cref_1',Cref_1);
          console.log('Cref_2',Cref_2);
          console.log('Cchargeable_weight_estimate',Cchargeable_weight_estimate);
          console.log('Cshipment_order_package_list',Cshipment_order_package_list);
          console.log('CDestinationlistsize',CDestinationlistsize);
          
        
	if(CAccID==null || CConID == null || CshipFrom == null || CService_required == null || Cint_courier==null || CLi_ion_Batteries == null || (CLi_ion_Batteries == "Yes" &&CLi_ion_BatteryTypes == null) || CDestinationlistsize == 0 || Cchargeable_weight_estimate == null){
			component.set("v.showLoadingSpinner", false);
			component.find('notifLib').showToast({
				"variant": "warning",
				"message": "Please Enter all required fields"
			});
		} 
		else{
        var action = component.get("c.CreateRollout");
      
        action.setParams({"PAccID" : CAccID,
                          "PConID" : CConID,
                          "PshipFrom" : CshipFrom,
                           "Pdestination" : Cdestination,
                           "Pint_courier" : Cint_courier,
                          "PService_required" : CService_required,
                          "Pshipment_value" : Cshipment_value,
                          "Pref_1" : Cref_1,
                          "Pref_2" : Cref_2,
                          "Pchargeable_weight_estimate" : Cchargeable_weight_estimate,
                          "Pshipment_order_package_list" : Cshipment_order_package_list,
                          "PPart_List" : CPart_List,
                          "PSecondhandgoods" : CSecondhandgoods,
                          "PLi_ion_Batteries" : CLi_ion_Batteries,
                          "PLi_ion_BatteryTypes" : CLi_ion_BatteryTypes,
                          "PDestinationlistsize" : CDestinationlistsize,
                          "PNumber_of_FinalDeliveries" : CNumber_of_FinalDeliveries
                                                  
                         });
         action.setCallback(this, function(response) {  
             //component.set("v.is_loading", false);
            var state = response.getState();
              console.log(state);
            if (state === 'SUCCESS'){
							
                var data = response.getReturnValue();
								console.log('Data ===> '+JSON.stringify(data));
                console.log(data);
                console.log(data.length);
                
               
                
                if(data.length===18){
                     component.set('v.CreatedRolloutID',data);
                     component.set('v.ROIDLength',data.length);
								var ROIDtoClose = component.get("v.CreatedRolloutID");
								
								component.set("v.showLoadingSpinner", false);
								component.find('notifLib').showToast({
									"variant": "success",
									"message": "Roll Out Created"
								});
                }
                else {
                  				component.set("v.showLoadingSpinner", false);
								component.find('notifLib').showToast({
								"variant": "error",
								"message": "Roll-Out not created, please contact Support"
										});
                    
                }
	
               }
            else{
								//alert("Rollout not created, Please contact Sf");
								component.set("v.showLoadingSpinner", false);
								component.find('notifLib').showToast({
								"variant": "error",
								"message": "Roll-Out not created, please contact Support"
										});
            }  
            
        });
        $A.enqueueAction(action); 

	}  
			

},
     //******** Part ModelBOX  start***********//
     openPartModel : function(cmp, event, helper) {
         
         
     var a = event.getSource().get("v.Partvalue");
         if(a=="No"){
         			cmp.set("v.isPartModelOpen",'false');
                     }
         else{
             cmp.set("v.isPartModelOpen",'true');
             
         }
         
         
     },
     
		 closePartModel : function(cmp, event, helper) {
			
			cmp.set("v.isPartModelOpen", false);
		
		 },
		 
		 
		 submitPartDetails : function(cmp, event, helper) {
			
			cmp.set("v.isPartModelOpen", false);
		 
		},
//******** Part ModelBOX  End***********//
     //******** Insert SOP Start***********//
    updateSOP : function(component, event, helper) {
				//component.set("v.is_loading", true);
				component.set("v.showLoadingSpinner", true);

        var Cshipment_order_package_list = component.get("v.shipment_order_package_list");
        var CROID = component.get("v.CreatedRolloutID");//RolloutID
       
        
        var action1 = component.get("c.updatepackages");
      
        action1.setParams({"PROID" : CROID,
                           "Pshipment_order_package_list" : Cshipment_order_package_list
                            });
         action1.setCallback(this, function(response) {  
             //component.set("v.is_loading", false);
            var state = response.getState();
              console.log(state);
            if (state === 'SUCCESS'){
							
							
                var data = response.getReturnValue();
                console.log(data);
                 //alert("SOP inserted");  
                 component.set('v.Secondstepcompleted','True');
								 component.set("v.showLoadingSpinner", false);
								 component.find('notifLib').showToast({
									"variant": "success",
									"message": "SOP inserted"
								});
               }
            else{
							
								//alert("SOP not inserted, Please contact Sf");
								component.set("v.showLoadingSpinner", false);
								component.find('notifLib').showToast({
									"variant": "error",
									"message": "SOP not inserted, Please contact Sf"
								});
            }  
            
        });
				$A.enqueueAction(action1);  
		
    }, 
    //******** insert Parts Start***********//
    insertParts : function(component, event, helper) {

				//component.set("v.is_loading", true);
        component.set("v.showLoadingSpinner", true);      
        
        var CPart_List = component.get("v.parts_list");// part list 
        var CROID = component.get("v.CreatedRolloutID");//RolloutID
        console.log('Created Rollout',CROID);
				
        var action2 = component.get("c.Insertingparts");
      
        action2.setParams({"PROID" : CROID,
                           "PPart_List" : CPart_List
                            });
         action2.setCallback(this, function(response) {  
             //component.set("v.is_loading", false);
            var state = response.getState();
              console.log(state);
            if (state === 'SUCCESS'){

							
                
                var data = response.getReturnValue();
                console.log(data);
									 //alert("Parts inserted"); 
                component.set('v.Thirdstepcompleted','True');
								
								component.set("v.showLoadingSpinner", false); 
								component.find('notifLib').showToast({
									"variant": "success",
									"message": "Parts inserted"
								});	           

                
               }
            else{
							
								//alert("Parts not inserted, Please contact Sf");
								component.set("v.showLoadingSpinner", false); 
								component.find('notifLib').showToast({
									"variant": "error",
									"message": "Parts not inserted, Please contact Sf"
								});
								
            }  
        
        });
				$A.enqueueAction(action2);  
			
		}, 
     //******** return to  Rollout***********//
    GotoRollout : function(component, event, helper) {
				component.set("v.is_loading", true);
				
              
                var ROIDtoClose = component.get("v.CreatedRolloutID");
                component.set('v.isRolloutDone',true);
         var action1 = component.get(
            "c.processRollouts");
        action1.setParams({rolloutId : ROIDtoClose});
        action1.setCallback(this,
                            function (response) {
                                var state =
                                    response.getState();
                                var result = response.getReturnValue(); 
                                console.log(result);
                            
                var hostname = window.location.hostname;
				var arr = hostname.split(".");
				var instance = arr[0];
                
                component.set("v.iframeUrl2","https://"+instance+".lightning.force.com/lightning/r/Rollout__c/"+ ROIDtoClose+"/view");
                var address =  component.get("v.iframeUrl2");
                window.open(address,'_top');
                                });
        $A.enqueueAction(action1);
                     
},
		 //********   ***********//
    RolloutSO : function(component, event, helper) {
				
               
                 
        var evt = $A.get("e.force:navigateToComponent");
        console.log('Event '+evt);
                var ROIDtoClose = component.get("v.CreatedRolloutID");
        		var AccId = component.get("v.account_id");
        		var ConID = component.get("v.client_contact");
        evt.setParams({
            componentDef  : "c:ChildRolloutAddmoresSO" ,
            componentAttributes : {
                CreatedRolloutID : ROIDtoClose,
                account_id : AccId, 
                client_contact : ConID
            }
        

        });
      
        evt.fire();
                     
},
		
    
})