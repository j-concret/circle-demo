({
  getSORecord : function(cmp) {
    var self = this;
    cmp.set('v.showSpinner',true);
    self.serverCall(cmp,'getShipmentRecord',{
      recordId : cmp.get('v.recordId')
    },function(response){
      cmp.set('v.showSpinner',false);
      cmp.set('v.countries',response.countries);

      let shipment = response.record;
      let options = [
        {'label': shipment.Account__r.Name+' is Selling the goods', 'value': 'To Sell Goods'},
        {'label': shipment.Account__r.Name+' is retaining ownership of the goods', 'value': 'Retain Ownership of Goods'},
        {'label': 'It\'s more complicated', 'value': 'Other'}
      ];
      cmp.set('v.options',options);
      cmp.set('v.shipment',shipment);
      cmp.set('v.task',response.task);
      cmp.set('v.countryandRegionMap',response.CountryandRegionMap);

    },function(err){
      cmp.set('v.showSpinner',false);
      self.showToast('error','ERROR',err);
    });

  },
  showToast : function(title,type,msg) {
    var toastEvent = $A.get('e.force:showToast');
    toastEvent.setParams({
      'title': title,
      'type' :type,
      'message': msg
    });
    toastEvent.fire();
  },
  updateSOTask:function(cmp){
    var self = this;
    cmp.set('v.showSpinner',true);
    self.serverCall(cmp,'updateShipmentTask',{
      task : cmp.get('v.task'),
      shipment : cmp.get('v.shipment')
    },function(response){
      cmp.set('v.showSpinner',false);
      cmp.set('v.submit_label','Closed');
      self.showToast('success','SUCCESS','Task successfully closed.');
      self.closeTab(cmp);
    },function(err){
      cmp.set('v.showSpinner',false);
      self.showToast('error','ERROR',err);
    });
  },
  closeTab : function(cmp){
    var workspaceAPI = cmp.find('workspace');
    var focusedTabId = cmp.get('v.tabId');

    if(focusedTabId){
      workspaceAPI.closeTab({tabId: focusedTabId});
    }
  }
})