({
  doInit : function(cmp, event, helper) {
    var soId = cmp.get('v.pageReference').state.c__tskId;
    cmp.set('v.recordId',soId);
    var workspaceAPI = cmp.find('workspace');

  workspaceAPI.getAllTabInfo().then(function(response) {
    var focusedTabId;
    for(var i=0;i<response.length;i++){
      if(response[i].isSubtab && response[i].title == 'Loading...' && response[i].pageReference.attributes && response[i].pageReference.attributes.componentName && response[i].pageReference.attributes.componentName == 'c__NLProductBO'){
        focusedTabId = response[i].tabId;
        break;
      }else if(response[i].subtabs){
        var subtabs = response[i].subtabs;
        for(var j=0;j<subtabs.length;j++){
          if(subtabs[j].title === 'Loading...' && subtabs[j].pageReference.attributes && subtabs[j].pageReference.attributes.componentName && subtabs[j].pageReference.attributes.componentName == 'c__NLProductBO'){
            focusedTabId = subtabs[j].tabId;
            break;
          }
        }
      }
    }

    if(focusedTabId){
      cmp.set('v.tabId',focusedTabId);
      workspaceAPI.setTabLabel({
        tabId: focusedTabId,
        label: 'Capture Beneficial Owner Detail'
      });
      workspaceAPI.setTabIcon({
        tabId: focusedTabId,
        icon: 'standard:document',
        iconAlt: 'Capture Beneficial Owner Detail'
      });
    }

  }).catch(function(error) {
    console.log(error);
  });
    helper.getSORecord(cmp);
  },
  moveToNextStep: function(cmp,event,helper){
    let step = event.getSource().get("v.name");
    cmp.set('v.step',step);
  },
  updateTask:function(cmp,event,helper){
    helper.updateSOTask(cmp);
  },
  countryChangeHandler : function(cmp,event,helper){
    let countryandRegionMap = cmp.get('v.countryandRegionMap');
    let val = event.getSource().get('v.value');
    let eu = false;
    if(val == 'none'){
      cmp.set('v.shipment.Beneficial_Owner_Country__c',null);
    }
    else{
      cmp.set('v.shipment.Beneficial_Owner_Country__c',val);
      if(countryandRegionMap && countryandRegionMap[val] && countryandRegionMap[val].European_Union__c){
        eu = countryandRegionMap[val].European_Union__c == 'European Union';
      }
    }
    cmp.set('v.shipment.Buyer_or_BO_Part_of_EU__c',eu);
  }
})