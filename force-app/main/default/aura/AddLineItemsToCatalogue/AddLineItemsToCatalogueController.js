({
  doInit:function(component,event,helper){
    var workspaceAPI = component.find("workspace");
    workspaceAPI.getFocusedTabInfo().then(function(response) {
      if(response.title == 'Loading...'){
        var focusedTabId = response.tabId;
        workspaceAPI.setTabLabel({
          tabId: focusedTabId,
          label: "Product Catalogue"
        });
        workspaceAPI.setTabIcon({
          tabId: focusedTabId,
          icon: "standard:document",
          iconAlt: "Catalogue"
        });
      }
    }).catch(function(error) {
      console.log(error);
    });

    component.set("v.cval", "<style>.slds-modal__container{width: 60%;max-width: 60rem;}</style>");
  },
  afterScriptsLoaded : function(component, event, helper) {
    helper.loadScript();
  },
  createPartLineItems: function(component, event, helper) {
    helper.createLI(component);
  }
})