({
  loadScript : function() {
      $('#catTableData').on('paste', 'input', function(e){
        var $this = $(this);
        $.each(e.originalEvent.clipboardData.items, function(i, v){
            if (v.type === 'text/plain'){
                v.getAsString(function(text){
                    var x = $this.closest('td').index(),
                        y = $this.closest('tr').index()+1,
                        obj = {};
                    text = text.trim('\n');
                    $.each(text.split('\n'), function(i2, v2){
                      if($this.closest('table').find('tr:eq('+(y+i2)+')').length <= 0 )
                        $this.closest('table').find('tbody').append("<tr><td><input type='text'/></td><td><input type='text'/></td><td><input type='text'/></td><td><input type='text'/></td></tr>");
                        $.each(v2.split('\t'), function(i3, v3){
                            var row = y+i2, col = x+i3;
                            obj['cell-'+row+'-'+col] = v3;

                            $this.closest('table').find('tr:eq('+row+') td:eq('+col+') input').val(v3);
                        });
                    });

                });
            }
        });
        return false;

    });
    },
    createLI:function(component) {
      component.set('v.showSpinner',true);
      var lineItems = [];
      var headers = ['internal_reference','product_code','description','unit_price'];
      var msg;
      $('#catTableData tr').has('td').each(function() {
          var arrayItem = {};
          var isFirstColEmpty = false;
          $('td', $(this)).each(function(index, item) {
              var val = $(item).find("input").val() ? $(item).find("input").val().trim() :'';

              if(index == 0 && val == '')
                isFirstColEmpty = true;
              else {
                if((isFirstColEmpty && (index == 1 || index == 3 ) && val != '') || (!isFirstColEmpty && (index == 1 || index == 3) && val == '')){
                  msg = 'Please enter required fields (Internal Reference, Product Code, Quantity)';
                  return false;
                }else if(isFirstColEmpty && val == ''){
                  return false;
                }
                if(index == 3)
                  val = parseFloat(val.replace(/[,$]/g,'').trim());

                arrayItem[headers[index]] = val;
            }
          });
          if(msg)
            return false;
          if(Object.keys(arrayItem).length > 0)
            lineItems.push(arrayItem);
      });

      if(msg || lineItems.length == 0 ){
        this.showToast('Error','error',msg?msg:lineItems.length == 0 ?'No LineItems or invalid data entered!':'Error occurred');
        component.set('v.showSpinner',false);
        return;
      }

      var action = component.get("c.createLineItems");
        action.setParams({
          account_id : component.get('v.account_id'),
          lineItems : JSON.stringify(lineItems)
        });
        action.setCallback(this, function(response) {
          component.set('v.showSpinner',false);
            var state = response.getState();
            if (state === "SUCCESS") {
              var data = response.getReturnValue();
              var msg = '';
              if(data.existing && data.existing.length > 0){
                msg = 'Some catalogues already exists, rest are created successfully';
                this.showToast('Success','success',msg);
              }else{
                msg = 'Successfully added Line items to product catalogue';
              }
              this.showToast('Success','success',msg);
            }
            else if (state === "INCOMPLETE") {
              this.showToast('Error','error','Request is incomplete');
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                      this.showToast('Error','error',("Error message: " + errors[0].message));
                    }
                } else {
                    this.showToast('Error','error',"Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    showToast : function(title,type,msg) {
      var toastEvent = $A.get("e.force:showToast");
      toastEvent.setParams({
          "title": title,
          "type" :type,
          "message": msg
      });
      toastEvent.fire();
  }
})