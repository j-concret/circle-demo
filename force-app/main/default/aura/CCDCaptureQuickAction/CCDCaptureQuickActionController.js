({
    
    navigateToCCDCaptureCmp : function(component, event, helper) {
        
        var navService = component.find("navService");
        var pageReference = {
            "type": "standard__component",
            "attributes": {
                "componentName": "c__CCDCapture"   
            },  
            "state": {
                "c__customsClearanceDocumentId": component.get("v.recordId")
            }
        };
        var defaultUrl = "#";
        navService.generateUrl(pageReference)
        .then($A.getCallback(function(url) {
            component.set("v.url", url ? url : defaultUrl);
            window.open(url,'_blank');
            $A.get("e.force:closeQuickAction").fire();
        }), $A.getCallback(function(error) {
            component.set("v.url", defaultUrl);
            $A.get("e.force:closeQuickAction").fire();
        }));
        
    } 
})