/**
 * Created by Chibuye Kunda on 2018/12/21.
 */
({


    /** this is our initialisation function
     * @param componentP this is the component we are updating
     * @param eventP this is the event that occured
     * @param helperP this is our helper object
     */
    doInit : function( componentP, eventP, helperP ){

        helperP.populatePickList( componentP, "picklist_value" );

    },//end of function definition





    /** this function will be called when value is changed
     *  @param componentP is the component we are updating
     *  @param eventP is the event that occured
     *  @param helperP is our helper object
     */
    valueChanged : function( componentP, eventP, helperP ){

        componentP.set( "v.selected_value", eventP.getSource().get( "v.value" ) );          //selected value

    }//end of function definition



});