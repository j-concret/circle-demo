/**
 * Created by Chibuye Kunda on 2018/12/24.
 */
({

    /** this function will populate our picklist
     * @param componentP this is the component we are updating
     * @param field_nameP this is the field name on object
     * @param element_idP this is the element ID
     */
    populatePickList : function( componentP, element_idP ){

        var action = componentP.get( "c.getPickListValues" );             //get the action

        //set the action parameter
        action.setParams({
            "objectP":componentP.get( "v.object" ),
            "field_nameP":componentP.get( "v.field_name" )
        });

        action.setCallback( this, function( response ){

            //check if we have success response
            if( response.getState() === "SUCCESS" )
                componentP.set( "v.picklist_values_list", response.getReturnValue() );

        });//end of callback

        $A.enqueueAction( action );

    }//end of function definition

})