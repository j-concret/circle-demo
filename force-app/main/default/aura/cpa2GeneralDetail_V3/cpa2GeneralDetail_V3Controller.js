({
    doInit: function(component, event, helper) {  
        helper.doInitHlpr(component,helper);
    }, 
    onSuccess : function(component, event, helper) {
        var toastEvent = $A.get("e.force:showToast");
          toastEvent.setParams({
            "title": "Success!",
            "type": "success",
            "message": "The record has been saved successfully."
        });
        toastEvent.fire();
        $A.get('e.force:refreshView').fire();
    },
    onSubmit : function(component, event, helper) {
        
      
		
    },
    onLoad : function(component, event, helper) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Loaded!",
            "message": ""
        });
        toastEvent.fire();
    },
    onError : function(component, event, helper) {
              
       var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Error!",
            "message": "Error."
        });
        toastEvent.fire();
    },

})