({
	doInitHlpr : function(component, helper) {
        
        var params = {
            recordTypeId: component.get('v.recordId')
        };
		this.makeServerCall(component, 'getCPARecordTypeName', params, function(result) {
            if(result !== undefined && result !== null) {
                component.set('v.recordTypeName', result);
            }
            component.set('v.bool', true);
        });
	}
})