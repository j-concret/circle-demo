({  
    //onload
    
    init : function(cmp, event, helper) {
      helper.fetchFreightStatus(cmp, event, helper); 
       
    },
    
    handleClose: function(component, event, helper) {
      // for Display Model,set the "isOpen" attribute to "true"
      component.set("v.isOpenFrightWizard", false);
      var appEvent = $A.get('e.c:closeModal');
        appEvent.fire();  
   },
   
  // Create CI
  openCreateRatesModal: function(component, event, helper) {
      // for Display Model,set the "isOpen" attribute to "true"
      
      component.set("v.isOpenRates", true);
      component.set("v.isOpenFrightWizard", false);
      
   },
  
  // Create CI
  openCreateAWBModal: function(component, event, helper) {
      // for Display Model,set the "isOpen" attribute to "true"
      component.set("v.isOpenCreateAWB", true);
      component.set("v.isOpenFrightWizard", false);
   },
    openPickUpModal: function(component, event, helper) {
      // for Display Model,set the "isOpen" attribute to "true"
      component.set("v.isOpenPickUp", true);
      component.set("v.isOpenFrightWizard", false);
   },  
    
})