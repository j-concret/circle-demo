({
	fetchFreightStatus : function(component, event, helper) {
        component.set('v.showSpinner',true);
        
        var action = component.get('c.getFreightData');
        action.setParams({
            recId : component.get('v.recordId'),
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            component.set('v.showSpinner',false);
            if(state == 'SUCCESS') {
                var result = response.getReturnValue();
                
                if(result.status == 'OK'){ 
                    
                    
                   console.log('freightStatus ', result.freightStatus);
                    
                    
                    
                    if(result.freightStatus){
                        
                        if(result.freightStatus === 'unAuthorized'){
                            this.showToast('Error','error','You do not have access to this component');
                            return false;
                        }
                        
                        if(component.get('v.shipmentStatus') === 'Cost Estimate'){
                            component.set('v.disableRates', false);
                            return;
                        }
                        if(result.freightStatus === 'Quote pending' || result.freightStatus === 'Submitted'){
                             component.set('v.disableRates', false);
                             component.set('v.disableCreateAWB', false);
                            return;
                        }
                        if(result.freightStatus === 'Live'){
                            component.set('v.disablePickUp', false);
                            return;
                        }
                        
                    }
                    
                }else if(state === "ERROR") {
                    var errors = response.getError();
                    if(errors) {
                        if(errors[0] && errors[0].message) {
                            this.showToast('Error','error',errors[0].message);
                        }
                    } else {
                        this.showToast('Error','error','Unknown Error');
                    }
                    
                }
            }
            
        });
        $A.enqueueAction(action);
    },
     showToast : function(title,type,msg) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title:title,
            type:type,
            message:msg
        });
        toastEvent.fire();
    }
})