({
    getBankTransaction : function(component, event, helper) {
        component.set('v.isLoading',true);
        component.set('v.selectedBTRecords',0);
        component.set('v.lastSelectedIndex',undefined);
        component.set('v.filter','');
        helper.getBankTransactionList(component);
    },
    getBankTransactionProcess : function(component, event, helper) {
        component.set('v.isLoading',true);
        
        helper.getBankTransactionProcessList(component);
    },
    Close:function(component, event, helper){
        helper.closeOnClick(component);
    },
    bTChange:function(component, event, helper){
        var selectedBTRecords = component.get('v.selectedBTRecords');
        var selectedBTRecordsChange = component.get('v.selectedBTRecords');
        var filteredbankTransactionList = component.get('v.filteredbankTransactionList');
        var indexvar = event.getSource().get("v.name");
        var lastSelectedIndex = component.get('v.lastSelectedIndex');
        console.log("indexvar:::" + indexvar);
        if(component.get('v.isKeyPressed') && filteredbankTransactionList[indexvar].isBTSelected && typeof lastSelectedIndex != 'undefined'){
            var start = 0;
            var end = 0;
            if(lastSelectedIndex <  (indexvar + 1)){
                start = lastSelectedIndex;
                end = indexvar;
            }else{
                start = indexvar;
                end = lastSelectedIndex;
            }
            if(((end-start)+selectedBTRecords) <= 100){
                selectedBTRecords--;
                for(let row = start ; row <= end ; row++){
                    (!filteredbankTransactionList[row].isBTSelected || indexvar == row || lastSelectedIndex == row) ? selectedBTRecords++ : '';    
                    filteredbankTransactionList[row].isBTSelected = true;
                }
                component.set('v.lastSelectedIndex',indexvar);
            }else{
                selectedBTRecords = selectedBTRecordsChange;
                filteredbankTransactionList[indexvar].isBTSelected =false;
                helper.showToast(component, event);
            }
            
            
        }else{
            
            if(filteredbankTransactionList[indexvar].isBTSelected){
                if(selectedBTRecords < 100){
                    
                    selectedBTRecords++;
                    
                    component.set('v.lastSelectedIndex',indexvar);
                }else{
                    filteredbankTransactionList[indexvar].isBTSelected = false;
                    helper.showToast(component, event);
                }
            }else{
                selectedBTRecords--;
            }
            
        }
        component.set('v.filteredbankTransactionList',filteredbankTransactionList);
        component.set('v.selectedBTRecords',selectedBTRecords);
    },
    handleSelection: function(component, event, helper) {
        component.set('v.isKeyPressed',event.shiftKey);
        console.log(component.get('v.isKeyPressed'));
    },
    searchTypeChange: function(component, event, helper) {
        var that = this;
        var searchType = component.get('v.searchType');
        if(searchType == 'Name'){
            component.set('v.isSearchName',true);
            
        }else if(searchType == 'Date'){
            component.set('v.isSearchName',false);
        }
	component.set('v.filter','');
        $A.enqueueAction(component.get('c.onFilterBT'));
	//that.onFilterBT: function(component, event, helper);        
    },
    onFilterBT: function(component, event, helper) {
        console.log('Filter '+component.get('v.filter'));
        var filter = component.get('v.filter');
        var bTInvoiceList = component.get('v.bankTransactionList');
         var searchType = component.get('v.searchType');
        var filteredData = [];
        if(filter){
            if(searchType == 'Name'){
                filteredData = bTInvoiceList.filter(data => {
                  return data.Name ? data.Name.toLowerCase().includes(filter.toLowerCase()) : false;
              });
                component.set('v.filteredbankTransactionList',filteredData);
            }else if(searchType == 'Date'){
                 filteredData = bTInvoiceList.filter(data => {
                  return data.Date__c ? data.Date__c.toLowerCase().includes(filter.toLowerCase()) : false;
              });
                component.set('v.filteredbankTransactionList',filteredData);
            }
        }else{
            component.set('v.filteredbankTransactionList',bTInvoiceList);
        }
    }
})