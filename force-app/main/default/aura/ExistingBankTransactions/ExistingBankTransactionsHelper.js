({
    getBankTransactionList : function(component) {
        var action = component.get("c.getBankTransactions");
        action.setParams({
            'bankAccountId':component.get('v.selectedLookUpRecord').Id
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS") {
                var result = response.getReturnValue();
                if(result.status === "OK") {
                    console.log(result);
                    component.set('v.bankTransactionList',result.bankTransactionsList);
                    component.set('v.filteredbankTransactionList',result.bankTransactionsList)
                    component.set('v.isBTApplied',true);
                    component.set('v.isLoading',false);
                    
                } else {
                    console.log(result.msg);
                    component.set('v.isLoading',false);
                }
            }
            else if(state === "ERROR") {
                var errors = response.getError();
                if(errors) {
                    if(errors[0] && errors[0].message) {
                        console.log(errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                    component.set('v.isLoading',false);
                }
                
            }
        });
        $A.enqueueAction(action);
    },
    getBankTransactionProcessList : function(component){
        var that = this;
        var bankTransactionList = component.get('v.bankTransactionList');
        var bankTransactionIds = [];
        var supplierIds = [];
        for(let bankTransaction of bankTransactionList){
            if(bankTransaction.isBTSelected){
                bankTransactionIds.push(bankTransaction.Id);
                supplierIds.push(bankTransaction.Supplier__c);
            }
        }
        var action = component.get("c.getBTAndInvoiceDetails");
        action.setParams({
            'bankTransactionIds':bankTransactionIds,
            'supplierIds':supplierIds
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS") {
                var result = response.getReturnValue();
                if(result.status === "OK") {
                    console.log(result);
                    component.set('v.isBTAEnabled',true);
                    var coof = that.sortTrackingNo(result.bTInvoiceList);
                    console.log(coof);
                    component.set('v.bTInvoiceList',coof);
                    component.set('v.isLoading',false);
                    /*component.set('v.bankTransactionList',result.bankTransactionsList);
                    component.set('v.isBTApplied',true);*/
                } else {
                    console.log(result.msg);
                    component.set('v.isLoading',false);
                }
            }
            else if(state === "ERROR") {
                var errors = response.getError();
                if(errors) {
                    if(errors[0] && errors[0].message) {
                        console.log(errors[0].message);
                        component.set('v.isLoading',false);
                    }
                } else {
                    console.log("Unknown error");
                    component.set('v.isLoading',false);
                }
                
            }
        });
        $A.enqueueAction(action);
        
    },
    closeOnClick: function(component) {
        component.set('v.isExistingComponentClosed',true);
        var outerDiv = document.getElementById('divOuter');
        outerDiv.classList.add("backgroundImage");
        outerDiv.classList.remove("relPos");
        component.destroy();
    },
    showToast : function(component, event) {
    var toastEvent = $A.get("e.force:showToast");
    toastEvent.setParams({
        "title": "Info!",
        "message": "Max Bank Transactions selection count is 100",
        "type":"info"
    });
    toastEvent.fire();
    },
    sortTrackingNo:function (array, order) {
    return array.sort(order === 'DESC'
        ? function (b, a) {
            a = (a.name);
            b = (b.name);
            return isNaN(b) - isNaN(a) || a > b || -(a < b);
        }
        : function (a, b) {
            a = (a.name);
            b = (b.name);
            return isNaN(a) - isNaN(b) || a > b || -(a < b);
        });
    }
})