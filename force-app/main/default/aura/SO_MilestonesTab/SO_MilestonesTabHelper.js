({

    activeSections: function(component, event){
        var openSections = component.find("Milestones").get("v.activeSectionName");
        if(openSections.length>0){
            for(var i =0; i<openSections.length; i++){
                if(openSections[i]==="shipmentProgressTracking") {
                                                 var editFormShipmentProgressTracking = component.find("editFormShipmentProgressTracking");
                                                 $A.util.toggleClass(editFormShipmentProgressTracking, "slds-hide");
                                                 var viewFormShipmentProgressTracking = component.find("viewFormShipmentProgressTracking");
                                                 $A.util.toggleClass(viewFormShipmentProgressTracking, "slds-hide");
            } else  {
                                      var editFormIncentiveTracking = component.find("editFormIncentiveTracking");
                                      $A.util.toggleClass(editFormIncentiveTracking, "slds-hide");
                                      var viewFormIncentiveTracking = component.find("viewFormIncentiveTracking");
                                      $A.util.toggleClass(viewFormIncentiveTracking, "slds-hide");
}

            }}
}  
} )