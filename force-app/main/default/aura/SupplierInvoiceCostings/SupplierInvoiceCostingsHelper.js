({
    getCreatedInvoice: function(component) {
        var that = this;
        var action = component.get("c.getInvoiceDetail");
        action.setParams({
            'invoiceId':component.get('v.recordId')
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS") {
                var result = response.getReturnValue(); if(result.status === "OK") {
                    console.log('updated Invoices Success');
                    component.set('v.createdInvoiceObject',result.invoice);
                    component.set('v.isCommunityOpen',result.isCommunityOpen);
                    if((!result.isCommunityOpen) && (result.invoice.Invoice_Status__c == 'New' || result.invoice.Invoice_Status__c == 'Queried' || result.invoice.Invoice_Status__c == 'FC Approved') ){
                        component.set('v.isInvoiceNonEditable',false);
                    }else if((result.isCommunityOpen) && (result.invoice.Invoice_Status__c == 'New' || result.invoice.Invoice_Status__c == 'Queried')){
                        component.set('v.isInvoiceNonEditable',false);
                    }else{
                        component.set('v.isInvoiceNonEditable',true);
                        that.fireToast('INFO', 'info', "You can't update costings of this invoice");
                    }
                    that.getCostings(component);
                       component.set('v.isLoading',false);
                } else {
                    console.log(result.msg);
                    component.set('v.isLoading',false);
                }
            }
            else if(state === "ERROR") {
                var errors = response.getError();
                if(errors) {
                    if(errors[0] && errors[0].message) {
                        console.log(errors[0].message);
                        component.set('v.isLoading',false);
                        //that.fireToast('ERROR', 'error', errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                    //that.fireToast('ERROR', 'error', "Unknown error");
                }
            }
        });
          $A.enqueueAction(action);
    },
    onClosing: function(component) {
        var that = this;
        var action = component.get("c.autoPayInvoices");
        action.setParams({
            'supplierId':component.get('v.createdInvoiceObject').Account__c,
            'shipmentOrderId':component.get('v.createdInvoiceObject').Shipment_Order__c
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS") {
                var result = response.getReturnValue(); if(result.status === "OK") {
                    console.log('updated Invoices Success');
                       var navService = component.find("navService");
        // Uses the pageReference definition in the init handler
        var pageReference = {
            "type": "standard__recordPage",
            "attributes": {
                "recordId": component.get('v.createdInvoiceObject').Id,
                "objectApiName": "Invoice_New__c",
                "actionName": "view"
            }
        };
        navService.navigate(pageReference);
        //$A.get('e.force:refreshView').fire();
                    component.set('v.isLoading',false);
                } else {
                    console.log(result.msg);
                    component.set('v.isLoading',false);
                }
            }
            else if(state === "ERROR") {
                var errors = response.getError();
                if(errors) {
                    if(errors[0] && errors[0].message) {
                        console.log(errors[0].message);
                        component.set('v.isLoading',false);
                        //that.fireToast('ERROR', 'error', errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                    //that.fireToast('ERROR', 'error', "Unknown error");
                }
            }
        });
          $A.enqueueAction(action);
      },
    updateCosting: function(component) {
        var that = this;
        var costings = component.get('v.updatedCostingList');
        var costingsToUpdate = [];
        for(let costing of costings){
            costingsToUpdate.push({
                Id : costing.Id,
                Invoice_amount_local_currency__c : costing.Invoice_amount_local_currency__c,
                Comment__c : costing.Comment__c,
                Within_Threshold__c : costing.Within_Threshold__c
            });
        }
        var action = component.get("c.updateCostingLists");
        action.setParams({
            'PCurr':component.get('v.createdInvoiceObject').Invoice_Currency__c,
            'SupplierInvoiceId':component.get('v.createdInvoiceObject').Id,
            'updateCostings': costingsToUpdate
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS") {
                var result = response.getReturnValue(); 
                if(result.status === "OK") {
                    console.log('updated Costings');
                    let sumOfCost = 0;
                    for(let cost of costingsToUpdate){
                        if(cost.Invoice_amount_local_currency__c){
                        sumOfCost += parseFloat(cost.Invoice_amount_local_currency__c) ;
                        }
                    }
                     let nfObject = new Intl.NumberFormat('en-US'); 
            		let output = nfObject.format(sumOfCost.toFixed(2)); 
                    component.set('v.sumOfCostings', output);
                    that.fireToast('SUCCESS', 'success', 'Costings Updated Successfully !');
                    component.set('v.isLoading',false);
                } else {
                    console.log(result.msg);
                    that.fireToast('ERROR', 'error', result.msg);
                    component.set('v.isLoading',false);
                }
            }
            else if(state === "ERROR") {
                var errors = response.getError();
                if(errors) {
                    if(errors[0] && errors[0].message) {
                        console.log(errors[0].message);
                        component.set('v.isLoading',false);
                        that.fireToast('ERROR', 'error', errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                    that.fireToast('ERROR', 'error', "Unknown error");
                }
            }
        });
          $A.enqueueAction(action);
      },
    getCostings: function(component) {
        var that = this;
        var action = component.get("c.getCostingsForInvoices");
        action.setParams({
            'PCurr':component.get('v.createdInvoiceObject').Invoice_Currency__c,
            'SupplierInvoiceId':component.get('v.createdInvoiceObject').Id
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS") {
                debugger;
                console.log(result);
                var result = response.getReturnValue(); if(result.status === "OK") {
                    /*var json = {
                        label:"--None--",
                        value:""
                    };
                    (result.statuses).unshift(json);*/
                    component.set('v.updatedCostingList', result.costingLists);
                    let sumOfCost = 0;
                    for(let cost of result.costingLists){
                        if(cost.Invoice_amount_local_currency__c){
                        sumOfCost += parseFloat(cost.Invoice_amount_local_currency__c) ;
                        }
                    }
                    let nfObject = new Intl.NumberFormat('en-US'); 
            		let output = nfObject.format(sumOfCost.toFixed(2)); 
                    component.set('v.sumOfCostings', output)
                    component.set('v.exchangeCurrency',result.currencyFields);
                    component.set('v.isLoading',false);
                    console.log(result.currencyFields);
                    console.log(result.costingLists);
                } else {
                    console.log(result.msg);
                    //that.fireToast('ERROR', 'error', result.msg);
                    component.set('v.isLoading',false);
                }
            }
            else if(state === "ERROR") {
                var errors = response.getError();
                if(errors) {
                    if(errors[0] && errors[0].message) {
                        console.log(errors[0].message);
                        component.set('v.isLoading',false);
                        //that.fireToast('ERROR', 'error', errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                    component.set('v.isLoading',false);
                   // that.fireToast('ERROR', 'error', "Unknown error");
                }
            }
        });
          $A.enqueueAction(action);
      },
    fireToast: function(title, type, message) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": title,
            "type": type,
            "message": message
        });
        toastEvent.fire();
    }
})