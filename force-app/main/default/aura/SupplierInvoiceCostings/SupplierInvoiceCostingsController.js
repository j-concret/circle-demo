({
    doInit: function(component, event, helper) {
        // create a Default RowItem [Contact Instance] on first time Component Load
        // by call this helper function  
        // helper.getCurrencies(component);
        var recordId = component.get('v.recordId');
        if(!recordId){
            var myPageRef = component.get("v.pageReference");
        	recordId = myPageRef.state.c__recordId;
            component.set('v.recordId',recordId);
        }
        component.set('v.isLoading',true);
        helper.getCreatedInvoice(component);
        
        //helper.createObjectData(component, event);
    },
    handleAmountChange : function(component, event, helper) {
        var indexvar = event.getSource().get("v.label");
        console.log("indexvar:::" + indexvar);
        var costingList = component.get('v.updatedCostingList');
        var localCurrency = costingList[indexvar].Invoice_amount_local_currency__c;
        var exchangeRate = component.get('v.exchangeCurrency');
        var AmountUSDToApplied = (localCurrency * exchangeRate[0].Conversion_Rate__c ) ;
        var AmountinUSD = costingList[indexvar].Amount_in_USD__c;
        var thresholdPercentage = costingList[indexvar].Threshold_Percentage__c;
        console.log(component.get('v.exchangeCurrency'));
        console.log('localCurrency',localCurrency);
        console.log('exchangeRate',exchangeRate);
        console.log('AmountUSDToApplied',AmountUSDToApplied);
        console.log('Threshold_Percentage__c',thresholdPercentage);
        console.log('AmountinUSD',AmountinUSD);
        var calculationPercentage = ((AmountUSDToApplied - AmountinUSD)/AmountinUSD)*100;
        console.log('calculationPercentage',calculationPercentage);
        if(calculationPercentage > thresholdPercentage){
            console.log('Within_Threshold__c No');
            costingList[indexvar].Within_Threshold__c = 'No';
        }else{
            console.log('Within_Threshold__c Yes');
            costingList[indexvar].Within_Threshold__c = 'Yes';
            //component.set('v.costingInstance.Within_Threshold__c','Yes');
        }
        component.set('v.updatedCostingList',costingList);
    },
    onClose: function(component, event, helper) {
        helper.onClosing(component);
    },
    onCostingsCreation: function(component, event, helper) {
        if(component.get('v.isNewCostingsCreated')){
            component.set('v.isLoading',true);
            helper.getCostings(component);
            component.get('v.isNewCostingsCreated',false);    
        }
    },
    UpdateCostings : function(component, event, helper){
        component.set('v.isLoading',true);
        var costingList = component.get('v.updatedCostingList');
        var commentElements = [];
        if(component.find("costingComment")){
            component.find("costingComment").length > 0 ?
                commentElements=component.find("costingComment") :
            commentElements.push(component.find("costingComment")) ;
        }
        var isValid = true;
        for(var index=0; index < costingList.length ; index++){
            var costing = costingList[index];
            if(commentElements[index]){
                if(! commentElements[index].get("v.value")){
                    isValid =false;
                    commentElements[index].setCustomValidity("Please add a comment");
                    commentElements[index].reportValidity();
                }else{
                    commentElements[index].setCustomValidity("");
                    commentElements[index].reportValidity();
                }
            }
        }
        if(isValid){
            helper.updateCosting(component);
        }else{
            component.set('v.isLoading',false);
        }
        
    },
    handleUploadFinished: function (cmp, event) {
        // This will contain the List of File uploaded data and status
        var uploadedFiles = event.getParam("files");
        alert("Files uploaded : " + uploadedFiles.length);
    }
})