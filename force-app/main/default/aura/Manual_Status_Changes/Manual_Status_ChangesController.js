({
    doInit : function(component, event, helper) {
        if(! component.get('v.ismanualStatusUpdate')){
            var cpa = event.getParams().recordUi.record.fields.CPA_v2_0__c.value;
            var ShipStatus = event.getParams().recordUi.record.fields.Shipping_Status__c.value;
            var SubStatus = event.getParams().recordUi.record.fields.Sub_Status_Update__c.value;
            var ExpectedDate = event.getParams().recordUi.record.fields.Expected_Date_to_Next_Status__c.value;
            var updateText = event.getParams().recordUi.record.fields.Update_Text__c.value;
            console.log('ShipStatus @@ '+ShipStatus);
            component.set('v.oldShipStatus',ShipStatus);
            console.log('oldSubStatus @@ '+SubStatus);//
            component.set('v.oldSubStatus',SubStatus);//oldSubStatus
            console.log('oldupdateText @@ '+updateText);//
            component.set('v.oldupdateText',updateText);
            component.set('v.cpaId',cpa);
            component.set('v.flag',true);
            console.log(' oldSubStatus ',component.get('v.oldSubStatus'));
            console.log(' oldShipStatus ',component.get('v.oldShipStatus'));
            component.set('v.ExpectedDate',ExpectedDate);
            console.log('CPA_v2_0__c -->' +cpa);
            helper.validationForApprovedToShip(component);
            helper.shippingStatusChangeHandler(component, event);
            helper.getCPADescription(component, event);
            component.set('v.loading',false);
        }
    },
    shippingStatusChange : function(component, event, helper) {
        helper.shippingStatusChangeHandler(component, event);
    },
    getDescription : function(component, event, helper) {
        component.set("v.flag", false);
        let shipStatus = component.find("shippingStatusId").get("v.value");
        
        if(shipStatus == component.get("v.oldShipStatus") && component.get("v.nonSystemAdminUser")){
            component.set('v.disabled',false);
        }else if(( shipStatus == 'Cost Estimate' || shipStatus == 'Cost Estimate Abandoned' || shipStatus == 'Roll-Out' || shipStatus == 'None') && component.get("v.nonSystemAdminUser")){
            //shipStatus == 'Cost Estimate Rejected' ||
            helper.showToast('Error','Error','Only system administrators can change this status, please log a ticket if you require assistance.');
            let button = component.find('saveBtn');
            button.set('v.disabled',true);
            return;
        }
        
        
        if(shipStatus === 'Customs Approved (Pending Payment)'){
            helper.showToast('Error','Error','Approval will automatically be granted when all Tasks have been completed.')
            let button = component.find('saveBtn');
            button.set('v.disabled',true);
        }else{            
            helper.getCPADescription(component, event);    
        }
        
    },
    handleOnSubmit : function(component, event, helper) {
        console.log('-----Start/END handleOnSubmit-----');
        component.set('v.ismanualStatusUpdate',true);
        component.set('v.loading',true);
        
        event.preventDefault();
        //event.preventDefault(); // stop form submission
  
        helper.handleFormSubmit(component,event);
    },
    handleOnSuccess : function(component, event, helper) {
        console.log('-----START handleOnSuccess-----'); 
        var param = event.getParams(); //get event params
        var fields = param.response.fields; //get all field info
        var childRelationships = param.response.childRelationships; //get child relationship info
        var recordTypeInfo = param.response.recordTypeInfo; //get record type info
        var recordId = param.response.id; //get record id
        
        console.log('Param - ' + JSON.stringify(param)); 
        console.log('Fields - ' + JSON.stringify(fields)); 
        console.log('Child Relationship - ' + JSON.stringify(childRelationships)); 
        console.log('Record Type Info - ' + JSON.stringify(recordTypeInfo)); 
        console.log('Record Id - ' + JSON.stringify(recordId)); 
        console.log('CPA_v2_0__c - ' + JSON.stringify(fields.CPA_v2_0__c.value));        
        component.set('v.loading',false);
        helper.generateSOTravelHistory(component, event);
        console.log('-----END handleOnSuccess-----');
        component.set("v.Refresh", true);
        
        
    },
    close : function(component, event, helper) {
        component.set("v.manualChanges", false);
        component.set("v.showManualChange", false);
        if(!component.get("v.newSOPage")){
            $A.get("e.force:closeQuickAction").fire();    
        }
    }
})