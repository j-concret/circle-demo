({
    shippingStatusChangeHandler: function(component, event) {
        
        component.set('v.disableShipment',!component.find("changeStatusId").get("v.value"));
    },
    getCPADescription: function(component, event) {
        
        var shipStatus = component.find("shippingStatusId").get("v.value");
        if(component.get('v.changeSubStatus') == true){
        component.find("subshippingStatusId").set('v.value','NONE');
        }
        component.set('v.changeSubStatus',true);
        var cpaId = component.get('v.cpaId');
        var shippingStatusValues = ["Client Approved to ship", "In Transit to Hub", "Arrived at Hub", 
                                    "In transit to country", "Arrived in Country, Awaiting Customs Clearance", 
                                    "Cleared Customs", "Final Delivery in Progress", 
                                    "Customs Clearance Docs Received"];
        var validation = component.get('v.isValidationApproved');
        
        console.log('cpaId '+cpaId);
        console.log('shipStatus '+shipStatus);
        
        if(shipStatus === 'Cancelled - With fees/costs' || shipStatus === 'Cancelled - No fees/costs'){
            component.set('v.reasonForField', true);
            console.log('reason field true');
        }
        else{
            component.set('v.reasonForField', false);
            console.log('reason field false');
        }
        if(component.get("v.flag")){
            component.find("changeStatusId").set("v.value", false);
            component.set("v.disableShipment", true);
        }
        
        if(component.find("changeStatusId").get("v.value")){
            if(shipStatus === 'Awaiting POD' || shipStatus === 'POD Received' || (shippingStatusValues.includes(shipStatus) && validation === false)){
                let button = component.find('saveBtn');
                button.set('v.disabled',true);
                var toastEvent = $A.get('e.force:showToast');
                
                let awaitingPODErrorMsg = 'In order to update the shipping status to Awaiting POD, please use the \'Update Final Deliveries\' component and amend the Final delivery status.';
                let podReceivedErrorMsg = 'In order to update the shipping status to POD Received, please use the \'Update Final Deliveries\' component and amend the Final delivery status and upload the POD document.';
                let approvedToShipErrorMsg = 'In order to update the shipping status to ' + shipStatus + ', please complete all the Tasks.';
                
                let errorMsg = '';
                
                if(shipStatus === 'Awaiting POD'){
                    errorMsg = awaitingPODErrorMsg;
                }else if(shippingStatusValues.includes(shipStatus)){
                    if(!validation){
                        errorMsg = approvedToShipErrorMsg;
                    }
                }else{
                    errorMsg = podReceivedErrorMsg;
                }            
                toastEvent.setParams({
                    'type':'error',
                    'title': 'Validation Error' ,
                    'message': errorMsg
                });
                toastEvent.fire();
                return false;
            }else{
                let button = component.find('saveBtn');
                button.set('v.disabled',false);
            }
        }
        
        
        var action1 = component.get("c.getDescriptionFromCPA");
        action1.setParams({
            CPAId: cpaId,
            status: shipStatus,
        });
        
        action1.setCallback(this, function (response) {
            var state1 = response.getState();
            if (state1 === "SUCCESS") {
                var result = response.getReturnValue();
                if(result.SoDesc){
                    component.set('v.descValue',result.SoDesc.Description_ETA__c);
                    
                }
                if(result.nonSystemAdminUser){
                    component.set('v.nonSystemAdminUser', result.nonSystemAdminUser);
                }
				if(!result.SoDesc){
                       if(shipStatus === 'Cancelled - With fees/costs'){
                        component.set('v.descValue','This shipment was cancelled and there have been costs incurred.'); 
                    }else if(shipStatus === 'Cancelled - No fees/costs'){
                        component.set('v.descValue','This shipment was cancelled and there have been no costs incurred.'); 
                    }else{
                        component.set('v.descValue','Test Description'); 
                        }   
                    }
                console.log('result status');
                console.log(result.status);
                
            }
        });
        var descFlagValue = component.get('v.descFlag');
        if(descFlagValue){
        $A.enqueueAction(action1);
        }
        component.set('v.descFlag',true);
    },
    generateSOTravelHistory : function(component, event) {
        console.log('-----Start generateSOTravelHistory-----');
        component.set('v.loading',true);
        var location = (component.find("locationId").get("v.value") != null ) ? component.find("locationId").get("v.value") : '' ;
        var mappedStatus = component.find("shippingStatusId").get("v.value");
        var description = (component.find("updateTextId").get("v.value") != null) ? component.find("updateTextId").get("v.value") : '';
        var subStatus = component.find("subshippingStatusId").get("v.value");
        var updateText = component.find("updateTextId").get("v.value");
       // subshippingStatusId
        
        
        // var typeOfUpdate = component.find("typeOfUpdateId").get("v.value");
        console.log('location '+location);
        console.log('newShipStatus '+mappedStatus);
        console.log('oldSHipStatus '+component.get('v.oldShipStatus'));
        console.log('oldSubStatus '+component.get('v.oldSubStatus'));
        console.log('oldupdateText '+component.get('v.oldupdateText'));
        console.log('description '+description);
        var isStatusChanged = false;
        var isSubStatusChanged = false;
        var isUpdateText = false;
        if(component.get('v.oldShipStatus') != mappedStatus){
            isStatusChanged = true;
        }
        if(component.get('v.oldSubStatus') != subStatus){
            isSubStatusChanged = true;
        }
        if(component.get('v.oldupdateText') != updateText && isStatusChanged == false && isSubStatusChanged == false){
            isUpdateText = true;
            console.log('Text Updated');
        }
        // console.log('typeOfUpdate '+typeOfUpdate);
        var action1 = component.get("c.createSOTravelObject");
        action1.setParams({
            source: 'Manual Status Change',
            location: location,
            description: description,
            StatusChanged:isStatusChanged,
            SubStatusChanged:isSubStatusChanged,
            textUpdate:isUpdateText,
            SOID: component.get('v.recordId')
        });
        
        action1.setCallback(this, function (response) {
            var state1 = response.getState();
            if (state1 === "SUCCESS") {
                var result = response.getReturnValue();
                
                console.log('result status');
                console.log(result.status);
                component.set('v.loading',false);
                if(!component.get("v.newSOPage")){
                    $A.get("e.force:closeQuickAction").fire();}
                component.set("v.showManualChange", false);
                var toastEvent = $A.get('e.force:showToast');
                
                toastEvent.setParams({
                    'type':'success',
                    'title': 'Success' ,
                    'message': 'Record Updated Successfully'
                });
                toastEvent.fire();
                if(component.get("v.newSOPage")){
                    var appEvent = $A.get("e.c:refreshEvent");
                    appEvent.fire();   
                }
                
            }else{
                component.set('v.loading',false);
            }
        });
        $A.enqueueAction(action1);
        console.log('-----END generateSOTravelHistory-----');
    },
    
    validationForApprovedToShip : function(component){
        
        var action2 = component.get("c.validateApprovedToShip");
        action2.setParams({
            SOID: component.get('v.recordId')
        });
        
        action2.setCallback(this, function (response) {
            var state2 = response.getState();
            if (state2 === "SUCCESS"){
                var result = response.getReturnValue();
                component.set('v.isValidationApproved', result);
                console.log('valueee--->'+result);                
            }
        });
        
        $A.enqueueAction(action2);
        
    },
    handleFormSubmit: function(component,event) {
        var showValidationError = false;
        var toastEvent = $A.get('e.force:showToast');
        var vaildationFailReason = ' "Awaiting Final Delivery Confirmation" can only be selected as a sub-status if the Mapped Shipping Status is Cleared Customs. Please change the Mapped Shipping Status to Cleared Customs to use this sub-status update.';
        let shipStatus = component.find("shippingStatusId").get("v.value");
        let subshippingStatus = component.find("subshippingStatusId").get("v.value");
        console.log("subshippingStatusId");
        console.log(subshippingStatus);
        var eventFields = event.getParam("fields");
        console.log("before eventFields");
        console.log(JSON.stringify(eventFields));
        if(subshippingStatus == null || subshippingStatus==''){
        eventFields["Sub_Status_Update__c"] = "NONE";
        }
        console.log("after eventFields");
        console.log(JSON.stringify(eventFields));
  //component.find('myform').submit(eventFields);
  
        if(shipStatus !== 'Cleared Customs' && subshippingStatus === 'Awaiting Final Delivery Confirmation'){
            showValidationError = true;
        }
        
         if(shipStatus !== 'Arrived in Country, Awaiting Customs Clearance' && subshippingStatus === 'Customs Clearance has started'){
            vaildationFailReason = ' "Customs Clearance has started" can only be selected as a sub-status if the Mapped Shipping Status is Arrived in Country. Please change the Mapped Shipping Status to Arrived in Country to use this sub-status update.';
             showValidationError = true;
        }
         
        if (!showValidationError) {
            component.find("ManualStatusUpdateForm").submit(eventFields);  
        } else {
            
            toastEvent.setParams({
                    'type':'error',
                    'title': 'Validation Error' ,
                    'message': vaildationFailReason
                });
                toastEvent.fire();
          
            component.set('v.loading', false); 
        }
    }
})