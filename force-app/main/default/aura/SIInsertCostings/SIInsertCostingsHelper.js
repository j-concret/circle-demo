({
    addAccountRecord: function(component, event) {
        //get the account List from component  
        var accountList = component.get("v.accountList");
        //Add New Account Record
        accountList.push({
            'sobjectType': 'CPA_Costing__c',
            'Name': '',
            'Cost_Category__c': '',
            'Invoice_amount_local_currency__c': ''
        });
        component.set("v.accountList", accountList);
    },
     
    validateAccountList: function(component, event) {
        //Validate all account records
        var isValid = true;
        var accountList = component.get("v.accountList");
        for (var i = 0; i < accountList.length; i++) {
            if (accountList[i].Name == '') {
                isValid = false;
                alert('Please enter a Costing Name ' + (i + 1));
            }
        }
        return isValid;
    },
     
    saveAccountList: function(component, event, helper) {
         component.set("v.is_loading", true);
        //Call Apex class and pass account list parameters
        var action = component.get("c.saveCostings");
        var accountList = component.get("v.accountList");
        var SupplierInvoiceID1 = component.get("v.soidFromFlow");
        
        action.setParams({
            "accList": component.get("v.accountList"),
            SupplierInvoiceID1 : component.get("v.soidFromFlow1"),
            SeleSOID : component.get("v.SelShipmentID")
        });
        action.setCallback(this, function(response) {
             component.set("v.is_loading", false);
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.accountList", []);
                alert('Costings records saved successfully');
                location.reload();
            }
            else{
                
                 alert('Costings are not created, Please send detailed email to support_SF@tecex.com');
                
            }
            
        }); 
        $A.enqueueAction(action);
    },
})