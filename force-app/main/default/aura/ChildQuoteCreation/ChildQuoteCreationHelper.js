({
    component :null,
    partslist:null,
    
    sethelper : function(componentP, eventP){this.component =componentP;  },
    //*******  shipment Package  Start****//
	 createTableRow : function( componentP, eventP ){

        var records = componentP.get( "v.shipment_order_package_list" );           //get the record list

        //add to records list
        records.push({
            "sobjectType":"Shipment_Order_Package__c",
            "packages_of_same_weight_dims__c":0,
            "Weight_Unit__c":"KGs",
            "Actual_Weight__c":0,
            "Dimension_Unit__c":"CMs",
            "Length__c":0,
            "Breadth__":0,
            "Height__":0

        });

        componentP.set( "v.shipment_order_package_list", records );
        console.log( "Records: " + records );
        console.log( " Size of the shipment_order_package_list " + records.length );
        componentP.set('v.ShipmentOrderPackageSize',records.length);

       // this.createObjectString( records );

    },//end of function definition





    /** this function will convert object list into a string object
     * @param object_listP this is list o objects we are converting to a string
     */
    createObjectString : function( object_listP ){

        var list_string = "";

        if( object_listP === undefined )
            return;                 //terminate function

        console.log( "List Count: " + object_listP.length );
       

        //loop through the object list
        for( var i=0; i < object_listP.length; ++i ){

            //add to list string
            list_string = list_string + object_listP[i].packages_of_same_weight_dims__c + "|" + object_listP[i].Weight_Unit__c + "|" +
                                        object_listP[i].Actual_Weight__c + "|" + object_listP[i].Dimension_Unit__c + "|" +
                                        object_listP[i].Length__c + "|" + object_listP[i].Breadth__c + "|" +
                                        object_listP[i].Height__c + ":";

        }//end of if block      

        console.log( "List String: " + list_string );

        return list_string;                 //return the list string

    },//end of function definition
    //*******  shipment Package End****//
  
  //*******  fetch Pickupaddress start****//
    fetchData: function (cmp,event,helper) {
        var CAccID = cmp.get("v.account_id"); // AccID
        var CShip_from = cmp.get("v.ship_from"); //Shipfrom
        
        var action = cmp.get("c.getpickupaddress");
       action.setParams({  
           PAccID : cmp.get("v.account_id"),
           PshipFrom : cmp.get("v.ship_from")
            });
         
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var data = response.getReturnValue();
                cmp.set('v.Pickadddata',data);
            console.log(data);
                  
            }
            // error handling when state is "INCOMPLETE" or "ERROR"
        });
        $A.enqueueAction(action);
    },
      //*******  Pickup addess create object ****//
	 createPickupAddObjectData : function( cmp, event ){
        
       var selected = event.getSource().get("v.text");
	   console.log(selected);
       cmp.set('v.selectedPickupadddressID',selected);
    
    },//end of function definition
    //*******  fetch Pickupaddress end****//
   
    //*******  fetch Finaldeladdress start****//
    fetchFinalDelData: function (cmp,event,helper) {
        
         console.log('in fetchFinalDelData');
        var CAccID = cmp.get("v.account_id"); // AccID
        var Cdestination = cmp.get("v.destination"); //Shipfrom
        
        var action = cmp.get("c.getFinDeladdress");
       action.setParams({  
           PAccID : cmp.get("v.account_id"),
           Pdestination : cmp.get("v.destination")
            });
         
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var data = response.getReturnValue();
             var data1 =  cmp.set('v.FinAdddata',data);
            console.log(data);
                
                  
            }
            // error handling when state is "INCOMPLETE" or "ERROR"
        });
        $A.enqueueAction(action);
    },
      //*******  Final del addess create object ****//
	 createFinAddObjectData : function( cmp, event ){
      	var Findelrecordlist = cmp.get("v.FinAdddata");
         var Findelrecordlistsize =Findelrecordlist.length ;
         console.log("Findelrecordlistsize--> ", Findelrecordlistsize);
         
         
     if(Findelrecordlistsize >1 ){ 
 		var itemsToPass = cmp.get("v.selectedFinaldeladddressIDs");
        var selectedAccounts = []; 
        
        
    var selected = cmp.find("accountSelected").filter(comp=>comp.get("v.value"));
    var selectedAccIds = selected.map(comp=>comp.get("v.name"));
    console.log("selected => ", selected, selected.length, selectedAccIds);
         for (var i=0; i< selected.length; i++)
         {            
      				selectedAccounts.push(selectedAccIds);
         }
    
        cmp.set("v.NumofFindelselected", selected.length);
        
      	console.log("selectedAccounts ", selectedAccounts);
     	
     	
            var action = cmp.get("c.UpdateseleFinaldeladIDs");
          action.setParams({changes : selectedAccounts});
          action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                 var data = response.getReturnValue();
             cmp.set("v.selectedFinaldeladddressIDs", data);
                console.log("selectedFinaldeladddressIDs ---> ", data);
                
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                    errors[0].message);
                    }
                } 
                else {
                    console.log("Unknown Error");
                }
            }
        });
        $A.enqueueAction(action);

    
         }         
         if(Findelrecordlistsize <=1) {
             
        var selected = event.getSource().get("v.value");
         var selectedID = event.getSource().get("v.name");     
	   console.log(selected);
       console.log(selectedID);
        
    
             
             
        var selectedAccounts = []; 
        		selectedAccounts.push(selectedID);
        
    
        cmp.set("v.NumofFindelselected", selectedAccounts.length);
        
      	console.log("selectedAccounts ", selectedAccounts);
     	
     	
            var action = cmp.get("c.UpdateseleFinaldeladIDs");
          action.setParams({changes : selectedAccounts});
          action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                 var data = response.getReturnValue();
             cmp.set("v.selectedFinaldeladddressIDs", data);
                console.log("selectedFinaldeladddressIDs ---> ", data);
                
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                    errors[0].message);
                    }
                } 
                else {
                    console.log("Unknown Error");
                }
            }
        });
        $A.enqueueAction(action);
        
         }
             
    },//end of function definition
    //*******  fetch Final Delivery end****//
    receiveMessage: function (event) {
      
     var myJSON = JSON.stringify(event.data.Parts);
      
       this.component.set("v.parts_list",myJSON);
       this.component.set("v.Secondhandgoods",event.data.Type_of_Goods);
       this.component.set("v.Li_ion_Batteries",event.data.Li_ion_Batteries);
       this.component.set("v.Li_ion_BatteryTypes",event.data.Li_ion_BatteryTypes);
       this.component.set("v.isPartModelOpen",false); 
       this.component.set("v.PartlistSize",event.data.Parts.length); 
        
        
        
        console.log("parts_list - Json ::", myJSON);
        console.log("parts_list ::", event.data.Parts);
        console.log("Secondhandgoods ::", event.data.Type_of_Goods);
        console.log("Li_ion_Batteries ::", event.data.Li_ion_Batteries);
        console.log("Li_ion_BatteryTypes ::", event.data.Li_ion_BatteryTypes);
      
        
        
			              
    },
    
    
    //Adding pickup new Address
    
    addAddressRecord: function(component, event) {
        //get the account List from component  
        var PickaddList = component.get("v.PickaddList");
        //Add New Account Record
        PickaddList.push({
            'sobjectType': 'Client_Address__c',
            'Contact_Full_Name__c': '',
            'Contact_email__c': '',
            'Contact_Phone_Number__c':'' ,
            'CompanyName__c': '',
            'Address__c': '',
            'Address2__c': '',
            'City__c': '',
            'Province__c': '',
            'Postal_Code__c':'',
            'All_Countries__c': ''
           
                 });
        component.set("v.PickaddList", PickaddList);
    },
    
     //Adding final del new Address
    
    addFinaldelAddressRecord: function(component, event) {
        //get the account List from component  
        var FinalDeladdList = component.get("v.FinalDeladdList");
        //Add New Account Record
        FinalDeladdList.push({
            'sobjectType': 'Client_Address__c',
            'Contact_Full_Name__c': '',
            'Contact_email__c': '',
            'Contact_Phone_Number__c':'' ,
            'CompanyName__c': '',
            'Address__c': '',
            'Address2__c': '',
            'City__c': '',
            'Province__c': '',
            'Postal_Code__c':'',
            'All_Countries__c': ''
           
                 });
        component.set("v.FinalDeladdList", FinalDeladdList);
    },
    
    
     //AutoPLACE HELPER
    getAddressDetailsByPlaceId: function(component,event,isFinal){
        
        var selectedValue = event.currentTarget.dataset.value;
        var action = component.get("c.getAddressDetailsByPlaceId");
        action.setParams({
            PlaceID:selectedValue 
        });
        action.setCallback(this, function(response){
            var res = response.getState();
            if(res == 'SUCCESS'){
                console.log(response.getReturnValue());
                var response = JSON.parse(response.getReturnValue());
                var postalCode = '', state = '', country= '', city = '', street1 = '',street2 = '', street_number = '', route = '', subLocal1 = '', subLocal2 = '' , subLocal3 = '',neighbor='' ;
                
                for(var i=0; i < response.result.address_components.length ; i++){
                    var FieldLabel = response.result.address_components[i].types[0];
                    //console.log(FieldLabel);
                    //debugger;
                    if( FieldLabel == 'neighborhood' || FieldLabel == 'sublocality_level_3' || FieldLabel == 'sublocality_level_2' || FieldLabel == 'sublocality_level_1' || FieldLabel == 'street_number' || FieldLabel == 'route' || FieldLabel == 'locality' || FieldLabel == 'country' || FieldLabel == 'postal_code' || FieldLabel == 'administrative_area_level_1'){
                        
                        switch(FieldLabel){
                                 case 'neighborhood':
                                neighbor = response.result.address_components[i].long_name;
                                break;
                                case 'sublocality_level_3':
                                subLocal3 = response.result.address_components[i].long_name;
                                break;
                            case 'sublocality_level_2':
                                subLocal2 = response.result.address_components[i].long_name;
                                break;
                            case 'sublocality_level_1':
                                subLocal1 = response.result.address_components[i].long_name;
                                break;
                            case 'street_number':
                                street_number = response.result.address_components[i].long_name;
                                break;
                            case 'route':
                                route = response.result.address_components[i].short_name;
                                break;
                            case 'postal_code':
                                postalCode = response.result.address_components[i].long_name;
                                break;
                            case 'administrative_area_level_1':
                                state = response.result.address_components[i].short_name;
                                break;
                            case 'country':
                                country = response.result.address_components[i].long_name;
                                break;
                            case 'locality':
                                city = response.result.address_components[i].long_name;
                                break;
                            default:
                                break;
                        }
                    }
                }
                
                street1 = street_number + ' ' + route + ' ' +neighbor;
                street2 = subLocal3+ ' ' +subLocal2 + ' ' + subLocal1;
                /*if(street == null || street == '' || street == ' '){
                    street = subLocal2 + ' ' + subLocal1;
                }*/
                //console.log(street);
                
               
             /*   var addIndex = component.get('v.pickIndex');
                var addressList = component.get('v.PickaddList');
                console.log('addressList ');
                console.log(addressList[addIndex]);
                addressList[addIndex].Address__c  = street;
                addressList[addIndex].City__c  = city;
                addressList[addIndex].Postal_Code__c  = postalCode;
                addressList[addIndex].Province__c  = state;
                addressList[addIndex].All_Countries__c  = country;*/
                // addressList.All_Countries__c  = country;
                // component.set('v.addressDetails.Street__c', street);
                // component.set('v.addressDetails.PostalCode__c', postalCode);
                // component.set('v.addressDetails.State__c', state);
                // component.set('v.addressDetails.Country__c', country);
                //component.set('v.addressDetails.City__c', city);
                
                if(postalCode == null || postalCode == '' || postalCode == ' '){
                    //alert('Postal code/ Zip Code/ Pin Code Not found Please do deep search');
                      var action1 = component.get("c.getZipCodeByCountry");
                    action1.setParams({
                        countryName: country
                    });
                    
                    action1.setCallback(this, function (response) {
                        var state1 = response.getState();
                        if (state1 === "SUCCESS") {
                            var result = response.getReturnValue();
                            
                            console.log('result');
                            console.log(JSON.stringify(result));
                            if(result.no_zip_code_countries && result.no_zip_code_countries.zip_code__c){
                                 postalCode = result.no_zip_code_countries.zip_code__c;
                              //component.set('v.searchKey', null)
        			component.set('v.AddressList', null);
                    component.set('v.FinalAddressList', null);
                   /* component.set("v.showModalBox", false);
                    component.set('v.PickaddList',addressList);*/
                    if(isFinal){
                        component.set("v.finalAddress1", street1);
                        component.set("v.finalAddress2", street2);
                        component.set("v.finalCity", city);
                        component.set("v.finalState", state);
                        component.set("v.finalZip", postalCode);
                        component.set("v.finalCountry", country);
                    }else{
                        component.set("v.address1", street1);
                        component.set("v.address2", street2);
                        component.set("v.city", city);
                        component.set("v.state", state);
                        component.set("v.zip", postalCode);
                        component.set("v.country", country);
                    }
                            }
                            else{
                                var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Error!",
                        "type": "error",
                        "message": "Postal code/ Zip Code/ Pin Code Not found Please do deep search"
                    });
                    toastEvent.fire();
                            }
                        }
                    });
                    $A.enqueueAction(action1);
                    
                }else{
                    //component.set('v.searchKey', null)
        			component.set('v.AddressList', null);
                    component.set('v.FinalAddressList', null);
                   /* component.set("v.showModalBox", false);
                    component.set('v.PickaddList',addressList);*/
                    if(isFinal){
                        component.set("v.finalAddress1", street1);
                component.set("v.finalAddress2", street2);
                component.set("v.finalCity", city);
                component.set("v.finalState", state);
                component.set("v.finalZip", postalCode);
                component.set("v.finalCountry", country);
                    }else{
                         component.set("v.address1", street1);
                component.set("v.address2", street2);
                component.set("v.city", city);
                component.set("v.state", state);
                component.set("v.zip", postalCode);
                component.set("v.country", country);
                    }
                   
                }
            }
        });
        $A.enqueueAction(action);
    },
    getAddressRecommendations: function(component, event,isFinal){
        var key = isFinal ? component.get("v.finalSearchKey"): component.get("v.searchKey");
        var action = component.get("c.getAddressFrom");
        action.setParams({
            "SearchText": key
        });
        
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var response = JSON.parse(response.getReturnValue());
                var predictions = response.predictions;
                var addresses = [];
                if (predictions.length > 0) {
                    for (var i = 0; i < predictions.length; i++) {
                        var bc =[];
                        addresses.push(
                            {
                                main_text: predictions[i].structured_formatting.main_text, 
                                secondary_text: predictions[i].structured_formatting.secondary_text,
                                place_id: predictions[i].place_id
                            });
                        
                    }
                }
                for(var i=0; i <addresses.length; i++){
                    console.log(addresses[i].main_text);
                    console.log(addresses[i].secondary_text);
                    console.log(addresses[i].place_id);
                }
                isFinal ? component.set("v.FinalAddressList", addresses) : component.set("v.AddressList", addresses);
                
            }
        });
        $A.enqueueAction(action);
    },
    clearAddressList : function(component) {
        console.log("Clearing the list!");
        component.set('v.searchKey', null)
        component.set('v.AddressList', null);
    }, 
  clearFinalAddressList : function(component) {
        console.log("Clearing the list!");
        component.set('v.finalSearchKey', null)
        component.set('v.FinalAddressList', null);
    }, 
})