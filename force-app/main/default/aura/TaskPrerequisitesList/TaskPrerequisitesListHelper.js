({
  getPrerequisitesTask : function(cmp) {
    var self = this;
    cmp.set('v.showSpinner',true);
    self.serverCall(cmp,'getPrerequisiteTasks',{
      taskId : cmp.get('v.recordId')
    },function(response){
      cmp.set('v.tasks',response);
    },function(err){
      cmp.set('v.showSpinner',false);
      self.showToast('error','ERROR',err);
    });
  },
  redirectToObject : function(cmp,recId) {
    var pageRef = {
      type: 'standard__recordPage',
      attributes: {
        recordId: recId,
        objectApiName: 'Task',
        actionName: 'view'
      }
    };
    cmp.find('navService').navigate(pageRef);
  },
});