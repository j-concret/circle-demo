({
  doInit : function(component, event, helper) {
    helper.getPrerequisitesTask(component);
  },
  redirectToRelated: function(cmp, event, helper) {
    var selectedItem = event.currentTarget;// this will give current element
    var recId = selectedItem.dataset.redirectid;
    helper.redirectToObject(cmp,recId);
  }
});