({
  handleAccordion: function(component, event, helper) {
    var dynamicId = component.get('v.dynamicId');
    var element = document.querySelector('#'+dynamicId);

    if(element.classList.contains("slds-is-open")){
      component.set('v.iconName','utility:down');
      element.classList.remove('slds-is-open');
    }else{
      component.set('v.iconName','utility:up');
      element.classList.add('slds-is-open');
    }
  }
})