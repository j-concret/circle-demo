({
	//Server call to get Notify Parties
    notifyAllParties: function(component, helper) {
        component.set('v.showInnerSpinner', true);
        component.set("v.notifyParties", []);
        var params = {
            SOID: component.get('v.recordId')
        };
        this.makeServerCall(component, 'GetNotifyPartiesContacts', params, function(result) {
            if(result.status === 'OK') {
                component.set('v.notifyParties', result.notifyParties);
                 
                if(result.notifyParties){
                    if(result.notifyParties.length > 8){
                        component.set("v.scroll", true);
                    }
                    
                }
                
            }
            else {
                helper.showToast('error', 'Error Occured', result.msg);
            }
            component.set('v.showInnerSpinner', false);
            
        });
    },
    //Server call to get Contacts For Notify Parties
    notifyContacts: function(component, helper) {
        component.set('v.showInnerSpinner', true);
        component.set("v.contactList", []);
        var params = {
            SOID: component.get('v.recordId')
        };
        this.makeServerCall(component, 'GetAllContactsForNotifyParties', params, function(result) {
            if(result.status === 'OK') {
                component.set('v.contactList', result.contacts);
                if(result.contacts){
                    if(result.contacts.length > 8){
                        component.set("v.scroll", true);
                    }
                    
                }
                
            }
            else {
                helper.showToast('error', 'Error Occured', result.msg);
            }
            component.set('v.showInnerSpinner', false);
        });
    },
    //Server call to update Contact
    
    updateAllContacts: function(component, helper) {
       
        
        var notifyParties = component.get("v.notifyParties");

		let notifyContacts = [];
        let notifyPartiesToDelete = [];
        for(let notifyParty of notifyParties){
            console.log(notifyParty['Contact__r'].Include_in_SO_Communication__c);
            if(notifyParty['Contact__r'].Include_in_SO_Communication__c == false){
                notifyPartiesToDelete.push(notifyParty.Id);
            }
            if(notifyParty.Contact__r != null){
             notifyContacts.push({
                     'Id':notifyParty.Contact__c,
                     'Include_in_SO_Communication__c': notifyParty['Contact__r'].Include_in_SO_Communication__c,
                     'Client_Notifications_Choice__c': notifyParty['Contact__r'].Client_Notifications_Choice__c,
                     'Client_Tasks_notification_choice__c': notifyParty['Contact__r'].Client_Tasks_notification_choice__c,
                     'Client_Push_Notifications_Choice__c': notifyParty['Contact__r'].Client_Push_Notifications_Choice__c 
             });
            }
        }
        
        console.log(notifyContacts);
        console.log(notifyPartiesToDelete);
        component.set('v.showInnerSpinner', true);
        var params = {
           
        };
        var action = component.get(
                "c.updateContacts"
            );
            action.setParams({
                 SOID: component.get('v.recordId'),
            	contacts : notifyContacts,
                NotifyPartiesToDelete:notifyPartiesToDelete
            });
            action.setCallback(this,
                               function (result) {
                                   var rtnValue =
                                       result.getReturnValue();
                                   if(rtnValue.status === 'OK'){
                                       
                                   }
                                   component.set('v.showInnerSpinner', false);
                                   component.set("v.isFieldEditable",false);
                                   
                               });
            $A.enqueueAction(action);
        
        
       
    },
    
    UpdateAndCreateNotifyParties: function(component, helper) {
        
       
         var action = component.get(
                "c.updateContactsAndNPCreate"
            );
            action.setParams({
                 SOID: component.get('v.recordId'),
            	contacts : component.get("v.contactList")
            });
            action.setCallback(this,
                               function (result) {
                                   var rtnValue =
                                       result.getReturnValue();
                                   if(rtnValue.status === 'OK'){
                                       
                                   }
                                   component.set('v.showInnerSpinner', false);
                                   component.set("v.isAddNewContacts",false);
                                   
                               });
            $A.enqueueAction(action);
        
    }
})