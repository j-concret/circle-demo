({
   onRefreshOfNP: function(component, event, helper) {
        helper.notifyAllParties(component, event);	
       helper.notifyContacts(component, event);
    },
    doInitCtrl : function(component, event, helper) {
      /*  var workspaceAPI = component.find("workspace");
    	workspaceAPI.getFocusedTabInfo().then(function(response) {
        var focusedTabId = response.tabId;
        workspaceAPI.setTabLabel({
            tabId: focusedTabId,
            label: "Client Contacts"
            
        });
             workspaceAPI.setTabIcon({
            tabId: focusedTabId,
            icon: "standard:contact",
            iconAlt: "Contact"
        });
    })*/
        var baseUrl = window.location.origin;
        component.set("v.domainUrl",baseUrl);
        var pageRef = component.get("v.pageReference");
        if(pageRef && pageRef.state.c__recordId){
            component.set("v.recordId",pageRef.state.c__recordId);
            helper.notifyAllParties(component, event);
            helper.notifyContacts(component, event);
            
        }
        console.log(component.get("v.recordId"));
    },
    onEdit:function(component, event, helper) {
        component.set("v.isFieldEditable",true);
    },
    checkboxSelect: function(cmp, event, helper) {
    console.log('checked'+event.getSource().get('v.checked'));
    console.log('value'+event.getSource().get('v.value'));
},
    checkboxSelect1: function(cmp, event, helper) {
    console.log('checked'+event.getSource().get('v.checked'));
    console.log('value'+event.getSource().get('v.value'));
},
     checkboxSelect2: function(cmp, event, helper) {
    console.log('checked'+event.getSource().get('v.checked'));
    console.log('value'+event.getSource().get('v.value'));
},
    
    onChange: function (cmp, evt, helper) {
    	console.log(evt.getSource().get('v.name'));
    	var index = evt.getSource().get('v.name');
    	console.log(evt.getSource().get("v.value"));
        
    },
    onCancel: function (cmp, evt, helper) {
    	 cmp.set("v.isFieldEditable",false);
    },
    onUpdate: function (cmp, evt, helper) {
    	console.log(cmp.get("v.notifyParties"));
        helper.updateAllContacts(cmp, evt);
        helper.notifyAllParties(cmp, evt);	
       helper.notifyContacts(cmp, evt);
    },
    onContactCancel: function (cmp, evt, helper) {
    	 cmp.set("v.isAddNewContacts",false);
    },
    onNewAdd: function (cmp, evt, helper) {
    	 cmp.set("v.isAddNewContacts",true);
    },
    onSave: function (cmp, evt, helper) {
    	console.log(cmp.get("v.contactList"));
        helper.UpdateAndCreateNotifyParties(cmp, evt);
        helper.notifyAllParties(cmp, evt);	
       helper.notifyContacts(cmp, evt);
    },

})