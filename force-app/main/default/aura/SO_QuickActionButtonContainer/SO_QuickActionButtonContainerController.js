({  
    //onload
    
    
    //View Finance
    
    openViewFinanceModal: function(component, event, helper) {
      // for Display Model,set the "isOpen" attribute to "true"
      component.set("v.isOpenViewFinance", true);
   },
 
   closeOpenViewFinanceModal: function(component, event, helper) {
      // for Hide/Close Model,set the "isOpen" attribute to "Fasle"  
      component.set("v.isOpenViewFinance", false);
   },
   
  // Create CI
  openCreateCIModal: function(component, event, helper) {
      // for Display Model,set the "isOpen" attribute to "true"
      component.set("v.isOpenCreateCI", true);
   },
 
   closeCreateCIModal: function(component, event, helper) {
      // for Hide/Close Model,set the "isOpen" attribute to "Fasle"  
      component.set("v.isOpenCreateCI", false);
   },

   // Add Line items CI
   openAddLineItemsModal: function(component, event, helper) {
    // for Display Model,set the "isOpen" attribute to "true"
    var soId = component.get("v.recordId");
    component.set("v.isOpenAddLineItems", true);
    component.set("v.iframeUrl","https://tecex.lightning.force.com/apex/PartCopyAndPaste?sobjectName=Part__c&id="+ soId);  
       
 },

 closeAddLineItemsModal: function(component, event, helper) {
    // for Hide/Close Model,set the "isOpen" attribute to "Fasle"  
    component.set("v.isOpenAddLineItems", false);
 }
    
    
})