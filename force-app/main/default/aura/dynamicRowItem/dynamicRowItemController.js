({
    doInit : function(component, event, helper){
        
    },
    removeRow : function(component, event, helper){
        // fire the DeleteRowEvt Lightning Event and pass the deleted Row Index to Event parameter/attribute
        component.getEvent("DeleteRowEvt").setParams({"indexVar" : component.get("v.rowIndex") }).fire();
    },
    keyCheck: function(component, event, helper){
        let amount = component.get('v.bankTransaction.Amount__c') ? component.get('v.bankTransaction.Amount__c') : 0;
        let bankCharge = component.get('v.bankTransaction.Bank_charges__c') ? component.get('v.bankTransaction.Bank_charges__c') : 0;
        var bankBalanceMovement = parseFloat(amount) + parseFloat(bankCharge);
        component.set('v.bankTransaction.bankBalanceMovement',bankBalanceMovement?bankBalanceMovement:0);
        //component.set('v.bankBalanceMovementList',bankBalanceMovement?bankBalanceMovement:0);
        component.set('v.bankTransactionList',component.get('v.bankTransactionList'));
        
    },
    recordTypeChange: function(component, event, helper){
        component.set('v.bankTransaction.Supplier__c', component.get('v.selectedLookUpRecord'));
        for(let recordItem of component.get('v.recordTypeList')){
        if(component.get('v.bankTransaction.Supplier__c.Type') == 'Client' && recordItem.Name == 'Receipt' && component.set('v.bankTransaction.RecordTypeId') != recordItem.Id){
           component.set('v.bankTransaction.RecordTypeId',recordItem.Id);
        }else if(component.get('v.bankTransaction.Supplier__c.Type') == 'Supplier' && recordItem.Name == 'Payment' && component.set('v.bankTransaction.RecordTypeId') != recordItem.Id){
            component.set('v.bankTransaction.RecordTypeId',recordItem.Id);
        } 
        }
       
    }
    
})