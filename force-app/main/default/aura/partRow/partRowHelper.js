({
  createNewProduct : function(cmp) {
      var self = this;
      cmp.set('v.showSpinner',true);
      var part = cmp.get('v.part');

      if(!cmp.get('v.isProductCreated')) {
        self.serverCall(cmp,'createProduct',{
          'parts': JSON.stringify(part)
        },function(product){
          cmp.set('v.showSpinner',false);
          cmp.set('v.part.isProductCreated', true);
          cmp.set('v.part.oldAlias',null);
          cmp.set('v.part.newProductId', product.Id);
          cmp.set('v.part.product', product);
          self.showToast('Success','success','New Product created successfully!');
        },function(err){
          cmp.set('v.showSpinner',false);
          self.showToast('error','ERROR',err);
        });
      }
  },
  showToast : function(title,type,message){
      var toastEvent = $A.get("e.force:showToast");
      toastEvent.setParams({
          "title": title,
          "type":type,
          "message": message
      });
      toastEvent.fire();
  }
})