({
    showGroupedParts: function(component, event, helper) {
        component.set('v.part.isOpen', !component.get('v.part.isOpen'));
    },
    createNewProduct: function(component, event, helper) {
       helper.createNewProduct(component);
  },
  partChangeHandler : function(component, event, helper) {
    component.set('v.updated',true);
    component.set('v.part.updated',true);
  },
  fireGrpUngrpEvent: function(component, event, helper) {
      event.stopPropagation();
      component.set('v.part.updated',true);
      var grpIndex = component.get('v.partIndex');
      var childpIndex = component.get('v.childPartIndex');
      var parentProduct = component.get('v.product');
      var childProduct = component.get('v.part.product');
      try {

          if((!parentProduct && typeof childpIndex !== 'undefined' && !childProduct) || (parentProduct && typeof childpIndex !== 'undefined' && !childProduct) || (parentProduct && childProduct && parentProduct.Id == childProduct.Id)) return;

          var grpUngrp = component.getEvent("grpUngrp");
          grpUngrp.setParams({
              'grpIndex': grpIndex,
              'childpIndex': component.get('v.childPartIndex')
          });
          grpUngrp.fire();
      } catch(err) {
          console.log(err.message);
      }

  }
})