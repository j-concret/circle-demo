({
    doInitHlpr: function(component, helper) {
        component.set("v.showSpinner", true);
        var params = {
            shipmentId: component.get("v.recordId")
        };
        this.makeServerCall(component, "getTaskData", params, function(result) {
            if(result.status === 'OK') {
                if(result.data.length> 0){
                    helper.filter(component, helper, result.data[0]);
                }
                if(result.ClientContactId){
                    component.set("v.clientContactId", result.ClientContactId);
                }
                
            }
            else {
                helper.showToast("error", "Error Occured Helper", result.msg);
            }
            component.set("v.showSpinner", false);
        });
    },
    
    //Server call to get SO Data
    soLog: function(component, helper) {
        component.set('v.showInnerSpinner', true);
        var params = {
            SOID: component.get('v.recordId')
        };
        this.makeServerCall(component, 'GetSoLog', params, function(result) {
            if(result.status === 'OK') {
                component.set('v.soLogs', result.soLogData);
                if(result.soLogData){
                    if(result.soLogData.length > 8){
                        component.set("v.scroll", true);
                    }
                    
                }
                
            }
            else {
                helper.showToast('error', 'Error Occured', result.msg);
            }
            component.set('v.showInnerSpinner', false);
        });
    },
    //Server call to get Notify Parties
    notifyAllParties: function(component, helper) {
        component.set('v.showInnerSpinner', true);
        var params = {
            SOID: component.get('v.recordId')
        };
        this.makeServerCall(component, 'GetNotifyPartiesContacts', params, function(result) {
            if(result.status === 'OK') {
                component.set('v.notifyParties', result.notifyParties);
                if(result.notifyParties){
                    if(result.notifyParties.length > 8){
                        component.set("v.scroll", true);
                    }
                    
                }
                
            }
            else {
                helper.showToast('error', 'Error Occured', result.msg);
            }
            component.set('v.showInnerSpinner', false);
        });
    },
    
    
    //Make Fashion to show Task/Case Cards
    filter: function(component, helper, res) {
        
        var taskAndCases = [];
        if(res.Tasks){
            for(var i=0;i<res.Tasks.length;i++){
                var task = res.Tasks[i];
                task.genre = 'T';
                if(res.Tasks[i].ClientlatestCommentUser__c){task.from = task.ClientlatestCommentUser__c;}
                if(res.Tasks[i].Master_Task__r){task.taskName = task.Master_Task__r.Name;}
                if(res.Tasks[i].State__c){task.Status = task.State__c;}
                taskAndCases.push(task);
            }
            
        }
        if(res.Cases__r){
            for(var i=0;i<res.Cases__r.length;i++){
                var unitCase = res.Cases__r[i];
                unitCase.genre = 'C';
                if(res.Cases__r[i].Owner){unitCase.from = unitCase.Owner.Name;}
                if(res.Cases__r[i].Subject){unitCase.taskName = unitCase.Subject;}
                taskAndCases.push(unitCase);
            }
        }
        
        let dateSortedData = taskAndCases.sort(function(a,b){
            return new Date(b.LastModifiedDate) - new Date(a.LastModifiedDate)
        });
        
        
        let resolvedTasks = dateSortedData.filter(function (e) {
            return (e.Status === 'Resolved' || e.Status === 'Closed' || e.Status === 'Cancelled' ||
                    e.Status === 'Duplicate' || e.Status === 'Could Not Replicate')
        }).map(function (obj) {
            return obj;
        });
        let openData = dateSortedData.filter(function (e) {
            return (e.Status != 'Resolved' && e.Status != 'Closed' && e.Status != 'Cancelled' &&
                    e.Status != 'Duplicate' && e.Status != 'Could Not Replicate')
        }).map(function (obj) {
            return obj;
        });
        
        component.set("v.resolvedTasks", resolvedTasks);
        component.set("v.openTasks", openData);
    },
    
    activeChatter:function(component,event, helper) {
        component.set('v.showChatterPublisher', true);
        var tabName = event.getSource().get("v.name");
        if(tabName == 'open'){
            var taskId = component.get("v.openTasks")[event.getSource().get("v.value")];
            if(component.get("v.openTasks")[event.getSource().get("v.value")].Reason && component.get("v.openTasks")[event.getSource().get("v.value")].Reason == 'Initiating message to client'){
                component.set('v.showChatterPublisher', false);
            }
        }else if(tabName == 'resolved'){
            var taskId = component.get("v.resolvedTasks")[event.getSource().get("v.value")];
            if(component.get("v.resolvedTasks")[event.getSource().get("v.value")].Reason && component.get("v.resolvedTasks")[event.getSource().get("v.value")].Reason == 'Initiating message to client'){
                component.set('v.showChatterPublisher', false);
            }
        }
        
        if(component.get("v.showSOChatter")){
            if(component.get("v.taskId") === taskId.Id){
                component.set("v.showSOChatter", !component.get("v.showSOChatter"));
            }
            else{
                component.set("v.taskId", taskId.Id);
                component.set("v.taskName", taskId.Subject);
            }
        }else{
            component.set("v.taskId", taskId.Id);
            component.set("v.taskName", taskId.Subject);
            component.set("v.showSOChatter", !component.get("v.showSOChatter"));
        }
    },
    activeSections: function(component){
        var openSections = component.get("v.selectedTab");
        
        if(openSections === "Cost Estimate Tracking") {
            var editFormCostField = component.find("editFormCostField");
            $A.util.toggleClass(editFormCostField, "slds-hide");
            var viewFormCostField = component.find("viewFormCostField");
            $A.util.toggleClass(viewFormCostField, "slds-hide");
        }else if(openSections === "Task Fields"){
            var editFormCostField = component.find("editFormTaskField");
            $A.util.toggleClass(editFormCostField, "slds-hide");
            var viewFormCostField = component.find("viewFormTaskField");
            $A.util.toggleClass(viewFormCostField, "slds-hide");
        }else if(openSections === "Quote ETA"){
            var editFormCostField = component.find("editFormQuoteETA");
            $A.util.toggleClass(editFormCostField, "slds-hide");
            var viewFormCostField = component.find("viewFormQuoteETA");
            $A.util.toggleClass(viewFormCostField, "slds-hide");
        }
    },
    createMessageForCase: function(component, event, helper,richContents,originaltext){
        component.set("v.loading", true);
        var params = {
            
            caseId : component.get('v.caseRecordId'),
            caseMessage : originaltext.replace( /(<([^>]+)>)/ig, ''),
            clientContactUserId : component.get("v.clientContactId"),
            richContents : richContents
        };
        this.makeServerCall(component, "inserFeeditem", params, function(result) {
            if(result.status === 'OK') {
                helper.showToast("Success", "Success", 'Case Created and Message Sent.');
                component.set("v.loading", false);
                component.set("v.showClientMessageModal", false);
                helper.doInitHlpr(component, helper);                
            }
            else {
                helper.showToast("error", "Error", result.status);
            }
            component.set("v.loading", false);
        });
    },
    insertCase: function(component, event, helper,richContents,originaltext){
        component.set("v.loading", true);
        var params = {
            SO: component.get("v.shipmentOrder"),
            caseSubject : component.find('caseSubject').get('v.value'),
            clientContactUserId : component.get("v.clientContactId")
        };
        this.makeServerCall(component, "insertCaseRecord", params, function(result) {
            if(result.status === 'Ok') {
                component.find("CaseButton").set("v.label",'Send Message');
                component.find("cancelCase").set("v.label",'Cancel');
                component.set("v.caseRecordId", result.CaseId);
                var caseSubject = component.find("caseSubjectLayout");
                $A.util.toggleClass(caseSubject, "slds-hide");
                var caseMessage = component.find("caseMessageLayout");
                $A.util.toggleClass(caseMessage, "slds-hide");
                var caseMessage1 = component.find("caseMessageLayout1");
                $A.util.toggleClass(caseMessage1, "slds-hide");
                // helper.showToast("Success", "Success", 'Case Created');
                component.set("v.loading", false);
                
                
                //component.set("v.showClientMessageModal", false);
                //helper.doInitHlpr(component, helper);                
            }
            else {
                helper.showToast("error", "Error", result.status);
            }
            component.set("v.loading", false);
        });
    },
    HandlerDeleteCase:function(component,event, helper) {
        component.set("v.loading", true);
        var params = {
			caseId : component.get('v.caseRecordId')
        };
        this.makeServerCall(component, "deleteCase", params, function(result) {
            if(result.status === 'Ok') {
                component.set("v.showClientMessageModal", !component.get("v.showClientMessageModal"));    
                //component.set("v.showClientMessageModal", false);
                //helper.doInitHlpr(component, helper);                
            }
            else {
                helper.showToast("error", "Error", result.status);
            }
            component.set("v.loading", false);
        });
        
    }
    
    
})