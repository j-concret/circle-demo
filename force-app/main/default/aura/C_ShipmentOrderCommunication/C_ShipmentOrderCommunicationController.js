({
    doInitCtrl: function(component, event, helper) {
        if(component.get("v.recordId")) {
            helper.doInitHlpr(component, helper);
            helper.soLog(component, helper);
            helper.notifyAllParties(component, helper);
        }
        else {
            helper.showToast("error", "Error Occured", "Invalid record!");
        }
    },
    
    //Show Manual Status change Modal
    showManualChanges : function(component, helper){
        component.set("v.showManualChange", true);
    },
    
    //Show Update Final Delivery Modal
    showUploadPOD : function(component, helper){
        component.set("v.closeModal", true);
    },
    
    //handle Task/Case Chatter functionality
    showChatter: function(component, event, helper){
        //var tabName = event.getSource().get("v.name");
        helper.activeChatter(component, event, helper)
    },
    
    //Navigate Task/Case Record
    handleClickNavigate: function(component, event, helper){
        var navService = component.find("navService");
        var pageReference = {    
            "type": "standard__recordPage", //example for opening a record page, see bottom for other supported types
            "attributes": {
                "recordId": event.currentTarget.name, //place your record id here that you wish to open
                "actionName": "view"
            }
        }
        navService.navigate(pageReference);
    },
    
    //Handle Shipment order Chatter
    handleShowChatterSectionCtrl: function(component, event, helper) {
        component.set("v.showChatterSection", !component.get("v.showChatterSection"));
    },
    
    //Required Fields
    showRequiredFields: function(component, event, helper){
        
    },
    
    //onsubmit
    onRecordSubmit: function (component, event, helper) {
        component.set("v.showSpinner", true);
    },
    
    //OnError
    onError : function(component, event, helper) {
        
        var toastEvent2 = $A.get("e.force:showToast");
        toastEvent2.setParams({
            "title": "Error!",
            "message": "Error."
        });
        toastEvent2.fire();
        component.set("v.showSpinner", false);
    },
    
    //OnSuccess
    handleSuccess : function(component, event, helper) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({ "title": "Success!",
                              "message": "Saved.",
                              "type": "success"});
        toastEvent.fire();
        helper.activeSections(component);
        component.set("v.showSpinner", false);
    },
    
    //Handle Output/Input fields of Cost Estimate Tracking & Task Tab.
    editCostField: function(component, event, helper) {
        helper.activeSections(component);        
    },
    
    //Handle Reverse Output/Input fields of Cost Estimate Tracking & Task Tab. 
    handleCancel: function(component, event, helper) {
        helper.activeSections(component);  
        event.preventDefault();
    },
    
    //Refresh SO Log
    refreshLogs: function(component, event, helper) {
        helper.soLog(component, helper);
    },
    refreshCmp: function(component, event, helper) {
        component.set('v.showChatterSection',!component.get('v.showChatterSection'));
        component.set('v.showChatterSection',!component.get('v.showChatterSection'));
        helper.doInitHlpr(component, helper);
        helper.soLog(component, helper);
    },
    showNewClientMessage: function(component, event, helper) {
        if(event.getSource().get('v.label') == 'Close'){
        	component.set("v.showClientMessageModal", !component.get("v.showClientMessageModal"));    
        }else if(event.getSource().get('v.label') == 'Cancel'){
            helper.HandlerDeleteCase(component, event, helper);
            
        }else{
            component.set("v.showClientMessageModal", !component.get("v.showClientMessageModal"));    
        }
        
    },
    CreateCase: function(component, event, helper) {
        if(event.getSource().get('v.label') == 'Next'){
            if(component.find('caseSubject').get('v.value').trim().length == 0){
                helper.showToast("error", "error", 'Enter Case Subject !!');
                return;
            }
             if(!component.get("v.clientContactId")){
                helper.showToast("error", "error", 'Can not create Case, Client contact dont have NCP asccess');
                return;
            }
            helper.insertCase(component, event, helper);
        }else if(event.getSource().get('v.label') == 'Send Message'){
            if(component.find('caseMessage').get('v.value').trim().length == 0){
                helper.showToast("error", "error", 'Enter Case Message !');
                return;
            }
            var originaltext = component.find('caseMessage').get('v.value');
            var richContents = [];
            if(originaltext.includes('<b>')){
                richContents.push('Bold');
            }
            if(originaltext.includes('<i>')){
                richContents.push('Italic');
            }
            if(originaltext.includes('<strike>')){
                richContents.push('Strikethrough');
            }
            if(originaltext.includes('<u>')){
                richContents.push('Underline');
            }
             if(originaltext.includes('</p>')){
            originaltext = originaltext.replaceAll('<p>','\n');
        }
        
           
            helper.createMessageForCase(component, event, helper,richContents,originaltext);
            
        }
        
        
        
    },
  /*  saveNewClientMessage: function(component, event, helper) {
        
        if(component.find('caseSubject').get('v.value').trim().length == 0 || component.find('caseMessage').get('v.value').trim().length == 0){
            helper.showToast("error", "error", 'Subject and message are required fields');
            return;
        }
        
        var originaltext = component.find('caseMessage').get('v.value');
        var richContents = [];
        if(originaltext.includes('<b>')){
            richContents.push('Bold');
        }
        if(originaltext.includes('<i>')){
            richContents.push('Italic');
        }
        if(originaltext.includes('<strike>')){
            richContents.push('Strikethrough');
        }
        if(originaltext.includes('<u>')){
            richContents.push('Underline');
        }
        if(originaltext.includes('</p>')){
            originaltext = originaltext.replaceAll('<p>','\n');
        }
        
        if(!component.get("v.clientContactId")){
            helper.showToast("error", "error", 'Can not create Case, Client contact dont have NCP asccess');
            return;
        }
        helper.insertCase(component, event, helper,richContents,originaltext);
        
    },*/
    handleUploadFinished: function(component, event, helper) {
        component.find('caseFileUpload').set('v.disabled', true);
    }
    
})