({
    recordSelected : function( cmp, event, helper ){
        var selected_record = cmp.get( "v.object_record" );
        var component_event = cmp.getEvent( "selected_record_event" );
        component_event.setParams( { "record_event" : selected_record } );
        component_event.fire();
    }
});