({
    doInit: function(component, event, helper) {
        // create a Default RowItem [Contact Instance] on first time Component Load
        // by call this helper function  
        helper.getCurrencies(component);
        //helper.getCostings(component);
        helper.createObjectData(component, event);
    },
    removeRow : function(component, event, helper){
        var index = event.target.dataset.index;
        console.log('index ',index);
        // fire the DeleteRowEvt Lightning Event and pass the deleted Row Index to Event parameter/attribute
        component.getEvent("DeleteRowEvt").setParams({"indexVar" : index }).fire();
    }, 
    AddNewRow : function(component, event, helper){
        // fire the AddNewRowEvt Lightning Event 
        component.getEvent("AddRowEvt").fire();     
    },
    createCostings : function(component, event, helper){
        var costingList = component.get('v.costingList');
        var nameElements = [];
        var commentElements = [];
        var costAmount = [];
        component.set('v.isLoading',true);
        component.find("costName").length > 0 ?
            nameElements=component.find("costName") :
        nameElements.push(component.find("costName")) ;
        
        component.find("costAmount").length > 0 ?
            costAmount=component.find("costAmount") :
        costAmount.push(component.find("costAmount")) ;
        
        component.find("costComment").length > 0 ?
            commentElements=component.find("costComment") :
        commentElements.push(component.find("costComment")) ;
        var isValid = true;
        for(var index=0; index < costingList.length ; index++){
            var costing = costingList[index];
            if(! nameElements[index].get("v.value")){
                isValid =false;
                nameElements[index].setCustomValidity("Complete this field");
                nameElements[index].reportValidity();
            }else{
                nameElements[index].setCustomValidity("");
                nameElements[index].reportValidity();
            }
            if(! costAmount[index].get("v.value")){
                isValid =false;
                costAmount[index].setCustomValidity("Complete this field");
                costAmount[index].reportValidity();
            }else{
                costAmount[index].setCustomValidity("");
                costAmount[index].reportValidity();
            }
            if(! commentElements[index].get("v.value")){
                isValid =false;
                commentElements[index].setCustomValidity("Complete this field");
                commentElements[index].reportValidity();
            }else{
                commentElements[index].setCustomValidity("");
                commentElements[index].reportValidity();
            }
        }
        if(isValid){
            helper.createCosting(component);
        }else{
            component.set('v.isLoading',false);
        }
        
    },addNewRow: function(component, event, helper) {
        // call the comman "createObjectData" helper method for add new Object Row to List  
        helper.createObjectData(component, event);
    },
    
    // function for delete the row 
    removeDeletedRow: function(component, event, helper) {
        // get the selected row Index for delete, from Lightning Event Attribute  
        var index = event.getParam("indexVar");
        // get the all List (contactList attribute) and remove the Object Element Using splice method    
        var AllRowsList = component.get("v.costingList");
        AllRowsList.splice(index, 1);
        // set the contactList after remove selected row element  
        component.set("v.costingList", AllRowsList);
    },
})