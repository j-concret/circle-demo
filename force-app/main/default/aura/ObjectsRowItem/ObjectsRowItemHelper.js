({
    createCosting: function(component) {
        console.log('costinslist');
        console.log(component.get('v.costingList'));
        component.set('v.isNewCostingsCreated',false);
        var that = this;
        var action = component.get("c.saveCostings");
        action.setParams({
           'PCurr':component.get('v.createdInvoiceObject').Invoice_Currency__c,
            'SupplierInvoiceId':component.get('v.createdInvoiceObject').Id,
            'costingsList': component.get('v.costingList')
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS") {
                var result = response.getReturnValue(); if(result.status === "OK") {
                    console.log('costings created Successfully');
                    /*var json = {
                        label:"--None--",
                        value:""
                    };
                    (result.statuses).unshift(json);
                    component.set('v.categoryPicklist', result.statuses.sort(that.sortByProperty("value")));
                    component.set('v.costCategory',result.statuses[0].value);*/
                    component.set('v.costingList',[]);
                    that.createObjectData(component, event);
                    component.set('v.isNewCostingsCreated',true);
                    that.fireToast('SUCCESS', 'success', 'Costings created Successfully !');
                    component.set('v.isLoading',false);
                } else {
                    console.log(result.msg);
                    that.fireToast('ERROR', 'error', result.msg);
                    component.set('v.isLoading',false);
                }
            }
           else if(state === "ERROR") {
               var errors = response.getError();
               if(errors) {
                   if(errors[0] && errors[0].message) {
                       console.log(errors[0].message);
                       component.set('v.isLoading',false);
                       that.fireToast('ERROR', 'error', errors[0].message);
                   }
               } else {
                   console.log("Unknown error");
                   component.set('v.isLoading',false);
                   that.fireToast('ERROR', 'error', "Unknown error");
               }
           }
       });
        $A.enqueueAction(action);
    },
    getCurrencies : function(component) {
        var that = this;
        var action = component.get("c.getPicklistValues");
        action.setParams({
            'objectName': 'CPA_Costing__c',
            'fieldName': 'Cost_Category__c'
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS") {
                var result = response.getReturnValue(); if(result.status === "OK") {
                    /*var json = {
                        label:"--None--",
                        value:""
                    };
                    (result.statuses).unshift(json);*/
                    component.set('v.categoryPicklist', result.statuses.sort(that.sortByProperty("value")));
                    component.set('v.costCategory',result.statuses[0].value);
                } else {
                    console.log(result.msg);
                }
            }
            else if(state === "ERROR") {
                var errors = response.getError();
                if(errors) {
                    if(errors[0] && errors[0].message) {
                        console.log(errors[0].message);
                        //that.fireToast('ERROR', 'error', errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                    //that.fireToast('ERROR', 'error', "Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    createObjectData: function(component, event) {
        // get the contactList from component and add(push) New Object to List  
        var RowItemList = component.get("v.costingList");
        RowItemList.push({
            'sobjectType': 'CPA_Costing__c',
            'Name': '',
            'Cost_Category__c': component.get('v.costCategory') ? component.get('v.costCategory') : 'Admin',
            'Invoice_amount_local_currency__c': '',
            'Comment__c':''
        });
        // set the updated list to attribute (contactList) again    
        component.set("v.costingList", RowItemList);
    },
    // helper function for check if first Name is not null/blank on save  
    validateRequired: function(component, event) {
        var isValid = true;
        var allContactRows = component.get("v.costingList");
        for (var indexVar = 0; indexVar < allContactRows.length; indexVar++) {
            if (allContactRows[indexVar].Name == '') {
                isValid = false;
                alert('First Name Can\'t be Blank on Row Number ' + (indexVar + 1));
            }
        }
        return isValid;
    },
    sortByProperty: function(property){  
        return function(a,b){  
            if(a[property] > b[property])  
                return 1;  
            else if(a[property] < b[property])  
                return -1;  
            
            return 0;  
        }  
    },
    fireToast: function(title, type, message) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": title,
            "type": type,
            "message": message
        });
        toastEvent.fire();
    }

})