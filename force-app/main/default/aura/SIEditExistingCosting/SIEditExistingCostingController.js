({
	init: function (cmp, event, helper) {
        helper.fetchData(cmp,event, helper);		
	},
   
    
    save: function (cmp, event, helper) {
        cmp.set("v.is_loading", true);
        var draftValues = cmp.get("v.data");
        
        console.log(draftValues);
        var action = cmp.get("c.updateCostings");
        action.setParams({"editedAccountList" : draftValues});
        action.setCallback(this, function(response) {
            cmp.set("v.is_loading", false);
            
            var state = response.getState();
            if (state === 'SUCCESS'){
            
          // alert("Shipment Order Packages are updated ");
           $A.get('e.force:refreshView').fire();
            }
            else{
                alert("We are in fail block");
            }
            
        });
        $A.enqueueAction(action);
        
    },
    /*page refresh after data save*/    
    isRefreshed: function(component, event, helper) {
       location.reload();
      
        //   $A.get('e.force:refreshView').fire(); 
    },
})