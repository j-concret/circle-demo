({
    doInit : function(component, event, helper) {
        //  var recordTypeId = helper.getJsonFromUrl().recordTypeId;
        component.set('v.loading', true);
        if(component.get('v.pageReference').state){
            var recordTypeId = component.get('v.pageReference').state.recordTypeId;
            if(recordTypeId){
                console.log('recordTypeId');
                console.log(recordTypeId);
                component.set('v.recordTypeId',recordTypeId);
                 var action1 = component.get("c.getrecordTypes");
                    action1.setParams({
                        recordTypeId: recordTypeId
                    });
                    
                    action1.setCallback(this, function (response) {
                        var state1 = response.getState();
                        if (state1 === "SUCCESS") {
                            var result = response.getReturnValue();
                            
                            console.log('result');
                            console.log(JSON.stringify(result));
                            component.set('v.recordTypeName',result.recordType.Name);
                           console.log('recordTypeName');
                            console.log(component.get('v.recordTypeName'));
                            if(result.recordType.Name == 'Consignee Address'){
                                component.set('v.isNoConsigneeCreation',false);
                            }
                        }
                    });
                    $A.enqueueAction(action1);
            }
            var additionalParams = component.get('v.pageReference').state.additionalParams;
            //  var str = 'CF00N0Y00000CBfc0=Internal+Testing+Account&CF00N0Y00000CBfc0_lkid=0010Y00000PEqvH&';
            var clientId = additionalParams.substring(
                additionalParams.lastIndexOf("=") + 1, 
                additionalParams.lastIndexOf("&")
            );
            console.log(clientId);
            if(clientId){
                component.set('v.clientId',clientId);
                
            }
            component.set('v.loading', false);
        }
        //will get abc as id
        //var strParam = helper.getJsonFromUrl().str;//will get test as str
    },
    handleSearchAddress : function(component, event, helper) {
        /*component.set('v.searchKey', null);
        component.set('v.AddressList', null);*/
        component.set("v.showModalBox", true);
        
        /*   component.set("v.address1", "");
            component.set("v.address2", "");
            component.set("v.city", "");
            component.set("v.state", "");
            component.set("v.zip", "");
            component.set("v.country", "");*/
        
    },
    handleOnSuccess : function(component, event, helper) {
        component.set("v.reloadForm", false);
        component.set("v.reloadForm", true);
        component.set('v.loading', false);
    },
    handleOnSubmit : function(component, event, helper) {
        component.set('v.loading', true);
        event.preventDefault(); //Prevent default submit
        var eventFields = event.getParam("fields"); //get the fields
        console.log('JSON GETTING eventFields');
        console.log(JSON.stringify(eventFields));
        //eventFields["Description"] = 'Lead was created from Lightning RecordEditForm'; //Add Description field Value
          var vaildationFailReason = '';
         var showValidationError = false;
        var errorFields = [];
        var currentDate = new Date().toJSON().slice(0,10);
 	var fields = component.find("newClientAddress");
        fields.forEach(function (field) {
            if(field.get("v.fieldName") === 'Address__c' && $A.util.isEmpty(field.get("v.value"))){
                showValidationError = true;
                errorFields.push('Address');
                //field.set("v.errors", [{message:"Input not a number: "}]);
                //vaildationFailReason = "The Required fields must be completed: Address";
            } else if (field.get("v.fieldName") === 'City__c' && $A.util.isEmpty(field.get("v.value"))) {
                showValidationError = true;
                errorFields.push('City');
                //vaildationFailReason = "The Required fields must be completed: City";
            }
        });
         
        if (!showValidationError) {
           // component.set('v.loading', true);
           component.find('clientAddressCreationForm').submit(eventFields);
        } else {
             // vaildationFailReason = 'These Required fields must be completed: ' +errorFields.toString();
            //component.find('OppMessage').setError(vaildationFailReason);
            var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Error!",
                        "type": "error",
                        "message": 'These Required fields must be completed: ' +errorFields.toString()
                    });
                    toastEvent.fire();
           // component.set('v.loading', false); 
          	  component.set('v.loading', false);
        }
      //  component.find('clientAddressCreationForm').submit(eventFields); //Submit Form
    },
    handleOnSuccess : function(component, event, helper) {
        var param = event.getParams(); //get event params
        var fields = param.response.fields; //get all field info
        var childRelationships = param.response.childRelationships; //get child relationship info
        var recordTypeInfo = param.response.recordTypeInfo; //get record type info
        var recordId = param.response.id; //get record id
        
        console.log('Param - ' + JSON.stringify(param)); 
        console.log('Fields - ' + JSON.stringify(fields)); 
        console.log('Child Relationship - ' + JSON.stringify(childRelationships)); 
        console.log('Record Type Info - ' + JSON.stringify(recordTypeInfo)); 
        console.log('Record Id - ' + JSON.stringify(recordId)); 
        
        
       
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            "recordId": recordId,
            "slideDevName": "related"
        });
        navEvt.fire();
    },
    closeFocusedTab : function(component, event, helper) {
        var workspaceAPI = component.find("workspace");
        workspaceAPI.getFocusedTabInfo().then(function(response) {
            var focusedTabId = response.tabId;
            workspaceAPI.closeTab({tabId: focusedTabId});
        })
        .catch(function(error) {
            console.log(error);
        });
    }
})