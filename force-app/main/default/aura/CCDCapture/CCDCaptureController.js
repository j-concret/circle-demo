({
    doInit:function(component, event, helper) {
        if(!component.get("v.newSOPage")){
            
            
            var pageReference = component.get("v.pageReference");
            var workspaceAPI = component.find("workspace");
            workspaceAPI.getFocusedTabInfo().then(function(response) {
                var focusedTabId = response.tabId;
                workspaceAPI.setTabLabel({
                    tabId: focusedTabId,
                    label: "Capture" //set label you want to set
                });
                workspaceAPI.setTabIcon({
                    tabId: focusedTabId,
                    icon: "utility:add_contact", //set icon you want to set
                    iconAlt: "Capture" //set label tooltip you want to set
                });
            })
            component.set("v.customsClearanceDocumentId", pageReference.state.c__customsClearanceDocumentId);
            var recordId = pageReference.state.c__customsClearanceDocumentId;
        } 
        var action = component.get("c.getCCDDetails");
        action.setParams({"recordId" : component.get("v.customsClearanceDocumentId")});
        action.setCallback(this, function(response) { 
            var state = response.getState();
            var toastEvent = $A.get("e.force:showToast");
            
            if (state === 'SUCCESS'){
                
                var responseMap = response.getReturnValue();
                console.log(responseMap.status);
                if(responseMap.status == 'OK'){
                    component.set('v.ccdRResponse',responseMap)
                    component.set('v.customs_Clearance_Document',responseMap.data);
                }else{
                    
                    toastEvent.setParams({
                        title : 'Error',
                        message:responseMap['message'],
                        duration:' 5000',
                        key: 'info_alt',
                        type: 'error',
                        mode: 'pester'
                    });
                    toastEvent.fire();
                }
            }
            else if (state === "INCOMPLETE") {
                toastEvent.setParams({
                    title : 'Error',
                    message:"Unknown error",
                    duration:' 5000',
                    key: 'info_alt',
                    type: 'error',
                    mode: 'pester'
                });
                toastEvent.fire();
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            toastEvent.setParams({
                                title : 'Error',
                                message:errors[0].message,
                                duration:' 5000',
                                key: 'info_alt',
                                type: 'error',
                                mode: 'pester'
                            });
                            toastEvent.fire();
                        }
                    } else {
                        toastEvent.setParams({
                            title : 'Error',
                            message:"Unknown error",
                            duration:' 5000',
                            key: 'info_alt',
                            type: 'error',
                            mode: 'pester'
                        });
                        toastEvent.fire();
                    }
                }
            
        });
        $A.enqueueAction(action);
        
    },
    expandCol: function(component, event, helper) {
        
        if(component.get('v.isExpand')){
            component.set('v.isExpand',false);
            component.set('v.expandSize',11)
            
        }else{
            component.set('v.isExpand',true);
            component.set('v.expandSize',8)
        }
    }
})