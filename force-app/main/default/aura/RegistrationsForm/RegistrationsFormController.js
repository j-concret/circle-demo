({
    handleOnSubmit : function(cmp, event, helper) {
        cmp.set('v.showSpinner',true);
        event.preventDefault();       // stop the form from submitting
            var fields = event.getParam('fields');
            
            cmp.find('registrationForm').submit(fields);
    },
    handleCancel : function(cmp, event, helper) {
        cmp.set("v.isRegistrationModalOpen",false);
    },
    handleOnSuccess : function(cmp, event, helper) {
        cmp.set('v.showSpinner',false);
        cmp.find('notifLib').showToast({
            "variant": "success",
            "title": "Registration Created",
            "message": "Successfully created Registration record."
        });
        
        cmp.set("v.RegistrationName",event.getParam("response").fields.Name.value);
        cmp.set("v.RegistrationID",'/lightning/r/Registrations__c/'+event.getParam("response").id+'/view');
        cmp.set("v.hasRegistration", true);
        cmp.set("v.isRegistrationModalOpen",false);
    },
    handleError : function(cmp, event, helper) {
        cmp.set('v.showSpinner',false);
        console.log(event.getParam("output").fieldErrors);
    },
    createRegistrationRecord : function(cmp, event, helper) {
        cmp.find("registrationForm").submit();
    }
})