({
    doInitHlpr : function(component, helper) {
        var params = {
            objectName: component.get("v.objectName"),
            recordId: component.get("v.selItem")
        };
        this.makeServerCall(component,'getRecord',params,function(result)
                            {
                                if(result.status === 'OK')
                                {
                                    component.set("v.selItem", result.record[0]);
                                }
                                else{
                                    helper.showToast('error', 'Error Occured', result.msg);
                                }
                            });
    },
	itemSelectedHlpr : function(component, event, helper) {
        var target = event.target;   
        var SelIndex = helper.getIndexFrmParentHlpr(target,helper,"data-selectedIndex");  
        if(SelIndex){
            var serverResult = component.get("v.server_result");
            var selItem = serverResult[SelIndex];
            if(selItem.val){
               component.set("v.selItem",selItem);
               component.set("v.last_ServerResult",serverResult);
            } 
            component.set("v.server_result",null); 
        } 
	}, 
    serverCallHlpr : function(component, event, helper) {  
        var target = event.target;  
        var searchText = target.value; 
        var last_SearchText = component.get("v.last_SearchText");
        //Escape button pressed 
        if (event.keyCode == 27 || !searchText.trim()) { 
            helper.clearSelectionHlpr(component, event, helper);
        }else if(searchText.trim() != last_SearchText){ 
            //Save server call, if last text not changed
            //Search only when space character entered
         
            var objectName = component.get("v.objectName");
            var field_API_text = component.get("v.field_API_text");
            var field_API_val = component.get("v.field_API_val");
            var field_API_search = component.get("v.field_API_search");
            var limit = component.get("v.limit");
            
            var action = component.get('c.searchDB');
            action.setStorable();
            
            action.setParams({
                objectName : objectName,
                fld_API_Text : field_API_text,
                fld_API_Val : field_API_val,
                lim : limit, 
                fld_API_Search : field_API_search,
                searchText : searchText
            });
    
            action.setCallback(this,function(a){
                this.handleResponseHlpr(a,component,helper);
            });
            
            component.set("v.last_SearchText",searchText.trim());
            console.log('Server call made');
            $A.enqueueAction(action); 
        }else if(searchText && last_SearchText && searchText.trim() == last_SearchText.trim()){ 
            component.set("v.server_result",component.get("v.last_ServerResult"));
            console.log('Server call saved');
        }         
	},
    handleResponseHlpr : function (res,component,helper){
        if (res.getState() === 'SUCCESS') {
            var retObj = JSON.parse(res.getReturnValue());
            if(retObj.length <= 0){
                var noResult = JSON.parse('[{"text":"No Results Found"}]');
                component.set("v.server_result",noResult); 
            	component.set("v.last_ServerResult",noResult);
            }else{
                component.set("v.server_result",retObj); 
            	component.set("v.last_ServerResult",retObj);
            }  
        }else if (res.getState() === 'ERROR'){
            var errors = res.getError();
            if (errors) {
                if (errors[0] && errors[0].message) {
                    helper.showToast('error', 'Error Occured', errors[0].message);
                }
            } 
        }
    },
    getIndexFrmParentHlpr : function(target,helper,attributeToFind){
        //User can click on any child element, so traverse till intended parent found
        var SelIndex = target.getAttribute(attributeToFind);
        while(!SelIndex){
            target = target.parentNode ;
			SelIndex = helper.getIndexFrmParentHlpr(target,helper,attributeToFind);           
        }
        return SelIndex;
    },
    clearSelectionHlpr: function(component, event, helper){
        component.set("v.selItem",null);
        component.set("v.server_result",null);
    } 
})