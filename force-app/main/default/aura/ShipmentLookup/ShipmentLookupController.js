({
    doInitCtrl : function(component, event, helper) {
        if(component.get("v.selItem")){
            helper.doInitHlpr(component, helper);
        }
	}, 
	itemSelectedCtrl : function(component, event, helper) {
		helper.itemSelectedHlpr(component, event, helper);
	}, 
    serverCallCtrl :  function(component, event, helper) {
		helper.serverCallHlpr(component, event, helper);
	},
    clearSelectionCtrl : function(component, event, helper){
        helper.clearSelectionHlpr(component, event, helper);
    } 
})