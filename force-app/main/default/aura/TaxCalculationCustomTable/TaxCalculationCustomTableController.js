({
    doInit : function(component, event, helper) {
        var recordId = component.get("v.customsClearanceDocumentId");
        
        var action = component.get("c.getTaxCalc");
        action.setParams({"recordId" : recordId});
        action.setCallback(this, function(response) { 
            var state = response.getState();
            var toastEvent = $A.get("e.force:showToast");
            if (state === 'SUCCESS'){
                var responseMap = response.getReturnValue();
                if(responseMap.status == 'OK'){
                    var   dataMap =   responseMap.data;
                    
                    component.set("v.captureTaxCalculationList",dataMap);
                    
                }else{
                    toastEvent.setParams({
                        title : 'Error',
                        message:responseMap['message'],
                        duration:' 5000',
                        key: 'info_alt',
                        type: 'error',
                        mode: 'pester'
                    });
                    toastEvent.fire();
                }
            }
            else if (state === "INCOMPLETE") {
                toastEvent.setParams({
                    title : 'Error',
                    message:"Unknown error",
                    duration:' 5000',
                    key: 'info_alt',
                    type: 'error',
                    mode: 'pester'
                });
                toastEvent.fire();
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            toastEvent.setParams({
                                title : 'Error',
                                message:errors[0].message,
                                duration:' 5000',
                                key: 'info_alt',
                                type: 'error',
                                mode: 'pester'
                            });
                            toastEvent.fire();
                        }
                    } else {
                        toastEvent.setParams({
                            title : 'Error',
                            message:"Unknown error",
                            duration:' 5000',
                            key: 'info_alt',
                            type: 'error',
                            mode: 'pester'
                        });
                        toastEvent.fire();
                    }
                }
            
        });
        $A.enqueueAction(action);
    }
})