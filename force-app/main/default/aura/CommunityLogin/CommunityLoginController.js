({
    doInit:function(component,event,helper){
        if(window.innerWidth < 480){
            component.set("v.isMobile",true);
            component.set("v.backgroundImageURL",$A.get('$Resource.loginBackgroundMobile'));
            
        }
   /*   var hostname = window.location.hostname;
				var arr = hostname.split(".");
				var instance = arr[0];
                component.set("v.iframeUrl2","https://"+instance+".salesforce.com/apex/testIframe");  */
    },
    resetPass:function(component,
                            event, helper) {
          component.set("v.mylabel",
                      "");
        
        var validResetForm1 =
            component.find(
                "FormReset").get(
                "v.validity");
        // If we pass error checking, do some real work
        if (validResetForm1.valid) {
            var Reuser1 = component.get(
                "v.ResetUsername");
            var action = component.get(
                "c.forgotPassowrd"
            );
            action.setParams({
                username: Reuser1
            });
            action.setCallback(this,
                               function (a) {
                                   var rtnValue =
                                       a.getReturnValue();
                                   console.log(
                                       '<<my return value>>>>>' +
                                       rtnValue);
                                   // component.set("v.mylabel1",'We’ve sent you an email with a link to finish resetting your password.');
                                   
                                   if (rtnValue !==
                                       null) {
                                       component.set(
                                           "v.mylabel1",
                                           rtnValue
                                       );
                                       // component.set("v.showError",true);
                                   }
                               });
            $A.enqueueAction(action);
        }

    },
    cancelPass:function(component,
                            event, helper) {
        component.set('v.isVisible',true);
    },
    forgotPassword:function(component,
                            event, helper) {
        component.set('v.isVisible',false);
    },
    getInput: function (component,
                        event, helper) {
        
        component.set("v.mylabel1",
                      "");
        
        var validForm = component.find(
            'FormVal').reduce(
            function (validSoFar,
                      inputCmp) {
                
                inputCmp.showHelpMessageIfInvalid();
                return validSoFar &&
                    inputCmp.get(
                    'v.validity'
                ).valid;
            }, true);
        if (validForm) {
            var user = component.get(
                "v.Username");
            var Pass = component.get(
                "v.Password");
            var action = component.get(
                "c.checkPortal");
            action.setParams({
                username: user,
                password: Pass
            });
            action.setCallback(this,
                               function (response) {
                                   var state =
                                       response.getState();
                                   var rtnValue =
                                       response.getReturnValue();
                                   if (rtnValue !==
                                       null) {
                                       component.set(
                                           "v.mylabel",
                                           rtnValue
                                       );
                                   }
                               });
            
           
            $A.enqueueAction(action);
        }
    }
})