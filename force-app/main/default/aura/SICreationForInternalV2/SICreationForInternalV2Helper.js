({
	fetchData: function (component,event,helper) {
        var SupplierInvoiceID1 = component.get("v.CreatedSupplierInvoice"); // supplierInvoiceID
        var action = component.get("c.getCostings");
       action.setParams({  
           SupplierInvoiceID1 : component.get("v.CreatedSupplierInvoice"),
            });
         console.log(SupplierInvoiceID1);
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var data = response.getReturnValue();
                component.set('v.data',data);
            console.log(data);
                  
            }
            // error handling when state is "INCOMPLETE" or "ERROR"
        });
        $A.enqueueAction(action);
    },
    
    //Adding new Costs
    
    addAccountRecord: function(component, event) {
        //get the account List from component  
        var accountList = component.get("v.accountList");
        //Add New Account Record
        accountList.push({
            'sobjectType': 'CPA_Costing__c',
            'Name': '',
            'Cost_Category__c': '',
            'Invoice_amount_local_currency__c': ''
        });
        component.set("v.accountList", accountList);
    },
     
    validateAccountList: function(component, event) {
        //Validate all account records
        var isValid = true;
        var accountList = component.get("v.accountList");
        for (var i = 0; i < accountList.length; i++) {
            if (accountList[i].Name == '' || accountList[i].Cost_Category__c == '' || accountList[i].Invoice_amount_local_currency__c == '') {
                isValid = false;
                alert('Please enter required information in line # ' + (i + 1));
            }
        }
        return isValid;
    },
     
    saveAccountList: function(component, event, helper) {
         component.set("v.is_loading", true);
        //Call Apex class and pass account list parameters
        var action = component.get("c.saveCostings");
        var accountList = component.get("v.accountList");
        var SupplierInvoiceID1 = component.get("v.CreatedSupplierInvoice");
        var CCurr = component.get("v.invoice_currency");
        var self = this;
        
        action.setParams({
            "accList": component.get("v.accountList"),
            SupplierInvoiceID1 : component.get("v.CreatedSupplierInvoice"),
            "PCurr" : CCurr
        });
        action.setCallback(this, function(response) {
             component.set("v.is_loading", false);
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.accountList", []);
                var SIIDtoClose = component.get("v.CreatedSupplierInvoice");
                alert('Costings records saved successfully');
                self.fetchData(component,event, helper);
                // window.open("https://tecex--account2.lightning.force.com/lightning/r/Supplier_Invoice__c/"+ SIIDtoClose+"/view",'_self');
         
               // location.reload();
            }
            else{
                
                 alert('Costings are not created, Please send detailed email to support_SF@tecex.com');
                
            }
            
        }); 
        $A.enqueueAction(action);
    },
})