({
    handleSelection: function(component, event, helper) {
        component.set('v.isKeyPressed',event.shiftKey);
    },
    invoiceChange: function(component, event, helper) {
        var isKeyPressed = component.get('v.isKeyPressed');
        var rowIndex = component.get('v.rowIndex');
        var invoice = component.get('v.invoice');
        var selectedInvoices = component.get('v.selectedInvoices');
        var selectedInvoicesCheck = component.get('v.selectedInvoices');
        var filteredInvoiceList = component.get('v.filteredInvoiceList');
        var lastSelectedIndex = component.get('v.lastSelectedIndex');
        if(isKeyPressed && typeof invoice != 'undefined' 
           && !invoice.isInvoiceDisabled 
           && invoice.isInvoiceSelected 
           && typeof lastSelectedIndex != 'undefined'){
            var start = 0;
            var end = 0;
            
            if(lastSelectedIndex <  (rowIndex + 1)){
                start = lastSelectedIndex;
                end = rowIndex;
                
            }else{
                start = rowIndex;
                end = lastSelectedIndex;
                
            }
            if(((end-start)+selectedInvoices) <= 150){
            selectedInvoices--;
            for(let row = start ; row <= end ; row++){
                if(!filteredInvoiceList[row].isInvoiceDisabled){
                    (!filteredInvoiceList[row].isInvoiceSelected || rowIndex == row || lastSelectedIndex == row) ? selectedInvoices++ : '';    
                    filteredInvoiceList[row].isInvoiceSelected = true;
                }
            }
            
                component.set('v.filteredInvoiceList',filteredInvoiceList);
                component.set('v.lastSelectedIndex',rowIndex);      
            }else{
                selectedInvoices = selectedInvoicesCheck;
                filteredInvoiceList = component.get('v.filteredInvoiceList');
                invoice.isInvoiceSelected = false; 
                component.set('v.invoice',invoice);
                helper.showToast(component, event);
                
            }
        }else{
             if(typeof invoice != 'undefined' && !invoice.isInvoiceDisabled){
                
                if(invoice.isInvoiceSelected){
                    if(selectedInvoices < 150){
                        selectedInvoices++;
                        component.set('v.lastSelectedIndex',rowIndex);
                    }else{
                        invoice.isInvoiceSelected = false; 
                        component.set('v.invoice',invoice);
                        helper.showToast(component, event);
                        
                    }
                }else{
                    selectedInvoices--;
                    component.set('v.lastSelectedIndex',undefined);
                }
                
                
            }
            
        }
        component.set('v.selectedInvoices',selectedInvoices);
        
    },
    createBTA : function(component, event, helper) {
        
    }
})