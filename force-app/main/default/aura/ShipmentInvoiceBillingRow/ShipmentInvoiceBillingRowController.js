({
    doInit: function(component,event,helper){
        var charge = 1 + (component.get('v.shipmentOrder.On_Charge_Mark_up__c')/100);
        if(component.get('v.isActualCostSelected')){
            component.set('v.shipmentOrder.Total_Handling',(component.get('v.shipmentOrder.Actual_Total_Handling_Costs__c') * charge));
            component.set('v.shipmentOrder.Total_Customs',(component.get('v.shipmentOrder.Actual_Total_Customs_Brokerage_Cost__c') * charge));
            component.set('v.shipmentOrder.Total_clearance',(component.get('v.shipmentOrder.Actual_Total_Clearance_Costs__c') * charge));
            component.set('v.shipmentOrder.Total_License',(component.get('v.shipmentOrder.Actual_Total_License_Cost__c') * charge));
        }else{
            component.set('v.shipmentOrder.Total_Handling',component.get('v.shipmentOrder.Recharge_Handling_Costs__c'));
            component.set('v.shipmentOrder.Total_Customs',component.get('v.shipmentOrder.Total_Customs_Brokerage_Cost__c'));
            component.set('v.shipmentOrder.Total_clearance',component.get('v.shipmentOrder.Total_clearance_Costs__c'));
            component.set('v.shipmentOrder.Total_License',component.get('v.shipmentOrder.Recharge_License_Cost__c'));
        }
    },
    populateActuals: function(component,event,helper){
        var charge = 1 + (component.get('v.shipmentOrder.On_Charge_Mark_up__c')/100);
        if(component.get('v.isActualCostSelected')){
            component.set('v.shipmentOrder.Total_Handling',(component.get('v.shipmentOrder.Actual_Total_Handling_Costs__c') * charge));
            component.set('v.shipmentOrder.Total_Customs',(component.get('v.shipmentOrder.Actual_Total_Customs_Brokerage_Cost__c') * charge));
            component.set('v.shipmentOrder.Total_clearance',(component.get('v.shipmentOrder.Actual_Total_Clearance_Costs__c') * charge));
            component.set('v.shipmentOrder.Total_License',(component.get('v.shipmentOrder.Actual_Total_License_Cost__c') * charge));
        }else{
            component.set('v.shipmentOrder.Total_Handling',component.get('v.shipmentOrder.Recharge_Handling_Costs__c'));
            component.set('v.shipmentOrder.Total_Customs',component.get('v.shipmentOrder.Total_Customs_Brokerage_Cost__c'));
            component.set('v.shipmentOrder.Total_clearance',component.get('v.shipmentOrder.Total_clearance_Costs__c'));
            component.set('v.shipmentOrder.Total_License',component.get('v.shipmentOrder.Recharge_License_Cost__c'));
        }
    },
    handleSelection: function(component, event, helper) {
        component.set('v.isKeyPressed',event.shiftKey);
        console.log(component.get('v.isKeyPressed'));
    },
    shipmentOrderChange: function(component, event, helper) {
        var selectedShipmentRecordCount = component.get('v.selectedShipmentRecordCount');
        var selectedShipmentRecordCountChange = component.get('v.selectedShipmentRecordCount');
        var isKeyPressed = component.get('v.isKeyPressed');
        var shipmentOrder = component.get('v.shipmentOrder');
        var lastSelectedIndex = component.get('v.lastSelectedIndex');
        var shipmentObjects =component.get('v.shipmentObjects');
        var rowIndex = component.get('v.rowIndex');
        if(!shipmentOrder.processStatus){
            if(isKeyPressed && shipmentOrder.Invocing){
                var start = 0;
                var end = 0;
                if(lastSelectedIndex <  (rowIndex + 1)){
                    start = lastSelectedIndex;
                    end = rowIndex;
                }else{
                    start = rowIndex;
                    end = lastSelectedIndex;
                }
                
                if(((end-start)+selectedShipmentRecordCount) <= 100){
                    selectedShipmentRecordCount--;
                    for(let row = start ; row <= end ; row++){
                        (!shipmentObjects[row].Invocing || rowIndex == row || lastSelectedIndex == row) ? selectedShipmentRecordCount++ : '';    
                        shipmentObjects[row].Invocing = true;
                    }
                    component.set('v.lastSelectedIndex',rowIndex);
                }else{
                    selectedShipmentRecordCount = selectedShipmentRecordCountChange;
                    shipmentOrder.Invocing =false;
                    console.log('Max limit reached');
                    helper.fireToast('Info !', 'info', 'Max limit of 100 selection reached');
                }
                
            }else{
                if(shipmentOrder.Invocing){
                    if(selectedShipmentRecordCount < 100){
                        selectedShipmentRecordCount++;
                        component.set('v.lastSelectedIndex',rowIndex);
                    }else{
                        shipmentOrder.Invocing = false; 
                        component.set('v.shipmentOrder',shipmentOrder);
                        helper.fireToast('Info !', 'info', 'Max limit of 100 selection reached');
                        
                    }
                }else{
                    selectedShipmentRecordCount--;
                    component.set('v.lastSelectedIndex',undefined);
                }
                
                /*if(shipmentOrder.Invocing){
            selectedShipmentRecordCount++;
           } else{
               selectedShipmentRecordCount--;
           }*/
            }
            
        }
        
        component.set('v.shipmentOrder',shipmentOrder);
        component.set('v.shipmentObjects',shipmentObjects);
        component.set('v.selectedShipmentRecordCount',selectedShipmentRecordCount);
    }
})