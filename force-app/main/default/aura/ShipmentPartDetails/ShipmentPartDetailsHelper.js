({
  onSaveHlpr: function(component, helper) {
    component.set("v.showInnerSpinner", true);
    var part = component.get("v.part");
    if(part.Product__r && part.Product__r.Manufacturer__c) part.Product__r.Manufacturer__c = part.Product__r.Manufacturer__c.val;
    if(part.matched_hs_code) {
      //part.Matched_HS_Code2__c = part.Matched_HS_Code2__c.val;
      part.Matched_HS_Code2__r.Id = part.matched_hs_code.val;
    }
    //delete part.isExpanded;
    //delete part.requiredDocs;
    //delete part.totalColumns;

    var params = {
      partId: part.Id,
      product: part.Product__r,
      hsCode: part.Matched_HS_Code2__r
    };
    this.makeServerCall(component, 'updateShipmentParts', params, function(result) {
      if(result.status === 'OK') {
        var parts = component.get("v.part");

        if(result.parts[0]) {
          parts.Matched_HS_Code2__r = result.parts[0].Matched_HS_Code2__r;
        }

        component.set("v.part", parts);
        component.set("v.isEditEnabled", true);
        helper.showToast('success', 'Success', result.msg);
      }
      else {
        helper.showToast('error', 'Error Occured', result.msg);
      }
      component.set("v.showInnerSpinner", false);
    });
  },
  calculateSubCatValuesHlpr: function(component, categoryValue) {
    var catWithSubCatValues = component.get("v.catWithSubCatValues");
    var productSubCategoryValues = [];

    if(!categoryValue || categoryValue == '') {
      component.set("v.productSubCategoryValues", productSubCategoryValues);
      return;
    }

    if(catWithSubCatValues[categoryValue]) {
      for(var i = 0; i < catWithSubCatValues[categoryValue].length; i++) {
        productSubCategoryValues.push(catWithSubCatValues[categoryValue][i]);
      }
      component.set("v.productSubCategoryValues", productSubCategoryValues);
    }
  }
})