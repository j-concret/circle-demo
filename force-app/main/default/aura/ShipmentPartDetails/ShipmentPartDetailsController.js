({
  doInitCtrl: function(component, event, helper) {
    helper.calculateSubCatValuesHlpr(component, component.get("v.part.Product__r.Category__c"))
  },
  onSaveCtrl: function(component, event, helper) {
    if(component.get("v.part.productId")) {
      helper.onSaveHlpr(component, helper);
    }
  },
  onEditCtrl: function(component, event, helper) {
    component.set("v.isEditEnabled", false);
  },
  handleCategoryChangeCtrl: function(component, event, helper) {
    var categoryValue = event.getSource().get("v.value");

    helper.calculateSubCatValuesHlpr(component, categoryValue);
  }
})