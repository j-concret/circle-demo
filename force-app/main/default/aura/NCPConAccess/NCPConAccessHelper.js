({
    callDoInIt : function(component, event, helper) {
        var self = this;
        component.set('v.showSpinner',true);
        self.serverCall(component,'getContacts',{
            accId:component.get('v.recordId')
        },function(response){
            component.set('v.showSpinner',false);
            
            
            
            for(var i=0;i<response.contactsWithoutNCP.length;i++){
                response.contactsWithoutNCP[i].Client_Notifications_Choice__c  = 'Opt-In';
                response.contactsWithoutNCP[i].Client_Push_Notifications_Choice__c = 'Opt-In';
                response.contactsWithoutNCP[i].Client_Tasks_notification_choice__c  = true;
            }
          
            component.set("v.contactsWithoutNCP", response.contactsWithoutNCP);
            component.set("v.contactsWithNCP", response.contactsWithNCP);
            
            component.set("v.contacts", null);
            component.set("v.selectedContactsWithNCP", []);
            component.set("v.selectedContactsWithoutNCP", []);
            component.set("v.selectedContactsNotActiveNCP", []);
            
            let button = component.find('nextButton');
            if(button){
                button.set("v.disabled",true);
            }            
            
        },function(err){
            component.set('v.showSpinner',false);
            helper.showToast('error','ERROR',err);
        });
    },
    showToast : function(type, title , msg) {
        var toastEvent = $A.get('e.force:showToast');
        toastEvent.setParams({
            'type':type,
            'title': title ,
            'message': msg
        });
        toastEvent.fire();
    },
    contactSelected: function(component, event, helper) {
        component.find('nextButton').set("v.disabled",false);
        let index = event.getSource().get('v.name');
        let selTab = component.get("v.selectedTab");
        if(selTab == 'Activate New User'){
            if(event.getSource().get('v.checked')){
                let con = component.get("v.contactsWithoutNCP")[index];
                var selectedCon = component.get('v.selectedContactsWithoutNCP');
                selectedCon.push(con);
                component.set('v.selectedContactsWithoutNCP', selectedCon);                
            }else{
                let con = component.get("v.contactsWithoutNCP")[index];
                var selectedCon = component.get('v.selectedContactsWithoutNCP');
                let filteredData = helper.fiteration(component, event,helper,con,selectedCon);
                component.set('v.selectedContactsWithoutNCP', filteredData);
            }
        }else if(selTab == 'Edit Active Users'){
            if(event.getSource().get('v.checked')){
                let con = component.get("v.contactsWithNCP")[index];
                var selectedCon = component.get('v.selectedContactsWithNCP');
                selectedCon.push(con);
                component.set('v.selectedContactsWithNCP', selectedCon);
                
            }else{
                let con = component.get("v.contactsWithNCP")[index];
                var selectedCon = component.get('v.selectedContactsWithNCP');
                let filteredData = helper.fiteration(component, event,helper,con,selectedCon);
                component.set('v.selectedContactsWithNCP', filteredData);
            }
            
        }else if(selTab == 'Deactivate users'){
            if(event.getSource().get('v.checked')){
                let con = component.get("v.contactsWithNCP")[index];
                var selectedCon = component.get('v.selectedContactsNotActiveNCP');
                selectedCon.push(con);
                component.set('v.selectedContactsNotActiveNCP', selectedCon);
                
            }else{
                let con = component.get("v.contactsWithNCP")[index];
                var selectedCon = component.get('v.selectedContactsNotActiveNCP');
                let filteredData = helper.fiteration(component, event,helper,con,selectedCon);
                component.set('v.selectedContactsNotActiveNCP', filteredData);
            }
        }
        
        
    },
    fiteration : function(component, event,helper,con,selectedCon) {
        var filtered = selectedCon.filter(function(el) { return el.Id != con.Id; }); 
        if(filtered.length < 1){
            component.find('nextButton').set("v.disabled",true);
        }
        return filtered;
    },
    
    showContacts: function(component, event, helper,contacts) {
        component.set("v.contacts", contacts);
        helper.toggleClass(component);
        
    },
    toggleClass : function(component) {
        var before = component.find("before");
        $A.util.toggleClass(before, "slds-hide");
        var nextDiv = component.find("nextDiv");
        $A.util.toggleClass(nextDiv, "slds-hide");
    },
    saveContacts: function(component, event, helper,contacts) {
        var self = this;
        component.set('v.showSpinner',true);
        self.serverCall(component,'saveContacts',{
            lstCon:contacts
        },function(response){
            component.set('v.showSpinner',false);
            helper.showToast('Success','Success','Changes Saved');
            $A.get("e.force:closeQuickAction").fire(); 
        },function(err){
            helper.showToast('error','ERROR',err);
            component.set('v.showSpinner',false);
        });
    },
    userOperation: function(component, event, helper,contacts,flag) {
        var self = this;
        component.set('v.showSpinner',true);
        self.serverCall(component,'activeAndDeactivateUsers',{
            lstCon:contacts,
            flag:flag
        },function(response){
            component.set('v.showSpinner',false);
            if(flag){
                helper.showToast('Success','Success','Users Activated');
            }else{
                helper.showToast('Success','Success','Users Deactivated');
            }
            $A.get("e.force:closeQuickAction").fire(); 
        },function(err){
            helper.showToast('error','ERROR',err);
            component.set('v.showSpinner',false);
        });
    },
    createUser: function(component, event, helper,contacts) {
        var self = this;
        component.set('v.showSpinner',true);
        self.serverCall(component,'createNCPUser',{
            contacts:contacts
        },function(response){
            component.set('v.showSpinner',false);
            helper.showToast('Success','Success','Users Created');
            $A.get("e.force:closeQuickAction").fire(); 
        },function(err){
            helper.showToast('error','ERROR',err);
            component.set('v.showSpinner',false);
        });
    },    
    
})