({
    doInit : function(component, event, helper) {
        helper.callDoInIt(component, event, helper);
    },
    handleOnSubmit: function (component, event, helper) {
        component.set("v.showSpinner", true);
    },                
    selectContact: function(component, event, helper) {
        helper.contactSelected(component, event, helper);        
    },
    onTabActive: function(component, event, helper) {
        let selTab = component.get("v.selectedTab");
        component.find('nextButton').set("v.disabled",true);
        if(selTab == 'Activate New User' && component.get('v.selectedContactsWithoutNCP').length > 0){
            component.find('nextButton').set("v.disabled",false);
        }else if(selTab == 'Edit Active Users' && component.get('v.selectedContactsWithNCP').length > 0){
            component.find('nextButton').set("v.disabled",false);
        }else if(selTab == 'Deactivate users' && component.get('v.selectedContactsNotActiveNCP').length > 0){
            component.find('nextButton').set("v.disabled",false);
        }
        if(selTab == 'Deactivate users'){
            component.find('nextButton').set("v.label",'Deactivate User');
        }else{
            component.find('nextButton').set("v.label",'Next');
        }
        
    },
    //onsuccess
    handleSuccess : function(component, event, helper) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({ "title": "Success!",
                              "message": "Saved.",
                              "type": "success"});
        toastEvent.fire();
        component.set("v.showSpinner", false);
        helper.callDoInIt(component, event, helper);
    },
    
    //OnError 
    onError : function(component, event, helper) {
        
        var toastEvent2 = $A.get("e.force:showToast");
        toastEvent2.setParams({
            "title": "Error!",
            "message": "Error."
        });
        toastEvent2.fire();
        component.set("v.showSpinner", false);
    },
    
    //Handles visibility input and outputs fields
    handleCancel : function(component, event, helper) {
        $A.get("e.force:closeQuickAction").fire(); 
    },
    handleClickNavigate: function(component, event, helper) {
        var navService = component.find("navService");
        var pageReference = {    
            "type": "standard__recordPage", //example for opening a record page, see bottom for other supported types
            "attributes": {
                "recordId": event.currentTarget.name, //place your record id here that you wish to open
                "actionName": "view"
            }
        }
        
        navService.navigate(pageReference);
    },
    next: function(component, event, helper) {
        let selTab = component.get("v.selectedTab");
        if(selTab == 'Activate New User'){
            let contacts = component.get('v.selectedContactsWithoutNCP');
            helper.showContacts(component, event, helper,contacts);
            component.find('save').set("v.label",'Save');
        }else if(selTab == 'Edit Active Users'){
            let contacts = component.get('v.selectedContactsWithNCP');
            helper.showContacts(component, event, helper,contacts);
            component.find('save').set("v.label",'Save');
        }else if(selTab == 'Deactivate users'){
            let contacts = component.get('v.selectedContactsNotActiveNCP');
            helper.showContacts(component, event, helper,contacts);
            component.find('save').set("v.label",'Deactivate User');            
        }
    },
    save: function(component, event, helper) {
        let selTab = component.get("v.selectedTab");
        if(selTab == 'Activate New User'){
            let contacts = component.get('v.selectedContactsWithoutNCP');
            helper.createUser(component, event, helper,contacts);
        }else if(selTab == 'Edit Active Users'){
            let contacts = component.get('v.selectedContactsWithNCP');
            helper.saveContacts(component, event, helper,contacts);
        }else if(selTab == 'Deactivate users'){
            let contacts = component.get('v.selectedContactsNotActiveNCP');
            helper.userOperation(component, event, helper,contacts,false);
            
        }
    },
    back : function(component, event, helper) {
        helper.toggleClass(component);
    },

    
})