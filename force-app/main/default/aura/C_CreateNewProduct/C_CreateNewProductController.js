({
	doinit : function(component, event, helper) {
		helper.callDoInIt(component, event, helper);
	},
    cancelModal :function(component, event, helper) {
        $A.get("e.force:closeQuickAction").fire();    
    },
    saveProduct:function(component, event, helper) {
        if(!component.get("v.manufacturer") || !component.get("v.part").Name){
            helper.showToast('Error', 'Please fill required fields !', 'Error');
            return;
        }
        helper.save(component,event,helper);
    }
})