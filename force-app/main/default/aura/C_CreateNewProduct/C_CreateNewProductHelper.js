({
    callDoInIt : function(component, event, helper) {
        component.set("v.loading", true);
        var params = {
            partId: component.get('v.recordId')
        };
        this.makeServerCall(component, 'getPartDetails', params, function(result) {
            if(result.status === 'OK') {
                if(result.part){
                    component.set("v.part", result.part);
                }
                component.set("v.manufacturer", result.manufacturer);
                 component.set("v.countryPicklist", result.countryPicklist);
            }
            else {
                helper.showToast('error', 'Error Occured', result.status);
            }
            component.set("v.loading", false);
        });
        
    },
    
    save : function(component, event, helper) {
        component.set("v.loading", true);
        var params = {
            part: component.get('v.part'),
            manufacturer : component.get("v.manufacturer").Id
        };
        this.makeServerCall(component, 'createProduct', params, function(result) {
            if(result.status === 'OK') {
               helper.showToast('Success', 'Product Created', 'Success');
                $A.get("e.force:closeQuickAction").fire(); 
                window.location.reload();
            }
            else {
                helper.showToast('error', 'Error Occured', result.status);
            }
            component.set("v.loading", false);
        });
        
    },
    
})