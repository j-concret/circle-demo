({
    doInit: function(component, event, helper) {
        helper.getStatuses(component);
        helper.getInvoiceTimings(component);
        
    },
    onInvoiceDataSelect:function(component, event, helper){
        if(component.get('v.isInvoiceSelected')){
            component.set('v.isLoading',true);
             helper.getInvoicesWithBlankStatus(component);
            console.log('Invoice Data Selected');
        }
        
    },
    closedChanged:function(component, event, helper){
        if(component.get('v.isShipmentComponentClose')){
            helper.closeOnClick(component);
        }
        
    },
    accountChanged: function(component, event, helper) {
        var clientAccount = component.get('v.clientAccount');
        if(typeof clientAccount.Id != 'undefined'){
            component.set('v.selectedInvoiceTiming',clientAccount.Invoice_Timing__c);
            console.log('clientAccount');
            console.log(clientAccount.Name);
            console.log(clientAccount.Invoice_Timing__c);
            console.log(component.get('v.selectedInvoiceTiming'));
        }
    },
    onFilterSO: function(component, event, helper) {
        var shipmentObjects = component.get('v.shipmentObjects');
        var filter = component.get('v.filter');
        var filteredData = [];
        if(filter){
           
                filteredData = shipmentObjects.filter(data => {
                  return data.Name ? data.Name.toLowerCase().includes(filter.toLowerCase()) : false;
              });
                component.set('v.filteredShipmentObjects',filteredData);
          
        }else{
            component.set('v.filteredShipmentObjects',shipmentObjects);
        }
    },
    onInvoiceTimingSelect: function(component, event, helper) {
        console.log(component.get('v.selectedInvoiceTiming'));
        if(component.get('v.selectedInvoiceTiming') == 'Upfront invoicing'){
            component.set('v.isUpfrontInvoicing',true);
        }else{
            component.set('v.isUpfrontInvoicing',false);
        }
        console.log(component.get('v.isUpfrontInvoicing'));
    },
    getShipments: function(component, event, helper) {
        component.set('v.isShipmentApplied',false);
        component.set('v.isShipmentLoading',true);
        helper.getShipmentObjects(component);
       // helper.getInvoicesWithBlankStatus(component);
    },
    showEmailComponent: function(component, event, helper) {
       helper.generateInvoicesPDFObjects(component)
       
    },
    refreshCmp: function(component, event, helper) {
        component.set('v.isLoading',true);
        component.set('v.shipmentObjects',[]);
        
        component.set('v.eId', undefined);
        component.set('v.isProcessStatus',false);
        component.set('v.enterpriseName','');
        component.set('v.customerPO','');
        component.set('v.billingStartDate','');
        component.set('v.billingEndDate','');
        component.set('v.filteredShipmentObjects',[]);
        component.set('v.selectedShipmentRecordCount',0);
        component.set('v.clientAccount',{});
        component.set('v.isShipmentApplied',false);
        component.set('v.filter','');
        component.set('v.lastSelectedIndex',undefined);
        component.set('v.selectedInvoiceTiming','Upfront Invoicing');
        component.set('v.isUpfrontInvoicing',true);
        component.set('v.reviewMember',false);
        component.set('v.isInvoiceSelected',false);
        component.set('v.filteredInvoiceObjects',[]);
        component.set('v.invoiceObjects',[]);
        component.set('v.isShipmentButtonEnabled',true);
        component.set('v.selectedStatuses',['Customs Clearance Docs Received','Cancelled - With fees/costs']);
        component.set('v.isLoading',false);
        
    },
    processShipments: function(component, event, helper) {
        //component.set('v.isShipmentApplied',false);
        component.set('v.isShipmentUpdateLoading',true);
        if(component.get('v.selectedShipmentRecordCount') > 0){
            helper.createEnterpriseObject(component);
        }else{
            helper.fireToast('Info !', 'info', 'Select Shipment Order to process');
            component.set('v.isShipmentUpdateLoading',false); 
        }
        
    },
    showFilter: function(component, event, helper) {
        //helper.toogleFilter(component);
        var showFilter = component.get('v.showFilter');
        component.set('v.showFilter', !showFilter);
    },
    applyFilters: function(component,event,helper){
        var selectedStatuses = component.get('v.selectedStatuses');
        console.log(component.get('v.reviewMember'));
        helper.toogleFilter(component);
    },
    toggleSOFields: function(component,event,helper){
        component.set('v.isSOVisible',!component.get('v.isSOVisible'));
    },
    toggleTecExCharges: function(component,event,helper){
        component.set('v.isTecExChargesVisible',!component.get('v.isTecExChargesVisible'));
    },
    toggleOnCharges: function(component,event,helper){
        component.set('v.isOnChargesVisible',!component.get('v.isOnChargesVisible'));
    },
    toggleVAService: function(component,event,helper){
        component.set('v.isVAServicesVisible',!component.get('v.isVAServicesVisible'));
    },
    toggleTax: function(component,event,helper){
        component.set('v.isTaxVisible',!component.get('v.isTaxVisible'));
    },
    toggleOther: function(component,event,helper){
        component.set('v.isOtherVisible',!component.get('v.isOtherVisible'));
    },
})