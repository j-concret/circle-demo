({
    closeOnClick: function(component) {
        var outerDiv = document.getElementById('divOuter');
        outerDiv.classList.add("backgroundImage");
        outerDiv.classList.remove("relPos");
        component.destroy();
    },
    
    getInvoicesWithBlankStatus: function(component) {
         var that = this;
        var action = component.get("c.getInvoicesForBlankStatus");
        action.setParams({
            'AccountId':component.get('v.clientAccount').Id
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS") {
                var result = response.getReturnValue();
                console.log('result ',result);
					if(result.status === "OK") {
                    component.set('v.filteredInvoiceObjects', result.invoiceList);
                         component.set('v.invoiceObjects', result.invoiceList);
                        component.set('v.isLoading',false);
                } else {
                    console.log(result.msg);
                    component.set('v.isLoading',false);
                    that.fireToast('ERROR', 'error',result.msg);
                }
            }
            else if(state === "ERROR") {
                var errors = response.getError();
                if(errors) {
                    if(errors[0] && errors[0].message) {
                        console.log(errors[0].message);
                        component.set('v.isLoading',false);
                        that.fireToast('ERROR', 'error', errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                    component.set('v.isLoading',false);
                    that.fireToast('ERROR', 'error', "Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    generateInvoicesPDFObjects:function(component,eId,ShipmentListId){
        var ShipmentListId = component.get('v.shipmentsListId');
        var invoiceObjects = component.get('v.invoiceObjects');
        
        for(let invoice of invoiceObjects){
            
            if(invoice.Invocing && invoice.processStatus && !ShipmentListId.includes(invoice.Shipment_Order__r.Id)){
                //invoiceList = invoiceList.concat(invoice);
                ShipmentListId = ShipmentListId.concat(invoice.Shipment_Order__r.Id);
            }
        }
        
        var eId = component.get('v.eId'); 
        component.set('v.isInvoicePDFLoading',true);
        var that = this;
        var shipOrdersListIdChunks = that.chunk(ShipmentListId,10);
        console.log('shipOrdersListIdChunks');
        console.log(shipOrdersListIdChunks);
        var chunkIndex = 0;
        if(shipOrdersListIdChunks.length > 0){
            for(let shipOrdersListIdA of shipOrdersListIdChunks){
                console.log('shipOrdersListIdA');
                console.log(shipOrdersListIdA);
                var action = component.get("c.populateInvoiceOnShipment");
                action.setParams({
                    'ebillId': eId,
                    'shipmentOrderIds':shipOrdersListIdA
                });
                action.setCallback(this, function(response) {
                    var state = response.getState();
                    if(state === "SUCCESS") {
                        var result = response.getReturnValue(); 
                        console.log(result);
                        if(result.status === "OK") {
                            chunkIndex++;
                            if(shipOrdersListIdChunks.length == chunkIndex){
                                component.set('v.isInvoicePDFLoading',false); 
                                component.set('v.isShipmentButtonEnabled',false); 
                                component.set('v.isShipmentComponent',false); 
                                that.fireToast('Success !', 'success', 'Successfully updated Invoices !');
                            }
                        } else {
                            //console.log(result.msg);
                            that.fireToast('Error !', 'error', result.msg);
                            component.set('v.isInvoicePDFLoading',false);
                            component.set('v.isShipmentComponent',false); 
                        }
                    }
                    else if(state === "ERROR") {
                        var errors = response.getError();
                        if(errors) {
                            if(errors[0] && errors[0].message) {
                                console.log(errors[0].message);
                                that.fireToast('Error !', 'error', errors[0].message);
                                component.set('v.isInvoicePDFLoading',false);
                                component.set('v.isShipmentComponent',false); 
                                //that.fireToast('ERROR', 'error', errors[0].message);
                            }
                        } else {
                            //console.log();
                            that.fireToast('Error !', 'error', "Unknown error");
                            component.set('v.isInvoicePDFLoading',false);
                            component.set('v.isShipmentComponent',false); 
                            //that.fireToast('ERROR', 'error', "Unknown error");
                        }
                    }
                });
                $A.enqueueAction(action);
            }
        }else{
            console.log('ERROR IN LIST CHUNK INVOICES');
            component.set('v.isInvoicePDFLoading',false);
            component.set('v.isShipmentComponent',false); 
        }
    },
    processShipmentObjects:function(component){
        var that = this;
        var shipOrders = component.get('v.shipmentObjects');
        var shipOrdersList = [];
        for(let shipOrder of shipOrders){
            let shipJson = {};
            if(shipOrder.Invocing && !shipOrder.processStatus){
                shipJson.Id = shipOrder.Id;
                shipJson.Client_Reference__c = shipOrder.Client_Reference__c;
                shipJson.Client_Reference_2__c = shipOrder.Client_Reference_2__c;
                shipJson.IOR_Min_USD__c = shipOrder.IOR_FEE_USD__c;
                shipJson.EOR_Min_USD_new__c = shipOrder.EOR_and_Export_Compliance_Fee_USD__c;
                shipJson.Bank_Fees__c = shipOrder.Bank_Fees__c;
                shipJson.Handling_and_Admin_Fee__c = shipOrder.Handling_and_Admin_Fee__c;
                shipJson.Minimum_Handling_Costs__c = shipOrder.Total_Handling/(1+(shipOrder.On_Charge_Mark_up__c/100));
                shipJson.Minimum_Brokerage_Costs__c = shipOrder.Total_Customs/(1+(shipOrder.On_Charge_Mark_up__c/100));
                shipJson.Minimum_Clearance_Costs__c = shipOrder.Total_clearance/(1+(shipOrder.On_Charge_Mark_up__c/100));
                shipJson.Minimum_License_Permit_Costs__c = shipOrder.Total_License/(1+(shipOrder.On_Charge_Mark_up__c/100));
                shipJson.Freight_Fee_Calculated_Freight_Request__c = shipOrder.International_Delivery_Fee__c;
                shipJson.On_Charge_Mark_up__c = shipOrder.On_Charge_Mark_up__c;
                //shipJson.Finance_Fee__c = shipOrder.Potential_Cash_Outlay_Fee__c;
                shipJson.Insurance_Fee__c = shipOrder.Insurance_Fee__c;
                component.get('v.isUpfrontInvoicing') ? 
                shipJson.Tax_Cost__c = shipOrder.Recharge_Tax_and_Duty__c :
                shipJson.Tax_Cost__c = shipOrder.Recharge_Tax_and_Duty__c;
                shipJson.Recharge_Tax_and_Duty_Other__c = shipOrder.Recharge_Tax_and_Duty_Other__c;
                shipJson.Miscellaneous_Fee__c = shipOrder.Miscellaneous_Fee__c;
                shipJson.Miscellaneous_Fee_Name__c = shipOrder.Miscellaneous_Fee_Name__c;
                shipJson.IOR_Fee_new__c = 0;
                shipJson.EOR_Fee_new__c = 0;
                //shipJson.Special_Client_Invoicing__c = true;
                shipJson.Forecast_Override__c = true;
                shipJson.Override_Calculated_Taxes__c = true;
                shipJson.Liability_Cover_Fee_Added__c = true;
                //shipJson.Included_in_OBS_billing__c = true;
                //shipJson.Populate_Invoice__c = true;
                shipOrdersList.push(shipJson);
            }
            
        }
        var shipOrdersListChunks = that.chunk(shipOrdersList,10);
        console.log('shipOrdersListChunks ');
        console.log(shipOrdersListChunks);
        var chunkIndex = 0;
        var Statuses = [];
        var ShipmentListId = [];
        var eid = component.get('v.eId');
        if(shipOrdersListChunks.length > 0){
            for(let shipOrdersAList of shipOrdersListChunks){
                var action = component.get("c.processShipmentOrders");
                action.setParams({
                    'eid':(eid?eid:''),
                    'shipmentOrders': shipOrdersAList
                });
                action.setCallback(this, function(response) {
                    var state = response.getState();
                    if(state === "SUCCESS") {
                        var result = response.getReturnValue(); 
                        if(result.status === "OK") {
                            chunkIndex++;
                            eid = result.enterpriseBilling.Id;
                            console.log("eid");
                            console.log(eid);
                            component.set('v.eId',result.enterpriseBilling.Id);
                            console.log(result);
                            component.set('v.isShipmentUpdateLoading',false);
                            console.log('statuses');
                            console.log(result.successList);
                            if(result.successList.length > 0){
                                for(let status of result.successList){
                                    Statuses.push(status);
                                }
                            }
                            //Statuses.put(result.successList);
                            
                            
                            
                            if(shipOrdersListChunks.length == chunkIndex){
                                console.log("chunk Statuses");
                                console.log(Statuses);
                                var countShip = 0;
                                for(let shipOrder of shipOrders){
                                    if(shipOrder.Invocing && !shipOrder.processStatus){
                                        for(let status of Statuses){
                                            if(status[shipOrder.Id]){
                                                shipOrder.Status = status[shipOrder.Id];
                                                shipOrder.Status ? ShipmentListId.push(shipOrder.Id) :'';
                                                shipOrder.processStatus = true;
                                            }
                                        }
                                    }
                                    if(shipOrder.Invocing && shipOrder.processStatus){
                                        console.log(JSON.stringify(shipOrder));
                                    }else if(shipOrder.Invocing){
                                        countShip = countShip+1;
                                        console.log('countShip'+countShip);
                                        //shipOrder.Status = false;
                                       // shipOrder.processStatus = true;
                                    }
                                }
                                component.set('v.selectedShipmentRecordCount', countShip);
                                 console.log('countShip'+countShip);
                                component.set('v.isProcessStatus',true);
                                component.set('v.shipmentObjects',shipOrders);
                                component.set('v.filteredShipmentObjects',shipOrders);
                                //component.set('v.selectedShipmentRecordCount',0);
                                component.set('v.isShipmentButtonEnabled',false); 
                                that.fireToast('Success !', 'success', 'Successfully updated Shipment Orders and created Invoices !');
                                if(countShip > 0){
                                    that.fireToast('Info !', 'info', 'Some Shipment Orders Failed to process !');
                                }
                                var newShips = component.get('v.shipmentsListId');
                                if(newShips){
                                var shipIds = ShipmentListId.concat(newShips);
                                }
                                component.set('v.shipmentsListId',shipIds);
                                //that.generateInvoicesPDFObjects(component,result.enterpriseBilling.Id,ShipmentListId); 
                            }
                            
                        } else {
                            that.fireToast('Error !', 'error', result.msg);
                            component.set('v.isShipmentUpdateLoading',false);
                        }
                    }
                    else if(state === "ERROR") {
                        var errors = response.getError();
                        if(errors) {
                            if(errors[0] && errors[0].message) {
                                //console.log();
                                that.fireToast('Error !', 'error', errors[0].message);
                                component.set('v.isShipmentUpdateLoading',false);
                                //that.fireToast('ERROR', 'error', errors[0].message);
                            }
                        } else {
                            //console.log();
                            that.fireToast('Error !', 'error', "Unknown error");
                            component.set('v.isShipmentUpdateLoading',false);
                            //that.fireToast('ERROR', 'error', "Unknown error");
                        }
                    }
                });
                if(shipOrdersAList.length > 0){
                    
                    $A.enqueueAction(action);
                    
                }else{
                    console.log('no records selected');
                    component.set('v.isShipmentUpdateLoading',false);
                }
            }
        }else{
            console.log('ERROR IN LIST CHUNK');
            component.set('v.isShipmentUpdateLoading',false);
        }
    },
    createEnterpriseObject:function(component){
        var that = this;
        var action = component.get("c.getEnterpriseObject");
        var eid = component.get('v.eId');
        action.setParams({
            'eid':(eid?eid:''),
            'enterpriseName':(component.get('v.enterpriseName')?component.get('v.enterpriseName'):''),
            'accountId':component.get('v.clientAccount').Id,
            'customerPO':component.get('v.customerPO'),
            'billingStartDate':component.get('v.billingStartDate'),
            'billingEndDate':component.get('v.billingEndDate'),
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS") {
                var result = response.getReturnValue(); 
                if(result.status === "OK") {
                    component.set('v.eId',result.ebill.Id);
                    component.set('v.enterpriseObject',result.ebill);
                    
                    that.processShipmentObjects(component)
                } else {
                   // console.log();
                    that.fireToast('Error !', 'error', result.msg);
                     component.set('v.isShipmentUpdateLoading',false);
                }
            }
            else if(state === "ERROR") {
                var errors = response.getError();
                if(errors) {
                    if(errors[0] && errors[0].message) {
                       // console.log();
                        that.fireToast('Error !', 'error', errors[0].message);
                       component.set('v.isShipmentUpdateLoading',false);  
                    }
                } else {
                    //console.log();
                    that.fireToast('Error !', 'error', "Unknown error");
                    component.set('v.isShipmentUpdateLoading',false); 
                }
            }
        });
        if(component.get('v.enterpriseName')){
            if(component.get('v.customerPO')){
                if(component.get('v.billingStartDate')){
                    if(component.get('v.billingEndDate')){
                        $A.enqueueAction(action);
                    }else{
                        //console.log('enterprise billingEndDate empty');
                        that.fireToast('Error !', 'error', 'please enter Billing End Date');
                        component.set('v.isShipmentUpdateLoading',false);
                    }
                }else{
                    //console.log('enterprise billingStartDate empty');
                    that.fireToast('Error !', 'error', 'please enter Billing Start Date');
                    component.set('v.isShipmentUpdateLoading',false);
                }
            }else{
                //console.log('enterprise customerPO empty');
                that.fireToast('Error !', 'error', 'please enter Customer P.O.');
                component.set('v.isShipmentUpdateLoading',false);
            }
            
        }else{
            //console.log('enterprise name empty');
            that.fireToast('Error !', 'error', 'please enter Enterprise Name');
            component.set('v.isShipmentUpdateLoading',false);
        }
    },
    getShipmentObjects:function(component){
        var action = component.get("c.getShipmentOrders");
        action.setParams({
            'account': component.get('v.clientAccount').Id,
            'invoiceTiming': component.get('v.selectedInvoiceTiming'),
            'includedEnterprise':component.get('v.reviewMember'),
            'statuses':component.get('v.selectedStatuses')
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS") {
                var result = response.getReturnValue(); 
                if(result.status === "OK") {
                    component.set('v.shipmentObjects', result.shipmentOrders );
                    component.set('v.filteredShipmentObjects', result.shipmentOrders );
                    component.set('v.isShipmentApplied',true);
                    component.set('v.isShipmentLoading',false); 
                    console.log(result);
                } else {
                    console.log(result.msg);
                    component.set('v.isShipmentLoading',false);
                }
            }
            else if(state === "ERROR") {
                var errors = response.getError();
                if(errors) {
                    if(errors[0] && errors[0].message) {
                        console.log(errors[0].message);
                        component.set('v.isShipmentLoading',false);
                        //that.fireToast('ERROR', 'error', errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                    component.set('v.isShipmentLoading',false);
                    //that.fireToast('ERROR', 'error', "Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    getStatuses: function(component) {
        var that = this;
        var action = component.get("c.getPicklistValues");
        action.setParams({
            'objectName': 'Shipment_Order__c',
            'fieldName': 'Shipping_Status__c'
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS") {
                var result = response.getReturnValue(); if(result.status === "OK") {
                    component.set('v.statuses', result.statuses);
                } else {
                    console.log(result.msg);
                }
            }
            else if(state === "ERROR") {
                var errors = response.getError();
                if(errors) {
                    if(errors[0] && errors[0].message) {
                        console.log(errors[0].message);
                        //that.fireToast('ERROR', 'error', errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                    //that.fireToast('ERROR', 'error', "Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    getInvoiceTimings: function(component) {
        var that = this;
        var action = component.get("c.getPicklistValues");
        action.setParams({
            'objectName': 'Shipment_Order__c',
            'fieldName': 'Invoice_Timing__c'
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS") {
                var result = response.getReturnValue(); if(result.status === "OK") {
                    component.set('v.invoiceTimings', result.statuses);
                } else {
                    console.log(result.msg);
                }
            }
            else if(state === "ERROR") {
                var errors = response.getError();
                if(errors) {
                    if(errors[0] && errors[0].message) {
                        console.log(errors[0].message);
                        //that.fireToast('ERROR', 'error', errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                    //that.fireToast('ERROR', 'error', "Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    fireToast: function(title, type, message) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": title,
            "type": type,
            "message": message
        });
        toastEvent.fire();
    },
    toogleFilter: function(component, event, helper) {
        //var showFilter = component.get('v.showFilter');
        component.set('v.showFilter', false);
    },
    chunk: function(array, count) {
        if (count == null || count < 1) return [];
        var result = [];
        var i = 0, length = array.length;
        while (i < length) {
            result.push(Array.prototype.slice.call(array, i, i += count));
        }
        return result;
    }
    
})