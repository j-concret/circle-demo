({
  doInit : function(component, event, helper) {
    var fieldName = component.get('v.fieldName');
    var val = component.get('v.data.'+fieldName);
    component.set('v.val',val);
  }
});