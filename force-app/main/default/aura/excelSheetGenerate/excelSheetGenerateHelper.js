({
    getFiledNames:function(component){
        component.set('v.showSpinner',true);
        var action = component.get("c.getFields");
        action.setParams({ enter_bill : component.get("v.enterpriseId") });
        action.setCallback(this, function(response) {
            var state = response.getState();
            component.set('v.showSpinner',false);
            if (state === "SUCCESS") {
                var data = response.getReturnValue();
                if(data.status == 'OK'){
                    component.set('v.fields',data.fields);
                    component.set("v.requiredOptions",data.defaultFields);
                    component.set("v.values",data.defaultFields);
                   component.set('v.selectedFields',data.defaultFields);
                    var arr = [];
                     for (var i=0;i<data.defaultFields.length;i++)
            arr.push(i);
                    
        component.set('v.loopIteration',arr);
                }
            }
            else if (state === "INCOMPLETE") {
                // do something
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
        });
        $A.enqueueAction(action);  
    },
    getData:function(component){
        component.set('v.showSpinner',true);
        var action = component.get("c.getExcelData");
        action.setParams({ 
            enter_bill : component.get("v.enterpriseId"),
            fields:JSON.stringify(component.get('v.selectedFields')||[])
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var data = response.getReturnValue();
                if(data.status == 'OK'){
                    this.prepareSpreedSheet(component,data);
                }
            }
            else if (state === "INCOMPLETE") {
                component.set('v.showSpinner',false);
                this.showToast('ERROR','error',"Unknown error");
            }
                else if (state === "ERROR") {
                    component.set('v.showSpinner',false);
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            this.showToast('ERROR','error',errors[0].message);
                        }
                    } else {
                        this.showToast('ERROR','error',"Unknown error");
                    }
                }
        });
        $A.enqueueAction(action);
        
    },
    prepareSpreedSheet: function(component,excelData) {
        var ws_data = excelData.data;
        
        var sheetName = "Test";
        var wb = XLSX.utils.book_new();
        wb.Props = {
            Title: "Enterprise Billing",
            Subject: "Invocies",
            Author: "Tecex",
            CreatedDate: new Date()
        };
        wb.SheetNames.push(sheetName);
        
        var ws = XLSX.utils.json_to_sheet(ws_data,{header:excelData.header,skipHeader: false,
                                                   origin: "A22",defval:""});
        
        var s_row = 22;
        var e_col = excelData.header.length;
        var second_last_col = '';
        var last_col = "";
        if(e_col <= 27){
        second_last_col = String.fromCharCode(65 + (e_col - 2));
         last_col = String.fromCharCode(65 + (e_col - 1));
        }else{
           second_last_col = String.fromCharCode(65)+String.fromCharCode(65+(e_col - 26 - 2)); 
            last_col = String.fromCharCode(65)+String.fromCharCode(65+(e_col - 26 - 1));
        }
        
        
         
        var fmt = '$#,##0.00';
        
        const merge = [
            { s: { r: 3, c: (e_col-2) }, e: { r: 3, c: (e_col-1) } },
            { s: { r: 7, c: (e_col-2) }, e: { r: 7, c: (e_col-1) } }
        ];
        ws["!merges"] = merge;
        ws[second_last_col+'4'] = {
            t: "s", v: 'Invoice Summary',
            s:{
                bold:true,
                alignment: {horizontal: "center"}
            }
        };
        ws[second_last_col+'8'] = {
            t: "s", v: 'Consolidated Invoice',
            s:{
                alignment: {horizontal: "center"}
            }
        };
        //horizontal Line
        XLSX.utils.sheet_set_range_style(ws,{ 
            s: { c: 0, r: 6 }, e: { c: 32, r: 6 } }, // range
                                         { 
                                             bottom: { style: "thick",color: { rgb: "#666699" }},
                                         }
                                        );
        
        ws['A13'] = {
            t: "s", v: 'Invoice To',
            s:{
                alignment: {horizontal: "center"}
            }
        };
        ws['B13'] = {
            t: "s", v: excelData.invoice_to
        };
        
        ws[second_last_col+'11'] = {
            t: "s", v: 'Customer PO.',
            s:{
                color: { rgb: "#FF0000" },
                top: { style: "thin",color: { rgb: "#000000" }},
                right: { style: "thin",color: { rgb: "#000000" }} ,
                bottom: { style: "thin",color: { rgb: "#000000" }},
                left: { style: "thin",color: { rgb: "#000000" }} ,
            }
        };
        ws[last_col+'11'] = {
            t: "s", v: excelData.cust_po,
            s:{
                alignment: {horizontal: "center"},
                top: { style: "thin",color: { rgb: "#000000" }},
                right: { style: "thin",color: { rgb: "#000000" }} ,
                bottom: { style: "thin",color: { rgb: "#000000" }},
                left: { style: "thin",color: { rgb: "#000000" }} ,
            }
        };
        ws[second_last_col+'13'] = {
            t: "s", v: 'Consolidated Invoice Date:'
        };
        ws[last_col+'13'] = {
            t: "s", v: excelData.invoice_date,
            s:{
                alignment: {horizontal: "center"},
                top: { style: "thin",color: { rgb: "#000000" }},
                right: { style: "thin",color: { rgb: "#000000" }} ,
                bottom: { style: "thin",color: { rgb: "#000000" }},
                left: { style: "thin",color: { rgb: "#000000" }} ,
            }
        };
        ws[second_last_col+'14'] = {t: "s", v: 'Consolidated Invoice #',s:{color: { rgb: "#FF0000" }}};
        ws[last_col+'14'] = {
            t: "s", v: excelData.consolidated_invoice,
            s:{
                alignment: {horizontal: "center"},
                top: { style: "thin",color: { rgb: "#000000" }},
                right: { style: "thin",color: { rgb: "#000000" }} ,
                bottom: { style: "thin",color: { rgb: "#000000" }},
                left: { style: "thin",color: { rgb: "#000000" }} ,
            }
        };
        ws[second_last_col+'15'] = {t: "s", v: 'Billing Period'};
        ws[last_col+'15'] = {
            t: "s", v: excelData.billing_period,
            s:{
                alignment: {horizontal: "center"},
                top: { style: "thin",color: { rgb: "#000000" }},
                right: { style: "thin",color: { rgb: "#000000" }} ,
                bottom: { style: "thin",color: { rgb: "#000000" }},
                left: { style: "thin",color: { rgb: "#000000" }} ,
            }
        };
        ws[second_last_col+'18'] = {t: "s", v: 'Payment Terms'};
        ws[last_col+'18'] = {
            t: "s", v: excelData.pay_term,
            s:{
                alignment: {horizontal: "center"},
                top: { style: "thin",color: { rgb: "#000000" }},
                right: { style: "thin",color: { rgb: "#000000" }} ,
                bottom: { style: "thin",color: { rgb: "#000000" }},
                left: { style: "thin",color: { rgb: "#000000" }} ,
            }
        };
        ws[second_last_col+'20'] = {t: "s", v: 'Payment Due date:'};
        ws[last_col+'20'] = {
            t: "s", v: excelData.pay_due,
            s:{
                alignment: {horizontal: "center"},
                top: { style: "thin",color: { rgb: "#000000" }},
                right: { style: "thin",color: { rgb: "#000000" }} ,
                bottom: { style: "thin",color: { rgb: "#000000" }},
                left: { style: "thin",color: { rgb: "#000000" }} ,
            }
        };
        ws[second_last_col+(26+ws_data.length)] = {t: "s", v: 'Total amount due'};
        ws[last_col+(26+ws_data.length)] = {
            t: "n", v: excelData.total_amt,
            s:{
                top: { style: "thin",color: { rgb: "#000000" }},
                right: { style: "thin",color: { rgb: "#000000" }} ,
                bottom: { style: "thin",color: { rgb: "#000000" }},
                left: { style: "thin",color: { rgb: "#000000" }} ,
            },
            z:fmt
        };
        
        var dat = new Date();
        ws["!gridlines"] = false;
        
        for(var r=s_row;r<=(s_row+ws_data.length);r++){
            
            for(var c=0;c<e_col;c++){
                 var chr = String.fromCharCode(65 + c);
                if(c >= 26){
                chr = String.fromCharCode(65)+String.fromCharCode(65+(c - 26));
					}
               
                if(!ws[chr+''+r]){
                    ws[chr+''+r]=  { v: ''};
                }
                ws[chr+''+r]['s'] = {
                    top: { style: "thin",color: { rgb: "#000000" }},
                    right: { style: "thin",color: { rgb: "#000000" }} ,
                    bottom: { style: "thin",color: { rgb: "#000000" }},
                    left: { style: "thin",color: { rgb: "#000000" }} ,
                };
                if(ws[chr+''+r]['t'] && ws[chr+''+r]['t'] == 's'){
                    ws[chr+''+r]['s']['alignment'] = {horizontal: "center"};
                }
                if(ws[chr+''+r]['t'] && ws[chr+''+r]['t'] == 'n') ws[chr+''+r]['z'] = fmt;
            }   
        }
        XLSX.utils.sheet_set_range_style(
            ws,
            { s: { c: 0, r: 21 }, e: { c: 32, r: 21 } },
            { 
                fgColor: { rgb: '#ffc000'},
                alignment: {horizontal: "center"}
            }
        );
        
        
        var rang = XLSX.utils.decode_range(ws['!ref']);
        rang.e['r'] += 7;
        ws['!ref'] = XLSX.utils.encode_range(rang);
        
        
        /* ws["A"+(27+ws_data.length)] = {
            t: "s", v: 'Reconciliation Of Payments'
        };
        XLSX.utils.sheet_add_json(
            ws, // worksheet
            [], // data
            {skipHeader: true,origin: "A"+(28+ws_data.length),headers:['PO Number','Amount Available','Amount Used','Amount Remaining']}
        );
        
        
        //horizontal Line
        XLSX.utils.sheet_set_range_style(
            ws, 
            { s: { c: 0, r: (rang.e['r'] - 3) }, e: { c: 20, r: (rang.e['r'] - 3) } }, // range
            { 
                bottom: { style: "thick",color: { rgb: "#666699" }},
            }
        );*/
        
        //extend worksheet with blank rows
        XLSX.utils.sheet_set_range_style(
            ws, 
            { s: { c: 0, r: rang.e['r'] }, e: { c: 20, r: rang.e['r'] } }, // range
            { 
                bottom: { style: "thin",color: { rgb: "#000000" }},
            }
        );
        ws['!images']=[{
            '!pos': {x: 40, y: 10, w: 95, h: 95},
            '!datatype': 'base64',
            '!data': this.getBase64Image(document.getElementById("logoImg"))
        }];
        
        ws['!cols'] = this.getColWidthHeight(e_col,{auto:1});
        ws['!rows'] = this.getColWidthHeight(s_row+ws_data.length+15,{hpx: 25});
        
        wb.Sheets[sheetName] = ws;
        
        //XLSX.writeFile(wb, 'T'+dat.getTime()+'.xls', {bookImages:true,cellStyles: true,bookSST:true,bookType:'xlsx'});
        //return;
        var wbout = XLSX.write(wb, {bookImages:true,cellStyles: true,bookSST:true,bookType:'xlsx',type:'base64'});
        
        //make a server call to create an attachment record
        var action = component.get("c.createExcelAttachment");
        action.setParams({
            'blobData':wbout,
            'eBillId':component.get("v.enterpriseId")
        });
        action.setCallback(this, function(response) {
            component.set('v.showSpinner',false);
            var state = response.getState();
            if(state === "SUCCESS") {
                var result = response.getReturnValue();
                if(result.status === "OK") {
                    component.set('v.showEmailing',true);
                } else {
                    this.showToast('Error','error',result.msg);
                }
            }
            else if(state === "ERROR") {
                var errors = response.getError();
                if(errors) {
                    if(errors[0] && errors[0].message) {
                        this.showToast('Error','error',errors[0].message);
                    }
                } else {
                    this.showToast('Error','error','Unknown Error');
                }
                
            }
        });
        $A.enqueueAction(action);
    },
    getBase64Image: function (img) {
        var canvas = document.createElement("canvas");
        canvas.width = img.width;
        canvas.height = img.height;
        var ctx = canvas.getContext("2d");
        ctx.drawImage(img, 0, 0);
        var dataURL = canvas.toDataURL("image/png");
        return dataURL.replace(/^data:image\/(png|jpg);base64,/, "");
    },
    getColWidthHeight : function(len,obj){
        let objectMaxLength = [];
        for (let i = 0; i < len; i++) {
            objectMaxLength[i] = obj;//width: {auto: 1}; height:{hpx: 25};
        }
        return objectMaxLength;
    },
    removeFromArray:function(original, remove) {
        return original.filter(value => !remove.includes(value));
    },
    showToast : function(title,type,msg) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title:title,
            type:type,
            message:msg
        });
        toastEvent.fire();
    },
    naviageToRecord:function(component){
        var navService = component.find("navService");
        // Uses the pageReference definition in the init handler
        var pageReference = {
            "type": "standard__recordPage",
            "attributes": {
                "recordId": component.get('v.enterpriseId'),
                "objectApiName": "Enterprise_Billing__c",
                "actionName": "view"
            }
        };
        navService.navigate(pageReference);
    }
})