({
    doInit: function(component,event,helper){
        var enterpriseId = component.get('v.enterpriseId');
        if(!enterpriseId){
            var myPageRef = component.get("v.pageReference");
        	enterpriseId = myPageRef.state.c__enterID;
            if(!enterpriseId){
                helper.showToast('Error','error','Enterprise record Id is missing!');
                return;
            }
            component.set('v.enterpriseId',enterpriseId);
        }
        helper.getFiledNames(component);
    },
    generateExcel: function(component, event,helper) {
        var dualListBox = component.find('sheetFields');
        if(!dualListBox.checkValidity()){
            dualListBox.reportValidity();
            return;
        }
        helper.getData(component);
    },
    handleFieldChange: function (component, event,helper) {
        component.set('v.showPreview',false);
        var selectedOptionValue = event.getParam("value");
        var required = component.get('v.requiredOptions');
        var end_ind = selectedOptionValue.length -1;
        var len = required.length;
        if(selectedOptionValue.length > 2){
            len = selectedOptionValue.length;
            if(!required.includes(selectedOptionValue[0]) || !required.includes(selectedOptionValue[end_ind])){
                var vals = helper.removeFromArray(selectedOptionValue,required);
                vals.unshift(required[0]);
                vals.push(required[1]);
                component.set('v.values',vals);
                component.set('v.selectedFields',vals);
            }else{
                component.set('v.selectedFields',selectedOptionValue);
            }
        }else{
            component.set('v.values',required);
            component.set('v.selectedFields',required);
        }
        var arr = [];
        for (var i=0;i<len;i++)
            arr.push(i);
        component.set('v.loopIteration',arr);
    },
    handleShowPreview:function(component,event,helper){
        var dualListBox = component.find('sheetFields');
        if(!dualListBox.checkValidity()){
            dualListBox.reportValidity();
            return;
        }
        component.set('v.showPreview',true);
    }
})