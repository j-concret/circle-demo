({
  getTask : function(cmp) {
    var self = this;
    cmp.set('v.showSpinner',true);
    self.makeServerCall(cmp,'getTaskRecord',{
      taskId : cmp.get('v.recordId'),
      clientView : cmp.get('v.clientView')
    },function(resp){
      cmp.set('v.showSpinner',false);
      try{
        cmp.set('v.states',resp.states);
        if(!resp.task.Binding_Quote_Exempted_V2__c || resp.task.Binding_Quote_Exempted_V2__c ==''){
          resp.task['Binding_Quote_Exempted_V2__c'] = 'Binding';
        }
          
        cmp.set('v.bindingOptions',resp.bindingOptions);
cmp.set('v.notApplicableReasonOptions',resp.notApplicableReasonOptions);
          if(resp.task.ETA_Countdown__c){
          var Str = resp.task.ETA_Countdown__c;
          if(Str.charAt(0) == '-'){
              cmp.set('v.isETACountNegative',true);
          }
          if(Str.charAt(0) == '.'){
              resp.task.ETA_Countdown__c = '0'+resp.task.ETA_Countdown__c;
          }
          }
        cmp.set('v.task',resp.task);
        cmp.set('v.freight',resp.freight);
        cmp.set('v.taskTemplate',resp.template);
        if(resp.account){
          cmp.set('v.account',resp.account);
        }
      }catch(e){
        console.log(e.message);
      }
    },function(err){
      cmp.set('v.showSpinner',false);
      self.showToast('error','ERROR',err);
    });
  },
  subscribe : function(component) {
    const self = this;
    var recordId = component.get('v.recordId');
    // Get the empApi component
    const empApi = component.find('empApi');
    // Get the channel from the input box
    const channel = '/event/TaskChange__e';//component.find('channel').get('v.value');
    // Replay option to get new events
    const replayId = -1;

    // Subscribe to an event
    empApi.subscribe(channel, replayId, $A.getCallback(eventReceived => {
        // Process event (this is called each time we receive an event)
        console.log('Received event ', JSON.stringify(eventReceived));
        if(eventReceived.data.payload.TaskIds__c){
          let taskIds = eventReceived.data.payload.TaskIds__c.split(',');
          if(taskIds.includes(recordId))
            self.getTask(component);
        }
    }))
    .then(subscription => {
        // Subscription response received.
        // We haven't received an event yet.
        console.log('Subscription request sent to: ', subscription.channel);
        // Save subscription to unsubscribe later
        component.set('v.subscription', subscription);
    });
  },
  submit:function(cmp) {
    let isAllValid;
      if(cmp.find('field')){
          isAllValid = [].concat(cmp.find('field')).reduce(function(isValidSoFar, inputCmp){
            inputCmp.reportValidity();
            return isValidSoFar && inputCmp.checkValidity();
        },true);
      }
      else{
          isAllValid = true;
      }
      
      if(isAllValid){
    var self = this;
    cmp.set('v.showSpinner',true);
    var tt = cmp.get('v.taskTemplate');
    self.makeServerCall(cmp,'updateSfRecord',{
      taskId : cmp.get('v.recordId'),
      data : JSON.stringify(tt)
    },function(resp){
      cmp.set('v.showSpinner',false);
      self.showToast('Success','success','Updated Successfully!');
      if(!tt.btnAction || tt.btnAction == 'No' || tt.btnObject != 'Task'){
        self.getTask(cmp);
      }
      //window.location.reload();
    },function(err){
      cmp.set('v.showSpinner',false);
      self.showToast('error','ERROR',err);
    });
      }
  },
  saveTask : function(cmp){
    //check input validation
    var inp = cmp.find('field');
    if(inp && !inp.get('v.value')){
      //display the error messages
      inp.reportValidity();
      return;
    }

    var self = this;
    cmp.set('v.showSpinner',true);
    self.makeServerCall(cmp,'updateTask',{
      task : cmp.get('v.task')
    },function(resp){
      cmp.set('v.showSpinner',false);
      cmp.set('v.editable',false);
      self.showToast('Success','success','Task Updated Successfully!');
      //self.getTask(cmp);
    },function(err){
      cmp.set('v.showSpinner',false);
      self.showToast('error','ERROR',err);
    });
  },
  showToast : function(title,type,msg) {
    var toastEvent = $A.get('e.force:showToast');
    toastEvent.setParams({
      'title': title,
      'type' :type,
      'message': msg
    });
    toastEvent.fire();
  },
  getParameterByName:function(name) {
    var url = window.location.href;
    name = name.replace(/[\[\]]/g, '\\$&');
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
      results = regex.exec(url);
    if (!results) {return null;}
    if (!results[2]) {return '';}
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
  }
});