({
  doInit : function(component, event, helper) {
    var view  = helper.getParameterByName('c__view');
    var isClient = false;
    if((view && view === 'Client')) isClient = true;
    component.set('v.clientView',isClient );
    helper.subscribe(component);
    helper.getTask(component);
  },
  viewChangeHandler:function(component, event, helper) {
    helper.getTask(component);
  },
  submitHandler: function(component, event, helper) {
    helper.submit(component);
  },
  navigateToRecord: function(component, event, helper) {
    var selectedItem = event.currentTarget;
    var objName = selectedItem.dataset.obj;
    var recid = selectedItem.dataset.recid;
    if(objName && recid){
      var navService = component.find('navService');
      var pageReference = {
        'type': 'standard__recordPage',
        'attributes': {
          'recordId': recid,
          'objectApiName': objName,
          'actionName': 'view'
        }
      };
      navService.navigate(pageReference);
    }
  },
  handleEditClick: function(component, event, helper) {
    component.set('v.taskCopy',JSON.parse(JSON.stringify(component.get('v.task'))));
    component.set('v.editable',true);
  },
  handleCancelClick:function(component, event, helper) {
    component.set('v.task',JSON.parse(JSON.stringify(component.get('v.taskCopy'))));
    component.set('v.editable',false);
  },
  handleSaveClick:function(component, event, helper) {
    helper.saveTask(component);
  },
  changeHandler:function(component, event, helper) {
    var tskCpy = component.get('v.taskCopy');
    if(event.getSource().get('v.checked') !== tskCpy.Inactive__c){
      component.set('v.task.Manually_Override_Inactive_Field__c',true);
    }else if(!tskCpy.Manually_Override_Inactive_Field__c){
      component.set('v.task.Manually_Override_Inactive_Field__c',false);
    }
  }
});