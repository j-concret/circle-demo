({
    
    // Auto Fill Address 
     OpenModal : function(component, event, helper) {
        component.set('v.searchKey', null);
        component.set('v.AddressList', null);
        component.set("v.showModalBox", true);
        var selectedItem = event.currentTarget;
        //Get the selected item index
        var indexvar = selectedItem.dataset.record;
        //var indexvar = event.getSource().get("v.label");
        //  console.log("indexvar:::" + indexvar);
        component.set('v.pickIndex',parseInt(indexvar));
        component.set('v.isNewPickAddOpen',false);
        component.set('v.isPickupaddressModelOpen',false);
        var addressList = component.get('v.PickaddList');
        console.log('addressList '+indexvar);
        console.log(JSON.stringify(addressList[indexvar]));
        component.set("v.address1", "");
            component.set("v.address2", "");
            component.set("v.city", "");
            component.set("v.state", "");
            component.set("v.zip", "");
            component.set("v.country", "");
        if(addressList && addressList[indexvar]){
            component.set("v.address1",addressList[indexvar].Address__c);
            component.set("v.address2",addressList[indexvar].Address2__c); 
            component.set("v.city",addressList[indexvar].City__c);
            component.set("v.state",addressList[indexvar].Province__c);
            component.set("v.zip",addressList[indexvar].Postal_Code__c);
            component.set("v.country",addressList[indexvar].All_Countries__c);
        }
        console.log('addressList after '+indexvar);
        console.log(JSON.stringify(addressList[indexvar]));
    },
    //Clear the address list on the user interface
    clear: function(component, event, helper) {
        helper.clearAddressList(component, event);
    },
    //On search of address get address list from the API
    keyPressController: function(component, event, helper) {
        helper.getAddressRecommendations(component,event,false);
    },
     selectOption:function(component, event, helper) {
        console.log('select Option clicked');
        helper.getAddressDetailsByPlaceId(component, event,false);
    },
    addPlace: function(component, event, helper) {
        var addIndex = component.get('v.pickIndex');
        var addressList = component.get('v.PickaddList');
        var ship_fromSO = '';
        if(component.get('v.ship_from')){
        ship_fromSO = (component.get('v.ship_from')).toLowerCase();
        }
        var countrySO = '';
        if( component.get("v.country")){
            countrySO = component.get("v.country").toLowerCase();
        }
        console.log('addressList ');
        console.log(addressList[addIndex]);
		console.log(countrySO);
		console.log(ship_fromSO);
         if(countrySO != ship_fromSO){
              var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Error!",
                        "type": "error",
                        "message": "Please Select address of "+component.get('v.ship_from')+' Only !'
                    });
                    toastEvent.fire();
              //alert('Add City');
              return;
          }
        if(! component.get("v.address1") || component.get("v.address1") == '  '){
              var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Error!",
                        "type": "error",
                        "message": "Address 1 is a required field"
                    });
                    toastEvent.fire();
              //alert('Add City');
              return;
          }
        
        if(! component.get("v.city")){
              var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Error!",
                        "type": "error",
                        "message": "City is a required field"
                    });
                    toastEvent.fire();
              //alert('Add City');
              return;
          }
           
           if(! component.get("v.state")){
              var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Error!",
                        "type": "error",
                        "message": "State is a required field"
                    });
                    toastEvent.fire();
              //alert('Add City');
              return;
          }
           if(! component.get("v.country")){
              var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Error!",
                        "type": "error",
                        "message": "Country is a required field"
                    });
                    toastEvent.fire();
              //alert('Add City');
              return;
          }
           if(! component.get("v.zip")){
              var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Error!",
                        "type": "error",
                        "message": "Zip/Postal Code is a required field"
                    });
                    toastEvent.fire();
              //alert('Add City');
              return;
          }
        
        addressList[addIndex].Address__c  = component.get("v.address1");
        addressList[addIndex].Address2__c  = component.get("v.address2"); 
        addressList[addIndex].City__c  = component.get("v.city");
        addressList[addIndex].Postal_Code__c  = component.get("v.zip") ;
        addressList[addIndex].Province__c  = component.get("v.state");
        addressList[addIndex].All_Countries__c  = component.get("v.country");
        component.set("v.showModalBox", false);
        component.set('v.PickaddList',addressList);
        component.set('v.isPickupaddressModelOpen',true);
        component.set('v.isNewPickAddOpen',true);
    }, 
    closeModal: function(component, event, helper) {
        component.set("v.showModalBox", false);
        component.set('v.isPickupaddressModelOpen',true);
        component.set('v.isNewPickAddOpen',true);
    },
    finalClear: function(component, event, helper) {
        helper.clearFinalAddressList(component, event);
    },
    finalKeyPressController: function(component, event, helper) {
        helper.getAddressRecommendations(component,event,true);
    }, 
     selectFinalOption:function(component, event, helper) {
        console.log('select Option clicked');
        helper.getAddressDetailsByPlaceId(component, event,true);
    },
      addFinalPlace: function(component, event, helper) {
        var addIndex = component.get('v.finalPickIndex');
        var addressFinalList = component.get('v.FinalDeladdList');
        console.log('addressList ');
        console.log(addressFinalList[addIndex]);
		//destination
		var destinationSO = '';
        if(component.get('v.destination')){
        destinationSO = (component.get('v.destination')).toLowerCase();
        }
        var finalCountrySO = '';
        if( component.get("v.finalCountry")){
            finalCountrySO = component.get("v.finalCountry").toLowerCase();
        }
        //console.log('addressList ');
        //console.log(addressList[addIndex]);
         if(finalCountrySO != destinationSO){
              var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Error!",
                        "type": "error",
                        "message": "Please Select address of "+component.get('v.destination')+' Only !'
                    });
                    toastEvent.fire();
              //alert('Add City');
              return;
          }
         if(! component.get("v.finalAddress1") || component.get("v.finalAddress1") == '  '){
              var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Error!",
                        "type": "error",
                        "message": "Address 1 is a required field"
                    });
                    toastEvent.fire();
              //alert('Add City');
              return;
          }
           if(! component.get("v.finalCity")){
              var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Error!",
                        "type": "error",
                        "message": "City is a required field"
                    });
                    toastEvent.fire();
              //alert('Add City');
              return;
          }
           if(! component.get("v.finalState")){
              var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Error!",
                        "type": "error",
                        "message": "State is a required field"
                    });
                    toastEvent.fire();
              //alert('Add City');
              return;
          }
           if(! component.get("v.finalCountry")){
              var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Error!",
                        "type": "error",
                        "message": "Country is a required field"
                    });
                    toastEvent.fire();
              //alert('Add City');
              return;
          }
           if(! component.get("v.finalZip")){
              var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Error!",
                        "type": "error",
                        "message": "Zip/Postal Code is a required field"
                    });
                    toastEvent.fire();
              //alert('Add City');
              return;
          }
        addressFinalList[addIndex].Address__c  = component.get("v.finalAddress1");
        addressFinalList[addIndex].Address2__c  = component.get("v.finalAddress2"); 
        addressFinalList[addIndex].City__c  = component.get("v.finalCity");
        addressFinalList[addIndex].Postal_Code__c  = component.get("v.finalZip");
        addressFinalList[addIndex].Province__c  = component.get("v.finalState");
        addressFinalList[addIndex].All_Countries__c  = component.get("v.finalCountry");
        component.set("v.showFinalModalBox", false);
        component.set('v.FinalDeladdList',addressFinalList);
        component.set('v.isfinaldeliveryaddModelOpen',true);
        component.set('v.isNewFinDelAddOpen',true);
    }, 
     closeFinalModal: function(component, event, helper) {
        component.set("v.showFinalModalBox", false);
        component.set('v.isfinaldeliveryaddModelOpen',true);
        component.set('v.isNewFinDelAddOpen',true);
    },
    OpenFinalModal : function(component, event, helper){
            component.set('v.finalSearchKey', null);
        component.set('v.FinalAddressList', null);
        component.set("v.showFinalModalBox", true);
        var selectedItem = event.currentTarget;
        //Get the selected item index
        var indexvar = selectedItem.dataset.record;
        //var indexvar = event.getSource().get("v.label");
        //  console.log("indexvar:::" + indexvar);
        component.set('v.finalPickIndex',parseInt(indexvar));
        component.set('v.isNewFinDelAddOpen',false);
        component.set('v.isfinaldeliveryaddModelOpen',false);
        var addressFinalList = component.get('v.FinalDeladdList');
        console.log('addressList '+indexvar);
        console.log(JSON.stringify(addressFinalList[indexvar]));
        component.set("v.finalAddress1", "");
            component.set("v.finalAddress2", "");
            component.set("v.finalCity", "");
            component.set("v.finalState", "");
            component.set("v.finalZip", "");
            component.set("v.finalCountry", "");
        if(addressFinalList && addressFinalList[indexvar]){
            component.set("v.finalAddress1",addressFinalList[indexvar].Address__c);
            component.set("v.finalAddress2",addressFinalList[indexvar].Address2__c); 
            component.set("v.finalCity",addressFinalList[indexvar].City__c);
            component.set("v.finalState",addressFinalList[indexvar].Province__c);
            component.set("v.finalZip",addressFinalList[indexvar].Postal_Code__c);
            component.set("v.finalCountry",addressFinalList[indexvar].All_Countries__c);
        }
        console.log('addressList after '+indexvar);
        console.log(JSON.stringify(addressFinalList[indexvar]));
    },
  
    
    // Auto Fill Address End Here
    
    
    Homereturn :function (cmp, event, helper) { cmp.set("v.displayedSection","HomePage"); },
  
   
   //******** Package Model Body Start***********//
   doInit : function( componentP, eventP, helperP ){
       helperP.sethelper(componentP, eventP);
       window.addEventListener("message", helperP.receiveMessage.bind(helperP), false);
       componentP.set( "v.table_headings",
                       [ "#","How many packages?", "Weight Unit", "Actual Weight",
                         "Dimension Unit", "Length", "Breadth", "Height" ] );
       
            
   },
   
   addNewRow : function( componentP, eventP, helperP ){

       helperP.createTableRow( componentP, eventP );

       var package_list = componentP.get( "v.shipment_order_package_list" );

       componentP.set( "v.object_list_string", helperP.createObjectString( package_list ) );

   },
    deleteRow : function( componentP, eventP, helperP ){

       var index = eventP.getParam( "index" );         //get the index
       var rows_list = componentP.get( "v.shipment_order_package_list" );

       rows_list.splice( index, 1 );           //remove the value

       componentP.set( "v.shipment_order_package_list", rows_list );
       componentP.set( "v.object_list_string", helperP.createObjectString( componentP.get( "v.shipment_order_package_list" ) ) );

         componentP.set('v.ShipmentOrderPackageSize',rows_list.length);
   },
   
 //******** Package Model Body end***********//
   
   //******** Package ModelBOX  start***********//
    openPackageModel : function(cmp, event, helper) {
        
        
    var a = event.getSource().get("v.value");
        if(a=="No"){
                    cmp.set("v.isPackageModelOpen",'false');
                    }
        else{
            cmp.set("v.isPackageModelOpen",'true');
            
        }
        
        
    },
    
    closePackageModel : function(cmp, event, helper) {cmp.set("v.isPackageModelOpen", false);},
    submitPackageDetails : function(cmp, event, helper) {cmp.set("v.isPackageModelOpen", false);},
//******** Package ModelBOX  End***********//


//******** Create Cost Estimate***********//
   CreateCEButton : function(component, event, helper) {
       component.set("v.is_loading", true);
     
       
       //AccId,ConID,ServiceRequired,FreightOption,ShipmentValue,Clientref1,ClientRef2,PoNumber,Chargableweight,Listof Packages
       //ShipFrom,Shipto
       var CAccID = component.get("v.account_id");
       var CConID = component.get("v.client_contact");
       var CshipFrom = component.get("v.ship_from");
       var Cdestination = component.get("v.destination");
       var Cint_courier = component.get("v.int_courier");
       var CService_required = component.get("v.Service_required");
       var Cshipment_value = component.get("v.shipment_value");
       var Cref_1 = component.get("v.ref_1");
       var Cref_2 = component.get("v.ref_2");
       var CPO_Number = component.get("v.PO_Number");
       var Cchargeable_weight_estimate = component.get("v.chargeable_weight_estimate");
       var Cshipment_order_package_list = component.get("v.shipment_order_package_list");
       var CselectedPickupadddressID = component.get("v.selectedPickupadddressID");//selected pipckupaddress ID
       var CselectedFinaldeladddressIDs_List = component.get("v.selectedFinaldeladddressIDs"); // selected final delivery address Ids
       var CPart_List = component.get("v.parts_list");// part list 
       if(component.get("v.Secondhandgoods") != 'New'){ var CSecondhandgoods='Second Hand' } else {var CSecondhandgoods = component.get("v.Secondhandgoods")}
      // var CSecondhandgoods = component.get("v.Secondhandgoods");
       var CLi_ion_Batteries = component.get("v.Li_ion_Batteries");
       var CLi_ion_BatteryTypes = component.get("v.Li_ion_BatteryTypes");

        console.log('CPart_List',CPart_List);
        console.log('shipfrom',CshipFrom);
        console.log(Cdestination);
       var action = component.get("c.CreateCE");
     
       action.setParams({"PAccID" : CAccID,
                         "PConID" : CConID,
                         "PshipFrom" : CshipFrom,
                         "Pdestination" : Cdestination,
                         "Pint_courier" : Cint_courier,
                         "PService_required" : CService_required,
                         "Pshipment_value" : Cshipment_value,
                         "Pref_1" : Cref_1,
                         "Pref_2" : Cref_2,
                         "PPO_Number" : CPO_Number,
                         "Pchargeable_weight_estimate" : Cchargeable_weight_estimate,
                         "Pshipment_order_package_list" : Cshipment_order_package_list,
                         "PselectedPickupadddressID" : CselectedPickupadddressID,
                         "PselectedFinaldeladddressIDs_List" : CselectedFinaldeladddressIDs_List,
                         "PPart_List" : CPart_List,
                         "PSecondhandgoods" : CSecondhandgoods,
                         "PLi_ion_Batteries" : CLi_ion_Batteries,
                         "PLi_ion_BatteryTypes" : CLi_ion_BatteryTypes
                         
                        });
        action.setCallback(this, function(response) {  
            component.set("v.is_loading", false);
           var state = response.getState();
           if (state === 'SUCCESS'){
               
               var data = response.getReturnValue();
               component.set('v.CreatedCostEstimateID',data);
               var CEIDtoClose = component.get("v.CreatedCostEstimateID");
             
               var hostname = window.location.hostname;
               var arr = hostname.split(".");
               var instance = arr[0];
               component.set("v.iframeUrl2","https://"+instance+".lightning.force.com/lightning/r/Shipment_Order__c/"+ CEIDtoClose+"/view");
                var address =  component.get("v.iframeUrl2");
                window.open(address,'_top');
    
               
              }
           else{
               alert("CE not created, Please fill required information");
           }
           
       });
       $A.enqueueAction(action);
   },
    
//******** Fetch pickup address***********//
handlePickupAddChange : function(cmp, event, helper) {
        
        
    var a = event.getSource().get("v.ship_from");
    console.log('Ship from', a);
    cmp.set("v.isPickupaddressModelOpen",true);
    helper.fetchData(cmp,event, helper);
        
        
    },
    closepickupaddmodel : function(cmp, event, helper) {
        cmp.set("v.isPickupaddressModelOpen",false);
        },
    PickUpaddressPush: function(cmp, event, helper) {helper.createPickupAddObjectData(cmp, event);},
   
   //******** Final Delivery pickup address***********//
handleFinalDelAddChange : function(cmp, event, helper) {
        
    cmp.set("v.NumofFindelselected",0);     
    var a = event.getSource().get("v.destination");
    console.log('Destination', a);
    cmp.set("v.isfinaldeliveryaddModelOpen",true);
    helper.fetchFinalDelData(cmp,event, helper);
        
        
    },
    closeFinDeladdmodel : function(cmp, event, helper) {cmp.set("v.isfinaldeliveryaddModelOpen", false);},
    FinaddaddressPush: function(cmp, event, helper) {helper.createFinAddObjectData(cmp, event);},
   
  
 //******** Parts test***********// 

/*  
 handleMessage: function(component, message, helper) {
     var payload = message.getParams().payload;
     var name = payload.Parts;
      console.log('Parts data', name);
      console.log('Parts data', payload);

 },
*/   
    //******** Part ModelBOX  start***********//
    openPartModel : function(cmp, event, helper) {
        
        
    var a = event.getSource().get("v.Partvalue");
        if(a=="No"){
                    cmp.set("v.isPartModelOpen",'false');
                    }
        else{
            cmp.set("v.isPartModelOpen",'true');
            
        }
        
        
    },
    
    closePartModel : function(cmp, event, helper) {cmp.set("v.isPartModelOpen", false);},
    submitPartDetails : function(cmp, event, helper) {cmp.set("v.isPartModelOpen", false);},
//******** Part ModelBOX  End***********//
   
    //******** New pickup address insert start***********//
     OpenNewpickupaddInsertmodel : function(cmp, event, helper) { cmp.set("v.isNewPickAddOpen", true);   },
     closeNewpickupaddInsertmodel : function(cmp, event, helper) {  cmp.set("v.isNewPickAddOpen", false);  },
   
    addPickaddRow: function(component, event, helper) {
       helper.addAddressRecord(component, event);
       },
    
   removePickaddRow: function(component, event, helper) {
       //Get the account list
       var PickaddList = component.get("v.PickaddList");
       //Get the target object
       var selectedItem = event.currentTarget;
       //Get the selected item index
       var index = selectedItem.dataset.record;
       PickaddList.splice(index, 1);
       component.set("v.PickaddList", PickaddList);
   },
    savePickAddList: function(cmp, event, helper) {
        cmp.set("v.is_loading", true);
       //Call Apex class and pass account list parameters
       var action = cmp.get("c.CreatePickAdd");
       var CPickAddList = cmp.get("v.PickaddList");
       var CAccID = cmp.get("v.account_id");
        var self = this;
      
      console.log('CPickAddList',CPickAddList);  
         console.log('CAccID',CAccID);  
       
       action.setParams({
           "PPickAddList": CPickAddList,
           "PAccID": cmp.get("v.account_id")
          
       });
      
       action.setCallback(this, function(response) {
            cmp.set("v.is_loading", false);
           var state = response.getState();
           if (state === "SUCCESS") {
               cmp.set("v.PickaddList", []);
               cmp.set("v.isNewPickAddOpen", false); 
               helper.fetchData(cmp,event, helper);
               
           }
           else{
               
                alert('pickup address not created, Please send detailed email to support_SF@tecex.com');
               
           }
           
       }); 
       $A.enqueueAction(action);
   },
   
   
    
     //******** New pickup address insert ends***********//
     
      //******** New Final del address insert start***********//
     OpenNewFinalDeladdInsertmodel : function(cmp, event, helper) { cmp.set("v.isNewFinDelAddOpen", true);   },
     closeNewFinalDeladdInsertmodel : function(cmp, event, helper) {  cmp.set("v.isNewFinDelAddOpen", false);  },
   
    addFinalDeladdRow: function(component, event, helper) {
       helper.addFinaldelAddressRecord(component, event);
       },
    
   removeFinalDeladdRow: function(component, event, helper) {
       //Get the account list
       var FinalDeladdList = component.get("v.FinalDeladdList");
       //Get the target object
       var selectedItem = event.currentTarget;
       //Get the selected item index
       var index = selectedItem.dataset.record;
       FinalDeladdList.splice(index, 1);
       component.set("v.FinalDeladdList", FinalDeladdList);
   },
    saveFinalDelAddList: function(cmp, event, helper) {
        cmp.set("v.is_loading", true);
       //Call Apex class and pass account list parameters
       var action = cmp.get("c.CreatefinaldelAdd");
       var CFinalDeladdList = cmp.get("v.FinalDeladdList");
       var CAccID = cmp.get("v.account_id");
        var self = this;
      
      console.log('CFinalDeladdList',CFinalDeladdList);  
         console.log('CAccID',CAccID);  
       
       action.setParams({
           "PFinalDeladdList": CFinalDeladdList,
           "PAccID": cmp.get("v.account_id")
          
       });
      
       action.setCallback(this, function(response) {
            cmp.set("v.is_loading", false);
           var state = response.getState();
           if (state === "SUCCESS") {
               cmp.set("v.FinalDeladdList", []);
               cmp.set("v.isNewFinDelAddOpen", false); 
               helper.fetchFinalDelData(cmp,event, helper);
               
           }
           else{
               
                alert('Final del address not created, Please send detailed email to support_SF@tecex.com');
               
           }
           
       }); 
       $A.enqueueAction(action);
   },
   
   
    
     //******** New Final del address insert ends***********// 

})