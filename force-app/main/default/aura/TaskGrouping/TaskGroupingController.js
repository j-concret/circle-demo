({
  expandCollapseHandler : function(component,event,helper){
    component.set('v.isGrouped',!component.get('v.isGrouped'));
  }
})