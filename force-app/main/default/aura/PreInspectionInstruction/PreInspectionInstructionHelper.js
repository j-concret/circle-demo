({
  getTask : function(cmp,taskId) {
    var self = this;
    cmp.set('v.showSpinner',true);
    self.serverCall(cmp,'getTaskRecord',{
      taskId : taskId
    },function(task){
      cmp.set('v.showSpinner',false);
      cmp.set('v.task',task);
      if(task.State__c === 'Under Review'){
        cmp.set('v.type','tecex');
      }
    },function(err){
      cmp.set('v.showSpinner',false);
      self.showToast('error','ERROR',err);
    });

  },
  updateTaskState:function(cmp,state){
    var self = this;
    cmp.set('v.showSpinner',true);
    self.serverCall(cmp,'updateTask',{
      taskId : cmp.get('v.task').Id,
      state : state
    },function(response){
      cmp.set('v.showSpinner',false);
      if(state == 'Under Review')
        cmp.set('v.type','tecex');
      else{
        self.showToast('success','SUCCESS','Task Resolved');
        self.closeTab(cmp);
      }
    },function(err){
      cmp.set('v.showSpinner',false);
      self.showToast('error','ERROR',err);
    });
  },
  showToast : function(title,type,msg) {
    var toastEvent = $A.get('e.force:showToast');
    toastEvent.setParams({
      'title': title,
      'type' :type,
      'message': msg
    });
    toastEvent.fire();
  },
  closeTab : function(cmp){
    var workspaceAPI = cmp.find('workspace');
    var focusedTabId = cmp.get('v.tabId');

    if(focusedTabId){
      workspaceAPI.closeTab({tabId: focusedTabId});
    }
  }

});