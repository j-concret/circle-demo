({
  doInit: function(component,event,helper){
    //component.set('v.cval','<style>.slds-template__container{background-color: white;}</style>');
    var tskId = component.get('v.pageReference').state.c__tskId;
    var workspaceAPI = component.find('workspace');

    workspaceAPI.getAllTabInfo().then(function(response) {
      var focusedTabId;
      for(var i=0;i<response.length;i++){
          if(response[i].isSubtab && response[i].title == 'Loading...' && response[i].pageReference.attributes && response[i].pageReference.attributes.componentName && response[i].pageReference.attributes.componentName == 'c__PreInspectionInstruction'){
            focusedTabId = response[i].tabId;
            break;
          }else if(response[i].subtabs){
            var subtabs = response[i].subtabs;
            for(var j=0;j<subtabs.length;j++){
              if(subtabs[j].title === 'Loading...' && subtabs[j].pageReference.attributes && subtabs[j].pageReference.attributes.componentName && subtabs[j].pageReference.attributes.componentName == 'c__PreInspectionInstruction'){
                focusedTabId = subtabs[j].tabId;
                break;
              }
            }
          }
      }

      if(focusedTabId){
        component.set('v.tabId',focusedTabId);

        workspaceAPI.setTabLabel({
          tabId: focusedTabId,
          label: 'PreInspection'
        });
        workspaceAPI.setTabIcon({
          tabId: focusedTabId,
          icon: 'standard:document',
          iconAlt: 'PreInspection'
        });
      }

    }).catch(function(error) {
      console.log(error);
    });

    if(tskId){
      helper.getTask(component,tskId);
    }else{
      helper.showToast('error','ERROR','No task found.');
    }
  },
  handleSelection : function(component, event, helper) {
    var type = event.getSource().get('v.name');
    if(type === 'tecex'){
      helper.updateTaskState(component,'Under Review');
    }else
      component.set('v.type',type);
  },
  resolveTask :function(component, event, helper) {
    helper.updateTaskState(component,'Resolved');
  }
});