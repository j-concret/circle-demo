({
     doInit: function(component,event,helper){
        var eId = component.get('v.eId');
         console.log('isShipmentApplied '+component.get('v.isShipmentApplied'));
        if(!eId){
            var myPageRef = component.get("v.pageReference");
        	eId = myPageRef.state.c__enterID;
            if(!eId){
                helper.showToast('Error','error','Enterprise record Id is missing!');
                return;
            }
            component.set('v.eId',eId);
        }
         component.set('v.isShipmentLoading',true);
        helper.getShipmentsWithInvoices(component);
    },
    processShipments: function(component, event, helper) {
        //component.set('v.isShipmentApplied',false);
        component.set('v.isShipmentUpdateLoading',true);
        if(component.get('v.selectedShipmentRecordCount') > 0){
            helper.processShipmentObjects(component);
        }else{
            helper.fireToast('Info !', 'info', 'Select Shipment Order to process');
            component.set('v.isShipmentUpdateLoading',false); 
        }
        
    },
    showEmailComponent: function(component, event, helper) {
       component.set('v.isLoading',true); 
       helper.markInvoiceToSent(component);
    },
    toggleSOFields: function(component,event,helper){
        component.set('v.isSOVisible',!component.get('v.isSOVisible'));
    },
    toggleTecExCharges: function(component,event,helper){
        component.set('v.isTecExChargesVisible',!component.get('v.isTecExChargesVisible'));
    },
    toggleOnCharges: function(component,event,helper){
        component.set('v.isOnChargesVisible',!component.get('v.isOnChargesVisible'));
    },
    toggleVAService: function(component,event,helper){
        component.set('v.isVAServicesVisible',!component.get('v.isVAServicesVisible'));
    },
    toggleTax: function(component,event,helper){
        component.set('v.isTaxVisible',!component.get('v.isTaxVisible'));
    },
    toggleOther: function(component,event,helper){
        component.set('v.isOtherVisible',!component.get('v.isOtherVisible'));
    },
    onFilterSO: function(component, event, helper) {
        var shipmentObjects = component.get('v.shipmentObjects');
        var filter = component.get('v.filter');
        var filteredData = [];
        if(filter){
           
                filteredData = shipmentObjects.filter(data => {
                  return data.Name ? data.Name.toLowerCase().includes(filter.toLowerCase()) : false;
              });
                component.set('v.filteredShipmentObjects',filteredData);
          
        }else{
            component.set('v.filteredShipmentObjects',shipmentObjects);
        }
    }
})