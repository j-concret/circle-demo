({
	/** this function will be called on init of component
     * @param componentP this is the component we are initialising
     * @param eventP this is the event that occured
     * @param helperP this is helper class
     */
    doInit: function( componentP, eventP, helperP ){

       var get_records = componentP.get( "c.getAccountAndContact" );                    //action to return account and contact record

        console.log( "getting contact and account" );

        get_records.setCallback( this, function( response ){

            var state = response.getState();                            //get the state

            //check if we have success
            if( state === "SUCCESS" ){

                console.log( "Success getting account and contact" );

                var server_response = response.getReturnValue();                //get the return value
                var account_populate_method = componentP.find( "account_lookup" );              //get the account lookup field
                var contact_populate_method = componentP.find( "contact_lookup" );              //get the contact lookup field


                //check if we have an error
                if( server_response.is_error ){

                    this.showToastMessage( "error", "Error Occured", server_response.result_message );
                    return;                         //indicate failure

                }
                //check if auto populate is not required
                else if( server_response.result_message === "NA" ) {

                    console.log("In NA");
                    return;             //terminate function

                }//end of if-else block

                console.log( server_response.sobject_list[0] );
                console.log( server_response.sobject_list[1] );

               //console.log( "Before the auto set" );
               // console.log( "Account: " + account_populate_method );
                //console.log( "Test Value: " + componentP.get( "v.shipment_value" ) );

                componentP.set( "v.account_id", server_response.sobject_list[0].Id );
                componentP.set( "v.contact_id", server_response.sobject_list[1].Id ); // test
                
                account_populate_method.setSelectedRecord( server_response.sobject_list[0] );       //set the selected Account record
                contact_populate_method.setSelectedRecord( server_response.sobject_list[1] );       //set the selected contact record

                console.log( "Account ID: " + componentP.get( "v.account_id" ) ); 
                console.log( "COntact ID: " + componentP.get( "v.contact_id" ) );//

            }
            //check if have errors
            else if( state === "ERROR" )
                this.processServerErrors( response.getError() );             //process the server errors

        });//end of

        $A.enqueueAction( get_records );                    //execute the action

    },//end of function definition
    
    SONameChanged : function(component, event, helper) {
     
        var SelectedSOID = component.get( "v.ShipmentOrderId" );
      //  alert("Expense name changed from" +SelectedSOID);
        var action = component.get("c.getCreatedSIs");
        action.setParams({"SOID1" : SelectedSOID});
        action.setCallback(this, function(response) {          
            var state = response.getState();
            if (state === 'SUCCESS'){
                
                var data = response.getReturnValue();
                component.set('v.ExistingSupplierInvoices',data);
                console.log(data);
             //    alert("in success" +data);
                
               }
            else{
                alert("We are in fail block");
            }
            
        });
        $A.enqueueAction(action);
    }
})