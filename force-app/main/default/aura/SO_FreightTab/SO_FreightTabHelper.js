({
    
    activeSections: function(component, event){
        var openSections = component.find("FreightAccordion").get("v.activeSectionName");
        if(openSections.length>0){
            for(var i =0; i<openSections.length; i++){
                //toggle edit and view
                if(openSections[i]==="freight") {
                    var editFormFreight = component.find("editFormFreight");
                    $A.util.toggleClass(editFormFreight, "slds-hide");
                    var viewFormFreight = component.find("viewFormFreight");
                    $A.util.toggleClass(viewFormFreight, "slds-hide");
                }else if(openSections[i]==="pickUpAddress") {
                    var editFormPickUpAddress = component.find("editFormPickUpAddress");
                    $A.util.toggleClass(editFormPickUpAddress, "slds-hide");
                    var viewFormPickUpAddress = component.find("viewFormPickUpAddress");
                    $A.util.toggleClass(viewFormPickUpAddress, "slds-hide");
                }
                    else if(openSections[i]==="storage") {
                        var editFormStorage = component.find("editFormStorage");
                        $A.util.toggleClass(editFormStorage, "slds-hide");
                        var viewFormStorage = component.find("viewFormStorage");
                        $A.util.toggleClass(viewFormStorage, "slds-hide");
                    }
                        else if(openSections[i]==="productsBeingShipped") {
                            var editFormProductsBeingShipped = component.find("editFormProductsBeingShipped");
                            $A.util.toggleClass(editFormProductsBeingShipped, "slds-hide");
                            var viewFormProductsBeingShipped = component.find("viewFormProductsBeingShipped");
                            $A.util.toggleClass(viewFormProductsBeingShipped, "slds-hide");
                        }
                            else {
                                var editFormFinalDelivery = component.find("editFormFinalDelivery");
                                $A.util.toggleClass(editFormFinalDelivery, "slds-hide");
                                var viewFormFinalDelivery = component.find("viewFormFinalDelivery");
                                $A.util.toggleClass(viewFormFinalDelivery, "slds-hide");
                            }
            }}},
})