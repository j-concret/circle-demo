({
	init: function (cmp, event, helper) {
        cmp.set('v.columns', [
            
            {label: 'How many packages?', fieldName: 'packages_of_same_weight_dims__c', editable:'true', type: 'number'},
            {label: 'Weight Unit', fieldName: 'Weight_Unit__c', editable:'true', type: 'option'},
            {label: 'Actual Weight', fieldName: 'Actual_Weight__c', editable:'true', type: 'number'},
            {label: 'Dimension Unit', fieldName: 'Dimension_Unit__c', editable:'true', type: 'option'},
            {label: 'Height', fieldName: 'Height__c', editable:'true', type: 'number'},
            {label: 'Length', fieldName: 'Length__c', editable:'true', type: 'number'},
            {label: 'Breadth', fieldName: 'Breadth__c', editable:'true', type: 'number'}
           
        ]);    
        helper.fetchData(cmp,event, helper);
    },
     handleSaveEdition: function (cmp, event, helper) {
        var draftValues = event.getParam('draftValues');
        console.log(draftValues);
        var action = cmp.get("c.updateSOPackages");
        action.setParams({"editedAccountList" : draftValues});
        action.setCallback(this, function(response) {
            
            var state = response.getState();
            if (state === 'SUCCESS'){
            
          // alert("Shipment Order Packages are updated ");
           $A.get('e.force:refreshView').fire();
            }
            else{
                alert("We are in fail block");
            }
            
        });
        $A.enqueueAction(action);
        
    },
    /*page refresh after data save*/    
    isRefreshed: function(component, event, helper) {
       location.reload();
      
        //   $A.get('e.force:refreshView').fire(); 
    },
     
    
    
    
    
})