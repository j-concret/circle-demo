({
    checkboxSelect: function(cmp, event, helper) {
    
	},
	doInit : function(component, event, helper) {
        var cost = component.get('v.costing');
        
		component.set('v.selectedLookUpRecord', cost.Supplier__r);
        
	},
    recordTypeChange: function(component, event, helper) {
       var cost = component.get('v.costing');
        
        cost.Supplier__r = component.get('v.selectedLookUpRecord');
        cost.Supplier__c = component.get('v.selectedLookUpRecord').Id;
        component.set('v.costing',cost);
        
	},
})